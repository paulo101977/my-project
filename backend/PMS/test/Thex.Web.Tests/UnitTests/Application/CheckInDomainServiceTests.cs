﻿using NSubstitute;
using Thex.Domain.Interfaces.Repositories;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Thex.Domain.Services;
using Thex.Web.Tests.Mocks;
using Thex.Kernel;
using Microsoft.AspNetCore.Http;
using Thex.Domain.Interfaces.Repositories.Read;
using Tnf.Repositories.Uow;
using System;
using System.Linq;
using System.Collections.Generic;
using Thex.Common.Enumerations;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class CheckInDomainServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        private INotificationHandler _notification;
        private IGuestReservationItemRepository _guestReservationItemRepository;
        private IBillingAccountRepository _billingAccountRepository;
        private IApplicationUser _applicationUser;
        private IHttpContextAccessor _httpContextAccessor;
        private IGuestReservationItemReadRepository _guestReservationItemReadRepository;
        private IReservationItemReadRepository _reservationItemReadRepository;
        private IBillingAccountReadRepository _billingAccountReadRepository;
        private IReservationBudgetReadRepository _reservationBudgetReadRepository;
        private IReservationBudgetRepository _reservationBudgetRepository;
        private IReservationItemRepository _reservationItemRepository;
        private IBillingAccountItemReadRepository _billingAccountItemReadRepository;
        private IUnitOfWorkManager _unitOfWorkManager;

        public CheckInDomainServiceTests()
        {
            _notification = new NotificationHandler(ServiceProvider);
            LoadDependencies();
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestReservationItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        private void LoadDependencies()
        {
            _guestReservationItemRepository = Substitute.For<IGuestReservationItemRepository>();
            _billingAccountRepository = Substitute.For<IBillingAccountRepository>();
            _applicationUser = ServiceProvider.GetService<IApplicationUser>();
            _httpContextAccessor = ServiceProvider.GetService<IHttpContextAccessor>();
            _guestReservationItemReadRepository = Substitute.For<IGuestReservationItemReadRepository>();
            _reservationItemReadRepository = Substitute.For<IReservationItemReadRepository>();
            _billingAccountReadRepository = Substitute.For<IBillingAccountReadRepository>();
            _reservationBudgetRepository = Substitute.For<IReservationBudgetRepository>();
            _reservationBudgetReadRepository = Substitute.For<IReservationBudgetReadRepository>();
            _reservationItemRepository = Substitute.For<IReservationItemRepository>();
            _reservationItemRepository = Substitute.For<IReservationItemRepository>();
            _billingAccountItemReadRepository = Substitute.For<IBillingAccountItemReadRepository>();
            _unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
        }

        //TO DO: Create mock service provider
        //[Fact]
        public void Should_CreateGuestAccounts()
        {
            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                _billingAccountItemReadRepository, _unitOfWorkManager)
                .CreateGuestAccounts(CheckinDomainServiceMock.GetGuestReservationItemList(), CheckinDomainServiceMock.GetReservationId(),
                CheckinDomainServiceMock.GetPropertyId(), CheckinDomainServiceMock.GetBusinessSourceId(), CheckinDomainServiceMock.GetMarketSegmentId(),
                CheckinDomainServiceMock.GetBillingAccountList());

            Assert.False(_notification.HasNotification());
        }

        //TO DO: Create mock service provider
        //[Fact]
        public void Should_NotCreateGuestAccounts()
        {
            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                _billingAccountItemReadRepository, _unitOfWorkManager)
                .CreateGuestAccounts(CheckinDomainServiceMock.GetGuestReservationItemList(), CheckinDomainServiceMock.GetReservationId(),
                CheckinDomainServiceMock.GetPropertyId(), 0, 0, CheckinDomainServiceMock.GetBillingAccountList());

            Assert.True(_notification.HasNotification());
        }

        [Fact]
        public void Should_UpdateToCheckInGuestReservationItem()
        {
            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                _billingAccountItemReadRepository, _unitOfWorkManager)
                .UpdateToCheckInGuestReservationItem(CheckinDomainServiceMock.GetGuestReservationItemList(), CheckinDomainServiceMock.GetCheckinDate());

            Assert.False(_notification.HasNotification());
        }

        [Fact]
        public void Should_NotUpdateToCheckInGuestReservationItem()
        {
            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                   _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                   _billingAccountItemReadRepository, _unitOfWorkManager)
                   .UpdateToCheckInGuestReservationItem(CheckinDomainServiceMock.GetGuestReservationItemList(), new System.DateTime());

            Assert.True(_notification.HasNotification());
        }

        [Fact]
        public void Should_Cancel_ReservationItem()
        {
            var guestReservationItemList = CheckinDomainServiceMock.GetGuestReservationItemCheckinList();

            _guestReservationItemReadRepository.GetAllByReservationItemId(1).Returns(x =>
            {
                return guestReservationItemList;
            });

            var reservationItem = CheckinDomainServiceMock.GetReservationItemWithGuestReservationItemCheckinList();

            _reservationItemReadRepository.GetByIdWithGuestReservationItemList(1).Returns(x =>
            {
                return reservationItem;
            });

            _billingAccountReadRepository.GetAllIdByFilters(new List<long> { 1 }, BillingAccountTypeEnum.Guest).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountIdList();
            });

            _billingAccountReadRepository.GetIdByFilters(1, BillingAccountTypeEnum.Company).Returns(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountId();
            });

            _billingAccountItemReadRepository.GetAllIdByBillingAccountIdList(new List<Guid> { Guid.NewGuid() }).ReturnsForAnyArgs(x =>
            {
                return new List<Guid>();
            });

            _billingAccountReadRepository.GetByFilters(1, BillingAccountTypeEnum.GroupAccount).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            _reservationBudgetReadRepository.GetAllIdByFilters(1, DateTime.Now).ReturnsForAnyArgs(x =>
            {
                return new List<long>();
            });

            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                   _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                   _billingAccountItemReadRepository, _unitOfWorkManager)
                   .Cancel(1, DateTime.Now);

            var guestReservationItem = guestReservationItemList.FirstOrDefault();

            Assert.False(_notification.HasNotification());
            Assert.Null(guestReservationItem.CheckInDate);
            Assert.Null(guestReservationItem.CheckInHour);
            Assert.Null(guestReservationItem.GuestRegistrationId);
            Assert.Null(guestReservationItem.GuestId);
            Assert.Null(guestReservationItem.GuestEmail);
            Assert.Null(guestReservationItem.GuestDocumentTypeId);
            Assert.Null(guestReservationItem.GuestDocument);
            Assert.Equal((int)ReservationStatus.ToConfirm, guestReservationItem.GuestStatusId);
            Assert.Null(reservationItem.CheckInDate);
            Assert.Null(reservationItem.CheckInHour);
            Assert.Equal((int)ReservationStatus.ToConfirm, reservationItem.ReservationItemStatusId);
            _guestReservationItemRepository.Received().UpdateRange(guestReservationItemList);
        }

        [Fact]
        public void Should_Not_Cancel_When_Status_Is_Not_Checkin()
        {
            var guestReservationItemList = CheckinDomainServiceMock.GetGuestReservationItemCheckinAndCheckoutList();

            _guestReservationItemReadRepository.GetAllByReservationItemId(1).ReturnsForAnyArgs(x =>
            {
                return guestReservationItemList;
            });

            _reservationItemReadRepository.GetByIdWithGuestReservationItemList(1).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWithGuestReservationItemCheckinList();
            });

            _billingAccountReadRepository.GetAllIdByFilters(new List<long> { 1 }, BillingAccountTypeEnum.Guest).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountIdList();
            });

            _billingAccountItemReadRepository.GetAllIdByBillingAccountIdList(new List<Guid> { Guid.NewGuid() }).Returns(x =>
            {
                return new List<Guid>();
            });

            _billingAccountReadRepository.GetByFilters(1, BillingAccountTypeEnum.GroupAccount).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            _reservationBudgetReadRepository.GetAllIdByFilters(1, DateTime.Now).ReturnsForAnyArgs(x =>
            {
                return new List<long>();
            });

            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                   _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                   _billingAccountItemReadRepository, _unitOfWorkManager)
                   .Cancel(1, DateTime.Now);

            Assert.True(_notification.HasNotification());
            _guestReservationItemRepository.DidNotReceive().UpdateRange(guestReservationItemList);
        }

        [Fact]
        public void Should_Not_Cancel_When_Checkin_Date_Is_Difference_System_Date()
        {
            var guestReservationItemList = CheckinDomainServiceMock.GetGuestReservationItemCheckinList();

            _guestReservationItemReadRepository.GetAllByReservationItemId(1).Returns(x =>
            {
                return guestReservationItemList;
            });

            _reservationItemReadRepository.GetByIdWithGuestReservationItemList(1).Returns(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWithGuestReservationItemCheckinList();
            });

            _billingAccountReadRepository.GetAllIdByFilters(new List<long> { 1 }, BillingAccountTypeEnum.Guest).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountIdList();
            });

            _billingAccountItemReadRepository.GetAllIdByBillingAccountIdList(new List<Guid> { Guid.NewGuid() }).Returns(x =>
            {
                return new List<Guid>();
            });

            _billingAccountReadRepository.GetByFilters(1, BillingAccountTypeEnum.GroupAccount).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            _reservationBudgetReadRepository.GetAllIdByFilters(1, DateTime.Now).ReturnsForAnyArgs(x =>
            {
                return new List<long>();
            });

            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                   _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                   _billingAccountItemReadRepository, _unitOfWorkManager)
                   .Cancel(1, DateTime.Now.AddDays(-1));

            Assert.True(_notification.HasNotification());
            _guestReservationItemRepository.DidNotReceive().UpdateRange(guestReservationItemList);
        }

        [Fact]
        public void Should_Not_Cancel_When_ReservationItem_Is_Walkin()
        {
            var guestReservationItemList = CheckinDomainServiceMock.GetGuestReservationItemCheckinList();

            _guestReservationItemReadRepository.GetAllByReservationItemId(1).ReturnsForAnyArgs(x =>
            {
                return guestReservationItemList;
            });

            _reservationItemReadRepository.GetByIdWithGuestReservationItemList(1).Returns(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWalkin();
            });

            _billingAccountReadRepository.GetAllIdByFilters(new List<long> { 1 }, BillingAccountTypeEnum.Guest).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountIdList();
            });

            _billingAccountItemReadRepository.GetAllIdByBillingAccountIdList(new List<Guid> { Guid.NewGuid() }).Returns(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountItemIdList();
            });

            _billingAccountReadRepository.GetByFilters(1, BillingAccountTypeEnum.GroupAccount).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            _reservationBudgetReadRepository.GetAllIdByFilters(1, DateTime.Now).ReturnsForAnyArgs(x =>
            {
                return new List<long>();
            });

            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                   _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                   _billingAccountItemReadRepository, _unitOfWorkManager)
                   .Cancel(1, DateTime.Now.AddDays(-1));

            Assert.True(_notification.HasNotification());
            _guestReservationItemRepository.DidNotReceive().UpdateRange(guestReservationItemList);
        }

        [Fact]
        public void Should_Not_Cancel_ReservationItem_When_Account_Has_Item()
        {
            var guestReservationItemList = CheckinDomainServiceMock.GetGuestReservationItemCheckinList();

            _guestReservationItemReadRepository.GetAllByReservationItemId(1).Returns(x =>
            {
                return guestReservationItemList;
            });

            var reservationItem = CheckinDomainServiceMock.GetReservationItemWithGuestReservationItemCheckinList();

            _reservationItemReadRepository.GetByIdWithGuestReservationItemList(1).Returns(x =>
            {
                return reservationItem;
            });

            _billingAccountReadRepository.GetAllIdByFilters(new List<long> { 1 }, BillingAccountTypeEnum.Guest).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountIdList();
            });

            _billingAccountReadRepository.GetIdByFilters(1, BillingAccountTypeEnum.Company).Returns(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountId();
            });

            _billingAccountItemReadRepository.GetAllIdByBillingAccountIdList(new List<Guid> { Guid.NewGuid() }).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountItemIdList();
            });

            _billingAccountReadRepository.GetByFilters(1, BillingAccountTypeEnum.GroupAccount).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            _reservationBudgetReadRepository.GetAllIdByFilters(1, DateTime.Now).ReturnsForAnyArgs(x =>
            {
                return new List<long>();
            });

            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                   _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                   _billingAccountItemReadRepository, _unitOfWorkManager)
                   .Cancel(1, DateTime.Now);

            var guestReservationItem = guestReservationItemList.FirstOrDefault();

            Assert.True(_notification.HasNotification());
            _billingAccountReadRepository.DidNotReceive().GetByFilters(reservationItem.ReservationId, BillingAccountTypeEnum.GroupAccount);
        }

        [Fact]
        public void Should_Cancel_ReservationItem_In_Group_With_Two_Checkin()
        {
            _guestReservationItemReadRepository.GetAllByReservationItemId(1).Returns(x =>
            {
                return CheckinDomainServiceMock.GetGuestReservationItemCheckinList();
            });

            _reservationItemReadRepository.GetByIdWithGuestReservationItemList(1).Returns(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWithGuestReservationItemCheckinList();
            });

            _billingAccountReadRepository.GetAllIdByFilters(new List<long> { 1 }, BillingAccountTypeEnum.Guest).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountIdList();
            });

            _billingAccountReadRepository.GetByFilters(1, BillingAccountTypeEnum.GroupAccount).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountGroup();
            });

            _billingAccountItemReadRepository.GetAllIdByBillingAccountIdList(new List<Guid> { Guid.NewGuid() }).ReturnsForAnyArgs(x =>
            {
                return new List<Guid>();
            });

            _reservationItemReadRepository.GetAllByReservationId(1).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWithTwoCheckinList();
            });

            _billingAccountItemReadRepository.AnyByBillingAccountId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            _reservationBudgetReadRepository.GetAllIdByFilters(1, DateTime.Now).ReturnsForAnyArgs(x =>
            {
                return new List<long>();
            });

            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                   _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                   _billingAccountItemReadRepository, _unitOfWorkManager)
                   .Cancel(1, DateTime.Now);

            Assert.False(_notification.HasNotification());
        }

        [Fact]
        public void Should_Cancel_ReservationItem_In_Group_With_One_Checkin_And_One_Checkout()
        {
            _guestReservationItemReadRepository.GetAllByReservationItemId(1).Returns(x =>
            {
                return CheckinDomainServiceMock.GetGuestReservationItemCheckinList();
            });

            _reservationItemReadRepository.GetByIdWithGuestReservationItemList(1).Returns(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWithGuestReservationItemCheckinList();
            });

            _billingAccountReadRepository.GetAllIdByFilters(new List<long> { 1 }, BillingAccountTypeEnum.Guest).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountIdList();
            });

            _billingAccountReadRepository.GetByFilters(1, BillingAccountTypeEnum.GroupAccount).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountGroup();
            });

            _billingAccountItemReadRepository.GetAllIdByBillingAccountIdList(new List<Guid> { Guid.NewGuid() }).ReturnsForAnyArgs(x =>
            {
                return new List<Guid>();
            });

            _reservationItemReadRepository.GetAllByReservationId(1).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWithOneCheckinAndOneCheckoutList();
            });

            _billingAccountItemReadRepository.AnyByBillingAccountId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            _reservationBudgetReadRepository.GetAllIdByFilters(1, DateTime.Now).ReturnsForAnyArgs(x =>
            {
                return new List<long>();
            });

            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                   _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                   _billingAccountItemReadRepository, _unitOfWorkManager)
                   .Cancel(1, DateTime.Now);

            Assert.False(_notification.HasNotification());
        }

        [Fact]
        public void Should_Cancel_ReservationItem_In_Group_With_One_Checkin_And_One_Pending()
        {
            _guestReservationItemReadRepository.GetAllByReservationItemId(1).Returns(x =>
            {
                return CheckinDomainServiceMock.GetGuestReservationItemCheckinList();
            });

            _reservationItemReadRepository.GetByIdWithGuestReservationItemList(1).Returns(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWithGuestReservationItemCheckinList();
            });

            _billingAccountReadRepository.GetAllIdByFilters(new List<long> { 1 }, BillingAccountTypeEnum.Guest).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountIdList();
            });

            _billingAccountReadRepository.GetByFilters(1, BillingAccountTypeEnum.GroupAccount).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountGroup();
            });

            _billingAccountItemReadRepository.GetAllIdByBillingAccountIdList(new List<Guid> { Guid.NewGuid() }).ReturnsForAnyArgs(x =>
            {
                return new List<Guid>();
            });

            _reservationItemReadRepository.GetAllByReservationId(1).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWithOneCheckinAndOnePendingList();
            });

            _billingAccountItemReadRepository.AnyByBillingAccountId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            _reservationBudgetReadRepository.GetAllIdByFilters(1, DateTime.Now).ReturnsForAnyArgs(x =>
            {
                return new List<long>();
            });

            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                   _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                   _billingAccountItemReadRepository, _unitOfWorkManager)
                   .Cancel(1, DateTime.Now);

            Assert.False(_notification.HasNotification());
        }

        [Fact]
        public void Should_Not_Cancel_ReservationItem_In_Group()
        {
            _guestReservationItemReadRepository.GetAllByReservationItemId(1).Returns(x =>
            {
                return CheckinDomainServiceMock.GetGuestReservationItemCheckinList();
            });

            _reservationItemReadRepository.GetByIdWithGuestReservationItemList(1).Returns(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWithGuestReservationItemCheckinList();
            });

            _billingAccountReadRepository.GetAllIdByFilters(new List<long> { 1 }, BillingAccountTypeEnum.Guest).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountIdList();
            });

            _billingAccountReadRepository.GetByFilters(1, BillingAccountTypeEnum.GroupAccount).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetBillingAccountGroup();
            });

            _billingAccountItemReadRepository.GetAllIdByBillingAccountIdList(new List<Guid> { Guid.NewGuid() }).ReturnsForAnyArgs(x =>
            {
                return new List<Guid>();
            });

            _reservationItemReadRepository.GetAllByReservationId(1).ReturnsForAnyArgs(x =>
            {
                return CheckinDomainServiceMock.GetReservationItemWithOneCheckinAndOneConfirmedList();
            });

            _billingAccountItemReadRepository.AnyByBillingAccountId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            _reservationBudgetReadRepository.GetAllIdByFilters(1, DateTime.Now).ReturnsForAnyArgs(x =>
            {
                return new List<long>();
            });

            new CheckinDomainService(_notification, _guestReservationItemRepository, _billingAccountRepository, _applicationUser, _guestReservationItemReadRepository, _reservationItemReadRepository,
                   _billingAccountReadRepository, _reservationBudgetReadRepository, _reservationBudgetRepository, _reservationItemRepository,
                   _billingAccountItemReadRepository, _unitOfWorkManager)
                   .Cancel(1, DateTime.Now);

            Assert.True(_notification.HasNotification());
        }

    }
}
