﻿using Shouldly;
using Thex.Infra.ReadInterfaces;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using Thex.Domain.Services.Interfaces;
using Thex.Domain.Interfaces.Services;
using Thex.Application.Interfaces;
using Tnf.Repositories.Uow;
using Thex.Kernel;
using Thex.Domain.Interfaces.Repositories;
using Thex.Common.Interfaces.Observables;
using Thex.Inventory.Domain.Services;
using Thex.Application.Services;
using System.Threading.Tasks;
using NSubstitute;
using Thex.Application.Adapters;
using System;
using Thex.EntityFrameworkCore.ReadInterfaces.Repositories;
using Thex.EntityFrameworkCore.Utils;
using Thex.Domain.Entities;
using Thex.Dto;
using Microsoft.EntityFrameworkCore;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class RoomAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        public INotificationHandler _notificationHandler;
        private EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;
        private IPropertyContractReadRepository _propertyContractReadRepository;
        private IRoomAdapter _roomAdapter;
        private IRoomDomainService _roomDomainService;
        private IIgnoreAuditedEntity _ignoreAuditedEntity;
        private IPropertyParameterReadRepository _propertyParameterReadRepository;
        private IUnitOfWorkManager _unitOfWorkManager;
        private IApplicationUser _applicationUser;

        public RoomAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
            LoadDependencyMocks();
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomTypeDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingStatusPropertyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyBaseRateReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestReservationItemAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationBudgetDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomBlockingReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomTypeDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyContractReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICreateRoomObservable>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomChangeRoomTypeObservable>().ShouldNotBeNull();
            ServiceProvider.GetService<IDeleteRoomObservable>().ShouldNotBeNull();
            ServiceProvider.GetService<Inventory.Domain.Repositories.Interfaces.IPropertyParameterRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<Inventory.Domain.Repositories.Interfaces.IRoomTypeInventoryRepository>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Get_Total_And_Available_Rooms()
        {
            _roomReadRepository.CountByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return 10;
            });

            _propertyContractReadRepository.GetTotalRoomsByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return 100;
            });

            var response = new RoomAppService(_roomAdapter, _roomReadRepository, _roomDomainService, null, null, null, null, null, null, null, null, _notificationHandler,
                                        null, ServiceProvider, null, _propertyContractReadRepository, null, null, null).GetTotalAndAvailableRoomsByPropertyAsync();

            Assert.True(!_notificationHandler.HasNotification() && response != null);
        }

        [Fact]
        public void Should_Delete_Room()
        {
            _ignoreAuditedEntity.SetEntityName("Test");

            _propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return DateTime.Now;
            });

            _roomReadRepository.GetRoomTypeIdByRoomId(1).ReturnsForAnyArgs(x =>
            {
                return 1;
            });

            _roomDomainService.Delete(new Room());

            new RoomAppService(_roomAdapter, _roomReadRepository, _roomDomainService, null, null, null, null, null, null, null, _propertyParameterReadRepository, _notificationHandler,
                                       null, ServiceProvider, _unitOfWorkManager, _propertyContractReadRepository, _ignoreAuditedEntity, null, _applicationUser).DeleteRoom(1);

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Not_Delete_Room()
        {
            _ignoreAuditedEntity.SetEntityName("Test");

            _propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return DateTime.Now;
            });

            _roomReadRepository.GetRoomTypeIdByRoomId(1).ReturnsForAnyArgs(x =>
            {
                return 1;
            });

            _roomDomainService.WhenForAnyArgs(x => x.Delete(r => r.Id == 1)).Do(x =>
            {
                throw new DbUpdateException("Teste de exceção de UK", new Exception());
            });

            new RoomAppService(_roomAdapter, _roomReadRepository, _roomDomainService, null, null, null, null, null, null, null, _propertyParameterReadRepository, _notificationHandler,
                                       null, ServiceProvider, _unitOfWorkManager, _propertyContractReadRepository, _ignoreAuditedEntity, null, _applicationUser).DeleteRoom(1);

            Assert.True(_notificationHandler.HasNotification());
        }

        private void LoadDependencyMocks()
        {
            _roomReadRepository = Substitute.For<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>();
            _propertyContractReadRepository = Substitute.For<IPropertyContractReadRepository>();
            _roomAdapter = Substitute.For<IRoomAdapter>();
            _roomDomainService = Substitute.For<IRoomDomainService>();
            _ignoreAuditedEntity = Substitute.For<IIgnoreAuditedEntity>();
            _propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            _unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            _applicationUser = ServiceProvider.GetService<IApplicationUser>();
        }
    }
}
