﻿using Shouldly;
using System;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.Application.Adapters;
using Thex.Domain.Interfaces.Services;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Thex.Application.Services;
using NSubstitute;
using Thex.Domain.Entities;
using Thex.Web.Tests.Mocks;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Dto;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class ChannelAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        public INotificationHandler _notificationHandler;

        public ChannelAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IChannelAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IChannelDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IChannelReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Create()
        {
            var channelDomainService = Substitute.For<IChannelDomainService>();
            var channelAdapter = Substitute.For<IChannelAdapter>();

            channelDomainService.InsertAndSaveChanges(Channel.Create(_notificationHandler)).ReturnsForAnyArgs(x => 
            {
               return ChannelMock.Get();
            });

            var dto = new ChannelAppService(channelAdapter, channelDomainService, null, null, _notificationHandler)
                                                    .CreateChannel(ChannelMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Update()
        {
            var channelDomainService = Substitute.For<IChannelDomainService>();
            var channelReadRepository = Substitute.For<IChannelReadRepository>();
            var channelAdapter = Substitute.For<IChannelAdapter>();
            var applicationUser = Substitute.For<IApplicationUser>();

            channelDomainService.Update(Channel.Create(_notificationHandler));

            channelReadRepository.GetDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ChannelMock.GetDto();
            });

            channelReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ChannelMock.Get();
            });

            var dto = new ChannelAppService(channelAdapter, channelDomainService, channelReadRepository, applicationUser, _notificationHandler)
                                                    .UpdateChannel(Guid.NewGuid(), ChannelMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Delete()
        {
            var channelDomainService = Substitute.For<IChannelDomainService>();
            var channelReadRepository = Substitute.For<IChannelReadRepository>();
            var channelAdapter = Substitute.For<IChannelAdapter>();
            var applicationUser = Substitute.For<IApplicationUser>();

            channelDomainService.DeleteChannel(Channel.Create(_notificationHandler));

            channelReadRepository.GetDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ChannelMock.GetDto();
            });

            channelReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ChannelMock.Get();
            });

            new ChannelAppService(channelAdapter, channelDomainService, channelReadRepository, applicationUser, _notificationHandler)
                                                   .DeleteChannel(Guid.NewGuid());


            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Toggle()
        {
            var channelDomainService = Substitute.For<IChannelDomainService>();
            var channelReadRepository = Substitute.For<IChannelReadRepository>();
            var channelAdapter = Substitute.For<IChannelAdapter>();
            var applicationUser = Substitute.For<IApplicationUser>();

            channelDomainService.ToggleAndSaveIsActive(Guid.NewGuid());

            channelReadRepository.GetDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ChannelMock.GetDto();
            });

            channelReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ChannelMock.Get();
            });

            new ChannelAppService(channelAdapter, channelDomainService, channelReadRepository, applicationUser, _notificationHandler)
                                                   .ToggleAndSaveIsActive(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_GetById()
        {
            var channelDomainService = Substitute.For<IChannelDomainService>();
            var channelReadRepository = Substitute.For<IChannelReadRepository>();
            var channelAdapter = Substitute.For<IChannelAdapter>();
            var applicationUser = Substitute.For<IApplicationUser>();

            channelReadRepository.GetDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ChannelMock.GetDto();
            });

            channelReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ChannelMock.Get();
            });

            new ChannelAppService(channelAdapter, channelDomainService, channelReadRepository, applicationUser, _notificationHandler)
                                                   .GetDtoById(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_GetAllByTenant()
        {
            var channelDomainService = Substitute.For<IChannelDomainService>();
            var channelReadRepository = Substitute.For<IChannelReadRepository>();
            var channelAdapter = Substitute.For<IChannelAdapter>();
            var applicationUser = Substitute.For<IApplicationUser>();

            channelReadRepository.GetAllChannelsDtoByTenantIdAsync(new GetAllChannelDto(), Guid.NewGuid()).ReturnsForAnyArgs(async x =>
            {
                return await Task.FromResult(ChannelMock.GetIListDto());
            });

            await new ChannelAppService(channelAdapter, channelDomainService, channelReadRepository, applicationUser, _notificationHandler)
                                                   .GetAllChannelsDtoByTenantIdAsync(new GetAllChannelDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Search()
        {
            var channelDomainService = Substitute.For<IChannelDomainService>();
            var channelReadRepository = Substitute.For<IChannelReadRepository>();
            var channelAdapter = Substitute.For<IChannelAdapter>();
            var applicationUser = Substitute.For<IApplicationUser>();

            channelReadRepository.SearchChannels("test").ReturnsForAnyArgs(x =>
            {
                return ChannelMock.GetIListDto();
            });

            new ChannelAppService(channelAdapter, channelDomainService, channelReadRepository, applicationUser, _notificationHandler)
                                                   .SearchChannel("test");

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
