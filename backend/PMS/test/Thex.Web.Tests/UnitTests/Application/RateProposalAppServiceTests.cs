﻿using Shouldly;
using Thex.Application.Interfaces;
using Thex.Infra.ReadInterfaces;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Tnf.Notifications;
using Thex.Domain.Interfaces.Repositories;
using Tnf.Localization;
using Thex.Application.Services.RateProposal;
using Thex.Web.Tests.Mocks.RateProposal;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class RateProposalAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {   
        public INotificationHandler _notificationHandler;        

        public RateProposalAppServiceTests()
        {          
            _notificationHandler = new NotificationHandler(ServiceProvider);           
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterReadRepository>().ShouldNotBeNull();         
            ServiceProvider.GetService<IRoomTypeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyBaseRateReadRepository>().ShouldNotBeNull();           
            ServiceProvider.GetService<IReservationItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationBudgetRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationBudgetReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocalizationManager>().ShouldNotBeNull();                 
            ServiceProvider.GetService<IPropertyMealPlanTypeAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomTypeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICompanyClientChannelReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICustomerStationReadRepository>().ShouldNotBeNull();            
            ServiceProvider.GetService<ICompanyClientReadRepository>().ShouldNotBeNull();            
            ServiceProvider.GetService<IReservationBudgetAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async void Should_GetAllByProperty()
        {
            var roomTypeReadRepository = Substitute.For<IRoomTypeReadRepository>();
            var reservationBudgetAppService = Substitute.For<IReservationBudgetAppService>();
            var rateProposalAppService = Substitute.For<IChannelReadRepository>();
            var requestDto = new Dto.ReservationItemBudgetRequestDto();
            var propertyId = 1;

            reservationBudgetAppService.GetWithBudgetOffer(requestDto, propertyId).ReturnsForAnyArgs(x =>
            {
                return RateProposalMock.GetDto();
            });

            await new RateProposalAppService(roomTypeReadRepository, reservationBudgetAppService).GetAllhBudgetOfferWithRateProposal(requestDto, propertyId);

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
