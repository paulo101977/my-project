﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Application.Adapters;
using Thex.Application.Services;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Thex.Web.Tests.Mocks.LevelRate;
using Tnf.AspNetCore.TestBase;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class LevelRateAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        public INotificationHandler _notificationHandler;
        private ILevelRateHeaderReadRepository _levelRateHeaderReadRepository;
        private IApplicationUser _applicationUser;
        private IPropertyMealPlanTypeRateReadRepository _propertyMealPlanTypeRateReadRepository;
        private ILevelRateHeaderRepository _levelRateHeaderRepository;
        private ILevelRateHeaderAdapter _levelRateHeaderAdapter;
        private IDomainService<LevelRateHeader> _levelRateHeaderDomainService;
        private IUnitOfWorkManager _unitOfWorkManager;
        private ILevelRateRepository _levelRateRepository;
        private IRoomTypeReadRepository _roomTypeReadRepository;
        private IPropertyMealPlanTypeReadRepository _propertyMealPlanTypeReadRepository;
        private ILevelRateAdapter _levelRateAdapter;
        private IDomainService<LevelRate> _levelRateDomainService;

        public LevelRateAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
            LoadDependencies();
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<ILevelRateAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<ILevelRateHeaderAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<ILevelRateHeaderReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ILevelRateHeaderRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ILevelRateRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyMealPlanTypeRateReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyMealPlanTypeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomTypeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IDomainService<LevelRate>>().ShouldNotBeNull();
            ServiceProvider.GetService<IDomainService<LevelRateHeader>>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Create()
        {
            SetupMockSuccessCreate();

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Create(LevelRateMock.GetLevelResultDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Not_Create_When_Level_Is_Not_Available()
        {
            SetupMockSuccessCreate();

            _levelRateHeaderReadRepository.LevelIsAvailable(DateTime.MinValue, DateTime.MaxValue,
                          Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), 1).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Create(LevelRateMock.GetLevelResultDto());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Create_When_Any_Adult_Amount_Is_Not_Fill()
        {
            SetupMockSuccessCreate();

            var dto = LevelRateMock.GetLevelResultDto();
            dto.LevelRateList.FirstOrDefault().Pax1Amount = null;

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Create(dto);

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Create_When_Any_Adult_Amount_Is_Invalid()
        {
            SetupMockSuccessCreate();

            var dto = LevelRateMock.GetLevelResultDto();
            dto.LevelRateList.FirstOrDefault(lr => lr.RoomTypeId == 1).Pax1Amount = 50;

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Create(dto);

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Create_When_Any_MealPlan_Amount_Is_Not_Fill()
        {
            SetupMockSuccessCreate();

            var dto = LevelRateMock.GetLevelResultDto();
            dto.LevelRateMealPlanTypeList.FirstOrDefault().AdultMealPlanAmount = null;

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Create(dto);

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Update()
        {
            SetupMockSucessUpdate();

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Update(LevelRateMock.GetLevelResultDto(), Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Not_Update_When_LevelRate_Not_Found()
        {
            SetupMockSucessUpdate();

            var dto = LevelRateMock.GetLevelResultDto();
            _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Update(dto, Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Update_When_Permission_Is_Denied()
        {
            SetupMockSucessUpdate();

            var dto = LevelRateMock.GetLevelResultDto();
            _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return LevelRateHeader.Create(_notificationHandler)
                     .WithCurrencyId(Guid.NewGuid())
                     .WithInitialDate(DateTime.Now)
                     .WithEndDate(DateTime.Now.AddDays(2))
                     .WithRateName("Teste")
                     .WithTenantId(Guid.NewGuid())
                     .Build();
            });

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Update(dto, Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Update_When_LevelRate_Already_Used()
        {
            SetupMockSucessUpdate();

            var dto = LevelRateMock.GetLevelResultDto();
            _levelRateHeaderReadRepository.LevelRateAlreadyUsed(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Update(dto, Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Update_When_Level_Is_Not_Available()
        {
            SetupMockSucessUpdate();

            var dto = LevelRateMock.GetLevelResultDto();
            _levelRateHeaderReadRepository.LevelIsAvailable(DateTime.MinValue, DateTime.MaxValue,
                          Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), 1).ReturnsForAnyArgs(x =>
                          {
                              return false;
                          });

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Update(dto, Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Update_When_Any_Adult_Amount_Is_Not_Fill()
        {
            SetupMockSucessUpdate();

            var dto = LevelRateMock.GetLevelResultDto();
            dto.LevelRateList.FirstOrDefault().Pax1Amount = null;

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Update(dto, Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Update_When_Any_Adult_Amount_Is_Invalid()
        {
            SetupMockSucessUpdate();

            var dto = LevelRateMock.GetLevelResultDto();
            dto.LevelRateList.FirstOrDefault(lr => lr.RoomTypeId == 1).Pax1Amount = 50;

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Update(dto, Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Update_When_Any_MealPlan_Amount_Is_Not_Fill()
        {
            SetupMockSucessUpdate();

            var dto = LevelRateMock.GetLevelResultDto();
            dto.LevelRateMealPlanTypeList.FirstOrDefault().AdultMealPlanAmount = null;

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Update(dto, Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_GetById()
        {
            _levelRateHeaderReadRepository.GetDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return new LevelRateResultDto();
            });

            var dto = new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
                _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
                _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
                _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).GetById(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_GetAll()
        {
            _levelRateHeaderReadRepository.GetAllDto(new Guid()).ReturnsForAnyArgs(x =>
            {
                return new List<LevelRateHeaderDto>();
            });

            var dto = new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
                _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
                _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
                _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).GetAllDto();

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_GetAllLevelsAvailable()
        {
            _levelRateHeaderReadRepository.GetAllLevelsAvailable(new LevelRateRequestDto(), 1).ReturnsForAnyArgs(x =>
            {
                return new List<LevelDto>();
            });

            _propertyMealPlanTypeRateReadRepository.ExistsForAllPeriod(Guid.NewGuid(), 1, new List<DateTime>()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            var dto = new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).GetAllLevelsAvailable(new LevelRateRequestDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_ToggleIsActive()
        {
            _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return LevelRateHeader.Create(_notificationHandler)
                       .WithCurrencyId(Guid.NewGuid())
                       .WithInitialDate(DateTime.Now)
                       .WithEndDate(DateTime.Now.AddDays(2))
                       .WithRateName("Teste")
                       .WithTenantId(new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d"))
                       .Build();
            });

            _levelRateHeaderReadRepository.LevelIsAvailable(DateTime.MinValue, DateTime.MaxValue,
                            Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), 1).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            _levelRateHeaderRepository.ToggleIsActive(Guid.NewGuid());

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).ToggleActive(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Not_ToggleIsActive_When_LevelRate_Not_Found()
        {
            _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            _levelRateHeaderReadRepository.LevelIsAvailable(DateTime.MinValue, DateTime.MaxValue,
                            Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), 1).ReturnsForAnyArgs(x =>
                            {
                                return true;
                            });

            _levelRateHeaderRepository.ToggleIsActive(Guid.NewGuid());

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).ToggleActive(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_ToggleIsActive_When_Permission_Is_Denied()
        {
            _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return LevelRateHeader.Create(_notificationHandler)
                       .WithCurrencyId(Guid.NewGuid())
                       .WithInitialDate(DateTime.Now)
                       .WithEndDate(DateTime.Now.AddDays(2))
                       .WithRateName("Teste")
                       .WithTenantId(Guid.NewGuid())
                       .Build();
            });

            _levelRateHeaderRepository.ToggleIsActive(Guid.NewGuid());

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).ToggleActive(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_ToggleIsActive_When_LevelRate_Is_Not_Available()
        {
                _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
                {
                    return LevelRateHeader.Create(_notificationHandler)
                           .WithCurrencyId(Guid.NewGuid())
                           .WithInitialDate(DateTime.Now)
                           .WithEndDate(DateTime.Now.AddDays(2))
                           .WithRateName("Teste")
                           .WithTenantId(new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d"))
                           .Build();
                });

            _levelRateHeaderReadRepository.LevelIsAvailable(DateTime.MinValue, DateTime.MaxValue,
                            Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), 1).ReturnsForAnyArgs(x =>
                            {
                                return false;
                            });

            _levelRateHeaderRepository.ToggleIsActive(Guid.NewGuid());

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).ToggleActive(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Remove()
        {
            _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return LevelRateHeader.Create(_notificationHandler)
                       .WithCurrencyId(Guid.NewGuid())
                       .WithInitialDate(DateTime.Now)
                       .WithEndDate(DateTime.Now.AddDays(2))
                       .WithRateName("Teste")
                       .WithTenantId(new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d"))
                       .Build();
            });

            _levelRateHeaderReadRepository.LevelRateAlreadyUsed(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            _levelRateHeaderRepository.RemoveAndSaveChanges(new LevelRateHeader());

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
              _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
              _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
              _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Remove(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Not_Remove_When_LevelRate_Not_Found()
        {
            _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            _levelRateHeaderReadRepository.LevelRateAlreadyUsed(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            _levelRateHeaderRepository.RemoveAndSaveChanges(new LevelRateHeader());

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Remove(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Remove_When_Permission_Is_Denied()
        {
            _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return LevelRateHeader.Create(_notificationHandler)
                       .WithCurrencyId(Guid.NewGuid())
                       .WithInitialDate(DateTime.Now)
                       .WithEndDate(DateTime.Now.AddDays(2))
                       .WithRateName("Teste")
                       .WithTenantId(Guid.NewGuid())
                       .Build();
            });

            _levelRateHeaderRepository.RemoveAndSaveChanges(new LevelRateHeader());

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Remove(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        [Fact]
        public void Should_Not_Remove_When_LevelRate_Used()
        {
            _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return LevelRateHeader.Create(_notificationHandler)
                       .WithCurrencyId(Guid.NewGuid())
                       .WithInitialDate(DateTime.Now)
                       .WithEndDate(DateTime.Now.AddDays(2))
                       .WithRateName("Teste")
                       .WithTenantId(new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d"))
                       .Build();
            });

            _levelRateHeaderReadRepository.LevelRateAlreadyUsed(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            _levelRateHeaderRepository.RemoveAndSaveChanges(new LevelRateHeader());

            new LevelRateAppService(_levelRateAdapter, _levelRateHeaderAdapter, _applicationUser, _levelRateHeaderReadRepository,
               _levelRateHeaderRepository, _levelRateRepository, _propertyMealPlanTypeRateReadRepository,
               _propertyMealPlanTypeReadRepository, _roomTypeReadRepository, _levelRateDomainService,
               _levelRateHeaderDomainService, _unitOfWorkManager, _notificationHandler).Remove(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification() && _notificationHandler.GetAll().Count() == 1);
        }

        private void LoadDependencies()
        {
            _levelRateAdapter = Substitute.For<ILevelRateAdapter>();
            _levelRateHeaderAdapter = Substitute.For<ILevelRateHeaderAdapter>();
            _levelRateHeaderReadRepository = Substitute.For<ILevelRateHeaderReadRepository>();
            _levelRateHeaderRepository = Substitute.For<ILevelRateHeaderRepository>();
            _levelRateRepository = Substitute.For<ILevelRateRepository>();
            _propertyMealPlanTypeRateReadRepository = Substitute.For<IPropertyMealPlanTypeRateReadRepository>();
            _propertyMealPlanTypeReadRepository = Substitute.For<IPropertyMealPlanTypeReadRepository>();
            _roomTypeReadRepository = Substitute.For<IRoomTypeReadRepository>();
            _levelRateDomainService = Substitute.For<IDomainService<LevelRate>>();
            _levelRateHeaderDomainService = Substitute.For<IDomainService<LevelRateHeader>>();
            _unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            _applicationUser = ServiceProvider.GetService<IApplicationUser>();
        }


        private IListDto<RoomTypeDto> GetRoomTypeList()
        {
            var roomTypeList = new List<RoomTypeDto>();
            roomTypeList.Add(new RoomTypeDto
            {
                Id = 1,
                AdultCapacity = 2,
                ChildCapacity = 1,
                MaximumRate = 400,
                MinimumRate = 150
            });

            roomTypeList.Add(new RoomTypeDto
            {
                Id = 2,
                AdultCapacity = 1,
                ChildCapacity = 0,
                MaximumRate = 500,
                MinimumRate = 200
            });

            roomTypeList.Add(new RoomTypeDto
            {
                Id = 3,
                AdultCapacity = 3,
                ChildCapacity = 2,
                MaximumRate = 600,
                MinimumRate = 250
            });

            return new ListDto<RoomTypeDto>
            {
                HasNext = false,
                Items = roomTypeList
            };
        }

        private ICollection<int> GetPropertyMealPlanTypeList()
            => new List<int> { 1, 2, 3, 4, 5, 6, 7 };

        private void SetupMockSuccessCreate()
        {
            _levelRateHeaderReadRepository.LevelIsAvailable(DateTime.MinValue, DateTime.MaxValue,
                           Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), 1).ReturnsForAnyArgs(x =>
                           {
                               return true;
                           });

            _roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return GetRoomTypeList();
            });

            _propertyMealPlanTypeReadRepository.GetAllMealPlanTypeIdListByPropertyId(1).ReturnsForAnyArgs(x =>
            {
                return GetPropertyMealPlanTypeList();
            });

            _propertyMealPlanTypeRateReadRepository.ExistsForAllPeriod(Guid.NewGuid(), 1, new List<DateTime>()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            _levelRateHeaderDomainService.InsertAndSaveChanges(LevelRateHeader.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return new LevelRateHeader();
            }); ;

            _levelRateHeaderAdapter.Map(new LevelRateHeaderDto()).ReturnsForAnyArgs(x =>
            {
                var dto = LevelRateMock.GetLevelResultDto().LevelRateHeader;

                var builder = new LevelRateHeader.Builder(_notificationHandler)
                  .WithId(Guid.NewGuid())
                  .WithRateName(dto.RateName)
                  .WithInitialDate(dto.InitialDate)
                  .WithEndDate(dto.EndDate)
                  .WithLevelId(dto.LevelId)
                  .WithCurrencyId(dto.CurrencyId)
                  .WithIsActive(true)
                  .WithPropertyMealPlanTypeRate(dto.PropertyMealPlanTypeRate)
                  .WithTenantId(_applicationUser.TenantId);

                return builder;
            });

            _levelRateAdapter.Map(new LevelRateDto()).ReturnsForAnyArgs(x =>
            {
                var dto = LevelRateMock.GetLevelResultDto().LevelRateList.FirstOrDefault();

                var builder = new LevelRate.Builder(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithPropertyId(int.Parse(_applicationUser.PropertyId))
                .WithMealPlanTypeDefault(dto.MealPlanTypeDefault)
                .WithMealPlanTypeId(dto.MealPlanTypeId)
                .WithRoomTypeId(dto.RoomTypeId)
                .WithPax1Amount(dto.Pax1Amount)
                .WithPax2Amount(dto.Pax2Amount)
                .WithPax3Amount(dto.Pax3Amount)
                .WithPax4Amount(dto.Pax4Amount)
                .WithPax5Amount(dto.Pax5Amount)
                .WithPax6Amount(dto.Pax6Amount)
                .WithPax7Amount(dto.Pax7Amount)
                .WithPax8Amount(dto.Pax8Amount)
                .WithPax9Amount(dto.Pax9Amount)
                .WithPax10Amount(dto.Pax10Amount)
                .WithPax11Amount(dto.Pax11Amount)
                .WithPax12Amount(dto.Pax12Amount)
                .WithPax13Amount(dto.Pax13Amount)
                .WithPax14Amount(dto.Pax14Amount)
                .WithPax15Amount(dto.Pax15Amount)
                .WithPax16Amount(dto.Pax16Amount)
                .WithPax17Amount(dto.Pax17Amount)
                .WithPax18Amount(dto.Pax18Amount)
                .WithPax19Amount(dto.Pax19Amount)
                .WithPax20Amount(dto.Pax20Amount)
                .WithPaxAdditionalAmount(dto.PaxAdditionalAmount)
                .WithChild1Amount(dto.Child1Amount)
                .WithChild2Amount(dto.Child2Amount)
                .WithChild3Amount(dto.Child3Amount)
                .WithAdultMealPlanAmount(dto.AdultMealPlanAmount)
                .WithChild1MealPlanAmount(dto.Child1MealPlanAmount)
                .WithChild2MealPlanAmount(dto.Child2MealPlanAmount)
                .WithChild3MealPlanAmount(dto.Child3MealPlanAmount)
                .WithCurrencyId(dto.CurrencyId)
                .WithCurrencySymbol(dto.CurrencySymbol)
                .WithTenantId(_applicationUser.TenantId)
                .WithLevelRateHeaderId(dto.LevelRateHeaderId);

                return builder;
            });

            _levelRateRepository.AddRangeAndSaveChanges(new List<LevelRate>());
        }

        private void SetupMockSucessUpdate()
        {
            _levelRateHeaderReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return LevelRateHeader.Create(_notificationHandler)
                       .WithCurrencyId(Guid.NewGuid())
                       .WithInitialDate(DateTime.Now)
                       .WithEndDate(DateTime.Now.AddDays(2))
                       .WithRateName("Teste")
                       .WithTenantId(new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d"))
                       .Build();
            });

            _levelRateHeaderReadRepository.LevelIsAvailable(DateTime.MinValue, DateTime.MaxValue,
                            Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), 1).ReturnsForAnyArgs(x =>
                            {
                                return true;
                            });

            _levelRateHeaderReadRepository.LevelRateAlreadyUsed(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            _roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return GetRoomTypeList();
            });

            _propertyMealPlanTypeReadRepository.GetAllMealPlanTypeIdListByPropertyId(1).ReturnsForAnyArgs(x =>
            {
                return GetPropertyMealPlanTypeList();
            });

            _propertyMealPlanTypeRateReadRepository.ExistsForAllPeriod(Guid.NewGuid(), 1, new List<DateTime>()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            _levelRateHeaderDomainService.Update(LevelRateHeader.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return new LevelRateHeader();
            }); ;

            _levelRateHeaderAdapter.Map(new LevelRateHeader(), new LevelRateHeaderDto()).ReturnsForAnyArgs(x =>
            {
                var dto = LevelRateMock.GetLevelResultDto().LevelRateHeader;

                var builder = new LevelRateHeader.Builder(_notificationHandler)
                  .WithId(Guid.NewGuid())
                  .WithRateName(dto.RateName)
                  .WithInitialDate(dto.InitialDate)
                  .WithEndDate(dto.EndDate)
                  .WithLevelId(dto.LevelId)
                  .WithCurrencyId(dto.CurrencyId)
                  .WithIsActive(true)
                  .WithPropertyMealPlanTypeRate(dto.PropertyMealPlanTypeRate)
                  .WithTenantId(_applicationUser.TenantId);

                return builder;
            });

            _levelRateAdapter.Map(new LevelRate(), new LevelRateDto()).ReturnsForAnyArgs(x =>
            {
                var dto = LevelRateMock.GetLevelResultDto().LevelRateList.FirstOrDefault();

                var builder = new LevelRate.Builder(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithPropertyId(int.Parse(_applicationUser.PropertyId))
                .WithMealPlanTypeDefault(dto.MealPlanTypeDefault)
                .WithMealPlanTypeId(dto.MealPlanTypeId)
                .WithRoomTypeId(dto.RoomTypeId)
                .WithPax1Amount(dto.Pax1Amount)
                .WithPax2Amount(dto.Pax2Amount)
                .WithPax3Amount(dto.Pax3Amount)
                .WithPax4Amount(dto.Pax4Amount)
                .WithPax5Amount(dto.Pax5Amount)
                .WithPax6Amount(dto.Pax6Amount)
                .WithPax7Amount(dto.Pax7Amount)
                .WithPax8Amount(dto.Pax8Amount)
                .WithPax9Amount(dto.Pax9Amount)
                .WithPax10Amount(dto.Pax10Amount)
                .WithPax11Amount(dto.Pax11Amount)
                .WithPax12Amount(dto.Pax12Amount)
                .WithPax13Amount(dto.Pax13Amount)
                .WithPax14Amount(dto.Pax14Amount)
                .WithPax15Amount(dto.Pax15Amount)
                .WithPax16Amount(dto.Pax16Amount)
                .WithPax17Amount(dto.Pax17Amount)
                .WithPax18Amount(dto.Pax18Amount)
                .WithPax19Amount(dto.Pax19Amount)
                .WithPax20Amount(dto.Pax20Amount)
                .WithPaxAdditionalAmount(dto.PaxAdditionalAmount)
                .WithChild1Amount(dto.Child1Amount)
                .WithChild2Amount(dto.Child2Amount)
                .WithChild3Amount(dto.Child3Amount)
                .WithAdultMealPlanAmount(dto.AdultMealPlanAmount)
                .WithChild1MealPlanAmount(dto.Child1MealPlanAmount)
                .WithChild2MealPlanAmount(dto.Child2MealPlanAmount)
                .WithChild3MealPlanAmount(dto.Child3MealPlanAmount)
                .WithCurrencyId(dto.CurrencyId)
                .WithCurrencySymbol(dto.CurrencySymbol)
                .WithTenantId(_applicationUser.TenantId)
                .WithLevelRateHeaderId(dto.LevelRateHeaderId);

                return builder;
            });

            _levelRateAdapter.Map(new LevelRateDto()).ReturnsForAnyArgs(x =>
            {
                var dto = LevelRateMock.GetLevelResultDto().LevelRateList.FirstOrDefault();

                var builder = new LevelRate.Builder(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithPropertyId(int.Parse(_applicationUser.PropertyId))
                .WithMealPlanTypeDefault(dto.MealPlanTypeDefault)
                .WithMealPlanTypeId(dto.MealPlanTypeId)
                .WithRoomTypeId(dto.RoomTypeId)
                .WithPax1Amount(dto.Pax1Amount)
                .WithPax2Amount(dto.Pax2Amount)
                .WithPax3Amount(dto.Pax3Amount)
                .WithPax4Amount(dto.Pax4Amount)
                .WithPax5Amount(dto.Pax5Amount)
                .WithPax6Amount(dto.Pax6Amount)
                .WithPax7Amount(dto.Pax7Amount)
                .WithPax8Amount(dto.Pax8Amount)
                .WithPax9Amount(dto.Pax9Amount)
                .WithPax10Amount(dto.Pax10Amount)
                .WithPax11Amount(dto.Pax11Amount)
                .WithPax12Amount(dto.Pax12Amount)
                .WithPax13Amount(dto.Pax13Amount)
                .WithPax14Amount(dto.Pax14Amount)
                .WithPax15Amount(dto.Pax15Amount)
                .WithPax16Amount(dto.Pax16Amount)
                .WithPax17Amount(dto.Pax17Amount)
                .WithPax18Amount(dto.Pax18Amount)
                .WithPax19Amount(dto.Pax19Amount)
                .WithPax20Amount(dto.Pax20Amount)
                .WithPaxAdditionalAmount(dto.PaxAdditionalAmount)
                .WithChild1Amount(dto.Child1Amount)
                .WithChild2Amount(dto.Child2Amount)
                .WithChild3Amount(dto.Child3Amount)
                .WithAdultMealPlanAmount(dto.AdultMealPlanAmount)
                .WithChild1MealPlanAmount(dto.Child1MealPlanAmount)
                .WithChild2MealPlanAmount(dto.Child2MealPlanAmount)
                .WithChild3MealPlanAmount(dto.Child3MealPlanAmount)
                .WithCurrencyId(dto.CurrencyId)
                .WithCurrencySymbol(dto.CurrencySymbol)
                .WithTenantId(_applicationUser.TenantId)
                .WithLevelRateHeaderId(dto.LevelRateHeaderId);

                return builder;
            });

            _levelRateRepository.RemoveRangeByLevelRateHeaderIdAndSaveChanges(Guid.NewGuid());

            _levelRateRepository.AddRangeAndSaveChanges(new List<LevelRate>());
        }
    }
}
