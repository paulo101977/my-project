﻿using Shouldly;
using System;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.Kernel;
using Thex.Application.Services;
using NSubstitute;
using System.Threading.Tasks;
using Tnf.Repositories.Uow;
using Thex.Domain.Services.Interfaces;
using Thex.AspNetCore.Security.Interfaces;
using Microsoft.Extensions.Configuration;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using Tnf.Localization;
using Thex.Domain.Interfaces.Factories;
using Thex.Application.Interfaces;
using Microsoft.AspNetCore.Http;
using Mock = Thex.Web.Tests.Mocks.ExternalRegistrationDomainServiceMock;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Thex.Dto.ExternalRegistration;
using Thex.Domain.Entities;
using Thex.Common.Enumerations;
using Thex.Common.Emails.AwsSes;
using Thex.Common;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class ExternalRegistrationAppServiceTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        protected readonly IUnitOfWorkManager _unitOfWorkManager;
        protected readonly IApplicationUser _applicationUser;
        protected readonly INotificationHandler _notificationHandler;
        protected readonly IExternalRegistrationDomainService _externalRegistrationDomainService;
        protected readonly ITokenManager _tokenManager;
        protected readonly IConfiguration _configuration;
        protected readonly IReservationDapperReadRepository _reservationDapperReadRepository;
        protected readonly IReservationItemDapperReadRepository _reservationItemDapperReadRepository;
        protected readonly IGuestReservationItemDapperReadRepository _guestReservationItemDapperReadRepository;
        protected readonly ICountrySubdivisionAppService _countrySubdivisionAppService;
        protected readonly ILocalizationManager _localizationManager;
        protected readonly IEmailFactory _emailFactory;
        protected readonly IDocumentAppService _documentAppService;
        protected IHttpContextAccessor _httpContextAccessor;

        public ExternalRegistrationAppServiceTest()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);

            _unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            _applicationUser = Substitute.For<IApplicationUser>();
            _notificationHandler = new NotificationHandler(ServiceProvider);
            _externalRegistrationDomainService = Substitute.For<IExternalRegistrationDomainService>();
            _tokenManager = Substitute.For<ITokenManager>();
            _reservationDapperReadRepository = Substitute.For<IReservationDapperReadRepository>();
            _reservationItemDapperReadRepository = Substitute.For<IReservationItemDapperReadRepository>();
            _guestReservationItemDapperReadRepository = Substitute.For<IGuestReservationItemDapperReadRepository>();
            _countrySubdivisionAppService = Substitute.For<ICountrySubdivisionAppService>();
            _localizationManager = Substitute.For<ILocalizationManager>();
            _emailFactory = Substitute.For<IEmailFactory>();
            _documentAppService = Substitute.For<IDocumentAppService>();

            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("appsettings.json");
            _configuration = configurationBuilder.Build();

            var claimsPrincipal = Substitute.For<ClaimsPrincipal>();
            claimsPrincipal.HasClaim(Arg.Any<string>(), Arg.Any<string>()).Returns(true);
            claimsPrincipal.HasClaim(Arg.Any<Predicate<Claim>>()).Returns(true);

            var httpContext = Substitute.For<HttpContext>();
            httpContext.User.Returns(claimsPrincipal);

            _httpContextAccessor = Substitute.For<HttpContextAccessor>();
            _httpContextAccessor.HttpContext = httpContext;
        }

        public ExternalRegistrationAppService AppService()
            => new ExternalRegistrationAppService(
            _reservationDapperReadRepository,
            _reservationItemDapperReadRepository,
            _externalRegistrationDomainService,
            _guestReservationItemDapperReadRepository,
            _countrySubdivisionAppService,
            _documentAppService,
            _emailFactory,
            _localizationManager,
            _tokenManager,
            _unitOfWorkManager,
            _notificationHandler,
            _applicationUser,
            _configuration,
            _httpContextAccessor);

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IExternalRegistrationDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<ITokenManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IConfiguration>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationDapperReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemDapperReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestReservationItemDapperReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICountrySubdivisionAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocalizationManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IEmailFactory>().ShouldNotBeNull();
            ServiceProvider.GetService<IDocumentAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IHttpContextAccessor>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetRoomListAsync_Success()
        {
            //parameters
            var reservationUid = Mock.GetRandomGuid();
            var reservationItemUid = Mock.GetRandomGuid();

            var header = Mock.GetExternalRoomListHeaderDto();
            var list = Mock.GetExternalRoomListDto();
            var hasNext = false;

            var service = AppService();

            _httpContextAccessor.HttpContext.User.AddUpdateClaim(TokenInfoClaims.ReservationUid, reservationUid.ToString());

            //mock
            _reservationItemDapperReadRepository.GetHeaderByReservationUid(reservationUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(header); });

            _reservationItemDapperReadRepository.GetExternalListByReservationUid(reservationUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(list); });

            //call
            var result = await service.GetRoomListAsync();

            //assert
            Assert.Equal(header.AdditionalAddressDetails, result.AdditionalAddressDetails);
            Assert.Equal(header.Adultcount, result.Adultcount);
            Assert.Equal(header.Childcount, result.Childcount);
            Assert.Equal(header.CityId, result.CityId);
            Assert.Equal(header.CountryId, result.CountryId);
            Assert.Equal(header.Latitude, result.Latitude);
            Assert.Equal(header.Longitude, result.Longitude);
            Assert.Equal(header.MaxDate, result.MaxDate);
            Assert.Equal(header.MinDate, result.MinDate);
            Assert.Equal(header.StateId, result.StateId);
            Assert.Equal(header.CityName, result.CityName);
            Assert.Equal(header.CountryCode, result.CountryCode);
            Assert.Equal(header.CountryName, result.CountryName);
            Assert.Equal(header.CountryTwoLetterIsoCode, result.CountryTwoLetterIsoCode);
            Assert.Equal(header.LanguageIsoCode, result.LanguageIsoCode);
            Assert.Equal(header.Name, result.Name);
            Assert.Equal(header.Neighborhood, result.Neighborhood);
            Assert.Equal(header.PostalCode, result.PostalCode);
            Assert.Equal(header.ReservationCode, result.ReservationCode);
            Assert.Equal(header.State, result.State);
            Assert.Equal(header.StreetName, result.StreetName);
            Assert.Equal(header.StreetNumber, result.StreetNumber);
            Assert.Equal(header.ExternalRoomListDto.HasNext, hasNext);
            Assert.NotEmpty(header.ExternalRoomListDto.Items);
            Assert.Equal(header.ExternalRoomListDto.Items.Count(), list.Items.Count());
            Assert.Equal(header.ExternalRoomListDto.Items.First().SearchableNames.Count(), list.Items.First().SearchableNames.Count());
        }

        [Fact]
        public async Task Should_GetRegistrationListAsync_With_GuestRegistration_Success()
        {
            //parameters
            var reservationItemUid = Mock.GetRandomGuid();
            var guestRegistrationList = Mock.GetGuestRegistrationDapperDto();
            var externalGuestList = Mock.GetExternalGuestListByReservationItemUid();

            var service = AppService();

            //mock
            _guestReservationItemDapperReadRepository.GetGuestRegistrationListByReservationItemUid(reservationItemUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(guestRegistrationList); });

            _guestReservationItemDapperReadRepository.GetExternalGuestListByReservationItemUid(reservationItemUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(externalGuestList); });

            //call
            var result = await service.GetRegistrationListAsync(reservationItemUid);

            //assert
            Assert.NotEmpty(result.Items);
            Assert.Equal(result.Items.Count(), guestRegistrationList.Count());
            Assert.Equal(result.Items.All(e => !e.IsEditable), true);
        }

        [Fact]
        public async Task Should_GetRegistrationListAsync_With_ExternalGuestRegistration_Success()
        {
            //parameters
            var reservationItemUid = Mock.GetRandomGuid();
            var externalGuestList = Mock.GetExternalGuestListByReservationItemUid();

            var service = AppService();

            //mock
            _guestReservationItemDapperReadRepository.GetGuestRegistrationListByReservationItemUid(reservationItemUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(new List<GuestRegistrationDapperDto>()); });

            _guestReservationItemDapperReadRepository.GetExternalGuestListByReservationItemUid(reservationItemUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(externalGuestList); });

            //call
            var result = await service.GetRegistrationListAsync(reservationItemUid);

            //assert
            Assert.NotEmpty(result.Items);
            Assert.Equal(result.Items.Count(), externalGuestList.Count());
            Assert.Equal(result.Items.All(e => e.IsEditable), true);
        }

        [Fact]
        public async Task Should_GetRegistrationListAsync_With_ExternalGuestRegistration_And_GuestRegistration_Success()
        {
            //parameters
            var reservationItemUid = Mock.GetRandomGuid();
            var guestRegistrationList = Mock.GetGuestRegistrationDapperDto();
            var externalGuestList = Mock.GetExternalGuestListByReservationItemUid();

            var service = AppService();

            //mock
            guestRegistrationList.RemoveAt(1);

            _guestReservationItemDapperReadRepository.GetGuestRegistrationListByReservationItemUid(reservationItemUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(guestRegistrationList); });

            _guestReservationItemDapperReadRepository.GetExternalGuestListByReservationItemUid(reservationItemUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(externalGuestList); });

            //call
            var result = await service.GetRegistrationListAsync(reservationItemUid);

            //assert
            Assert.NotEmpty(result.Items);
            Assert.Equal(result.Items.Count(), externalGuestList.Count());
            Assert.Equal(result.Items.Count(e => e.IsEditable), 1);
            Assert.Equal(result.Items.Count(e => !e.IsEditable), 1);
        }

        [Fact]
        public async Task Should_GetRegistrationAsync_With_GuestRegistration_Success()
        {
            //parameters
            var guestReservationItemId = 1;
            var guestRegistration = Mock.GetGuestRegistrationDapperDto().First();
            var externalGuest = Mock.GetExternalGuestListByReservationItemUid().First();

            var service = AppService();

            //mock
            _guestReservationItemDapperReadRepository.GetGuestRegistrationByGuestReservationItemId(guestReservationItemId)
                .ReturnsForAnyArgs(x => { return Task.FromResult(guestRegistration); });

            _guestReservationItemDapperReadRepository.GetExternalGuestByGuestReservationItemId(guestReservationItemId)
                .ReturnsForAnyArgs(x => { return Task.FromResult(externalGuest); });

            //call
            var result = await service.GetRegistrationAsync(guestReservationItemId);

            //assert
            Assert.NotNull(result);
            Assert.Equal(result.IsEditable, false);
        }

        [Fact]
        public async Task Should_GetRegistrationAsync_With_ExternalGuestRegistration_Success()
        {
            //parameters
            var guestReservationItemId = 1;
            var externalGuest = Mock.GetExternalGuestListByReservationItemUid().First();

            var service = AppService();

            //mock
            _guestReservationItemDapperReadRepository.GetGuestRegistrationByGuestReservationItemId(guestReservationItemId)
                .ReturnsForAnyArgs(x => { return Task.FromResult<GuestRegistrationDapperDto>(null); });

            _guestReservationItemDapperReadRepository.GetExternalGuestByGuestReservationItemId(guestReservationItemId)
                .ReturnsForAnyArgs(x => { return Task.FromResult(externalGuest); });

            //call
            var result = await service.GetRegistrationAsync(guestReservationItemId);

            //assert
            Assert.NotNull(result);
            Assert.Equal(result.IsEditable, true);
        }

        [Fact]
        public async Task Should_UpdateAsync_With_Success()
        {
            //parameters
            var reservationUid = Mock.GetRandomGuid();
            var guestReservationItemId = 1;
            var externalGuest = Mock.GetExternalGuestListByReservationItemUid().First();

            var service = AppService();

            _httpContextAccessor.HttpContext.User.AddUpdateClaim(TokenInfoClaims.ReservationUid, reservationUid.ToString());

            //mock
            _reservationDapperReadRepository.AnyByGuestReservationItemIdAsync(reservationUid, guestReservationItemId)
                .ReturnsForAnyArgs(x => { return Task.FromResult(true); });

            _documentAppService.GetBuilder(new Dto.DocumentDto())
                .ReturnsForAnyArgs(x => {
                    return new Document.Builder(_notificationHandler)
                        .WithDocumentInformation(externalGuest.DocumentInformation)
                        .WithDocumentTypeId(externalGuest.DocumentTypeId ?? 1);
                });

            //call
            var result = await service.UpdateAsync(guestReservationItemId, externalGuest);

            //assert
            Assert.True(!_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_UpdateAsync_With_Invalid_GuestReservationItemId()
        {
            //parameters
            var reservationUid = Mock.GetRandomGuid();
            var guestReservationItemId = 1;
            var externalGuest = Mock.GetExternalGuestListByReservationItemUid().First();

            var service = AppService();

            _httpContextAccessor.HttpContext.User.AddUpdateClaim(TokenInfoClaims.ReservationUid, reservationUid.ToString());

            //mock
            _reservationDapperReadRepository.AnyByGuestReservationItemIdAsync(reservationUid, guestReservationItemId)
                .ReturnsForAnyArgs(x => { return Task.FromResult(false); });

            //call
            var result = await service.UpdateAsync(guestReservationItemId, externalGuest);

            //assert
            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_UpdateAsync_With_Location_Success()
        {
            //parameters
            var reservationUid = Mock.GetRandomGuid();
            var guestReservationItemId = 1;
            var externalGuest = Mock.GetExternalGuestListByReservationItemUid().First();

            var service = AppService();

            _httpContextAccessor.HttpContext.User.AddUpdateClaim(TokenInfoClaims.ReservationUid, reservationUid.ToString());

            //mock
            _reservationDapperReadRepository.AnyByGuestReservationItemIdAsync(reservationUid, guestReservationItemId)
                .ReturnsForAnyArgs(x => { return Task.FromResult(true); });

            _documentAppService.GetBuilder(new Dto.DocumentDto())
                .ReturnsForAnyArgs(x => {
                    return new Document.Builder(_notificationHandler)
                        .WithDocumentInformation(externalGuest.DocumentInformation)
                        .WithDocumentTypeId(externalGuest.DocumentTypeId ?? 1);
                    });

            _countrySubdivisionAppService.CreateCountrySubdivisionTranslateAndGetCityId(externalGuest.GetLocationDto(), externalGuest.LanguageIsoCode)
                .Returns(x => { return (1, 1, 1); });

            //call
            var result = await service.UpdateAsync(guestReservationItemId, externalGuest);

            //assert
            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_UpdateAsync_With_Invalid_Document_Notification()
        {
            //parameters
            var reservationUid = Mock.GetRandomGuid();
            var guestReservationItemId = 1;
            var externalGuest = Mock.GetExternalGuestListByReservationItemUid().First();

            var service = AppService();

            _httpContextAccessor.HttpContext.User.AddUpdateClaim(TokenInfoClaims.ReservationUid, reservationUid.ToString());

            //mock
            _reservationDapperReadRepository.AnyByGuestReservationItemIdAsync(reservationUid, guestReservationItemId)
                .ReturnsForAnyArgs(x => { return Task.FromResult(true); });

            _documentAppService.GetBuilder(new Dto.DocumentDto())
                .ReturnsForAnyArgs(x => {
                    return new Document.Builder(_notificationHandler)
                        .WithDocumentInformation("0")
                        .WithDocumentTypeId(externalGuest.DocumentTypeId ?? 1);
                });

            //call
            var result = await service.UpdateAsync(guestReservationItemId, externalGuest);

            //assert
            Assert.True(_notificationHandler.HasNotification());
            Assert.Null(result);
        }

        [Fact]
        public async Task Should_CreateAsync_With_Success()
        {
            //parameters
            var reservationUid = Mock.GetReservationUid();
            var email = Mock.GetEmail();
            var externalLink = Mock.GetRandomString();

            var service = AppService();

            _httpContextAccessor.HttpContext.User.AddUpdateClaim(TokenInfoClaims.ReservationUid, reservationUid.ToString());

            //mock
            _reservationDapperReadRepository.AnyByReservationUidAsync(reservationUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(true); });

            _reservationItemDapperReadRepository.GetMaxDepartureDateByReservationUidAsync(reservationUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(DateTime.Now); });

             _tokenManager.CreateRoomListExternalLink(Mock.GetRandomString(), Mock.GetEmail(), 10000, reservationUid, Guid.Empty)
                .ReturnsForAnyArgs(x => { return externalLink; });

            _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailSubjectExternalAccessReservation.ToString())
                .ReturnsForAnyArgs(x => { return Mock.GetRandomString(); });

            _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailBodyExternalAccessReservation.ToString())
                .ReturnsForAnyArgs(x => { return Mock.GetRandomString(); });

            _emailFactory.GenerateEmail(EmailTemplateEnum.EmailExternalAccessReservation, new Dto.Email.EmailBaseDto())
               .ReturnsForAnyArgs(x => { return Mock.GetRandomString(); });

            //call
            var result = await service.CreateAsync(reservationUid, email);

            //assert
            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(externalLink, result);
        }

        [Fact]
        public async Task Should_CreateAsync_With_Invalid_Reservation_Generates_A_Notification()
        {
            //parameters
            var reservationUid = Mock.GetReservationUid();
            var email = Mock.GetEmail();
            var externalLink = Mock.GetRandomString();

            var service = AppService();

            _httpContextAccessor.HttpContext.User.AddUpdateClaim(TokenInfoClaims.ReservationUid, reservationUid.ToString());

            //mock
            _reservationDapperReadRepository.AnyByReservationUidAsync(reservationUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(false); });

            //call
            var result = await service.CreateAsync(reservationUid, email);

            //assert
            Assert.True(_notificationHandler.HasNotification());
            Assert.Null(result);
        }

        [Fact]
        public async Task Should_CreateAsync_With_Invalid_CreateAsync_Generates_A_Notification()
        {
            //parameters
            var reservationUid = Mock.GetReservationUid();
            var email = Mock.GetEmail();
            var externalLink = Mock.GetRandomString();

            var service = AppService();

            _httpContextAccessor.HttpContext.User.AddUpdateClaim(TokenInfoClaims.ReservationUid, reservationUid.ToString());

            //mock
            _reservationDapperReadRepository.AnyByReservationUidAsync(reservationUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(true); });

            _reservationItemDapperReadRepository.GetMaxDepartureDateByReservationUidAsync(reservationUid)
                .ReturnsForAnyArgs(x => { return Task.FromResult(DateTime.Now); });

            _tokenManager.CreateRoomListExternalLink(Mock.GetRandomString(), Mock.GetEmail(), 10000, reservationUid, Guid.Empty)
               .ReturnsForAnyArgs(x => { return externalLink; })
               .AndDoes(x => {
                   _notificationHandler.Raise(_notificationHandler
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                    .Build());
               });
            
            //call
            var result = await service.CreateAsync(reservationUid, email);

            //assert
            Assert.True(_notificationHandler.HasNotification());
            Assert.Null(result);
        }
    }
}
