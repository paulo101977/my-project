﻿using Shouldly;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Web.Tests.UnitTests.Controllers
{
    public class RoomControllerUnitTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        private readonly NotificationHandler _notificationHandler;

        public RoomControllerUnitTest()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<RoomController>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Get_Total_And_Available_Rooms()
        {
            var response = await GetResponseAsObjectAsync<RoomsAvailableDto>(
            $"{RouteConsts.Room}/gettotalandavailablerooms");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Delete_Room()
        {
            await DeleteResponseAsObjectAsync<RoomsAvailableDto>(
            $"{RouteConsts.Room}/1");

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
