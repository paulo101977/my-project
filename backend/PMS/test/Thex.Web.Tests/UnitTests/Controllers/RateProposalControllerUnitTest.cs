﻿
using Shouldly;
using System.Threading.Tasks;
using Thex.Application.Interfaces.RateProposal;
using Thex.Kernel;
using Thex.Dto;
using Thex.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;

namespace Thex.Web.Tests.UnitTests.Controllers
{
    public class RateProposalControllerUnitTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<RateProposalController>().ShouldNotBeNull();
            ServiceProvider.GetService<IRateProposalAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            // Arrange
            var propertyId = 1;

            // Act
            var response = await GetResponseAsObjectAsync<ReservationItemBudgetRequestDto>(
                $"{RouteConsts.RateProposal}/{propertyId}"
            );

            // Assert
            Assert.True(response != null);
        }
    }
}
