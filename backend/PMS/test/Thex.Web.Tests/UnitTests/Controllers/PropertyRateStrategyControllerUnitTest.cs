﻿using Shouldly;
using Thex.Kernel;
using Thex.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.Application.Interfaces;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Web.Tests.Mocks.PropertyRateStrategy;
using System;

namespace Thex.Web.Tests.UnitTests.Controllers
{
    public class PropertyRateStrategyControllerUnitTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<PropertyRateStrategyController>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyRateStrategyAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAllByPropertyId()
        {
            // Arrange
            var getAll = "getallbypropertyid";

            // Act
            var response = await GetResponseAsObjectAsync<GetAllPropertyRateStrategyDto>(
                $"{RouteConsts.PropertyRateStrategy}/{getAll}"
            );

            // Assert
            Assert.True(response != null);
        }

        [Fact]
        public async Task Should_GetPropertyRateStrategyById()
        {
            // Arrange
            var get = Guid.NewGuid();

            // Act
            var response = await GetResponseAsObjectAsync<PropertyRateStrategyDto>(
                $"{RouteConsts.PropertyRateStrategy}/{get}"
            );

            // Assert
            Assert.True(response != null);
        }

        [Fact]
        public async void Should_Create()
        {
            var propertyRateStrategy = await PostResponseAsObjectAsync<PropertyRateStrategyDto, PropertyRateStrategyDto>(
           $"{RouteConsts.PropertyRateStrategy}", await PropertyRateStrategyMock.GetDto());

            Assert.NotNull(propertyRateStrategy);
        }

        [Fact]
        public async Task Should_Put()
        {
            var propertyRateStrategy = await PutResponseAsObjectAsync<PropertyRateStrategyDto, PropertyRateStrategyDto>(
            $"{RouteConsts.PropertyRateStrategy}/{Guid.NewGuid()}", await PropertyRateStrategyMock.GetDto());

            Assert.NotNull(propertyRateStrategy);
        }

        [Fact]
        public Task Should_Toggle()
        {
            return PatchResponseAsync(
                $"{RouteConsts.PropertyRateStrategy}/{Guid.NewGuid()}/{"toggleactivation"}",
                null
            );
        }
    }
}
