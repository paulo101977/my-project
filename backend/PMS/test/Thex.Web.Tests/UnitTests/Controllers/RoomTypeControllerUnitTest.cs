﻿using Shouldly;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Dto;
using Thex.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Tnf.Dto;
using Xunit;
using Microsoft.Extensions.DependencyInjection;

namespace Thex.Web.Tests.UnitTests.Controllers
{
    public class RoomTypeControllerUnitTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<RoomTypeController>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomTypeAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            // Act
            var response = await GetResponseAsObjectAsync<ListDto<RoomTypeDto>>(
                RouteConsts.RoomType
            );

            // Assert
            Assert.False(response.HasNext);
            Assert.Equal(1, response.Items.Count);
        }
    }
}
