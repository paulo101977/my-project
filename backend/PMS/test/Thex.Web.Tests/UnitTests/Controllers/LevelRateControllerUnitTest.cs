﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Tnf.AspNetCore.TestBase;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using Thex.Web.Controllers;
using Shouldly;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Web.Tests.UnitTests.Controllers
{
    public class LevelRateControllerUnitTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        public INotificationHandler _notificationHandler;

        public LevelRateControllerUnitTest()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<LevelRateController>().ShouldNotBeNull();
            ServiceProvider.GetService<ILevelRateAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ListDto<LevelRateDto>>(
                RouteConsts.LevelRate
            );

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_GetAllLevelAvailable()
        {
            var response = await GetResponseAsObjectAsync<ListDto<LevelRateResultDto>>(
                $"{RouteConsts.LevelRate}/getalllevelsavailable"
            );

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Post()
        {
            var levelRateResult = await PostResponseAsObjectAsync<LevelRateResultDto, Task>(
            $"{RouteConsts.LevelRate}", new LevelRateResultDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<LevelRateResultDto>(
                $"{RouteConsts.LevelRate}/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
           await PutResponseAsObjectAsync<LevelRateResultDto, Task>(
            $"{RouteConsts.LevelRate}/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}", new LevelRateResultDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public Task Should_Delete()
        {
            return DeleteResponseAsync(
                $"{RouteConsts.LevelRate}/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}"
            );
        }

        [Fact]
        public Task Should_Toggle()
        {
            return PatchResponseAsync(
                $"{RouteConsts.LevelRate}/toggleisactive/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}",
                null
            );
        }
    }
}
