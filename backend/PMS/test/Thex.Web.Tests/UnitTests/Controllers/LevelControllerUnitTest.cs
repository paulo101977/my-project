﻿using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Thex.Web.Controllers;
using Thex.Application.Interfaces;
using Thex.Kernel;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Dto;
using Thex.Web.Tests.Mocks;
using System;
using Thex.Common;

namespace Thex.Web.Tests.UnitTests.Controllers
{
    public class LevelControllerUnitTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {       
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<LevelController>().ShouldNotBeNull();
            ServiceProvider.GetService<ILevelAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull(); 
        }

        [Fact]
        public async Task Should_GetAll()
        {
            // Act
            var response = await GetResponseAsObjectAsync<ListDto<LevelDto>>(
               $"{RouteConsts.Level}");

            // Assert
            Assert.False(response.HasNext);
            Assert.Equal(4, response.Items.Count);
        }

        [Fact]
        public async Task Should_Post()
        {   
            var level = await PostResponseAsObjectAsync<LevelDto, LevelDto>(
            $"{RouteConsts.Level}", LevelMock.GetDto(1,1));

            Assert.NotNull(level);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<LevelDto>(
                $"{RouteConsts.Level}/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
            var level = await PutResponseAsObjectAsync<LevelDto, LevelDto>(
            $"{RouteConsts.Level}", LevelMock.GetDto(1, 1));

            Assert.NotNull(level);
        }

        [Fact]
        public Task Should_Delete()
        {
            return DeleteResponseAsync(
                $"{RouteConsts.Level}/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}"
            );
        }

        [Fact]
        public Task Should_Toggle()
        {
            return PatchResponseAsync(
                $"{RouteConsts.Level}/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}/{"toggleactivation"}",
                null
            );
        }
    }
}
