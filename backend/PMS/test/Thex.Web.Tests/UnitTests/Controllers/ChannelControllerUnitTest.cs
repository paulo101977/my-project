﻿using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Thex.Web.Controllers;
using Thex.Application.Interfaces;
using Thex.Kernel;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Dto;
using Thex.Web.Tests.Mocks;


namespace Thex.Web.Tests.UnitTests.Controllers
{
    public class ChannelControllerUnitTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {       
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<ChannelController>().ShouldNotBeNull();
            ServiceProvider.GetService<IChannelAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            // Act
            var response = await GetResponseAsObjectAsync<ListDto<ChannelDto>>(
                RouteConsts.Channel
            );

            // Assert
            Assert.False(response.HasNext);
            Assert.Equal(3, response.Items.Count);
        }

        [Fact]
        public async Task Should_Post()
        {   
            var channel = await PostResponseAsObjectAsync<ChannelDto, ChannelDto>(
            $"{RouteConsts.Channel}", ChannelMock.GetDto());

            Assert.NotNull(channel);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<ChannelDto>(
                $"{RouteConsts.Channel}/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
            var channel = await PutResponseAsObjectAsync<ChannelDto, ChannelDto>(
            $"{RouteConsts.Channel}/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}", ChannelMock.GetDto());

            Assert.NotNull(channel);
        }

        [Fact]
        public Task Should_Delete()
        {
            return DeleteResponseAsync(
                $"{RouteConsts.Channel}/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}"
            );
        }

        [Fact]
        public Task Should_Toggle()
        {
            return PatchResponseAsync(
                $"{RouteConsts.Channel}/{"BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"}/{"toggleactivation"}",
                null
            );
        }
    }
}
