﻿using NSubstitute;
using Thex.Domain.Interfaces.Repositories;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Thex.Domain.Services;
using Thex.Kernel;
using System;
using System.Collections.Generic;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using System.Threading.Tasks;
using Thex.Dto.ExternalRegistration;
using Thex.Domain.Entities;
using Mock = Thex.Web.Tests.Mocks.ExternalRegistrationDomainServiceMock;

namespace Thex.Web.Tests.UnitTests.Domain
{
    public class ExternalRegistrationDomainServiceTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        protected readonly IExternalRegistrationRepository _externalRegistrationRepository;
        protected readonly IGuestReservationItemDapperReadRepository _guestReservationItemDapperReadRepository;
        protected readonly IGuestReservationItemRepository _guestReservationItemRepository;
        protected readonly INotificationHandler _notification;
        protected readonly IApplicationUser _applicationUser;


        public ExternalRegistrationDomainServiceTest()
        {
            _notification = new NotificationHandler(ServiceProvider);
            _applicationUser = ServiceProvider.GetService<IApplicationUser>();

            _externalRegistrationRepository = Substitute.For<IExternalRegistrationRepository>();
            _guestReservationItemDapperReadRepository = Substitute.For<IGuestReservationItemDapperReadRepository>();
            _guestReservationItemRepository = Substitute.For<IGuestReservationItemRepository>();
        }

        public ExternalRegistrationDomainService DomainService()
            => new ExternalRegistrationDomainService(
                _externalRegistrationRepository,
                _guestReservationItemDapperReadRepository,
                _guestReservationItemRepository,
                _notification);

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IExternalRegistrationRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestReservationItemDapperReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestReservationItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Create_Without_GuestIdList_Success()
        {
            //parameters
            var reservationUid = Mock.GetReservationUid();
            var email = Mock.GetEmail();
            var link = Mock.GetLink();

            //mock
            _guestReservationItemDapperReadRepository.GetIdListByReservationUidAsync(reservationUid)
                .Returns(Task.FromResult(new List<long>()));
            
            await DomainService().CreateAsync(reservationUid, email, link);

            //assert
            Assert.False(_notification.HasNotification());
        }

        [Fact]
        public async Task Should_Create_With_GuestIdList_Success()
        {
            //parameters
            var reservationUid = Mock.GetReservationUid();
            var email = Mock.GetEmail();
            var link = Mock.GetLink();

            //mock
            _guestReservationItemDapperReadRepository.GetIdListByReservationUidAsync(reservationUid)
                .Returns(Task.FromResult(new List<long> { 1, 2 }));

            await DomainService().CreateAsync(reservationUid, email, link);

            //assert
            Assert.False(_notification.HasNotification());
        }

        [Fact]
        public async Task Should_UpdateExternalGuest_With_Invalid_GuestReservationId_Generates_A_Notification()
        {
            //parameters
            var externalGuestRegistrationDto = new ExternalGuestRegistrationDto
            {
                GuestReservationItemId = 0
            };

            //mock
            _externalRegistrationRepository.GetByGuestReservationItemIdAsync(externalGuestRegistrationDto.GuestReservationItemId)
                .Returns(Task.FromResult<ExternalGuestRegistration>(null));

            await DomainService().UpdateExternalGuestAsync(externalGuestRegistrationDto);

            //assert
            Assert.True(_notification.HasNotification());
        }

        [Fact]
        public async Task Should_UpdateExternalGuest_With_Correct_Arguments()
        {
            //parameters
            var dto = new ExternalGuestRegistrationDto
            {
                GuestReservationItemId = 1,
                Id = Guid.NewGuid(),
                Email = Mock.GetEmail(),
                DocumentInformation = "135.222.333.44",
                DocumentTypeId = 1,
                FirstName = Mock.GetRandomString(),
                LastName = Mock.GetRandomString(),
                SocialName = Mock.GetRandomString(),
                BirthDate = Mock.GetRandomDate(),
                Gender = Mock.GetRandomString(),
                OccupationId = 1,
                PhoneNumber = Mock.GetRandomPhoneString(),
                MobilePhoneNumber = Mock.GetRandomPhoneString(),
                NationalityId = 1,
                PurposeOfTrip = 1,
                ArrivingBy = 1,
                ArrivingFrom = 1,
                ArrivingFromText = Mock.GetRandomString(),
                NextDestination = 1,
                AdditionalInformation = Mock.GetRandomString(),
                HealthInsurance = Mock.GetRandomString(),
                Latitude = 0,
                Longitude = 0,
                StreetName = Mock.GetRandomString(),
                StreetNumber = Mock.GetRandomString(),
                AdditionalAddressDetails = Mock.GetRandomString(),
                Neighborhood = Mock.GetRandomString(),
                CityId = 1,
                StateId = 1,
                CountryId = 1,
                PostalCode = Mock.GetRandomString(),
                CountryCode = Mock.GetRandomString()
            };

            //mock
            var mockExternalGuestReserv = Substitute.For<ExternalGuestRegistration>(new object[] { dto.GuestReservationItemId });
            mockExternalGuestReserv.Id.Returns(Mock.GetRandomGuid());
            mockExternalGuestReserv.GuestReservationItemId.Returns(dto.GuestReservationItemId);
            mockExternalGuestReserv.ExternalRegistrationEmail.Returns(Mock.GetEmail());

            var mockGuestReserv = Substitute.For<GuestReservationItem>();
            mockGuestReserv.Id.Returns(dto.GuestReservationItemId);

            _externalRegistrationRepository.GetByGuestReservationItemIdAsync(dto.GuestReservationItemId)
                .Returns(Task.FromResult(mockExternalGuestReserv));

            _guestReservationItemRepository.GetById(dto.GuestReservationItemId)
                .Returns(mockGuestReserv);

            await DomainService().UpdateExternalGuestAsync(dto);

            //assert
            Assert.False(_notification.HasNotification());

            Assert.Equal(dto.Id, mockExternalGuestReserv.Id);
            Assert.Equal(dto.ExternalRegistrationEmail, mockExternalGuestReserv.ExternalRegistrationEmail);

            Assert.Equal(mockGuestReserv.GuestEmail, dto.Email);
            Assert.Equal(mockGuestReserv.GuestDocument, dto.DocumentInformation);
            Assert.Equal(mockGuestReserv.GuestDocumentTypeId, dto.DocumentTypeId);
            Assert.Equal(mockGuestReserv.GuestName, dto.GetFullName());
            Assert.Equal(mockGuestReserv.PreferredName, dto.GetFullName());


            Assert.Equal(mockExternalGuestReserv.Email, dto.Email);
            Assert.Equal(mockExternalGuestReserv.DocumentInformation, dto.DocumentInformation);
            Assert.Equal(mockExternalGuestReserv.DocumentTypeId, dto.DocumentTypeId);
            Assert.Equal(mockExternalGuestReserv.FirstName, dto.FirstName);
            Assert.Equal(mockExternalGuestReserv.LastName, dto.LastName);
            Assert.Equal(mockExternalGuestReserv.SocialName, dto.SocialName);
            Assert.Equal(mockExternalGuestReserv.BirthDate, dto.BirthDate);
            Assert.Equal(mockExternalGuestReserv.Gender, dto.Gender);
            Assert.Equal(mockExternalGuestReserv.OccupationId, dto.OccupationId);
            Assert.Equal(mockExternalGuestReserv.PhoneNumber, dto.PhoneNumber);
            Assert.Equal(mockExternalGuestReserv.MobilePhoneNumber, dto.MobilePhoneNumber);
            Assert.Equal(mockExternalGuestReserv.NationalityId, dto.NationalityId);
            Assert.Equal(mockExternalGuestReserv.PurposeOfTrip, dto.PurposeOfTrip);
            Assert.Equal(mockExternalGuestReserv.ArrivingBy, dto.ArrivingBy);
            Assert.Equal(mockExternalGuestReserv.ArrivingFrom, dto.ArrivingFrom);
            Assert.Equal(mockExternalGuestReserv.ArrivingFromText, dto.ArrivingFromText);
            Assert.Equal(mockExternalGuestReserv.NextDestination, dto.NextDestination);
            Assert.Equal(mockExternalGuestReserv.AdditionalInformation, dto.AdditionalInformation);
            Assert.Equal(mockExternalGuestReserv.HealthInsurance, dto.HealthInsurance);
            Assert.Equal(mockExternalGuestReserv.Latitude, dto.Latitude);
            Assert.Equal(mockExternalGuestReserv.Longitude, dto.Longitude);
            Assert.Equal(mockExternalGuestReserv.StreetName, dto.StreetName);
            Assert.Equal(mockExternalGuestReserv.StreetNumber, dto.StreetNumber);
            Assert.Equal(mockExternalGuestReserv.AdditionalAddressDetails, dto.AdditionalAddressDetails);
            Assert.Equal(mockExternalGuestReserv.Neighborhood, dto.Neighborhood);
            Assert.Equal(mockExternalGuestReserv.CityId, dto.CityId);
            Assert.Equal(mockExternalGuestReserv.StateId, dto.StateId);
            Assert.Equal(mockExternalGuestReserv.CountryId, dto.CountryId);
            Assert.Equal(mockExternalGuestReserv.PostalCode, dto.PostalCode);
            Assert.Equal(mockExternalGuestReserv.CountryCode, dto.CountryCode);
        }
    }
}
