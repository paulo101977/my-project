﻿using Tnf.AspNetCore.TestBase;
using Xunit;
using Thex.Domain.Entities;
using Mock = Thex.Web.Tests.Mocks.ExternalRegistrationDomainServiceMock;
using Thex.Dto.ExternalRegistration;

namespace Thex.Web.Tests.UnitTests.Domain
{
    public class GuestReservationItemDomainEntiyTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        [Fact]
        public void Constructor_GuestReservationItem_FullTest()
        {
            //parameters
            var dto = new ExternalGuestRegistrationDto
            {
                Email = Mock.GetEmail(),
                DocumentInformation = "135.222.333.44",
                DocumentTypeId = 1,
                FirstName = Mock.GetRandomString(),
                LastName = Mock.GetRandomString(),
                SocialName = Mock.GetRandomString(),
            };

            //call
            var guestReservationItem = new GuestReservationItem();

            guestReservationItem.ChangeGuestInformation(dto.Email, dto.DocumentInformation, dto.DocumentTypeId, dto.GetFullName(), dto.GetFullName());

            //assert
            Assert.Equal(guestReservationItem.GuestEmail, dto.Email);
            Assert.Equal(guestReservationItem.GuestDocument, dto.DocumentInformation);
            Assert.Equal(guestReservationItem.GuestDocumentTypeId, dto.DocumentTypeId);
            Assert.Equal(guestReservationItem.GuestName, dto.GetFullName());
            Assert.Equal(guestReservationItem.PreferredName, dto.GetFullName());
        }


    }
}
