﻿using NSubstitute;
using Thex.Domain.Interfaces.Repositories;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Shouldly;
using Thex.Kernel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using Thex.Domain.Entities;
using Mock = Thex.Web.Tests.Mocks.ExternalRegistrationDomainServiceMock;
using System;
using System.Linq;
using Thex.Dto.ExternalRegistration;
using Thex.Common.Enumerations;

namespace Thex.Web.Tests.UnitTests.Domain
{
    public class ExternalGuestRegistrationDomainEntiyTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        [Fact]
        public void Constructor_ExternalGuestRegistration_FullTest()
        {
            //parameters
            var dto = new ExternalGuestRegistrationDto
            {
                GuestReservationItemId = 1,
                Id = Guid.NewGuid(),
                Email = Mock.GetEmail(),
                DocumentInformation = "135.222.333.44",
                DocumentTypeId = 1,
                FirstName = Mock.GetRandomString(),
                LastName = Mock.GetRandomString(),
                SocialName = Mock.GetRandomString(),
                BirthDate = Mock.GetRandomDate(),
                Gender = Mock.GetRandomString(),
                OccupationId = 1,
                PhoneNumber = Mock.GetRandomPhoneString(),
                MobilePhoneNumber = Mock.GetRandomPhoneString(),
                NationalityId = 1,
                PurposeOfTrip = 1,
                ArrivingBy = 1,
                ArrivingFrom = 1,
                ArrivingFromText = Mock.GetRandomString(),
                NextDestination = 1,
                AdditionalInformation = Mock.GetRandomString(),
                HealthInsurance = Mock.GetRandomString(),
                Latitude = 0,
                Longitude = 0,
                StreetName = Mock.GetRandomString(),
                StreetNumber = Mock.GetRandomString(),
                AdditionalAddressDetails = Mock.GetRandomString(),
                Neighborhood = Mock.GetRandomString(),
                CityId = 1,
                StateId = 1,
                CountryId = 1,
                PostalCode = Mock.GetRandomString(),
                CountryCode = Mock.GetRandomString()
            };

            //call
            var externalRegistration = new ExternalGuestRegistration(dto.GuestReservationItemId);

            //assert
            Assert.NotEqual(externalRegistration.Id, Guid.Empty);
            Assert.Equal(externalRegistration.GuestReservationItemId, dto.GuestReservationItemId);
            Assert.Equal(externalRegistration.LocationCategoryId, (int)LocationCategoryEnum.Residential);

            //call
            externalRegistration.ChangeGuestRegistration(dto.FirstName, dto.LastName, dto.GetFullName(), dto.SocialName, dto.Email,
                                                    dto.BirthDate, dto.Gender, dto.OccupationId, dto.PhoneNumber, dto.MobilePhoneNumber,
                                                    dto.NationalityId, dto.PurposeOfTrip, dto.ArrivingBy, dto.ArrivingFrom, dto.ArrivingFromText,
                                                    dto.NextDestination, dto.NextDestinationText, dto.AdditionalInformation, dto.HealthInsurance,
                                                    dto.DocumentTypeId, dto.DocumentInformation, dto.Latitude, dto.Longitude, dto.StreetName, dto.StreetNumber,
                                                    dto.AdditionalAddressDetails, dto.Neighborhood, dto.CityId, dto.StateId, dto.CountryId,
                                                    dto.PostalCode, dto.CountryCode);

            Assert.Equal(externalRegistration.Email, dto.Email);
            Assert.Equal(externalRegistration.DocumentInformation, dto.DocumentInformation);
            Assert.Equal(externalRegistration.DocumentTypeId, dto.DocumentTypeId);
            Assert.Equal(externalRegistration.FirstName, dto.FirstName);
            Assert.Equal(externalRegistration.LastName, dto.LastName);
            Assert.Equal(externalRegistration.SocialName, dto.SocialName);
            Assert.Equal(externalRegistration.BirthDate, dto.BirthDate);
            Assert.Equal(externalRegistration.Gender, dto.Gender);
            Assert.Equal(externalRegistration.OccupationId, dto.OccupationId);
            Assert.Equal(externalRegistration.PhoneNumber, dto.PhoneNumber);
            Assert.Equal(externalRegistration.MobilePhoneNumber, dto.MobilePhoneNumber);
            Assert.Equal(externalRegistration.NationalityId, dto.NationalityId);
            Assert.Equal(externalRegistration.PurposeOfTrip, dto.PurposeOfTrip);
            Assert.Equal(externalRegistration.ArrivingBy, dto.ArrivingBy);
            Assert.Equal(externalRegistration.ArrivingFrom, dto.ArrivingFrom);
            Assert.Equal(externalRegistration.ArrivingFromText, dto.ArrivingFromText);
            Assert.Equal(externalRegistration.NextDestination, dto.NextDestination);
            Assert.Equal(externalRegistration.AdditionalInformation, dto.AdditionalInformation);
            Assert.Equal(externalRegistration.HealthInsurance, dto.HealthInsurance);
            Assert.Equal(externalRegistration.Latitude, dto.Latitude);
            Assert.Equal(externalRegistration.Longitude, dto.Longitude);
            Assert.Equal(externalRegistration.StreetName, dto.StreetName);
            Assert.Equal(externalRegistration.StreetNumber, dto.StreetNumber);
            Assert.Equal(externalRegistration.AdditionalAddressDetails, dto.AdditionalAddressDetails);
            Assert.Equal(externalRegistration.Neighborhood, dto.Neighborhood);
            Assert.Equal(externalRegistration.CityId, dto.CityId);
            Assert.Equal(externalRegistration.StateId, dto.StateId);
            Assert.Equal(externalRegistration.CountryId, dto.CountryId);
            Assert.Equal(externalRegistration.PostalCode, dto.PostalCode);
            Assert.Equal(externalRegistration.CountryCode, dto.CountryCode);

        }


    }
}
