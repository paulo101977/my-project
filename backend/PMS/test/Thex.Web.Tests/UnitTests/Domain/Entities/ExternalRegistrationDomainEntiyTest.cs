﻿using NSubstitute;
using Thex.Domain.Interfaces.Repositories;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Shouldly;
using Thex.Kernel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Mock = Thex.Web.Tests.Mocks.ExternalRegistrationDomainServiceMock;
using System;
using System.Linq;

namespace Thex.Web.Tests.UnitTests.Domain
{
    public class ExternalRegistrationDomainEntiyTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        [Fact]
        public void Constructor_ExternalRegistration_FullTest()
        {
            //parameters
            var reservationUid = Mock.GetReservationUid();
            var email = Mock.GetEmail();
            var link = Mock.GetLink();
            var guestReservListEmpty = new List<long>();
            var guestReservList = new List<long> { 1, 2, 3 };
            List<ExternalGuestRegistration> externalGuestRegistrationsNullList = null;

            //call
            var externalRegistration = new ExternalRegistration(reservationUid, email, link);

            //assert
            Assert.NotEqual(externalRegistration.Id, Guid.Empty);
            Assert.Equal(externalRegistration.Email, email);
            Assert.Equal(externalRegistration.Link, link);
            Assert.Equal(externalRegistration.ReservationUid, reservationUid);


            //call
            externalRegistration.AddExternalGuestRegistrationList(null);

            //assert
            Assert.Equal(externalRegistration.ExternalGuestRegistrationList, externalGuestRegistrationsNullList);

            //call
            externalRegistration.AddExternalGuestRegistrationList(guestReservListEmpty);

            //assert
            Assert.Empty(externalRegistration.ExternalGuestRegistrationList);

            //call
            externalRegistration.AddExternalGuestRegistrationList(guestReservList);

            //assert
            Assert.NotEmpty(externalRegistration.ExternalGuestRegistrationList);
            Assert.Equal(externalRegistration.ExternalGuestRegistrationList.Count(), guestReservList.Count());

        }


    }
}
