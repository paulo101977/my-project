﻿using Microsoft.AspNetCore.Http;
using System.Security.Principal;

namespace Thex.Web.Tests.Mocks
{
    public class HttpContextAccessorMock : IHttpContextAccessor
    {

        public HttpContextAccessorMock()
        {
            HttpContext = new DefaultHttpContext
            {
                User = new GenericPrincipal(
               new GenericIdentity("username"),
               new string[0]
               )
            };
        }

        public HttpContext HttpContext { get; set; }
    }
}
