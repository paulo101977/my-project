﻿using System;
using System.Collections.Generic;
using Thex.GenericLog;
using Thex.Web.Tests.Mocks.GenericLogEventBuilder;

namespace Thex.Web.Tests.Mocks.GenericLogHandler
{
    public class GenericLogHandlerMock : IGenericLogHandler
    {
        public IGenericLogEventBuilder DefaultBuilder => new GenericLogEventBuilderMock();      

        public void AddLog(params IGenericLogEvent[] GenericLogEvents)
        {
            
        }

        public void Commit(string index = "database-log")
        {
            
        }

        public void Commit()
        {
            throw new NotImplementedException();
        }

        public void Delete(IGenericLogEvent GenericLogEvent)
        {
            
        }

        public IEnumerable<IGenericLogEvent> GetAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IGenericLogEvent> GetAll(Func<IGenericLogEvent, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IGenericLogEvent> GetByGuid(Guid guid)
        {
            throw new NotImplementedException();
        }

        public bool HasGenericLog()
        {
            return true;
        }

        public bool HasGenericLog(GenericLogType type)
        {
            throw new NotImplementedException();
        }
    }
}
