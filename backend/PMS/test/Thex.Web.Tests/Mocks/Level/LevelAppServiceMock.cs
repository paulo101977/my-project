﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Web.Tests.Mocks
{
    public class LevelAppServiceMock : ILevelAppService
    {
        public async Task<LevelDto> Create(LevelDto dto)
        {
            return LevelMock.GetDto(1, 1);
        }

        public LevelDto CreateLevel(LevelDto levelDto)
        {
            return LevelMock.GetDto(1, 1);
        }

        public Task Delete(Guid id)
        {
            return Task.CompletedTask;
        }

        public void DeleteLevel(Guid id)
        {
             
        }

        public Task<LevelDto> Get(DefaultIntRequestDto id)
        {
            throw new NotImplementedException();
        }

        public IListDto<LevelDto> GetAllDto(GetAllLevelDto requestDto)
        {
            var dto = LevelMock.GetDtoList();

            if (requestDto != null)
            {
                if (!requestDto.All)
                {
                    dto.Items =  dto.Items.Where(x => x.IsActive).ToList();
                }
            }

            return new ListDto<LevelDto>
            {
                HasNext = false,
                Items = dto.Items
            };

        }
          

        public LevelDto GetDtoById(Guid id)
        {
            return LevelMock.GetDto(1, 1);
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
           
        }

        public Task<LevelDto> Update(Guid id, LevelDto dto)
        {
            throw new NotImplementedException();
        }

        public LevelDto UpdateLevel(LevelDto levelDto)
        {
            return LevelMock.GetDto(1, 1);
        }
    }
}
