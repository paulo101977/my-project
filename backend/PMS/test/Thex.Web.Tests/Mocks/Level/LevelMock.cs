﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Web.Tests.Mocks
{
    public class LevelMock
    {
        public INotificationHandler NotificationHandler { get; }

        public LevelMock(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }


        public static LevelDto CreateLevelDto()
        {
            return new LevelDto()
            {
                Id = null,
                LevelCode = 2,
                Description = "Teste unitário 1",
                Order = 2,
                IsActive = true,
                PropertyId = 11

            };
        }  

        public static LevelDto GetDto(int levelCode, int ordem)
        {
            return new LevelDto()
            {
                Id = new Guid("BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"),
                LevelCode = levelCode,
                Description = "Teste unitário 1",
                Order = ordem,
                IsActive = true,
                PropertyId = 11

            };
        }

        public static IListDto<LevelDto> GetDtoList()
        {
            var ret = new ListDto<LevelDto>();

            ret.Items.Add(GetDto(1, 1));
            ret.Items.Add(GetDto(3, 3));
            ret.Items.Add(GetDto(4, 2));
            ret.Items.Add(GetDto(5, 5));

            return ret;
        }

        public static Level Get()
        {
            return new Level()
            {
                Id = new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d"),
                LevelCode = 1,
                Description = "Teste unitário 1",
                Order = 1,
                IsActive = false,
                IsDeleted = false,
                TenantId = new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d"),
                CreationTime = new DateTime(2018, 10, 18, 10, 10, 20),
                CreatorUserId = new Guid("7D220627-853D-466F-835F-A06B6D6B1922"),
                PropertyId = 11

            };
        }

        public static List<Level> GetListLevel()
        {
            var list = new List<Level>();
            list.Add(new Level()
            {
                Id = new Guid("bd65c4a7-1fd9-4c21-85da-aaf6e4109e12"),
                LevelCode = 1,
                Description = "Teste unitário 1",
                Order = 1,
                IsActive = true,
                IsDeleted = false,
                TenantId = Guid.Empty,
                CreationTime = new DateTime(2018, 10, 18, 10, 10, 20),
                CreatorUserId = new Guid("7D220627-853D-466F-835F-A06B6D6B1922"),
                PropertyId = 11
            });

            return list;
        }

        public static Level GetCreateLevel()
        {
            return new Level()
            {
                Id = new Guid("a9584477-b26d-46c2-8913-ef98e7cc35ae"),
                LevelCode = 1,
                Description = "Teste unitário 1",
                Order = 1,
                IsActive = true,
                IsDeleted = false,
                TenantId = new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d"),
                CreationTime = new DateTime(2018, 10, 18, 10, 10, 20),
                CreatorUserId = new Guid("7D220627-853D-466F-835F-A06B6D6B1922")

            };

        }

        public static Level GetValidTennantId()
        {
            return new Level()
            {
                Id = new Guid("a9584477-b26d-46c2-8913-ef98e7cc35ae"),
                LevelCode = 1,
                Description = "Teste unitário 1",
                Order = 1,
                IsActive = true,
                IsDeleted = false,
                TenantId = new Guid("a0c1b6f8-6116-4bd1-b1fc-547c0426d236"),
                CreationTime = new DateTime(2018, 10, 18, 10, 10, 20),
                CreatorUserId = new Guid("7D220627-853D-466F-835F-A06B6D6B1922")

            };

        }

        public static LevelDto LevelDtoValidNull()
        {
            return new LevelDto()
            {
                Id = new Guid("BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"),
                LevelCode = default(int),
                Description = "",
                Order = 0,
                IsActive = true,
                PropertyId = 11
            };
        }

        public static LevelDto LevelDtoValidateLevel()
        {
            return new LevelDto()
            {
                Id = new Guid("5bc608d1-737a-453f-a0e8-9657444848c9"),
                LevelCode = 1,
                Description = "teste",
                Order = 0,
                IsActive = true,
                PropertyId = 0
            };
        }

        public static LevelDto LevelDtoNull()
        {
            return null;
        }

        public static LevelDto GetDtoIdValidUpdate()
        {
            return new LevelDto()
            {
                Id = new Guid("9ca35cff-0c99-40cb-a809-323ac1590693"),
                LevelCode = 5,
                Description = "teste",
                Order = 1,
                IsActive = true,
                PropertyId = 11
            };

        }

        public static LevelDto GetDtoLevelCodeValidUpdate()
        {
            return new LevelDto()
            {
                Id = new Guid("BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"),
                LevelCode = 5,
                Description = "teste",
                Order = 1,
                IsActive = true,
                PropertyId = 11
            };

        }



        public Level.Builder Map(LevelDto dto)
        {
            var builder = new Level.Builder(NotificationHandler)
             .WithId(Guid.NewGuid())
             .WithLevelCode(dto.LevelCode)
             .WithDescription(dto.Description)
             .WithOrder(dto.Order)
             .WithPropertyId(11)
             .WithIsActive(dto.IsActive);

            return builder;
        }


        public Level.Builder Map(Level entity, LevelDto dto)
        {
            var builder = new Level.Builder(NotificationHandler)
             .WithId(entity.Id)
             .WithLevelCode(dto.LevelCode)
             .WithDescription(dto.Description)
             .WithOrder(dto.Order)
             .WithPropertyId(11)
             .WithIsActive(dto.IsActive);

            return builder;
        }


        public static IListDto<LevelDto> ListValidOrder()
        {
            var list = new ListDto<LevelDto>();

            list.Items.Add(new LevelDto()
            {
                Id = new Guid("e22b4348-0339-4e38-bf15-844e0788842d"),
                LevelCode = 2,
                Description = "Teste unitário",
                Order = 2,
                IsActive = true,
                PropertyId = 11

            });


            list.Items.Add(new LevelDto()
            {
                Id = new Guid("9d62b9c6-2b38-4d7b-9e77-3fd3d589a0e0"),
                LevelCode = 3,
                Description = "Teste unitário",
                Order = 3,
                IsActive = true,
                PropertyId = 11

            });

            list.Items.Add(new LevelDto()
            {
                Id = new Guid("ecd27175-be73-4a46-bc5a-aa0780b048fe"),
                LevelCode = 4,
                Description = "Teste unitário",
                Order = 4,
                IsActive = true,
                PropertyId = 11

            });

            list.Items.Add(new LevelDto()
            {
                Id = new Guid("23e4825e-93ff-4a2c-93a4-ed71e1d19b09"),
                LevelCode = 5,
                Description = "Teste unitário",
                Order = 5,
                IsActive = true,
                PropertyId = 11

            });

            list.Items.Add(new LevelDto()
            {
                Id = new Guid("3ea63a2c-b2b7-47cc-85ab-0947404c0471"),
                LevelCode = 6,
                Description = "Teste unitário",
                Order = 6,
                IsActive = true,
                PropertyId = 11

            });
             
            return list;
             
        }


    }
}
