﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.GenericLog;
using Thex.Web.Tests.Mocks.GenericLogEvent;

namespace Thex.Web.Tests.Mocks.GenericLogEventBuilder
{
    public class GenericLogEventBuilderMock : IGenericLogEventBuilder
    {
        public IGenericLogEventBuilder AsError()
        {
            return new GenericLogEventBuilderMock();
        }

        public IGenericLogEventBuilder AsInformation()
        {
            return new GenericLogEventBuilderMock();
        }

        public IGenericLogEventBuilder AsSpecification()
        {
            return new GenericLogEventBuilderMock();
        }

        public IGenericLogEventBuilder AsWarning()
        {
            return new GenericLogEventBuilderMock();
        }

        public IGenericLogEvent Build()
        {
            return new GenericLogEventMock();
        }

        public void Dispose()
        {
          
        }

        public IGenericLogEventBuilder FromException(Exception ex, string error)
        {
            return new GenericLogEventBuilderMock();
        }

        public void Raise()
        {
            
        }

        public void Throw()
        {
          
        }

        public IGenericLogEventBuilder WithDetailedMessage(string detailedMessage)
        {
            return new GenericLogEventBuilderMock();
        }

        public IGenericLogEventBuilder WithMessage(string enumeration)
        {
            return new GenericLogEventBuilderMock();
        }
    }
}
