﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Dto;
using Thex.Dto.Base;
using Thex.Dto.Housekeeping;
using Thex.Dto.Room;
using Thex.Dto.Rooms;
using Tnf.Dto;

namespace Thex.Web.Tests.Mocks.Room
{
    public class RoomAppServiceMock : IRoomAppService
    {
        public RoomAppServiceMock()
        {
        }

        public Task<RoomDto> Create(RoomDto dto)
        {
            throw new NotImplementedException();
        }

        public RoomDto CreateRoom(RoomDto roomDto)
        {
            throw new NotImplementedException();
        }

        public Task Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void DeleteRoom(int id)
        {
            
        }

        public Task<RoomDto> Get(DefaultIntRequestDto id)
        {
            throw new NotImplementedException();
        }

        public Task<IListDto<RoomDto>> GetAll(GetAllRoomDto request)
        {
            throw new NotImplementedException();
        }

        public IListDto<RoomDto> GetAllAvailableByPeriodOfRoomType(PeriodDto request, int propertyId, int roomTypeId, int? reservationItemId, bool isCheckin)
        {
            throw new NotImplementedException();
        }

        public List<int> GetAllOccupedRoomsByPropertyAndPeriod(DateTime initialDate, DateTime endDate, int propertyId)
        {
            throw new NotImplementedException();
        }

        public IListDto<RoomDto> GetAllRoomsByPropertyAndPeriod(GetAllRoomsByPropertyAndPeriodDto request, int propertyId)
        {
            throw new NotImplementedException();
        }

        public IListDto<GetAllHousekeepingAlterStatusDto> GetAllRoomWithHousekeepingStatus(int propertyId)
        {
            throw new NotImplementedException();
        }

        public IListDto<RoomDto> GetAllStatusCheckinRooms(GetAllRoomsByPropertyAndPeriodDto request, int propertyId)
        {
            throw new NotImplementedException();
        }

        public IListDto<RoomDto> GetChildRoomsByParentRoomId(GetAllRoomsDto request, int parentRoomId)
        {
            throw new NotImplementedException();
        }

        public Task<BudgetCurrentOrFutureDto> GetCurrentOrFutureBudgetByReservationItem(int propertyId, int roomTypeId, DateTime initialDate, DateTime finalDate, long reservationItemId)
        {
            throw new NotImplementedException();
        }

        public IListDto<RoomDto> GetParentRoomsByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            throw new NotImplementedException();
        }

        public RoomDto GetRoom(DefaultIntRequestDto id)
        {
            throw new NotImplementedException();
        }

        public IListDto<RoomDto> GetRoomsByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            throw new NotImplementedException();
        }

        public IListDto<RoomDto> GetRoomsWithoutChildrenByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            throw new NotImplementedException();
        }

        public IListDto<RoomDto> GetRoomsWithoutParentAndChildrenByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            throw new NotImplementedException();
        }

        public Task<RoomsAvailableDto> GetTotalAndAvailableRoomsByPropertyAsync()
        {
            return new RoomsAvailableDto().AsTask();
        }

        public bool RoomIsAvailable(int propertyId, int roomId, long reservationItemId, DateTime systemDate)
        {
            throw new NotImplementedException();
        }

        public void ToggleActivation(int id)
        {
            throw new NotImplementedException();
        }

        public Task<RoomDto> Update(int id, RoomDto dto)
        {
            throw new NotImplementedException();
        }

        public RoomDto UpdateRoom(int id, RoomDto roomDto)
        {
            throw new NotImplementedException();
        }
    }
}
