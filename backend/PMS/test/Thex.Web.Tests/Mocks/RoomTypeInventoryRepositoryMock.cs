﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Inventory.Domain.Entities;

namespace Thex.Web.Tests.Mocks
{
    public class RoomTypeInventoryRepositoryMock : Inventory.Domain.Repositories.Interfaces.IRoomTypeInventoryRepository
    {
        public void CreateList(IList<Inventory.Domain.Entities.RoomTypeInventory> roomTypeInventoryList)
        {
            throw new NotImplementedException();
        }

        public void Delete(int roomTypeId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Inventory.Domain.Entities.RoomTypeInventory> GetAllByPropertyIdAndDate(int propertyId, DateTime date)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Inventory.Domain.Entities.RoomTypeInventory> GetAllByRoomTypeIdAndDate(int roomTypeId, DateTime date)
        {
            return new List<Inventory.Domain.Entities.RoomTypeInventory>();
        }

        public IEnumerable<Inventory.Domain.Entities.RoomTypeInventory> GetAllByRoomTypeIdAndPeriod(int roomTypeId, DateTime initialDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Inventory.Domain.Entities.RoomTypeInventory> GetAllByRoomTypeIdListAndPeriod(IEnumerable<int> roomTypeIdList, DateTime initialDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public Inventory.Domain.Entities.RoomTypeInventory GetLastByRoomTypeId(int roomTypeId)
        {
            throw new NotImplementedException();
        }

        public void UpdateList(IList<Inventory.Domain.Entities.RoomTypeInventory> roomTypeInventoryList)
        {

        }
    }
}
