﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Application.Interfaces.RateProposal;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Web.Tests.Mocks.RateProposal
{
    public class RateProposalAppServiceMock : IRateProposalAppService
    {
        public async Task<ReservationItemBudgetHeaderDto> GetAllhBudgetOfferWithRateProposal(ReservationItemBudgetRequestDto requestDto, int propertyId)
        {
            return await RateProposalMock.GetDto();
        }
    }
}
