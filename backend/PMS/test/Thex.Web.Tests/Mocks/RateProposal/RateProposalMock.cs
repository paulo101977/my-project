﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;

namespace Thex.Web.Tests.Mocks.RateProposal
{
    public class RateProposalMock
    {
        public static async Task<ReservationItemBudgetHeaderDto> GetDto(string name = "RateProposal")
        {
           return await new ReservationItemBudgetHeaderDto()
            {
                Id = 1,
                ReservationItemId = 1,
                ReservationItemCode = "1",
                MealPlanTypeId = 1,
                RatePlanId = Guid.NewGuid(),
                CurrencyId = Guid.NewGuid(),
                GratuityTypeId = 1,
                CommercialBudgetList = new List<ReservationItemCommercialBudgetDto>(),
                ReservationBudgetDtoList = new List<ReservationBudgetDto>()
            }.AsTask();            
        }
    }
}
