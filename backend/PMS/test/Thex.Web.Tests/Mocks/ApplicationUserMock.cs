﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Kernel;
using Thex.Kernel.Dto;

namespace Thex.Web.Tests.Mocks
{
    public class ApplicationUserMock : IApplicationUser
    {
        public string Name => "Teste";

        public string UserEmail => "teste@email.com";

        public Guid UserUid => new Guid("215bc8a0-07db-4428-9955-fa53b58b4ded");

        public string UserName { get { return "Teste"; } set { } }

        public Guid TenantId => new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d");
        public string PropertyUid => "e1a9185e-5e1b-49bd-a17e-0d18cd15eaf0";

        public string PropertyId => "11";

        public string ChainId => "1";

        public bool IsAdmin => false;

        public string PreferredLanguage => "pt-br";

        public string PreferredCulture => "pt-br";

        public string PropertyLanguage => "pt-br";

        public string PropertyCulture => "pt-br";

        public string TimeZoneName => "America/Sao_Paulo";

        public string PropertyCountryCode => "BR";

        public string PropertyDistrictCode => "RJ";

        public List<LoggedUserPropertyDto> PropertyList => new List<LoggedUserPropertyDto>();

        public Dictionary<string, string> GetClaimsIdentity()
        {
            var claims = new Dictionary<string, string>();
            claims.Add("admin", "true");
            return claims;
        }

        public bool IsAuthenticated()
        {
            return true;
        }

        public void SetProperties(string userUid, string tenantId, string propertyId, string timeZoneName)
        {
            throw new NotImplementedException();
        }
    }
}
