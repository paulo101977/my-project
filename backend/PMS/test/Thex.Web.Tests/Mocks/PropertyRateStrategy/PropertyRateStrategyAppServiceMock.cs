﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Web.Tests.Mocks.PropertyRateStrategy
{
    public class PropertyRateStrategyAppServiceMock : IPropertyRateStrategyAppService
    {
        public async Task<PropertyRateStrategyDto> CreateAsync(PropertyRateStrategyDto propertyRateStrategyDto)
        {
            return await PropertyRateStrategyMock.GetDto();
        }

        public IListDto<GetAllPropertyRateStrategyDto> GetAllByPropertyId()
        {
            return PropertyRateStrategyMock.GetAllDto();
        }

        public async Task<PropertyRateStrategyDto> GetPropertyRateStrategyById(Guid propertyRateStrategyId)
        {
            return await PropertyRateStrategyMock.GetDto();
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
           
        }

        public async Task<PropertyRateStrategyDto> UpdateAsync(Guid id, PropertyRateStrategyDto propertyRateStrategyDto)
        {
            return await PropertyRateStrategyMock.GetDto();
        }
    }
}
