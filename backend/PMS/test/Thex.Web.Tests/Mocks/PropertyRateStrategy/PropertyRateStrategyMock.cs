﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Web.Tests.Mocks.PropertyRateStrategy
{
    public class PropertyRateStrategyMock
    {
        public static IListDto<GetAllPropertyRateStrategyDto> GetAllDto(string name = "PropertyRateStrategy")
        {
            return new List<GetAllPropertyRateStrategyDto>()
            {
                new GetAllPropertyRateStrategyDto()
                {
                    Id = Guid.NewGuid(),
                    IsActive = true,
                    Name = "Estratégia 01",
                    StartDate = new DateTime(2018, 12, 14),
                    EndDate = new DateTime(2018, 12, 14),
                    RoomTypeAbbreviationList = new List<string>()
                    {
                        "STD"
                    }
                }
            }.ToListDto(false);
        }

        public static async Task<PropertyRateStrategyDto> GetDto(string name = "PropertyRateStrategy")
        {
            return await new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 12, 14),
                EndDate = new DateTime(2018, 12, 14),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20
                    }
                }
            }.AsTask();
        }
    }
}
