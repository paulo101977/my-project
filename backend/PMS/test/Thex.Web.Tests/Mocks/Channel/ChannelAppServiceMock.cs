﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Web.Tests.Mocks
{
    public class ChannelAppServiceMock : IChannelAppService
    {
        public async Task<ChannelDto> Create(ChannelDto dto)
        {
            return ChannelMock.GetDto();
        }

        public ChannelDto CreateChannel(ChannelDto channelDto)
        {
            return ChannelMock.GetDto();
        }

        public Task Delete(Guid id)
        {
            return Task.CompletedTask;
        }

        public void DeleteChannel(Guid id)
        {
            
        }

        public Task<ChannelDto> Get(DefaultIntRequestDto id)
        {
            throw new NotImplementedException();
        }

        public async Task<IListDto<ChannelDto>> GetAll(GetAllChannelDto request)
        {
            return new ListDto<ChannelDto>
            {
                HasNext = false,
                Items = ChannelMock.GetDtoList()
            };
        }

        public async Task<IListDto<ChannelDto>> GetAllChannelsDtoByTenantIdAsync(GetAllChannelDto requestDto)
        {
            return new ListDto<ChannelDto>
            {
                HasNext = false,
                Items = ChannelMock.GetDtoList()
            };
        }

        public ChannelDto GetDtoById(Guid id)
        {
            return ChannelMock.GetDto();
        }

        public IListDto<ChannelDto> SearchChannel(string search)
        {
            return new ListDto<ChannelDto>
            {
                HasNext = false,
                Items = ChannelMock.GetDtoList()
            };
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            
        }

        public async Task<ChannelDto> Update(Guid id, ChannelDto dto)
        {
            return ChannelMock.GetDto();
        }

        public ChannelDto UpdateChannel(Guid id, ChannelDto channelDto)
        {
            return ChannelMock.GetDto();
        }
    }
}
