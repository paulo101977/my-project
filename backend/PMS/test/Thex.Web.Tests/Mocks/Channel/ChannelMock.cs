﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Web.Tests.Mocks
{
    public class ChannelMock
    {
        public static ChannelDto GetDto(string name = "Channel")
        {
            return new ChannelDto()
            {
                Id = new Guid("BD65C4A7-1FD9-4C21-85DA-AAF6E4109E12"),
                ChannelCodeName = "Teste 1",
                BusinessSourceId = 1,
                BusinessSourceName = "Booking",
                ChannelCodeId = 1,
                Description = "Teste unitário 1",
                DistributionAmount = 500.00m,
                IsActive = true,
                MarketSegmentId = 1
            };
        }

        public static IList<ChannelDto> GetDtoList()
        {
            var ret = new List<ChannelDto>();

            ret.Add(GetDto("Channel 1"));
            ret.Add(GetDto("Channel 2"));
            ret.Add(GetDto("Channel 3"));

            return ret;
        }

        public static Channel Get()
        {
            return new Channel()
            {
                Id = Guid.NewGuid(),
                BusinessSourceId = 1,
                ChannelCodeId = 1,
                Description = "Descrição",
                DistributionAmount = 500.00m,
                IsActive = true,
                IsDeleted = false,
                TenantId = Guid.Empty,
                CreationTime = new DateTime(2018, 10, 18, 10, 10, 20),
                CreatorUserId = new Guid("7D220627-853D-466F-835F-A06B6D6B1922")
            };
        }

        public static List<Channel> GetList()
        {
            var list = new List<Channel>();
            list.Add(new Channel()
            {
                Id = Guid.NewGuid(),
                BusinessSourceId = 1,
                ChannelCodeId = 1,
                Description = "Descrição",
                DistributionAmount = 500.00m,
                IsActive = true,
                IsDeleted = false,
                TenantId = Guid.Empty,
                CreationTime = new DateTime(2018, 10, 18, 10, 10, 20),
                CreatorUserId = new Guid("7D220627-853D-466F-835F-A06B6D6B1922")
            });

            list.Add(new Channel()
            {
                Id = Guid.NewGuid(),
                BusinessSourceId = 1,
                ChannelCodeId = 1,
                Description = "Descrição 2",
                DistributionAmount = 500.00m,
                IsActive = true,
                IsDeleted = false,
                TenantId = Guid.Empty,
                CreationTime = new DateTime(2018, 10, 18, 10, 10, 20),
                CreatorUserId = new Guid("7D220627-853D-466F-835F-A06B6D6B1922")
            });

            list.Add(new Channel()
            {
                Id = Guid.NewGuid(),
                BusinessSourceId = 1,
                ChannelCodeId = 1,
                Description = "Descrição 3",
                DistributionAmount = 500.00m,
                IsActive = true,
                IsDeleted = false,
                TenantId = Guid.Empty,
                CreationTime = new DateTime(2018, 10, 18, 10, 10, 20),
                CreatorUserId = new Guid("7D220627-853D-466F-835F-A06B6D6B1922")
            });

            return list;
        }

        public static IListDto<ChannelDto> GetIListDto()
        {
            var channelDtoList = GetDtoList();

            return new ListDto<ChannelDto>
            {
                HasNext = false,
                Items = channelDtoList
            };
        }
    }
}
