﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Dto;

namespace Thex.Web.Tests.Mocks.LevelRate
{
    public static class LevelRateMock
    {
        public static LevelRateResultDto GetLevelResultDto(string name = "Tarifa 1")
        {
            var result = new LevelRateResultDto();
            result.LevelRateHeader = new LevelRateHeaderDto
            {
                CurrencyId = Guid.NewGuid(),
                InitialDate = DateTime.MinValue,
                EndDate = DateTime.MaxValue,
                IsActive = true,
                RateName = name,
                LevelId = Guid.NewGuid()
            };

            result.LevelRateList = GetLevelRateList();
            result.LevelRateMealPlanTypeList = GetLevelRateMealPlanTypeList();

            return result;
        }

        public static ICollection<LevelRateMealPlanTypeDto> GetLevelRateMealPlanTypeList()
        {
            var levelRateMealPlanList = new List<LevelRateMealPlanTypeDto>();
            levelRateMealPlanList.Add(new LevelRateMealPlanTypeDto()
            {
                AdultMealPlanAmount = 0,
                Child1MealPlanAmount = 0,
                Child2MealPlanAmount = 0,
                Child3MealPlanAmount = 0,
                MealPlanTypeId = 1,
                IsMealPlanTypeDefault = false
            });

            levelRateMealPlanList.Add(new LevelRateMealPlanTypeDto()
            {
                AdultMealPlanAmount = 50,
                Child1MealPlanAmount = 50,
                Child2MealPlanAmount = 50,
                Child3MealPlanAmount = 50,
                MealPlanTypeId = 2,
                IsMealPlanTypeDefault = false
            });

            levelRateMealPlanList.Add(new LevelRateMealPlanTypeDto()
            {
                AdultMealPlanAmount = 50,
                Child1MealPlanAmount = 50,
                Child2MealPlanAmount = 50,
                Child3MealPlanAmount = 50,
                MealPlanTypeId = 3,
                IsMealPlanTypeDefault = false
            });

            levelRateMealPlanList.Add(new LevelRateMealPlanTypeDto()
            {
                AdultMealPlanAmount = 50,
                Child1MealPlanAmount = 50,
                Child2MealPlanAmount = 50,
                Child3MealPlanAmount = 50,
                MealPlanTypeId = 4,
                IsMealPlanTypeDefault = false
            });

            levelRateMealPlanList.Add(new LevelRateMealPlanTypeDto()
            {
                AdultMealPlanAmount = 50,
                Child1MealPlanAmount = 50,
                Child2MealPlanAmount = 50,
                Child3MealPlanAmount = 50,
                MealPlanTypeId = 5,
                IsMealPlanTypeDefault = false
            });

            levelRateMealPlanList.Add(new LevelRateMealPlanTypeDto()
            {
                AdultMealPlanAmount = 100,
                Child1MealPlanAmount = 100,
                Child2MealPlanAmount = 100,
                Child3MealPlanAmount = 100,
                MealPlanTypeId = 6,
                IsMealPlanTypeDefault = false
            });

            levelRateMealPlanList.Add(new LevelRateMealPlanTypeDto()
            {
                AdultMealPlanAmount = 100,
                Child1MealPlanAmount = 100,
                Child2MealPlanAmount = 100,
                Child3MealPlanAmount = 100,
                MealPlanTypeId = 7,
                IsMealPlanTypeDefault = true
            });

            return levelRateMealPlanList;
        }

        private static ICollection<LevelRateDto> GetLevelRateList()
        {
            var levelRateList = new List<LevelRateDto>();
            levelRateList.Add(new LevelRateDto
            {
                CurrencyId = Guid.NewGuid(),
                RoomTypeId = 1,
                LevelRateHeaderId = Guid.NewGuid(),
                Id = Guid.Empty,
                Pax1Amount = 150,
                Pax2Amount = 150,
                Pax3Amount = 150,
                Pax4Amount = 150,
                Pax5Amount = 150,
                PaxAdditionalAmount = 150,
                Child1Amount = 150,
                Child2Amount = 150,
                Child3Amount = 150
            });

            levelRateList.Add(new LevelRateDto
            {
                CurrencyId = Guid.NewGuid(),
                RoomTypeId = 2,
                LevelRateHeaderId = Guid.NewGuid(),
                Id = Guid.Empty,
                Pax1Amount = 200,
                Pax2Amount = 200,
                Pax3Amount = 200,
                Pax4Amount = 200,
                Pax5Amount = 200,
                PaxAdditionalAmount = 200,
                Child1Amount = 200,
                Child2Amount = 200,
                Child3Amount = 200
            });

            levelRateList.Add(new LevelRateDto
            {
                CurrencyId = Guid.NewGuid(),
                RoomTypeId = 3,
                LevelRateHeaderId = Guid.NewGuid(),
                Id = Guid.Empty,
                Pax1Amount = 250,
                Pax2Amount = 250,
                Pax3Amount = 250,
                Pax4Amount = 250,
                Pax5Amount = 250,
                PaxAdditionalAmount = 250,
                Child1Amount = 250,
                Child2Amount = 250,
                Child3Amount = 250
            });

            return levelRateList;
        }
    }
}
