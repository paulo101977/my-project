﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Web.Tests.Mocks
{
    public class LevelRateAppServiceMock : ILevelRateAppService
    {
        public void Create(LevelRateResultDto dto)
        {
            
        }

        public Task<LevelRateDto> Create(LevelRateDto dto)
        {
            return new LevelRateDto().AsTask();
        }

        public Task Delete(Guid id)
        {
            return Task.CompletedTask;
        }

        public Task<LevelRateDto> Get(DefaultGuidRequestDto id)
        {
            return new LevelRateDto().AsTask();
        }

        public async Task<IListDto<LevelRateDto>> GetAll(GetAllLevelRateDto request)
        {
            return new ListDto<LevelRateDto>
            {
                HasNext = false,
                Items = new List<LevelRateDto>()
            };
        }

        public IListDto<LevelRateHeaderDto> GetAllDto()
        {
            return new ListDto<LevelRateHeaderDto>
            {
                HasNext = false,
                Items = new List<LevelRateHeaderDto>()
            };
        }

        public IListDto<PropertyBaseRateByLevelRateResultDto> GetAllLevelRateForPropertyBaseRate(DateTime initialDate, DateTime endDate, Guid? currencyId)
        {
            return new ListDto<PropertyBaseRateByLevelRateResultDto>();
        }

        public LevelRateResultRequestDto GetAllLevelsAvailable(LevelRateRequestDto request)
        {
            return new LevelRateResultRequestDto();
        }

        public LevelRateResultDto GetById(Guid levelRateHeaderId)
        {
            return new LevelRateResultDto();
        }

        public void Remove(Guid id)
        {
            
        }

        public void ToggleActive(Guid id)
        {
           
        }

        public void Update(LevelRateResultDto dto, Guid id)
        {
           
        }

        public async Task<LevelRateDto> Update(Guid id, LevelRateDto dto)
        {
            return await new LevelRateDto().AsTask();
        }
    }
}
