﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Dto;
using Thex.Dto.RoomTypes;
using Tnf.Dto;

namespace Thex.Web.Tests.Mocks
{
    public class RoomTypeAppServiceMock : IRoomTypeAppService
    {
        public static Guid customerGuid = Guid.Parse("1b92f96f-6a71-4655-a0b9-93c5f6ad9637");

        private List<RoomTypeDto> list = new List<RoomTypeDto>() {
            new RoomTypeDto
            {
                PropertyId = 1,
                Abbreviation = "LXO",
                Name = "Luxo",
                Order = 3,
                MinimumRate = 100,
                MaximumRate = 200,
                AdultCapacity = 2,
                ChildCapacity = 3,
                DistributionCode = "DLXO"
            }
        };

        public Task<RoomTypeDto> Create(RoomTypeDto dto)
        {
            throw new NotImplementedException();
        }

        public RoomTypeDto CreateRoomType(RoomTypeDto dto)
        {
            list.Add(dto);
            return dto;
        }

        public Task Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void DeleteRoomType(int id)
        {
            list.RemoveAll(r => r.Id == id);
        }

        public Task DeleteRoomTypeAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<RoomTypeDto> Get(DefaultIntRequestDto id)
        {
            throw new NotImplementedException();
        }

        public Task<IListDto<RoomTypeDto>> GetAll(GetAllRoomTypeDto request)
        {
            IListDto<RoomTypeDto> result = new ListDto<RoomTypeDto> { HasNext = false, Items = list };

            return result.AsTask();
        }

        public IListDto<RoomTypeOverbookingDto> GetAllRoomTypesByPropertyAndPeriodDto(GetAllRoomTypesByPropertyAndPeriodDto request, int propertyId)
        {
            throw new NotImplementedException();
        }

        public RoomTypeDto GetRoomTypeById(int roomTypeId)
        {
            return list.FirstOrDefault(r => r.Id == roomTypeId);
        }

        public RoomTypeDto GetRoomTypeByRoomId(int roomId)
        {
            throw new NotImplementedException();
        }

        public IListDto<RoomTypeDto> GetRoomTypesByPropertyId(GetAllRoomTypesDto request, int propertyId)
        {
            throw new NotImplementedException();
        }

        public void ToggleActivation(int id)
        {
            throw new NotImplementedException();
        }

        public Task<RoomTypeDto> Update(int id, RoomTypeDto dto)
        {
            throw new NotImplementedException();
        }

        public RoomTypeDto UpdateRoomType(int id, RoomTypeDto dto)
        {
            list.RemoveAll(r => r.Id == id);
            dto.Id = id;
            list.Add(dto);

            return dto;
        }

        void IRoomTypeAppService.DeleteRoomTypeAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
