﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto.ExternalRegistration;
using Tnf.Dto;

namespace Thex.Web.Tests.Mocks
{
    public class ExternalRegistrationDomainServiceMock
    {
        public static Guid GetReservationUid()
            => Guid.Parse("e8e582a7-35fe-4974-95c6-748cba8c64f6");

        public static string GetEmail()
            => "teste@email.com";

        public static string GetRandomString()
            => $"Teste {Guid.NewGuid().ToString()}";

        public static string GetRandomDocumentString()
            => $"12322274606";

        public static string GetRandomPhoneString()
            => $"2112313242";

        public static DateTime GetRandomDate()
            => DateTime.Now;

        public static Guid GetRandomGuid()
            => Guid.NewGuid();

        public static string GetLink()
            => "http://pms-front/roomlist?token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMCIsImp0aSI6ImJjOTY0M2Q3MGNiNzQ2ZmU4M2NmMjdhNWVkMGUwY2Q1Iiwic3ViIjoiODVlYjA3M2QyNzNlNDdhYWE2Y2I1ZjU2ZjRkMmYyOWQiLCJMb2dnZXJVc2VyRW1haWwiOiJzYXVsby5icml0bzAxQGdtYWlsLmNvbSIsIlByb3BlcnR5SWQiOiIxMSIsIlRlbmFudElkIjoiMjNlYjgwM2MtNzI2YS00YzdjLWIyNWItMmMyMmE1Njc5M2Q5IiwiTG9nZ2VkVXNlclVpZCI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMCIsIlJlc2VydmF0aW9uVWlkIjoiYWNkMGYxNTgtMDk1OS00N2FhLWE4MzEtMzY5OGE1MDczZDBmIiwiR3Vlc3RSZXNlcnZhdGlvbkl0ZW1VaWQiOiIwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDAiLCJJc0V4dGVybmFsQWNjZXNzIjoidHJ1ZSIsIm5iZiI6MTU2NzEwNzUwMiwiZXhwIjoxNTcwOTc5ODA2LCJpYXQiOjE1NjcxMDc1MDIsImlzcyI6InRoZXhJc3N1ZXIiLCJhdWQiOiJ0aGV4QXVkaWVuY2UifQ.UYAIv4BRi9B-6YOBqcHXwK5PjTPL7FyG2TU2d9ZaF-Zp_4Q0AoVZYC_SoZ6dF0mBhdZ3x7ACv8CPLvEcNWlpM90tYthhR51YXKOLmvu7JkynJqb8kuXXA_wPlphjIxvcQ1NMrAra53DJywv4oIHtaFNhEeOHfcueZuhvqTzE58qICrV9r4tq8_Qp_UfU29hCMj7ImfJZp1G-tLLGsUpVK0Y8YOp8UffALVCQnBC-S5jdNs6JUK_QsJDMPOz5zp3_yZVIYSxZiDvcpC4HnKkmIliRg-8oqksbA-5m4MqgTPLY4PCn9mfn1yZOCi67TIprZFygjQdc7ysOo6wxt44J7A";

        public static ExternalRoomListHeaderDto GetExternalRoomListHeaderDto()
            => new ExternalRoomListHeaderDto
            {
                AdditionalAddressDetails = GetRandomString(),
                Adultcount = 1,
                Childcount = 1,
                CityId = 1,
                CountryId = 1,
                Latitude = 1,
                Longitude = 1,
                MaxDate = DateTime.Now,
                MinDate = DateTime.Now,
                StateId = 1,
                CityName = GetRandomString(),
                CountryCode = "BR",
                CountryName = GetRandomString(),
                CountryTwoLetterIsoCode = "BR",
                LanguageIsoCode = "pt-BR",
                Name = GetRandomString(),
                Neighborhood = GetRandomString(),
                PostalCode = GetRandomString(),
                ReservationCode = GetRandomString(),
                State = GetRandomString(),
                StreetName = GetRandomString(),
                StreetNumber = GetRandomString(),
            };


        public static IListDto<ExternalRoomListDto> GetExternalRoomListDto()
            => new ListDto<ExternalRoomListDto>()
            {
                HasNext = false,
                Items = new List<ExternalRoomListDto>
                {
                    new ExternalRoomListDto
                    {
                        ReservationUid = GetRandomGuid(),
                        ReservationItemUid = GetRandomGuid(),
                        ReservationItemCode = GetRandomString(),
                        RoomNumber = GetRandomString(),
                        RoomTypeName = GetRandomString(),
                        RoomTypeAbbreviation = GetRandomString(),
                        CheckinDate = GetRandomDate(),
                        CheckoutDate = GetRandomDate(),
                        EstimatedArrivalDate = GetRandomDate(),
                        EstimatedDepartureDate = GetRandomDate(),
                        IsValid = true,
                        StatusId = 1,
                        SearchableNames = new List<string> { "Joao","Maria" },
                        AdultCount = 1,
                        ChildCount = 1,
                        ReservationItemStatus = GetRandomString(),
                    },
                    new ExternalRoomListDto
                    {
                        ReservationUid = GetRandomGuid(),
                        ReservationItemUid = GetRandomGuid(),
                        ReservationItemCode = GetRandomString(),
                        RoomNumber = GetRandomString(),
                        RoomTypeName = GetRandomString(),
                        RoomTypeAbbreviation = GetRandomString(),
                        CheckinDate = GetRandomDate(),
                        CheckoutDate = GetRandomDate(),
                        EstimatedArrivalDate = GetRandomDate(),
                        EstimatedDepartureDate = GetRandomDate(),
                        IsValid = true,
                        StatusId = 1,
                        SearchableNames = new List<string> { "Joao2","Maria2" },
                        AdultCount = 1,
                        ChildCount = 1,
                        ReservationItemStatus = GetRandomString(),
                    },
                }

            };

        public static List<GuestRegistrationDapperDto> GetGuestRegistrationDapperDto()
            => new List<GuestRegistrationDapperDto>
            {
                new GuestRegistrationDapperDto
                {
                    Id = GetRandomGuid(),
                    GuestReservationItemId = 1
                },
                new GuestRegistrationDapperDto
                {
                    Id = GetRandomGuid(),
                    GuestReservationItemId = 2
                }
            };

        public static List<ExternalGuestRegistrationDto> GetExternalGuestListByReservationItemUid()
            => new List<ExternalGuestRegistrationDto>
            {
                new ExternalGuestRegistrationDto
                {
                    Id = GetRandomGuid(),
                    GuestReservationItemId = 1,
                    FirstName = GetRandomString(),
                    LastName = GetRandomString(),
                    SocialName = GetRandomString(),
                    DocumentInformation = "039.209.410-04",
                    DocumentTypeId = 1,
                    CityId = 1,
                    CountryId = 1,
                    Latitude = 1,
                    Longitude = 1,
                    StateId = 1,
                    CityName = GetRandomString(),
                    CountryCode = "BR",
                    CountryName = GetRandomString(),
                    CountryTwoLetterIsoCode = "BR",
                    LanguageIsoCode = "pt-BR",
                    Neighborhood = GetRandomString(),
                    PostalCode = GetRandomString(),
                    StreetName = GetRandomString(),
                    StreetNumber = GetRandomString(),
                },
                new ExternalGuestRegistrationDto
                {
                    Id = GetRandomGuid(),
                    GuestReservationItemId = 2,
                    FirstName = GetRandomString(),
                    LastName = GetRandomString(),
                    SocialName = GetRandomString(),
                    DocumentInformation = "624.920.880-14",
                    DocumentTypeId = 1
                }
            };
    }
}
