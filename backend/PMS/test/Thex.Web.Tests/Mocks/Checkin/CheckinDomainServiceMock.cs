﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;

namespace Thex.Web.Tests.Mocks
{
    public class CheckinDomainServiceMock
    {
        public static List<GuestReservationItem> GetGuestReservationItemList()
        {
            var result = new List<GuestReservationItem>
            {
                new GuestReservationItem
                {
                    GuestEmail = "teste@teste.com",
                    ReservationItemId = 1,
                    GuestName = "Teste",
                    Id = 1,
                    EstimatedArrivalDate = DateTime.Parse("2019-08-08"),
                    EstimatedDepartureDate = DateTime.Parse("2019-08-10"),
                    PctDailyRate = 50,
                    GuestDocument = "1234",
                    GuestTypeId = 1
                }
            };

            return result;
        }        

        public static long GetReservationId()
        {
            var reservationId = 1;
            return reservationId;
        }

        public static int GetPropertyId()
        {
            var propertyId = 1;
            return propertyId;
        }

        public static int GetBusinessSourceId()
        {
            var businessSourceId = 1;
            return businessSourceId;
        }

        public static int GetMarketSegmentId()
        {
            var marketSegmentId = 1;
            return marketSegmentId;
        }

        public static List<BillingAccount> GetBillingAccountList()
        {
            var result = new List<BillingAccount>
            {
                new BillingAccount
                {
                   
                }
            };

            return result;
        }

        public static DateTime GetCheckinDate()
        {
            var checkinDate = DateTime.Parse("2019-08-08");
            return checkinDate;
        }

        public static List<GuestReservationItem> GetGuestReservationItemCheckinList()
        {
            var result = new List<GuestReservationItem>
            {
                new GuestReservationItem
                {
                    GuestEmail = "teste@teste.com",
                    ReservationItemId = 1,
                    GuestName = "Teste",
                    Id = 1,
                    EstimatedArrivalDate = DateTime.Parse("2019-08-08"),
                    EstimatedDepartureDate = DateTime.Parse("2019-08-10"),
                    PctDailyRate = 50,
                    GuestDocument = "1234",
                    GuestTypeId = 1,
                    GuestStatusId = (int)ReservationStatus.Checkin,
                    CheckInDate = DateTime.Now,
                    CheckInHour = DateTime.Now,
                }
            };

            return result;
        }

        public static ReservationItem GetReservationItemWithGuestReservationItemCheckinList()
        {
            var result = new ReservationItem {
                Id = 1,
                ReservationItemStatusId = (int)ReservationStatus.Checkin,
                CheckInDate = DateTime.Now,
                CheckInHour = DateTime.Now,
                GuestReservationItemList = GetGuestReservationItemCheckinList()
            };

            return result;
        }

        public static List<Guid> GetBillingAccountIdList()
        {
            var result = new List<Guid>
            {
                Guid.NewGuid()
            };

            return result;
        }

        public static Guid GetBillingAccountId()
        {
            var result = Guid.NewGuid();

            return result;
        }

        public static List<Guid> GetBillingAccountItemIdList()
        {
            var result = new List<Guid>
            {
                Guid.NewGuid()
            };

            return result;
        }

        public static List<GuestReservationItem> GetGuestReservationItemCheckinAndCheckoutList()
        {
            var result = new List<GuestReservationItem>
            {
                new GuestReservationItem
                {
                    GuestEmail = "teste@teste.com",
                    ReservationItemId = 1,
                    GuestName = "Teste",
                    Id = 1,
                    EstimatedArrivalDate = DateTime.Parse("2019-08-08"),
                    EstimatedDepartureDate = DateTime.Parse("2019-08-10"),
                    PctDailyRate = 50,
                    GuestDocument = "1234",
                    GuestTypeId = 1,
                    GuestStatusId = (int)ReservationStatus.Checkin,
                    CheckInDate = DateTime.Now,
                    CheckInHour = DateTime.Now,
                },
                new GuestReservationItem
                {
                    GuestEmail = "teste@teste.com",
                    ReservationItemId = 1,
                    GuestName = "Teste",
                    Id = 2,
                    EstimatedArrivalDate = DateTime.Parse("2019-08-08"),
                    EstimatedDepartureDate = DateTime.Parse("2019-08-10"),
                    PctDailyRate = 50,
                    GuestDocument = "1234",
                    GuestTypeId = 1,
                    GuestStatusId = (int)ReservationStatus.Checkout,
                    CheckInDate = DateTime.Now,
                    CheckInHour = DateTime.Now,
                }
            };

            return result;
        }

        public static ReservationItem GetReservationItemWalkin()
        {
            var result = new ReservationItem
            {
                Id = 1,
                WalkIn = true
            };

            return result;
        }

        public static BillingAccount GetBillingAccountGroup()
        {
            var result = new BillingAccount
            {
                Id = Guid.NewGuid()
            };

            return result;
        }

        public static List<ReservationItem> GetReservationItemWithTwoCheckinList()
        {
            var result = new List<ReservationItem>
            {
                new ReservationItem
                {
                    Id = 1,
                    ReservationItemStatusId = (int)ReservationStatus.Checkin
                },
                new ReservationItem
                {
                    Id = 2,
                    ReservationItemStatusId = (int)ReservationStatus.Checkin
                }
            };

            return result;
        }

        public static List<ReservationItem> GetReservationItemWithOneCheckinAndOneCheckoutList()
        {
            var result = new List<ReservationItem>
            {
                new ReservationItem
                {
                    Id = 1,
                    ReservationItemStatusId = (int)ReservationStatus.Checkin
                },
                new ReservationItem
                {
                    Id = 2,
                    ReservationItemStatusId = (int)ReservationStatus.Checkout
                }
            };

            return result;
        }

        public static List<ReservationItem> GetReservationItemWithOneCheckinAndOnePendingList()
        {
            var result = new List<ReservationItem>
            {
                new ReservationItem
                {
                    Id = 1,
                    ReservationItemStatusId = (int)ReservationStatus.Checkin
                },
                new ReservationItem
                {
                    Id = 2,
                    ReservationItemStatusId = (int)ReservationStatus.Pending
                }
            };

            return result;
        }

        public static List<ReservationItem> GetReservationItemWithOneCheckinAndOneConfirmedList()
        {
            var result = new List<ReservationItem>
            {
                new ReservationItem
                {
                    Id = 1,
                    ReservationItemStatusId = (int)ReservationStatus.Checkin
                },
                new ReservationItem
                {
                    Id = 2,
                    ReservationItemStatusId = (int)ReservationStatus.Confirmed
                }
            };

            return result;
        }
    }
}
