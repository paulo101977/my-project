﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Thex.Application;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Application.Interfaces.ReservationReport;
using Thex.Application.Services;
using Thex.Application.Services.ReservationReport;
using Thex.Common;
using Thex.Kernel;
using Thex.Domain;
using Thex.Domain.Entities;
using Thex.Domain.Factories;
using Thex.Domain.Interfaces.Factories;
using Thex.Domain.Interfaces.Integration;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Domain.Services;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.EntityFrameworkCore.Repositories;
using Thex.Infra;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Repositories;
using Thex.Infra.Repositories.Read;
using Thex.Maps.Geocode;
using Thex.Web.Tests.Context;
using Thex.Web.Tests.Mocks;
using Tnf.Configuration;
using Thex.AspNetCore.Security.Interfaces;
using Thex.AspNetCore.Security;
using Thex.GenericLog;


namespace Thex.Web.Tests
{
    public class StartupIntegratedPropertyTest
    {
        private IConfiguration Configuration { get; set; }

        public StartupIntegratedPropertyTest(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            

            services
               .AddMapperDependency()
               .AddPMSApplicationServiceDependency(options =>
               {
                   var settingsSection = Configuration.GetSection("MapsGeocodeConfig");
                   var settings = settingsSection.Get<MapsGeocodeConfig>();
               })
               .AddTnfAspNetCoreSetupTest()
               .AddTnfEfCoreSqliteInMemory()                      
               .RegisterDbContextToSqliteInMemory<ThexContext, FakeThexContext>();  // Configura o cotexto a ser usado em memória pelo EntityFrameworkCore

            ConfigureAuthetication(services);


            //Registro das dependências
            AddApplicationServiceDependency(services);
            AddDomainDependency(services);
            AddInfraDependency(services);

            
            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            // Configura o uso do teste
            app.UseTnfAspNetCoreSetupTest(options =>
            {
                // Adiciona as configurações de localização da aplicação a ser testada
                options.UseDomainLocalization();

                // Configuração global de como irá funcionar o Get utilizando o repositorio do Tnf
                // O exemplo abaixo registra esse comportamento através de uma convenção:
                // toda classe que implementar essas interfaces irão ter essa configuração definida
                // quando for consultado um método que receba a interface IRequestDto do Tnf
                options.Repository(repositoryConfig =>
                {
                    repositoryConfig.Entity<IEntityGuid>(entity =>
                        entity.RequestDto<IDefaultGuidRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityInt>(entity =>
                        entity.RequestDto<IDefaultIntRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityLong>(entity =>
                        entity.RequestDto<IDefaultLongRequestDto>((e, d) => e.Id == d.Id));
                });

                // Configuração para usar teste com EntityFramework em memória
                //options.UnitOfWorkOptions().IsolationLevel = IsolationLevel.Unspecified;
            });

            // Habilita o uso do UnitOfWork em todo o request
            app.UseTnfUnitOfWork();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }

        public void ConfigureAuthetication(IServiceCollection services)
        {
            ITokenConfiguration tokenConfiguration = new TokenConfiguration();
            new ConfigureFromConfigurationOptions<ITokenConfiguration>(
                Configuration.GetSection("TokenConfiguration"))
                    .Configure(tokenConfiguration);
            services.AddSingleton(tokenConfiguration);


            ISignConfiguration signConfiguration = new SignConfiguration();
            services.AddSingleton(signConfiguration);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IApplicationUser>(e => new ApplicationUserMock());
            services.AddScoped<ITokenManager, TokenManager>();

            services.AddAuthentication(authOptions =>
            {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultSignOutScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultForbidScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(bearerOptions =>
            {
                var paramsValidation = bearerOptions.TokenValidationParameters;
                paramsValidation.IssuerSigningKey = signConfiguration.Key;
                paramsValidation.ValidAudience = tokenConfiguration.Audience;
                paramsValidation.ValidIssuer = tokenConfiguration.Issuer;

                // Valida a assinatura de um token recebido
                paramsValidation.ValidateIssuerSigningKey = true;

                // Verifica se um token recebido ainda é válido
                paramsValidation.ValidateLifetime = true;

                // Tempo de tolerância para a expiração de um token (utilizado
                // caso haja problemas de sincronismo de horário entre diferentes
                // computadores envolvidos no processo de comunicação)
                paramsValidation.ClockSkew = TimeSpan.Zero;

                bearerOptions.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        // Add the access_token as a claim, as we may actually need it
                        var accessToken = context.SecurityToken as JwtSecurityToken;
                        if (accessToken != null)
                        {
                            // Todo : Get User and Tenant
                            //ITokenInfo tokenInfo = ExtractTokenInfo(accessToken);
                        } 

                        return Task.CompletedTask;
                    }
                };
                bearerOptions.SaveToken = true;

            });

            // Ativa o uso do token como forma de autorizar o acesso
            // a recursos deste projeto
            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());
            });
        }

        private IServiceCollection AddApplicationServiceDependency(IServiceCollection services)
        {
            // Dependencia do projeto Thex.Domain
            services
                .AddMapsGeocodeDependency(options =>
                {
                    var settingsSection = Configuration.GetSection("MapsGeocodeConfig");
                    var settings = settingsSection.Get<MapsGeocodeConfig>();
                })
                .AddDomainDependency();

            services.AddTnfDefaultConventionalRegistrations();

            // Registro dos serviços
            services.AddTransient<IAgreementTypeAdapter, AgreementTypeAdapter>();
            services.AddTransient<IApplicationModuleAdapter, ApplicationModuleAdapter>();
            services.AddTransient<IApplicationParameterAdapter, ApplicationParameterAdapter>();
            services.AddTransient<IAuditStepTypeAdapter, AuditStepTypeAdapter>();
            services.AddTransient<IBedTypeAdapter, BedTypeAdapter>();
            services.AddTransient<IBillingAccountAdapter, BillingAccountAdapter>();
            services.AddTransient<IBillingAccountItemAdapter, BillingAccountItemAdapter>();
            services.AddTransient<IBillingAccountItemTransferAdapter, BillingAccountItemTransferAdapter>();
            services.AddTransient<IBillingAccountItemTypeAdapter, BillingAccountItemTypeAdapter>();
            services.AddTransient<IBillingAccountTypeAdapter, BillingAccountTypeAdapter>();
            services.AddTransient<IBillingInvoiceAdapter, BillingInvoiceAdapter>();
            services.AddTransient<IBillingInvoiceModelAdapter, BillingInvoiceModelAdapter>();
            services.AddTransient<IBillingInvoicePropertyAdapter, BillingInvoicePropertyAdapter>();
            services.AddTransient<IBillingInvoicePropertySupportedTypeAdapter, BillingInvoicePropertySupportedTypeAdapter>();
            services.AddTransient<IBillingItemAdapter, BillingItemAdapter>();
            services.AddTransient<IBillingItemCategoryAdapter, BillingItemCategoryAdapter>();
            services.AddTransient<IBillingItemPaymentConditionAdapter, BillingItemPaymentConditionAdapter>();
            services.AddTransient<IBillingItemTypeAdapter, BillingItemTypeAdapter>();
            services.AddTransient<IBillingTaxAdapter, BillingTaxAdapter>();
            services.AddTransient<IBrandAdapter, BrandAdapter>();
            services.AddTransient<IBusinessPartnerAdapter, BusinessPartnerAdapter>();
            services.AddTransient<IBusinessPartnerTypeAdapter, BusinessPartnerTypeAdapter>();
            services.AddTransient<IBusinessSourceAdapter, BusinessSourceAdapter>();
            services.AddTransient<IChainAdapter, ChainAdapter>();
            services.AddTransient<IChainBusinessSourceAdapter, ChainBusinessSourceAdapter>();
            services.AddTransient<IChainMarketSegmentAdapter, ChainMarketSegmentAdapter>();
            services.AddTransient<ICompanyAdapter, CompanyAdapter>();
            services.AddTransient<ICompanyClientAdapter, CompanyClientAdapter>();
            services.AddTransient<ICompanyClientContactPersonAdapter, CompanyClientContactPersonAdapter>();
            services.AddTransient<ICompanyContactPersonAdapter, CompanyContactPersonAdapter>();
            services.AddTransient<IContactInformationAdapter, ContactInformationAdapter>();
            services.AddTransient<IContactInformationTypeAdapter, ContactInformationTypeAdapter>();
            services.AddTransient<ICountryCurrencyAdapter, CountryCurrencyAdapter>();
            services.AddTransient<ICountryLanguageAdapter, CountryLanguageAdapter>();
            services.AddTransient<ICountryPersonDocumentTypeAdapter, CountryPersonDocumentTypeAdapter>();
            services.AddTransient<ICountrySubdivisionAdapter, CountrySubdivisionAdapter>();
            services.AddTransient<ICountrySubdivisionTranslationAdapter, CountrySubdivisionTranslationAdapter>();
            services.AddTransient<ICountrySubdvisionServiceAdapter, CountrySubdvisionServiceAdapter>();
            services.AddTransient<ICurrencyAdapter, CurrencyAdapter>();
            services.AddTransient<ICurrencyExchangeAdapter, CurrencyExchangeAdapter>();
            services.AddTransient<IDocumentAdapter, DocumentAdapter>();
            services.AddTransient<IEmployeeAdapter, EmployeeAdapter>();
            services.AddTransient<IFeatureGroupAdapter, FeatureGroupAdapter>();
            services.AddTransient<IGeneralOccupationAdapter, GeneralOccupationAdapter>();
            services.AddTransient<IGratuityTypeAdapter, GratuityTypeAdapter>();
            services.AddTransient<IGuestAdapter, GuestAdapter>();
            services.AddTransient<IGuestRegistrationAdapter, GuestRegistrationAdapter>();
            services.AddTransient<IGuestReservationItemAdapter, GuestReservationItemAdapter>();
            services.AddTransient<IHousekeepingStatusAdapter, HousekeepingStatusAdapter>();
            services.AddTransient<IHousekeepingStatusPropertyAdapter, HousekeepingStatusPropertyAdapter>();
            services.AddTransient<ILocationAdapter, LocationAdapter>();
            services.AddTransient<ILocationCategoryAdapter, LocationCategoryAdapter>();
            services.AddTransient<IMarketSegmentAdapter, MarketSegmentAdapter>();
            services.AddTransient<IMealPlanTypeAdapter, MealPlanTypeAdapter>();
            services.AddTransient<IOccupationAdapter, OccupationAdapter>();
            services.AddTransient<IParameterTypeAdapter, ParameterTypeAdapter>();
            services.AddTransient<IPaymentTypeAdapter, PaymentTypeAdapter>();
            services.AddTransient<IPersonAdapter, PersonAdapter>();
            services.AddTransient<IPersonInformationAdapter, PersonInformationAdapter>();
            services.AddTransient<IPersonInformationTypeAdapter, PersonInformationTypeAdapter>();
            services.AddTransient<IPlasticAdapter, PlasticAdapter>();
            services.AddTransient<IPlasticBrandAdapter, PlasticBrandAdapter>();
            services.AddTransient<IPlasticBrandPropertyAdapter, PlasticBrandPropertyAdapter>();
            services.AddTransient<IPolicyTypeAdapter, PolicyTypeAdapter>();
            services.AddTransient<IPropertyAdapter, PropertyAdapter>();
            services.AddTransient<IPropertyAuditProcessAdapter, PropertyAuditProcessAdapter>();
            services.AddTransient<IPropertyAuditProcessStepAdapter, PropertyAuditProcessStepAdapter>();
            services.AddTransient<IPropertyAuditProcessStepErrorAdapter, PropertyAuditProcessStepErrorAdapter>();
            services.AddTransient<IPropertyBaseRateAdapter, PropertyBaseRateAdapter>();
            services.AddTransient<IPropertyBusinessPartnerAdapter, PropertyBusinessPartnerAdapter>();
            services.AddTransient<IPropertyCompanyClientCategoryAdapter, PropertyCompanyClientCategoryAdapter>();
            services.AddTransient<IPropertyContactPersonAdapter, PropertyContactPersonAdapter>();
            services.AddTransient<IPropertyCurrencyExchangeAdapter, PropertyCurrencyExchangeAdapter>();
            services.AddTransient<IPropertyGuestPrefsAdapter, PropertyGuestPrefsAdapter>();
            services.AddTransient<IPropertyGuestTypeAdapter, PropertyGuestTypeAdapter>();
            services.AddTransient<IPropertyMealPlanTypeAdapter, PropertyMealPlanTypeAdapter>();
            services.AddTransient<IPropertyParameterAdapter, PropertyParameterAdapter>();
            services.AddTransient<IPropertyPolicyAdapter, PropertyPolicyAdapter>();
            services.AddTransient<IPropertyTypeAdapter, PropertyTypeAdapter>();
            services.AddTransient<IRatePlanAdapter, RatePlanAdapter>();
            services.AddTransient<IRatePlanCommissionAdapter, RatePlanCommissionAdapter>();
            services.AddTransient<IRatePlanCompanyClientAdapter, RatePlanCompanyClientAdapter>();
            services.AddTransient<IRatePlanRoomTypeAdapter, RatePlanRoomTypeAdapter>();
            services.AddTransient<IReasonAdapter, ReasonAdapter>();
            services.AddTransient<IReasonCategoryAdapter, ReasonCategoryAdapter>();
            services.AddTransient<IReservationAdapter, ReservationAdapter>();
            services.AddTransient<IReservationBudgetAdapter, ReservationBudgetAdapter>();
            services.AddTransient<IReservationConfirmationAdapter, ReservationConfirmationAdapter>();
            services.AddTransient<IReservationItemAdapter, ReservationItemAdapter>();
            services.AddTransient<IRoomAdapter, RoomAdapter>();
            services.AddTransient<IRoomBlockingAdapter, RoomBlockingAdapter>();
            services.AddTransient<IRoomLayoutAdapter, RoomLayoutAdapter>();
            services.AddTransient<IRoomTypeAdapter, RoomTypeAdapter>();
            services.AddTransient<IRoomTypeBedTypeAdapter, RoomTypeBedTypeAdapter>();
            services.AddTransient<IStatusAdapter, StatusAdapter>();
            services.AddTransient<IStatusCategoryAdapter, StatusCategoryAdapter>();
            services.AddTransient<ISubscriptionAdapter, SubscriptionAdapter>();
            services.AddTransient<ITenantAdapter, TenantAdapter>();
            services.AddTransient<IGuestAdapter, GuestAdapter>();

            services.AddTransient<IAgreementTypeAppService, AgreementTypeAppService>();
            services.AddTransient<IApplicationModuleAppService, ApplicationModuleAppService>();
            services.AddTransient<IApplicationParameterAppService, ApplicationParameterAppService>();
            services.AddTransient<IAuditStepTypeAppService, AuditStepTypeAppService>();
            services.AddTransient<IAvailabilityAppService, AvailabilityAppService>();
            services.AddTransient<IBedTypeAppService, BedTypeAppService>();
            services.AddTransient<IBillingAccountAppService, BillingAccountAppService>();
            services.AddTransient<IBillingAccountClosureAppService, BillingAccountClosureAppService>();
            services.AddTransient<IBillingAccountItemAppService, BillingAccountItemAppService>();
            services.AddTransient<IBillingAccountItemTransferAppService, BillingAccountItemTransferAppService>();
            services.AddTransient<IBillingAccountItemTypeAppService, BillingAccountItemTypeAppService>();
            services.AddTransient<IBillingAccountTypeAppService, BillingAccountTypeAppService>();
            services.AddTransient<IBillingInvoiceAppService, BillingInvoiceAppService>();
            services.AddTransient<IBillingInvoiceModelAppService, BillingInvoiceModelAppService>();
            services.AddTransient<IBillingInvoicePropertyAppService, BillingInvoicePropertyAppService>();
            services.AddTransient<IBillingInvoicePropertySupportedTypeAppService, BillingInvoicePropertySupportedTypeAppService>();
            services.AddTransient<IBillingItemAppService, BillingItemAppService>();
            services.AddTransient<IBillingItemCategoryAppService, BillingItemCategoryAppService>();
            services.AddTransient<IBillingItemPaymentConditionAppService, BillingItemPaymentConditionAppService>();
            services.AddTransient<IBillingItemPDVAppService, BillingItemPDVAppService>();
            services.AddTransient<IBillingItemTypeAppService, BillingItemTypeAppService>();
            services.AddTransient<IBillingTaxAppService, BillingTaxAppService>();
            services.AddTransient<IBrandAppService, BrandAppService>();
            services.AddTransient<IBusinessPartnerAppService, BusinessPartnerAppService>();
            services.AddTransient<IBusinessPartnerTypeAppService, BusinessPartnerTypeAppService>();
            services.AddTransient<IBusinessSourceAppService, BusinessSourceAppService>();
            services.AddTransient<IChainAppService, ChainAppService>();
            services.AddTransient<IChainBusinessSourceAppService, ChainBusinessSourceAppService>();
            services.AddTransient<IChainMarketSegmentAppService, ChainMarketSegmentAppService>();
            services.AddTransient<ICheckinAppService, CheckinAppService>();
            services.AddTransient<ICompanyAppService, CompanyAppService>();
            services.AddTransient<ICompanyClientAppService, CompanyClientAppService>();
            services.AddTransient<ICompanyClientContactPersonAppService, CompanyClientContactPersonAppService>();
            services.AddTransient<ICompanyContactPersonAppService, CompanyContactPersonAppService>();
            services.AddTransient<IContactInformationAppService, ContactInformationAppService>();
            services.AddTransient<IContactInformationTypeAppService, ContactInformationTypeAppService>();
            services.AddTransient<ICountryCurrencyAppService, CountryCurrencyAppService>();
            services.AddTransient<ICountryLanguageAppService, CountryLanguageAppService>();
            services.AddTransient<ICountryPersonDocumentTypeAppService, CountryPersonDocumentTypeAppService>();
            services.AddTransient<ICountrySubdivisionAppService, CountrySubdivisionAppService>();
            services.AddTransient<ICountrySubdivisionTranslationAppService, CountrySubdivisionTranslationAppService>();
            services.AddTransient<ICountrySubdvisionServiceAppService, CountrySubdvisionServiceAppService>();
            services.AddTransient<ICurrencyAppService, CurrencyAppService>();
            services.AddTransient<ICurrencyExchangeAppService, CurrencyExchangeAppService>();
            services.AddTransient<IDocumentAppService, DocumentAppService>();
            services.AddTransient<IEmployeeAppService, EmployeeAppService>();
            services.AddTransient<IFeatureGroupAppService, FeatureGroupAppService>();
            services.AddTransient<IGeneralOccupationAppService, GeneralOccupationAppService>();
            services.AddTransient<IGratuityTypeAppService, GratuityTypeAppService>();
            services.AddTransient<IGuestAppService, GuestAppService>();
            services.AddTransient<IGuestRegistrationAppService, GuestRegistrationAppService>();
            services.AddTransient<IGuestReservationItemAppService, GuestReservationItemAppService>();
            services.AddTransient<IHousekeepingStatusAppService, HousekeepingStatusAppService>();
            services.AddTransient<IHousekeepingStatusPropertyAppService, HousekeepingStatusPropertyAppService>();
            services.AddTransient<IIntegrationAppService, IntegrationAppService>();
            services.AddTransient<ILocationAppService, LocationAppService>();
            services.AddTransient<ILocationCategoryAppService, LocationCategoryAppService>();
            services.AddTransient<IMarketSegmentAppService, MarketSegmentAppService>();
            services.AddTransient<IMealPlanTypeAppService, MealPlanTypeAppService>();
            services.AddTransient<IOccupationAppService, OccupationAppService>();
            services.AddTransient<IParameterTypeAppService, ParameterTypeAppService>();
            services.AddTransient<IPaymentTypeAppService, PaymentTypeAppService>();
            services.AddTransient<IPersonAppService, PersonAppService>();
            services.AddTransient<IPersonInformationAppService, PersonInformationAppService>();
            services.AddTransient<IPersonInformationTypeAppService, PersonInformationTypeAppService>();
            services.AddTransient<IPlasticAppService, PlasticAppService>();
            services.AddTransient<IPlasticBrandAppService, PlasticBrandAppService>();
            services.AddTransient<IPlasticBrandPropertyAppService, PlasticBrandPropertyAppService>();
            services.AddTransient<IPolicyTypeAppService, PolicyTypeAppService>();
            services.AddTransient<IPropertyAppService, PropertyAppService>();
            services.AddTransient<IPropertyAuditProcessAppService, PropertyAuditProcessAppService>();
            services.AddTransient<IPropertyAuditProcessStepAppService, PropertyAuditProcessStepAppService>();
            services.AddTransient<IPropertyAuditProcessStepErrorAppService, PropertyAuditProcessStepErrorAppService>();
            services.AddTransient<IPropertyBaseRateAppService, PropertyBaseRateAppService>();
            services.AddTransient<IPropertyBusinessPartnerAppService, PropertyBusinessPartnerAppService>();
            services.AddTransient<IPropertyCompanyClientCategoryAppService, PropertyCompanyClientCategoryAppService>();
            services.AddTransient<IPropertyContactPersonAppService, PropertyContactPersonAppService>();
            services.AddTransient<IPropertyCurrencyExchangeAppService, PropertyCurrencyExchangeAppService>();
            services.AddTransient<IPropertyGuestPrefsAppService, PropertyGuestPrefsAppService>();
            services.AddTransient<IPropertyGuestTypeAppService, PropertyGuestTypeAppService>();
            services.AddTransient<IPropertyMealPlanTypeAppService, PropertyMealPlanTypeAppService>();
            services.AddTransient<IPropertyParameterAppService, PropertyParameterAppService>();
            services.AddTransient<IPropertyPolicyAppService, PropertyPolicyAppService>();
            services.AddTransient<IPropertyTypeAppService, PropertyTypeAppService>();
            services.AddTransient<IRateCalendarAppService, RateCalendarAppService>();
            services.AddTransient<IRatePlanAppService, RatePlanAppService>();
            services.AddTransient<IRatePlanCommissionAppService, RatePlanCommissionAppService>();
            services.AddTransient<IRatePlanCompanyClientAppService, RatePlanCompanyClientAppService>();
            services.AddTransient<IRatePlanRoomTypeAppService, RatePlanRoomTypeAppService>();
            services.AddTransient<IReasonAppService, ReasonAppService>();
            services.AddTransient<IReasonCategoryAppService, ReasonCategoryAppService>();
            services.AddTransient<IReservationAppService, ReservationAppService>();
            services.AddTransient<IReservationBudgetAppService, ReservationBudgetAppService>();
            services.AddTransient<IReservationConfirmationAppService, ReservationConfirmationAppService>();
            services.AddTransient<IReservationItemAppService, ReservationItemAppService>();
            services.AddTransient<IReservationReportAppService, ReservationReportAppService>();
            services.AddTransient<IRoomAppService, RoomAppService>();
            services.AddTransient<IRoomBlockingAppService, RoomBlockingAppService>();
            services.AddTransient<IRoomLayoutAppService, RoomLayoutAppService>();
            services.AddTransient<IRoomTypeAppService, RoomTypeAppService>();
            services.AddTransient<IRoomTypeBedTypeAppService, RoomTypeBedTypeAppService>();
            services.AddTransient<IStatusAppService, StatusAppService>();
            services.AddTransient<IStatusCategoryAppService, StatusCategoryAppService>();
            services.AddTransient<ISubscriptionAppService, SubscriptionAppService>();
            services.AddTransient<ITenantAppService, TenantAppService>();
            services.AddTransient<IGuestAppService, GuestAppService>();

            services.AddTransient<IReservationReportReadRepository, ReservationReportReadRepository>();
            services.AddTransient<IReservationReportRepository, ReservationReportRepository>();

            return services;
        }

        private IServiceCollection AddDomainDependency(IServiceCollection services)
        {
            // Adiciona as dependencias para utilização dos serviços de crud generico do Tnf
            services
                .AddTnfDomain()
                .AddCommonDependency();

            // Para habilitar as convenções do Tnf para Injeção de dependência (ITransientDependency, IScopedDependency, ISingletonDependency)
            // descomente a linha abaixo:
            services.AddTnfDefaultConventionalRegistrations();

            // Registro dos serviços
            services.AddTransient<IReservationItemStatusFactory, ReservationItemStatusFactory>();
            services.AddTransient<IEmailFactory, EmailFactory>();

            services.AddTransient<IBillingAccountDomainService, BillingAccountDomainService>();
            services.AddTransient<IBillingAccountItemDomainService, BillingAccountItemDomainService>();
            services.AddTransient<IBusinessSourceDomainService, BusinessSourceDomainService>();
            services.AddTransient<ICompanyClientContactPersonDomainService, CompanyClientContactPersonDomainService>();
            services.AddTransient<ICompanyClientDomainService, CompanyClientDomainService>();
            services.AddTransient<IGuestReservationItemDomainService, GuestReservationItemDomainService>();
            services.AddTransient<IMarketSegmentDomainService, MarketSegmentDomainService>();
            services.AddTransient<IPropertyAuditProcessDomainService, PropertyAuditProcessDomainService>();
            services.AddTransient<IPropertyParameterDomainService, PropertyParameterDomainService>();
            services.AddTransient<IRatePlanDomainService, RatePlanDomainService>();
            services.AddTransient<IReservationBudgetDomainService, ReservationBudgetDomainService>();
            services.AddTransient<IReservationDomainService, ReservationDomainService>();
            services.AddTransient<IReservationItemDomainService, ReservationItemDomainService>();
            services.AddTransient<IReservationItemStatusDomainService, ReservationItemStatusDomainService>();
            services.AddTransient<IRoomDomainService, RoomDomainService>();
            services.AddTransient<IRoomLayoutDomainService, RoomLayoutDomainService>();
            services.AddTransient<IRoomTypeDomainService, RoomTypeDomainService>();

            return services;
        }

        private IServiceCollection AddInfraDependency(IServiceCollection services)
        {

            services
                .AddTnfEntityFrameworkCore()    // Configura o uso do EntityFrameworkCore registrando os contextos que serão usados pela aplicação
                .AddMapperDependency();         // Configura o uso do AutoMappper

            services.AddTnfDefaultConventionalRegistrations();
            services.AddScoped<IGenericLogHandler, GenericLogHandler>();

            // Registro dos repositórios
            //services.AddTransient<IInvoiceIntegration, InvoiceIntegration>();

            services.AddTransient<IBillingAccountItemRepository, BillingAccountItemRepository>();
            services.AddTransient<IBillingAccountItemTransferRepository, BillingAccountItemTransferRepository>();
            services.AddTransient<IBillingAccountRepository, BillingAccountRepository>();
            services.AddTransient<IBillingInvoicePropertyRepository, BillingInvoicePropertyRepository>();
            services.AddTransient<IBillingInvoicePropertySupportedTypeRepository, BillingInvoicePropertySupportedTypeRepository>();
            services.AddTransient<IBillingInvoiceRepository, BillingInvoiceRepository>();
            services.AddTransient<IBillingItemCategoryRepository, BillingItemCategoryRepository>();
            services.AddTransient<IBillingItemRepository, BillingItemRepository>();
            services.AddTransient<IBillingTaxRepository, BillingTaxRepository>();
            services.AddTransient<IBusinessSourceRepository, BusinessSourceRepository>();
            services.AddTransient<IChainBusinessSourceRepository, ChainBusinessSourceRepository>();
            services.AddTransient<IChannelRepository, ChannelRepository>();
            services.AddTransient<ICompanyClientContactPersonRepository, CompanyClientContactPersonRepository>();
            services.AddTransient<ICompanyClientRepository, CompanyClientRepository>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();
            services.AddTransient<IContactInformationRepository, ContactInformationRepository>();
            services.AddTransient<IDocumentRepository, DocumentRepository>();
            services.AddTransient<IGuestReservationItemReadRepository, GuestReservationItemReadRepository>();
            services.AddTransient<IGuestReservationItemRepository, GuestReservationItemRepository>();
            services.AddTransient<IHousekeepingStatusPropertyRepository, HousekeepingStatusPropertyRepository>();
            services.AddTransient<ILocationRepository, LocationRepository>();
            services.AddTransient<IMarketSegmentRepository, MarketSegmentRepository>();
            services.AddTransient<IPersonInformationRepository, PersonInformationRepository>();
            services.AddTransient<IPersonRepository, PersonRepository>();
            services.AddTransient<IPropertyBaseRateRepository, PropertyBaseRateRepository>();
            services.AddTransient<IPropertyMealPlanTypeRepository, PropertyMealPlanTypeRepository>();
            services.AddTransient<IPropertyParameterRepository, PropertyParameterRepository>();
            services.AddTransient<IPropertyPolicyRepository, PropertyPolicyRepository>();
            services.AddTransient<IRatePlanCommissionRepository, RatePlanCommissionRepository>();
            services.AddTransient<IRatePlanCompanyClientRepository, RatePlanCompanyClientRepository>();
            services.AddTransient<IRatePlanRepository, RatePlanRepository>();
            services.AddTransient<IRatePlanRoomTypeRepository, RatePlanRoomTypeRepository>();
            services.AddTransient<IReasonRepository, ReasonRepository>();
            services.AddTransient<IReservationBudgetRepository, ReservationBudgetRepository>();
            services.AddTransient<IReservationRepository, ReservationRepository>();
            services.AddTransient<IRoomBlockingRepository, RoomBlockingRepository>();
            services.AddTransient<IRoomRepository, RoomRepository>();
            services.AddTransient<IRoomTypeRepository, RoomTypeRepository>();
            services.AddTransient<ILevelRepository, LevelRepository>();


            services.AddTransient<IAgreementTypeReadRepository, AgreementTypeReadRepository>();
            services.AddTransient<IBillingAccountItemReadRepository, BillingAccountItemReadRepository>();
            services.AddTransient<IBillingAccountReadRepository, BillingAccountReadRepository>();
            services.AddTransient<IBillingInvoicePropertyReadRepository, BillingInvoicePropertyReadRepository>();
            services.AddTransient<IBillingInvoicePropertySupportedTypeReadRepository, BillingInvoicePropertySupportedTypeReadRepository>();
            services.AddTransient<IBillingInvoiceReadRepository, BillingInvoiceReadRepository>();
            services.AddTransient<IBillingItemCategoryReadRepository, BillingItemCategoryReadRepository>();
            services.AddTransient<IBillingItemPDVReadRepository, BillingItemPDVReadRepository>();
            services.AddTransient<IBillingItemReadRepository, BillingItemReadRepository>();
            services.AddTransient<IBrandReadRepository, BrandReadRepository>();
            services.AddTransient<IBusinessSourceReadRepository, BusinessSourceReadRepository>();
            services.AddTransient<IChainReadRepository, ChainReadRepository>();
            services.AddTransient<IChannelReadRepository, ChannelReadRepository>();
            services.AddTransient<IChannelCodeReadRepository, ChannelCodeReadRepository>();
            services.AddTransient<ICompanyClientContactPersonReadRepository, CompanyClientContactPersonReadRepository>();
            services.AddTransient<ICompanyClientReadRepository, CompanyClientReadRepository>();
            services.AddTransient<ICompanyReadRepository, CompanyReadRepository>();
            services.AddTransient<IContactInformationReadRepository, ContactInformationReadRepository>();
            services.AddTransient<ICountryPersonDocumentTypeReadRepository, CountryPersonDocumentTypeReadRepository>();
            services.AddTransient<ICountrySubdivisionTranslationReadRepository, CountrySubdivisionTranslationReadRepository>();
            services.AddTransient<ICountrySubdvisionServiceReadRepository, CountrySubdvisionServiceReadRepository>();
            services.AddTransient<ICurrencyReadRepository, CurrencyReadRepository>();
            services.AddTransient<IDocumentReadRepository, DocumentReadRepository>();
            services.AddTransient<IGuestRegistrationReadRepository, GuestRegistrationReadRepository>();
            services.AddTransient<IHousekeepingStatusPropertyReadRepository, HousekeepingStatusPropertyReadRepository>();
            services.AddTransient<ILocationReadRepository, LocationReadRepository>();
            services.AddTransient<IMarketSegmentReadRepository, MarketSegmentReadRepository>();
            services.AddTransient<IOccupationReadRepository, OccupationReadRepository>();
            services.AddTransient<IPersonReadRepository, PersonReadRepository>();
            services.AddTransient<IPlasticBrandPropertyReadRepository, PlasticBrandPropertyReadRepository>();
            services.AddTransient<IPolicyTypesReadRepository, PolicyTypesReadRepository>();
            services.AddTransient<IPropertyAuditProcessReadRepository, PropertyAuditProcessReadRepository>();
            services.AddTransient<IPropertyAuditProcessStepReadRepository, PropertyAuditProcessStepReadRepository>();
            services.AddTransient<IPropertyBaseRateReadRepository, PropertyBaseRateReadRepository>();
            services.AddTransient<IPropertyCompanyClientCategoryReadRepository, PropertyCompanyClientCategoryReadRepository>();
            services.AddTransient<IPropertyCompanyClientCategoryRepository, PropertyCompanyClientCategoryRepository>();
            services.AddTransient<IPropertyCurrencyExchangeReadRepository, PropertyCurrencyExchangeReadRepository>();
            services.AddTransient<IPropertyMealPlanTypeReadRepository, PropertyMealPlanTypeReadRepository>();
            services.AddTransient<IPropertyParameterReadRepository, PropertyParameterReadRepository>();
            services.AddTransient<IPropertyPolicyReadRepository, PropertyPolicyReadRepository>();
            services.AddTransient<IPropertyReadRepository, PropertyReadRepository>();
            services.AddTransient<IRatePlanReadRepository, RatePlanReadRepository>();
            services.AddTransient<IRatePlanRoomTypeReadRepository, RatePlanRoomTypeReadRepository>();
            services.AddTransient<IReasonCategoryReadRepository, ReasonCategoryReadRepository>();
            services.AddTransient<IReasonReadRepository, ReasonReadRepository>();
            services.AddTransient<IReservationBudgetReadRepository, ReservationBudgetReadRepository>();
            services.AddTransient<IReservationConfirmationReadRepository, ReservationConfirmationReadRepository>();
            services.AddTransient<IReservationItemReadRepository, ReservationItemReadRepository>();
            services.AddTransient<IReservationReadRepository, ReservationReadRepository>();
            services.AddTransient<IRoomBlockingReadRepository, RoomBlockingReadRepository>();
            services.AddTransient<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository, RoomReadRepository>();
            services.AddTransient<IRoomTypeReadRepository, RoomTypeReadRepository>();
            services.AddTransient<ITenantReadRepository, TenantReadRepository>();
            services.AddTransient<IPropertyGuestTypeReadRepository, PropertyGuestTypeReadRepository>();
            services.AddTransient<IRateTypeReadRepository, RateTypeReadRepository>();
            services.AddTransient<IPropertyBaseRateHeaderHistoryReadRepository, PropertyBaseRateHeaderHistoryReadRepository>();
            services.AddTransient<IPropertyBaseRateHistoryReadRepository, PropertyBaseRateHistoryReadRepository>();
            services.AddTransient<ILevelReadRepository, LevelReadRepository>();

            services.AddTransient<IPowerBiReadRepository, PowerBiReadRepository>();
            return services;
        }
    }
}
