﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.AspNetCore.Security;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Common;
using Thex.Kernel;

namespace Thex.Web.Tests.Middlewares
{
    public class AttachTokenTest
    {
        private readonly RequestDelegate _next;

        public AttachTokenTest(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext http, ITokenManager tokenManager, IApplicationUser applicationUser)
        {
            var tokenInfo = new TokenInfo()
            {
                IsAdmin = applicationUser.IsAdmin,
                PropertyId = int.Parse(applicationUser.PropertyId),
                TenantId = applicationUser.TenantId,
                PreferredLanguage = applicationUser.PreferredLanguage,
                TimeZoneName = applicationUser.TimeZoneName,
                PropertyLanguage = applicationUser.PropertyLanguage,
                ChainId = int.Parse(applicationUser.ChainId),
                LoggedUserEmail = applicationUser.UserEmail,
                LoggedUserName = applicationUser.UserName,
                LoggedUserUid = applicationUser.UserUid
            };

            var claimsIdentity = tokenManager.CreateClaimIdentity(Guid.NewGuid(), tokenInfo);
            var token = tokenManager.CreateUserToken(claimsIdentity);
            http.Request.Headers.Add("Authorization", $"Bearer {token.NewPasswordToken}");

            return _next(http);
        }
    }
}
