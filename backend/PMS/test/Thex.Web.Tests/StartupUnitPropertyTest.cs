﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using Thex.Application;
using Thex.Application.Interfaces;
using Thex.Application.Interfaces.RateProposal;
using Thex.Common;
using Thex.Kernel;
using Thex.Domain;
using Thex.Infra;
using Thex.Infra.Context;
using Thex.Web.Tests.Context;
using Thex.Web.Tests.Middlewares;
using Thex.Web.Tests.Mocks;
using Thex.Web.Tests.Mocks.RateProposal;
using Thex.AspNetCore.Security;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Web.Tests.Mocks.PropertyRateStrategy;
using Tnf.Configuration;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.AspNetCore.Filters;
using Thex.GenericLog;
using Thex.Web.Tests.Mocks.GenericLogHandler;
using Thex.Web.Tests.Mocks.GenericLogEventBuilder;
using Thex.Maps.Geocode;
using Thex.Web.Tests.Mocks.Room;
using Thex.Domain.Interfaces.Repositories;
using Thex.Common.Interfaces.Observables;
using Thex.Inventory.Domain.Services;
using Microsoft.AspNetCore.Http;

namespace Thex.Web.Tests
{
    public class StartupUnitPropertyTest
    {
        private IConfiguration Configuration { get; set; }

        public StartupUnitPropertyTest(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();

            ITokenConfiguration tokenConfiguration = new TokenConfiguration();
            new ConfigureFromConfigurationOptions<ITokenConfiguration>(
                Configuration.GetSection("TokenConfiguration"))
                    .Configure(tokenConfiguration);
            services.AddSingleton(tokenConfiguration);

            // Configura o setup de teste para AspNetCore
            services
                     .AddMapperDependency()
                     .AddPMSApplicationServiceDependency(options =>
                     {
                         var settingsSection = Configuration.GetSection("MapsGeocodeConfig");
                         var settings = settingsSection.Get<MapsGeocodeConfig>();
                     })
                     .AddTnfAspNetCoreSetupTest()
                     .AddTnfEfCoreSqliteInMemory()
                     .RegisterDbContextToSqliteInMemory<ThexContext, FakeThexContext>();  // Configura o cotexto a ser usado em memória pelo EntityFrameworkCore       

            services.AddInfraDependency();
            services.AddDomainDependency();

            services.AddSingleton(Configuration.GetSection("ServicesConfiguration").Get<ServicesConfiguration>());
            services.AddScoped<ActionLogFilter>();

            // Registro dos serviços de Mock
            services.AddSingleton<IHttpContextAccessor>(a => new HttpContextAccessorMock());
            services.AddTransient<IRoomTypeAppService, RoomTypeAppServiceMock>();
            services.AddTransient<IChannelAppService, ChannelAppServiceMock>();
            services.AddTransient<IApplicationUser>(a => new ApplicationUserMock());
            services.AddTransient<IRateProposalAppService, RateProposalAppServiceMock>();
            services.AddTransient<IPropertyRateStrategyAppService, PropertyRateStrategyAppServiceMock>();
            services.AddTransient<ILevelAppService, LevelAppServiceMock>();
            services.AddTransient<ILevelRateAppService, LevelRateAppServiceMock>();
            services.AddTransient<IRoomAppService, RoomAppServiceMock>();

            services.AddTransient<Inventory.Domain.Repositories.Interfaces.IPropertyParameterRepository, PropertyParameterInventoryMock>();
            services.AddTransient<Inventory.Domain.Repositories.Interfaces.IRoomTypeInventoryRepository, RoomTypeInventoryRepositoryMock>();

            services.AddTransient<ITokenManager, TokenManager>();
            services.AddTransient<ISignConfiguration, SignConfiguration>();
            services.AddScoped<IGenericLogEventBuilder, GenericLogEventBuilderMock>();
            services.AddScoped<IGenericLogHandler, GenericLogHandlerMock>();

            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, ITokenManager tokenManager)
        {
            // Configura o uso do teste
            app.UseTnfAspNetCoreSetupTest();

            app.UseMiddleware<AttachTokenTest>();
            
            app.UseTnfAspNetCoreSetupTest(options =>
            {
                options.UseDomainLocalization();

                options.Repository(repositoryConfig =>
                {
                    repositoryConfig.Entity<IEntityGuid>(entity =>
                        entity.RequestDto<IDefaultGuidRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityInt>(entity =>
                        entity.RequestDto<IDefaultIntRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityLong>(entity =>
                        entity.RequestDto<IDefaultLongRequestDto>((e, d) => e.Id == d.Id));

                });
            });

            // Habilita o uso do UnitOfWork em todo o request
            app.UseTnfUnitOfWork();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
