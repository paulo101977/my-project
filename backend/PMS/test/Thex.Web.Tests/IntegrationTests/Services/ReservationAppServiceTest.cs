﻿using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using System.Globalization;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Services;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.InMemoryDatabaseSetup;
using Tnf.AspNetCore.TestBase;
using Tnf.Domain.Services;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.Web.Tests.IntegrationTests.Services
{
    public class ReservationAppServiceTest : TnfAspNetCoreIntegratedTestBase<StartupIntegratedPropertyTest>
    {
        private readonly IReservationAppService _reservationAppService;
        private readonly INotificationHandler _notificationHandler;
        public ReservationAppServiceTest()
        {
            _reservationAppService = ServiceProvider.GetRequiredService<IReservationAppService>();
            _notificationHandler = ServiceProvider.GetRequiredService<INotificationHandler>();
            SetRequestCulture(CultureInfo.GetCultureInfo("pt-BR"));
            BaseSetup baseSetup = new BaseSetup(ServiceProvider, _notificationHandler);
        }
        //[Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestReservationItemAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICountrySubdivisionTranslationReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICompanyClientReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICompanyClientContactPersonReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationConfirmationAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IDomainService<ReservationConfirmation>>().ShouldNotBeNull();
            ServiceProvider.GetService<IDomainService<Plastic>>().ShouldNotBeNull();
            ServiceProvider.GetService<IPlasticAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IPersonReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IContactInformationAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IDocumentReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyCurrencyExchangeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationBudgetAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocalizationManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemStatusDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomBlockingReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomAppService>().ShouldNotBeNull();
        }
        //[Fact]
        public void Should_Get_Reservation_By_ReservationId()
        {
            // Act
            var reservation = _reservationAppService.GetReservation(new DefaultLongRequestDto() { Id = 1, Expand = "ReservationItemList" });
            //Assert
            reservation.Id.ShouldBe(1);
            reservation.ReservationItemList.Count.ShouldBe(2);
        }
        //[Fact]
        public void Should_Get_Reservation_By_ReservationItemId()
        {
            //Act
            var reservation = _reservationAppService.GetByReservationItemId(new DefaultLongRequestDto() { Id = 1, Expand = "ReservationItemList" });
            //Assert
            reservation.Id.ShouldBe(1);
            reservation.ReservationItemList.Count.ShouldBe(2);
        }
        //[Fact]
        //public void Should_Create_Reservation_Without_CompanyClient()
        //{
        //    //Arrange
        //    var reservationDto = new ReservationDto
        //    {
        //        PropertyId = 1,
        //        ContactName = "Nome do ContactName",
        //        ContactEmail = "Nome do ContactEmail",
        //        ContactPhone = "Nome do ContactPhone",
        //        BusinessSourceId = 1,
        //        MarketSegmentId = 2,
        //        ReservationItemList = new List<ReservationItemDto>
        //        {
        //            new ReservationItemDto {
        //                EstimatedArrivalDate = new DateTime(2018, 05, 23),
        //                EstimatedDepartureDate = new DateTime(2018, 05, 26),
        //                ReservationItemCode = "0001",
        //                RequestedRoomTypeId = 1,
        //                ReceivedRoomTypeId = 1,
        //                RoomId = 1,
        //                AdultCount = 1,
        //                ChildCount = 0,
        //                ReservationItemStatusId = (int)ReservationStatus.Confirmed,
        //                RoomLayoutId = Guid.Parse("00b59f7a-dcb1-4ccd-b1cd-3b35cf6d9125"),
        //                ExtraBedCount = 0,
        //                ExtraCribCount = 0,
        //                GuestReservationItemList = new List<GuestReservationItemDto>
        //                {
        //                    new GuestReservationItemDto {
        //                    }
        //                }
        //            },
        //        }
        //    };
        //    //Act
        //    _reservationAppService.CreateReservation(reservationDto);
        //    //Assert
        //    Assert.False(_notificationHandler.HasNotification());
        //}
    }
}