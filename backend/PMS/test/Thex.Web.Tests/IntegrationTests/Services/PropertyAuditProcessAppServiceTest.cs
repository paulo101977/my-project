﻿using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using System.Globalization;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Services;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Repositories.Read;
using Thex.InMemoryDatabaseSetup;
using Tnf.AspNetCore.TestBase;
using Tnf.Domain.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.Web.Tests.IntegrationTests.Services
{
    public class PropertyAuditProcessAppServiceTest : TnfAspNetCoreIntegratedTestBase<StartupIntegratedPropertyTest>
    {
        private readonly IPropertyAuditProcessAppService _propertyAuditProcessAppService;
        private readonly INotificationHandler _notificationHandler;

        public PropertyAuditProcessAppServiceTest()
        {
            _propertyAuditProcessAppService = ServiceProvider.GetRequiredService<IPropertyAuditProcessAppService>();
            _notificationHandler = ServiceProvider.GetRequiredService<INotificationHandler>();

            SetRequestCulture(CultureInfo.GetCultureInfo("pt-BR"));
            BaseSetup baseSetup = new BaseSetup(ServiceProvider, _notificationHandler);
        }

        //[Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyAuditProcessAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyAuditProcessDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<PropertyParameterReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyAuditProcessStepReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomBlockingAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingStatusPropertyAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyAuditProcessReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IDomainService<PropertyAuditProcessStep>>().ShouldNotBeNull();
            ServiceProvider.GetService<IDomainService<PropertyAuditProcessStepError>>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestReservationItemAppService>().ShouldNotBeNull();
        }

        //[Fact]
        public void ShouldToRunAudit()
        {
            var propertyId = 1;

            var result = _propertyAuditProcessAppService.Execute(propertyId);

            Assert.True(result != null && !_notificationHandler.HasNotification());   
        }
    }
}
