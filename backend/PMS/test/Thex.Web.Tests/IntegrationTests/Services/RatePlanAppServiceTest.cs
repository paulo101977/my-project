﻿using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using System.Globalization;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.ReadInterfaces;
using Thex.InMemoryDatabaseSetup;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.Web.Tests.IntegrationTests.Services
{
    public class RatePlanAppServiceTest : TnfAspNetCoreIntegratedTestBase<StartupIntegratedPropertyTest>
    {
        private readonly IRatePlanAppService _ratePlanAppService;

        public RatePlanAppServiceTest()
        {
            _ratePlanAppService = ServiceProvider.GetRequiredService<IRatePlanAppService>();

            var notificationHandler = new NotificationHandler(ServiceProvider);

            SetRequestCulture(CultureInfo.GetCultureInfo("pt-BR"));

            BaseSetup baseSetup = new BaseSetup(ServiceProvider, notificationHandler);
        }

        //[Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<ICurrencyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IAgreementTypeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyMealPlanTypeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanRoomTypeAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanCompanyClientAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanCommissionAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanRoomTypeAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyCompanyClientCategoryReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRateTypeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IMarketSegmentAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyBaseRateHistoryReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanCommissionAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }
    }
}
