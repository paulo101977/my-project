﻿using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Globalization;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.InMemoryDatabaseSetup;
using Tnf.AspNetCore.TestBase;
using Tnf.Domain.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;
using Tnf.Application.Services;

namespace Thex.Web.Tests.IntegrationTests.Services
{
    public class PropertyBaseRateAppServiceTest : TnfAspNetCoreIntegratedTestBase<StartupIntegratedPropertyTest>
    {
        private readonly IPropertyBaseRateAppService _propertyBaseRateAppService;
        private readonly INotificationHandler _notificationHandler;

        public PropertyBaseRateAppServiceTest()
        {
            _propertyBaseRateAppService = ServiceProvider.GetRequiredService<IPropertyBaseRateAppService>();            
           _notificationHandler = ServiceProvider.GetRequiredService<INotificationHandler>();

            SetRequestCulture(CultureInfo.GetCultureInfo("pt-BR"));
            BaseSetup baseSetup = new BaseSetup(ServiceProvider, _notificationHandler);
        }

        //[Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyBaseRateAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IDomainService<PropertyBaseRate>>().ShouldNotBeNull();
            ServiceProvider.GetService<ICurrencyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyMealPlanTypeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomTypeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyBaseRateReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICurrencyAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyBaseRateRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyBaseRateHeaderHistoryAppService>().ShouldNotBeNull();
        }

        //[Fact]
        public async void Should_CreatePropertyBaseRate()
        {
            var baseRateConfigurationDto = GetBaseRateConfigurationDtoMock();

            Client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer","eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjdkMjIwNjI3LTg1M2QtNDY2Zi04MzVmLWEwNmI2ZDZiMTkyMiIsImp0aSI6IjQ5MDllMGZkNDM3NTRiZGJhMjlmMDQ5MGRkNjRmYjA5Iiwic3ViIjoiN2QyMjA2MjctODUzZC00NjZmLTgzNWYtYTA2YjZkNmIxOTIyIiwiTG9nZ2VyVXNlckVtYWlsIjoidGhleEB0b3R2c2NtbmV0LmNvbS5iciIsIkxvZ2dlZFVzZXJOYW1lIjoidGhleEB0b3R2c2NtbmV0LmNvbS5iciIsIkxvZ2dlZFVzZXJVaWQiOiI3ZDIyMDYyNy04NTNkLTQ2NmYtODM1Zi1hMDZiNmQ2YjE5MjIiLCJQcm9wZXJ0eUlkIjoiMTEiLCJCcmFuZElkIjoiMSIsIkNoYWluSWQiOiIxIiwiVGVuYW50SWQiOiIyM2ViODAzYy03MjZhLTRjN2MtYjI1Yi0yYzIyYTU2NzkzZDkiLCJJc0FkbWluIjoidHJ1ZSIsIlByZWZlcnJlZEN1bHR1cmUiOiJwdC1iciIsIlByZWZlcnJlZExhbmd1YWdlIjoicHQtYnIiLCJQcm9wZXJ0eUN1bHR1cmUiOiIiLCJQcm9wZXJ0eUxhbmd1YWdlIjoiIiwiVGltZVpvbmVOYW1lIjoiQW1lcmljYS9TYW9fUGF1bG8iLCJuYmYiOjE1MzYwODMzNTIsImV4cCI6MTUzODY3NTM1MiwiaWF0IjoxNTM2MDgzMzUyLCJpc3MiOiJ0aGV4SXNzdWVyIiwiYXVkIjoidGhleEF1ZGllbmNlIn0.KFsjK2_Ur5D9qtofA13e_rF57jAhEvAAVj-aw-G6ulUs_zgiZ2vgwzy2R438KYFjVECeszDF7wbXDCxnN99cUa3cF9NrwQzYvVgQHhDzI_3_uAushD5QqCP1StQ4uLHWoG73tZr7WUmoFxwO2C7H_zqp6mwgSaW7l6xE9JtYQ6zQI2_CJEX1JvHAYpEWO4F_LBYARxfya4-xTpNO1eYXxFiNBlu6Na73pr6uEaUPNdmP8xtb7b4ZUCXIQL5_wRtk-1gLY1hA3q4oglxEX0G_jpo3pxqrG_NLAmKn2xzVknEBtXRbaxgOTpe8hQFIglzbfodX27t05BUKnBenzjHJ5w");

            var response = await PostResponseAsObjectAsync<BaseRateConfigurationDto, BaseRateConfigurationDto>(
               RouteConsts.PropertyBaseRate,
               baseRateConfigurationDto,
               System.Net.HttpStatusCode.OK
           );

            //await _propertyBaseRateAppService.Create(baseRateConfigurationDto);

            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreatePropertyBaseRate()
        {
            var baseRateConfigurationDto = GetBaseRateConfigurationDtoMock();
            baseRateConfigurationDto.StartDate = new DateTime(2018, 09, 29);
            baseRateConfigurationDto.FinalDate = new DateTime(2018, 09, 20);

            _propertyBaseRateAppService.Create(baseRateConfigurationDto);
            
            Assert.True(_notificationHandler.HasNotification());
        }

        private BaseRateConfigurationDto GetBaseRateConfigurationDtoMock()
        {
            var baseRateConfigurationRoomTypeList = new List<BaseRateConfigurationRoomTypeDto>();
            baseRateConfigurationRoomTypeList.Add(new BaseRateConfigurationRoomTypeDto()
            {
                Pax1Amount = 150,
                Pax2Amount = 150,
                Child1Amount = 100,
                Child2Amount = 100,
                Child3Amount = 100,
                RoomTypeId = 1,
            });

            baseRateConfigurationRoomTypeList.Add(new BaseRateConfigurationRoomTypeDto()
            {
                Pax1Amount = 150,
                Pax2Amount = 150,
                Child1Amount = 100,
                Child2Amount = 100,
                Child3Amount = 100,
                RoomTypeId = 2,
            });

            var baseRateConfigurationMealPlanTypeList = new List<BaseRateConfigurationMealPlanTypeDto>();
            baseRateConfigurationMealPlanTypeList.Add(new BaseRateConfigurationMealPlanTypeDto()
            {
                AdultMealPlanAmount = 120,
                MealPlanTypeId = 1,
                Child1MealPlanAmount = 120,
                Child2MealPlanAmount = 120,
                Child3MealPlanAmount = 120,
            });

            return new BaseRateConfigurationDto()
            {
                StartDate = new DateTime(2018, 09, 20),
                FinalDate = new DateTime(2018, 09, 29),
                MealPlanTypeDefaultId = 2,
                CurrencyId = new Guid("67838acb-9525-4aeb-b0a6-127c1b986c48"),
                PropertyId = 1,
                DaysOfWeekDto = new List<int>() { 0, 1, 2, 3, 4, 5, 6 },
                BaseRateConfigurationRoomTypeList = baseRateConfigurationRoomTypeList,
                BaseRateConfigurationMealPlanTypeList = baseRateConfigurationMealPlanTypeList,
            };
        }
    }
}
