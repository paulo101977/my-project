﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Thex.Application.Interfaces;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.InMemoryDatabaseSetup;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.Web.Tests.IntegrationTests.Services
{
    public class BillingAccountItemIntegrationTest : TnfAspNetCoreIntegratedTestBase<StartupIntegratedPropertyTest>
    {
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;
        private readonly INotificationHandler _notificationHandler;

        public BillingAccountItemIntegrationTest()
        {
            _billingAccountItemAppService = ServiceProvider.GetRequiredService<IBillingAccountItemAppService>();

            _notificationHandler = new NotificationHandler(ServiceProvider);

            SetRequestCulture(CultureInfo.GetCultureInfo("pt-BR"));

            BaseSetup baseSetup = new BaseSetup(ServiceProvider, _notificationHandler);
        }

        //[Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IReasonReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemTransferAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationConfirmationReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyCurrencyExchangeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHttpContextAccessor>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
        }

        //[Fact]
        public void Should_CreateDebitIntegration()
        {
            // Arrange        
            var billingAccountItemDto = GetBillingAccountItemDebitConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemDebitDto, BillingAccountItemDebitDto>(
            $"{RouteConsts.BillingAccountItemIntegration}/debit",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreateDebitIntegration()
        {
            // Arrange        
            var billingAccountItemDto = new BillingAccountItemDebitDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemDebitDto, BillingAccountItemDebitDto>(
            $"{RouteConsts.BillingAccountItemIntegration}/debit",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_CreateReversalIntegration()
        {
            // Arrange        
            var billingAccountItemDto = GetBillingAccountItemReversalIntegrationConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemReversalParentDto, BillingAccountItemReversalParentDto>(
            $"{RouteConsts.BillingAccountItemIntegration}/reversal",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreateReversalIntegration()
        {
            // Arrange        
            var billingAccountItemDto = new BillingAccountItemReversalParentDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemReversalParentDto, BillingAccountItemReversalParentDto>(
            $"{RouteConsts.BillingAccountItemIntegration}/reversal",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        private BillingAccountItemDebitDto GetBillingAccountItemDebitConfigurationDtoMock()
        {

            //var detailIntegration = new List<DetailIntegration>
            //{
            //    new DetailIntegration()
            //    {
            //        Quantity = 2,
            //        Description = "Teste",
            //        Amount = 70
            //    }
            //};

            return new BillingAccountItemDebitDto()
            {
                PropertyId = 1,
                BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                BillingItemId = 1,
                Amount = 100,
                CurrencyId = new Guid("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                CurrencyExchangeReferenceId = new Guid("EF085323-3F4A-4B53-8B6E-01904BADDAEF"),
                TenantId = new Guid("23EB803C-726A-4C7C-B25B-2C22A56793D9"),
                DateParameter = new DateTime(2018, 09, 20)/*,
                DetailList = detailIntegration*/
            };
        }

        private BillingAccountItemReversalParentDto GetBillingAccountItemReversalIntegrationConfigurationDtoMock()
        {
            var billingAccountItemReversals = new List<BillingAccountItemReversalDto>
            {
                new BillingAccountItemReversalDto()
                {
                    BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                    OriginalBillingAccountItemId = new Guid("F3265FDA-1161-4F85-8370-0061401F1AB0"),
                    ReasonId = 1,
                    PropertyId = 1
                }
            };

            return new BillingAccountItemReversalParentDto()
            {
                BillingAccountItems = billingAccountItemReversals,
                TenantId = new Guid("23EB803C-726A-4C7C-B25B-2C22A56793D9")
            };
        }
    }
}
