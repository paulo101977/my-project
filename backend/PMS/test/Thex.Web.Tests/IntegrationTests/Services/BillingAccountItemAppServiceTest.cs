﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Globalization;
using Thex.Application.Interfaces;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Dto.BillingAccountItem;
using Thex.Infra.ReadInterfaces;
using Thex.InMemoryDatabaseSetup;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.Web.Tests.IntegrationTests.Services
{
    public class BillingAccountItemAppServiceTest : TnfAspNetCoreIntegratedTestBase<StartupIntegratedPropertyTest>
    {
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;
        private readonly INotificationHandler _notificationHandler;

        public BillingAccountItemAppServiceTest()
        {
            _billingAccountItemAppService = ServiceProvider.GetRequiredService<IBillingAccountItemAppService>();

            _notificationHandler = new NotificationHandler(ServiceProvider);

            SetRequestCulture(CultureInfo.GetCultureInfo("pt-BR"));

            BaseSetup baseSetup = new BaseSetup(ServiceProvider, _notificationHandler);
        }

        //[Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IReasonReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemTransferAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationConfirmationReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyCurrencyExchangeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHttpContextAccessor>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
        }

        //[Fact]
        public void Should_CreateDebit()
        {
            // Arrange        
            var billingAccountItemDto = GetBillingAccountItemDebitConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemDebitDto, BillingAccountItemDebitDto>(
            $"{RouteConsts.BillingAccountItem}/debit",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreateDebit()
        {
            // Arrange        
            var billingAccountItemDto = new BillingAccountItemDebitDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemDebitDto, BillingAccountItemDebitDto>(
            $"{RouteConsts.BillingAccountItem}/debit",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_CreateCredit()
        {
            // Arrange        
            var billingAccountItemDto = GetBillingAccountItemCreditConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemCreditDto, BillingAccountItemCreditDto>(
            $"{RouteConsts.BillingAccountItem}/credit",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreateCredit()
        {
            // Arrange        
            var billingAccountItemDto = new BillingAccountItemCreditDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemCreditDto, BillingAccountItemCreditDto>(
            $"{RouteConsts.BillingAccountItem}/credit",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_CreateReversal()
        {
            // Arrange        
            var billingAccountItemDto = GetBillingAccountItemReversalConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemReversalParentDto, BillingAccountItemReversalParentDto>(
            $"{RouteConsts.BillingAccountItem}/reversal",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreateReversal()
        {
            // Arrange        
            var billingAccountItemDto = new BillingAccountItemReversalParentDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemReversalParentDto, BillingAccountItemReversalParentDto>(
            $"{RouteConsts.BillingAccountItem}/reversal",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_CreateTransfer()
        {   
            // Arrange        
            var billingAccountItemDto = GetBillingAccountItemTransferConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemTransferSourceDestinationDto, BillingAccountItemTransferSourceDestinationDto>(
            $"{RouteConsts.BillingAccountItem}/transfer",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreateTransfer()
        {
            // Arrange        
            var billingAccountItemDto = new BillingAccountItemTransferSourceDestinationDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemTransferSourceDestinationDto, BillingAccountItemTransferSourceDestinationDto>(
            $"{RouteConsts.BillingAccountItem}/transfer",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_UndoTransfer()
        {
            // Arrange        
            var billingAccountItemDto = GetBillingAccountItemUndoTransferConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemUndoTransferSourceDestinationDto, BillingAccountItemUndoTransferSourceDestinationDto>(
            $"{RouteConsts.BillingAccountItem}/undotransfer",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotUndoTransfer()
        {
            // Arrange        
            var billingAccountItemDto = new BillingAccountItemUndoTransferSourceDestinationDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemUndoTransferSourceDestinationDto, BillingAccountItemUndoTransferSourceDestinationDto>(
            $"{RouteConsts.BillingAccountItem}/undotransfer",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_CreatePartial()
        {
            // Arrange        
            var billingAccountItemDto = GetBillingAccountItemCreatePartialConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemPartialDto, BillingAccountItemPartialDto>(
            $"{RouteConsts.BillingAccountItem}/partial",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreatePartial()
        {
            // Arrange        
            var billingAccountItemDto = new BillingAccountItemPartialDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemPartialDto, BillingAccountItemPartialDto>(
            $"{RouteConsts.BillingAccountItem}/partial",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_UndoPartial()
        {
            // Arrange        
            var billingAccountItemDto = GetBillingAccountItemUndoPartialConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemUndoPartialDto, BillingAccountItemUndoPartialDto>(
            $"{RouteConsts.BillingAccountItem}/undopartial",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotUndoPartial()
        {
            // Arrange        
            var billingAccountItemDto = new BillingAccountItemUndoPartialDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountItemUndoPartialDto, BillingAccountItemUndoPartialDto>(
            $"{RouteConsts.BillingAccountItem}/undopartial",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_CreateBillingAccountItemDiscountList()
        {
            // Arrange        
            var billingAccountItemDto = GetBillingAccountItemItemDiscountListConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<IList<BillingAccountItemDiscountDto>, IList<BillingAccountItemDiscountDto>>(
            $"{RouteConsts.BillingAccountItem}/discount",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreateBillingAccountItemDiscountList()
        {
            // Arrange        
            var billingAccountItemDto = new List<BillingAccountItemDiscountDto>();

            // Act
            PostResponseAsObjectAsync<IList<BillingAccountItemDiscountDto>, IList<BillingAccountItemDiscountDto>>(
            $"{RouteConsts.BillingAccountItem}/discount",
            billingAccountItemDto,
            System.Net.HttpStatusCode.OK
            );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }        

        private BillingAccountItemCreditDto GetBillingAccountItemCreditConfigurationDtoMock()
        {
            return new BillingAccountItemCreditDto()
            {
                PropertyId = 1,
                BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                BillingItemId = 1,
                Amount = 100,
                NSU = null,
                CheckNumber = null,
                InstallmentsQuantity = 0,
                CurrencyId = new Guid("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                CurrencyExchangeReferenceId = new Guid("EF085323-3F4A-4B53-8B6E-01904BADDAEF"),
                DateParameter = new DateTime(2018, 09, 20)
            };
        }

        private BillingAccountItemDebitDto GetBillingAccountItemDebitConfigurationDtoMock()
        {

            //var detailIntegration = new List<DetailIntegration>
            //{
            //    new DetailIntegration()
            //    {
            //        Quantity = 2,
            //        Description = "Teste",
            //        Amount = 70
            //    }
            //};

            return new BillingAccountItemDebitDto()
            {
                PropertyId = 1,
                BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                BillingItemId = 1,
                Amount = 100,
                CurrencyId = new Guid("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                CurrencyExchangeReferenceId = new Guid("EF085323-3F4A-4B53-8B6E-01904BADDAEF"),
                TenantId = new Guid("23EB803C-726A-4C7C-B25B-2C22A56793D9"),
                DateParameter = new DateTime(2018, 09, 20)/*,
                DetailList = detailIntegration*/
            };
        }

        private BillingAccountItemReversalParentDto GetBillingAccountItemReversalConfigurationDtoMock()
        {

            var billingAccountItemReversals = new List<BillingAccountItemReversalDto>
            {
                new BillingAccountItemReversalDto()
                {
                    BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                    OriginalBillingAccountItemId = new Guid("F3265FDA-1161-4F85-8370-0061401F1AB0"),
                    ReasonId = 1,
                    PropertyId = 1
                }
            };

            return new BillingAccountItemReversalParentDto()
            {
                BillingAccountItems = billingAccountItemReversals,
                TenantId = new Guid("23EB803C-726A-4C7C-B25B-2C22A56793D9")
            };
        }

        private BillingAccountItemTransferSourceDestinationDto GetBillingAccountItemTransferConfigurationDtoMock()
        {

            var billingAccountItemTransferChildren = new List<BillingAccountItemTransferChildDto>
            {
                new BillingAccountItemTransferChildDto()
                {
                   BillingAccountItemId = new Guid("2C0B1FE8-4524-4394-A76B-00EDE1273467")
                }
            };

            return new BillingAccountItemTransferSourceDestinationDto()
            {
                BillingAccountItemIdDestination = new Guid("F3265FDA-1161-4F85-8370-0061401F1AB0"),
                BillingAccountItemTransferList = billingAccountItemTransferChildren
            };
        }

        private BillingAccountItemUndoTransferSourceDestinationDto GetBillingAccountItemUndoTransferConfigurationDtoMock()
        {

            var billingAccountItemTransferChildren = new List<BillingAccountItemTransferChildDto>
            {
                new BillingAccountItemTransferChildDto()
                {
                   BillingAccountItemId = new Guid("2C0B1FE8-4524-4394-A76B-00EDE1273467")
                }
            };

            return new BillingAccountItemUndoTransferSourceDestinationDto()
            {
                BillingAccountItemTransferList = billingAccountItemTransferChildren
            };
        }

        private BillingAccountItemPartialDto GetBillingAccountItemCreatePartialConfigurationDtoMock()
        {
            return new BillingAccountItemPartialDto()
            {
                Id = Guid.NewGuid(),
                BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                BillingAccountItemId = new Guid("F3265FDA-1161-4F85-8370-0061401F1AB0"),
                Value = 100,
                Parts = 0
            };
        }

        private BillingAccountItemUndoPartialDto GetBillingAccountItemUndoPartialConfigurationDtoMock()
        {
            return new BillingAccountItemUndoPartialDto()
            {
                Id = Guid.NewGuid(),
                PropertyId = 1,
                BillingAccountItemId = new Guid[] { new Guid("F3265FDA-1161-4F85-8370-0061401F1AB0") }
            };
        }

        private BillingAccountItemReversalParentDto GetBillingAccountItemReversalIntegrationConfigurationDtoMock()
        {
            var billingAccountItemReversals = new List<BillingAccountItemReversalDto>
            {
                new BillingAccountItemReversalDto()
                {
                    BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                    OriginalBillingAccountItemId = new Guid("F3265FDA-1161-4F85-8370-0061401F1AB0"),
                    ReasonId = 1,
                    PropertyId = 1
                }
            };

            return new BillingAccountItemReversalParentDto()
            {
                BillingAccountItems = billingAccountItemReversals,
                TenantId = new Guid("23EB803C-726A-4C7C-B25B-2C22A56793D9")
            };
        }

        private List<Guid> GetBillingAccountItemTransferAccountsToAccountConfigurationDtoMock()
        {
            return new List<Guid>
            {
                new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE")
            };
        }

        private List<Guid> GetBillingAccountItemTransferAccountsToAccount2ConfigurationDtoMock()
        {
            return new List<Guid>
            {
                new Guid("F3265FDA-1161-4F85-8370-0061401F1AB0")
            };
        }

        private IList<BillingAccountItemDiscountDto> GetBillingAccountItemItemDiscountListConfigurationDtoMock()
        {
            return new List<BillingAccountItemDiscountDto>
            {
                new BillingAccountItemDiscountDto()
                {
                    BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                    BillingAccountItemId = new Guid("F3265FDA-1161-4F85-8370-0061401F1AB0"),
                    BillingItemId = 1,
                    IsPercentual = false,
                    Percentual = 0,
                    ReasonId = 1,
                    AmountDiscount = 0,
                    IsCredit = false,
                    PropertyId = 1,
                    CurrencyId = new Guid("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                    CurrencyExchangeReferenceId = new Guid("EF085323-3F4A-4B53-8B6E-01904BADDAEF"),
                    DateParameter = new DateTime(2018, 09, 24)
                }
            };
        }
    }
}
