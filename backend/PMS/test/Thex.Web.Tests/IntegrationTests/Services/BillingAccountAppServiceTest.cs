﻿using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using System;
using System.Globalization;
using System.Net.Http;
using Thex.Application.Interfaces;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Dto.BillingAccount;
using Thex.Infra.ReadInterfaces;
using Thex.InMemoryDatabaseSetup;
using Tnf.AspNetCore.TestBase;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.Web.Tests.IntegrationTests.Services
{
    public class BillingAccountAppServiceTest : TnfAspNetCoreIntegratedTestBase<StartupIntegratedPropertyTest>
    {
        private readonly IBillingAccountAppService _billingAccountAppService;
        private readonly INotificationHandler _notificationHandler;

        protected readonly TestServer _server;
        protected readonly HttpClient _client;

        public BillingAccountAppServiceTest()
        {

            _billingAccountAppService = ServiceProvider.GetRequiredService<IBillingAccountAppService>();

            _notificationHandler = new NotificationHandler(ServiceProvider);

            SetRequestCulture(CultureInfo.GetCultureInfo("pt-BR"));

            BaseSetup baseSetup = new BaseSetup(ServiceProvider, _notificationHandler);
          
            Client.DefaultRequestHeaders.Add("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjdkMjIwNjI3LTg1M2QtNDY2Zi04MzVmLWEwNmI2ZDZiMTkyMiIsImp0aSI6ImE5NzIxNDVjNDk2NzRjNWQ4NGM4NzM1MDU3YzI0NDk3Iiwic3ViIjoiN2QyMjA2MjctODUzZC00NjZmLTgzNWYtYTA2YjZkNmIxOTIyIiwiTG9nZ2VyVXNlckVtYWlsIjoidGhleEB0b3R2c2NtbmV0LmNvbS5iciIsIkxvZ2dlZFVzZXJOYW1lIjoidGhleEB0b3R2c2NtbmV0LmNvbS5iciIsIkxvZ2dlZFVzZXJVaWQiOiI3ZDIyMDYyNy04NTNkLTQ2NmYtODM1Zi1hMDZiNmQ2YjE5MjIiLCJQcm9wZXJ0eUlkIjoiMSIsIkJyYW5kSWQiOiIxIiwiQ2hhaW5JZCI6IjEiLCJUZW5hbnRJZCI6IjIzZWI4MDNjLTcyNmEtNGM3Yy1iMjViLTJjMjJhNTY3OTNkOSIsIklzQWRtaW4iOiJ0cnVlIiwiUHJlZmVycmVkQ3VsdHVyZSI6InB0LUJSIiwiUHJlZmVycmVkTGFuZ3VhZ2UiOiJwdC1CUiIsIlByb3BlcnR5Q3VsdHVyZSI6IiIsIlByb3BlcnR5TGFuZ3VhZ2UiOiIiLCJUaW1lWm9uZU5hbWUiOiJBbWVyaWNhL1Nhb19QYXVsbyIsIm5iZiI6MTUzODE2OTUxNywiZXhwIjoxNTQwNzYxNTE3LCJpYXQiOjE1MzgxNjk1MTcsImlzcyI6InRoZXhJc3N1ZXIiLCJhdWQiOiJ0aGV4QXVkaWVuY2UifQ.supsVWbft6gsdSWlrytrWfjNKPFWKYlZYquqMN7a08lrVkWAezpnMkY0kpKxzR_nhbPdY_AhVyZgyE216KA4YDc7i1ZSr9zLPs2XN1ZYtHvvIZPJ5X8vMmtAqdIq1GzSsaV3nTbX1qxTIeyfgJc7ianm62HkJvARjCkjqfS8-1H_14z7KDzI7bgzYXpX8Ffwqx0dxbViUrxc0HAl3Of-0eUqIZQFrn2x4MJHDsNNMHq6sn1CgRs-a4w5XgmuqnKpkTbLrKgNZdAMgUPFZ-TY5vD8LrJYRqLkVKad9b0XJNctYvKPh6EEH6nWQ_Dy3NfbcHM4JSI3w2rrHXSMI-1xgA");

        }

        //[Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<ICompanyClientAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReasonReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingInvoiceReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocalizationManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountRepository>().ShouldNotBeNull();
        }


        //[Fact]
        public void Should_CreateBillingAccount()
        {
            // Arrange                      
            var billingAccountDto = GetBillingAccountConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountDto, BillingAccountDto>(
            RouteConsts.BillingAccount,
            billingAccountDto,
            System.Net.HttpStatusCode.OK
        );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreateBillingAccount()
        {
            // Arrange         
            var billingAccountDto = new BillingAccountDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountDto, BillingAccountDto>(
            RouteConsts.BillingAccount,
            billingAccountDto,
            System.Net.HttpStatusCode.OK
        );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_CreateSparceAccount()
        {
            // Arrange         
            var billingAccountDto = GetBillingAccountConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<BillingAccountDto, BillingAccountDto>(
            $"{RouteConsts.BillingAccount}/sparce",
            billingAccountDto,
            System.Net.HttpStatusCode.OK
        );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotCreateSparceAccount()
        {
            // Arrange        
            var billingAccountDto = new BillingAccountDto();

            // Act
            PostResponseAsObjectAsync<BillingAccountDto, BillingAccountDto>(
            $"{RouteConsts.BillingAccount}/sparce",
            billingAccountDto,
            System.Net.HttpStatusCode.OK
        );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_AddBillingAccount()
        {
            // Arrange        
            var billingAccountDto = GetBillingAccountAddConfigurationDtoMock();

            // Act
            PostResponseAsObjectAsync<AddBillingAccountDto, AddBillingAccountDto>(
            $"{RouteConsts.BillingAccount}/add",
            billingAccountDto,
            System.Net.HttpStatusCode.OK
        );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotAddBillingAccount()
        {
            // Arrange        
            var billingAccountDto = GetBillingAccountAddConfigurationDtoMock();
            billingAccountDto.BillingAccountId = Guid.Empty;

            // Act
            PostResponseAsObjectAsync<AddBillingAccountDto, AddBillingAccountDto>(
            $"{RouteConsts.BillingAccount}/add",
            billingAccountDto,
            System.Net.HttpStatusCode.OK
        );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void Should_GetAllByFilters()
        {
            // Arrange        
            var billingAccountDto = GetAllBillingAccountConfigurationDtoMock();
            var propertyId = 1;

            // Act
            GetResponseAsObjectAsync<AddBillingAccountDto>(
            $"{RouteConsts.BillingAccount}/{propertyId}/search",
            System.Net.HttpStatusCode.OK
        );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        public void ShouldNotGetAllByFilters()
        {
            // Arrange        
            var billingAccountDto = GetAllBillingAccountConfigurationDtoMock();
            var propertyId = 0;

            // Act
            GetResponseAsObjectAsync<AddBillingAccountDto>(
            $"{RouteConsts.BillingAccount}/{propertyId}/search",
            System.Net.HttpStatusCode.OK
        );

            // Assert
            Assert.False(_notificationHandler.HasNotification());
        }

        private BillingAccountDto GetBillingAccountConfigurationDtoMock()
        {
            return new BillingAccountDto()
            {
                CompanyClientId = new Guid("81B40933-0593-4827-8E86-00C1A0AE4350"),
                ReservationId = 1,
                ReservationItemId = 8,
                GuestReservationItemId = 15,
                BillingAccountTypeId = 2,
                StartDate = new DateTime(2018, 09, 20),
                EndDate = null,
                StatusId = 1,
                IsMainAccount = false,
                MarketSegmentId = 1,
                BusinessSourceId = 1,
                PropertyId = 1,
                GroupKey = new Guid("487AAB2E-A841-48C0-8721-E71C27B93FD9"),
                Blocked = false,
                BillingAccountName = "Conta Teste",
                ReopeningReasonId = null,
                ReopeningDate = null,
                CreateMainAccountOfCompanyInReservationItem = null
            };
        }

        private AddBillingAccountDto GetBillingAccountAddConfigurationDtoMock()
        {
            return new AddBillingAccountDto()
            {
                BillingAccountId = Guid.NewGuid(),
                BillingAccountName = "teste conta",
                CreatePrincipalAccount = true
            };
        }

        private SearchBillingAccountDto GetAllBillingAccountConfigurationDtoMock()
        {
            return new SearchBillingAccountDto()
            {

            };
        }

    }
}
