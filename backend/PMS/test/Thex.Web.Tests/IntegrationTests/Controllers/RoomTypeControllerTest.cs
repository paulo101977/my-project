﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Thex.Web.Controllers;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Tnf.Dto;
using Thex.Dto;
using Thex.InMemoryDatabaseSetup;
using Thex.Domain.Interfaces.Repositories;

namespace Thex.Web.Tests.IntegrationTests.Controllers
{
    public class RoomTypeControllerTest : TnfAspNetCoreIntegratedTestBase<StartupIntegratedPropertyTest>
    {
        public RoomTypeControllerTest()
        {
            var notificationHandler = new NotificationHandler(ServiceProvider);

            SetRequestCulture(CultureInfo.GetCultureInfo("pt-BR"));

            RoomTypeSetupRegister roomTypeSetupRegister = new RoomTypeSetupRegister(ServiceProvider, notificationHandler);
        }

        private string NumberToAlphabetLetter(int number, bool isCaps)
        {
            Char c = (Char)((isCaps ? 65 : 97) + (number - 1));
            return c.ToString();
        }

        //[Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<RoomTypeController>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomTypeAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        //[Fact]
        public async Task Should_GetAll()
        {
            // Act
            var response = await GetResponseAsObjectAsync<ListDto<RoomTypeDto>>(
                RouteConsts.RoomType
            );

            // Assert
            Assert.False(response.HasNext);
            Assert.Equal(2, response.Items.Count);
        }

        //[Fact]
        public async Task Should_Get_RoomType()
        {
            // Act
            var roomType = await GetResponseAsObjectAsync<RoomTypeDto>(
                $"{RouteConsts.RoomType}/1"
            );

            // Assert
            Assert.Equal(1, roomType.Id);
            Assert.Equal("Standard", roomType.Name);
            Assert.Equal("STD", roomType.Abbreviation);
        }

        //Fact]
        public async Task Should_Create_RoomType()
        {
            // Act
            var roomTypeBedTypeList = new List<RoomTypeBedTypeDto>();
            roomTypeBedTypeList.Add(new RoomTypeBedTypeDto { RoomTypeId = 2, BedTypeId = 2 });

            
            var roomType = await PostResponseAsObjectAsync<RoomTypeDto, RoomTypeDto>(
                RouteConsts.RoomType,
                new RoomTypeDto
                {
                    PropertyId = 1,
                    Abbreviation = "LXO",
                    Name = "Luxo",
                    Order = 3,
                    MinimumRate = 100,
                    MaximumRate = 200,
                    AdultCapacity = 2,
                    ChildCapacity = 3,
                    DistributionCode = "DLXO",
                    RoomTypeBedTypeList = roomTypeBedTypeList
                },
                System.Net.HttpStatusCode.OK,
                "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjdkMjIwNjI3LTg1M2QtNDY2Zi04MzVmLWEwNmI2ZDZiMTkyMiIsImp0aSI6ImI0ZjcxMjgzODNlODRlYzU5ZTMyZDZhYjI4ZWYxZDU3Iiwic3ViIjoiN2QyMjA2MjctODUzZC00NjZmLTgzNWYtYTA2YjZkNmIxOTIyIiwiTG9nZ2VyVXNlckVtYWlsIjoidGhleEB0b3R2c2NtbmV0LmNvbS5iciIsIkxvZ2dlZFVzZXJOYW1lIjoidGhleEB0b3R2c2NtbmV0LmNvbS5iciIsIkxvZ2dlZFVzZXJVaWQiOiI3ZDIyMDYyNy04NTNkLTQ2NmYtODM1Zi1hMDZiNmQ2YjE5MjIiLCJQcm9wZXJ0eUlkIjoiMTEiLCJCcmFuZElkIjoiMSIsIkNoYWluSWQiOiIxIiwiVGVuYW50SWQiOiIyM2ViODAzYy03MjZhLTRjN2MtYjI1Yi0yYzIyYTU2NzkzZDkiLCJJc0FkbWluIjoidHJ1ZSIsIlByZWZlcnJlZEN1bHR1cmUiOiJwdC1CUiIsIlByZWZlcnJlZExhbmd1YWdlIjoicHQtQlIiLCJQcm9wZXJ0eUN1bHR1cmUiOiIiLCJQcm9wZXJ0eUxhbmd1YWdlIjoiIiwiVGltZVpvbmVOYW1lIjoiQW1lcmljYS9TYW9fUGF1bG8iLCJuYmYiOjE1MzU0NzQ0NTYsImV4cCI6MTUzODA2NjQ1NiwiaWF0IjoxNTM1NDc0NDU2LCJpc3MiOiJ0aGV4SXNzdWVyIiwiYXVkIjoidGhleEF1ZGllbmNlIn0.fzfo4ng3hagvbYsdY3InoXSm6CoHf8Q_FI8q4gBTQn2CU5kr-j1KZ_t5C25XQ5S2-63uJHoWRdU6j9DdVGeh14Bp5wMvEXGkhZlScqvn5NZ9FHbQ3eYJo4PJSFSQpVqRrMRebQycdRc96onvIaYjCqXUchQwN8ZzpjwDXjUBVZN3RHh5nQoTytawyb_zm7qTlYWEco-r-jmsXm2UukdiLC4s0giObJOHjm2zXGze0YaICkOkg8J_qIg7tqy2e3AajgeXpQJMq_A_J8q7u8j8wO3arWlv8SivO8dNEg37hrYRVULr1comk5CbgBUtnGw4in2xNDGv4xFf4nmeB0i6pw"
            );

            // Assert
            Assert.NotNull(roomType);
        }

        //[Fact]
        public async Task Should_Update_RoomType()
        {
            // Act
            var roomTypeBedTypeList = new List<RoomTypeBedTypeDto>();
            roomTypeBedTypeList.Add(new RoomTypeBedTypeDto { RoomTypeId = 2, BedTypeId = 2 });

            var roomType = await PutResponseAsObjectAsync<RoomTypeDto, RoomTypeDto>(
                $"{RouteConsts.RoomType}/1",
                new RoomTypeDto
                {
                    Id = 1,
                    PropertyId = 1,
                    Abbreviation = "PRE",
                    Name = "Presidencial",
                    Order = 1,
                    MinimumRate = 200,
                    MaximumRate = 400,
                    AdultCapacity = 4,
                    ChildCapacity = 5,
                    DistributionCode = "DPRE",
                    RoomTypeBedTypeList = roomTypeBedTypeList
                }
            );

            // Assert
            Assert.Equal(1, roomType.Id);
            Assert.Equal("PRE", roomType.Abbreviation);
            Assert.Equal("Presidencial", roomType.Name);
        }

        //[Fact]
        public Task Should_Delete_RoomType()
        {
            // Act
            return DeleteResponseAsync(
                $"{RouteConsts.RoomType}/2"
            );
        }

    }
}
