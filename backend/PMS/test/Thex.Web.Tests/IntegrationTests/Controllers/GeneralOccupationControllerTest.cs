﻿using Shouldly;
using System.Globalization;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Tnf.Dto;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.Kernel;
using Thex.Application.Interfaces;
using Thex.InMemoryDatabaseSetup;

namespace Thex.Web.Tests.IntegrationTests.Controllers
{
    public class GeneralOccupationControllerTest : TnfAspNetCoreIntegratedTestBase<StartupIntegratedPropertyTest>
    {
        public GeneralOccupationControllerTest()
        {
            var notificationHandler = new NotificationHandler(ServiceProvider);

            SetRequestCulture(CultureInfo.GetCultureInfo("pt-BR"));

            var generalOccupationSetupRegister = new GeneralOccupationSetupRegister(ServiceProvider, notificationHandler);
        }

        //[Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<GeneralOccupationController>().ShouldNotBeNull();
            ServiceProvider.GetService<IGeneralOccupationAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        //[Fact]
        public async Task Should_GetAll()
        {
            // Act
            var response = await GetResponseAsObjectAsync<ListDto<GetAllGeneralOccupationDto>>(
                RouteConsts.GeneralOccupation
            );

            // Assert
            Assert.False(response.HasNext);
            Assert.Equal(1, response.Items.Count);
        }
    }
}
