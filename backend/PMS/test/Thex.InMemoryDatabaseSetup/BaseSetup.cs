﻿using System;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.Notifications;

namespace Thex.InMemoryDatabaseSetup
{
    public class BaseSetup
    {
        public BaseSetup(IServiceProvider serviceProvider,
            INotificationHandler notificationHandler)
        {
            var personId = Guid.NewGuid();
            var tenantId = Guid.Parse("9bece27c-cf67-4ea3-8ae8-357b3080475d");
            var chainId = 1;
            var brandId = 1;
            var companyId = 1;
            var propertyId = 1;
            var roomTypeId = 1;
            var currencyId = new Guid("67838acb-9525-4aeb-b0a6-127c1b986c48");
            var reservation1Id = 1;
            var reservation2Id = 2;
            var reservation3Id = 3;
            var marketSegmentId = 2;
            var businessSourceId = 1;
            var roomLayoutId = new Guid("00b59f7a-dcb1-4ccd-b1cd-3b35cf6d9125");
            var houseKeepingStatusPropertyId = Guid.NewGuid();
            var roomId = 1;
            var guestId1 = 1;
            var guestId2 = 2;
            var propertyGuestTypeNormalId = 1;
            var propertyGuestTypeVipId = 2;
            var guestRegistrationYoseph = Guid.NewGuid();
            var guestRegistrationYosephJR = Guid.NewGuid();
            var guestRegistrationSaulo = Guid.NewGuid();
            var guestRegistrationMaria = Guid.NewGuid();


            serviceProvider.UsingDbContext<ThexContext>(context =>
            {
                // GENERAL OCCUPATIONS
                context.GeneralOccupations.Add(GeneralOccupation.Create(notificationHandler)
                    .WithId(1)
                    .WithOccupation("Contador")
                    .Build());

                // STATUS
                context.StatusCategories.Add(StatusCategory.Create(notificationHandler)
                    .WithId(1)
                    .WithName("Reservation")
                    .Build());
                context.StatusCategories.Add(StatusCategory.Create(notificationHandler)
                    .WithId(2)
                    .WithName("BILLING INVOICE")
                    .Build());
                context.StatusCategories.Add(StatusCategory.Create(notificationHandler)
                    .WithId(3)
                    .WithName("PROPERTY")
                    .Build());

                for (var i = 0; i <= 15; i++)
                    context.Statuss.Add(Status.Create(notificationHandler)
                        .WithId(i)
                        .WithName($"{NumberToAlphabetLetter(i, true)}")
                        .WithStatusCategoryId(i <= 8 ? 1 : i <= 12 ? 2 : 3)
                        .Build());

                // TENANT
                context.Tenants.Add(Tenant.Create(notificationHandler)
                    .WithId(tenantId)
                    .WithTenantName("Tenant Name")
                    .Build());

                // CHAIN
                var chain = new Chain { TenantId = tenantId, Id = chainId, IsTemporary = false, Name = "Chain 1" };
                context.Chains.Add(chain);

                // BRAND
                var brand = new Brand { TenantId = tenantId, Id = brandId, ChainId = chainId, Name = "Brand 1" };
                context.Brands.Add(brand);

                // PERSON
                context.Persons.Add(Person.Create(notificationHandler)
                    .WithId(personId)
                    .WithPersonType(PersonTypeEnum.Legal)
                    .WithFirstName("First Name")
                    .WithLastName("Last Name")
                    .WithFirstName("Full Name")
                    .Build());

                // COMPANY
                context.Companies.Add(Company.Create(notificationHandler)
                    .WithId(companyId)
                    .WithPersonId(personId)
                    .WithShortName("Short Name")
                    .WithTradeName("Trade Name")
                    .Build());

                // PROPERTY
                context.PropertyTypes.Add(PropertyType.Create(notificationHandler)
                    .WithId(1)
                    .WithName("Hotel")
                    .Build());

                context.Properties.Add(Property.Create(notificationHandler)
                    .WithId(propertyId)
                    .WithCompanyId(companyId)
                    .WithBrandId(brandId)
                    .WithPropertyTypeId((PropertyTypeEnum)1)
                    .WithTenantId(tenantId)
                    .WithName("Property Name")
                    .WithPropertyUId(Guid.NewGuid())
                    .WithPropertyStatusId(13)
                    .Build()); ;

                // PARÂMETROS - MÓDULO
                context.ApplicationModules.Add(ApplicationModule.Create(notificationHandler)
                    .WithId(1)
                    .WithApplicationModuleName("Hospitality")
                    .WithApplicationModuleDescription("Hospitality")
                    .Build());

                // PARÂMETROS - GRUPOS
                context.FeatureGroups.Add(FeatureGroup.Create(notificationHandler)
                    .WithId(1)
                    .WithFeatureGroupName("Parâmetros Gerais")
                    .WithDescription("Parâmetros Gerais")
                    .Build());

                context.FeatureGroups.Add(FeatureGroup.Create(notificationHandler)
                    .WithId(2)
                    .WithFeatureGroupName("Tipos de Serviço")
                    .WithDescription("Tipos de Serviço")
                    .Build());

                context.FeatureGroups.Add(FeatureGroup.Create(notificationHandler)
                    .WithId(3)
                    .WithFeatureGroupName("Faixa Etária de Criança")
                    .WithDescription("Faixa Etária de Criança")
                    .Build());

                // PARÂMETROS - TIPOS
                context.ParameterTypes.Add(ParameterType.Create(notificationHandler)
                    .WithId(1)
                    .WithParameterTypeName("Checkbox")
                    .Build());
                context.ParameterTypes.Add(ParameterType.Create(notificationHandler)
                    .WithId(2)
                    .WithParameterTypeName("ComboBox")
                    .Build());
                context.ParameterTypes.Add(ParameterType.Create(notificationHandler)
                    .WithId(3)
                    .WithParameterTypeName("Number")
                    .Build());
                context.ParameterTypes.Add(ParameterType.Create(notificationHandler)
                    .WithId(4)
                    .WithParameterTypeName("Date")
                    .Build());
                context.ParameterTypes.Add(ParameterType.Create(notificationHandler)
                    .WithId(5)
                    .WithParameterTypeName("Default")
                    .Build());
                context.ParameterTypes.Add(ParameterType.Create(notificationHandler)
                    .WithId(6)
                    .WithParameterTypeName("Time")
                    .Build());

                // PARÂMETROS - APLICAÇÃO
                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(1)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(1)
                    .WithParameterName("Horário de Check-in")
                    .WithParameterDescription("Horário de Check-in")
                    .WithParameterDefaultValue("14:00")
                    .WithParameterTypeId(6)
                    .WithParameterComponentType("Configuração do Hotel")
                    .WithParameterMinValue("00:00")
                    .WithParameterMaxValue("23:59")
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(2)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(1)
                    .WithParameterName("Horário de Check-out")
                    .WithParameterDescription("Horário de Check-out")
                    .WithParameterDefaultValue("12:00")
                    .WithParameterTypeId(6)
                    .WithParameterComponentType("Configuração do Hotel")
                    .WithParameterMinValue("00:00")
                    .WithParameterMaxValue("23:59")
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(3)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(1)
                    .WithParameterName("Lançamento de Diárias")
                    .WithParameterDescription("Lançamento de Diárias")
                    .WithParameterDefaultValue(null)
                    .WithParameterTypeId(2)
                    .WithParameterComponentType("Configuração do Hotel")
                    .WithParameterMinValue(null)
                    .WithParameterMaxValue("Check-in")
                    .WithParameterPossibleValues("[{'value':'Check-in','title':'Check-in'},{'value':'Auditoria','title':'Auditoria'}]")
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(4)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(1)
                    .WithParameterName("Moeda Padrão")
                    .WithParameterDescription("Moeda Padrão")
                    .WithParameterDefaultValue(null)
                    .WithParameterTypeId(2)
                    .WithParameterComponentType("Configuração do Hotel")
                    .WithParameterMinValue(null)
                    .WithParameterMaxValue(null)
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(5)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(2)
                    .WithParameterName("Diária")
                    .WithParameterDescription("Diária")
                    .WithParameterDefaultValue(null)
                    .WithParameterTypeId(2)
                    .WithParameterComponentType("Tipos de Serviço")
                    .WithParameterMinValue(null)
                    .WithParameterMaxValue(null)
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(7)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(3)
                    .WithParameterName("Criança 1")
                    .WithParameterDescription("Criança 1")
                    .WithParameterDefaultValue("6")
                    .WithParameterTypeId(3)
                    .WithParameterComponentType("Faixa Etária de Criança")
                    .WithParameterMinValue("0")
                    .WithParameterMaxValue("9")
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(8)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(3)
                    .WithParameterName("Criança 2")
                    .WithParameterDescription("Criança 2")
                    .WithParameterDefaultValue("10")
                    .WithParameterTypeId(3)
                    .WithParameterComponentType("Faixa Etária de Criança")
                    .WithParameterMinValue("7")
                    .WithParameterMaxValue("11")
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(9)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(3)
                    .WithParameterName("Criança 3")
                    .WithParameterDescription("Criança 3")
                    .WithParameterDefaultValue("12")
                    .WithParameterTypeId(3)
                    .WithParameterComponentType("Faixa Etária de Criança")
                    .WithParameterMinValue("11")
                    .WithParameterMaxValue("17")
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(10)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(1)
                    .WithParameterName("Data do Sistema")
                    .WithParameterDescription("Data do Sistema")
                    .WithParameterDefaultValue(null)
                    .WithParameterTypeId(4)
                    .WithParameterComponentType("Data do Sistema")
                    .WithParameterMinValue(null)
                    .WithParameterMaxValue(null)
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(11)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(1)
                    .WithParameterName("Passo da Auditoria")
                    .WithParameterDescription("Passo da Auditoria")
                    .WithParameterDefaultValue(null)
                    .WithParameterTypeId(3)
                    .WithParameterComponentType("Passo da Auditoria")
                    .WithParameterMinValue(null)
                    .WithParameterMaxValue(null)
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(12)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(1)
                    .WithParameterName("TimeZone")
                    .WithParameterDescription("TimeZone")
                    .WithParameterDefaultValue(null)
                    .WithParameterTypeId(5)
                    .WithParameterComponentType("TimeZone")
                    .WithParameterMinValue(null)
                    .WithParameterMaxValue(null)
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                context.ApplicationParameters.Add(ApplicationParameter.Create(notificationHandler)
                    .WithId(13)
                    .WithApplicationModuleId(1)
                    .WithFeatureGroupId(1)
                    .WithParameterName("Número de dias para troca de status da UH")
                    .WithParameterDescription("Número de dias para troca de status da UH")
                    .WithParameterDefaultValue(null)
                    .WithParameterTypeId(3)
                    .WithParameterComponentType("Configuração do Hotel")
                    .WithParameterMinValue(null)
                    .WithParameterMaxValue(null)
                    .WithParameterPossibleValues(null)
                    .WithIsNullable(false)
                    .Build());

                // PARÂMETROS - PROPERTY
                var propertyParameter1 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 13,
                    PropertyParameterValue = "3",
                    PropertyParameterMinValue = null,
                    PropertyParameterMaxValue = null,
                    PropertyParameterPossibleValues = null,
                    IsActive = true
                };

                var propertyParameter2 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 10,
                    PropertyParameterValue = "22/05/2018",
                    PropertyParameterMinValue = null,
                    PropertyParameterMaxValue = null,
                    PropertyParameterPossibleValues = null,
                    IsActive = true
                };

                var propertyParameter3 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 11,
                    PropertyParameterValue = "0",
                    PropertyParameterMinValue = null,
                    PropertyParameterMaxValue = null,
                    PropertyParameterPossibleValues = null,
                    IsActive = true
                };

                var propertyParameter4 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 5,
                    PropertyParameterValue = "6",
                    PropertyParameterMinValue = null,
                    PropertyParameterMaxValue = null,
                    PropertyParameterPossibleValues = "[{'value':'6','title':'Totvs serviço'},{'value':'10','title':'Passar Roupa'}]",
                    IsActive = true
                };

                var propertyParameter5 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 1,
                    PropertyParameterValue = "14:00",
                    PropertyParameterMinValue = "00:00",
                    PropertyParameterMaxValue = "23:59",
                    PropertyParameterPossibleValues = null,
                    IsActive = true
                };

                var propertyParameter6 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 4,
                    PropertyParameterValue = "167838acb-9525-4aeb-b0a6-127c1b986c48",
                    PropertyParameterMinValue = null,
                    PropertyParameterMaxValue = null,
                    PropertyParameterPossibleValues = "[{'value':'67838acb-9525-4aeb-b0a6-127c1b986c48','title':'BRL'}]",
                    IsActive = true
                };

                var propertyParameter7 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 8,
                    PropertyParameterValue = "10",
                    PropertyParameterMinValue = "7",
                    PropertyParameterMaxValue = "11",
                    PropertyParameterPossibleValues = null,
                    IsActive = true
                };

                var propertyParameter8 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 9,
                    PropertyParameterValue = "12",
                    PropertyParameterMinValue = "11",
                    PropertyParameterMaxValue = "17",
                    PropertyParameterPossibleValues = null,
                    IsActive = true
                };

                var propertyParameter9 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 7,
                    PropertyParameterValue = "6",
                    PropertyParameterMinValue = "0",
                    PropertyParameterMaxValue = "9",
                    PropertyParameterPossibleValues = null,
                    IsActive = true
                };

                var propertyParameter10 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 2,
                    PropertyParameterValue = "12:00",
                    PropertyParameterMinValue = "00:00",
                    PropertyParameterMaxValue = "23:59",
                    PropertyParameterPossibleValues = null,
                    IsActive = true
                };

                var propertyParameter11 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 3,
                    PropertyParameterValue = "Check-in",
                    PropertyParameterMinValue = null,
                    PropertyParameterMaxValue = "Check-in",
                    PropertyParameterPossibleValues = "[{'value':'Check -in','title':'Check -in'},{'value':'Auditoria','title':'Auditoria'}]",
                    IsActive = true
                };

                var propertyParameter12 = new PropertyParameter
                {
                    TenantId = tenantId,
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    ApplicationParameterId = 12,
                    PropertyParameterValue = "E. South America Standard Time",
                    PropertyParameterMinValue = null,
                    PropertyParameterMaxValue = null,
                    PropertyParameterPossibleValues = null,
                    IsActive = true
                };

                context.PropertyParameters.Add(propertyParameter1);
                context.PropertyParameters.Add(propertyParameter2);
                context.PropertyParameters.Add(propertyParameter3);
                context.PropertyParameters.Add(propertyParameter4);
                context.PropertyParameters.Add(propertyParameter5);
                context.PropertyParameters.Add(propertyParameter6);
                context.PropertyParameters.Add(propertyParameter7);
                context.PropertyParameters.Add(propertyParameter8);
                context.PropertyParameters.Add(propertyParameter9);
                context.PropertyParameters.Add(propertyParameter10);
                context.PropertyParameters.Add(propertyParameter11);
                context.PropertyParameters.Add(propertyParameter12);

                // ROOM TYPE
                context.RoomTypes.Add(new RoomType
                {
                    Id = roomTypeId,
                    PropertyId = propertyId,
                    Order = 1,
                    Name = "Standard",
                    Abbreviation = "STD",
                    AdultCapacity = 2,
                    ChildCapacity = 3,
                    IsActive = true,
                    FreeChildQuantity1 = 1,
                    FreeChildQuantity2 = 2,
                    FreeChildQuantity3 = 3,
                    DistributionCode = "CDSTD",
                    MaximumRate = 300,
                    MinimumRate = 150

                });

                context.RoomTypes.Add(new RoomType
                {
                    Id = 2,
                    PropertyId = propertyId,
                    Order = 2,
                    Name = "Será Excluído",
                    Abbreviation = "SE",
                    AdultCapacity = 2,
                    ChildCapacity = 3,
                    IsActive = true,
                    FreeChildQuantity1 = 1,
                    FreeChildQuantity2 = 1,
                    FreeChildQuantity3 = 1,
                    DistributionCode = "CSE",
                    MaximumRate = 300,
                    MinimumRate = 150

                });

                // BED TYPE
                context.BedTypes.Add(BedType.Create(notificationHandler)
                    .WithId(1)
                    .WithName("Single")
                    .WithCapactity(1)
                    .Build());
                context.BedTypes.Add(BedType.Create(notificationHandler)
                    .WithId(2)
                    .WithName("Double")
                    .WithCapactity(2)
                    .Build());
                context.BedTypes.Add(BedType.Create(notificationHandler)
                    .WithId(3)
                    .WithName("Reversible")
                    .WithCapactity(2)
                    .Build());
                context.BedTypes.Add(BedType.Create(notificationHandler)
                    .WithId(4)
                    .WithName("Extra")
                    .WithCapactity(1)
                    .Build());
                context.BedTypes.Add(BedType.Create(notificationHandler)
                    .WithId(5)
                    .WithName("Crib")
                    .WithCapactity(0)
                    .Build());

                // MealPlanType
                context.MealPlanTypes.Add(new MealPlanType()
                {
                    Id = 1,
                    MealPlanTypeCode = "SAP",
                    MealPlanTypeName = "None"
                });

                context.MealPlanTypes.Add(new MealPlanType()
                {
                    Id = 2,
                    MealPlanTypeCode = "CM",
                    MealPlanTypeName = "Breakfast"
                });

                // Currency
                context.Currencies.Add(new Currency()
                {
                    Id = currencyId,
                    CurrencyName = "BRL",
                    AlphabeticCode = "BRL",
                    NumnericCode = "2",
                    MinorUnit = 2,
                    Symbol = "R$",
                    ExchangeRate = 40.0000m,
                    CreationTime = DateTime.Now,
                    CreatorUserId = personId
                });

                // MarketSegment
                context.MarketSegments.Add(new MarketSegment()
                {
                    Id = 1,
                    Code = "01",
                    Name = "NOVA ORIGEM",
                    SegmentAbbreviation = "NEWSEG",
                    CreationTime = DateTime.Now,
                    IsActive = true,
                    IsDeleted = false
                });

                context.MarketSegments.Add(new MarketSegment()
                {
                    Id = marketSegmentId,
                    Code = "01.11",
                    Name = "Nova origem secundária",
                    SegmentAbbreviation = "NOS",
                    IsActive = true,
                    ParentMarketSegmentId = 1,
                    IsDeleted = false
                });

                // BusinessSource
                context.BusinessSources.Add(new BusinessSource()
                {
                    Id = businessSourceId,
                    Code = "33",
                    Description = "Nova Origem",
                    CreationTime = DateTime.Now,
                    IsActive = true,
                    IsDeleted = false
                });

                //  Reservation
                context.Reservations.Add(new Reservation()
                {
                    Id = reservation1Id,
                    ReservationUid = Guid.NewGuid(),
                    ReservationCode = "BR-20170914-XSDSDSDFRZX1",
                    ReservationVendorCode = "00001",
                    PropertyId = propertyId,
                    ContactName = "Ricardo",
                    ContactEmail = "ricardo.mendes@totvscmnet.com.br",
                    ContactPhone = "11111111",
                    InternalComments = "",
                    ExternalComments = "",
                    UnifyAccounts = true,
                    CreationTime = DateTime.Now,
                    BusinessSourceId = businessSourceId,
                    MarketSegmentId = marketSegmentId,
                    IsDeleted = false,
                    TenantId = tenantId,

                });

                context.Reservations.Add(new Reservation()
                {
                    Id = reservation2Id,
                    ReservationUid = Guid.NewGuid(),
                    ReservationCode = "BR-20171014-XSDSDSDFRZX1",
                    ReservationVendorCode = "00002",
                    PropertyId = propertyId,
                    ContactName = "Yoseph",
                    ContactEmail = "yoseph.santos@totvscmnet.com.br",
                    ContactPhone = "22222222",
                    InternalComments = "",
                    ExternalComments = "",
                    UnifyAccounts = false,
                    CreationTime = DateTime.Now,
                    BusinessSourceId = businessSourceId,
                    MarketSegmentId = marketSegmentId,
                    IsDeleted = false,
                    TenantId = tenantId,
                });

                context.Reservations.Add(new Reservation()
                {
                    Id = reservation3Id,
                    ReservationUid = Guid.NewGuid(),
                    ReservationCode = "BR-20171114-XSDSDSDFRZX1",
                    ReservationVendorCode = "00003",
                    PropertyId = propertyId,
                    ContactName = "Saulo",
                    ContactEmail = "saulo.brito@totvscmnet.com.br",
                    ContactPhone = "33333333",
                    InternalComments = "",
                    ExternalComments = "",
                    UnifyAccounts = true,
                    CreationTime = DateTime.Now,
                    BusinessSourceId = businessSourceId,
                    MarketSegmentId = marketSegmentId,
                    IsDeleted = false,
                    TenantId = tenantId,
                });

                // RoomLayout
                context.RoomLayouts.Add(new RoomLayout()
                {
                    Id = roomLayoutId,
                    QuantityDouble = 4,
                    QuantitySingle = 6,
                });

                // HouseKeepingStatus
                context.HousekeepingStatuss.Add(new HousekeepingStatus()
                {
                    Id = 1,
                    StatusName = "Clean",
                    DefaultColor = "1DC29D"
                });

                context.HousekeepingStatuss.Add(new HousekeepingStatus()
                {
                    Id = 2,
                    StatusName = "Dirty",
                    DefaultColor = "FF6969"
                });

                context.HousekeepingStatuss.Add(new HousekeepingStatus()
                {
                    Id = 3,
                    StatusName = "Maintenance",
                    DefaultColor = "A497E3"
                });

                context.HousekeepingStatuss.Add(new HousekeepingStatus()
                {
                    Id = 4,
                    StatusName = "Inspection",
                    DefaultColor = "F8E71C",
                });

                context.HousekeepingStatuss.Add(new HousekeepingStatus()
                {
                    Id = 5,
                    StatusName = "Stowage",
                    DefaultColor = "00C5FF"
                });


                // HouseKeepingStatusProperty
                context.HousekeepingStatusProperties.Add(new HousekeepingStatusProperty()
                {
                    Id = houseKeepingStatusPropertyId,
                    PropertyId = propertyId,
                    HousekeepingStatusId = 1,
                    StatusName = "Limpo",
                    Color = "1DC29D",
                    TenantId = tenantId,
                    IsActive = true
                });

                context.HousekeepingStatusProperties.Add(new HousekeepingStatusProperty()
                {
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    HousekeepingStatusId = 2,
                    StatusName = "Sujo",
                    Color = "FF6969",
                    TenantId = tenantId,
                    IsActive = true
                });

                context.HousekeepingStatusProperties.Add(new HousekeepingStatusProperty()
                {
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    HousekeepingStatusId = 3,
                    StatusName = "Manutenção",
                    Color = "A497E3",
                    TenantId = tenantId,
                    IsActive = true
                });

                context.HousekeepingStatusProperties.Add(new HousekeepingStatusProperty()
                {
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    HousekeepingStatusId = 4,
                    StatusName = "Inspeção",
                    Color = "F8E71C",
                    TenantId = tenantId,
                    IsActive = true
                });

                context.HousekeepingStatusProperties.Add(new HousekeepingStatusProperty()
                {
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    HousekeepingStatusId = 5,
                    StatusName = "Arrumação",
                    Color = "00C5FF",
                    TenantId = tenantId,
                    IsActive = true
                });

                context.HousekeepingStatusProperties.Add(new HousekeepingStatusProperty()
                {
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    HousekeepingStatusId = 5,
                    StatusName = "Stand By",
                    Color = "F5A623",
                    TenantId = tenantId,
                    IsActive = true
                });

                // Room
                context.Rooms.Add(new Room()
                {
                    Id = roomId,
                    PropertyId = propertyId,
                    RoomTypeId = roomTypeId,
                    Building = "A",
                    Wing = "South",
                    Floor = "1",
                    RoomNumber = "101",
                    Remarks = "N",
                    IsActive = true,
                    HousekeepingStatusPropertyId = houseKeepingStatusPropertyId,
                    TenantId = tenantId,
                    HousekeepingStatusLastModificationTime = DateTime.Now
                });

                // ReservationItem
                context.ReservationItems.Add(new ReservationItem()
                {
                    Id = 1,
                    ReservationItemUid = Guid.NewGuid(),
                    ReservationId = reservation1Id,
                    EstimatedArrivalDate = new DateTime(2018, 05, 23),
                    EstimatedDepartureDate = new DateTime(2018, 05, 26),
                    ReservationItemCode = "0001",
                    RequestedRoomTypeId = 1,
                    ReceivedRoomTypeId = 1,
                    RoomId = roomId,
                    AdultCount = 1,
                    ChildCount = 0,
                    ReservationItemStatusId = (int)ReservationStatus.Confirmed,
                    RoomLayoutId = roomLayoutId,
                    ExtraBedCount = 0,
                    ExtraCribCount = 0,
                    IsDeleted = false,
                    CreationTime = DateTime.Now,
                    TenantId = tenantId
                });

                context.ReservationItems.Add(new ReservationItem()
                {
                    Id = 2,
                    ReservationItemUid = Guid.NewGuid(),
                    ReservationId = reservation2Id,
                    EstimatedArrivalDate = new DateTime(2018, 05, 22),
                    EstimatedDepartureDate = new DateTime(2018, 05, 23),
                    ReservationItemCode = "0002",
                    RequestedRoomTypeId = 1,
                    ReceivedRoomTypeId = 2,
                    RoomId = roomId,
                    AdultCount = 1,
                    ChildCount = 1,
                    ReservationItemStatusId = (int)ReservationStatus.Checkin,
                    RoomLayoutId = roomLayoutId,
                    ExtraBedCount = 0,
                    ExtraCribCount = 0,
                    IsDeleted = false,
                    CreationTime = DateTime.Now,
                });

                context.ReservationItems.Add(new ReservationItem()
                {
                    Id = 3,
                    ReservationItemUid = Guid.NewGuid(),
                    ReservationId = reservation1Id,
                    EstimatedArrivalDate = new DateTime(2018, 05, 16),
                    EstimatedDepartureDate = new DateTime(2018, 05, 21),
                    ReservationItemCode = "0004",
                    RequestedRoomTypeId = 2,
                    ReceivedRoomTypeId = 2,
                    RoomId = roomId,
                    AdultCount = 2,
                    ChildCount = 0,
                    ReservationItemStatusId = (int)ReservationStatus.Checkout,
                    RoomLayoutId = roomLayoutId,
                    ExtraBedCount = 0,
                    ExtraCribCount = 0,
                    IsDeleted = false,
                    CreationTime = DateTime.Now,
                });

                // Guest
                context.Guests.Add(new Guest()
                {
                    Id = guestId1,
                    PersonId = personId,
                });

                context.Guests.Add(new Guest()
                {
                    Id = guestId2,
                    PersonId = personId
                });

                // PropertyGuestType
                context.PropertyGuestTypes.Add(new PropertyGuestType()
                {
                    Id = propertyGuestTypeNormalId,
                    PropertyId = propertyId,
                    GuestTypeName = "Normal",
                    IsVip = false,
                    IsActive = true,
                    IsIncognito = false,
                    Code = "N"
                });

                context.PropertyGuestTypes.Add(new PropertyGuestType()
                {
                    Id = propertyGuestTypeVipId,
                    PropertyId = propertyId,
                    GuestTypeName = "VIP",
                    IsVip = true,
                    IsActive = true,
                    IsIncognito = false,
                    Code = "V"
                });

                //DocumentTypes
                context.DocumentTypes.Add(new DocumentType()
                {
                    Id = 1,
                    PersonType = "N",
                    CountrySubdivisionId = 1,
                    Name = "CPF",
                    IsMandatory = true
                });

                context.DocumentTypes.Add(new DocumentType()
                {
                    Id = 2,
                    PersonType = "L",
                    CountrySubdivisionId = 1,
                    Name = "CNPJ",
                    IsMandatory = true
                });

                //CountrySubdivisions                
                context.CountrySubdivisions.Add(new CountrySubdivision()
                {
                    Id = 1,
                    TwoLetterIsoCode = "BR",
                    ThreeLetterIsoCode = "BRA",
                    CountryTwoLetterIsoCode = "BR",
                    CountryThreeLetterIsoCode = null,
                    ExternalCode = null,
                    SubdivisionTypeId = 1
                });

                context.CountrySubdivisions.Add(new CountrySubdivision()
                {
                    Id = 2,
                    TwoLetterIsoCode = "AC",
                    ThreeLetterIsoCode = null,
                    CountryTwoLetterIsoCode = "BR",
                    CountryThreeLetterIsoCode = null,
                    ExternalCode = "12",
                    ParentSubdivisionId = 1,
                    SubdivisionTypeId = 2
                });

                context.CountrySubdivisions.Add(new CountrySubdivision()
                {
                    Id = 3,
                    TwoLetterIsoCode = "MG",
                    ThreeLetterIsoCode = null,
                    CountryTwoLetterIsoCode = "BR",
                    CountryThreeLetterIsoCode = null,
                    ExternalCode = "31",
                    ParentSubdivisionId = 1,
                    SubdivisionTypeId = 2
                });

                // GuestRegistration
                context.GuestRegistrations.Add(new GuestRegistration()
                {
                    Id = guestRegistrationYoseph,
                    FirstName = "Yoseph",
                    LastName = "Santos",
                    FullName = "Yoseph Santos",
                    Email = "yoseph.santos@totvscmnet.com.br",
                    BirthDate = new DateTime(1992, 11, 29),
                    Gender = "M",
                    GuestTypeId = propertyGuestTypeNormalId,
                    PropertyId = propertyId,
                    TenantId = tenantId,
                    GuestId = guestId1,
                    AdditionalInformation = "",
                    DocumentTypeId = 1,
                    DocumentInformation = "315.890.060-20"
                });

                context.GuestRegistrations.Add(new GuestRegistration()
                {
                    Id = guestRegistrationYosephJR,
                    FirstName = "Yoseph",
                    LastName = "Santos Jr",
                    FullName = "Yoseph Santos Jr",
                    Email = "yoseph.santosjr@totvscmnet.com.br",
                    BirthDate = new DateTime(2010, 11, 29),
                    Gender = "M",
                    GuestTypeId = propertyGuestTypeNormalId,
                    PropertyId = propertyId,
                    TenantId = tenantId,
                    GuestId = guestId2,
                    IsLegallyIncompetent = true
                });

                context.GuestRegistrations.Add(new GuestRegistration()
                {
                    Id = guestRegistrationSaulo,
                    FirstName = "Saulo",
                    LastName = "Brito",
                    FullName = "Saulo Brito",
                    Email = "saulo.brito@totvscmnet.com.br",
                    BirthDate = new DateTime(1992, 11, 29),
                    Gender = "M",
                    GuestTypeId = propertyGuestTypeVipId,
                    PropertyId = propertyId,
                    TenantId = tenantId,
                    GuestId = guestId1,
                    DocumentTypeId = 1,
                    DocumentInformation = "702.134.030-30"
                });

                context.GuestRegistrations.Add(new GuestRegistration()
                {
                    Id = guestRegistrationMaria,
                    FirstName = "Maria",
                    LastName = "Brito",
                    FullName = "Maria Brito",
                    Email = "maria.brito@totvscmnet.com.br",
                    BirthDate = new DateTime(1992, 11, 29),
                    Gender = "F",
                    GuestTypeId = propertyGuestTypeVipId,
                    PropertyId = propertyId,
                    TenantId = tenantId,
                    GuestId = guestId1,
                    DocumentTypeId = 1,
                    DocumentInformation = "461.617.440-74"
                });

                // GuestReservation
                context.GuestReservationItems.Add(new GuestReservationItem()
                {
                    Id = 1,
                    ReservationItemId = 2,
                    GuestId = 1,
                    GuestName = "Yoseph Santos",
                    GuestEmail = "yoseph.santos@totvscmnet.com.br",
                    GuestDocument = "315.890.060-20",
                    EstimatedArrivalDate = new DateTime(2018, 05, 22),
                    CheckInDate = new DateTime(2018, 05, 22),
                    CheckInHour = new DateTime(2018, 05, 22, 14, 30, 00),
                    EstimatedDepartureDate = new DateTime(2018, 05, 23),
                    GuestTypeId = propertyGuestTypeNormalId,
                    GuestStatusId = (int)ReservationStatus.Checkin,
                    PreferredName = "Yoseph",
                    GuestCitizenship = "Brasileira",
                    GuestLanguage = "Português",
                    PctDailyRate = 100,
                    IsIncognito = false,
                    IsChild = false,
                    ChildAge = 0,
                    IsMain = true,
                    GuestRegistrationId = guestRegistrationYoseph,
                    PropertyId = propertyId
                });

                context.GuestReservationItems.Add(new GuestReservationItem()
                {
                    Id = 2,
                    ReservationItemId = 2,
                    GuestId = 2,
                    GuestName = "Yoseph Santos Jr",
                    GuestEmail = "yoseph.santosjr@totvscmnet.com.br",
                    EstimatedArrivalDate = new DateTime(2018, 05, 22),
                    CheckInDate = new DateTime(2018, 05, 22),
                    CheckInHour = new DateTime(2018, 05, 22, 14, 30, 00),
                    EstimatedDepartureDate = new DateTime(2018, 05, 23),
                    GuestTypeId = propertyGuestTypeNormalId,
                    GuestStatusId = (int)ReservationStatus.Checkin,
                    PreferredName = "Juninho",
                    GuestCitizenship = "Brasileira",
                    GuestLanguage = "Português",
                    PctDailyRate = 100,
                    IsIncognito = false,
                    IsChild = true,
                    ChildAge = 7,
                    IsMain = false,
                    GuestRegistrationId = guestRegistrationYosephJR,
                    PropertyId = propertyId
                });

                context.GuestReservationItems.Add(new GuestReservationItem()
                {
                    Id = 3,
                    ReservationItemId = 3,
                    GuestId = 1,
                    GuestName = "Saulo Brito",
                    GuestEmail = "saulo.brito@totvscmnet.com.br",
                    GuestDocument = "702.134.030-30",
                    EstimatedArrivalDate = new DateTime(2018, 05, 19),
                    CheckInDate = new DateTime(2018, 05, 19),
                    CheckInHour = new DateTime(2018, 05, 19, 18, 48, 23),
                    EstimatedDepartureDate = new DateTime(2018, 05, 21),
                    CheckOutDate = new DateTime(2018, 05, 21),
                    CheckOutHour = new DateTime(2018, 05, 21, 09, 12, 35),
                    GuestTypeId = propertyGuestTypeVipId,
                    GuestStatusId = (int)ReservationStatus.Checkout,
                    PreferredName = "Saulo",
                    GuestCitizenship = "Brasileira",
                    GuestLanguage = "Português",
                    PctDailyRate = 100,
                    IsIncognito = false,
                    IsChild = false,
                    ChildAge = 0,
                    IsMain = true,
                    GuestRegistrationId = guestRegistrationSaulo,
                    PropertyId = propertyId
                });

                context.GuestReservationItems.Add(new GuestReservationItem()
                {
                    Id = 4,
                    ReservationItemId = 3,
                    GuestId = 1,
                    GuestName = "Maria Brito",
                    GuestEmail = "maria.brito@totvscmnet.com.br",
                    GuestDocument = "461.617.440-74",
                    EstimatedArrivalDate = new DateTime(2018, 05, 19),
                    CheckInDate = new DateTime(2018, 05, 19),
                    CheckInHour = new DateTime(2018, 05, 19, 18, 48, 23),
                    EstimatedDepartureDate = new DateTime(2018, 05, 21),
                    CheckOutDate = new DateTime(2018, 05, 21),
                    CheckOutHour = new DateTime(2018, 05, 21, 09, 12, 35),
                    GuestTypeId = propertyGuestTypeVipId,
                    GuestStatusId = (int)ReservationStatus.Checkout,
                    PreferredName = "Saulo",
                    GuestCitizenship = "Brasileira",
                    GuestLanguage = "Português",
                    PctDailyRate = 100,
                    IsIncognito = false,
                    IsChild = false,
                    ChildAge = 0,
                    IsMain = false,
                    GuestRegistrationId = guestRegistrationMaria,
                    PropertyId = propertyId
                });



                context.SaveChanges();
            });
        }

        private string NumberToAlphabetLetter(int number, bool isCaps)
        {
            Char c = (Char)((isCaps ? 65 : 97) + (number - 1));
            return c.ToString();
        }
    }
}
