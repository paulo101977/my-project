﻿using System;
using System.Diagnostics.CodeAnalysis;
using Tnf.Notifications;

namespace Thex.InMemoryDatabaseSetup
{
    [ExcludeFromCodeCoverage]
    public class RoomTypeSetupRegister: BaseSetup
    {
        public RoomTypeSetupRegister(IServiceProvider serviceProvider, NotificationHandler notificationHandler)
            : base(serviceProvider, notificationHandler)
        {  
        }
    }
}
