﻿using System;
using System.Diagnostics.CodeAnalysis;
using Tnf.Notifications;

namespace Thex.InMemoryDatabaseSetup
{
    [ExcludeFromCodeCoverage]
    public class GeneralOccupationSetupRegister : BaseSetup
    {
        public GeneralOccupationSetupRegister(IServiceProvider serviceProvider, NotificationHandler notificationHandler)
            :base(serviceProvider, notificationHandler)
        { 
        }
    }
}
