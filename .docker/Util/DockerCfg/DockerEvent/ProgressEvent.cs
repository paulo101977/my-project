﻿using System;
using System.Collections.Generic;

namespace DockerEvent
{
    public class ProgressEvent
    {
        public ProgressEvent() {}

        public void Add(string result, int percent)
        {
            var res = new ProgressEventArgs();

            res.Result = result;
            res.Percent = percent;
            res.TimeReached = DateTime.Now;

            OnThresholdReached(res);
        }

        protected virtual void OnThresholdReached(ProgressEventArgs e)
        {
            ProcessReached?.Invoke(this, e);
        }

        public event EventHandler<ProgressEventArgs> ProcessReached;
    }

    public class ProgressEventArgs : EventArgs
    {
        public string Result { get; set; }
        public int Percent { get; set; }
        public DateTime TimeReached { get; set; }
    }
}
