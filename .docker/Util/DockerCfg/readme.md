## Thex - Docker: Criado por Totvs RJ

#### Dependências:
* Docker 
   - https://hub.docker.com/editions/community/docker-ce-desktop-windows
* Dac Framework
   - https://docs.microsoft.com/pt-br/sql/tools/sqlpackage-download?view=sql-server-2017
*  Command Line tools SQL Server (caso não tenha sql server client instalado)
   - https://www.microsoft.com/en-us/download/details.aspx?id=53591
* Git 
   - https://git-scm.com/downloads
* Acesso ao repositório Thex
   - https://totvshoteis.visualstudio.com/all/_git/thex
* Compartilhar o diretório C:\ ou o diretório do Thex no Docker 
   - https://rominirani.com/docker-on-windows-mounting-host-directories-d96f3f056a2c (Até o passo 5)

#### Como utilizar:
1.  Ao abrir o programa, selecione o diretório do projeto Thex e click em OK;
2.  Selecione a branch desejada;
3.  Caso haja banco, informar o caminho ou string de conexão para PMS ou Tax;
4.  Selecione os projetos que deseja subir com o Docker;
5.  Click em executar e aguarde o procedimento.

[Dúvidas: patryk.moura@totvs.com.br](mailto:patryk.moura@totvs.com.br)

**Enjoy!**