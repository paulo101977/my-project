﻿namespace DockerCfg
{
    partial class Main
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.cbBranch = new System.Windows.Forms.ComboBox();
            this.lblBranch = new System.Windows.Forms.Label();
            this.lblProjects = new System.Windows.Forms.Label();
            this.lbProjects = new System.Windows.Forms.ListBox();
            this.btnExecute = new System.Windows.Forms.Button();
            this.lblDbPMS = new System.Windows.Forms.Label();
            this.tbDbPMS = new System.Windows.Forms.TextBox();
            this.tbDbTax = new System.Windows.Forms.TextBox();
            this.lblDbTax = new System.Windows.Forms.Label();
            this.ofdPMS = new System.Windows.Forms.OpenFileDialog();
            this.ofdTax = new System.Windows.Forms.OpenFileDialog();
            this.btnSearchDbPMS = new System.Windows.Forms.Button();
            this.btnSearchDbTax = new System.Windows.Forms.Button();
            this.pgbCmd = new System.Windows.Forms.ProgressBar();
            this.fbdGit = new System.Windows.Forms.FolderBrowserDialog();
            this.bgwProgress = new System.ComponentModel.BackgroundWorker();
            this.lblPercent = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblCurrentProcess = new System.Windows.Forms.Label();
            this.llShowPorts = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bancoDeDadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarPMSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importarTaxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofdScript = new System.Windows.Forms.OpenFileDialog();
            this.ofdDatabase = new System.Windows.Forms.OpenFileDialog();
            this.tmCounter = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbBranch
            // 
            this.cbBranch.FormattingEnabled = true;
            this.cbBranch.Location = new System.Drawing.Point(16, 55);
            this.cbBranch.Name = "cbBranch";
            this.cbBranch.Size = new System.Drawing.Size(269, 21);
            this.cbBranch.TabIndex = 0;
            // 
            // lblBranch
            // 
            this.lblBranch.AutoSize = true;
            this.lblBranch.Location = new System.Drawing.Point(13, 39);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(41, 13);
            this.lblBranch.TabIndex = 1;
            this.lblBranch.Text = "Branch";
            // 
            // lblProjects
            // 
            this.lblProjects.AutoSize = true;
            this.lblProjects.Location = new System.Drawing.Point(14, 184);
            this.lblProjects.Name = "lblProjects";
            this.lblProjects.Size = new System.Drawing.Size(45, 13);
            this.lblProjects.TabIndex = 2;
            this.lblProjects.Text = "Projetos";
            // 
            // lbProjects
            // 
            this.lbProjects.FormattingEnabled = true;
            this.lbProjects.Location = new System.Drawing.Point(16, 202);
            this.lbProjects.Name = "lbProjects";
            this.lbProjects.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbProjects.Size = new System.Drawing.Size(269, 108);
            this.lbProjects.TabIndex = 5;
            // 
            // btnExecute
            // 
            this.btnExecute.BackColor = System.Drawing.Color.SteelBlue;
            this.btnExecute.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExecute.ForeColor = System.Drawing.Color.White;
            this.btnExecute.Location = new System.Drawing.Point(16, 404);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(269, 42);
            this.btnExecute.TabIndex = 6;
            this.btnExecute.Text = "Executar";
            this.btnExecute.UseVisualStyleBackColor = false;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // lblDbPMS
            // 
            this.lblDbPMS.AutoSize = true;
            this.lblDbPMS.Location = new System.Drawing.Point(13, 85);
            this.lblDbPMS.Name = "lblDbPMS";
            this.lblDbPMS.Size = new System.Drawing.Size(64, 13);
            this.lblDbPMS.TabIndex = 7;
            this.lblDbPMS.Text = "Banco PMS";
            // 
            // tbDbPMS
            // 
            this.tbDbPMS.Location = new System.Drawing.Point(16, 101);
            this.tbDbPMS.Name = "tbDbPMS";
            this.tbDbPMS.Size = new System.Drawing.Size(202, 20);
            this.tbDbPMS.TabIndex = 8;
            // 
            // tbDbTax
            // 
            this.tbDbTax.Location = new System.Drawing.Point(17, 149);
            this.tbDbTax.Name = "tbDbTax";
            this.tbDbTax.Size = new System.Drawing.Size(201, 20);
            this.tbDbTax.TabIndex = 10;
            // 
            // lblDbTax
            // 
            this.lblDbTax.AutoSize = true;
            this.lblDbTax.Location = new System.Drawing.Point(14, 133);
            this.lblDbTax.Name = "lblDbTax";
            this.lblDbTax.Size = new System.Drawing.Size(59, 13);
            this.lblDbTax.TabIndex = 9;
            this.lblDbTax.Text = "Banco Tax";
            // 
            // ofdPMS
            // 
            this.ofdPMS.FileName = "Thex.Database.Schema.bacpac";
            // 
            // ofdTax
            // 
            this.ofdTax.FileName = "Thex.Rules.Schema.bacpac";
            // 
            // btnSearchDbPMS
            // 
            this.btnSearchDbPMS.Location = new System.Drawing.Point(226, 99);
            this.btnSearchDbPMS.Name = "btnSearchDbPMS";
            this.btnSearchDbPMS.Size = new System.Drawing.Size(58, 23);
            this.btnSearchDbPMS.TabIndex = 11;
            this.btnSearchDbPMS.Text = "Buscar";
            this.btnSearchDbPMS.UseVisualStyleBackColor = true;
            this.btnSearchDbPMS.Click += new System.EventHandler(this.btnSearchDbPMS_Click);
            // 
            // btnSearchDbTax
            // 
            this.btnSearchDbTax.Location = new System.Drawing.Point(226, 147);
            this.btnSearchDbTax.Name = "btnSearchDbTax";
            this.btnSearchDbTax.Size = new System.Drawing.Size(60, 23);
            this.btnSearchDbTax.TabIndex = 12;
            this.btnSearchDbTax.Text = "Buscar";
            this.btnSearchDbTax.UseVisualStyleBackColor = true;
            this.btnSearchDbTax.Click += new System.EventHandler(this.btnSearchDbTax_Click);
            // 
            // pgbCmd
            // 
            this.pgbCmd.Location = new System.Drawing.Point(18, 337);
            this.pgbCmd.Name = "pgbCmd";
            this.pgbCmd.Size = new System.Drawing.Size(185, 23);
            this.pgbCmd.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbCmd.TabIndex = 13;
            this.pgbCmd.Visible = false;
            // 
            // fbdGit
            // 
            this.fbdGit.Description = "Selecionar caminho do Projeto";
            this.fbdGit.SelectedPath = "c:\\";
            // 
            // bgwProgress
            // 
            this.bgwProgress.WorkerReportsProgress = true;
            this.bgwProgress.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwProgress_DoWork);
            this.bgwProgress.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwProgress_ProgressChanged);
            this.bgwProgress.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwProgress_RunWorkerCompleted);
            // 
            // lblPercent
            // 
            this.lblPercent.AutoSize = true;
            this.lblPercent.Location = new System.Drawing.Point(210, 343);
            this.lblPercent.Name = "lblPercent";
            this.lblPercent.Size = new System.Drawing.Size(33, 13);
            this.lblPercent.TabIndex = 14;
            this.lblPercent.Text = "100%";
            this.lblPercent.Visible = false;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(254, 343);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(34, 13);
            this.lblTime.TabIndex = 15;
            this.lblTime.Text = "00:00";
            this.lblTime.Visible = false;
            // 
            // lblCurrentProcess
            // 
            this.lblCurrentProcess.Location = new System.Drawing.Point(17, 364);
            this.lblCurrentProcess.Name = "lblCurrentProcess";
            this.lblCurrentProcess.Size = new System.Drawing.Size(290, 37);
            this.lblCurrentProcess.TabIndex = 16;
            this.lblCurrentProcess.Text = "Carregando ... \r\n";
            this.lblCurrentProcess.Visible = false;
            // 
            // llShowPorts
            // 
            this.llShowPorts.AutoSize = true;
            this.llShowPorts.Location = new System.Drawing.Point(232, 184);
            this.llShowPorts.Name = "llShowPorts";
            this.llShowPorts.Size = new System.Drawing.Size(56, 13);
            this.llShowPorts.TabIndex = 18;
            this.llShowPorts.TabStop = true;
            this.llShowPorts.Text = "Ver Portas";
            this.llShowPorts.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llShowPorts_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(313, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(279, 468);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(591, 24);
            this.menuStrip1.TabIndex = 19;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bancoDeDadosToolStripMenuItem,
            this.sairToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // bancoDeDadosToolStripMenuItem
            // 
            this.bancoDeDadosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importarScriptToolStripMenuItem,
            this.importarPMSToolStripMenuItem,
            this.importarTaxToolStripMenuItem});
            this.bancoDeDadosToolStripMenuItem.Name = "bancoDeDadosToolStripMenuItem";
            this.bancoDeDadosToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.bancoDeDadosToolStripMenuItem.Text = "Banco de Dados";
            // 
            // importarScriptToolStripMenuItem
            // 
            this.importarScriptToolStripMenuItem.Name = "importarScriptToolStripMenuItem";
            this.importarScriptToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.importarScriptToolStripMenuItem.Text = "Importar Script";
            this.importarScriptToolStripMenuItem.Click += new System.EventHandler(this.ImportarScriptToolStripMenuItem_Click);
            // 
            // importarPMSToolStripMenuItem
            // 
            this.importarPMSToolStripMenuItem.Name = "importarPMSToolStripMenuItem";
            this.importarPMSToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.importarPMSToolStripMenuItem.Text = "Importar PMS";
            this.importarPMSToolStripMenuItem.Click += new System.EventHandler(this.ImportarPMSToolStripMenuItem_Click);
            // 
            // importarTaxToolStripMenuItem
            // 
            this.importarTaxToolStripMenuItem.Name = "importarTaxToolStripMenuItem";
            this.importarTaxToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.importarTaxToolStripMenuItem.Text = "Importar Tax";
            this.importarTaxToolStripMenuItem.Click += new System.EventHandler(this.ImportarTaxToolStripMenuItem_Click);
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.sairToolStripMenuItem.Text = "Sair";
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.SairToolStripMenuItem_Click);
            // 
            // ofdScript
            // 
            this.ofdScript.FileName = "script.sql";
            this.ofdScript.InitialDirectory = "C:";
            // 
            // tmCounter
            // 
            this.tmCounter.Interval = 1000;
            this.tmCounter.Tick += new System.EventHandler(this.tmCounter_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 467);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pgbCmd);
            this.Controls.Add(this.lblPercent);
            this.Controls.Add(this.llShowPorts);
            this.Controls.Add(this.lblCurrentProcess);
            this.Controls.Add(this.btnSearchDbTax);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.btnSearchDbPMS);
            this.Controls.Add(this.tbDbTax);
            this.Controls.Add(this.lblDbTax);
            this.Controls.Add(this.tbDbPMS);
            this.Controls.Add(this.lblDbPMS);
            this.Controls.Add(this.btnExecute);
            this.Controls.Add(this.lbProjects);
            this.Controls.Add(this.lblProjects);
            this.Controls.Add(this.lblBranch);
            this.Controls.Add(this.cbBranch);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configurar Docker";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbBranch;
        private System.Windows.Forms.Label lblBranch;
        private System.Windows.Forms.Label lblProjects;
        private System.Windows.Forms.ListBox lbProjects;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.Label lblDbPMS;
        private System.Windows.Forms.TextBox tbDbPMS;
        private System.Windows.Forms.TextBox tbDbTax;
        private System.Windows.Forms.Label lblDbTax;
        private System.Windows.Forms.OpenFileDialog ofdPMS;
        private System.Windows.Forms.OpenFileDialog ofdTax;
        private System.Windows.Forms.Button btnSearchDbPMS;
        private System.Windows.Forms.Button btnSearchDbTax;
        private System.Windows.Forms.FolderBrowserDialog fbdGit;
        private System.ComponentModel.BackgroundWorker bgwProgress;
        private System.Windows.Forms.Label lblPercent;
        public System.Windows.Forms.ProgressBar pgbCmd;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblCurrentProcess;
        private System.Windows.Forms.LinkLabel llShowPorts;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bancoDeDadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importarScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importarPMSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importarTaxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog ofdScript;
        private System.Windows.Forms.OpenFileDialog ofdDatabase;
        private System.Windows.Forms.Timer tmCounter;
    }
}

