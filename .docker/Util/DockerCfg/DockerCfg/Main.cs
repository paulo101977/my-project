﻿using DockerEvent;
using DockerModel.Configuration;
using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace DockerCfg
{
    public partial class Main : Form
    {
        public List<Branch> _branches = new List<Branch>();
        public List<string> _command = new List<string>();
        public List<string> _output = new List<string>();

        private ProgressEvent _progressEvent = new ProgressEvent();
        private DockerBusiness.Execute _execBus = new DockerBusiness.Execute();
        private DockerBusiness.Configuration _configBus = new DockerBusiness.Configuration();

        int _second = 0;
        DateTime _timerDate = new DateTime();
        bool bSimpleLog = false;

        public Main()
        {
            InitializeComponent();
            _progressEvent.ProcessReached += ProcessReached;
        }

        void ProcessReached(object sender, ProgressEventArgs e)
        {
            var log = e.Result;
            var inc = e.Percent;

            if (bSimpleLog && !String.IsNullOrEmpty(log) && log.Substring(0, 1) == ">" && log.Length > 2)
            {
                BeginInvoke((MethodInvoker)delegate () { FillCaptionLabel(log); });
                _output.Add(log);
            }
            else if (!String.IsNullOrEmpty(log))
            {
                BeginInvoke((MethodInvoker)delegate () { FillCaptionLabel(log); });
                _output.Add(log);
            }

            bgwProgress.ReportProgress(inc);
        }

        private void LoadBranches(string path)
        {
            var gitBus = new DockerBusiness.Git();
            if (!gitBus.LoadBranches(path))
            {
                MessageBox.Show("O caminho informado não é um projeto Git válido");
                TryInit();
            }
            else
            {
                cbBranch.ValueMember = "FriendlyName";
                cbBranch.DisplayMember = "FriendlyName";
                cbBranch.DataSource = gitBus._branches;
                cbBranch.SelectedValue = gitBus._headName;
            }
        }

        private void LoadProjects()
        {
            lbProjects.DataSource = GetAllProjects();
            lbProjects.ValueMember = "Name";
            lbProjects.DisplayMember = "FriendlyName";
        }

        private List<ProjectDetail> GetAllProjects()
        {
            return _configBus.GetAllProjects();
        }

        private bool ValidateForm()
        {
            var bRet = true;

            if (bRet && cbBranch.SelectedValue == null)
            {
                MessageBox.Show("Por favor selecione uma branch para continuar.");
                bRet = false;
            }

            if (bRet && !lbProjects.SelectedItems.Cast<ProjectDetail>().Any())
            {
                MessageBox.Show("Por favor selecione um projeto para continuar.");
                bRet = false;
            }

            if (bRet && tbDbPMS.Text != "" && !File.Exists(tbDbPMS.Text) && !_execBus.CheckSQLServerConn(tbDbPMS.Text, false))
            {
                MessageBox.Show("Arquivo do banco PMS não encontrado.");
                bRet = false;
            }

            if (bRet && tbDbTax.Text != "" && !File.Exists(tbDbTax.Text) && !_execBus.CheckSQLServerConn(tbDbTax.Text, false))
            {
                MessageBox.Show("Arquivo do banco Tax não encontrado.");
                bRet = false;
            }

            return bRet;
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
                return;

            _command.Clear();
            _output.Clear();

            _command = _execBus.AddCommands(
                cbBranch.SelectedValue.ToString().Replace("origin/", ""),
                lbProjects.SelectedItems.Cast<ProjectDetail>().ToList(),
                tbDbPMS.Text,
                tbDbTax.Text
            );

            ExecuteBackground();
        }

        public void ExecuteBackground()
        {
            btnExecute.BackColor = Color.Gray;
            btnExecute.ForeColor = Color.White;
            btnExecute.Enabled = false;

            pgbCmd.Visible = true;
            lblPercent.Visible = true;
            lblCurrentProcess.Visible = true;
            lblTime.Visible = true;

            pgbCmd.ForeColor = Color.Gray;
            pgbCmd.Value = 0;
            lblPercent.Text = "";

            _second = 0;
            tmCounter.Start();
            bgwProgress.RunWorkerAsync();
        }

        private void ExecuteCmd(string arg, int inc)
        {
            _execBus._workingDirectory = Path.Combine(fbdGit.SelectedPath, ".docker");
            _execBus.ExecuteCmd(_progressEvent, arg, inc);
        }

        private void TryInit()
        {
            if (fbdGit.ShowDialog() == DialogResult.OK)
            {
                Focus();
                Activate();

                LoadBranches(fbdGit.SelectedPath);
                LoadProjects();
            }
            else
            {
                Close();
            }
        }

        private void btnSearchDbPMS_Click(object sender, EventArgs e)
        {
            if (ofdPMS.ShowDialog() == DialogResult.OK)
            {
                tbDbPMS.Text = ofdPMS.FileName;
            }
        }

        private void btnSearchDbTax_Click(object sender, EventArgs e)
        {
            if (ofdTax.ShowDialog() == DialogResult.OK)
            {
                tbDbTax.Text = ofdTax.FileName;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TryInit();
        }

        private void bgwProgress_DoWork(object sender, DoWorkEventArgs e)
        {
            var i = 0;

            foreach (var arg in _command)
            {
                var percent = (int)Math.Round(((double)i / (double)_command.Count) * 100);

                if (arg.ToLower().Contains("sqlcmd") || arg.ToLower().Contains("sqlpackage"))
                {
                    _execBus.CheckSQLServerConn();
                }

                ExecuteCmd(arg, percent);
                i++;
            }
        }

        private void FillCaptionLabel(string arg)
        {
            lblCurrentProcess.Text = "Processo: ";

            if (arg.Length > 60 && arg.Length > 64)
            {
                lblCurrentProcess.Text += arg.Substring(0, 60) + " ...";
            }
            else
            {
                lblCurrentProcess.Text += arg;
            }
        }

        private void bgwProgress_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pgbCmd.ForeColor = Color.Green;
            pgbCmd.Value = e.ProgressPercentage;
            lblPercent.Text = e.ProgressPercentage.ToString() + "%";
        }

        private void bgwProgress_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pgbCmd.Visible = false;
            lblPercent.Visible = false;
            lblCurrentProcess.Visible = false;
            lblTime.Visible = false;

            btnExecute.BackColor = Color.SteelBlue;
            btnExecute.Enabled = true;
            
            tmCounter.Stop();

            var bErro = _output.Any(x => x.ToLower().Contains("erro ") || x.ToLower().Contains("error "));
            var title = (bErro) ? "Erro na execução" : "Executado com sucesso!";
            var body = (bErro) ? "Ocorreram erros na execução, deseja verificar o log?" : "Parabéns, o processo foi finalizado com sucesso! Deseja verificar o log?";
            var icon = (bErro) ? MessageBoxIcon.Error : MessageBoxIcon.Information;

            var messageRes = MessageBox.Show(body, title, MessageBoxButtons.YesNo, icon);
            if (messageRes == DialogResult.Yes)
            {
                var LogWindow = new LogResult();
                LogWindow.LoadData(_output);
                LogWindow.ShowDialog();
            }
        }

        private void tmCounter_Tick(object sender, EventArgs e)
        {
            _second++;
            lblTime.Text = _timerDate.AddSeconds(_second).ToString("mm:ss");
        }

        private void llShowPorts_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var portList = new PortList();
            portList._projects = GetAllProjects();
            portList.ShowDialog();
        }

        private void ImportarScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ofdScript.ShowDialog() == DialogResult.OK)
            {
                _command.Clear();
                _output.Clear();

                _command.Add("docker start sql-data");
                if (!_execBus.ImportDBScript(ofdScript.FileName))
                {
                    MessageBox.Show("Erro ao realizar tarefa.");
                    return;
                }

                _command = _command.Concat(_execBus._command).ToList();
                ExecuteBackground();
            }
        }

        private void ImportarPMSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dbImport = new DBImport();

            dbImport._main = this;
            dbImport.ChangeLabel("Thex.Dev");
            dbImport.ShowDialog();
        }

        private void ImportarTaxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dbImport = new DBImport();

            dbImport._main = this;
            dbImport.ChangeLabel("Thex.Rules");
            dbImport.ShowDialog();
        }

        private void SairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
