﻿using DockerModel.Configuration;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DockerCfg
{
    public partial class PortList : Form
    {
        public List<ProjectDetail> _projects = new List<ProjectDetail>();

        public PortList()
        {
            InitializeComponent();
        }

        private void PortList_Load(object sender, EventArgs e)
        {
            dgvPortas.DataSource = _projects;
        }
    }
}
