﻿namespace DockerCfg
{
    partial class DBImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBImport));
            this.btnSearchDb = new System.Windows.Forms.Button();
            this.tbDatabase = new System.Windows.Forms.TextBox();
            this.lblDb = new System.Windows.Forms.Label();
            this.btnExecute = new System.Windows.Forms.Button();
            this.ofdDB = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // btnSearchDb
            // 
            this.btnSearchDb.Location = new System.Drawing.Point(237, 36);
            this.btnSearchDb.Name = "btnSearchDb";
            this.btnSearchDb.Size = new System.Drawing.Size(60, 23);
            this.btnSearchDb.TabIndex = 13;
            this.btnSearchDb.Text = "Buscar";
            this.btnSearchDb.UseVisualStyleBackColor = true;
            this.btnSearchDb.Click += new System.EventHandler(this.BtnSearchDb_Click);
            // 
            // tbDatabase
            // 
            this.tbDatabase.Location = new System.Drawing.Point(29, 38);
            this.tbDatabase.Name = "tbDatabase";
            this.tbDatabase.Size = new System.Drawing.Size(202, 20);
            this.tbDatabase.TabIndex = 12;
            // 
            // lblDb
            // 
            this.lblDb.AutoSize = true;
            this.lblDb.Location = new System.Drawing.Point(27, 14);
            this.lblDb.Name = "lblDb";
            this.lblDb.Size = new System.Drawing.Size(38, 13);
            this.lblDb.TabIndex = 14;
            this.lblDb.Text = "Banco";
            // 
            // btnExecute
            // 
            this.btnExecute.BackColor = System.Drawing.Color.SteelBlue;
            this.btnExecute.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExecute.ForeColor = System.Drawing.Color.White;
            this.btnExecute.Location = new System.Drawing.Point(205, 75);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(92, 42);
            this.btnExecute.TabIndex = 15;
            this.btnExecute.Text = "Processar";
            this.btnExecute.UseVisualStyleBackColor = false;
            this.btnExecute.Click += new System.EventHandler(this.BtnExecute_Click);
            // 
            // ofdDB
            // 
            this.ofdDB.FileName = "Thex.Database.Schema.bacpac";
            // 
            // DBImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 126);
            this.Controls.Add(this.btnExecute);
            this.Controls.Add(this.lblDb);
            this.Controls.Add(this.btnSearchDb);
            this.Controls.Add(this.tbDatabase);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DBImport";
            this.Text = "Importar Banco";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSearchDb;
        private System.Windows.Forms.TextBox tbDatabase;
        private System.Windows.Forms.Label lblDb;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.OpenFileDialog ofdDB;
    }
}