﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DockerCfg
{
    public partial class LogResult : Form
    {
        public LogResult()
        {
            InitializeComponent();
        }

        public void LoadData(List<string> logs)
        {
            tbLog.Text = "";
            //logs.Reverse();
            logs.ForEach(log => tbLog.Text += log + Environment.NewLine);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
