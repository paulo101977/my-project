﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DockerCfg
{
    public partial class DBImport : Form
    {
        public Main _main;
        private DockerBusiness.Execute _execBus = new DockerBusiness.Execute();
        public string dbName = "Thex.Dev";

        public DBImport()
        {
            InitializeComponent();
        }

        public void ChangeLabel(string _dbName)
        {
            dbName = _dbName;
            lblDb.Text += ": " + dbName;
        }

        private void BtnSearchDb_Click(object sender, EventArgs e)
        {
            if (ofdDB.ShowDialog() == DialogResult.OK)
            {
                tbDatabase.Text = ofdDB.FileName;
            }
        }

        private void BtnExecute_Click(object sender, EventArgs e)
        {
            if (_main != null)
            {
                _main._command.Clear();
                _main._output.Clear();

                _main._command.Add("docker start sql-data");
                if (!_execBus.ImportDB(tbDatabase.Text, dbName))
                {
                    MessageBox.Show("Erro ao realizar tarefa.");
                    return;
                }


                _main._command = _main._command.Concat(_execBus._command).ToList();
                _main.ExecuteBackground();
                Close();
            } 
        }
    }
}
