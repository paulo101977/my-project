﻿using System;
using System.Configuration;

namespace DockerModel.Configuration
{
    [ConfigurationCollection(typeof(ProjectDetail))]
    public class ProjectDetails : ConfigurationElementCollection
    {
        internal const string PropertyName = "ProjectDetail";

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }
        protected override string ElementName
        {
            get
            {
                return PropertyName;
            }
        }

        protected override bool IsElementName(string elementName)
        {
            return elementName.Equals(PropertyName,
              StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool IsReadOnly()
        {
            return false;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ProjectDetail();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ProjectDetail)(element)).Id;
        }

        public ProjectDetail this[int idx]
        {
            get { return (ProjectDetail)BaseGet(idx); }
        }
    }
}
