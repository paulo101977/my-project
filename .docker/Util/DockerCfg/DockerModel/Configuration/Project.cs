﻿using System.Configuration;

namespace DockerModel.Configuration
{
    public class ProjectSettings : ConfigurationSection
    {
        [ConfigurationProperty("ProjectDetails")]
        public ProjectDetails Details
        {
            get { return ((ProjectDetails)(this["ProjectDetails"])); }
            set { this["ProjectDetails"] = value; }
        }
    }
}
