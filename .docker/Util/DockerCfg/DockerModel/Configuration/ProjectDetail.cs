﻿using System.Configuration;

namespace DockerModel.Configuration
{
    public class ProjectDetail : ConfigurationElement
    {
        [ConfigurationProperty("Id", IsRequired = true, IsKey = true)]
        public int Id
        {
            get { return (int)this["Id"]; }
        }

        [ConfigurationProperty("Name", DefaultValue = "", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["Name"];
            }
            set
            {
                value = (string)this["Name"];
            }
        }

        [ConfigurationProperty("FriendlyName", DefaultValue = "", IsRequired = true)]
        public string FriendlyName
        {
            get
            {
                return (string)this["FriendlyName"];
            }
            set
            {
                value = (string)this["FriendlyName"];
            }
        }

        [ConfigurationProperty("Port", DefaultValue = "", IsRequired = true)]
        public string Port
        {
            get
            {
                return (string)this["Port"];
            }
            set
            {
                value = (string)this["Port"];
            }
        }
    }
}
