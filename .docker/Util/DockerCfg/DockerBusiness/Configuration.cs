﻿using DockerModel.Configuration;
using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace DockerBusiness
{
    public class Configuration
    {
        public List<ProjectDetail> _projectDetails = new List<ProjectDetail>();

        public List<ProjectDetail> GetAllProjects()
        {
            _projectDetails = (ConfigurationManager.GetSection("Project") as ProjectSettings).Details.Cast<ProjectDetail>().ToList();
            return _projectDetails;
        }
    }
}
