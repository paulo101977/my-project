﻿using DockerModel.Configuration;
using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Diagnostics;
using DockerEvent;

namespace DockerBusiness
{
    public class Execute
    {
        public List<string> _command = new List<string>();
        public string _workingDirectory = "";
        private string sqlPass = "cmsol@strongpass!123";

        public bool bSimpleLog;

        public void ExecuteCmd(ProgressEvent _progressEvent, string arg, int inc)
        {
            Process cmd = new Process();
            cmd.StartInfo.WorkingDirectory = _workingDirectory;
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();

            cmd.StandardInput.WriteLine(arg);

            cmd.OutputDataReceived += (sender, args) =>
            {
                var log = (args.Data != null) ? args.Data.Replace(cmd.StartInfo.WorkingDirectory, "") : "";
                _progressEvent.Add(log, inc);
            };

            cmd.ErrorDataReceived += (sender, args) =>
            {
                _progressEvent.Add(args.Data, inc);
            };

            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.BeginOutputReadLine();
            cmd.WaitForExit();
        }

        public List<string> AddCommands(string branch, List<ProjectDetail> projectList, string PMSDatabase, string TaxDatabase)
        {
            var _projects = projectList.Select(k => k.Name).Aggregate((k, j) => k + " " + j);
            _command.Clear();

            _command.Add("git config --global credential.helper store");
            _command.Add("git reset --hard");
            _command.Add("git clean -fd");
            _command.Add("git fetch --all");
            _command.Add("git checkout " + branch);
            _command.Add("git pull");
            _command.Add("docker stop sql-data");

            projectList.ForEach(p =>
            {
                _command.Add("docker-compose up -d --build " + p.Name);
            });

            _command.Add("docker start sql-data");

            ImportDB(PMSDatabase, "THEx.Dev");
            ImportDB(TaxDatabase, "THEx.Rules");

            return _command;
        }

        public bool ImportDB(string DBImport, string DBLocal)
        {
            var _sqlpackage = Path.Combine(Environment.ExpandEnvironmentVariables("%ProgramW6432%"), @"Microsoft SQL Server\150\DAC\bin\sqlpackage");

            if (!String.IsNullOrEmpty(DBImport))
            {
                if (File.Exists(DBImport))
                {
                    _command.Add($"sqlcmd -S localhost -U sa -P \"{sqlPass}\" -Q \"drop database [{DBLocal}]\" ");
                    _command.Add($"\"{_sqlpackage}\"  /a:Import /sf:\"{DBImport}\" /tsn:\"localhost\" /tdn:\"THEx.Dev\" /tu:\"sa\" /tp:\"{sqlPass}\"");
                }
                else if (CheckSQLServerConn(DBImport, false))
                {
                    _command.Add($"sqlcmd -S localhost -U sa -P \"{sqlPass}\" -Q \"drop database [{DBLocal}]\" ");
                    _command.Add($"\"{_sqlpackage}\"  /a:Export /scs:\"{DBImport}\" /tf:\"{DBLocal}.bacpac\"");
                    _command.Add($"\"{_sqlpackage}\"  /a:Import /sf:\"{DBLocal}.bacpac\" /tsn:\"localhost\" /tdn:\"{DBLocal}\" /tu:\"sa\" /tp:\"{sqlPass}\"");
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public bool ImportDBScript(string script)
        {
            if (!String.IsNullOrEmpty(script))
            {
                if (File.Exists(script))
                {
                    _command.Add($"sqlcmd -S localhost -U sa -P \"{sqlPass}\" -i \"{script}\" ");
                    return true;
                }
            }

            return false;
        }

        public bool CheckSQLServerConn(string conn = "", bool bRecursive = true)
        {
            if (String.IsNullOrEmpty(conn))
            {
                conn = $"Data Source=localhost;Integrated Security=False;User ID=sa;Password={sqlPass}";
            }

            try
            {
                using (var connection = new SqlConnection(conn))
                {
                    var query = "select 1";
                    var command = new SqlCommand(query, connection);
                    connection.Open();
                    command.ExecuteScalar();
                }

                return true;
            }
            catch (Exception)
            {
                if (bRecursive)
                {
                    System.Threading.Thread.Sleep(20000);
                    return CheckSQLServerConn();
                }

                return false;
            }
        }
    }
}
