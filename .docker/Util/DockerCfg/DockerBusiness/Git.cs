﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DockerBusiness
{
    public class Git
    {
        public List<Branch> _branches = new List<Branch>();
        public string _headName = "";

        public bool LoadBranches(string path)
        {
            try
            {
                using (var repo = new Repository(path))
                {
                    _branches = repo.Branches.ToList();
                    _headName = repo.Head.FriendlyName;
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
    }
}
