import { addDecorator, configure } from '@storybook/angular';
import { setOptions } from '@storybook/addon-options';
import { withNotes } from '@storybook/addon-notes';

import '../styles/main.scss';

// setup separator
setOptions({
  hierarchyRootSeparator: /\|/,
});

addDecorator(
  withNotes()
)

function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

function loadStories() {
  requireAll(require.context('../projects/ui/', true, /\.stories\.ts$/))
}

configure(loadStories, module);
