import * as Remarkable from 'remarkable';

import hljs from 'highlight.js'; // https://highlightjs.org/

const getHighlight = (str, lang) => {
    if (lang && hljs.getLanguage(lang)) {
        try {
            return hljs.highlight(lang, str).value;
        } catch (err) {
            console.error(err)
        }
    }

    try {
        return hljs.highlightAuto(str).value;
    } catch (err) {
        console.error(err)
    }

    return ''; // use external default escaping
}

export const getHtmlString = (str) => {
    const instance = new Remarkable({highlight: getHighlight});

    let outputStr = "";

    if(!str || typeof str !== "string") return "";

    instance.set({
        html: true,
        breaks: true
    });

    outputStr = instance.render(str);

    return outputStr;
}
