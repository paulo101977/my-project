const path = require("path");

module.exports = ({ config, mode }) => {

    // reset the config default
    //config.module.rules = []

    // tsx loader
    config.module.rules.push({
        test: /\.stories\.ts$/,
        loaders: [
            {
                loader: require.resolve('@storybook/addon-storysource/loader'),
                options: {
                    parser: 'typescript',
                },
            }
        ],
        include: [path.resolve(__dirname, '..')],
        enforce: 'pre',
    });

    // story snippet
    config.module.rules.push({
        test: /\.stories\.jsx?$/,
        loaders: [require.resolve('@storybook/addon-storysource/loader')],
        enforce: 'pre',
    });

    // scss loader
    config.module.rules.push({
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
        include: path.resolve(__dirname, '../stylus'),
    });

    // uncomment if necessary
    /*config.module.rules.push({
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
    });*/

    // resolve relative paths
    config.resolve = {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        alias: {
            // 'app': path.resolve(__dirname, '../src/app'),
            // 'addon-jest.testresults.json': path.resolve(__dirname, '../addon-jest.testresults.json'),
            '.storybook': path.resolve(__dirname, '../.storybook')
        }
    }

    return config;
};
