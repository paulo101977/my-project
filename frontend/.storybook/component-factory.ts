import { Component  } from '@angular/core';

/**
* create generic component for use in storybook
*
* @param {string} selector the component selector
* @param {string} template the template string of component
* @param {Array<any>>} scope optional component scope parameter
*
* @return {Component} the created component class
*/

const componentFactory = (selector: string, template: string, scope?: Object) => {
    @Component({
        selector,
        template
    })
    class CreatedComponent {
        public scope = scope;
    }

    return CreatedComponent;
};


export { componentFactory };
