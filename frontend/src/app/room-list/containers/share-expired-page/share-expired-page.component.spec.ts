import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareExpiredPageComponent } from './share-expired-page.component';
import { configureTestSuite } from 'ng-bullet';

describe('ShareExpiredPageComponent', () => {
  let component: ShareExpiredPageComponent;
  let fixture: ComponentFixture<ShareExpiredPageComponent>;


  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareExpiredPageComponent ]
    });
    fixture = TestBed.createComponent(ShareExpiredPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
