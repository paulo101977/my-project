import { TestBed } from '@angular/core/testing';

import { RoomlistCardSelectedService } from './roomlist-card-selected.service';

describe('RoomlistCardSelectedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoomlistCardSelectedService = TestBed.get(RoomlistCardSelectedService);
    expect(service).toBeTruthy();
  });
});
