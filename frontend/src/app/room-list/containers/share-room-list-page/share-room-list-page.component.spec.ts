// import { ComponentFixture, TestBed } from '@angular/core/testing';
//
// import { ShareRoomListPageComponent } from './share-room-list-page.component';
// import { configureTestSuite } from 'ng-bullet';
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import {ThexAddressApiService, ThexApiTestingModule} from '@inovacaocmnet/thx-bifrost';
// import {Observable, of} from 'rxjs';
// import {HttpClientTestingModule} from '@angular/common/http/testing';
//
// describe('ShareRoomListPageComponent', () => {
//   let component: ShareRoomListPageComponent;
//   let fixture: ComponentFixture<ShareRoomListPageComponent>;
//
//   const thexAddressApiStub: Partial<ThexAddressApiService> = {
//     getCountries(): Observable<any> {
//       return of([]);
//     },
//     getNationalities(): Observable<any> {
//       return of([]);
//     }
//   };
//
//   configureTestSuite(() => {
//     TestBed.configureTestingModule({
//       imports: [ TranslateTestingModule,
//         FormsModule,
//         ReactiveFormsModule,
//         HttpClientTestingModule,
//         ThexApiTestingModule ],
//       declarations: [ ShareRoomListPageComponent ],
//       providers: [
//         {
//           provide: ThexAddressApiService,
//           useValue: thexAddressApiStub
//         }
//       ],
//       schemas: [ NO_ERRORS_SCHEMA ]
//     });
//     fixture = TestBed.createComponent(ShareRoomListPageComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
