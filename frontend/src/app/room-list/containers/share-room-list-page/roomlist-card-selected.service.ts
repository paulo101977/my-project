import { ExternalRoomlistDto } from 'app/room-list/shared/models/external-roomlist-dto';
import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoomlistCardSelectedService {

  private roomTypeChangeSource = new Subject<any>();
  public roomTypeChange$ = this.roomTypeChangeSource.asObservable();

  private listItems: ExternalRoomlistDto[];
  private activatedItem: ExternalRoomlistDto;
  private index: number;

  constructor() { }


  public setList(list: ExternalRoomlistDto[]) {
    this.listItems = list;
    if (list && list.length > 0 ) {
      this.setActivatedItemByIndex(0);
    }
  }

  public getList(): ExternalRoomlistDto[] {
    return this.listItems;
  }

  public getActivatedItem(): ExternalRoomlistDto {
    return this.activatedItem;
  }

  public getActivatedIndex(): number {
    return this.index;
  }

  public setActivatedItem(item: ExternalRoomlistDto) {
    if (this.activatedItem != item) {
      this.index = this.listItems.indexOf(item);
      this.activatedItem = item;
      this.roomTypeChangeSource.next(this.activatedItem);
    }
  }

  public setActivatedItemByIndex(index: number) {
    if (this.index != index) {
      this.index = index;
      this.activatedItem = this.listItems[index];
      this.roomTypeChangeSource.next(this.activatedItem);
    }
  }

  public activateNext() {
      const newIndex = this.index + 1;
      if (newIndex < this.listItems.length) {
        this.setActivatedItemByIndex( newIndex );
      }
  }

  public activatePrevious() {
    const newIndex = this.index - 1;
    if (newIndex >= 0) {
      this.setActivatedItemByIndex( newIndex );
    }
  }

}
