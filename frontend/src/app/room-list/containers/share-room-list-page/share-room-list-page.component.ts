import { Component, OnInit } from '@angular/core';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ExternalRoomlistHeaderDto } from 'app/room-list/shared/models/external-roomlist-header-dto';
import { RoomlistResource } from 'app/room-list/shared/services/roomlist.resource';
import { RoomlistCardSelectedService } from 'app/room-list/containers/share-room-list-page/roomlist-card-selected.service';
import { ExternalRoomlistDto } from 'app/room-list/shared/models/external-roomlist-dto';
import { pluck } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { ExternalRoomlistGuest } from 'app/room-list/shared/models/external-roomlist-guest';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-share-room-list-page',
  templateUrl: './share-room-list-page.component.html',
  styleUrls: ['./share-room-list-page.component.scss'],
})
export class ShareRoomListPageComponent implements OnInit {

  public roomlistInfo: ExternalRoomlistHeaderDto;

  public previousButtonConfig: ButtonConfig;
  public nextButtonConfig: ButtonConfig;

  public cardSelected: ExternalRoomlistDto;
  public guestList: ExternalRoomlistGuest[];
  private guestSubsciption: Subscription;

  constructor( private sharedService: SharedService,
               private roomlistResource: RoomlistResource,
               public roomlistCardSelected: RoomlistCardSelectedService,
               private translateService: TranslateService) { }

  ngOnInit() {
    this.laodRoomList();
    this.listenRoomTypeChanges();
    this.setButtonConfig();
  }

  private setButtonConfig() {
    this.previousButtonConfig = this.sharedService.getButtonConfig('previous-button',
      () => this.previousCard(),
      '',
      ButtonType.Secondary,
      ButtonSize.Normal);

    this.nextButtonConfig = this.sharedService.getButtonConfig('next-button',
      () => this.nextCard(),
      '',
      ButtonType.Secondary,
      ButtonSize.Normal);
  }

  public isExpanded(index: number) {
    return index == 0; // todo apply logic to control forms
  }

  private nextCard() {
    this.roomlistCardSelected.activateNext();
  }

  private previousCard() {
    this.roomlistCardSelected.activatePrevious();
  }

  private laodRoomList() {
    this.roomlistResource.getRoomlistInfo().subscribe( result => {
      this.roomlistInfo = result;
      this.roomlistCardSelected.setList(result.externalRoomListDto.items);
    });
  }

  private listenRoomTypeChanges() {
    this.roomlistCardSelected.roomTypeChange$.subscribe(cardSelected => {
      this.cardSelected = cardSelected;
      if (this.guestSubsciption) {
        this.guestSubsciption.unsubscribe();
      }
      this.guestSubsciption = this.roomlistResource
        .getRoomListGuests(this.cardSelected.reservationItemUid)
        .pipe( pluck('items'))
        .subscribe((list: ExternalRoomlistGuest[]) => {
          this.guestList = list.sort( (left) => left.isChild ? 1 : -1);
        });
    });
  }

  getFormTitle(guest: ExternalRoomlistGuest) {
      const filteredList: ExternalRoomlistGuest[] = this.guestList.filter( item => item.isChild == guest.isChild );
      const index = filteredList.indexOf(guest);
      return guest.isChild ?
        `${this.translateService.instant('label.child')} ${index + 1}` :
        `${this.translateService.instant('label.adult')} ${index + 1}`;
  }

  public canShowNextButton() {
    if (this.roomlistCardSelected && this.roomlistInfo && this.roomlistInfo.externalRoomListDto) {
      const index = (+this.roomlistCardSelected.getActivatedIndex() + 1);
      return  index < this.roomlistInfo.externalRoomListDto.items.length;
    }
    return true;
  }
}
