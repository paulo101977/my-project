import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareRoomListPageComponent } from './containers/share-room-list-page/share-room-list-page.component';
import { RoomListRoutingModule } from 'app/room-list/room-list-routing.module';
import { ShareExpiredPageComponent } from './containers/share-expired-page/share-expired-page.component';
import { RouterModule } from '@angular/router';
import { RoomListTopCardComponent } from 'app/room-list/shared/components/room-list-top-card/room-list-top-card.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'app/shared/shared.module';
import { RoomListSideBarComponent } from './shared/components/room-list-side-bar/room-list-side-bar.component';
import { RoomListFormComponent } from './shared/components/room-list-form/room-list-form.component';
import {
  ThxSubtitleV2Module,
  ThxInputV2Module,
  ThxTextareaV2Module,
  ThxSelectV2Module,
  ThxButtonModule,
  ThxImgModule,
  CardBaseModule,
  ExpandableBoxModule,
  InputSelectGroupModule } from '@inovacao-cmnet/thx-ui';
import { ReactiveFormsModule } from '@angular/forms';
import { ThexApiModule } from '@inovacaocmnet/thx-bifrost';

@NgModule({
  declarations: [
    ShareRoomListPageComponent,
    RoomListTopCardComponent,
    ShareExpiredPageComponent,
    RoomListSideBarComponent,
    RoomListFormComponent
  ],
  imports: [
    CommonModule,
    ThexApiModule,
    ReactiveFormsModule,
    TranslateModule,
    RouterModule,
    RoomListRoutingModule,
    ThxImgModule,
    CardBaseModule,
    SharedModule,
    ExpandableBoxModule,
    ThxSubtitleV2Module,
    ThxInputV2Module,
    ThxTextareaV2Module,
    ThxSelectV2Module,
    ThxButtonModule,
    InputSelectGroupModule,
  ],
  exports: [ ShareRoomListPageComponent, RoomListSideBarComponent, RoomListFormComponent ]
})
export class RoomListModule { }
