import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShareRoomListPageComponent } from 'app/room-list/containers/share-room-list-page/share-room-list-page.component';
import { ShareExpiredPageComponent } from 'app/room-list/containers/share-expired-page/share-expired-page.component';

const routes: Routes = [
  { path: '', component: ShareRoomListPageComponent },
  { path: 'expired/:token', component: ShareExpiredPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomListRoutingModule { }
