export interface ExternalRoomlistDto {
  name: string;
  reservationUid: string;
  reservationItemUid: string;
  statusId: number;
  roomNumber: string;
  roomTypeName: string;
  roomTypeAbbreviation: string;
  checkinDate?: string;
  checkoutDate?: string;
  estimatedArrivalDate: string;
  estimatedDepartureDate: string;
  adultCount: number;
  childCount: number;
  isValid: boolean;
  reservationItemCode: string;
  reservationItemStatus: number;
  guestName: string;
  searchableNames: string[];
}
