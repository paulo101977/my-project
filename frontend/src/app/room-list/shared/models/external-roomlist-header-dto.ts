import { ExternalRoomlistDto } from 'app/room-list/shared/models/external-roomlist-dto';
import {ItemsResponse} from 'app/shared/models/backend-api/item-response';

export interface ExternalRoomlistHeaderDto {
  name: string;
  adultcount: number;
  childcount: number;
  reservationCode: string;
  maxDate: string;
  minDate: string;
  countryCode: string;
  streetNumber: string;
  streetName: string;
  cityName: string;
  postalCode: string;
  countryName: string;
  countryTwoLetterIsoCode: string;
  state: string;
  neighborhood: string;
  additionalAddressDetails: string;
  cityId?: number;
  stateId?: number;
  countryId?: number;
  latitude?: number;
  longitude?: number;
  languageIsoCode: string;
  externalRoomListDto:  ItemsResponse<ExternalRoomlistDto>;
}
