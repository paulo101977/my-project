import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { RoomListTopCardComponent } from 'app/room-list/shared/components/room-list-top-card/room-list-top-card.component';
import { createServiceStub, padStub } from '../../../../../../testing';
import { DateService } from 'app/shared/services/shared/date.service';
import { MOCK_externalRoomlistHeader } from 'app/room-list/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('RoomListTopCardComponent', () => {
  let component: RoomListTopCardComponent;
  let fixture: ComponentFixture<RoomListTopCardComponent>;

  const dateServiceStub = createServiceStub(DateService, {
    convertUniversalDateToViewFormat: () => '10/10/2019'
  });
  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateTestingModule ],
      declarations: [ RoomListTopCardComponent, padStub ],
      providers: [ dateServiceStub ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(RoomListTopCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
