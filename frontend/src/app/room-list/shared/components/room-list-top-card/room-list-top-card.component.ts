import { Component, Input, OnInit } from '@angular/core';
import { ExternalRoomlistHeaderDto } from 'app/room-list/shared/models/external-roomlist-header-dto';
import { DateService } from 'app/shared/services/shared/date.service';

@Component({
  selector: 'app-room-list-top-card',
  templateUrl: './room-list-top-card.component.html',
  styleUrls: ['./room-list-top-card.component.scss']
})
export class RoomListTopCardComponent implements OnInit {

  @Input() externalRoomlistHeader: ExternalRoomlistHeaderDto;
  constructor( private dateService: DateService) { }

  ngOnInit() {}

  getPeriod() {
    if (this.externalRoomlistHeader) {
      const dateBegin = this.dateService
        .convertUniversalDateToViewFormat(this.externalRoomlistHeader.minDate);
      const endDate = this.dateService
        .convertUniversalDateToViewFormat(this.externalRoomlistHeader.minDate);
      return { dateBegin, endDate };
    }
    return { dateBegin: '-', endDate: '-' };
  }
}
