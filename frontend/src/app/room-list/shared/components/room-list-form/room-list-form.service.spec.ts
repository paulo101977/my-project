import { TestBed } from '@angular/core/testing';
import { RoomListFormService } from './room-list-form.service';
import {configureTestSuite} from 'ng-bullet';
import {ReactiveFormsModule} from '@angular/forms';
import {DocumentTypeResource} from 'app/shared/resources/document-type/document-type.resource';
import {Observable, of} from 'rxjs';
import {ThexAddressApiService} from '@inovacaocmnet/thx-bifrost';

describe('RoomListFormService', () => {

  const documentTypeResourceStub: Partial<DocumentTypeResource> = {
    getAll(): Observable<any> {
      return of([]);
    }
  };

  const addressResourceStub: Partial<ThexAddressApiService> = {
    getNationalities(): Observable<any> {
      return of([]);
    }
  };

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [ ReactiveFormsModule ],
      providers: [
        {
          provide: DocumentTypeResource,
          useValue: documentTypeResourceStub
        },
        {
          provide: ThexAddressApiService,
          useValue: addressResourceStub
        },
      ]
    });
  });

  it('should be created', () => {
    const service: RoomListFormService = TestBed.get(RoomListFormService);
    expect(service).toBeTruthy();
  });
});
