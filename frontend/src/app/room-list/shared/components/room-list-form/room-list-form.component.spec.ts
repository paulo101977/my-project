// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { RoomListFormComponent } from './room-list-form.component';
// import { configureTestSuite } from 'ng-bullet';
// import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { createAccessorComponent, padStub } from '../../../../../../testing';
// import { FormsModule, ReactiveFormsModule} from '@angular/forms';
// import { RoomListFormService} from 'app/room-list/shared/components/room-list-form/room-list-form.service';
// import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
// import { SelectOptionItem } from '@inovacao-cmnet/thx-ui';
// import {ThexApiTestingModule} from '@inovacaocmnet/thx-bifrost';
// import {HttpClientTestingModule} from '@angular/common/http/testing';
// import {RouterTestingModule} from '@angular/router/testing';
//
// const thxInputText = createAccessorComponent('thx-input-text');
// const thxInputSelectGroup = createAccessorComponent('thx-input-select-group');
// const thxSelectV2 = createAccessorComponent('thx-select-v2');
// const thxTextareaV2 = createAccessorComponent('thx-textarea-v2');
//
// fdescribe('RoomListFormComponent', () => {
//   let component: RoomListFormComponent;
//   let fixture: ComponentFixture<RoomListFormComponent>;
//
//   const roomListServiceStub: Partial<RoomListFormService> = {
//     getNationalityList(): SelectOptionItem[] {
//       return [];
//     },
//     getDocumentTypeList(): SelectOptionItem[] {
//       return [];
//     }
//   };
//
//   configureTestSuite(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         TranslateTestingModule,
//         FormsModule,
//         ReactiveFormsModule,
//         HttpClientTestingModule,
//         ThexApiTestingModule,
//         RouterTestingModule
//       ],
//       declarations: [
//         RoomListFormComponent,
//         padStub,
//         thxInputText,
//         thxInputSelectGroup,
//         thxSelectV2,
//         thxTextareaV2,
//       ],
//       providers: [
//         {
//           provide: RoomListFormService,
//           useValue: roomListServiceStub
//         },
//         sharedServiceStub,
//       ],
//       schemas: [ NO_ERRORS_SCHEMA ]
//     });
//     fixture = TestBed.createComponent(RoomListFormComponent)
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
