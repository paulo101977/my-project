import { Injectable } from '@angular/core';
import { DocumentTypeResource } from 'app/shared/resources/document-type/document-type.resource';
import { toSelectOption, SelectOptionItem } from '@inovacao-cmnet/thx-ui';
import { Subscription } from 'rxjs';
import {pluck} from 'rxjs/operators';
import {ThexAddressApiService} from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: 'root'
})
export class RoomListFormService {

  private documentTypeSubscription: Subscription;
  private documentTypeList: SelectOptionItem[];
  private nationalityList: SelectOptionItem[];

  constructor( private documentTypeResource: DocumentTypeResource,
               private addressResource: ThexAddressApiService ) {
    this.loadDocumentTypes();
    this.loadNationalities();
  }


  public getDocumentTypeList(): SelectOptionItem[] {
    return this.documentTypeList;
  }

  public getNationalityList(): SelectOptionItem[] {
    return this.nationalityList;
  }

  private loadDocumentTypes() {
    this.documentTypeSubscription = this.documentTypeResource.getAll()
      .pipe( pluck(  'items') )
      .subscribe((list: any[]) => {
      this.documentTypeList = toSelectOption(list, 'name', 'id');
    });
  }

  private loadNationalities() {
    this.addressResource.getNationalities()
      .pipe(pluck('items'))
      .subscribe((items: any[]) => {
        this.nationalityList = toSelectOption(items, 'nationality', 'id');
      });
  }
}
