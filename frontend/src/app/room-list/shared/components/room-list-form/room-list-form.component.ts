import {AfterViewInit, ChangeDetectorRef, Component, DoCheck, Input, OnInit, ViewChild} from '@angular/core';
import {ButtonConfig, ButtonSize, ButtonType} from 'app/shared/models/button-config';
import {SharedService} from 'app/shared/services/shared/shared.service';
import {AddressV2Component} from 'app/shared/components/address-v2/address-v2.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SelectOptionItem} from '@inovacao-cmnet/thx-ui';
import {RoomListFormService} from 'app/room-list/shared/components/room-list-form/room-list-form.service';
import {ExternalRoomlistGuest} from 'app/room-list/shared/models/external-roomlist-guest';
import {RoomlistResource} from 'app/room-list/shared/services/roomlist.resource';
import {DateService} from 'app/shared/services/shared/date.service';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {SuccessError} from 'app/shared/models/success-error-enum';

@Component({
  selector: 'app-room-list-form',
  templateUrl: './room-list-form.component.html',
  styleUrls: ['./room-list-form.component.scss']
})
export class RoomListFormComponent implements OnInit, DoCheck, AfterViewInit {

  @ViewChild(AddressV2Component) addressComponent: AddressV2Component;

  @Input() title: string;
  @Input() expanded = false;
  @Input() guest: ExternalRoomlistGuest;

  public guestForm: FormGroup;

  // config
  public saveButtonConfig: ButtonConfig;

  // list
  public documentTypeList: SelectOptionItem[];
  public nationalityList: SelectOptionItem[];

  constructor( private cdr: ChangeDetectorRef,
               private sharedService: SharedService,
               private fb: FormBuilder,
               private roomListService: RoomListFormService,
               private roomlistResource: RoomlistResource,
               private dateService: DateService,
               private toasterService: ToasterEmitService) {
  }

  ngOnInit() {
    this.setForm();
    this.setButtonConfig();
  }

  ngAfterViewInit() {
    this.populateForm(this.guest);
    this.cdr.detectChanges();
  }

  ngDoCheck() {
    this.documentTypeList = this.roomListService.getDocumentTypeList();
    this.nationalityList = this.roomListService.getNationalityList();
  }

  private setForm() {
    this.guestForm = this.fb.group({
      fullName: [null, Validators.required ],
      document: null,
      email: [null, Validators.email],
      nationalityId: null,
      birthDate: null,
      age: null,
      obs: null
    });
    this.guestForm.valueChanges.subscribe( (values) => {
      this.saveButtonConfig = {...this.saveButtonConfig, isDisabled: this.guestForm.invalid };
      if (values.birthDate) {
        this.calculateAge(values.birthDate);
      }
    });
  }

  setButtonConfig() {
    this.saveButtonConfig = this.sharedService.getButtonConfig('roomlist-save',
      () => this.saveGuest(),
      'action.save',
      ButtonType.Primary,
      ButtonSize.Normal,
      this.guestForm.invalid );
  }

  saveGuest() {
    if ( this.addressComponent.addressForm.valid &&
      this.guestForm.valid ) {
      this.roomlistResource
        .saveRoomList(
          this.prepareDataToSave(
            {
              ...this.guestForm.value,
              address: this.addressComponent.addressForm.getRawValue()
            }),
          this.guest.guestReservationItemId )
        .subscribe( item => {
          this.toasterService.emitChange(SuccessError.success, 'variable.saveSuccessM');
      });
    }

  }

  private populateForm(guest: ExternalRoomlistGuest) {
    if (this.guestForm) {
      this.guestForm.patchValue({
        ...guest,
        document: {
          inputValue: guest.documentInformation,
          selectValue: guest.documentTypeId
        }
      });
    }
    if (this.addressComponent && this.addressComponent.addressForm) {
      this.addressComponent.addressForm.patchValue({
        ...guest,
        streetComplement: guest.additionalAddressDetails,
        subdivision: guest.cityName,
        division: guest.stateTwoLetterIsoCode
      });
    }
  }

  private prepareDataToSave(params: any) {
    if (params) {
      const {fullName, email, nationalityId, obs, birthDate, address } = params;
      const documentInformation = params.document.inputValue;
      const documentTypeId = params.document.selectValue;

      const { subdivision, countryCode, postalCode, streetComplement, streetNumber, division, neighborhood, streetName } = address;
      return {
        fullName,
        documentInformation,
        documentTypeId,
        email,
        nationalityId,
        birthDate,
        obs,
        streetNumber,
        neighborhood,
        streetName,
        additionalAddressDetails: streetComplement,
        postalCode,
        countryCode,
        subdivision,
        division,
      };
    }
  }

  private calculateAge(date: any) {
    const age = this.dateService.calculateAgeByDate(new Date(date));
    this.guestForm.get('age').setValue(age, {onlySelf: true, emitEvent: true});
  }
}
