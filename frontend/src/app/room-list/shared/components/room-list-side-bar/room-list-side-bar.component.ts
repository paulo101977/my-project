import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { ExternalRoomlistDto } from 'app/room-list/shared/models/external-roomlist-dto';
import { DateService } from 'app/shared/services/shared/date.service';
import { RoomlistCardSelectedService } from 'app/room-list/containers/share-room-list-page/roomlist-card-selected.service';

@Component({
  selector: 'app-room-list-side-bar',
  templateUrl: './room-list-side-bar.component.html',
  styleUrls: ['./room-list-side-bar.component.scss']
})
export class RoomListSideBarComponent implements OnInit, OnChanges {

  @Input() externalRoomlistItemList: ExternalRoomlistDto[];

  constructor(private dateService: DateService,
              private roomListService: RoomlistCardSelectedService ) { }

  ngOnInit() {}

  getPeriod(item: ExternalRoomlistDto) {
    if (item) {
      const beginDate = this.dateService.convertUniversalDateToViewFormat(item.estimatedArrivalDate);
      const endDate = this.dateService.convertUniversalDateToViewFormat(item.estimatedDepartureDate);
      return `${beginDate} - ${endDate}`;
    }
    return '--';
  }

  activateItem(item: ExternalRoomlistDto) {
    this.roomListService.setActivatedItem(item);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.externalRoomlistItemList) {
      this.roomListService.setList(changes.externalRoomlistItemList.currentValue);
    }
  }
}
