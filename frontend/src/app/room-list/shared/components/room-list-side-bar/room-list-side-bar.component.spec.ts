import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomListSideBarComponent } from './room-list-side-bar.component';
import { configureTestSuite } from 'ng-bullet';
import { createServiceStub, padStub } from '../../../../../../testing';
import { DateService } from 'app/shared/services/shared/date.service';
import { MOCK_externalRoomlistHeader, MOCK_externalRoomlistItemList } from 'app/room-list/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('RoomListSideBarComponent', () => {
  let component: RoomListSideBarComponent;
  let fixture: ComponentFixture<RoomListSideBarComponent>;
  const dateServiceStub = createServiceStub(DateService, {
    convertUniversalDateToViewFormat: () => '10/10/2019'
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomListSideBarComponent, padStub ],
      providers: [ dateServiceStub ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(RoomListSideBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get period when has item', () => {
    const item = MOCK_externalRoomlistItemList[0];

    const period = component.getPeriod(item);

    expect(period).toBe('10/10/2019 - 10/10/2019');
  });

  it('should get period when item null', () => {
    const period = component.getPeriod(null);

    expect(period).toBe('--');
  });
});
