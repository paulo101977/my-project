import { Injectable } from '@angular/core';
import { ThexApiService, TokenInterceptor, TokenManagerService } from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';
import { ExternalRoomlistHeaderDto } from 'app/room-list/shared/models/external-roomlist-header-dto';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { ExternalRoomlistGuest } from 'app/room-list/shared/models/external-roomlist-guest';
import { HttpHeaders } from '@angular/common/http';

@Injectable({ providedIn: 'root'})
export class RoomlistResource {
  constructor( private  thexApi: ThexApiService,
               private tokenInterceptor: TokenInterceptor,
               private tokenManager: TokenManagerService) { }

  public shareRoomList(reservationId: string, email: string) {
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.thexApi.post(`ExternalRegistration/reservationUid/${reservationId}/email/${email}`, null, {
      headers,
      responseType: 'text'
    });
  }

  public getRoomlistInfo(): Observable<ExternalRoomlistHeaderDto> {
    return this.thexApi.get('ExternalRegistration/roomList', this.getHeaders() );
  }

  public getRoomListGuests(reservationItemUID: string): Observable<ItemsResponse<ExternalRoomlistGuest>> {
    return this.thexApi.get(`ExternalRegistration/reservationItemUid/${reservationItemUID}/registrationList`, this.getHeaders());
  }

  public saveRoomList(guest: any, id: number) {
    return this.thexApi.put(`ExternalRegistration/${id}`, guest, this.getHeaders() );
  }

  private getHeaders() {
    const headers = new HttpHeaders().set('Authorization', `bearer ${this.tokenManager.getExternalToken()}` );
    this.tokenInterceptor.disableOnce();
    return { headers };
  }
}
