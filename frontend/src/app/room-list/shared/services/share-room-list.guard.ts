import {Inject, Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Route, Router, RouterStateSnapshot } from '@angular/router';
import { TokenManagerService, APP_NAME } from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: `root`
})
export class ShareRoomListGuard implements CanActivate  {
  constructor(private router: Router,
              private tokenManager: TokenManagerService,
              @Inject(APP_NAME) private appName ) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const token = route.queryParams.token;
    if (!this.tokenManager.isTokenExpired(token)) {
      this.tokenManager.setExternalToken(token);
      return true;
    }
    this.router.navigate(['room-list', 'expired', token]);
    return false;
  }

}
