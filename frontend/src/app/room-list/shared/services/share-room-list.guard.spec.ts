import { ShareRoomListGuard } from './share-room-list.guard';
import { configureTestSuite } from 'ng-bullet';
import { inject, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { configureAppName, TokenManagerService} from '@inovacaocmnet/thx-bifrost';


describe('ShareRoomListGuard', () => {

  const tokenManagerStub: Partial<TokenManagerService> = {
    isTokenExpired(token: string): boolean {
      return false;
    },
    setToken(appName: string, token: string): void {
    }
  };

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [
        ShareRoomListGuard,
        {
          provide: TokenManagerService,
          useValue: tokenManagerStub
        },
        configureAppName('pms')
      ]
    });
  });

  it('should ...', inject([ShareRoomListGuard], (guard: ShareRoomListGuard) => {
    expect(guard).toBeTruthy();
  }));
});
