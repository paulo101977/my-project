import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { BudgetCompareConfig } from '../../../shared/models/budget/budget-compare-config';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { ActivatedRoute } from '@angular/router';
import { ReasonCategoryEnum } from '../../../shared/models/reason/reason-category-enum';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ReasonResource } from '../../../shared/resources/reason/reason.resource';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../shared/models/button-config';

@Component({
  selector: 'selling-budget-modal',
  templateUrl: 'selling-budget-modal.component.html',
})
export class SellingBudgetModalComponent implements OnInit {
  @Input() budgetCompareConfig: BudgetCompareConfig;
  @Input() type: string;
  @Output() saveMoveRoom = new EventEmitter();
  @Output() closeModal = new EventEmitter();

  public moveRoomFormConfirmModal: FormGroup;
  public moveRoomreasonList: Array<SelectObjectOption>;
  public propertyId: number;

  // Buttons
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    private reasonResource: ReasonResource,
    private route: ActivatedRoute,
    private commomService: CommomService,
    private location: Location,
    private sharedService: SharedService,
  ) {}

  ngOnInit() {
    this.setVars();
    this.setModalForm();
    this.getMoveRoomList();
    this.setButtonConfig();
    this.setListener();
  }

  private setVars() {
    this.propertyId = this.route.snapshot.params.property;
  }

  private setModalForm() {
    this.moveRoomFormConfirmModal = this.formBuilder.group({
      reason: ['', [Validators.required]],
    });
  }

  private getMoveRoomList() {
    this.reasonResource.getMoveRoomList(this.propertyId, ReasonCategoryEnum.RoomTransference).subscribe(moveRoomReason => {
      this.moveRoomreasonList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        moveRoomReason,
        'id',
        'reasonName',
      );
    });
  }

  private setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      this.goBackPreviewPage,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'save',
      this.callMoveRoomEvent,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
  }

  private callMoveRoomEvent = () => {
    this.saveMoveRoom.emit(this.moveRoomFormConfirmModal.get('reason').value);
    this.moveRoomFormConfirmModal.get('reason').setValue(null);
  }

  private goBackPreviewPage = () => {
    this.closeModal.emit();
    this.moveRoomFormConfirmModal.get('reason').setValue(null);
  }

  private setListener() {
    this.moveRoomFormConfirmModal.get('reason').valueChanges.subscribe(() => {
      this.confirmButtonConfig.isDisabled = false;
    });
  }
}
