import { Component, Input } from '@angular/core';
import { BudgetCompareConfig } from '../../../shared/models/budget/budget-compare-config';

@Component({
  selector: 'budget-compare',
  templateUrl: 'budget-compare.component.html',
})
export class BudgetCompareComponent {
  @Input() budgetCompareConfig: BudgetCompareConfig;
  @Input() gridClassRightSide: string;
  @Input() gridClassLeftSide: string;
}
