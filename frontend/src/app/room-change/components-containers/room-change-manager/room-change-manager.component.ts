import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { AutocompleteConfig } from 'app/shared/models/autocomplete/autocomplete-config';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { GuestInRoom } from 'app/shared/models/room-change/guest-in-room';
import { StatusCheckInRooms } from 'app/shared/resources/status-checkin-room';
import { BudgetCompareConfig } from 'app/shared/models/budget/budget-compare-config';
import { AccomodationChange } from 'app/shared/models/accomodation-change';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { BudgetService } from 'app/shared/services/budget/budget.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import { RoomResource } from 'app/shared/resources/room/room.resource';
import { GuestResource } from 'app/shared/resources/guest/guest.resource';
import { BudgetResource } from 'app/shared/resources/budget/budget.resource';
import { ReservationItemResource } from 'app/shared/resources/reservation-item/reservation-item.resource';
import { PropertyDateService } from 'app/shared/services/shared/property-date.service';
import { CheckInRoomTypesDto } from 'app/shared/models/dto/checkin/checkin-room-types-dto';
import { CheckInNumUhsDto } from 'app/shared/models/dto/checkin/checkin-num-uhs-dto';
import { BedTypeService } from 'app/room-type/shared/services/bed-type/bed-type.service';
import { ReservationItemBudget } from 'app/shared/models/revenue-management/reservation-budget/reservation-item-budget';
import { AutocompleteComponent } from 'app/shared/components/autocomplete/autocomplete.component';

// TODO: make tests: [DEBIT: 7517]

@Component({
  selector: 'room-change-manager',
  templateUrl: './room-change-manager.component.html',
  styleUrls: ['./room-change-manager.component.css'],
})
export class RoomChangeManagerComponent implements OnInit {
  @ViewChild('autocompleteUh') autocompleteUh: AutocompleteComponent;
  public autocompleteConfig: AutocompleteConfig;
  private actualDate: string;
  private propertyId: number;
  public guestListInRoom: GuestInRoom[] = [];
  public guestInRoom: GuestInRoom;
  public budgetCompareConfig: BudgetCompareConfig;
  private accomodationChange: AccomodationChange;
  public roomTypeChosen: CheckInRoomTypesDto;
  public roomChosen: CheckInNumUhsDto;
  public roomTypeUhList: CheckInRoomTypesDto[] = [];
  public roomList: CheckInNumUhsDto[] = [];

  // fix const
  private readonly BACKSPACE = 8;

  // Select component
  public newOptionRoomTypeUhList: SelectObjectOption[] = [];
  public newOptionRoomNumberUhList: SelectObjectOption[] = [];

  // Buttons
  public cancelButtonConfig: ButtonConfig;
  public saveButtonConfig: ButtonConfig;

  // Modal
  public readonly SELLINGMODAL = 'sellingModal';
  public sellingModalType: string;



  constructor(
    public formBuilder: FormBuilder,
    private translateService: TranslateService,
    private location: Location,
    private route: ActivatedRoute,
    private checkinResource: CheckinResource,
    private sharedService: SharedService,
    private dateService: DateService,
    private roomResource: RoomResource,
    private commomService: CommomService,
    private guestResource: GuestResource,
    private toasterEmitService: ToasterEmitService,
    private budgetResource: BudgetResource,
    private modalEmitToggleService: ModalEmitToggleService,
    private budgetService: BudgetService,
    private reservationItemResource: ReservationItemResource,
    private propertyDateService: PropertyDateService,
    private bedTypeService: BedTypeService
  ) {}

  ngOnInit() {
    this.setVars();
    this.setAutoCompleteConfig();
    this.setButtonConfig();
    this.loadRoomTypes();
  }

  private setVars() {
    this.budgetCompareConfig = new BudgetCompareConfig();
    this.propertyId = this.route.snapshot.params.property;
    this.actualDate = this.propertyDateService.getTodayDatePropertyTimezone(this.propertyId);
    this.cleanCurrencySymbol();
  }

  private cleanCurrencySymbol() {
    const { budgetCompareConfig } = this;
    if ( budgetCompareConfig ) {
      budgetCompareConfig.actualCurrencySymbol = ' ';
      budgetCompareConfig.newCurrencySymbol = ' ';
    }
  }

  private loadRoomTypes() {
    this.newOptionRoomTypeUhList = [];
    this.checkinResource.getRoomTypesActive(this.propertyId, this.actualDate).subscribe(roomtypes => {
      this.roomTypeUhList = roomtypes.items;
      this.newOptionRoomTypeUhList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        roomtypes,
        'id',
        'abbreviation',
      );
    });
  }

  private setAutoCompleteConfig() {
    this.autocompleteConfig = new AutocompleteConfig();
    this.autocompleteConfig.callbackToSearch = this.searchRoom;
    this.autocompleteConfig.callbackToGetSelectedValue = this.getGuestList;
    this.autocompleteConfig.fieldObjectToDisplay = 'roomNumber';
    this.autocompleteConfig.placeholder = this.translateService.instant('label.placeholder.writeRoom');
  }

  public getGuestList = (room: StatusCheckInRooms) => {
    if (room) {
      this.guestResource.getGuestList(room.id, this.actualDate).subscribe(guestsInRoom => {
        if (guestsInRoom.items && guestsInRoom.items.length > 0) {
          this.guestListInRoom = guestsInRoom.items;
          this.guestInRoom = this.guestListInRoom[0];
          this.budgetCompareConfig.actualBudget = this.guestInRoom.currentBudget;
          this.budgetCompareConfig.actualCurrencyId = this.guestInRoom.currencyId;
          this.budgetCompareConfig.actualCurrencySymbol = this.guestInRoom.currencySymbol;
          this.roomTypeChosen = new CheckInRoomTypesDto();
          this.roomTypeChosen.id = room.roomTypeId;
        }
      });
    }
  }

  public keyUpEvent(event) {
    // const backspaceEvent = 8;
    if (event.keyCode === this.BACKSPACE) {
      this.setDefaultPageConfig();
    }
  }

  public setDefaultPageConfig() {
    this.guestListInRoom = [];
    this.guestInRoom = null;
    this.newOptionRoomNumberUhList = [];
    this.roomTypeChosen = null;
    this.roomChosen = null;
    this.budgetCompareConfig = new BudgetCompareConfig();
    this.autocompleteUh.text = '';

    this.cleanCurrencySymbol();
  }

  public searchRoom = data => {
    if ( data ) {
      const { query } = data;
      if ( query && typeof query === 'string' ) {
        this.roomResource
          .getSearchRooms(this.propertyId, query)
          .subscribe(({ items }) => {
            this.autocompleteConfig.resultList = [...items];
            if (items && items.length === 0) {
              this.guestListInRoom = [];
            }
          });
      }
    }
  }

  private setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      () => this.location.back(),
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.saveButtonConfig = this.sharedService.getButtonConfig(
      'save',
      this.saveUhMove,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  private saveUhMove = () => {
    if (this.budgetService.isUpSelling(this.budgetCompareConfig.actualBudget, this.budgetCompareConfig.newBudget)) {
      this.sellingModalType = 'up';
    }
    if (this.budgetService.isDownSelling(this.budgetCompareConfig.actualBudget, this.budgetCompareConfig.newBudget)) {
      this.sellingModalType = 'down';
    }
    if (this.budgetService.isSameSelling(this.budgetCompareConfig.actualBudget, this.budgetCompareConfig.newBudget)) {
      this.sellingModalType = 'same';
    }
    this.toggleModal();
  }

  public toggleModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.SELLINGMODAL);
  }

  public chooseRoomType(roomTypeId) {
    if (roomTypeId) {
      this.roomChosen = null;
      this.newOptionRoomNumberUhList = [];
      this.roomTypeChosen = this.roomTypeUhList.find(rt => rt.id == roomTypeId);

      let estimatedDepartureDate = this.dateService.removeTimeFromUniversalDate(this.guestInRoom.estimatedDepartureDate);
      const beginDate = this.dateService.convertStringToDate(this.actualDate, DateService.DATE_FORMAT_UNIVERSAL);
      const endDate = this.dateService.convertStringToDate(estimatedDepartureDate, DateService.DATE_FORMAT_UNIVERSAL);
      const dateListFromPeriod = this.dateService.convertPeriodToDateList(beginDate, endDate);
      const checkinDate = this.dateService.convertDateToString(dateListFromPeriod[0]);
      estimatedDepartureDate = this.dateService.convertDateToString(dateListFromPeriod[dateListFromPeriod.length - 1]);

      this.checkinResource
        .getAvailableRoomsByPeriod(roomTypeId, checkinDate, estimatedDepartureDate, this.propertyId)
        .subscribe(rooms => {
          if (rooms.items) {
            this.roomList = rooms.items;
            this.newOptionRoomNumberUhList = rooms.items.map(room => {
              return <SelectObjectOption> {
                key: room.id,
                value: `${room.roomNumber} - <span style="color: #${room.color}">${room.housekeepingStatusName}</span>`
              };
            });
            const reservationItemId = this.guestInRoom.reservationItemId.toString();
            this.budgetResource
              .getBudgetByRoomTypeAndRoom(this.propertyId, roomTypeId, reservationItemId, checkinDate, estimatedDepartureDate)
              .subscribe((reservationBudgetMoveUh: ReservationItemBudget) => {
                if (reservationBudgetMoveUh.commercialBudgetList &&
                  reservationBudgetMoveUh.commercialBudgetList.length > 0) {
                  const newBudget = reservationBudgetMoveUh.commercialBudgetList
                    .find(bud => bud.isMealPlanTypeDefault == true && !bud.ratePlanId);
                  this.budgetCompareConfig.newBudget = newBudget.totalBudget;
                  this.budgetCompareConfig.newCurrencyId = newBudget.currencyId;
                  this.budgetCompareConfig.newCurrencySymbol = newBudget.currencySymbol;
                }
              });
          }
        });
    }
    this.checkButton();
  }

  public chooseRoom(roomId) {
    this.roomChosen = this.roomList.find(r => r.id == roomId);
    this.checkButton();
  }

  public saveMoveGuestToAnotherRoom(reasonId) {
    this.accomodationChange = new AccomodationChange();
    this.accomodationChange.propertyId = this.propertyId;
    this.accomodationChange.reasonId = reasonId;
    this.accomodationChange.reservationItemId = this.guestInRoom.reservationItemId;
    this.accomodationChange.roomTypeId = this.roomTypeChosen.id;
    this.accomodationChange.roomId = this.roomChosen.id;
    this.toggleModal();

    this.reservationItemResource.setAccomodationChange(this.accomodationChange).subscribe(() => {
      this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant('alert.alertRoomMoved'));
      this.setDefaultPageConfig();
    });
  }

  public getBedType(bedType) {
    return this.bedTypeService.getInfoBedType(bedType.bedTypeId);
  }

  private checkButton() {
    this.saveButtonConfig.isDisabled = this.roomChosen == null ||
      this.roomTypeChosen == null;
  }
}
