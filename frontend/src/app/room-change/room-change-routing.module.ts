import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoomChangeManagerComponent } from './components-containers/room-change-manager/room-change-manager.component';

export const roomChangeRoutes: Routes = [{ path: '', component: RoomChangeManagerComponent }];

@NgModule({
  imports: [RouterModule.forChild(roomChangeRoutes)],
  exports: [RouterModule],
})
export class RoomChangeRoutingModule {}
