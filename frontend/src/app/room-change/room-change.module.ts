import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { RoomChangeManagerComponent } from './components-containers/room-change-manager/room-change-manager.component';
import { BudgetCompareComponent } from './components/budget-compare/budget-compare.component';
import { BudgetResource } from '../shared/resources/budget/budget.resource';
import { RoomChangeRoutingModule } from './room-change-routing.module';
import { SellingBudgetModalComponent } from './components/selling-budget-modal/selling-budget-modal.component';

@NgModule({
  imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule, RoomChangeRoutingModule],
  providers: [BudgetResource],
  declarations: [
    RoomChangeManagerComponent,
    BudgetCompareComponent,
    SellingBudgetModalComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class RoomChangeModule {}
