import * as moment from 'moment';
import { DateService } from '../../shared/services/shared/date.service';
export {};

declare global {
  interface Date {
    toStringUniversal(): string;
  }
}
Date.prototype.toStringUniversal = function() {
  return moment(this).format(DateService.DATE_FORMAT_UNIVERSAL);
};
