
import {tap} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

import { LoadPageService } from '../../shared/services/shared/load-page.service';
import { SuccessError } from '../../shared/models/success-error-enum';
import { ToasterEmitService } from '../../shared/services/shared/toaster-emit.service';
import { ErrorMessageHandlerAPI } from '../../shared/models/error-message/error-message-handler-api';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class ErrorHandlerInterceptor implements HttpInterceptor {
  constructor(private loadPage: LoadPageService,
              private toasterEmitService: ToasterEmitService,
              private interceptorHandler: InterceptorHandlerService) {}

  private readonly INTERCEPTOR_STORAGE_NAME = 'pms_error';
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.shouldRun()) {
      return next.handle(req);
    }

    return next.handle(req).pipe(tap(
      (event: HttpEvent<any>) => {
        // Do Nothing
      },
      (err: any) => {
        let errorMessage = 'commomData.errorMessage';
        if (err.status == 400 || err.status == 500 ) {
          errorMessage = new ErrorMessageHandlerAPI(err).getErrorMessage();
        } else if (err.status == 403 || err.status == 401) {
          errorMessage = 'alert.permissionDenied';
        }

        errorMessage && this.toasterEmitService.emitChange(SuccessError.error, errorMessage);
      },
    ));
  }

  public disable() {
    this.interceptorHandler.disable(this.INTERCEPTOR_STORAGE_NAME);
  }

  public disableOnce() {
    this.interceptorHandler.disableOnce(this.INTERCEPTOR_STORAGE_NAME);
  }

  public enable() {
    this.interceptorHandler.enable(this.INTERCEPTOR_STORAGE_NAME);
  }

  private shouldRun() {
    return this.interceptorHandler.shouldRun(this.INTERCEPTOR_STORAGE_NAME);
  }
}
