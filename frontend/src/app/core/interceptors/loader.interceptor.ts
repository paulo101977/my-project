
import {tap} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';

import { LoadPageService } from '../../shared/services/shared/load-page.service';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class LoaderInterceptor implements HttpInterceptor {
  private readonly INTERCEPTOR_STORAGE_NAME = 'pms_loader';
  constructor(private loadPage: LoadPageService, private interceptorHandler: InterceptorHandlerService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.shouldRun()) {
      return next.handle(req);
    }

    this.loadPage.show();

    return next.handle(req).pipe(tap(
      (event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          this.loadPage.hide();
        }
      },
      (err: any) => {
        this.loadPage.hide();
      },
    ));
  }

  public disable() {
    this.interceptorHandler.disable(this.INTERCEPTOR_STORAGE_NAME);
  }

  public disableOnce() {
    this.interceptorHandler.disableOnce(this.INTERCEPTOR_STORAGE_NAME);
  }

  public enable() {
    this.interceptorHandler.enable(this.INTERCEPTOR_STORAGE_NAME);
  }

  private shouldRun() {
    return this.interceptorHandler.shouldRun(this.INTERCEPTOR_STORAGE_NAME);
  }
}
