import { inject, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ApiCultureInterceptor } from './api-culture.interceptor';
import { RouterTestingModule } from '@angular/router/testing';
import { HTTP_INTERCEPTORS, HttpClient, HttpRequest } from '@angular/common/http';
import { User } from '../../shared/models/user';

xdescribe('ApiCultureInterceptor', () => {
  let interceptor: ApiCultureInterceptor;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ApiCultureInterceptor,
          multi: true,
        },
      ],
    });
    const user = new User();
    user.preferredCulture = 'en-us';

    spyOn<any>(localStorage, 'getItem').and.returnValue({preferredCulture: 'en-us'});
    interceptor = TestBed.get(ApiCultureInterceptor);
  });

  describe('intercept HTTP requests setParams', () => {
    it('should set params to any requests', inject(
      [HttpClient, HttpTestingController],
      (http: HttpClient, httpMock: HttpTestingController, requestApi: HttpRequest<any>) => {
        http.get('/api').subscribe(response => {
          expect(response).toBeTruthy();
        });

        /*
        * Issue do angular opened about queryParams on HttpTestingController https://github.com/angular/angular/issues/19974
        */
        // const formatedURL = encodeURI(
        // 'https://ths-dev.azurewebsites.net/api/properties/getall?culture=en-us');
        // const req = httpMock.expectOne((request) => request.urlWithParams === formatedURL);
        // expect(req.request.params.get('culture')).toEqual('en-us');
        expect(interceptor).toBeTruthy();
      }
    ));
  });

  afterEach(inject([HttpTestingController], (mock: HttpTestingController) => {
    mock.verify();
  }));
});
