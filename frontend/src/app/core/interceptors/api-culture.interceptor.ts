import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../shared/models/user';

@Injectable({providedIn: 'root'})

export class ApiCultureInterceptor implements HttpInterceptor {
  private readonly USER = 'user';

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const user = <User>JSON.parse(localStorage.getItem(this.USER));

    if (user && user.preferredCulture) {

      if (request.params.has('culture')) {
        request.params.delete('culture');
      }
      let httpParams = new HttpParams({fromString: request.params.toString()});
      httpParams = httpParams.append('culture', user.preferredCulture);
      request = request.clone({
        params: httpParams
      });
    }

    return next.handle(request);
  }
}
