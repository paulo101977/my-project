import { inject, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { LoaderInterceptor } from './loader.interceptor';
import { RouterTestingModule } from '@angular/router/testing';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('LoaderInterceptor', () => {
  let interceptor: LoaderInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: LoaderInterceptor,
          multi: true,
        },
        InterceptorHandlerService
      ],
    });

    interceptor = TestBed.get(LoaderInterceptor);
  });

  describe('intercept HTTP requests to Load Page', () => {
    it('should add Load Page to any requests', inject(
      [HttpClient, HttpTestingController],
      (http: HttpClient, mock: HttpTestingController) => {
        spyOn(interceptor['loadPage'], 'show');
        spyOn(interceptor['loadPage'], 'hide');

        http.get('/api').subscribe(response => {
          expect(response).toBeTruthy();
        });

        const request = mock.expectOne('/api');

        // before request
        expect(interceptor['loadPage'].show).toHaveBeenCalled();
        request.flush({ data: 'test' });
        // after request
        expect(interceptor['loadPage'].hide).toHaveBeenCalled();
        mock.verify();
      },
    ));
  });

  afterEach(inject([HttpTestingController], (mock: HttpTestingController) => {
    mock.verify();
  }));
});
