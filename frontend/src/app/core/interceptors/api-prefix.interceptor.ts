import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environment';


@Injectable({ providedIn: 'root' })
export class ApiPrefixInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.startsWith('User')) {
      request = request.clone({ url: environment.urlSuperAdmin + request.url });
    } else if (!request.url.startsWith('./')) {
      request = request.clone({ url: environment.urlBase + request.url });
    }
    return next.handle(request);
  }
}
