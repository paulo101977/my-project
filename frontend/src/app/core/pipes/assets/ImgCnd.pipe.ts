import { Pipe, PipeTransform } from '@angular/core';
import { AssetsService } from '@app/core/services/assets.service';

@Pipe({
  name: 'imgCdn'
})
export class ImgCndPipe implements PipeTransform {

  constructor(private assetsService: AssetsService) {}
  transform(value: any, args?: any): any {
    return this.assetsService.getImgUrlTo(value);
  }

}
