import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxToasterService } from '@inovacao-cmnet/thx-ui';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { httpInterceptorProviders } from '../http-interceptors';

import '../core/global/date-universal';
import { AssetsService } from '@app/core/services/assets.service';
import { ImgCndPipe } from './pipes/assets/ImgCnd.pipe';
import { ComponentsCdnPipe } from './pipes/assets/components-cdn.pipe';
import {
  AdminApiModule,
  configureApi,
  configureAppName,
  HigsApiModule,
  HousekeepingApiModule,
  PdvApiModule,
  PreferenceApiModule,
  RmsApiModule,
  ThexApiModule,
  TributesApiModule,
  SupportApiModule,
  ReportsApiModule
} from '@inovacaocmnet/thx-bifrost';
import { environment } from '@environment';

const ApiConfigProvider = configureApi(environment.apiConfig);

@NgModule({
  imports: [
    CommonModule,
    ThexApiModule,
    AdminApiModule,
    PdvApiModule,
    PreferenceApiModule,
    HousekeepingApiModule,
    RmsApiModule,
    TributesApiModule,
    HigsApiModule,
    SupportApiModule,
    ReportsApiModule
  ],
  declarations: [ImgCndPipe, ComponentsCdnPipe],
  providers: [
    httpInterceptorProviders,
    AssetsService,
    ApiConfigProvider,
    configureAppName('pms'),
    { provide: ToasterEmitService, useExisting: ThxToasterService }
  ],
  exports: [ImgCndPipe],
})
export class CoreModule {}
