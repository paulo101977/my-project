import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AssetsService {

  private readonly URL = 'https://cdn.totvscmnet-cloud.net';
  private readonly VERSION = '0.45.10';

  constructor() { }

  getComponentsUrlTo(item: string): string {
    return this.getUrlTo('components', item);
  }

  getImgUrlTo(item: string): string {
    return this.getUrlTo('img', item);
  }

  private getUrlTo(path: string, item: string): string {
    return `${this.URL}/${path}/thex/${this.VERSION}/${item}`;
  }


}
