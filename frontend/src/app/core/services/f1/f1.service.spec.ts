import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { ACTIVATED_ROUTE_SNAPSHOT, ActivatedRouteStub } from '../../../../../testing';

import { F1Service } from './f1.service';

describe('F1Service', () => {
  let service: F1Service;

  const snapshot: ActivatedRouteSnapshot = {
    ...ACTIVATED_ROUTE_SNAPSHOT,
    params: {property: 1},
  };

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [
        F1Service,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub }
      ]
    });

    service = TestBed.get(F1Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#init', () => {
    it('should set the route and router, and call the listenToPmsHelpKey', fakeAsync(() => {
      spyOn(service, 'listenToPmsHelpKey');

      service.init();
      tick();

      expect(service['router']).not.toBeUndefined();
      // TODO: uncomment when all pages are avaiable
      // expect(service.listenToPmsHelpKey).toHaveBeenCalled();
    }));
  });

  describe('#listenToPmsHelpKey', () => {
    // TODO whoever knows how to test it, create a test
    // it('should ', fakeAsync(() => {
    //   const f1 = new KeyboardEvent('keydown', {key: 'F1'});
    //   const howToSnapshot  = {
    //     ...snapshot,
    //     routeConfig: {
    //       ...snapshot.routeConfig,
    //       path: 'how-to'
    //     }
    //   };
    //
    //   service['route'] = TestBed.get(ActivatedRoute);
    //   service['router'] = TestBed.get(Router);
    //
    //   spyOn(service['router'], 'navigate');
    //   service['route'].snapshot = {...snapshot};
    //   service['route'].snapshot.children.push({...snapshot});
    //   service['route'].snapshot.children[0].children.push(howToSnapshot);
    //
    //   document.dispatchEvent(f1);
    //   tick(301);
    //
    //   expect(service['router'].navigate).toHaveBeenCalled();
    // }));
  });
});
