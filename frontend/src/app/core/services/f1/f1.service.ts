import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { fromEvent, Observable, pipe } from 'rxjs';
import { filter, map, tap, throttleTime } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class F1Service {
  private router: Router;

  constructor(
    private injector: Injector
  ) {}

  public init(): Promise<any> {
    return new Promise((resolve) => {
      this.router = this.injector.get(Router);
      this.listenToPmsHelpKey();
      resolve();
    });
  }

  public listenToPmsHelpKey() {
    this.listenKeyDown()
      .pipe(
        this.preventUnnecessaryNavigation(),
      )
      .subscribe((property) => {
        this.router.navigate(['/p', property, 'how-to']);
      });
  }

  public listenKeyDown(): Observable<string> {
    return fromEvent(document, 'keydown')
      .pipe(
        this.handleF1(),
        this.getProperty(),
      );
  }

  private handleF1() {
    return pipe(
      filter((event: KeyboardEvent) => event.key == 'F1'),
      tap(event => {
        event.stopPropagation();
        event.preventDefault();
      }),
      throttleTime(300),
    );
  }

  private getProperty() {
    return pipe(
      map(() => this.router.url),
      filter((url: string) => url.includes('/p/')),
      map((url: string) => url.replace(/.*(\/p\/)(\d+)(.*)*/ig, '$2')),
      filter((property: string) => property != null),
    );
  }

  private preventUnnecessaryNavigation() {
    return pipe(
      filter(() => {
        return !this.router.url.includes('how-to');
      })
    );
  }
}
