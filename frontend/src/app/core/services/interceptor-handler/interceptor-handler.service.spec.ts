import { TestBed } from '@angular/core/testing';
import { InterceptorDisabledState } from 'app/core/models/interceptor-disabled-state.enum';
import { InterceptorHandlerService } from './interceptor-handler.service';

describe('InspectorHandler', () => {
  let service: InterceptorHandlerService;

  const getInterceptorStorage = (): any => {
    return service['storage'];
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});

    service = TestBed.get(InterceptorHandlerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should set the interceptor as enabled', () => {
    service.enable('test');
    const interceptorStorage = getInterceptorStorage();
    expect(interceptorStorage.test.disabled).toEqual(InterceptorDisabledState.Enabled);
  });

  it('should set the interceptor as disabled', () => {
    service.disable('test');
    const interceptorStorage = getInterceptorStorage();
    expect(interceptorStorage.test.disabled).toEqual(InterceptorDisabledState.Disabled);
  });

  it('should set the interceptor as disabled once', () => {
    service.disableOnce('test');
    const interceptorStorage = getInterceptorStorage();
    expect(interceptorStorage.test.disabled).toEqual(InterceptorDisabledState.DisabledOnce);
  });

  describe('#shouldRun', () => {
    it('should return true if not defined', () => {
      const shouldRun = service.shouldRun('test');
      expect(shouldRun).toEqual(true);
    });

    it('should return true if not disabled', () => {
      const interceptorName = 'test';
      service.enable(interceptorName);
      const shouldRun = service.shouldRun(interceptorName);
      expect(shouldRun).toEqual(true);
    });

    it('should return false if disabled', () => {
      const interceptorName = 'test';
      service.disable(interceptorName);
      const shouldRun = service.shouldRun(interceptorName);
      expect(shouldRun).toEqual(false);
    });

    it('should return false and enable the interceptor if disabled once', () => {
      const interceptorName = 'test';
      service.disableOnce(interceptorName);
      const shouldRun = service.shouldRun(interceptorName);
      const interceptorStorage = getInterceptorStorage();
      expect(shouldRun).toEqual(false);
      expect(interceptorStorage[interceptorName].disabled).toEqual('false');
    });
  });
});
