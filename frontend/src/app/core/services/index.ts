import { createServiceStub } from '../../../../testing';
import { AssetsService } from 'app/core/services/assets.service';

export const assetsServiceStub = createServiceStub(AssetsService, {
    getComponentsUrlTo(item: string): string {
        return '';
    },

    getImgUrlTo(item: string): string {
        return '';
    }
});
