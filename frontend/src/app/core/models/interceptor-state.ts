import { InterceptorDisabledState } from './interceptor-disabled-state.enum';

export class InterceptorState {
  disabled: InterceptorDisabledState;
}
