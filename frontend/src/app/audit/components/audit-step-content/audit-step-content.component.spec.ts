import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditStepContentComponent } from './audit-step-content.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CoreModule } from '@app/core/core.module';

describe('AuditStepContentComponent', () => {
  let component: AuditStepContentComponent;
  let fixture: ComponentFixture<AuditStepContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuditStepContentComponent],
      imports: [TranslateModule.forRoot(), RouterTestingModule, HttpClientTestingModule, CoreModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditStepContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
