import { DateService } from 'app/shared/services/shared/date.service';
import { ParameterTypeEnum } from './../../../shared/models/parameter/pameterType.enum';
import { SessionParameterService } from './../../../shared/services/parameter/session-parameter.service';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuditResource } from '../../../shared/resources/audit/audit.resource';
import { AuditStepStatusEnum } from '../../models/audit-step-status.enum';
import { Parameter } from '../../../shared/models/parameter';
import { TranslateService } from '@ngx-translate/core';
import { AuditStepEnum } from '../../models/audit-step-enum';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';

@Component({
  selector: 'app-audit-step-content',
  templateUrl: './audit-step-content.component.html',
  styleUrls: ['./audit-step-content.component.css'],
})
export class AuditStepContentComponent implements OnInit, OnChanges {
  public readonly CONFIRM_DAILY_LAUNCH: 'confirm-daily-launch-modal';
  @Input() public auditAvailable: boolean;
  @Input() public timeForAudit: number;
  @Input() public currentDate: Date;
  @Input() public newDate: any;

  @Output() public nextStep: EventEmitter<void> = new EventEmitter<void>();

  @Output() public stepStatusChange: EventEmitter<AuditStepStatusEnum> = new EventEmitter<AuditStepStatusEnum>();
  private _stepStatus: AuditStepStatusEnum;

  @Input()
  public get stepStatus() {
    return this._stepStatus;
  }

  public set stepStatus(status: AuditStepStatusEnum) {
    this._stepStatus = status;
    this.updateStatus(status);
    this.stepStatusChange.emit(this.stepStatus);
  }

  private _currentStep: number;

  public get currentStep() {
    return this._currentStep;
  }

  @Input()
  public set currentStep(step: number) {
    this.updateStep(step);
    this._currentStep = step;
  }

  public auditStepErrorList: any[];
  public stepStatusEnumRef = AuditStepStatusEnum;
  public propertyId: number;

  // Attributes used to control elements when the status changes
  public stepTitle: string;
  public progressbarDescription: string;
  public progressbarBgColor: string;
  public progressbarTextColor: string;
  public progressbarPercentual: number;
  public startButtonLabel: string;

  constructor(
    public route: ActivatedRoute,
    public auditResource: AuditResource,
    public translateService: TranslateService,
    public dateService: DateService,
    public sessionParameterService: SessionParameterService,
    private modalToggle: ModalEmitToggleService) {}

  ngOnInit() {
    this.auditStepErrorList = [];
    this.propertyId = this.route.snapshot.params.property;
  }

  ngOnChanges() {
    this.autoStart();
  }

  autoStart() {
    setTimeout(() => {
      if (this.stepStatus == AuditStepStatusEnum.IDLE && this.currentStep > 0) {
        this.start();
      }
    }, 500);
  }

  start() {
    if (!this.auditAvailable) {
      this.modalToggle.emitOpenWithRef(this.CONFIRM_DAILY_LAUNCH);
    } else {
      this.startAudit();
    }
  }

  private startAudit() {
    this.stepStatus = AuditStepStatusEnum.PROCESSING;
    this.auditResource.startAuditProcess(this.propertyId).subscribe(
      data => {
        if (data.propertyAuditProcessStepErrorList && data.propertyAuditProcessStepErrorList.length > 0) {
          this.auditStepErrorList = data.propertyAuditProcessStepErrorList;
          for (const stepError of this.auditStepErrorList) {
            stepError.auditStatus = this.translateService.instant('label.problemFound');
          }
          this.stepStatus = AuditStepStatusEnum.PENDING;
        } else {
          if (this.currentStep == AuditStepEnum.UPDATE_DATE) {
            this.currentDate = this.newDate;
            const systemDateParam = this.sessionParameterService.getParameter(this.propertyId, ParameterTypeEnum.SistemDate);
            systemDateParam.propertyParameterValue = this.dateService.removeTimeFromUniversalDate(this.newDate);
            this.sessionParameterService.updateParameterOnList(this.propertyId, systemDateParam);
          }

          this.stepStatus = AuditStepStatusEnum.CONCLUDED;

          if (this.auditAvailable && this.currentStep < AuditStepEnum.UPDATE_STATUS_MAINTENACE) {
            this.next();
          }
        }
      },
      error => {
        this.stepStatus = AuditStepStatusEnum.PENDING;
      },
    );
  }

  next() {
    this.nextStep.emit();
  }

  private updateStatus(value: AuditStepStatusEnum) {
    switch (value) {
      case AuditStepStatusEnum.IDLE:
        this.progressbarDescription = 'label.readyToStart';
        this.progressbarBgColor = 'blue-light';
        this.progressbarTextColor = 'blue';
        this.progressbarPercentual = 0;
        break;
      case AuditStepStatusEnum.PROCESSING:
        this.progressbarDescription = 'label.processing';
        this.progressbarBgColor = 'blue';
        this.progressbarTextColor = 'white';
        this.progressbarPercentual = 0;
        break;
      case AuditStepStatusEnum.PENDING:
        this.progressbarDescription = 'variable.taskCompleteProblemsFound';
        this.progressbarBgColor = 'orange';
        this.progressbarTextColor = 'white';
        this.progressbarPercentual = null;
        break;
      case AuditStepStatusEnum.CONCLUDED:
        this.progressbarDescription = 'label.taskCompleted';
        this.progressbarBgColor = 'green';
        this.progressbarTextColor = 'white';
        this.progressbarPercentual = 100;
        break;
    }

    this.startButtonLabel = this.stepStatus == AuditStepStatusEnum.IDLE ? 'action.start' : 'action.reprocess';
  }

  private updateStep(step: number) {
    switch (step) {
      case AuditStepEnum.CAST_RATES:
        this.stepTitle = 'title.castRate';
        break;
      case AuditStepEnum.UPDATE_DATE:
        this.stepTitle = 'title.updateDate';
        break;
      case AuditStepEnum.NO_SHOW:
        this.stepTitle = 'title.noShow';
        break;
      case AuditStepEnum.UNEXPECTED_ROOM_PERMANENCY:
        this.stepTitle = 'title.unespectedRoomPermanency';
        break;
      case AuditStepEnum.UPDATE_STATUS_MAINTENACE:
        this.stepTitle = 'title.updateStatusMaintenaceAndHouseKeeping';
        break;
    }
  }

  public cancelDailyLaunch() {
    this.modalToggle.emitCloseWithRef(this.CONFIRM_DAILY_LAUNCH);
  }

  public confirmDailyLaunch() {
    this.modalToggle.emitCloseWithRef(this.CONFIRM_DAILY_LAUNCH);
    this.startAudit();
  }
}
