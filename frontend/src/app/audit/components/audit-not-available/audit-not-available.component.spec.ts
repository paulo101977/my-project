import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditNotAvailableComponent } from './audit-not-available.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';

describe('AuditNotAvailableComponent', () => {
  let component: AuditNotAvailableComponent;
  let fixture: ComponentFixture<AuditNotAvailableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuditNotAvailableComponent],
      imports: [TranslateModule.forRoot(), RouterTestingModule, CoreModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditNotAvailableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
