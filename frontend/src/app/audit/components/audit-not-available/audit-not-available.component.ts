
import {interval as observableInterval,  Observable } from 'rxjs';

import { finalize, map, take } from 'rxjs/operators';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';




@Component({
  selector: 'app-audit-not-available',
  templateUrl: './audit-not-available.component.html',
  styleUrls: ['./audit-not-available.component.css'],
})
export class AuditNotAvailableComponent implements OnInit, OnChanges {
  @Input() timeForAudit: number;
  @Output() finish: EventEmitter<void> = new EventEmitter<void>();
  timeLeft: string;

  constructor(public router: Router) {}

  ngOnInit() {
    this.updateTimer();
  }

  ngOnChanges() {
    this.updateTimer();
  }

  updateTimer() {
    if (this.timeForAudit) {
      const time = this.timeForAudit;

      observableInterval(1000)
        .pipe(
          take(time),
          finalize(() => this.finish.emit()),
          map(v => time - 1 - v)
        )
        .subscribe(v => (this.timeLeft = moment.utc(v * 1000).format('LTS')));
    }
  }
}
