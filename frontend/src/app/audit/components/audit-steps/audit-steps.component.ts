import { Component, Input, OnChanges } from '@angular/core';
import { AuditStep } from '../../models/audit-step';
import { AuditStepStatusEnum } from '../../models/audit-step-status.enum';

@Component({
  selector: 'app-audit-steps',
  templateUrl: './audit-steps.component.html',
  styleUrls: ['./audit-steps.component.css'],
})
export class AuditStepsComponent implements OnChanges {
  @Input() steps: AuditStep[];
  @Input() currentStep: number;
  @Input() currentStatus: AuditStepStatusEnum;

  constructor() {}

  ngOnChanges() {
    this.updateStepStatus();
  }

  updateStepStatus() {
    if (this.currentStep >= 0 && this.steps && this.steps.length > 0) {
      for (let i = this.currentStep - 1; i >= 0; i--) {
        this.setStepStatus(this.steps[i], AuditStepStatusEnum.CONCLUDED);
      }

      if (this.currentStatus >= 0) {
        this.setStepStatus(this.steps[this.currentStep], this.currentStatus);
      }
    }
  }

  setStepStatus(step: AuditStep, status: AuditStepStatusEnum) {
    switch (status) {
      case AuditStepStatusEnum.CONCLUDED:
        step.color = 'green';
        step.icon = 'icon-check-ok';
        step.description = 'label.complete';
        break;
      case AuditStepStatusEnum.PROCESSING:
        step.color = 'blue';
        step.icon = 'icon-rotate-right';
        step.description = 'label.processing';
        break;
      case AuditStepStatusEnum.PENDING:
        step.color = 'orange';
        step.icon = 'icon-incomplete';
        step.description = 'label.pending';
        break;
      case AuditStepStatusEnum.IDLE:
        step.color = null;
    }
  }
}
