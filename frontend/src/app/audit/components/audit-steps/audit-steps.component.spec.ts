import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditStepsComponent } from './audit-steps.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('AuditStepsComponent', () => {
  let component: AuditStepsComponent;
  let fixture: ComponentFixture<AuditStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuditStepsComponent],
      imports: [TranslateModule.forRoot(), RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
