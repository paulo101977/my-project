import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditErrorsComponent } from './audit-errors.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MomentModule } from 'ngx-moment';
import { CoreModule } from '@app/core/core.module';

describe('AuditErrorsComponent', () => {
  let component: AuditErrorsComponent;
  let fixture: ComponentFixture<AuditErrorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuditErrorsComponent],
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, MomentModule, CoreModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditErrorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
