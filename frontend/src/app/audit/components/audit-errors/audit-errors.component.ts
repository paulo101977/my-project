import { Component, Input } from '@angular/core';
import { AuditStepErrorDto } from '../../models/audit-step-error-dto';
import { TranslateService } from '@ngx-translate/core';
import { ColorsService } from '../../../shared/services/color/colors.service';

@Component({
  selector: 'app-audit-errors',
  templateUrl: './audit-errors.component.html',
  styleUrls: ['./audit-errors.component.css'],
  providers: [ColorsService],
})
export class AuditErrorsComponent {
  public columns: any[];
  private _auditStepErrorList: AuditStepErrorDto[];

  get auditStepErrorList(): AuditStepErrorDto[] {
    return this._auditStepErrorList;
  }

  @Input()
  set auditStepErrorList(value: AuditStepErrorDto[]) {
    this._auditStepErrorList = value;
  }

  constructor(public translateService: TranslateService, public colorService: ColorsService) {}

  getColorByBgColor(bgColor) {
    return this.colorService.getFontBasedOnColor(bgColor);
  }
}
