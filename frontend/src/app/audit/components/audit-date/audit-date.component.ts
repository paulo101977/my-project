import { Component, Input, OnInit } from '@angular/core';
import { AuditStepStatusEnum } from '../../models/audit-step-status.enum';

@Component({
  selector: 'app-audit-date',
  templateUrl: './audit-date.component.html',
  styleUrls: ['./audit-date.component.css'],
})
export class AuditDateComponent implements OnInit {
  @Input() public currentDate: Date;
  @Input() public newDate: Date;
  @Input() public currentStatus: AuditStepStatusEnum;

  public stepStatusEnumReference = AuditStepStatusEnum; // used in the page

  constructor() {}

  ngOnInit() {}
}
