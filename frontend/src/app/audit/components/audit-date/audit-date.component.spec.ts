import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DateFormatPipe } from 'ngx-moment';
import { AuditDateComponent } from './audit-date.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@app/shared/shared.module';
import { CoreModule } from '@app/core/core.module';

describe('AuditDateComponent', () => {
  let component: AuditDateComponent;
  let fixture: ComponentFixture<AuditDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot() , CoreModule],
      declarations: [AuditDateComponent, DateFormatPipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
