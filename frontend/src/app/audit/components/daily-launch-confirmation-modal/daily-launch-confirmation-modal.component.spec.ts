import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyLaunchConfirmationModalComponent } from './daily-launch-confirmation-modal.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DateService } from 'app/shared/services/shared/date.service';
import { Observable, of } from 'rxjs';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('DailyLaunchConfirmationModalComponent', () => {
  let component: DailyLaunchConfirmationModalComponent;
  let fixture: ComponentFixture<DailyLaunchConfirmationModalComponent>;

  const dateServiceStub = {
    getSystemDate: (p) => new Date
  };

  const translateServiceStub = {
    instant: (p) => 'text',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, TranslateTestingModule],
      declarations: [DailyLaunchConfirmationModalComponent],
      providers: [{
        provide: DateService,
        useValue: dateServiceStub
      },
        {
          provide: TranslateService,
          useValue: translateServiceStub
        }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyLaunchConfirmationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
