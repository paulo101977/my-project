import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DateService } from 'app/shared/services/shared/date.service';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-daily-launch-confirmation-modal',
  templateUrl: './daily-launch-confirmation-modal.component.html',
  styleUrls: ['./daily-launch-confirmation-modal.component.css']
})
export class DailyLaunchConfirmationModalComponent implements OnInit {

  @Input() modalId: string;
  @Input() propertyId: number;
  @Output() cancelClick = new EventEmitter();
  @Output() confirmClick = new EventEmitter();

  public dates: any;
  public textConfirmStr: string;
  public textConfirm: string;

  constructor(private dateService: DateService, private translateService: TranslateService) { }

  ngOnInit() {
    this.dates = this.prepareDates();
    this.textConfirmStr = this.translateService.instant('label.confirm');
  }

  private prepareDates() {
    const myToday = this.dateService.getSystemDate(this.propertyId);
    const today = moment(myToday).format('L');
    const yesterday = moment(myToday).add(-1, 'days').format('L');
    return { yesterday, today };
  }

  cancel() {
    this.cancelClick.emit();
  }

  confirm() {
    this.confirmClick.emit();
  }

  canConfirm() {
    return this.textConfirm === this.textConfirmStr;
  }
}
