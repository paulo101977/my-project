import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditContainerComponent } from './audit-container.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('AuditContainerComponent', () => {
  let component: AuditContainerComponent;
  let fixture: ComponentFixture<AuditContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuditContainerComponent],
      imports: [TranslateModule.forRoot(), RouterTestingModule, HttpClientTestingModule],
      providers: [ InterceptorHandlerService ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
