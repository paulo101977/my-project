import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuditResource } from '../../../shared/resources/audit/audit.resource';
import { AuditStep } from '../../models/audit-step';
import { AuditStepStatusEnum } from '../../models/audit-step-status.enum';
import { DateService } from '../../../shared/services/shared/date.service';

@Component({
  selector: 'app-audit-container',
  templateUrl: './audit-container.component.html',
  styleUrls: ['./audit-container.component.css'],
})
export class AuditContainerComponent implements OnInit {
  public propertyId: number;

  public auditAvailable: boolean;
  public timeForAudit: number;
  public currentDate: Date;
  public newDate: Date;

  public auditStep: number;
  public auditStepStatus: AuditStepStatusEnum;
  public steps: AuditStep[];

  constructor(public route: ActivatedRoute, public dateService: DateService, public auditResource: AuditResource) {}

  ngOnInit() {
    this.setVars();
  }

  setVars() {
    this.propertyId = this.route.snapshot.params.property;
    this.getTimeForAudit();
  }

  setSteps() {
    this.steps = [
      {
        color: 'grey-submarine',
        title: 'title.unespectedRoomPermanency',
        description: 'label.waiting',
        icon: 'icon-unexpected-room-permanency',
      },
    ];

    if (this.auditAvailable) {
      this.steps = this.steps.concat([
        {
          color: null,
          title: 'title.castingRates',
          description: 'label.waiting',
          icon: 'icon-rate',
        },
        {
          color: 'grey-submarine',
          title: 'title.updatingDates',
          description: 'label.waiting',
          icon: 'icon-change-date',
        },
        {
          color: 'grey-submarine',
          title: 'title.noShow',
          description: 'label.waiting',
          icon: 'icon-block',
        },
        {
          color: 'grey-submarine',
          title: 'title.updateStatusMaintenaceAndHouseKeeping',
          description: 'label.waiting',
          icon: 'icon-swap-horiz',
        },
      ]);
    } else {
      this.steps = [
        {
          color: null,
          title: 'title.castingRates',
          description: 'label.waiting',
          icon: 'icon-rate',
        }
      ];
    }
  }

  getTimeForAudit() {
    this.auditResource.getTimeForAudit(this.propertyId).subscribe(data => {
      this.auditAvailable = data.propertyAuditProcessIsAvailable;
      this.currentDate = data.currentSystemDate;
      this.newDate = data.nextSystemDate;

      if (this.auditAvailable === false) {
        this.timeForAudit = this.dateService.convertTimeToSeconds(null, data.hour, data.minutes);
      }

      this.getAuditStep();
    });
  }

  getAuditStep() {
    this.auditResource.getAuditStep(this.propertyId).subscribe(data => {
      this.setSteps();
      this.auditStepStatus = AuditStepStatusEnum.IDLE;
      this.auditStep = data.stepNumber;
    });
  }
}
