import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuditContainerComponent } from './components/audit-container/audit-container.component';

export const routes: Routes = [{ path: '', component: AuditContainerComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuditRoutingModule {}
