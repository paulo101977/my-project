export class AuditStepErrorDto {
  propertyAuditProcessStepId: string;
  description: string;
  reservationItemId: number;
  reservationItemCode: string;
  reservationItemStatusId: number;
  reservationItemStatusName: string;
  estimatedArrivalDate: string;
  estimatedDepartureDate: string;
  receivedRoomTypeId: number;
  roomTypeName: string;
  roomId: number;
  roomNumber: string;
  guestName: string;
  housekeepingStatusId: number;
  housekeepingStatusName: string;
  housekeepingStatusColor: string;
  housekeepingStatusPropertyId: string;
  housekeepingStatusPropertyName: string;
  propertyId: number;
  roomStatusName: string;
  auditStatus: string;
  reservationItemStatusColor: any;
}
