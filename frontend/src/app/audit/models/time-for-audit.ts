export class TimeForAudit {
  hour: number;
  minutes: number;
  propertyAuditProcessIsAvailable: boolean;
  currentSystemDate: Date;
  nextSystemDate: Date;
}
