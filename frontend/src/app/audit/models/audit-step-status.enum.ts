export enum AuditStepStatusEnum {
  IDLE,
  PROCESSING,
  PENDING,
  CONCLUDED,
}
