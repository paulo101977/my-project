export class AuditStepProcessDto {
  id: string;
  propertyAuditProcessId: string;
  startDate: string;
  endDate: string;
  description: string;
  rowTotal: number;
  currentRow: number;
  propertyId: number;
  auditStepTypeId: number;
  tenantId: string;
  propertyAuditProcessStepErrorList: AuditStepProcessDto[];
}
