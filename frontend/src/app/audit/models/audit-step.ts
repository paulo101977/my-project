import { SafeResourceUrl } from '@angular/platform-browser';
import { AuditStepStatusEnum } from './audit-step-status.enum';

export class AuditStep {
  color: string;
  title: string;
  description: string;
  icon?: SafeResourceUrl;
  status?: AuditStepStatusEnum;
}
