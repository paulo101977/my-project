import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuditRoutingModule } from './audit-routing.module';
import { AuditContainerComponent } from './components/audit-container/audit-container.component';
import { SharedModule } from '../shared/shared.module';
import { AuditNotAvailableComponent } from './components/audit-not-available/audit-not-available.component';
import { AuditStepContentComponent } from './components/audit-step-content/audit-step-content.component';
import { AuditStepsComponent } from './components/audit-steps/audit-steps.component';
import { AuditErrorsComponent } from './components/audit-errors/audit-errors.component';
import { AuditDateComponent } from './components/audit-date/audit-date.component';
import { WizardModule } from '../shared/components/wizard/wizard.module';
import {
  DailyLaunchConfirmationModalComponent
} from 'app/audit/components/daily-launch-confirmation-modal/daily-launch-confirmation-modal.component';

@NgModule({
  imports: [CommonModule, AuditRoutingModule, SharedModule, WizardModule],
  declarations: [
    AuditContainerComponent,
    AuditNotAvailableComponent,
    AuditStepContentComponent,
    AuditStepsComponent,
    AuditErrorsComponent,
    AuditDateComponent,
    DailyLaunchConfirmationModalComponent
  ],
})
export class AuditModule {}
