import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonConfig, ButtonType } from '../../../../shared/models/button-config';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { PropertyGuestTypeDto } from '../../../../shared/models/dto/property-guest-type-dto';

@Component({
  selector: 'guest-type-delete',
  templateUrl: './guest-type-delete.component.html'
})
export class GuestTypeDeleteComponent implements OnInit {

  @Input() guestTypeToBeDeleted: PropertyGuestTypeDto;
  @Output() close = new EventEmitter();
  @Output() confirm = new EventEmitter();

  // Buttons dependencies
  public buttonCancelDeleteModal: ButtonConfig;
  public buttonConfirmDeleteModal: ButtonConfig;

  constructor(private sharedService: SharedService) {
  }

  ngOnInit() {
    this.setButtonsConfig();
  }

  private setButtonsConfig() {
    this.buttonCancelDeleteModal = this.sharedService.getButtonConfig(
      'cancel-modal-delete-guest-type',
      this.cancelDelete,
      'commomData.cancel',
      ButtonType.Secondary
    );

    this.buttonConfirmDeleteModal = this.sharedService.getButtonConfig(
      'save-modal-delete-guest-type',
      this.confirmDelete,
      'commomData.confirm',
      ButtonType.Primary
    );
  }

  private cancelDelete = () => {
    this.close.emit();
  }

  private confirmDelete = () => {
    this.close.emit();
    this.confirm.emit(this.guestTypeToBeDeleted);
  }

}
