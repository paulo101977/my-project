import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestTypeDeleteComponent } from './guest-type-delete.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../../shared/shared.module';

describe('GuestTypeDeleteComponent', () => {
  let component: GuestTypeDeleteComponent;
  let fixture: ComponentFixture<GuestTypeDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        SharedModule
      ],
      declarations: [ GuestTypeDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestTypeDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
