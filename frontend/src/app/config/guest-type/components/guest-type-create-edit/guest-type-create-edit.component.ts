import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { PropertyGuestTypeDto } from '../../../../shared/models/dto/property-guest-type-dto';
import { ButtonConfig, ButtonType } from '../../../../shared/models/button-config';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'guest-type-create-edit',
  templateUrl: './guest-type-create-edit.component.html'
})
export class GuestTypeCreateEditComponent implements OnInit, OnChanges {

  @Input() guestTypeToEdit: PropertyGuestTypeDto;
  @Output() cancel = new EventEmitter();
  @Output() confirm = new EventEmitter();

  private propertyId: number;

  // Form dependencies
  public modalForm: FormGroup;

  // Buttons dependencies
  public buttonCancelModal: ButtonConfig;
  public buttonConfirmModal: ButtonConfig;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private sharedService: SharedService) {
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setFormConfig();
    this.setButtonsConfig();
  }

  ngOnChanges() {
    this.setFormConfig();
    this.setButtonsConfig();
  }

  private setFormConfig() {
    this.modalForm = this.formBuilder.group({
      id: this.isEdit() ? this.guestTypeToEdit.id : 0,
      code: [this.isEdit() ? this.guestTypeToEdit.code : '', [Validators.required, Validators.maxLength(10)]],
      guestTypeName: [this.isEdit() ? this.guestTypeToEdit.guestTypeName : '', [Validators.required, Validators.maxLength(60)]],
      propertyId: this.isEdit() ? this.guestTypeToEdit.propertyId : this.propertyId,
      isIncognito: this.isEdit() ? this.guestTypeToEdit.isIncognito : true,
      isVip: this.isEdit() ? this.guestTypeToEdit.isVip : false,
      isActive: this.isEdit() ? this.guestTypeToEdit.isActive : true
    });
    this.modalForm.valueChanges
      .subscribe(value => this.buttonConfirmModal.isDisabled = this.modalForm.invalid);
  }

  private setButtonsConfig() {
    this.buttonCancelModal = this.sharedService.getButtonConfig(
      'cancel-modal-create-edit-guest-type',
      this.cancelDelete,
      'commomData.cancel',
      ButtonType.Secondary
    );

    this.buttonConfirmModal = this.sharedService.getButtonConfig(
      'save-modal-create-edit-guest-type',
      this.confirmDelete,
      'commomData.confirm',
      ButtonType.Primary,
      null,
      this.modalForm.invalid
    );
  }

  private isEdit() {
    return this.guestTypeToEdit;
  }

  private cancelDelete = () => {
    this.customResetForm();
    this.cancel.emit();
  }

  private confirmDelete = () => {
    if (this.modalForm.valid) {
      this.confirm.emit(this.modalForm.value);
      this.customResetForm();
      this.cancel.emit();
    }
  }

  private customResetForm() {
    this.modalForm.reset({
      id: 0,
      code: null,
      guestTypeName: null,
      propertyId: this.propertyId,
      isIncognito: true,
      isVip: false,
      isActive: true
    });
  }

}
