import { SharedModule } from '../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuestTypeRoutingModule } from './guest-type-routing.module';
import { GuestTypeListComponent } from './guest-type-list/guest-type-list.component';
import { GuestTypeDeleteComponent } from './guest-type-delete/guest-type-delete.component';
import { GuestTypeCreateEditComponent } from './guest-type-create-edit/guest-type-create-edit.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    GuestTypeRoutingModule
  ],
  declarations: [
    GuestTypeListComponent,
    GuestTypeDeleteComponent,
    GuestTypeCreateEditComponent
  ]
})
export class GuestTypeModule {
}
