import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuestTypeListComponent } from './guest-type-list/guest-type-list.component';
import { AdminGuard } from '../../../auth/services/admin-guard.service';

export const guestTypeRoutes: Routes = [
  { path: '', component: GuestTypeListComponent, canActivate: [AdminGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(guestTypeRoutes)],
  exports: [RouterModule]
})
export class GuestTypeRoutingModule {
}
