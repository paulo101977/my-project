import { Component, OnInit } from '@angular/core';
import { ToasterEmitService } from '../../../../shared/services/shared/toaster-emit.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigHeaderPageNew } from '../../../../shared/components/header-page-new/config-header-page-new';
import { TranslateService } from '@ngx-translate/core';
import { GuestTypeResource } from '../../../../shared/resources/guest-type/guest-type.resource';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../../shared/models/button-config';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { FormBuilder } from '@angular/forms';
import { ModalEmitToggleService } from '../../../../shared/services/shared/modal-emit-toggle.service';
import { PropertyGuestTypeDto } from '../../../../shared/models/dto/property-guest-type-dto';
import { SuccessError } from '../../../../shared/models/success-error-enum';

@Component({
  selector: 'app-guest-type-list',
  templateUrl: './guest-type-list.component.html'
})
export class GuestTypeListComponent implements OnInit {

  public propertyId: number;
  public configHeaderPage: ConfigHeaderPageNew;
  public toBeDeleted: PropertyGuestTypeDto;
  public toBeEdit: PropertyGuestTypeDto;

  // Table dependencies
  public columns: Array<any>;
  public pageItemsList: Array<PropertyGuestTypeDto>;
  public itemsOption: Array<any>;

  // Buttons dependencies
  public buttonNewEdit: ButtonConfig;

  // Modal dependencies
  public modalTitle: string;

  constructor(public router: Router,
              private route: ActivatedRoute,
              private translateService: TranslateService,
              private toasterEmitService: ToasterEmitService,
              private sharedService: SharedService,
              private formBuilder: FormBuilder,
              private modalEmitToggleService: ModalEmitToggleService,
              private guestTypeResource: GuestTypeResource) {
  }

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.propertyId = this.route.snapshot.params.property;
    this.setButtonsConfig();
    this.setConfigTable();
    this.getList();
  }

  private setButtonsConfig() {
    this.buttonNewEdit = this.sharedService.getButtonConfig(
      'new-edit-guest-type',
      this.toggleModal,
      '',
      ButtonType.Secondary,
      ButtonSize.Small
    );
  }

  private setConfigTable() {
    this.setColumnsName();
    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.toggleModal },
      { title: 'commomData.delete', callbackFunction: this.toggleDeleteModal }
    ];
  }

  private setColumnsName() {
    this.columns = [
      { name: 'label.code', prop: 'code' },
      { name: 'label.description', prop: 'guestTypeName' },
      { name: 'label.vip', prop: 'isVipFormatted' },
      { name: 'label.incognito', prop: 'isIncognitoFormatted' }
    ];
  }

  private getList() {
    this.guestTypeResource.getAllGuestTypeListByPropertyId(this.propertyId)
      .subscribe((response) => {
        if (response.items) {
          // TODO pedir pro backend alterar
          const yes = this.translateService.instant('commomData.yes');
          const not = this.translateService.instant('commomData.not');
          this.pageItemsList = response.items.map(guestType => {
            guestType['isVipFormatted'] = guestType.isVip ? yes : not;
            guestType['isIncognitoFormatted'] = guestType.isIncognito ? yes : not;
            return guestType;
          });
        }
      });
  }

  public updateStatus(item: PropertyGuestTypeDto) {
    this.guestTypeResource.updateGuestTypeStatus(item.id)
      .subscribe((response) => {
        const message = item.isActive ? 'variable.lbActivatedWithSuccessM' : 'variable.lbInactivatedWithSuccessM';

        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant(message, {
            labelName: this.translateService.instant('label.guestType')
          })
        );
        this.getList();
      });
  }

  public toggleModal = (item?: PropertyGuestTypeDto) => {
    if (item instanceof MouseEvent) {
      item = null;
    }
    this.toBeEdit = item;
    this.modalTitle = this.translateService.instant(item ? 'action.lbEdit' : 'action.lbNew', {
      labelName: this.translateService.instant('label.guestType')
    });
    this.modalEmitToggleService.emitToggleWithRef('modalCreateEdit');
  }

  public saveItem(item: PropertyGuestTypeDto) {
    let action;
    let message;

    if (item.id != 0) {
      action = this.guestTypeResource.updateGuestType(item);
      message = 'variable.lbEditSuccessM';
    } else {
      action = this.guestTypeResource.createGuestType(item);
      message = 'variable.lbSaveSuccessM';
    }

    action.subscribe(result => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant(message, {
          labelName: this.translateService.instant('label.guestType')
        })
      );
      this.getList();
    });
  }

  public toggleDeleteModal = (item?: PropertyGuestTypeDto) => {
    this.toBeDeleted = item;
    this.modalEmitToggleService.emitToggleWithRef('modalDelete');
  }

  public deleteItem = (item: PropertyGuestTypeDto) => {
    this.guestTypeResource.deleteGuestType(item.id)
      .subscribe(result => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbDeleteSuccessM', {
            labelName: this.translateService.instant('label.guestType')
          })
        );
        this.getList();
      });
  }
}
