import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';
import { SelectObjectOption } from '../../../../shared/models/selectModel';
import { IMyDate, IMyDpOptions } from 'mydatepicker';
import { ButtonConfig, ButtonType } from '../../../../shared/models/button-config';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { DateService } from '../../../../shared/services/shared/date.service';
import { CommomService } from '../../../../shared/services/shared/commom.service';
import { BillingItemServiceResource } from '../../../../shared/resources/billingItem/billing-item-service.resource';
import { ActivatedRoute } from '@angular/router';
import { Fee } from '../../../../shared/models/billingType/fee';
import { GetAllBillingItemTax } from '../../../../shared/models/billingType/get-all-billing-item-tax';
import { CurrencyService } from '../../../../shared/services/shared/currency-service.service';
import * as _ from 'lodash';

@Component({
  selector: 'associate-tax',
  templateUrl: './associate-tax.component.html',
  styleUrls: ['./associate-tax.component.css'],
})
export class AssociateTaxComponent implements OnInit, OnChanges {
  @Input() editFee: Fee;
  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();

  public propertyId: number;
  public maskRateConfig: CurrencyMaskConfig;

  public formAssociate: FormGroup;
  private taxList: GetAllBillingItemTax[];

  // Select dependencies
  public taxAssociateOptionList: Array<SelectObjectOption>;
  // DatePicker dependencies
  public datePickerDisableUntil: IMyDate;

  // Button dependencies
  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;

  constructor(
    public route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private dateService: DateService,
    private commomService: CommomService,
    private billingItemServiceResource: BillingItemServiceResource,
    private currencyService: CurrencyService,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.editFee && changes.editFee.currentValue) {
      this.setEditForm();
    }
  }

  public setVars() {
    this.propertyId = +this.route.snapshot.params.property;
    this.setButtonConfig();
    this.setFormModal();
    this.getTaxAssociateListToSelect();
    this.maskRateConfig = this.currencyService.getMaskRateConfig();

    const today = new Date();
    this.datePickerDisableUntil = {
      year: today.getFullYear(),
      month: today.getMonth() + 1,
      day: today.getDate() - 1,
    };
  }

  private setFormModal() {
    this.formAssociate = this.formBuilder.group({
      isActive: true,
      billingItemTaxId: [0, [Validators.required]],
      taxPercentage: [null, [Validators.required]],
      beginDate: [null, [Validators.required]],
      endDate: null,
    });

    this.formAssociate.valueChanges.subscribe(() => {
      this.buttonSaveModalConfig.isDisabled = this.formAssociate.invalid;
    });
  }

  private setButtonConfig() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'associate-tax--cancel',
      this.cancelCallback,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
      'associate-tax--save',
      this.saveCallback,
      'commomData.confirm',
      ButtonType.Primary,
      null,
      true,
    );
  }

  private getTaxAssociateListToSelect() {
    this.taxAssociateOptionList = [];
    this.billingItemServiceResource.getAllBillingItemTaxToAssociateList(this.propertyId).subscribe(response => {
      if (response && !_.isEmpty(response.items)) {
        this.taxList = response.items;
        this.taxAssociateOptionList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
          response,
          'id',
          'billingItemName',
        );
      }
    });
  }

  public cancelCallback = () => {
    this.cancel.emit();
  }

  public saveCallback = () => {
    const feeData: GetAllBillingItemTax = this.taxList.find(item => item.id == this.formAssociate.get('billingItemTaxId').value);
    this.save.emit({
      feeData: feeData,
      formValue: this.formAssociate.value,
    });
    this.formAssociate.reset();
  }

  private setEditForm() {
    if (this.editFee) {
      const tax = this.editFee;
      this.formAssociate.patchValue({
        isActive: tax.isActive,
        taxPercentage: tax.taxPercentage,
        beginDate: tax.beginDate,
        endDate: tax.endDate ? tax.endDate : null,
        billingItemTaxId: tax.billingItemTaxId
      });
    }
  }
}
