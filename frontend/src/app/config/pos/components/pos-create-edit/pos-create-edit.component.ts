import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfigHeaderPageNew } from '../../../../shared/components/header-page-new/config-header-page-new';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../../shared/models/button-config';
import { SelectObjectOption } from '../../../../shared/models/selectModel';
import { POS } from '../../../../shared/models/pos/pos';
import { SuccessError } from '../../../../shared/models/success-error-enum';
import { ModalEmitToggleService } from '../../../../shared/services/shared/modal-emit-toggle.service';
import { CommomService } from '../../../../shared/services/shared/commom.service';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { ToasterEmitService } from '../../../../shared/services/shared/toaster-emit.service';
import { BillingItemCategoryResource } from '../../../../shared/resources/billing-item-category/billing-item-category.resource';
import { POSResource } from '../../../../shared/resources/pos/pos.resource';
import { TranslateService } from '@ngx-translate/core';
import { Fee } from '../../../../shared/models/billingType/fee';
import * as _ from 'lodash';
import { GuidDefaultData } from '../../../../shared/mock-data/guid-default-data';

@Component({
  selector: 'app-pos-create-edit',
  templateUrl: './pos-create-edit.component.html',
  styleUrls: ['./pos-create-edit.component.css'],
})
export class PosCreateEditComponent implements OnInit {
  public propertyId: number;
  public posIdToEdit: number;
  public posToSave: POS;
  public configHeaderPage: ConfigHeaderPageNew;
  public pageTitle: string;
  public form: FormGroup;
  public columns: Array<any>;
  public itemsOption: Array<any>;
  public feeToEdit: Fee;
  public integrationCode: string;

  // Select dependencies
  public categoryList: SelectObjectOption[];

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;
  public buttonImplementConfig: ButtonConfig;

  @ViewChild('templateMomentFormat') templateMomentFormat: TemplateRef<any>;
  @ViewChild('percentTemplate') percentTemplate: TemplateRef<any>;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private location: Location,
    private formBuilder: FormBuilder,
    private commomService: CommomService,
    private sharedService: SharedService,
    private billingItemCategoryResource: BillingItemCategoryResource,
    private posResource: POSResource,
    private modalEmitToggleService: ModalEmitToggleService,
    private translateService: TranslateService,
    private toasterEmitService: ToasterEmitService,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.propertyId = +this.route.snapshot.params.property;
    this.posIdToEdit = +this.route.snapshot.params.id;

    this.setConfigHeaderPage();
    this.setForm();
    this.setButtonConfig();
    this.setColumnsName();
    this.getAllCategoryList();

    if (this.isEditPage()) {
      this.getById(this.posIdToEdit);
    } else {
      this.posToSave = new POS();
    }

    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.editFee },
      { title: 'commomData.delete', callbackFunction: this.desassociateFee },
    ];
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  private setForm() {
    this.form = this.formBuilder.group({
      isActive: true,
      name: [null, [Validators.required]],
      billingItemCategoryId: [null, [Validators.required]],
      id: 0,
      integrationCode: null
    });

    this.form.valueChanges.subscribe(value => {
      this.buttonSaveConfig = this.sharedService.getButtonConfig(
        'group-create-edit-save',
        this.savePos,
        'commomData.confirm',
        ButtonType.Primary,
      );
      this.buttonSaveConfig.isDisabled = this.form.invalid;
    });
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'group-create-edit-cancel',
      () => this.location.back(),
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'group-create-edit-save',
      this.savePos,
      'commomData.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
    this.buttonImplementConfig = this.sharedService.getButtonConfig(
      'group-create-edit-implementation',
      this.toggleModal,
      'entryTypeModule.service.createEdit.associatedFees.buttonNew',
      ButtonType.Secondary,
      ButtonSize.Small,
    );
  }

  private setColumnsName() {
    this.columns = [
      { name: 'label.taxName', prop: 'name' },
      { name: 'label.group', prop: 'categoryName' },
      { name: 'label.taxPercentage', prop: 'taxPercentage', cellTemplate: this.percentTemplate },
      { name: 'label.validityBegin', prop: 'beginDate', cellTemplate: this.templateMomentFormat },
      { name: 'label.validityEnd', prop: 'endDate', cellTemplate: this.templateMomentFormat },
    ];
  }

  public toggleModal = () => {
    this.feeToEdit = null;
    this.modalEmitToggleService.emitToggleWithRef('modalAssociate');
  }

  private getAllCategoryList() {
    this.categoryList = [];
    this.billingItemCategoryResource.getAllBillingItemCategoryActiveListByPropertyId(this.propertyId).subscribe(response => {
      if (response) {
        this.categoryList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
          response,
          'id',
          'billingItemCategoryName',
        );
      }
    });
  }

  public isEditPage() {
    return this.posIdToEdit;
  }

  public getById(id: number) {
    this.posResource.getPOSById(id, this.propertyId).subscribe(response => {
      this.posToSave = response;
      this.form.patchValue({
        isActive: this.posToSave.isActive,
        name: this.posToSave.name,
        billingItemCategoryId: this.posToSave.billingItemCategoryId,
        id: this.posToSave.id,
        integrationCode: this.posToSave.integrationCode
      });
    });
  }

  public associateFee = (feeAssociate: Fee) => {
    if (feeAssociate) {
      const feeToList: Fee = {
        id: GuidDefaultData,
        name: feeAssociate['feeData']['billingItemName'],
        categoryName: feeAssociate['feeData']['billingItemCategoryName'],
        isActive: feeAssociate['formValue']['isActive'],
        taxPercentage: feeAssociate['formValue']['taxPercentage'],
        beginDate: feeAssociate['formValue'].beginDate,
        endDate: feeAssociate['formValue'].endDate ? feeAssociate['formValue'].endDate : null,
        billingItemTaxId: feeAssociate['feeData']['id'],
        billingItemServiceId: null,
        identify: null,
      };

      const index = _.findIndex(this.posToSave.serviceAndTaxList, item => item.billingItemTaxId == feeToList.billingItemTaxId);
      if (index > -1) {
        this.posToSave.serviceAndTaxList.splice(index, 1, feeToList);
      } else {
        this.posToSave.serviceAndTaxList.push(feeToList);
      }
      this.posToSave.serviceAndTaxList = [...this.posToSave.serviceAndTaxList];
    }
    this.toggleModal();
  }

  public desassociateFee = (feeToDelete: Fee) => {
    const fee = this.posToSave.serviceAndTaxList.find(item => item.id == feeToDelete.id);
    if (fee) {
      _.remove(this.posToSave.serviceAndTaxList, fee);
    }
    this.posToSave.serviceAndTaxList = [...this.posToSave.serviceAndTaxList];
  }

  public editFee = (feeToEdit: Fee) => {
    this.toggleModal();
    this.feeToEdit = feeToEdit;
  }

  public savePos = () => {
    this.mapperFormToPos();

    let message, route, resource;
    if (this.isEditPage()) {
      message = 'variable.lbEditSuccessM';
      route = '../../';
      resource = this.posResource.updatePOS(this.posToSave);
    } else {
      message = 'variable.lbSaveSuccessM';
      route = '../';
      resource = this.posResource.createPOS(this.posToSave);
    }

    resource.subscribe(response => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant(message, {
          labelName: this.translateService.instant('label.pos'),
        }),
      );
      this.location.back();
    });
  }

  private mapperFormToPos() {
    this.posToSave.id = this.form.get('id').value;
    this.posToSave.name = this.form.get('name').value;
    this.posToSave.billingItemCategoryId = this.form.get('billingItemCategoryId').value;
    this.posToSave.isActive = this.form.get('isActive').value;
    this.posToSave.propertyId = this.propertyId;
    this.posToSave.integrationCode = this.form.get('integrationCode').value;
  }
}
