import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ConfigHeaderPageNew } from '../../../../shared/components/header-page-new/config-header-page-new';
import { ToasterEmitService } from '../../../../shared/services/shared/toaster-emit.service';
import { ModalEmitService } from '../../../../shared/services/shared/modal-emit.service';
import { POS } from '../../../../shared/models/pos/pos';
import { POSResource } from '../../../../shared/resources/pos/pos.resource';
import { SuccessError } from '../../../../shared/models/success-error-enum';

@Component({
  selector: 'app-pos-list',
  templateUrl: './pos-list.component.html',
  styleUrls: ['./pos-list.component.css'],
})
export class PosListComponent implements OnInit {
  public propertyId: number;
  public configHeaderPage: ConfigHeaderPageNew;
  private toBeDeleted: POS;

  // Table dependencies
  public columns: Array<any>;
  public pageItemsList: Array<POS>;
  public filteredItems: Array<POS>;
  public itemsOption: Array<any>;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private modalEmitService: ModalEmitService,
    private translateService: TranslateService,
    private toasterEmitService: ToasterEmitService,
    private posResource: POSResource,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.setConfigTable();
      this.setConfigHeaderPage();
      this.getList();
    });
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      keepButtonActive: true,
      callBackFunction: this.goToNewPage,
    };
  }

  private setConfigTable() {
    this.setColumnsName();
    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.goToEditPage },
      { title: 'commomData.delete', callbackFunction: this.confirmDeleteItem },
    ];
  }

  private setColumnsName() {
    this.columns = [{ name: 'label.description', prop: 'billingItemName' }, { name: 'label.group', prop: 'billingItemCategoryName' }];
  }

  private getList() {
    this.posResource.getAllPOSListByPropertyId(this.propertyId).subscribe(response => {
      if (response) {
        this.pageItemsList = response.items;
        this.filteredItems = [...response.items];
      }
    });
  }

  public goToNewPage = () => {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

  public goToEditPage = item => {
    this.router.navigate(['edit', item.id], { relativeTo: this.route });
  }

  public updateList(searchData: string) {
    this.filteredItems = this.pageItemsList.filter(item => {
      return (
        item.billingItemName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.billingItemCategoryName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1
      );
    });
  }

  public updateStatus(item: POS) {
    this.posResource.updatePOSStatus(item.id).subscribe(response => {
      const message = item.isActive ? 'variable.lbActivatedWithSuccessM' : 'variable.lbInactivatedWithSuccessM';

      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant(message, {
          labelName: this.translateService.instant('label.pos'),
        }),
      );
      this.getList();
    });
  }

  public confirmDeleteItem = (item: POS) => {
    this.toBeDeleted = item;
    this.modalEmitService.emitSimpleModal(
      this.translateService.instant('action.lbDelete', {
        labelName: this.translateService.instant('label.pos'),
      }),
      this.translateService.instant('variable.lbConfirmDeleteF', {
        labelName: this.translateService.instant('label.pos'),
      }) +
        ' ' +
        this.toBeDeleted.billingItemName +
        '?',
      this.deleteItem,
      [],
    );
  }

  public deleteItem = () => {
    this.posResource.deletePOS(this.toBeDeleted.id, this.propertyId).subscribe(response => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('variable.lbDeleteSuccessM', {
          labelName: this.translateService.instant('label.pos'),
        }),
      );
      this.getList();
    });
  }
}
