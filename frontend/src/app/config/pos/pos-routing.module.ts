import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PosListComponent } from './components/pos-list/pos-list.component';
import { PosCreateEditComponent } from './components/pos-create-edit/pos-create-edit.component';

export const routes: Routes = [
  { path: '', component: PosListComponent },
  { path: 'new', component: PosCreateEditComponent },
  { path: 'edit/:id', component: PosCreateEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PosRoutingModule {}
