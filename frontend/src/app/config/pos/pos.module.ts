import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PosRoutingModule } from './pos-routing.module';
import { PosListComponent } from './components/pos-list/pos-list.component';
import { AssociateTaxComponent } from './components/associate-tax/associate-tax.component';
import { PosCreateEditComponent } from './components/pos-create-edit/pos-create-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule, PosRoutingModule],
  declarations: [PosListComponent, PosCreateEditComponent, AssociateTaxComponent],
})
export class PosModule {}
