import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { PosModule } from './pos/pos.module';
import { GuestTypeModule } from './guest-type/components/guest-type.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    PosModule,
    GuestTypeModule
  ]
})
export class ConfigModule {}
