export class PropertyAuthInfo {
  propertyId: number;
  token: string;
  propertyName: string;
}
