import { Injectable, Inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { AuthService } from 'app/shared/services/auth-service/auth.service';
import { LocationManagerService, TokenManagerService, PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { CountryCodeEnum, APP_NAME } from '@inovacaocmnet/thx-bifrost';

// TODO: create logic to accept other countries DEBIT 7092
@Injectable({ providedIn: 'root' })
export class LocationGuardService implements CanActivate {
  constructor(
    private locationService: LocationManagerService,
    private tokenManagerService: TokenManagerService,
    private propertyStorage: PropertyStorageService,
    @Inject(APP_NAME) private appName
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const propertyId = this.propertyStorage.getCurrentPropertyId();
    const token = this.tokenManagerService.getPropertyToken(this.appName, propertyId);

    return token && this.locationService.couldShow([CountryCodeEnum.PT], token);
  }
}
