import {Inject, Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService, APP_NAME, TokenManagerService, PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { PropertyService } from 'app/shared/services/property/property.service';

@Injectable({ providedIn: 'root' })
export class PropertyAuthGuard implements CanActivate {
  constructor(@Inject(APP_NAME) private appName,
              private authService: AuthService,
              private router: Router,
              private propertyService: PropertyService,
              private tokenManager: TokenManagerService,
              private propertyStorageService: PropertyStorageService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.checkProperty(route.params.property);
  }

  private async checkProperty(propertyId: number): Promise<boolean> {
    let isAuthenticated = await this.authService.isAuthenticatedOnProperty(this.appName, propertyId);
    if (isAuthenticated && ! await this.tokenManager.getCurrentPropertyToken()) {
      const propertyToken = await this.tokenManager.getPropertyToken(this.appName, propertyId);
      const property = await this.propertyStorageService.getProperty(this.appName, propertyId);
      await this.tokenManager.setCurrentPropertyToken(propertyToken);
      await this.propertyStorageService.setCurrentProperty(property);
    } else if (!isAuthenticated) {
      isAuthenticated = !!(await this.propertyService.asyncAuthenticateOnProperty(this.appName, { id: propertyId}));
    }


    if (isAuthenticated) {
      return true;
    } else {
      this.router.navigate(['/home']);
      return false;
    }
  }
}
