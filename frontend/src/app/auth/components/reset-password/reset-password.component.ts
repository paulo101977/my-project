import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthResource } from '../../../shared/resources/auth/auth.resource';
import { NewPasswordForm } from '../../models/new-password-form';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
})
export class ResetPasswordComponent implements OnInit {
  private token: string;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public translateService: TranslateService,
    private authResource: AuthResource,
    private toasterEmitService: ToasterEmitService,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.activatedRoute.params.subscribe(params => (this.token = params.token));
  }

  resetPassword(value: NewPasswordForm) {
    this.authResource.resetPassword(value, this.token).subscribe(
      () => {
        this.router.navigate(['/auth/login']);
        this.toasterEmitService
          .emitChange(SuccessError.success, this.translateService.instant('authModule.resetPassword.successMessage'));
      }
    );
  }

}
