import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { APP_NAME, AuthService, LoginForm, ProductEnum } from '@inovacaocmnet/thx-bifrost';
import { environment } from '@environment';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public modalTitle: string;
  public loginForm: FormGroup;
  public forgotPasswordEmail = '';
  public appVersion: string;

  constructor(
    @Inject(APP_NAME) public appName: string,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    public translateService: TranslateService,
    public route: Router,
    private toasterEmitService: ToasterEmitService,
    private modalEmitToggleService: ModalEmitToggleService) { }

  ngOnInit() {
    this.appVersion = environment.appVersion;
    if (this.authService.isAuthenticated(this.appName)) {
      this.navigateToHotelPanel();
    }
    this.modalTitle = this.translateService.instant('authModule.login.forgotPasswordModal.title');
  }

  public login(loginForm) {
    const loginFormData: LoginForm = {
      login: loginForm.email,
      password: loginForm.password,
      productId: ProductEnum.PMS
    };

    this.authService
      .authenticate(this.appName, loginFormData)
      .subscribe(() => this.navigateToHotelPanel(), error => {
        this.toasterEmitService.emitChange(SuccessError.error, this.authService.extractErrorMessage(error));
      });
  }

  public toggleModal(): void {
    this.modalEmitToggleService.emitToggleWithRef('modalForgotPassword');
  }

  recoverPassword(userEmail) {
    this.authService.recoverPassword(userEmail).subscribe(() => {
      this.forgotPasswordEmail = userEmail;
      this.toggleModal();
    });
  }

  private navigateToHotelPanel() {
    const route = this.authService.redirectUrl || '/home';
    this.authService.redirectUrl = null;
    this.route.navigateByUrl(route);
  }

}
