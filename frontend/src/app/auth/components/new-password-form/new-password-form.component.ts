import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NewPasswordForm } from '../../models/new-password-form';
import { PasswordValidator } from '../../../shared/validators/password-validator';
import { ThxPopoverDirective, ThxButtonColorType } from '@inovacao-cmnet/thx-ui';

@Component({
  selector: 'app-new-password-form',
  templateUrl: './new-password-form.component.html',
  styleUrls: ['./new-password-form.component.scss'],
})
export class NewPasswordFormComponent implements OnInit {
  @ViewChild(ThxPopoverDirective) public passwordRulesPopover: ThxPopoverDirective;

  @Output() public submitNewPasswordForm: EventEmitter<NewPasswordForm> = new EventEmitter();

  public passwordForm: FormGroup;
  public productLogoImg = 'thex-pms-logo.svg';
  public buttonColorType: ThxButtonColorType = ThxButtonColorType.Primary;

  constructor(public router: Router, public formBuilder: FormBuilder, public translateService: TranslateService) {}

  ngOnInit() {
    this.initFormConfig();
  }

  private initFormConfig() {
    this.passwordForm = this.formBuilder.group(
      {
        password: [
          '',
          [
            Validators.required,
            Validators.minLength(8),
            Validators.pattern(
              /^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?])[A-Za-z\d!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]{8,}/,
            ),
          ],
        ],
        confirmPassword: ['', [Validators.required]],
      },
      {
        validator: PasswordValidator.match,
      },
    );
  }

  hidePopover() {
    this.passwordRulesPopover.hidePopover();
  }

  onSubmit({ value, valid }: { value: NewPasswordForm; valid: boolean }) {
    if (valid) {
      this.submitNewPasswordForm.emit(value);
    }
  }
}
