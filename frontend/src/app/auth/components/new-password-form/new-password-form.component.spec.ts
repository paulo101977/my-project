import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NewPasswordFormComponent } from './new-password-form.component';
import { ThxButtonModule, ThxIconModule, ThxLabelModule, ThxPopoverModule } from '@inovacao-cmnet/thx-ui';
import { CoreModule } from '@app/core/core.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';
import { FormBuilder } from '@angular/forms';

describe('NewPasswordFormComponent', () => {
  let component: NewPasswordFormComponent;
  let fixture: ComponentFixture<NewPasswordFormComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [NewPasswordFormComponent],
      imports: [TranslateModule.forRoot(),
        RouterTestingModule,
        ThxLabelModule,
        ThxIconModule,
        ThxButtonModule,
        ThxPopoverModule,
        CoreModule],
      providers: [FormBuilder],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(NewPasswordFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
