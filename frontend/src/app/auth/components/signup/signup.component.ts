import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { AuthResource } from '../../../shared/resources/auth/auth.resource';
import { NewPasswordForm } from '../../models/new-password-form';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { ImgCdnPipe } from '@inovacao-cmnet/thx-ui';
import { environment } from '@environment';
import { APP_NAME, AuthService } from '@inovacaocmnet/thx-bifrost';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  private token: string;
  public appVersion: string;
  public toasterVisible = false;

  @HostBinding('style.background') backgroundImage: SafeStyle;
  public image: string;
  public versionName = 'Horseshoe';
  public versionNumber = '1.0';
  public productName = 'PMS';

  private readonly CONFIG_BACKGROUND_IMAGE = 'no-repeat center center / cover';
  private readonly CONFIG_OVERLAY_BACKGROUND_IMAGE = 'linear-gradient(0deg,rgba(0,0,0,0.15),rgba(0,0,0,0.15))';
  public year = new Date().getFullYear();

  constructor(
    @Inject(APP_NAME) public appName: string,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public translateService: TranslateService,
    private authResource: AuthResource,
    private toasterEmitService: ToasterEmitService,
    private imgCdnPipe: ImgCdnPipe,
    private sanitizer: DomSanitizer,
    private authService: AuthService,
  ) {}

  ngOnInit() {
    this.appVersion = environment.appVersion;
    if (this.authService.isAuthenticated(this.appName)) {
      this.authService.logout(this.appName);
    }
    this.setVars();
    const image = this.imgCdnPipe.transform('pms_login_bg_v2.jpg');
    if (image) {
      this.backgroundImage = this.sanitizer.bypassSecurityTrustStyle(
        `${this.CONFIG_OVERLAY_BACKGROUND_IMAGE},url("${image}") ${this.CONFIG_BACKGROUND_IMAGE}`
      );
    }
  }

  private setVars() {
    this.activatedRoute.params.subscribe(params => (this.token = params.token));
  }

  signup(form: NewPasswordForm) {
    this.authResource.signup(form, this.token)
      .subscribe(() => {
        this.toasterEmitService.emitChange(SuccessError.success, 'authModule.signup.successMessage');
      });
  }

  public navigateToLogin() {
    this.router.navigate(['/auth/login']);
  }
}
