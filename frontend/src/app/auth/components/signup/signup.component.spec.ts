import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { SignupComponent } from './signup.component';
import { ImgCdnPipe, ThxPopoverModule } from '@inovacao-cmnet/thx-ui';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { CoreModule } from '@app/core/core.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [SignupComponent],
      imports: [
        TranslateModule.forRoot(),
        RouterTestingModule,
        HttpClientTestingModule,
        ThxPopoverModule,
        CoreModule
      ],
      providers: [
        {provide: AdminApiService, useValue: {}},
        {
          provide: ImgCdnPipe, useValue: {
            transform() {
            }
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
