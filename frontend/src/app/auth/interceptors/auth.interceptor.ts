import { Location } from '@angular/common';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InterceptorHandlerService } from 'app/core/services/interceptor-handler/interceptor-handler.service';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class TokenInterceptor implements HttpInterceptor {
  private readonly INTERCEPTOR_STORAGE_NAME = 'auth';

  constructor(
    public location: Location,
    private interceptorHandler: InterceptorHandlerService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.shouldRun()) {
      return next.handle(request);
    }

    const propertyToken = this.getPropertyToken();
    if (propertyToken && !(this.location.path().indexOf('/home') > -1)) {
      if (propertyToken) {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${propertyToken.token}`,
          },
        });
      }
      return next.handle(request);
    }

    const token = this.getToken();
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.getToken()}`,
        },
      });
    }

    return next.handle(request);
  }

  private getPropertyToken() {
    return JSON.parse(sessionStorage.getItem('propertyToken'));
  }

  private getToken() {
    return localStorage.getItem('token');
  }

  public disable() {
    this.interceptorHandler.disable(this.INTERCEPTOR_STORAGE_NAME);
  }

  public disableOnce() {
    this.interceptorHandler.disableOnce(this.INTERCEPTOR_STORAGE_NAME);
  }

  public enable() {
    this.interceptorHandler.enable(this.INTERCEPTOR_STORAGE_NAME);
  }

  private shouldRun() {
    return this.interceptorHandler.shouldRun(this.INTERCEPTOR_STORAGE_NAME);
  }
}
