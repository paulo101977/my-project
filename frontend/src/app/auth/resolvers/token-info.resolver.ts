import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../../shared/services/auth-service/auth.service';

@Injectable({ providedIn: 'root' })
export class TokenInfoResolver implements Resolve<any> {
  constructor(private authService: AuthService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const propertyId = route.params.property;
    const propertyInfo = this.authService.getPropertyTokenFromLocalStorage(propertyId);
    this.authService.addPropertyTokenInSession(propertyInfo);
    return null;
  }
}
