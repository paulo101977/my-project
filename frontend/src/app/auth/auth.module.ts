import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { NewPasswordFormComponent } from './components/new-password-form/new-password-form.component';
import { AuthRoutingModule } from './auth-routing.module';
import { CoreModule } from '@app/core/core.module';
import {
  configureRecaptcha,
  ImgCdnPipe,
  LoginModule,
  ThxButtonModule,
  ThxImgModule,
  ThxInputV2Module,
  ThxToasterV2Module
} from '@inovacao-cmnet/thx-ui';

const RecaptchaConfiguration = configureRecaptcha({
  sitekey: '6Lc_K44UAAAAAJ07K1ygtZT3BrmNIvzA73QdjmOr'
});

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    AuthRoutingModule,
    CoreModule,
    LoginModule,
    ThxImgModule,
    ThxInputV2Module,
    ThxButtonModule,
    ThxToasterV2Module
  ],
  declarations: [
    LoginComponent,
    SignupComponent,
    ResetPasswordComponent,
    NewPasswordFormComponent
  ],
  providers: [
    RecaptchaConfiguration,
    ImgCdnPipe
  ]
})
export class AuthModule {}
