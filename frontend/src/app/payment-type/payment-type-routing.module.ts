import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentTypeListComponent } from './components/payment-type-list/payment-type-list.component';
import { PaymentTypeConfigComponent } from './components-container/payment-type-config/payment-type-config.component';

export const paymentTypeRoutes: Routes = [
  { path: '', component: PaymentTypeListComponent },
  { path: 'config', component: PaymentTypeConfigComponent },
  { path: 'config/:paymentType/:acquirer', component: PaymentTypeConfigComponent },
  { path: 'config/:paymentType', component: PaymentTypeConfigComponent },
];

@NgModule({
  imports: [RouterModule.forChild(paymentTypeRoutes)],
  exports: [RouterModule],
})
export class PaymentTypeRoutingModule {}
