import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { PaymentTypeEnum } from '../../../shared/models/reserves/payment-type-enum';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { GetAllPaymentTypeDto } from '../../../shared/models/payment-type/payment-type';
import { PaymentTypeWithouBePlasticCardOrDebit } from '../../../shared/models/billingType/payment-type-withou-be-plastic-card-Debit';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { PaymentTypeResource } from '../../../shared/resources/payment-type/payment-type.resource';

@Component({
  selector: 'app-payment-type-config',
  templateUrl: './payment-type-config.component.html',
})
export class PaymentTypeConfigComponent implements OnInit {
  public configHeaderPage: ConfigHeaderPageNew;
  public paymentTypeForm: FormGroup;
  public paymentTypeList: Array<SelectObjectOption>;
  public showBankEmitterSelect: Boolean;
  private propertyId: number;
  public propertyPaymentTypeIssuingBank: GetAllPaymentTypeDto;
  public isDebit: boolean;
  public isCreditCard: boolean;
  public isDisabled: boolean;
  public acquirerList: Array<SelectObjectOption>;
  public paymentTypeId: number;
  public acquirerId: string;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    private commomService: CommomService,
    private location: Location,
    private sharedService: SharedService,
    private router: Router,
    public route: ActivatedRoute,
    private paymentTypeResource: PaymentTypeResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.propertyPaymentTypeIssuingBank = new GetAllPaymentTypeDto();
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.paymentTypeId = params.paymentType ? params.paymentType : false;

      this.route.queryParams.subscribe(queryParams => {
        this.acquirerId = queryParams.acquirer ? queryParams.acquirer : false;
        if (this.isEditPage()) {
          this.loadPaymentData();
        }
        this.setConfigHeaderPage();
        this.setForm();
        this.getPaymentTypeList();
        this.getAcquirerList();
        this.setButtonsConfig();
        this.setListeners();
      });
    });
  }

  public isEditPage() {
    return this.paymentTypeId || this.acquirerId;
  }

  public loadPaymentData() {
    if (this.acquirerId) {
      this.paymentTypeResource.getPaymentTypePlasticCardOrDebit(this.propertyId, this.paymentTypeId, this.acquirerId).subscribe(payment => {
        this.propertyPaymentTypeIssuingBank = payment;
        this.propertyPaymentTypeIssuingBank.id = payment.id;
        this.paymentTypeForm.get('paymentType').setValue(payment.paymentTypeId);
        this.paymentTypeForm.get('acquirer').setValue(payment.acquirerId.toUpperCase());
        this.paymentTypeForm.get('isActive').setValue(payment.isActive);
        this.paymentTypeForm.get('integrationCode').setValue(payment.integrationCode);
      });
    } else {
      this.paymentTypeResource.getPaymentTypeWithoutBePlasticCardOrDebit(this.propertyId, this.paymentTypeId).subscribe(payment => {
        this.propertyPaymentTypeIssuingBank = payment;
        this.paymentTypeForm.get('paymentType').setValue(payment.paymentTypeId);
        this.paymentTypeForm.get('isActive').setValue(payment.isActive);
        this.paymentTypeForm.get('description').setValue(payment.description);
        this.paymentTypeForm.get('integrationCode').setValue(payment.integrationCode);
      });
    }
  }

  public setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  public setForm() {
    this.paymentTypeForm = this.formBuilder.group({
      paymentType: [null, Validators.required],
      acquirer: null,
      description: null,
      isActive: [true, Validators.required],
      integrationCode: [null]
    });
  }

  private getAcquirerList() {
    this.acquirerList = [];
    this.paymentTypeResource.getAcquirerList(this.propertyId).subscribe(response => {
      this.acquirerList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(response, 'id', 'tradeName');
    });
  }

  private getPaymentTypeList() {
    this.paymentTypeList = [];
    this.paymentTypeResource.getPaymentTypeList(this.propertyId).subscribe(response => {
      this.paymentTypeList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(response, 'id', 'paymentTypeName');
    });
  }

  private setButtonsConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'payment-type-cancel',
      this.goBackPreviousPage,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'payment-type-save',
      this.savePaymentType,
      'commomData.confirm',
      ButtonType.Primary,
      null,
      true,
    );
  }

  private setListeners(): void {
    this.paymentTypeForm.get('isActive').valueChanges.subscribe(value => {
      this.isDisabled = !value;
    });

    this.paymentTypeForm.get('paymentType').valueChanges.subscribe(value => {
      if (value) {
        this.isDebit = value == PaymentTypeEnum.Debit;
        this.isCreditCard = value == PaymentTypeEnum.CreditCard;
        this.showBankEmitterSelect = this.isDebit || this.isCreditCard;
        this.resetValidators();
      }
    });

    this.paymentTypeForm.valueChanges.subscribe(value => {
      this.buttonSaveConfig = this.sharedService.getButtonConfig(
        'payment-type-save',
        this.savePaymentType,
        'commomData.confirm',
        ButtonType.Primary,
        null,
        this.paymentTypeForm.invalid,
      );
    });
  }

  private goBackPreviousPage = () => {
    this.location.back();
  }

  private savePaymentType = () => {
    if (this.isEditPage()) {
      if (this.acquirerId) {
        this.updatePaymentTypePlasticCardOrDebit();
      } else {
        this.updatePaymentTypeWithoutBePlasticCardOrDebit();
      }
    } else {
      if (this.isDebit || this.isCreditCard) {
        this.saveNewPaymentTypePlasticCardOrDebit();
      } else {
        this.saveNewPaymentTypeWithoutBePlasticCardOrDebit();
      }
    }
  }

  private saveNewPaymentTypeWithoutBePlasticCardOrDebit() {
    const dataToUpdate = new PaymentTypeWithouBePlasticCardOrDebit();
    dataToUpdate.id = 0;
    dataToUpdate.propertyId = this.propertyId;
    dataToUpdate.description = this.paymentTypeForm.get('description').value;
    dataToUpdate.isActive = this.paymentTypeForm.get('isActive').value;
    dataToUpdate.paymentTypeId = this.paymentTypeForm.get('paymentType').value;
    dataToUpdate.integrationCode = this.paymentTypeForm.get('integrationCode').value;

    this.paymentTypeResource.createPaymentTypeWithoutBePlasticCardOrDebit(dataToUpdate).subscribe(() => {
      this.toasterEmitService.emitChangeTwoMessages(
        SuccessError.success,
        this.translateService.instant('title.paymentType'),
        this.translateService.instant('variable.saveSuccessF'),
      );
      this.router.navigate(['../'], { relativeTo: this.route });
    });
  }

  private updatePaymentTypePlasticCardOrDebit() {
    this.propertyPaymentTypeIssuingBank.paymentTypeId = this.paymentTypeForm.get('paymentType').value;
    this.propertyPaymentTypeIssuingBank.acquirerId = this.paymentTypeForm.get('acquirer').value;
    this.propertyPaymentTypeIssuingBank.isActive = this.paymentTypeForm.get('isActive').value;
    this.propertyPaymentTypeIssuingBank.integrationCode = this.paymentTypeForm.get('integrationCode').value;
    this.paymentTypeResource.updatePaymentTypePlasticCardOrDebit(this.propertyPaymentTypeIssuingBank).subscribe(() => {
      this.toasterEmitService.emitChangeTwoMessages(
        SuccessError.success,
        this.translateService.instant('title.paymentType'),
        this.translateService.instant('variable.updateSuccessF'),
      );
      this.router.navigate(['../../'], { relativeTo: this.route });
    });
  }

  private updatePaymentTypeWithoutBePlasticCardOrDebit() {
    const dataToUpdate = new PaymentTypeWithouBePlasticCardOrDebit();
    dataToUpdate.id = this.propertyPaymentTypeIssuingBank.id;
    dataToUpdate.propertyId = this.propertyPaymentTypeIssuingBank.propertyId;
    dataToUpdate.description = this.paymentTypeForm.get('description').value;
    dataToUpdate.isActive = this.paymentTypeForm.get('isActive').value;
    dataToUpdate.paymentTypeId = this.paymentTypeForm.get('paymentType').value;
    dataToUpdate.integrationCode = this.paymentTypeForm.get('integrationCode').value;
    this.paymentTypeResource.updatePaymentTypeWithoutBePlasticCardOrDebit(dataToUpdate).subscribe(() => {
      this.toasterEmitService.emitChangeTwoMessages(
        SuccessError.success,
        this.translateService.instant('title.paymentType'),
        this.translateService.instant('variable.updateSuccessF'),
      );
      this.router.navigate(['../../'], { relativeTo: this.route });
    });
  }

  private saveNewPaymentTypePlasticCardOrDebit() {
    this.propertyPaymentTypeIssuingBank.propertyId = this.propertyId;
    this.propertyPaymentTypeIssuingBank.paymentTypeId = this.paymentTypeForm.get('paymentType').value;
    this.propertyPaymentTypeIssuingBank.acquirerId = this.paymentTypeForm.get('acquirer').value;
    this.propertyPaymentTypeIssuingBank.description = null;
    this.propertyPaymentTypeIssuingBank.isActive = this.paymentTypeForm.get('isActive').value;
    this.propertyPaymentTypeIssuingBank.id = 0;
    this.propertyPaymentTypeIssuingBank.integrationCode = this.paymentTypeForm.get('integrationCode').value;

    this.paymentTypeResource.createPaymentTypePlasticCardOrDebit(this.propertyPaymentTypeIssuingBank).subscribe(() => {
      this.toasterEmitService.emitChangeTwoMessages(
        SuccessError.success,
        this.translateService.instant('title.paymentType'),
        this.translateService.instant('variable.saveSuccessF'),
      );
      this.router.navigate(['../'], { relativeTo: this.route });
    });
  }

  private resetValidators() {
    this.paymentTypeForm.get('acquirer').clearValidators();
    this.paymentTypeForm.get('description').clearValidators();
    if (this.isDebit || this.isCreditCard) {
      this.paymentTypeForm.get('acquirer').reset();
      this.paymentTypeForm.get('acquirer').setValidators([Validators.required]);
    } else {
      this.paymentTypeForm.get('description').reset();
      this.paymentTypeForm.get('description').setValidators([Validators.required]);
    }
    this.paymentTypeForm.get('acquirer').updateValueAndValidity();
    this.paymentTypeForm.get('description').updateValueAndValidity();
  }
}
