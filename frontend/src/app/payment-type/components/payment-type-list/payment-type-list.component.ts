import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { PropertyPaymentTypeIssuingBankListDetail } from '../../../shared/models/payment-type/property-payment-type-issuing-bank-detail';
import { GetAllPaymentTypeDto } from '../../../shared/models/payment-type/payment-type';
import { PlasticBrandEnum } from '../../../shared/models/payment-type/plastic-brand-enum';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { PathImg } from '../../../shared/mock-data/path-img';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { PaymentTypeResource } from 'app/shared/resources/payment-type/payment-type.resource';
import { PaymentTypeService } from '../../../shared/services/shared/payment-type.service';

@Component({
  styles: [
    // TODO remover quando o projeto de Componentes estiver ok
      `
      .thf-tag-outline {
        padding: 3px 6px;
        border-radius: 5px;
        border: 1px solid var(--color-grey-submarine);
        margin: 0 2px;
        text-align: center;
      }

      .thf-tag-outline:first-child {
        margin-left: 0;
      }
    `
  ],
  selector: 'payment-type-list',
  templateUrl: 'payment-type-list.component.html',
})
export class PaymentTypeListComponent implements OnInit {
  public pageItemsList: Array<GetAllPaymentTypeDto>;
  public filteredItemsList: Array<GetAllPaymentTypeDto>;
  public pathImg: string;
  public altImg: string;

  // Header dependencies
  public configHeaderPage: ConfigHeaderPageNew;
  public searchableItems: SearchableItems;
  public propertyId: number;

  // Table dependencies
  public itemsOption: Array<any>;

  // Search filter
  public searchFilter: string;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private paymentTypeResource: PaymentTypeResource,
    private toasterEmitService: ToasterEmitService,
    private paymentTypeService: PaymentTypeService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.setVars();
    this.setSearchableItems();
    this.setConfigHeaderPage();
    this.getPaymentTypeList();
  }

  public updateList(searchData: any) {
    this.filteredItemsList = this.paymentTypeService.filterBySearchData(searchData, this.pageItemsList);
  }

  setVars() {
    this.itemsOption = [{ title: 'action.edit' }, { title: 'action.delete' }];

    this.route.params.subscribe(params => (this.propertyId = +params.property));
  }

  public paymentTypeWasUpdated(payment: GetAllPaymentTypeDto) {
    this.paymentTypeResource
      .toggleStatus(payment.propertyId, payment.paymentTypeId, payment.acquirerId, payment.isActive)
      .subscribe(response => {
        if (payment.isActive) {
          this.toasterEmitService.emitChangeTwoMessages(
            SuccessError.success,
            this.translateService.instant('title.paymentType'),
            this.translateService.instant('variable.isActiveF'),
          );
        } else {
          this.toasterEmitService.emitChangeTwoMessages(
            SuccessError.success,
            this.translateService.instant('title.paymentType'),
            this.translateService.instant('variable.isInactiveF'),
          );
        }
      });
  }

  private setSearchableItems(): void {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.pageItemsList;
    this.searchableItems.placeholderSearchFilter = 'placeholder.searchPaymentType';
    this.searchableItems.placeholderActionWordFilter = 'action.search';
    this.searchableItems.tableRowTypeEnum = TableRowTypeEnum.BillingAccount;
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      keepButtonActive: true,
      callBackFunction: this.goToPaymentTypeConfigPage,
    };
  }

  private getPaymentTypeList() {
    this.pageItemsList = [];
    this.paymentTypeResource.getAllPaymentsType(this.propertyId)
      .subscribe((data) => {
        this.pageItemsList = data.items;
        this.filteredItemsList = data.items;
      });
  }

  public goToPaymentTypeConfigPage = () => {
    this.router.navigate(['config'], { relativeTo: this.route });
  }

  public rowAction(event, row: PropertyPaymentTypeIssuingBankListDetail, rowIndex: number) {
    if (event.title == 'action.delete') {
      this.removeRow(row);
    } else if (event.title == 'action.edit') {
      this.editRow(row, rowIndex);
    }
  }

  public removeRow(row: PropertyPaymentTypeIssuingBankListDetail) {
    const callbackFunction = (p => p.acquirerId == row.acquirerId);

    if (this.sharedService.elementExistsInList(null, this.pageItemsList, callbackFunction)) {
      this.paymentTypeResource.delete(this.propertyId, row.paymentTypeId, row.acquirerId).subscribe(() => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbDeleteSuccessF', {
            labelName: this.translateService.instant('title.paymentType'),
          }),
        );
        this.getPaymentTypeList();
      });
    }
  }

  public editRow(row: PropertyPaymentTypeIssuingBankListDetail, rowIndex: number) {
    this.router.navigate(['config', row.paymentTypeId], {
      relativeTo: this.route,
      queryParams: { acquirer: row.acquirerId },
    });
  }

  public setPlasticBrandImg(brandId: number) {
    if (brandId == PlasticBrandEnum.visa) {
      this.pathImg = PathImg + 'icon_visa.svg';
      this.altImg = 'img.visa';
      return true;
    }

    if (brandId == PlasticBrandEnum.mastercard) {
      this.pathImg = PathImg + 'icon_mastercard.svg';
      this.altImg = 'img.mastercard';
      return true;
    }
    return false;
  }
}
