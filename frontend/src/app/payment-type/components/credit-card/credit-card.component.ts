import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PropertyPaymentTypeIssuingBankDetail } from 'app/shared/models/payment-type/property-payment-type-issuing-bank-detail';
import { ButtonConfig } from '../../../shared/models/button-config';
import { ButtonType, ButtonSize } from 'app/shared/models/button-config';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { GetAllPaymentTypeDto } from '../../../shared/models/payment-type/payment-type';
import { PlasticBrandProperty } from '../../../shared/models/payment-type/plastic-brand-property';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { PaymentTypeMapper } from 'app/shared/mappers/payment-type-mapper';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { PaymentTypeResource } from 'app/shared/resources/payment-type/payment-type.resource';

@Component({
  selector: 'credit-card-payment',
  templateUrl: 'credit-card-payment.html',
  styleUrls: ['credit-card-payment.component.css'],
})
export class CreditCardPaymentComponent implements OnInit {
  private readonly MAX_PLOTS = 12;

  public propertyPaymentTypeIssuingBankDetailList: Array<PropertyPaymentTypeIssuingBankDetail>;
  private propertyId: number;
  public maxPlotsOptionList: Array<SelectObjectOption>;
  public isNewAssociateFlag: boolean;
  public modalTitle: string;
  public paymentTypeId: number;
  public acquirerId: string;

  widthColumns = Array<number>();

  // Table dependencies
  public itemsOption: Array<any>;

  // Buttons dependencies
  public buttonAssociateFlag: ButtonConfig;
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  // Modal dependencies
  public formModalAssociateFlag: FormGroup;
  public brandList: Array<SelectObjectOption>;
  public brandListFull: Array<PlasticBrandProperty>;
  public acquirerList: Array<SelectObjectOption>;

  @ViewChild('myTable') table: DatatableComponent;
  @Input() public propertyPaymentTypeIssuingBank: GetAllPaymentTypeDto;
  @Input() public isDisabled: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private paymentTypeResource: PaymentTypeResource,
    private commomService: CommomService,
    private paymentTypeMapper: PaymentTypeMapper,
    private toasterEmitService: ToasterEmitService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.setVars();
    this.setFormModal();
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.paymentTypeId = params.paymentType;
      this.acquirerId = params.acquirer;
      this.getBrandList();
      this.getAcquirerList();
      if (this.isEditPage()) {
        this.loadPaymentData();
      }
    });
  }

  public isEditPage() {
    return this.paymentTypeId || this.acquirerId;
  }

  public loadPaymentData() {
    this.propertyPaymentTypeIssuingBankDetailList = [...this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList];
  }

  private setVars() {
    this.propertyPaymentTypeIssuingBankDetailList = new Array<PropertyPaymentTypeIssuingBankDetail>();
    this.itemsOption = [{ title: 'commomData.edit' }, { title: 'commomData.delete' }];
    this.modalTitle = 'paymentTypeModule.commomData.subTitleTwo';
    const maxPlotsList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.maxPlotsOptionList = maxPlotsList.map(elem => ({ key: elem, value: `${elem}` }));
    this.setButtonConfig();
  }

  private getBrandList(): void {
    this.brandList = new Array<SelectObjectOption>();
    this.paymentTypeResource.getPlasticBrandPropertyList(this.propertyId).subscribe(response => {
      this.brandListFull = response.items;
      this.brandList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        response,
        'plasticBrandId',
        'plasticBrandName',
      );
    });
  }

  private getAcquirerList(): void {
    this.acquirerList = new Array<SelectObjectOption>();
    this.paymentTypeResource.getAcquirerList(this.propertyId).subscribe(response => {
      this.acquirerList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(response, 'id', 'tradeName');
    });
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-credit-card',
      this.goBackPreviewPage,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirm-credit-card',
      this.savePropertyPaymentTypeIssuingBankDetail,
      'commomData.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
    this.buttonAssociateFlag = this.sharedService.getButtonConfig(
      'associate-flag',
      this.newAssociateFlag,
      'paymentTypeModule.commomData.associateFlag',
      ButtonType.Secondary,
      ButtonSize.Small,
    );
  }

  private newAssociateFlag = () => {
    this.toggleEditFormModal();
  }

  private goBackPreviewPage = () => {
    this.toggleEditFormModal();
  }

  public itemWasUpdated(paymentTypeIssuingBankDetail: PropertyPaymentTypeIssuingBankDetail) {}

  public rowAction(event, paymentTypeIssuingBankDetail: PropertyPaymentTypeIssuingBankDetail, rowIndex: number) {
    if (event.title == 'commomData.delete') {
      this.removeRow(paymentTypeIssuingBankDetail);
    } else if (event.title == 'commomData.edit') {
      this.editRow(paymentTypeIssuingBankDetail, rowIndex);
    }
  }

  public removeRow(row: PropertyPaymentTypeIssuingBankDetail) {
    const callbackFunction = p => p.acquirerId == row.acquirerId && p.plasticBrandPropertyId == row.plasticBrandPropertyId;

    if (
      this.sharedService.elementExistsInList(null, this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList, callbackFunction)
    ) {
      const index = this.sharedService.getIndexInList(
        null,
        this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList,
        callbackFunction,
      );
      this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList.splice(index, 1);
      this.propertyPaymentTypeIssuingBankDetailList = [...this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList];
    }
  }

  public editRow(row: PropertyPaymentTypeIssuingBankDetail, rowIndex: number) {
    this.toggleEditFormModal();
    this.modalTitle = 'paymentTypeModule.modal.editFlag';
    this.isNewAssociateFlag = false;
    this.formModalAssociateFlag.patchValue({
      id: row.id,
      rowIndex: rowIndex,
      plasticBrandProperty: {
        plasticBrandPropertyId: row.plasticBrandId,
      },
      maxPlots: row.maximumInstallmentsQuantity,
      isActive: row.isActive,
      conditionList: row.billingItemPaymentTypeDetailConditionList,
    });
  }

  public toggleEditFormModal = () => {
    this.isNewAssociateFlag = true;
    this.modalEmitToggleService.emitToggleWithRef('credit-card-payment-type');
    this.setFormModal();
  }

  private setFormModal() {
    this.formModalAssociateFlag = this.formBuilder.group(
      {
        id: 0,
        rowIndex: null,
        plasticBrandProperty: this.formBuilder.group({
          plasticBrandPropertyId: [null, [Validators.required]],
        }),
        isActive: true,
        maxPlots: [1, [Validators.required]],
        conditionList: this.formBuilder.array([]),
      },
      { validator: form => this.validateConditionList(form) },
    );

    this.createConditionForm();
    this.setButtonConfirmConfigAndListenerFormModal();
  }

  private validateConditionList(form: FormGroup) {
    const conditionList = <FormArray>form.get('conditionList');
    if (conditionList.controls.length > 0) {
      let countValid = 0;
      const maxPlots = form.get('maxPlots').value;
      for (let i = 0; i < maxPlots; i++) {
        if (this.validateCondition(conditionList.controls[i])) {
          countValid++;
        }
      }
      return countValid == maxPlots ? null : { maxPlots: 'invalid' };
    }
    return null;
  }

  private createConditionForm() {
    const conditionList = <FormArray>this.formModalAssociateFlag.controls['conditionList'];

    for (let i = 1; i <= this.MAX_PLOTS; i++) {
      conditionList.push(
        this.formBuilder.group({
          id: null,
          installmentNumber: null,
          commissionPct: null,
          paymentDeadline: null,
        }),
      );
    }
  }

  public getFormArray(form) {
    return form.get('conditionList').controls;
  }

  private validateCondition(condition) {
    return condition.get('installmentNumber').value && condition.get('commissionPct').value && condition.get('paymentDeadline').value;
  }

  private savePropertyPaymentTypeIssuingBankDetail = () => {
    const propertyPaymentTypeIssuingBankDetail = this.paymentTypeMapper.toCreditCardPropertyPaymentTypeIssuingBankDetail(
      this.formModalAssociateFlag,
      this.propertyPaymentTypeIssuingBank,
      this.acquirerList,
      this.brandListFull,
    );
    const callbackFunction = p =>
      p.acquirerId == propertyPaymentTypeIssuingBankDetail.acquirerId &&
      p.plasticBrandPropertyId == propertyPaymentTypeIssuingBankDetail.plasticBrandPropertyId;
    if (this.isNewAssociateFlag) {
      if (
        this.sharedService.elementExistsInList(null, this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList, callbackFunction)
      ) {
        this.toasterEmitService.emitChange(SuccessError.error, 'paymentTypeModule.commomData.validationErrorMessage');
        return;
      }
      if (!this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList) {
        this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList = new Array<PropertyPaymentTypeIssuingBankDetail>();
      }
      this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList.push(propertyPaymentTypeIssuingBankDetail);
    } else {
      if (this.alreadyHasPlasticBrandPropertyAndAcquirerInOtherElement(callbackFunction)) {
        this.toasterEmitService.emitChange(SuccessError.error, 'paymentTypeModule.commomData.validationErrorMessage');
        return;
      }
      this.updatePropertyPaymentTypeIssuingBankDetailList(propertyPaymentTypeIssuingBankDetail, callbackFunction);
    }

    this.propertyPaymentTypeIssuingBankDetailList = [...this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList];
    this.toggleEditFormModal();
  }

  private alreadyHasPlasticBrandPropertyAndAcquirerInOtherElement(callbackFunction: Function): boolean {
    return this.sharedService.hasMoreOfOneElementInList(
      this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList,
      this.formModalAssociateFlag.get('rowIndex').value,
      callbackFunction,
    );
  }

  private updatePropertyPaymentTypeIssuingBankDetailList(
    propertyPaymentTypeIssuingBankDetail: PropertyPaymentTypeIssuingBankDetail,
    callbackFunction: Function,
  ): void {
    this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList[
      this.formModalAssociateFlag.get('rowIndex').value
    ] = propertyPaymentTypeIssuingBankDetail;
  }

  public toggleExpandRow(row, expanded) {
    this.table.rowDetail.toggleExpandRow(row);
    this.widthColumns = this.table.headerComponent.columns.map(c => c.width);
  }

  private setButtonConfirmConfigAndListenerFormModal() {
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirm-credit-card',
      this.savePropertyPaymentTypeIssuingBankDetail,
      'commomData.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
    this.formModalAssociateFlag.valueChanges.subscribe(value => {
      this.buttonConfirmConfig = this.sharedService.getButtonConfig(
        'confirm-credit-card',
        this.savePropertyPaymentTypeIssuingBankDetail,
        'commomData.confirm',
        ButtonType.Primary,
        ButtonSize.Normal,
        this.formModalAssociateFlag.invalid,
      );
    });
  }
}
