import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PropertyPaymentTypeIssuingBankDetail } from 'app/shared/models/payment-type/property-payment-type-issuing-bank-detail';
import { ButtonConfig } from '../../../shared/models/button-config';
import { ButtonType } from 'app/shared/models/button-config';
import { PlasticBrandProperty } from '../../../shared/models/payment-type/plastic-brand-property';
import { ButtonSize } from '../../../shared/models/button-config';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { GetAllPaymentTypeDto } from '../../../shared/models/payment-type/payment-type';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { PaymentTypeMapper } from '../../../shared/mappers/payment-type-mapper';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { PaymentTypeResource } from '../../../shared/resources/payment-type/payment-type.resource';
import { SelectObjectOption } from 'app/shared/models/selectModel';

@Component({
  selector: 'debit-payment',
  templateUrl: 'debit-payment.html',
})
export class DebitPaymentComponent implements OnInit {
  @Input() public propertyPaymentTypeIssuingBank: GetAllPaymentTypeDto;
  @Input() public isDisabled: boolean;

  readonly guidDefault = '00000000-0000-0000-0000-000000000000';
  private propertyId: number;
  public isNewAssociateFlag: boolean;
  public paymentTypeId: number;
  public acquirerId: string;

  // Table dependencies
  public itemsOption: Array<any>;
  public rowList = [];

  // Modal dependencies
  public modalId = 'debit-payment-type';
  public modalTitle: string;
  public formModalAssociateFlag: FormGroup;
  public brandList: Array<SelectObjectOption>;
  public brandListFull: Array<PlasticBrandProperty>;
  public acquirerList: Array<SelectObjectOption>;

  // Buttons dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;
  public buttonAssociateFlag: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    private modalEmitToggleService: ModalEmitToggleService,
    private paymentTypeResource: PaymentTypeResource,
    private commomService: CommomService,
    private paymentTypeMapper: PaymentTypeMapper,
    private sharedService: SharedService,
    private toasterEmitService: ToasterEmitService,
    private router: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.modalTitle = 'paymentTypeModule.modal.associateFlag';
    this.setButtonConfig();
    this.setFormModal();
    this.itemsOption = [{ title: 'commomData.edit' }, { title: 'commomData.delete' }];
    this.router.params.subscribe(params => {
      this.propertyId = +params.property;
      this.paymentTypeId = params.paymentType;
      this.acquirerId = params.acquirer;
      if (this.isEditPage()) {
        this.loadPaymentData();
      }
      this.getBrandList();
    });
  }

  public isEditPage() {
    return this.paymentTypeId || this.acquirerId;
  }

  public loadPaymentData() {
    this.rowList = [...this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList];
  }

  private setFormModal() {
    this.formModalAssociateFlag = this.formBuilder.group({
      id: 0,
      rowIndex: null,
      plasticBrandProperty: this.formBuilder.group({
        plasticBrandPropertyId: [null, [Validators.required]],
      }),
      isActive: true,
      commissionPct: [null, [Validators.required]],
      paymentDeadline: [null, [Validators.required]],
      propertyPaymentTypeIssuingBankDetailConditionId: this.guidDefault,
    });

    this.setButtonConfirmConfigAndListenerFormModal();
  }

  private getBrandList(): void {
    this.brandList = new Array<SelectObjectOption>();
    this.paymentTypeResource.getPlasticBrandPropertyList(this.propertyId).subscribe(response => {
      this.brandListFull = response.items;
      this.brandList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        response,
        'plasticBrandId',
        'plasticBrandName',
      );
    });
  }

  private setButtonConfig(): void {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-debit',
      this.goBackPreviewPage,
      'commomData.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );

    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirm-debit',
      this.savePropertyPaymentTypeIssuingBankDetail,
      'commomData.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
    );

    this.setButtonAssociateFlag();
  }

  private setButtonAssociateFlag(): void {
    this.buttonAssociateFlag = this.sharedService.getButtonConfig(
      'associate-flag',
      this.toggleFormModal,
      'paymentTypeModule.commomData.associateFlag',
      ButtonType.Secondary,
      ButtonSize.Small,
      this.isDisabled,
    );
  }

  private goBackPreviewPage = () => {
    this.toggleFormModal();
  }

  private savePropertyPaymentTypeIssuingBankDetail = () => {
    const propertyPaymentTypeIssuingBankDetail = this.paymentTypeMapper.toPropertyPaymentTypeIssuingBankDetail(
      this.formModalAssociateFlag,
      this.propertyPaymentTypeIssuingBank,
      this.acquirerList,
      this.brandListFull,
    );
    const callbackFunction = p =>
      p.acquirerId == propertyPaymentTypeIssuingBankDetail.acquirerId &&
      p.plasticBrandPropertyId == propertyPaymentTypeIssuingBankDetail.plasticBrandPropertyId;
    if (this.isNewAssociateFlag) {
      if (
        this.sharedService.elementExistsInList(null, this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList, callbackFunction)
      ) {
        this.toasterEmitService.emitChange(SuccessError.error, 'paymentTypeModule.commomData.validationErrorMessage');
        return;
      }
      if (!this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList) {
        this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList = new Array<PropertyPaymentTypeIssuingBankDetail>();
      }
      this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList.push(propertyPaymentTypeIssuingBankDetail);
    } else {
      if (this.alreadyHasPlasticBrandPropertyAndAcquirerInOtherElement(callbackFunction)) {
        this.toasterEmitService.emitChange(SuccessError.error, 'paymentTypeModule.commomData.validationErrorMessage');
        return;
      }
      this.updatePropertyPaymentTypeIssuingBankDetailList(propertyPaymentTypeIssuingBankDetail, callbackFunction);
    }

    this.rowList = [...this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList];
    this.toggleFormModal();
  }

  private alreadyHasPlasticBrandPropertyAndAcquirerInOtherElement(callbackFunction: Function): boolean {
    return this.sharedService.hasMoreOfOneElementInList(
      this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList,
      this.formModalAssociateFlag.get('rowIndex').value,
      callbackFunction,
    );
  }

  private updatePropertyPaymentTypeIssuingBankDetailList(
    propertyPaymentTypeIssuingBankDetail: PropertyPaymentTypeIssuingBankDetail,
    callbackFunction: Function,
  ): void {
    this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList[
      this.formModalAssociateFlag.get('rowIndex').value
    ] = propertyPaymentTypeIssuingBankDetail;
  }

  public rowAction(event, row: PropertyPaymentTypeIssuingBankDetail, rowIndex: number) {
    if (event.title == 'commomData.delete') {
      this.removeRow(row);
    } else if (event.title == 'commomData.edit') {
      this.editRow(row, rowIndex);
    }
  }

  public removeRow(row: PropertyPaymentTypeIssuingBankDetail) {
    const callbackFunction = p => p.acquirerId == row.acquirerId && p.plasticBrandPropertyId == row.plasticBrandPropertyId;

    if (
      this.sharedService.elementExistsInList(null, this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList, callbackFunction)
    ) {
      const index = this.sharedService.getIndexInList(
        null,
        this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList,
        callbackFunction,
      );
      this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList.splice(index, 1);
      this.rowList = [...this.propertyPaymentTypeIssuingBank.billingItemPaymentTypeDetailList];
    }
  }

  public editRow(row: PropertyPaymentTypeIssuingBankDetail, rowIndex: number) {
    this.toggleFormModal();
    this.modalTitle = 'paymentTypeModule.modal.editFlag';
    this.isNewAssociateFlag = false;
    this.formModalAssociateFlag.patchValue({
      id: row.id,
      rowIndex: rowIndex,
      plasticBrandProperty: {
        plasticBrandPropertyId: row.plasticBrandId,
      },
      isActive: row.isActive,
      commissionPct: row.billingItemPaymentTypeDetailConditionList[0].commissionPct,
      paymentDeadline: row.billingItemPaymentTypeDetailConditionList[0].paymentDeadline,
      propertyPaymentTypeIssuingBankDetailConditionId: row.billingItemPaymentTypeDetailConditionList[0].id,
    });
  }

  private toggleFormModal = () => {
    this.isNewAssociateFlag = true;
    this.modalTitle = 'paymentTypeModule.modal.associateFlag';
    this.modalEmitToggleService.emitToggleWithRef(this.modalId);
    this.setFormModal();
  }

  private setButtonConfirmConfigAndListenerFormModal() {
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirm-debit',
      this.savePropertyPaymentTypeIssuingBankDetail,
      'commomData.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
    this.formModalAssociateFlag.valueChanges.subscribe(value => {
      this.buttonConfirmConfig = this.sharedService.getButtonConfig(
        'confirm-debit',
        this.savePropertyPaymentTypeIssuingBankDetail,
        'commomData.confirm',
        ButtonType.Primary,
        ButtonSize.Normal,
        this.formModalAssociateFlag.invalid,
      );
    });
  }
}
