import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, RouterOutlet } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { PaymentTypeRoutingModule } from './payment-type-routing.module';
import { CreditCardPaymentComponent } from './components/credit-card/credit-card.component';
import { DebitPaymentComponent } from './components/debit/debit.component';
import { PaymentTypeListComponent } from './components/payment-type-list/payment-type-list.component';
import { PaymentTypeConfigComponent } from './components-container/payment-type-config/payment-type-config.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, FormsModule, RouterModule, SharedModule, PaymentTypeRoutingModule],
  declarations: [DebitPaymentComponent, CreditCardPaymentComponent, PaymentTypeListComponent, PaymentTypeConfigComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PaymentTypeModule {}
