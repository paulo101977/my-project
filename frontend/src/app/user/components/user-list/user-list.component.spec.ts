import { NO_ERRORS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { User, UserInvitation } from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { getUserData } from 'app/shared/mock-data/user.data';
import { Property } from 'app/shared/models/property';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { UserResource } from 'app/shared/resources/user/user.resource';
import { PropertyService } from 'app/shared/services/property/property.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { Observable, of } from 'rxjs';
import { ActivatedRouteStub } from '../../../../../testing';
import { ButtonConfig, ButtonSize, ButtonType, Type } from 'app/shared/models/button-config';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { UserListComponent } from './user-list.component';
import { RoleResourceService } from '@thex/security';
import { configureTestSuite } from 'ng-bullet';

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;
  let route: ActivatedRouteStub;
  let userResource: UserResource;
  let toasterEmitService: ToasterEmitService;
  const resultLanguages = {
    next: false,
    total: 2,
    items: [
      {key: 'en-us', value: 'en-us'},
      {key: 'pt-br', value: 'pt-br'}
    ]
  };

  const translateServiceStub: Partial<TranslateService> = {
    instant(key: string | Array<string>, interpolateParams?: Object): string | any { return 'null'; }
  };

  const userResourceStub: Partial<UserResource> = {
    updateAdmin(userId: string, isAdmin: boolean): Observable<Object> { return of(null); },
    getAll(): Observable<User[]> { return of(null); },
    toggleActivation(uid: string, propertyId: number): Observable<Object> { return of(null); },
    invite(userInvitation: UserInvitation): Observable<Object> { return of(null); },
    updateLanguage(userId: string, culture: string): Observable<Object> { return of(null); }
  };

  const modalEmitToggleServiceStub: Partial<ModalEmitToggleService> = {
    emitToggleWithRef(modalId: any): void {}
  };

  const toasterEmitServiceStub: Partial<ToasterEmitService> = {
    emitChange(isSuccess: SuccessError, message: string) {}
  };

  const propertyResourceStub: Partial<PropertyResource> = {
    getPropertyLanguages(): Observable<{ next: boolean; total: number; items: ({ value: string; key: string })[] }> {
      return of({
        next: false,
        total: 1,
        items: [
          {value: '', key: 'pt-BR'}
        ]
      });
    }
  };

  const propertyServiceStub: Partial<PropertyService> = {
    getPropertySession(propertyId: number): Property { return null; }
  };

  const sharedServiceStub: Partial<SharedService> = {
    getButtonConfig(
      id: string,
      callback: Function,
      textButton: string,
      buttonType: ButtonType,
      buttonSize?: ButtonSize,
      isDisabled?: boolean,
      type?: Type
    ): ButtonConfig { return null; }
  };

  const roleStubResource: Partial<RoleResourceService> = {
    listAll(search: string = ''): Observable<any> { return of({items: []}); }
  };

  @Pipe({name: 'translate'})
  class TranslatePipeStub implements PipeTransform {
    transform(value: any, ...args: any[]): any { return 'null'; }
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [
        UserListComponent,
        TranslatePipeStub
      ],
      imports: [
        RouterTestingModule,
      ],
      providers: [
        FormBuilder,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: TranslateService, useValue: translateServiceStub },
        { provide: UserResource, useValue: userResourceStub },
        { provide: ModalEmitToggleService, useValue: modalEmitToggleServiceStub },
        { provide: ToasterEmitService, useValue: toasterEmitServiceStub },
        { provide: PropertyResource, useValue: propertyResourceStub },
        { provide: PropertyService, useValue: propertyServiceStub },
        { provide: SharedService, useValue: sharedServiceStub },
        { provide: RoleResourceService, useValue: roleStubResource }
      ]
    });

    route = TestBed.get(ActivatedRoute);
    userResource = TestBed.get(UserResource);
    toasterEmitService = TestBed.get(ToasterEmitService);

    spyOn<any>(userResource, 'getAll').and.returnValue(of([]));

    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when navigate to a valid property', () => {
    beforeEach(() => {
      route.setParamMap({property: 1});
    });

    it('should setVars', () => {
      spyOn<any>(component, 'setButtonConfig');
      spyOn<any>(component, 'setColumnsName');
      spyOn<any>(component, 'getUsers');
      spyOn<any>(component, 'getPropertyLanguages')
        .and.callFake( () => of(resultLanguages) );
      spyOn<any>(component, 'configFormModal');

      component['setVars']();

      expect(component['propertyId']).toEqual(1);
      expect(component['setButtonConfig']).toHaveBeenCalled();
      expect(component['setColumnsName']).toHaveBeenCalled();
      expect(component['getUsers']).toHaveBeenCalled();
      expect(component['getPropertyLanguages']).toHaveBeenCalled();
      expect(component['configFormModal']).toHaveBeenCalled();
      expect(component.itemsOption).toEqual([
        {title: 'commomData.copyLink', callbackFunction: jasmine.any(Function) },
        {title: 'action.edit', callbackFunction: jasmine.any(Function)}
      ]);
    });
  });

  describe('onInit', () => {
    it('should setColumnsName', () => {
      component['setColumnsName']();
      const [name, email, language, role] = component.columns;

      expect(component.columns.length).toEqual(4);
      expect(name).toEqual({name: 'label.userName', prop: 'name'});
      expect(email).toEqual({name: 'label.email', prop: 'email'});
      expect(language).toEqual({
        name: 'label.language',
        prop: 'preferredLanguage',
        cellTemplate: component.languageCellTemplate,
        cellClass: 'thf-u-padding-vertical--05'
      });
      expect(role).toEqual({
        name: 'label.profile',
        prop: 'roleIdList',
        cellTemplate: component.roleCellTemplate,
        cellClass: 'thf-u-padding-vertical--05'
      });
    });

    it('should get Languages from Property', () => {
      spyOn<any>(component['propertyResource'], 'getPropertyLanguages')
        .and.returnValue(of(resultLanguages));

      component['getPropertyLanguages']();

      expect(component.languageList).toEqual(resultLanguages.items);
    });

    it('should update one user language from Property', () => {
      const newLanguage = 'pt-br';
      // TODO: There are two User classes with different signatures. Fix it.
      const userData = <any>getUserData();

      spyOn<any>(component['userResource'], 'updateLanguage').and.returnValue(of(true));
      spyOn<any>(component, 'getUsers');
      spyOn<any>(component['toasterEmitService'], 'emitChange');

      component['updateLanguage'](newLanguage, userData);

      expect(component['getUsers']).toHaveBeenCalled();
      expect(component['userResource'].updateLanguage).toHaveBeenCalledWith(userData.id, newLanguage);
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        component.translateService.instant('variable.lbEditSuccessM', {
          labelName: component.translateService.instant('label.language')
        })
      );
    });

    it('should set buttons config', () => {
      component['setButtonConfig']();

      expect(component.newUserBtnCfg).toEqual(component['sharedService'].getButtonConfig(
        'new-user',
        component.toggleModal,
        component.translateService.instant('action.lbNew', {
          labelName: component.translateService.instant('label.user')
        }),
        ButtonType.Secondary,
        ButtonSize.Small
      ));
      expect(component.cancelModalBtnCfg).toEqual(component['sharedService'].getButtonConfig(
        'cancel-new-user',
        component.toggleModal,
        'action.cancel',
        ButtonType.Secondary,
        ButtonSize.Normal
      ));
      expect(component.saveModalBtnCfg).toEqual(component['sharedService'].getButtonConfig(
        'save-new-user',
        component.actionSaveModalUser,
        'commomData.send',
        ButtonType.Primary,
        ButtonSize.Normal,
        true
      ));
    });

    it('should configFormModal', () => {
      spyOn<any>(component['propertyService'], 'getPropertySession').and.returnValue(null);

      component['configFormModal']();

      expect(component.modalTitle).toEqual(component.translateService.instant('action.lbNew', {
        labelName: component.translateService.instant('label.user')
      }));
      expect(Object.keys(component.modalForm.controls)).toEqual([
        'id', 'name', 'email', 'preferredLanguage', 'role', 'isActive'
      ]);
      expect(component.modalForm.get('isActive').value).toBeTruthy();
      expect(component.modalForm.get('preferredLanguage').value).toEqual(component.languageList[0].key);
    });
  });

  describe('updateRole', () => {
    beforeEach(() => {
      spyOn<any>(userResource, 'invite').and.returnValue(of(null));
    });

    it('should not run when role is falsy', () => {
      component.updateRole(null, null);
      component.updateRole(undefined, null);
      component.updateRole('', null);

      expect(userResource.invite).not.toHaveBeenCalled();
    });

    it('should not run when selected role equals to user\'s role', () => {
      const sameRole = '1234';
      const user: UserInvitation = {
        brandId: 0,
        brandName: '',
        chainId: 0,
        chainName: '',
        email: '',
        id: '',
        invitationAcceptanceDate: '',
        invitationDate: '',
        invitationLink: '',
        isActive: true,
        name: '',
        preferredCulture: '',
        preferredLanguage: '',
        propertyId: 0,
        propertyName: '',
        roleIdList: [sameRole]
      };

      component.updateRole(sameRole, user);

      expect(userResource.invite).not.toHaveBeenCalled();
    });

    it('should update the user role', () => {
      spyOn<any>(toasterEmitService, 'emitChange').and.callThrough();

      const differentRole = '1234';
      const user: UserInvitation = {
        brandId: 0,
        brandName: '',
        chainId: 0,
        chainName: '',
        email: '',
        id: '',
        invitationAcceptanceDate: '',
        invitationDate: '',
        invitationLink: '',
        isActive: true,
        name: '',
        preferredCulture: '',
        preferredLanguage: '',
        propertyId: 0,
        propertyName: '',
        roleIdList: ['5678']
      };

      component.updateRole(differentRole, user);

      expect(userResource.invite).toHaveBeenCalled();
      expect(toasterEmitService.emitChange).toHaveBeenCalledWith(SuccessError.success, jasmine.any(String));
    });
  });
});
