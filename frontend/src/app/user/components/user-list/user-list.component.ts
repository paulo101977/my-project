import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  swapKeys,
  toOptions,
  transduceList,
  User,
  UserInvitation
} from '@inovacaocmnet/thx-bifrost';
import { RoleResourceService } from '@thex/security';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, iif, Observable, of, pipe } from 'rxjs';
import { pluck, take, tap } from 'rxjs/operators';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { UserResource } from 'app/shared/resources/user/user.resource';
import { PropertyService } from 'app/shared/services/property/property.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent implements OnInit {
  public columns: Array<any>;
  public pageItemsList: User[];
  public modalForm: FormGroup;
  private propertyId: number;
  public itemsOption: Array<any>;
  public languageList: any[];
  public roleList: any;

  // Modal Dependencies
  public modalTitle: string;

  // Button Dependencies
  public newUserBtnCfg: ButtonConfig;
  public cancelModalBtnCfg: ButtonConfig;
  public saveModalBtnCfg: ButtonConfig;

  @ViewChild('languageCell') languageCellTemplate: TemplateRef<any>;
  @ViewChild('roleCell') roleCellTemplate: TemplateRef<any>;

  constructor(
    public translateService: TranslateService,
    private userResource: UserResource,
    private modalEmitToggleService: ModalEmitToggleService,
    private toasterEmitService: ToasterEmitService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private propertyResource: PropertyResource,
    private propertyService: PropertyService,
    private sharedService: SharedService,
    private roleResource: RoleResourceService
  ) {}

  ngOnInit(): void {
    this.setVars();
  }

  private setVars(): void {
    this.propertyId = this.route.snapshot.params.property;
    this.getRoleOptions();
    this.setButtonConfig();
    this.setColumnsName();
    this.getUsers();
    this.getPropertyLanguages();
    this.configFormModal();
    this.itemsOption = [
      {title: 'commomData.copyLink', callbackFunction: (user: User) => this.copyUserLink(user)},
      {title: 'action.edit', callbackFunction: (user: User) => this.openEditModal(user)}
    ];
  }

  public getPropertyLanguages() {
    this.propertyResource.getPropertyLanguages().subscribe(data => {
      this.languageList = data.items;
    });
  }

  public getUsers(): void {
    this.userResource.getAll().subscribe(data => {
      this.pageItemsList = [...data];
    });
  }

  public updateUserStatus(userItem: User) {
    this.userResource.toggleActivation(userItem.id, this.propertyId)
      .subscribe(() => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbEditSuccessM', {
            labelName: this.translateService.instant('label.user')
          })
        );
      });
  }

  public setButtonConfig() {
    this.newUserBtnCfg = this.sharedService.getButtonConfig(
      'new-user',
      this.toggleModal,
      this.translateService.instant('action.lbNew', {
        labelName: this.translateService.instant('label.user')
      }),
      ButtonType.Secondary,
      ButtonSize.Small
    );
    this.cancelModalBtnCfg = this.sharedService.getButtonConfig(
      'cancel-new-user',
      this.toggleModal,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.saveModalBtnCfg = this.sharedService.getButtonConfig(
      'save-new-user',
      () => this.actionSaveModalUser(),
      'commomData.send',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  public setColumnsName() {
    this.columns = [
      {name: 'label.userName', prop: 'name'},
      {name: 'label.email', prop: 'email'},
      {
        name: 'label.language',
        prop: 'preferredLanguage',
        cellTemplate: this.languageCellTemplate,
        cellClass: 'thf-u-padding-vertical--05'
      },
      {
        name: 'label.profile',
        prop: 'roleIdList',
        cellTemplate: this.roleCellTemplate,
        cellClass: 'thf-u-padding-vertical--05'
      }
    ];
  }

  public configFormModal(): void {
    this.modalTitle = this.translateService.instant('action.lbNew', {
      labelName: this.translateService.instant('label.user')
    });
    this.modalForm = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.maxLength(60)]],
      email: ['', [Validators.required, Validators.email]],
      preferredLanguage: [this.getDefaultPropertyLanguage(), [Validators.required]],
      role: ['', [Validators.required]],
      isActive: true,
    });
    this.modalForm.valueChanges.subscribe(() => {
      this.saveModalBtnCfg.isDisabled = this.modalForm.invalid;
    });
  }

  public toggleModal = (formData = {}) => {
    this.resetForm(formData);
    this.modalEmitToggleService.emitToggleWithRef('modalUser');
  }

  public createUpdateUser(userInvitation: UserInvitation): Observable<any> {
    return this.userResource.invite(userInvitation);
  }

  public actionSaveModalUser() {
    if (this.modalForm.valid) {
      const { id, ...formValues } = this.modalForm.value;
      const isNew = !id;
      const user = this.mapperUserToInvite(formValues);
      const successMessage$ = iif(() => isNew, of('variable.lbSaveSuccessM'), of('variable.lbEditSuccessM'));
      this.saveUser(user, successMessage$)
        .subscribe(() => {
          this.getUsers();
          this.modalEmitToggleService.emitToggleWithRef('modalUser');
        });
    }
  }

  private saveUser(user: UserInvitation, successMessage$: Observable<string>) {
    const request$ = this.createUpdateUser(user);
    const source$ = forkJoin(successMessage$, request$);

    return source$
      .pipe(
        take(1),
        tap(([successMessage]) => {
          this.toasterEmitService.emitChange(
            SuccessError.success,
            this.translateService.instant(successMessage, {
              labelName: this.translateService.instant('label.user')
            })
          );
        })
      );
  }

  private copyUserLink = (item: User): void => {
    this.userResource.getLink(item.id).subscribe( link => {
      const copyElement = document.createElement('textarea');
      copyElement.style.position = 'fixed';
      copyElement.style.opacity = '0';
      copyElement.textContent = `${link}`;

      const body = document.getElementsByTagName('body')[0];
      body.appendChild(copyElement);
      copyElement.select();
      document.execCommand('copy');
      body.removeChild(copyElement);

      this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant('commomData.copied'));
    });
  }

  public updateLanguage(language: string, user: User) {
    // TODO alterar lógica quando consertar o componente de select
    if (language != user.preferredLanguage) {
      this.userResource.updateLanguage(user.id, language).subscribe(() => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbEditSuccessM', {
            labelName: this.translateService.instant('label.language')
          })
        );
        this.getUsers();
      });
    }
  }

  public updateRole(role: string, user: UserInvitation): void {
    if (!role || role === user.roleIdList[0]) {
      return;
    }

    user.roleIdList = [role];
    this.saveUser(user, of('variable.lbEditSuccessM')).subscribe();
  }

  private getDefaultPropertyLanguage() {
    const property = this.propertyService.getPropertySession(this.propertyId);
    return property && property.preferredLanguage
      ? property.preferredLanguage
      : this.languageList && this.languageList.length > 0
        ? this.languageList[0].key
        : null;
  }

  private mapperUserToInvite(form) {
    const newUser = new UserInvitation();
    newUser.propertyId = this.propertyId;
    newUser.name = form['name'];
    newUser.email = form['email'];
    newUser.preferredLanguage = form['preferredLanguage'];
    newUser.preferredCulture = form['preferredLanguage'];
    newUser.isActive = form['isActive'] == null ? true : form['isActive'];
    newUser.invitationDate = new Date().toISOString();
    newUser.roleIdList = [form.role];
    return newUser;
  }

  private getRoleOptions() {
    return this.roleResource
      .listAll()
      .pipe(
        pluck('items'),
        transduceList(pipe(
          toOptions('id', 'name'),
          swapKeys(['label'], ['value'])
        )),
        take(1)
      )
      .subscribe((roleList) => {
        this.roleList = roleList;
      });
  }

  private openEditModal(user: User) {
    const formData = {
      id: {value: user.id, disabled: true},
      name: {value: user.name, disabled: true},
      email: {value: user.email, disabled: true},
      preferredLanguage: {value: user.preferredLanguage, disabled: true},
      isActive: {value: user.isActive, disabled: true},
      role: user.roleIdList[0]
    };

    this.toggleModal(formData);
  }

  private resetForm(formData = {}) {
    this.modalForm.reset(formData);
  }
}
