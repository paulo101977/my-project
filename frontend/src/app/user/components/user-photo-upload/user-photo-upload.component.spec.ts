import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPhotoUploadComponent } from './user-photo-upload.component';
import { RouterTestingModule } from '@angular/router/testing';
import { PipeTransform, NO_ERRORS_SCHEMA, Pipe } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateTestingModule } from '@app/shared/mock-test/translate-testing.module';

describe('UserPhotoUploadComponent', () => {
  let component: UserPhotoUploadComponent;
  let fixture: ComponentFixture<UserPhotoUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserPhotoUploadComponent
      ],
      imports: [
        CommonModule,
        SharedModule,
        TranslateTestingModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPhotoUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show input', () => {
    const img = 'data:image/';

    component.selectedImage = img;
    component.loadImage();
    fixture.detectChanges();

    expect(component.selectedImage).toEqual(img);
  });
});
