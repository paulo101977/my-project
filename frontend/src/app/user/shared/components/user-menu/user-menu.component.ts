import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APP_NAME, AuthService, PropertyStorageService, User } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {
  @Input() user: User;
  public propertyId: number;

  constructor(
    @Inject(APP_NAME) private appName,
    private authService: AuthService,
    private router: Router,
    private propertyStorage: PropertyStorageService
  ) { }

  ngOnInit(): void {
    this.propertyId = this.propertyStorage.getCurrentPropertyId();
  }

  public logout() {
    this.authService.logout(this.appName);
    this.router.navigateByUrl('/auth/login');
  }
}
