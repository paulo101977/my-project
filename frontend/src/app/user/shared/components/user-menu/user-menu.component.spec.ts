import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AcronymTestingModule, ImgCdnTestingModule } from '@inovacao-cmnet/thx-ui';
import { AuthService, configureAppName, PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { amDateFormatPipeStub, createPipeStub, createServiceStub } from '../../../../../../testing';

import { UserMenuComponent } from './user-menu.component';

describe('UserMenuComponent', () => {
  let component: UserMenuComponent;
  let fixture: ComponentFixture<UserMenuComponent>;

  const authProvider = createServiceStub(AuthService, {
    logout(appName: string): void {}
  });

  const propertyStorageProvider = createServiceStub(PropertyStorageService, {
    getCurrentPropertyId(): number { return 1; }
  });

  const yearsPipeStub = createPipeStub({name: 'years'});

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMenuComponent, yearsPipeStub, amDateFormatPipeStub ],
      imports: [
        ImgCdnTestingModule,
        TranslateTestingModule,
        RouterTestingModule,
        AcronymTestingModule
      ],
      providers: [
        configureAppName('APP'),
        authProvider,
        propertyStorageProvider,
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(UserMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#logout', () => {
    it('should clean the user data and redirect it to the login page', function () {
      spyOn(component['authService'], 'logout');
      spyOn(component['router'], 'navigateByUrl');

      component.logout();

      expect(component['authService'].logout).toHaveBeenCalled();
      expect(component['router'].navigateByUrl).toHaveBeenCalledWith('/auth/login');
    });
  });
});
