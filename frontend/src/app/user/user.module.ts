import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AcronymModule, ImgCdnModule } from '@inovacao-cmnet/thx-ui';
import { SharedModule } from '../shared/shared.module';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserRoutingModule } from './user-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { UserPhotoUploadComponent } from './components/user-photo-upload/user-photo-upload.component';
import { UserMenuComponent } from './shared/components/user-menu/user-menu.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TranslateModule.forChild(),
    FormsModule,
    ReactiveFormsModule,
    UserRoutingModule,
    ImgCdnModule,
    AcronymModule,
  ],
  declarations: [UserListComponent, UserPhotoUploadComponent, UserMenuComponent],
  exports: [UserMenuComponent],
})
export class UserModule {}
