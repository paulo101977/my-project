import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getInitialsName'
})
export class GetInitialsNamePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value) {
      const splited = value.trim().toUpperCase().split(' ');
      let nameToReturn = splited[0].charAt(0);

      if ( splited.length > 1 ) {
        nameToReturn += splited[splited.length - 1].charAt(0);
        return nameToReturn;
      } else { return value.substring(0, 2); }
    }
    return '';
  }

}
