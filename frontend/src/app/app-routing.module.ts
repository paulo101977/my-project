import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PropertiesPanelComponent } from 'app/property/components/properties-panel/properties-panel.component';
import {
  PropertyContentContainerComponent
  } from './property/components/property-content-container/property-content-container.component';
import {
  PropertyConfigContainerComponent
  } from './property/components-containers/property-config-container/property-config-container.component';
import { PropertyCardComponent } from './property/components/property-card/property-card.component';
import { AuthGuard } from './auth/services/auth-guard.service';
import { ParameterResolver } from './shared/resolvers/parameter.resolver';
import { PropertyResolver } from './shared/resolvers/property.resolver';
import { PropertyAuthGuard } from './auth/services/property-auth-guard.service';
import { TokenInfoResolver } from './auth/resolvers/token-info.resolver';
import { ScrollPositionResolver } from './shared/resolvers/scroll-position.resolver';
import { AdminGuard } from './auth/services/admin-guard.service';
import { ShareRoomListGuard } from 'app/room-list/shared/services/share-room-list.guard';

export const appRoutes: Routes = [
  {path: '*', redirectTo: '/', pathMatch: 'full'},
  {path: '', redirectTo: 'auth', pathMatch: 'full'},
  {path: 'login', redirectTo: 'auth', pathMatch: 'full'},
  {path: 'auth', loadChildren: './auth/auth.module#AuthModule'},
  {path: 'roomlist', canActivate: [ShareRoomListGuard], loadChildren: './room-list/room-list.module#RoomListModule'},
  {
    path: 'home',
    component: PropertyContentContainerComponent,
    canActivate: [AuthGuard],
    children: [{path: '', component: PropertiesPanelComponent}],
  },
  {
    path: 'wizard',
    loadChildren: './wizard/wizard.module#WizardModule'
  },
  {
    path: 'p/:property',
    component: PropertyContentContainerComponent,
    canActivate: [AuthGuard, PropertyAuthGuard],
    resolve: {
      parameter: ParameterResolver,
      propertyR: PropertyResolver,
      AuthTokenInfo: TokenInfoResolver,
      scroll: ScrollPositionResolver
    },
    runGuardsAndResolvers: 'always',
    children: [
      {path: '', redirectTo: 'config', pathMatch: 'full'},
      {
        path: 'config',
        component: PropertyConfigContainerComponent,
        loadChildren: './property/property.module#PropertyModule',
        canActivate: [AdminGuard]
      },
      {path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'},
      {path: 'availability-grid', loadChildren: './availability/availability.module#AvailabilityModule'},
      {path: 'room-type', loadChildren: './room-type/room-type.module#RoomTypeModule', canActivate: [AdminGuard]},
      {path: 'room', loadChildren: './room/room.module#RoomModule', canActivate: [AdminGuard]},
      {path: 'reservation', loadChildren: './reservation/reservation.module#ReservationModule'},
      {path: 'guest', loadChildren: './guest/guest.module#GuestModule'},
      {path: 'origin', loadChildren: './origin/origin.module#OriginModule', canActivate: [AdminGuard]},
      {path: 'client', loadChildren: './client/client.module#ClientModule', canActivate: [AdminGuard]},
      {path: 'billing-account', loadChildren: './billing-account/billing-account.module#BillingAccountModule'},
      {path: 'entry-type', loadChildren: './entry-type/entry-type.module#EntryTypeModule', canActivate: [AdminGuard]},
      {
        path: 'market-segment',
        loadChildren: './market-segment/market-segment.module#MarketSegmentModule',
        canActivate: [AdminGuard]
      },
      {path: 'user', loadChildren: './user/user.module#UserModule', canActivate: [AdminGuard]},
      {path: 'profile', loadChildren: './profile/profile.module#ProfileModule'},
      {path: 'reason', loadChildren: './reason/reason.module#ReasonModule', canActivate: [AdminGuard]},
      {
        path: 'payment-type',
        loadChildren: './payment-type/payment-type.module#PaymentTypeModule',
        canActivate: [AdminGuard]
      },
      {path: 'governance', loadChildren: './governance/governance.module#GovernanceModule'},
      {path: 'rm', loadChildren: './revenue-management/revenue-management.module#RevenueManagementModule'},
      {path: 'room-change', loadChildren: './room-change/room-change.module#RoomChangeModule'},
      {path: 'audit', loadChildren: './audit/audit.module#AuditModule'},
      {
        path: 'currency-exchange-manage',
        loadChildren: './currency-exchange/currency-exchange.module#CurrencyExchangeModule',
        canActivate: [AdminGuard]
      },
      {path: 'pos', loadChildren: './config/pos/pos.module#PosModule', canActivate: [AdminGuard]},
      {
        path: 'interactive-report',
        loadChildren: './interactive-report/interactive-report.module#InteractiveReportModule'
      },
      {
        path: 'products',
        loadChildren: './products/products.module#ProductsModule'
      },
      {
        path: 'tributes',
        loadChildren: './tributes/tributes.module#TributesModule'
      },
      {path: 'integration', loadChildren: './integration/integration.module#IntegrationModule'},
      {
        path: 'how-to',
        loadChildren: './how-to/how-to.module#HowToModule'
      }
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      paramsInheritanceStrategy: 'always',
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
