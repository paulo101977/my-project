import {EmbedConfigDto} from 'app/interactive-report/models/dashboard';

export class PowerBIReport {
  powerBIReportId: string;
  name: string;
  embedUrl: string;
  tenantId: string;
  webUrl: string;
  embedConfigDto: EmbedConfigDto;
  minutesToExpiration: number;
  isEffectiveIdentityRolesRequired: boolean;
  isEffectiveIdentityRequired: boolean;
  enableRLS: boolean;
  username: string;
  roles: string;
  errorMessage: string;
}
