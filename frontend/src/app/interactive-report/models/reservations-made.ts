export class ReservationsMadeDTo {
  reportReservationsMadeReturnDtoList: {
    items: Array<ReservationsMade>;
  };
  reportReservationsMadeTotalizerReturnDtoList: Array<ReservationsMadeStatus>;
}

export class ReservationsMadeStatus {
  reservationStatus: string;
  total: number;
}

export class ReservationsMade {
  guestName: string;
  guestType: string;
  uh: string;
  abbreviation: string;
  checkinDate: string;
  checkoutDate: string;
  adultsAndChildren: string;
  reservationCode: string;
  statusName: string;
  mealPlan: string;
  reservoir: string;
  tradeName: string;
  userName: string;
}

export class ReservationsMadeFilter {
  Canceled: string;
  ToConfirm: string;
  Confirmed: string;
  Checkin: string;
  Checkout: string;
  Pending: string;
  NoShow: string;
  WaitList: string;
  ReservationProposal: string;
  InitialDate: string;
  EndDate: string;
}
