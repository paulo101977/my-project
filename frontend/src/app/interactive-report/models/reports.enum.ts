export enum RoomAvailabilityEnum {
  Vague = 1,
  Busy = 2,
  Blocked = 3
}

export enum HousekeepingRoomDisagreementEnum {
  Vague = 16,
  Busy = 17,
  Blocked = 18
}

export enum HousekeepingRoomDisagreementBagEnum {
  None = 0,
  Few = 1,
  Much = 2
}

export enum HousekeepingRoomInspectionEnum {
  DidNotWantTo = 1,
  DoNotDisturb = 2,
  ComeBackLater = 3
}

export enum ReportRouterEnum {
  None = 0,
  GuestInTheRouse = 1,
  Checkout = 2,
  Checkin = 3,
  GuestSales = 4,
  IssuedNotes = 5,
  RoomSituation = 6,
  Bordereau = 7,
  Occupancy = 8,
  ReservationsMade = 9,
  Governance = 10,
  ReservationsInAdvance = 11,
  PensionPrevision = 12,
  BillingAccountTransfer = 13
}
