import {EmbedConfigDto} from 'app/interactive-report/models/dashboard';

export class PowerBIDashboard {
  powerBIDashboardId: string;
  name: string;
  embedUrl: string;
  tenantId: string;
  embedConfigDto: EmbedConfigDto;
}


