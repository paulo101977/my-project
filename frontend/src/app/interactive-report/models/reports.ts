

export enum PrintMealList {
  all = 0,
  coffe = 1,
  lunch = 2,
  dinner = 3
}

export class UhSituationModel {
  id: number;
  uh: string;
  uhSituation: string;
  typeUH: string;
  housekeepingStatus: string;
  wing: string;
  floor: string;
  adultsAndChildren: string;
  guestName: string;
  arrivalDate: string;
  departureDate: string;
  company: string;
  mealPlan: string;
  observation: string;
  isVIP: boolean;
}

export class GuestInTheHouse {
  id: number;
  uh: string;
  typeUH: string;
  guestTypeName: string;
  reservationCode: string;
  adultsAndChildren: string;
  adult: number;
  child: number;
  guestName: string;
  client: string;
  mealPlanTypeAmount: number;
  mealPlan: string;
  mealPlanType: string;
  dailyAmount: number;
  arrivalDate: string;
  departureDate: string;
  isInternalUsage: boolean;
  isCourtesy: boolean;
}

export class CheckinCheckout {
  id: number;
  guestName: string;
  guestTypeName: string;
  uh: string;
  typeUH: string;
  arrivalDate: string;
  status: string;
  departureDate: string;
  adultsAndChildren: string;
  reservationCode: string;
  mealPlan: string;
  mealPlanType: string;
  ensuresNoShow: boolean;
  deposit: number;
  userName: string;
  externalComments: string;
  internalComments: string;
  partnerComments: string;
}

export class IssuedNotes {
  billingAccountId: string;
  uh: string;
  reservationNumber: string;
  accountName: string;
  type: string;
  rps: number;
  emissionDate: string;
  status: string;
  service: number;
  rate: number;
  iss: number;
  typeOfPayment: number;
  pointOfSale: number;
  recipientsName: string;
  externalNumber: string;
  externalSeries: string;
  total: number;
}

export class ReportControl {
  guestMealPlanControlId: string;
  guestReservationItemId: number;
  uh: string;
  mealPlanTypeName: string;
  mealPlanTypeId: number;
  propertyGuestTypeId: number;
  fullName: string;
  guestTypeName: string;
  type: string;
  isChild: boolean;
  coffee: boolean;
  lunch: boolean;
  dinner: boolean;
}

export class ReportControlPost {
  guestMealPlanControlId: string;
  guestReservationItemId: number;
  coffee: boolean;
  lunch: boolean;
  dinner: boolean;
  guestMealPlanControlDate: string;
  propertyId: number;
}

export class ReservationsInAdvance {
  reservationCode: string;
  uh: string;
  abbreviation: string;
  paymentTypeName: string;
  amountFormatted?: string;
  billingAccountName: string;
  guestName: string;
  statusName: string;
  arrivalDate: string;
  arrivalDateFormat?: Date;
  departureDate: string;
  departureDateFormat?: Date;
  billingAccountItemDate: string;
  billingAccountItemDateFormat?: Date;
  currencySymbol?: string;
}

