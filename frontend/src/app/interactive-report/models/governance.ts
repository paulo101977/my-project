import {
  HousekeepingRoomDisagreementBagEnum,
  HousekeepingRoomDisagreementEnum, HousekeepingRoomInspectionEnum
} from 'app/interactive-report/models/reports.enum';

export class GovernanceModel {
  id: string;
  housekeepingStatusId: string;
  housekeepingStatusPropertyId: string;
  color: string;
  housekeepingStatusName: string;
  roomId: string;
  roomNumber: string;
  statusRoom: string;
  roomTypeName: string;
  checkInDate: string;
  checkOutDate: string;
  quantityDouble: number;
  quantitySingle: number;
  extraCribCount: number;
  extraBadCount: number;
  adultCount: number;
  childCount: number;
  reservationItemStatusId: string;
  housekeepingStatusLastModificationTime: string;
  blocked: boolean;
  wing: string;
  floor: string;
  building: string;
  housekeepingRoomDisagreementId: string;
  housekeepingRoomDisagreementStatusId: string;
  housekeepingRoomDisagreementStatus: string;
  disagreementAdultCount: number;
  bagCount: string;
  bag: string;
  observation: string;
  loggedUserUid: string;
  disagreementRoomId: string;
  housekeepingRoomInspectionId: string;
  housekeepingRoomInspection: string;
  totalDays: string;
  date: string;
}


export class HousekeepingRoomDisagreementEnumClass {
  public static readonly Blocked = {
    color: '#FF6969',
    cls: 'mdi-lock'
  };

  public static readonly Vague = {
    color: '#1DC29D',
    cls: 'mdi-account-outline'
  };

  public static readonly Busy = {
    color: '#4A4A4A',
    cls: 'mdi-account'
  };
}

export class HousekeepingRoomDisagreementBagEnumClass {
  public static readonly None = {
    color: '#075C72',
    cls: 'mdi-close',
    text: 'text.none'
  };

  public static readonly Few = {
    color: '#00729F',
    cls: 'mdi-briefcase',
    text: 'text.few'
  };
  public static readonly Much = {
    color: '#F5A623',
    cls: 'mdi-briefcase',
    text: 'text.much'
  };
}

export class HousekeepingRoomInspectionEnumClass {
  public static readonly DidNotWantTo = {
    color: '#075C72',
    cls: 'mdi-information-outline',
    text: 'text.didNotWantTo'
  };
  public static readonly DoNotDisturb = {
    color: '#FF6969',
    cls: 'mdi-block-helper',
    text: 'text.doNotDisturb'
  };
  public static readonly ComeBackLater = {
    color: '#F5A623',
    cls: 'mdi-history',
    text: 'text.comeBackLater'
  };
}
