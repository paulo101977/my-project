import {PowerBIDashboard} from 'app/interactive-report/models/powerBIDashboard';
import {PowerBIReport} from 'app/interactive-report/models/PowerBIReport';

export class Dashboard {
  powerBiGroupDtoList: [
    {
      groupId: string;
      groupName: string;
      powerBIDashboardDtoList: Array<PowerBIDashboard>;
      powerBIReportDtoList: Array<PowerBIReport>;
    }];
}


export class EmbedToken {
  token: string;
  tokenId: string;
  expiration: any;
}

export class EmbedConfigDto {
  id: string;
  embedToken: EmbedToken;
  minutesToExpiration: number;
  enableRLS: boolean;
  embedUrl: string;
}
