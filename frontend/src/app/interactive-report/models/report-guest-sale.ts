export class ReportGuestSale {
  // TODO mudar para o inglês quando o backend alterar
  propertyId: number;
  roomNumber: string;
  numReserva: string;
  nomeConta: string;
  nomeHotel: string;
  tenantId: string;
  tipoConta: string;
  restaurante: number;
  outros: number;
  bares: number;
  diaria: number;
  eventos: number;
  banquetes: number;
  lavandeira: number;
  telecomunicacoes: number;
  iss: number;
  taxadeServico: number;
  creditos: number;
  total: number;
}
