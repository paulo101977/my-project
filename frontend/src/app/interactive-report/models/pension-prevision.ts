export interface ReportPensionPrevision {
  date: string;
  propertyId: number;
  totalOccupied: number;
  totalCheckIn: number;
  totalCheckOut: number;
  totalAdult: number;
  totalChild?: number;
  totalChild2?: number;
  totalChild3?: number;
  mealPlan: MealPlan[];
}

export interface MealPlan {
  mealPlanType: number;
  mealPlanTypeName: string;
  totalAdult: number;
  totalChild: number;
  totalChild2: number;
  totalChild3: number;
  totalMealPlan: number;
}

