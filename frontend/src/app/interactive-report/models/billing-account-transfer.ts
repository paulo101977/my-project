export class BillingAccountTransfer {
  billingAccountItemId: string;
  billingItemName: string;
  billingAccountOriginal: string;
  billingAccountSource: string;
  billingAccountDestination: string;
  billingAccountItemType: string;
  amountFormatted: string;
  creationTime: string;
  userName: string;
  reservationItemCodeSource: string;
  reservationItemCodeDestination: string;
}

export const billingAccountTransferDataList: BillingAccountTransfer[] = [
  {
    billingAccountItemId: '1885fef3-61a8-4e2d-9f87-82de161d0ee8',
    billingItemName: 'Diaria',
    billingAccountOriginal: 'Igor Maciel',
    billingAccountSource: 'Igor Maciel',
    billingAccountDestination: 'Pré-Conta',
    billingAccountItemType: 'Débito',
    amountFormatted: 'R$ -100',
    creationTime: '2019-09-10T10:39:59',
    userName: 'thex@totvscmnet.com.br',
    reservationItemCodeSource: 'E3VU2Z7CX-0001',
    reservationItemCodeDestination: 'E3VU2Z7CX-0001'
  },
  {
    billingAccountItemId: '1885fef3-61a8-4e2d-9f87-82de161d0ee8',
    billingItemName: 'Diaria',
    billingAccountOriginal: 'Igor Maciel',
    billingAccountSource: 'Pré-Conta',
    billingAccountDestination: 'Serrão não Lança Diaria',
    billingAccountItemType: 'Débito',
    amountFormatted: 'R$ -100',
    creationTime: '2019-09-10T10:40:59',
    userName: 'thex@totvscmnet.com.br',
    reservationItemCodeSource: 'E3VU2Z7CX-0001',
    reservationItemCodeDestination: 'ISNPAS5D1-0001'
  },
  {
    billingAccountItemId: '1885fef3-61a8-4e2d-9f87-82de161d0ee8',
    billingItemName: 'Diaria',
    billingAccountOriginal: 'Igor Maciel',
    billingAccountSource: 'Serrão não Lança Diaria',
    billingAccountDestination: 'Totvs Short 22',
    billingAccountItemType: 'Débito',
    amountFormatted: 'R$ -100',
    creationTime: '2019-09-10T10:41:35',
    userName: 'thex@totvscmnet.com.br',
    reservationItemCodeSource: 'ISNPAS5D1-0001',
    reservationItemCodeDestination: 'ISNPAS5D1-0001'
  },
  {
    billingAccountItemId: '1885fef3-61a8-4e2d-9f87-82de161d0ee8',
    billingItemName: 'Diaria',
    billingAccountOriginal: 'Igor Maciel',
    billingAccountSource: 'Totvs Short 22',
    billingAccountDestination: 'Teste Transferência',
    billingAccountItemType: 'Débito',
    amountFormatted: 'R$ -100',
    creationTime: '2019-09-10T10:45:18',
    userName: 'thex@totvscmnet.com.br',
    reservationItemCodeSource: 'ISNPAS5D1-0001',
    reservationItemCodeDestination: 'PASM6B8L9-0001'
  },
  {
    billingAccountItemId: '40663bf0-4e27-4071-bccf-ff27dde2e6ed',
    billingItemName: 'Diaria',
    billingAccountOriginal: 'Serrão não Lança Diaria',
    billingAccountSource: 'Serrão não Lança Diaria',
    billingAccountDestination: 'Teste Transferência',
    billingAccountItemType: 'Débito',
    amountFormatted: 'R$ -100',
    creationTime: '2019-09-10T11:55:28',
    userName: 'thex@totvscmnet.com.br',
    reservationItemCodeSource: 'ISNPAS5D1-0001',
    reservationItemCodeDestination: 'PASM6B8L9-0001'
  }
];
