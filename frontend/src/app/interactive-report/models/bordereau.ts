export class Bordereau {
  tenantId: string;
  properyId: number;
  accountName: string;
  billingAccountName: string;
  uh: string;
  reservationItemCode: string;
  fullName: string;
  guestName: string;
  billingItemName: string;
  amount: number;
  originalAmount: number;
  checkNumber: string;
  nsu: string;
  billingAccountItemDate: string;
  name: string;
  paymentTypeId: number;
  paymentTypeName: string;
  acquirerName: string;
}
