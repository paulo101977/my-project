import { Bordereau } from 'app/interactive-report/models/bordereau';

export class BordereauGroup {
  billingItemName: string;
  items: Array<Bordereau>;
  grossAmount: number;
  netAmount: number;
  reversedAmount: number;
  currencySymbol?: string;
}
