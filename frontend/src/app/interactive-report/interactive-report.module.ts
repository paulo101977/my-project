import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './components-container/reports/reports.component';
import { InterativeReportRoutingModule } from './interative-report-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MenuComponent } from './components/menu/menu.component';
import { ReportsRouterComponent } from './components-container/reports-router/reports-router.component';
import { ReportsGuestInTheHouseComponent } from './components-container/reports-guest-in-the-house/reports-guest-in-the-house.component';
import { PensionControlComponent } from './components/pension-control/pension-control.component';
import { PensionControlItemComponent } from './components/pension-control-item/pension-control-item.component';
import { PensionControlCheckboxComponent } from './components/pension-control-checkbox/pension-control-checkbox.component';
import { GuestsInTheHouseComponent } from './components/guests-in-the-house/guests-in-the-house.component';
import { GuestSalesComponent } from './components/guest-sales/guest-sales.component';
import { CheckoutCheckinComponent } from './components-container/checkout-checkin/checkout-checkin.component';
import { IssuedNotesComponent } from './components-container/issued-notes/issued-notes.component';
import { RoomSituationComponent } from './components-container/room-situation/room-situation.component';
import { RoomAvailabilityReportEnumPipe } from './pipes/room-availability-report-enum.pipe';
import { BordereauComponent } from './components-container/bordereau/bordereau.component';
import { OccupancyComponent } from './components-container/occupancy/occupancy.component';
import { ReservationsMadeComponent } from './components-container/reservations-made/reservations-made.component';
import { GovernanceStatusComponent } from './components-container/governance-status/governance-status.component';
import { GovernanceStatusItemComponent } from './components/governance-status-item/governance-status-item.component';
import { DashboardComponent } from './components-container/powerbi/dashboard/dashboard.component';
import { PowerBiReportComponent } from './components-container/powerbi/power-bi-report/power-bi-report.component';

import {
  ReservationsInAdvanceComponent
} from 'app/interactive-report/components/reservations-in-advance/reservations-in-advance.component';
import {
  PrintableBordereauGroupReportComponent
} from 'app/interactive-report/components/printable-bordereau-group-report/printable-bordereau-group-report.component';
import { PrintableIssuedNotesComponent } from './components/printable-issued-notes/printable-issued-notes.component';
import { SibaIntegrationModule } from 'app/siba-integration/siba-integration.module';
import { PensionPrevisionComponent } from './components/pension-prevision/pension-prevision.component';
import { BillingAccountTransferComponent } from './components/billing-account-transfer/billing-account-transfer.component';


@NgModule({
  imports: [
    CommonModule,
    InterativeReportRoutingModule,
    SharedModule,
    SibaIntegrationModule
  ],
  declarations: [
    ReportsComponent,
    MenuComponent,
    ReportsRouterComponent,
    ReportsGuestInTheHouseComponent,
    PensionControlComponent,
    PensionControlItemComponent,
    PensionControlCheckboxComponent,
    GuestsInTheHouseComponent,
    GuestSalesComponent,
    CheckoutCheckinComponent,
    IssuedNotesComponent,
    RoomSituationComponent,
    RoomAvailabilityReportEnumPipe,
    BordereauComponent,
    OccupancyComponent,
    ReservationsMadeComponent,
    GovernanceStatusComponent,
    GovernanceStatusItemComponent,
    DashboardComponent,
    PowerBiReportComponent,
    PrintableBordereauGroupReportComponent,
    ReservationsInAdvanceComponent,
    PrintableIssuedNotesComponent,
    PensionPrevisionComponent,
    BillingAccountTransferComponent
  ]
})
export class InteractiveReportModule { }
