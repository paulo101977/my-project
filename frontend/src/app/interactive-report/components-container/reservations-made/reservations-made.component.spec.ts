import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationsMadeComponent } from './reservations-made.component';
import {  NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { createAccessorComponent, currencyFormatPipe } from '../../../../../testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { reportsResourceStub } from 'app/interactive-report/resources/testing';
import { sessionParameterServiceStub } from 'app/shared/services/parameter/testing';
import { currencyExchangeResourceStub } from 'app/shared/resources/currency-exchange/testing';
import { printHelperServiceStub } from 'app/shared/services/shared/testing/print-helper-service';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';

const thxDate = createAccessorComponent('thx-date-range-picker');

describe('ReservationsMadeComponent', () => {
  let component: ReservationsMadeComponent;
  let fixture: ComponentFixture<ReservationsMadeComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        ReservationsMadeComponent,
        thxDate,
        currencyFormatPipe,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {property: 1}
            }
          }
        },
        sharedServiceStub,
        reportsResourceStub,
        sessionParameterServiceStub,
        currencyExchangeResourceStub,
        printHelperServiceStub,
        dateServiceStub,
      ]
    });

    fixture = TestBed.createComponent(ReservationsMadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
