import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TableColumn } from '@swimlane/ngx-datatable/src/types/table-column.type';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { ActivatedRoute } from '@angular/router';
import { ReportsResource } from 'app/interactive-report/resources/reports.resource';
import { TranslateService } from '@ngx-translate/core';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { CurrencyExchangeResource } from 'app/shared/resources/currency-exchange/currency-exchange.resource';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { ReportRouterEnum } from 'app/interactive-report/models/reports.enum';
import {
  ReservationsMade,
  ReservationsMadeFilter,
  ReservationsMadeStatus
} from 'app/interactive-report/models/reservations-made';

@Component({
  selector: 'app-reservations-made',
  templateUrl: './reservations-made.component.html',
  styleUrls: ['./reservations-made.component.css']
})
export class ReservationsMadeComponent implements OnInit {

  private propertyId: number;
  public symbol: string;
  public title: string;

  public passingByList: Array<any>;

  // Form dependencies
  public form: FormGroup;
  public checkboxOptions: any[];

  // Table dependencies
  public columns: TableColumn[];
  public pageItemsList: ReservationsMade[];
  public totalIItemsList: Array<ReservationsMadeStatus>;
  public totalColumns: Array<any>;

  // Button dependencies
  public btnConfigSearch: ButtonConfig;

  @ViewChild('table', {read: ElementRef}) componentToPrint: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private reportResource: ReportsResource,
    private translateService: TranslateService,
    private sessionParameterService: SessionParameterService,
    private currencyService: CurrencyExchangeResource,
    private printService: PrintHelperService,
    private currencyFormatPipe: CurrencyFormatPipe,
    private sharedService: SharedService,
    private dateService: DateService,
  ) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.title = this.translateService.instant('title.reservationsMade');
    this.setForm();
    this.setColumns();
    this.setTotalColums();
    this.setPasssingByList();
    this.setBtnConfig();
  }

  private setPasssingByList() {
    this.passingByList = [
      {
        key: false,
        value: 'label.made',
      },
      {
        key: true,
        value: 'label.goingThrough',
      }
    ];
  }

  private setForm() {
    this.setCheckboxOptions();
    const controls = this.checkboxOptions.map(() => new FormControl(false));
    this.form = this.formBuilder.group({
      reportType: ReportRouterEnum.ReservationsMade,
      passingBy: [ false, Validators.required ],
      period: [ null, Validators.required ],
      options: this.formBuilder.array(controls)
    });
  }

  get options() {
    return <FormArray>this.form.get('options');
  }

  private setCheckboxOptions() {
    this.checkboxOptions = [
      {id: 'Canceled', name: 'reservationModule.reserveList.reservationStatusEnum.Canceled'},
      {id: 'ToConfirm', name: 'reservationModule.reserveList.reservationStatusEnum.ToConfirm'},
      {id: 'Confirmed', name: 'reservationModule.reserveList.reservationStatusEnum.Confirmed'},
      {id: 'Checkin', name: 'reservationModule.reserveList.reservationStatusEnum.Checkin'},
      {id: 'Checkout', name: 'reservationModule.reserveList.reservationStatusEnum.Checkout'},
      {id: 'Pending', name: 'reservationModule.reserveList.reservationStatusEnum.Pending'},
      {id: 'NoShow', name: 'reservationModule.reserveList.reservationStatusEnum.NoShow'},
      {id: 'WaitList', name: 'reservationModule.reserveList.reservationStatusEnum.WaitList'},
      {id: 'ReservationProposal', name: 'reservationModule.reserveList.reservationStatusEnum.ReservationProposal'},
    ];
  }

  private setBtnConfig() {
    this.btnConfigSearch = this.sharedService.getButtonConfig(
      'search',
      this.search,
      'action.list',
      ButtonType.Primary,
      ButtonSize.Normal
    );
  }

  private setColumns() {
    this.columns = [
      {name: 'label.guestName', prop: 'guestName'},
      {name: 'label.guestType', prop: 'guestType'},
      {name: 'label.uh', prop: 'uh'},
      {name: 'label.abbreviation', prop: 'abbreviation'},
      {name: 'label.arrival', prop: 'checkinDate'},
      {name: 'label.departure', prop: 'checkoutDate'},
      {name: 'label.adultsAndChildren', prop: 'adultsAndChildren'},
      {name: 'label.reservationSingle', prop: 'reservationCode'},
      {name: 'label.status', prop: 'statusName'},
      {name: 'label.mealPlan', prop: 'mealPlan'},
      {name: 'label.reservoir', prop: 'reservoir'},
      {name: 'label.company', prop: 'tradeName'},
      {name: 'label.user', prop: 'userName'}
    ];
  }

  private setTotalColums() {
    this.totalColumns = [
      {
        name: 'label.totals',
        prop: 'reservationStatus',
        printableAlignRight: false
      },
      {
        name: 'label.value',
        prop: 'total',
        printableAlignRight: false
      }
    ];
  }

  private search = () => {
    const passingBy = this.form.get('passingBy').value;
    const filter = this.setFilters();
    this.reportResource.getAllReservationsMade(this.propertyId, filter, passingBy)
      .subscribe(({reportReservationsMadeReturnDtoList, reportReservationsMadeTotalizerReturnDtoList}) => {
        this.pageItemsList = this.formatList(reportReservationsMadeReturnDtoList.items);
        this.totalIItemsList = reportReservationsMadeTotalizerReturnDtoList;
      });

    this.setTitleToPrintTable();
  }

  private setFilters() {
    const filter = new ReservationsMadeFilter();
    this.checkboxOptions.forEach((o, i) => {
      filter[o.id] = this.form.get('options').value[i];
    });
    const {value} = this.form.get('period');
    if (value) {
      filter.InitialDate = value.beginDate;
      filter.EndDate = value.endDate;
    }
    return filter;
  }

  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  private formatList(list: ReservationsMade[]) {
    return list.map(item => {
      item.checkinDate = this.dateService.convertUniversalDateToViewFormat(item.checkinDate);
      item.checkoutDate = this.dateService.convertUniversalDateToViewFormat(item.checkoutDate);
      return item;
    });
  }

  private setTitleToPrintTable = () => {
    this.title = `${this.translateService.instant('title.reservationsMade')}`;
    const { value } = this.form.get('period');
    if (value) {
      this.title += ` ${this.dateService.convertUniversalDateToViewFormat(value.beginDate)} ` +
        `${this.translateService.instant('text.until')} ` +
        `${this.dateService.convertUniversalDateToViewFormat(value.endDate)}`;
    }
  }

}
