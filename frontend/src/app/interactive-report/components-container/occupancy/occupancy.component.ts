import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AvailabilityGridPeriodEnum } from 'app/shared/models/availability-grid/availability-grid-period.enum';
import { AvailabilityGridFilter } from 'app/shared/models/availability-grid/availability-grid-filter';
import { DateService } from 'app/shared/services/shared/date.service';
import * as moment from 'moment';
import { Room } from '@inovacao-cmnet/thx-ui';
import { ActivatedRoute } from '@angular/router';
import { AvailabilityGridResource } from 'app/shared/resources/availability-grid/availability-grid.resource';
import { ButtonBarButtonStatus } from 'app/shared/components/button-bar/button-bar-button-status.enum';
import { ButtonBarButtonConfig } from 'app/shared/components/button-bar/button-bar-button-config';
import { ButtonBarColor } from 'app/shared/components/button-bar/button-bar-color.enum';
import { ButtonBarConfig } from 'app/shared/components/button-bar/button-bar-config';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { AvailabilityResourcesDto } from 'app/shared/models/dto/availability-grid/availability-resources-dto';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { TableColumn } from '@swimlane/ngx-datatable';
import { ReportRouterEnum } from 'app/interactive-report/models/reports.enum';

@Component({
  selector: 'app-occupation',
  templateUrl: './occupancy.component.html',
  styleUrls: ['./occupancy.component.css']
})
export class OccupancyComponent implements OnInit {

  constructor(public route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private dateService: DateService,
              public occupancyResource: AvailabilityGridResource,
              private sharedService: SharedService,
              private printService: PrintHelperService) {
  }

  @ViewChild('printTable', {read: ElementRef}) componentToPrint: ElementRef;

  public occupancyForm: FormGroup;
  public roomList: Room[];

  public startDate: string;
  public days: number;
  public propertyId: number;
  public buttonBarConfig: ButtonBarConfig;
  public consultButton: ButtonConfig;

  public items: Array<any>;
  public columns: Array<TableColumn>;
  public props: Array<any>;

  public printButton: ButtonConfig;

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setAvailabilityGridFilterForm();
    this.setButtonBarConfig();
    this.setButtonConfig();
  }

  private setButtonBarConfig() {
    this.buttonBarConfig = {
      color: ButtonBarColor.black,
      buttons: [
        {
          id: AvailabilityGridPeriodEnum.weekly,
          title: 'propertyModule.availabilityGrid.periodTypes.weekly',
          status: ButtonBarButtonStatus.active,
          callBackFunction: () => this.setPeriod(AvailabilityGridPeriodEnum.weekly),
        } as ButtonBarButtonConfig,
        {
          title: 'propertyModule.availabilityGrid.periodTypes.fortnightly',
          callBackFunction: () => this.setPeriod(AvailabilityGridPeriodEnum.fortnightly),
        } as ButtonBarButtonConfig,
        {
          title: 'propertyModule.availabilityGrid.periodTypes.monthly',
          callBackFunction: () => this.setPeriod(AvailabilityGridPeriodEnum.monthly),
          cssClass: 'hidden-sm-down',
        } as ButtonBarButtonConfig,
      ],
    };
  }

  private setButtonConfig() {
    this.consultButton = this.sharedService
      .getButtonConfig('consultButton',
        this.consult,
        'action.list',
        ButtonType.Primary
    );

    this.printButton = this.sharedService
      .getButtonConfig('print-button',
        this.print,
        'action.print',
        ButtonType.Primary
      );
  }

  public consult = () => {
      this.filter(this.occupancyForm);
  }

  private setPeriod(period: number) {
    this.occupancyForm.patchValue({ period: period });
    this.buttonBarConfig.buttons.map(
      button => (button.status = button.id == period ? ButtonBarButtonStatus.active : ButtonBarButtonStatus.default),
    );
  }

  private setAvailabilityGridFilterForm() {
    const today = new Date();
    this.occupancyForm = this.formBuilder.group({
      reportType: ReportRouterEnum.Occupancy,
      initialDate: [today, [Validators.required]],
      period: [AvailabilityGridPeriodEnum.weekly, [Validators.required]],
    });

    this.filter(this.occupancyForm);
  }

  public filter({value}: { value: AvailabilityGridFilter }) {
    const myData = this.dateService.convertUniversalDateToIMyDate(value.initialDate);
    const year = myData.date.year;
    const month = myData.date.month;
    const day = myData.date.day;

    const initialDate = new Date(year, month - 1, day);

    this.startDate = moment(initialDate)
      .format(DateService.DATE_FORMAT_UNIVERSAL)
      .toString();
    this.days = value.period;
    this.getRoomTypes(this.propertyId, this.startDate, this.days == 30 ? this.dateService.getTotalDaysFromMonth(initialDate) : this.days);
  }

  public getRoomTypes(propertyId: number, startDate: string, days: number) {
    this.occupancyResource
      .getRoomTypes(propertyId, startDate, days).subscribe(data => {
        this.items = this.prepareList(data);
        this.setColumns(data);
    });
  }

  private reduceArray(array: Array<any>, propNameKey: string, propNameValue: string, changes: Function) {
      return array
        .reduce( (prev, curr) => {
          const i = {...prev};
          let key = curr[propNameKey];
          if (changes) {
            key = changes(key);
          }
          i[key] = curr[propNameValue] ;
          return i;
        }, {});
  }

  public prepareList(availabilityDto: AvailabilityResourcesDto) {
    const items = [];
    for ( const item of availabilityDto.roomTypeList ) {
      const filteredList = availabilityDto.availabilityList
        .filter( val => val.roomTypeId === item.id);
      const daysForUh = this.reduceArray(filteredList,
        'dateFormatted',
        'availableRooms',
        null);

      items.push({...item, ...daysForUh});
    }
    for ( const item of availabilityDto.footerList ) {
      const f = (val) => {
        return `${val}T00:00:00`;
      };
      const daysForUh = this.reduceArray(item.values,
        'date',
        'number',
        f);

      items.push({...item, ...daysForUh});
    }
    return items;
  }

  public setColumns(info) {
    this.columns = [
      { name: 'label.uhType', prop: 'name', width: 300  },
    ];
    if (info && info.hasOwnProperty('availabilityList')) {
      info.availabilityList.forEach( item => {
        const itemDate = moment(item.dateFormatted, DateService.DATE_FORMAT_UNIVERSAL);
        const formDate = moment(this.occupancyForm.get('initialDate').value, DateService.DATE_FORMAT_UNIVERSAL);
        formDate.startOf('day');
        const periodFinal = moment(formDate.toDate());

        let periodType = this.occupancyForm.get('period').value;
        periodType = periodType == 30 ? this.dateService.getTotalDaysFromMonth(formDate.toDate()) : periodType;
        periodFinal.add(periodType, 'days');
        if (!this.columns.find( col => col.prop == item.dateFormatted)
          && itemDate.isBetween(formDate, periodFinal, null, '[)') ) {
          this.columns.push(
            { name: moment(item.dateFormatted, DateService.DATE_FORMAT_UNIVERSAL).format('DD/MM'),
              prop: item.dateFormatted,
            });
        }
      });
    }

    this.props = this.columns.map( item => item.prop);
  }

  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }
}
