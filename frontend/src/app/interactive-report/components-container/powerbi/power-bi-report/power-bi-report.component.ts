import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PowerbiService} from '../services/powerbi.service';
import {PowerBIReport} from 'app/interactive-report/models/PowerBIReport';
import {Location} from '@angular/common';

import { models, service, factories } from 'powerbi-client';

@Component({
  selector: 'app-power-bi-report',
  templateUrl: './power-bi-report.component.html',
  styleUrls: ['./power-bi-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class PowerBiReportComponent implements OnInit {
  public token: string;
  public embedUrl: string;
  public requestId: string;
  public pageTitle: string;
  private report;

  constructor(
    private powerBiService: PowerbiService,
    private location: Location
  ) {
  }

  ngOnInit() {
    const selectedPowerBi: PowerBIReport = this.powerBiService.getPowerBi();
    if (selectedPowerBi) {
      this.embedUrl = selectedPowerBi.embedUrl + '&filterPaneEnabled=false';
      this.token = selectedPowerBi.embedConfigDto.embedToken.token;
      this.requestId = selectedPowerBi.powerBIReportId;
      this.pageTitle = selectedPowerBi.name;

      const embedConfiguration = {
        type: 'report',
        id: this.requestId,
        embedUrl: this.embedUrl,
        accessToken: this.token,
        tokenType: models.TokenType.Embed,
        pageView: 'fitToWidth',
      };

      const reportContainer = document.getElementById('power-bi');
      const pbiObject = new service.Service(factories.hpmFactory, factories.wpmpFactory, factories.routerFactory);
      this.report = pbiObject.embed(reportContainer, embedConfiguration);

    } else {
      this.goBackPreviewPage();
    }
  }

  public printReport() {
    this.report.print().catch(error => console.log('error > ', error));
  }

  public goBackPreviewPage() {
    this.location.back();
  }
}
