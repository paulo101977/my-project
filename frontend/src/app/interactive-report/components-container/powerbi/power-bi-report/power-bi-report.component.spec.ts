// import {
//   async,
//   ComponentFixture,
//   TestBed,
// } from '@angular/core/testing';

// // common and necessary imports
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { PowerBiReportComponent} from './power-bi-report.component';
// import {DATA, SELECTED_POWER_BI_PROPERTY} from '../mock-data';
// import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
// import { configureTestSuite } from 'ng-bullet';
// import {Location} from '@angular/common';

// describe('PowerBiReportComponent', () => {
//   let component: PowerBiReportComponent;
//   let fixture: ComponentFixture<PowerBiReportComponent>;

//   configureTestSuite(() => {
//     TestBed.configureTestingModule({
//       declarations: [ PowerBiReportComponent ],
//       imports: [
//         TranslateTestingModule,
//       ],
//       providers: [
//         { provide: Location, useValue: { back: () => {} } }
//       ],
//       schemas: [NO_ERRORS_SCHEMA]
//     });

//     fixture = TestBed.createComponent(PowerBiReportComponent);
//     component = fixture.componentInstance;
//     spyOn(component['powerBiService'], 'getPowerBi').and.returnValue(DATA[SELECTED_POWER_BI_PROPERTY]);

//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it('should call init functions', done => {
//     spyOn(component, 'goBackPreviewPage').and.callFake(() => {} );

//     component.ngOnInit();

//     expect(component['powerBiService'].getPowerBi).toHaveBeenCalled();
//     expect(component.embedUrl).toEqual(`${DATA[SELECTED_POWER_BI_PROPERTY].embedUrl}&filterPaneEnabled=false`);
//     expect(component.token).toEqual(DATA[SELECTED_POWER_BI_PROPERTY].embedConfigDto.embedToken.token);
//     expect(component.requestId).toEqual(DATA[SELECTED_POWER_BI_PROPERTY].powerBIReportId);
//     expect(component.pageTitle).toEqual(DATA[SELECTED_POWER_BI_PROPERTY].name);
//     done();
//   });


// });
