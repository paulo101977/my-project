import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PowerbiService {
  private _selectedPowerBi;

  constructor() { }

  public getPowerBi () {
    return this._selectedPowerBi;
  }

  public setPowerBi (value) {
    this._selectedPowerBi = value;
  }

}
