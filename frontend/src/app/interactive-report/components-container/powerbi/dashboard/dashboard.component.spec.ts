import {
  ComponentFixture, fakeAsync,
  TestBed, tick
} from '@angular/core/testing';

// common and necessary imports
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DashboardComponent } from './dashboard.component';
import { of } from 'rxjs';
import { DATA, ITEMS_PROPERTY, SELECTED_POWER_BI_PROPERTY } from '../mock-data';
import { PowerBIReport } from 'app/interactive-report/models/PowerBIReport';
import { ActivatedRoute } from '@angular/router';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { powerbiResourceStub } from 'app/interactive-report/resources/testing';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { configureTestSuite } from 'ng-bullet';
import {NO_ERRORS_SCHEMA} from '@angular/core';


describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;


  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        FormsModule
      ],
      declarations: [
        DashboardComponent
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { params: { property: 1} }
          }
        },
        powerbiResourceStub,
        commomServiceStub,
        sharedServiceStub,
        dateServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });


    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call init functions', () => {
    spyOn(component, 'loadData');
    spyOn(component, 'setForm');

    component.ngOnInit();

    expect(component.loadData).toHaveBeenCalled();
    expect(component.setForm).toHaveBeenCalled();
  });

  it ('should loadData', fakeAsync(() => {

    spyOn<any>(component['powerBIResource'], 'getPowerBi').and.returnValue(of(DATA[ITEMS_PROPERTY]));

    component.loadData();
    tick();
    expect<any>(component.powerBiList[0]).toEqual(DATA[ITEMS_PROPERTY].powerBiGroupDtoList[0].powerBIReportDtoList[0]);
  }));

  it ('should configReport', () => {
    spyOn(component, 'configReport').and.callThrough();
    spyOn(component, 'addDateRangePickerToForm');
    spyOn(component, 'removeDatePickerToForm');

    component.reportData = <PowerBIReport>DATA[SELECTED_POWER_BI_PROPERTY];
    component.configReport();

    expect(component.removeDatePickerToForm).toHaveBeenCalled();
    expect(component.addDateRangePickerToForm).toHaveBeenCalled();
  });

  it('should add date in form', () => {
    component.addDatePickerToForm();

    expect(component.form.get('date')).toBeTruthy();
  });

  describe('configReport', () => {
    beforeEach(() => {
      spyOn(component, `addDatePickerToForm`).and.callThrough();
      component.reportData = DATA.REPORT_ITEM;

      component.configReport();
    });

    it('Should set true hasDate when report has Date ', () => {
      expect(component.hasDatePicker).toBe(true);
    });

    it('should call addDatePickerToForm when hasDate', () => {
      expect(component.addDatePickerToForm).toHaveBeenCalled();
    });

  });
});
