import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {PowerbiResource} from 'app/interactive-report/resources/powerbi.resource';
import {PowerBIReport} from 'app/interactive-report/models/PowerBIReport';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommomService} from 'app/shared/services/shared/commom.service';
import {PowerbiService} from '../services/powerbi.service';
import {ActivatedRoute, Router} from '@angular/router';
import { DashboardGroupField } from '../models/dashboard-group-field-enum';
import * as moment from 'moment';
import {ButtonConfig, ButtonSize, ButtonType} from 'app/shared/models/button-config';
import {SharedService} from 'app/shared/services/shared/shared.service';
import {DateService} from 'app/shared/services/shared/date.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public embedUrl: any;
  public form: FormGroup;
  public reportTypeList: Array<any>;
  public powerBiList: Array<PowerBIReport>;
  public reportData: PowerBIReport;
  public hasDateRangePicker = false;
  public hasDatePicker = false;
  public reportButtonConfig: ButtonConfig;
  public systemDate: any;
  public initialDate: any;
  public lastDate: any;
  public propertyId: number;

  constructor(
    private powerBIResource: PowerbiResource,
    private fb: FormBuilder,
    private commonService: CommomService,
    private cdRef: ChangeDetectorRef,
    private pbService: PowerbiService,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private dateService: DateService
  ) { }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setButton();
    this.setForm();
    this.loadData();
  }

  public setButton() {
    this.reportButtonConfig = this.sharedService.getButtonConfig(
      'save-new-user',
      this.viewReport,
      'label.generateReport',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  public loadData() {
    this.powerBIResource.getPowerBi().subscribe( ({ powerBiGroupDtoList } ) => {
      powerBiGroupDtoList.forEach((data) => {
        const {powerBIReportDtoList, powerBIDashboardDtoList} = data;
        if (powerBiGroupDtoList) {
          const powerBIDashboardDtoListArr = powerBIDashboardDtoList.map(item => {
            return {
              ...item,
              powerBIReportId: item.powerBIDashboardId
            };
          });

          const powerBiList = <Array<any>>powerBIReportDtoList
              .concat(<Array<any>>powerBIDashboardDtoListArr);

          const resultList = this
            .commonService
            .toOption(
              powerBiList,
              'powerBIReportId',
              'name'
            );

          if (!this.reportTypeList) {
            this.powerBiList = powerBiList;
            this.reportTypeList = resultList;
          } else {
            this.reportTypeList.concat(resultList);
            this.powerBiList.concat(powerBiList);
          }
        }
      });
    });
  }

  public changeRoute() {
    this.router.navigate(
      ['power-bi-report'],
      { relativeTo: this.route }
    );
  }

  public setForm() {
    this.form = this.fb.group({
      reportId: [ null, Validators.required ],
    });

    this
      .form
      .get('reportId')
      .valueChanges
      .subscribe(  value => {
        this.reportData =
          <PowerBIReport>this.powerBiList.find(
            item => item.powerBIReportId === value
          );
        this.configReport();
      });

    this
      .form
      .valueChanges
      .subscribe( () => {
        this.reportButtonConfig.isDisabled = this.validate();
    });
  }

  public viewReport = () => {
    const { period, date } = this.form.getRawValue();
    if ( period ) {
      const {beginDate, endDate} = period;

      const bDate = moment(beginDate).format('YYYY-MM-DD');
      const eDate = moment(endDate).format('YYYY-MM-DD');
      const periodStr = ` and dCalendar/DateKey ge ${ bDate } and dCalendar/DateKey le ${eDate}`;

      this.reportData.embedUrl = this.reportData.embedUrl + periodStr;
    }

    if ( date ) {
      const myDate = this.dateService.convertStringToDate(date, DateService.DATE_FORMAT_UNIVERSAL);
      const dateStr = ` and dCalendar/DateKey ge ${ moment(myDate).format('YYYY-MM-DD') }`;
      this.reportData.embedUrl = this.reportData.embedUrl + dateStr;
    }

    if (!this.reportButtonConfig.isDisabled) {
      this.pbService.setPowerBi(this.reportData);
      this.changeRoute();
    }
  }

  public validate() {
    const { form, hasDateRangePicker, hasDatePicker } = this;
    const { period , date } = form.getRawValue();
    return (!form.valid && hasDateRangePicker && period) || (!form.valid && hasDatePicker && date );
  }

  public addDateRangePickerToForm() {
    const { fb, form } = this;
    form.addControl(
      'period',
      fb.control(null, Validators.required)
    );
  }

  public addDatePickerToForm() {
    const { fb, form } = this;
    form.addControl(
      'date',
      fb.control(null, Validators.required)
    );
  }

  public getDateSystem() {
    let date = this.dateService.getSystemDate(this.propertyId);
    this.systemDate = this.dateService
      .convertDateToStringWithFormat(date, DateService.DATE_FORMAT_UNIVERSAL);
    if (!date || (date && isNaN(date.getDay()))) {
      date = moment().toDate();
    }
    const initial = moment( date ).startOf('months');
    const final = moment( date ).add(-1, 'days');
    if (final.isBefore(initial)) {
      final.add(1, 'days');
    }
    this.initialDate = initial.toDate();
    this.lastDate = final.toDate();
  }

  public removeDatePickerToForm() {
    this.form.removeControl('period');
    this.form.removeControl('date');
  }

  public configReport() {
    this.hasDateRangePicker = false;
    this.hasDatePicker = false;
    if ( this.reportData ) {
      const { reportData: { name } } = this;
      const dashBoard = <any>(<DashboardGroupField>DashboardGroupField[name.toUpperCase()
        .replace(' ', '_')]);
      if (dashBoard) {
        const { hasDateRangePicker, hasDatePicker } = dashBoard;
        this.hasDateRangePicker = hasDateRangePicker;
        this.hasDatePicker = hasDatePicker;
      }

      this.removeDatePickerToForm();

      if ( this.hasDateRangePicker ) {
        this.addDateRangePickerToForm();
        this.getDateSystem();
        const periodOfStay = this.dateService.getDateRangePicker(
          this.initialDate,
          this.lastDate
        );
        this.form.setControl('period', this.fb.control(periodOfStay)
        );
      }

      if ( this.hasDatePicker ) {
        this.addDatePickerToForm();
        this.getDateSystem();
        this.form.setControl('date', this.fb.control(this.systemDate));
      }
    }
  }
}
