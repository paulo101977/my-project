export class DashboardGroupField {
  public static readonly RDS = <any>{hasDateRangePicker: true};
  public static readonly REPORT_MANAGER = <any>{hasDatePicker: true};
}

export enum DashboardGroupFieldEnum {
  RDS = 'RDS',
  REPORT_MANAGER = 'Report Manager'
}
