import {EmbedConfigDto} from 'app/interactive-report/models/dashboard';

export const ITEMS_PROPERTY = 'ITEMS';
export const SELECTED_POWER_BI_PROPERTY = 'SELECTEDPOWERBI';

export class DATA {
  public static readonly ITEMS = {
    powerBiGroupDtoList: [
      {
        groupId: '0df75b7a-e174-49fd-843a-b45c35028ec4',
        groupName: 'Thex-Dev',
        powerBIDashboardDtoList: [
          {
            embedConfigDto: [{
              embedToken: [{
                token: 'H4sIAAAAAAAEAC2Wxa7FCnJF_-VNHcl'
              }]
            }],
            embedUrl: 'https://app.powerbi.com/dashboardEmbed?',
            name: 'RelatórioTHEx.xlsx',
            powerBIDashboardId: '25c59405-9726-4c46-9706-da6be851d487',
            tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9'
          }
        ],
        powerBIReportDtoList: [
          {
            embedConfigDto: [{
              embedToken: [{
                token: 'H4sIAAAAAAAEAC2Wxa7FCnJF_-VNHcl'
              }]
            }],
            embedUrl: 'https://app.powerbi.com/reportEmbed?',
            name: 'RDONEW_v3',
            powerBIDashboardId: '25c59405-9726-4c46-9706-da6be851d487',
            tenantId: 'fd869bfb-2d3d-4095-8894-858765c210be'
          }
        ]
      }
    ]
  };

  public static readonly SELECTEDPOWERBI = {
    embedConfigDto: {
      embedToken: {
        token: 'H4sIAAAAAAAEAC2Wxa7FCnJF_-VNHcl'
      }
    },
    embedUrl: 'https://app.powerbi.com/reportEmbed',
    name: 'RDS',
    powerBIReportId: 'B3BBBE8E-6CA5-42A0-8F41-FF65393249CD',
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9'
  };

  public static readonly REPORT_ITEM = {
    powerBIReportId: '1',
    name: 'report manager',
    embedUrl: 'url',
    tenantId: '1',
    webUrl: 'weburl',
    embedConfigDto: null,
    minutesToExpiration: 1000,
    isEffectiveIdentityRolesRequired: true,
    isEffectiveIdentityRequired: true,
    enableRLS: true,
    username: 'name',
    roles: 'roles',
    errorMessage: null
  };
}


