import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BordereauComponent } from './bordereau.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { reportsResourceStub } from 'app/interactive-report/resources/testing';
import { sessionParameterServiceStub } from 'app/shared/services/parameter/testing';
import { printHelperServiceStub } from 'app/shared/services/shared/testing/print-helper-service';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteStub, createAccessorComponent } from '../../../../../testing';

const thxDate = createAccessorComponent('thx-date-range-picker');

describe('BordereauComponent', () => {
  let component: BordereauComponent;
  let fixture: ComponentFixture<BordereauComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        ReactiveFormsModule,
        FormsModule,
      ],
      providers: [
        reportsResourceStub,
        sessionParameterServiceStub,
        printHelperServiceStub,
        sharedServiceStub,
        dateServiceStub,
        {provide: ActivatedRoute, useClass: ActivatedRouteStub},
      ],
      declarations: [
        BordereauComponent,
        thxDate,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(BordereauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
