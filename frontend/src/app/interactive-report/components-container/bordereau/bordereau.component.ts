import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ReportsResource } from '../../resources/reports.resource';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { TranslateService } from '@ngx-translate/core';
import { TableColumn } from '@swimlane/ngx-datatable/src/types/table-column.type';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateService } from 'app/shared/services/shared/date.service';
import { ActivatedRoute } from '@angular/router';
import { Bordereau } from '../../models/bordereau';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ReportRouterEnum } from '../../models/reports.enum';
import { BordereauGroup } from 'app/interactive-report/models/bordereau-group';

@Component({
  selector: 'app-bordereau',
  templateUrl: './bordereau.component.html',
  styleUrls: ['./bordereau.component.css']
})
export class BordereauComponent implements OnInit {

  private propertyId: number;
  public symbol: string;

  // Form dependencies
  public form: FormGroup;

  // Table dependencies
  public columns: TableColumn[];
  public props: any[];
  public pageItemsList: Bordereau[];
  public pageItemsGroupList: BordereauGroup[];
  public isSynthesise = false;
  public columnsSynthesise: TableColumn[];
  public propsSynthesise: any[];
  public periodInfo: string;

  // Button dependencies
  public btnConfigSearch: ButtonConfig;

  @ViewChild('table', {read: ElementRef}) componentToPrint: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private reportResource: ReportsResource,
    private translateService: TranslateService,
    private sessionParameterService: SessionParameterService,
    private printService: PrintHelperService,
    private sharedService: SharedService,
    private dateService: DateService
  ) {
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setForm();
    this.setColumns();
    this.setBtnConfig();
  }

  private setForm() {
    this.form = this.formBuilder.group({
      reportType: ReportRouterEnum.Bordereau,
      period: null,
      accountName: '',
      issuePayment: false,
      name: '',
      wasReversed: false,
      synthesise: false
    });

    this.form.get('synthesise').valueChanges.subscribe(value => {
      if (value) {
        this.form.get('accountName').disable();
        this.form.get('wasReversed').disable();
      } else {
        this.form.get('accountName').enable();
        this.form.get('wasReversed').enable();
      }
    });
  }

  private setBtnConfig() {
    this.btnConfigSearch = this.sharedService.getButtonConfig(
      'search',
      this.search,
      'action.list',
      ButtonType.Primary,
      ButtonSize.Normal
    );
  }

  private setColumns() {
    this.columns = [
      {name: 'label.accountName', prop: 'accountName'},
      {name: 'label.uh', prop: 'uh'},
      {name: 'label.reservationItemCode', prop: 'reservationItemCode'},
      {name: 'label.fullName', prop: 'fullName'},
      {name: 'label.billingItemName', prop: 'billingItemName'},
      {name: 'label.amount', prop: 'amountFormatted'},
      {name: 'label.checkNumber', prop: 'checkNumber'},
      {name: 'label.nsu', prop: 'nsu'},
      {name: 'label.billingAccountItemDate', prop: 'billingAccountItemDate'},
      {name: 'label.userName', prop: 'name'},
      {name: 'label.paymentTypeName', prop: 'paymentTypeName'}
    ];

    this.props = [
      'accountName',
      'uh',
      'reservationItemCode',
      'fullName',
      'billingItemName',
      'amountFormatted',
      'originalAmountFormatted',
      'checkNumber',
      'nsu',
      'billingAccountItemDate',
      'name',
      'paymentTypeName'
    ];

    this.columnsSynthesise = [
      {name: 'label.billingItemName', prop: 'billingItemName'},
      {name: 'label.amount', prop: 'amountFormatted'},
    ];

    this.propsSynthesise = [
      'billingItemName',
      'amountFormatted'
    ];

    if (this.form && (this.form.get('wasReversed') || {})['value']) {
      this.columns.splice(6, 0, {name: 'label.reasonReverse', prop: 'reasonName'});
      this.columns.splice(7, 0, {name: 'label.observation', prop: 'observation'});

      this.columns.splice(8, 2);
      this.props.splice(7, 2);

      this.columns.pop();
      this.props.pop();
    }
  }

  private setPeriodInfo() {
    const {form} = this;

    this.periodInfo = null;

    if (form) {
      const period = form.get('period').value;

      if (period) {
        let {beginDate, endDate} = period;

        beginDate = this.dateService.convertUniversalDateToViewFormat(beginDate);
        endDate = this.dateService.convertUniversalDateToViewFormat(endDate);

        this.periodInfo = this.translateService.instant(
          'text.periodOfLauncher',
          {beginDate, endDate}
        );
      }
    }
  }

  private search = () => {
    // need to add column reason
    this.setColumns();

    this.reportResource.getAllBordereau(this.propertyId, this.form.value)
      .subscribe((result) => {
        this.pageItemsList = this.formatList(result.listBordereau.items || []);
        this.pageItemsGroupList = this.formatGroupList(result.listBordereauPerGroup.items || []);
      });

    this.setPeriodInfo();
    this.viewTable();
  }

  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  private   formatList(list: Bordereau[]) {
    return list.map(item => {
      item['billingAccountItemDate'] = this.dateService.convertUniversalDateToViewFormat(item.billingAccountItemDate);
      item['paymentTypeName'] = item.acquirerName ? `${ item.paymentTypeName } (${ item.acquirerName })` : item.paymentTypeName;
      return item;
    });
  }

  private formatGroupList(list: Array<any>) {
    return list.map( item => {
      const group = new BordereauGroup();
      group.billingItemName = item.billingItemName;
      group.grossAmount = item.grossAmount;
      group.netAmount = item.netAmount;
      group.reversedAmount = item.reversedAmount;
      group.currencySymbol = item.currencySymbol;
      group.items = this.formatList(item.bordereauPerGroup.items || []);
      return group;
    });
  }

  private viewTable() {
    this.isSynthesise = this.form.get('synthesise').value;
  }

}
