import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportsResource } from '../../resources/reports.resource';
import { TranslateService } from '@ngx-translate/core';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { CurrencyExchangeResource } from 'app/shared/resources/currency-exchange/currency-exchange.resource';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { IssuedNotes } from '../../models/reports';
import { TableColumn } from '@swimlane/ngx-datatable/src/types/table-column.type';
import { DateService } from 'app/shared/services/shared/date.service';


@Component({
  selector: 'app-checkout',
  templateUrl: './issued-notes.component.html',
})
export class IssuedNotesComponent implements OnInit {

  private propertyId: number;
  public symbol: string;
  public reportGroup: FormGroup;

  // Table dependencies
  public columns: TableColumn[];
  public pageItemsList: IssuedNotes[];
  public listIssuedNotesProduct: any; // TODO: add model
  public listIssuedNotesService: any; // TODO: add model

  @ViewChild('table', { read: ElementRef }) componentToPrint: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private reportResource: ReportsResource,
    private translateService: TranslateService,
    private sessionParameterService: SessionParameterService,
    private currencyService: CurrencyExchangeResource,
    private printService: PrintHelperService,
    private dateService: DateService,
    private router: Router
  ) { }


  private setColumns() {
    this.columns = [
      { name: 'label.uh', prop: 'uh', width: 100 },
      { name: 'label.recipientsName', prop: 'recipientsName' },
      { name: 'label.reservationNumber', prop: 'reservationNumber' },
      { name: 'label.accountName', prop: 'accountName' },
      { name: 'label.type', prop: 'type' },
      { name: 'label.number', prop: 'externalNumber', width: 100 },
      { name: 'label.series', prop: 'externalSeries', width: 100 },
      { name: 'label.rps', prop: 'rps', width: 100 },
      { name: 'label.emissionDate', prop: 'emissionDate' },
      { name: 'label.status', prop: 'status', width: 100 },
      { name: 'label.service', prop: 'serviceFormatted', width: 100 },
      { name: 'label.rate', prop: 'rateFormatted', width: 100 },
      { name: 'label.iss', prop: 'issFormatted', width: 100 },
      { name: 'label.totalOfPayment', prop: 'typeOfPaymentFormatted', width: 140 },
      { name: 'label.pointOfSale', prop: 'pointOfSaleFormatted', width: 140 },
      { name: 'label.total', prop: 'totalFormatted', width: 140 }
    ];
  }

  private setForm() {
    this.reportGroup  = this.formBuilder.group({
      reportType: 5,
      reservationNumber: null,
      filterOnlyCancelledNotes: false,
      numberInitialNote: null,
      numberEndNote: null,
      companyClientName: null,
      period: null
    });
  }

  public searchReport() {
    const { value } = this.reportGroup;

    this.loadData(value);
  }

  private loadData(
    value?: Object
  ) {
    this
      .reportResource
      .getAllIssuedNotes(
        this.propertyId,
        value
      )
      .subscribe((response) => {
        const {
          listIssuedNotes: { items },
          listIssuedNotesPerSeries: { items: totalInfo }
        } = response;

        const {
          listIssuedNotesProduct: { items: product },
          listIssuedNotesService: { items: service }
        }  = totalInfo[0];

        this.listIssuedNotesProduct = product[0];
        this.listIssuedNotesService = service[0];
        this.pageItemsList = this.formatList(items);
      });
  }

  public async getCurrencySymbol(): Promise<any> {
    const currency = this
      .sessionParameterService
      .getParameter(this.propertyId, ParameterTypeEnum.DefaultCurrency);

    return new Promise( resolve => {
      this
        .currencyService
        .getCurrencies()
        .subscribe( res => {

          if ( res ) {
            const item = res.items.find(c => c.id == currency.propertyParameterValue);

            if ( item ) {
              this.symbol = item.symbol;
            }
          }

          resolve(this.symbol);
        });
    });
  }

  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  private formatList(list: IssuedNotes[]) {
    return list.map(item => {
      item['emissionDate'] = this.dateService.convertUniversalDateToViewFormat(item.emissionDate);
      return item;
    });
  }

  ngOnInit() {
    this.setForm();

    this.route.params.subscribe( async (params) => {
      this.propertyId = +params.property;

      await this.getCurrencySymbol();

      this.setColumns();
    });
  }

  public goToAccountPage(item: IssuedNotes) {
    this.router.navigate(['p', this.propertyId, 'billing-account', 'edit'], {
      queryParams: {'billingAccountId': item.billingAccountId}
    });
  }

}
