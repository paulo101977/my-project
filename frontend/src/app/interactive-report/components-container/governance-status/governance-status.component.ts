import { Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReportsResource } from 'app/interactive-report/resources/reports.resource';
import {
  HousekeepingRoomDisagreementBagEnum,
  HousekeepingRoomInspectionEnum,
  ReportRouterEnum
} from 'app/interactive-report/models/reports.enum';
import { DateService } from 'app/shared/services/shared/date.service';
import { ActivatedRoute, Params } from '@angular/router';
import { IMyDate } from 'mydatepicker';
import * as moment from 'moment';
import { GovernanceModel } from 'app/interactive-report/models/governance';
import { RoomTypeResource } from '../../../room-type/shared/services/room-type-resource/room-type.resource';
import { RoomAvailabilityReportEnumPipe } from 'app/interactive-report/pipes/room-availability-report-enum.pipe';
import { RoomAvailabilityReportService } from 'app/interactive-report/services/room-availability-report.service';
import { GovernanceStatusResource } from 'app/shared/resources/governance/governance-status.resource';
import { GovernanceStatusService } from 'app/shared/services/governance/governance-status.service';
import { Governance } from 'app/shared/models/governance/governance';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { HousekeepingRoomDisagreementService } from 'app/interactive-report/services/housekeeping-room-disagreement.service';
import { GovernanceService } from 'app/interactive-report/services/governance.service.ts';
import { TranslateService } from '@ngx-translate/core';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { Bordereau } from 'app/interactive-report/models/bordereau';

@Component({
  selector: 'app-governance-status',
  templateUrl: './governance-status.component.html',
  providers: [ RoomAvailabilityReportEnumPipe ],
  encapsulation: ViewEncapsulation.None
})
export class GovernanceStatusComponent implements OnInit {

  private readonly MAX_ITEMS_LAYOUT_SIMPLIFIED = 10;
  columns: any[];
  columnsSimplified: any[];
  public readonly HOUSEKEEP_STATUS_MODAL = 'housekeepStatusModal';
  public readonly ROOM_STATUS_MODAL = 'roomStatusModal';
  public readonly BAGAGE_STATUS_MODAL = 'bagageStatusModal';
  public readonly SURVEY_STATUS_MODAL = 'surveyStatusModal';
  public readonly OBSERVATION_STATUS_MODAL = 'observationStatusModal';

  public reportGroup: FormGroup;
  public disagreementBeginDate: string;
  public propertyId: number;
  public disableSince: IMyDate;
  public selectedItem: GovernanceModel;
  public modalObservationTitle: string;

  public observationForm: FormGroup;

  public items: Array<GovernanceModel>;
  public itemsBack: Array<GovernanceModel>;

  // Buttons
  public buttonCancel: ButtonConfig;
  public buttonConfirm: ButtonConfig;

  public roomTypeList: Array<any>;
  public statusRoomList: Array<any>;
  public housekeepingRoomDisagreementList: Array<any>;
  public bagageList: Array<any>;
  public surveyList: Array<any>;

  // buttons modal
  public housekeepingStatusList: Array<Governance> = [];

  // max character translation
  public maxCharacter: string;
  public readonly MAX_CHAR_NUMBER = 100;

  // date changed and is minus then date system
  public isDisabled = false;
  public currentDate = null;

  // to print component
  @ViewChild('componentToPrint', {read: ElementRef}) componentToPrint: ElementRef;
  @ViewChild('identicalTemplate') identicalTemplate: TemplateRef<any>;
  @ViewChild('beds') beds: TemplateRef<any>;
  @ViewChild('disagreement') disagreement: TemplateRef<any>;

  constructor(
    private reportResource: ReportsResource,
    private formBuilder: FormBuilder,
    private dateService: DateService,
    private route: ActivatedRoute,
    private roomTypeResource: RoomTypeResource,
    private roomAvaliabilityPipe: RoomAvailabilityReportEnumPipe,
    private roomAvailabilityReportService: RoomAvailabilityReportService,
    public housekeepingStatusResouce: GovernanceStatusResource,
    public houseKeepingStatusService: GovernanceStatusService,
    public modalEmitService: ModalEmitService,
    public modalEmitToggleService: ModalEmitToggleService,
    private printService: PrintHelperService,
    private housekeepingDisagreementService: HousekeepingRoomDisagreementService,
    private governanceService: GovernanceService,
    private translateService: TranslateService,
    private sharedService: SharedService
  ) { }

  private setButtonConfig(): void {
    this.buttonCancel = this.sharedService.getButtonConfig(
      'cancel-modal-observation',
      this.closeModalObservation,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirm = this.sharedService.getButtonConfig(
      'confirm-modal-observation',
      this.confirmObservationChange,
      'commomData.confirm',
      ButtonType.Primary,
    );
  }

  public dataChanged(event) {
    this.currentDate = event;
  }


  public closeModalObservation = () => {
    this.toggleModal(this.OBSERVATION_STATUS_MODAL);
  }

  public confirmObservationChange = () => {
    const { value } = this.observationForm.get('observation');

    const params = {
      key: value
    };

    this.updateOtherParams(params, this.OBSERVATION_STATUS_MODAL, 'observation');
  }

  public print() {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  public toggleModal(ref) {
    this.modalEmitToggleService.emitToggleWithRef(ref);
  }

  public togggleEmitedFromChild(item, ref, observationChange?: boolean) {
    this.selectedItem = item;

    const { roomNumber, observation } = item;

    if ( observationChange ) {
      this.observationForm.patchValue({
        observation
      });

      this.modalObservationTitle = this.translateService.instant(
        'title.generalObservations', {
          roomNumber
      });
    }

    this.toggleModal(ref);
  }

  public updateList(event) {
    if ( event && typeof event === 'string') {
      const exp = new RegExp(event, 'i');

      this.items = this.itemsBack.filter( item => {
        const {
          roomNumber,
          roomTypeName,
          housekeepingStatusName,
          statusRoom
        } = item;

        return exp.test(roomNumber) ||
                exp.test(roomTypeName) ||
                exp.test(housekeepingStatusName) ||
                exp.test(statusRoom);
      });
    } else {
      this.items = this.itemsBack;
    }
  }

  public setForm() {
    this.reportGroup  = this.formBuilder.group({
      reportType: ReportRouterEnum.Governance,
      disagreementBeginDate: [ null, [ Validators.required ] ],
      roomTypeId: null,
      statusRoomId: null,
      layoutSimplified: false
    });

    this.observationForm = new FormBuilder().group({
      observation: null
    });
  }

  public loadUhSituationTypeList(): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .roomTypeResource
        .getRoomTypesByPropertyId(this.propertyId)
        .subscribe(roomTypes => {
          if ( roomTypes ) {
            this.roomTypeList = roomTypes.map(({ abbreviation , id }) => {
              return {
                key: id,
                value: abbreviation
              };
            });

            resolve(null);
          }
        });
    });
  }


  public getHousekeepingStatusList() {
    this.housekeepingStatusResouce.getAll(this.propertyId).subscribe(
      result => {
        this.housekeepingStatusList = [
          ...this.houseKeepingStatusService.sortByDefault(result).map(item => {
            return Object.assign({
              icon: this
                .houseKeepingStatusService
                .getHousekeepingStatusIconById(item.housekeepingStatusId)
              },
              item
            );
          }),
        ];
      },
      error => {
        console.error(error);
        this.housekeepingStatusList = [];
      },
    );
  }

  public getHousekeepingRoomDisagreementList() {
    const keys =
      this.housekeepingDisagreementService.getRoomEnumHowArrayOfKeys();

    this.housekeepingRoomDisagreementList = keys.map( key => {
      const { color, cls } = this.governanceService.getHousekeepingRoomColorsAndClass(+key);
      return {
        value: this.roomAvaliabilityPipe.transform(+key),
        key: +key,
        color,
        cls
      };
    });
  }

  public getBagageList() {
    const keys =
      this.governanceService.getEnumKeys(HousekeepingRoomDisagreementBagEnum);

    this.bagageList = keys.map( key => {

      const { color, cls, text } = this.governanceService.getBagageColorsAndClassAndText(+key);
      return {
        value: text,
        key: +key,
        color,
        cls
      };
    });
  }

  public getSurveyList() {
    const keys =
      this.governanceService.getEnumKeys(HousekeepingRoomInspectionEnum);

    this.surveyList = keys.map( key => {

      const { color, cls, text } = this.governanceService.getSurveyColorsAndClassAndText(+key);
      return {
        value: text,
        key: +key,
        color,
        cls
      };
    });
  }

  public gethouseKeepingStatusList() {
    const keys = this.roomAvailabilityReportService.getRoomEnumHowArrayOfKeys();
    this.statusRoomList = keys.map( key => {
      return {
        value: this.roomAvaliabilityPipe.transform(+key),
        key: +key
      };
    });
  }

  public setInitialDate(propertyId: number) {
    this.disagreementBeginDate = this
      .dateService
      .getSystemDateWithoutFormat(propertyId);
  }

  public setDisableSince(disagreementBeginDate) {
    const today = new Date(disagreementBeginDate);
    this.disableSince =  {
      year: today.getFullYear(),
      month: today.getMonth() + 1,
      day: moment(today)
        .add(2, 'days')
        .toDate()
        .getDate(),
    };
  }

  public updateAndCloseModal(arr, ref) {
    this
      .reportResource
      .updateReportGovernanceItem(arr)
      .subscribe( item => {
        if (ref)  {
          this.toggleModal(ref);
        }
        this.searchReport();
      });
  }

  public updateAdultCount(item) {
    this.selectedItem = item;

    this.updateOtherParams(null, null, null);
  }

  public updateOtherParams(params, ref, prop) {
    const { selectedItem, propertyId } = this;
    const { value } = this.reportGroup.get('disagreementBeginDate');

    if ( selectedItem ) {

      const {
        roomId,
        housekeepingRoomDisagreementId,
        housekeepingRoomDisagreementStatusId,
        bagCount,
        housekeepingRoomInspectionId,
        disagreementAdultCount,
        observation
      } = selectedItem;

      const housekeepingId = housekeepingRoomDisagreementId ? housekeepingRoomDisagreementId : '';

      const obj = {
        roomId,
        propertyId,
        housekeepingRoomDisagreementStatusId,
        bagCount,
        housekeepingRoomInspectionId,
        disagreementAdultCount,
        observation,
        housekeepingRoomDisagreementId: housekeepingId,
        date: value
      };

      if ( params ) {
        obj[prop] = params.key;
      }

      if ( ref ) {
        this.updateAndCloseModal(
          [obj],
          ref
        );
      } else {
        this
          .reportResource
          .updateReportGovernanceItem([obj])
          .subscribe( item => {
            this.searchReport();
          });
      }
    }
  }

  public chooseHousekeepingStatus(status) {
    // this.toggleModalHousekeepStatus();
    const { selectedItem, propertyId } = this;
    const { housekeepingStatusId } = status;

    if ( selectedItem ) {

      const { roomId } = selectedItem;


      this.updateAndCloseModal(
        [{
          roomId,
          housekeepingStatusId,
          propertyId
        }],
        this.HOUSEKEEP_STATUS_MODAL
      );
    }
  }

  public searchReport = () => {
    this
      .reportResource
      .getAllGovernanceData(this.propertyId, this.reportGroup.value)
      .subscribe( ({ items }) => {
        const systemDate = new Date(this.disagreementBeginDate);
        const currentDate = new Date(this.currentDate);

        this.items = this.formatList(items);
        this.itemsBack = [...this.items];

        this.isDisabled = currentDate && systemDate && currentDate < systemDate;

        if (items.length > this.MAX_ITEMS_LAYOUT_SIMPLIFIED) {
          this.reportGroup.get('layoutSimplified').setValue(true);
        }
      });
  }

  public setMaxChar() {
    this.maxCharacter = this.translateService.instant(
      'label.maxCharacter', {
        number: this.MAX_CHAR_NUMBER
    });
  }

  private formatList(list: GovernanceModel[]) {
    return [...list.map(item => {
      item['checkInDate'] = this.dateService.convertUniversalDateToViewFormat(item.checkInDate);
      item['checkOutDate'] = this.dateService.convertUniversalDateToViewFormat(item.checkOutDate);
      return item;
    })];
  }


  ngOnInit() {
    this.setForm();

    // set modal observation
    this.setButtonConfig();

    this
      .route
      .params
      .subscribe(async (params: Params) => {
        this.propertyId = +params.property;

        // fill the apps-select
        this.gethouseKeepingStatusList();
        await this.loadUhSituationTypeList();

        // fill modals
        this.setMaxChar();
        this.getHousekeepingStatusList();
        this.getHousekeepingRoomDisagreementList();
        this.getBagageList();
        this.getSurveyList();

        // patch initial date
        this.setInitialDate(this.propertyId);
        this.reportGroup.patchValue({ disagreementBeginDate: this.disagreementBeginDate });
        this.setDisableSince(this.disagreementBeginDate);

        this.searchReport();
    });

    this.columns = [
      {printableTemplate: this.identicalTemplate}
    ];

    this.columnsSimplified = [
      {
        name: 'label.uh',
        prop: 'roomNumber',
      },
      {
        name: 'label.type',
        prop: 'roomTypeName'
      },
      {
        name: 'label.housekeepingStatus',
        prop: 'housekeepingStatusName'
      },
      {
        name: 'label.checkin',
        prop: 'checkInDate'
      },
      {
        name: 'label.checkout',
        prop: 'checkOutDate'
      },
      {
        name: 'label.adults',
        prop: 'adultCount'
      },
      {
        name: 'label.childs',
        prop: 'childCount'
      },
      {
        name: 'label.status',
        prop: 'statusRoom'
      },
      {
        name: 'label.qtdBeds',
        printableTemplate: this.beds
      },
      {
        name: 'label.days',
        prop: 'totalDay'
      },
      {
        name: 'text.disagreement',
        printableTemplate: this.disagreement,
        width: '500px'
      }
    ];
  }

}
