import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PrintHelperService } from '../../../shared/services/shared/print-helper.service';
import { GovernanceStatusResource } from '../../../shared/resources/governance/governance-status.resource';
import { RoomTypeResource } from '../../../room-type/shared/services/room-type-resource/room-type.resource';
import { RoomAvailabilityEnum } from '../../models/reports.enum';
import { RoomAvailabilityReportEnumPipe } from '../../pipes/room-availability-report-enum.pipe';
import { UhSituationModel } from '../../models/reports';
import { ReportsResource } from '../../resources/reports.resource';
import { DateService } from 'app/shared/services/shared/date.service';


@Component({
  selector: 'room-situation',
  templateUrl: './room-situation.component.html',
  providers: [ RoomAvailabilityReportEnumPipe ],
})
export class RoomSituationComponent implements OnInit {

  private propertyId: number;
  public symbol: string;
  public reportGroup: FormGroup;
  public columns: Array<any>;

  public pageItemsList: Array<UhSituationModel>;
  @ViewChild('observationCell') observationCell: TemplateRef<any>;

  public uhSituationList: Array<any>;
  public housekeepingStatusList: Array<any>;
  public typeUHList: Array<any>;

  @ViewChild('table', { read: ElementRef }) componentToPrint: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private reportResource: ReportsResource,
    private printService: PrintHelperService,
    private roomAvaliabilityPipe: RoomAvailabilityReportEnumPipe,
    private housekeepingStatusResouce: GovernanceStatusResource,
    private roomTypeResource: RoomTypeResource,
    private dateService: DateService,
  ) { }


  public setColumns() {

    const repeatedInfo = {
      width: 80,
      draggable: false,
      canAutoResize: false
    };

    this.columns = [{
      name: 'label.uh',
      prop: 'uh',
      ...repeatedInfo,
      width: 60
    },
    {
      name: 'label.situation',
      prop: 'uhSituation',
      ...repeatedInfo,
    },
    {
      name: 'label.typeUH',
      prop: 'typeUH',
      ...repeatedInfo,
    },
    {
      name: 'label.housekeepingStatus',
      prop: 'housekeepingStatus',
    },
    {
      name: 'label.wing', // ala
      prop: 'wing',
      ...repeatedInfo,
    },
    {
      name: 'label.floor',
      prop: 'floor',
      ...repeatedInfo,
    },
    {
      name: 'label.adultsAndChildren',
      prop: 'adultsAndChildren',
      ...repeatedInfo,
      width: 120,
    },
    {
      name: 'label.guestName',
      prop: 'guestName',
    },
    {
      name: 'label.arrivalDate',
      prop: 'arrivalDate'
    },
    {
      name: 'label.departureDate',
      prop: 'departureDate',
    },
    {
      name: `label.company`,
      prop: 'company',
    },
    {
      name: `label.pension`,
      prop: 'mealPlan',
    },
    {
      name: '',
      prop: 'observation',
      cellTemplate: this.observationCell,
      cellClass: 'cell-to-print-next-line'
    }];
  }

  public setForm() {
    this.reportGroup  = this.formBuilder.group({
      reportType: 6,
      uhSituationId: null,
      housekeepingStatusId: null,
      typeUHId: null,
      clientName: null,
      groupName: null,
      onlyVipGuests: false,
    });
  }

  public loadUhSituationTypeList() {
    this
      .roomTypeResource
      .getRoomTypesByPropertyId(this.propertyId)
      .subscribe(roomTypes => {
        if ( roomTypes ) {
          this.typeUHList = [];
          roomTypes.map(({ abbreviation , id, name }) => {
            this.typeUHList.push({
              key: id,
              value: `${abbreviation} - ${name}`
            });
          });
        }
    });
  }

  public gethouseKeepingStatusList(): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .housekeepingStatusResouce
        .getAll(this.propertyId)
        .subscribe( res => {
          if (res) {
            this.housekeepingStatusList = [];

            res.map(({housekeepingStatusId, statusName}) => {
                this.housekeepingStatusList.push({
                  key: housekeepingStatusId,
                  value: statusName
                });
            });
          }

          resolve(res);
        });
    });
  }

  public setUhSituationList() {
    this.uhSituationList = [];

    for (const key in RoomAvailabilityEnum) {
      if (!isNaN(Number(key))) {
        this.uhSituationList.push({
          value: this.roomAvaliabilityPipe.transform(+key),
          key: +key
        });
      }
    }
  }

  public searchReport() {
    const { value } = this.reportGroup;

    this.loadData(value);
  }

  public loadData(value: Object) {
    this
      .reportResource
      .getAllUhSituation(
        this.propertyId,
        value
      )
      .subscribe(({items}) => {
        this.pageItemsList = this.formatList(items);
      });
  }


  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  private formatList(list: UhSituationModel[]) {
    return list.map(item => {
      item.arrivalDate = this.dateService.convertUniversalDateToViewFormat(item.arrivalDate);
      item.departureDate = this.dateService.convertUniversalDateToViewFormat(item.departureDate);
      return item;
    });
  }

  ngOnInit() {
    this.setForm();

    this.route.params.subscribe( async (params) => {
      this.propertyId = +params.property;
      await this.gethouseKeepingStatusList();
      this.setUhSituationList();
      this.loadUhSituationTypeList();
      this.setColumns();
    });
  }

}
