import { ComponentFixture, TestBed } from '@angular/core/testing';
import { roomTypeResourceStub } from 'app/room-type/shared/services/room-type-resource/testing';
import { RoomSituationComponent } from './room-situation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { reportsResourceStub } from 'app/interactive-report/resources/testing';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { printHelperServiceStub } from 'app/shared/services/shared/testing/print-helper-service';
import { createPipeStub } from '../../../../../testing';
import { governanceStatusResourceStub } from 'app/shared/resources/governance/testing';
import { of } from 'rxjs';

const roomAvailabilityReportText = createPipeStub({ name: 'roomAvailabilityReportText'});

describe('RoomSituationComponent', () => {
  let component: RoomSituationComponent;
  let fixture: ComponentFixture<RoomSituationComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        RoomSituationComponent,
        roomAvailabilityReportText,
      ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        reportsResourceStub,
        dateServiceStub,
        printHelperServiceStub,
        governanceStatusResourceStub,
        roomTypeResourceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(RoomSituationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn<any>(component['route'], 'params').and.returnValue(of(null));
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should check that the form has been set', () => {

    let raw = {};

    component.setForm();

    raw = component.reportGroup.getRawValue();

    expect(raw['reportType']).toBe(6);
  });


  it('should check that the columns has been set', () => {

    let value = {};

    component.setColumns();

    value = component.columns[4];

    expect(value['prop']).toBe('wing');
  });
});
