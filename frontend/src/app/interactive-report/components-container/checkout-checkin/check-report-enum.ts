export enum CheckReportEnum {
  Estimates = 1,
  Effectives = 2,
  All = 0
}
