import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StatusReservationTextPipe } from '../../../shared/pipes/status-reservation-text.pipe';
import { ReservationStatusEnum } from '../../../shared/models/reserves/reservation-status-enum';
import { ReportsResource } from '../../resources/reports.resource';
import { TranslateService } from '@ngx-translate/core';
import { SessionParameterService } from '../../../shared/services/parameter/session-parameter.service';
import { CurrencyExchangeResource } from '../../../shared/resources/currency-exchange/currency-exchange.resource';
import { PrintHelperService } from '../../../shared/services/shared/print-helper.service';
import { CheckinCheckout } from '../../models/reports';
import { DateService } from 'app/shared/services/shared/date.service';
import { CheckReportEnum } from 'app/interactive-report/components-container/checkout-checkin/check-report-enum';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout-checkin.component.html',
  styleUrls: ['./checkout-checkin.component.css'],
  providers: [ StatusReservationTextPipe ]
})
export class CheckoutCheckinComponent implements OnInit {

  private propertyId: number;
  public symbol: string;
  public reportCheckoutFormGroup: FormGroup;
  public isCheckin = false;
  public labelDate = 'label.exitDate';
  public reportTitle = 'title.checkoutReport';
  public checkinReportOptionList: Array<any> = [];

  public accommodationStatusList: Array<any>;
  public columns: Array<any>;
  public columnsTotals: Array<any>;
  public columnsExtra: Array<any>;
  public pageItemsList: Array<CheckinCheckout>;
  public pageTotalItemsList: Array<any>;
  public totalReservations: number;
  public totalGuests: number;
  public showComments = false;


  @ViewChild('table', { read: ElementRef }) componentToPrint: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private reservationPipe: StatusReservationTextPipe,
    private reportResource: ReportsResource,
    private translateService: TranslateService,
    private sessionParameterService: SessionParameterService,
    private currencyService: CurrencyExchangeResource,
    private printService: PrintHelperService,
    private router: Router,
    private dateService: DateService,
  ) { }


  private setColumns() {

    this.columnsTotals = [
      {
        name: this.isCheckin ? 'label.totalCheckinsInPeriod' : 'label.totalCheckoutsInPeriod' ,
        prop: 'totalChecksInPeriod'
      },
      {
        name: 'label.totalGuests',
        prop: 'totalGuests'
      },
      {
        name: this.isCheckin ? 'label.totalEstimatedCheckinReservations' : 'label.totalEstimatedCheckoutReservations',
        prop: 'totalEstimatedReservations'
      },
      {
        name: this.isCheckin ? 'label.totalEstimatedCheckinGuests' : 'label.totalEstimatedCheckoutGuests',
        prop: 'totalEstimatedGuests'
      },
      {
        name: this.isCheckin ? 'label.totalCheckinReservationsMade' : 'label.totalCheckoutReservationsMade',
        prop: 'totalReservationsMade'
      },
      {
        name: this.isCheckin ? 'label.totalCheckinGuestsMade' : 'label.totalCheckoutGuestsMade',
        prop: 'totalGuestsMade'
      },
    ];

    this.columns = [
      {
        name: 'label.uh',
        prop: 'uh',
      },
      {
        name: 'label.typeUHComplete',
        prop: 'typeUH'
      },
      {
        name: 'label.fullName',
        prop: 'guestName'
      },
      {
        name: 'label.guestType',
        prop: 'guestTypeName'
      },
      {
        name: 'label.userName',
        prop: 'userName'
      },
      {
        name: 'label.reservationSingle',
        prop: 'reservationCode'
      },
      {
        name: 'label.adultsAndChildren',
        prop: 'adultsAndChildren'
      },
      {
        name: 'label.client',
        prop: 'client'
      },
      {
        name: 'label.statusReservation',
        prop: 'status'
      },
      {
        name: 'label.pension',
        prop: 'mealPlanType'
      },
      {
        name: 'label.deposit',
        prop: 'depositFormatted'
      },
      {
        name: 'label.arrival',
        prop: 'arrivalDate'
      },
      {
        name: 'label.departure',
        prop: 'departureDate'
      }
    ];
  }

  private setAccommodationStatusList() {
    this.accommodationStatusList = [
      {
        value: this.reservationPipe.transform(ReservationStatusEnum.ToConfirm),
        key: ReservationStatusEnum.ToConfirm
      },
      {
        value: this.reservationPipe.transform(ReservationStatusEnum.Confirmed),
        key: ReservationStatusEnum.Confirmed
      }
    ];

    if (!this.isCheckin) {
      this.accommodationStatusList.push({
        value: this.reservationPipe.transform(ReservationStatusEnum.Checkin),
        key: ReservationStatusEnum.Checkin
      });
    }
  }

  private setForm() {
    this.reportCheckoutFormGroup  = this.formBuilder.group({
      reportType: 2,
      groupName: null,
      companyClientName: null,
      clientName: null,
      userName: null,
      CheckReport: null,
      period: null,
      isMain: null,
      showInternalComments: false,
      showExternalComments: false,
      showPartnerComments: false
    });
  }

  public searchReport() {
    const { value } = this.reportCheckoutFormGroup;

    this.showComments = value.showExternalComments || value.showInternalComments || value.showPartnerComments
    ? true
    : false;

    this.loadData(value);
  }

  private loadData(value: Object) {
    this
      .reportResource
      .getAllCheckinCheckoutReport(
        this.propertyId,
        this.isCheckin,
        value
      )
      .subscribe(({listReservationCheckReportDto, totalReservationCheckReport}) => {
        this.pageItemsList = this.formatList(listReservationCheckReportDto.items);
        this.totalReservations = listReservationCheckReportDto.totalReservations;
        this.totalGuests = listReservationCheckReportDto.totalGuests;
        this.pageTotalItemsList = [totalReservationCheckReport];
      });
  }

  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  public changeType(url) {
    if ( url.indexOf('checkin') >= 0 ) {
      this.isCheckin = true;
      this.reportCheckoutFormGroup.get('reportType').setValue(3);
      this.labelDate = 'label.entryDate';
      this.reportTitle = 'title.checkinReport';
    }
  }

  private formatList(list: CheckinCheckout[]) {
    const listWithDate =  list.map(item => {
      item.arrivalDate = this.dateService.convertUniversalDateToViewFormat(item.arrivalDate);
      item.departureDate = this.dateService.convertUniversalDateToViewFormat(item.departureDate);
      return item;
    });
    return this.getExtraInfo(listWithDate);
  }


  private getExtraInfo(list: CheckinCheckout[]) {

    const { value } = this.reportCheckoutFormGroup;

    const listUpdate = list.reduce((newList, item) => {
      const showExternalComments = !!(item.externalComments && value.showExternalComments);
      const showInternalComments = !!(item.internalComments && value.showInternalComments);
      const showPartnerComments = !!(item.partnerComments && value.showPartnerComments);

      const itemCopy = {
        ...item,
        details: [
          ...(showInternalComments ? [{label: 'label.internalComments', value: item.internalComments}] : []),
          ...(showExternalComments ? [{label: 'label.externalComments', value: item.externalComments}] : []),
          ...(showPartnerComments ? [{label: 'label.partnerComments', value: item.partnerComments}] : [])
        ]
      };
      newList.push(itemCopy);

      return newList;

    }, []);
    return listUpdate;
  }

  ngOnInit() {
    this.setForm();
    this.getCheckinReport();
    this.route.params.subscribe( async (params) => {
      this.propertyId = +params.property;

      this.changeType(this.router.url);
      this.setAccommodationStatusList();
      this.setColumns();
    });

  }

  public getCheckinReport() {
    for ( const checkItem in CheckReportEnum) {
      if (!isNaN(+checkItem)) {
        switch (+checkItem) {
          case CheckReportEnum.All:
            this.checkinReportOptionList.push({
              key: checkItem,
              value: 'label.checkAll'
            });
            break;
          case CheckReportEnum.Effectives:
            this.checkinReportOptionList.push({
              key: checkItem,
              value: 'label.checkEffectives'
            });
            break;
          case CheckReportEnum.Estimates:
            this.checkinReportOptionList.push({
              key: checkItem,
              value: 'label.checkEstimates'
            });
            break;
        }
      }
    }
  }
}
