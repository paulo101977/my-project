import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GuestTypeResource } from '../../../shared/resources/guest-type/guest-type.resource';
import { ActivatedRoute } from '@angular/router';
import { PrintMealList } from '../../models/reports';

@Component({
  selector: 'app-reports-guest-in-the-house',
  templateUrl: './reports-guest-in-the-house.component.html',
  styles: [
    `
    :host .btn-container{
      display: flex;
      align-items: flex-end;
    }
    `
  ]
})
export class ReportsGuestInTheHouseComponent implements OnInit {

  public reportGuestFormGroup: FormGroup;
  public showControlReport = true;
  private propertyId: number;
  public typeOfGuestList: Array<any>;
  public printingOfMealsList: Array<any>;
  public filters: Array<any>;

  constructor(
    private formBuilder: FormBuilder,
    private guestTypeResource: GuestTypeResource,
    private route: ActivatedRoute,
  ) { }

  private setForm() {
    this.reportGuestFormGroup  = this.formBuilder.group({
      reportType: 1,
      pensionControl: true,
      typeOfGuest: [0, [Validators.required]],
      printingOfMeals: [0, [Validators.required]]
    });

    this.reportGuestFormGroup.valueChanges.subscribe(values => {
      const filteredGuest = this.typeOfGuestList.find( item => {
        return item.key === values.typeOfGuest;
      });

      this.filters = values;
      this.filters['filteredGuest'] = filteredGuest;
    });
  }

  private setPrintingOfMealsList() {
    this.printingOfMealsList = [
      { key: PrintMealList.all, value: 'label.entiresF' },
      { key: PrintMealList.coffe, value: 'label.coffe' },
      { key: PrintMealList.lunch , value: 'label.lunch' },
      { key: PrintMealList.dinner, value: 'label.dinner' }
    ];
  }

  pensionControlChange($event) {
    const { checked } = $event.target;
    this.showControlReport = <boolean>checked;
  }

  private getList(): Promise<any> {
    return new Promise((resolve) => {
      this
        .guestTypeResource
        .getAllGuestTypeListByPropertyId(this.propertyId)
        .subscribe(({items}) => {
          if (items) {
            this.typeOfGuestList = items.map(item => {
              return {
                key: item.code,
                value: item.guestTypeName
              };
            });
            this.typeOfGuestList.unshift({
              key: 0,
              value: 'label.entires'
            });

            resolve(this.typeOfGuestList);
          }
        });
    });
  }

  ngOnInit() {
    this.setForm();

    this.route.params.subscribe( async ({ property }) => {
      this.propertyId = +property;
      await this.getList();
      this.setPrintingOfMealsList();
    });
  }

}
