import { Injectable } from '@angular/core';
import { MealPlan, ReportPensionPrevision } from 'app/interactive-report/models/pension-prevision';
import { DateService } from 'app/shared/services/shared/date.service';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class ReportPensionPrevisionService {

  constructor(
    private dateService: DateService,
    private translate: TranslateService
  ) {
  }

  public formatList(list: ReportPensionPrevision[]) {
    if (list) {
      return list.map(item => {
        const systemDate = new Date(this.dateService.getSystemDate(item.propertyId));
        const date = new Date(item.date);
        item['date'] = this.dateService.convertUniversalDateToViewFormat(item.date);
        item['totalOccupation'] = date < systemDate
          ? this.translate.instant('label.notAvailable')
          : item.totalOccupied + ' | ' + item.totalCheckIn + ' | ' + item.totalCheckOut;
        item['quantityMeals'] = item.totalAdult + ' | ' + item.totalChild + ' | ' + item.totalChild2 + ' | ' + item.totalChild3;
        if (item.mealPlan) {
          item.mealPlan.forEach((mealPlan: MealPlan) => {
            item[mealPlan.mealPlanType] = mealPlan.totalAdult + ' | ' +
              mealPlan.totalChild + ' | '
              + mealPlan.totalChild2 + ' | '
              + mealPlan.totalChild3 + ' | '
              + mealPlan.totalMealPlan;
          });
        }
        return item;
      });
    }
    return [];
  }

}
