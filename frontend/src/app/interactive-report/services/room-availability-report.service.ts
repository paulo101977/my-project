import { Injectable } from '@angular/core';
import { RoomAvailabilityEnum } from '../models/reports.enum';

@Injectable({
  providedIn: 'root'
})
export class RoomAvailabilityReportService {

  constructor() { }

  public getRoomEnumHowArrayOfKeys() {
    const arr = [];
    for (const item in RoomAvailabilityEnum) {
      if (!isNaN(Number(item))) {
        arr.push(item);
      }
    }

    return arr;
  }
}
