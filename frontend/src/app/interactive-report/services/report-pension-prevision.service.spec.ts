import { inject, TestBed } from '@angular/core/testing';

import { ReportPensionPrevisionService } from './report-pension-prevision.service';
import { PENSION_PREVISION_LIST_DATA } from 'app/interactive-report/mock-data/pension-prevision-data';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateTestingModule} from 'app/shared/mock-test/translate-testing.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';

describe('ReportPensionPrevisionService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateTestingModule, RouterTestingModule],
      providers: [
        ReportPensionPrevisionService,
        dateServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  it('should be created', inject([ReportPensionPrevisionService], (service: ReportPensionPrevisionService) => {
    expect(service).toBeTruthy();
  }));

  it('should be format list', inject([ReportPensionPrevisionService], (service: ReportPensionPrevisionService) => {
    const list = service.formatList(PENSION_PREVISION_LIST_DATA.items);

    expect(list[0].mealPlan[0].mealPlanType).toEqual(4);
    expect(list[0].mealPlan[0].mealPlanTypeName).toEqual('Meia Pensão Almoço');
    expect(list[0].mealPlan[0].totalAdult).toEqual(1);
    expect(list[0].mealPlan[0].totalChild).toEqual(1);
    expect(list[0].mealPlan[0].totalChild2).toEqual(1);
    expect(list[0].mealPlan[0].totalChild3).toEqual(0);
    expect(list[0].mealPlan[0].totalMealPlan).toEqual(3);

    expect(list[1].mealPlan[1].mealPlanType).toEqual(6);
    expect(list[1].mealPlan[1].mealPlanTypeName).toEqual('Pensão Completa');
    expect(list[1].mealPlan[1].totalAdult).toEqual(0);
    expect(list[1].mealPlan[1].totalChild).toEqual(0);
    expect(list[1].mealPlan[1].totalChild2).toEqual(0);
    expect(list[1].mealPlan[1].totalChild3).toEqual(0);
    expect(list[1].mealPlan[1].totalMealPlan).toEqual(0);
  }));
});
