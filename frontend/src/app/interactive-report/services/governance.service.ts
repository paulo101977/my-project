import { Injectable } from '@angular/core';
import {
  HousekeepingRoomDisagreementBagEnum,
  HousekeepingRoomDisagreementEnum,
  HousekeepingRoomInspectionEnum
} from 'app/interactive-report/models/reports.enum';
import {
  HousekeepingRoomDisagreementEnumClass,
  HousekeepingRoomDisagreementBagEnumClass, HousekeepingRoomInspectionEnumClass
} from 'app/interactive-report/models/governance';

@Injectable({
  providedIn: 'root'
})
export class GovernanceService {

  constructor() { }

  public getEnumKeys(type) {
    const arr = [];
    for (const item in type) {
      if (!isNaN(Number(item))) {
        arr.push(item);
      }
    }

    return arr;
  }

  public getHousekeepingRoomColorsAndClass(key: HousekeepingRoomDisagreementEnum) {

    switch ( key ) {
      case HousekeepingRoomDisagreementEnum.Blocked:
        return  HousekeepingRoomDisagreementEnumClass.Blocked;
      case HousekeepingRoomDisagreementEnum.Vague:
        return  HousekeepingRoomDisagreementEnumClass.Vague;
      case HousekeepingRoomDisagreementEnum.Busy:
        return  HousekeepingRoomDisagreementEnumClass.Busy;
    }
  }

  public getBagageColorsAndClassAndText(key: HousekeepingRoomDisagreementBagEnum) {
    switch ( key ) {
      case HousekeepingRoomDisagreementBagEnum.None:
        return  HousekeepingRoomDisagreementBagEnumClass.None;
      case HousekeepingRoomDisagreementBagEnum.Few:
        return  HousekeepingRoomDisagreementBagEnumClass.Few;
      case HousekeepingRoomDisagreementBagEnum.Much:
        return HousekeepingRoomDisagreementBagEnumClass.Much;
    }
  }

  public getSurveyColorsAndClassAndText(key: HousekeepingRoomInspectionEnum) {
    switch ( key ) {
      case HousekeepingRoomInspectionEnum.DidNotWantTo:
        return  HousekeepingRoomInspectionEnumClass.DidNotWantTo;
      case HousekeepingRoomInspectionEnum.DoNotDisturb:
        return  HousekeepingRoomInspectionEnumClass.DoNotDisturb;
      case HousekeepingRoomInspectionEnum.ComeBackLater:
        return HousekeepingRoomInspectionEnumClass.ComeBackLater;
    }
  }
}
