import { Injectable } from '@angular/core';
import { HousekeepingRoomDisagreementEnum, RoomAvailabilityEnum } from 'app/interactive-report/models/reports.enum';

@Injectable({
  providedIn: 'root'
})
export class HousekeepingRoomDisagreementService {

  constructor() { }

  public getRoomEnumHowArrayOfKeys() {
    const arr = [];
    for (const item in HousekeepingRoomDisagreementEnum) {
      if (!isNaN(Number(item))) {
        arr.push(item);
      }
    }

    return arr;
  }
}
