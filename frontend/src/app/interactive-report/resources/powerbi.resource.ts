import {Injectable} from '@angular/core';
import {HttpService} from 'app/core/services/http/http.service';
import {ItemsResponse} from 'app/shared/models/backend-api/item-response';
import {ReportControl} from 'app/interactive-report/models/reports';
import {Observable} from 'rxjs';
import {Dashboard} from 'app/interactive-report/models/dashboard';

@Injectable({providedIn: 'root'})
export class PowerbiResource {
  private readonly  urlPowerBi = 'PowerBI';

  constructor(private http: HttpService) {}

  public getPowerBi(): Observable<Dashboard> {
    return this
      .http
      .get<Dashboard>(
        this.urlPowerBi
      );
  }

}
