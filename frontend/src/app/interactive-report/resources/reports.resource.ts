import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'app/core/services/http/http.service';
import {
  ItemsResponse,
  ReportCheckinCheckoutObj,
} from 'app/shared/models/backend-api/item-response';
import { ReportGuestSale } from '../models/report-guest-sale';
import { HttpParams } from '@angular/common/http';
import {
  CheckinCheckout,
  GuestInTheHouse,
  IssuedNotes,
  ReportControl,
  ReportControlPost,
  ReservationsInAdvance,
  UhSituationModel
} from '../models/reports';
import { ReportRouterEnum } from '../models/reports.enum';
import {
  ReservationsMade,
  ReservationsMadeDTo,
  ReservationsMadeFilter
} from 'app/interactive-report/models/reservations-made';
import { GovernanceModel } from 'app/interactive-report/models/governance';
import { DashboardItem } from '@app/dashboard/model/dashboard-item';
import { ReportsApiService } from '@inovacaocmnet/thx-bifrost';
import { ReportPensionPrevision } from 'app/interactive-report/models/pension-prevision';
import { BillingAccountTransfer } from 'app/interactive-report/models/billing-account-transfer';


@Injectable({providedIn: 'root'})
export class ReportsResource {
  private readonly  urlPostControl = 'reservationReport/MeanPlan';
  private readonly urlReport = 'reservationReport';

  constructor(
    private http: HttpService,
    private reportsApiService: ReportsApiService
  ) {}

  public getRouteUrl(roomRouterEnum: ReportRouterEnum): string {
    switch (roomRouterEnum) {
      case ReportRouterEnum.None: return '';
      case ReportRouterEnum.GuestInTheRouse: return 'guest-in-the-house';
      case ReportRouterEnum.Checkout: return 'checkout';
      case ReportRouterEnum.Checkin: return 'checkin';
      case ReportRouterEnum.GuestSales: return 'guest-sales';
      case ReportRouterEnum.IssuedNotes: return 'issued-notes';
      case ReportRouterEnum.RoomSituation: return 'room-situation';
      case ReportRouterEnum.Bordereau: return 'bordereau';
      case ReportRouterEnum.Occupancy: return 'occupancy';
      case ReportRouterEnum.ReservationsMade: return 'reservations-made';
      case ReportRouterEnum.Governance: return 'governance-status';
      case ReportRouterEnum.ReservationsInAdvance: return 'reservations-in-advance';
      case ReportRouterEnum.PensionPrevision: return 'pension-prevision';
      case ReportRouterEnum.BillingAccountTransfer: return 'billing-account-transfer';
      default: return '';
    }
  }

  public getControlReport(propertyId: number): Observable<ItemsResponse<ReportControl>> {
    return this
      .http
      .get<ItemsResponse<ReportControl>>(
        `${this.urlReport}/${propertyId}/searchMeanPlan?reservationItemStatus=2&guestReservationItemStatusId=2`
      );
  }

  public getDashBoardItems(): Observable<DashboardItem> {
    return this
      .http
      .get<DashboardItem>(
        `${this.urlReport}/propertydashboard`
      );
  }

  public updateReportControlItem(item): Observable<ReportControlPost> {
    return this
      .http
      .post<ReportControlPost>(this.urlPostControl, item);
  }

  private makeParamsCheckinCheckout(
    value: Object
  ) {
    let params = new HttpParams();

    if ( value ) {
      params = new HttpParams()
        .set('GroupName', value['groupName'] ?  value['groupName'] : '')
        .set('CompanyClientName', value['companyClientName'] ? value['companyClientName'] : '')
        .set('ClientName', value['clientName'] ? value['clientName'] : '')
        .set('UserName', value['userName'] ? value['userName'] : '')
        .set('CheckReport', value['CheckReport'] ? value['CheckReport'] : '0');

      if ( value['accommodationStatus'] !== undefined ) {
        params = params.append('ReservationItemStatus', value['accommodationStatus']);
      }

      if ( value['isMain'] !== undefined && value['isMain'] === true) {
        params = params.append('isMain', 'true');
      }

      if (value['period']) {
        params = params.append('DateInitial', value['period'].beginDate);
        params = params.append('DateFinal', value['period'].endDate);
      }
    }
    return params;
  }

  public getAllCheckinCheckoutReport(
    propertyId: number,
    isCheckin: boolean,
    value?: Object
  ): Observable<ReportCheckinCheckoutObj<CheckinCheckout>> {
    return this
      .http
      .get<ReportCheckinCheckoutObj<CheckinCheckout>>(
        `${this.urlReport}/${propertyId}/searchCheck?IsCheckIn=${isCheckin}`,
        {
          params: this.makeParamsCheckinCheckout(value)}
      );
  }

  private makeParamsIssuedNotes(value) {
    let params = new HttpParams();

    if ( value ) {
      params = new HttpParams()
        .set('FilterOnlyCancelledNotes', value['filterOnlyCancelledNotes'] ? value['filterOnlyCancelledNotes'] : '')
        .set('CompanyClientName', value['companyClientName'] ? value['companyClientName'] : '')
        .set('NumberInitialNote', value['numberInitialNote'] ? value['numberInitialNote'] : '')
        .set('NumberEndNote', value['numberEndNote'] ? value['numberEndNote'] : '')
        .set('ReservationNumber', value['reservationNumber'] ? value['reservationNumber'] : '');

      if (value['period']) {
        params = params.append('InitialDate', value['period'].beginDate);
        params = params.append('EndDate', value['period'].endDate);
      }
    }

    return params;
  }

  // TODO: create model
  public getAllIssuedNotes(
    propertyId: number,
    value?: Object
  ): Observable<any> {
    return this
      .http
      .get<ItemsResponse<IssuedNotes>>(
        `${this.urlReport}/${propertyId}/issuednotes`,
        { params: this.makeParamsIssuedNotes(value) }
      );
  }

  private makeParamsUhSituation(value): HttpParams {
    let  params = new HttpParams();

    if ( value ) {
      params = new HttpParams()
        .set('UhSituationId', value['uhSituationId'] ? value['uhSituationId'] : '')
        .set('HousekeepingStatusId', value['housekeepingStatusId'] ? value['housekeepingStatusId'] : '')
        .set('TypeUHId', value['typeUHId'] ? value['typeUHId'] : '')
        .set('ClientName', value['clientName'] ? value['clientName'] : '')
        .set('GroupName', value['groupName'] ? value['groupName'] : '')
        .set('OnlyVipGuests', value['onlyVipGuests'] ? value['onlyVipGuests'] : false);
    }

    return params;
  }

  public getAllUhSituation(
    propertyId: number,
    value?: any
  ): Observable<ItemsResponse<UhSituationModel>> {
    return this
      .http
      .get<ItemsResponse<UhSituationModel>>(
        `${this.urlReport}/${propertyId}/UhSituation`,
        {params: this.makeParamsUhSituation(value)}
      );
  }


  public getAllGuestInTheHouse(propertyId: number): Observable<ItemsResponse<GuestInTheHouse>> {
    return this
      .http
      .get<ItemsResponse<GuestInTheHouse>>
      (`${this.urlReport}/${propertyId}/search?reservationItemStatus=2&guestReservationItemStatusId=2`);
  }

  public getAllGuestSales(propertyId: number, itemsString: string): Observable<ItemsResponse<ReportGuestSale>> {
    return this
      .http
      .get<ItemsResponse<ReportGuestSale>>(`${this.urlReport}/${propertyId}/${itemsString}/searchGuestSale`);
  }

  public getAllBordereau(propertyId: number, search: any): Observable<any> {
    let httpParams = new HttpParams()
      .set('AccountName', search.accountName)
      .set('IssuePayment', search.issuePayment)
      .set('Name', search.name)
      .set('WasReversed', search.wasReversed)
      .set('Synthesise', search.synthesise);

    if (search.period) {
      httpParams = httpParams.append('InitialDate', search.period.beginDate);
      httpParams = httpParams.append('EndDate', search.period.endDate);
    }

    return this
      .http
      .get<any>(`${this.urlReport}/${propertyId}/Bordereau`, {params: httpParams});
  }

  public getAllReservationsMade(
    propertyId: number,
    search: ReservationsMadeFilter,
    passingBy: string
  ): Observable<ReservationsMadeDTo> {
    const httpParams = new HttpParams()
      .set('Canceled', search.Canceled)
      .set('ToConfirm', search.ToConfirm)
      .set('Confirmed', search.Confirmed)
      .set('Checkin', search.Checkin)
      .set('Checkout', search.Checkout)
      .set('Pending', search.Pending)
      .set('NoShow', search.NoShow)
      .set('WaitList', search.WaitList)
      .set('ReservationProposal', search.ReservationProposal)
      .set('InitialDate', search.InitialDate)
      .set('EndDate', search.EndDate)
      .set('PassingBy', passingBy);

    return this
      .http
      .get<ReservationsMadeDTo>(
    `${this.urlReport}/${propertyId}/ReservationsMade`, {params: httpParams}
      );
  }

  public getAllGovernanceData(propertyId: number, search: any): Observable<ItemsResponse<GovernanceModel>> {
    let httpParams = new HttpParams()
      .set('StatusRoomId', search.statusRoomId)
      .set('RoomTypeId', search.roomTypeId);

    if (search && search.disagreementBeginDate) {
      httpParams = httpParams.append('Date', search.disagreementBeginDate);
    }

    return this
      .http
      .get<ItemsResponse<GovernanceModel>>
      (
        `ReservationReport/${propertyId}/HousekeepingStatusDisagreement`,
        {params: httpParams}
      );
  }


  public updateReportGovernanceItem(item): Observable<GovernanceModel> {
    return this
      .http
      .post<GovernanceModel>(
        `ReservationReport/HousekeepingRoomDisagreement`,
        item
      );
  }

  public getAllReservationsInAdvanceReport(propertyId: number, search: any): Observable<ItemsResponse<ReservationsInAdvance>> {
    let httpParams = new HttpParams();

    if (search.period) {
      httpParams = httpParams.append('InitialDate', search.period.beginDate);
      httpParams = httpParams.append('EndDate', search.period.endDate);
    }
    return this
      .http
      .get<ItemsResponse<ReservationsInAdvance>>(`${this.urlReport}/${propertyId}/ReservationsInAdvance`, {params: httpParams});
  }

  public getPensionPrevision(propertyId: number, date: any): Observable<ItemsResponse<ReportPensionPrevision>> {
    let httpParams = new HttpParams();

    if (date) {
      httpParams = httpParams.append('DateStart', date.beginDate);
      httpParams = httpParams.append('DateEnd', date.endDate);
    }

    return this
      .reportsApiService
      .get<ItemsResponse<ReportPensionPrevision>>(`pensionPrevision`, {
        params: httpParams
      });
  }

  public getBillingAccountTransfer(propertyId: number, search: any): Observable<ItemsResponse<BillingAccountTransfer>> {
    let httpParams = new HttpParams()
      .set('BillingAccountName', search.billingAccountName)
      .set('ReservationItemCode', search.reservationItemCode);

    if (search.period) {
      httpParams = httpParams.append('DateStart', search.period.beginDate);
      httpParams = httpParams.append('DateEnd', search.period.endDate);
    }

    return this
      .reportsApiService
      .get<ItemsResponse<BillingAccountTransfer>>(`BillingAccountTransfer`, {params: httpParams});
  }
}
