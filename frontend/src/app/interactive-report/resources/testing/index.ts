import {createServiceStub} from '../../../../../testing';
import {PowerbiResource} from 'app/interactive-report/resources/powerbi.resource';
import {Observable, of} from 'rxjs';
import {ReportsResource} from 'app/interactive-report/resources/reports.resource';
import {
  CheckinCheckout,
  GuestInTheHouse,
  ReportControl,
  ReportControlPost,
  ReservationsInAdvance,
  UhSituationModel
} from 'app/interactive-report/models/reports';
import {ItemsResponse, ReportCheckinCheckoutObj} from 'app/shared/models/backend-api/item-response';
import {GovernanceModel} from 'app/interactive-report/models/governance';
import {ReportGuestSale} from 'app/interactive-report/models/report-guest-sale';
import {
    ReservationsMadeDTo,
    ReservationsMadeFilter
} from 'app/interactive-report/models/reservations-made';
import {ReportRouterEnum} from 'app/interactive-report/models/reports.enum';

export const powerbiResourceStub = createServiceStub(PowerbiResource, {
    getPowerBi: () => of( null ),
});

export const reportsResourceStub = createServiceStub(ReportsResource, {
    getAllCheckinCheckoutReport(propertyId: number, isCheckin: boolean, value?: Object):
        Observable<ReportCheckinCheckoutObj<CheckinCheckout>> {
        return of(null);
    },
    getAllGovernanceData(propertyId: number, search: any): Observable<ItemsResponse<GovernanceModel>> {
        return of(null);
    },
    getAllGuestInTheHouse(propertyId: number): Observable<ItemsResponse<GuestInTheHouse>> {
        return of(null);
    },
    getAllGuestSales(propertyId: number, itemsString: string): Observable<ItemsResponse<ReportGuestSale>> {
        return of(null);
    },
    getAllIssuedNotes(propertyId: number, value?: Object): Observable<any> {
        return of(null);
    },
    getAllReservationsInAdvanceReport(propertyId: number, search: any): Observable<ItemsResponse<ReservationsInAdvance>> {
        return of(null);
    },
    getAllUhSituation(propertyId: number, value?: any): Observable<ItemsResponse<UhSituationModel>> {
        return of(null);
    },
    getControlReport(propertyId: number): Observable<ItemsResponse<ReportControl>> {
        return of(null);
    },
    getRouteUrl(roomRouterEnum: ReportRouterEnum): string {
        return '';
    },
    updateReportControlItem(item): Observable<ReportControlPost> {
        return of(null);
    },
    updateReportGovernanceItem(item): Observable<GovernanceModel> {
        return of(null);
    },
    getAllBordereau(propertyId: number, search: any): Observable<any> {
        return of(null);
    },
    getAllReservationsMade(propertyId: number, search: ReservationsMadeFilter, passingBy: string): Observable<ReservationsMadeDTo> {
        return of( null );
    }
});
