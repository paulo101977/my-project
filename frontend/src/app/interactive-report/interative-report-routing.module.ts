import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportsComponent } from './components-container/reports/reports.component';
import { ReportsRouterComponent } from './components-container/reports-router/reports-router.component';
import {
  ReportsGuestInTheHouseComponent
} from './components-container/reports-guest-in-the-house/reports-guest-in-the-house.component';
import { GuestSalesComponent } from './components/guest-sales/guest-sales.component';
import { CheckoutCheckinComponent } from './components-container/checkout-checkin/checkout-checkin.component';
import { IssuedNotesComponent } from './components-container/issued-notes/issued-notes.component';
import { RoomSituationComponent } from './components-container/room-situation/room-situation.component';
import { BordereauComponent } from './components-container/bordereau/bordereau.component';
import { OccupancyComponent } from './components-container/occupancy/occupancy.component';
import { ReservationsMadeComponent } from './components-container/reservations-made/reservations-made.component';
import {
  GovernanceStatusComponent
} from 'app/interactive-report/components-container/governance-status/governance-status.component';
import {
  PowerBiReportComponent
} from 'app/interactive-report/components-container/powerbi/power-bi-report/power-bi-report.component';
import { DashboardComponent } from 'app/interactive-report/components-container/powerbi/dashboard/dashboard.component';
import {
  ReservationsInAdvanceComponent
} from 'app/interactive-report/components/reservations-in-advance/reservations-in-advance.component';
import { SibaIntegrationComponent } from 'app/siba-integration/components/siba-integration/siba-integration.component';
import {PensionPrevisionComponent} from 'app/interactive-report/components/pension-prevision/pension-prevision.component';
import {BillingAccountTransferComponent} from 'app/interactive-report/components/billing-account-transfer/billing-account-transfer.component';


export const interativeRoutingRoutes: Routes = [
  {
    path: '',
    component: ReportsRouterComponent,
    children: [
      {path: 'reports/all', component: ReportsComponent},
      {path: 'reports/guest-in-the-house', component: ReportsGuestInTheHouseComponent},
      {path: 'reports/guest-sales', component: GuestSalesComponent},
      {path: 'reports/checkout', component: CheckoutCheckinComponent},
      {path: 'reports/checkin', component: CheckoutCheckinComponent},
      {path: 'reports/issued-notes', component: IssuedNotesComponent},
      {path: 'reports/room-situation', component: RoomSituationComponent},
      {path: 'reports/bordereau', component: BordereauComponent},
      {path: 'reports/occupancy', component: OccupancyComponent},
      {path: 'reports/reservations-made', component: ReservationsMadeComponent},
      {path: 'reports/governance-status', component: GovernanceStatusComponent },
      {path: 'reports/reservations-in-advance', component: ReservationsInAdvanceComponent },
      {path: 'reports/pension-prevision', component: PensionPrevisionComponent },
      {path: 'reports/billing-account-transfer', component: BillingAccountTransferComponent }
    ]
  },
  {path: 'dashboard', component: DashboardComponent },
  {path: 'dashboard/power-bi-report', component: PowerBiReportComponent },
  {path: 'siba-integration', component: SibaIntegrationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(interativeRoutingRoutes)],
  exports: [RouterModule],
})
export class InterativeReportRoutingModule {
}
