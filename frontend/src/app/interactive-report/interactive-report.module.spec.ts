import { InteractiveReportModule } from './interactive-report.module';

describe('InteractiveReportModule', () => {
  let interactiveReportModule: InteractiveReportModule;

  beforeEach(() => {
    interactiveReportModule = new InteractiveReportModule();
  });

  it('should create an instance', () => {
    expect(interactiveReportModule).toBeTruthy();
  });
});
