import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { PrintMealList, ReportControl} from '../../models/reports';
import { MealPlanTypeEnum } from '../../../shared/models/revenue-management/meal-plan-type.enum';
import { AssetsService } from '@app/core/services/assets.service';

@Component({
  selector: 'pension-control-item',
  templateUrl: './pension-control-item.component.html',
  styleUrls: ['./pension-control-item.component.css']
})
export class PensionControlItemComponent implements OnInit, OnChanges {

  // TODO: create models
  @Input() item: ReportControl;
  @Input() filter: any;
  @Input() isToPrint: false;


  public showAll: boolean;
  public showCoffe = false;
  public showLunch = false;
  public showDinner = false;

  public isCoffeDisabled = false;
  public isLunchDisabled = false;
  public isDinnerDisabled = false;

  public imgSrc: string;

  public readonly personSrc = this.assetsService.getImgUrlTo('person-gray.svg');
  public readonly faceSrc = this.assetsService.getImgUrlTo('face.svg');

  public selectedColorClass: string;

  constructor( private assetsService: AssetsService ) { }

  changeColor (mealPlanTypeId) {
    switch (mealPlanTypeId) {
      case MealPlanTypeEnum.None:
        this.selectedColorClass = 'selected-blue-dark';
        break;
      case MealPlanTypeEnum.Halfboard:
      case MealPlanTypeEnum.Halfboardlunch:
      case MealPlanTypeEnum.Halfboarddinner:
        this.selectedColorClass = 'selected-blue-light';
        break;
      case MealPlanTypeEnum.Breakfast:
        this.selectedColorClass = 'selected-brown';
        break;
      case MealPlanTypeEnum.Fullboard:
      case MealPlanTypeEnum.Allinclusive:
        this.selectedColorClass = 'selected-blue';
        break;
    }
  }

  ngOnInit() {
   this.showAll  = true;

   if (this.item) {
     this.changeColor(this.item.mealPlanTypeId);
     this.disableOrEnableCheckbox(this.item.mealPlanTypeId);
     this.changeImageSrc(this.item.type);
     this.halfBoardChange(this.item);
   }
  }

  checkboxChanged($event) {
    this.halfBoardChange($event);
  }

  halfBoardChange(item) {
    if (item.mealPlanTypeId ===  MealPlanTypeEnum.Halfboard) {
      switch (true) {
        case !item.lunch && !item.dinner:
          this.isDinnerDisabled = false;
          this.isLunchDisabled = false;
          break;
        case item.lunch && !item.dinner:
          this.isLunchDisabled = false;
          this.isDinnerDisabled = true;
          break;
        case item.dinner && !item.lunch:
          this.isDinnerDisabled = false;
          this.isLunchDisabled = true;
          break;
      }

    }
  }

  disableOrEnableCheckbox(mealPlanTypeId) {
    this.isCoffeDisabled = false;
    this.isLunchDisabled = false;
    this.isDinnerDisabled = false;

    switch (mealPlanTypeId) {
      case MealPlanTypeEnum.None:
        this.isCoffeDisabled = true;
        this.isLunchDisabled = true;
        this.isDinnerDisabled = true;
        break;
      case MealPlanTypeEnum.Halfboard:
        break;
      case MealPlanTypeEnum.Halfboardlunch:
        this.isDinnerDisabled = true;
        break;
      case MealPlanTypeEnum.Halfboarddinner:
        this.isLunchDisabled = true;
        break;
      case MealPlanTypeEnum.Breakfast:
        this.isLunchDisabled = true;
        this.isDinnerDisabled = true;
        break;
      case MealPlanTypeEnum.Fullboard:
      case MealPlanTypeEnum.Allinclusive:
        break;
    }
  }

  selectCheckbox(value) {
    const iValue = +value;

    this.showAll = false;
    this.showCoffe = false;
    this.showLunch = false;
    this.showDinner = false;

    switch (iValue) {
      case PrintMealList.all:
        this.showAll = true;
        break;
      case PrintMealList.coffe:
        this.showCoffe = true;
        break;
      case PrintMealList.lunch:
        this.showLunch = true;
        break;
      case PrintMealList.dinner:
        this.showDinner = true;
        break;
      default:
    }
  }

  changeImageSrc(type) {
    if (type === 'Principal') {
      this.imgSrc = this.personSrc;
    } else {
      this.imgSrc = this.faceSrc;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.filter) {
      this.selectCheckbox(changes.filter.currentValue);
    }
  }

}
