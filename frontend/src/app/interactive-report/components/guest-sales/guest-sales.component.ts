import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ReportsResource } from '../../resources/reports.resource';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../shared/models/button-config';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ReportGuestSale } from '../../models/report-guest-sale';
import { TableColumn } from '@swimlane/ngx-datatable/src/types/table-column.type';
import { PrintHelperService } from '../../../shared/services/shared/print-helper.service';
import { PropertyDateService } from '../../../shared/services/shared/property-date.service';

@Component({
  selector: 'app-guest-sales',
  templateUrl: './guest-sales.component.html',
  styleUrls: ['./guest-sales.component.css']
})
export class GuestSalesComponent implements OnInit {

  @ViewChild('table', { read: ElementRef }) componentToPrint: ElementRef;

  private propertyId: number;
  private actualHour: any;

  // Form dependencies
  public form: FormGroup;
  public checkboxOptions: any[];

  // Table dependencies
  public columns: TableColumn[];
  public props: any[];
  public pageItemsList: ReportGuestSale[];

  // Button dependencies
  public btnConfigSearch: ButtonConfig;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private reportsResource: ReportsResource,
    private sharedService: SharedService,
    private printService: PrintHelperService,
    private propertyDateService: PropertyDateService,
  ) {
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setForm();
    this.setColumns();
    this.setBtnConfig();
    this.actualHour = this.propertyDateService.getTodayDatePropertyTimezone(this.propertyId);
  }

  public setForm() {
    this.setCheckboxOptions();
    const controls = this.checkboxOptions.map(c => new FormControl(false));
    this.form = this.formBuilder.group({
      reportType: 4,
      options: this.formBuilder.array(controls),
      sale: null,
    });
  }

  private setCheckboxOptions() {
    this.checkboxOptions = [
      { id: 'guestSale', name: 'label.guestSale' },
      { id: 'sparseSale', name: 'label.sparseSale' },
      { id: 'masterGroupSale', name: 'label.masterGroupSale' },
      { id: 'companySale', name: 'label.companySale' },
      { id: 'eventSale', name: 'label.eventSale' },
      { id: 'pendingSale', name: 'label.pendingSale' },
      { id: 'positiveSale', name: 'label.positiveSale' },
      { id: 'canceledSale', name: 'label.canceledSale' },
      { id: 'noShowSale', name: 'label.noShowSale' },
      { id: 'aheadSale', name: 'label.aheadSale' },
    ];
  }

  private setBtnConfig() {
    this.btnConfigSearch = this.sharedService.getButtonConfig(
      'search',
      this.search,
      'action.list',
      ButtonType.Primary,
      ButtonSize.Normal,
    );
  }

  public setColumns() {
    this.columns = [
      {name: 'label.roomNumber', prop: 'roomNumber'},
      {name: 'label.reservationNumber', prop: 'numReserva'},
      {name: 'label.accountName', prop: 'nomeConta'},
      {name: 'label.hotelName', prop: 'nomeHotel'},
      {name: 'label.accountType', prop: 'tipoConta'},
      {name: 'label.restaurant', prop: 'restauranteFormatted'},
      {name: 'label.others', prop: 'outrosFormatted'},
      {name: 'label.bars', prop: 'baresFormatted'},
      {name: 'label.daily', prop: 'diariaFormatted'},
      {name: 'label.events', prop: 'eventosFormatted'},
      {name: 'label.banquet', prop: 'banquetesFormatted'},
      {name: 'label.lavender', prop: 'lavandeiraFormatted'},
      {name: 'label.telecommunications', prop: 'telecomunicacoesFormatted'},
      {name: 'label.iss', prop: 'issFormatted'},
      {name: 'label.serviceTax', prop: 'taxadeServicoFormatted'},
      {name: 'label.credits', prop: 'creditosFormatted'},
      {name: 'label.total', prop: 'totalFormatted'},
    ];
  }

  public search = () => {
    const options = this.form.get('options').value;
    const itemsString = [
      options[0],
      options[1],
      options[2],
      options[3],
      options[4],
      options[5],
      options[6],
      options[7],
      options[8],
      this.form.get('sale').value ? this.form.get('sale').value : 'null',
      options[9],
    ];

    this.reportsResource.getAllGuestSales(this.propertyId, itemsString.join('/'))
      .subscribe(response => {
        if (response.items) {
          this.pageItemsList = response.items;
        }
      });
  }

  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  get options() {
    return <FormArray>this.form.get('options');
  }

}
