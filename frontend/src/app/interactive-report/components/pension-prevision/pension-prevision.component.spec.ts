import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PensionPrevisionComponent } from './pension-prevision.component';
import { configureTestSuite } from 'ng-bullet';
import { ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { PENSION_PREVISION_LIST_DATA } from 'app/interactive-report/mock-data/pension-prevision-data';
import { FormBuilder } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PropertyDateService } from 'app/shared/services/shared/property-date.service';
import { createServiceStub } from '../../../../../testing';
import { Observable } from 'rxjs';
import { ReportsResource } from 'app/interactive-report/resources/reports.resource';
import { ItemsResponse } from '@inovacaocmnet/thx-bifrost';
import { ReportPensionPrevision } from 'app/interactive-report/models/pension-prevision';
import {ReportsApiTestingModule} from '@inovacaocmnet/thx-bifrost';

describe('PensionPrevisionComponent', () => {
  let component: PensionPrevisionComponent;
  let fixture: ComponentFixture<PensionPrevisionComponent>;

  const propertyDateStubService: Partial<PropertyDateService> = {
    getTodayDatePropertyTimezone: () => {
    }
  };

  const reportsResourceStub = createServiceStub(ReportsResource, {
    getPensionPrevision(propertyId: number, date: any): Observable<ItemsResponse<ReportPensionPrevision>> {
      return;
    }
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReportsApiTestingModule
      ],
      declarations: [PensionPrevisionComponent],
      providers: [
        FormBuilder,
        {
          provide: PropertyDateService,
          useValue: propertyDateStubService
        },
        {
          provide: ReportsResource,
          useValue: reportsResourceStub
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(PensionPrevisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set config', () => {
    it('should setForm', () => {
      component['setForm']();

      expect(Object.keys(component.form.controls)).toEqual([
        'reportType',
        'date'
      ]);
    });

    it('should setBtnConfig', () => {
      component['setBtnConfig']();

      expect(component.btnConfigSearch.id).toEqual('search');
      expect(component.btnConfigSearch.textButton).toEqual('action.list');
      expect(component.btnConfigSearch.buttonType).toEqual(ButtonType.Primary);
      expect(component.btnConfigSearch.buttonSize).toEqual(ButtonSize.Normal);
    });

    it('should setColumns', () => {
      component['setColumns'](PENSION_PREVISION_LIST_DATA.items);

      expect(component.columns[0].name).toEqual('label.date');
      expect(component.columns[1].name).toEqual('label.occupancy');
      expect(component.columns[2].name).toEqual('label.quantityMeals');
      expect(component.columns[3].name).toEqual('Meia Pensão Almoço');
      expect(component.columns[4].name).toEqual('Pensão Completa');
      expect(component.columns[5].name).toEqual('Sem Pensão');
      expect(component.columns[6].name).toEqual('Café da Manha');
      expect(component.columns[7].name).toEqual('Meia Pensão');
      expect(component.columns[8].name).toEqual('Tudo Incluso');
      expect(component.columns[9].name).toEqual('Meia Pensão Jantar');
    });

    it('should formatList', () => {
      spyOn(component['reportPensionPrevisionService'], 'formatList');

      component['formatList'](PENSION_PREVISION_LIST_DATA.items);

      expect(component['reportPensionPrevisionService'].formatList)
        .toHaveBeenCalledWith(PENSION_PREVISION_LIST_DATA.items);
    });

    it('should setPeriod', () => {
      spyOn(component['printService'], 'printHtml');

      component.form.get('date').setValue({
        beginDate: '2019-06-20',
        endDate: '2019-06-27'
      });
      component['setPeriod']();

      expect(component.periodInfo).toEqual('text.periodOfLauncher');
    });

    it('should print', () => {
      spyOn(component['printService'], 'printHtml');

      component['print']();

      expect(component['printService'].printHtml)
        .toHaveBeenCalledWith(component.componentToPrint.nativeElement.innerHTML);
    });
  });
});
