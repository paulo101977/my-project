import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { ActivatedRoute } from '@angular/router';
import { ReportsResource } from 'app/interactive-report/resources/reports.resource';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { TableColumn } from '@swimlane/ngx-datatable/src/types/table-column.type';
import { PropertyDateService } from 'app/shared/services/shared/property-date.service';
import { ReportRouterEnum } from 'app/interactive-report/models/reports.enum';
import { ReportPensionPrevision } from 'app/interactive-report/models/pension-prevision';
import { tap } from 'rxjs/operators';
import { ReportPensionPrevisionService } from 'app/interactive-report/services/report-pension-prevision.service';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-pension-prevision',
  templateUrl: './pension-prevision.component.html',
  styleUrls: ['./pension-prevision.component.css']
})
export class PensionPrevisionComponent implements OnInit {

  private propertyId: number;
  private actualHour: any;
  // Form dependencies
  public form: FormGroup;
  // Table dependencies
  public columns: TableColumn[];
  public props: any[];
  public pageItemsList: ReportPensionPrevision[];
  // Button dependencies
  public btnConfigSearch: ButtonConfig;
  @ViewChild('quantityMealsHeader') quantityMealsHeader: TemplateRef<any>;
  @ViewChild('dateTableHeader') dateTableHeader: TemplateRef<any>;
  @ViewChild('occupationTableHeader') occupationTableHeader: TemplateRef<any>;
  @ViewChild('mealPlanHeader') mealPlanHeader: TemplateRef<any>;
  @ViewChild('table', { read: ElementRef }) componentToPrint: ElementRef;
  // Print dependencies
  public periodInfo: string;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private reportsResource: ReportsResource,
    private sharedService: SharedService,
    private propertyDateService: PropertyDateService,
    private reportPensionPrevisionService: ReportPensionPrevisionService,
    private printService: PrintHelperService,
    private dateService: DateService,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setForm();
    this.setBtnConfig();
    this.actualHour = this.propertyDateService.getTodayDatePropertyTimezone(this.propertyId);
  }

  private setForm() {
    this.form = this.formBuilder.group({
      reportType: ReportRouterEnum.PensionPrevision,
      date: ''
    });
  }

  private setBtnConfig() {
    this.btnConfigSearch = this.sharedService.getButtonConfig(
      'search',
      this.search,
      'action.list',
      ButtonType.Primary,
      ButtonSize.Normal,
    );
  }

  public setColumns(items: ReportPensionPrevision[]) {
    let cellClass;
    cellClass = 'thf-u-text-left subtitle';

    const columns: TableColumn[] = [
      { name: 'label.date', prop: 'date', headerTemplate: this.dateTableHeader, cellClass },
      { name: 'label.occupancy', prop: 'totalOccupation', headerTemplate: this.occupationTableHeader, cellClass },
      { name: 'label.quantityMeals', prop: 'quantityMeals', headerTemplate: this.quantityMealsHeader , cellClass},
    ];
    let mealPlanColumns: TableColumn[] = [];
    if (items) {
      items.forEach(pension => {
        if (pension.mealPlan) {
          mealPlanColumns = pension.mealPlan.map(mealPlan => {
            return {
              name: mealPlan.mealPlanTypeName,
              prop: mealPlan.mealPlanType,
              headerTemplate: this.mealPlanHeader,
            };
          });
        }
      });
    }
    this.columns = [
      ...columns,
      ...mealPlanColumns
    ];
  }

  public search = () => {
    const date = this.form.get('date').value;

    this.setPeriod();

    this.reportsResource.getPensionPrevision(this.propertyId, date)
      .pipe(
        tap(response => this.setColumns(response.items))
      )
      .subscribe(response => {
        if (response.items) {
          this.pageItemsList = this.formatList(response.items);
        }
      });
  }

  private setPeriod() {
    const date = this.form.get('date').value;

    if (date) {
      let { beginDate, endDate } = date;
      beginDate = this.dateService.convertUniversalDateToViewFormat(beginDate);
      endDate = this.dateService.convertUniversalDateToViewFormat(endDate);
      this.periodInfo = this.translateService.instant(
        'text.periodOfLauncher',
        { beginDate, endDate }
      );
    }
  }

  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  private formatList(list: ReportPensionPrevision[]) {
    return this.reportPensionPrevisionService.formatList(list);
  }

}
