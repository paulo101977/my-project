import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-printable-issued-notes',
  templateUrl: './printable-issued-notes.component.html',
  styleUrls: ['./printable-issued-notes.component.scss']
})
export class PrintableIssuedNotesComponent {
  public listProduct;
  public listService;
  public grossProduct;
  public netProduct;
  public grossService;
  public netService;

  @Input() columns;
  @Input() pageItemsList;

  @Input() set listIssuedNotesProduct(listIssuedNotesProduct) {
    if ( listIssuedNotesProduct ) {
      const { listIssuedNotesPerStatus, grossIssuedNotes, netIssuedNotes } = listIssuedNotesProduct;
      this.listProduct = listIssuedNotesPerStatus.items;
      this.grossProduct = grossIssuedNotes;
      this.netProduct = netIssuedNotes;
    }
  }

  @Input() set listIssuedNotesService(listIssuedNotesService) {
    if (listIssuedNotesService ) {
      const { listIssuedNotesPerStatus, grossIssuedNotes, netIssuedNotes } = listIssuedNotesService;
      this.listService = listIssuedNotesPerStatus.items;
      this.grossService = grossIssuedNotes;
      this.netService = netIssuedNotes;
    }
  }
}
