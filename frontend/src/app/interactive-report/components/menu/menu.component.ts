import { Component, Input, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { ReportsResource } from '../../resources/reports.resource';
import { ReportRouterEnum } from '../../models/reports.enum';

@Component({
  selector: 'report-menu',
  templateUrl: './menu.component.html',
  styles: [`:host{ display: block; }`]
})
export class MenuComponent implements OnInit {

  public reportTypeList: Array<any>;
  @Input() formGroupAppSelect: any;

  private propertyId: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private reportsResource: ReportsResource,
  ) { }

  private menuChangeRoute(key) {
    const nKey = +key;
    const routeUrl =  this.reportsResource.getRouteUrl(nKey);

    this.changeRoute(routeUrl);
  }

  // TODO: this has been changed in other PR
  public loadReportTypes() {
    this.reportTypeList = [
      { key: ReportRouterEnum.GuestInTheRouse, value: 'label.guestInTheHouse' },
      { key: ReportRouterEnum.Checkout, value: 'label.checkout' },
      { key: ReportRouterEnum.Checkin, value: 'label.checkinCaps' },
      { key: ReportRouterEnum.GuestSales, value: 'label.balanceOfAccounts' },
      { key: ReportRouterEnum.IssuedNotes, value: 'label.issuedNotes' },
      { key: ReportRouterEnum.RoomSituation, value: 'label.uhSituation' },
      { key: ReportRouterEnum.Bordereau, value: 'label.bordereau' },
      { key: ReportRouterEnum.Occupancy, value: 'label.occupancy' },
      { key: ReportRouterEnum.ReservationsMade, value: 'label.reservationMade' },
      { key: ReportRouterEnum.Governance, value: 'label.governanceAndDisagreement' },
      { key: ReportRouterEnum.ReservationsInAdvance, value: 'label.reservationsInAdvance' },
      { key: ReportRouterEnum.PensionPrevision, value: 'label.pensionPrevision' },
      { key: ReportRouterEnum.BillingAccountTransfer, value: 'label.billingAccountTransfer' },
    ];
  }

  private changeRoute(url, params?) {
    this
      .router
      .navigate([`/p/${this.propertyId}/interactive-report/reports/${url}`]);
  }

  ngOnInit() {

    this.route.params.subscribe( ({ property}) => {
      this.propertyId = +property;

      this.loadReportTypes();

      if ( this.formGroupAppSelect) {
        this
          .formGroupAppSelect
          .get('reportType')
          .valueChanges
          .subscribe( key => this.menuChangeRoute(key));
      }
    });
  }

}
