import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';
import {
  PrintableBordereauGroupReportComponent
} from 'app/interactive-report/components/printable-bordereau-group-report/printable-bordereau-group-report.component';

describe('PrintableBorderauxGroupReportComponent', () => {
  let component: PrintableBordereauGroupReportComponent;
  let fixture: ComponentFixture<PrintableBordereauGroupReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateTestingModule ],
      declarations: [ PrintableBordereauGroupReportComponent, CurrencyFormatPipe ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintableBordereauGroupReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
