import { Component, Input, OnInit } from '@angular/core';
import { BordereauGroup } from 'app/interactive-report/models/bordereau-group';

@Component({
  selector: 'app-printable-bordereau-group-report',
  templateUrl: './printable-bordereau-group-report.component.html',
  styleUrls: ['./printable-bordereau-group-report.component.css']
})
export class PrintableBordereauGroupReportComponent implements OnInit {

  @Input() borderoGroupList: Array<BordereauGroup>;
  @Input() props: any;
  @Input() items;
  @Input() periodInfo: any;
  @Input() columnsName;
  @Input() isSynthesise = false;

  constructor() { }

  ngOnInit() {
  }

}
