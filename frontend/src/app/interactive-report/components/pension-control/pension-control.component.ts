import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { ReportsResource } from '../../resources/reports.resource';
import { PrintMealList, ReportControl } from '../../models/reports';
import { PrintHelperService } from '../../../shared/services/shared/print-helper.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'pension-control',
  templateUrl: './pension-control.component.html'
})
export class PensionControlComponent implements OnChanges, OnInit {

  public columns: any[];
  public allControlList: Array<ReportControl>;
  public loadControlListBack: Array<ReportControl>;
  public _filter: any;
  public selection: PrintMealList;
  public isFiltered = false;

  private propertyId: number;
  public isPrint: boolean;

  @ViewChild('componentToPrint', {read: ElementRef}) componentToPrint: ElementRef;
  @ViewChild('child') child: TemplateRef<any>;

  @Input() set filter(filter: any) {
    this._filter = filter;
  }

  get filter() {
    return this._filter;
  }

  constructor(
    private reportsResource: ReportsResource,
    private printService: PrintHelperService,
    private route: ActivatedRoute,
  ) {
  }

  loadControlList() {
    this
      .reportsResource
      .getControlReport(this.propertyId)
      .subscribe(({items}) => {
        if (items) {
          this.allControlList = <Array<ReportControl>>items;
          this.loadControlListBack = this.allControlList;
        }
      });
  }

  updateList($event) {
    this.allControlList = this.loadControlListBack;

    if (typeof  $event === 'string' && $event !== '' && this.loadControlListBack) {
      const regex = new RegExp($event, 'i');

      this.isFiltered = true;

      this.allControlList = this.loadControlListBack.filter(item => {
        return regex.test(item.uh)
          || regex.test(item.fullName)
          || regex.test(item.type);
      });
    } else {
      this.isFiltered = false;
      this.allControlList = this.loadControlListBack;
    }
  }


  public print = () => {
    this.printService.printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  ngOnInit() {
    this.route.params.subscribe(async ({property}) => {
      this.propertyId = +property;

      this.loadControlList();
    });

    this.columns = [
      {printableTemplate: this.child}
    ];
  }

  enableOrDisableCheckbox(value) {
    if (value) {
      this.selection = value.printingOfMeals;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.filter) {
      const {currentValue} = changes.filter;

      if (currentValue) {
        this.enableOrDisableCheckbox(currentValue);

        const {filteredGuest} = currentValue;

        if (this.isFiltered
          && filteredGuest
          && filteredGuest.key !== 0
          && this.allControlList) {
          this.allControlList = this
            .allControlList
            .filter(item => item.guestTypeName === filteredGuest.value);
        } else if (filteredGuest
          && filteredGuest.key !== 0
          && this.loadControlListBack) {
          this.allControlList = this
            .loadControlListBack
            .filter(item => item.guestTypeName === filteredGuest.value);
        } else {
          // load state back
          this.allControlList = this.loadControlListBack;
        }
      }
    }
  }

}
