import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { ReportsResource } from '../../resources/reports.resource';
import { ActivatedRoute, Router } from '@angular/router';
import { PrintHelperService } from '../../../shared/services/shared/print-helper.service';
import { SessionParameterService } from '../../../shared/services/parameter/session-parameter.service';
import { TranslateService } from '@ngx-translate/core';
import { CurrencyExchangeResource } from '../../../shared/resources/currency-exchange/currency-exchange.resource';
import { MealPlanTypeEnum } from '../../../shared/models/revenue-management/meal-plan-type.enum';
import { GuestInTheHouse } from '../../models/reports';
import { DateService } from 'app/shared/services/shared/date.service';

@Component({
  selector: 'guests-in-the-house',
  templateUrl: './guests-in-the-house.component.html',
  styleUrls: ['./guests-in-the-house.component.css']
})
export class GuestsInTheHouseComponent implements OnInit, OnChanges {

  public columns: Array<any>;
  public totalColumns: Array<any>;
  public mealColumns: Array<any>;
  public mealTotalItemsList: Array<any>;
  private propertyId: number;
  public day: string;
  public totalData: Object;

  @Input() filter: any;

  @ViewChild('table', { read: ElementRef }) componentToPrint: ElementRef;

  public pageItemsList: Array<GuestInTheHouse>;
  public pageItemsListBack: Array<GuestInTheHouse>;
  public totalItemsList: Array<any>;
  public isFiltered = false;

  constructor(
    private reportsResource: ReportsResource,
    private route: ActivatedRoute,
    private printService: PrintHelperService,
    private sessionParameterService: SessionParameterService,
    private translateService: TranslateService,
    private currencyService: CurrencyExchangeResource,
    private router: Router,
    private dateService: DateService,
  ) { }

  private setColumns() {
    this.columns = [
      {
        name: 'label.uh',
        prop: 'uh',
      },
      {
        name: 'label.typeUHComplete',
        prop: 'typeUH'
      },
      {
        name: 'label.fullName',
        prop: 'guestName'
      },
      {
        name: 'label.guestType',
        prop: 'guestTypeName'
      },
      {
        name: 'label.reservationSingle',
        prop: 'reservationCode'
      },
      {
        name: 'label.adultsAndChildren',
        prop: 'adultsAndChildren'
      },
      {
        name: 'label.client',
        prop: 'client'
      },
      {
        name: 'label.pension',
        prop: 'mealPlanType'
      },
      {
        name: 'label.daily',
        prop: 'dailyAmountFormatted',
        printableAlignRight: true
      },
      {
        name: 'label.arrival',
        prop: 'arrivalDate'
      },
      {
        name: 'label.departure',
        prop: 'departureDate'
      }
    ];
  }

  private setTotalColums() {
    this.totalColumns = [
      {
        name: 'label.totals',
        prop: 'label',
        printableAlignRight: true
      },
      {
        name: 'label.value',
        prop: 'value',
        printableAlignRight: true
      }
    ];
  }

  private setMealColumns() {
    this.mealColumns = [
      {
        name: 'label.totalByMealPlainType',
        prop: 'label',
        width: 200,
        draggable: false,
        canAutoResize: false
      },
      {
        name: 'label.adults',
        prop: 'adults',
        width: 80,
        draggable: false,
        canAutoResize: false
      },
      {
        name: 'label.childOne',
        prop: 'child1',
        width: 80,
        draggable: false,
        canAutoResize: false
      },
      {
        name: 'label.childTwo',
        prop: 'child2',
        width: 80,
        draggable: false,
        canAutoResize: false
      },
      {
        name: 'label.childThree',
        prop: 'child3',
        width: 80,
        draggable: false,
        canAutoResize: false
      }
    ];
  }

  private loadGuestData() {
    this
      .reportsResource
      .getAllGuestInTheHouse(this.propertyId)
      .subscribe( ({ items}) => {
        this.pageItemsList = this.formatList(items);
        this.pageItemsListBack = <GuestInTheHouse[]>this.pageItemsList;

        this.calculateTotalData(this.pageItemsList);
        this.calculateTotalGuest(this.pageItemsList);
      });
  }

  private formatList(list: GuestInTheHouse[]) {
    return list.map(item => {
      item['arrivalDate'] = this.dateService.convertUniversalDateToViewFormat(item.arrivalDate);
      item['departureDate'] = this.dateService.convertUniversalDateToViewFormat(item.departureDate);
      return item;
    });
  }

  public updateList = ($event) => {
    if (typeof  $event === 'string' && $event !== '') {
      const regex = new RegExp($event, 'i');

      this.pageItemsList = this.pageItemsList.filter( item => {
        return regex.test(item.uh)
          || regex.test(item.guestName)
          || regex.test(item.mealPlanType)
          || regex.test(item.reservationCode);
      });

      this.calculateTotalData(this.pageItemsList);
      this.calculateTotalGuest(this.pageItemsList);

      this.isFiltered = true;
    } else {
      this.pageItemsList = this.pageItemsListBack;

      this.calculateTotalData(this.pageItemsList);
      this.calculateTotalGuest(this.pageItemsList);

      this.isFiltered = false;
    }
  }

  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }


  // TODO: remove any code below when backend back to me a endpoint with summarized data
  calculateTotalData(pageItemsList) {
    if (pageItemsList) {
      this.totalData = {};

      this.totalData['mealPlanTypeAmount'] = 0;
      this.totalData['dailyAmount'] = 0;
      this.totalData['totalPayers'] = 0;
      this.totalData['totalOfCourtesies'] = 0;
      this.totalData['totalIsInternalUsage'] = 0;

      pageItemsList.map( item => {
        const mealPlanTypeAmount = +item.mealPlanTypeAmount;
        this.totalData['mealPlanTypeAmount'] += mealPlanTypeAmount;
        this.totalData['dailyAmount'] += parseFloat(item.dailyAmount);

        if (mealPlanTypeAmount) {
          this.totalData['totalPayers'] += 1;
        }
        if (item.isCourtesy) {
          this.totalData['totalOfCourtesies'] += 1;
        }
        if (item.isInternalUsage) {
          this.totalData['totalIsInternalUsage'] += 1;
        }
      });

      this.totalItemsList = [{
        label: `${this.translateService.instant('label.mealPlanTypeAmount')}`,
        value: `${this.totalData['mealPlanTypeAmount']}`
      },
      {
        label: `${this.translateService.instant('label.dailyAmount')}`,
        value: `${this.totalData['dailyAmount']}`
      },
      {
        label: `${this.translateService.instant('label.totalPayers')}`,
        value: this.totalData['totalPayers']
      },
      {
        label: `${this.translateService.instant('label.totalOfCourtesies')}`,
        value: this.totalData['totalOfCourtesies']
      },
      {
        label: `${this.translateService.instant('label.totalIsInternalUsage')}`,
        value: this.totalData['totalIsInternalUsage']
      }];
    }
  }

  // TODO: remove any code below when backend back to me a endpoint with summarized data
  sumPerson(personArr, arr) {
    personArr.map( (value, i) => {
      arr[i] += parseInt(value, 10);
    });
  }

  // TODO: remove any code below when backend back to me a endpoint with summarized data
  pushLabelAndData(label, data) {
    this.mealTotalItemsList.push({
      label,
      adults: data[0],
      child1: data[1],
      child2: data[2],
      child3: data[3]
    });
  }

  // TODO: remove any code below when backend back to me a endpoint with summarized data
  calculateTotalGuest(pageItemsList) {
    if (pageItemsList) {

      const Halfboard = [0, 0, 0, 0];
      const Halfboarddinner = [0, 0, 0, 0];
      const Halfboardlunch = [0, 0, 0, 0];
      const Fullboard = [0, 0, 0, 0];
      const Breakfast = [0, 0, 0, 0];
      const None = [0, 0, 0, 0];

      pageItemsList.map(item => {
        const adultsAndChildren = item.adultsAndChildren.split('|');

        switch (item.mealPlanTypeId) {
          case MealPlanTypeEnum.None:
            this.sumPerson(adultsAndChildren, None);
            break;
          case MealPlanTypeEnum.Allinclusive:
          case MealPlanTypeEnum.Fullboard:
            this.sumPerson(adultsAndChildren, Fullboard);
            break;
          case MealPlanTypeEnum.Halfboardlunch:
            this.sumPerson(adultsAndChildren, Halfboardlunch);
            break;
          case MealPlanTypeEnum.Halfboarddinner:
            this.sumPerson(adultsAndChildren, Halfboarddinner);
            break;
          case MealPlanTypeEnum.Halfboard:
            this.sumPerson(adultsAndChildren, Halfboard);
            break;
          case MealPlanTypeEnum.Breakfast:
            this.sumPerson(adultsAndChildren, Breakfast);
            break;
        }
      });

      this.mealTotalItemsList = [];

      this.pushLabelAndData(this.translateService.instant('label.halfboard'), Halfboard);
      this.pushLabelAndData(this.translateService.instant('label.halfboardlunch'), Halfboardlunch);
      this.pushLabelAndData(this.translateService.instant('label.halfboarddinner'), Halfboarddinner);
      this.pushLabelAndData(this.translateService.instant('label.breakFast'), Breakfast);
      this.pushLabelAndData(this.translateService.instant('label.fullboard'), Fullboard);
      this.pushLabelAndData(this.translateService.instant('label.noneBoard'), None);
    }
  }

  ngOnInit() {

    this.route.params.subscribe( async ({ property}) => {
      this.propertyId = +property;

      this.setColumns();
      this.setTotalColums();
      this.setMealColumns();

      this.day = new Date().toStringUniversal();

      this.loadGuestData();
    });
  }

  public goToEditReservationPage(reservation) {
    this
      .router
      .navigate(['p', this.propertyId, 'reservation', 'new'], { queryParams: { reservation: reservation.id } });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ( changes && changes.filter ) {
      const { currentValue } = changes.filter;

      if (currentValue) {
        const { filteredGuest } = currentValue;

        if (this.isFiltered
          && filteredGuest
          && filteredGuest.key !== 0
          && this.pageItemsList ) {
          this.pageItemsList = this
            .pageItemsList
            .filter(item => item.guestTypeName === filteredGuest.value);
        } else if (filteredGuest
          && filteredGuest.key !== 0
          && this.pageItemsListBack ) {
          this.pageItemsList = this
            .pageItemsListBack
            .filter(item => item.guestTypeName === filteredGuest.value);
        } else {
          // load state back
          this.pageItemsList = this.pageItemsListBack;
        }
      }
    }
  }

}
