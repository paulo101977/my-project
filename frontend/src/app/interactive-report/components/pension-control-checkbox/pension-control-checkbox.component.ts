import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroupName } from '@angular/forms';
import { ReportsResource } from '../../resources/reports.resource';
import { ActivatedRoute } from '@angular/router';
import { PropertyDateService } from '../../../shared/services/shared/property-date.service';
import { ReportControl, ReportControlPost} from '../../models/reports';

@Component({
  selector: 'pension-control-checkbox',
  templateUrl: './pension-control-checkbox.component.html',
  styleUrls: ['./pension-control-checkbox.component.css']
})
export class PensionControlCheckboxComponent implements OnInit {

  private propertyId: number;

  @Input() item: ReportControl;
  @Input() type: string;
  @Input() checkboxId: any;
  @Input() checkboxText: string;
  @Input() checkboxGroupName: FormGroupName;
  @Input() isChecked: boolean;
  @Input() isDisabled: boolean;
  @Output() emitChange = new EventEmitter();

  constructor(
    private reportResource: ReportsResource,
    private route: ActivatedRoute,
    private dateService: PropertyDateService,
  ) { }

  public changed($event) {
    const { checked } = $event.target;


    if (this.item && this.type && this.propertyId != null) {
      const { propertyId } = this;

      const {
        guestMealPlanControlId,
        guestReservationItemId,
        coffee,
        lunch,
        dinner
      } = this.item;

      const guestMealPlanControlDate = this
        .dateService
        .getTodayDatePropertyTimezone(propertyId);

      const objectToSend = <ReportControlPost>{
        guestMealPlanControlId,
        guestReservationItemId,
        coffee,
        lunch,
        dinner,
        guestMealPlanControlDate,
        propertyId
      };

      objectToSend[this.type] = checked;

      this
        .reportResource
        .updateReportControlItem(objectToSend)
        .subscribe( nItem => {

          this.item.guestMealPlanControlId = nItem['guestMealPlanControlId'];

          this.item.lunch = nItem['lunch'];
          this.item.coffee = nItem['coffee'];
          this.item.dinner = nItem['dinner'];

          this.emitChange.emit(this.item);
        });

    }
  }

  ngOnInit() {
    this.route.params.subscribe( async ({ property }) => {
      this.propertyId = +property;
    });
  }

}
