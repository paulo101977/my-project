import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { GovernanceModel } from 'app/interactive-report/models/governance';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'governance-status-item',
  templateUrl: './governance-status-item.component.html',
  styleUrls: ['./governance-status-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GovernanceStatusItemComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
  ) { }

  public adultCountList: Array<any>;

  @Input() item: GovernanceModel;
  @Input() isPrintMode: boolean;
  @Input() isDisabled = false;
  @Input() layoutSimplified = false;

  // dispatcher
  @Output() dispatchHousekeep = new EventEmitter();
  @Output() dispatchStatusRoom = new EventEmitter();
  @Output() dispatchBagage = new EventEmitter();
  @Output() dispatchSurvey = new EventEmitter();
  @Output() dispatchObservation = new EventEmitter();
  @Output() dispatchNumberChange = new EventEmitter();

  // form group
  public form: FormGroup;

  public setForm(value: number) {
    this.form = this.formBuilder.group({
      disagreementAdultCount: [value]
    });

    this
      .form
      .get('disagreementAdultCount')
      .valueChanges
      .subscribe(nValue => {
        this.selectedAdultCount(nValue);
    });
  }

  // fill the array with numbers of 1 to 20
  public createAdultCountList() {
    this.adultCountList =
      Array(20)
        .fill(0)
        .map(( e, i ) => {
          const key = i + 1;
          return {
            key,
            value: `${key}`
          };
      });
  }

  public selectedAdultCount(value) {
    if ( value ) {
      this.dispatchNumberChange.emit({
        ...this.item,
        disagreementAdultCount: +value
      });
    }
  }

  ngOnInit() {
    this.createAdultCountList();

    const { item } = this;

    if ( item ) {
      const {disagreementAdultCount} = item;

      this.setForm(disagreementAdultCount);
    }
  }

  public dispatchOpenModalHouseKeeping(item) {
    this.dispatchHousekeep.emit(item);
  }

  public dispatchOpenModalStatus(item) {
    this.dispatchStatusRoom.emit(item);
  }

  public dispatchOpenBagage(item) {
    this.dispatchBagage.emit(item);
  }

  public dispatchOpenSurvey(item) {
    this.dispatchSurvey.emit(item);
  }

  public dispatchOpenObservation(item) {
    this.dispatchObservation.emit(item);
  }

}
