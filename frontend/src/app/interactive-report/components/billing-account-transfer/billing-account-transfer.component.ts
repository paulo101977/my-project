import {Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TableColumn} from '@swimlane/ngx-datatable';
import {ButtonConfig, ButtonSize, ButtonType} from 'app/shared/models/button-config';
import {ActivatedRoute} from '@angular/router';
import {ReportsResource} from 'app/interactive-report/resources/reports.resource';
import {SharedService} from 'app/shared/services/shared/shared.service';
import {PropertyDateService} from 'app/shared/services/shared/property-date.service';
import {PrintHelperService} from 'app/shared/services/shared/print-helper.service';
import {DateService} from 'app/shared/services/shared/date.service';
import {TranslateService} from '@ngx-translate/core';
import {ReportRouterEnum} from 'app/interactive-report/models/reports.enum';
import {BillingAccountTransfer} from 'app/interactive-report/models/billing-account-transfer';
import {CurrencyFormatPipe} from 'app/shared/pipes/currency-format.pipe';
import {PropertyStorageService} from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-billing-account-transfer',
  templateUrl: './billing-account-transfer.component.html'
})
export class BillingAccountTransferComponent implements OnInit {
  private propertyId: number;
  private actualHour: any;

  // form dependencies
  public form: FormGroup;

  // table dependencies
  public columns: any[];
  public pageItemsList: BillingAccountTransfer[];

  // button dependencies
  public btnConfigSearch: ButtonConfig;
  @ViewChild('table', {read: ElementRef}) componentToPrint: ElementRef;

  // print dependencies
  public periodInfo: string;


  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private reportsResource: ReportsResource,
    private sharedService: SharedService,
    private propertyDateService: PropertyDateService,
    private printService: PrintHelperService,
    private dateService: DateService,
    private translateService: TranslateService,
    private currencyFormatPipe: CurrencyFormatPipe,
    private propertyService: PropertyStorageService,
  ) {
  }

  ngOnInit() {
    this.propertyId = this.propertyService.getCurrentPropertyId();
    this.setForm();
    this.setColumns();
    this.setBtnConfig();
    this.actualHour = this.propertyDateService.getTodayDatePropertyTimezone(this.propertyId);
  }

  private formatList(list: BillingAccountTransfer[]) {
    return list.map(item => {
      item['creationTimeFormatted'] = this.dateService
        .convertStringToDate(item.creationTime, DateService.DATE_FORMAT_UNIVERSAL);
      return item;
    });
  }

  private setBtnConfig() {
    this.btnConfigSearch = this.sharedService.getButtonConfig(
      'search',
      () => this.search(),
      'action.list',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  private setForm() {
    this.form = this.formBuilder.group({
      reportType: ReportRouterEnum.BillingAccountTransfer,
      period: ['', [Validators.required]],
      billingAccountName: '',
      reservationItemCode: ''
    });

    this.form.valueChanges
      .subscribe(() => {
        this.btnConfigSearch.isDisabled = this.form.invalid;
      });
  }

  public setColumns() {
    this.columns = [
      { name: 'label.reservationItemCodeSource', prop: 'reservationItemCodeSource' },
      { name: 'label.reservationItemCodeDestination', prop: 'reservationItemCodeDestination' },
      { name: 'label.billingItemName', prop: 'billingItemName' },
      { name: 'label.billingAccountOriginal', prop: 'billingAccountOriginal' },
      { name: 'label.billingAccountSource', prop: 'billingAccountSource' },
      { name: 'label.billingAccountDestination', prop: 'billingAccountDestination' },
      { name: 'label.entryType', prop: 'billingAccountItemType' },
      { name: 'label.value', prop: 'amountFormatted' },
      { name: 'label.billingAccountDate', prop: 'creationTimeFormatted', type: 'date'},
      { name: 'label.billingAccountItemId', prop: 'billingAccountItemId' },
      { name: 'label.userName', prop: 'userName' },
    ];
  }

  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  public search() {
    const {value} = this.form;

    this.reportsResource.getBillingAccountTransfer(this.propertyId, value)
      .subscribe(response => {
        if (response.items) {
          this.pageItemsList = this.formatList(response.items);
        }
      });
  }

}
