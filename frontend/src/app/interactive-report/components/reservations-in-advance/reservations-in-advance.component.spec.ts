import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationsInAdvanceComponent } from './reservations-in-advance.component';
import { NO_ERRORS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PropertyDateService } from 'app/shared/services/shared/property-date.service';
import {ReportsApiTestingModule} from '@inovacaocmnet/thx-bifrost';

describe('ReservationsInAdvanceComponent', () => {
  let component: ReservationsInAdvanceComponent;
  let fixture: ComponentFixture<ReservationsInAdvanceComponent>;

  @Pipe({name: 'translate'})
  class TranslateStubPipe implements PipeTransform {
    transform() {
    }
  }

  @Pipe({name: 'amDateFormat'})
  class AmDateFormatStubPipe implements PipeTransform {
    transform(value: any, ...args: any[]): any {
      return 'null';
    }
  }

  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };

  const propertyDateStubService: Partial<PropertyDateService> = {
    getTodayDatePropertyTimezone: () => {
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        ReportsApiTestingModule
      ],
      declarations: [
        ReservationsInAdvanceComponent,
        TranslateStubPipe,
        AmDateFormatStubPipe
      ],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: PropertyDateService,
          useValue: propertyDateStubService
        },
        FormBuilder
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationsInAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
