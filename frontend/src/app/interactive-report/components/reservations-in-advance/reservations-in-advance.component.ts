import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ReportsResource } from 'app/interactive-report/resources/reports.resource';
import { TranslateService } from '@ngx-translate/core';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { TableColumn } from '@swimlane/ngx-datatable/src/types/table-column.type';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { PropertyDateService } from 'app/shared/services/shared/property-date.service';
import { ReservationsInAdvance } from 'app/interactive-report/models/reports';
import { ReportRouterEnum } from 'app/interactive-report/models/reports.enum';
import { DateService } from 'app/shared/services/shared/date.service';
import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';

@Component({
  selector: 'app-reservations-in-advance',
  templateUrl: './reservations-in-advance.component.html',
  styleUrls: ['./reservations-in-advance.component.css']
})
export class ReservationsInAdvanceComponent implements OnInit {

  @ViewChild('table', {read: ElementRef}) componentToPrint: ElementRef;

  private propertyId: number;
  private actualHour: any;

  // Form dependencies
  public form: FormGroup;

  // Table dependencies
  public columns: any[];
  public pageItemsList: ReservationsInAdvance[];

  // Button dependencies
  public btnConfigSearch: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private reportResource: ReportsResource,
    private translateService: TranslateService,
    private printService: PrintHelperService,
    private sharedService: SharedService,
    private propertyDateService: PropertyDateService,
    private dateService: DateService,
    private currencyFormatPipe: CurrencyFormatPipe,
  ) {
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setBtnConfig();
    this.setForm();
    this.setColumns();
    this.actualHour = this.propertyDateService.getTodayDatePropertyTimezone(this.propertyId);
  }

  private setForm() {
    this.form = this.formBuilder.group({
      reportType: ReportRouterEnum.ReservationsInAdvance,
      period: [null, Validators.required]
    });

    this.form.valueChanges
      .subscribe(value => {
        this.btnConfigSearch.isDisabled = this.form.invalid;
      });
  }

  public search() {
    const {value} = this.form;

    this.loadData(value);
  }

  private loadData(value: Object) {
    this
      .reportResource
      .getAllReservationsInAdvanceReport(
        this.propertyId,
        value
      )
      .subscribe(({items}) => {
        this.pageItemsList = this.formatList(items);
      });
  }

  private setBtnConfig() {
    this.btnConfigSearch = this.sharedService.getButtonConfig(
      'search',
      () => this.search(),
      'action.list',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  public setColumns() {
    this.columns = [
      {name: 'label.reservationItemCode', prop: 'reservationCode'},
      {name: 'label.uh', prop: 'uh'},
      {name: 'label.abbreviation', prop: 'abbreviation'},
      {name: 'label.entryType', prop: 'paymentTypeName'},
      {name: 'label.billingAccountItemDate', prop: 'billingAccountItemDateFormat', type: 'date'},
      {name: 'label.value', prop: 'amountFormatted'},
      {name: 'label.accountName', prop: 'billingAccountName'},
      {name: 'label.guestName', prop: 'guestName'},
      {name: 'label.statusReservation', prop: 'statusName'},
      {name: 'label.arrivalDate', prop: 'arrivalDateFormat', type: 'date'},
      {name: 'label.departureDate', prop: 'departureDateFormat', type: 'date'}
    ];
  }

  public print = () => {
    this
      .printService
      .printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  private formatList(list: ReservationsInAdvance[]) {
    return list.map(item => {
      item.arrivalDateFormat = this.dateService.convertStringToDate(item['arrivalDate'], DateService.DATE_FORMAT_UNIVERSAL);
      item.departureDateFormat = this.dateService.convertStringToDate(item['departureDate'], DateService.DATE_FORMAT_UNIVERSAL);
      item.billingAccountItemDateFormat = this.dateService
        .convertStringToDate(item.billingAccountItemDate, DateService.DATE_FORMAT_UNIVERSAL);
      return item;
    });
  }
}
