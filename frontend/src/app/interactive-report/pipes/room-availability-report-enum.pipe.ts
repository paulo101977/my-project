import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HousekeepingRoomDisagreementEnum, RoomAvailabilityEnum } from '../models/reports.enum';

@Pipe({
  name: 'roomAvailabilityReportText',
})
export class RoomAvailabilityReportEnumPipe implements PipeTransform {
  constructor(private translateService: TranslateService) {}

  // TODO: create pipe to HousekeepingRoomDisagreementEnum
  transform(roomAvailabilityEnum: RoomAvailabilityEnum | HousekeepingRoomDisagreementEnum) {
    switch (roomAvailabilityEnum) {
      case HousekeepingRoomDisagreementEnum.Vague:
      case RoomAvailabilityEnum.Vague:
        return this.translateService.instant('commomData.roomAvailability.vague');
      case HousekeepingRoomDisagreementEnum.Blocked:
      case RoomAvailabilityEnum.Blocked :
        return this.translateService.instant('commomData.roomAvailability.blocked');
      case HousekeepingRoomDisagreementEnum.Busy:
      case RoomAvailabilityEnum.Busy:
        return this.translateService.instant('commomData.roomAvailability.busy');
    }
  }
}
