import {ItemsResponse} from 'app/shared/models/backend-api/item-response';
import {ReportPensionPrevision} from 'app/interactive-report/models/pension-prevision';

export const PENSION_PREVISION_LIST_DATA = <ItemsResponse<ReportPensionPrevision>>{
  hasNext: false,
  items: [
    {
      date: '2019-06-20T00:00:00',
      totalOccupied: 0,
      totalCheckIn: 0,
      totalCheckOut: 0,
      totalAdult: 0,
      totalChild: 0,
      totalChild2: 0,
      totalChild3: 0,
      mealPlan: [
        {
          mealPlanType: 4,
          mealPlanTypeName: 'Meia Pensão Almoço',
          totalAdult: 1,
          totalChild: 1,
          totalChild2: 1,
          totalChild3: 0,
          totalMealPlan: 3
        },
        {
          mealPlanType: 6,
          mealPlanTypeName: 'Pensão Completa',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 1,
          mealPlanTypeName: 'Sem Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 2,
          mealPlanTypeName: 'Café da Manhã',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 3,
          mealPlanTypeName: 'Meia Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 7,
          mealPlanTypeName: 'Tudo Incluso',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 5,
          mealPlanTypeName: 'Meia Pensão Jantar',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        }
      ]
    },
    {
      date: '2019-06-21T00:00:00',
      totalOccupied: 0,
      totalCheckIn: 0,
      totalCheckOut: 0,
      totalAdult: 0,
      totalChild: 0,
      totalChild2: 0,
      totalChild3: 0,
      mealPlan: [
        {
          mealPlanType: 4,
          mealPlanTypeName: 'Meia Pensão Almoço',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 6,
          mealPlanTypeName: 'Pensão Completa',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 1,
          mealPlanTypeName: 'Sem Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 2,
          mealPlanTypeName: 'Café da Manha',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 3,
          mealPlanTypeName: 'Meia Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 7,
          mealPlanTypeName: 'Tudo Incluso',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 5,
          mealPlanTypeName: 'Meia Pensão Jantar',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        }
      ]
    },
    {
      date: '2019-06-22T00:00:00',
      totalOccupied: 0,
      totalCheckIn: 0,
      totalCheckOut: 0,
      totalAdult: 0,
      totalChild: 0,
      totalChild2: 0,
      totalChild3: 0,
      mealPlan: [
        {
          mealPlanType: 4,
          mealPlanTypeName: 'Meia Pensão Almoço',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 6,
          mealPlanTypeName: 'Pensão Completa',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 1,
          mealPlanTypeName: 'Sem Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 2,
          mealPlanTypeName: 'Café da Manha',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 3,
          mealPlanTypeName: 'Meia Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 7,
          mealPlanTypeName: 'Tudo Incluso',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 5,
          mealPlanTypeName: 'Meia Pensão Jantar',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        }
      ]
    },
    {
      date: '2019-06-23T00:00:00',
      totalOccupied: 0,
      totalCheckIn: 0,
      totalCheckOut: 0,
      totalAdult: 0,
      totalChild: 0,
      totalChild2: 0,
      totalChild3: 0,
      mealPlan: [
        {
          mealPlanType: 4,
          mealPlanTypeName: 'Meia Pensão Almoço',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 6,
          mealPlanTypeName: 'Pensão Completa',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 1,
          mealPlanTypeName: 'Sem Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 2,
          mealPlanTypeName: 'Café da Manha',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 3,
          mealPlanTypeName: 'Meia Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 7,
          mealPlanTypeName: 'Tudo Incluso',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 5,
          mealPlanTypeName: 'Meia Pensão Jantar',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        }
      ]
    },
    {
      date: '2019-06-24T00:00:00',
      totalOccupied: 0,
      totalCheckIn: 4,
      totalCheckOut: 0,
      totalAdult: 4,
      totalChild: 0,
      totalChild2: 0,
      totalChild3: 0,
      mealPlan: [
        {
          mealPlanType: 4,
          mealPlanTypeName: 'Meia Pensão Almoço',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 6,
          mealPlanTypeName: 'Pensão Completa',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 1,
          mealPlanTypeName: 'Sem Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 2,
          mealPlanTypeName: 'Café da Manha',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 3,
          mealPlanTypeName: 'Meia Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 7,
          mealPlanTypeName: 'Tudo Incluso',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 5,
          mealPlanTypeName: 'Meia Pensão Jantar',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        }
      ]
    },
    {
      date: '2019-06-25T00:00:00',
      totalOccupied: 0,
      totalCheckIn: 0,
      totalCheckOut: 4,
      totalAdult: 4,
      totalChild: 0,
      totalChild2: 0,
      totalChild3: 0,
      mealPlan: [
        {
          mealPlanType: 4,
          mealPlanTypeName: 'Meia Pensão Almoço',
          totalAdult: 4,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 4
        },
        {
          mealPlanType: 6,
          mealPlanTypeName: 'Pensão Completa',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 1,
          mealPlanTypeName: 'Sem Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 2,
          mealPlanTypeName: 'Café da Manha',
          totalAdult: 4,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 4
        },
        {
          mealPlanType: 3,
          mealPlanTypeName: 'Meia Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 7,
          mealPlanTypeName: 'Tudo Incluso',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 5,
          mealPlanTypeName: 'Meia Pensão Jantar',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        }
      ]
    },
    {
      date: '2019-06-26T00:00:00',
      totalOccupied: 0,
      totalCheckIn: 0,
      totalCheckOut: 0,
      totalAdult: 0,
      totalChild: 0,
      totalChild2: 0,
      totalChild3: 0,
      mealPlan: [
        {
          mealPlanType: 4,
          mealPlanTypeName: 'Meia Pensão Almoço',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 6,
          mealPlanTypeName: 'Pensão Completa',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 1,
          mealPlanTypeName: 'Sem Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 2,
          mealPlanTypeName: 'Café da Manha',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 3,
          mealPlanTypeName: 'Meia Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 7,
          mealPlanTypeName: 'Tudo Incluso',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 5,
          mealPlanTypeName: 'Meia Pensão Jantar',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        }
      ]
    },
    {
      date: '2019-06-27T00:00:00',
      totalOccupied: 0,
      totalCheckIn: 0,
      totalCheckOut: 0,
      totalAdult: 0,
      totalChild: 0,
      totalChild2: 0,
      totalChild3: 0,
      mealPlan: [
        {
          mealPlanType: 4,
          mealPlanTypeName: 'Meia Pensão Almoço',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 6,
          mealPlanTypeName: 'Pensão Completa',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 1,
          mealPlanTypeName: 'Sem Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 2,
          mealPlanTypeName: 'Café da Manha',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 3,
          mealPlanTypeName: 'Meia Pensão',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 7,
          mealPlanTypeName: 'Tudo Incluso',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        },
        {
          mealPlanType: 5,
          mealPlanTypeName: 'Meia Pensão Jantar',
          totalAdult: 0,
          totalChild: 0,
          totalChild2: 0,
          totalChild3: 0,
          totalMealPlan: 0
        }
      ]
    }
  ]
};
