import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SingleAccountComponent } from './components/single-account/single-account.component';
import { GuestAccountComponent } from './components/guest-account/guest-account.component';
import { ClientAccountComponent } from './components/client-account/client-account.component';
import { AccountTypeCreateComponent } from './components/account-type-create/account-type-create.component';
import { DocumentTypeListResolver } from '../shared/services/resolvers/document-type/document-type-list-resolver.service';

export const newAccountTypeRoutes: Routes = [
  { path: '', redirectTo: 'single-account', pathMatch: 'full' },
  {
    path: '',
    component: AccountTypeCreateComponent,
    children: [
      { path: 'single-account', resolve: { documentTypes: DocumentTypeListResolver }, component: SingleAccountComponent },
      { path: 'guest-account', component: GuestAccountComponent },
      { path: 'client-account', component: ClientAccountComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(newAccountTypeRoutes)],
  exports: [RouterModule],
  providers: [DocumentTypeListResolver],
})
export class AccountTypeRoutingModule {}
