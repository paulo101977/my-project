import { ClientDocumentTypesDto } from 'app/shared/models/dto/client/client-document-types';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ButtonBarConfig } from 'app/shared/components/button-bar/button-bar-config';
import { ButtonBarButtonConfig } from 'app/shared/components/button-bar/button-bar-button-config';
import { ButtonBarColor } from 'app/shared/components/button-bar/button-bar-color.enum';
import { ButtonBarButtonStatus } from 'app/shared/components/button-bar/button-bar-button-status.enum';
import { DocumentType, DocumentTypeLegalEnum, DocumentTypeNaturalEnum } from 'app/shared/models/document-type';
import { UpdateItemResponse } from 'app/shared/models/autocomplete/update-item-response';
import { SearchPersonsResultDto } from 'app/shared/models/person/search-persons-result-dto';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { SearchClientAndPersonsResultDto } from 'app/shared/models/dto/search-client-person-result-dto';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { BillingAccountMapper } from 'app/shared/mappers/billing-account-mapper';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { AddressService } from 'app/shared/services/shared/address.service';
import { DocumentTypeResource } from 'app/shared/resources/document-type/document-type.resource';
import { PersonResource } from 'app/shared/resources/person/person.resource';
import { BillingAccountResource } from 'app/shared/resources/billing-account/billing-account.resource';
import { MarketSegmentService } from 'app/shared/services/market-segment/market-segment.service';
import { debounceTime, map, pluck, switchMap } from 'rxjs/operators';
import { OriginResource } from 'app/shared/resources/origin/origin.resource';
import { MarketSegmentResource } from 'app/shared/resources/market-segment/market-segment.resource';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { AutocompleteDisplayEnum, AutocompleteTypeEnum } from 'app/shared/models/autocomplete/autocomplete.enum';
import { ValidatorFormService } from 'app/shared/services/shared/validator-form.service';
import { CountryCodeEnum } from 'app/shared/models/country-code-enum';
import { DocumentService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'single-account',
  templateUrl: 'single-account.component.html',
})
export class SingleAccountComponent implements OnInit {
  public buttonBarConfig: ButtonBarConfig;
  public clientName: string;
  public singleAccountForm: FormGroup;
  public autocompleteDocumentTypes: Array<DocumentType>;
  public autocompleSelectItemsDocumentTypes: Array<SelectObjectOption>;
  public referenceToDisplayAutocompleteEnum = AutocompleteDisplayEnum;
  public referenceToTypeAutocompleteEnum = AutocompleteTypeEnum;
  public propertyId: number;

  public optionsOriginList: any;
  public optionsBusinessSourceList: any;
  public inputIdDocument: string;
  public paramDisplayDocument: string;
  public autocompleItemsDocument: Array<SearchClientAndPersonsResultDto>;
  public searchDataDocument: string;
  public optionDocumentType: DocumentType;

  public inputIdEmail: string;
  public autocompleItemsEmail: Array<SearchPersonsResultDto>;
  private locationIsValid: boolean;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    private documentTypeResource: DocumentTypeResource,
    private router: Router,
    private route: ActivatedRoute,
    private personResource: PersonResource,
    private location: Location,
    private commomService: CommomService,
    private billingAccountMapper: BillingAccountMapper,
    private billingAccountResorce: BillingAccountResource,
    private toasterEmitService: ToasterEmitService,
    private sharedService: SharedService,
    private addressService: AddressService,
    private marketSegmentService: MarketSegmentService,
    private originResource: OriginResource,
    private marketSegmentResource: MarketSegmentResource,
    private validatorFormService: ValidatorFormService,
    private documentService: DocumentService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.setSingleAccountForm();
      this.setVars();
      this.setListeners();
      this.getDocumentTypes('n');
    });
  }

  private setVars() {
    this.optionsOriginList = [];
    this.optionsBusinessSourceList = [];
    this.optionDocumentType = new DocumentType();
    this.getOrigins();
    this.marketSegments();
    this.setConfigButtonBar();
    this.clientName = 'accountTypeModule.new.singleAccount.naturalClientName';
    this.setConfigAutocompleItemsDocument();
    this.setConfigAutocompleItemsEmail();
    this.setButtonConfig();
  }

  private getOrigins(): void {
    this.originResource.getOrigin(this.propertyId).subscribe(result => {
      this.optionsOriginList = this.commomService
        .toOption(result, 'id', 'description');
    });
  }

  private marketSegments() {
    this.marketSegmentResource.getAllMarketSegmentList(this.propertyId)
      .subscribe(result => {
        this.optionsBusinessSourceList = this.marketSegmentService.prepareSecondarySelectList(result.items);
      });
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'single-account-cancel',
      this.goBackPreviewPage,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'single-account-save',
      this.saveSingleAccount,
      'commomData.createAccount',
      ButtonType.Primary,
    );
  }

  private setConfigButtonBar() {
    this.buttonBarConfig = {
      color: ButtonBarColor.blue,
      buttons: [
        {
          title: 'accountTypeModule.new.singleAccount.naturalPerson',
          status: ButtonBarButtonStatus.active,
          callBackFunction: () => {
            this.clientName = 'accountTypeModule.new.singleAccount.naturalClientName';
            this.autocompleteDocumentTypes = null;
            this.autocompleSelectItemsDocumentTypes = null;
            this.getDocumentTypes('n');
          },
        } as ButtonBarButtonConfig,
        {
          title: 'accountTypeModule.new.singleAccount.legalPerson',
          callBackFunction: () => {
            this.clientName = 'accountTypeModule.new.singleAccount.legalClientName';
            this.autocompleteDocumentTypes = null;
            this.autocompleSelectItemsDocumentTypes = null;
            this.getDocumentTypes('l');
          },
        } as ButtonBarButtonConfig,
      ],
    };
  }

  private setSingleAccountForm() {
    this.singleAccountForm = this.formBuilder.group({
      personId: [''],
      companyClientId: [''],
      guestId: [''],
      personType: ['', [Validators.required]],
      document: ['', [Validators.required, this.documentIsValid()]],
      documentTypeId: ['', [Validators.required]],
      email: ['', [Validators.required]],
      clientName: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      staticalData: this.formBuilder.group({
        origin: ['', [Validators.required]],
        marketSegment: ['', [Validators.required]],
      }),
      address: this.formBuilder.group({
        id: null,
        ownerId: GuidDefaultData,
        postalCode: null,
        streetName: null,
        streetNumber: null,
        additionalAddressDetails: null,
        division: [{value: null, disabled: true}],
        subdivision: [{value: null, disabled: true}],
        neighborhood: null,
        browserLanguage: window.navigator.language,
        latitude: null,
        longitude: null,
        countryCode: null,
        country: null,
        completeAddress: null,
        countrySubdivisionId: null,
        locationCategoryId: null
      }),
    });
  }

  private setListeners(): void {
    this.singleAccountForm.valueChanges.subscribe(value => {
      if (value) {
        this.optionDocumentType.id = value.documentTypeId;
      }
      this.buttonSaveConfig.isDisabled = this.singleAccountForm.invalid || !this.locationIsValid;
    });
  }

  private documentIsValid() {
    return (control) => {
      if (this.singleAccountForm && control.value) {
        const documentTypeId = this.singleAccountForm.get('documentTypeId').value;
        const countryCode = this.singleAccountForm.get('address').get('countryCode').value;
        switch (+documentTypeId) {
          case DocumentTypeNaturalEnum.NIF:
          case DocumentTypeLegalEnum.NIF:
            if (countryCode == CountryCodeEnum.PT) {
              return this.documentService
                .nifControlIsValid(documentTypeId, control.value, countryCode);
            }
            return null;
          default:
            return null;
        }
      }
    };
  }

  public setValueAddressByEventEmitted(address) {
    this.locationIsValid = address.valid;
    this.singleAccountForm.get('address').patchValue(address);
  }

  private getDocumentTypes(personType: string) {
    this.singleAccountForm.get('personType').setValue(personType);
    this.documentTypeResource.getAllByPropertyCountryCode()
      .pipe(
        pluck('items'),
        map((items: ClientDocumentTypesDto[]) => items.filter(item => item.personType === personType.toUpperCase())),
      )
      .subscribe((items: ClientDocumentTypesDto[]) => {
        this.autocompleSelectItemsDocumentTypes = this.commomService.toOption(
          items,
          'id',
          'name',
        );
        this.autocompleteDocumentTypes = items;
        this.singleAccountForm.get('documentTypeId').setValue(this.autocompleSelectItemsDocumentTypes[0].key);
      });
  }

  private setConfigAutocompleItemsDocument() {
    this.inputIdDocument = this.paramDisplayDocument = 'document';
  }

  private setConfigAutocompleItemsEmail() {
    this.inputIdEmail = 'email';
  }

  public searchItemDocument(searchData: string) {
    this.searchDataDocument = searchData;
    if (searchData && this.optionDocumentType) {
      this.singleAccountForm
        .get('document')
        .valueChanges
        .pipe(
          debounceTime(1000),
          switchMap(criteria =>
            this.personResource.getClientAndPersonsByCriteria(
              criteria,
              this.propertyId,
              this.singleAccountForm.get('personType').value,
              this.optionDocumentType,
            ),
          )
        )
        .subscribe(persons => {
          this.autocompleItemsDocument = persons.items;
        });
    }
  }

  public updateItemDocument(itemData: UpdateItemResponse<SearchPersonsResultDto> | string, singleAccountForm: FormGroup) {
    if (typeof itemData === 'string') {
      singleAccountForm.get('document').setValue(itemData);
      singleAccountForm.get('personId').setValue(null);
      singleAccountForm.get('companyClientId').setValue(null);
    } else {
      singleAccountForm.get('document').setValue(itemData.completeItem.document);
      this.setFormGroupWithAutocompleteData(itemData.completeItem);
    }
  }

  private setFormGroupWithAutocompleteData(result: SearchPersonsResultDto): void {
    this.singleAccountForm.patchValue({
      personId: result.id,
      companyClientId: result.companyClientId,
      guestId: result.guestId,
      email: result.contactInformation.find(c => c.typeValue == 'Email').value,
      clientName: result.name,
      phone: result.contactInformation.find(c => c.typeValue == 'PhoneNumber').value,
      address: result.locationList[0],
    });
  }

  public getDocumentInformation(guest: FormGroup): string {
    return guest.value.document;
  }

  public searchByEmail(searchData: string) {
    if (searchData) {
      this.personResource.getPersonsByEmail(searchData).subscribe(persons => {
        this.autocompleItemsEmail = persons;
      });
    }
  }

  public saveSingleAccount = () => {
    const dataToSave = this.billingAccountMapper.toSaveSingleAccountDto(this.singleAccountForm, this.propertyId);

    this.billingAccountResorce.saveBillingAccountSparce(dataToSave).subscribe(response => {
      this.toasterEmitService.emitChange(SuccessError.success, 'commomData.saveBillingAccount');
      this.router.navigate(['../../edit'], {
        relativeTo: this.route,
        queryParams: { billingAccountId: response.id },
      });
    });
  }

  public goBackPreviewPage = () => {
    this.location.back();
  }

}
