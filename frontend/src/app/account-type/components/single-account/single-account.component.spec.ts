// TODO: create tests [DEBIT: 7517]

// import { ComponentFixture, TestBed, async, tick, fakeAsync } from '@angular/core/testing';
// import { ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { Function } from 'core-js/library/web/timers';
// import { HttpClientModule } from '@angular/common/http/';
// import { RouterTestingModule } from '@angular/router/testing';
// import { ActivatedRoute } from '@angular/router';
// import { Observable } from 'rxjs';
// import { Router } from '@angular/router';
// import { ButtonBarConfig } from '../../../shared/components/button-bar/button-bar-config';
// import { ButtonBarButtonConfig } from '../../../shared/components/button-bar/button-bar-button-config';
// import { ButtonBarButtonStatus } from '../../../shared/components/button-bar/button-bar-button-status.enum';
// import { SingleAccountComponent } from './single-account.component';
// import { SelectModel } from '../../../shared/models/selectModel';
// import { ItemsResponse } from '../../../shared/models/backend-api/item-response';
// import { DocumentType } from '../../../shared/models/document-type';
// import { SearchPersonsResultDto } from '../../../shared/models/person/search-persons-result-dto';
// import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
// import { LocationDto } from '../../../shared/models/dto/location-dto';
// import { SharedService } from '../../../shared/services/shared/shared.service';
// import { GuestService } from '../../../shared/services/guest/guest.service';
// import { CommomService } from '../../../shared/services/shared/commom.service';
// import { BillingAccountMapper } from '../../../shared/mappers/billing-account-mapper';
// import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
// import { AddressService } from '../../../shared/services/shared/address.service';
// import { SessionParameterService } from '../../../shared/services/parameter/session-parameter.service';
//
// import { DocumentTypeResource } from '../../../shared/resources/document-type/document-type.resource';
// import { PersonResource } from '../../../shared/resources/person/person.resource';
// import { UpdateItemResponse } from '../../../shared/models/autocomplete/update-item-response';
// import { BookingDetailsResource } from '../../../shared/resources/booking-details/booking-details.resource';
// import { BillingAccountResource } from '../../../shared/resources/billing-account/billing-account.resource';
// import { ParameterResource } from '../../../shared/resources/parameter/parameter-resource';
//
// describe('SingleAccountComponent', () => {
//   let component: SingleAccountComponent;
//   let fixture: ComponentFixture<SingleAccountComponent>;
//   let routeStub;
//   const formBuilder = new FormBuilder();
//
//   beforeAll(() => {
//     TestBed.resetTestEnvironment();
//
//     routeStub = {
//       data: null
//     };
//
//     TestBed
//       .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
//       .configureTestingModule({
//         imports: [
//           TranslateModule.forRoot(),
//           ReactiveFormsModule,
//
//           HttpClientModule,
//           RouterTestingModule
//         ],
//         declarations: [SingleAccountComponent],
//         providers: [],
//         schemas: [NO_ERRORS_SCHEMA]
//       });
//
//     fixture = TestBed.createComponent(SingleAccountComponent);
//     component = fixture.componentInstance;
//
//     const documentTypes: ItemsResponse<DocumentType> = {
//       items: [],
//       hasNext: false,
//       total: 0
//     };
//
//     fixture.detectChanges();
//     fixture.whenStable().then(() => {
//       expect(component.autocompleSelectItemsDocumentTypes).toEqual(documentTypes.items);
//     });
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should setVars', () => {
//     spyOn(component, 'setConfigButtonBar');
//     spyOn(component, 'setSingleAccountForm');
//
//     component['setVars']();
//
//     expect(component.clientName).toEqual('accountTypeModule.new.singleAccount.naturalClientName');
//     expect(component['setConfigButtonBar']).toHaveBeenCalled();
//   });
//
//   it('should setConfigButtonBar', () => {
//     component['setConfigButtonBar']();
//
//     expect(component.buttonBarConfig.buttons).not.toBeNull();
//     expect(component.buttonBarConfig.buttons.length).toEqual(2);
//     expect(component.buttonBarConfig.buttons[0].title).toEqual('accountTypeModule.new.singleAccount.naturalPerson');
//     expect(component.buttonBarConfig.buttons[0].status).toEqual(ButtonBarButtonStatus.active);
//     expect(component.buttonBarConfig.buttons[0].callBackFunction).not.toBeNull();
//     expect(component.buttonBarConfig.buttons[1].title).toEqual('accountTypeModule.new.singleAccount.legalPerson');
//     expect(component.buttonBarConfig.buttons[1].callBackFunction).not.toBeNull();
//   });
//
//   it('should instance singleAccountForm and controls', () => {
//     expect(component.singleAccountForm.controls).toBeDefined();
//     expect(component.singleAccountForm instanceof FormGroup).toBe(true);
//     expect(Object.keys(component.singleAccountForm.controls)).toEqual([
//       'personId', 'companyClientId', 'guestId', 'personType', 'document',
// 'documentTypeId', 'email', 'clientName', 'phone', 'staticalData', 'address'
//     ]);
//   });
//
//   describe('on address', () => {
//     it('should set ValueAddressTypeByEventEmitted', () => {
//       const optionSelected = new SelectModel<string>();
//       optionSelected.formGroupName = 'address.selectValueType';
//       optionSelected.fieldName = 'type';
//       optionSelected.value = 'Avenida';
//
//       component.setValueAddressByEventEmitted(optionSelected);
//
//       expect(component.singleAccountForm.get(optionSelected.formGroupName + '.' + optionSelected.fieldName).value)
// .toEqual(optionSelected.value);
//     });
//   });
//
//   describe('on document autocomplete with select', () => {
//
//     it('should call updateDocumentForm', () => {
//       spyOn(component['addressService'], 'getLocationCategoryEnumByLocationCategoryId');
//       const item = new UpdateItemResponse<SearchPersonsResultDto>();
//       item.completeItem = new SearchPersonsResultDto();
//       item.completeItem.document = '';
//       item.completeItem.documentType = 'CPF';
//       item.completeItem.name = '';
//       item.completeItem.locationList = new Array<LocationDto>();
//       item.completeItem.locationList.push(new LocationDto());
//       item.completeItem.contactInformation = new Array<{ type: any, typeValue: string, value: string }>();
//       item.completeItem.contactInformation.push({
//         type: null,
//         typeValue: 'Email',
//         value: 'teste1@email.com'
//       });
//       item.completeItem.contactInformation.push({
//         type: null,
//         typeValue: 'PhoneNumber',
//         value: '123345678'
//       });
//
//       const guest = formBuilder.group({
//         document: '',
//         documentTypeId: '',
//         id: '',
//         clientName: '',
//       });
//
//       component.updateItemDocument(item, guest);
//
//       expect(guest.get('document').value).toEqual('');
//       expect(guest.get('clientName').value).toEqual('');
//     });
//
//     it('should call updateAutocompleteItemsDocument', () => {
//       spyOn(component, 'searchItemDocument');
//
//       const option = new DocumentType();
//       option.name = 'CPF';
//
//       component.searchDataDocument = '';
//
//       component.updateAutocompleteItemsDocument(option);
//
//       expect(component.optionDocument).toEqual(option);
//       expect(component.searchItemDocument).toHaveBeenCalledWith(component.searchDataDocument);
//     });
//
//     it('should searchItemDocument', fakeAsync(() => {
//       const search = 'abcde';
//
//       const searchPersonsResult = new SearchPersonsResultDto();
//       searchPersonsResult.id = '1';
//       const searchPersonsResult2 = new SearchPersonsResultDto();
//       searchPersonsResult2.id = '2';
//       const searchPersonsResult3 = new SearchPersonsResultDto();
//       searchPersonsResult3.id = '3';
//
//       const personsResult = new Array<SearchPersonsResultDto>();
//       personsResult.push(searchPersonsResult);
//       personsResult.push(searchPersonsResult2);
//       personsResult.push(searchPersonsResult3);
//
//       const promise = spyOn(component['personResource'], 'getPersonsByCriteria').and.callFake(() => {
//         return new Promise((resolve, reject) => resolve(personsResult));
//       });
//       promise(search, this.optionDocument).then(persons => component.autocompleItemsDocument = persons);
//       tick();
//
//       component.searchItemDocument(search);
//
//       expect(component.searchDataDocument).toEqual(search);
//       expect(component.autocompleItemsDocument).toEqual(personsResult);
//     }));
//
//     it('should getDocumentInformation', () => {
//       const guest = formBuilder.group({
//         document: { documentInformation: '999.999.999-99' }
//       });
//
//       expect(component.getDocumentInformation(guest)).toEqual({ documentInformation: '999.999.999-99' });
//     });
//
//     it('should not getDocumentInformation', () => {
//       const guest = formBuilder.group({
//         document: null
//       });
//
//       expect(component.getDocumentInformation(guest)).toEqual(null);
//
//       const guest2 = formBuilder.group({
//         document: ''
//       });
//
//       expect(component.getDocumentInformation(guest2)).toEqual('');
//     });
//   });
//
//   describe('on email autocomplete', () => {
//
//     it('should searchGuestByEmail', fakeAsync(() => {
//       const search = 'abcde';
//
//       const searchPersonsResult = new SearchPersonsResultDto();
//       searchPersonsResult.id = '1';
//       const searchPersonsResult2 = new SearchPersonsResultDto();
//       searchPersonsResult2.id = '2';
//       const searchPersonsResult3 = new SearchPersonsResultDto();
//       searchPersonsResult3.id = '3';
//
//       const personsResult = new Array<SearchPersonsResultDto>();
//       personsResult.push(searchPersonsResult);
//       personsResult.push(searchPersonsResult2);
//       personsResult.push(searchPersonsResult3);
//
//       const promise = spyOn(component['personResource'], 'getPersonsByEmail').and.callFake(() => {
//         return new Promise((resolve, reject) => resolve(personsResult));
//       });
//       promise(search).then(persons => component.autocompleItemsEmail = persons);
//       tick();
//
//       component.searchByEmail(search);
//
//       expect(component.autocompleItemsEmail).toEqual(personsResult);
//     }));
//   });
//
//   describe('on methods', () => {
//     it('should goBackToPreviewPage', () => {
//       spyOn(component['location'], 'back');
//
//       component.goBackPreviewPage();
//
//       expect(component['location']['back']).toHaveBeenCalled();
//     });
//   });
// });
