import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientModule} from '@angular/common/http/';
import {of} from 'rxjs';
import {RouterTestingModule} from '@angular/router/testing';
import {GuestAccountComponent} from '../../components/guest-account/guest-account.component';
import {GuestAccount} from 'app/shared/models/guest-account';
import {GuestAccountData, GuestAccountData1} from 'app/shared/mock-data/guest-account-deta';
import {ButtonConfig, ButtonType} from 'app/shared/models/button-config';
import {InterceptorHandlerService, ThexApiTestingModule} from '@inovacaocmnet/thx-bifrost';
import {configureTestSuite} from 'ng-bullet';
import {guestServiceStub} from 'app/shared/services/guest/testing';
import {ItemsResponse} from 'app/shared/models/backend-api/item-response';
import {ReservationStatusEnum} from 'app/shared/models/reserves/reservation-status-enum';
import {SuccessError} from 'app/shared/models/success-error-enum';

describe('GuestAccountComponent', () => {
  let component: GuestAccountComponent;
  let fixture: ComponentFixture<GuestAccountComponent>;

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        imports: [
          ReactiveFormsModule,
          RouterTestingModule,
          HttpClientModule,
          TranslateModule.forRoot(),
          ThexApiTestingModule
        ],
        declarations: [
          GuestAccountComponent
        ],
        providers: [
          InterceptorHandlerService,
          guestServiceStub,
        ],
        schemas: [
          NO_ERRORS_SCHEMA
        ]
      });

    fixture = TestBed.createComponent(GuestAccountComponent);
    component = fixture.componentInstance;

    const guestAccountResponse = <ItemsResponse<any>>{
      items: new Array<GuestAccount>(),
      hasNext: false,
    };
    guestAccountResponse.items.push(GuestAccountData);
    guestAccountResponse.items.push(GuestAccountData1);
    spyOn(component['guestResource'], 'getGuestAccountByPropertyId').and.returnValue(of(guestAccountResponse));

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should set vars', () => {

      expect(component.columns).toEqual([
        {name: '', cellTemplate: component.rowRadio, flexGrow: 0.2},
        {name: 'text.reservation', prop: 'reservationCode'},
        {name: 'label.guestName', prop: 'guestName'},
        {name: 'label.arrivalDate', prop: 'strArrivalDate'},
        {name: 'label.departureDate', prop: 'strDepartureDate'},
        {name: 'label.statusAccommodation', prop: 'strReservationItemStatus'}
      ]);
      expect(component.allItemList).toEqual([ GuestAccountData , GuestAccountData1 ]);
    });

    it('should set buttonConfig', () => {
      const cancelBtn = new ButtonConfig();
      cancelBtn.textButton = 'commomData.cancel';
      cancelBtn.buttonType = ButtonType.Secondary;
      cancelBtn.callback = component.goBackPreviewPage;

      const saveBtn = new ButtonConfig();
      saveBtn.textButton = 'commomData.createAccount';
      saveBtn.buttonType = ButtonType.Primary;
      saveBtn.callback = component.saveGuestAccount;

      expect(component.buttonCancelConfig).toEqual(cancelBtn);
      expect(component.buttonSaveConfig).toEqual(saveBtn);
    });
  });

  describe('on methods', () => {
    it('should setListFiltered', () => {
        const listFiltered = [];
        listFiltered.push(GuestAccountData);
        listFiltered.push(GuestAccountData1);

        component.setListFiltered(listFiltered);

        expect(component.pageItemsList[1].guestName).toEqual(GuestAccountData1.guestName);
        expect(component.pageItemsList[0].guestName).toEqual(GuestAccountData.guestName);
    });

    it('should getGuestAccountData', () => {
        component.getGuestAccountData(GuestAccountData);

        expect(component.guestSelected).toEqual(GuestAccountData);
    });

    it('should goBackToPreviewPage', () => {
      spyOn(component['location'], 'back');

      component.goBackPreviewPage();

      expect(component['location']['back']).toHaveBeenCalled();
    });

    it('should goToNewReservationPage', () => {
      component.propertyId = 1;
      spyOn(component['router'], 'navigate');

      component.goToNewReservationPage();

      expect(component['router'].navigate).toHaveBeenCalledWith(['p', 1, 'reservation', 'new']);
    });

    it('should close newAccountModal when closeNewAccountModal fired', () => {
      spyOn(component['modalEmitToggleService'], 'emitCloseWithRef');

      component.closeNewAccountModal();

      expect(component['modalEmitToggleService'].emitCloseWithRef).toHaveBeenCalledWith(component.MODAL_NEW_ACCOUNT);
    });

    it('should open newAccountModal when openNewAccountModal fired', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');

      component.openNewAccountModal();

      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.MODAL_NEW_ACCOUNT);
    });

    it(' should save account when confirmNewAccountModal fired', () => {
      const accountName = 'Account Name';
      component.guestSelected = new GuestAccount();
      component.guestSelected.billingAccountId = '1';
      spyOn(component['billingAccountResource'], 'createBillingAccountWithEntry').and.returnValue(of('algo'));
      spyOn(component['toasterEmitService'], 'emitChange');
      spyOn(component, 'closeNewAccountModal');

      component['confirmNewAccountModal'](accountName);

      expect(component['billingAccountResource'].createBillingAccountWithEntry)
        .toHaveBeenCalledWith(component.guestSelected.billingAccountId, accountName);
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalled();
      expect(component.closeNewAccountModal).toHaveBeenCalled();
    });
  });

  describe('#onSave', () => {
    it('should show toaster when guest is checkout', () => {
      spyOn<any>(component['toasterEmitService'], 'emitChange');
      spyOn<any>(component['translateService'], 'instant').and.callFake( () => 'bla' );
      component.guestSelected = new GuestAccount();
      component.guestSelected.reservationItemStatus = ReservationStatusEnum.Checkout;

      component.saveGuestAccount();

      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(SuccessError.error, 'bla');
    });
  });

});
