import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DatePipe, Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { GuestAccount, GuestAccountDatatable } from 'app/shared/models/guest-account';
import { TableRowTypeEnum } from 'app/shared/models/table-row-type-enum';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { BillingAccountMapper } from 'app/shared/mappers/billing-account-mapper';
import { GuestResource } from 'app/shared/resources/guest/guest.resource';
import { BillingAccountResource } from 'app/shared/resources/billing-account/billing-account.resource';
import { StatusReservationTextPipe } from 'app/shared/pipes/status-reservation-text.pipe';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';

@Component({
  selector: 'guest-account',
  templateUrl: 'guest-account.component.html',
  providers: [DatePipe, StatusReservationTextPipe],
})
export class GuestAccountComponent implements OnInit {
  @ViewChild('rowRadio') rowRadio: TemplateRef<any>;

  public readonly MODAL_NEW_ACCOUNT: 'modal_new_account';

  public guestSelected: GuestAccount;
  public propertyId: number;

  // Table dependencies
  public columns: Array<any>;

  public allItemList: Array<GuestAccount>;
  public pageItemsList: Array<GuestAccountDatatable>;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;

  public tableRowTypeEnum: TableRowTypeEnum;

  constructor(
    private guestResource: GuestResource,
    private billingAccountResource: BillingAccountResource,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private billingAccountMapper: BillingAccountMapper,
    private toasterEmitService: ToasterEmitService,
    private billingAccountResorce: BillingAccountResource,
    public datePipe: DatePipe,
    public statusReservationTextPipe: StatusReservationTextPipe,
    private translateService: TranslateService,
    private modalEmitToggleService: ModalEmitToggleService
  ) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setConfigTable();
    this.getGuest();
    this.setButtonConfig();
  }

  private setButtonConfig() {
    this.buttonCancelConfig = new ButtonConfig();
    this.buttonCancelConfig.textButton = 'commomData.cancel';
    this.buttonCancelConfig.buttonType = ButtonType.Secondary;
    this.buttonCancelConfig.callback = this.goBackPreviewPage;

    this.buttonSaveConfig = new ButtonConfig();
    this.buttonSaveConfig.textButton = 'commomData.createAccount';
    this.buttonSaveConfig.buttonType = ButtonType.Primary;
    this.buttonSaveConfig.callback = this.saveGuestAccount;
  }

  private setConfigTable() {
    this.setColumnsName();
    this.tableRowTypeEnum = TableRowTypeEnum.GuestAccount;
  }

  private setColumnsName() {
    this.columns = [];
    this.columns.push({
      name: '',
      cellTemplate: this.rowRadio,
      flexGrow: 0.2,
    });
    this.columns.push({
      name: 'text.reservation',
      prop: 'reservationCode',
    });
    this.columns.push({
      name: 'label.guestName',
      prop: 'guestName',
    });
    this.columns.push({
      name: 'label.arrivalDate',
      prop: 'strArrivalDate',
    });
    this.columns.push({
      name: 'label.departureDate',
      prop: 'strDepartureDate',
    });
    this.columns.push({
      name: 'label.statusAccommodation',
      prop: 'strReservationItemStatus',
    });
  }

  private getGuest(): void {
    this.guestResource.getGuestAccountByPropertyId(this.propertyId).subscribe(response => {
      if (response.items) {
        const { items } = response;
        this.allItemList = items;
        this.pageItemsList = this.mapToArray(items);
      }
    });
  }

  private mapToArray(items: Array<GuestAccount>): Array<GuestAccountDatatable> {
    return items.map(item => {
      return <GuestAccountDatatable>{
        ...item,
        strArrivalDate: this.datePipe.transform(item.arrivalDate, 'dd/MM/yyyy'),
        strDepartureDate: this.datePipe.transform(item.departureDate, 'dd/MM/yyyy'),
        strReservationItemStatus: this.statusReservationTextPipe.transform(item.reservationItemStatus),
      };
    });
  }

  public goToNewReservationPage() {
    this.router.navigate(['p', this.propertyId, 'reservation', 'new']);
  }

  public setListFiltered(listFiltered: Array<GuestAccount>) {
    this.pageItemsList = this.mapToArray(listFiltered);
  }

  public getGuestAccountData(guest: GuestAccount) {
    this.guestSelected = guest;
  }

  public saveGuestAccount = () => {
    if (this.guestSelected) {
      if (this.guestSelected.reservationItemStatus === ReservationStatusEnum.Checkout) {
        this.toasterEmitService
          .emitChange(SuccessError.error, this.translateService.instant('text.reservationCheckoutError'));
        return;
      }
      const dataToSave = this.billingAccountMapper.toSaveGuestAccountDto(this.guestSelected, this.propertyId);
      this.billingAccountResorce.saveBillingAccount(dataToSave).subscribe(response => {
        this.toasterEmitService.emitChange(SuccessError.success, 'commomData.saveBillingAccount');
        this.router.navigate(['../../edit'], {
          relativeTo: this.route,
          queryParams: { billingAccountId: response.id },
        });
      }, error => {
        if ( this.guestSelected.reservationItemStatus >= ReservationStatusEnum.Checkin ) {
          this.openNewAccountModal();
        } else {
          this.toasterEmitService
            .emitChange(SuccessError.error, 'text.subAccountReservationConfirmedError');
        }
      });
    }
  }

  public goBackPreviewPage = () => {
    this.location.back();
  }

  public openNewAccountModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_NEW_ACCOUNT);
  }

  public closeNewAccountModal() {
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_NEW_ACCOUNT);
  }

  public confirmNewAccountModal( accountName ) {
    this.billingAccountResource
      .createBillingAccountWithEntry(this.guestSelected.billingAccountId , accountName)
      .subscribe( result => {
        this.toasterEmitService.emitChange(SuccessError.success, 'commomData.saveBillingAccount');
        this.closeNewAccountModal();
      });
  }
}
