import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyClientAccount } from 'app/shared/models/dto/company-client-account-dto';
import { ClientResource } from 'app/shared/resources/client/client.resource';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { BillingAccountMapper } from '../../../shared/mappers/billing-account-mapper';
import { BillingAccountResource } from '../../../shared/resources/billing-account/billing-account.resource';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';

@Component({
  selector: 'client-account',
  templateUrl: 'client-account.component.html',
})
export class ClientAccountComponent implements OnInit {
  @ViewChild('rowRadio') rowRadio: TemplateRef<any>;

  public readonly MODAL_NEW_ACCOUNT: 'modal_new_account';


  public companyClientSelected: CompanyClientAccount;
  public propertyId: number;

  // Table dependencies
  public columns: Array<any>;

  public allItemList: Array<CompanyClientAccount>;
  public pageItemsList: Array<CompanyClientAccount>;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;

  public tableRowTypeEnum: TableRowTypeEnum;

  constructor(
    private clientResource: ClientResource,
    private billingAccountResource: BillingAccountResource,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private billingAccountMapper: BillingAccountMapper,
    private toasterEmitService: ToasterEmitService,
    private billingAccountResorce: BillingAccountResource,
    private translateService: TranslateService,
    private modalEmitToggleService: ModalEmitToggleService
  ) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setConfigTable();
    this.getCompanyClient();
    this.setButtonConfig();
  }

  private setButtonConfig() {
    this.buttonCancelConfig = new ButtonConfig();
    this.buttonCancelConfig.textButton = 'commomData.cancel';
    this.buttonCancelConfig.buttonType = ButtonType.Secondary;
    this.buttonCancelConfig.callback = this.goBackPreviewPage;

    this.buttonSaveConfig = new ButtonConfig();
    this.buttonSaveConfig.textButton = 'commomData.createAccount';
    this.buttonSaveConfig.buttonType = ButtonType.Primary;
    this.buttonSaveConfig.callback = this.saveClientAccount;
  }

  private setConfigTable() {
    this.setColumnsName();
    this.tableRowTypeEnum = TableRowTypeEnum.CompanyClientAccount;
  }

  private setColumnsName() {
    this.columns = [];
    this.columns.push({
      name: '',
      cellTemplate: this.rowRadio,
      flexGrow: 0.1,
    });
    this.columns.push({
      name: 'text.reservation',
      prop: 'reservationItemCode',
    });
    this.columns.push({
      name: 'label.company',
      prop: 'companyClientName',
    });
  }

  private getCompanyClient(): void {
    this.clientResource.getReservationCompanyClientAccount(this.propertyId).subscribe(response => {
      if (response.items) {
        this.allItemList = response.items;
        this.pageItemsList = [...response.items];
      }
    });
  }

  public setListFiltered(listFiltered: Array<CompanyClientAccount>) {
    this.pageItemsList = listFiltered;
  }

  public getCompanyClientAccountData(guest: CompanyClientAccount) {
    this.companyClientSelected = guest;
  }

  public saveClientAccount = () => {
    if (this.companyClientSelected) {
      const dataToSave = this.billingAccountMapper.toSaveClientAccountDto(this.companyClientSelected, this.propertyId);
      this.billingAccountResorce.saveBillingAccount(dataToSave).subscribe(response => {
        this.toasterEmitService.emitChange(SuccessError.success, 'commomData.saveBillingAccount');
        this.router.navigate(['../../edit'], {
          relativeTo: this.route,
          queryParams: { billingAccountId: response.id },
        });
      }, error => {
        this.openNewAccountModal();
      });
    }
  }

  public goBackPreviewPage = () => {
    this.location.back();
  }

  public goToNewReservationPage() {
    this.router.navigate(['p', this.propertyId, 'reservation', 'new']);
  }

  public openNewAccountModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_NEW_ACCOUNT);
  }

  public closeNewAccountModal() {
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_NEW_ACCOUNT);
  }

  public confirmNewAccountModal( accountName ) {
    this.billingAccountResource
      .createBillingAccountWithEntry(this.companyClientSelected.billingAccountId , accountName)
      .subscribe( result => {
        this.toasterEmitService.emitChange(SuccessError.success, 'commomData.saveBillingAccount');
        this.closeNewAccountModal();
      });
  }
}
