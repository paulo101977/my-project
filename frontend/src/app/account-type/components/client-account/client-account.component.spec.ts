import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { ClientAccountComponent } from './client-account.component';
import { CompanyClientAccount } from '../../../shared/models/dto/company-client-account-dto';
import {
  CompanyClientAccountData,
  CompanyClientAccountData1
} from '../../../shared/mock-data/company-client-account-data';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { InterceptorHandlerService,
  ThexApiTestingModule,
  ApiEnvironment,
  configureApi
} from '@inovacaocmnet/thx-bifrost';

describe('ClientAccountComponent', () => {
  let component: ClientAccountComponent;
  let fixture: ComponentFixture<ClientAccountComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed
      .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: [
          ReactiveFormsModule,
          HttpClientModule,
          RouterTestingModule,
          TranslateModule.forRoot(),
          ThexApiTestingModule
        ],
        declarations: [
          ClientAccountComponent
        ],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              snapshot: {
                params: { property: 1 }
              }
            }
          },
          InterceptorHandlerService,
          configureApi({
            environment: ApiEnvironment.Development
          })
        ],
        schemas: [
          CUSTOM_ELEMENTS_SCHEMA
        ]
      });

    fixture = TestBed.createComponent(ClientAccountComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should set columns', () => {
      expect(component.columns).toEqual([
        {
          name: '',
          cellTemplate: component.rowRadio,
          flexGrow: 0.1,
        },
        {
          name: 'text.reservation',
          prop: 'reservationItemCode'
        },
        {
          name: 'label.company',
          prop: 'companyClientName'
        }
      ]);
    });

    it('should set allItemList', () => {
      const itens = [CompanyClientAccountData, CompanyClientAccountData1];
      const result = {
        hasNext: false,
        items: itens
      };

      spyOn(component['clientResource'], 'getReservationCompanyClientAccount')
        .and.returnValue(of(result));

      component['getCompanyClient']();

      expect(component.allItemList).toEqual(itens);
      expect(component.pageItemsList).toEqual(itens);
    });

    it('should set buttonConfig', () => {
      const cancelBtn = new ButtonConfig();
      cancelBtn.textButton = 'commomData.cancel';
      cancelBtn.buttonType = ButtonType.Secondary;
      cancelBtn.callback = component.goBackPreviewPage;

      const saveBtn = new ButtonConfig();
      saveBtn.textButton = 'commomData.createAccount';
      saveBtn.buttonType = ButtonType.Primary;
      saveBtn.callback = component.saveClientAccount;

      expect(component.buttonCancelConfig).toEqual(cancelBtn);
      expect(component.buttonSaveConfig).toEqual(saveBtn);
    });
  });

  describe('on methods', () => {
    it('should setListFiltered', () => {
      const listFiltered = new Array<CompanyClientAccount>();
      listFiltered.push(CompanyClientAccountData);
      listFiltered.push(CompanyClientAccountData1);

      component.setListFiltered(listFiltered);

      expect(component.pageItemsList).toEqual(listFiltered);
    });

    it('should getCompanyClientAccountData', () => {
      component.getCompanyClientAccountData(CompanyClientAccountData);

      expect(component.companyClientSelected).toEqual(CompanyClientAccountData);
    });

    it('should goBackToPreviewPage', () => {
      spyOn(component['location'], 'back');

      component.goBackPreviewPage();

      expect(component['location']['back']).toHaveBeenCalled();
    });

    it('should goToNewReservationPage', () => {
      spyOn(component['router'], 'navigate');

      component.goToNewReservationPage();

      expect(component['router'].navigate).toHaveBeenCalledWith(['p', 1, 'reservation', 'new']);
    });
  });
});
