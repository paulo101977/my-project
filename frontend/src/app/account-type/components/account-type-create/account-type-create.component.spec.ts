import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { AccountTypeCreateComponent } from './account-type-create.component';

describe('AccountTypeCreateComponent', () => {
  let component: AccountTypeCreateComponent;
  let fixture: ComponentFixture<AccountTypeCreateComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), RouterTestingModule],
      providers: [],
      declarations: [AccountTypeCreateComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(AccountTypeCreateComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  describe('on initials settings', () => {
    it('should config HeaderPage', () => {
      expect(component.configHeaderPage.hasBackPreviewPage).toBeTruthy();
    });
  });
});
