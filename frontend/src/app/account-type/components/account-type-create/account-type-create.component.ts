import { Component } from '@angular/core';
import { RouterModule, RouterOutlet, ActivatedRoute } from '@angular/router';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';

@Component({
  selector: 'account-type-create',
  templateUrl: 'account-type-create.component.html',
})
export class AccountTypeCreateComponent {
  public configHeaderPage: ConfigHeaderPageNew;

  public constructor() {
    this.setConfigHeaderPage();
  }

  public setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }
}
