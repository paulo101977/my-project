import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, RouterOutlet } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { AccountTypeRoutingModule } from './account-type-routing.module';
import { ClientAccountComponent } from './/components/client-account/client-account.component';
import { GuestAccountComponent } from './/components/guest-account/guest-account.component';
import { SingleAccountComponent } from './/components/single-account/single-account.component';
import { AccountTypeCreateComponent } from './components/account-type-create/account-type-create.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, FormsModule, RouterModule, SharedModule, AccountTypeRoutingModule],
  declarations: [SingleAccountComponent, GuestAccountComponent, ClientAccountComponent, AccountTypeCreateComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AccountTypeModule {}
