import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MomentModule } from 'ngx-moment';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import {
  ReservationDownloadResourceService
} from 'app/shared/services/reservation-download-resource/reservation-download-resource.service';
import { createServiceStub } from '../../../../../testing/create-service-stub';

import { IntegrationLogPageComponent } from './integration-log-page.component';

describe('IntegrationLogPageComponent', () => {
  let component: IntegrationLogPageComponent;
  let fixture: ComponentFixture<IntegrationLogPageComponent>;

  const ReservationDownloadResourceProvider = createServiceStub(ReservationDownloadResourceService, {});

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ IntegrationLogPageComponent ],
      imports: [ TranslateTestingModule, MomentModule ],
      providers: [ ReservationDownloadResourceProvider ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationLogPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
