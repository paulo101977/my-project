import { Component, OnInit } from '@angular/core';
import {
  ReservationDownloadResourceService
} from 'app/shared/services/reservation-download-resource/reservation-download-resource.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { from, Observable } from 'rxjs';
import { filter, pluck, switchMap, take, tap } from 'rxjs/operators';

@Component({
  selector: 'app-integration-log-page',
  templateUrl: './integration-log-page.component.html',
  styleUrls: ['./integration-log-page.component.css']
})
export class IntegrationLogPageComponent implements OnInit {
  public titlePage = 'title.reservationIntegrationPanel';
  public readonly MODAL_ID: 'INTEGRATION-LOG-SUMMARY';
  public onCloseModal: Function;
  public selectedLog$: Observable<any>;

  constructor(
    private modalEmitService: ModalEmitToggleService,
    private reservationDownloadResource: ReservationDownloadResourceService
  ) { }

  ngOnInit() {
    // TODO implement
    this.onCloseModal = () => {};
  }

  public openModal(reservation: any) {
    this.selectedLog$ = this.reservationDownloadResource
      .getHistory(reservation.id)
      .pipe(
        pluck('items'),
        switchMap((items: any[]) => from(items)),
        filter(item => item.id == reservation.lastHistoryId),
        take(1),
        tap(() => { this.toggleModal(); })
      );
  }

  private toggleModal() {
    this.modalEmitService.emitToggleWithRef(this.MODAL_ID);
  }
}
