import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AutocompleteConfig } from 'app/shared/models/autocomplete/autocomplete-config';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { ChannelResource } from 'app/shared/resources/channel/channel.resource';
import { OriginResource } from 'app/shared/resources/origin/origin.resource';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { pluck } from 'rxjs/operators';
import {
  IntegrationLogTableService
} from 'app/integration-log/integration-log-table/services/integration-log-table.service';

@Component({
  selector: 'app-integration-log-form',
  templateUrl: './integration-log-form.component.html',
  styleUrls: ['./integration-log-form.component.css']
})
export class IntegrationLogFormComponent implements OnInit {
  public form: FormGroup;
  public originAutocompleteConfig: AutocompleteConfig;
  public filteredOrigins = [];
  public channelAutocompleteConfig: AutocompleteConfig;
  public filteredChannels = [];
  public confirmButtonConfig: ButtonConfig;

  private propertyId: number;
  private channels = [];
  private origins = [];
  private isFiltering: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private channelResource: ChannelResource,
    private originResource: OriginResource,
    private route: ActivatedRoute,
    private integrationLogTableService: IntegrationLogTableService
  ) { }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setForm();
    this.setButtonConfig();
    this.setChannelConfig();
    this.setOriginConfig();
  }

  public submit() {
    if (this.form.invalid) {
      return;
    }

    this.confirmButtonConfig = {
      ...this.confirmButtonConfig,
      textButton: 'action.update'
    };

    this.search(this.form.value);
    this.isFiltering = false;
  }

  public search(formValue) {
    this.integrationLogTableService.search({
      formValue,
      isFiltering: this.isFiltering
    });
  }

  public searchChannel({query}: {query: string}) {
    if (!query || query.length < 3) {
      this.form.patchValue({
        channelCodeId: null
      });
      return;
    }

    this.filteredChannels = this.channels
      .filter((channel) => {
        const regexp = new RegExp(query, 'ig');
        return channel.description.search(regexp) > -1;
      });
    this.channelAutocompleteConfig.resultList = this.filteredChannels;
  }

  public selectChannel({id, description}: any) {
    this.form.patchValue({
      channelCodeId: id,
      channelCodeName: description
    });
  }

  public searchOrigin({query}: {query: string}) {
    if (!query || query.length < 3) {
      return;
    }

    this.filteredOrigins = this.origins
      .filter((origin) => {
        const regexp = new RegExp(query, 'ig');
        return origin.description.search(regexp) > -1;
      });
    this.originAutocompleteConfig.resultList = this.filteredOrigins;
  }

  public selectOrigin({id, description}: any) {
    this.form.patchValue({
      reservationOrigin: description,
      reservationOriginId: id
    });
  }

  private setChannelConfig() {
    this.channelAutocompleteConfig = {
      ...(new AutocompleteConfig()),
      resultList: [],
      callbackToSearch: (channelName) => this.searchChannel(channelName),
      callbackToGetSelectedValue: (channel) => this.selectChannel(channel),
      dataModel: 'channelCodeId',
      fieldObjectToDisplay: 'description',
      extraClasses: 'thf-input thf-u-padding--1',
    };

    this.channelResource.getAllChannelList({})
      .pipe(pluck('items'))
      .subscribe((channels: any[]) => this.channels = channels);
  }

  private setOriginConfig() {
    this.originAutocompleteConfig = {
      ...(new AutocompleteConfig()),
      resultList: [],
      callbackToSearch: (channelName) => this.searchOrigin(channelName),
      callbackToGetSelectedValue: (channel) => this.selectOrigin(channel),
      dataModel: 'reservationOriginId',
      fieldObjectToDisplay: 'description',
      extraClasses: 'thf-input thf-u-padding--1',
    };

    this.originResource.getActiveOrigins(this.propertyId)
      .pipe(pluck('items'))
      .subscribe((origins: any[]) => this.origins = origins);
  }

  private setButtonConfig() {
    this.confirmButtonConfig = this.sharedService
      .getButtonConfig('modal-message-confirm',
        () => this.submit(),
        'action.filter',
        ButtonType.Primary
      );
  }

  private setForm() {
    this.isFiltering = true;
    this.form = this.formBuilder.group({
      processingDate: [null],
      reservationNumber: [null],
      reservationOrigin: [null],
      reservationOriginId: [null],
      channelCodeId: [null],
      channelCodeName: [null]
    });

    this.form.valueChanges.subscribe(() => {
      this.isFiltering = true;
      this.confirmButtonConfig = {
        ...this.confirmButtonConfig,
        textButton: 'action.filter',
        isDisabled: this.form.invalid
      };
    });


    this.form.get('reservationOrigin').valueChanges
      .subscribe((value) => {
        if (!value || value.length < 3) {
          this.form.patchValue({
            reservationOriginId: null
          });
        }
      });

    this.form.get('channelCodeName').valueChanges
      .subscribe((value) => {
        if (!value || value.length < 3) {
          this.form.patchValue({
            channelCodeId: null
          });
        }
      });
  }
}
