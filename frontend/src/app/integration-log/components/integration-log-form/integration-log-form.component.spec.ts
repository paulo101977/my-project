import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { Channel } from 'app/shared/models/integration/channel';
import { Origin } from 'app/shared/models/origin';
import { ChannelResource } from 'app/shared/resources/channel/channel.resource';
import { OriginResource } from 'app/shared/resources/origin/origin.resource';
import { Observable, of } from 'rxjs';
import { ActivatedRouteStub } from '../../../../../testing/activated-route-stub';
import { createServiceStub } from '../../../../../testing/create-service-stub';

import { IntegrationLogFormComponent } from './integration-log-form.component';

describe('IntegrationLogFormComponent', () => {
  let component: IntegrationLogFormComponent;
  let fixture: ComponentFixture<IntegrationLogFormComponent>;

  const channels: Channel[] = [
    {
      businessSourceId: 0,
      businessSourceName: '',
      channelCodeId: 0,
      channelCodeName: '',
      code: '',
      costOfDistribution: 0,
      description: 'foooo',
      distributionAmount: 0,
      id: '',
      isActive: false,
      marketSegmentId: 0,
      origin: ''
    }
  ];
  const origins: Origin[] = [
    {
      code: '',
      description: 'foooo',
      id: 0,
      isActive: false,
      propertyId: 0
    }
  ];

  const channelProvider = createServiceStub(ChannelResource, {
    getAllChannelList({all, searchData}: { all?: boolean; searchData?: string }): Observable<ItemsResponse<Channel>> {
      return of({hasNext: false, items: channels});
    }
  });
  const originProvider = createServiceStub(OriginResource, {
    getActiveOrigins(propertyId: number): Observable<ItemsResponse<Origin>> {
      return of({hasNext: false, items: origins});
    }
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ IntegrationLogFormComponent ],
      imports: [ TranslateTestingModule ],
      providers: [
        FormBuilder,
        channelProvider,
        originProvider,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationLogFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#searchChannel', () => {
    it('shouldn\'t search when query is empty', () => {
      const filtered = [1, 2, 3];
      component.filteredChannels = filtered;
      component.channelAutocompleteConfig.resultList = filtered;

      component.searchChannel({query: ''});

      expect(component.filteredChannels).toEqual(filtered);
      expect(component.channelAutocompleteConfig.resultList).toEqual(filtered);
    });

    it('shouldn\'t search when query is less then 3 characters', () => {
      const filtered = [1, 2, 3];
      component.filteredChannels = filtered;
      component.channelAutocompleteConfig.resultList = filtered;

      component.searchChannel({query: '12'});

      expect(component.filteredChannels).toEqual(filtered);
      expect(component.channelAutocompleteConfig.resultList).toEqual(filtered);
    });

    it('should be case insensitive', () => {
      const filtered = [1, 2, 3];

      component.searchChannel({query: 'FOO'});

      expect(component.filteredChannels).toEqual([channels[0]]);
      expect(component.channelAutocompleteConfig.resultList).toEqual([channels[0]]);

      component.filteredChannels = filtered;
      component.channelAutocompleteConfig.resultList = filtered;

      component.searchChannel({query: 'OoO'});

      expect(component.filteredChannels).toEqual([channels[0]]);
      expect(component.channelAutocompleteConfig.resultList).toEqual([channels[0]]);
    });
  });

  describe('#selectChannel', () => {
    it('should update the channel form fields', () => {
      const channel = {
        id: 'newId',
        description: 'newDescription'
      };

      component.form.patchValue({
        channelCodeId: 'id',
        channelCodeName: 'description'
      });

      component.selectChannel(channel);

      expect(component.form.value).toEqual(jasmine.objectContaining({
        channelCodeId: channel.id,
        channelCodeName: channel.description
      }));
    });
  });

  describe('#searchOrigin', () => {
    it('shouldn\'t search when query is empty', () => {
      const filtered = [1, 2, 3];
      component.filteredOrigins = filtered;
      component.originAutocompleteConfig.resultList = filtered;

      component.searchOrigin({query: ''});

      expect(component.filteredOrigins).toEqual(filtered);
      expect(component.originAutocompleteConfig.resultList).toEqual(filtered);
    });

    it('shouldn\'t search when query is less then 3 characters', () => {
      const filtered = [1, 2, 3];
      component.filteredOrigins = filtered;
      component.originAutocompleteConfig.resultList = filtered;

      component.searchOrigin({query: '12'});

      expect(component.filteredOrigins).toEqual(filtered);
      expect(component.originAutocompleteConfig.resultList).toEqual(filtered);
    });

    it('should be case insensitive', () => {
      const filtered = [1, 2, 3];

      component.searchOrigin({query: 'FOO'});

      expect(component.filteredOrigins).toEqual([origins[0]]);
      expect(component.originAutocompleteConfig.resultList).toEqual([origins[0]]);

      component.filteredOrigins = filtered;
      component.originAutocompleteConfig.resultList = filtered;

      component.searchOrigin({query: 'OoO'});

      expect(component.filteredOrigins).toEqual([origins[0]]);
      expect(component.originAutocompleteConfig.resultList).toEqual([origins[0]]);
    });
  });

  describe('#selectOrigin', () => {
    it('should update the origin form fields', () => {
      const origin = {
        id: 'newId',
        description: 'newDescription'
      };

      component.form.patchValue({
        reservationOriginId: 'id',
        reservationOrigin: 'description'
      });

      component.selectOrigin(origin);

      expect(component.form.value).toEqual(jasmine.objectContaining({
        reservationOriginId: origin.id,
        reservationOrigin: origin.description
      }));
    });
  });
});
