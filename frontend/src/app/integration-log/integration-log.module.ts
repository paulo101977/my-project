import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'app/shared/shared.module';
import { IntegrationLogFormComponent } from './components/integration-log-form/integration-log-form.component';
import { IntegrationLogPageComponent } from './components/integration-log-page/integration-log-page.component';

import { IntegrationLogRoutingModule } from './integration-log-routing.module';
import { IntegrationLogSummaryModule } from './integration-log-summary/integration-log-summary.module';
import { IntegrationLogTableModule } from './integration-log-table/integration-log-table.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IntegrationLogRoutingModule,
    IntegrationLogTableModule,
    IntegrationLogSummaryModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [
    IntegrationLogPageComponent,
    IntegrationLogFormComponent
  ]
})
export class IntegrationLogModule { }
