import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IntegrationLogSummaryComponent
} from './components/integration-log-summary/integration-log-summary.component';
import {
  IntegrationLogSummaryHeaderComponent
} from './components/integration-log-summary-header/integration-log-summary-header.component';
import {
  IntegrationLogSummaryMessagesComponent
} from './components/integration-log-summary-messages/integration-log-summary-messages.component';
import {
  IntegrationLogSummaryJsonComponent
} from './components/integration-log-summary-json/integration-log-summary-json.component';
import { IntegrationLogTableModule } from 'app/integration-log/integration-log-table/integration-log-table.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'app/shared/shared.module';
import { ClipboardModule } from 'ngx-clipboard';

@NgModule({
  imports: [
    CommonModule,
    IntegrationLogTableModule,
    TranslateModule,
    SharedModule,
    ClipboardModule
  ],
  exports: [
    IntegrationLogSummaryComponent
  ],
  declarations: [
    IntegrationLogSummaryComponent,
    IntegrationLogSummaryHeaderComponent,
    IntegrationLogSummaryMessagesComponent,
    IntegrationLogSummaryJsonComponent
  ]
})
export class IntegrationLogSummaryModule { }
