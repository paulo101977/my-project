import { AfterViewInit, Component, Input, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-integration-log-summary-header',
  templateUrl: './integration-log-summary-header.component.html',
  styleUrls: ['./integration-log-summary-header.component.css']
})
export class IntegrationLogSummaryHeaderComponent implements AfterViewInit {
  @Input() set log(log: any) {
    if (log == null) {
      return;
    }

    this._log = {
      ...log,
      ...log.reservationDownloadDetailsDto
    };
  }

  get log() {
    return this._log;
  }

  _log: any;
  @ViewChild('statusCellTemplate') statusCellTemplate: TemplateRef<any>;

  public tableColumns: any[];

  ngAfterViewInit(): void {
    this.tableColumns = [
      { name: 'label.status', prop: 'status', flexGrow: 0.3, cellTemplate: this.statusCellTemplate },
      { name: 'label.operation', prop: 'operationName', flexGrow: 0.4 },
      { name: 'label.reservationSource', prop: 'businessSourceName', flexGrow: 0.5 },
      { name: 'label.sourceReservationNumber', prop: 'partnerReservationNumber', flexGrow: 0.5 },
      { name: 'label.pmsReservationNumber', prop: 'reservationCode', flexGrow: 0.5 },
      { name: 'label.partner', prop: 'partnerName', flexGrow: 0.5 }
    ];
  }
}
