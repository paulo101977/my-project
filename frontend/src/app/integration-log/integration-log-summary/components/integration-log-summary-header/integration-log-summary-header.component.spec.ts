import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationLogSummaryHeaderComponent } from './integration-log-summary-header.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('IntegrationLogSummaryHeaderComponent', () => {
  let component: IntegrationLogSummaryHeaderComponent;
  let fixture: ComponentFixture<IntegrationLogSummaryHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ IntegrationLogSummaryHeaderComponent ],
      imports: [ TranslateTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationLogSummaryHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
