import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-integration-log-summary-messages',
  templateUrl: './integration-log-summary-messages.component.html',
  styleUrls: ['./integration-log-summary-messages.component.css']
})
export class IntegrationLogSummaryMessagesComponent implements OnInit {
  @ViewChild('statusCellTemplate') statusCellTemplate: TemplateRef<any>;

  @Input() public set alerts(alerts) {
    this._alerts = this.formatDates(alerts);
  }
  public get alerts() {
    return this._alerts;
  }

  private _alerts: any;
  public columns: any[];

  ngOnInit() {
    this.setTable();
  }


  private setTable() {
    this.columns = [
      { name: 'label.status', prop: 'status', flexGrow: 0.3, cellTemplate: this.statusCellTemplate },
      { name: 'label.firstAttempt', prop: 'firstAttemptDate', flexGrow: 1 },
      { name: 'label.lastAttempt', prop: 'lastAttemptDate', flexGrow: 1},
      { name: 'label.resolutionDate', prop: 'resolutionDate', flexGrow: 1 },
      { name: 'label.message', prop: 'message', flexGrow: 1 },
      { name: 'label.origin', prop: 'key', flexGrow: 0.3 }
    ];
  }

  private formatDates(alerts: any[]) {
    if (!alerts) {
      return;
    }

    return alerts.map((alert) => {
      return {
        ...alert,
        firstAttemptDate: this.formatDate(alert.firstAttemptDate),
        lastAttemptDate: this.formatDate(alert.lastAttemptDate),
        resolutionDate: this.formatDate(alert.resolutionDate),
        errorsNumber: alert.errorsNumber ? alert.errorsNumber : 0
      };
    });
  }

  private formatDate(date: string) {
    return date ? moment(date).format('L - LT') : '';
  }
}
