import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationLogSummaryMessagesComponent } from './integration-log-summary-messages.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('IntegrationLogSummaryMessagesComponent', () => {
  let component: IntegrationLogSummaryMessagesComponent;
  let fixture: ComponentFixture<IntegrationLogSummaryMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ IntegrationLogSummaryMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationLogSummaryMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
