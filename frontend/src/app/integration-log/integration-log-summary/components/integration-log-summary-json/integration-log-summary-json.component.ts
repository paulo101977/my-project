import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-integration-log-summary-json',
  templateUrl: './integration-log-summary-json.component.html',
  styleUrls: ['./integration-log-summary-json.component.css']
})
export class IntegrationLogSummaryJsonComponent {
  @Input() response: string;

  private timeout: any;
  private copy = `${this.translate.instant('action.copy')} ${this.translate.instant('label.response')}`;
  private copied = this.translate.instant('commomData.copied');
  public buttonText = this.copy;

  constructor(private translate: TranslateService) {}

  onCopy() {
    this.buttonText = this.copied;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.buttonText = this.copy;
    }, 3000);
  }
}
