import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationLogSummaryJsonComponent } from './integration-log-summary-json.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('IntegrationLogSummaryJsonComponent', () => {
  let component: IntegrationLogSummaryJsonComponent;
  let fixture: ComponentFixture<IntegrationLogSummaryJsonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ TranslateTestingModule ],
      declarations: [ IntegrationLogSummaryJsonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationLogSummaryJsonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
