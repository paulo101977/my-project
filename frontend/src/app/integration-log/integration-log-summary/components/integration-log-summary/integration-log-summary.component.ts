import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-integration-log-summary',
  templateUrl: './integration-log-summary.component.html',
  styleUrls: ['./integration-log-summary.component.css']
})
export class IntegrationLogSummaryComponent implements OnInit {
  @Input() log: any;
  constructor() { }

  ngOnInit() {
  }

}
