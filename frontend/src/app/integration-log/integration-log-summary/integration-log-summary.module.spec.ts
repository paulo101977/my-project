import { IntegrationLogSummaryModule } from './integration-log-summary.module';

describe('IntegrationLogSummaryModule', () => {
  let integrationLogSummaryModule: IntegrationLogSummaryModule;

  beforeEach(() => {
    integrationLogSummaryModule = new IntegrationLogSummaryModule();
  });

  it('should create an instance', () => {
    expect(integrationLogSummaryModule).toBeTruthy();
  });
});
