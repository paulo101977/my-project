import { IntegrationLogTableModule } from './integration-log-table.module';

describe('IntegrationLogTableModule', () => {
  let integrationLogTableModule: IntegrationLogTableModule;

  beforeEach(() => {
    integrationLogTableModule = new IntegrationLogTableModule();
  });

  it('should create an instance', () => {
    expect(integrationLogTableModule).toBeTruthy();
  });
});
