import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationLogTableComponent } from './integration-log-table.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {DateService} from 'app/shared/services/shared/date.service';
import {createServiceStub, createPipeStub} from '../../../../../../testing';
import {ReservationDownloadResourceService} from 'app/shared/services/reservation-download-resource/reservation-download-resource.service';
import {ItemsResponse} from 'app/shared/models/backend-api/item-response';
import {Observable, of} from 'rxjs';

describe('IntegrationLogTableComponent', () => {
  let component: IntegrationLogTableComponent;
  let fixture: ComponentFixture<IntegrationLogTableComponent>;

  const dateProvider = createServiceStub(DateService, {
    clearTime (date: string): string {
      return null;
    }
  });

  const reservationDownloadProvider = createServiceStub( ReservationDownloadResourceService, {
    search (params): Observable<ItemsResponse<any>> {
      return of(null);
    }
  });

  const amDateFormatPipe = createPipeStub({
    name: 'amDateFormat'
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [
        IntegrationLogTableComponent,
        amDateFormatPipe
      ],
      providers: [
        dateProvider,
        reservationDownloadProvider
      ]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationLogTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
