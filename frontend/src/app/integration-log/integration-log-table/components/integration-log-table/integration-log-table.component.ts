import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { TableColumn } from '@swimlane/ngx-datatable/src/types/table-column.type';
import {
  IntegrationLogTableService
} from 'app/integration-log/integration-log-table/services/integration-log-table.service';
import {
  ReservationDownloadResourceService
} from 'app/shared/services/reservation-download-resource/reservation-download-resource.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-integration-log-table',
  templateUrl: './integration-log-table.component.html',
  styleUrls: ['./integration-log-table.component.css']
})
export class IntegrationLogTableComponent implements OnInit {
  @Output() clickedRow = new EventEmitter();

  @ViewChild('statusCellTemplate') statusCellTemplate: TemplateRef<any>;

  public searchedResult$: Observable<any>;
  public pageSize = 10;
  public page = 1;

  private searchedValues: any;

  public columns: TableColumn[];

  constructor(
    private reservationDownloadResource: ReservationDownloadResourceService,
    private integrationLogTableService: IntegrationLogTableService,
    private dateService: DateService
  ) { }

  ngOnInit() {
    this.setTable();
    this.integrationLogTableService.searched$.subscribe((data: any) => {
      this.onSearch(data);
    });
  }

  public setPage(pageInfo) {
    this.page = pageInfo.offset + 1;
    this.search();
  }

  public onSearch({formValue, isFiltering}) {
    if (isFiltering) {
      this.page = 1;
      this.searchedValues = {
        date: this.dateService.clearTime(formValue.processingDate),
        reservationNumber: formValue.reservationNumber,
        businessSourceId: formValue.reservationOriginId,
        partnerId: formValue.channelCodeId,
      };
    }

    this.search();
  }

  public search() {
    const params = {
      ...this.searchedValues,
      page: this.page,
      pageSize: this.pageSize,
    };

    this.searchedResult$ = this.reservationDownloadResource.search(params);
  }

  private setTable() {
    this.columns = [
      { name: 'label.status', prop: 'status', flexGrow: 0.3, cellTemplate: this.statusCellTemplate },
      { name: 'label.operation', prop: 'operationName', flexGrow: 0.4 },
      { name: 'label.reservationSource', prop: 'businessSourceName', flexGrow: 0.5 },
      { name: 'label.sourceReservationNumber', prop: 'partnerReservationNumber', flexGrow: 0.5 },
      { name: 'label.pmsReservationNumber', prop: 'reservationCode', flexGrow: 0.5 },
      { name: 'label.partner', prop: 'partnerName', flexGrow: 0.5 },
      { name: 'label.message', prop: 'message', flexGrow: 1 }
    ];
  }
}
