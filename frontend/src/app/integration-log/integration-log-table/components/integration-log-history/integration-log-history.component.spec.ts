import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationLogHistoryComponent } from './integration-log-history.component';

describe('IntegrationLogHistoryComponent', () => {
  let component: IntegrationLogHistoryComponent;
  let fixture: ComponentFixture<IntegrationLogHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntegrationLogHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationLogHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
