import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntegrationLogTableComponent } from './components/integration-log-table/integration-log-table.component';
import { IntegrationLogHistoryComponent } from './components/integration-log-history/integration-log-history.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    IntegrationLogTableComponent
  ],
  declarations: [
    IntegrationLogTableComponent,
    IntegrationLogHistoryComponent
  ]
})
export class IntegrationLogTableModule { }
