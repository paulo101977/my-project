import { TestBed, inject } from '@angular/core/testing';

import { IntegrationLogTableService } from './integration-log-table.service';

describe('IntegrationLogTableService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IntegrationLogTableService]
    });
  });

  it('should be created', inject([IntegrationLogTableService], (service: IntegrationLogTableService) => {
    expect(service).toBeTruthy();
  }));
});
