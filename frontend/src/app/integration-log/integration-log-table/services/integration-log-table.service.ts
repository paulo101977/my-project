import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IntegrationLogTableService {
  private searched = new Subject();
  public searched$ = this.searched.asObservable();

  public search({ formValue, isFiltering }) {
    this.searched.next({ formValue, isFiltering });
  }
}
