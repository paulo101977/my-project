import { IntegrationLogModule } from './integration-log.module';

describe('IntegrationLogModule', () => {
  let integrationLogModule: IntegrationLogModule;

  beforeEach(() => {
    integrationLogModule = new IntegrationLogModule();
  });

  it('should create an instance', () => {
    expect(integrationLogModule).toBeTruthy();
  });
});
