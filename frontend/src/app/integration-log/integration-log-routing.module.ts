import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntegrationLogPageComponent } from './components/integration-log-page/integration-log-page.component';

const routes: Routes = [
  {
    path: '',
    component: IntegrationLogPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntegrationLogRoutingModule { }
