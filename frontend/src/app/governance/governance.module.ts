import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GovernanceStatusListComponent } from './components/governance-status-list/governance-status-list.component';
import { GovernanceStatusFormComponent } from './components/governance-status-form/governance-status-form.component';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { GovernanceRoomListComponent } from './components/governance-room-list/governance-room-list.component';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { GovernanceRoomBlockComponent } from './components/governance-room-block/governance-room-block.component';
import { GovernanceRoutingModule } from './governance-routing.module';
import { GovernanceAssocComponent } from './components/governance-assoc/governance-assoc.component';
import { GovernanceAssocModalComponent } from './components/governance-assoc-modal/governance-assoc-modal.component';
import { GovernanceScheduleModalComponent } from './components/governance-schedule-modal/governance-schedule-modal.component';
import { GovernanceResumeModalComponent } from './components/governance-resume-modal/governance-resume-modal.component';
import { GovernanceAutoAssocModalComponent } from './components/governance-auto-assoc-modal/governance-auto-assoc-modal.component';
import { ThxImgModule, ThxImgInfoModule } from '@inovacao-cmnet/thx-ui';
import { GovernanceInfoModalComponent } from 'app/governance/components/governance-info-modal/governance-info-modal.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MyDateRangePickerModule,
    GovernanceRoutingModule,
    ThxImgModule,
    ThxImgInfoModule
  ],
  declarations: [
    GovernanceStatusListComponent,
    GovernanceStatusFormComponent,
    GovernanceRoomListComponent,
    GovernanceRoomBlockComponent,
    GovernanceAssocComponent,
    GovernanceAssocModalComponent,
    GovernanceScheduleModalComponent,
    GovernanceResumeModalComponent,
    GovernanceAutoAssocModalComponent,
    GovernanceInfoModalComponent
  ],
  exports: [GovernanceRoomBlockComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class GovernanceModule {}
