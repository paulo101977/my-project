export class MockData {

  public roomAssocList = [
    {
      checkInDate: '2018-08-02T00:00:00',
      checkOutDate: '0001-01-01T00:00:00',
      creationTime: '0001-01-01T00:00:00',
      elapsedTime: 2400,
      fullName: 'Thex Thex',
      housekeepingRoomId: '00000000-0000-0000-0000-000000000000',
      housekeepingStatusColor: '1DC29D',
      housekeepingStatusName: 'Limpo',
      id: 1,
      isDeleted: false,
      isStarted: false,
      ownerId: '00000000-0000-0000-0000-000000000000',
      personId: 'abee3fae-ff86-4eb3-a333-3e757dd01f53',
      photoUrl: 'https://thexdev.blob.core.windows.net/abee3fae-ff86-4eb3-a333-3e757dd01f53/abee3fae-ff86-4eb3-a333-3e757dd01f53',
      propertyId: 0,
      quantityDouble: 0,
      quantitySingle: 0,
      reservationItemStatusId: 2,
      roomId: 1,
      roomList: [],
      roomNumber: 101,
      roomStatusName: 'Ocupado',
      roomTypeAbbreviation: 'STD',
      roomTypeId: 1,
      serviceDate: '2019-01-07T12:03:26.0175455+00:00',
      serviceDateEnd: '2019-01-07T12:03:26.0175639+00:00',
      serviceDateStart: '2019-01-07T12:03:26.0175615+00:00',
      total: 0,
      userId: '00000000-0000-0000-0000-000000000000',
    },
    {
      checkInDate: '2018-08-02T00:00:00',
      checkOutDate: '0001-01-01T00:00:00',
      creationTime: '0001-01-01T00:00:00',
      elapsedTime: 2400,
      fullName: 'Thex Other',
      housekeepingRoomId: '00000000-0000-0000-0000-000000000000',
      housekeepingStatusColor: '1DC29D',
      housekeepingStatusName: 'Sujo',
      id: 2,
      isDeleted: false,
      isStarted: false,
      ownerId: '00000000-0000-0000-0000-000000000000',
      personId: 'abee3fae-ff86-4eb3-a333-3e757dd01f53',
      photoUrl: 'https://thexdev.blob.core.windows.net/abee3fae-ff86-4eb3-a333-3e757dd01f53/abee3fae-ff86-4eb3-a333-3e757dd01f53',
      propertyId: 0,
      quantityDouble: 0,
      quantitySingle: 0,
      reservationItemStatusId: 2,
      roomId: 1,
      roomList: [],
      roomNumber: 102,
      roomStatusName: 'Ocupado',
      roomTypeAbbreviation: 'STD',
      roomTypeId: 1,
      serviceDate: '2019-01-07T12:03:26.0175455+00:00',
      serviceDateEnd: '2019-01-07T12:03:26.0175639+00:00',
      serviceDateStart: '2019-01-07T12:03:26.0175615+00:00',
      total: 0,
      userId: '00000000-0000-0000-0000-000000000000',
    }
  ];

  public employeeList = [
    {
      checkInDate: '0001-01-01T00:00:00',
      checkOutDate: '0001-01-01T00:00:00',
      creationTime: '0001-01-01T00:00:00',
      elapsedTime: 2400,
      housekeepingRoomId: '00000000-0000-0000-0000-000000000000',
      id: 0,
      isDeleted: false,
      isStarted: false,
      name: 'test 1',
      ownerId: '00000000-0000-0000-0000-000000000000',
      personId: '00000000-0000-0000-0000-000000000000',
      propertyId: 0,
      quantityDouble: 0,
      quantitySingle: 0,
      reservationItemStatusId: 0,
      roomId: 0,
      roomList: [],
      roomNumber: 0,
      roomTypeId: 0,
      serviceDate: '2019-01-07T12:40:41.9655669+00:00',
      serviceDateEnd: '2019-01-07T12:40:41.9655726+00:00',
      serviceDateStart: '2019-01-07T12:40:41.9655703+00:00',
      total: 0,
      userId: 'b6f2cb85-a110-47c5-c8eb-08d62fad530e',
    },
    {
      checkInDate: '0001-01-01T00:00:00',
      checkOutDate: '0001-01-01T00:00:00',
      creationTime: '0001-01-01T00:00:00',
      elapsedTime: 2400,
      housekeepingRoomId: '00000000-0000-0000-0000-000000000000',
      id: 0,
      isDeleted: false,
      isStarted: false,
      name: 'test 123',
      ownerId: '00000000-0000-0000-0000-000000000000',
      personId: '00000000-0000-0000-0000-000000000000',
      propertyId: 0,
      quantityDouble: 0,
      quantitySingle: 0,
      reservationItemStatusId: 0,
      roomId: 0,
      roomList: [],
      roomNumber: 0,
      roomTypeId: 0,
      serviceDate: '2019-01-07T12:40:41.9655669+00:00',
      serviceDateEnd: '2019-01-07T12:40:41.9655726+00:00',
      serviceDateStart: '2019-01-07T12:40:41.9655703+00:00',
      total: 0,
      userId: 'b6f2cb85-a110-47c5-c8eb-08d62fad530e4',
    }
  ];

  public modalEmployeeList = [
    {
      checkInDate: '0001-01-01T00:00:00',
      checkOutDate: '0001-01-01T00:00:00',
      creationTime: '0001-01-01T00:00:00',
      elapsedTime: 2400,
      endDate: '2019-01-03T17:39:58',
      housekeepingRoomId: '11bab4e6-289b-4f37-a316-1a67141c915a',
      housekeepingStatusColor: '1DC29D',
      housekeepingStatusName: 'Limpo',
      id: 0,
      isDeleted: false,
      isStarted: false,
      name: 'Totvs Hotel',
      ownerId: 'ecf249c5-6d42-4abd-91e1-52a9f9e3f89d',
      personId: '00000000-0000-0000-0000-000000000000',
      propertyId: 1,
      quantityDouble: 0,
      quantitySingle: 0,
      reservationItemStatusId: 2,
      roomId: 242,
      roomList: [],
      roomNumber: 506,
      roomStatusName: 'Ocupado',
      roomTypeAbbreviation: 'LXO',
      roomTypeId: 0,
      serviceDate: '2019-01-07T15:16:30.3559732+00:00',
      serviceDateEnd: '2019-01-07T15:16:30.3559803+00:00',
      serviceDateStart: '2019-01-07T15:16:30.355978+00:00',
      startDate: '2019-01-03T17:39:28',
      total: 0,
      userId: '00000000-0000-0000-0000-000000000000'
    },
    {
      checkInDate: '0001-01-01T00:00:00',
      checkOutDate: '0001-01-01T00:00:00',
      creationTime: '0001-01-01T00:00:00',
      elapsedTime: 2400,
      endDate: '2019-01-03T17:39:58',
      housekeepingRoomId: '11bab4e6-289b-4f37-a316-1a67141c915a',
      housekeepingStatusColor: '1DC29D',
      housekeepingStatusName: 'Sujo',
      id: 1,
      isDeleted: false,
      isStarted: false,
      name: 'Totvs Hotel 2',
      ownerId: 'ecf249c5-6d42-4abd-91e1-52a9f9e3f89d',
      personId: '00000000-0000-0000-0000-000000000000',
      propertyId: 1,
      quantityDouble: 0,
      quantitySingle: 0,
      reservationItemStatusId: 2,
      roomId: 242,
      roomList: [],
      roomNumber: 506,
      roomStatusName: 'Ocupado',
      roomTypeAbbreviation: 'LXO',
      roomTypeId: 0,
      serviceDate: '2019-01-07T15:16:30.3559732+00:00',
      serviceDateEnd: '2019-01-07T15:16:30.3559803+00:00',
      serviceDateStart: '2019-01-07T15:16:30.355978+00:00',
      startDate: '2019-01-03T17:39:28',
      total: 0,
      userId: '00000000-0000-0000-0000-000000000000'
    }
  ];

  public event = {
    preventDefault: () => {},
    stopPropagation: () => {}
  };

  public houseKeepingList = [
    {
      userId: '1a',
      name: 'João'
    },
    {
      userId: '2b',
      name: 'Marta Joaquina'
    },
    {
      userId: '3c',
      name: 'Beatriz'
    }
  ];

  public housekeepingGenerator = [
    {
      countRoom: 2,
      name: 'João',
      userId: '1a',
      listRoom: [
        {
          checkin: '2019-07-25T14:00:00',
          floor: 1,
          roomAbbreviation: 'STD',
          roomId: 3,
          roomNumber: '103',
          roomStatus: 'Vago',
          roomType: 'STD - 103'
        }
      ]
    },
    {
      countRoom: 2,
      name: 'Marta Joaquina',
      userId: '2b',
      listRoom: []
    }
  ];
}
