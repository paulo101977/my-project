import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GovernanceRoomListComponent } from './components/governance-room-list/governance-room-list.component';
import { GovernanceStatusListComponent } from './components/governance-status-list/governance-status-list.component';
import { GovernanceAssocComponent } from './components/governance-assoc/governance-assoc.component';

export const governanceRouting: Routes = [
  { path: 'status', component: GovernanceStatusListComponent },
  { path: 'room', component: GovernanceRoomListComponent },
  {path: 'room/association', component: GovernanceAssocComponent}
];

@NgModule({
  imports: [RouterModule.forChild(governanceRouting)],
  exports: [RouterModule],
})
export class GovernanceRoutingModule {}
