import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';

@Component({
  selector: 'app-governance-info-modal',
  templateUrl: './governance-info-modal.component.html',
  styleUrls: ['./governance-info-modal.component.scss']
})
export class GovernanceInfoModalComponent implements OnInit {

  @Input() modalId: string;
  @Output() closed = new EventEmitter();
  public confirmButtonConfig: ButtonConfig;

  constructor(
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.setButton();
  }

  public setButton() {
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'understand',
      () => this.closedModal(),
      'action.understand',
      ButtonType.Primary,
      ButtonSize.Normal
    );
    this.confirmButtonConfig.extraClass = 'button-bold-size';
  }

  public closedModal() {
    this.closed.emit();
  }

}
