import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GovernanceInfoModalComponent } from './governance-info-modal.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ImgCdnModule } from '@inovacao-cmnet/thx-ui';

describe('GovernanceInfoModalComponent', () => {
  let component: GovernanceInfoModalComponent;
  let fixture: ComponentFixture<GovernanceInfoModalComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ GovernanceInfoModalComponent ],
      imports: [
        TranslateTestingModule,
        ImgCdnModule
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    });
    fixture = TestBed.createComponent(GovernanceInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
