import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ColorPickerModule } from 'ngx-color-picker';
import { GovernanceStatusFormComponent } from './governance-status-form.component';
import { ModalWrapperComponent } from '../../../shared/components/modal-wrapper/modal-wrapper.component';
import { ColorPickerComponent } from '../../../shared/components/color-picker/color-picker.component';
import { Governance } from '../../../shared/models/governance/governance';

describe('GovernanceStatusFormComponent', () => {
  let component: GovernanceStatusFormComponent;
  let fixture: ComponentFixture<GovernanceStatusFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GovernanceStatusFormComponent, ModalWrapperComponent, ColorPickerComponent],
      imports: [FormsModule, ReactiveFormsModule, TranslateModule.forRoot(), ColorPickerModule],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernanceStatusFormComponent);
    component = fixture.componentInstance;
    const governanceStatus = new Governance();
    governanceStatus.propertyId = 1;
    governanceStatus.statusName = 'teste';
    governanceStatus.color = 'FFFFFF';
    component.governanceStatus = governanceStatus;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
