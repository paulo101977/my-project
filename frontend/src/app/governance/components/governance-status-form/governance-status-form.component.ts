import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Governance } from '../../../shared/models/governance/governance';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-governance-status-form',
  templateUrl: './governance-status-form.component.html',
  styleUrls: ['./governance-status-form.component.css'],
})
export class GovernanceStatusFormComponent implements OnInit, OnChanges {
  private _governanceStatus: Governance;
  @Input()
  set governanceStatus(governance) {
    this._governanceStatus = governance;
    this.setFormGroupControls();
  }
  get governanceStatus(): Governance {
    return this._governanceStatus;
  }

  @Output() cancel: EventEmitter<Governance> = new EventEmitter<Governance>();
  @Output() save: EventEmitter<Governance> = new EventEmitter<Governance>();

  public modalForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.setFormGroup();
  }

  ngOnChanges() {
    this.setFormGroupControls();
  }

  public setFormGroup() {
    const isActive = this._governanceStatus.id ? this._governanceStatus.isActive : true;
    this.modalForm = this.formBuilder.group({
      id: this.formBuilder.control(this._governanceStatus.id),
      name: this.formBuilder.control(this._governanceStatus.statusName, [Validators.required]),
      color: this.formBuilder.control(this._governanceStatus.color, [Validators.required]),
      isActive: this.formBuilder.control(isActive),
    });
  }

  public setFormGroupControls() {
    const isActive = this._governanceStatus.id ? this._governanceStatus.isActive : true;
    if (this.modalForm) {
      this.modalForm.controls.id.setValue(this._governanceStatus.id);
      this.modalForm.controls.name.setValue(this._governanceStatus.statusName);
      this.modalForm.controls.isActive.setValue(isActive);
      this.modalForm.controls.color.setValue(this._governanceStatus.color);
    }
  }
  public cancelCallback() {
    this.modalForm.reset();
    this.cancel.emit();
  }
  public saveCallback() {
    const governance = new Governance();
    if (this.modalForm.controls.id) {
      governance.id = this.modalForm.controls.id.value;
    }
    governance.propertyId = this._governanceStatus.propertyId;
    governance.housekeepingStatusId = this._governanceStatus.housekeepingStatusId;
    governance.statusName = this.modalForm.controls.name.value;
    governance.isActive = this.modalForm.controls.isActive.value ? true : false;
    governance.color = this.modalForm.controls.color.value.replace('#', '');
    this.modalForm.reset();
    this.save.emit(governance);
  }
}
