import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GovernanceRoomBlockComponent } from './governance-room-block.component';
import { MomentCalendarPipe } from '../../../shared/pipes/moment-calendar.pipe';
import { DateFormatPipe } from 'ngx-moment';
import { DateRangePickerComponent } from '../../../shared/components/date-range-picker/date-range-picker.component';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('GovernanceRoomBlockComponent', () => {
  let component: GovernanceRoomBlockComponent;
  let fixture: ComponentFixture<GovernanceRoomBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GovernanceRoomBlockComponent, MomentCalendarPipe, DateFormatPipe, DateRangePickerComponent],
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgxDatatableModule,
        MyDateRangePickerModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      providers: [ InterceptorHandlerService ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernanceRoomBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
