import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { RoomBlock } from '../../../shared/models/dto/housekeeping/room-block';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { RoomBlockResource } from '../../../shared/resources/room-block/room-block.resource';
import { ActivatedRoute, Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DateService } from '../../../shared/services/shared/date.service';
import { ReasonResource } from '../../../shared/resources/reason/reason.resource';
import { DataTableGlobals } from '../../../shared/models/DataTableGlobals';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import * as moment from 'moment';

@Component({
  selector: 'app-governance-room-block',
  templateUrl: './governance-room-block.component.html',
  styleUrls: ['./governance-room-block.component.css'],
})
export class GovernanceRoomBlockComponent implements OnInit {
  @Input() public selectedItens: any[] = [];

  @Output() public roomBlocked: EventEmitter<any> = new EventEmitter<any>();

  private MODAL_REF_BLOCK = 'modalBlock';
  private MODAL_REF_BLOCK_ERROR = 'modalBlockError';
  private REASON_CATEGORY_ID_BLOCK_ROOM = 5;

  public modalStatusTitle: string;
  public modalBlockTitle: string;
  public modalBlockErrorTitle: string;
  public modalUnblockTitle: string;
  public propertyId: number;

  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;

  public reasonList: Array<any>;

  public blockForm: FormGroup;
  public blockMessage;
  public errorReservationList: Array<any> = [];

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public dataTableGlobals: DataTableGlobals,
    public modalEmitService: ModalEmitService,
    public modalEmitToggleService: ModalEmitToggleService,
    private loadPageEmitService: LoadPageEmitService,
    public translateService: TranslateService,
    private toastEmitService: ToasterEmitService,
    private formBuilder: FormBuilder,
    private dateService: DateService,
    private reasonResource: ReasonResource,
    private sharedService: SharedService,
    private commomService: CommomService,
    public roomBlockResource: RoomBlockResource,
  ) {}

  ngOnInit() {
    this.setVars();
    this.setBlockForm();
    this.setButtonConfig();
    this.getReasonsList();
  }

  setVars() {
    this.propertyId = +this.route.snapshot.params.property;
  }

  private setButtonConfig() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'block-cancel-modal',
      this.onCloseModal.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
      'block-save-modal',
      this.block.bind(this),
      'action.confirm',
      ButtonType.Primary,
      null,
      true,
    );
  }

  onCloseModal() {
    this.toggleModal(this.MODAL_REF_BLOCK);
  }

  public setBlockForm() {
    this.blockForm = this.formBuilder.group({
      vigence: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      obs: '',
    });

    this.blockForm.valueChanges.subscribe(value => {
      this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
        'block-save-modal',
        this.block.bind(this),
        'action.confirm',
        ButtonType.Primary,
      );
      this.buttonSaveModalConfig.isDisabled = this.blockForm.valid ? false : true;
    });
  }

  getReasonsList() {
    this.reasonResource.getAllReasonListByPropertyIdAndCategoryId(this.propertyId, this.REASON_CATEGORY_ID_BLOCK_ROOM).subscribe(result => {
      this.reasonList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(result, 'id', 'reasonName');
    });
  }

  callBlock() {
    this.clearForm();
    this.setModalTitle();
    this.setBlockMessage();
    this.toggleModal(this.MODAL_REF_BLOCK);
  }

  block() {
    this.loadPageEmitService.emitChange(ShowHide.show);
    const toBlockList: Array<RoomBlock> = this.prepareBlockRoomList();
    this.roomBlockResource.blockRoomList(toBlockList).subscribe(
      result => {
        this.loadPageEmitService.emitChange(ShowHide.hide);
        let messageToDisplay: string;
        if (this.selectedItens.length > 1) {
          messageToDisplay = 'alert.roomsBlockedsSuccess';
        } else {
          messageToDisplay = 'alert.roomsBlockedSuccess';
        }

        this.toggleModal(this.MODAL_REF_BLOCK);
        this.toastEmitService.emitChange(SuccessError.success, this.translateService.instant(messageToDisplay));

        this.roomBlocked.emit();
      },
      error => {
        this.loadPageEmitService.emitChange(ShowHide.hide);
        this.toggleModal(this.MODAL_REF_BLOCK);

        if (error.status == 403) {
          this.errorReservationList = JSON.parse(error.error.details[0].message);
          this.toggleModal(this.MODAL_REF_BLOCK_ERROR);
        } else {
          this.toastEmitService.emitChange(SuccessError.error, this.translateService.instant(error.error.details[0].message));
        }
      },
    );
  }

  private prepareBlockRoomList(): Array<RoomBlock> {
    const prepareList: Array<RoomBlock> = [];
    this.selectedItens.forEach(item => {
      const roomBlock = new RoomBlock();
      roomBlock.propertyId = this.propertyId;
      roomBlock.roomId = item.id;
      roomBlock.blockingStartDate = this.blockForm.get('vigence').value.beginDate;
      roomBlock.blockingEndDate = this.blockForm.get('vigence').value.endDate;
      roomBlock.reasonId = this.blockForm.get('reason').value ? this.blockForm.get('reason').value : null;
      roomBlock.comments = this.blockForm.get('obs').value ? this.blockForm.get('obs').value : null;

      prepareList.push(roomBlock);
    });
    return prepareList;
  }

  private setBlockMessage() {
    if (this.selectedItens.length == 1) {
      this.blockMessage = this.translateService.instant('alert.blockAttentionMessageSingle', {
        name: ' a UH ' + this.selectedItens[0].roomTypeName + '-' + this.selectedItens[0].roomNumber,
      });
    } else {
      this.blockMessage = this.translateService.instant('alert.blockAttentionMessagePlural');
    }
  }

  private clearForm() {
    this.blockForm.get('vigence').setValue(null);
    this.blockForm.get('reason').setValue(null);
    this.blockForm.get('obs').setValue(null);

    if (this.selectedItens.length == 1 && this.selectedItens[0].blockingStartDate && this.selectedItens[0].blockingEndDate) {
      const selectedItem = this.selectedItens[0];
      this.blockForm.patchValue({
        vigence: this.dateService.getDateRangePicker(
          moment(selectedItem.blockingStartDate).toDate(),
          moment(selectedItem.blockingEndDate).toDate(),
        ),
      });
    }
  }

  private setModalTitle() {
    let blockTitle;
    if (this.selectedItens.length == 1) {
      blockTitle = {
        title: 'alert.blockUH',
        params: { name: ' - ' + this.selectedItens[0].roomTypeName + ' ' + this.selectedItens[0].roomNumber },
      };
    } else {
      blockTitle = {
        title: 'alert.blockUH',
        params: { name: '\'s' },
      };
    }

    this.modalStatusTitle = this.translateService.instant('title.governanceStatus');
    this.modalBlockTitle = blockTitle;
    this.modalUnblockTitle = this.translateService.instant('title.governanceRoomUnblock');
    this.modalBlockErrorTitle = this.translateService.instant('label.attention');
  }

  toggleModal(ref) {
    this.modalEmitToggleService.emitToggleWithRef(ref);
  }

  public redirectReservation(id: number) {
    this.router
      .navigate(['p', this.propertyId, 'reservation', 'new'], {
        queryParams: {reservation: id}
      });
  }
}
