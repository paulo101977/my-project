import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { GovernanceAssocComponent } from './governance-assoc.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CoreModule } from '@app/core/core.module';
import { SharedModule } from '@app/shared/shared.module';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { MockData } from '../../mock-data/mock-data';
import { configureTestSuite } from 'ng-bullet';


describe('GovernanceAssocComponent', () => {
  let component: GovernanceAssocComponent;
  let fixture: ComponentFixture<GovernanceAssocComponent>;
  const mockData = new MockData();

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        CoreModule,
        SharedModule
      ],
      declarations: [ GovernanceAssocComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernanceAssocComponent);
    component = fixture.componentInstance;

    spyOn(component, 'setColumns');
    spyOn(component, 'setButtonConfig').and.callThrough();
    spyOn(component, 'setInitialDate').and.callThrough();
    spyOn(component['assocResource'], 'getRooms')
      .and
      .returnValue(of(mockData.roomAssocList));
    spyOn(component['assocResource'], 'getEmployeeList')
      .and
      .returnValue(of(mockData.employeeList));
    spyOn(component['assocResource'], 'getRoomsByUser')
      .and
      .returnValue(of(mockData.modalEmployeeList));
    spyOn(component, 'pullRooms').and.callThrough();
    spyOn(component, 'pullEmployees').and.callThrough();
    spyOn(component, 'enableOrDisableButton').and.callThrough();

    component.ngOnInit();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call init methods', () => {

    expect(component.setColumns).toHaveBeenCalled();
    expect(component['setButtonConfig']).toHaveBeenCalled();
    expect(component.setInitialDate).toHaveBeenCalled();
    expect(component.pullRooms).toHaveBeenCalled();
    expect(component.pullEmployees).toHaveBeenCalled();
  });

  it('should check if room list is filled', fakeAsync(() => {

    tick();
    fixture.detectChanges();

    expect(component.roomAssocList).toEqual(mockData.roomAssocList);
  }));

  it('should check selectRoom method', fakeAsync(() => {

    spyOn(component, 'selectRoom').and.callThrough();

    tick();
    fixture.detectChanges();

    component.selectRoom(mockData.roomAssocList[1]);


    expect(component.selectedItemsRooms).toEqual([mockData.roomAssocList[1]]);
    expect(component.selectedItemsRooms.length).toEqual(1);
    expect(component.enableOrDisableButton).toHaveBeenCalled();

    // clear selection
    component.selectRoom(mockData.roomAssocList[1]);

    expect(component.selectedItemsRooms).toEqual([]);
    expect(component.selectedItemsRooms.length).toEqual(0);
  }));

  it('should check if Employees list is filled', fakeAsync(() => {

    tick();
    fixture.detectChanges();

    expect(component.employeesAssocList).toEqual(mockData.employeeList);
  }));

  it('should check if modal employee is called', fakeAsync(() => {
    spyOn(component, 'openInfo').and.callThrough();
    spyOn(component, 'viewModal').and.callThrough();


    component.openInfo(mockData.employeeList[1], mockData.event);

    tick();
    fixture.detectChanges();

    expect(component.viewModal).toHaveBeenCalledWith(component['MODAL_GOVERNANCE_ASSOC_ID']);
  }));

  it('should check if filterThreatName work', fakeAsync(() => {
    spyOn(component, 'filterThreatName').and.callThrough();

    component.ngOnInit();
    tick();


    component.filterThreatName('10');


    expect(component.filterThreatName).toHaveBeenCalledWith('10');
    expect(component.roomAssocList.length).toEqual(2);


    component.filterThreatName('102');

    expect(component.filterThreatName).toHaveBeenCalledWith('102');
    expect(component.roomAssocList.length).toEqual(1);


    // rollback
    component.filterThreatName('');

    expect(component.filterThreatName).toHaveBeenCalledWith('');
    expect(component.roomAssocList.length).toEqual(2);


    component.filterThreatName('limpo');

    expect(component.filterThreatName).toHaveBeenCalledWith('limpo');
    expect(component.roomAssocList.length).toEqual(1);
  }));

  it('should check if filterEmployee work', fakeAsync(() => {
    spyOn(component, 'filterEmployee').and.callThrough();

    component.ngOnInit();
    tick();


    component.filterEmployee('test');


    expect(component.filterEmployee).toHaveBeenCalledWith('test');
    expect(component.employeesAssocList.length).toEqual(2);


    // rollback
    component.filterEmployee('');

    expect(component.filterEmployee).toHaveBeenCalledWith('');
    expect(component.employeesAssocList.length).toEqual(2);


    // rollback
    component.filterEmployee('123');

    expect(component.filterEmployee).toHaveBeenCalledWith('123');
    expect(component.employeesAssocList.length).toEqual(1);
  }));

  it('should check if selectEmployee correctly', fakeAsync(() => {
    spyOn(component, 'selectEmployee').and.callThrough();
    spyOn(component, 'unsetOld').and.callThrough();

    component.ngOnInit();
    tick();

    component.selectEmployee(mockData.employeeList[1]);


    expect(component.selectedEmployee).toEqual(mockData.employeeList[1]);
    expect(component.enableOrDisableButton).toHaveBeenCalled();
    expect(component.unsetOld).toHaveBeenCalledTimes(0);


    component.selectEmployee(mockData.employeeList[0]);

    expect(component.selectedEmployee).toEqual(mockData.employeeList[0]);
    expect(component.enableOrDisableButton).toHaveBeenCalled();
    expect(component.unsetOld).toHaveBeenCalledTimes(1);
  }));

  it('should check if item is save correctly', fakeAsync(() => {
    spyOn(component, 'apply').and.callThrough();
    spyOn(component['assocResource'], 'saveRoomsByUser')
      .and
      .returnValue(of(true));
    spyOn(component, 'clearSchedule').and.callThrough();


    component.ngOnInit();
    tick();

    component.selectedEmployee = {...mockData.employeeList[1]};
    component.selectedItemsRooms = [...mockData.roomAssocList];

    component.apply();

    expect(component.clearSchedule).toHaveBeenCalled();
    expect(component.setInitialDate).toHaveBeenCalled();
    expect(component.pullRooms).toHaveBeenCalled();
    expect(component.pullEmployees).toHaveBeenCalled();
    expect(component.apply).toHaveBeenCalled();
    expect(component.hasSchedule).toEqual(false);
  }));

  it('should check if click call modal schedule', fakeAsync(() => {
    spyOn(component, 'viewModal').and.callThrough();
    spyOn(component, 'openModalSchedule').and.callThrough();

    const btnSchedule = fixture
      .debugElement
      .query(
        By.css('#schedule-handler')
      );

    btnSchedule.triggerEventHandler('click', null);

    fixture.detectChanges();
    tick();

    expect(component.openModalSchedule).toHaveBeenCalled();
    expect(component.viewModal)
      .toHaveBeenCalledWith(component['MODAL_GOVERNANCE_ASSOC_SCHEDULE']);
  }));

  it('should openClosedAutoModal without list', () => {
    spyOn(component, 'viewModal').and.callThrough();
    spyOn(component, 'openClosedResumeModal');
    component.openClosedAutoModal();

    expect(component.viewModal)
      .toHaveBeenCalledWith(component['MODAL_GOVERNANCE_AUTO_ASSOC']);
    expect(component.openClosedResumeModal).not.toHaveBeenCalled();
  });

  it('should openClosedAutoModal with list', () => {
    spyOn(component, 'viewModal').and.callThrough();
    spyOn(component, 'openClosedResumeModal');
    spyOn(component['assocResource'],
      'housekeepingGenerator').and
      .returnValue(of({roomMaidList: mockData.housekeepingGenerator} ));

    component.openClosedAutoModal(mockData.houseKeepingList);

    expect(component.openClosedResumeModal).toHaveBeenCalled();
    expect(component.housekeepingGenerator).toEqual(mockData.housekeepingGenerator);
  });

  it('should check method dateChanged', fakeAsync(() => {
    spyOn(component, 'dateChanged').and.callThrough();

    const dateObj = {
      scheduleBeginDate: '2019-01-01T00:00:00',
      scheduleEndDate: '2019-01-07T00:00:00'
    };

    component.dateChanged(dateObj);


    expect(component.enableOrDisableButton).toHaveBeenCalled();
    expect(component.dateBeginToSend).toEqual(dateObj.scheduleBeginDate);
    expect(component.dateFinalToSend).toEqual(dateObj.scheduleEndDate);
    expect(component.dateBegin)
      .toEqual(component['dateService'].convertUniversalDateToViewFormat(dateObj.scheduleBeginDate));
    expect(component.dateFinal)
      .toEqual(component['dateService'].convertUniversalDateToViewFormat(dateObj.scheduleEndDate));
    expect(component.hasSchedule).toEqual(true);
  }));

});
