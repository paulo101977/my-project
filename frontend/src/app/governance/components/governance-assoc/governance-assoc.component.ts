import { Component, OnInit, Output, EventEmitter, ViewChild, TemplateRef, ViewEncapsulation, Renderer2 } from '@angular/core';
import { ButtonConfig, ButtonSize, ButtonType } from '@app/shared/models/button-config';
import { Location } from '@angular/common';
import { SharedService } from '@app/shared/services/shared/shared.service';
import { GovernanceAssocResource } from '@app/shared/resources/governance/governance.assoc.resource';
import { GovernanceGridassoc } from '@app/shared/models/governance/governancegridassoc';
import { ModalEmitToggleService } from '@app/shared/services/shared/modal-emit-toggle.service';
import { TranslateService } from '@ngx-translate/core';
import { DateService } from 'app/shared/services/shared/date.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { GovernanceRoomAssoc } from 'app/shared/models/governance/governance-room-assoc';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-governance-assoc',
  templateUrl: './governance-assoc.component.html',
  styleUrls: ['./governance-assoc.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class GovernanceAssocComponent implements OnInit {

  @ViewChild('checkbox') checkbox: TemplateRef<any>;
  @ViewChild('roomStatus') roomStatus: TemplateRef<any>;
  @ViewChild('imgContainer') imgContainer: TemplateRef<any>;
  @ViewChild('templateImg') templateImg: TemplateRef<any>;
  @ViewChild('templateIconInfo') templateIconInfo: TemplateRef<any>;

  public cancelButtonConfig: ButtonConfig;
  public automaticButtonConfig: ButtonConfig;
  public selectedItemsRooms = <any>[];
  public saveButtonConfig: ButtonConfig;

  public columns: Array<any>;
  public columnsGovernance: Array<any>;
  public housekeepingGenerator: Array<GovernanceRoomAssoc>;

  public hasSchedule = false;
  public dateBegin: string;
  public dateFinal: string;
  public dateBeginToSend: string;
  public dateFinalToSend: string;
  public schedulingFor: string;
  public propertyId: number;
  public schedule = false;

  public roomAssocList: Array<GovernanceGridassoc> = [];
  public roomAssocListBack: Array<GovernanceGridassoc> = [];
  public employeesAssocList: Array<any> = [];
  public employeesAssocListBack: Array<any> = [];

  public readonly MODAL_GOVERNANCE_ASSOC_ID = 'MODAL_GOVERNANCE_ASSOC_ID';
  public readonly MODAL_GOVERNANCE_ASSOC_SCHEDULE = 'MODAL_GOVERNANCE_ASSOC_SCHEDULE';
  public readonly MODAL_GOVERNANCE_AUTO_ASSOC = 'MODAL_GOVERNANCE_AUTO_ASSOC';
  public readonly MODAL_GOVERNANCE_INFO_AUTO = 'MODAL_GOVERNANCE_INFO_AUTO';
  public readonly MODAL_GOVERNANCE_RESUME = 'MODAL_GOVERNANCE_RESUME';

  // TODO: create model
  public selectedItemToShow: any;
  public itemsToShow: Array<any>;
  public selectedEmployee = null;
  public selectedRoom = null;
  public selectedSchedule = null;

  constructor(
    private location: Location,
    private sharedService: SharedService,
    private modalToggle: ModalEmitToggleService,
    private assocResource: GovernanceAssocResource,
    private translateService: TranslateService,
    private dateService: DateService,
    private renderer: Renderer2,
    private toasterEmitter: ToasterEmitService,
    private route: ActivatedRoute
  ) {}

  public setColumns() {

    this.columns = [
      {
        prop: '',
        cellTemplate: this.checkbox,
        width: 50,
        flexGrow: 0.3
      },
      {
        name: 'label.uh',
        prop: 'roomNumber',
        width: 100,
        flexGrow: 0.5
      },
      {
        prop: '',
        cellTemplate: this.roomStatus,
        name: 'label.governanceStatus',
        flexGrow: 2
      },
      {
        name: 'label.statusUH',
        prop: 'roomStatusName',
        flexGrow: 1
      },
      {
        prop: '',
        cellTemplate: this.imgContainer,
        flexGrow: 0.3
      }
    ];

    this.columnsGovernance = [
      {
        prop: '',
        cellTemplate: this.templateImg,
        name: 'label.employees',
        flexGrow: 2.5
      },
      {
        prop: 'total',
        name: 'label.roomQuantity',
        flexGrow: 2
      },
      {
        prop: '',
        cellTemplate: this.templateIconInfo,
        flexGrow: 1
      }
    ];
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setColumns();
    this.setButtonConfig();
    this.setInitialDate();
    this.pullRooms();
    this.pullEmployees();
  }

  public pullEmployees(): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .assocResource
        .getEmployeeList()
        .subscribe(res => {
          this.employeesAssocList = [...res];
          this.employeesAssocListBack = this.employeesAssocList;

          resolve(null);
        });
    });
  }

  public  pullRooms(): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .assocResource
        .getRooms()
        .subscribe(rooms => {
          this.roomAssocList = rooms;
          this.roomAssocListBack = this.roomAssocList;

          resolve(null);
      });
    });
  }

  public selectRoom(element) {
    const position = this.selectedItemsRooms.indexOf(element);

    if (position >= 0) {
      this.selectedItemsRooms.splice(position, 1);
    } else {
      this.selectedItemsRooms.push(element);
    }

    this.enableOrDisableButton();
  }

  public enableOrDisableButton() {
    this.saveButtonConfig.isDisabled = !(this.selectedItemsRooms.length > 0
      && this.selectedEmployee
      && this.dateBegin
      && this.dateFinal);
  }

  public getInitialsName( name ) {
    let nameToReturn = '';

    if ( name ) {
      const splited = name.trim().toUpperCase().split(' ');

      nameToReturn = splited[0].charAt(0);

      if ( splited.length > 1 ) {
        nameToReturn += splited[splited.length - 1].charAt(0);
      }

      return nameToReturn;
    }

    return '';
  }

  public openInfo( item , event ) {
    const { userId } = item;

    event.preventDefault();
    event.stopPropagation();

    this.selectedItemToShow = item;

    this
      .assocResource
      .getRoomsByUser(userId)
      .subscribe(
      res => {
        this.itemsToShow = res;

        if ( res ) {
          this.viewModal(this.MODAL_GOVERNANCE_ASSOC_ID);
        }
    });
  }

  public async getGridUserByRoomId( roomId ): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .assocResource
        .getGridUserByRoomId( roomId )
        .subscribe( item => {
          resolve(item);
        });
    });

  }


  public async selectItemRoom(room, event) {
    event.stopPropagation();
    event.preventDefault();


    // TODO:  implement below
    // FIXME: fix the popover dont close
    // console.log('el', this.renderer.selectRootElement('thx-popover'));
    // this.renderer.selectRootElement('thx-popover').hidePopover();

    if ( room.isOpen ) {
      room.isOpen = false;
      return;
    }

    const userInfo = await this.getGridUserByRoomId( room.housekeepingRoomId );

    this.selectedRoom = room;
    this.selectedSchedule = {};

    if ( userInfo && userInfo.startDate && userInfo.endDate ) {
      const { startDate , endDate } = userInfo;

      this.selectedSchedule.startDate = startDate;
      this.selectedSchedule.endDate = endDate;
    }

    room.isOpeon = true;
  }

  public getElapsedTime( item ) {
    const { selectedSchedule } = this;


    if ( selectedSchedule ) {
      const { startDate, endDate } = selectedSchedule;

      if ( startDate && endDate ) {
        const diff = Math.abs(new Date(endDate).getTime() - new Date(startDate).getTime());
        return this.translateService.instant('text.elapsedTime', {
          elapsedTime: Math.floor(diff / 60000),
          maxTime: 40
        });
      }

      return '';
    }

    return '';
  }

  public unsetOld(item) {
    if ( this.employeesAssocListBack ) {
      const old = this.employeesAssocListBack.find(cur => item.userId === cur.userId);

      if ( old ) {
        old['isSelected'] = false;
      }
    }
  }

  public selectEmployee(item: any) {
    const { selectedEmployee } = this;

    item['isSelected'] = true;

    if ( selectedEmployee && selectedEmployee.userId !== item.userId) {
      this.unsetOld(selectedEmployee);
    }

    this.selectedEmployee = Object.assign({}, item);

    this.enableOrDisableButton();
  }

  public viewModal(id: string) {
    this.modalToggle.emitToggleWithRef(id);
  }

  public apply = async () => {
    const {
      selectedEmployee,
      selectedItemsRooms,
      dateBeginToSend,
      dateFinalToSend
    } = this;

    const { userId } = selectedEmployee;

    const senderObj = {
      ownerId: userId,
      serviceDateStart: dateBeginToSend,
      serviceDateEnd: dateFinalToSend,
      roomList: []
    };

    senderObj.roomList = selectedItemsRooms.map( item => {
      const { roomId } = item;
      return {
        id: roomId
      };
    });

    this
      .assocResource
      .saveRoomsByUser(senderObj)
      .subscribe( async item => {
        this.selectedEmployee = null;
        this.selectedItemsRooms = [];
        this.clearSchedule();

        await this.pullRooms();
        await this.pullEmployees();

        this
          .toasterEmitter
          .emitChange(SuccessError.success, 'text.roomsAssociatedSucess');
    });
  }

  public setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      () => this.location.back(),
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.saveButtonConfig = this.sharedService.getButtonConfig(
      'save',
      this.apply,
      'action.associate',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
    this.automaticButtonConfig = this.sharedService.getButtonConfig(
      'automatic',
      () => this.openClosedAutoModal(),
      'action.automatic',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.automaticButtonConfig.extraClass = 'thf-button-info button-bold-size';
  }

  public openClosedAutoModal(list?) {
    this.employeesAssocList = [...this.employeesAssocList];
    this.viewModal(this.MODAL_GOVERNANCE_AUTO_ASSOC);
    if (list && list.length > 0) {
      const houseKeepingList = {
        GeneratorList: list
      };

      this.assocResource.housekeepingGenerator({ GeneratorList: list }).subscribe( ({ roomMaidList }) => {
        this.housekeepingGenerator = roomMaidList;
      });

      this.openClosedResumeModal();
    }
  }

  public openClosedResumeModal() {
    this.viewModal(this.MODAL_GOVERNANCE_RESUME);
    this.pullRooms();
    this.pullEmployees();
  }

  public closedInfoModal() {
    this.viewModal(this.MODAL_GOVERNANCE_INFO_AUTO);
  }

  public openClosedInfoAutoModal() {
    this.viewModal(this.MODAL_GOVERNANCE_INFO_AUTO);
  }

  public openModalSchedule() {
    this.clearSchedule();

    this.viewModal(this.MODAL_GOVERNANCE_ASSOC_SCHEDULE);
  }

  public closeModalSchedule() {
    this.viewModal(this.MODAL_GOVERNANCE_ASSOC_SCHEDULE);
  }

  public getSystemDate() {
    return this.dateService.getSystemDate(this.propertyId);
  }

  public setInitialDate() {
    const { dateService } = this;

    this.dateBeginToSend = this.getSystemDate().toStringUniversal();
    this.dateFinalToSend = this.getSystemDate().toStringUniversal();

    this.dateBegin = dateService.convertUniversalDateToViewFormat(this.dateBeginToSend);
    this.dateFinal = dateService.convertUniversalDateToViewFormat(this.dateFinalToSend);
  }

  public clearSchedule() {
    this.setInitialDate();

    this.hasSchedule = false;

    this.enableOrDisableButton();
  }

  public dateChanged(dateObj) {

    if ( dateObj ) {
      const { dateService } = this;
      const { scheduleBeginDate, scheduleEndDate } = dateObj;

      this.dateBeginToSend = scheduleBeginDate;
      this.dateFinalToSend = scheduleEndDate;


      this.dateBegin = dateService.convertUniversalDateToViewFormat(scheduleBeginDate);
      this.dateFinal = dateService.convertUniversalDateToViewFormat(scheduleEndDate);
      this.hasSchedule = true;

      this.schedulingFor = this.translateService.instant('label.schedulingFor', {
        beginDate: this.dateBegin,
        finalDate: this.dateFinal
      });

      this.enableOrDisableButton();
    }
  }


  public filterEmployee(value: string) {
    if ( value && this.employeesAssocListBack) {
      const regexExp = new RegExp(value, 'i');
      this.employeesAssocList = this.employeesAssocListBack.filter( ({name}) =>
        regexExp.test(name)
      );
    } else {
      this.employeesAssocList = this.employeesAssocListBack;
    }
  }

  public filterThreatName(value: string) {
    if ( value && this.roomAssocListBack) {
      const regexExp = new RegExp(value, 'i');

      this.roomAssocList = this.roomAssocListBack.filter(
        ({
          roomNumber,
          housekeepingStatusName,
          roomStatusName
        }) => {

        return regexExp.test(`${roomNumber}`)
          || regexExp.test(housekeepingStatusName)
          || regexExp.test(roomStatusName);
      });
    } else {
      this.roomAssocList = this.roomAssocListBack;
    }
 }
}
