import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { GovernanceStatusListComponent } from './governance-status-list.component';
import { GovernanceStatusFormComponent } from '../governance-status-form/governance-status-form.component';
import { SubmenuOptionsComponent } from '../../../shared/components/submenu-options/submenu-options.component';

describe('GovernanceStatusListComponent', () => {
  let component: GovernanceStatusListComponent;
  let fixture: ComponentFixture<GovernanceStatusListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxDatatableModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],

      declarations: [GovernanceStatusListComponent, GovernanceStatusFormComponent, SubmenuOptionsComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernanceStatusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
