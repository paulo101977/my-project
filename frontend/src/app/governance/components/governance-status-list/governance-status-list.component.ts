import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GovernanceStatusFormComponent } from '../governance-status-form/governance-status-form.component';
import { TranslateService } from '@ngx-translate/core';
import { GovernanceStatusService } from '../../../shared/services/governance/governance-status.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { GovernanceStatusResource } from '../../../shared/resources/governance/governance-status.resource';
import { Governance } from '../../../shared/models/governance/governance';
import { DataTableGlobals } from '../../../shared/models/DataTableGlobals';
import { ConfigHeaderPage, SizeOfSearchButtonEnum } from '../../../shared/models/config-header-page';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../shared/models/button-config';

@Component({
  selector: 'app-governance-status-list',
  templateUrl: './governance-status-list.component.html',
  styleUrls: ['./governance-status-list.component.css'],
  providers: [DataTableGlobals, GovernanceStatusResource],
  entryComponents: [GovernanceStatusFormComponent],
})
export class GovernanceStatusListComponent implements OnInit {
  @ViewChild('statusFormComponent') formComponent: GovernanceStatusFormComponent;
  public configHeaderPage: ConfigHeaderPage;
  public searchableItems: SearchableItems;

  public govervanceStatusList: Array<Governance>;
  private itemsOptionDefault: Array<any>;
  private itemsOption: Array<any>;
  public modalTitle: string;
  private propertyId: number;

  public createButtonConfig: ButtonConfig;

  public governanceSelected: Governance = new Governance();

  constructor(
    private dataTableGlobals: DataTableGlobals,
    public translateService: TranslateService,
    private modalEmitToggleService: ModalEmitToggleService,
    private governanceService: GovernanceStatusService,
    private modalEmitService: ModalEmitService,
    private governanceResource: GovernanceStatusResource,
    private loadPageEmitService: LoadPageEmitService,
    private toastEmitService: ToasterEmitService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.getAllGovernanceStatus();
    this.setVars();
  }

  public getAllGovernanceStatus() {
    this.governanceResource.getAll(this.propertyId).subscribe(
      result => {
        if (result) {
          this.govervanceStatusList = [
            ...result.map(item => {
              item.color = item.color.indexOf('#') < 0 ? '#' + item.color : item.color;
              return item;
            }),
          ];
          this.govervanceStatusList = this.governanceService.sortByDefault(this.govervanceStatusList);
        }
      },
      error => {
        console.error(error);
        this.govervanceStatusList = [];
      },
    );
  }
  setVars() {
    this.setConfigHeaderPage();
    this.setSearchableItems();

    this.itemsOptionDefault = [{ title: 'commomData.edit', callbackFunction: this.editStatus.bind(this) }];

    this.itemsOption = this.itemsOptionDefault.concat([{ title: 'commomData.delete', callbackFunction: this.confimRemove.bind(this) }]);

    this.createButtonConfig = new ButtonConfig();
    this.createButtonConfig.buttonType = ButtonType.Secondary;
    this.createButtonConfig.buttonSize = ButtonSize.Small;
    this.createButtonConfig.textButton = 'variable.newGovernanceStatus';
    this.createButtonConfig.callback = this.goToCreatePage.bind(this);
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = new ConfigHeaderPage(HeaderPageEnum.List);
    this.configHeaderPage.titlePage = 'hotelPanelModule.governance.title';
    this.configHeaderPage.titleCreateButton = 'hotelPanelModule.governance.newStatusGovernance';
    this.configHeaderPage.goToCreatePage = this.goToCreatePage;
    this.configHeaderPage.sizeOfSearchButtonEnum = SizeOfSearchButtonEnum.Medium;
  }

  private setSearchableItems() {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.govervanceStatusList;
    this.searchableItems.tableRowTypeEnum = TableRowTypeEnum.Governance;
    this.searchableItems.placeholderSearchFilter = 'hotelPanelModule.governance.filterSearchPlaceholder';
  }

  public goToCreatePage = () => {
    this.modalTitle = this.translateService.instant('hotelPanelModule.governance.createModalTitle');
    this.governanceSelected = new Governance();
    this.formComponent.modalForm.markAsPristine();
    this.toggleModal();
  }

  public editStatus(item) {
    this.modalTitle = this.translateService.instant('hotelPanelModule.governance.editModalTitle');
    this.governanceSelected = item;
    this.formComponent.setFormGroupControls();
    this.toggleModal();
  }

  public confimRemove(item: Governance) {
    this.governanceSelected = item;
    this.modalEmitService.emitSimpleModal(
      this.translateService.instant('commomData.delete'),
      this.translateService.instant('variable.deleteConfimationM', { name: 'Status "' + item.statusName + '"' }),
      this.remove.bind(this),
      [],
    );
  }

  /* EVENTS */
  public updateList(items) {
    if (items != undefined) {
      this.govervanceStatusList = items;
    }
  }

  public updateStatusFlag(governanceStatus: Governance) {
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.governanceResource.toggleStatus(governanceStatus.id).subscribe(
      result => {
        this.loadPageEmitService.emitChange(ShowHide.hide);
        if (governanceStatus.isActive) {
          this.toastEmitService.emitChange(SuccessError.success, this.translateService.instant('variable.toggleStatusEnableSuccess'));
        } else {
          this.toastEmitService.emitChange(SuccessError.success, this.translateService.instant('variable.toggleStatusDisableSuccess'));
        }
      },
      error => {
        this.toastEmitService.emitChange(SuccessError.error, this.translateService.instant('variable.toggleStatusError'));
        governanceStatus.isActive = !governanceStatus.isActive;
        this.loadPageEmitService.emitChange(ShowHide.hide);
      },
    );
  }

  public saveGovernanceStatus(governance: Governance) {
    if (governance.id) {
      this.callEditResource(governance);
    } else {
      this.callCreateResource(governance);
    }
  }

  public callEditResource(governance) {
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.governanceResource.edit(governance).subscribe(
      result => {
        this.getAllGovernanceStatus();
        this.toastEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.editSuccessM', { name: governance.statusName }),
        );
        this.loadPageEmitService.emitChange(ShowHide.hide);
        this.toggleModal();
      },
      error => {
        this.getAllGovernanceStatus();
        this.toastEmitService.emitChange(SuccessError.error, this.translateService.instant('variable.editError'));
        this.loadPageEmitService.emitChange(ShowHide.hide);
      },
    );
  }

  public callCreateResource(governance) {
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.governanceResource.create(governance, this.propertyId).subscribe(
      result => {
        this.getAllGovernanceStatus();
        this.toastEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.saveSuccessM', { name: governance.statusName }),
        );
        this.loadPageEmitService.emitChange(ShowHide.hide);
        this.toggleModal();
      },
      error => {
        this.getAllGovernanceStatus();
        this.toastEmitService.emitChange(SuccessError.error, this.translateService.instant('variable.saveError'));
        this.loadPageEmitService.emitChange(ShowHide.hide);
      },
    );
  }
  public toggleModal() {
    this.modalEmitToggleService.emitToggleWithRef('modalGovernance');
  }

  public remove() {
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.governanceResource.delete(this.governanceSelected.id).subscribe(
      result => {
        this.getAllGovernanceStatus();
        this.loadPageEmitService.emitChange(ShowHide.hide);
        this.toastEmitService.emitChange(SuccessError.success, this.translateService.instant('variable.deletedM'));
      },
      error => {
        this.toastEmitService.emitChange(SuccessError.error, this.translateService.instant('variable.deleteError'));
        this.loadPageEmitService.emitChange(ShowHide.hide);
      },
    );
  }

  public runCallbackFunction(item: any, element: any) {
    if (item.callbackFunction) {
      item.callbackFunction(element);
    }
  }
}
