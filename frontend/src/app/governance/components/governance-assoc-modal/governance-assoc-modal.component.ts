import {
  Component,
  OnInit,
  Input,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'governance-assoc-modal',
  templateUrl: './governance-assoc-modal.component.html',
  styleUrls: ['./governance-assoc-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class GovernanceAssocModalComponent implements OnInit {

  @Input() modalId: string;

  // TODO: create model
  @Input() items: Array<any>;
  @Input() selectedItemToShow: any;

  @ViewChild('roomStatus') roomStatus: TemplateRef<any>;

  public columns: Array<any>;

  public getInitialsName( item ) {
    if ( item ) {
      const { name } = item;

      let nameToReturn = '';

      if (name) {
        const splited = name.trim().toUpperCase().split(' ');

        nameToReturn = splited[0].charAt(0);

        if (splited.length > 1) {
          nameToReturn += splited[splited.length - 1].charAt(0);
        }

        return nameToReturn;
      }
    }

    return '';
  }

  public setColumn() {
    this.columns = [
      {
        name: 'label.uh',
        prop: 'roomNumber',
        flexGrow: 0.5
      },
      {
        prop: '',
        cellTemplate: this.roomStatus,
        name: 'label.governanceStatus',
        flexGrow: 2
      },
      {
        name: 'label.statusUH',
        prop: 'roomStatusName',
        flexGrow: 1
      }
    ];
  }

  ngOnInit(): void {
    this.setColumn();
  }

}
