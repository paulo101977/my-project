import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { GovernanceRoomAssoc } from 'app/shared/models/governance/governance-room-assoc';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-governance-auto-assoc-modal',
  templateUrl: './governance-auto-assoc-modal.component.html',
  styleUrls: ['./governance-auto-assoc-modal.component.scss']
})
export class GovernanceAutoAssocModalComponent implements OnInit, OnChanges {

  @Input() modalId: string;
  @Input() houseKeepingList: Array<GovernanceRoomAssoc> = [];
  @Output() closedModal = new EventEmitter();

  @ViewChild('checkbox') checkbox: TemplateRef<any>;
  @ViewChild('imgContainer') imgContainer: TemplateRef<any>;

  public selectedItems = <any>[];
  public housekeepingIdslist = <any>[];
  public columns: Array<any>;
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;
  public housekeepingTotal: number;
  public formHousekeeping: FormGroup;

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.setColumns();
    this.setButtonConfig();
  }

  ngOnChanges (changes: SimpleChanges): void {
    if (this.houseKeepingList) {
      const maidList = this.houseKeepingList.map(item => this.formBuilder.control(true));
      this.formHousekeeping = this.formBuilder.group({
        maidList: this.formBuilder.array(maidList)
      });

      this.housekeepingIdslist = this.houseKeepingList.map(item => item.userId);
      this.selectedItems = this.housekeepingIdslist;
      this.housekeepingTotal = this.houseKeepingList.length;
    }
  }

  public setColumns() {
    this.columns = [
      {
        prop: '',
        cellTemplate: this.checkbox,
        flexGrow: 0.3
      },
      {
        name: 'text.housekeepingDay',
        prop: 'name',
        flexGrow: 3
      }
    ];
  }

  public setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      () => this.closedAutoModal(),
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'save',
      () => this.closedAutoModal(this.selectedItems),
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal
    );
    this.cancelButtonConfig.extraClass = 'button-bold-size';
    this.confirmButtonConfig.extraClass = 'button-bold-size';
  }

  public closedAutoModal(list?: Array<GovernanceRoomAssoc>) {
    this.closedModal.emit(list);
  }

  public selectHouseKeeping(element) {
    const position = this.housekeepingIdslist.indexOf(element.userId);

    if (position >= 0) {
      this.selectedItems.splice(position, 1);
    } else {
      this.selectedItems.push(element.userId);
    }

    this.housekeepingTotal = this.selectedItems.length;
    this.confirmButtonConfig.isDisabled = this.selectedItems.length == 0 ? true : false;
  }
}
