import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GovernanceAutoAssocModalComponent } from './governance-auto-assoc-modal.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { configureTestSuite } from 'ng-bullet';
import { MockData } from 'app/governance/mock-data/mock-data';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImgCdnModule } from '@inovacao-cmnet/thx-ui';

describe('GovernanceAutoAssoc.ModalComponent', () => {
  let component: GovernanceAutoAssocModalComponent;
  let fixture: ComponentFixture<GovernanceAutoAssocModalComponent>;
  const mockData = new MockData();

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ GovernanceAutoAssocModalComponent ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        ImgCdnModule
      ],
      providers: [ sharedServiceStub ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(GovernanceAutoAssocModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnChanges', () => {
    const items = [
      'b6f2cb85-a110-47c5-c8eb-08d62fad530e',
      'b6f2cb85-a110-47c5-c8eb-08d62fad530e4'
    ];
    component.houseKeepingList = mockData.employeeList;
    component.ngOnChanges({});

    expect(component.housekeepingTotal).toEqual(2);
    expect(component.selectedItems).toEqual(component.housekeepingIdslist);
  });

  it('should selectHouseKeeping', () => {
    component.selectHouseKeeping(mockData.employeeList[0]);

    expect(component.selectedItems[0]).toEqual('b6f2cb85-a110-47c5-c8eb-08d62fad530e4');
  });
});
