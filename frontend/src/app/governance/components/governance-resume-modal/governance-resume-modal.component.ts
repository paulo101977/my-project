import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { GovernanceRoomAssoc } from 'app/shared/models/governance/governance-room-assoc';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { GovernanceAssocResource } from 'app/shared/resources/governance/governance.assoc.resource';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-governance-resume-modal',
  templateUrl: './governance-resume-modal.component.html',
  styleUrls: ['./governance-resume-modal.component.scss']
})
export class GovernanceResumeModalComponent implements OnInit, OnChanges {

  @Input() modalId: string;
  @Input() roomMaidList: Array<any>;
  @Output() closedModal = new EventEmitter();

  public columns: Array<any>;
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;
  public roomMaidListTypeRoom: Array<GovernanceRoomAssoc>;

  constructor(
    private sharedService: SharedService,
    private govAssocRsc: GovernanceAssocResource,
    private toastEmitService: ToasterEmitService,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.setColumns();
    this.setButtonConfig();
  }

  ngOnChanges (changes: SimpleChanges): void {
    this.roomMaidListTypeRoom = [];
    if (this.confirmButtonConfig) {
      this.confirmButtonConfig.isDisabled = true;
    }

    if (this.roomMaidList) {
      this.roomMaidListTypeRoom = JSON.parse(JSON.stringify(this.roomMaidList));
      this.roomMaidListTypeRoom.forEach(item => {
        item.listRoom.map(data =>
          data.roomType = data.roomAbbreviation + ' - ' + data.roomNumber);
      });

      this.confirmButtonConfig.isDisabled = false;
    }
  }

  public setColumns() {
    this.columns = [
      {
        name: 'label.uhs',
        prop: 'roomType',
        flexGrow: 2
      },
      {
        name: 'label.floor',
        prop: 'floor',
        flexGrow: 1
      },
      {
        name: 'label.status',
        prop: 'roomStatus',
        flexGrow: 2
      },
    ];
  }

  public setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      () => this.closedAutoModal(),
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'save',
      () => this.save(),
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
    this.cancelButtonConfig.extraClass = 'button-bold-size';
    this.confirmButtonConfig.extraClass = 'button-bold-size';
  }

  public closedAutoModal() {
    this.closedModal.emit();
  }

  public save() {
    this.govAssocRsc.createHousekeepingGenerator(this.roomMaidList)
      .subscribe( item => {
        this.toastEmitService
          .emitChange(SuccessError.success,
            this.translateService.instant('text.autoEnableSuccess'));
        this.closedModal.emit();
    });
  }

  public hasRoomMaidListTypeRoom() {
    return !this.roomMaidListTypeRoom || !this.roomMaidListTypeRoom.length;
  }

  public getInitialsName( name ) {
    let nameToReturn = '';

    if ( name ) {
      const splited = name.trim().toUpperCase().split(' ');

      nameToReturn = splited[0].charAt(0);

      if ( splited.length > 1 ) {
        nameToReturn += splited[splited.length - 1].charAt(0);
      }

      return nameToReturn;
    }

    return '';
  }
}
