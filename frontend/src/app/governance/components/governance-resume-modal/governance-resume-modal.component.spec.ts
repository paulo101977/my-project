import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GovernanceResumeModalComponent } from './governance-resume-modal.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';
import { MockData } from 'app/governance/mock-data/mock-data';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { governanceAssocResourceStub } from 'app/shared/resources/governance/testing';

describe('GovernanceResumeModal', () => {
  let component: GovernanceResumeModalComponent;
  let fixture: ComponentFixture<GovernanceResumeModalComponent>;
  const mockData = new MockData();

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ GovernanceResumeModalComponent ],
      imports: [ TranslateTestingModule ],
      providers: [ governanceAssocResourceStub ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    });

    fixture = TestBed.createComponent(GovernanceResumeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnChanges', () => {
    component.roomMaidList = mockData.housekeepingGenerator;
    component.ngOnChanges({});

    expect(component.roomMaidList[0].listRoom[0].roomType).toEqual('STD - 103');
    expect(component.roomMaidListTypeRoom[1]).toEqual(component.roomMaidList[1]);
  });
});
