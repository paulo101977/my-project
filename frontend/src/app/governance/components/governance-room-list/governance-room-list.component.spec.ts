import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GovernanceRoomListComponent } from './governance-room-list.component';
import { HeaderPageNewComponent } from '../../../shared/components/header-page-new/header-page-new.component';
import { GroupButtonsComponent } from '../../../shared/components/group-buttons/group-buttons.component';
import { ModalWrapperComponent } from '../../../shared/components/modal-wrapper/modal-wrapper.component';
import { ButtonComponent } from '../../../shared/components/button/button.component';
import { MomentCalendarPipe } from '../../../shared/pipes/moment-calendar.pipe';
import { DateFormatPipe } from 'ngx-moment';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CoreModule } from '@app/core/core.module';

describe('GovernanceRoomListComponent', () => {
  let component: GovernanceRoomListComponent;
  let fixture: ComponentFixture<GovernanceRoomListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GovernanceRoomListComponent,
        HeaderPageNewComponent,
        GroupButtonsComponent,
        ModalWrapperComponent,
        ButtonComponent,
        MomentCalendarPipe,
        DateFormatPipe,
      ],
      providers: [],
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgxDatatableModule,
        MyDateRangePickerModule,
        HttpClientTestingModule,
        RouterTestingModule,
        CoreModule
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovernanceRoomListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
