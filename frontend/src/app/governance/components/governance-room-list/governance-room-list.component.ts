import { GroupButtonsComponent } from './../../../shared/components/group-buttons/group-buttons.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GovernanceRoomBlockComponent } from '../governance-room-block/governance-room-block.component';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { GovernanceStatusService } from '../../../shared/services/governance/governance-status.service';
import { GovernanceStatusResource } from '../../../shared/resources/governance/governance-status.resource';
import { HousekeepingStatusPropertyResource } from '../../../shared/resources/housekeeping/housekeeping-status-property-resource';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { DataTableGlobals } from '../../../shared/models/DataTableGlobals';
import { Governance } from '../../../shared/models/governance/governance';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateService } from '../../../shared/services/shared/date.service';
import { ReasonResource } from '../../../shared/resources/reason/reason.resource';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { RoomBlock } from '../../../shared/models/dto/housekeeping/room-block';
import { RoomBlockResource } from '../../../shared/resources/room-block/room-block.resource';
import { HousekeepingStatusProperty } from '../../../shared/models/dto/housekeeping/housekeeping-status-property-dto';
import { ColorsService } from '../../../shared/services/color/colors.service';

@Component({
  selector: 'app-governance-list',
  templateUrl: './governance-room-list.component.html',
  providers: [HousekeepingStatusPropertyResource, GovernanceStatusResource, ColorsService],
})
export class GovernanceRoomListComponent implements OnInit {
  @ViewChild(GovernanceRoomBlockComponent) public governanceRoomBlock: GovernanceRoomBlockComponent;

  @ViewChild(GroupButtonsComponent)
  public buttonsFilter: GroupButtonsComponent;

  private MODAL_REF_STATUS = 'modalStatus';

  public rowItens: Array<any> = [];
  public housekeepingStatusList: Array<Governance> = [];
  public selectedItens: Array<any> = [];
  public searchConfig: SearchableItems;
  public searchButtons: Array<any>;
  public modalStatusTitle: string;
  public modalBlockTitle: string;
  public modalBlockErrorTitle: string;
  public modalUnblockTitle: string;
  public propertyId: number;

  public blockItemsOptions: Array<any>;
  public unblockItemsOptions: Array<any>;

  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;
  public buttonUnblockConfig: ButtonConfig;
  public buttonBlockConfig: ButtonConfig;
  public buttonModalStatusConfig: ButtonConfig;

  public blockForm: FormGroup;

  constructor(
    public dataTableGlobals: DataTableGlobals,
    public modalEmitService: ModalEmitService,
    public modalEmitToggleService: ModalEmitToggleService,
    private loadPageEmitService: LoadPageEmitService,
    public translateService: TranslateService,
    public houseKeepingStatusService: GovernanceStatusService,
    public housekeepingStatusResouce: GovernanceStatusResource,
    public housekeepingResources: HousekeepingStatusPropertyResource,
    private toastEmitService: ToasterEmitService,
    private formBuilder: FormBuilder,
    private dateService: DateService,
    private reasonResource: ReasonResource,
    private sharedService: SharedService,
    public roomBlockResource: RoomBlockResource,
    public colorService: ColorsService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setVars();
    this.searchConfig = new SearchableItems();
    this.searchConfig.items = this.rowItens;
    this.searchConfig.tableRowTypeEnum = TableRowTypeEnum.HousekeepingStatusProperty;
    this.searchConfig.placeholderSearchFilter = 'placeholder.searchGovernance';

    this.blockItemsOptions = [
      { title: 'alert.blockUHOption', callbackFunction: this.callSingleBlock.bind(this), elementClass: 'text-danger' },
    ];

    this.unblockItemsOptions = [{ title: 'alert.unblockUHOption', callbackFunction: this.callSingleUnblock.bind(this) }];
  }

  setVars() {
    this.setBlockForm();
    this.setModalTitle();
    this.getHousekeepingStatusList();
    this.getHousekeepingStatusPropertyList();
    this.setButtonConfig();
  }

  private setButtonConfig() {
    this.buttonUnblockConfig = this.sharedService.getButtonConfig(
      'unblock-rooms',
      this.callUnblock.bind(this),
      'action.unblock',
      ButtonType.Secondary,
    );
    this.buttonUnblockConfig.extraClass = 'thf-u-width-auto thf-button-bar-height';

    this.buttonBlockConfig = this.sharedService
      .getButtonConfig('block-rooms', this.callBlock.bind(this), 'action.block', ButtonType.Danger);
    this.buttonBlockConfig.extraClass = 'thf-u-width-auto thf-button-bar-height';

    this.buttonModalStatusConfig = this.sharedService.getButtonConfig(
      'modal-status',
      this.toggleModalStatus.bind(this),
      'label.changeStatus',
      ButtonType.Primary,
    );
    this.buttonModalStatusConfig.extraClass = 'thf-u-width-auto thf-button-bar-height';

    this.updateButtonsCount();
  }

  public updateButtonsCount() {
    this.buttonUnblockConfig.textComplementButton = '(' + this.selectedItens.length + ')';
    this.buttonBlockConfig.textComplementButton = '(' + this.selectedItens.length + ')';
    this.buttonModalStatusConfig.textComplementButton = '(' + this.selectedItens.length + ')';
  }

  public setBlockForm() {
    this.blockForm = this.formBuilder.group({
      vigence: [null, [Validators.required]],
      reason: [null, [Validators.required]],
      obs: '',
    });
  }

  prepareSearchButtons() {
    const searchBtn = this.houseKeepingStatusService.filterDefault(this.housekeepingStatusList);
    this.searchButtons = searchBtn.map(item => {
      item = Object.assign({ title: item.statusName }, item);
      return item;
    });
  }

  getHousekeepingStatusList() {
    this.housekeepingStatusResouce.getAll(this.propertyId).subscribe(
      result => {
        this.housekeepingStatusList = [
          ...this.houseKeepingStatusService.sortByDefault(result).map(item => {
            return Object.assign({ icon: this.houseKeepingStatusService.getHousekeepingStatusIconById(item.housekeepingStatusId) }, item);
          }),
        ];
        this.prepareSearchButtons();
      },
      error => {
        console.error(error);
        this.housekeepingStatusList = [];
      },
    );
  }

  getHousekeepingStatusPropertyList() {
    this.cleanSelected();
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.housekeepingResources.getAll(this.propertyId).subscribe(
      result => {
        this.loadPageEmitService.emitChange(ShowHide.hide);
        this.rowItens = [...result];
        this.searchConfig.items = this.rowItens;
      },
      error => {
        this.loadPageEmitService.emitChange(ShowHide.hide);
        console.error(error);
        this.rowItens = [];
      },
    );
  }

  // called when some filter button is selected
  filterBy(item: Governance) {
    this.cleanSelected();
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.housekeepingResources.getAllFilterByHousekeepingStatus(this.propertyId, item.housekeepingStatusId).subscribe(
      result => {
        this.loadPageEmitService.emitChange(ShowHide.hide);
        this.rowItens = [...result];
      },
      () => {
        this.loadPageEmitService.emitChange(ShowHide.hide);
      },
    );
  }

  // called when user search a word on search input
  onSearch(list) {
    this.rowItens = this.buttonsFilter.selectedBtn
      ? [...list].filter(i => i.housekeepingStatusId == this.buttonsFilter.selectedBtn.housekeepingStatusId)
      : [...list];
  }

  toggleSingle(element) {
    const position = this.selectedItens.indexOf(element);
    if (position >= 0) {
      this.selectedItens.splice(position, 1);
    } else {
      this.selectedItens.push(element);
    }
    this.updateButtonsCount();
  }

  toggleAll(checked) {
    if (!checked) {
      this.cleanSelected();
    } else {
      this.selectedItens = [...this.rowItens];
    }
    this.updateButtonsCount();
  }

  toggleModal(ref) {
    this.modalEmitToggleService.emitToggleWithRef(ref);
  }

  toggleModalStatus() {
    this.toggleModal(this.MODAL_REF_STATUS);
  }

  toggleModalSimpleStatus(element) {
    this.cleanSelected();
    this.selectedItens.push(element);
    this.toggleModalStatus();
  }

  chooseHousekeepingStatus(status: Governance) {
    this.toggleModalStatus();
    this.loadPageEmitService.emitChange(ShowHide.show);
    const idList = this.selectedItens.map(item => item.id);
    this.housekeepingResources.changeMultipleStatus(status.id, idList).subscribe(
      () => {
        this.getHousekeepingStatusPropertyList();
        this.selectedItens = [];
        this.loadPageEmitService.emitChange(ShowHide.hide);
        this.toastEmitService.emitChange(SuccessError.success, this.translateService.instant('alert.statusChangeSuccess'));
      },
      () => {
        this.loadPageEmitService.emitChange(ShowHide.hide);
        this.toastEmitService.emitChange(SuccessError.error, this.translateService.instant('alert.statusChangeError'));
      },
    );
  }

  callBlock() {
    this.governanceRoomBlock.selectedItens = this.selectedItens;
    this.governanceRoomBlock.callBlock();
  }

  callSingleBlock(element) {
    this.selectedItens = [element];
    this.updateButtonsCount();
    this.callBlock();
  }

  callUnblock() {
    let title;
    let message;
    if (this.selectedItens.length == 1) {
      title = this.translateService.instant('alert.unblockUH', {
        name: ' - ' + this.selectedItens[0].roomTypeName + ' ' + this.selectedItens[0].roomNumber,
      });
      message = this.translateService.instant('alert.unblockUHConfirmation', {
        name: ' ' + this.selectedItens[0].roomTypeName + ' ' + this.selectedItens[0].roomNumber,
      });
    } else {
      title = this.translateService.instant('alert.unblockUH', { name: '\'s' });
      message = this.translateService.instant('alert.unblockUHConfirmation', {
        name: '\'s ' + this.translateService.instant('label.selecteds'),
      });
    }

    this.modalEmitService.emitSimpleModal(
      this.translateService.instant(title),
      this.translateService.instant(message),
      this.unblock.bind(this),
      [],
    );
  }

  callSingleUnblock(element) {
    this.cleanSelected();
    this.selectedItens.push(element);
    this.callUnblock();
  }

  private prepareBlockRoomList(): Array<RoomBlock> {
    const prepareList: Array<RoomBlock> = [];
    this.selectedItens.forEach(item => {
      const roomBlock = new RoomBlock();
      roomBlock.propertyId = this.propertyId;
      roomBlock.roomId = item.id;
      roomBlock.blockingStartDate = this.blockForm.get('vigence').value ? this.blockForm.get('vigence').value['beginJsDate'] : null;
      roomBlock.blockingEndDate = this.blockForm.get('vigence').value ? this.blockForm.get('vigence').value['endJsDate'] : null;
      roomBlock.reasonId = this.blockForm.get('reason').value ? this.blockForm.get('reason').value : null;
      roomBlock.comments = this.blockForm.get('obs').value ? this.blockForm.get('obs').value : null;
      prepareList.push(roomBlock);
    });
    return prepareList;
  }

  unblock() {
    this.loadPageEmitService.emitChange(ShowHide.show);

    const toUnblockList = this.prepareBlockRoomList();
    this.roomBlockResource.unblockRoomList(this.propertyId, toUnblockList).subscribe(
      () => {
        this.loadPageEmitService.emitChange(ShowHide.hide);
        let messageToDisplay: string;
        if (this.selectedItens.length > 1) {
          messageToDisplay = 'alert.roomsUnblockedsSuccess';
        } else {
          messageToDisplay = 'alert.roomsUnblockedSuccess';
        }
        this.cleanSelected();
        this.getHousekeepingStatusPropertyList();
        this.toastEmitService.emitChange(SuccessError.success, this.translateService.instant(messageToDisplay));
      },
      error => {
        this.loadPageEmitService.emitChange(ShowHide.hide);
        this.toastEmitService.emitChange(SuccessError.error, this.translateService.instant(error.details[0].message));
      },
    );
  }

  onCloseModal(modalId) {
    this.cleanSelected();
    this.toggleModal(modalId);
  }

  public runCallbackFunctionOptionsMenu(item: any, element: any) {
    if (item.callbackFunction) {
      item.callbackFunction(element);
    }
  }

  private cleanSelected() {
    this.selectedItens = [];
  }

  private setModalTitle() {
    let blockTitle;
    if (this.selectedItens.length == 1) {
      blockTitle = {
        title: 'alert.blockUH',
        params: { name: ' - ' + this.selectedItens[0].roomTypeName + ' ' + this.selectedItens[0].roomNumber },
      };
    } else {
      blockTitle = {
        title: 'alert.blockUH',
        params: { name: '\'s' },
      };
    }

    this.modalStatusTitle = this.translateService.instant('title.governanceStatus');
    this.modalBlockTitle = blockTitle;
    this.modalUnblockTitle = this.translateService.instant('title.governanceRoomUnblock');
    this.modalBlockErrorTitle = this.translateService.instant('label.attention');
  }

  public onRoomBlocked() {
    this.getHousekeepingStatusPropertyList();
    this.selectedItens = [];
  }

  public statusChecked(status: HousekeepingStatusProperty) {
    if (this.selectedItens.length == 1 && this.selectedItens[0].housekeepingStatusPropertyId == status.id) {
      return true;
    }
    return false;
  }

  getColorByBgColor(bgColor) {
    return this.colorService.getFontBasedOnColor(bgColor);
  }
}
