import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateService } from 'app/shared/services/shared/date.service';
import { ActivatedRoute } from '@angular/router';
import { IMyDate } from 'mydaterangepicker';

@Component({
  selector: 'governance-schedule-modal',
  templateUrl: './governance-schedule-modal.component.html'
})
export class GovernanceScheduleModalComponent implements OnInit {

  @Input() modalId;
  @Output() emitClose = new EventEmitter();
  @Output() dateChange = new EventEmitter();

  public buttonCancelScheduleConfig;
  public buttonConfirmScheduleConfig;

  public scheduleForm: FormGroup;
  public propertyId: number;

  public disableToday: IMyDate;
  public scheduleEndDateErrors;

  public scheduleBeginDate;
  public scheduleEndDate;

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private dateService: DateService,
    private route: ActivatedRoute
  ) {}

  public setForm() {
    let systemDate: Date;
    systemDate = this.dateService.getSystemDate(this.propertyId);
    const convertDate = this
      .dateService
      .convertUniversalDateToIMyDate(systemDate.toString());
    this.disableToday = convertDate ? convertDate : new Date();

    this.scheduleForm = this.formBuilder.group({
      scheduleBeginDate: [ systemDate ],
      scheduleEndDate: [ systemDate ],
    });
  }

  public setButtonConfig() {
    this.buttonCancelScheduleConfig = this.sharedService.getButtonConfig(
      'cancel',
      this.close,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.buttonConfirmScheduleConfig = this.sharedService.getButtonConfig(
      'save',
      this.confirm,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  public confirm = () => {
    const { scheduleBeginDate, scheduleEndDate } = this;

    this.dateChange.emit({ scheduleBeginDate, scheduleEndDate});
    this.close();
  }

  public close = () => {
    this.emitClose.emit(null);
  }

  public notifyBeginDateChange(event) {
    this.scheduleBeginDate = event;
  }

  public notifyEndDateChange(event) {
    this.scheduleEndDate = event;
  }

  ngOnInit() {
    let systemDate: Date;
    this.propertyId = this.route.snapshot.params.property;
    this.setButtonConfig();
    this.setForm();

    systemDate = this.dateService.getSystemDate(this.propertyId);

    this.disableToday = {
      year: systemDate.getFullYear(),
      month: systemDate.getMonth() + 1,
      day: systemDate.getDate() - 1,
    };

    this.buttonConfirmScheduleConfig.isDisabled = false;

    this.scheduleForm.valueChanges.subscribe( value => {
      const { scheduleBeginDate , scheduleEndDate } = value;

      this.buttonConfirmScheduleConfig.isDisabled = true;

      this.scheduleForm.get('scheduleEndDate').setErrors(null);
      this.scheduleEndDateErrors = false;


      if ( scheduleBeginDate &&  scheduleEndDate ) {
        this.buttonConfirmScheduleConfig.isDisabled = false;

        if ( scheduleEndDate < scheduleBeginDate ) {
          this.scheduleForm.get('scheduleEndDate').setErrors({'more-than': true});
          this.scheduleEndDateErrors = true;
          this.buttonConfirmScheduleConfig.isDisabled = true;
          return;
        }
      }
    });
  }

}
