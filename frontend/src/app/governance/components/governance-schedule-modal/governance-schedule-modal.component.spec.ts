import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { GovernanceScheduleModalComponent } from './governance-schedule-modal.component';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MyDatePickerModule } from 'mydatepicker';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { imgCdnStub } from '../../../../../testing';

const dateToday = new Date();

describe('GovernanceScheduleModalComponent', () => {
  let component: GovernanceScheduleModalComponent;
  let fixture: ComponentFixture<GovernanceScheduleModalComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        GovernanceScheduleModalComponent,
        imgCdnStub,
      ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        MyDatePickerModule
      ],
      providers: [
        FormBuilder,
        sharedServiceStub,
        dateServiceStub
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(GovernanceScheduleModalComponent);
    component = fixture.componentInstance;

    spyOn<any>(component['dateService'], 'getSystemDate').and.returnValue(dateToday);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test if ngOnInit call methods', fakeAsync(() => {
    spyOn(component, 'setButtonConfig');
    spyOn(component, 'setForm');

    component.ngOnInit();

    tick();

    const obj = {
      year: dateToday.getFullYear(),
      month: dateToday.getMonth() + 1,
      day: dateToday.getDate() - 1,
    };

    expect( component.setButtonConfig ).toHaveBeenCalled();
    expect( component.setForm ).toHaveBeenCalled();
    expect( component.disableToday).toEqual(obj);
  }));

  it('should test if check form changes', fakeAsync(() => {

    // simulate begin more than end date
    const dateObj = {
      scheduleBeginDate: '2019-01-01T00:00:00',
      scheduleEndDate: '2018-01-07T00:00:00'
    };

    component.ngOnInit();

    tick();

    component['scheduleForm'].get('scheduleBeginDate').setValue(dateObj.scheduleBeginDate);
    component['scheduleForm'].get('scheduleEndDate').setValue(dateObj.scheduleEndDate);

    tick();

    expect(component.scheduleEndDateErrors).toEqual(true);
  }));
});
