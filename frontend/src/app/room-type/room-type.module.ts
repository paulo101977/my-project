import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RoomTypeRoutingModule } from './room-type-routing.module';
import { SharedModule } from '../shared/shared.module';

import { RoomTypeListComponent } from './components/room-type-list/room-type-list.component';
import { RoomTypeCreateComponent } from './components/room-type-create/room-type-create.component';
import { RoomTypeEditComponent } from './components/room-type-edit/room-type-edit.component';

@NgModule({
  imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule, RouterModule, RoomTypeRoutingModule],
  declarations: [RoomTypeListComponent, RoomTypeCreateComponent, RoomTypeEditComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class RoomTypeModule {}
