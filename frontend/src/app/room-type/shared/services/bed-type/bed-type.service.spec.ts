import { inject, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { BedType } from '../../../../shared/models/bed-type';
import { BedTypeService } from './bed-type.service';

describe('BedTypeService', () => {
  const listWithBedTypeList = [
    {
      roomTypeBedTypeList: [
        { bedType: 1, count: 4, optionalLimit: 3, id: 0 },
        { bedType: 2, count: 1, optionalLimit: 0, id: 0 },
        { bedType: 3, count: 2, optionalLimit: 0, id: 0 },
        { bedType: 5, count: 0, optionalLimit: 5, id: 0 },
      ],
    },
  ];

  const rooms = [
    {
      roomType: {
        roomTypeBedTypeList: [
          { bedType: 1, count: 4, optionalLimit: 3, id: 0 },
          { bedType: 2, count: 1, optionalLimit: 0, id: 0 },
          { bedType: 3, count: 2, optionalLimit: 0, id: 0 },
          { bedType: 5, count: 0, optionalLimit: 5, id: 0 },
        ],
      },
    },
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      providers: [],
    });
  });

  it('should create service', inject([BedTypeService], (service: BedTypeService) => {
    expect(service).toBeTruthy();
  }));

  it('should setBedTypeCount', inject([BedTypeService], (service: BedTypeService) => {
    service.setBedTypeCount(listWithBedTypeList);
    expect<any>(listWithBedTypeList).toEqual([
      {
        bedSingleCount: 4,
        bedDoubleCount: 1,
        bedReversibleCount: 2,
        bedExtraCount: 3,
        bedCribCount: 5,
        roomTypeBedTypeList: [
          { bedType: 1, count: 4, optionalLimit: 3, id: 0 },
          { bedType: 2, count: 1, optionalLimit: 0, id: 0 },
          { bedType: 3, count: 2, optionalLimit: 0, id: 0 },
          { bedType: 5, count: 0, optionalLimit: 5, id: 0 },
        ],
      },
    ]);
  }));

  it('should setBedTypeCountForRooms', inject([BedTypeService], (service: BedTypeService) => {
    service.setBedTypeCountForRooms(rooms);
    expect<any>(rooms).toEqual([
      {
        bedCribCount: 5,
        bedDoubleCount: 1,
        bedExtraCount: 3,
        bedReversibleCount: 2,
        bedSingleCount: 4,
        roomType: {
          roomTypeBedTypeList: [
            { bedType: 1, count: 4, optionalLimit: 3, id: 0 },
            { bedType: 2, count: 1, optionalLimit: 0, id: 0 },
            { bedType: 3, count: 2, optionalLimit: 0, id: 0 },
            { bedType: 5, count: 0, optionalLimit: 5, id: 0 },
          ],
        },
      },
    ]);
  }));

  it('should setBedTypeListData', inject([BedTypeService], (service: BedTypeService) => {
    const dataForm = new FormBuilder();
    const bedList: Array<BedType> = [];
    const singleBed = new BedType();
    singleBed.count = 3;
    singleBed.bedType = 1;
    singleBed.optionalLimit = 5;
    bedList.push(singleBed);

    const doubleBed = new BedType();
    doubleBed.count = 2;
    doubleBed.bedType = 2;
    doubleBed.optionalLimit = 0;
    bedList.push(doubleBed);

    const reversibleBed = new BedType();
    reversibleBed.count = 1;
    reversibleBed.bedType = 3;
    reversibleBed.optionalLimit = 0;
    bedList.push(reversibleBed);

    const cribBed = new BedType();
    cribBed.count = 0;
    cribBed.bedType = 5;
    cribBed.optionalLimit = 4;
    bedList.push(cribBed);

    expect(
      service.setBedTypeListData(
        dataForm.group({
          doubleBedCapacity: [2, [Validators.required]],
          reversibleBedCapacity: [1, [Validators.required]],
          singleBedCapacity: [3, [Validators.required]],
          cribBedCapacity: [4, [Validators.required]],
          extraBedCapacity: [5, [Validators.required]],
        }),
      ),
    ).toEqual(bedList);
  }));
});
