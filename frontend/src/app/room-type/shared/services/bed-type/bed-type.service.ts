import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Beds } from 'app/room-type/shared/models/beds';
import { ExtraBeds } from 'app/room-type/shared/models/extra-beds';
import { BedType, BedTypeEnum } from '../../../../shared/models/bed-type';

@Injectable({ providedIn: 'root' })
export class BedTypeService {
  public setBedTypeCount(listWithBedTypeList: Array<any>) {
    return listWithBedTypeList.forEach(roomType => {
      const bedTypes = roomType.roomTypeBedTypeList;

      roomType.bedSingleCount = 0;
      roomType.bedDoubleCount = 0;
      roomType.bedReversibleCount = 0;
      roomType.bedExtraCount = 0;
      roomType.bedCribCount = 0;

      this.setBedTypes(roomType, bedTypes);
    });
  }

  public setBedTypeCountForRooms(rooms: Array<any>) {
    return rooms.forEach(room => {
      const bedTypes = room.roomType.roomTypeBedTypeList;

      room.bedSingleCount = 0;
      room.bedDoubleCount = 0;
      room.bedReversibleCount = 0;
      room.bedExtraCount = 0;
      room.bedCribCount = 0;

      this.setBedTypes(room, bedTypes);
    });
  }

  private setBedTypes(room: any, bedTypes: Array<any>) {
    bedTypes.forEach(bed => {
      if (bed.bedType == BedTypeEnum.Single) {
        room.bedSingleCount = bed.count;
        room.bedExtraCount = bed.optionalLimit;
      }
      if (bed.bedType == BedTypeEnum.Double) {
        room.bedDoubleCount = bed.count;
      }
      if (bed.bedType == BedTypeEnum.Reversible) {
        room.bedReversibleCount = bed.count;
      }
      if (bed.bedType == BedTypeEnum.Crib) {
        room.bedCribCount = bed.optionalLimit;
      }
    });
  }

  public setBedTypeListData(form: AbstractControl) {
    const bedList: Array<BedType> = [];

    if (form.get('singleBedCapacity').value > 0 || form.get('extraBedCapacity').value > 0) {
      const bedSingle = new BedType();
      bedSingle.count = form.get('singleBedCapacity').value;
      bedSingle.bedType = BedTypeEnum.Single;
      bedSingle.optionalLimit = form.get('extraBedCapacity').value;
      bedList.push(bedSingle);
    }

    if (form.get('doubleBedCapacity').value > 0) {
      const bedDouble = new BedType();
      bedDouble.count = form.get('doubleBedCapacity').value;
      bedDouble.bedType = BedTypeEnum.Double;
      bedDouble.optionalLimit = 0;
      bedList.push(bedDouble);
    }

    if (form.get('reversibleBedCapacity').value > 0) {
      const bedReversible = new BedType();
      bedReversible.count = form.get('reversibleBedCapacity').value;
      bedReversible.bedType = BedTypeEnum.Reversible;
      bedReversible.optionalLimit = 0;
      bedList.push(bedReversible);
    }

    if (form.get('cribBedCapacity').value > 0) {
      const bedCrib = new BedType();
      bedCrib.count = 0;
      bedCrib.bedType = BedTypeEnum.Crib;
      bedCrib.optionalLimit = form.get('cribBedCapacity').value;
      bedList.push(bedCrib);
    }

    return bedList;
  }

  public getBedsTypeList(
    {singleBeds, doubleBeds, reversibleBeds}: Beds,
    {extraBeds, cribs}: ExtraBeds
  ): BedType[] {
    const getBedType = (count: number, bedType: BedTypeEnum, optionalLimit: number = 0): BedType => {
      return { count, bedType, optionalLimit };
    };

    const hasSingleBeds = singleBeds > 0 || extraBeds > 0;
    const hasDoubleBeds = doubleBeds > 0;
    const hasReversibleBeds = reversibleBeds > 0;
    const hasCribs = cribs > 0;

    return [
      ...(hasSingleBeds ? [getBedType(singleBeds, BedTypeEnum.Single, extraBeds)] : []),
      ...(hasDoubleBeds ? [getBedType(doubleBeds, BedTypeEnum.Double, 0)] : []),
      ...(hasReversibleBeds ? [getBedType(reversibleBeds, BedTypeEnum.Reversible, 0)] : []),
      ...(hasCribs ? [getBedType(0, BedTypeEnum.Crib, cribs)] : []),
    ];
  }

  public getInfoBedType(bedType) {
    switch (bedType) {
      case BedTypeEnum.Single || BedTypeEnum.Extra:
        return {
          src: 'camaSolteiro.min.svg',
          alt: 'commomData.images.singleBed',
          translate: 'commomData.images.singleBed'
        };
      case BedTypeEnum.Double:
        return {
          src: 'camaCasal.min.svg',
          alt: 'commomData.images.doubleBed',
          translate: 'commomData.images.doubleBed'
        };
      case BedTypeEnum.Crib:
        return {
          src: 'berco.min.svg',
          alt: 'commomData.images.cribBed',
          translate: 'commomData.images.cribBed'
        };
      case BedTypeEnum.Reversible:
        return {
          src: 'camaReversivel.min.svg',
          alt: 'commomData.images.reversibleBed',
          translate: 'commomData.images.reversibleBed'
        };
    }
  }
}
