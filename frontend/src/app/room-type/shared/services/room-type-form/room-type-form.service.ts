import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RoomType } from 'app/room-type/shared/models/room-type';
import { BedTypeService } from 'app/room-type/shared/services/bed-type/bed-type.service';
import { bedRequiredValidator } from 'app/room-type/shared/validators/bed-required-validator';
import { greaterMaximumRateValidator } from 'app/room-type/shared/validators/greater-maximum-rate-validator';
import { NoWhitespaceValidator } from 'app/shared/validators/no-whitespace-validator';


@Injectable({ providedIn: 'root' })
export class RoomTypeFormService {
  public form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private bedType: BedTypeService,
  ) {
    this.form = this.getForm();
  }

  public addCourtesy(quantity: number = 0) {
    const courtesy = this.formBuilder.control(quantity, [Validators.min(0)]);

    this.courtesy.push(courtesy);
  }

  public getRoomType(propertyId: string, roomType: RoomType = null): RoomType {
    const { acronym, description, courtesy, rate, capacity, beds, extras } = this.form.value;

    return {
      ...roomType,
      propertyId,
      order: 0,
      abbreviation: acronym,
      name: description,
      adultCapacity: capacity.adults,
      childCapacity: capacity.children,
      maximumRate: rate.maximumRate,
      minimumRate: rate.minimumRate,
      roomTypeBedTypeList: this.bedType.getBedsTypeList(beds, extras),
      ...this.getRoomTypeCourtesy(courtesy),
    };
  }

  private get courtesy(): FormArray {
    return this.form.get('courtesy') as FormArray;
  }

  private getForm(): FormGroup {
    return this.formBuilder.group({
      acronym: [null, [Validators.required, Validators.maxLength(5), NoWhitespaceValidator.match]],
      description: [null, [Validators.required, NoWhitespaceValidator.match]],
      courtesy: this.formBuilder.array([]),
      rate: this.getRateGroup(),
      capacity: this.getCapacityGroup(),
      beds: this.getBedsGroup(),
      extras: this.getExtrasGroup(),
    });
  }

  private getRateGroup() {
    return this.formBuilder.group({
      minimumRate: [null, [Validators.required, Validators.min(0)]],
      maximumRate: [null, [Validators.required, Validators.min(0)]],
    }, {
      validator: [greaterMaximumRateValidator]
    });
  }

  private getCapacityGroup() {
    return this.formBuilder.group({
      adults: [0, [Validators.required, Validators.min(1)]],
      children: [0, [Validators.min(0)]],
    });
  }

  private getBedsGroup() {
    return this.formBuilder.group({
      doubleBeds: [0, [Validators.min(0)]],
      reversibleBeds: [0, [Validators.min(0)]],
      singleBeds: [0, [Validators.min(0)]],
    }, {
      validator: [bedRequiredValidator]
    });
  }

  private getExtrasGroup() {
    return this.formBuilder.group({
      cribs: [0, [Validators.min(0)]],
      extraBeds: [0, [Validators.min(0)]],
    });
  }

  private getRoomTypeCourtesy(courtesy: any[]) {
    return courtesy.reduce((list, item, index) => {
      return {
        ...list,
        [`freeChildQuantity${index + 1}`]: item
      };
    }, {});
  }
}
