import { TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RoomTypeFormService } from 'app/room-type/shared/services/room-type-form/room-type-form.service';
import { configureTestSuite } from 'ng-bullet';

describe('RoomTypeFormService', () => {
  let service: RoomTypeFormService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [RoomTypeFormService, FormBuilder],
    });

    service = TestBed.get(RoomTypeFormService);
  });
});
