import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { RoomType } from '../../models/room-type';
import { ItemsResponse } from '../../../../shared/models/backend-api/item-response';
import { ChildrenAgeRange } from '../../models/children-age-range';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class RoomTypeResource {
  private urlRoomTypePost = 'roomtypes';
  private urlRoomTypeUpdateAndDelete = 'roomtypes/id';
  private urlRoomTypePatch = 'roomtypes/roomTypeId/toggleactivation';
  private urlRoomTypeGet = 'roomtypes/id?Expand=RoomTypeBedTypeList';
  private urlPropertyRoomType = 'properties/propertyId/roomtypes';
  private urlPropertyRoomTypeWithOverBooking = 'roomtypes/propertyId/withoverbooking';

  constructor(private httpClient: ThexApiService, private sharedService: SharedService) {}

  getRoomTypesByPropertyId(propertyId: number, all = false): Observable<RoomType[]> {
    let params = new HttpParams();
    params = params.append('All', all.toString());
    return this.httpClient
      .get<ItemsResponse<RoomType>>(this.urlPropertyRoomType.replace('propertyId', propertyId.toString()), {params})
      .pipe(map(response => response.items));
  }

  getRoomTypesWithOverBookingByPropertyId(
    propertyId: number,
    initialDate: string,
    finalDate: string,
    propertyIsActive: boolean,
  ): Observable<RoomType[]> {
    return this.httpClient
      .get<ItemsResponse<RoomType>>(
        this.urlPropertyRoomTypeWithOverBooking.replace('propertyId', propertyId.toString()) +
        '?InitialDate=' +
        initialDate +
        '&FinalDate=' +
        finalDate +
        '&IsActive=' +
        propertyIsActive +
        '&IsReservation=true' // this method is only call by Reservation
      ).pipe(
        map(response => response.items));
  }

  getRoomTypeByRoomTypeId(roomTypeId: number): Observable<RoomType> {
    return this.httpClient.get<RoomType>(this.urlRoomTypeGet.replace('id', roomTypeId.toString()));
  }

  createRoomType(roomType: RoomType): Observable<RoomType> {
    return this.httpClient.post<any>(this.urlRoomTypePost, roomType).pipe(map(res => res.data as RoomType));
  }

  editRoomType(roomType: RoomType): Observable<RoomType> {
    return this.httpClient.put<RoomType>(this.urlRoomTypeUpdateAndDelete.replace('id', roomType.id.toString()), roomType);
  }

  deleteRoomType(id: number): Observable<any> {
    return this.httpClient.delete(this.urlRoomTypeUpdateAndDelete.replace('id', id.toString()));
  }

  updateRoomTypeStatusById(roomTypeId: number): Observable<any> {
    return this.httpClient.patch(this.urlRoomTypePatch.replace('roomTypeId', roomTypeId.toString()), null);
  }

  public getAllChildrenAgeRangeList(propertyId: number): Observable<ItemsResponse<ChildrenAgeRange>> {
    return this.httpClient.get<ItemsResponse<ChildrenAgeRange>>(
      `PropertyParameter/${propertyId}/propertyparametersagelist`,
    );
  }
}
