import { ItemsResponse } from '@inovacaocmnet/thx-bifrost';
import { ChildrenAgeRange } from 'app/room-type/shared/models/children-age-range';
import { RoomType } from 'app/room-type/shared/models/room-type';
import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../../testing';


export const roomTypeResourceStub = createServiceStub(RoomTypeResource, {
    createRoomType(roomType: RoomType): Observable<RoomType> {
        return of(null);
    },

    deleteRoomType(id: number): Observable<any> {
        return of(null);
    },

    editRoomType(roomType: RoomType): Observable<RoomType> {
        return of(null);
    },

    getAllChildrenAgeRangeList(propertyId: number): Observable<ItemsResponse<ChildrenAgeRange>> {
        return of(null);
    },

    getRoomTypeByRoomTypeId(roomTypeId: number): Observable<RoomType> {
        return of(null);
    },

    getRoomTypesWithOverBookingByPropertyId(
        propertyId: number, initialDate: string, finalDate: string, propertyIsActive: boolean): Observable<RoomType[]> {
        return of(null);
    },

    getRoomTypesByPropertyId(propertyId: number, all: boolean = false): Observable<RoomType[]> {
        return of(null);
    },

    updateRoomTypeStatusById(roomTypeId: number): Observable<any> {
        return of(null);
    }
});
