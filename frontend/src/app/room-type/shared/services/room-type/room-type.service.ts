import { Injectable } from '@angular/core';
import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { RoomType } from '../../models/room-type';
import { BedType } from '../../../../shared/models/bed-type';

@Injectable({ providedIn: 'root' })
export class RoomTypeService {
  public hasSomeAccommodationIncludedCapacity(form: any) {
    return (
      form.accommodationGroup.doubleBedCapacity +
        form.accommodationGroup.reversibleBedCapacity +
        form.accommodationGroup.singleBedCapacity <=
        0 ||
      form.accommodationGroup.doubleBedCapacity == null ||
      form.accommodationGroup.reversibleBedCapacity == null ||
      form.accommodationGroup.singleBedCapacity == null
    );
  }

  public hasChangeSomeAccommodationIncludedCapacity(form: FormGroup) {
    return (
      form.get('accommodationGroup.doubleBedCapacity').pristine ||
      form.get('accommodationGroup.reversibleBedCapacity').pristine ||
      form.get('accommodationGroup.singleBedCapacity').pristine
    );
  }

  public getRoomTypesBySearchData(searchData: string, allItems: Array<RoomType>): Array<RoomType> {
    return allItems.filter(
      item =>
        item.name.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.abbreviation.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.order
          .toString()
          .toLocaleLowerCase()
          .search(searchData.toLocaleLowerCase()) > -1,
    );
  }

  public setErrorValidatorAccommodationIncluded(form: FormGroup) {
    form.get('accommodationGroup.doubleBedCapacity').setErrors({ required: true });
    form.get('accommodationGroup.reversibleBedCapacity').setErrors({ required: true });
    form.get('accommodationGroup.singleBedCapacity').setErrors({ required: true });
  }

  public setTrueValidatorAccommodationIncluded(form: FormGroup) {
    form.get('accommodationGroup.doubleBedCapacity').setErrors(null);
    form.get('accommodationGroup.reversibleBedCapacity').setErrors(null);
    form.get('accommodationGroup.singleBedCapacity').setErrors(null);
  }

  public getQuantityBedList(roomType: RoomType, bedType: any): any {
    const quantityBedType = this.getQuantityBedType(roomType, bedType);
    if (quantityBedType) {
      return quantityBedType.count;
    }
    return 0;
  }

  private getQuantityBedType(roomType: RoomType, bedType: any): any {
    return roomType.roomTypeBedTypeList.find(bed => bed.count > 0 && bed.bedType == bedType);
  }

  public getQuantityOptionalBedList(roomType: RoomType, bedType: any): any {
    const quantityoptionalBedType = this.getQuantityOptionalBedType(roomType, bedType);
    if (quantityoptionalBedType) {
      return quantityoptionalBedType.optionalLimit;
    }
    return 0;
  }

  public getQuantityOptionalBedType(roomType: RoomType, bedType: any): BedType {
    return roomType.roomTypeBedTypeList.find(bed => bed.optionalLimit > 0 && bed.bedType == bedType);
  }

  public hasRoomTypeAtRoomTypeList(roomTypeList: Array<RoomType>, roomType: RoomType) {
    return roomTypeList.find(roomTypeOfList => roomTypeOfList.id === roomType.id);
  }

  public freeChildQuantityIsGreaterThanChildCapacity(form: AbstractControl): boolean {
    const childCapacity = form.get('accommodationGroup.childCapacity').value;
    const formArray = form.get('ranges') as FormArray;
    let childQuantityRangeIsGreater = false;
    formArray.controls.forEach(control => {
      if (control.get('freeChildQuantity').value > childCapacity) {
        childQuantityRangeIsGreater = true;
      }
    });
    return childQuantityRangeIsGreater;
  }

  public maximumRateIsLessThanMinimumRate(form: AbstractControl): boolean {
    return form.get('basicGroup.maximumRate').value < form.get('basicGroup.minimumRate').value;
  }

  public hasOverbooking(roomTypes: RoomType[]) {
    if (roomTypes) {
      return roomTypes.some(roomType => roomType.overbooking);
    }
    return false;
  }
}
