import { inject, TestBed } from '@angular/core/testing';
import { RoomType } from '../../models/room-type';
import { BedType, BedTypeEnum } from '../../../../shared/models/bed-type';
import { RoomTypeLuxoData, RoomTypeStandardData } from '../../../../shared/mock-data/room-type.data';
import { RoomTypeService } from './room-type.service';

describe('RoomTypeService', () => {
  let service: RoomTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  beforeEach(inject([RoomTypeService], (roomTypeService: RoomTypeService) => {
    service = roomTypeService;
  }));

  describe('Get room types by search data ', () => {
    const roomType = RoomTypeStandardData;

    const singleBedType = new BedType();
    singleBedType.bedType = BedTypeEnum.Single;
    singleBedType.count = 3;
    const extraBedType = new BedType();
    extraBedType.bedType = BedTypeEnum.Single;
    extraBedType.optionalLimit = 4;
    const doubleBedType = new BedType();
    doubleBedType.bedType = BedTypeEnum.Double;
    doubleBedType.count = 1;
    const cribBedType = new BedType();
    cribBedType.bedType = BedTypeEnum.Crib;
    cribBedType.optionalLimit = 2;
    const reversibleBedType = new BedType();
    reversibleBedType.bedType = BedTypeEnum.Reversible;
    reversibleBedType.count = 6;

    roomType.roomTypeBedTypeList = new Array<BedType>();
    roomType.roomTypeBedTypeList.push(doubleBedType);
    roomType.roomTypeBedTypeList.push(reversibleBedType);
    roomType.roomTypeBedTypeList.push(singleBedType);
    roomType.roomTypeBedTypeList.push(cribBedType);
    roomType.roomTypeBedTypeList.push(extraBedType);

    const roomTypes = new Array<RoomType>();
    roomTypes.push(roomType);

    it('should get room types by abbreviation', () => {
      const searchData = roomType.abbreviation;
      const roomTypesSearched = service.getRoomTypesBySearchData(searchData, roomTypes);
      expect(roomTypesSearched[0].abbreviation).toEqual(roomType.abbreviation);
    });

    it('should get room types by name', () => {
      const searchData = roomType.name;
      const roomTypesSearched = service.getRoomTypesBySearchData(searchData, roomTypes);
      expect(roomTypesSearched[0].abbreviation).toEqual(roomType.abbreviation);
    });

    it('should get room types by order', () => {
      const searchData = roomType.order.toString();
      const roomTypesSearched = service.getRoomTypesBySearchData(searchData, roomTypes);
      expect(roomTypesSearched[0].abbreviation).toEqual(roomType.abbreviation);
    });

    it('should not get room types when the search data does not match in abbreviation, name, order', () => {
      const searchData = 'Does not exist';
      const roomTypesSearched = service.getRoomTypesBySearchData(searchData, roomTypes);
      expect(roomTypesSearched.length).toEqual(0);
    });

    it('should return 3 Singles bed from bed list in RoomType', () => {
      expect(service.getQuantityBedList(roomType, BedTypeEnum.Single)).toEqual(3);
    });

    it('should return 4 Extras bed from bed list in RoomType', () => {
      expect(service.getQuantityOptionalBedList(roomType, BedTypeEnum.Single)).toEqual(4);
    });

    it('should return 1 Double bed from bed list in RoomType', () => {
      expect(service.getQuantityBedList(roomType, BedTypeEnum.Double)).toEqual(1);
    });

    it('should return 1 Crib bed from bed list in RoomType', () => {
      expect(service.getQuantityOptionalBedList(roomType, BedTypeEnum.Crib)).toEqual(2);
    });

    it('should return 6 Reversible bed from bed list in RoomType', () => {
      expect(service.getQuantityBedList(roomType, BedTypeEnum.Reversible)).toEqual(6);
    });
  });

  describe('on BedType', () => {
    it('should getQuantityoptionalBedType', () => {
      const cribBedType = new BedType();
      cribBedType.bedType = BedTypeEnum.Crib;
      cribBedType.optionalLimit = 2;
      cribBedType.count = 0;
      const roomType = new RoomType();
      roomType.roomTypeBedTypeList = new Array<BedType>();
      roomType.roomTypeBedTypeList.push(cribBedType);

      const roomTypeWithCrib = new BedType();
      roomTypeWithCrib.bedType = BedTypeEnum.Crib;
      roomTypeWithCrib.optionalLimit = 2;
      roomTypeWithCrib.count = 0;

      expect(service.getQuantityOptionalBedType(roomType, BedTypeEnum.Crib)).toEqual(roomTypeWithCrib);
    });
  });

  describe('on find roomType', () => {
    it('should return true when has roomtype in roomTypeList', () => {
      const roomTypes = new Array<RoomType>();
      roomTypes.push(RoomTypeStandardData);

      expect(service.hasRoomTypeAtRoomTypeList(roomTypes, RoomTypeStandardData)).toBeTruthy();
    });

    it('should return false when has not roomtype in roomTypeList', () => {
      const roomTypes = new Array<RoomType>();

      roomTypes.push(RoomTypeLuxoData);

      expect(service.hasRoomTypeAtRoomTypeList(roomTypes, RoomTypeStandardData)).toBeFalsy();
    });
  });
});
