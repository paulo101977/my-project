import { Injectable } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { RoomTypeView } from '../../../../shared/models/room-type-view';
import { RoomType } from '../../models/room-type';
import { BedTypeService } from '../bed-type/bed-type.service';

@Injectable({ providedIn: 'root' })
export class RoomTypeMapper {
  constructor(private bedTypeService: BedTypeService) {}

  public mapRoomTypeListToRoomTypeViewList(roomTypeList: Array<RoomType>): Array<RoomTypeView> {
    const roomTypeViewList = new Array<RoomTypeView>();
    for (const roomType of roomTypeList) {
      const roomTypeView = new RoomTypeView();
      roomTypeView.id = roomType.id;
      roomTypeView.order = roomType.order;
      roomTypeView.name = roomType.name;
      roomTypeView.abbreviation = roomType.abbreviation;
      roomTypeView.adultCapacity = roomType.adultCapacity;
      roomTypeView.childCapacity = roomType.childCapacity;
      roomTypeView.isActive = roomType.isActive;
      roomTypeView.isInUse = roomType.isInUse;
      roomTypeView.roomTypeBedTypeList = roomType.roomTypeBedTypeList;

      roomTypeViewList.push(roomTypeView);
    }

    return roomTypeViewList;
  }

  public formGroupToRoomType(form: FormGroup, propertyId: number, roomType: RoomType = null): RoomType {
    if (!roomType) {
      roomType = new RoomType();
      roomType.isActive = true;
    }
    roomType.name = form.get('basicGroup.description').value;
    roomType.abbreviation = form.get('basicGroup.initials').value;
    roomType.order = form.get('basicGroup.order').value;
    roomType.adultCapacity = form.get('accommodationGroup.adultCapacity').value;
    roomType.childCapacity = form.get('accommodationGroup.childCapacity').value;
    roomType.propertyId = propertyId;
    roomType.distributionCode = form.get('basicGroup.distributionCode').value;
    roomType.minimumRate = form.get('basicGroup.minimumRate').value;
    roomType.maximumRate = form.get('basicGroup.maximumRate').value;
    roomType.roomTypeBedTypeList = this.bedTypeService.setBedTypeListData(form.get('accommodationGroup'));

    const formArray = form.get('ranges') as FormArray;
    formArray.controls.forEach((age, index) => {
      roomType['freeChildQuantity' + (index + 1)] = formArray.controls[index].get('freeChildQuantity').value;
    });
    return roomType;
  }
}
