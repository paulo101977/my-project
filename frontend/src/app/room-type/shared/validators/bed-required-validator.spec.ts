import { FormControl, FormGroup } from '@angular/forms';
import { bedRequiredValidator } from 'app/room-type/shared/validators/bed-required-validator';

describe('bedRequiredValidator', () => {
  const bedGroup = new FormGroup({
    doubleBeds: new FormControl(0),
    reversibleBeds: new FormControl(0),
    singleBeds: new FormControl(0),
  });

  beforeEach(() => {
    bedGroup.reset({
      doubleBeds: 0,
      reversibleBeds: 0,
      singleBeds: 0,
    });
  });

  it('should not return an error if doubleBeds has value', () => {
    bedGroup.patchValue({doubleBeds: 1});
    const error = bedRequiredValidator(bedGroup);
    expect(error).toBeNull();
  });

  it('should not return an error if reversibleBeds has value', () => {
    bedGroup.patchValue({reversibleBeds: 1});
    const error = bedRequiredValidator(bedGroup);
    expect(error).toBeNull();
  });

  it('should not return an error if singleBeds has value', () => {
    bedGroup.patchValue({singleBeds: 1});
    const error = bedRequiredValidator(bedGroup);
    expect(error).toBeNull();
  });

  it('should return an error if there are no beds', () => {
    const error = bedRequiredValidator(bedGroup);
    expect(error).toEqual({ bedRequired: 'error.bedRequired' });
  });
});
