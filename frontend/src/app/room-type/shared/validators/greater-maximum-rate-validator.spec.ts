import { FormControl, FormGroup } from '@angular/forms';
import { bedRequiredValidator } from 'app/room-type/shared/validators/bed-required-validator';

describe('greaterMaximumRateValidator', () => {
  const rateGroup = new FormGroup({
    minimumRate: new FormControl(0),
    maximumRate: new FormControl(0),
  });

  beforeEach(() => {
    rateGroup.reset({
      minimumRate: 0,
      maximumRate: 0,
    });
  });

  it('should return an error if minimumRate is greater maximumRate', () => {
    rateGroup.patchValue({minimumRate: 1});
    const error = bedRequiredValidator(rateGroup);
    expect(error).toBeNull();
  });

  it('should not return an error if minimumRate is equal maximumRate', () => {
    const error = bedRequiredValidator(rateGroup);
    expect(error).toBeNull();
  });

  it('should not return an error if minimumRate is lower than maximumRate', () => {
    rateGroup.patchValue({maximumRate: 1});
    const error = bedRequiredValidator(rateGroup);
    expect(error).toBeNull();
  });
});
