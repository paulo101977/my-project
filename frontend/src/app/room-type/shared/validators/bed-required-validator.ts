import { FormGroup, ValidatorFn } from '@angular/forms';

export const bedRequiredValidator: ValidatorFn = (bedGroup: FormGroup) => {
  const { doubleBeds, reversibleBeds, singleBeds } = bedGroup.value;
  const beds = [doubleBeds, reversibleBeds, singleBeds].map(quantity => parseInt(quantity, 10));

  return beds.every(quantity => quantity === 0)
    ? { bedRequired: 'error.bedRequired' }
    : null;
};
