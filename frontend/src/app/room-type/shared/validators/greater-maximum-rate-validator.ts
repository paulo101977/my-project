import { FormGroup, ValidatorFn } from '@angular/forms';

export const greaterMaximumRateValidator: ValidatorFn = (rateGroup: FormGroup) => {
  let { minimumRate, maximumRate } = rateGroup.value;

  if (
    (minimumRate == null || minimumRate == '')
    || (maximumRate == null || maximumRate == '')
  ) {
    return null;
  }

  minimumRate = parseInt(minimumRate, 10);
  maximumRate = parseInt(maximumRate, 10);

  return minimumRate <= maximumRate ? null : {greaterMaximumRate: 'error.greaterMaximumRate'};
};
