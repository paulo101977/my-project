export interface Beds {
  singleBeds: number;
  doubleBeds: number;
  reversibleBeds: number;
}
