// Models
import { BedType } from '../../../shared/models/bed-type';
import { Room } from '../../../shared/models/room';
import { Property } from '../../../shared/models/property';

export class RoomType {
  id: number;
  name: string;
  abbreviation: string;
  order: number;
  adultCapacity: number;
  childCapacity: number;
  roomTypeBedTypeList: BedType[];
  property: Property;
  propertyId: number;
  isActive: boolean;
  isInUse: boolean;
  roomList: Array<Room>;
  maximumRate: number;
  minimumRate: number;
  distributionCode: string;
  freeChildQuantity1: number;
  freeChildQuantity2: number;
  freeChildQuantity3: number;
  overbooking: boolean;

  constructor() {
    this.roomTypeBedTypeList = new Array<BedType>();
  }
}
