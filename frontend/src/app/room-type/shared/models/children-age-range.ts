export class ChildrenAgeRange {
  public parameterDescription: string;
  public propertyParameterMinValue: string;
  public propertyParameterMaxValue: string;
  public freeChildQuantity: number;
}
