import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoomTypeListComponent } from './components/room-type-list/room-type-list.component';
import { RoomTypeCreateComponent } from './components/room-type-create/room-type-create.component';
import { RoomTypeEditComponent } from './components/room-type-edit/room-type-edit.component';

export const roomTypeRoutes: Routes = [
  { path: '', component: RoomTypeListComponent },
  { path: 'new', component: RoomTypeCreateComponent },
  { path: 'edit/:id', component: RoomTypeEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(roomTypeRoutes)],
  exports: [RouterModule],
})
export class RoomTypeRoutingModule {}
