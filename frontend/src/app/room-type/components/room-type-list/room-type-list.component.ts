import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { DataTableGlobals } from '../../../shared/models/DataTableGlobals';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { RoomTypeView } from '../../../shared/models/room-type-view';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { Column } from '../../../shared/models/table/column';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { RoomType } from '../../shared/models/room-type';
import { BedTypeService } from '../../shared/services/bed-type/bed-type.service';
import { RoomTypeMapper } from '../../shared/services/room-type-mapper/room-type-mapper';

@Component({
  selector: 'app-room-type-list',
  templateUrl: './room-type-list.component.html',
})
export class RoomTypeListComponent implements OnInit {
  public tableRowTypeEnum: TableRowTypeEnum;
  public configHeaderPage: ConfigHeaderPageNew;
  public hasRoomTypes: boolean;
  public columns: Array<Column>;
  public allRoomTypeViewList: Array<RoomTypeView>;
  public roomTypeViewList: Array<RoomTypeView>;
  public searchableItems: SearchableItems;
  public roomTypeList: Array<RoomType>;
  private propertyId: any;
  public quantityOfItemsToDisplayPerPage: number;

  constructor(
    public translateService: TranslateService,
    public _route: ActivatedRoute,
    public route: Router,
    public roomTypeResource: RoomTypeResource,
    public roomTypeMapper: RoomTypeMapper,
    private toasterEmitService: ToasterEmitService,
    public bedTypeService: BedTypeService,
    private dataTableGlobals: DataTableGlobals,
  ) {}

  ngOnInit() {
    this.setVars();
    this.getRoomTypesRegistered();
  }

  setVars() {
    this._route.params.subscribe(params => {
      this.propertyId = +params['property'];
    });
    this.quantityOfItemsToDisplayPerPage = 10;
    this.hasRoomTypes = false;
    this.tableRowTypeEnum = TableRowTypeEnum.RoomType;
    this.setConfigHeaderPage();
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      keepButtonActive: true,
      hasBackPreviewPage: true,
      callBackFunction: this.goToCreatePage,
    };
  }

  public goToCreatePage = () => {
    this.route.navigate(['new'], { relativeTo: this._route });
  }

  public goEditRoomTypePage(roomTypeSelected) {
    this.route.navigate(['edit', roomTypeSelected.id], { relativeTo: this._route });
  }

  private getRoomTypesRegistered() {
    this.roomTypeResource.getRoomTypesByPropertyId(this.propertyId, true).subscribe(roomTypes => {
      this.allRoomTypeViewList = this.roomTypeMapper.mapRoomTypeListToRoomTypeViewList(roomTypes);
      this.setSearchableItems();

      this.hasRoomTypes = roomTypes.length > 0 ? true : false;
      this.bedTypeService.setBedTypeCount(this.allRoomTypeViewList);
      this.showAllRoomTypeViewListInView();
      this.setColumnsName();
    });
  }

  private setSearchableItems() {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.allRoomTypeViewList;
    this.searchableItems.tableRowTypeEnum = TableRowTypeEnum.RoomType;
    this.searchableItems.placeholderSearchFilter = 'hotelPanelModule.roomTypeList.filterSearchPlaceholder';
  }

  private setColumnsName() {
    this.columns = new Array<Column>();
    this.columns.push(new Column(this.translateService.instant('commomData.tableColumns.active'), 1, 'center'));
    this.columns.push(new Column(this.translateService.instant('commomData.tableColumns.abbreviation'), 1));
    this.columns.push(new Column(this.translateService.instant('commomData.tableColumns.description'), 2));
    this.columns.push(new Column(this.translateService.instant('hotelPanelModule.roomTypeList.order'), 1));
    this.columns.push(new Column(this.translateService.instant('hotelPanelModule.roomTypeList.capacity'), 2, 'center'));
    this.columns.push(new Column(this.translateService.instant('commomData.tableColumns.bedsQuantity'), 2, 'center'));
    this.columns.push(new Column(this.translateService.instant('hotelPanelModule.roomTypeList.extrasQuantity'), 2, 'center'));
    this.columns.push(new Column('', 1));
  }

  private showAllRoomTypeViewListInView() {
    this.roomTypeViewList = this.allRoomTypeViewList;
  }

  public updateRoomTypeViewList(items: Array<any>) {
    if (items != undefined) {
      this.hasRoomTypes = items.length > 0 ? true : false;
      this.roomTypeViewList = items;
    }
  }

  public updateRoomTypeStatus(roomType: RoomTypeView) {
    if (roomType.isInUse) {
      if (roomType.isActive) {
        this.toasterEmitService.emitChange(
          SuccessError.error,
          this.translateService.instant('hotelPanelModule.roomTypeList.roomTypeInactiveIsInUse'),
        );
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.error,
          this.translateService.instant('hotelPanelModule.roomTypeList.roomTypeActiveIsInUse'),
        );
      }
      return;
    }
    this.roomTypeResource.updateRoomTypeStatusById(roomType.id).subscribe(result => {
      if (roomType.isActive) {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('hotelPanelModule.roomTypeList.roomTypeEnabledWithSuccess'),
        );
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('hotelPanelModule.roomTypeList.roomTypeInactivatedWithSuccess'),
        );
      }
    });
  }
}
