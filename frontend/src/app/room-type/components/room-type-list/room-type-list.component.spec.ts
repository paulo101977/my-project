// import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { RouterTestingModule } from '@angular/router/testing';
// import { Http } from '@angular/http';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { TranslateService } from '@ngx-translate/core';
// import { platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
// import { HttpClientModule } from '@angular/common/http';
// import { ActivatedRoute } from '@angular/router';
// import { Observable } from 'rxjs/Observable';
// import { roomTypeRoutes } from '../../room-type-routing.module';
// import { RoomTypeMapper } from '../../../shared/mappers/room-type-mapper';
// import { RoomTypeListComponent } from './room-type-list.component';
// import { HeaderPageComponent } from '../../../shared/components/header-page/header-page.component';
// import { HeaderListPageComponent } from '../../../shared/components/header-list-page/header-list-page.component';
// import { HeaderCreatePageComponent } from '../../../shared/components/header-create-page/header-create-page.component';
// import { HeaderEditPageComponent } from '../../../shared/components/header-edit-page/header-edit-page.component';
// import { TableComponent } from '../../../shared/components/table/table.component';
// import { TableHeaderComponent } from '../../../shared/components/table-header/table-header.component';
// import { TableFooterComponent } from '../../../shared/components/table-footer/table-footer.component';
// import { TableRowRoomTypeComponent } from '../../../shared/components/table-row-room-type/table-row-room-type.component';
// import { TableRowRoomComponent } from '../../../shared/components/table-row-room/table-row-room.component';
// import { FilterSearchComponent } from '../../../shared/components/filter-search/filter-search.component';
// import { TableRowReserveComponent } from '../../../shared/components/table-row-reserve/table-row-reserve.component';
// import { TableRowOriginComponent } from '../../../shared/components/table-row-origin/table-row-origin.component';
// import { TableRowClientComponent } from '../../../shared/components/table-row-client/table-row-client.component';
// import { TableRowClientAddressComponent } from '../../../shared/components/table-row-client-address/table-row-client-address.component';
// import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
// import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
// import { ShowHide } from '../../../shared/models/show-hide-enum';
// import { RoomTypeView } from '../../../shared/models/room-type-view';
// import { RoomTypeViewStandardData } from '../../../shared/mock-data/room-type.data';
// import { Column } from '../../../shared/models/table/column';
// import { RoomTypeStandardData, RoomTypeViewStandardIsInUseAndNotActiveData,
// RoomTypeViewStandardNotInUseDataAndIsActive,
// RoomTypeViewStandardNotInUseDataAndIsNotActive } from '../../../shared/mock-data/room-type.data';
// import { RoomType } from '../../../shared/models/room-type/room-type';
// import { RoomTypeService } from '../../../shared/services/room-type/room-type.service';
// import { RoomService } from '../../../shared/services/room/room.service';
// import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
// import { SharedService } from '../../../shared/services/shared/shared.service';
// import { BedTypeService } from '../../../shared/services/bed-type/bed-type.service';
// import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
// import { ClientService } from '../../../shared/services/client/client.service';
// import { DateService } from '../../../shared/services/shared/date.service';
// import { GuestService } from '../../../shared/services/guest/guest.service';
// import { BillingAccountService } from '../../../shared/services/billing-account/billing-account.service';
// import { BillingEntryTypeService } from '../../../shared/services/billing-entry-type/billing-entry-type.service';
// import { HousekeepingStatusPropertyService } from '../../../shared/services/governance/housekeeping-status-property.service';
// import { RoomTypeResource } from '../../../shared/resources/room-type/room-type.resource';
// import { PersonResource } from '../../../shared/resources/person/person.resource';
// import { appRoutes } from 'app/app-routing.module';
// import { SuccessError } from 'app/shared/models/success-error-enum';
// import { DataTableGlobals } from '../../../shared/models/DataTableGlobals';
//
// xdescribe('RoomTypeListComponent', () => {
//   let component: RoomTypeListComponent;
//   let fixture: ComponentFixture<RoomTypeListComponent>;
//   const propertyId = 1;
//   let promiseRoomType, promiseRoomtypeUpdate;
//   const roomTypeMock = RoomTypeStandardData;
//   const roomTypeList = new Array<RoomType>();
//   const roomTypeViewList = new Array<RoomTypeView>();
//   const roomTypeViewMock = RoomTypeViewStandardData;
//   roomTypeViewList.push(roomTypeViewMock);
//   roomTypeList.push(roomTypeMock);
//
//   beforeAll(() => {
//     TestBed.resetTestEnvironment();
//     TestBed
//       .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
//       .configureTestingModule({
//         imports: [
//
//           FormsModule,
//           ReactiveFormsModule,
//           TranslateModule.forRoot(),
//           RouterTestingModule,
//           HttpClientModule
//         ],
//         declarations: [
//           RoomTypeListComponent,
//           HeaderPageComponent,
//           HeaderListPageComponent,
//           HeaderCreatePageComponent,
//           HeaderEditPageComponent,
//           FilterSearchComponent,
//           TableComponent,
//           TableHeaderComponent,
//           TableFooterComponent,
//           TableRowRoomTypeComponent
//         ],
//         providers: [],
//         schemas: [NO_ERRORS_SCHEMA]
//       });
//
//     fixture = TestBed.createComponent(RoomTypeListComponent);
//     component = fixture.componentInstance;
//
//     spyOn(component.loadPageEmitService, 'emitChange');
//     spyOn(component.roomTypeMapper, 'mapRoomTypeListToRoomTypeViewList').and.returnValue(roomTypeViewList);
//     spyOn(component['bedTypeService'], 'setBedTypeCount');
//     spyOn(component.route, 'navigate');
//     spyOn(component['toasterEmitService'], 'emitChange');
//
//     promiseRoomtypeUpdate = spyOn(component.roomTypeResource, 'updateRoomTypeStatusById');
//     promiseRoomtypeUpdate.and.callFake(() => {
//       return new Promise((resolve, reject) => resolve({}));
//     });
//
//     promiseRoomType = spyOn(component.roomTypeResource, 'getRoomTypesByPropertyId');
//     promiseRoomType.and.callFake(() => {
//       return new Promise((resolve, reject) => resolve(roomTypeList));
//     });
//
//     fixture.detectChanges();
//   });
//
//   describe('on init when request works', () => {
//     it('should set vars', () => {
//       expect(component['propertyId']).toEqual(1);
//       expect(component.quantityOfItemsToDisplayPerPage).toEqual(10);
//       expect(component.tableRowTypeEnum).toEqual(TableRowTypeEnum.RoomType);
//       expect(component.hasRoomTypes).toBeFalsy();
//     });
//
//     it('should call loadpage on list RoomType', fakeAsync(() => {
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);
//
//       tick();
//
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.hide);
//       expect(component.allRoomTypeViewList).toEqual(roomTypeViewList);
//       expect(component.searchableItems.items).toEqual(component.allRoomTypeViewList);
//       expect(component.searchableItems.tableRowTypeEnum).toEqual(TableRowTypeEnum.RoomType);
//       expect(component.searchableItems.placeholderSearchFilter).toEqual('hotelPanelModule.roomTypeList.filterSearchPlaceholder');
//       expect(component.hasRoomTypes).toBeTruthy();
//       expect(component.roomTypeViewList).toEqual(component.allRoomTypeViewList);
//       expect(component.columns).toEqual([
//         new Column('commomData.tableColumns.active', 1, 'center'),
//         new Column('commomData.tableColumns.abbreviation', 1),
//         new Column('commomData.tableColumns.description', 2),
//         new Column('hotelPanelModule.roomTypeList.order', 1),
//         new Column('hotelPanelModule.roomTypeList.capacity', 2, 'center'),
//         new Column('commomData.tableColumns.bedsQuantity', 2, 'center'),
//         new Column('hotelPanelModule.roomTypeList.extrasQuantity', 2, 'center'),
//         new Column('', 1)
//       ]);
//     }));
//   });
//
//   describe('on init when request fails', () => {
//     beforeAll(() => {
//       promiseRoomType.and.callFake(() => {
//         return new Promise((resolve, reject) => reject('Error'));
//       });
//     });
//
//     it('should hide load when resource fails', fakeAsync(() => {
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);
//       spyOn(component, 'setSearchableItems');
//
//       tick();
//
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.hide);
//       expect(component['setSearchableItems']).not.toHaveBeenCalled();
//     }));
//   });
//
//   describe('on methods', () => {
//     it('should goToCreatePage', () => {
//       component.goToCreatePage();
//
//       expect(component.route.navigate).toHaveBeenCalledWith(
//         ['new'], { relativeTo: component._route }
//       );
//     });
//
//     it('should updateRoomTypeViewList', () => {
//       component.updateRoomTypeViewList(roomTypeList);
//
//       expect(component.hasRoomTypes).toBeTruthy();
//       expect(component.roomTypeViewList).toEqual(roomTypeList);
//     });
//   });
//
//   describe('on updateRoomTypeStatus when request works', () => {
//     it('should show toaster error when updateRoomTypeStatus is called and roomtype isInUse also isActive', () => {
//       component.updateRoomTypeStatus(roomTypeViewMock);
//
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.hide);
//       expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
//         SuccessError.error,
//         'hotelPanelModule.roomTypeList.roomTypeInactiveIsInUse'
//       );
//     });
//
//     it('should show toaster error when updateRoomTypeStatus is called. Roomtype isInUse and is not Active', () => {
//       component.updateRoomTypeStatus(RoomTypeViewStandardIsInUseAndNotActiveData);
//
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.hide);
//       expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
//         SuccessError.error,
//         'hotelPanelModule.roomTypeList.roomTypeActiveIsInUse'
//       );
//     });
//
//     it('should updateRoomTypeStatus when Roomtype is not InUse and isActive', fakeAsync(() => {
//       component.updateRoomTypeStatus(RoomTypeViewStandardNotInUseDataAndIsActive);
//
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);
//
//       tick();
//
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.hide);
//       expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
//         SuccessError.success,
//         'hotelPanelModule.roomTypeList.roomTypeEnabledWithSuccess'
//       );
//     }));
//
//     it('should updateRoomTypeStatus when Roomtype is not InUse and is not active', fakeAsync(() => {
//       component.updateRoomTypeStatus(RoomTypeViewStandardNotInUseDataAndIsNotActive);
//
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);
//
//       tick();
//
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.hide);
//       expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
//         SuccessError.success,
//         'hotelPanelModule.roomTypeList.roomTypeInactivatedWithSuccess'
//       );
//     }));
//   });
//
//   describe('on updateRoomTypeStatus when request fails', () => {
//     beforeAll(() => {
//       promiseRoomtypeUpdate.and.callFake(() => {
//         return new Promise((resolve, reject) => reject({}));
//       });
//     });
//
//     it('should updateRoomTypeStatus error', fakeAsync(() => {
//       component.updateRoomTypeStatus(RoomTypeViewStandardNotInUseDataAndIsNotActive);
//
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);
//
//       tick();
//
//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.hide);
//       expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
//         SuccessError.error,
//         'commomData.errorMessage'
//       );
//     }));
//   });
// });
