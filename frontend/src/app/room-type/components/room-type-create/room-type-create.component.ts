import { Location } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { RoomTypeMapper } from 'app/room-type/shared/services/room-type-mapper/room-type-mapper';
import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { RoomTypeService } from '../../shared/services/room-type/room-type.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { ModalCancelActionService } from '../../../shared/services/shared/modal-cancel-action.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ValidatorFormService } from '../../../shared/services/shared/validator-form.service';
import { BedTypeService } from '../../shared/services/bed-type/bed-type.service';
import { ConfigHeaderPageNew } from './../../../shared/components/header-page-new/config-header-page-new';
import { ButtonConfig, ButtonSize, ButtonType } from './../../../shared/models/button-config';
import { ChildrenAgeRange } from '../../shared/models/children-age-range';

@Component({
  selector: 'app-room-type-create',
  templateUrl: './room-type-create.component.html',
  styleUrls: ['./room-type-create.component.css'],
})
export class RoomTypeCreateComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent) myTable: DatatableComponent;

  public configHeaderPage: ConfigHeaderPageNew;
  public propertyId: number;
  public formGroupCreateRoomType: FormGroup;
  public childrenAgeRangeList: Array<ChildrenAgeRange>;
  public optionsCurrencyMask: any;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;

  constructor(
    public translateService: TranslateService,
    private formBuilder: FormBuilder,
    private roomTypeService: RoomTypeService,
    public roomTypeResource: RoomTypeResource,
    public _route: Router,
    public route: ActivatedRoute,
    private toasterEmitService: ToasterEmitService,
    private _location: Location,
    public loadPageEmitService: LoadPageEmitService,
    private bedTypeService: BedTypeService,
    private validatorFormService: ValidatorFormService,
    private modalCancelActionService: ModalCancelActionService,
    private roomTypeMapper: RoomTypeMapper,
    private sharedService: SharedService,
  ) {}

  ngOnInit() {
    this.setVars();
    this.setListeners();
  }

  ngAfterViewInit() {
    this.myTable.rowHeight = 60;
  }

  private setVars() {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
    this.formGroupCreateRoomType = this.formBuilder.group({
      basicGroup: this.formBuilder.group({
        initials: ['', [Validators.required, Validators.maxLength(10), this.validatorFormService.noWhitespace]],
        description: ['', [Validators.required, this.validatorFormService.noWhitespace]],
        order: ['', [Validators.required]],
        distributionCode: '',
        minimumRate: [0, [Validators.required, this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        maximumRate: [0, [Validators.required, this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
      }),
      accommodationGroup: this.formBuilder.group({
        adultCapacity: [
          0,
          [
            Validators.required,
            this.validatorFormService.capacityNumberIsMajorThanOne,
            this.validatorFormService.setNumberToZero,
            this.validatorFormService.notAcceptNegatives,
          ],
        ],
        childCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        doubleBedCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        reversibleBedCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        singleBedCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        cribBedCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        extraBedCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
      }),
      ranges: this.formBuilder.array([]),
    });
    this.propertyId = this.route.snapshot.params.property;
    this.setConfigHeaderPage();
    this.setButtonConfig();
    this.setChildrenAgeRangeList();
  }

  public setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-create-room-type',
      this.modalCallCancel,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
      false,
    );
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'create-room-type',
      this.saveRoomType,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
  }

  private setChildrenAgeRangeList(): void {
    this.childrenAgeRangeList = [];
    this.roomTypeResource.getAllChildrenAgeRangeList(this.propertyId).subscribe(
      response => {
        if (response.items) {
          this.childrenAgeRangeList = [...response.items];
          this.childrenAgeRangeList.forEach(() => {
            this.addFreeChildQuantityInRangesFormArray();
          });
        }
      });
  }

  private createFreeChildQuantityGroup(): FormGroup {
    return this.formBuilder.group({
      freeChildQuantity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
    });
  }

  private addFreeChildQuantityInRangesFormArray(): void {
    const formArray = this.formGroupCreateRoomType.get('ranges') as FormArray;
    formArray.push(this.createFreeChildQuantityGroup());
  }

  private setListeners() {
    this.hasBedCapacityChanges();
    this.bedCapacityListener();
  }

  private hasBedCapacityChanges() {
    if (this.roomTypeService.hasChangeSomeAccommodationIncludedCapacity(this.formGroupCreateRoomType)) {
      this.roomTypeService.setErrorValidatorAccommodationIncluded(this.formGroupCreateRoomType);
    }
  }

  private bedCapacityListener() {
    this.formGroupCreateRoomType.valueChanges.subscribe(createRoomTypeForm => {
      this.buttonSaveConfig.isDisabled = this.formGroupCreateRoomType.invalid;
      if (this.roomTypeService.hasSomeAccommodationIncludedCapacity(createRoomTypeForm)) {
        this.roomTypeService.setErrorValidatorAccommodationIncluded(this.formGroupCreateRoomType);
      } else {
        this.roomTypeService.setTrueValidatorAccommodationIncluded(this.formGroupCreateRoomType);
      }
    });
  }

  private saveRoomType = () => {
    if (this.roomTypeService.freeChildQuantityIsGreaterThanChildCapacity(this.formGroupCreateRoomType)) {
      this.toasterEmitService.emitChange(SuccessError.error, 'text.quantityOfChildrenForCourtesyIsHigherThanAllowed');
      return;
    }
    if (this.roomTypeService.maximumRateIsLessThanMinimumRate(this.formGroupCreateRoomType)) {
      this.toasterEmitService.emitChange(SuccessError.error, 'text.maximumRateCanNotBeLessThanTheMinimum');
      return;
    }
    const roomTypeDataToSave = this.roomTypeMapper.formGroupToRoomType(this.formGroupCreateRoomType, this.propertyId);
    this.roomTypeResource.createRoomType(roomTypeDataToSave).subscribe(roomType => {
      this.formGroupCreateRoomType.reset();
      this.loadPageEmitService.emitChange(ShowHide.hide);
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('hotelPanelModule.roomTypeCreate.createRoomTypeSuccessMessage'),
      );
      this._route.navigate(['../'], { relativeTo: this.route });
    });
  }

  private modalCallCancel = () => {
    this.modalCancelActionService.cancelAction();
  }

  public invalidFreeChildQuantity(index) {
    const freeChildQuantityList = this.formGroupCreateRoomType.get('ranges') as FormArray;
    const control = freeChildQuantityList.at(index);
    if (control) {
      return control.get('freeChildQuantity').dirty
      && control.get('freeChildQuantity').invalid;
    }
    return true;
  }

}
