/*
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormBuilder, FormsModule } from '@angular/forms';
import { Http } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { ErrorMessageHandlerAPIData } from '../../../shared/mock-data/error-message-handler-API';
import { ErrorMessageHandlerAPI } from '../../../shared/models/error-message/error-message-handler-api';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { BedTypeService } from '../../../shared/services/bed-type/bed-type.service';
import { RoomTypeResource } from '../../../shared/resources/room-type/room-type.resource';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { RoomTypeService } from '../../../shared/services/room-type/room-type.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { ValidatorFormService } from '../../../shared/services/shared/validator-form.service';
import { ModalCancelActionService } from '../../../shared/services/shared/modal-cancel-action.service';

// Componets
import { RoomTypeCreateComponent } from './room-type-create.component';
import { HeaderPageComponent } from '../../../shared/components/header-page/header-page.component';
import { HeaderListPageComponent } from '../../../shared/components/header-list-page/header-list-page.component';
import { HeaderCreatePageComponent } from '../../../shared/components/header-create-page/header-create-page.component';
import { HeaderEditPageComponent } from '../../../shared/components/header-edit-page/header-edit-page.component';
import { FilterSearchComponent } from '../../../shared/components/filter-search/filter-search.component';
import { RoomType } from '../../../shared/models/room-type/room-type';
import { ConfigHeaderPage } from '../../../shared/models/config-header-page';
import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
import { ShowHide } from '../../../shared/models/show-hide-enum';

xdescribe('RoomTypeCreateComponent', () => {
  let component: RoomTypeCreateComponent;
  let fixture: ComponentFixture<RoomTypeCreateComponent>;
  let promiseRoomType;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed
    .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
    .configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterModule,

        FormsModule,
        TranslateModule.forRoot(),
        RouterTestingModule
      ],
      declarations: [
        RoomTypeCreateComponent,
        HeaderPageComponent,
        HeaderListPageComponent,
        HeaderCreatePageComponent,
        HeaderEditPageComponent,
        FilterSearchComponent
      ],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(RoomTypeCreateComponent);
    component = fixture.componentInstance;

    spyOn(component.loadPageEmitService, 'emitChange');
    spyOn(component, 'setRoomTypeToSendServer');
    spyOn(component.route, 'navigate' );
    spyOn(component['toasterEmitService'], 'emitChange' );
    component.propertyId = 1;
    promiseRoomType = spyOn(component.roomTypeResource, 'createRoomType');
    promiseRoomType.and.callFake(() => {
      return new Promise((resolve, reject) => resolve({}));
    });
    spyOn(component.route.params, 'subscribe').and.callFake((success) => {
      success({property: 20});
    });

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should setVars', fakeAsync(() => {
      expect(component.formGroupCreateRoomType.get('initials').value).toEqual('');
      expect(component.formGroupCreateRoomType.get('description').value).toEqual('');
      expect(component.formGroupCreateRoomType.get('order').value).toEqual('');
      expect(component.formGroupCreateRoomType.get('adultCapacity').value).toEqual(0);
      expect(component.formGroupCreateRoomType.get('childCapacity').value).toEqual(0);
      expect(component.formGroupCreateRoomType.get('doubleBedCapacity').value).toEqual(0);
      expect(component.formGroupCreateRoomType.get('reversibleBedCapacity').value).toEqual(0);
      expect(component.formGroupCreateRoomType.get('singleBedCapacity').value).toEqual(0);
      expect(component.formGroupCreateRoomType.get('cribBedCapacity').value).toEqual(0);
      expect(component.formGroupCreateRoomType.get('extraBedCapacity').value).toEqual(0);

      tick();

      fixture.whenStable().then(() => {
        expect(component.propertyId).toEqual(20);
      });
    }));

    it('should set configHeaderPage', () => {
      component.setConfigHeaderPage();
      expect(component.configHeaderPage.hasBackPreviewPage).toBeTruthy();
    });
  });

  describe('on methods', () => {
    it('should call loadpage on create RoomType', fakeAsync(() => {
      spyOn(component.formGroupCreateRoomType, 'reset' );
      component['saveRoomType']();

      expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);

      tick();

      fixture.whenStable().then(() => {
        expect(component.formGroupCreateRoomType.reset).toHaveBeenCalled();
        expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.hide);
        expect(component._route.navigate).toHaveBeenCalledWith(['../'], { relativeTo: component['_route'] });
        expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
          SuccessError.success,
          'hotelPanelModule.roomTypeCreate.createRoomTypeSuccessMessage'
        );
      });
    }));

    it('should call modalCancel', () => {
      const modalCancell = component['modalCancelActionService'];
      spyOn(modalCancell, 'cancelAction');
      component['modalCallCancel']();
      expect(modalCancell.cancelAction).toHaveBeenCalled();
    });
  });
});
*/
