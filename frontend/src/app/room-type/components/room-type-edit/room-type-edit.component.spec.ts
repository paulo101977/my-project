/*
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { RoomTypeService } from '../../../shared/services/room-type/room-type.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { BedTypeService } from '../../../shared/services/bed-type/bed-type.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { ValidatorFormService } from '../../../shared/services/shared/validator-form.service';
import { ModalCancelActionService } from '../../../shared/services/shared/modal-cancel-action.service';

// Resouses
import { RoomTypeResource } from '../../../shared/resources/room-type/room-type.resource';
import { RoomTypeEditComponent } from './room-type-edit.component';
import { HeaderPageComponent } from '../../../shared/components/header-page/header-page.component';
import { HeaderListPageComponent } from '../../../shared/components/header-list-page/header-list-page.component';
import { HeaderCreatePageComponent } from '../../../shared/components/header-create-page/header-create-page.component';
import { HeaderEditPageComponent } from '../../../shared/components/header-edit-page/header-edit-page.component';
import { FilterSearchComponent } from '../../../shared/components/filter-search/filter-search.component';
import { RoomType } from '../../../shared/models/room-type/room-type';
import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
import { ShowHide } from '../../../shared/models/show-hide-enum';

xdescribe('RoomTypeEditComponent', () => {
  let component: RoomTypeEditComponent;
  let fixture: ComponentFixture<RoomTypeEditComponent>;
  const roomTypeId = 1;
  let promiseRoomType, promiseDeleteRoomType, promiseRoomTypeEdit;
  const roomTypeMock = new RoomType();
  roomTypeMock.abbreviation = 'STD';
  roomTypeMock.name = 'Standard';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [

        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        RouterTestingModule
      ],
      declarations: [
        RoomTypeEditComponent,
        HeaderPageComponent,
        HeaderListPageComponent,
        HeaderCreatePageComponent,
        HeaderEditPageComponent,
        FilterSearchComponent
      ],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomTypeEditComponent);
    component = fixture.componentInstance;

    spyOn(component.validatorFormService, 'capacityNumberIsMajorThanOne');
    spyOn(component.roomTypeService, 'getQuantityBedList');
    spyOn(component.roomTypeService, 'getQuantityOptionalBedList');
    spyOn(component.route.params, 'subscribe').and.callFake((success) => {
      success({ roomType: '1' });
    });

    promiseRoomType = spyOn(component.roomTypeResource, 'getRoomTypeByRoomTypeId').and.callFake(() => {
      return new Promise((resolve, reject) => resolve(roomTypeMock));
    });

    promiseDeleteRoomType = spyOn(component.roomTypeResource, 'deleteRoomType').and.callFake(() => {
      return new Promise((resolve, reject) => resolve(roomTypeMock));
    });

    promiseRoomTypeEdit = spyOn(component.roomTypeResource, 'editRoomType').and.callFake(() => {
      return new Promise((resolve, reject) => resolve());
    });

    fixture.detectChanges();
  });

  it('should create RoomTypeEditComponent', fakeAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should set configHeaderPage', fakeAsync(() => {
    component['roomType'] = new RoomType();
    component['roomType'].abbreviation = 'STD';

    component['setConfigHeaderPage']();

    expect(component.configHeaderPage.hasBackPreviewPage).toBeTruthy();
  }));

  it('should call loadpage on delete RoomType', fakeAsync(() => {
    spyOn(component.loadPageEmitService, 'emitChange');
    spyOn(component.route, 'navigate');

    tick();
    component['deleteRoomType']();

    expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);
  }));

  it('should call loadpage on edit RoomType', fakeAsync(() => {
    spyOn(component.loadPageEmitService, 'emitChange');
    spyOn(component, 'setRoomTypeToSendServer');
    spyOn(component.route, 'navigate');

    tick();
    component['editRoomType']();

    expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);
  }));

  it('should call modalCancel', () => {
    const modalCancell = component['modalCancelActionService'];
    spyOn(modalCancell, 'cancelAction');
    component['modalCallCancel']();
    expect(modalCancell.cancelAction).toHaveBeenCalled();
  });

});
*/
