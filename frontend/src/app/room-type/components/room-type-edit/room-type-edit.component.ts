import { Location } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { RoomType } from '../../shared/models/room-type';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ModalCancelActionService } from '../../../shared/services/shared/modal-cancel-action.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ValidatorFormService } from '../../../shared/services/shared/validator-form.service';
import { BedTypeService } from '../../shared/services/bed-type/bed-type.service';
import { RoomTypeMapper } from '../../shared/services/room-type-mapper/room-type-mapper';
import { RoomTypeService } from '../../shared/services/room-type/room-type.service';
import { BedTypeEnum } from './../../../shared/models/bed-type';
import { ButtonConfig, ButtonSize, ButtonType } from './../../../shared/models/button-config';
import { ChildrenAgeRange } from '../../shared/models/children-age-range';

@Component({
  selector: 'app-room-type-edit',
  templateUrl: './room-type-edit.component.html',
  styleUrls: ['./room-type-edit.component.css'],
})
export class RoomTypeEditComponent implements OnInit, AfterViewInit {
  @ViewChild(DatatableComponent) myTable: DatatableComponent;

  private roomType: RoomType;
  public propertyId: number;
  public formGroupEditRoomType: FormGroup;
  public childrenAgeRangeList: Array<ChildrenAgeRange>;
  public optionsCurrencyMask: any;
  public roomTypeId: number;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    public route: ActivatedRoute,
    public roomTypeResource: RoomTypeResource,
    public _route: Router,
    public roomTypeService: RoomTypeService,
    private toasterEmitService: ToasterEmitService,
    private modalEmitService: ModalEmitService,
    private translateService: TranslateService,
    private _location: Location,
    private bedTypeService: BedTypeService,
    public validatorFormService: ValidatorFormService,
    public modalCancelActionService: ModalCancelActionService,
    private roomTypeMapper: RoomTypeMapper,
    private sharedService: SharedService,
  ) {}

  ngOnInit() {
    this.setVars();
    this.setListeners();
  }

  ngAfterViewInit() {
    this.myTable.rowHeight = 60;
  }

  private setVars() {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
    this.setForm();
    this.propertyId = this.route.snapshot.params.property;
    this.roomTypeId = this.route.snapshot.params.id;
    this.setButtonConfig();
    this.loadRoomType(this.roomTypeId);
  }

  private setForm(): void {
    this.formGroupEditRoomType = this.formBuilder.group({
      basicGroup: this.formBuilder.group({
        initials: ['', [Validators.required, Validators.maxLength(10), this.validatorFormService.noWhitespace]],
        description: ['', [Validators.required, this.validatorFormService.noWhitespace]],
        order: ['', [Validators.required]],
        distributionCode: '',
        minimumRate: [0, [Validators.required, this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        maximumRate: [0, [Validators.required, this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
      }),
      accommodationGroup: this.formBuilder.group({
        adultCapacity: [
          0,
          [
            Validators.required,
            this.validatorFormService.capacityNumberIsMajorThanOne,
            this.validatorFormService.setNumberToZero,
            this.validatorFormService.notAcceptNegatives,
          ],
        ],
        childCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        doubleBedCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        reversibleBedCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        singleBedCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        cribBedCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
        extraBedCapacity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
      }),
      ranges: this.formBuilder.array([]),
    });
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-edit-room-type',
      this.modalCallCancel,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
      false,
    );
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'edit-room-type',
      this.saveRoomType,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
  }

  private createFreeChildQuantityGroup(): FormGroup {
    return this.formBuilder.group({
      freeChildQuantity: [0, [this.validatorFormService.setNumberToZero, this.validatorFormService.notAcceptNegatives]],
    });
  }

  private setListeners() {
    this.hasBedCapacityChanges();
    this.bedCapacityListener();
  }

  private loadRoomType(roomTypeId: number) {
    this.roomType = new RoomType();

    this.roomTypeResource.getRoomTypeByRoomTypeId(roomTypeId).subscribe(roomType => {
      this.roomType = roomType;
      this.loadFormControls();
      this.setChildrenAgeRangeList();
    });
  }

  private loadFormControls() {
    this.formGroupEditRoomType.get('basicGroup.initials').setValue(this.roomType.abbreviation);
    this.formGroupEditRoomType.get('basicGroup.description').setValue(this.roomType.name);
    this.formGroupEditRoomType.get('basicGroup.order').setValue(this.roomType.order);
    this.formGroupEditRoomType.get('basicGroup.distributionCode').setValue(this.roomType.distributionCode);
    this.formGroupEditRoomType.get('basicGroup.minimumRate').setValue(this.roomType.minimumRate);
    this.formGroupEditRoomType.get('basicGroup.maximumRate').setValue(this.roomType.maximumRate);
    this.formGroupEditRoomType.get('accommodationGroup.adultCapacity').setValue(this.roomType.adultCapacity);
    this.formGroupEditRoomType.get('accommodationGroup.childCapacity').setValue(this.roomType.childCapacity);
    this.formGroupEditRoomType
      .get('accommodationGroup.doubleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(this.roomType, BedTypeEnum.Double));
    this.formGroupEditRoomType
      .get('accommodationGroup.reversibleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(this.roomType, BedTypeEnum.Reversible));
    this.formGroupEditRoomType
      .get('accommodationGroup.singleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(this.roomType, BedTypeEnum.Single));
    this.formGroupEditRoomType
      .get('accommodationGroup.cribBedCapacity')
      .setValue(this.roomTypeService.getQuantityOptionalBedList(this.roomType, BedTypeEnum.Crib));
    this.formGroupEditRoomType
      .get('accommodationGroup.extraBedCapacity')
      .setValue(this.roomTypeService.getQuantityOptionalBedList(this.roomType, BedTypeEnum.Single));
  }

  private setChildrenAgeRangeList(): void {
    this.childrenAgeRangeList = [];
    this.roomTypeResource.getAllChildrenAgeRangeList(this.propertyId).subscribe(
      response => {
        if (response.items) {
          this.childrenAgeRangeList = [...response.items];
          this.childrenAgeRangeList.forEach(() => {
            this.addFreeChildQuantityInRangesFormArray();
          });
          this.loadRangesFormArray();
        }
      });
  }

  private addFreeChildQuantityInRangesFormArray(): void {
    const formArray = this.formGroupEditRoomType.get('ranges') as FormArray;
    formArray.push(this.createFreeChildQuantityGroup());
  }

  private loadRangesFormArray(): void {
    if (this.roomType) {
      const freeChildQuantityList = [
        this.roomType.freeChildQuantity1,
        this.roomType.freeChildQuantity2,
        this.roomType.freeChildQuantity3
      ];
      const formArray = this.formGroupEditRoomType.get('ranges') as FormArray;
      if (formArray) {
        formArray.controls
          .forEach((control, index) => {
            control.get('freeChildQuantity')
              .patchValue(freeChildQuantityList[index] || 0);
          });
      }
    }
  }

  private hasBedCapacityChanges() {
    if (this.roomTypeService.hasChangeSomeAccommodationIncludedCapacity(this.formGroupEditRoomType)) {
      this.roomTypeService.setErrorValidatorAccommodationIncluded(this.formGroupEditRoomType);
    }
  }

  private bedCapacityListener() {
    this.formGroupEditRoomType.valueChanges.subscribe(editRoomTypeForm => {
      this.buttonSaveConfig.isDisabled = this.formGroupEditRoomType.invalid;
      if (this.roomTypeService.hasSomeAccommodationIncludedCapacity(editRoomTypeForm)) {
        this.roomTypeService.setErrorValidatorAccommodationIncluded(this.formGroupEditRoomType);
      } else {
        this.roomTypeService.setTrueValidatorAccommodationIncluded(this.formGroupEditRoomType);
      }
    });
  }

  private saveRoomType = () => {
    if (this.roomTypeService.freeChildQuantityIsGreaterThanChildCapacity(this.formGroupEditRoomType)) {
      this.toasterEmitService.emitChange(SuccessError.error, 'text.quantityOfChildrenForCourtesyIsHigherThanAllowed');
      return;
    }
    if (this.roomTypeService.maximumRateIsLessThanMinimumRate(this.formGroupEditRoomType)) {
      this.toasterEmitService.emitChange(SuccessError.error, 'text.maximumRateCanNotBeLessThanTheMinimum');
      return;
    }
    this.roomType = this.roomTypeMapper.formGroupToRoomType(this.formGroupEditRoomType, this.propertyId, this.roomType);
    this.roomTypeResource.editRoomType(this.roomType).subscribe(roomType => {
      this.formGroupEditRoomType.reset();
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('hotelPanelModule.roomTypeEdit.editRoomTypeSuccessMessage'),
      );
      this._route.navigate(['../../'], { relativeTo: this.route });
    });
  }

  private modalCallCancel = () => {
    this.modalCancelActionService.cancelAction();
  }

  public invalidFreeChildQuantity(index) {
    const freeChildQuantityList = this.formGroupEditRoomType.get('ranges') as FormArray;
    const control = freeChildQuantityList.at(index);
    if (control) {
      return control.get('freeChildQuantity').dirty
        && control.get('freeChildQuantity').invalid;
    }
    return true;
  }
}
