import { Card } from '@inovacao-cmnet/thx-ui';

export const CARD_REGISTER = 'CARD_REGISTER';
export const CARD_RESERVATION = 'CARD_RESERVATION';
export const CARD_ACCOMMODATION = 'CARD_ACCOMMODATION';
export const CARD_GOVERNANCE = 'CARD_GOVERNANCE';
export const CARD_FINANCE = 'CARD_FINANCE';
export const CARD_INTEGRATION = 'CARD_INTEGRATION';
export const CARD_MANAGEMENT = 'CARD_MANAGEMENT';
export const CARD_REPORT = 'CARD_REPORT';
export const ALL_CARD_LIST = 'ALL_CARD_LIST';
export const CARD_OTHERS = 'CARD_OTHERS';

export class CardData {
  public static readonly CARD_REGISTER = <Card[]>[

  ];

  public static readonly CARD_RESERVATION = <Card[]>[
    {
      idTag: '2',
      id: '1',
      headerCard: 'Como utilizar a tela de sugestão tarifária',
      imgCard: 'ht-bookings.svg',
      tagLeft: 'Tarifa',
      tagRight: 'Financeiro',
      time: '3 min'
    },
    {
      idTag: '2',
      id: '2',
      headerCard: 'Como realizar uma Reserva de Grupo',
      imgCard: 'ht-bookings.svg',
      tagLeft: 'Rerserva',
      tagRight: 'Grupo',
      time: '4 min'
    },
    {
      idTag: '2',
      id: '3',
      headerCard: 'Como realizar uma Reserva Individual',
      imgCard: 'ht-bookings.svg',
      tagLeft: 'Rerserva',
      tagRight: '',
      time: '3 min'
    },
  ];

  public static readonly CARD_ACCOMMODATION = <Card[]>[
    {
      idTag: '3',
      id: '4',
      headerCard: 'Como realizar o check-in',
      imgCard: 'ht-accommodation.svg',
      tagLeft: 'Check-in',
      tagRight: '',
      time: '3 min'
    },
  ];

  public static readonly CARD_GOVERNANCE = <Card[]>[

  ];

  public static readonly CARD_FINANCE = <Card[]>[

  ];

  public static readonly CARD_INTEGRATION = <Card[]>[

  ];

  public static readonly CARD_MANAGEMENT = <Card[]>[

  ];

  public static readonly CARD_REPORT = <Card[]>[

  ];

  public static readonly CARD_OTHERS = <Card[]>[
    {
      idTag: '9',
      id: '5',
      headerCard: 'Entendendo o painel de hotéis',
      imgCard: 'ht-others.svg',
      tagLeft: 'Hotéis',
      tagRight: 'Home',
      time: '2 min'
    },
  ];

  public static readonly ALL_CARD_LIST = <Card[]>[
    {
      idTag: '2',
      id: '6',
      headerCard: 'Como utilizar a tela de sugestão tarifária',
      imgCard: 'ht-bookings.svg',
      tagLeft: 'Tarifa',
      tagRight: 'Financeiro',
      time: '3 min'
    },
  ];

}
