import { Article } from 'app/how-to/models/article';

export const SINGLE_ARTICLE = 'SINGLE_ARTICLE';
export const ARTICLES = 'ARTICLES';

export class ArticleData {
  public static readonly SINGLE_ARTICLE: Article = {
    id: 'as76',
    category: 'Hospedagem',
    title: 'Como realizar o check-in de um hóspede',
    categoryId: '3',
    categoryImage: 'ht-accommodation.svg',
    categoryImageIdle: 'ht-accommodation.svg',
    categoryColor: '#F5AB2F',
    time: 15,
    tags: ['Recepção', 'Gerência'],
    videoUrl: 'https://www.youtube.com/embed/7rXnQZT2BX4',
    description: '<h1>Suficiente</h1>',
    tdnUrl: 'https://google.com.br/'
  };

  public static readonly ARTICLES: Article[] = [
    {
      id: '1',
      category: 'menu.accomodation',
      title: 'Como realizar o check-in',
      categoryId: '3',
      categoryImage: 'ht-accommodation.svg',
      categoryImageIdle: 'ht-accommodation-gray.svg',
      categoryColor: '#F5A623',
      time: 3,
      tags: ['menu.accomodation'],
      videoUrl: 'https://www.youtube.com/embed/-yLrewxFWaw',
      description: `
        <h3><b>Como realizar o check-in</b></h3>
        <br>
        <p>Acesse diretamente o menu <b>Hospedagem / Check-in</b>.</p>
        <p>O sistema abrirá a tela de consulta de reservas, com a opção de filtro <b>"Check-in hoje"</b>, já habilitado.</p>
        <p>Nesta mesma tela, também é possível consultar os <b>"Check-outs de hoje"</b>, os <b>"Deadlines vencidos"</b>,
         os <b>"Hóspedes na casa"</b> e a <b>pesquisa avançada</b>.</p>
        <p>Na <b>pesquisa avançada</b>, você pode preencher os campos de filtragem para realizar uma busca por reservas.
         Neste caso, utilize apenas o <b>nome do hóspede</b> e clique em <b>"Pesquisar"</b>.</p>
        <p>Ao abrir a tela, devemos selecionar a reserva e pela opção lateral dos 03 pontinhos, abrir a
         opção de <b>Check-In</b>, para confirmar, clique no botão <b>"Check-in"</b>, ou cancelar o Check-in. </p>
        <p>Do lado direito da tela, é exibido um breve resumo da reserva. Na tela de <b>Consultar Reserva</b>,
         que foi aberta para fazermos o check-In, podemos ainda </b>Editar a Reserva, Verificar o Orçamento
          da Reserva, Preencher a Ficha do Hóspede e Imprimir o Slip da Reserva</b>. O preenchimento
           da <b>Ficha do Hóspede (FNRH) </b> é necessário para que possamos confirmar o check-in.</p>
	`,
      tdnUrl: ''
    },
    {
      id: '2',
      category: 'menu.others',
      categoryId: '9',
      title: 'Entendendo o Painel de hotéis',
      categoryImage: 'ht-others.svg',
      categoryImageIdle: 'ht-others-gray.svg',
      categoryColor: '#0C9ABE',
      time: 3,
      tags: ['menu.hotel', 'menu.home'],
      videoUrl: 'https://www.youtube.com/embed/5H9coPIYmqY',
      description: `
        <h3><b>Entendendo o Painel de Hotéis</b></h3>
        <br>
        <p>O painel de hotéis é o local que você pode escolher em qual hotel quer operar. </p>
        <p>Esta escolha aparece assim que você efetua o login no Thex, mas não se preocupe:
        você pode trocar o hotel em que está trabalhando a qualquer momento. </p>
        <p>Para isto, basta clicar diretamente no menu Home - Painel de hotéis, porém, o
        acesso a esta tela vai depender da sua configuração de direitos de utilização do Thex.</p>
        <br>
        <h3><b>O passo a passo é o seguinte:</b></h3>
        <p>Efetuar o login;
        <p>Em seguida, selecionar o hotel em que iremos trabalhar.</p>
        <p>Caso seja necessário trocar de hotel, basta clicar no menu Painel de Hotéis e selecionar um outro hotel.</p>
      `,
      tdnUrl: '',
    },
    {
      id: '3',
      category: 'menu.reservation',
      categoryId: '2',
      title: 'Como realizar uma Reserva de Grupo',
      categoryImage: 'ht-bookings.svg',
      categoryImageIdle: 'ht-bookings-gray.svg',
      categoryColor: '#FF6D6D',
      time: 4,
      tags: ['menu.group', 'menu.reservation'],
      videoUrl: 'https://www.youtube.com/embed/6nUmIUrIT6k',
      description: `
        <h3><b>Como realizar uma Reserva de Grupo</b></h3>
        <br>
        <p>Para criar uma Reserva de Grupo, utilizaremos a tela de reserva, onde já existe um uma reserva criada.</p>
        <p>Faremos uma outra reserva, onde, ao incluir essa nova reserva, na parte de baixo da tela, o sistema exibirá
         o botão Opções de Grupo.</p>
        <p>Este botão nos permite criar um nome para o grupo e definir a conta master.</p>
        <p>Em seguida, basta clicar em Confirmar para validar a reserva.</p>
      `,
      tdnUrl: '',
    },
    {
      id: '4',
      category: 'menu.reservation',
      categoryId: '2',
      title: 'Como realizar uma Reserva Individual',
      categoryImage: 'ht-bookings.svg',
      categoryImageIdle: 'ht-bookings-gray.svg',
      categoryColor: '#FF6D6D',
      time: 3,
      tags: ['menu.reservation'],
      videoUrl: 'https://www.youtube.com/embed/-yLrewxFWaw',
      description: `
        <h3><b>Como realizar uma Reserva Individual</b></h3>
        <br>
        <p>Utilizando o menu Reservas / Nova Reserva ou clicando diretamente pelo Grid de Disponibilidade em uma data
        ou período, acessando a opção Reservar Período.</p>
        <p>Preencha os campos:</p>
             <p>* Clientes,</p>
             <p>* Contato,</p>
             <p>* E-mail (lembrando que os campos com asterisco são obrigatórios).</p>
        <p>Em Confirmação da Reserva, indique o tipo de pagamento e, de acordo com o tipo, faça a configuração correta;</p>
        <p>Em Detalhes da Reserva informe a Origem e o Segmento;</p>
        <p>Indique o período, qtde de UH, Qtd Adultos e Tipo de UH;</p>
        <p>Verifique se há oferta no período em Orçamento;</p>
        <p>Por fim, clique em Inserir, para finalizar a Reserva.</p>
      `,
      tdnUrl: '',
    },
    {
      id: '5',
      categoryId: '2',
      category: 'menu.reservation',
      title: 'Selecionando uma tarifa com Sugestão Tarifária',
      categoryImage: 'ht-bookings.svg',
      categoryImageIdle: 'ht-bookings-gray.svg',
      categoryColor: '#FF6D6D',
      time: 3,
      tags: ['menu.finance', 'menu.others'],
      videoUrl: 'https://www.youtube.com/embed/WYNmmVOScnQ',
      description: `
        <h3><b>Como utilizar a tela de sugestão tarifária</b></h3>
        <br>
        <p>A tela se <b>Sugestão Tarifária</b> é aquela que ajuda na <b>negociação e cotação junto aos hóspedes</b>.
         Nesta tela, você consegue visualizar todas as tarifas listadas do seu hotel.</p>
        <br>
        <p>Você pode acessar esta tela através do menu <b>Reservas / Sugestão Tarifária</b> ou clicando diretamente,
         pelo <b>Grid de Disponibilidade</b>, em uma <b>data</b> ou <b>período</b>, acessando a opção sugestão tarifária.
          Na área de <b>Dados da Reserva</b>, você pode pesquisar por um cliente (caso possua um acordo comercial
           especial para um cliente), período, quantidade de quartos, adultos e criança. No campo Tipo de UH,
            você pode selecionar TODAS ou um tipo de UH específico. Podemos alterar a tarifa por período e
             definir o tipo de pensão. Uma vez selecionada a tarifa, você pode alterar o valor cobrado dia a
              dia, aplicar % de desconto e até mesmo alterar o tipo de pensão da tarifa ou de um dia específico.
               Todas as configurações feitas pelo usuário na tarifa selecionada são exibidas na barra azul inferior.
                Em seguida basta clicar em "Ir para a Reserva" e finalizar o processo. Na próxima tela, aparecerá
                 o período com o valor orçado.</p>
         `,
      tdnUrl: '',
    },
    {
      id: '6',
      categoryId: '1',
      category: 'menu.register',
      title: 'Como realizar o cadastro de clientes',
      categoryImage: 'ht-register.svg',
      categoryImageIdle: 'ht-register-gray.svg',
      categoryColor: '#AB85E9',
      time: 0,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
        <h3><b>Com realizar o cadastro de clientes?</b></h3>
        <br>
        <p> Por que este cadastro È importante? </p>
        <p>… importante que o cliente esteja cadastrado para que ele seja associado aos acordos comerciais do módulo
        Revenue Manegement (RM ou Gestão de Receita). Este cadastro permite inserir tanto pessoa física quanto pessoa
        jurídica. </p>
        <p>Veja aqui o passo a passo: </p>
        <p>1 - Clique em <b>Cadastros / Gestão de Rede / Cliente.</b> </p>
        <p>2 - Os campos que estão com asteriscos s„o de preenchimento obrigatório. </p>
        <p>3 - Em <b>Tipo de Cliente</b>, selecione se È pessoa <b>física</b> ou <b>jurídica</b>; </p>
        <p>3.1 - Se optar por <b><u>pessoa física</u></b>, informe o <b><u>nome do cliente</u></b>; </p>
        <p>4 - Os campos telefone (fixo e celular) são de preenchimento opcional; </p>
        <p>5 - Selecione a <b>Categoria</b>; </p>
        <p>6 - Insira o <b>e-mail</b>; </p>
        <p>7 - Clique no botão <b>"Incluir endereço"</b> e preenchas os campos</p>
        <p>8 - Incluir os <b>Documentos</b> necessários, selecione o <b>tipo de documento</b> a ser inserido e informe o <b>número</b>.</p>
        <p>9 - Incluir os <b>Contatos</b> necessários. Informando o <b>Nome</b>, a <b>Função</b>,
        o <b>Telefone</b> e o <b>E-Mail</b>, para cada um. </p>
      `,
      tdnUrl: '',
    },
    {
      id: '7',
      categoryId: '1',
      category: 'menu.register',
      title: 'Como cadastrar acomodações',
      categoryImage: 'ht-register.svg',
      categoryImageIdle: 'ht-register-gray.svg',
      categoryColor: '#AB85E9',
      time: 0,
      tags: ['menu.reservation', 'menu.others'],
      videoUrl: '',
      description: `
        <h3><b>Como cadastrar acomodações</b></h3>
        <br>
        <p>Por que é importante?
        <p>Esta rotina permite que cadastre e atualize as UH do hotel. Por meio deste cadastro, você pode
         registrar todos os dados da unidade habitacional que está sendo cadastrada. Vale lembrar que o usuário
          somente conseguirá cadastrar o número de UH que esteja vinculado ao contrato. O sistema não permite
           ultrapassar a quantidade contratada sem acionar o comercial a fim de que o contrato seja renegociado.</p>
        <br>
        <p>Como cadastrar?</p>
        <p>Clique em <b>Cadastros / Gestão de Hotel / Tipo de UH</b></p>
        <p>Insira a <b>sigla da UH, a descrição</b> e informe a <b>ordem de disponibilidade</b>, esta ordem definirá
         a ordem na qual este tipo de apartamento aparecerá na grade de disponibilidade. </p>
        <p>Insira as <b>tarifas mínima e máxima</b></p>
        <br>
        <p>Em <b>acomodação</b>, informe:</p>
        <p>* Quantidade de adultos; </p>
        <p>* Quantidade de crianças; </p>
        <p>* Quantidade de cama de casal; </p>
        <p>* Quantidade cama reversível; </p>
        <p>* Quantidade Cama de Solteiro; </p>
        <p>* Quantidade de acomodações extras; </p>
        <p>* Faixa etária da criança cortesia, bem como sua quantidade. </p>
        <br>
        <p>Pressione o botão "Confirmar", para gravar as informações.</p>
      `,
      tdnUrl: '',
    },
    {
      id: '8',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Visão geral da Operação de Caixa',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 0,
      tags: ['menu.finance', 'menu.others'],
      videoUrl: '',
      description: `
		<h3><b>Visão geral da Operação de Caixa </b></h3>
		<br>
		<p>Neste item, veremos a manutenção das contas, lançamentos, encerramento de contas e demais operaÁıes que
		irão compor o fluxo de caixa do hotel. </p>
		<p>Acessando o menu Financeiro - Operação de Caixa. Na parte superior da tela, podemos efetuar uma busca, de
		acordo com os filtro indicados, por uma conta específica.</p>
		<p>Podemos também criar uma nova conta, isso ser· assunto para o tutorial Conta avulsa, mas em um breve resumo,
		ao clicar neste botão, podemos criar uma conta avulsa, uma nova conta para um hóspede ou para empresa. </p>
		<p>No caso de uma conta avulsa, basta definir por CPF ou CNPJ, efetuar o preenchimento e clicar no botão criar
		conta e no caso de uma conta para hóspede ou empresa, basta selecionar um conta e clicar no botão criar conta.</p>
		<p>Abaixo, nÛs podemos fazer uma filtragem por CONTAS ABERTAS, CONTAS FECHADAS, CONTAS PENDENTES E TODAS AS CONTAS.</p>
		<p>Ao selecionar uma conta, observamos as informaÁıes gerais da mesma como o total de contas vinculadas a mesma,
		saldo, nome do hóspede, tipo de conta, período, e status. </p>
		<p>O botão <b>Extrato</b> nos permite, verificar os lançamentos e o saldo da conta, podemos imprimir
		um extrato ou nota via impressora térmica, podemos detalhar melhores informaÁıes via impressão A4 e
		encerrar a conta. E mais abaixo temos mais uma opção para imprimir esta conta e pelo botão pagar, podemos
		encerrar ou consolidar a conta, definindo a forma de pagamento e finalizando em PAGAR novamente. </p>
		<p>O botão <b>Lançar</b> nos permite acrescentar um serviÁo, um POS (consumo de PDV ou loja do hotel) e adicionar um crédito a conta.</p>
		<p>O botão encerrar nos possibilita as mesmas opções de impressão já informadas e finalizar a conta, efetuando o seu pagamento. </p>
		<p>… possível fazer uma filtragem por contas ou por serviços adquiridos durante a estadia. E,
		podemos ainda adicionar uma nova conta, pela opção <b>ADICIONAR NOVA CONTA</b>. </p>
		<p>Já no campo <b>Lançamentos</b>, podemos: </p>
		</p><b>Emitir NF e RPS</b>;</p>
		<p><b>Editar o Cabeçalho da Nota</b>;</p>
		<p>Ao clicar no flag <b>DATA/HORA</b>, o sistema nos mostra as opções <b>TRANSFERIR e ESTORNAR</b>; </p.
		<p>E, ao selecionar apenas uma conta, além das duas anteriores, temos a opção <b>DIVIDIR</b>. </p>
		<p>Mais abaixo temos o contador de páginas e o saldo da operação.</p>
		<p>Na opção <b>Contas Fechadas</b>, além de algumas telas e botões já comentados, temos o botão <b>REABRIR</b>. </p>
		<p>O Campo Observações, mostra as observações descritas durante algum lançamento. </p>
		<p>Temos ainda <b>CONTAS PENDENTES e TODAS AS CONTAS</b>.</p>
      `,
      tdnUrl: '',
    },
    {
      id: '9',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Transferência de Lançamentos',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
        <h3><b>Transferência de lançamentos</b></h3>
        <br>
        <p>Já com <b>Operação de Caixa</b> aberta, vamos marcar e, ao selecionarmos um lançamento, clicar no botão
         <b>"Transferir"</b>.</p>
        <p>Ao abrir a tela vemos os dados da <b>conta de origem</b>, selecionamos uma <b>conta
         de destino</b> e clicamos em <b>"Transferir"</b>.</p>
        <p>É possível desfazer o processo, clicando na opção <b>"Desfazer Transferência"</b>.
         Ao clicarmos no botão <b>"Voltar para a Conta"</b>, o lançamento já será visto na conta de destino.</p>
      `,
      tdnUrl: '',
    },
    {
      id: '10',
      categoryId: '1',
      category: 'menu.register',
      title: 'Parametrizando meu hotel',
      categoryImage: 'ht-register.svg',
      categoryImageIdle: 'ht-register-gray.svg',
      categoryColor: '#7ECFD6',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
        <h3><b>Parametrizando meu Hotel</b></h3>
        <br>
        <p>Acessar o menu <b>Cadastros / Gestão do Hotel / Hotel / Parâmetros do Hotel</b>.</p>
        <br>
        <p>Em <b>Parâmetros Gerais</b>, podemos definir: </p>
        <p>* Horário de Check-in </p>
        <p>* Horário de Check-out </p>
        <p>* Se o lançamento das diárias ocorrerá na auditoria </p>
        <p>* Qual a moeda padrão </p>
        <p>* A data do sistema </p>
        <p>* Em qual passo se inicia a auditoria </p>
        <p>* Seu fuso horário </p>
        <p>* N° de dias para a troca do status da UH </p>
        <p>* Se será permitida a edição de reservas migradas</p>
        <br>
        <p>Em <b>Tipos de Serviço</b>, serão definidos quais os <b>tipos</b> irão controlar a <b>Diária</b> e a <b>
        Diferença de Diária</b></p>
        <br>
        <p>Em <b>Faixa Etária de Criança</b>, serão definidos quais os 03 tipos de faixa etária para a
         acomodação e entrada de crianças nas UH´s.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '11',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Efetuando pagamento parcial',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
		    <h3><b>Efetuando pagamento parcial</b></h3>
        <br>
        <p>Em <b>Operação de Caixa</b>, selecionar uma conta. </p>
        <p>Em seguida, um lançamento e clicar no botão <b>"Pagar Parcial"</b>.</p>
        <p>Após a abertura da tela de pagamento, informar um nome para uma nova conta.
         Esta nova conta se tornará a conta principal para a movimentação. </p>
        <p>Após isto, devemos indicar um <b>tipo de pagamento</b> e o <b>valor a ser pago</b>.
         Abaixo, podemos ver o valor total e o saldo da diferença entre o valor total e o pagamento parcial efetuado. </p>
        <br>
        <p>Por fim basta clicar no botão <b>"Pagar"</b>, para validar a operação. </p>
        <p>Poderá ser visto na nova conta, que assume a movimentação do valor lançado com o pagamento
         parcial, o saldo ainda por ser pago.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '12',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Opções de encerramento de conta',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
        <h3><b>Opções de encerramento de conta</b></h3>
        <br>
        <p>Já com a <b>Operação de Caixa</b> aberta, selecione uma conta. </p>
        <p>Clicar no botão <b>"Encerar"</b> e confirmar para validar a operação. </p>
        <p>Clicar no botão <b>"Extrato"</b> e, no canto superior direito, clicar no botão <b>"Encerar"</b>.</p>
        <p>O sistema irá abrir uma janela para a escolha da <b>forma de pagamento</b> e encerrar a conta. </p>
        <p>O mesmo processo acontece com o botão <b>"Pagar"</b>, onde devemos indicar a <b>forma
         de pagamento</b> para, na sequência, encerrarmos a conta. </p>
        <p>Repare que <b>ao validarmos o encerramento</b>, o botão <b>"Reabrir"</b> foi habilitado,
         em substituição ao <b>"Encerar"</b>.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '13',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Como liberar uma UH',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
        <h3><b>Como liberar uma UH? </b></h3>
        <br>
        <p>Vale lembrar que é possível liberar uma UH sem que o hóspede tenha ido embora e/ou um hóspede
         que libera seu quarto, mas continua consumindo no hotel aguardando horário de vôo.</p>
        <br>
        <p>Vamos ao passo a passo:</p>
        <p>Já com a tela de <b>Operação de Caixa</b> aberta, selecionar uma conta.</p>
        <p>No canto superior direito, clicar no botão <b>"Liberar UH"</b>.</p>
        <p>Será aberta uma janela onde, ao confirmar a liberação, a acomodação permanecerá como pendente até
         que a conta seja encerrada.</p>
        <p>A mesma irá aparecer para a governança como <b>Vago</b> e <b>Sujo</b>.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '14',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Como efetuar lançamentos na conta do hóspede',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
		    <h3><b>Lançando débitos e créditos na conta do hóspede</b></h3>
        <br>
        <p>Com a tela de <b>Operação de Caixa</b> aberta, selecionar uma conta (Hóspede ou Empresa) e clicar no botão  <b>"Lançar"</b>.</p>
        <p>Podemos lançar como <b>débito</b>, um <b>Serviço</b> ou um <b>POS</b>.</p>
        <p>Clicando em Serviço, selecionar um dos grupos de serviços disponíveis e em seguida lançar o serviço,
         informar o valor do mesmo (repare que há uma taxa associada e será acrescida ao valor) e, se necessário,
          preencher a observação.</p>
        <p>O mesmo procedimento é aplicado a um POS e ao indicar o grupo, repare que o item já vem configurado com
         preço cadastrado, onde podemos apenas acrescentar mais itens ou excluir.</p>
        <p>Ao clicar em confirmar, o item lançado já constará como débito na conta do hóspede, assim como o serviço
         lançado anteriormente.</p>
        <br>
        <p>Podemos lançar um <b>crédito</b>, que será um pagamento. </p>
        <p>Deverá ser definido o <b>tipo de pagamento/crédito</b>, informado o valor e, em seguida, confirmada a operação. </p>
        <p>Será possível ver o extrato da conta com os lançamentos de débito e crédito, lançados na conta do hóspede.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '15',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Como efetuar estorno de lançamentos',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
		    <h3><b>Como efetuar estorno de lançamentos</b></h3>
        <br>
        <p>Com a tela de <b>Operação de Caixa</b> aberta, selecionar um lançamento e clicar no botão <b>"Estornar"</b>. Na janela
         de estorno, selecionar um <b>motivo para o estorno</b> e informar uma justificativa ou informação
          relevante no campo <b>"Observação"</b>.</p>
        <p>Após isto, deverá ser clicado no botão <b>"Confirmar"</b>, para validar a operação. Ao voltar a tela de lançamentos,
         o sistema nos mostrará o lançamento riscado, indicando o estorno com valor zerado.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '16',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Como fazer a divisão de lançamentos',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
		    <h3><b>Como fazer uma divisão de lançamentos</b></h3>
        <br>
        <p>Na tela de <b>Operação de Caixa</b>, selecionar uma conta, selecionar um lançamento desta conta e clicar
         no botão <b>"Dividir"</b>.</p>
        <p>Na janela de divisão, efetuar o processo utilizando a opção <b>por valor</b>, onde basta informar o valor
         para que o sistema calcule o valor restante do lançamento.</p>
        <p>Também pode-se selecionar a opção <b>por partes</b>, onde ao informar em quantas partes o lançamento será
         dividido, o sistema calcule os valores de acordo com o número de parcelas.</p>
        <p>Após isto, basta <b>confirmar</b> para validar a operação.</p>
        <p>Ao voltarmos para a conta, na tela de lançamentos, já serão exibidos os lançamentos oriundos da divisão,
         com a indicação da divisão ao lado direito.</p>
        <br>
        <p>Ao abrir o lançamento podemos observar o valor da tarifa e taxa de serviços.</p>
        <p>Ao selecionar uma das contas divididas, é possível desfazer a operação, através do botão <b>"Juntar"</b>.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '17',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Como efetuar desconto em lançamentos',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
		    <h3><b>Como efetuar desconto em lançamentos</b></h3>
        <br>
        <p>Na tela de <b>Operação de Caixa</b>, selecionar um lançamento a débito e já podemos clicar no botão <b>"Desconto"</b>.</p>
        <p>Deverá ser indicado se o desconto será em <b>valor</b> ou <b>percentual</b>. </p>
        <p>Preencha o valor. </p>
        <p>Selecione o motivo (Este cadastro será visto em um outro tutorial). </p>
        <br>
        <p>Abaixo, poderá ser visto um comparativo entre o valor original e o valor com desconto. </p>
        <br>
        <p>Clicar em <b>"Confirmar"</b> para validar a operação.</p>
        <br>
        <p>Ao voltar a tela de lançamentos da operação de caixa, o lançamento recebe um indicativo de
         informação onde, ao passar o mouse sobre, o sistema nos informa que um desconto foi aplicado e ao
          expandir o lançamento, podemos ver o desconto que foi aplicado.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '18',
      categoryId: '1',
      category: 'menu.register',
      title: 'Como efetuar a configuração da origem e segmento',
      categoryImage: 'ht-register.svg',
      categoryImageIdle: 'ht-register-gray.svg',
      categoryColor: '#AB85E9',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
		    <h3><b>Como efetuar a configuração da Origem e Segmento</b></h3>
        <br>
        <p>Acessar o menu <b>Cadastros / Gestão do Hotel / Hotel / Cadastro de Segmento</b> Pode-se criar
         os <b>tipos de origem</b>, muito importantes na identificação de onde se iniciou a reserva no
          estabelecimento. Para isto, clicar em <b>"Nova Origem"</b>, informar o <b>código</b> e a <b>descrição</b> do campo. </p>
        <p>Em seguida, definir que esta origem ficará apta para utilização do sistema, no campo <b>Ativar Origem</b>. </p>
        <p>Após isto, <b>Confirmar</b> para validar a operação. </p>
        <br>
        <p>Ainda na tela de <b>Gestão do Hotel</b>, clicar em <b>"Segmento"</b>. Esse cadastro é importante
         na apuração de relatórios para identificarmos as diversas finalidades de estadia no hotel. Clicar
          em <b>"Incluir Segmento Principal"</b>, que será um agrupado de segmentos mais específicos, preenchendo: </p>
        <p>* Segmento Principal </p>
        <p>* Sigla </p>
        <p>* Código </p>
        <p>* Definir como <b>Ativo</b></p>
        <br>
        <p>Para maior detalhamento do segmento, pode-se criar segmentos secundários. Clicando no botão <b>"Incluir Segmento
         Secundário"</b>.</p>
        <p>Após a criação do <b>Segmento Secundário</b>, devemos associa-lo a um <b>Segmento Principal</b> e preencher: </p>
        <p>* NOME </p>
        <p>* SIGLA </p>
        <p>* CÓDIGO </p>
        <p>* Definir como <b>Ativo</b>. </p>
        <br>
        <p>Após isto, <b>Confirmar</b> para validar a operação.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '19',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Como imprimir extratos',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
		    <h3><b>Como Imprimir Extratos</b></h3>
        <br>
        <p>Na tela de <b>Operação de Caixa</b>, selecionar uma conta.</p>
        <p>Em seguida, clicar no botão <b>"Extrato"</b>.</p>
        <p>Ao abrir a tela de extrato, podemos imprimi-lo através de uma impressora de cupom fiscal ou em formato A4.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '20',
      categoryId: '8',
      category: 'menu.reports',
      title: 'Como fazer para editar o cabeçalho da nota fiscal',
      categoryImage: 'ht-reports.svg',
      categoryImageIdle: 'ht-reports-gray.svg',
      categoryColor: '#f1c141',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
		    <h3><b>Como fazer para editar o cabeçalho da nota fiscal</b></h3>
        <br>
        <p>Na tela de Operação de Caixa, clicar no botão "Cabeçalho da Nota".</p>
        <p>Agora basta preencher os campos em Detalhes do Cabeçalho e Endereço. </p>
        <p>Lembrando que os campos com asterisco são obrigatórios! </p>
        <p>* Documento</p>
        <p>* E-mail </p>
        <p>* Nome </p>
        <p>* Telefone (não obrigatório) </p>
        <p>* CEP (os campos UF, Cidade e Bairro são preenchidos automaticamente) </p>
        <p>* Informe a localidade </p>
        <p>* Número </p>
        <p>* Complemento </p>
        <p>* Tipo de endereço. </p>
        <br>
        <p>Clique em <b>Salvar</b> para finalizar o processo.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '21',
      categoryId: '3',
      category: 'menu.accomodation',
      title: 'Como fazer a Auditoria',
      categoryImage: 'ht-accommodation.svg',
      categoryImageIdle: 'ht-accommodation-gray.svg',
      categoryColor: '#F5A623',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
        <h3><b>Como fazer a Auditoria</b></h3>
        <br>
        <p>A Auditoria é responsável, basicamente, pela conferência de todas as informações digitadas
         no sistema durante um dia de trabalho. </p>
        <p>Normalmente durante a madrugada, onde a Recepção e os Pontos de Venda estão com o movimento mais tranquilo. </p>
        <p>Acessando o menu <b>Hospedagem</b>, opção <b>Auditoria</b>. </p>
        <p>Abrindo a tela vamos começar falando dos passos da auditoria, onde temos: </p>
        <p>* <b>Permanência inesperada de uh -</b> quando o hóspede deveria efetuar o check-out no dia mas,
         permaneceu no quarto e o processo de auditoria irá atualizar a data de saída e o valor da estadia para o dia seguinte; </p>
        <p>* <b>Lançamento de Diárias -</b> onde o sistema efetua o lançamento de todas as diárias registradas no dia; </p>
        <p>* <b>Atualizando Datas -</b> que atualiza as datas do sistema; </p>
        <p>* <b>No Show -</b> altera o status para no show das reservas com confirmação para o dia, não confirmadas em check-in.</p>
        <p>* <b>Atualizar Status de Manutenção e Governança -</b> atualiza os status das UHs em bloqueio e
         desbloqueio de acordo com o agendamento e para vago / ocupado / limpo / sujo, de acordo com disponibilidade das UHs. </p>
        <br>
        <p>O <b>status</b> de cada passo permanece como <b>aguardando</b>, até que se inicie a auditoria.
         Pela ordem, mudam para <b>processando e completo</b>, ao fim de cada passo. </p>
        <br>
        <p>Para nossa referência, temos a barra de progresso, que nos mostra das etapas da auditoria,
         e o campo <b>problemas encontrados</b>, que nos mostrará qualquer problema encontrado durante a auditoria. </p>
         e o campo <b>problemas encontrados</b>, que nos mostrará qualquer problema encontrado durante a auditoria. </p>
        <br>
        <p>Para iniciar a Auditoria, devemos clicar no botão <b>"Iniciar"</b> e, em seguida, clicar em <b>"avançar"</b>.</p>
        <br>
        <p>Ao término da Auditoria, poderemos ver a mudança na <b>data do sistema</b>.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '22',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Como adicionar subconta para hóspede ou empresa',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
        <h3><b>Como adicionar subconta para hóspede ou empresa</b></h3>
        <br>
        <p>Na tela de <b>Operação de Caixa</b>, vamos selecionar uma conta (<b>Hóspede</b> ou <b>Empresa</b>)
         e clicar no link <b>"Adicionar Conta"</b>.</p>
        <p>Em seguida, informar um nome para esta subconta e clicar em <b>"Confirmar"</b>.</p>
        <p>Após esta operação, a conta foi criada e já está apta a receber lançamentos e demais movimentações
         da <b>Operação de Caixa</b>.</p>
	  `,
      tdnUrl: '',
    },
    {
      id: '23',
      categoryId: '1',
      category: 'menu.register',
      title: 'Como configurar Tipos de Pensão',
      categoryImage: 'ht-register.svg',
      categoryImageIdle: 'ht-register-gray.svg',
      categoryColor: '#AB85E9',
      time: 2,
      tags: ['menu.others'],
      videoUrl: '',
      description: `
        <h3><b>Como configurar Tipos de Pensão</b></h3>
        <br>
        <p>Esta rotina tem como objetivo mostrar ao usuário a funcionalidade de <b>Renomear os Nomes das Pensões</b>,
         a fim de se adequar à realidade do hotel.</p>
        <br>
        <p>Clique no menu <b>Gerenciamento de Receita / Cadastro de Pensão</b>. Na tela, vemos as colunas <b>Tipos
         de Pensão, Código e Pensão Referência</b>. </p>
        <br>
        <p>Para as pensões que <b>estão de acordo com o hotel</b>, clique no botão para deixar <b>ativo</b> ou,
         para as pensões que <b>não forem utilizadas pelo hotel, inativo</b>. </p>
        <p>Após realizar o processo, pressione o botão <b>"Confirmar"</b>, para gravar as alterações realizadas.</p>
	  `,
      tdnUrl: '',
    },
    // {
    //   id: '24',
    //   category: 'menu.finance',
    //   categoryId: '5',
    //   title: 'Gestão de Documentos Fiscais',
    //   categoryImage: 'ht-bookings.svg',
    //   categoryImageIdle: 'ht-bookings-gray.svg',
    //   categoryColor: '#40D9B4',
    //   time: 4,
    //   tags: ['menu.group', 'menu.finance'],
    //   videoUrl: '',
    //   description: `
    //
    //
    //   `,
    //   tdnUrl: '',
    // },
    {
      id: '25',
      category: 'menu.reservation',
      categoryId: '2',
      title: 'Como preencher o Roomlist',
      categoryImage: 'ht-bookings.svg',
      categoryImageIdle: 'ht-bookings-gray.svg',
      categoryColor: '#FF6D6D',
      time: 4,
      tags: ['menu.group', 'menu.reservation'],
      videoUrl: '',
      description: `
        <h3><b>Como preencher o Roomlist</b></h3>
        <br>
        <p>O preenchimento do roomlist acontece no ato de confirmação da reserva, na tela de <b>Resumo da Reserva</b>. </p>
        <p>Para isso, basta clicar no botão <b>"Roomlist"</b>, existente nesta tela, e, em seguida, no botão <b>"Editar"</b>.</p>
        <p>O roomlist também poderá ser preenchido durante a realização do Check-In da reserva. </p>
        <br>
        <p>Se você optar por editar <b>as informações já trazidas do cadastro da reserva</b>, revise: </p>
        <p>* CPF; </p>
        <p>* e-mail; </p>
        <p>* Período de estadia; </p>
        <p>* Nome do Hóspede; </p>
        <p>* Tipo de Hóspede; </p>
        <p>* Indique se o hóspede deseja ficar Incógnito ou não. </p>
        <br>
        <p>Em <b>Informações Complementares</b> indique: </p>
        <p>* Nacionalidade; </p>
        <p>* Idioma; </p>
        <p>* % na diária. </p>
        <p><b>O campo de informações adicionais é preenchido de acordo com o cadastro do hóspede no hotel</b>. </p>
        <br>
        <p>Caso queira <b>preencher todos os campos da roomlist</b>, utilize o botão <b>"Limpar"</b>, confirme
         com <b>Sim</b>, e, em seguida, clique no botão <b>"Editar"</b>.</p>
        <br>
        <p>Esse é o procedimento do preenchimento de uma <b>Roomlist</b>.</p>
      `,
      tdnUrl: '',
    },
    {
      id: '26',
      categoryId: '2',
      category: 'menu.reservation',
      title: 'Como navegar pela Disponibilidade',
      categoryImage: 'ht-bookings.svg',
      categoryImageIdle: 'ht-bookings-gray.svg',
      categoryColor: '#FF6D6D',
      time: 3,
      tags: ['menu.finance', 'menu.others'],
      videoUrl: 'https://www.youtube.com/embed/T_yYtzXGuwM',
      description: `
        <h3><b>Como navegar pela Disponibilidade.</b></h3>
        <br>
        <p>Esta tela tem como objetivo a seleção do <b>Tipo de UH</b>, bem como a escolha do <b>período desejado</b>. </p>
        <p>Pode-se alterar a data clicando em <b>Calendário</b>; </p>
        <p>Realizar a <b>alteração do período</b> em <b>semanal, quinzenal</b> ou <b>mensal</b> e ao
         escolher o período a tela se adequada ao selecionado. </p>
        <p>Também podemos ver os <b>tipos de UH</b> com as quantidades disponíveis. </p>
        <p>Clicando sobre uma opção e arrastando-o o período fica marcado aparecendo o tipo UH e o período. </p>
        <p>Ao clicar em <b>Sugestão Tarifária</b> o sistema nos direciona para essa tela. Há um tutorial - <b>Selecionando uma
         tarifa com a sugestão tarifária</b> - ensinando como realizar este processo. </p>
        <p>Posicionando o mouse ao lado da UH desejada aparece o <b>período</b> como sendo de um dia,
         a opção de <b>bloquear a UH</b> bem como a <b>sugestão tarifária</b> e <b>Reservar período</b>. Ao clicar
          em <b>Bloquear a UH</b> o sistema abre uma nova janela com os campos, <b>período bloqueado</b>, o <b>motivo</b>
           e <b>observações</b>. </p>
        <p>Clicando em <b>Reservar Período</b>, o sistema também nos direciona para essa tela, esse processo
         também possui um tutorial explicando como proceder. </p>
        <p>Também são exibidos a legenda e os gráficos, demonstrando o total de UH e a situação panorâmica. </p>
        <p>O Status de Governança também mostra o gráfico e a situação de cada UH. </p>
        <p>A recepção com o prognóstico de checkin, checkout e walkin.</p>
         `,
      tdnUrl: '',
    },
    {
      id: '27',
      categoryId: '3',
      category: 'menu.accomodation',
      title: 'Como mudar um hóspede de UH',
      categoryImage: 'ht-accommodation.svg',
      categoryImageIdle: 'ht-accommodation-gray.svg',
      categoryColor: '#F5A623',
      time: 3,
      tags: ['menu.accomodation'],
      videoUrl: 'https://www.youtube.com/embed/JYlKkXFsmSo',
      description: `
        <h3><b>Como mudar um hóspede de UH</b></h3>
        <br>
        <p>Acessar o menu <b>Hospedagem / Mudança de UH</b>. </p>
        <p>Em seguida, vamos pesquisar por uma <b>UH atual</b>. </p>
        <p>As UHs listadas aqui serão somente as que já estiverem no status <b>Check-In</b>. </p>
        <p>Em seguida, selecione a <b>Nova UH</b>, indicando o <b>tipo</b> e o <b>número</b>. </p>
        <p>Podemos ver abaixo os valores do orçamento atual e o novo. </p>
        <p>Os dados apresentados na tela são apenas informativos. Para que o orçamento seja realmente ajustado,
         devemos acessar a tela de <b>Orçamento da Reserva</b>, para recalcular o valor. </p>
        <p>Clicar em <b>Confirmar</b> para validar a operação.</p>
         `,
      tdnUrl: '',
    },
    {
      id: '28',
      categoryId: '1',
      category: 'menu.register',
      title: 'Como realizar Operações de Governança',
      categoryImage: 'ht-register.svg',
      categoryImageIdle: 'ht-register-gray.svg',
      categoryColor: '#AB85E9',
      time: 3,
      tags: ['menu.register'],
      videoUrl: 'https://www.youtube.com/embed/JYlKkXFsmSo',
      description: `
        <h3><b>Como realizar Operações de Governança</b></h3>
        <br>
        <p>Esse processo permite alterar o status de uma unidade habitacional (UH) para realizar uma manutenção, por
         estar suja, dentre outras possibilidades. </p>
        <p>Para realizar estes processos, no menu, clicar em <b>Governança / Consultar Governança</b>. </p>
        <p>A tela mostrará todas as UHs, com suas descrições. Caso deseje ver um tipo específico, é necessário
         selecioná-lo por meio dos filtros na parte superior da tela. </p>
        <p>Ao selecionar o filtro <b>Inspeção</b>, o sistema trará somente as UHs com o filtro selecionado. </p>
        <p>Sempre que optar por um filtro, o sistema realiza a operação, mostrando-o. </p>
        <p>Ao marcar o flag de um determinado campo, o sistema habilita as opções no canto direito da tela. </p>
        <p>Pode-se marcar o flag <b>Status Governança</b> e todas as UHs serão marcadas aparecem no canto
         superior direito da tela as opções para dar continuidade ao processo: <b>Trocar Status, Bloquear e Desbloquear</b>. </p>
        <p>Posso optar por selecionar somente uma e ao clicar sobre ela, abre uma janela para realizar o processo. </p>
        <p>Para realizar o bloqueio de uma UH, há duas formas: <b>marcando o flag ao lado da unidade
         desejada</b> ou <b>clicando nos pontos verticais no canto direito da tela</b>. </p>
        <p>Aparece uma janela onde necessito preencher s campos para realizar tal operação. </p>
        <p>Terminado os processos, clicar em <b>Confirmar</b>.</p>
         `,
      tdnUrl: '',
    },
    {
      id: '29',
      categoryId: '3',
      category: 'menu.accomodation',
      title: 'Como efetuar o Walk-In',
      categoryImage: 'ht-accommodation.svg',
      categoryImageIdle: 'ht-accommodation-gray.svg',
      categoryColor: '#F5A623',
      time: 3,
      tags: ['menu.accomodation'],
      videoUrl: 'https://www.youtube.com/embed/nVMoLmM06nQ',
      description: `
        <h3><b>Como efetuar o Walk-In</b></h3>
        <br>
        <p>Acesse o menu <b>Hospedagem / Walk-in</b>. </p>
        <p>O sistema abrirá a tela de reservas onde devem ser preenchidos os campos básicos, que são: </p>
        <p>* Clientes</p>
        <p>* Contato </p>
        <p>* E-mail </p>
        <p>* Telefone</p>
        <p><b>Os campos com asterisco são obrigatórios</b>. </p>
        <br>
        <p>Após isto, em <b>confirmação da reserva</b>, indique o <b>tipo de pagamento</b> e de <b>acordo</b>,
         verificando a configuração correta. </p>
        <p>Em <b>detalhes da reserva</b>, informe a <b>origem</b> e o <b>segmento</b>. </p>
        <p>Indique o <b>Período, Qtd de UH, Adultos, Crianças e o tipo de UH</b>. </p>
        <p>O sistema verificará todas as ofertas disponíveis para as condições selecionadas. </p>
        <p>Selecione a oferta desejada para esta reserva. Caso o sistema não encontre uma oferta para as condições
         selecionadas, pode ser que o tarifário para esse período ainda não tenha sido configurado.
          É possível verificar isso nas telas de <b>Calendário de Tarifas</b> ou na de <b>Sugestão Tarifária</b>. </p>
        <br>
        <p>Verificar todos os dados da reserva e finalizar a inclusão clicando em <b>"Inserir"</b> e <b>"Ir para Check-In"</b>.</p>
         `,
      tdnUrl: '',
    },
    {
      id: '30',
      category: 'menu.finance',
      categoryId: '5',
      title: 'Realizando cotação de moedas',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 4,
      tags: ['menu.group', 'menu.finance'],
      videoUrl: '',
      description: `
        <h3><b>Realizando cotação de moedas</b></h3>
        <br>
        <p>Nesta funcionalidade, verificamos a <b>cotação da moeda</b> com <b>relação à taxa do câmbio</b> e a <b>cotação do hotel</b>. </p>
        <p>Pode-se colocar a cotação para mais ou para menos com relação a cotação de mercado. </p>
        <br>
        <p>No menu, clique em <b>Cadastros / Gestão de Hotel / Cotação de Moeda</b>. </p>
        <p><b>Os campos com asteriscos são de preenchimento obrigatório</b>. </p>
        <br>
        <p>Informe o <b>período para consulta</b>, clicando em <b>calendário</b>. </p>
        <p>Selecione a moeda. </p>
        <p>Pressione o botão <b>Pesquisar</b>.</p>
        <br>
        <p>No canto superior direito há o botão <b>Inserir cotação por período</b> e o sistema abrirá uma tela onde
         poderá ser informado um <b>período de cotação, selecionar a moeda e a cotação do hotel</b>. </p>
        <p>Inseridas as informações, pressione o botão <b>Consultar</b>. </p>
        <p>O sistema exibirá a tela com os campos preenchidos.</p>
      `,
      tdnUrl: '',
    },
    {
      id: '31',
      categoryId: '3',
      category: 'menu.accomodation',
      title: 'Como fazer uma conta Avulsa',
      categoryImage: 'ht-accommodation.svg',
      categoryImageIdle: 'ht-accommodation-gray.svg',
      categoryColor: '#F5A623',
      time: 3,
      tags: ['menu.accomodation'],
      videoUrl: '',
      description: `
        <h3><b>Como fazer uma conta Avulsa</b></h3>
        <br>
        <p>As contas avulsas podem ser utilizadas para várias funcionalidades: </p>
        <p>* registro de passantes, </p>
        <p>* contas de funcionários, </p>
        <p>* conta de controle do café da manhã, </p>
        <p>* contas temporárias para apuração de diferenças</p>
        <p>* qualquer outro tipo de conta que você deseje controlar no sistema. </p>
        <br>
        <p>No menu, clique em <b>Financeiro / Operação de Caixa</b>. </p>
        <p>Na tela aberta, no canto superior direito, clique em <b>Nova Conta</b>. </p>
        <p>Há dois tipos de conta a ser escolhida: a <b>física</b> e a <b>jurídica</b>. Preencha todos os campos, eles são
         de preenchimento obrigatório. Ao concluir, pressione o botão <b>Criar Conta</b>. </p>
        <br>
        <p>Ainda na tela <b>Nova Conta</b>, há os botões <b>Hóspede</b> e <b>Empresa</b>. Estes são utilizados quando um hóspede
         realiza a reserva e <b>faz um pagamento</b> ou <b>contrata um serviço antecipadamente</b>. </p>
        <p>Clique no botão <b>Hóspede</b> e selecione a conta. Em seguida, pressione o botão <b>Criar Conta</b>. Informe o
         nome da conta e clique em <b>Confirmar</b>. </p>
        <br>
        <p>Para uma empresa, o processo é o mesmo, selecione a reserva, pressione <b>Criar Conta</b>, informe o nome
         e pressione o botão <b>Confirmar</b>.</p>
         `,
      tdnUrl: '',
    },
    {
      id: '32',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Como inserir a Tarifa Base',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.finance'],
      videoUrl: 'https://www.youtube.com/embed/bOZC-uQDgkc',
      description: `
        <h3><b>Como inserir a Tarifa Base</b></h3>
        <p>Esta funcionalidade tem como objetivo definir uma base de cálculo para tarifas em uma determinada
         vigência, seja por valor, premissa ou nível tarifário.</p>
        <br>
        <p>Acessando o menu Gestão de Receitas / Inserir Tarifa Base.</p>
        <p>Abrindo a tela vamos:</p>
           <p>* Informar a Vigência.</p>
           <p>* O Tipo de Edição.</p>
           <p>* A Moeda.</p>
           <p>* E em quais dias da semana ela será aplicada.</p>
        <p>Temos o acesso a uma lista de tarifas base e aos cadastros de premissa, tipo de pensão e
        acordo comercial.</p>
        <p>A configuração da Tarifa Base vai depender do que foi definido no campo Tipo de Edição.</p>
        <p>Sendo ela feita pelo tipo valor:</p>
           <p>* Devemos informar os valores de acordo com a sugestão do cadastro e características das UHs.</p>
           <p>* Configurar a pensão, utilizando os valores da pensão padrão ou ajustando os valores da mesma.</p>
        <p>Por Premissa:</p>
           <p>* De acordo com o cadastro de premissas e sua disponibilidade de período, basta:</p>
           <p>* Selecionar a premissa.</p>
           <p>* Indicar o valor da diária referência.</p>
           <p>* Salvar.</p>
           <p>* Configurar a pensão, utilizando os valores da pensão padrão ou ajustando os valores da mesma</p>
        <p>Por Nível Tarifário:</p>
           <p>* Indicando o nível, a moeda e quais os dias da semana que será aplicado.</p>
        <p>Clicar em Salvar, para validar a operação.</p>
	 `,
      tdnUrl: '',
    },
    {
      id: '33',
      categoryId: '1',
      category: 'menu.register',
      title: 'Como realizar o cadastro de uma Premissa',
      categoryImage: 'ht-register.svg',
      categoryImageIdle: 'ht-register-gray.svg',
      categoryColor: '#7ECFD6',
      time: 3,
      tags: ['menu.register', 'menu.others'],
      videoUrl: 'https://www.youtube.com/embed/NiX-H9KCCFE',
      description: `
        <h3><b>Como realizar o cadastro de uma Premissa</b></h3>
        <br>
        <p>Este cadastro tem como objetivo principal, facilitar o processo de ajuste tarifário,
        onde, por tirarmos o valor de uma UH como base, podemos atualizar as demais de acordo
        com as premissas definidas em cada UH.</p>
        <p>Acessando o menu Gestão de Receitas / Cadastro de Premissas, já podemos ver as premissas
        cadastradas. Onde o sistema nos permite editar ou excluir uma premissa.</p>
        <p>Para este tutorial, vamos clicar em Inserir Nova Premissa e:</p>
           <p>* Informar o Nome da Premissa.</p>
           <p>* Período de Vigência.</p>
           <p>* O Tipo de UH que será utilizada como referência.</p>
           <p>* Quantidade de PAX.</p>
           <p>* E a Moeda.</p>
        <p>Passando para Configurações Gerais, iremos:</p>
           <p>* Na UH Referência, podemos acrescentar um valor fixo, somar ou subtrair um valor de
           acordo com a base de valor desta UH referência.</p>
           <p>* Iremos repetir o processo para as demais UHs, ajustando os valores de acordo como
           a necessidade e características de cada UH que foi definida em seu cadastro.</p>
        <p>Feito isso, clicar em Confirmar, para validar a operação.</p>
	 `,
      tdnUrl: '',
    },
    {
      id: '34',
      category: 'menu.finance',
      categoryId: '5',
      title: 'Como aplicar Diária Referência e Nível Tarifário',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 2,
      tags: ['menu.finance', 'menu.others'],
      videoUrl: 'https://www.youtube.com/embed/E5OEX7olbdI',
      description: `
        <h3><b>Como aplicar Diária Referência e Nível Tarifário</b></h3>
        <br>
        <p>Este processo facilita o Gerenciamento do Tarifário, possibilitando alterações de tarifas
        de acordo com o calendário.</p>
        <p>Vamos abrir o Calendário em Gestão de Receitas / Calendário de Tarifas.</p>
        <p>Na parte superior a direita temos acesso aos cadastros de Tarifa Base, Tipo de Pensão e Acordo Comercial.</p>
        <p>O botão Exibir Hoje deve ser usado quando estamos andando pelos botões de navegação, dispostos nas
        laterais e precisamos voltar a data vigente.</p>
        <p>Para inserirmos uma tarifa base dentro do calendário de tarifas basta:</p>
        <p>Selecionar um dia ou período e clique na opção Inserir Tarifa Base.</p>
        <p>O sistema nos leva diretamente para o cadastro de tarifa base, onde podemos criar uma nova tarifa
        para o período selecionado.</p>
        <p>E para aplicarmos um nível tarifário basta:</p>
        <p>Selecionar um dia ou período e com um clique do mouse, selecionar a opção Aplicar Nível Tarifário.</p>
        <p>Em seguida, basta indicar qual nível tarifário será utilizado para o período selecionado.</p>
        <p>Podemos ainda:</p>
           <p>* Ver detalhes do calendário ao selecionar um dia.</p>
           <p>* Aplicar uma Diária Referência ao dia período selecionado.</p>
           <p>* Indicando o valor da Diária Referência no campo indicado.</p>
        <p>Desta forma, nós podemos aplicar Diária Referência e Nível Tarifário, no calendário de tarifas do Thex PMS.</p>
	 `,
      tdnUrl: '',
    },
    {
      id: '35',
      categoryId: '1',
      category: 'menu.register',
      title: 'Como realizar o Cadastro de uma Pensão',
      categoryImage: 'ht-register.svg',
      categoryImageIdle: 'ht-register-gray.svg',
      categoryColor: '#AB85E9',
      time: 1,
      tags: ['menu.register', 'menu.others'],
      videoUrl: 'https://www.youtube.com/embed/nz-c-82BcPY',
      description: `
        <h3><b>Como realizar o Cadastro de uma Pensão</b></h3>
        <br>
        <p>Este cadastro permite que se defina o total de pensões que hotel irá trabalhar, de acordo com a
        quantidade de referências disponibilizada pelo sistema.</p>
        <p>Acessando o menu Gestão de Receitas / Cadastro de Pensão, podemos verificar que temos sete
        referências para trabalhar e com isso, basta:</p>
           <p>* Inserir o nome da pensão no campo Tipo de Pensão.</p>
           <p>* Informar o código.</p>
           <p>* Ambos os campos de acordo com a referência.</p>
           <p>* Defina sua pensão como Ativa para que seja utilizada pelo sistema, caso contrário,
           desmarque o campo.</p>
           <p>* Por fim clique em Confirmar, para validar a operação.</p>
        <p>Esse foi o Cadastro de Pensão do Thex PMS.</p>
	 `,
      tdnUrl: '',
    },
    {
      id: '36',
      categoryId: '5',
      category: 'menu.finance',
      title: 'Como configurar a Pensão Padrão',
      categoryImage: 'ht-billing.svg',
      categoryImageIdle: 'ht-billing-gray.svg',
      categoryColor: '#40D9B4',
      time: 1,
      tags: ['menu.finance', 'menu.others'],
      videoUrl: 'https://www.youtube.com/embed/TCZEeQGKFJM',
      description: `
        <h3><b>Como configurar a Pensão Padrão</b></h3>
        <br>
        <p>Este cadastro permite a configuração de Pensão utilizada pelo hotel em um determinado período,
        ajustado ao valor da Pensão Padrão como base de cálculo.</p>
        <p>Acessando o menu Gestão de Receitas / Configurar Pensão, preencha:</p>
           <p>* O Período de Vigência.</p>
           <p>* A Moeda.</p>
           <p>* E configure a pensão, acrescentando ou descontando valores de acordo a pensão padrão,
           definida em Tipos de Pensão.</p>
        <p>Feito isso, basta clicar em Confirmar, para validar a operação.</p>
	 `,
      tdnUrl: '',
    },
    {
      id: '37',
      categoryId: '1',
      category: 'menu.register',
      title: 'Como realizar o Cadastro de um Nível Tarifário',
      categoryImage: 'ht-register.svg',
      categoryImageIdle: 'ht-register-gray.svg',
      categoryColor: '#AB85E9',
      time: 4,
      tags: ['menu.others'],
      videoUrl: 'https://www.youtube.com/embed/H_6JAjIJ94o',
      description: `
        <h3><b>Como realizar o Cadastro de um Nível Tarifário</b></h3>
        <br>
        <p>Este cadastro tem como objetivo principal, definir a estratégia tarifária utilizada pelo hotel,
        de acordo com períodos específicos, particularidades do hotel e de acordo com a demanda e oferta,
        facilitando a montagem de toda sua parte tarifária.</p>
        <p>Acessando o menu Gestão de Receitas / Nível Tarifário, vejamos primeiro o Cadastro de Nível de Tarifa.</p>
        <p>Podemos editar ou excluir um nível existente, neste caso, iremos incluir um novo nível.</p>
        <p>Agora basta:</p>
         <p>* Indicar o código de nível, numeral, indicado pelo sistema.</p>
         <p>* Digitar o nome do nível.</p>
         <p>* Definir a ordem de aparição no calendário de tarifas.</p>
         <p>* E por fim, definir que o mesmo está ativo.</p>
        <p>Clicar no botão Confirmar, para validar a operação.</p>
        <p>Agora novamente em Gestão de Receitas / Nível Tarifário, acessando o Cadastro de Tarifa por Nível,
        clicar no link tela de cadastro ou na opção Inserir Tarifa.</p>
        <p>Abrindo a tela vamos:</p>
         <p>* Informar a Vigência.</p>
         <p>* A Moeda.</p>
         <p>* Clicar no botão Aplicar.</p>
         <p>* Indicar o Nível que será utilizado.</p>
         <p>* O nome da Tarifa.</p>
        <p>O Botão Limpar Tudo, exclui todas as informações preenchidas até o momento.</p>
        <p>Agora iremos definir como utilizar o nível:</p>
         <p>* Inserindo valores em cada UH, de acordo com o cadastro da mesma.</p>
           Repare que o sistema nos informa os valores aceitáveis para este preenchimento.</p>
        <p>E para a pensão especial, nós podemos:</p>
         <p>* Podemos também utilizar o valor padrão.</p>
         <p>* Ou ainda pela atualização de valores a pensão padrão.</p>
        <p>Agora, basta clicar em Salvar para validar a operação.</p>
      `,
      tdnUrl: '',
    }
  ];
}
