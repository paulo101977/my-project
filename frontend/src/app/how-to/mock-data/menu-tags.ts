import { Tag } from '@inovacao-cmnet/thx-ui';
import {
  CardData,
  CARD_REGISTER,
  CARD_RESERVATION,
  CARD_ACCOMMODATION,
  CARD_GOVERNANCE,
  CARD_FINANCE,
  CARD_INTEGRATION,
  CARD_MANAGEMENT,
  CARD_REPORT,
  CARD_OTHERS
} from './card-data';

export const MENU_TAGS = 'MENU_TAGS';

export class MenuTags {
  public static readonly MENU_TAGS = <Tag[]>[
    {
      id: 1,
      title: 'menu.register',
      idleSvg: 'ht-register-gray.svg',
      activeSvg: 'ht-register.svg',
      color: 'var(--color-register)',
      cardList: CardData[CARD_REGISTER]
    },
    {
      id: 2,
      title: 'menu.reservation',
      idleSvg: 'ht-bookings-gray.svg',
      activeSvg: 'ht-bookings.svg',
      color: 'var( --color-reservation)',
      cardList: CardData[CARD_RESERVATION]
    },
    {
      id: 3,
      title: 'menu.accomodation',
      idleSvg: 'ht-accommodation-gray.svg',
      activeSvg: 'ht-accommodation.svg',
      color: 'var(--color-accommodation)',
      cardList: CardData[CARD_ACCOMMODATION]
    },
    {
      id: 4,
      title: 'menu.housekeeping',
      idleSvg: 'ht-governance-gray.svg',
      activeSvg: 'ht-governance.svg',
      color: 'var(--color-governance)',
      cardList: CardData[CARD_GOVERNANCE]
    },
    {
      id: 5,
      title: 'menu.finance',
      idleSvg: 'ht-billing-gray.svg',
      activeSvg: 'ht-billing.svg',
      color: 'var(--color-finance)',
      cardList: CardData[CARD_FINANCE]
    },
    {
      id: 6,
      title: 'menu.integration',
      idleSvg: 'ht-integration-gray.svg',
      activeSvg: 'ht-integration.svg',
      color: 'var(--color-integration)',
      cardList: CardData[CARD_INTEGRATION]
    },
    {
      id: 7,
      title: 'menu.rm',
      idleSvg: 'ht-management-gray.svg',
      activeSvg: 'ht-management.svg',
      color: 'var(--color-revenue-management)',
      cardList: CardData[CARD_MANAGEMENT]
    },
    {
      id: 8,
      title: 'menu.reports',
      idleSvg: 'ht-reports-gray.svg',
      activeSvg: 'ht-reports.svg',
      color: 'var(--color-report)',
      cardList: CardData[CARD_REPORT]
    },
    {
      id: 9,
      title: 'menu.others',
      idleSvg: 'ht-others-gray.svg',
      activeSvg: 'ht-others.svg',
      color: 'var(--color-blue)',
      cardList: CardData[CARD_OTHERS]
    },
  ];
}
