// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { ActivatedRoute } from '@angular/router';
// import { RouterTestingModule } from '@angular/router/testing';
// import { Card, Tag } from '@inovacao-cmnet/thx-ui';
// import { ArticleData, ARTICLES } from 'app/how-to/mock-data/article-data';
// import { Article } from 'app/how-to/models/article';
// import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
// import { configureTestSuite } from 'ng-bullet';
// import { ActivatedRouteStub } from '../../../../../testing';
// import { MENU_TAGS, MenuTags } from '../../mock-data/menu-tags';

// import { HowToComponent } from './how-to.component';

// describe('HowToComponent', () => {
//   let component: HowToComponent;
//   let fixture: ComponentFixture<HowToComponent>;

//   configureTestSuite(() => {
//     TestBed.configureTestingModule({
//       declarations: [
//         HowToComponent
//       ],
//       imports: [
//         TranslateTestingModule,
//         FormsModule,
//         ReactiveFormsModule,
//         RouterTestingModule,
//       ],
//       providers: [
//         { provide: ActivatedRoute, useClass: ActivatedRouteStub }
//       ],
//       schemas: [ NO_ERRORS_SCHEMA ]
//     });

//     fixture = TestBed.createComponent(HowToComponent);
//     component = fixture.componentInstance;

//     component.menuTags = MenuTags[MENU_TAGS];
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it('should loadAllCard', () => {
//     component.loadAllCard();

//     const article: Article = ArticleData[ARTICLES][0];
//     const card: Card = {
//       headerCard: article.title,
//       id: article.id,
//       idTag: article.categoryId,
//       imgCard: article.categoryImage,
//       tagLeft: article.tags[0],
//       tagRight: article.tags[1],
//       time: `${article.time} min`
//     };

//     expect(component.viewCardData).toContain(card);
//     expect(component.cardDataAux).toEqual(component.viewCardData);
//   });

//   it('should loadTagCard', () => {
//     component.loadTagCard(MenuTags[MENU_TAGS][0]);
//     expect(component.viewCardData[0]).toBeDefined();
//   });

//   it('should loadTagCard with search', () => {
//     spyOn(component, 'search');
//     component.searchWord = 'cada';

//     component.loadTagCard(MenuTags[MENU_TAGS][0]);
//     expect(component.search).toHaveBeenCalled();
//   });

//   it('should clearSearch', () => {
//     component.statusButton = true;
//     spyOn(component, 'loadTagCard');

//     component.clearSearch();
//     expect(component.loadTagCard).toHaveBeenCalled();
//   });

//   it('should checkSearchChanges search', () => {
//     spyOn(component, 'search');
//     component.checkSearchChanges('cada');
//     expect(component.search).toHaveBeenCalled();
//   });

//   it('should checkSearchChanges loadTagCard', () => {
//     spyOn(component, 'loadTagCard');
//     component.tagElement = <Tag> {
//       id: 3
//     };

//     component.checkSearchChanges('');
//     expect(component.loadTagCard).toHaveBeenCalled();
//   });
// });
