import { Component, HostBinding, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tag, Card, ThxTagsTabComponent } from '@inovacao-cmnet/thx-ui';
import { HowToService } from '../../service/how-to.service';

@Component({
  selector: 'app-how-to',
  templateUrl: './how-to.component.html',
  styleUrls: ['./how-to.component.scss']
})
export class HowToComponent implements OnInit {
  @ViewChild('tagTab') tagTab: ThxTagsTabComponent;
  @HostBinding('class.howTo') howTo = true;
  public menuTags: Array<Tag>;
  public viewCardData: Array<Card>;
  public cardDataAux: Array<Card>;
  public statusButton = false;
  public tagElement: Tag;
  public searchWord: string;
  public propertyId: string;

  constructor(
    private howToSvc: HowToService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.loadData();
  }

  public loadData() {
    this.menuTags = this.howToSvc.getTagsCard();
    this.loadAllCard();
  }

  public loadAllCard() {
    this.howToSvc.getAllCards().subscribe((cardList) => {
      this.cardDataAux = cardList;
      this.viewCardData = cardList;
    });
  }

  public checkSearchChanges(item) {
    this.searchWord = item;

    if (item && item.length > 3) {
      this.search(item);
    } else if (this.tagElement && this.tagElement.id) {
      this.loadTagCard(this.tagElement);
    } else {
      this.viewCardData = this.cardDataAux;
    }
  }

  public loadTagCard(tag: Tag) {
    this.tagElement = tag;
    const cardList = this.cardDataAux.filter(item => +item.idTag === this.tagElement.id );

    if (this.searchWord && this.searchWord.length > 3) {
      this.search(this.searchWord);
    } else {
      this.viewCardData = cardList;
    }
  }

  public search(term: any): void {
    this.viewCardData = this.howToSvc.search(term, this.cardDataAux,
      this.tagElement && this.tagElement.id ? this.tagElement.id : null);
  }

  public clearSearch() {
    this.searchWord = null;
    if (this.tagElement) {
      this.loadTagCard(this.tagElement);
    }
  }

  public clearFilter() {
    this.tagElement = null;
    this.viewCardData = this.cardDataAux;
    this.tagTab.unselectTag();
  }

  public goToArticle(card: Card) {
    this.router.navigate([card.id], { relativeTo: this.route });
  }
}
