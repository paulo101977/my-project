import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HowToService } from 'app/how-to/service/how-to.service';

@Component({
  selector: 'app-search-tag',
  templateUrl: './search-tag.component.html',
  styleUrls: ['./search-tag.component.scss']
})
export class SearchTagComponent implements OnInit, OnChanges {
  @Input() searchWrite: string;
  @Output() searchWord = new EventEmitter();
  @Output() closedSearch = new EventEmitter();

  public formHelp: FormGroup;
  public statusButton = false;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.setForm();
  }

  ngOnChanges (changes: SimpleChanges): void {
    if (this.formHelp) {
      this.formHelp.get('searchHelp').setValue(changes.searchWrite.currentValue);
    }
  }

  public setForm() {
    this.formHelp = this.formBuilder.group(
      {
        searchHelp: null
      }
    );

    this.formHelp.valueChanges.subscribe( item => {
      this.searchWord.emit(item.searchHelp);
    });
  }

  public changeViewSearch() {
    this.statusButton = !this.statusButton;
    if (!this.statusButton) {
      this.closedSearch.emit();
    }
  }

}
