import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTagComponent } from './search-tag.component';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('SearchTagComponent', () => {
  let component: SearchTagComponent;
  let fixture: ComponentFixture<SearchTagComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTagComponent ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(SearchTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
