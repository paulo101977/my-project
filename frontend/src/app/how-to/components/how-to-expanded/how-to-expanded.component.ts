import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article } from 'app/how-to/models/article';
import { HowToService } from 'app/how-to/service/how-to.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-how-to-expanded',
  templateUrl: './how-to-expanded.component.html',
  styleUrls: ['./how-to-expanded.component.css']
})
export class HowToExpandedComponent implements OnInit {

  public propertyId: number;
  public article$: Observable<Article>;

  constructor(
    private route: ActivatedRoute,
    private howToService: HowToService
  ) { }

  ngOnInit() {
    const { property, id } = this.route.snapshot.params;

    this.propertyId = property;

    this.article$ = this.howToService.getArticle(id);
  }

}
