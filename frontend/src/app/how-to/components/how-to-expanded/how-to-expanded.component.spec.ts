import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { Article } from 'app/how-to/models/article';
import { HowToService } from 'app/how-to/service/how-to.service';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { Observable, of } from 'rxjs';
import { BypassUrlTestingModule, ImgCdnPipeStub } from '@inovacao-cmnet/thx-ui';
import { ActivatedRouteStub, createServiceStub } from '../../../../../testing';

import { HowToExpandedComponent } from './how-to-expanded.component';

describe('HowToExpandedComponent', () => {
  let component: HowToExpandedComponent;
  let fixture: ComponentFixture<HowToExpandedComponent>;

  const howToProvider = createServiceStub(HowToService, {
    getArticle(id: string): Observable<Article> {
      return of(null);
    }
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ HowToExpandedComponent, ImgCdnPipeStub ],
      providers: [
        howToProvider,
        {provide: ActivatedRoute, useClass: ActivatedRouteStub},
      ],
      imports: [ TranslateTestingModule, BypassUrlTestingModule ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(HowToExpandedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
