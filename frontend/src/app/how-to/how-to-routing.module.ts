import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HowToExpandedComponent } from 'app/how-to/components/how-to-expanded/how-to-expanded.component';
import { HowToComponent } from 'app/how-to/components/how-to/how-to.component';

const routes: Routes = [
  {path: '', component: HowToComponent},
  {path: ':id', component: HowToExpandedComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HowToRoutingModule {}
