import { TestBed } from '@angular/core/testing';
import { ArticleData, ARTICLES } from 'app/how-to/mock-data/article-data';
import { HowToService } from 'app/how-to/service/how-to.service';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('HowToService', () => {
  let service: HowToService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule
      ]
    });

    service = TestBed.get(HowToService);
  });

  describe('#getArticle', () => {
    it('should get return an article with the corresponding ID', () => {
      service.getArticle('2').subscribe((article) => {
        expect(article).not.toBeNull();
        expect(article).not.toBeUndefined();
      });
    });

    it('should return nothing if there is no article', () => {
      service.getArticle(Symbol('2').toString()).subscribe((article) => {
        expect(article).toBeUndefined();
      });
    });
  });

  describe('#getAllArticles', () => {
    it('should return all articles', () => {
      const mockArticles = ArticleData[ARTICLES];

      service.getAllArticles().subscribe((articles) => {
        expect(articles[0].id).toBe(mockArticles[0].id);
      });
    });
  });

  describe('#getAllCards', () => {
    it('should return all articles as cards', () => {
      const mockArticle = ArticleData[ARTICLES][0];
      const mockCard = {
        id: mockArticle.id,
        idTag: mockArticle.categoryId,
        time: `${mockArticle.time} min`,
        headerCard: mockArticle.title,
        imgCard: mockArticle.categoryImage,
        tagLeft: mockArticle.tags[0],
        tagRight: mockArticle.tags[1],
      };

      service.getAllCards().subscribe((cards) => {
        expect(cards[0].id).toBe(mockCard.id);
      });
    });
  });
});
