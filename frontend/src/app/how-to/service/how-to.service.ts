import { Injectable } from '@angular/core';
import { Card } from '@inovacao-cmnet/thx-ui';
import { TranslateService } from '@ngx-translate/core';
import { ArticleData, ARTICLES } from 'app/how-to/mock-data/article-data';
import { Article } from 'app/how-to/models/article';
import { from, Observable, of } from 'rxjs';
import { filter, map, switchMap, take, toArray } from 'rxjs/operators';
import { MENU_TAGS, MenuTags } from '../mock-data/menu-tags';

@Injectable({
  providedIn: 'root'
})
export class HowToService {
  constructor(
    public translateService: TranslateService,
  ) {}

  public getTagsCard() {
    const menuTag = MenuTags[MENU_TAGS];
    menuTag.map(item => {
      item.title = this.translateService.instant(item.title);
    });
    return menuTag;
  }

  public getAllCards(): Observable<Card[]> {
    return this.getAllArticles().pipe(
      switchMap(articles => from(articles)),
      map((article: Article): Card => ({
        id: article.id,
        idTag: article.categoryId,
        time: `${article.time} min`,
        headerCard: article.title,
        imgCard: article.categoryImage,
        tagLeft: article.tags[0],
        tagRight: article.tags[1],
      })),
      toArray(),
      take(1)
    );
  }

  public search(term: any, cardDataAux: Array<Card>, tagId?: number ) {
    if (typeof term === 'string' && term !== '') {
       return cardDataAux.filter(
        item => tagId ?
          RegExp(term, 'i').test(item.headerCard) && +item.idTag === tagId :
          RegExp(term, 'i').test(item.headerCard)
      );
    } else {
      return null;
    }
  }

  public getAllArticles(): Observable<Article[]> {
    return from(ArticleData[ARTICLES]).pipe(
      map(item => ({
        ...item,
        category: this.translateService.instant(item.category),
        tags: item.tags.map(tag => this.translateService.instant(tag))
      })),
      toArray(),
      take(1)
    );
  }

  public getArticle(id: string): Observable<Article> {
    return this.getAllArticles().pipe(
      switchMap(articles => from(articles)),
      filter((article: Article) => article.id == id),
      take(1)
    );
  }
}
