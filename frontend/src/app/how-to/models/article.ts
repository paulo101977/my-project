export interface Article {
  id: string;
  title: string;
  categoryId: string;
  category: string;
  categoryImage: string;
  categoryImageIdle?: string;
  categoryColor: string;
  time: number;
  tags: string[];
  videoUrl?: string;
  description: string;
  tdnUrl?: string;
}
