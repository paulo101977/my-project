import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HowToRoutingModule } from './how-to-routing.module';
import { HowToComponent } from './components/how-to/how-to.component';
import { TranslateModule } from '@ngx-translate/core';
import { ImgCdnModule, ThxCardModule, ThxTagsTabModule, ThxVideoModule } from '@inovacao-cmnet/thx-ui';
import { HowToExpandedComponent } from './components/how-to-expanded/how-to-expanded.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchTagComponent } from './components/search-tag/search-tag.component';

@NgModule({
  imports: [
    CommonModule,
    HowToRoutingModule,
    TranslateModule,
    ThxTagsTabModule,
    ThxCardModule,
    ImgCdnModule,
    ThxVideoModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    HowToComponent,
    HowToExpandedComponent,
    SearchTagComponent
  ]
})
export class HowToModule { }
