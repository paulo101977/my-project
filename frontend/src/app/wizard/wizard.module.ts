import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import { ImgCdnModule, ThxImgModule, ThxStepsModule, ThxToasterV2Module } from '@inovacao-cmnet/thx-ui';
import {TranslateModule} from '@ngx-translate/core';
import { SharedModule } from 'app/shared/shared.module';
import {ModalStepperModule} from './modal-stepper/modal-stepper.module';
import {WizardRoutingModule} from './wizard-routing.module';
import {LeftCardComponent} from './components/left-card/left-card.component';
import {WizardComponent} from './components/wizard/wizard.component';
import {BedTypeCounterPipe} from 'app/wizard/rooms-step/components/popover/pipe/bedType-counter.pipe';

@NgModule({
  imports: [
    CommonModule,
    WizardRoutingModule,
    TranslateModule,
    ThxImgModule,
    ThxStepsModule,
    ImgCdnModule,
    ModalStepperModule,
    SharedModule,
    ThxToasterV2Module,
  ],
  exports: [
    BedTypeCounterPipe
  ],
  declarations: [
    LeftCardComponent,
    WizardComponent,
    BedTypeCounterPipe
  ]
})
export class WizardModule { }

