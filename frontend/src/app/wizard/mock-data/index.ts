import { StepInfo } from 'app/wizard/models/step-info';

export const STEP = 'STEP';
export const STEP_MOCK = 'STEP_MOCK';

export class Data {
  public static readonly STEP_MOCK  = <StepInfo[]> [
    {
      stepId: 1,
      titleStep: 'Hospedagem',
      textExplain: `Neste módulo, é possível cadastrar seus tipos de acomodação e suas acomodações,
       seguindo opções como quantidade máxima de pessoas para o quarto, layout de camas e muito mais.`,
      textAddition: `O seu inventário pode ser distribuído para os Channels de mercado (Booking, Expedia, etc)
       adquirindo a integração do THEx PMS com o THEx HIGS, Channel Manager que envia a disponibilidade dos seus
       quartos para esses canais. Caso você tenha adquirido o HIGS, você deverá informar o código da acomodação
       para os canais e sua porcentagem de distribuição, na caixa de opções do HIGS na etapa de HOSPEDAGEM.`,
      imgStep: 'w-step1-booking.svg',
      videoStep: ''
    },
  ];

  public static readonly STEP  = <StepInfo[]> [
    {
      stepId: 1,
      titleStep: 'menu.accomodation',
      textExplain: 'text.step1TextExplain',
      textAddition: 'text.step1TextAddition',
      imgStep: 'w-step1-booking.svg',
      videoStep: ''
    },
    {
      stepId: 2,
      titleStep: 'menu.releases',
      textExplain: 'text.step2TextExplain',
      textAddition: 'text.step2TextAddition',
      imgStep: 'w-step2-launch.svg',
      videoStep: ''
    },
    {
      stepId: 3,
      titleStep: 'menu.rate',
      textExplain: 'text.step3TextExplain',
      textAddition: 'text.step3TextAddition',
      imgStep: 'w-step3-rate.svg',
      videoStep: ''
    },
    {
      stepId: 4,
      titleStep: 'menu.managementUsers',
      textExplain: 'text.step4TextExplain',
      textAddition: 'text.step4TextAddition',
      imgStep: 'w-step4-user-management.svg',
      videoStep: ''
    },
  ];
}
