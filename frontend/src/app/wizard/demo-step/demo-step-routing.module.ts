import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoComponent } from 'app/wizard/demo-step/components/demo/demo.component';
import { TypeDemoComponent } from 'app/wizard/demo-step/components/type-demo/type-demo.component';
import { DemoStepComponent } from './components/demo-step/demo-step.component';

const routes: Routes = [
  {
    path: '',
    component: DemoStepComponent,
    children: [
      {path: '', redirectTo: 'type-demo', pathMatch: 'full'},
      {path: 'type-demo', component: TypeDemoComponent},
      {path: 'demo', component: DemoComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoStepRoutingModule { }
