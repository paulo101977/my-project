import { Component, OnInit } from '@angular/core';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

  constructor(public modalStepperService: ModalStepperService) { }

  ngOnInit() {
    this.modalStepperService.setValid(false);
  }

}
