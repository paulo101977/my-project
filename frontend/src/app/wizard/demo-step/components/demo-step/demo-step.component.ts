import { Component, OnInit } from '@angular/core';
import { Step } from '@inovacao-cmnet/thx-ui';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-demo-step',
  templateUrl: './demo-step.component.html',
  styleUrls: ['./demo-step.component.css'],
  providers: [ ModalStepperService ]
})
export class DemoStepComponent implements OnInit {
  steps$: Observable<Step[]>;
  stepsActions: any;
  isValid$: Observable<boolean>;

  constructor(
    private modalStepperService: ModalStepperService,
  ) { }

  ngOnInit() {
    this.isValid$ = this.modalStepperService.isValid$;
    this.steps$ = this.modalStepperService.getSteps('uh', [
      {
        id: '1',
        title: 'Tipo de Demo',
      },
      {
        id: '2',
        title: 'Demo',
        disabled: true
      }
    ]);

    this.stepsActions = {
      '1': {
        next: () => this.goToDemo(),
        select: () => this.modalStepperService.navigate('type-demo'),
      },
      '2': {
        next: () => this.goToOther(),
        select: () => this.modalStepperService.navigate('demo'),
      }
    };
  }

  public next() {
    this.modalStepperService.nextStep(this.stepsActions);
  }

  public previous() {
    this.modalStepperService.previousStep(this.stepsActions);
  }

  public selectStep(selectedStep: Step) {
    this.modalStepperService.selectStep(selectedStep.id, this.stepsActions);
  }

  public goToDemo() {
    return this.modalStepperService.navigate('demo');
  }

  public goToOther() {
    return of();
  }
}
