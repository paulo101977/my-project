import { Component, OnInit } from '@angular/core';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';

@Component({
  selector: 'app-type-demo',
  templateUrl: './type-demo.component.html',
  styleUrls: ['./type-demo.component.css']
})
export class TypeDemoComponent implements OnInit {

  constructor(public modalStepperService: ModalStepperService) { }

  ngOnInit() {
    this.modalStepperService.setValid(false);
  }

}
