import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalStepperModule } from 'app/wizard/modal-stepper/modal-stepper.module';
import { DemoStepComponent } from './components/demo-step/demo-step.component';

import { DemoStepRoutingModule } from './demo-step-routing.module';
import { TypeDemoComponent } from './components/type-demo/type-demo.component';
import { DemoComponent } from './components/demo/demo.component';

@NgModule({
  imports: [
    CommonModule,
    DemoStepRoutingModule,
    ModalStepperModule
  ],
  declarations: [DemoStepComponent, TypeDemoComponent, DemoComponent]
})
export class DemoStepModule { }
