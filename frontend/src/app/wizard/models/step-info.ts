export interface StepInfo {
  stepId: number;
  titleStep: string;
  textExplain: string;
  textAddition: string;
  imgStep: string;
  videoStep?: string;
}
