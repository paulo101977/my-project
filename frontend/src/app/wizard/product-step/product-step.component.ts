import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Step } from '@inovacao-cmnet/thx-ui';
import { TranslateService } from '@ngx-translate/core';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';
import { from, Observable } from 'rxjs';

@Component({
  selector: 'app-product-step',
  template: `
      <app-modal-stepper
        [steps]="steps$ | async"
        [isValid]="isValid$ | async"
        (selectedStep)="selectStep($event)"
        (nextStep)="next()"
        (previousStep)="previous()"
      ></app-modal-stepper>
  `
})
export class ProductStepComponent implements OnInit {
  steps$: Observable<Step[]>;
  stepsActions: any;
  isValid$: Observable<boolean>;

  constructor(
    private modalStepperService: ModalStepperService,
    private translateService: TranslateService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.isValid$ = this.modalStepperService.isValid$;
    this.steps$ = this.modalStepperService.getSteps('product', [
      {
        id: '1',
        title: this.translateService.instant('title.group'),
      },
      {
        id: '2',
        title: this.translateService.instant('title.products'),
      },
      {
        id: '3',
        title: this.translateService.instant('title.myFridge'),
      }
    ]);

    this.stepsActions = {
      '1': {
        next: () => this.goToProduct(),
        select: () => this.navigate('group'),
      },
      '2': {
        next: () => this.goToProduct(),
        select: () => this.navigate('products'),
      },
      '3': {
        next: () => this.goToProduct(),
        select: () => this.navigate('my-fridge'),
      },
    };
  }

  public next() {
    this.modalStepperService.nextStep(this.stepsActions);
  }

  public previous() {
    this.modalStepperService.previousStep(this.stepsActions);
  }

  public selectStep(selectedStep: Step) {
    this.modalStepperService.selectStep(selectedStep.id, this.stepsActions);
  }

  public goToProduct() {
    return this.navigate('product');
  }

  public navigate(route: string): Observable<boolean> {
    return from(this.router.navigate([route], {relativeTo: this.route}));
  }
}
