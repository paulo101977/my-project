import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalStepperModule } from 'app/wizard/modal-stepper/modal-stepper.module';

import { ProductStepRoutingModule } from './product-step-routing.module';
import { ProductStepComponent } from './product-step.component';
import { ProductsComponent } from './components/products/products.component';

@NgModule({
  declarations: [ProductStepComponent, ProductsComponent],
  imports: [
    CommonModule,
    ProductStepRoutingModule,
    ModalStepperModule
  ]
})
export class ProductStepModule { }
