import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from 'app/wizard/product-step/components/products/products.component';
import { ProductStepComponent } from 'app/wizard/product-step/product-step.component';

const routes: Routes = [
  {
    path: '',
    component: ProductStepComponent,
    children: [
      {path: '', redirectTo: 'products', pathMatch: 'full'},
      {path: 'products', component: ProductsComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductStepRoutingModule { }
