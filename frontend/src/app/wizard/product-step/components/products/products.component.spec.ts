import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';
import { configureTestSuite } from 'ng-bullet';
import { createServiceStub } from '../../../../../../testing';

import { ProductsComponent } from './products.component';

describe('ProductsComponent', () => {
  let component: ProductsComponent;
  let fixture: ComponentFixture<ProductsComponent>;

  const modalStepperServiceStub = createServiceStub(ModalStepperService, {
    setValid(valid: boolean) { return false; },
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsComponent ],
      providers: [ modalStepperServiceStub ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    fixture = TestBed.createComponent(ProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
