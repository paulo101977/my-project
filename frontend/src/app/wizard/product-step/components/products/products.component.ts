import { Component, OnInit } from '@angular/core';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(
    private modalStepperService: ModalStepperService
  ) { }

  ngOnInit() {
    this.modalStepperService.setValid(false);
  }

}
