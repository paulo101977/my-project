import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Step } from '@inovacao-cmnet/thx-ui';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';
import { modalStepperProvider } from 'app/wizard/modal-stepper/testing/modal-stepper.provider';
import { RoomsStepComponent } from 'app/wizard/rooms-step/rooms-step.component';
import { configureTestSuite } from 'ng-bullet';
import { ActivatedRouteStub } from '../../../../testing';

import { ProductStepComponent } from './product-step.component';

describe('ProductStepComponent', () => {
  let component: ProductStepComponent;
  let fixture: ComponentFixture<ProductStepComponent>;
  let modalStepperService: ModalStepperService;
  let router: Router;

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        declarations: [ ProductStepComponent ],
        imports: [ RouterTestingModule, TranslateTestingModule ],
        providers: [
          {provide: ActivatedRoute, useClass: ActivatedRouteStub}
        ],
        schemas: [ NO_ERRORS_SCHEMA ],
      })
      .overrideComponent(RoomsStepComponent, {
        set: {
          providers: [modalStepperProvider]
        }
      });

    router = TestBed.get(Router);

    fixture = TestBed.createComponent(ProductStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    modalStepperService = component['modalStepperService'];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#next', () => {
    it('should call nextStep', () => {
      spyOn(modalStepperService, 'nextStep');

      component.next();
      expect(modalStepperService.nextStep).toHaveBeenCalled();
    });
  });

  describe('#previous', () => {
    it('should call previousStep', () => {
      spyOn(modalStepperService, 'previousStep');

      component.previous();
      expect(modalStepperService.previousStep).toHaveBeenCalled();
    });
  });

  describe('#selectStep', () => {
    it('should call selectStep', () => {
      spyOn(modalStepperService, 'selectStep');
      const step: Step = { active: false, complete: false, disabled: false, id: '', title: '' };

      component.selectStep(step);
      expect(modalStepperService.selectStep).toHaveBeenCalled();
    });
  });

  describe('#navigate', () => {
    it('should ', function () {
      spyOn(router, 'navigate').and.returnValue(Promise.resolve(true));

      component.navigate('')
        .subscribe(() => {
          expect(router.navigate).toHaveBeenCalled();
        });
    });
  });
});
