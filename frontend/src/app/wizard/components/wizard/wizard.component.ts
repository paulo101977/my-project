import { Component, OnInit } from '@angular/core';
import { Step } from '@inovacao-cmnet/thx-ui';
import { StepInfo } from 'app/wizard/models/step-info';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss']
})
export class WizardComponent implements OnInit {

  public stepInfo: StepInfo;
  public currentStep: Step;
  public titleStep: string;

  constructor() { }

  ngOnInit() {

  }

}
