import { Component, Input, OnInit } from '@angular/core';
import { StepInfo } from 'app/wizard/models/step-info';


@Component({
  selector: 'app-left-card',
  templateUrl: './left-card.component.html',
  styleUrls: ['./left-card.component.scss']
})
export class LeftCardComponent implements OnInit {
  @Input() stepInfo: StepInfo;

  constructor() { }

  ngOnInit() {
  }

}
