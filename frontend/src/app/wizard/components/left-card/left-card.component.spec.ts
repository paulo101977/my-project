import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

import { LeftCardComponent } from './left-card.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { imgCdnStub } from '../../../../../testing';

describe('LeftCardComponent', () => {
  let component: LeftCardComponent;
  let fixture: ComponentFixture<LeftCardComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        LeftCardComponent,
        imgCdnStub
      ],
      imports: [
        TranslateTestingModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(LeftCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
