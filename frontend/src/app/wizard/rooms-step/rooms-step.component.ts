import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Step } from '@inovacao-cmnet/thx-ui';
import { TranslateService } from '@ngx-translate/core';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';
import { from, Observable, of } from 'rxjs';

@Component({
  selector: 'app-rooms-step',
  template: `
      <app-modal-stepper
        [steps]="steps$ | async"
        [isValid]="isValid$ | async"
        (selectedStep)="selectStep($event)"
        (nextStep)="next()"
        (previousStep)="previous()"
      ></app-modal-stepper>
  `,
  providers: [ ModalStepperService ]
})
export class RoomsStepComponent implements OnInit {
  steps$: Observable<Step[]>;
  stepList: Step[];
  stepsActions: any;
  isValid$: Observable<boolean>;

  constructor(
    private modalStepperService: ModalStepperService,
    private translateService: TranslateService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.isValid$ = this.modalStepperService.isValid$;
    this.steps$ = this.modalStepperService.getSteps('uh', [
      {
        id: '1',
        title: this.translateService.instant('label.uhType'),
      },
      {
        id: '2',
        title: this.translateService.instant('label.uh')
      }
    ]);

    this.stepsActions = {
      '1': {
        next: () => this.goToRoom(),
        select: () => this.navigate('room-type'),
      },
      '2': {
        next: () => this.goToNext(),
        select: () => of(null),
      }
    };

    this.checkActiveStep(this.steps$);
  }


  /** TODO: Use the stepper to set the activeStep */
  public checkActiveStep(steps$: Observable<Step[]>) {
    steps$.subscribe(steps => { this.stepList = steps; });

    if (this.router.url.includes('/room-type')) {
      this.stepList[0].active = true;
    } else {  this.stepList[1].active = true; }

  }

  public next() {
    this.modalStepperService.nextStep(this.stepsActions);
  }

  public previous() {
    this.modalStepperService.previousStep(this.stepsActions);
  }

  public selectStep(selectedStep: Step) {
    this.modalStepperService.selectStep(selectedStep.id, this.stepsActions);
  }

  public goToRoom() {
    return this.navigate('room');
  }

  public goToNext() {
    return of();
  }

  public navigate(route: string): Observable<boolean> {
    return from(this.router.navigate([route], {relativeTo: this.route}));
  }
}
