import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ThxButtonV2Module,
  ThxSubtitleV2Module,
  ThxTitleV2Module,
  ThxInputV2Module,
  CardBaseModule,
  ContainerListModule,
  ThxIconTextModule,
  ThxCounterModule,
  ThxTextareaV2Module,
  ThxSelectV2Module,
  ThxImgModule,
  RequiredModule
} from '@inovacao-cmnet/thx-ui';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'app/shared/shared.module';

import { ModalStepperModule } from 'app/wizard/modal-stepper/modal-stepper.module';
import { RoomsStepRoutingModule } from './rooms-step-routing.module';
import { RoomTypeComponent } from './components/room-type/room-type.component';
import { RoomComponent } from './components/room/room.component';
import { RoomsStepComponent } from './rooms-step.component';
import { RoomTypeCardComponent } from './components/room-type-card/room-type-card.component';
import { RoomCardComponent } from './components/room-card/room-card.component';
import { RoomTypePopoverComponent } from 'app/wizard/rooms-step/components/popover/popover.component';
import {WizardModule} from 'app/wizard/wizard.module';

@NgModule({
  imports: [
    CommonModule,
    RoomsStepRoutingModule,
    ModalStepperModule,
    ThxTitleV2Module,
    ThxSubtitleV2Module,
    ThxButtonV2Module,
    ThxInputV2Module,
    TranslateModule,
    SharedModule,
    ContainerListModule,
    CardBaseModule,
    ThxIconTextModule,
    ThxCounterModule,
    ThxTextareaV2Module,
    ThxImgModule,
    ThxSelectV2Module,
    WizardModule,
    RequiredModule
  ],
  declarations: [
    RoomTypeComponent,
    RoomComponent,
    RoomsStepComponent,
    RoomTypeCardComponent,
    RoomCardComponent,
    RoomTypePopoverComponent
  ]
})
export class RoomsStepModule { }
