import { configureTestSuite } from 'ng-bullet';
import { RoomsStepModule } from './rooms-step.module';

describe('RoomsStepModule', () => {
  let roomsStepModule: RoomsStepModule;

  configureTestSuite(() => {
    roomsStepModule = new RoomsStepModule();
  });

  it('should create an instance', () => {
    expect(roomsStepModule).toBeTruthy();
  });
});
