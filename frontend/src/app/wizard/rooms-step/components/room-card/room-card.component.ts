import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Room } from '@app/shared/models/room';

@Component({
  selector: 'app-room-card',
  templateUrl: './room-card.component.html',
  styleUrls: ['./room-card.component.scss']
})
export class RoomCardComponent {

  @Input() room: Room;
  @Output() delete = new EventEmitter<Room>();
}
