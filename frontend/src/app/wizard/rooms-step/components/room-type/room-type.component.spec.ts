import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { propertyStorageServiceStub } from '@bifrost/storage/services/testing/property-storage-service.stub';
import { ImgCdnPipeStub, ThxToasterService, RequiredPipeStub } from '@inovacao-cmnet/thx-ui';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { RoomTypeFormService } from 'app/room-type/shared/services/room-type-form/room-type-form.service';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';
import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { modalStepperProvider } from 'app/wizard/modal-stepper/testing/modal-stepper.provider';
import { configureTestSuite } from 'ng-bullet';
import { of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing';

import { RoomTypeComponent } from './room-type.component';

describe('RoomTypeComponent', () => {
  let component: RoomTypeComponent;
  let fixture: ComponentFixture<RoomTypeComponent>;
  let roomTypeResource: RoomTypeResource;

  const roomTypeFormProvider = createServiceStub(RoomTypeFormService, {
    addCourtesy: jasmine.createSpy('addCourtesy'),
    getRoomType: jasmine.createSpy('getRoomType'),
    form: jasmine.createSpyObj('form', ['invalid', 'valid', 'reset'])
  });

  const roomTypeResourceProvider = createServiceStub(RoomTypeResource, {
    createRoomType: jasmine.createSpy('createRoomType').and.returnValue(of([])),
    getRoomTypesByPropertyId: jasmine.createSpy('getRoomTypesByPropertyId').and.returnValue(of([])),
  });

  const propertyStorageProvider = createServiceStub(PropertyStorageService, {
    getCurrentPropertyId: jasmine.createSpy('getCurrentPropertyId')
  });

  const thxToasterServiceStub = createServiceStub(ThxToasterService, {
    showMessage(config: any): void {}
  });

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        declarations: [ RoomTypeComponent, CurrencyFormatPipe, ImgCdnPipeStub, RequiredPipeStub ],
        schemas: [ NO_ERRORS_SCHEMA ],
        providers: [
          roomTypeResourceProvider,
          propertyStorageProvider,
          modalStepperProvider,
          thxToasterServiceStub,
        ],
        imports: [TranslateTestingModule]
      })
      .overrideComponent(RoomTypeComponent, {
        set: {
          providers: [roomTypeFormProvider]
        }
      });

    roomTypeResource = TestBed.get(RoomTypeResource);

    fixture = TestBed.createComponent(RoomTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#save', () => {
    it('should create an room type and update the list', () => {
      const roomType = 'a';
      (<jasmine.Spy> component['roomTypeForm'].getRoomType).and.returnValue(roomType);
      spyOn<any>(component, 'updateList');

      component.save();

      expect(roomTypeResource.createRoomType).toHaveBeenCalledWith(roomType);
      expect(component['roomTypeForm'].form.reset).toHaveBeenCalled();
      expect(component['updateList']).toHaveBeenCalled();
    });
  });
});
