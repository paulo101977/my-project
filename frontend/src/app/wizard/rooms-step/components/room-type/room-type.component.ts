import {Component, OnInit, ViewChildren} from '@angular/core';
import { Validators } from '@angular/forms';
import { ThxToasterService } from '@inovacao-cmnet/thx-ui';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { RoomType } from 'app/room-type/shared/models/room-type';
import { RoomTypeFormService } from 'app/room-type/shared/services/room-type-form/room-type-form.service';
import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import {RoomTypeCardComponent} from 'app/wizard/rooms-step/components/room-type-card/room-type-card.component';

@Component({
  selector: 'app-room-type',
  templateUrl: './room-type.component.html',
  styleUrls: ['./room-type.component.scss'],
  providers: [RoomTypeFormService]
})
export class RoomTypeComponent implements OnInit {

  roomTypeList$: Observable<RoomType[]>;
  private propertyId: number;
  @ViewChildren('cardList') cardList: RoomTypeCardComponent[];

  constructor(
    public roomTypeForm: RoomTypeFormService,
    private roomTypeResource: RoomTypeResource,
    private propertyStorage: PropertyStorageService,
    private modalStepperService: ModalStepperService,
    private thxToasterService: ThxToasterService,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.propertyId = this.propertyStorage.getCurrentPropertyId();
    this.roomTypeForm.addCourtesy(0);
    this.roomTypeForm.addCourtesy(0);
    this.updateList();
  }

  public save() {
    const roomType = this.roomTypeForm.getRoomType(`${this.propertyId}`);
    this.roomTypeResource.createRoomType(roomType).subscribe(() => {
      this.roomTypeForm.form.reset({
        capacity: {
          adults: 0,
          children: 0,
        },
        extras: {
          cribs: 0,
          extraBeds: 0,
        },
        beds: {
          doubleBeds: 0,
          reversibleBeds: 0,
          singleBeds: 0,
        },
        courtesy: [0, 0]
      });
      const message = this.translateService.instant('variable.lbSaveSuccessM', {
        labelName: this.translateService.instant('label.room')
      });
      this.thxToasterService.showMessage({messages: message});
      this.updateList();
    });
  }

  public delete(roomType: RoomType) {
    this.roomTypeResource.deleteRoomType(roomType.id).subscribe(() => {
      const message = this.translateService.instant('variable.lbDeleteSuccessM', {
        labelName: this.translateService.instant('label.typeUHComplete')
      });
      this.thxToasterService.showMessage({messages: message});
      this.updateList();
    });
  }

  public closePopovers() {
    if (this.cardList && this.cardList.length) {
      this.cardList.forEach(item => item.popover = false);
    }
  }

  public getButtonClass(isValid: boolean) {
    return isValid ? 'arrow-blue-right.svg' : 'arrow-gray-right.svg';
  }

  private updateList() {
    this.roomTypeList$ = this.roomTypeResource.getRoomTypesByPropertyId(this.propertyId, true)
      .pipe(
        map(roomTypeList =>
          roomTypeList.filter(roomType => roomType.isActive)
        ),
        tap(roomTypeList => this.modalStepperService.setValid(!!roomTypeList.length))
      );
  }
}
