import {Component, EventEmitter, Input, Output} from '@angular/core';
import { RoomType } from 'app/room-type/shared/models/room-type';

@Component({
  selector: 'app-room-type-card',
  templateUrl: './room-type-card.component.html',
  styleUrls: ['./room-type-card.component.scss']
})
export class RoomTypeCardComponent {

  @Input() roomType: RoomType;
  @Output() delete = new EventEmitter<RoomType>();
  @Output() popoverState = new EventEmitter();
  public popover = false;

  public togglePopover() {
    if (this.popover === false) {
      this.popoverState.emit();
    }
    this.popover = !this.popover;
  }
}
