import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ImgCdnPipeStub } from '@inovacao-cmnet/thx-ui';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';

import { RoomTypeCardComponent } from './room-type-card.component';

describe('RoomTypeCardComponent', () => {
  let component: RoomTypeCardComponent;
  let fixture: ComponentFixture<RoomTypeCardComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateTestingModule ],
      declarations: [ RoomTypeCardComponent, ImgCdnPipeStub ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(RoomTypeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
