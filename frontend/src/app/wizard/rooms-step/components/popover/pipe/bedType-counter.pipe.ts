import { Pipe, PipeTransform } from '@angular/core';
import {BedType} from 'app/shared/models/bed-type';

@Pipe({
  name: 'bedTypeCounter'
})
export class BedTypeCounterPipe implements  PipeTransform {
  transform(bedTypeList: BedType[], value: number): any {
    let  counter = 0;
    bedTypeList.map(bedType => {
      if (bedType.bedType === value) {
        counter = bedType.count;
      }
    });
    return counter;
  }
}
