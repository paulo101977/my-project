import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RoomTypePopoverComponent } from './popover.component';
import {configureTestSuite} from 'ng-bullet';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateTestingModule} from 'app/shared/mock-test/translate-testing.module';
import {CurrencyFormatPipe} from 'app/shared/pipes/currency-format.pipe';
import {BedTypeCounterPipe} from 'app/wizard/rooms-step/components/popover/pipe/bedType-counter.pipe';

describe('RoomTypePopoverComponent', () => {
  let component: RoomTypePopoverComponent;
  let fixture: ComponentFixture<RoomTypePopoverComponent>;

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        declarations: [
          RoomTypePopoverComponent,
          BedTypeCounterPipe,
          CurrencyFormatPipe
        ],
        schemas: [ NO_ERRORS_SCHEMA ],
        providers: [],
        imports: [

          TranslateTestingModule
        ]
      });
    fixture = TestBed.createComponent(RoomTypePopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
