import {Component, Input } from '@angular/core';
import {RoomType} from 'app/room-type/shared/models/room-type';

@Component({
  selector: 'app-roomtype-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss']
})
export class RoomTypePopoverComponent {
  @Input() roomType: RoomType;
  @Input() popover: boolean;
}
