import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { roomTypeResourceStub } from '@app/room-type/shared/services/room-type-resource/testing';
import { roomFormStub } from '@app/room/shared/services/room-form/testing';
import { Room } from '@app/shared/models/room';
import { RoomResource } from '@app/shared/resources/room/room.resource';
import { roomResourceStub } from '@app/shared/resources/room/testing';
import { propertyStorageServiceStub } from '@bifrost/storage/services/testing/property-storage-service.stub';
import { ThxToasterService, RequiredPipeStub } from '@inovacao-cmnet/thx-ui';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { of } from 'rxjs';
import { createServiceStub, currencyFormatPipe, imgCdnStub, padStub } from 'testing';
import { RoomComponent } from './room.component';

describe('RoomComponent', () => {
  let component: RoomComponent;
  let fixture: ComponentFixture<RoomComponent>;
  let roomResource: RoomResource;

  const thxToasterServiceStub = createServiceStub(ThxToasterService, {
    showMessage(config: any): void {}
  });

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        declarations: [ RoomComponent, currencyFormatPipe, imgCdnStub, padStub, RequiredPipeStub ],
        schemas: [ NO_ERRORS_SCHEMA ],
        providers: [
          roomResourceStub,
          roomTypeResourceStub,
          propertyStorageServiceStub,
          thxToasterServiceStub,
        ],
        imports: [TranslateTestingModule]
      })
      .overrideComponent(RoomComponent, {
        set: {
          providers: [roomFormStub]
        }
      });

    roomResource = TestBed.get(RoomResource);

    fixture = TestBed.createComponent(RoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#save', () => {
    it('should create a room and update the list', () => {
      const propertyId = 1;

      component.roomTypeSelected = {id: 1};
      component.roomTypeList = [{text: '', value: ''}];
      spyOn(component['roomForm'], 'getRoom').and.returnValue(new Room());
      spyOn(component['roomResource'], 'createRoom').and.returnValue(of(null));
      spyOn(component['roomForm'].form, 'reset');
      spyOn<any>(component, 'updateList');

      component['propertyId'] = propertyId;
      component.save();

      expect(component['roomForm'].getRoom).toHaveBeenCalledWith(propertyId);
      expect(roomResource['createRoom']).toHaveBeenCalled();
      expect(component['roomForm'].form.reset).toHaveBeenCalled();
      expect<any>(component['updateList']).toHaveBeenCalled();
    });
  });

  describe('#delete', () => {
    it('should delete a room and update the list', () => {
      const propertyId = 1;
      const room = component['roomForm'].getRoom(propertyId);

      spyOn<any>(component, 'updateList');

      component.delete(room);

      expect(component['updateList']).toHaveBeenCalled();
    });
  });
});
