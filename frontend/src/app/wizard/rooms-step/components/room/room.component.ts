import { Component, OnInit } from '@angular/core';
import { RoomFormService } from '@app/room/shared/services/room-form/room-form.service';
import { RoomTypeResource } from '@app/room-type/shared/services/room-type-resource/room-type.resource';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { BedTypeService } from '@app/room-type/shared/services/bed-type/bed-type.service';
import { RoomTypeView } from '@app/shared/models/room-type-view';
import { RoomResource } from '@app/shared/resources/room/room.resource';
import { Room } from '@app/shared/models/room';
import { toSelectOption, SelectOptionItem, ThxToasterService } from '@inovacao-cmnet/thx-ui';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
  providers: [RoomFormService]
})
export class RoomComponent implements OnInit {
  propertyId: number;
  roomList$: Observable<Room[]>;
  roomTypeListOriginal: RoomTypeView[];
  roomTypeList: SelectOptionItem[];
  roomTypeSelected: any;

  constructor(
    public roomForm: RoomFormService,
    private roomResource: RoomResource,
    private roomTypeResource: RoomTypeResource,
    private propertyStorage: PropertyStorageService,
    public bedTypeService: BedTypeService,
    private thxToasterService: ThxToasterService,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.propertyId = this.propertyStorage.getCurrentPropertyId();

    this.updateList();
    this.loadRoomTypes();
  }

  public save() {
    const room = this.roomForm.getRoom(this.propertyId);
    room.roomType = this.roomTypeSelected;
    room.roomTypeId = this.roomTypeSelected.id;

    this.roomResource.createRoom(room).subscribe(() => {
      this.roomForm.form.reset({
        'uhType': this.roomTypeList[0].value
      });
      const message = this.translateService.instant('variable.lbSaveSuccessM', {
        labelName: this.translateService.instant('label.room')
      });
      this.thxToasterService.showMessage({messages: message});
      this.updateList();
    });
  }

  private loadRoomTypes() {
    this.roomTypeResource.getRoomTypesByPropertyId(this.propertyId, true).subscribe((items: any) => {
      if (items && items.length) {
        this.bedTypeService.setBedTypeCount(items);
        this.roomTypeListOriginal = items;
        this.roomTypeList = toSelectOption(items, 'name', 'id');
        this.roomTypeSelected = items[0];

        this.roomForm.form.patchValue({'uhType': this.roomTypeSelected.id});
        this.roomForm.form.get('uhType').valueChanges.subscribe(value => this.selectItem(value));
      }
    });
  }

  private updateList() {
    this.roomList$ = this.roomResource.getRoomsWithoutChildByPropertyId(this.propertyId);
  }

  public delete(room: Room) {
    this.roomResource.deleteRoom(room.id).subscribe(() => {
      const message = this.translateService.instant('variable.lbDeleteSuccessM', {
        labelName: this.translateService.instant('label.room')
      });
      this.thxToasterService.showMessage({messages: message});
      this.updateList();
    });
  }

  public selectItem(id: number) {
    this.roomTypeSelected = this.roomTypeListOriginal.find(room => room.id == id);
  }

  public getButtonClass(isValid: boolean) {
    return isValid ? 'arrow-blue-right.svg' : 'arrow-gray-right.svg';
  }
}
