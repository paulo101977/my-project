import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoomTypeComponent } from 'app/wizard/rooms-step/components/room-type/room-type.component';
import { RoomComponent } from 'app/wizard/rooms-step/components/room/room.component';
import { RoomsStepComponent } from 'app/wizard/rooms-step/rooms-step.component';

const routes: Routes = [
  {
    path: '',
    component: RoomsStepComponent,
    children: [
      {path: '', redirectTo: 'room-type', pathMatch: 'full'},
      {path: 'room-type', component: RoomTypeComponent},
      {path: 'room', component: RoomComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomsStepRoutingModule { }
