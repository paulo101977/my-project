import {Component, OnInit} from '@angular/core';
import {from, Observable} from 'rxjs';
import {Step} from '@inovacao-cmnet/thx-ui';
import {ModalStepperService} from 'app/wizard/modal-stepper/services/modal-stepper.service';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-step',
  template: `
  <app-modal-stepper
    [steps]="steps$ | async"
    [isValid]="isValid$ | async"
    (selectedStep)="selectedStep($event)"
    (nextStep)="next()"
    (previousStep)="previous()"
  ></app-modal-stepper>`,
  styleUrls: ['./user-step.component.css']
})
export class UserStepComponent implements OnInit {
  steps$: Observable<Step[]>;
  stepsActions: any;
  isValid$: Observable<boolean>;
  constructor(
    private modalStepperService: ModalStepperService,
    private translateService: TranslateService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.isValid$ = this.modalStepperService.isValid$;
    this.steps$ = this.modalStepperService.getSteps('user', [
      {
        id: '1',
        title: this.translateService.instant('title.managementUser')
      }
    ]);

    this.stepsActions = {
      '1': {
        next: () => this.nextStep(),
        select: () => this.navigate('users')
      }
    };

  }

  public next() {
   this.modalStepperService.nextStep(this.stepsActions);
  }

  public previous() {
    this.modalStepperService.previousStep(this.stepsActions);
  }
  public selectedStep(selectedStep: Step) {
    this.modalStepperService.selectStep(selectedStep.id, this.stepsActions);
  }
  public nextStep() {
    // TODO call finish wizard
  }
  public navigate(route: string): Observable<boolean> {
    return from(this.router.navigate([route], {relativeTo: this.route}));
  }



}
