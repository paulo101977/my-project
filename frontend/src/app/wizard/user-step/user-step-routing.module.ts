import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserStepComponent} from 'app/wizard/user-step/user-step.component';
import {UsersComponent} from 'app/wizard/user-step/components/users/users.component';

const routes: Routes = [
  {
    path: '',
    component: UserStepComponent,
    children: [
      {path: '', redirectTo: 'users', pathMatch: 'full'},
      {path: 'users', component: UsersComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserStepRoutingModule { }
