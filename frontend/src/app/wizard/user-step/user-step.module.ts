import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {
  CardBaseModule,
  ContainerListModule,
  ImgCdnModule,
  ThxButtonV2Module, ThxImgInfoModule, ThxImgModule,
  ThxInputV2Module,
  ThxSelectV2Module,
  ThxSubtitleV2Module,
  ThxTitleV2Module,
  RequiredModule
} from '@inovacao-cmnet/thx-ui';
import {TranslateModule} from '@ngx-translate/core';
import {ProfileModule} from 'app/profile/profile.module';
import {ModalStepperModule} from 'app/wizard/modal-stepper/modal-stepper.module';
import {UserStepRoutingModule} from 'app/wizard/user-step/user-step-routing.module';
import {UserStepComponent} from 'app/wizard/user-step/user-step.component';
import {UsersComponent} from './components/users/users.component';
import {UserListCardComponent} from './components/user-card/user-list-card.component';

@NgModule({
  declarations: [UserStepComponent, UsersComponent, UserListCardComponent],
  imports: [
    CommonModule,
    UserStepRoutingModule,
    ModalStepperModule,
    ImgCdnModule,
    TranslateModule,
    ContainerListModule,
    ThxButtonV2Module,
    ThxTitleV2Module,
    ThxSubtitleV2Module,
    ThxSelectV2Module,
    ReactiveFormsModule,
    ThxInputV2Module,
    ProfileModule,
    CardBaseModule,
    ProfileModule,
    ThxImgInfoModule,
    ThxImgModule,
    RequiredModule
  ]
})
export class UserStepModule {
}
