import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ThxToasterService } from '@inovacao-cmnet/thx-ui';
import { PropertyStorageService, swapKeys, toOptions, User } from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { ProfileSelectorComponent } from 'app/profile/shared/components/profile-selector/profile-selector.component';
import { ProfileResourceService } from 'app/profile/shared/services/profile-resource.service';
import { LanguageNamesEnum } from 'app/shared/components/user-language-selection/language-names-enum';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { UserResource } from 'app/shared/resources/user/user.resource';
import { InternationalizationService } from 'app/shared/services/shared/internationalization.service';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';
import { from, Observable } from 'rxjs';
import { filter, map, pluck, switchMap, toArray } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  public users$: Observable<User[]>;
  public profilesOptions$: Observable<any>;
  public userForm: FormGroup;
  public propertyLanguagesOptions$: Observable<any[]>;
  private readonly profileOrder = [
    '84B76BD5-6E78-4A64-9AFC-D972C9488850',
    'CFA00683-9308-448C-9BED-D2FF29259AD5',
    '113EBD17-3B6E-4A2E-A3D0-444C276DB57C'
  ];

  @ViewChild('selector') selector: ProfileSelectorComponent;

  constructor(
    private userResource: UserResource,
    private profileResource: ProfileResourceService,
    private propertyResource: PropertyResource,
    private internationalizationService: InternationalizationService,
    private propertyStorageService: PropertyStorageService,
    private modalStepperService: ModalStepperService,
    private thxToasterService: ThxToasterService,
    private translateService: TranslateService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.modalStepperService.setValid(false);

    this.users$ = this.userResource.getAll();
    this.profilesOptions$ = this.getProfilesOptions();
    this.propertyLanguagesOptions$ = this.getPropertyLanguagesOptions();

    this.userForm = this.formBuilder.group({
      profileId: [null, Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      languageId: [null, Validators.required],
    });
  }

  public setProfile(att) {
    this.userForm.patchValue({profileId: att.value});
  }

  public save(userForm) {
    if (userForm.invalid) {
      return;
    }

    const userData = this.prepareUserData(userForm.value);
    this.createUser(userData)
      .subscribe(() => {
        this.userForm.reset();
        this.selector.clear();
        const message = this.translateService.instant('text.emailSent', {
          labelName: this.translateService.instant('label.user')
        });
        this.thxToasterService.showMessage({messages: message});
        this.users$ = this.userResource.getAll();
      });
  }

  public createUser(user) {
    return this.userResource.invite(user);
  }

  private prepareUserData(userFormValue) {
    const propertyId = this.propertyStorageService.getCurrentPropertyId();

    return {
      email: userFormValue.email,
      invitationDate: new Date().toISOString(),
      isActive: true,
      name: userFormValue.name,
      preferredCulture: userFormValue.languageId,
      preferredLanguage: userFormValue.languageId,
      propertyId: propertyId,
      roleIdList: [userFormValue.profileId]
    };
  }

  private getProfilesOptions() {
    return this.profileResource.list()
      .pipe(
        pluck('items'),
        switchMap((items: any[]) => from(items)),
        filter(item => item.identityProductId === 1),
        toOptions('id', 'name'),
        swapKeys(['label', 'key'], ['text', 'value']),
        map((item: any) => {
          return {...item, text: item.text.replace('PMS - ', '')};
        } ),
        toArray(),
        map((items: any) => {
          return this.profileOrder.map(profile => {
            return items.find(item => item.value.toUpperCase() == profile);
          });
        })
      );
  }

  private getPropertyLanguagesOptions() {
    return this.propertyResource.getPropertyLanguages()
      .pipe(
        pluck('items'),
        switchMap((items: any[]) => from(items)),
        map((propertyLanguage) => {
          const languageValue = this.internationalizationService.getCountry(propertyLanguage.value);

          return {
            text: LanguageNamesEnum[languageValue],
            value: propertyLanguage.value,
            leftIcon: `${languageValue.toLowerCase()}.svg`,
          };
        }),
        toArray(),
      );
  }
  public getButtonClass(isValid: boolean) {
    return isValid ? 'arrow-blue-right.svg' : 'arrow-gray-right.svg';
  }
}
