import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { propertyStorageServiceStub } from '@bifrost/storage/services/testing';
import { ImgCdnTestingModule, RequiredTestingModule, ThxToasterService } from '@inovacao-cmnet/thx-ui';
import { profileResourceStub } from 'app/profile/testing/profile-resource.service.stub';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { propertyResourceStub } from 'app/shared/resources/property/testing';
import { userResourceStub } from 'app/shared/resources/user/testing';
import { internationalizationServiceStub } from 'app/shared/services/shared/testing/internationalization-service';
import { of } from 'rxjs';

import { UsersComponent } from './users.component';
import {createServiceStub} from '../../../../../../testing';
import {ModalStepperService} from 'app/wizard/modal-stepper/services/modal-stepper.service';
import {configureTestSuite} from 'ng-bullet';
import {Component, NO_ERRORS_SCHEMA} from '@angular/core';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  const modalStepperServiceStub = createServiceStub(ModalStepperService, {
    setValid(valid: boolean) { return false; },
  });

  const thxToasterServiceStub = createServiceStub(ThxToasterService, {
    showMessage(config: any): void {}
  });

  @Component({
    selector: 'app-profile-selector',
    template: ''
  })
  class ProfileSelectorStubComponent {
    clear() {}
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        UsersComponent,
        ProfileSelectorStubComponent
      ],
      providers: [
        FormBuilder,
        modalStepperServiceStub,
        thxToasterServiceStub,
        userResourceStub,
        profileResourceStub,
        propertyResourceStub,
        internationalizationServiceStub,
        propertyStorageServiceStub,
      ],
      imports: [ TranslateTestingModule, ImgCdnTestingModule, RequiredTestingModule ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;

    spyOn<any>(component, 'getProfilesOptions').and.returnValue(of([]));
    spyOn<any>(component, 'getPropertyLanguagesOptions').and.returnValue(of([]));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#save', () => {
    it('should do nothing if form is invalid', () => {
      spyOn<any>(component, 'createUser').and.returnValue(of(null));

      component.save({invalid: true});

      expect(component.createUser).not.toHaveBeenCalled();
    });

    it('should create a user and send a success message when the form is valid', () => {
      spyOn<any>(component, 'createUser').and.returnValue(of(null));
      spyOn<any>(component['thxToasterService'], 'showMessage');
      spyOn<any>(component, 'prepareUserData').and.returnValue(<any>{});

      component.save({invalid: false, value: {}});

      expect(component.createUser).toHaveBeenCalled();
      expect(component['thxToasterService'].showMessage).toHaveBeenCalled();
    });
  });

  describe('#createUser', () => {
    it('should call UserResource#invite', () => {
      spyOn<any>(component['userResource'], 'invite');

      component.createUser(<any>{});

      expect(component['userResource'].invite).toHaveBeenCalled();
    });
  });
});
