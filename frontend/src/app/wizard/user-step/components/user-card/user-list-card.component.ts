import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-user-list-card',
  templateUrl: './user-list-card.component.html',
  styleUrls: ['./user-list-card.component.scss']
})
export class UserListCardComponent {

  @Input() user: User;

  getRoleCssClass(roleId: string) {
    const upperRoleId = roleId && roleId.toUpperCase();
    return {
      '--cam': upperRoleId === '113EBD17-3B6E-4A2E-A3D0-444C276DB57C',
      '--adm': upperRoleId === 'CFA00683-9308-448C-9BED-D2FF29259AD5',
    };
  }
}
