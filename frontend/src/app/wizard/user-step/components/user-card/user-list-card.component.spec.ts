import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ImgCdnTestingModule } from '@inovacao-cmnet/thx-ui';
import { profileAcronymPipeStub } from 'app/profile/testing/profile-acronym.pipe.stub';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';

import { UserListCardComponent } from './user-list-card.component';

describe('UserListCardComponent', () => {
  let component: UserListCardComponent;
  let fixture: ComponentFixture<UserListCardComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserListCardComponent,
        profileAcronymPipeStub,
      ],
      imports: [
        TranslateTestingModule,
        ImgCdnTestingModule,
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    fixture = TestBed.createComponent(UserListCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
