import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UserStepComponent} from './user-step.component';
import { RequiredTestingModule, Step } from '@inovacao-cmnet/thx-ui';
import {ModalStepperService} from 'app/wizard/modal-stepper/services/modal-stepper.service';
import {modalStepperProvider} from 'app/wizard/modal-stepper/testing/modal-stepper.provider';
import {ActivatedRoute, Router} from '@angular/router';
import {configureTestSuite} from 'ng-bullet';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateTestingModule} from 'app/shared/mock-test/translate-testing.module';
import {ActivatedRouteStub} from '../../../../testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RoomsStepComponent} from 'app/wizard/rooms-step/rooms-step.component';

describe('UserStepComponent', () => {
  let component: UserStepComponent;
  let fixture: ComponentFixture<UserStepComponent>;
  let modalStepperService: ModalStepperService;
  let router: Router;

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        declarations: [UserStepComponent],
        imports: [ RouterTestingModule, TranslateTestingModule, RequiredTestingModule ],
        providers: [
          {provide: ActivatedRoute, useClass: ActivatedRouteStub}
        ],
        schemas: [NO_ERRORS_SCHEMA],
      })
      .overrideComponent(RoomsStepComponent, {
        set: {
          providers: [modalStepperProvider]
        }
      });

    router = TestBed.get(Router);

    fixture = TestBed.createComponent(UserStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    modalStepperService = component['modalStepperService'];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#next', () => {
    it('should call nextStep', () => {
      spyOn(modalStepperService, 'nextStep');

      component.next();
      expect(modalStepperService.nextStep).toHaveBeenCalled();
    });
  });

  describe('#previous', () => {
    it('should call previousStep', () => {
      spyOn(modalStepperService, 'previousStep');

      component.previous();
      expect(modalStepperService.previousStep).toHaveBeenCalled();
    });
  });

  describe('#selectStep', () => {
    it('should call selectedStep', () => {
      spyOn(modalStepperService, 'selectStep');
      const step: Step = { active: false, complete: false, disabled: false, id: '', title: ''};

      component.selectedStep(step);
      expect(modalStepperService.selectStep).toHaveBeenCalled();
    });
  });

  describe('#navigate', () => {
    it('should navigate ', () => {
      spyOn(router, 'navigate').and.returnValue(Promise.resolve(true));

      component.navigate('')
        .subscribe(() => {
          expect(router.navigate).toHaveBeenCalled();
        });
    });
  });
});
