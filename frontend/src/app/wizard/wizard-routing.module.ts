import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WizardComponent } from 'app/wizard/components/wizard/wizard.component';


export const routes: Routes = [
  {
    path: '',
    component: WizardComponent,
    children: [
      { path: '', redirectTo: 'rooms-step', pathMatch: 'full' },
      { path: 'rooms-step', loadChildren: './rooms-step/rooms-step.module#RoomsStepModule'},
      { path: 'products-step', loadChildren: './product-step/product-step.module#ProductStepModule'},
      { path: 'users-step', loadChildren: './user-step/user-step.module#UserStepModule'}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WizardRoutingModule {}
