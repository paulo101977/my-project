import { Step } from '@inovacao-cmnet/thx-ui';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../testing';

export const modalStepperProvider = createServiceStub(ModalStepperService, {
  getSteps(key: string, defaultSteps: Step[]): Observable<Step[]> { return of([]); },
  nextStep(stepsActions) {},
  selectStep(stepId: string, stepsActions, steps: Step[] = this._steps) {},
  previousStep(stepsActions) {},
});
