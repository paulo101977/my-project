import { Location } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { createServiceStub } from '../../../../../../testing';

import { ModalFooterComponent } from './modal-footer.component';

describe('ModalFooterComponent', () => {
  let component: ModalFooterComponent;
  let fixture: ComponentFixture<ModalFooterComponent>;
  let location: Location;

  const locationStub = createServiceStub(Location, {
    back(): void {}
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFooterComponent ],
      providers: [ locationStub ],
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ TranslateTestingModule ]
    });

    fixture = TestBed.createComponent(ModalFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    location = TestBed.get(Location);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#previousStep', () => {
    it('should call location#back', () => {
      spyOn(component.previous, 'emit');
      component.previousStep();
      expect(component.previous.emit).toHaveBeenCalled();
    });
  });

  describe('#nextStep', () => {
    it('should call next#emit', () => {
      spyOn(component.next, 'emit');
      component.nextStep();
      expect(component.next.emit).toHaveBeenCalled();
    });
  });

  describe('#getNextButtonSvg', () => {
    beforeEach(() => {
      component.isLastStep = true;
      component.isNextEnabled = false;
    });

    it('should return an empty string when it is the last step', () => {
      const svg = component.getNextButtonSvg();

      expect(svg).toBe('');
    });

    it('should return the enabled svg when next step is enabled', () => {
      component.isLastStep = false;
      component.isNextEnabled = true;
      const svg = component.getNextButtonSvg();

      expect(svg).toBe('arrow-white-right.svg');
    });

    it('should return the enabled svg when next step is disabled', () => {
      component.isLastStep = false;
      component.isNextEnabled = false;
      const svg = component.getNextButtonSvg();

      expect(svg).toBe('arrow-gray-right.svg');
    });
  });

  describe('#getPreviousButtonSvg', () => {
    it('should return the previous button svg', () => {
      const svg = component.getPreviousButtonSvg();

      expect(svg).toBe('arrow-blue-left.svg');
    });
  });
});
