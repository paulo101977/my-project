import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-modal-footer',
  templateUrl: './modal-footer.component.html',
  styleUrls: ['./modal-footer.component.scss']
})
export class ModalFooterComponent {

  @Input() isNextEnabled = false;
  @Input() skipStepRoute: string;
  @Input() isPreviousAvailable = false;
  @Input() isLastStep = false;

  @Output() next = new EventEmitter();
  @Output() previous = new EventEmitter();

  constructor() { }

  public getNextButtonSvg() {
    const enabledArrow = 'arrow-white-right.svg';
    const disabledArrow = 'arrow-gray-right.svg';

    if (this.isLastStep) {
      return '';
    }

    return this.isNextEnabled ? enabledArrow : disabledArrow;
  }

  public getPreviousButtonSvg() {
    return 'arrow-blue-left.svg';
  }

  public previousStep() {
    this.previous.emit();
  }

  public nextStep() {
    this.next.emit();
  }
}
