import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ModalStepperComponent } from './modal-stepper.component';

describe('ModalStepperComponent', () => {
  let component: ModalStepperComponent;
  let fixture: ComponentFixture<ModalStepperComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalStepperComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(ModalStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#selectStep', () => {
    it('should emit the step', () => {
      spyOn(component.selectedStep, 'emit');

      component.selectStep(null);
      expect(component.selectedStep.emit).toHaveBeenCalledWith(null);
    });
  });

  describe('#next', () => {
    it('should emit nextStep', () => {
      spyOn(component.nextStep, 'emit');

      component.next();

      expect(component.nextStep.emit).toHaveBeenCalled();
    });
  });

  describe('#previous', () => {
    it('should emit previousStep', () => {
      spyOn(component.previousStep, 'emit');

      component.previous();

      expect(component.previousStep.emit).toHaveBeenCalled();
    });
  });

  describe('#setNavigationStates', () => {
    beforeEach(() => {
      component.isPreviousAvailable = false;
      component.isNextEnabled = false;
      component.isLastStep = false;
    });

    it('should set isNextEnabled as isValid value', () => {
      component.setNavigationStates(true);

      expect(component.isNextEnabled).toBe(true);
    });

    it('should set isLastStep as true if there is no step', () => {
      component.setNavigationStates(false);

      expect(component.isLastStep).toBe(true);
    });

    it('should set isLastStep as true if the active step is the last one', () => {
      component.setNavigationStates(false, [{id: '1'}, {id: '2', active: true}]);

      expect(component.isLastStep).toBe(true);
    });

    it('should set isPreviousAvailable as true if there is a step before the active one', () => {
      component.setNavigationStates(false, [{id: '1'}, {id: '2', active: true}]);

      expect(component.isPreviousAvailable).toBe(true);
    });
  });
});
