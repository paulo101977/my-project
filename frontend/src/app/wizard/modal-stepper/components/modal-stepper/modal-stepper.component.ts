import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Step } from '@inovacao-cmnet/thx-ui';

@Component({
  selector: 'app-modal-stepper',
  templateUrl: './modal-stepper.component.html',
  styleUrls: ['./modal-stepper.component.css']
})
export class ModalStepperComponent implements OnChanges {
  @Input() steps: Step[];
  @Input() title: string;
  @Input() skipStepRoute: string;
  @Input() isValid = false;

  @Output() selectedStep = new EventEmitter<Step>();
  @Output() nextStep = new EventEmitter();
  @Output() previousStep = new EventEmitter();

  public isPreviousAvailable = false;
  public isNextEnabled = false;
  public isLastStep = false;

  ngOnChanges(changes: SimpleChanges): void {
    const { isValid, steps } = changes;

    if (!isValid && !steps) {
      return;
    }

    this.setNavigationStates(this.isValid, this.steps);
  }

  public selectStep(step: Step) {
    this.selectedStep.emit(step);
  }

  public next() {
    this.nextStep.emit();
  }

  public previous() {
    this.previousStep.emit();
  }

  public setNavigationStates(isValid: boolean, steps: Step[] = []) {
    this.isNextEnabled = isValid;

    const activeStepIndex = steps.findIndex(step => step.active);
    this.isPreviousAvailable = activeStepIndex > 0;
    this.isLastStep = activeStepIndex == (steps.length - 1);
  }
}
