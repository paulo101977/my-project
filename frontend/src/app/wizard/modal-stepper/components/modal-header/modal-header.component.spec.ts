import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Step } from '@inovacao-cmnet/thx-ui';
import { configureTestSuite } from 'ng-bullet';

import { ModalHeaderComponent } from './modal-header.component';

describe('ModalHeaderComponent', () => {
  let component: ModalHeaderComponent;
  let fixture: ComponentFixture<ModalHeaderComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalHeaderComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(ModalHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#setStepTitle', () => {
    it('should use the title if there is no step', () => {
      const title = 'my title';
      component.setStepTitle(title);

      expect(component.stepTitle).toEqual(title);
    });

    it('should use the title if there is no active step', () => {
      const title = 'my title';
      component.setStepTitle(title, [{id: '1'}]);

      expect(component.stepTitle).toEqual(title);
    });

    it('should use active step title', () => {
      const title = 'my title';
      const stepTitle = 'other title';
      component.setStepTitle(title, [{id: '1', active: true, title: stepTitle}]);

      expect(component.stepTitle).toEqual(stepTitle);
    });
  });

  describe('#selectStep', () => {
    it('should send the selected step', () => {
      spyOn(component.selectedStep, 'emit');

      const step: Step = {id: '1'};
      component.selectStep(step);

      expect(component.selectedStep.emit).toHaveBeenCalledWith(step);
    });
  });
});
