import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Step } from '@inovacao-cmnet/thx-ui';

@Component({
  selector: 'app-modal-header',
  templateUrl: './modal-header.component.html',
  styleUrls: ['./modal-header.component.scss']
})
export class ModalHeaderComponent implements OnChanges {

  @Input() title: string;
  @Input() steps: Step[];
  @Output() selectedStep = new EventEmitter<Step>();

  public stepTitle: string;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.title || changes.steps) {
      this.setStepTitle(this.title, this.steps);
    }
  }

  public setStepTitle(title: string, steps: Step[] = []) {
    const activeStep = steps.find(step => step.active);
    this.stepTitle = activeStep ? activeStep.title : title;
  }

  public selectStep(step: Step) {
    this.selectedStep.emit(step);
  }
}
