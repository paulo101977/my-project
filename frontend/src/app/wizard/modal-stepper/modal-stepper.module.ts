import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ImgCdnModule, ThxStepsModule, ThxButtonV2Module } from '@inovacao-cmnet/thx-ui';
import { TranslateModule } from '@ngx-translate/core';
import { ModalFooterComponent } from './components/modal-footer/modal-footer.component';
import { ModalHeaderComponent } from './components/modal-header/modal-header.component';
import { ModalStepperComponent } from './components/modal-stepper/modal-stepper.component';

@NgModule({
  imports: [
    CommonModule,
    ThxStepsModule,
    RouterModule,
    TranslateModule,
    ImgCdnModule,
    ThxButtonV2Module
  ],
  declarations: [
    ModalHeaderComponent,
    ModalFooterComponent,
    ModalStepperComponent
  ],
  exports: [
    ModalHeaderComponent,
    ModalFooterComponent,
    ModalStepperComponent
  ]
})
export class ModalStepperModule { }
