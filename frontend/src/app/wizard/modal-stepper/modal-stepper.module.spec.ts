import { ModalStepperModule } from './modal-stepper.module';

describe('ModalStepperModule', () => {
  let modalStepperModule: ModalStepperModule;

  beforeEach(() => {
    modalStepperModule = new ModalStepperModule();
  });

  it('should create an instance', () => {
    expect(modalStepperModule).toBeTruthy();
  });
});
