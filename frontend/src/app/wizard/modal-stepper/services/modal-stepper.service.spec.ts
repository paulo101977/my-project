import { TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ModalStepperService } from 'app/wizard/modal-stepper/services/modal-stepper.service';
import { configureTestSuite } from 'ng-bullet';
import { forkJoin, of } from 'rxjs';
import { ActivatedRouteStub } from '../../../../../testing';


describe('ModalStepperService', () => {
  let service: ModalStepperService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [
        ModalStepperService,
        {provide: ActivatedRoute, useClass: ActivatedRouteStub}
      ],
      imports: [RouterTestingModule]
    });

    service = TestBed.get(ModalStepperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getSteps', () => {
    it('should return a steps BehaviorSubject', () => {
      const steps = [];

      service.getSteps('', steps).subscribe((data) => {
        expect(data).toEqual(steps);
      });
    });
  });

  describe('#setValid', () => {
    it('should emit the next isValid value', () => {
      const isValid = true;
      service.setValid(isValid);
      service.isValid$.subscribe((data) => {
        expect(data).toEqual(isValid);
      });
    });
  });

  describe('#getStepById', () => {
    it('should return a step if matched by id', () => {
      const step = service.getStepById('1', [{id: '1'}]);
      expect(step).not.toBeUndefined();
    });

    it('should return undefined if it failed', () => {
      const step = service.getStepById('1', [{id: '2'}]);
      expect(step).toBeUndefined();
    });
  });

  describe('#getActiveStepIndex', () => {
    it('should return the active step index', () => {
      const step = service.getActiveStepIndex([{id: '1', active: true}]);
      expect(step).toEqual(0);
    });

    it('should return -1 if it failed', () => {
      const step = service.getActiveStepIndex([{id: '2'}]);
      expect(step).toEqual(-1);
    });
  });

  describe('#getActiveStep', () => {
    it('should return an active step', () => {
      const step = service.getActiveStep([{id: '1', active: true}]);
      expect(step).not.toBeUndefined();
    });

    it('should return undefined if it failed', () => {
      const step = service.getActiveStep([{id: '2'}]);
      expect(step).toBeUndefined();
    });
  });

  describe('#navigate', () => {
    it('should call router navigate', done => {
      spyOn<any>(service['router'], 'navigate').and.returnValue(of(true));
      const route = 'ola';

      service.navigate(route).subscribe((data) => {
        expect(service['router'].navigate).toHaveBeenCalledWith([route], {
          relativeTo: service['route']
        });
        done();
      });
    });
  });

  describe('#activateStepById', () => {
    it('should return the steps if no step was matched', () => {
      const steps = [];
      const newSteps = service.activateStepById('1', steps);

      expect(newSteps).toEqual(steps);
    });

    it('should return the steps if the selected is disabled', () => {
      const steps = [{id: '1', disabled: true}];
      const newSteps = service.activateStepById(steps[0].id, steps);

      expect(newSteps).toEqual(steps);
    });

    it('should activate the step with the matched id', () => {
      const steps = [{id: '1'}, {id: '2'}];
      const newSteps = service.activateStepById(steps[1].id, steps);

      expect(newSteps[1].active).toEqual(true);
    });
  });

  describe('#hasActiveStepChanged', () => {
    it('should return true if it doesn\'t find an active step', () => {
      const stepsWithActive = [{id: '1', active: true}];
      const steps = [];

      const hasPreviousChanged = service.hasActiveStepChanged(steps, stepsWithActive);
      const hasCurrentChanged = service.hasActiveStepChanged(stepsWithActive, steps);


      expect(hasPreviousChanged).toBe(true);
      expect(hasCurrentChanged).toBe(true);
    });

    it('should return false if both lists have the same active step', () => {
      const steps = [{id: '1', active: true}];
      const hasChanged = service.hasActiveStepChanged(steps, steps);

      expect(hasChanged).toBe(false);
    });

    it('should return true if both lists have different active steps', () => {
      const steps1 = [{id: '1', active: true}];
      const steps2 = [{id: '2', active: true}];
      const hasChanged = service.hasActiveStepChanged(steps1, steps2);

      expect(hasChanged).toBe(true);
    });
  });

  describe('#nextStep', () => {
    const actions = {
      next: () => of(null),
    };

    beforeEach(() => {
      spyOn(service, 'selectStep');
      spyOn<any>(service, 'setSteps');
    });

    it('should complete the current step and active the next', () => {
      const steps = [{id: '1', active: true}, {id: '2'}];
      const stepsActions = {'1': actions};
      service['_steps'] = steps;

      service.nextStep(stepsActions);

      expect(service['setSteps']).not.toHaveBeenCalled();
      expect(service.selectStep).toHaveBeenCalledWith(
        steps[1].id,
        stepsActions,
        [
          {id: '1', active: true, complete: true},
          {id: '2', disabled: false}
        ]
      );
    });

    it('should only complete the current step if it is the last step', () => {
      const steps = [{id: '1'}, {id: '2', active: true}];
      const stepsActions = {'2': actions};
      service['_steps'] = steps;

      service.nextStep(stepsActions);

      expect(service.selectStep).not.toHaveBeenCalled();
      expect(service['setSteps']).toHaveBeenCalledWith([
        {id: '1'},
        {id: '2', active: true, complete: true}
      ]);
    });
  });

  describe('#previousStep', () => {
    it('should call #selectStep on the previous step', () => {
      spyOn(service, 'selectStep');

      const steps = [{id: '1'}, {id: '2', active: true}];
      service['_steps'] = steps;
      service.previousStep(null);
      expect(service.selectStep).toHaveBeenCalledWith(steps[0].id, null);
    });
  });

  describe('#selectStep', () => {
    const actions = {
      select: () => of(null)
    };

    beforeEach(() => {
      spyOn<any>(service, 'setSteps');
    });

    it('should select the step if it wasn\'t previously active', () => {
      const steps = [{id: '1'}];
      const stepsActions = {'1': actions};
      service.selectStep(steps[0].id, stepsActions, steps);

      expect(service['setSteps']).toHaveBeenCalledWith([{id: '1', active: true}]);
    });

    it('should do nothing if the step is active', () => {
      const steps = [{id: '1', active: true}];
      const stepsActions = {'1': actions};
      service.selectStep(steps[0].id, stepsActions, steps);

      expect(service['setSteps']).not.toHaveBeenCalled();
    });
  });

  describe('#enableStep', () => {
    it('should return a function to enable the step with the same id', () => {
      const enableStep = service.enableStep('1');

      expect(enableStep({id: '1'})).toEqual({id: '1', disabled: false});
      expect(enableStep({id: '2'})).toEqual({id: '2'});
    });
  });

  describe('#completeStep', () => {
    it('should return a function to complete the step with the same id', () => {
      const completeStep = service.completeStep('1');

      expect(completeStep({id: '1'})).toEqual({id: '1', complete: true});
      expect(completeStep({id: '2'})).toEqual({id: '2'});
    });
  });

  describe('#enableNextStep', () => {
    it('should return a function to enable the step if there is a next step', done => {
      const enableNextStep = service.enableNextStep({id: '1'});

      forkJoin(enableNextStep({id: '1'}), enableNextStep({id: '2'}))
        .subscribe(([step1, step2]) => {
          expect(step1).toEqual({id: '1', disabled: false});
          expect(step2).toEqual({id: '2'});
          done();
        });
    });

    it('should return a function to return the step if there isn\'t a next step', done => {
      const enableNextStep = service.enableNextStep(null);

      forkJoin(enableNextStep({id: '1'}), enableNextStep({id: '2'}))
        .subscribe(([step1, step2]) => {
          expect(step1).toEqual({id: '1'});
          expect(step2).toEqual({id: '2'});
          done();
        });
    });
  });
});
