import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Step } from '@inovacao-cmnet/thx-ui';
import { BehaviorSubject, from, iif, Observable, of } from 'rxjs';
import { distinctUntilChanged, map, switchMap, take, tap, toArray } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModalStepperService {
  private isValid = new BehaviorSubject<boolean>(false);
  public isValid$ = this.isValid.asObservable().pipe(distinctUntilChanged());

  private _steps: Step[];

  private stepsStore: BehaviorSubject<Step[]>;
  public steps$: Observable<Step[]>;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {}

  // TODO create logic to get the steps from localStorage
  public getSteps(key: string, defaultSteps: Step[]): Observable<Step[]> {
    this.stepsStore = new BehaviorSubject<Step[]>(defaultSteps);
    this.steps$ = this.stepsStore.asObservable().pipe(
      distinctUntilChanged(),
      tap(steps => this._steps = steps)
    );

    return this.steps$;
  }

  private setSteps(steps: Step[]): void {
    this.stepsStore.next(steps);
  }

  public setValid(valid: boolean) {
    this.isValid.next(valid);
  }

  public getStepById(stepId: string, steps: Step[]): Step {
    return steps.find(step => step.id === stepId);
  }

  public getActiveStepIndex(steps: Step[]): number {
    return steps.findIndex(step => step.active);
  }

  public getActiveStep(steps: Step[]): Step {
    return steps.find(step => step.active);
  }

  public navigate(route: string): Observable<boolean> {
    return from(this.router.navigate([route], {relativeTo: this.route}));
  }

  public activateStepById(stepId: string, steps: Step[] = this._steps) {
    const selectedStep = this.getStepById(stepId, steps);

    if (!selectedStep || selectedStep.disabled) {
      return steps;
    }

    return steps.map(step => {
      return {
        ...step,
        active: step.id === stepId
      };
    });
  }

  public hasActiveStepChanged(previousSteps, currentSteps) {
    const previousActiveStep = this.getActiveStep(previousSteps);
    const currentActiveStep = this.getActiveStep(currentSteps);

    return !previousActiveStep
      || !currentActiveStep
      || (previousActiveStep.id != currentActiveStep.id);
  }

  public nextStep(stepsActions) {
    const activeStepIndex = this.getActiveStepIndex(this._steps);
    const activeStep = this._steps[activeStepIndex];
    const nextStep = this._steps[activeStepIndex + 1];

    stepsActions[activeStep.id].next()
      .pipe(
        take(1),
        switchMap(() => from(this._steps)),
        switchMap(this.enableNextStep(nextStep)),
        map(this.completeStep(activeStep.id)),
        toArray(),
      )
      .subscribe((steps) => {
        if (nextStep) {
          this.selectStep(nextStep.id, stepsActions, steps);
          return;
        }

        this.setSteps(steps);
      });
  }

  public previousStep(stepsActions) {
    const activeStepIndex = this.getActiveStepIndex(this._steps);
    const previousStep = this._steps[activeStepIndex - 1];

    this.selectStep(previousStep.id, stepsActions);
  }

  public selectStep(stepId: string, stepsActions, steps: Step[] = this._steps) {
    const changedSteps = this.activateStepById(stepId, steps);

    if (!this.hasActiveStepChanged(steps, changedSteps)) {
      return;
    }

    stepsActions[stepId].select()
      .pipe(take(1))
      .subscribe(() => this.setSteps(changedSteps));
  }

  public enableStep(stepId: string) {
    return step => ({
      ...step,
      ...(step.id === stepId ? {disabled: false} : {}),
    });
  }

  public completeStep(stepId: string) {
    return step => ({
      ...step,
      ...(step.id === stepId ? {complete: true} : {})
    });
  }

  public enableNextStep(nextStep: Step) {
    return step => {
      const transformStep = nextStep
        ? this.enableStep(nextStep.id)
        : f => f;

      return of(transformStep(step));
    };
  }
}
