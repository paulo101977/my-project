import { Injectable } from '@angular/core';
import { ChannelClientAssociation } from 'app/shared/models/integration/channel';

@Injectable({
  providedIn: 'root'
})
export class ChannelService {

  constructor() { }

  public channelAlreadyExists(channel: ChannelClientAssociation, channelList: ChannelClientAssociation[]) {
    if (channelList.length) {
      return channelList.some(ch => ch.channelId == channel.channelId);
    }
    return false;
  }
}
