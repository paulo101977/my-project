import { TestBed, inject } from '@angular/core/testing';

import { ChannelService } from './channel.service';
import { CHANNEL_COMPANY_ASSOCIATION_DATA } from 'app/integration/components/mock/channel-data';

describe('ChannelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChannelService]
    });
  });

  it('should be created', inject([ChannelService], (service: ChannelService) => {
    expect(service).toBeTruthy();
  }));

  it('should channelAlreadyExists', inject([ChannelService], (service: ChannelService) => {
    const result = service
      .channelAlreadyExists(CHANNEL_COMPANY_ASSOCIATION_DATA, [CHANNEL_COMPANY_ASSOCIATION_DATA]);

    expect(result).toBeTruthy();
  }));

  it('should channelAlreadyExists#NOT', inject([ChannelService], (service: ChannelService) => {
    const result = service
      .channelAlreadyExists(CHANNEL_COMPANY_ASSOCIATION_DATA, []);

    expect(result).toBeFalsy();
  }));
});
