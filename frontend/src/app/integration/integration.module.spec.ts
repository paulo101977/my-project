import { IntegrationModule } from './integration.module';

describe('ChannelModule', () => {
  let channelModule: IntegrationModule;

  beforeEach(() => {
    channelModule = new IntegrationModule();
  });

  it('should create an instance', () => {
    expect(channelModule).toBeTruthy();
  });
});
