import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { LoadPageEmitService } from 'app/shared/services/shared/load-emit.service';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { TranslateService } from '@ngx-translate/core';
import { CustomerResource } from 'app/shared/resources/consult-customer/customer.resource';
import { CustomerStation } from 'app/shared/models/integration/customerStation';
import { ModalDeleteComponent } from '../modal-delete/modal-delete.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'consult-customer',
  templateUrl: './consult-customer.component.html'
})
export class ConsultCustomerComponent implements OnInit {

  // header
  public configHeaderPage: ConfigHeaderPageNew;

  // to change the router is necessary the propertyId
  private propertyId;

  // table columns
  public columns: Array<any>;

  // table data
  public pageItemsList: Array<CustomerStation>;
  public pageItemsListBack: Array<CustomerStation>;

  // config table options
  public itemsOption: Array<any>;

  // modal ref
  @ViewChild('deletemodal') modalDelete: ModalDeleteComponent;

  constructor(
    private loaderService: LoadPageEmitService,
    private toastEmitter: ToasterEmitService,
    private translateService: TranslateService,
    private customerResource: CustomerResource,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  // modal methods:
  private setColumnsName(): void {
    this.columns = [];

    this.columns.push({
      name: 'label.stationName',
      prop: 'stationName',
    });
    this.columns.push({
      name: 'label.client',
      prop: 'client',
    });
  }

  public rowItemClicked($event): void {
    const row = <CustomerStation>$event;
    const { id } = row;

    if (typeof id !== 'undefined') {
      this.router.navigate(
        [
          `/p/${this.propertyId}/integration/customer/edit`, { mode: 'edit', customerId:  id }
        ],
        { relativeTo: this.route}
      );
    } else {
      console.error('id undefined');
    }
  }

  public updateStatus($event): void {
    const row = <CustomerStation>$event;
    const { isActive } = row;

    this.customerResource.edit(row).subscribe( res => {
      this.showToastStatus(SuccessError.success, isActive);
    },
    err => {
      this.showToastStatus(SuccessError.error, isActive);
    });
  }

  // Utils
  public showToastStatus(typeMessage: SuccessError, isActive) {
    let message;

    if (typeMessage == SuccessError.success && isActive) {
      message = 'label.customerStatusActivateSuccess';
    }
    if (typeMessage == SuccessError.success && !isActive) {
      message = 'label.customerStatusInactivateSuccess';
    }
    if (typeMessage == SuccessError.error && isActive) {
      message = 'label.customerStatusActivateError';
    }
    if (typeMessage == SuccessError.error && !isActive) {
      message = 'label.customerStatusInactivateError';
    }

    this
      .toastEmitter
      .emitChangeTwoMessages(typeMessage, this.translateService.instant(message), '');
  }



  // end datatable
  // the term parameter, now comes as object, now comes as string
  public search(term: any): void {

    if (typeof term === 'string' && term !== '') {
      this.pageItemsList = this.pageItemsListBack.filter(
        item =>
          new RegExp(term, 'i').test(item.stationName) ||
              new RegExp(term, 'i').test(item.client)
      );
    } else {
      // the original resource
      this.pageItemsList = this.pageItemsListBack;
    }
  }

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: false,
      keepTitleButton: true,
      callBackFunction: this.addNewCustomer
    };
  }

  private addNewCustomer = () => {
    this.router.navigate(
      [
        `/p/${this.propertyId}/integration/customer/new`, { mode: 'insert' }
      ],
      { relativeTo: this.route }
    );
  }

  private editCustomer = ($event) => {
    const { id } = $event;

    this.router.navigate(
      [
        `/p/${this.propertyId}/integration/customer/edit`, { mode: 'edit', customerId:  id }
      ],
      { relativeTo: this.route}
    );
  }


  loadData(): void {

    this.loaderService.emitChange(ShowHide.show);

    this.pageItemsList = [];

    this.customerResource.getAllList().subscribe(
      res => {
        const list = <Array<CustomerStation>>res;

        if (list) {
          this.pageItemsList = list;
          this.pageItemsListBack = list;
        }

        this.loaderService.emitChange(ShowHide.hide);
      },
      err => {
        console.error(err);
        this.loaderService.emitChange(ShowHide.hide);
      }
    );

  }


  private toggleModalToExcludeAction = (row) =>  {
    this.modalDelete.openModalToDelete(row);
  }

  ngOnInit() {
    // init head config
    this.setHeaderConfig();

    // init table config
    this.setColumnsName();

    // datable submenu option config
    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.editCustomer },
      { title: 'commomData.delete', callbackFunction: this.toggleModalToExcludeAction },
    ];

    this.loadData();

    this.route.params.subscribe( params => {
      this.propertyId = +params.property;
    });
  }

}
