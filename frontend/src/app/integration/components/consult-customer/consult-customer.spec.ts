import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Location } from '@angular/common';
import { ConsultCustomerComponent } from './consult-customer.component';
import { IntegrationModule } from 'app/integration/integration.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { customerResourceStub } from 'app/shared/resources/consult-customer/testing';
import { loadPageEmitServiceStub } from '../../../../../testing';

describe('ConsultCustomer', () => {
  let component: ConsultCustomerComponent;
  let fixture: ComponentFixture<ConsultCustomerComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ConsultCustomerComponent,
      ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
      ],
      providers: [
        toasterEmitServiceStub,
        customerResourceStub,
        loadPageEmitServiceStub,
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ],
    });

    fixture = TestBed.createComponent(ConsultCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn<any>(component['router'], 'navigate').and.callFake( () => {} );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
