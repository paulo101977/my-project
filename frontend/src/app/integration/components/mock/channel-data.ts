import { Channel, ChannelClientAssociation } from 'app/shared/models/integration/channel';

export const channelData = <Channel> {
  id: '3751359d-f8f2-4d0d-9456-13cbd750d1ed',
  channelCodeId: 1,
  description: 'bla bla',
  businessSourceId: 1,
  distributionAmount: 2150,
  isActive: true,
  businessSourceName: 'Nova origem',
  channelCodeName: 'CODE 000'
};

export const CHANNEL_COMPANY_ASSOCIATION_DATA = <ChannelClientAssociation> {
  id: '50ae231e-c8e9-473d-b3eb-ddd6bdeb9802',
  channelId: '0f9da47b-151d-42d6-96e5-47aaa7a7c886',
  companyClientId: '761aa16d-2425-457f-bc6e-5955f35ed830',
  isActive: true,
  companyId: 'Teste',
  channelCodeName: 'CODE',
  channelCodeId: 1,
  channelDescription: 'QQQQQQQ',
  businessSourceId: 4,
  businessSourceDescription: 'Origem Automatizada Ativada'
};
