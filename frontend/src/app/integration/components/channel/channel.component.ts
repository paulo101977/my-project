import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { ModalAddEditComponent } from '../modal-add-edit/modal-add-edit.component';
import { ModalDeleteComponent } from '../modal-delete/modal-delete.component';
import { ChannelResource } from 'app/shared/resources/channel/channel.resource';
import { Channel } from 'app/shared/models/integration/channel';
import { LoadPageEmitService } from 'app/shared/services/shared/load-emit.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html'
})
export class ChannelComponent implements OnInit {

  // modal ref
  @ViewChild('modal') modal: ModalAddEditComponent;
  @ViewChild('deletemodal') modalDelete: ModalDeleteComponent;

  propertyId: number;
  // header
  public configHeaderPage: ConfigHeaderPageNew;
  // table columns
  columns: Array<any>;
  // config table options
  itemsOption: Array<any>;
  // the table data
  pageItemsList: Array<Channel>;
  pageItemsListBack: Array<Channel>;

  constructor(
    private channelResource: ChannelResource,
    private loaderService: LoadPageEmitService,
    private toastEmitter: ToasterEmitService,
    private translateService: TranslateService,
    public route: ActivatedRoute,
  ) {
  }

  // modal methods:
  private setColumnsName(): void {
    this.columns = [
      {name: 'label.code', prop: 'channelCodeName'},
      {name: 'label.description', prop: 'description'},
      {name: 'label.origin', prop: 'businessSourceName'},
      {name: 'label.costDistribution', prop: 'distributionAmount'},
    ];
  }

  public rowItemClicked(row): void {
    this.modal.openModalToEditChannel(<Channel>row);
  }

  public updateStatus($event): void {
    const channel = <Channel>$event;
    const {isActive} = channel;

    const message = this.translateService.instant(isActive
      ? 'variable.lbActivatedWithSuccessM'
      : 'variable.lbInactivatedWithSuccessM', {
      labelName: this.translateService.instant('label.channel')
    });

    this.channelResource.edit(channel)
      .subscribe(() => {
        this.toastEmitter
          .emitChange(SuccessError.success, message);
      });
  }

  public search(term: string): void {
    const regexp = new RegExp(term, 'i');
    this.pageItemsList = this.pageItemsListBack.filter(item => {
      return regexp.test(item.businessSourceName)
        || regexp.test(item.channelCodeName)
        || regexp.test(item.description);
    });
  }

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: false,
      keepTitleButton: true,
      callBackFunction: this.openModalToSaveChannel
    };
  }

  // init modal methods
  public openModalToSaveChannel = () => {
    this.modal.openModalToSaveChannel();
  }

  public toggleModalToEditAction = (row) => {
    this.modal.openModalToEditChannel(<Channel>row);
  }

  public toggleModalToExcludeAction = (row) => {
    this.modalDelete.openModalToDelete(row);
  }

  loadData(): void {
    this
      .channelResource
      .getAllChannelList({all: true})
      .subscribe(async res => {
        if (res && res.items) {
          this.pageItemsList = this.pageItemsListBack = res.items;
        }
      });
  }

  ngOnInit() {
    // init head config
    this.setHeaderConfig();

    // init table config
    this.setColumnsName();

    // datable submenu option config
    this.itemsOption = [
      {title: 'commomData.edit', callbackFunction: this.toggleModalToEditAction},
      {title: 'commomData.delete', callbackFunction: this.toggleModalToExcludeAction},
    ];

    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.loadData();
    });

  }

}
