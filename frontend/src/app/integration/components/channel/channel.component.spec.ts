import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { of } from 'rxjs';

import { ChannelComponent } from './channel.component';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Channel } from 'app/shared/models/integration/channel';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { loadPageEmitServiceStub } from '../../../../../testing';
import { channelResourceStub } from 'app/shared/resources/channel/testing';

describe ('ChannelComponent', () => {
  let component: ChannelComponent;
  let fixture: ComponentFixture<ChannelComponent>;
  let channel: Channel;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
      ],
      declarations: [ChannelComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ property: 1 }),
          },
        },
        InterceptorHandlerService,
        toasterEmitServiceStub,
        loadPageEmitServiceStub,
        channelResourceStub,
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ],
    });

    fixture = TestBed.createComponent(ChannelComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  beforeEach(() => {
    channel = new Channel();
    channel.id = '1';
    channel.code = '1';
    channel.costOfDistribution = 0.00;
    channel.isActive = true;
    channel.origin = '2';
  });


  it('should create ChannelComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should setVars', async(() => {
    spyOn<any>(component, 'setHeaderConfig');
    spyOn<any>(component, 'setColumnsName');
    spyOn(component, 'loadData');

    component.ngOnInit();

    expect(component['setHeaderConfig']).toHaveBeenCalled();
    expect(component['setColumnsName']).toHaveBeenCalled();

    expect(component['loadData']).toHaveBeenCalled();
  }));


  it('should call updateStatus and showToastStatus message', () => {
    spyOn(component, 'updateStatus');

    channel.isActive = false;
    component.updateStatus(channel);

    expect(component.updateStatus).toHaveBeenCalled();
    expect(channel.isActive).toEqual(false);
  });

  it('should call openModalToSaveChannel', () => {
    spyOn(component, 'openModalToSaveChannel');

    component.openModalToSaveChannel();

    expect(component.openModalToSaveChannel).toHaveBeenCalled();
  });

  it('should call toggleModalToEditAction', () => {
    spyOn(component, 'toggleModalToEditAction');

    component.toggleModalToEditAction(channel);

    expect(component.toggleModalToEditAction).toHaveBeenCalled();
  });

  it('should call rowItemClicked', () => {
    spyOn(component, 'rowItemClicked');

    component.rowItemClicked(channel);

    expect(component.rowItemClicked).toHaveBeenCalled();
  });

  it('should call toggleModalToExcludeAction', () => {
    spyOn(component, 'toggleModalToExcludeAction');

    component.toggleModalToExcludeAction(channel);

    expect(component.toggleModalToExcludeAction).toHaveBeenCalled();
  });
});
