import { async, fakeAsync , ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { GetAllCompanyClientResultDto } from 'app/shared/models/dto/client/get-all-company-client-result-dto';

// component for test
import { CustomerNewEditComponent } from './customer-new-edit.component';

// include modules
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder } from '@angular/forms';

// models
import { ClientDto } from 'app/shared/models/dto/client/client-dto';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteStub, createAccessorComponent, loadPageEmitServiceStub } from '../../../../../testing';
import { clientResourceStub } from 'app/shared/resources/client/testing';
import { customerResourceStub } from 'app/shared/resources/consult-customer/testing';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { of } from 'rxjs';

const appAutocomplete = createAccessorComponent('app-autocomplete');
const thxError = createAccessorComponent('thx-error');

describe('CustomerNewEditComponent', () => {
  let component: CustomerNewEditComponent;
  let fixture: ComponentFixture<CustomerNewEditComponent>;

  const formBuilder = new FormBuilder();

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        CustomerNewEditComponent,
        appAutocomplete,
        thxError,
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        TranslateTestingModule,
        RouterTestingModule,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        clientResourceStub,
        customerResourceStub,
        toasterEmitServiceStub,
        loadPageEmitServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(CustomerNewEditComponent);
    component = fixture.componentInstance;

    component.parentForm = formBuilder.group({
      clientStationMatrix: '',
      stationName: '',
      socialName: '',
      subsidiaryCategory: '',
      homePage: '',
      isActive: true,
      personType: ''
    });

    component.dataSend = <ClientDto>{
      'locationList': [],
      'documentList': [],
      'companyClientContactPersonList': []
    };

    // apply changes to component and listen
    fixture.detectChanges();
  });


  it('should create the component', () => {
    expect(component).toBeTruthy();
  });


  it('should test if component form is correctly configurated', () => {

    expect(Object.keys(component.parentForm.controls)).toEqual([
      'clientStationMatrix',
      'stationName',
      'socialName',
      'subsidiaryCategory',
      'homePage',
      'isActive',
      'personType'
    ]);

  });

  it('should call saveFormAction mode save and edit', fakeAsync( () => {

    component.result = {
      stationName: 'testName' ,
      clientStationMatrix: 'testStation'
    };

    spyOn<any>(
      component['customerResource'],
      'addNewClientStation'
    )
    .and
    .returnValue(of(null));

    spyOn<any>(
     component['customerResource'],
    'editClientStation'
    ).and.returnValue(of(null));

    spyOn(
      component['customerResource'],
      'add'
    ).and.returnValue(of(null));

    spyOn(
        component['customerResource'],
        'edit'
    ).and.returnValue(of(null));

    component['mode'] = 'insert';
    component.saveFormAction();

    tick();


    expect(component['customerResource'].addNewClientStation).toHaveBeenCalled();
    expect(component['customerResource'].add).toHaveBeenCalled();

    // edit mode
    component['mode'] = 'edit';
    component.saveFormAction();

    tick();

    expect(component['customerResource'].editClientStation).toHaveBeenCalled();

  }));

  it('should call autocomplete method', async(() => {
      const list = {
        items: [
          {
            name: '111118811234567',
            document: '6666666666666'
          },
          {
            name: '6666886666',
            document: '88888345688'
          }
        ]
      };

      const data = { query : '123' };

      // first query test
      component.clientListToSearch = <ItemsResponse<GetAllCompanyClientResultDto>>list;

      component.searchItemsClient(data);


      expect(
        component
          .parentForm
          .get('clientStationMatrix')
          .value
      ).toEqual('123');

      expect(
        component.autocompleteResultList.length
      ).toEqual(1);


      // second query test
      data.query = '88';
      component.searchItemsClient(data);


      expect(
        component.autocompleteResultList.length
      ).toEqual(2);
  }));
});
