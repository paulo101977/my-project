import { Component, OnInit } from '@angular/core';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { CustomerResource } from '../../../shared/resources/consult-customer/customer.resource';
import { ClientResource } from '../../../shared/resources/client/client.resource';
import { Client } from '../../../shared/models/client';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AutocompleteConfig } from '../../../shared/models/autocomplete/autocomplete-config';
import { ItemsResponse } from '../../../shared/models/backend-api/item-response';
import { GetAllCompanyClientResultDto } from '../../../shared/models/dto/client/get-all-company-client-result-dto';
import { ClientDto } from '../../../shared/models/dto/client/client-dto';
import { ClientDocumentTypeEnum } from '../../../shared/models/dto/client/client-document-types';
import { CustomerStation, CustomerStationClient } from '../../../shared/models/integration/customerStation';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import {Location} from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'customer-new-edit',
  templateUrl: './customer-new-edit.component.html'
})
export class CustomerNewEditComponent implements OnInit {

  // client model
   public dataSend: ClientDto;

  // change the title to new or edit mode config
  public pageTitle = 'consultCustomerModule.createEdit.titleInsert';
  private mode = 'insert';
  private customerId: number;

  // form config
  public parentForm: FormGroup;
  public result: any;

  // header
  public configHeaderPage: ConfigHeaderPageNew;

  // autocomplete
  public propertyId: number;
  public autocompleteConfig: AutocompleteConfig;
  public autocompleteResultList: Array<any>;
  public clientListToSearch: ItemsResponse<GetAllCompanyClientResultDto>;

  // app select
  // TODO: create model
  public subsidiaryCategoryList;

  constructor(
    private loaderService: LoadPageEmitService,
    private toastEmitter: ToasterEmitService,
    private translateService: TranslateService,
    private customerResource: CustomerResource,
    private formBuilder: FormBuilder,
    private clientResource: ClientResource,
    public route: ActivatedRoute,
    public router: Router,
    private _location: Location,
  ) {}

  // table methods


  private loadSubsidiaryCategory() {
    this
      .customerResource
      .loadSubsidiaryCategory()
      .subscribe(res => this.subsidiaryCategoryList = res );
  }

  private loadClientToSearch() {
    this
      .clientResource
      .getAllCompanyClientResultDto(this.propertyId)
      .subscribe(res => this.clientListToSearch = res );
  }

  // form methods
  // init autocomplete
  private setAutoCompleteConfig = () => {
    this.autocompleteConfig = new AutocompleteConfig();

    this.autocompleteConfig.dataModel = this
      .parentForm
      .get('clientStationMatrix')
      .value;
    this.autocompleteConfig.callbackToSearch = this.searchItemsClient;
    this.autocompleteConfig.extraClasses = 'thf-u-font-input-default';
    this.autocompleteConfig.callbackToGetSelectedValue = this.updateItemClient;
    this.autocompleteConfig.fieldObjectToDisplay = 'templateClientSearch';
    this.translateService.get('placeholder.searchByNameAndCnpj').subscribe(value => {
      this.autocompleteConfig.placeholder = value;
    });
  }


  public searchItemsClient = (searchData: any) => {
    const reg = new RegExp(searchData.query, 'i');

    this.autocompleteResultList = new Array<any>();

    if (this.clientListToSearch) {
      const { items } = this.clientListToSearch;

      items.map(item => {
        const {name, document} = item;

        if (reg.test(name) || reg.test(document)) {
          this.autocompleteResultList.push({
            templateClientSearch: `${name} ${document}`,
            item
          });
        }
      });
    }

    this.parentForm.get('clientStationMatrix').setValue(searchData.query);
  }

  private updateItemClient = (query) => {

    const { name, homePage, document } = query.item;

    this.parentForm.get('clientStationMatrix').setValue(name);
    this.parentForm.get('homePage').setValue(homePage);
    this.parentForm.get('socialName').setValue(document);
  }

  // end autocomplete

  private setFormClient() {
    this.dataSend = new ClientDto();

    this.parentForm = this.formBuilder.group({
      clientStationMatrix: [null, [Validators.required]],
      stationName: [null, [Validators.required]],
      socialName: [null, [Validators.required]],
      subsidiaryCategory: null,
      homePage: [null, [Validators.required]],
      isActive: true,
      personType: [ClientDocumentTypeEnum.Legal, [Validators.required]],
    });
  }

  private setForm() {
    this.setFormClient();
    this.setAutoCompleteConfig();
  }

  // end form

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true
    };
  }


  public checkIsValid(): boolean {
    const { parentForm, dataSend } = this;
    const { documentList, locationList} = dataSend;

    return !(parentForm.valid && documentList.length && locationList.length);
  }

  public async saveFormAction()  {
    const { value } = this.parentForm;
    const { dataSend, customerResource, mode } = this;

    const method = mode === 'edit' ? 'edit' : 'add';
    const customerMethod  = mode === 'edit'
          ? 'editClientStation' : 'addNewClientStation';

    const sendObject = {
      ...value,
      ...dataSend,
      id: this.customerId
    };


    this.result = await customerResource[customerMethod](sendObject);

    // add to customer list
    if (this.result) {
        // TODO: create model
        const { stationName , clientStationMatrix } = <CustomerStationClient>this.result;
        const customer = <CustomerStation>{
          // TODO: the string type will be removed from the model
          // and the code in the final unmocked version
          id: `${this.customerId}`,
          stationName,
          client: clientStationMatrix
        };

        // TODO: add isActive property
        this.customerResource[method](customer).subscribe( res => {
            this._location.back();
            this.result = null;
        }, err => {
          console.error(err);
        });
    }

  }

  public cancelFormAction() {
    this._location.back();
  }

  private loadCustomerData() {
    this
      .customerResource
      .loadCustomerDataById(this.customerId)
      .subscribe( res => {

        const {
          clientStationMatrix,
          companyClientContactPersonList,
          documentList,
          homePage,
          id,
          isActive,
          locationList,
          personType,
          socialName,
          stationName,
          subsidiaryCategory,
        } = <CustomerStationClient>res;

        this.parentForm.setValue({
          clientStationMatrix,
          stationName,
          socialName,
          subsidiaryCategory,
          homePage,
          isActive,
          personType
        });


        this.dataSend.documentList = documentList;
        this.dataSend.locationList = locationList;
        this.dataSend.companyClientContactPersonList = companyClientContactPersonList;

        this.loaderService.emitChange(ShowHide.hide);
      }, err => {
        this.loaderService.emitChange(ShowHide.hide);
      });
  }

  ngOnInit() {
    // init head config
    this.setHeaderConfig();

    this.setForm();

    // load category list app-select
    this.loadSubsidiaryCategory();


    // get propertyId
     this.route.params.subscribe(params => {

        const { property, customerId, mode } = params;

        this.propertyId = +property;

        this.loadClientToSearch();

        if (mode === 'edit' && !!customerId) {

          this.mode = mode;
          this.pageTitle = 'consultCustomerModule.createEdit.titleEdit';
          this.customerId = +customerId;
          this.loaderService.emitChange(ShowHide.show);
          this.loadCustomerData();

        } else if (mode === 'insert') {

          this.mode = mode;
          this.pageTitle = 'consultCustomerModule.createEdit.titleInsert';

        } else {
          // reflesh or reload, customerId is empty
          this._location.back();
        }
     });
  }

}
