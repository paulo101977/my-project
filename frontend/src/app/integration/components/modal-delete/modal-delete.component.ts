import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { Channel } from '../../../shared/models/integration/channel';

@Component({
  selector: 'channel-modal-delete',
  templateUrl: './modal-delete.component.html'
})
export class ModalDeleteComponent implements OnInit {

  @Input() type;
  @Input() title;
  @Input() description;
  @Output() complete: EventEmitter<Channel> = new EventEmitter<Channel>();

  // Buttons dependencies
  public buttonCancelDeleteModal: ButtonConfig;
  public buttonConfirmDeleteModal: ButtonConfig;

  // selected row
  row: Channel;

  constructor(
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
  ) {}

  ngOnInit() {
    this.setButtonsConfig();
  }

  private setButtonsConfig() {
    this.buttonCancelDeleteModal = this.sharedService.getButtonConfig(
      'cancel-modal-delete',
      this.cancelDelete,
      'commomData.cancel',
      ButtonType.Secondary
    );

    this.buttonConfirmDeleteModal = this.sharedService.getButtonConfig(
      'save-modal-delete',
      this.confirmDelete,
      'commomData.confirm',
      ButtonType.Primary
    );
  }

  private cancelDelete = () => {
    this.row = null;
    this.toggleModalDelete();
  }

  private confirmDelete = () => {

    if (this.row) {
      this[this.type]
        .deleteById(this.row.id)
        .subscribe(res => {
          this.row = null;

          this.toggleModalDelete();
          this.complete.emit(this.row);
        });
    }
  }

  public openModalToDelete(row: Channel) {
    this.row = row;

    this.toggleModalDelete();
  }

  private toggleModalDelete = () => {
    this.modalEmitToggleService.emitToggleWithRef('channel-modal-delete');
  }

}
