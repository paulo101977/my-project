import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { Channel, ChannelCode, ChannelOrigin } from '../../../shared/models/integration/channel';
import { ChannelResource } from '../../../shared/resources/channel/channel.resource';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MarketSegment } from 'app/shared/models/dto/market-segment';
import { MarketSegmentResource } from 'app/shared/resources/market-segment/market-segment.resource';
import { map, pluck, switchMap, toArray } from 'rxjs/operators';
import { from } from 'rxjs';
import { CommomService } from 'app/shared/services/shared/commom.service';

@Component({
  selector: 'modal-add-edit',
  templateUrl: './modal-add-edit.component.html'
})
export class ModalAddEditComponent implements OnInit {

  @Output() complete: EventEmitter<Channel> = new EventEmitter<Channel>();

  // modal
  public formModalNew: FormGroup;
  public modalTitle = '';
  readonly CHANNEL_MODAL = 'channel-modal';

  // selection options
  codeOptionList: Array<ChannelCode>;
  originList: Array<ChannelOrigin>;
  marketSegmentList: Array<any>;
  marketSegmentListOptionList: Array<any>;
  propertyId: number;

  // button dependencies
  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private modalEmitToggleService: ModalEmitToggleService,
    private channelResource: ChannelResource,
    public route: ActivatedRoute,
    public router: Router,
    private translate: TranslateService,
    private marketSegmentResource: MarketSegmentResource,
    private commomService: CommomService
  ) {
  }

  private setFormModalChannel() {
    this.formModalNew = this.formBuilder.group({
      id: null,
      channelCodeId: [0, Validators.required],
      businessSourceId: [0, Validators.required],
      distributionAmount: 0,
      isActive: true,
      description: [null, [Validators.required, Validators.maxLength(60)]],
      businessSourceName: null,
      channelCodeName: null,
      marketSegmentId: 0
    });

    this.formModalNew.valueChanges.subscribe(values => {
      this.buttonSaveModalConfig.isDisabled = !this.formModalNew.valid;
    });
  }

  private resetForm() {
    this.formModalNew.reset({
      id: null,
      channelCodeId: 0,
      businessSourceId: 0,
      distributionAmount: 0,
      isActive: true,
      description: '',
      businessSourceName: '',
      channelCodeName: '',
      marketSegmentId: null
    });
  }

  private confirmSave = () => {
    let resource;
    const row = this.formModalNew.getRawValue();

    const item = <Channel>{
      id: row.id,
      channelCodeId: row.channelCodeId,
      businessSourceId: row.businessSourceId,
      description: row.description,
      distributionAmount: +row.distributionAmount,
      isActive: row.isActive,
      marketSegmentId: row.marketSegmentId
    };

    resource = item.id
      ? this.channelResource.edit(item)
      : this.channelResource.add(item);

    // commit change
    resource.subscribe(() => this.complete.emit(row));

    this.toggleModalEdit();
  }

  private setButtonConfig() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'reason-cancel-modal',
      this.toggleModalEdit,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
      'reason-save-modal',
      this.confirmSave,
      'commomData.confirm',
      ButtonType.Primary,
      null,
      true,
    );
  }

  private getMarketSegment() {

    this.marketSegmentResource
      .getAllMarketSegmentList(this.propertyId, false)
      .pipe(
        pluck('items'),
        switchMap( (items: Array<MarketSegment>) => from(items)),
        map( item => {
            const segmentName = item.name;
            return item.childMarketSegmentList.map( child => {
              return {
                ...child,
                fullName: `${segmentName} / ${child.name}`
              };
            });
        }),
        switchMap( (items: Array<any>) => from(items)),
        toArray()
      )
      .subscribe( result => {
      this.marketSegmentList = result;
      this.marketSegmentListOptionList = this.commomService
        .toOption(result, 'id', 'fullName');
    });
  }

  private getCodeOptionsList() {
    this.channelResource.getOptionList().subscribe(
      ({items}) => {
        if (items) {
          this.codeOptionList = items.map(({id, channelCode1}) => {
            return {
              key: id,
              value: channelCode1
            };
          });
        }
      }
    );
  }

  private getOriginOptionsList() {
    this.channelResource.getOriginList(this.propertyId).subscribe(
      ({items}) => {
        if (items) {
          this.originList = items.map(({id, description}) => {
            return {
              key: id,
              value: description
            };
          });
        }
      }
    );
  }

  public setTitle(modalTitle) {
    this.modalTitle = this.translate.instant(modalTitle, {
      labelName: this.translate.instant('label.channel')
    });
  }

  public openModalToEditChannel = (row: Channel) => {
    this.formModalNew.patchValue({
      id: row.id,
      channelCodeId: row.channelCodeId,
      businessSourceId: row.businessSourceId,
      distributionAmount: row.distributionAmount,
      isActive: row.isActive,
      description: row.description,
      businessSourceName: row.businessSourceName,
      channelCodeName: row.channelCodeName,
      marketSegmentId: row.marketSegmentId
    });

    this.setTitle('action.lbEdit');
    this.toggleModalEdit();
  }

  public openModalToSaveChannel = () => {
    this.setTitle('action.lbNew');
    this.resetForm();
    this.toggleModalEdit();
  }

  private toggleModalEdit = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.CHANNEL_MODAL);
  }

  ngOnInit() {
    // init form modal config
    this.setFormModalChannel();

    // init the modal button
    this.setButtonConfig();

    this.route.params.subscribe(params => {

      this.propertyId = +params.property;

      // load options
      this.getCodeOptionsList();
      this.getOriginOptionsList();
      this.getMarketSegment();
    });
  }

}
