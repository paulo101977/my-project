import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddEditComponent } from './modal-add-edit.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ButtonType } from 'app/shared/models/button-config';
import { channelData } from 'app/integration/components/mock/channel-data';
import { of } from 'rxjs';
import { Channel } from 'app/shared/models/integration/channel';
import { commomServiceStub } from 'testing';
import { TranslateTestingModule } from '@app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import { channelResourceStub } from 'app/shared/resources/channel/testing';
import { configureTestSuite } from 'ng-bullet';
import { marketResourceStub } from 'app/shared/resources/market-segment/testing';

xdescribe('ModalAddEditComponent', () => {
  let component: ModalAddEditComponent;
  let fixture: ComponentFixture<ModalAddEditComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      imports: [
        TranslateTestingModule,
        SharedModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ModalAddEditComponent],
      providers: [
        channelResourceStub,
        marketResourceStub,
        commomServiceStub,
        FormBuilder
      ],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setButtonConfig', () => {
      component['setButtonConfig']();

      expect(component.buttonCancelModalConfig).toEqual(component['sharedService'].getButtonConfig(
        'reason-cancel-modal',
        jasmine.any(Function),
        'commomData.cancel',
        ButtonType.Secondary,
      ));
      expect(component.buttonSaveModalConfig).toEqual(component['sharedService'].getButtonConfig(
        'reason-save-modal',
        jasmine.any(Function),
        'commomData.confirm',
        ButtonType.Primary,
        null,
        true,
      ));
    });
  });

  describe('actions', () => {
    it('should openModalToEditChannel', () => {
      spyOn(component, 'setTitle');
      spyOn<any>(component, 'toggleModalEdit');

      component['openModalToEditChannel'](channelData);

      expect(component.formModalNew.get('channelCodeName').value).toEqual(channelData.channelCodeName);
      expect(component.setTitle).toHaveBeenCalledWith('action.lbEdit');
      expect(component['toggleModalEdit']).toHaveBeenCalled();
    });

    it('should openModalToSaveChannel', () => {
      spyOn(component, 'setTitle');
      spyOn<any>(component, 'toggleModalEdit');

      component['openModalToSaveChannel']();

      expect(component.setTitle).toHaveBeenCalledWith('action.lbNew');
      expect(component['toggleModalEdit']).toHaveBeenCalled();
    });

    it('should toggleModalEdit', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');

      component['toggleModalEdit']();

      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.CHANNEL_MODAL);
    });

    it('should resetForm', () => {
      component['resetForm']();

      expect(component['formModalNew'].get('id').value).toBeNull();
      expect(component['formModalNew'].get('channelCodeId').value).toEqual(0);
      expect(component['formModalNew'].get('businessSourceId').value).toEqual(0);
      expect(component['formModalNew'].get('distributionAmount').value).toEqual(0);
      expect(component['formModalNew'].get('isActive').value).toBeTruthy();
      expect(component['formModalNew'].get('description').value).toEqual('');
      expect(component['formModalNew'].get('businessSourceName').value).toEqual('');
      expect(component['formModalNew'].get('channelCodeName').value).toEqual('');
    });
  });

  describe('resources', () => {
    it('should confirmSave#create', () => {
      spyOn<any>(component['channelResource'], 'add').and.returnValue(of({}));
      spyOn<any>(component, 'toggleModalEdit');

      channelData.id = '';
      component.formModalNew.patchValue(channelData);

      const item = <Channel>{
        id: channelData.id,
        channelCodeId: channelData.channelCodeId,
        businessSourceId: channelData.businessSourceId,
        description: channelData.description,
        distributionAmount: +channelData.distributionAmount,
        isActive: channelData.isActive,
        marketSegmentId: channelData.marketSegmentId ? channelData.marketSegmentId : 0
      };

      component['confirmSave']();

      expect(component['channelResource'].add).toHaveBeenCalledWith(item);
      expect(component['toggleModalEdit']).toHaveBeenCalled();
    });

    it('should confirmSave#edit', () => {
      spyOn<any>(component['channelResource'], 'edit').and.returnValue(of({}));
      spyOn<any>(component, 'toggleModalEdit');

      channelData.id = '3751359d-f8f2-4d0d-9456-13cbd750d1ed';
      component.formModalNew.patchValue(channelData);

      const item = <Channel>{
        id: channelData.id,
        channelCodeId: channelData.channelCodeId,
        businessSourceId: channelData.businessSourceId,
        description: channelData.description,
        distributionAmount: +channelData.distributionAmount,
        isActive: channelData.isActive,
        marketSegmentId: channelData.marketSegmentId ? channelData.marketSegmentId : 0
      };

      component['confirmSave']();

      expect(component['channelResource'].edit).toHaveBeenCalledWith(item);
      expect(component['toggleModalEdit']).toHaveBeenCalled();
    });
  });
});
