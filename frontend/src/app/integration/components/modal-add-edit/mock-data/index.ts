export class DATA {
    static MARKET_SEGMENT_LIST = {
        items: [{
          name: 'Market Segment 1',
          isActive: true,
          code: 'MS1',
          segmentAbbreviation: 'MS1',
          parentMarketSegmentId: 1,
          id: 1,
          childMarketSegmentList: [],
          propertyId: 1
        }],
        hasNext: false
      };
}
