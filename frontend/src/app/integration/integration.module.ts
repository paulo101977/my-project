import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChannelComponent } from './components/channel/channel.component';
import { IntegrationRoutingModule } from './integration-routing.module';


import { SharedModule } from '../shared/shared.module';
import { ModalAddEditComponent } from './components/modal-add-edit/modal-add-edit.component';
import { ModalDeleteComponent } from './components/modal-delete/modal-delete.component';
import { ConsultCustomerComponent  } from './components/consult-customer/consult-customer.component';
import { CustomerNewEditComponent } from './components/customer-new-edit/customer-new-edit.component';

// inside clientmodule, there are modals, forms, etc.
import { ClientModule } from '../client/client.module';

@NgModule({
  imports: [
    CommonModule,
    IntegrationRoutingModule,
    SharedModule,
    ClientModule
  ],
  declarations: [
    ChannelComponent,
    ModalAddEditComponent,
    ModalDeleteComponent,
    ConsultCustomerComponent,
    CustomerNewEditComponent,
  ]
})
export class IntegrationModule { }
