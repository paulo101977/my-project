import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChannelComponent } from './components/channel/channel.component';
import { ConsultCustomerComponent } from './components/consult-customer/consult-customer.component';
import { CustomerNewEditComponent } from './components/customer-new-edit/customer-new-edit.component';

export const routes: Routes = [
  { path: 'channel', component: ChannelComponent },
  { path: 'customer', component: ConsultCustomerComponent },
  { path: 'customer/new', component: CustomerNewEditComponent },
  { path: 'customer/edit', component: CustomerNewEditComponent },
  { path: 'log', loadChildren: '../integration-log/integration-log.module#IntegrationLogModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IntegrationRoutingModule {}
