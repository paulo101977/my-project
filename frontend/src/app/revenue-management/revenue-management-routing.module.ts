import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from '../auth/services/admin-guard.service';

export const routes: Routes = [
  {path: '', redirectTo: 'calendar', pathMatch: 'full'},
  {path: 'calendar', loadChildren: './calendar/calendar.module#CalendarModule', canActivate: [AdminGuard]},
  {path: 'base-rate', loadChildren: './base-rate/base-rate.module#BaseRateModule', canActivate: [AdminGuard]},
  {
    path: 'meal-plan-type',
    loadChildren: './meal-plan-type/meal-plan-type.module#MealPlanTypeModule',
    canActivate: [AdminGuard]
  },
  {path: 'rate-plan', loadChildren: './rate-plan/rate-plan.module#RatePlanModule', canActivate: [AdminGuard]},
  {path: 'reservation-budget/:reservationId', loadChildren: './reservation-budget/reservation-budget.module#ReservationBudgetModule'},
  {path: 'rate-strategy', loadChildren: './rate-strategy/rate-strategy.module#RateStrategyModule'},
  {path: 'meal-plan', loadChildren: './meal-plan/meal-plan.module#MealPlanModule'},
  {path: 'premises', loadChildren: './premises/premises.module#PremisesModule'},
  {path: 'level', loadChildren: './level/level.module#LevelModule'},
  {path: 'premises', loadChildren: './premises/premises.module#PremisesModule'},
  {path: 'rate-level', loadChildren: './rate-level/rate-level.module#RateLevelModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RevenueManagementRoutingModule {}
