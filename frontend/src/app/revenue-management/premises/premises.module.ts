import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PremisesListComponent } from './containers/premises-list/premises-list.component';
import { PremiseNewEditComponent } from './containers/premise-new-edit/premise-new-edit.component';
import { SharedModule } from './../../shared/shared.module';
import { PremisesRoutingModule } from './premises-routing.module';
import { PremiseTableComponent } from './components/premise-table/premise-table.component';
import { PremiseOperatorComponent } from './components/premise-operator/premise-operator.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    PremisesRoutingModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    PremisesListComponent,
    PremiseNewEditComponent,
    PremiseTableComponent,
    PremiseOperatorComponent,
  ]
})
export class PremisesModule { }
