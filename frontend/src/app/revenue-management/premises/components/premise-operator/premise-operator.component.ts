import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Operators } from '../../models/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-premise-operator',
  templateUrl: './premise-operator.component.html',
  styleUrls: ['./premise-operator.component.scss']
})
export class PremiseOperatorComponent implements OnInit {


  public operationSymbolList: Array<Operators>;

  public optionsCurrencyMask: any;
  public selected: string;
  public isOpen: boolean;
  public value: number;


  // operator
  @Input() set initialValue (_op: Operators) {
    this.selected = _op;
  }

  // input value
  @Input() set inputValue(_value: number) {
    this.value = _value;
  }


  @Output() changeOperator = new EventEmitter();
  @Output() valueChangedEmitter = new EventEmitter();

  constructor(
    private translateService: TranslateService,
  ) { }

  public toggle(e): void {
    e.stopPropagation();

    this.isOpen = !this.isOpen;
  }


  public selectItem(e, operator: Operators): void {
    e.stopPropagation();

    this.selected = operator;
    this.isOpen = false;

    this.changeOperator.emit(operator);
  }

  public valueChanged(value: number): void {
    this.valueChangedEmitter.emit(value);
  }

  public setMask(): void {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'center' };
  }


  public translateOperator(operator: Operators): string {
    switch (operator) {
      case Operators.fixed:
        return this.translateService.instant('text.fixed');
      case Operators.minus:
        return this.translateService.instant('text.subtraction');
      case Operators.sum:
        return this.translateService.instant('text.addition');
    }

    return '';
  }

  public retrieveOperatorsForEnum(): Array<Operators> {
    const keys = Object.keys(Operators);
    return keys.map(el => Object(Operators)[el]);
  }

  ngOnInit() {
    this.setMask();
    this.operationSymbolList = this.retrieveOperatorsForEnum();
    this.selected = this.operationSymbolList[0];
  }

}
