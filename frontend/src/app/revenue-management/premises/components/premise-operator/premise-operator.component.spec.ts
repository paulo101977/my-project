import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PremiseOperatorComponent } from './premise-operator.component';
import { Operators } from 'app/revenue-management/premises/models/operators';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';


describe('PremiseOperatorComponent', () => {
  let component: PremiseOperatorComponent;
  let fixture: ComponentFixture<PremiseOperatorComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiseOperatorComponent ],
      imports: [
        TranslateTestingModule,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(PremiseOperatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(component, 'setMask');
    spyOn(component, 'retrieveOperatorsForEnum').and.callThrough();
    spyOn(component, 'valueChanged').and.callThrough();
    spyOn(component, 'selectItem').and.callThrough();
    spyOn(component, 'toggle').and.callThrough();
    spyOn(component['changeOperator'], 'emit').and.returnValue(null);
    spyOn(component['valueChangedEmitter'], 'emit').and.returnValue(null);
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnInit methods', () => {
    component.ngOnInit();

    expect(component.retrieveOperatorsForEnum).toHaveBeenCalled();
    expect(component.setMask).toHaveBeenCalled();
    expect(component.selected).toEqual(Operators.sum);
  });

  it('should test toggle method', () => {
    const e = { stopPropagation: () => {} };
    component.toggle(e);

    expect(component.isOpen).toEqual(true);
  });

  it('should test selectItem method', () => {
    const e = { stopPropagation: () => {} };
    const op = Operators.minus;

    component.isOpen = true;

    component.selectItem(e, op);

    expect(component.isOpen).toEqual(false);
    expect(component.selected).toEqual(op);
    expect(component['changeOperator'].emit).toHaveBeenCalledWith(op);
  });

  it('should test valueChanged method', () => {
    const value = 2.22;

    component.valueChanged(value);

    expect(component['valueChangedEmitter'].emit).toHaveBeenCalledWith(value);
  });

  it('should test retrieveOperatorsForEnum method', () => {

    expect(component.retrieveOperatorsForEnum().length).toEqual( 3);
  });
});
