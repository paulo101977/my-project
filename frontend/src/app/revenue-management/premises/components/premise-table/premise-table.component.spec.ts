import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { PremiseTableComponent } from './premise-table.component';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';

import * as MOCK from './../../mock-data';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';


describe('PremiseTableComponent', () => {
  let component: PremiseTableComponent;
  let fixture: ComponentFixture<PremiseTableComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiseTableComponent ],
      imports: [
        TranslateTestingModule,
      ],
      providers: [
        FormBuilder
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(PremiseTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(component, 'setColumns').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngInit', () => {
    component.ngOnInit();

    expect(component.setColumns).toHaveBeenCalled();
  });

  it('should test setColumns', inject([TranslateService], (tl: TranslateService) => {
    const { CHILDREN_LIST_PROPERTY, DATA } = MOCK;
    component.childrenArr = [...DATA[CHILDREN_LIST_PROPERTY]];

    component.setColumns();


    expect(component.columns.length).toEqual(9);
  }));

  it('should test set selectedReference', () => {
    const obj = { id: 1 };
    const obj2 = { id: 2 };

    component._selectedReference = undefined;
    component._beforeReference = undefined;

    component.selectedReference = null;


    expect(component._selectedReference).toEqual(null);
    expect(component._beforeReference).toEqual(undefined);

    component.selectedReference = obj;


    expect(component._selectedReference).toEqual({ id: 1, isReference: true });
    expect(component._beforeReference).toEqual(component._selectedReference);

    component.selectedReference = obj2;

    expect(component._selectedReference).toEqual({ id: 2, isReference: true });
    expect<any>(obj).toEqual({id: 1, isReference: false});
  });

  it('should test changeValue', () => {
    const obj = {};

    component.changeValue(10, obj, 3, component['OPERATOR']);


    expect(Object.keys(obj)).toContain('pax3Operator');

    component.changeValue(123, obj, 7, component['VALUE']);

    expect(Object.keys(obj)).toContain('child1');
    expect(obj['child1']).toEqual(123);

    component.changeValue(1234, obj, 5, component['VALUE']);

    expect(Object.keys(obj)).toContain('pax5');
    expect(obj['pax5']).toEqual(1234);
  });
});
