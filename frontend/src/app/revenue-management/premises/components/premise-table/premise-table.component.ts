import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Pax } from 'app/revenue-management/premises/models/pax';


@Component({
  selector: 'app-premise-table',
  templateUrl: './premise-table.component.html',
  styleUrls: ['./premise-table.component.scss']
})
export class PremiseTableComponent implements OnInit {

  public readonly OPERATOR = 'Operator';
  public readonly VALUE = '';


  public columns: Array<any>;
  public _children: Array<any>;
  public ageText1: string;
  public ageText2: string;
  public ageText3: string;
  public _selectedReference: any;
  public _beforeReference: any;
  public _selectedPaxAmount: number;

  @Input() itemsList: Array<any>;

  @Input() set selectedPaxAmount( _selectedPaxAmount ) {
    this._selectedPaxAmount = _selectedPaxAmount;
  }

  @Input() set selectedReference( _selectedReference) {
    this._selectedReference = _selectedReference;

    if ( this._selectedReference ) {
      this._selectedReference.isReference = true;

      if ( this._beforeReference ) {
        this._beforeReference.isReference = false;
      }

      this._beforeReference = this._selectedReference;
    }
  }

  @Input() set childrenArr( arrChildren ) {
    this._children = arrChildren;
    this.setColumns();
  }

  @ViewChild('header1') header1: TemplateRef<any>;
  @ViewChild('header2') header2: TemplateRef<any>;
  @ViewChild('header3') header3: TemplateRef<any>;
  @ViewChild('title') title: TemplateRef<any>;
  @ViewChild('paxOne') paxOne: TemplateRef<any>;
  @ViewChild('paxTwo') paxTwo: TemplateRef<any>;
  @ViewChild('paxThree') paxThree: TemplateRef<any>;
  @ViewChild('paxFour') paxFour: TemplateRef<any>;
  @ViewChild('paxFive') paxFive: TemplateRef<any>;
  @ViewChild('paxAdditional') paxAdditional: TemplateRef<any>;
  @ViewChild('child1') child1: TemplateRef<any>;
  @ViewChild('child2') child2: TemplateRef<any>;
  @ViewChild('child3') child3: TemplateRef<any>;

  constructor(
    private translateService: TranslateService,
  ) { }



  public changeValue(
    value: number | string,
    row: any,
    col: Pax,
    type: string
  ) {
    const colInt = +col;

    switch (colInt) {
      case Pax.paxOne:
      case Pax.paxTwo:
      case Pax.paxThree:
      case Pax.paxFour:
      case Pax.paxFive:
        row[`pax${col}${type}`] = value;
        break;
      case Pax.paxAdditional:
        row[`paxAdditional${type}`] = value;
        break;
      case Pax.childOne:
        row[`child1${type}`] = value;
        break;
      case Pax.childTwo:
        row[`child2${type}`] = value;
        break;
      case Pax.childThree:
        row[`child3${type}`] = value;
        break;
    }
  }


  public setColumns() {
    const { _children } = this;

    this.columns = [
      {
        name: 'label.typeUHComplete',
        cellTemplate: this.title,
        sortable: false
      },
      {
        name: 'label.paxOne',
        prop: 'input1',
        cellTemplate: this.paxOne,
        sortable: false
      },
      {
        name: 'label.paxTwo',
        prop: 'input2',
        cellTemplate: this.paxTwo,
        sortable: false
      },
      {
        name: 'label.paxThree',
        prop: 'input3',
        cellTemplate: this.paxThree,
        sortable: false
      },
      {
        name: 'label.paxFour',
        prop: 'input1',
        cellTemplate: this.paxFour,
        sortable: false
      },
      {
        name: 'label.paxFive',
        prop: 'input2',
        cellTemplate: this.paxFive,
        sortable: false
      },
      {
        name: 'label.paxAdditional',
        prop: 'input3',
        cellTemplate: this.paxAdditional,
        sortable: false
      },
    ];

    if ( _children ) {
      _children.forEach( (child, i) => {
        const index = i + 1;
        if ( child.isActive ) {
          this.columns.push({
            headerTemplate: this[`header${ index }`],
            prop: `child${ index }`,
            cellTemplate: this[`child${ index }`],
            sortable: false
          });

          this[`ageText${ index }`] = this
            .translateService
            .instant('label.ageByYears', {
              min: child.propertyParameterMinValue,
              max: child.propertyParameterValue
            });
        }
      });
    }
  }


  ngOnInit() {
    this.setColumns();
  }

}
