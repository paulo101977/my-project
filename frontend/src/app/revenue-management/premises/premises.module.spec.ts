import { TranslateModule } from '@ngx-translate/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PremisesListComponent } from './containers/premises-list/premises-list.component';
import { SharedModule } from '@app/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('PremisesListModule', () => {
  let component: PremisesListComponent;
  let fixture: ComponentFixture<PremisesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [ PremisesListComponent ],
        imports: [
            TranslateModule.forRoot(),
            SharedModule,
            HttpClientTestingModule,
            RouterTestingModule
        ],
        providers: [],
        schemas: [NO_ERRORS_SCHEMA],
        })
        .compileComponents();
    }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremisesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
});

  it('should create an instance', () => {
    expect(component).toBeTruthy();
  });
});
