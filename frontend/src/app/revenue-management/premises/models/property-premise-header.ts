import { PropertyPremise } from './property-premise';
import { RoomType } from 'app/room-type/shared/models/room-type';
import { Currency } from 'app/shared/models/currency';
export class PropertyPremiseHeader {
  id: string;
  name: string;
  finalDate: string;
  initialDate: any;
  isActive: boolean;
  paxReference: number;
  propertyId: number;
  propertyPremiseList:  Array<PropertyPremise>;
  tenantId: string;
  currencyId: string;
  currency: Array<Currency>;
  roomTypeReferenceId: number;
  roomTypeReference: Array<RoomType>;
  roomTypeId: number;
  roomTypeName?: string;
}

