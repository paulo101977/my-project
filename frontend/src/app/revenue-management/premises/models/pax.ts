export enum Pax {
  paxOne = 1,
  paxTwo = 2,
  paxThree = 3,
  paxFour = 4,
  paxFive = 5,
  paxAdditional = 6,
  childOne = 7,
  childTwo = 8,
  childThree = 9
}
