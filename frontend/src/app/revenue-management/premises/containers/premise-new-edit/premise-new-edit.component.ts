import { Component, OnInit } from '@angular/core';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseRateResource } from 'app/shared/resources/base-rate/base-rate.resource';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { TranslateService } from '@ngx-translate/core';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { PremiseResource } from 'app/revenue-management/premises/resources/premise.resource';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { UserService } from 'app/shared/services/user/user.service';
import { PropertyPremiseHeader } from '../../models/property-premise-header';

@Component({
  selector: 'app-premise-new-edit',
  templateUrl: './premise-new-edit.component.html'
})
export class PremiseNewEditComponent implements OnInit {

  public readonly DEFAULT_OPERATOR = '+';

  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;

  // TODO: add model
  public form: FormGroup;
  public propertyId: number;
  public baseRateConfig: any;
  public children: any;
  public itemsList: Array<any>;
  public roomTypeList: Array<any>;
  public paxList: Array<any>;
  public currencyList: Array<any>;
  public selectedReference: any;
  public selectedPaxReference: number;
  public disableToday;
  public premiseId: any;
  public title: string;
  public premise: PropertyPremiseHeader;
  public tenantId: string;

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private baseRateResource: BaseRateResource,
    private route: ActivatedRoute,
    private sessionParameterService: SessionParameterService,
    private translateService: TranslateService,
    private commomService: CommomService,
    private dateService: DateService,
    private premiseResource: PremiseResource,
    private toasterEmitter: ToasterEmitService,
    private userService: UserService,
    private router: Router,
  ) { }


  public setForm() {

    this.form = this.formBuilder.group({
      name: [null, Validators.required ],
      roomTypeReferenceId: [null, Validators.required ],
      paxReference: [null, Validators.required ],
      currencyId: [null, Validators.required ],
      period: [ null , Validators.required ],
    });


    this.form.get('paxReference').valueChanges.subscribe( value => {
        this.selectedPaxReference = value;
    });

    this.form.get('roomTypeReferenceId').valueChanges.subscribe( value => {
      if ( value ) {
        this.selectedReference = this.findItemById(this.itemsList, value);
      }
    });


    this.form.valueChanges.subscribe( value => {
      this.confirmButtonConfig.isDisabled = !this.form.valid;
    });
  }

  public findItemById( arr: Array<any>, id: string ) {
    if ( arr ) {
      return arr.find( item => item['roomTypeId'] === +id );
    }

    return null;
  }

  public configButton() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel-button',
      this.cancel,
      'action.cancel',
      ButtonType.Secondary,
    );

    this.confirmButtonConfig =  this.sharedService.getButtonConfig(
      'confirm-button',
      this.confirm,
      'action.confirm',
      ButtonType.Primary,
      null,
      true,
    );
  }

  public goBack = () => {
    this.router.navigate(['..'], {relativeTo: this.route});
  }

  public cancel = () => {
    this.goBack();
  }

  public confirm = () => {

    let message = 'text.premiseSavedSuccess';

    const {
      currencyId,
      paxReference,
      period,
      name,
      roomTypeReferenceId,
    } = this.form.getRawValue();


    const senderObj = {
      propertyId: this.propertyId,
      initialDate: period.beginDate,
      finalDate: period.endDate,
      currencyId,
      name,
      roomTypeReferenceId,
      paxReference,
      propertyPremiseList: []
    };

    const isValid = this.isValidateSenderObj(roomTypeReferenceId, paxReference, this.itemsList);

    if ( !isValid ) {
      this.toasterEmitter.emitChange(SuccessError.error, 'label.fillAllField');
    } else {

      if ( this.itemsList ) {
        senderObj.propertyPremiseList = this.itemsList;
      }

      if ( this.premiseId ) {
        message = 'text.premiseEditedSuccess';
        this
          .premiseResource
          .editPremiseHeader(this.premiseId, senderObj)
          .subscribe( res => {
            this.toasterEmitter.emitChange(SuccessError.success, message);
            this.goBack();
          });
      } else {
        this
          .premiseResource
          .addNewPremiseHeader(senderObj)
          .subscribe(res => {
            this.toasterEmitter.emitChange(SuccessError.success, message);
            this.goBack();
          });
      }
    }
  }

  public getBaseRateConfig(): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .baseRateResource
        .getConfigurationDataParameters(this.propertyId)
        .subscribe( items => {
          this.baseRateConfig = items;

          resolve(items);
        });
    });
  }

  public getChildren() {
    return this.sessionParameterService.getChildren(this.propertyId);
  }

  public isValidateSenderObj(roomTypeReferenceId, paxReference: number, itemsList) {
    const { baseRateConfig, children } = this;
    let valid = true;

    if ( baseRateConfig && baseRateConfig.roomTypeList ) {
      const { roomTypeList } = baseRateConfig;

      if ( roomTypeList && roomTypeList.items && children) {
        roomTypeList.items.forEach(item => {
          const adultCount = +item.adultCapacity;
          const list = itemsList.find( data => data.roomTypeId === item.id );
          if ( !list ) {
            valid = false;
          }
          if ( adultCount > 5) {
            if ( list[`paxAdditional`] === undefined || isNaN(list[`paxAdditional`])) {
              valid = false;
            }
          }

          for (let i = 1; i <= adultCount && i <= 5 ; i++) {
            if ( (list[`pax${i}`] === undefined || isNaN(list[`pax${i}`])) &&
              !(list.roomTypeId === (+roomTypeReferenceId) && (+paxReference) === i) ) {
              valid = false;
              break;
            }
          }

          children.forEach((child, i) => {
            const index = i + 1;
            if ( child.isActive &&  ( list[`child${index}`] === undefined || isNaN(list[`child${index}`]))) {
              valid = false;
            }

          });
        });
        return valid;
      }
    } else {
      valid = false;
    }
    return valid;
  }

  public fillTable() {
    const { baseRateConfig, children, tenantId } = this;
    if ( baseRateConfig && baseRateConfig.roomTypeList ) {
      const { roomTypeList } = baseRateConfig;

      this.itemsList = [];

      roomTypeList['items'].forEach( item => {
        const obj = {};

        const {
          abbreviation,
          adultCapacity,
          minimumRate,
          maximumRate,
          id
        } = item;

        const maxAdult = +adultCapacity;


        obj['tenantId'] = tenantId;
        obj['roomTypeId'] = id;
        obj['abbreviation'] = abbreviation;
        obj['minimumRate'] = minimumRate;
        obj['maximumRate'] = maximumRate;
        obj['roomTypeListMinMaxRate'] = this
          .translateService
          .instant('label.roomTypeListMinMaxRate', { minimumRate, maximumRate });

        for (let i = 1; (i <= maxAdult && i <= 5); i++) {
          obj[`hasPax${i}`] = true;
          obj[`pax${i}Operator`] = this.DEFAULT_OPERATOR;
        }

        if ( maxAdult > 5 ) {
          obj[`hasPaxAdditional`] = true;
          obj['paxAdditionalOperator'] = this.DEFAULT_OPERATOR;
        }

        if ( children ) {
          children.forEach( (child, i) => {
            if ( child.isActive ) {
              obj[`hasChild${i + 1}`] = true;
              obj[`child${i + 1}Operator`] = this.DEFAULT_OPERATOR;
            }
          });
        }
        this.itemsList.push(obj);
      });
    }
  }

  public configRoomTypeList() {
    if ( this.baseRateConfig ) {
      const { roomTypeList } = this.baseRateConfig;

      this.roomTypeList =  this.commomService
        .toOption(
          roomTypeList['items'],
          'id',
          'abbreviation'
        );
    }
  }

  public fillPaxList() {
    this.paxList = [];

    const { baseRateConfig } = this;
    if ( baseRateConfig && baseRateConfig.roomTypeList ) {
      const { roomTypeList } = baseRateConfig;
      const lowerResult = roomTypeList['items'].reduce((prev, curr) => {
        return prev.adultCapacity < curr.adultCapacity ? prev : curr;
      });

      let lowerAdultCapacity = 0;

      if ( lowerResult ) {
        lowerAdultCapacity = +lowerResult.adultCapacity;
      }

      this.paxList = Array(lowerAdultCapacity)
        .fill(0)
        .map((e, i) => {
          const index = i + 1;
          return {
            key: index,
            value: `${index}`
          };
      });
    }
  }

  public getTenantId() {
    const user = this.userService.getUserLocalStorage();

    if ( user ) {
      this.tenantId = user.tenantId;
    }
  }

  public getCurrencies() {
    const { baseRateConfig } = this;

    this.currencyList = [];

    if ( baseRateConfig && baseRateConfig.currencyList ) {
      const items  = baseRateConfig.currencyList['items'];

      this.currencyList = items.map( item => {
        return {
          key: item.id,
          value: `${ item.currencyName } - (${ item.symbol })`
        };
      });
    }
  }

  public fillTableWithEditData( propertyPremiseList ) {

    const { itemsList } = this;
    if ( propertyPremiseList && itemsList) {
      propertyPremiseList.map( item => {
        const keys = Object.keys(item);

        const toEdit = this.itemsList.find( lItem =>
          (+lItem.roomTypeId) === (+item.roomTypeId)
        );

        if ( toEdit && keys && keys.length) {
          keys.forEach(key => {
            if ( key.indexOf('child') || key.indexOf('pax')) {
              toEdit[key] = item[key];
            }
          });
        }

      });
    }
  }

  public retrievePremiseHeader(): Promise<PropertyPremiseHeader> {
    return new Promise<PropertyPremiseHeader>( resolve => {
      this
        .premiseResource
        .retrievePremiseHeader(this.premiseId)
        .subscribe( item => {
          const {
            initialDate,
            finalDate,
            propertyPremiseList,
            ...formData
          } = item;

          this.premise = item;

          this.form.patchValue({
            ...formData,
            period: this.dateService.getDateRangePicker(initialDate, finalDate)
          });
          this.fillTableWithEditData( propertyPremiseList );
          resolve( null );
      });
    });
  }

  ngOnInit() {
    let systemDate;
    let dateObj;

    this.premiseId = this.route.snapshot.params.premiseId;
    this.title = 'title.insertPremise';

    this.configButton();
    this.setForm();

    this.route.params.subscribe(async params => {
      this.propertyId = +params.property;

      systemDate = this.dateService.getSystemDate(this.propertyId);

      dateObj = this
        .dateService
        .convertUniversalDateToIMyDate(systemDate);

      this.disableToday = dateObj ? dateObj.date : {};

      this.getTenantId();

      await this.getBaseRateConfig();

      this.children = this.getChildren();

      this.getCurrencies();
      this.configRoomTypeList();
      this.fillTable();
      this.fillPaxList();

      if ( this.premiseId ) {
        this.title = 'title.editPremise';

        await this.retrievePremiseHeader();
      }
    });

  }

}
