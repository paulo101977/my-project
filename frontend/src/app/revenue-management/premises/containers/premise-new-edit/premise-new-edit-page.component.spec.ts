import { PremiseNewEditComponent } from './premise-new-edit.component';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import {
  CHILDREN_SESSION_LIST_PROPERTY,
  CONFIGURATION_PROPERTY,
  DATA,
  DATE_PROPERTY, FORM_DATA_PROPERTY,
  FORM_PREMISE_INSERT_EDIT_PROPERTY, PREMISE_BY_ID_PROPERTY,
  TEXT_MESSAGE_SUCCESS_PROPERTY,
  FILL_TABLE_DATA_PROPERTY
} from './../../mock-data/index';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { createAccessorComponent, userServiceStub } from '../../../../../../testing';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { RouterTestingModule } from '@angular/router/testing';
import { sessionParameterServiceStub } from 'app/shared/services/parameter/testing';
import { baseRateResourceProvider } from 'app/shared/resources/base-rate/testing/base-rate.resource.provider';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { premiseResourceStub } from 'app/revenue-management/premises/resources/testing';

const thxError = createAccessorComponent('thx-error');
const thxDatePicker = createAccessorComponent('thx-date-range-picker');


describe('PremiseNewEditComponent', () => {
  let component: PremiseNewEditComponent;
  let fixture: ComponentFixture<PremiseNewEditComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        PremiseNewEditComponent,
        thxError,
        thxDatePicker,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateTestingModule,
      ],
      providers: [
        sharedServiceStub,
        commomServiceStub,
        dateServiceStub,
        userServiceStub,
        sessionParameterServiceStub,
        baseRateResourceProvider,
        toasterEmitServiceStub,
        premiseResourceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(PremiseNewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn<any>(component['dateService'], 'getSystemDate').and.returnValue({});
    spyOn<any>(component['dateService'], 'convertUniversalDateToIMyDate')
        .and
        .returnValue({date: DATA[DATE_PROPERTY] });

    spyOn<any>(component['baseRateResource'], 'getConfigurationDataParameters')
        .and
        .returnValue(of(DATA[CONFIGURATION_PROPERTY]));


    spyOn<any>(component, 'configButton');
    spyOn<any>(component, 'setForm');
    spyOn<any>(component['route'], 'params').and.returnValue(of( null ));
    spyOn<any>(component, 'getBaseRateConfig').and.callThrough();
    spyOn<any>(component, 'getChildren').and.callThrough();
    spyOn<any>(component, 'getCurrencies').and.callThrough();
    spyOn<any>(component, 'fillTable').and.callThrough();
    spyOn<any>(component, 'fillPaxList').and.callThrough();
    spyOn<any>(component, 'configRoomTypeList').and.callThrough();
    spyOn<any>(component, 'retrievePremiseHeader').and.callThrough();
    spyOn<any>(component['sessionParameterService'], 'getChildren')
        .and
        .returnValue( DATA[CHILDREN_SESSION_LIST_PROPERTY]);
    spyOn<any>(component['premiseResource'], 'retrievePremiseHeader')
        .and
        .returnValue( of(DATA[PREMISE_BY_ID_PROPERTY]) );
    spyOn<any>(component['premiseResource'], 'addNewPremiseHeader')
        .and
        .returnValue(of(true));
    spyOn<any>(component['premiseResource'], 'editPremiseHeader')
        .and
        .returnValue(of(true));
    spyOn<any>(component['toasterEmitter'], 'emitChange');
    spyOn<any>(component, 'getTenantId').and.callFake( () => {
      component.tenantId = '123';
    });
    spyOn<any>(component, 'goBack').and.callFake( () => {} );
    spyOn<any>(component['router'], 'navigate').and.callFake( () => {} );
    spyOn<any>(component, 'fillTableWithEditData').and.callThrough();
    component.baseRateConfig = DATA[CONFIGURATION_PROPERTY];
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngInit', fakeAsync(() => {
    component['route'].snapshot.params.premiseId = 2;

    component.ngOnInit();

    tick();

    expect(component.disableToday).toEqual(DATA[DATE_PROPERTY]);
    expect(component.configButton).toHaveBeenCalled();
    expect(component.getBaseRateConfig).toHaveBeenCalled();
    expect(component.getChildren).toHaveBeenCalled();
    expect(component.getCurrencies).toHaveBeenCalled();
    expect(component.fillTable).toHaveBeenCalled();
    expect(component.fillPaxList).toHaveBeenCalled();
    expect(component.setForm).toHaveBeenCalled();
    expect(component.configRoomTypeList).toHaveBeenCalled();
    expect(component.getTenantId).toHaveBeenCalled();
    expect(component.premiseId).toEqual(2);
    expect(component.retrievePremiseHeader).toHaveBeenCalled();
    expect(component.title).toEqual('title.editPremise');
    expect(component.children).toEqual(DATA[CHILDREN_SESSION_LIST_PROPERTY]);
  }));

  it('should test getCurrencies', () => {
    component.getCurrencies();

   expect(component.currencyList).toContain({
     key: '67838acb-9525-4aeb-b0a6-127c1b986c48',
     value: 'Real - (R$)'
   });
  });

  it('should test fillPaxList', () => {
    component.fillPaxList();

    expect(component.paxList.length).toEqual(1);
  });


  it('should test fillTable', () => {
    component.getTenantId();
    component.fillTable();

    expect(component.itemsList[1].roomTypeId).toEqual(3);

    expect(component.itemsList[6].roomTypeId).toEqual(31);
  });

  it('should test confirm save', fakeAsync(() => {

    spyOn(component, 'isValidateSenderObj').and.returnValue(true);
    component['form'].setValue(DATA[FORM_PREMISE_INSERT_EDIT_PROPERTY]);
    component['premiseId'] = undefined;

    component.confirm();

    tick();


    expect(component.goBack).toHaveBeenCalled();
    expect(component['toasterEmitter'].emitChange)
      .toHaveBeenCalledWith(true, DATA[TEXT_MESSAGE_SUCCESS_PROPERTY][0]);
    expect(component.confirmButtonConfig.isDisabled).toEqual(false);
  }));

  it('should test confirm edit', fakeAsync(() => {
    spyOn(component, 'isValidateSenderObj').and.returnValue(true);
    component['form'].setValue(DATA[FORM_PREMISE_INSERT_EDIT_PROPERTY]);
    component['premiseId'] = '1';

    component.confirm();

    tick();

    expect(component.goBack).toHaveBeenCalled();
    expect(component['toasterEmitter'].emitChange)
      .toHaveBeenCalledWith(true, DATA[TEXT_MESSAGE_SUCCESS_PROPERTY][1]);
    expect(component.confirmButtonConfig.isDisabled).toEqual(false);
  }));

  it('should test form change values', fakeAsync(() => {

    component.fillTable();
    component['form'].reset();

    component['form'].setValue(DATA[FORM_PREMISE_INSERT_EDIT_PROPERTY]);

    tick();

    expect<any>(component.selectedPaxReference).toEqual(DATA[FORM_PREMISE_INSERT_EDIT_PROPERTY].paxReference);
    expect(component.selectedReference).toEqual(component.itemsList[1]);
  }));

  // TODO: comment the file until solve the test problem
  // it('should test fillTableWithEditData', () => {
  //
  //   component.tenantId = GuidDefaultData;
  //   component.fillTable();
  //   component.fillTableWithEditData(DATA[PREMISE_BY_ID_PROPERTY].propertyPremiseList);
  //
  //   expect(component.itemsList).toContain(DATA[FILL_TABLE_ITEM_CHILD_PROPERTY][0]);
  //   expect(component.itemsList).toContain(DATA[FILL_TABLE_ITEM_CHILD_PROPERTY][1]);
  // });

  it('should test retrievePremiseHeader', fakeAsync(() => {

    const data = DATA[PREMISE_BY_ID_PROPERTY];
    const form = DATA[FORM_DATA_PROPERTY];

    spyOn(component['dateService'], 'getDateRangePicker').and.returnValue(form.period);
    component.fillTable();
    component.retrievePremiseHeader();

    tick();

    expect(component.premise).toEqual(data);
    expect(component.fillTableWithEditData).toHaveBeenCalledWith(DATA[PREMISE_BY_ID_PROPERTY].propertyPremiseList);
    expect(component['form'].value).toEqual(form);
  }));

  it('should test isValidateSenderObj', () => {
    const { roomTypeReferenceId, paxReference } = component['form'].value;
    component.children[2].isActive = false;
    expect(
    component.isValidateSenderObj(roomTypeReferenceId , paxReference, DATA[FILL_TABLE_DATA_PROPERTY])
    ).toBe(true);
  });

});
