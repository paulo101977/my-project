import { SuccessError } from '@app/shared/models/success-error-enum';
import { DateService } from '@app/shared/services/shared/date.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { PremiseResource } from './../../resources/premise.resource';
import { ConfigHeaderPageNew } from '@app/shared/components/header-page-new/config-header-page-new';
import { PropertyPremiseHeader } from '../../models/property-premise-header';
import { ToasterEmitService } from '@app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-premises-list',
  templateUrl: './premises-list.component.html',
  styleUrls: ['./premises-list.component.css']
})
export class PremisesListComponent implements OnInit {

  public premisesArray: any;
  public moneySymbolArray: Array<any>;
  public configHeaderPage: ConfigHeaderPageNew;
  public itemsOption: Array<any>;
  public columns: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private premiseResource: PremiseResource,
    private dateService: DateService,
    private toastEmitter: ToasterEmitService,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {
    this.setHeaderConfig();
    this.setColumns();
    this.loadPremises();

    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.goToEdit },
      { title: 'commomData.delete', callbackFunction: this.removePremise },
    ];
  }

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: false,
      keepTitleButton: true,
      callBackFunction: this.gotoNewPremise
    };
  }

  public setColumns() {
    this.columns = [{
      name: 'label.namePremise',
      prop: 'name'
    },
    {
      name: 'label.validityPeriod',
      prop: 'period'
    },
    {
      name: 'label.roomType',
      prop: 'roomTypeReferenceName'
    },
    {
      name: 'label.qtdPax',
      prop: 'paxReference'
    },
    {
      name: 'label.currency',
      prop: 'currencyName'
    }
    ];
  }

  public loadPremises() {
    this.premiseResource
        .getPropertyPremiseHeader()
        .subscribe( item => {
          this.premisesArray = item.items;
          this.setPremises();
    });
  }

  public setPremises() {
    if (this.premisesArray) {
      this.premisesArray =  this.premisesArray.map( item => {
        return {
          ...item,
          roomTypeReferenceName: `${item.roomTypeReference.abbreviation}`,
          currencyName: `${item.currency.currencyName} (${item.currency.symbol})`,
          period: this.dateService.convertUniversalDateToViewFormat((item.initialDate)) + ' - ' +
          this.dateService.convertUniversalDateToViewFormat((item.finalDate))
        };
      });
    }
  }

  public goToEdit = (row) => {
    const { id } = row;
    this.router.navigate(
      [`add-edit/${id}`],
      { relativeTo: this.route}
    );
  }

  public gotoNewPremise = () => {
    this.router.navigate(
      [`add-edit`],
      { relativeTo: this.route}
    );
  }

  public removePremise = (item: PropertyPremiseHeader) => {
    const { id } = item;

    this.premiseResource
    .deletePropertyPremiseHeader(id).subscribe(response => {
      this.toastEmitter.emitChange(
        SuccessError.success,
        this.translateService.instant('variable.lbDeleteSuccessM', {
          labelName: this.translateService.instant('label.premise'),
        }),
      );
      this.loadPremises();
    });
  }

  public updateStatus(event: PropertyPremiseHeader): void {
    const { isActive, id } = event;

    this.premiseResource.setTogglePremiseHeader(id).subscribe(
      () => {
        this.showToastStatus(isActive);
      }
    );
  }

  public showToastStatus(modify: boolean) {
    const typeMessage = SuccessError.success;
    let message = 'label.premiseStatusInactivateSuccess';

    if ( modify ) {
      message = 'label.premiseStatusActivateSuccess';
    }

    this
      .toastEmitter
      .emitChange(typeMessage, this.translateService.instant(message));
  }

}
