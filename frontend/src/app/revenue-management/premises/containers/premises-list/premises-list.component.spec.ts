import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {Observable, of} from 'rxjs';
import { PremisesListComponent } from './premises-list.component';
import * as moment from 'moment';
import {TranslateTestingModule} from 'app/shared/mock-test/translate-testing.module';
import {createServiceStub} from '../../../../../../testing';
import {PremiseResource} from 'app/revenue-management/premises/resources/premise.resource';
import {PropertyPremiseHeader} from 'app/revenue-management/premises/models/property-premise-header';
import {ItemsResponse} from 'app/shared/models/backend-api/item-response';

const premiseArray = {
  creationTime: '2019-01-18T19:04:56',
  creatorUserId: 'ae45fef3-81f5-4d15-ed6e-08d648aa4156',
  currencyAlphabeticCode: 'BRL',
  currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
  currencyName: 'Real',
  currencySymbol: 'R$',
  finalDate: '2019-01-18T19:04:56',
  id: 'bbe77a49-826b-41b6-b887-d480e35a35ce',
  initialDate: '2019-01-18T19:04:56',
  isActive: true,
  isDeleted: false,
  name: 'Tester',
  paxReference: 1,
  propertyId: 1,
  propertyPremiseHeaderId: 'bbe77a49-826b-41b6-b887-d480e35a35ce',
  propertyPremiseId: '00000000-0000-0000-0000-000000000000',
  propertyPremiseList: [],
  roomTypeId: 0,
  roomTypeReferenceId: 1,
  roomTypeReferenceName: 'LXO',
  roomTypeReference: {
    abbreviation: 'LXO'
  },
  currency: {
    alphabeticCode: 'BRL',
    currencyName: 'Real',
    symbol: 'R$'
  },
  tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9'
};

const premiseResourceProvider = createServiceStub(PremiseResource, {
  deletePropertyPremiseHeader (id: string): Observable<PropertyPremiseHeader> {
    return of(null);
  },
  setTogglePremiseHeader (id: string): Observable<Array<PropertyPremiseHeader>> {
    return of(null);
  },
  getPropertyPremiseHeader (): Observable<ItemsResponse<PropertyPremiseHeader>> {
    return of({
      hasNext: false,
      items: []
    });
  }
});

describe('PremisesListComponent', () => {
  let component: PremisesListComponent;
  let fixture: ComponentFixture<PremisesListComponent>;
  let premiseResource;
  moment.locale('pt-BR');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PremisesListComponent
      ],
      imports: [
        CommonModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateTestingModule,
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [ premiseResourceProvider ]
    })
    .compileComponents();
    premiseResource = TestBed.get(PremiseResource);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremisesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call init functions', () => {
    spyOn<any>(component, 'setHeaderConfig');
    spyOn(component, 'setColumns');
    component.ngOnInit();

    expect(component['setHeaderConfig']).toHaveBeenCalled();
    expect(component['setColumns']).toHaveBeenCalled();
  });

  it('should call load premises', fakeAsync(() => {
    const item = {
      items: [premiseArray]
    };
    spyOn<any>(component['premiseResource'], 'getPropertyPremiseHeader')
      .and.returnValue(of(item));
    component.loadPremises();
    tick();

    expect(component.premisesArray).toContain(jasmine.objectContaining({
      ...premiseArray,
      currencyName: `${premiseArray.currencyName} (${premiseArray.currencySymbol})`,
      period: '18/01/2019 - 18/01/2019'
    }));
  }));

});
