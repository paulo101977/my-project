import { TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { PremiseResource } from './premise.resource';
import { ThexApiService, ThexApiTestingModule } from '@inovacaocmnet/thx-bifrost';

describe('PremiseResource', () => {
  let premiseResource: PremiseResource;
  let thexApi: ThexApiService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [
        PremiseResource
      ],
      imports: [
        ThexApiTestingModule
      ]
    });

    premiseResource = TestBed.get(PremiseResource);
    thexApi = TestBed.get(ThexApiService);
  });

  it('should create an instance', () => {
    expect(premiseResource).toBeTruthy();
  });

  describe('getPropertyPremiseHeaderByDate', () => {
    beforeEach(() => {
      spyOn(thexApi, 'get').and.callFake(url => url);
    });

    it('shoud return with startDate', () => {
      const startDate = '2010-01-01';
      const url = premiseResource.getPropertyPremiseHeaderByDate(startDate, '', '');
      expect(url.toString().includes(startDate)).toBeTruthy();
    });

    it('shoud return with startDate, endDate', () => {
      const startDate = '2010-01-01';
      const endDate = '2010-01-02';
      const url = premiseResource.getPropertyPremiseHeaderByDate(startDate, endDate, '');
      expect(url.toString().includes(startDate)).toBeTruthy();
      expect(url.toString().includes(endDate)).toBeTruthy();
    });

    it('shoud return with startDate, endDate, currencyId', () => {
      const startDate = '2010-01-01';
      const endDate = '2010-01-02';
      const currencyId = '21481957-A8F5-446C-AF10-5F297A9D1E88';
      const url = premiseResource.getPropertyPremiseHeaderByDate(startDate, endDate, currencyId);
      expect(url.toString().includes(startDate)).toBeTruthy();
      expect(url.toString().includes(endDate)).toBeTruthy();
      expect(url.toString().includes(currencyId)).toBeTruthy();
    });
  });
});
