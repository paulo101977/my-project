import { createServiceStub } from '../../../../../../testing';
import { PremiseResource } from 'app/revenue-management/premises/resources/premise.resource';
import { of } from 'rxjs';

export const premiseResourceStub = createServiceStub(PremiseResource, {
    addNewPremiseHeader: (item) => of( null ),

    deletePropertyPremiseHeader: (id: string) => of( null ),

    editPremiseHeader: (premiseId, item ) => of( null ),

    getPropertyPremise: () => of( null ),

    getPropertyPremiseHeader: () => of( null ),

    getPropertyPremiseHeaderByDate: (startDate: string, finalDate: string, currencyId: string) => of( null ),

    retrievePremiseHeader: (id) => of( null ),

    setTogglePremiseHeader: (id: string) => of( null ),
});
