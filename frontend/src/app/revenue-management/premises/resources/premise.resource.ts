import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

import { PropertyPremiseHeader } from '../models/property-premise-header';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { PropertyPremise } from '../models/property-premise';
import { ItemsResponse } from '@app/shared/models/backend-api/item-response';

@Injectable({ providedIn: 'root' })
export class PremiseResource {
  private urlPremiseHeader = 'propertyPremiseHeader';
  private urlPremise = 'propertyPremise';

  constructor(private thexApi: ThexApiService) {}

  public getPropertyPremise():
    Observable<PropertyPremise> {
      return this
        .thexApi
        .get<PropertyPremise>(this.urlPremise);
  }

  public getPropertyPremiseHeader():
    Observable<ItemsResponse<PropertyPremiseHeader>> {
      return this
        .thexApi
        .get<ItemsResponse<PropertyPremiseHeader>>(this.urlPremiseHeader);
  }

  public getPropertyPremiseHeaderByDate(startDate: string, finalDate: string, currencyId: string):
    Observable<ItemsResponse<PropertyPremiseHeader>> {
      let url = `${this.urlPremiseHeader}?startDate=${startDate}`;
      if (finalDate) {
        url += `&finalDate=${finalDate}`;
      }

      if (currencyId) {
        url += `&currencyId=${currencyId}`;
      }

      return this
        .thexApi
        .get<ItemsResponse<PropertyPremiseHeader>>(url);
  }

  public addNewPremiseHeader(item):
    Observable<any> {
    return this
      .thexApi
      .post(this.urlPremiseHeader, item);
  }

  public editPremiseHeader( premiseId, item ): Observable<any> {
    const param = item;
    return this
    .thexApi
    .put(`${this.urlPremiseHeader}/${premiseId}`, param);
  }

  public retrievePremiseHeader(id): Observable<any> {
    return this
      .thexApi
      .get(`${this.urlPremiseHeader}/${id}`);
  }

  public setTogglePremiseHeader( id: string): Observable<Array<PropertyPremiseHeader>> {
    return this
      .thexApi
      .patch<Array<PropertyPremiseHeader>>(`${this.urlPremiseHeader}/${id}`);
  }

  public deletePropertyPremiseHeader(id: string):
    Observable<PropertyPremiseHeader> {
      return this
        .thexApi
        .delete<PropertyPremiseHeader>(`${this.urlPremiseHeader}/${id}`);
  }

}
