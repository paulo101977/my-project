import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PremisesListComponent } from './containers/premises-list/premises-list.component';
import { PremiseNewEditComponent } from './containers/premise-new-edit/premise-new-edit.component';

export const propertyBaseRoutes: Routes = [
  { path: '', component: PremisesListComponent },
  { path: 'add-edit', component: PremiseNewEditComponent},
  { path: 'add-edit/:premiseId', component: PremiseNewEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(propertyBaseRoutes)],
  exports: [RouterModule],
})
export class PremisesRoutingModule {}
