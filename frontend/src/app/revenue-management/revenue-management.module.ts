import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarModule } from './calendar/calendar.module';
import { MealPlanTypeModule } from './meal-plan-type/meal-plan-type.module';
import { BaseRateModule } from './base-rate/base-rate.module';
import { RatePlanModule } from './rate-plan/rate-plan.module';
import { ReservationBudgetModule } from './reservation-budget/reservation-budget.module';
import { RevenueManagementRoutingModule } from './revenue-management-routing.module';
import { RateStrategyModule } from './rate-strategy/rate-strategy.module';
import { PremisesModule } from './premises/premises.module';
import {RateLevelModule} from './rate-level/rate-level.module';
import {LevelModule} from './level/level.module';


@NgModule({
  imports: [
    CommonModule,
    CalendarModule,
    MealPlanTypeModule,
    BaseRateModule,
    RatePlanModule,
    RateStrategyModule,
    ReservationBudgetModule,
    RevenueManagementRoutingModule,
    PremisesModule,
    LevelModule,
    RateLevelModule
  ],
  declarations: [],
})
export class RevenueManagementModule {}
