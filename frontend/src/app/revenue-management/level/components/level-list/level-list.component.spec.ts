import { ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { LevelListComponent } from './level-list.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {
  Data,
  LEVEL_LIST
} from 'app/revenue-management/level/mock-data';
import {SuccessError} from 'app/shared/models/success-error-enum';
import {of} from 'rxjs/internal/observable/of';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { ActivatedRouteStub } from '../../../../../../testing';
import { ActivatedRoute } from '@angular/router';
import { levelResourceStub } from 'app/revenue-management/level/resource/testing';

describe('LevelListComponent', () => {
  let component: LevelListComponent;
  let fixture: ComponentFixture<LevelListComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        LevelListComponent
      ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
      ],
      providers: [
        toasterEmitServiceStub,
        levelResourceStub,
        {provide: ActivatedRoute, useClass: ActivatedRouteStub}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(LevelListComponent);
    component = fixture.componentInstance;

    component.levelList = Data[LEVEL_LIST];
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn<any>(component, 'setHeaderConfig');
    spyOn<any>(component, 'setColumnsName');
    spyOn(component, 'loadData');

    component.ngOnInit();

    expect(component['setHeaderConfig']).toHaveBeenCalled();
    expect(component['setColumnsName']).toHaveBeenCalled();
    expect(component.loadData).toHaveBeenCalled();
    expect(component.itemsOption[0].title).toEqual('commomData.edit');
  });

  it ('should loadData', fakeAsync( () => {
    const item = { items: Data[LEVEL_LIST] };

    spyOn(component['levelResource'], 'getAllLevel').and.returnValue(of( item ));
    component.loadData();
    tick();

    expect(component.levelList).toEqual(component.levelListBack);
    expect(component.levelList[0].levelCode).toEqual(Data[LEVEL_LIST][0].levelCode);
    expect(component.levelList[0].description).toEqual(Data[LEVEL_LIST][0].description);
    expect(component.levelList[1].levelCode).toEqual(Data[LEVEL_LIST][1].levelCode);
    expect(component.levelList[1].description).toEqual(Data[LEVEL_LIST][1].description);
  }));

  it('should updateStatus', fakeAsync(() => {
    spyOn(component, 'showToastStatus');
    spyOn(component['levelResource'], 'setToggleLevel').and.returnValue(of(null));
    component.updateStatus(Data[LEVEL_LIST][0]);
    tick();
    expect(component.showToastStatus).toHaveBeenCalled();
  })) ;

  describe('should showToastStatus', () => {
    it('is true', () => {
      spyOn(component['toastEmitter'], 'emitChange');
      component.showToastStatus(true);

      expect(component['toastEmitter'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'label.levelStatusActivateSuccess'
      );
    });

    it('is false', () => {
      spyOn(component['toastEmitter'], 'emitChange');
      component.showToastStatus(false);

      expect(component['toastEmitter'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'label.levelStatusInactivateSuccess'
      );
    });
  });
});
