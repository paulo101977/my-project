import { Component, OnInit } from '@angular/core';
import { LevelModal } from 'app/revenue-management/level/models/level-modal';
import { LevelModalDeleteService } from 'app/revenue-management/level/services/level-modal-delete.service';
import { LevelModalFormService } from 'app/revenue-management/level/services/level-modal-form.service';
import {ConfigHeaderPageNew} from 'app/shared/components/header-page-new/config-header-page-new';
import {LevelModel} from 'app/revenue-management/level/models/level.model';
import {SuccessError} from 'app/shared/models/success-error-enum';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';
import {LevelResource} from 'app/revenue-management/level/resource/level.resource';

@Component({
  selector: 'app-level-list',
  templateUrl: './level-list.component.html',
})
export class LevelListComponent implements OnInit {

  public configHeaderPage: ConfigHeaderPageNew;
  public columns: Array<any>;
  public itemsOption: Array<any>;

  public levelList: Array<LevelModel>;
  public levelListBack: Array<LevelModel>;

  constructor(
    private toastEmitter: ToasterEmitService,
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private levelModalFormService: LevelModalFormService,
    private levelModalDeleteService: LevelModalDeleteService,
    private levelResource: LevelResource
  ) { }

  ngOnInit() {
    this.setHeaderConfig();
    this.setColumnsName();
    this.loadData();

    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: (row) => this.gotoEdit(row) },
      { title: 'commomData.delete', callbackFunction: (row) => this.callExclude(row) },
    ];
  }

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: false,
      keepTitleButton: true,
      callBackFunction: f => {}
    };
  }

  // table methods:
  private setColumnsName(): void {
    this.columns = [
      {
      name: 'label.exhibition',
      prop: 'order',
      },
      {
        name: 'label.levelCode',
        prop: 'levelCode',
      },
      {
        name: 'label.leveldescription',
        prop: 'description',
      }
    ];
  }

  public loadData() {
    this.levelResource.getAllLevel(true).subscribe( ({items}) => {
      this.levelList = items;
      this.levelListBack = items;
    });
  }

  public gotoEdit(row: LevelModal) {
    this.openFormModal(row);
  }

  public callExclude(row: LevelModal) {
    this.openDeleteModal(row);
  }

  public updateStatus($event): void {
    const { isActive, id } = <LevelModel>$event;
    this.levelResource.setToggleLevel(id).subscribe(
      () => {
        this.showToastStatus(isActive);
      }
    );
  }

  public showToastStatus(modify: boolean) {
    const typeMessage = SuccessError.success;

    let message = 'label.levelStatusInactivateSuccess';

    if ( modify ) {
      message = 'label.levelStatusActivateSuccess';
    }

    this
      .toastEmitter
      .emitChange(typeMessage, this.translateService.instant(message));
  }

  public rowItemClicked(row): void {
    this.gotoEdit(row);
  }

  public search(item: any) {
    const { value } = item.target;

    if (value != '' && value.length > 1) {
      const regexp = new RegExp(value, 'i');
      this.levelList = this
        .levelListBack
        .filter(v => {
          return regexp.test(v.levelCode.toString())
            || regexp.test(v.description);
        });
    } else {
      // the original resource
      this.levelList = this.levelListBack;
    }
  }

  openFormModal(levelData?: LevelModal) {
    this.levelModalFormService.openModal(levelData);
  }

  openDeleteModal(levelData: LevelModal) {
    this.levelModalDeleteService.openModal(levelData);
  }

  returnModal(modeEdit) {
    this.levelModalFormService.closeModal();
    if (!modeEdit) {
      this.showToast('variable.lbSaveSuccessM', {
        labelName: this.translateService.instant('label.level')
      });
    } else {
      this.showToast('variable.lbEditSuccessM', {
        labelName: this.translateService.instant('label.level')
      });
    }
    this.loadData();
  }

  public showToast(message: string, options?) {
    this
      .toastEmitter
      .emitChange(SuccessError.success, this.translateService.instant(message, options));
  }
}
