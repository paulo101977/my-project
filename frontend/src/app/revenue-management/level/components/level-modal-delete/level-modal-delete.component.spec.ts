import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LevelResource } from 'app/revenue-management/level/resource/level.resource';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { createServiceStub } from '../../../../../../testing';

import { LevelModalDeleteComponent } from './level-modal-delete.component';

describe('LevelModalDeleteComponent', () => {
  let component: LevelModalDeleteComponent;
  let fixture: ComponentFixture<LevelModalDeleteComponent>;

  const LevelResourceStub = createServiceStub(LevelResource, {
    deleteLevel(id: string): Observable<any> { return of(null); }
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ LevelModalDeleteComponent ],
      imports: [
        TranslateTestingModule
      ],
      providers: [
        LevelResourceStub
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LevelModalDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
