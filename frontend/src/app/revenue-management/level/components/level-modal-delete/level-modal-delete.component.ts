import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LevelModal } from 'app/revenue-management/level/models/level-modal';
import { LevelResource } from 'app/revenue-management/level/resource/level.resource';
import { LevelModalDeleteService } from 'app/revenue-management/level/services/level-modal-delete.service';
import { ButtonType } from 'app/shared/models/button-config';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-level-modal-delete',
  templateUrl: './level-modal-delete.component.html',
  styleUrls: ['./level-modal-delete.component.css']
})
export class LevelModalDeleteComponent implements OnInit {
  public buttonCancelConfig: any;
  public buttonConfirmConfig: any;
  public selectedLevel: LevelModal;

  @Output() deleted = new EventEmitter();

  constructor(
    public levelModalDeleteService: LevelModalDeleteService,
    private sharedService: SharedService,
    private levelResource: LevelResource,
    private translateService: TranslateService,
    private toastEmitter: ToasterEmitService
  ) { }

  ngOnInit() {
    this.setButtonsConfigs();
    this.levelModalDeleteService.sentModalData$
      .subscribe((levelData) => {
        this.selectedLevel = levelData;
      });
  }

  private setButtonsConfigs() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'level-modal-delete-cancel',
      () => this.closeModal(),
      'action.no',
      ButtonType.Secondary,
    );

    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'level-modal-delete-confirm',
      () => this.deleteLevel(),
      'action.yes',
      ButtonType.Primary,
    );
  }

  public deleteLevel() {
    this.levelResource.deleteLevel(this.selectedLevel.id).subscribe(() => {
      this.deleted.emit();
      this
        .toastEmitter
        .emitChange(SuccessError.success, this.translateService.instant('variable.lbDeleteSuccessM', {
          labelName: this.translateService.instant('label.level')
        }));
      this.levelModalDeleteService.closeModal();
    });
  }

  public closeModal() {
    this.levelModalDeleteService.closeModal();
  }
}
