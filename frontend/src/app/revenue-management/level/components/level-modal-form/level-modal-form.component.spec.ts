import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

import { LevelModalFormComponent } from './level-modal-form.component';
import { configureTestSuite } from 'ng-bullet';
import { createAccessorComponent } from '../../../../../../testing';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { levelResourceStub } from 'app/revenue-management/level/resource/testing';

const thxError = createAccessorComponent('thx-error');

describe('LevelModalFormComponent', () => {
  let component: LevelModalFormComponent;
  let fixture: ComponentFixture<LevelModalFormComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [
        LevelModalFormComponent,
        thxError,
      ],
      imports: [
        TranslateTestingModule,
      ],
      providers: [
        FormBuilder,
        sharedServiceStub,
        levelResourceStub,
      ]
    });

    fixture = TestBed.createComponent(LevelModalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('form', () => {
    it('should be invalid when there is no code', () => {
      component.form.patchValue({
        description: '111',
        order: '111',
        isActive: '111',
      });

      expect(component.form.invalid).toBe(true);
    });

    it('should be invalid when there is no description', () => {
      component.form.reset();
      component.form.patchValue({
        levelCode: 111,
        order: '111',
        isActive: '111',
      });

      expect(component.form.invalid).toBe(true);
    });

    it('should be invalid when there is no order', () => {
      component.form.reset();
      component.form.patchValue({
        levelCode: 111,
        description: '111',
        isActive: '111',
      });

      expect(component.form.invalid).toBe(true);
    });

    it('should be valid even if there is no isActive', () => {
      component.form.patchValue({
        levelCode: 111,
        description: '111',
        order: '111',
      });

      expect(component.form.valid).toBe(true);
    });
  });
});
