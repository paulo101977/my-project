import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { LevelModal } from '../../models/level-modal';
import { LevelModalFormService } from 'app/revenue-management/level/services/level-modal-form.service';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import {LevelResource} from 'app/revenue-management/level/resource/level.resource';

@Component({
  selector: 'app-level-modal-form',
  templateUrl: './level-modal-form.component.html',
  styleUrls: ['./level-modal-form.component.css']
})
export class LevelModalFormComponent implements OnInit {
  public form: FormGroup;
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;
  private modeEdit: boolean;
  public modalTitle: string;
  @Output() public confirm = new EventEmitter<boolean>();

  constructor(
    public levelModalFormService: LevelModalFormService,
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private levelResource: LevelResource,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setForm();
    this.setButtonsConfigs();
    this.listenModalCall();
  }

  public submit() {
    if (this.form.invalid) {
      return;
    }
    if (!this.modeEdit) {
      this.newLevel();
    } else {
      this.editLevel();
    }
  }

  public cancel() {
    this.levelModalFormService.closeModal();
  }

  private setButtonsConfigs() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'level-modal-form-cancel',
      () => this.cancel(),
      'action.cancel',
      ButtonType.Secondary,
    );

    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'level-modal-form-confirm',
      () => this.submit(),
      'action.confirm',
      ButtonType.Primary,
    );
  }

  private setForm() {
    this.form = this.formBuilder.group({
      levelCode: [null, [Validators.required, Validators.maxLength(10)]],
      description: [null, [Validators.required, Validators.maxLength(60)]],
      order: [null, [Validators.required, Validators.maxLength(3)]],
      isActive: true,
      id: null
    });

    this.form.valueChanges.subscribe(() => {
      this.buttonConfirmConfig.isDisabled = this.form.invalid;
    });
  }

  private listenModalCall() {
    this.levelModalFormService.sentModalData$
      .subscribe((levelData?: LevelModal) => {
        if (levelData == null) {
          this.modeEdit = false;
          this.modalTitle = this.translate.instant('title.lbInsert', {
            labelName: this.translate.instant('label.level')
          });
          this.form.reset({isActive: true});
        } else {
          this.modeEdit = true;
          this.modalTitle = this.translate.instant('title.lbEdit', {
            labelName: this.translate.instant('label.level')
          });
          this.form.reset(levelData);
        }
      });
  }

  public newLevel() {
    this.levelResource.addNewLevel(this.form.value).subscribe( () => {
      this.confirm.emit(this.modeEdit);
    });
  }

  public editLevel() {
    this.levelResource.editLevel(this.form.value).subscribe( () => {
      this.confirm.emit(this.modeEdit);
    });
  }
}
