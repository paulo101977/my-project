import {LevelRoutingModule} from 'app/revenue-management/level/level-routing.module';


describe('LevelRoutingModule', () => {
  let premisesModule: LevelRoutingModule;

  beforeEach(() => {
    premisesModule = new LevelRoutingModule();
  });

  it('should create an instance', () => {
    expect(premisesModule).toBeTruthy();
  });
});
