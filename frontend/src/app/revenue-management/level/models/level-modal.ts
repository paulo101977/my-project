import { LevelModel } from './level.model';

// TODO move to a shareable place
type PartialPicking<T, R extends keyof T, O extends keyof T> = Pick<T, R> & Partial<Pick<T, O>>;

export type LevelModal = PartialPicking<
  LevelModel,
  'levelCode' | 'order' | 'description' | 'isActive',
  'id'
>;
