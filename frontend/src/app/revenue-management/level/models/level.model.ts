export class LevelModel {
  id: string;
  order: number;
  isActive: boolean;
  levelCode: number;
  description: string;
}
