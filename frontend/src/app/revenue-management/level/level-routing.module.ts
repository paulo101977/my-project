import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LevelListComponent} from '../level/components/level-list/level-list.component';


export const propertyBaseRoutes: Routes = [
  { path: '', component: LevelListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(propertyBaseRoutes)],
  exports: [RouterModule],
})
export class LevelRoutingModule {}
