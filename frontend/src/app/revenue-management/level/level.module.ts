import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'app/shared/shared.module';
import { LevelListComponent } from './components/level-list/level-list.component';
import { LevelModalFormComponent } from './components/level-modal-form/level-modal-form.component';
import { LevelRoutingModule } from 'app/revenue-management/level/level-routing.module';
import { LevelModalDeleteComponent } from './components/level-modal-delete/level-modal-delete.component';
import { InfoTooltipModule } from '@inovacao-cmnet/thx-ui';

@NgModule({
  imports: [
    CommonModule,
    LevelRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    InfoTooltipModule
  ],
  declarations: [
    LevelListComponent,
    LevelModalFormComponent,
    LevelModalDeleteComponent
  ]
})
export class LevelModule { }
