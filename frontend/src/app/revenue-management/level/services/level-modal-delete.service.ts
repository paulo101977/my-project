import { Injectable } from '@angular/core';
import { LevelModal } from 'app/revenue-management/level/models/level-modal';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LevelModalDeleteService {
  private modalId = 'LEVEL_DELETE';
  private modalData = new Subject<LevelModal>();
  public sentModalData$ = this.modalData.asObservable();

  constructor(
    private modalToggle: ModalEmitToggleService,
  ) { }

  public getModalId() {
    return this.modalId;
  }

  public openModal(levelData: LevelModal) {
    this.modalToggle.emitOpenWithRef(this.modalId);
    this.modalData.next(levelData);
  }

  public closeModal() {
    this.modalToggle.emitCloseWithRef(this.modalId);
  }
}
