import { TestBed, inject } from '@angular/core/testing';

import { LevelModalDeleteService } from './level-modal-delete.service';

describe('LevelModalDeleteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LevelModalDeleteService]
    });
  });

  it('should be created', inject([LevelModalDeleteService], (service: LevelModalDeleteService) => {
    expect(service).toBeTruthy();
  }));
});
