import { TestBed, inject } from '@angular/core/testing';

import { LevelModalFormService } from './level-modal-form.service';

describe('LevelModalFormService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LevelModalFormService]
    });
  });

  it('should be created', inject([LevelModalFormService], (service: LevelModalFormService) => {
    expect(service).toBeTruthy();
  }));
});
