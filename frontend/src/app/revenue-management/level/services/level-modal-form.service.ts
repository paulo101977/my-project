import { Injectable } from '@angular/core';
import { LevelModal } from '../models/level-modal';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LevelModalFormService {
  private modalId = 'LEVEL_FORM';
  private modalData = new Subject();
  public sentModalData$ = this.modalData.asObservable();

  constructor(
    private modalToggle: ModalEmitToggleService,
  ) { }

  public getModalId() {
    return this.modalId;
  }

  public openModal(levelData?: LevelModal) {
    this.modalToggle.emitOpenWithRef(this.modalId);
    this.modalData.next(levelData);
  }

  public closeModal() {
    this.modalToggle.emitCloseWithRef(this.modalId);
  }
}
