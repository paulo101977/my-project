import {LevelModel} from 'app/revenue-management/level/models/level.model';

export const LEVEL_LIST = 'LEVEL_LIST';

export class Data {

  public static readonly LEVEL_LIST = <LevelModel[]>[
    {
      id: '123',
      order: 1,
      isActive: true,
      levelCode: 1,
      description: 'Best Available Rate 1'
    },
    {
      id: '456',
      order: 2,
      isActive: false,
      levelCode: 2,
      description: 'Best Available Rate 2'
    },
    {
      id: '789',
      order: 3,
      isActive: true,
      levelCode: 3,
      description: 'Best Available Rate 3'
    },
    {
      id: '9123',
      order: 4,
      isActive: true,
      levelCode: 4,
      description: 'Best Available Rate 4'
    },
    {
      id: '9456',
      order: 5,
      isActive: false,
      levelCode: 5,
      description: 'Best Available Rate 5'
    },
    {
      id: '9789',
      order: 6,
      isActive: true,
      levelCode: 6,
      description: 'Best Available Rate 6'
    },
    {
      id: '92123',
      order: 7,
      isActive: true,
      levelCode: 7,
      description: 'Best Available Rate 7'
    },
    {
      id: '92456',
      order: 8,
      isActive: true,
      levelCode: 8,
      description: 'Best Available Rate 8'
    },
    {
      id: '92456',
      order: 9,
      isActive: true,
      levelCode: 9,
      description: 'Best Available Rate 9'
    },
    {
      id: '92456',
      order: 10,
      isActive: false,
      levelCode: 10,
      description: 'Best Available Rate 10'
    },
    {
      id: 92456,
      order: 11,
      isActive: false,
      levelCode: 11,
      description: 'Best Available Rate 11'
    },
    {
      id: 92456,
      order: 12,
      isActive: true,
      levelCode: 12,
      description: 'Best Available Rate 12'
    },
  ];
}
