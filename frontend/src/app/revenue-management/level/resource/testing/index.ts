import { createServiceStub } from '../../../../../../testing';
import { LevelResource } from 'app/revenue-management/level/resource/level.resource';
import { Observable, of } from 'rxjs';

export const levelResourceStub = createServiceStub(LevelResource, {
    addNewLevel(item): Observable<any> {
        return of( null );
    },

    deleteLevel(id: string): Observable<any> {
        return of( null );
    },

    editLevel(item): Observable<any> {
        return of( null );
    },

    getAllLevel(all?: boolean): Observable<any> {
        return of( null );
    },

    setToggleLevel(id: string): Observable<any> {
        return of( null );
    }
});
