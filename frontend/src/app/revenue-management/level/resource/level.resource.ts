import {ThexApiService} from '@inovacaocmnet/thx-bifrost';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LevelResource {
  private level = 'level/';
  constructor(
    private thexApiSvc: ThexApiService
  ) { }

  public getAllLevel(all?: boolean): Observable<any> {
    return this.thexApiSvc.get(`${this.level}?All=${all}`);
  }

  public addNewLevel(item):
    Observable<any> {
    return this
      .thexApiSvc
      .post(this.level, item);
  }

  public editLevel(item): Observable<any> {
    const param = item;
    return this
      .thexApiSvc
      .put(`${this.level}`, param);
  }

  public deleteLevel(id: string):
    Observable<any> {
    return this
      .thexApiSvc
      .delete<any>(`${this.level}${id}`);
  }

  public setToggleLevel(id: string): Observable<any> {
    return this.thexApiSvc.patch(`${this.level}${id}/toggleactivation`);
  }
}
