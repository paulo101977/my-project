import { Injectable } from '@angular/core';
import { DateService } from '../../../shared/services/shared/date.service';
import { RateHistoryHeaderDto } from '../models/rate-history-header-dto';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class RateHistoryService {

  constructor( private translateService: TranslateService, private dateService: DateService ) { }

  public getWeekDaysActiveAsArray( history: RateHistoryHeaderDto): Array<string> {
    const weekDaysArray = [];
    if ( history ) {
      if (history.hasOwnProperty('monday') && history.monday ) {
        weekDaysArray.push(this.translateService.instant('label.mondayInitials'));
      }
      if (history.hasOwnProperty('tuesday') && history.tuesday ) {
        weekDaysArray.push(this.translateService.instant('label.tuesdayInitials'));
      }
      if (history.hasOwnProperty('wednesday') && history.wednesday  ) {
        weekDaysArray.push(this.translateService.instant('label.wednesdayInitials'));
      }
      if (history.hasOwnProperty('thursday') && history.thursday   ) {
        weekDaysArray.push(this.translateService.instant('label.thursdayInitials'));
      }
      if (history.hasOwnProperty('friday') && history.friday) {
        weekDaysArray.push(this.translateService.instant('label.fridayInitials'));
      }
      if (history.hasOwnProperty('saturday') && history.saturday ) {
        weekDaysArray.push(this.translateService.instant('label.saturdayInitials'));
      }
      if (history.hasOwnProperty('sunday') && history.sunday) {
        weekDaysArray.push(this.translateService.instant('label.sundayInitials'));
      }
    }

    return weekDaysArray;
  }

  public filterByVigence(searchParamsDate: string, itens: Array<RateHistoryHeaderDto>): Array<RateHistoryHeaderDto> {
    if (itens) {
      return itens.filter( item => {
        const initDate = moment(item.initialDate, DateService.DATE_FORMAT_UNIVERSAL);
        const finalDate = moment(item.finalDate, DateService.DATE_FORMAT_UNIVERSAL);
        const searchDate = moment(searchParamsDate, DateService.DATE_FORMAT_UNIVERSAL);
        return searchDate.isSame(initDate) || searchDate.isSame(finalDate) || searchDate.isBetween(initDate, finalDate);
      });
    }
    return [];
  }

  // Mapper
  public headerHistoryMapper(items: Array<RateHistoryHeaderDto>): Array<any> {
    if (items && items.length >= 1) {
      return items.map(item => {
        let object = {
          vigence: this.dateService.formatVigenceString(item.initialDate, item.finalDate, this.translateService.instant('label.to')),
          appliedTo: this.getWeekDaysActiveAsArray(item)
        };
        object = Object.assign(object, item);
        return object;
      });
    }
    return [];
  }

}
