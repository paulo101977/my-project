import { TestBed } from '@angular/core/testing';

import { RateHistoryService } from './rate-history.service';
import { RateHistoryHeaderDto } from '@app/revenue-management/rate-history/models/rate-history-header-dto';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

describe('RateHistoryService', () => {
  let mockObject;
  let service;

  beforeAll( () => {
    TestBed.resetTestEnvironment();
    TestBed
    .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
    .configureTestingModule({
      imports: [
        TranslateTestingModule,
      ],
      providers: [
        dateServiceStub,
      ]
    });

    mockObject = <RateHistoryHeaderDto> {
      id: '1',
      initialDate: '2018-01-01T00:00:00',
      finalDate: '2018-12-01T00:00:00',
      currencyId: null,
      mealPlanTypeDefaultId: null,
      propertyId: 1,
      ratePlanId: null,
      monday: true,
      saturday: true,
      sunday: true,
      thursday: true,
      tuesday: true,
      wednesday: true,
      friday: true,
      tenantId: null,
    };

    service = TestBed.get(RateHistoryService);
  });

  it('should be created', () =>  {
    expect(service).toBeTruthy();
  });

  it('should generate weekDays array', () => {
    const result = service.getWeekDaysActiveAsArray(mockObject);

    expect(result.length).toEqual(7);
  });

  it('should filter byDate', () => {
    const list = [
      <RateHistoryHeaderDto>{
        initialDate: '2018-01-01T00:00:00',
        finalDate: '2018-02-01T00:00:00',
      },
      <RateHistoryHeaderDto>{
        initialDate: '2018-01-15T00:00:00',
        finalDate: '2018-03-01T00:00:00',
      }
    ];

    const result = service.filterByVigence('2018-01-02T00:00:00', list);

    expect(result.length).toEqual(1);
  });

  it('should map historyList ', () => {
    const list: Array<RateHistoryHeaderDto> = [ mockObject ];

    const result = service.headerHistoryMapper(list);

    const expectedObject = Object.assign(
      {
        vigence: service['dateService']
          .formatVigenceString(list[0].initialDate, list[0].finalDate, service['translateService'].instant('label.to')),
        appliedTo: service.getWeekDaysActiveAsArray(list[0])
      }, list[0]);
    expect(result[0]).toEqual(expectedObject);
  });
});
