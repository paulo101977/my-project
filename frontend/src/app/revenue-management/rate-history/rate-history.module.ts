import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RateHistoryComponent } from '@app/revenue-management/rate-history/components/rate-history/rate-history.component';
import { SharedModule } from '@app/shared/shared.module';
import { RatePlanRoutingModule } from '@app/revenue-management/rate-plan/rate-plan-routing.module';
import { RouterModule } from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { RateHistoryRoutingModule } from '@app/revenue-management/rate-history/rate-history-routing.module';
import { ThxTabsModule } from '@inovacao-cmnet/thx-ui';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    RatePlanRoutingModule,
    MyDatePickerModule,
    RateHistoryRoutingModule,
    ThxTabsModule
  ],
  declarations: [ RateHistoryComponent ],
  exports: [ RateHistoryComponent ]
})
export class RateHistoryModule { }
