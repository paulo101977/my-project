import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateHistoryComponent } from './rate-history.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '../../../../../../node_modules/@angular/common/http/testing';
import { MomentModule } from 'ngx-moment';
import { CoreModule } from '@app/core/core.module';
import { CurrencyFormatPipe } from '@app/shared/pipes/currency-format.pipe';
import { ThxTabsModule } from '@inovacao-cmnet/thx-ui';

describe('RatePlanHistoryComponent', () => {
  let component: RateHistoryComponent;
  let fixture: ComponentFixture<RateHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateHistoryComponent, CurrencyFormatPipe ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MomentModule,
        TranslateModule.forRoot(),
        CoreModule,
        ThxTabsModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
