import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BaseRateResource } from '../../../../shared/resources/base-rate/base-rate.resource';
import { ActivatedRoute } from '@angular/router';
import { RatePlanResource } from '../../../../shared/resources/rate-plan/rate-plan.resource';
import { RatePlanHistoryDto } from '../../../../shared/models/dto/rate-plan/rate-plan-history-dto';
import { PropertyBaseRateHistory } from '../../../../shared/models/dto/rate-plan/property-base-rate-history';
import { RateHistoryService } from '@app/revenue-management/rate-history/services/rate-history.service';
import { RateHistoryResource } from '@app/revenue-management/rate-history/resources/rate-history.resource';

@Component({
  selector: 'app-rate-history',
  templateUrl: './rate-history.component.html',
  styleUrls: ['./rate-history.component.css']
})
export class RateHistoryComponent implements OnInit {

  @ViewChild('ageRange1Template') ageRange1Template: TemplateRef<any>;
  @ViewChild('ageRange2Template') ageRange2Template: TemplateRef<any>;
  @ViewChild('ageRange3Template') ageRange3Template: TemplateRef<any>;
  @ViewChild('currencyTemplate') currencyTemplate: TemplateRef<any>;

  public propertyParameterList: Array<any>;
  public ageRange1;
  public ageRange2;

  public rateColumns: Array<PropertyBaseRateHistory>;
  public mealPlanColumns: Array<PropertyBaseRateHistory>;

  private propertyId: number;
  private ratePlanId: string;
  private historyId: string;

  public historyList: Array<RatePlanHistoryDto>;
  public appliedTo: Array<string>;

  constructor(private translateService: TranslateService,
              private route: ActivatedRoute,
              private rateHistoryService: RateHistoryService,
              private baseRateResource: BaseRateResource,
              private rateHistoryResource: RateHistoryResource) {
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.historyId = this.route.snapshot.params.historyId;
    if ( this.route.snapshot.params.ratePlanId ) {
      this.ratePlanId = this.route.snapshot.params.ratePlanId;
      this.setRatePlanHistory();
    } else {
      this.setBaseRateHistory();
    }
    this.propertyParameterList = [];
    this.setColumnsName();

    this.setRows();
    this.setParameters();
  }

  private setRatePlanHistory() {
    this.rateHistoryResource.getRatePlanHistory(this.ratePlanId)
      .subscribe(data => this.setHistoryList(data.items));
  }

  private setBaseRateHistory() {
    this.rateHistoryResource.getRateHistoryById(this.historyId)
      .subscribe(data => this.setHistoryList(data.items));
  }

  private setHistoryList(historyList: Array<RatePlanHistoryDto>) {
    for (const history of historyList) {
      history.currencyName = history.propertyBaseRateHistoryList[0].currencyName;
      history.creatorUserName = history.propertyBaseRateHistoryList[0].creatorUserName;
      history.appliedTo = this.rateHistoryService.getWeekDaysActiveAsArray(history);

      history.rateRows = [];
      history.mealPlanRows = [];
      for (const baseRateHistory of history.propertyBaseRateHistoryList) {
        const rateExists = history.rateRows.find(rateRow => rateRow.roomTypeId === baseRateHistory.roomTypeId);

        if (!rateExists) {
          history.rateRows.push(baseRateHistory);
        }

        const mealPlanExists = history.mealPlanRows.find(
          mealPlanRow => mealPlanRow.mealPlanTypeId === baseRateHistory.mealPlanTypeId
        );

        if (!mealPlanExists) {
          history.mealPlanRows.push(baseRateHistory);
        }
      }
    }

    this.historyList = historyList;
  }

  private setParameters() {
    this.baseRateResource.getConfigurationDataParameters(this.propertyId).subscribe(
      configurationDataParameters => {
        this.propertyParameterList = <Array<any>> configurationDataParameters.propertyParameterList;
        this.ageRange1 = parseInt(this.propertyParameterList[0].propertyParameterValue, 10) + 1;
        this.ageRange2 = parseInt(this.propertyParameterList[1].propertyParameterValue, 10) + 1;
      }
    );
  }

  private setColumnsName() {
    const rateColumns = [
      { name: 'label.typeUHComplete', prop: 'roomTypeName' },
      { name: 'label.paxOne', prop: 'pax1Amount' },
      { name: 'label.paxTwo', prop: 'pax2Amount' },
      { name: 'label.paxThree', prop: 'pax3Amount' },
      { name: 'label.paxFour', prop: 'pax4Amount' },
      { name: 'label.paxFive', prop: 'pax5Amount' },
      { name: 'label.paxAdditional', prop: 'paxAdditionalAmount' },
      { name: 'variable.childs', prop: 'child1Amount', headerTemplate: this.ageRange1Template },
      { name: 'variable.childs', prop: 'child2Amount', headerTemplate: this.ageRange2Template },
      { name: 'variable.childs', prop: 'child3Amount', headerTemplate: this.ageRange3Template }
    ];

    const mealPlanColumns = [
      { name: 'label.mealPlanType', prop: 'mealPlanTypeName' },
      { name: 'label.adult', prop: 'adultMealPlanAmount' },
      { name: 'variable.childs', prop: 'child1MealPlanAmount', headerTemplate: this.ageRange1Template },
      { name: 'variable.childs', prop: 'child2MealPlanAmount', headerTemplate: this.ageRange2Template },
      { name: 'variable.childs', prop: 'child3MealPlanAmount', headerTemplate: this.ageRange3Template }
    ];

    this.rateColumns = this.mapCommonColumnData(rateColumns);
    this.mealPlanColumns = this.mapCommonColumnData(mealPlanColumns);
  }

  private mapCommonColumnData(columnList: Array<any>) {
    return columnList.map((column, index) => {
      column.name = this.translateService.instant(column.name);
      column.resizeable = false;
      column.draggable = false;

      if (index > 0) {
        column.headerClass = 'thf-u-text-center';
        column.cellClass = 'thf-u-text-center';
        column.cellTemplate = this.currencyTemplate;
      }

      return column;
    });

  }

  private setRows() {

  }

}
