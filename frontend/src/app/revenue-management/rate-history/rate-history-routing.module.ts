import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { RateHistoryComponent } from '../rate-history/components/rate-history/rate-history.component';

export const rateHistoryRoutes: Routes = [
  { path: 'base-rate/history/:historyId', component: RateHistoryComponent },
  { path: ':ratePlanId/history/:historyId', component: RateHistoryComponent },
];

@NgModule({
  imports: [RouterModule.forChild(rateHistoryRoutes)],
  exports: [RouterModule],
})
export class RateHistoryRoutingModule {}
