import { Injectable } from '@angular/core';
import { HttpService } from '@app/core/services/http/http.service';
import { Observable } from 'rxjs';
import { RatePlanHistoryDto } from '@app/shared/models/dto/rate-plan/rate-plan-history-dto';
import { map } from 'rxjs/operators';
import { ItemsResponse } from '@app/shared/models/backend-api/item-response';

@Injectable({ providedIn: 'root' })
export class RateHistoryResource {

  constructor( private httpClient: HttpService ) { }

  public gelAllByPropertyId(propertyId: number): Observable<any> {
    return this.httpClient.get(`propertyBaseRate/${propertyId}/PropertyBaseRateHeaderHistory`);
  }

  public getRatePlanHistory(ratePlanId: string) {
    return this.httpClient.get<ItemsResponse<RatePlanHistoryDto>>(
      `ratePlan/${ratePlanId}/rateplanhistory`
    ).pipe(
      map(data => (<any>data).result) // TODO: Check why is returning a "result" attribute
    );
  }

  public getRateHistoryById(rateHistoryId): Observable<any> {
    return this.httpClient.get(`ratePlan/${rateHistoryId}/ratePlanHistoryId`).pipe(
      map(data => (<any>data).result) // TODO: Check why is returning a "result" attribute
    );
  }
}
