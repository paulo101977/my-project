export class RateHistoryHeaderDto {
  id: string;
  initialDate: string;
  finalDate: string;
  currencyId: string;
  mealPlanTypeDefaultId: string;
  propertyId: number;
  ratePlanId: string;
  monday: boolean;
  saturday: boolean;
  sunday: boolean;
  thursday: boolean;
  tuesday: boolean;
  wednesday: boolean;
  friday: boolean;
  tenantId: string;
}
