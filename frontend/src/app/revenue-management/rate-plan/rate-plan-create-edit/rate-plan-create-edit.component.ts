import { Component, DoCheck, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { DateService } from '../../../shared/services/shared/date.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { RatePlanResource } from '../../../shared/resources/rate-plan/rate-plan.resource';
import { RatePlanCreateGetDto } from '../../../shared/models/dto/rate-plan/rate-plan-create-get-dto';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { RatePlanUhTypeDto } from '../../../shared/models/dto/rate-plan/rate-plan-uh-type-dto';
import { CompanyClientDto } from '../../../shared/models/dto/rate-plan/company-client-dto';
import { CompanyClientCategoryDto } from '../../../shared/models/dto/rate-plan/company-client-category-dto';
import { RatePlanBillingItemDto } from '../../../shared/models/dto/rate-plan/rate-plan-billing-item-dto';
import { RatePlanDto } from '../../../shared/models/dto/rate-plan/rate-plan-dto';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import { AgreementTypeEnum } from '../../../shared/models/dto/rate-plan/agreement-type-dto';
import { IMyDate } from 'mydatepicker';
import { RateTypeEnum } from '../../../shared/models/rate-plan/rate-type-enum';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { MarketSegmentService } from 'app/shared/services/market-segment/market-segment.service';
import {RoundEnum} from 'app/revenue-management/rate-plan/models/round-enum';
import {RoundEnumService} from 'app/revenue-management/rate-plan/service/round-enum.service';

@Component({
  selector: 'app-rate-plan-create-edit',
  templateUrl: './rate-plan-create-edit.component.html',
  styleUrls: ['./rate-plan-create-edit.component.css'],
})
export class RatePlanCreateEditComponent implements OnInit, DoCheck {
  public readonly MODAL_CONFIRM_FIXED_RATE = 'confirm-message-fixed-rate';

  // info
  public uhTypeList: Array<RatePlanUhTypeDto> = [];
  public companyClientList: Array<CompanyClientDto> = [];
  public companyClientCategoryList: Array<CompanyClientCategoryDto> = [];
  public billingItemTypeList: Array<RatePlanBillingItemDto> = [];

  public associatedUhTypes: Array<any> = [];
  public associatedCustomers: Array<any> = [];
  public associatedComissions: Array<any> = [];

  // config
  public actionName: string;
  public propertyId: number;
  public ratePlanId: string;
  public isEditMode: boolean;
  public isConfirm = false;
  public cancelButtonConfig: ButtonConfig;
  public saveButtonConfig: ButtonConfig;
  public isB2B = false;
  public isBaseRate = false;
  public disableEndPickerUntil: IMyDate;
  public isFixedRate: boolean;
  public currentRatePlan: RatePlanDto;
  public ratePlanRequerid = false;
  public infoFormRound: string;

  // form
  public ratePlanForm: FormGroup;
  public ratePlanTypeList: Array<any> = [];
  public ratePlanCurrencyList: Array<any> = [];
  public ratePlanMealPlanList: Array<any> = [];
  public defaultSegmentList: Array<any> = [];
  public rateTypeList: Array<any> = [];
  public historyList: Array<any>; // todo criar DTO
  public roundTypeList: Array<any> = [];

  constructor(
    private router: ActivatedRoute,
    private formBuilder: FormBuilder,
    private dateService: DateService,
    private ratePlanResource: RatePlanResource,
    private commomService: CommomService,
    private sharedService: SharedService,
    private toastService: ToasterEmitService,
    private _route: Router,
    private translateService: TranslateService,
    private loaderService: LoadPageEmitService,
    private location: Location,
    private modalEmitToggleService: ModalEmitToggleService,
    private marketSegmentService: MarketSegmentService,
    private roundEnumService: RoundEnumService
  ) {}

  // Lifecycle
  ngOnInit() {
    this.propertyId = this.router.snapshot.params.property;
    this.ratePlanId = this.router.snapshot.params.ratePlanId;
    if (this.ratePlanId) {
      this.actionName = 'action.edit';
      this.isEditMode = true;
    } else {
      this.actionName = 'action.new';
      this.isEditMode = false;
    }

    this.infoFormRound = this.translateService.instant('text.infoFormRound');
    this.loadData();
    this.setEndDateSince();
    this.setForm();
    this.setButtonsConfig();
  }

  ngDoCheck(): void {
    this.isB2B = this.ratePlanForm.get('ratePlanType').value == (AgreementTypeEnum.B2B as number);
    if (this.saveButtonConfig && this.ratePlanForm) {
      if (!this.isEditMode) {
        if (!this.ratePlanForm.get('ratePlanShouldPublish').value) {
          this.ratePlanForm.get('ratePlanCode').setValue(null);
        }
      }
      let isDisabled;
      if (this.isB2B) {
        if (this.ratePlanForm.get('rateType').value != RateTypeEnum.FixedRate) {
          isDisabled =
            this.ratePlanForm.invalid ||
            !this.associatedUhTypes ||
            this.associatedUhTypes.length <= 0 ||
            !this.associatedCustomers ||
            this.associatedCustomers.length <= 0;
        } else {
          isDisabled =
            this.ratePlanForm.invalid ||
            !this.associatedCustomers ||
            this.associatedCustomers.length <= 0 ||
            !this.historyList ||
            this.historyList.length <= 0;
        }
      } else {
        if (this.ratePlanForm.get('rateType').value != RateTypeEnum.FixedRate) {
          isDisabled = this.ratePlanForm.invalid || !this.associatedUhTypes || this.associatedUhTypes.length <= 0;
        } else {
          isDisabled = true;
        }
      }

      if (this.ratePlanForm.get('ratePlanShouldPublish').value === true &&
        (!this.ratePlanForm.get('ratePlanCode').value)) {
        this.ratePlanRequerid = true;
        isDisabled = true;
        this.ratePlanForm.get('ratePlanCode').enable();
      } else if (this.ratePlanForm.get('ratePlanShouldPublish').value === false) {
        this.ratePlanRequerid = false;
        this.ratePlanForm.get('ratePlanCode').disable();
      } else if (this.ratePlanForm.get('ratePlanShouldPublish').value === true && this.isEditMode
        && !this.currentRatePlan.distributionCode) {
        this.ratePlanForm.get('ratePlanCode').enable();
      }

      this.saveButtonConfig.isDisabled = isDisabled;
    }
  }

  // Actions
  public cancel() {
    this.location.back();
  }

  public save = () => {
    if (!this.currentRatePlan) {
      this.currentRatePlan = new RatePlanDto();
      this.currentRatePlan.propertyId = this.propertyId;
    }

    this.currentRatePlan.distributionCode = this.ratePlanForm.get('ratePlanCode').value;
    this.currentRatePlan.agreementTypeId = this.ratePlanForm.get('ratePlanType').value;
    this.currentRatePlan.agreementName = this.ratePlanForm.get('ratePlanName').value;
    this.currentRatePlan.startDate = this.ratePlanForm.get('ratePlanBeginDate').value;
    this.currentRatePlan.endDate = this.ratePlanForm.get('ratePlanEndDate').value;
    this.currentRatePlan.currencyId = this.ratePlanForm.get('ratePlanCurrency').value;
    this.currentRatePlan.mealPlanTypeId = this.ratePlanForm.get('ratePlanMealPlanTypeDefault').value;
    this.currentRatePlan.isActive = this.ratePlanForm.get('ratePlanIsActive').value;
    this.currentRatePlan.publishOnline = this.ratePlanForm.get('ratePlanShouldPublish').value;
    this.currentRatePlan.rateNet = this.ratePlanForm.get('ratePlanNetTax').value;
    this.currentRatePlan.rateTypeId = this.ratePlanForm.get('rateType').value;
    this.currentRatePlan.marketSegmentId = this.ratePlanForm.get('defaultSegment').value;
    this.currentRatePlan.description = this.ratePlanForm.get('description').value;
    this.currentRatePlan.roundingTypeId = this.ratePlanForm.get('roundType').value ?
      this.ratePlanForm.get('roundType').value : RoundEnum.Default;

    this.currentRatePlan.ratePlanRoomTypeList = this.associatedUhTypes.map(item => {
      let itemAux = {
        roomTypeId: item.roomTypeId,
        percentualAmmount: item.percentualAmmount,
        isDecreased: item.isDecreased,
        publishOnline: item.publishOnline,
      };
      if (this.isEditMode) {
        itemAux = Object.assign({ ratePlanId: this.currentRatePlan.id }, itemAux);
        if (item.id && isNaN(item.id)) {
          itemAux = Object.assign({ id: item.id }, itemAux);
        }
      }
      return itemAux;
    });
    if (this.isB2B) {
      this.currentRatePlan.ratePlanCompanyClientList = this.getRatePlanCompanyClientList();
      this.currentRatePlan.ratePlanCommissionList = this.getRatePlanCommissionList();
    } else {
      this.currentRatePlan.ratePlanCompanyClientList = [];
      this.currentRatePlan.ratePlanCommissionList = [];
    }

    this.ratePlanResource.save(this.currentRatePlan).subscribe(result => {

      let message;
      let routeParam;
      if (this.isEditMode) {
        message = this.translateService.instant('label.ratePlanSuccessfullyEdited');
        routeParam = ['../../'];
      } else {
        this.ratePlanId = result.id;
        message = this.translateService.instant('label.ratePlanSuccessfullyCreated');
        routeParam = ['../'];
      }
      this.toastService.emitChangeTwoMessages(SuccessError.success, message, '');

      if (this.isFixedRate && this.isConfirm) {
        this.redirectToFixedRate();
      } else {
        this._route.navigate(routeParam, { relativeTo: this.router });
      }
    });
  }

  private getRatePlanCommissionList() {
    return this.associatedComissions.map(({billingItemId, commissionPercentualAmount, id}) => {
      return {
        billingItemId,
        commissionPercentualAmount,
        ...(this.isEditMode ? {ratePlanId: this.currentRatePlan.id} : {}),
        ...(id ? {id} : {})
      };
    });
  }

  private getRatePlanCompanyClientList() {
    return this.associatedCustomers.map(({companyClientId, id}) => {
      return {
        ...(companyClientId != null ? {companyClientId} : {companyClientId: id}),
        ...(this.isEditMode ? {ratePlanId: this.currentRatePlan.id} : {}),
        ...(this.isEditMode && id && companyClientId ? {id} : {id: GuidDefaultData})
      };
    });
  }

// Config
  public loadData() {
    this.loaderService.emitChange(ShowHide.show);
    if (this.isEditMode) {
      this.ratePlanResource.getCompleteRatePlanEditInfo(this.propertyId, this.ratePlanId).subscribe(
        response => {
          this.populate(response);
        },
        error2 => {},
      );
    } else {
      this.ratePlanResource.getAllCreateInfo(this.propertyId).subscribe(
        response => {
          this.populate(response);
        },
        error => {
          this.loaderService.emitChange(ShowHide.hide);
        },
      );
    }
  }

  public populate(info) {
    this.loaderService.emitChange(ShowHide.hide);
    if (info && info.length > 0) {
      this.ratePlanTypeList = this.commomService.toOption(
        (<RatePlanCreateGetDto>info[0]).agreementTypeList,
        'id',
        'agreementTypeName',
      );
      this.ratePlanCurrencyList = this.commomService.toOption(
        (<RatePlanCreateGetDto>info[0]).currencyList,
        'id',
        'currencyName',
      );
      this.ratePlanMealPlanList = this.commomService.toOption(
        (<RatePlanCreateGetDto>info[0]).propertyMealPlanTypeList,
        'mealPlanTypeId',
        'propertyMealPlanTypeName',
      );
      this.defaultSegmentList = this.marketSegmentService
        .prepareSecondarySelectList((<RatePlanCreateGetDto>info[0]).marketSegmentList);

      this.rateTypeList = this.commomService.toOption(
        (<RatePlanCreateGetDto>info[0]).propertyRateTypeList,
        'id',
        'rateTypeName',
      );
      this.uhTypeList = info[1];
      this.companyClientList = info[2];
      this.billingItemTypeList = info[3];
      this.companyClientCategoryList = info[4];

      if (info.length == 6) {
        // is edit
        this.setformInformation(info[5]);
        this.historyList = info[5].propertyBaseRateHeaderHistoryList;
      }
    }

    this.roundTypeList = this.roundEnumService.getRoundEnumList();
  }

  public setformInformation(ratePlan: RatePlanDto) {
    this.currentRatePlan = ratePlan;
    this.associatedUhTypes = ratePlan.ratePlanRoomTypeList;
    this.associatedCustomers = ratePlan.ratePlanCompanyClientList;
    this.associatedComissions = ratePlan.ratePlanCommissionList;
    this.setDataInForm(this.currentRatePlan);
  }

  public setForm() {
    this.ratePlanForm = this.formBuilder.group({
      ratePlanType: [null, Validators.required],
      ratePlanName: [null, Validators.required],
      defaultSegment: [null],
      ratePlanBeginDate: [null, Validators.required],
      ratePlanEndDate: [null, Validators.required],
      rateType: [null, Validators.required],
      ratePlanCurrency: [null],
      ratePlanMealPlanTypeDefault: [null, Validators.required],
      ratePlanIsActive: true,
      ratePlanShouldPublish: false,
      ratePlanNetTax: false,
      ratePlanCode: [null],
      description: [null],
      roundType: ['', Validators.required],
    });

    this.ratePlanForm.get('rateType').valueChanges.subscribe(value => {
      this.isFixedRate = value == RateTypeEnum.FixedRate;
      this.isBaseRate = !!(this.ratePlanForm.get('rateType').value == RateTypeEnum.BaseRate);
      this.isBaseRate
        ? this.ratePlanForm.get('roundType').enable()
        : this.ratePlanForm.get('roundType').disable();
    });

    this.ratePlanForm.get('ratePlanType').valueChanges.subscribe(value => {
      if (value == AgreementTypeEnum.B2C) {
        this.ratePlanForm.get('rateType').setValue(RateTypeEnum.BaseRate);
        this.ratePlanForm.get('rateType').disable();
      } else {
        this.ratePlanForm.get('rateType').enable();
      }
    });
  }

  public setDataInForm(ratePlan: RatePlanDto) {
    this.ratePlanForm.get('ratePlanType').setValue(ratePlan.agreementTypeId);
    this.ratePlanForm.get('ratePlanName').setValue(ratePlan.agreementName);

    if (ratePlan.startDate) {
      this.ratePlanForm.get('ratePlanBeginDate').setValue(ratePlan.startDate);
    }
    if (ratePlan.endDate) {
      this.ratePlanForm.get('ratePlanEndDate').setValue(ratePlan.endDate);
    }
    this.ratePlanForm.get('ratePlanCurrency').setValue(ratePlan.currencyId);
    this.ratePlanForm.get('ratePlanMealPlanTypeDefault').setValue(ratePlan.mealPlanTypeId);
    this.ratePlanForm.get('ratePlanIsActive').setValue(ratePlan.isActive);
    this.ratePlanForm.get('ratePlanShouldPublish').setValue(ratePlan.publishOnline);
    this.ratePlanForm.get('ratePlanNetTax').setValue(ratePlan.rateNet);
    this.ratePlanForm.get('rateType').setValue(ratePlan.rateTypeId);
    this.ratePlanForm.get('defaultSegment').setValue(ratePlan.marketSegmentId);

    this.ratePlanForm.get('ratePlanCode').setValue(ratePlan.distributionCode);
    this.ratePlanForm.get('description').setValue(ratePlan.description);
    this.ratePlanForm.get('roundType').setValue(ratePlan.roundingTypeId);
  }

  private setEndDateSince() {
    if (this.ratePlanForm) {
      const dateBegin = this.ratePlanForm.get('ratePlanBeginDate').value;
      if (dateBegin) {
        this.disableEndPickerUntil = this.dateService.convertUniversalDateToIMyDate(dateBegin);
      }
    }
  }

  public notifyBeginDateChange(date) {
    this.setEndDateSince();
  }

  private setButtonsConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancelRatePlan',
      this.cancel.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.saveButtonConfig = this.sharedService
      .getButtonConfig('saveRatePlan', this.save, 'action.confirm', ButtonType.Primary, null, true);
  }

  public goToCalendar() {

    const params = {
      date: this.ratePlanForm.get('ratePlanBeginDate').value,
      initialDate: this.ratePlanForm.get('ratePlanBeginDate').value,
      finalDate: this.ratePlanForm.get('ratePlanEndDate').value,
      currencyId: this.ratePlanForm.get('ratePlanCurrency').value,
      mealPlanTypeId: this.ratePlanForm.get('ratePlanMealPlanTypeDefault').value,
      ratePlanId: this.ratePlanId,
      adultCount: 1,
      childrenAge: 0
    };
    this._route.navigate(['p', this.propertyId, 'rm', 'calendar'], { queryParams: params});
  }

  public addFixedRate() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_CONFIRM_FIXED_RATE);
  }

  public cancelAddFixedRate() {
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_CONFIRM_FIXED_RATE);
  }

  public goToFixedRateAndSave() {
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_CONFIRM_FIXED_RATE);
    this.isConfirm = true;
    this.save();
  }

  public redirectToFixedRate() {
    const params = {
      ratePlanId: this.ratePlanId,
      mealPlanId: this.ratePlanForm.get('ratePlanMealPlanTypeDefault').value
    };
    this._route.navigate(['p', this.propertyId, 'rm', 'base-rate', 'new'], { queryParams: params});
  }

  public goToHistory(historyId: string) {
    this._route.navigateByUrl(`/p/${this.propertyId}/rm/rate-plan/${this.ratePlanId}/history/${historyId}`);
  }

}
