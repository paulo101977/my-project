import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RatePlanCreateEditComponent } from './rate-plan-create-edit.component';
import { of } from 'rxjs';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { createAccessorComponent, imgCdnStub, loadPageEmitServiceStub } from '../../../../../testing';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { ratePlanResourceStub } from 'app/shared/resources/rate-plan/testing';

const thxError = createAccessorComponent('thx-error');
const thxDatepicker = createAccessorComponent('thx-datepicker');

describe('RatePlanCreateEditComponent', () => {
  let component: RatePlanCreateEditComponent;
  let fixture: ComponentFixture<RatePlanCreateEditComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
      ],
      providers: [
        sharedServiceStub,
        commomServiceStub,
        toasterEmitServiceStub,
        dateServiceStub,
        loadPageEmitServiceStub,
        modalToggleServiceStub,
        ratePlanResourceStub,
      ],
      declarations: [
        RatePlanCreateEditComponent,
        thxError,
        imgCdnStub,
        thxDatepicker,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RatePlanCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(' should go to Calendar when goToCalendar fired', () => {
    spyOn<any>(component['_route'], 'navigate').and.callFake( () => {});

    component.ratePlanForm.patchValue( {
      ratePlanBeginDate: '2018-01-01T00:00:00',
      ratePlanEndDate: '2018-01-10T00:00:00',
      ratePlanCurrency: '1123',
      ratePlanMealPlanTypeDefault: '5677'
    });
    component.ratePlanId = '9999';
    component.propertyId = 1;
    const params = {
      date: component.ratePlanForm.get('ratePlanBeginDate').value,
      initialDate: component.ratePlanForm.get('ratePlanBeginDate').value,
      finalDate: component.ratePlanForm.get('ratePlanEndDate').value,
      currencyId: component.ratePlanForm.get('ratePlanCurrency').value,
      mealPlanTypeId: component.ratePlanForm.get('ratePlanMealPlanTypeDefault').value,
      ratePlanId: component.ratePlanId,
      adultCount: 1,
      childrenAge: 0
    };

    component.goToCalendar();

    expect(component['_route'].navigate)
      .toHaveBeenCalledWith( ['p', component.propertyId, 'rm', 'calendar'] , { queryParams: params} );
  });

  it('should open modal when addFixedRate fired', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');

    component.addFixedRate();

    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.MODAL_CONFIRM_FIXED_RATE);
  });

  it('should close modal when cancelAddFixedRate fired', () => {
    spyOn(component['modalEmitToggleService'], 'emitCloseWithRef');

    component.cancelAddFixedRate();

    expect(component['modalEmitToggleService'].emitCloseWithRef).toHaveBeenCalledWith(component.MODAL_CONFIRM_FIXED_RATE);
  });

  it('should close modal and save when goToFixedRateAndSave fired ', () => {
    spyOn(component['modalEmitToggleService'], 'emitCloseWithRef');
    spyOn<any>(component, 'save').and.returnValue(of({}));

    component.goToFixedRateAndSave();

    expect(component['modalEmitToggleService'].emitCloseWithRef)
      .toHaveBeenCalledWith(component.MODAL_CONFIRM_FIXED_RATE);
    expect(component.save).toHaveBeenCalled();
  });

  it('should redirect to baseRate with params when redictToBaseRate', () => {
    spyOn<any>(component['_route'], 'navigate').and.returnValue('');

    component.redirectToFixedRate();

    const params = {
      ratePlanId: component.ratePlanId,
      mealPlanId: component.ratePlanForm.get('ratePlanMealPlanTypeDefault').value
    };
    expect(component['_route'].navigate)
      .toHaveBeenCalledWith(['p', component.propertyId, 'rm', 'base-rate', 'new'], { queryParams: params});
  });
});
