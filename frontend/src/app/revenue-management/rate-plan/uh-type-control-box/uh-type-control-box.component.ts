import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { TranslateService } from '@ngx-translate/core';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { RatePlanUhTypeDto } from '../../../shared/models/dto/rate-plan/rate-plan-uh-type-dto';
import { UhTypeAssociateDto } from '../../../shared/models/dto/rate-plan/uh-type-associate-dto';

import { RateTypeDto } from '../../../shared/models/dto/rate-type-dto';
import { ComissionTypeEnum } from '../../../shared/models/rate-plan/comission-type-enum';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import * as _ from 'lodash';
import { CurrencyService } from '../../../shared/services/shared/currency-service.service';

@Component({
  selector: 'uh-type-control-box',
  templateUrl: './uh-type-control-box.component.html',
  styleUrls: ['./uh-type-control-box.component.css'],
})
export class UhTypeControlBoxComponent implements OnInit {
  public readonly MODAL_ASSOCIATE_UH_ID = 'id_rate_plan_modal_assotiate_uh_type';

  @ViewChild('variationCell') variationCellTemplate: TemplateRef<any>;

  @Input() uhTypeList: Array<RatePlanUhTypeDto>;
  @Input()
  set associatedUhTypes(list: Array<UhTypeAssociateDto>) {
    if (list && list.length > 0) {
      this.mapList(list);
    }

    this.pageItemsList = list;
    if (this.searchBarConfig) {
      this.searchBarConfig.items = this.pageItemsList;
    }
    this.removeUhsSelectedFromUhList(list);
    this.associatedUhTypesChange.emit(list);
  }

  get associatedUhTypes(): Array<UhTypeAssociateDto> {
    return this.pageItemsList;
  }
  @Output() associatedUhTypesChange = new EventEmitter();

  // Table
  public columns: Array<any>;
  public pageItemsList: Array<UhTypeAssociateDto>;
  public rateTypeList: Array<any>;

  // General
  public associateButtonConfig: ButtonConfig;
  public showTable = false;
  public searchBarConfig: SearchableItems;

  constructor(
    private sharedService: SharedService,
    private modalToggleService: ModalEmitToggleService,
    private translateService: TranslateService,
    private commomService: CommomService,
    private currencyService: CurrencyService,
  ) {}

  // Lifecycle
  ngOnInit() {
    this.setTableColumns();
    this.setSearchConfig();
    this.setButtonConfig();
    this.setRateType();
  }

  // Actions
  public prepareToAssociate() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_ASSOCIATE_UH_ID);
  }

  public closeModal() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_ASSOCIATE_UH_ID);
  }

  public associateUhType(listTypes: Array<UhTypeAssociateDto>) {
    this.associatedUhTypes = [..._.concat(this.pageItemsList, listTypes)];
    this.modalToggleService.emitToggleWithRef(this.MODAL_ASSOCIATE_UH_ID);
  }

  public removeUhsSelectedFromUhList(listTypes: Array<UhTypeAssociateDto>) {
    this.uhTypeList = [..._.differenceBy(this.uhTypeList, listTypes, 'abbreviation')];
  }

  public removeUhTypeAssociated(element: UhTypeAssociateDto) {
    const indexOfElement: number = this.pageItemsList.indexOf(element);
    this.pageItemsList.splice(indexOfElement, 1);
    this.associatedUhTypes = _.clone(this.pageItemsList);
    this.uhTypeList.push(element);
    this.uhTypeList = [...this.uhTypeList];
  }

  public changeVariationType(item, element) {
    element.isDecreased = item == ComissionTypeEnum.DECREATE;
    element.variationType = item;
  }

  public updateList(filteredList) {
    this.pageItemsList = filteredList;
  }

  public varifyPercentField(event, row) {
    row.percentualAmmount = this.currencyService.verifyPecentageField(event, row.percentualAmmount);
  }

  private mapList(list: Array<UhTypeAssociateDto>) {
    list.forEach(item => {
      item['variationType'] = item.isDecreased ? ComissionTypeEnum.DECREATE : ComissionTypeEnum.INCREASE;
      item['name'] = item.hasOwnProperty('roomTypeName') ? item.roomTypeName : item.name;
    });
  }

  // Config
  public setButtonConfig() {
    this.associateButtonConfig = this.sharedService.getButtonConfig(
      'associateUhType',
      this.prepareToAssociate.bind(this),
      'label.associateUhType',
      ButtonType.Secondary,
    );
    this.associateButtonConfig.extraClass = 'thf-u-width-flex';
  }

  public setTableColumns() {
    if (!this.pageItemsList) {
      this.pageItemsList = [];
    }
    this.columns = [];

    this.translateService.get('label.abbreviation').subscribe(value => {
      this.columns.push({ name: value, prop: 'abbreviation' });
    });
    this.translateService.get('label.description').subscribe(value => {
      this.columns.push({ name: value, prop: 'name' });
    });
    this.translateService.get('label.rateVariation').subscribe(value => {
      this.columns.push({
        name: value,
        prop: 'rateVariation',
        cellTemplate: this.variationCellTemplate,
        cellClass: 'thf-u-padding-vertical--05',
      });
      this.showTable = true;
    });
  }

  public setRateType() {
    const rateList = [];
    const rateIncrease = new RateTypeDto();
    rateIncrease.id = ComissionTypeEnum.INCREASE;
    rateIncrease.name = 'label.increase';
    rateList.push(rateIncrease);

    const rateDecrease = new RateTypeDto();
    rateDecrease.id = ComissionTypeEnum.DECREATE;
    rateDecrease.name = 'label.decrease';
    rateList.push(rateDecrease);
    this.rateTypeList = this.commomService.toOption(rateList, 'id', 'name');
  }

  public setSearchConfig() {
    this.searchBarConfig = new SearchableItems();
    this.searchBarConfig.items = this.pageItemsList;
    this.searchBarConfig.tableRowTypeEnum = TableRowTypeEnum.RatePlanUhType;
    this.searchBarConfig.placeholderSearchFilter = 'placeholder.searchByAbbreviationAndDesc';
  }
}
