import { RouterModule, Routes } from '@angular/router';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RatePlanListComponent } from './rate-plan-list/rate-plan-list.component';
import { RatePlanCreateEditComponent } from './rate-plan-create-edit/rate-plan-create-edit.component';

export const ratePlanRoutes: Routes = [
  { path: '', component: RatePlanListComponent },
  { path: 'new', component: RatePlanCreateEditComponent },
  { path: 'edit/:ratePlanId', component: RatePlanCreateEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(ratePlanRoutes)],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class RatePlanRoutingModule {}
