import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { CompanyClientDto } from 'app/shared/models/dto/rate-plan/company-client-dto';
import { CompanyClientCategoryDto } from 'app/shared/models/dto/rate-plan/company-client-category-dto';
import * as _ from 'lodash';
import { TableRowTypeEnum } from 'app/shared/models/table-row-type-enum';
import { SearchableItems } from 'app/shared/models/filter-search/searchable-item';

@Component({
  selector: 'customer-associate-control-box',
  templateUrl: './customer-associate-control-box.component.html',
  styleUrls: ['./customer-associate-control-box.component.css'],
})
export class CustomerAssociateControlBoxComponent implements OnInit {
  public readonly MODAL_ASSOCIATE_CUSTOMER_ID = 'id_rate_plan_modal_assotiate_customer';

  @Input() companyClientList: Array<CompanyClientDto>;
  @Input() companyClientCategoryList: Array<CompanyClientCategoryDto>;

  @Input()
  set associatedCustomers(list: Array<any>) {
    this.pageItemsList = list;
    this.removeCustomersSelectedFromCustomerList(list);
    this.associatedCustomersChange.emit(list);
  }
  get associatedCustomers() {
    return this.pageItemsList;
  }
  @Output() associatedCustomersChange = new EventEmitter();

  // Table
  public columns: Array<any>;
  public pageItemsList: Array<any>;

  // General
  public associateButtonConfig: ButtonConfig;
  public showTable = false;
  public searchBarConfig: SearchableItems;

  constructor(
    private sharedService: SharedService,
    private modalToggle: ModalEmitToggleService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.setButtonConfig();
    this.setTableConfig();
    this.setSeachConfig();
  }

  // Actions
  public prepareToAssociate() {
    this.modalToggle.emitToggleWithRef(this.MODAL_ASSOCIATE_CUSTOMER_ID);
  }

  public closeModal() {
    this.modalToggle.emitToggleWithRef(this.MODAL_ASSOCIATE_CUSTOMER_ID);
  }

  public removeCustomerAssociated(element) {
    const indexOfElement: number = this.pageItemsList.indexOf(element);
    this.pageItemsList.splice(indexOfElement, 1);
    this.associatedCustomers = _.clone(this.pageItemsList);
    this.companyClientList.push(element);
    this.companyClientList = [...this.companyClientList];
  }

  public associateCustomer(customerList) {
    this.associatedCustomers = [..._.concat(this.pageItemsList, customerList)];
    this.modalToggle.emitToggleWithRef(this.MODAL_ASSOCIATE_CUSTOMER_ID);
  }

  public removeCustomersSelectedFromCustomerList(customerList) {
    this.companyClientList = [..._.differenceBy(this.companyClientList, customerList, 'document')];
  }

  public updateList(filteredList) {
    this.pageItemsList = filteredList;
  }

  // Config
  public setButtonConfig() {
    this.associateButtonConfig = this.sharedService.getButtonConfig(
      'associateCustomer',
      this.prepareToAssociate.bind(this),
      'label.associateCustomer',
      ButtonType.Secondary,
    );
    this.associateButtonConfig.extraClass = 'thf-u-width-flex';
  }

  public setTableConfig() {
    if (!this.pageItemsList) {
      this.pageItemsList = [];
    }
    this.columns = [];

    this.translateService.get('label.name').subscribe(value => {
      this.columns.push({ name: value, prop: 'name' });
    });
    this.translateService.get('label.cnpj').subscribe(value => {
      this.columns.push({ name: value, prop: 'document' });
    });
    this.translateService.get('label.category').subscribe(value => {
      this.columns.push({ name: value, prop: 'clientCategoryName' });
    });
    this.translateService.get('label.phone').subscribe(value => {
      this.columns.push({ name: value, prop: 'phone' });
    });
    this.translateService.get('label.address').subscribe(value => {
      this.columns.push({ name: value, prop: 'address' });
      this.showTable = true;
    });
  }

  public setSeachConfig() {
    this.searchBarConfig = new SearchableItems();
    this.searchBarConfig.items = this.pageItemsList;
    this.searchBarConfig.tableRowTypeEnum = TableRowTypeEnum.RatePlanCustomers;
    this.searchBarConfig.placeholderSearchFilter = 'placeholder.searchByNameAndCnpj';
  }
}
