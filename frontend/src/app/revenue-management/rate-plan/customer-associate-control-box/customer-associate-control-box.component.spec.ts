import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CustomerAssociateControlBoxComponent } from './customer-associate-control-box.component';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';

describe('CustomerAssociateControlBoxComponent', () => {
  let component: CustomerAssociateControlBoxComponent;
  let fixture: ComponentFixture<CustomerAssociateControlBoxComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule
      ],
      declarations: [CustomerAssociateControlBoxComponent],
      providers: [
        sharedServiceStub,
        modalToggleServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(CustomerAssociateControlBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
