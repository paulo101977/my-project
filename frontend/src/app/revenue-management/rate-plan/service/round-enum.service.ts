import {Injectable} from '@angular/core';
import {RoundEnum} from 'app/revenue-management/rate-plan/models/round-enum';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class RoundEnumService {

  constructor(
    private translateService: TranslateService
  ) {}

  public getRoundEnumList() {
    return [
      {
        key: RoundEnum.Default,
        value: this.translateService.instant('label.default')
      },
      {
        key: RoundEnum.NotRound,
        value: this.translateService.instant('label.notRound')
      }
    ];
  }
}
