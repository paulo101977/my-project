import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintRpsComponent } from './print-rps.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';

describe('PrintRpsComponent', () => {
  let component: PrintRpsComponent;
  let fixture: ComponentFixture<PrintRpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), SharedModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintRpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
