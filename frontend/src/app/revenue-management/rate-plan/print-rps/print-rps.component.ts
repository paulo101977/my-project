import { Component, Input, OnInit } from '@angular/core';
import { RpsPrintDto } from '../../../shared/models/dto/rps/rps-print-dto';
import { BillingAccountItemEntry } from '../../../shared/models/billing-account/billing-account-item-entry';

@Component({
  selector: 'app-print-rps',
  templateUrl: './print-rps.component.html',
  styleUrls: ['./print-rps.component.css'],
})
export class PrintRpsComponent implements OnInit {
  @Input() showTable = false;
  private _rpsPrint;
  set rpsPrint(rps: RpsPrintDto) {
    this._rpsPrint = rps;
    this.extractBillingAccountItens();
  }
  get rpsPrint(): RpsPrintDto {
    return this._rpsPrint;
  }

  public billintItens: Array<BillingAccountItemEntry>;
  public accountTotal: number;

  constructor() {}

  ngOnInit() {
    this.setTableConfig();
  }

  public extractBillingAccountItens() {
    const accounts = this.rpsPrint.accountEntries.billingAccounts;
    this.accountTotal = 0;
    this.billintItens = [];
    for (const account of accounts) {
      for (const item of account.billingAccountItems) {
        if (item.billingAccountChildrenItems) {
          this.billintItens.push(...item.billingAccountChildrenItems);
          this.accountTotal += item.billingAccountChildrenItems.map(children => children.total).reduce((total1, total2) => total1 + total2);
        }
      }
    }
  }

  public setTableConfig() {
    this.showTable = true;
  }
}
