import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedRateAssociateControlBoxComponent } from './fixed-rate-associate-control-box.component';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { imgCdnStub } from '../../../../../testing';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FixedRateAssociateControlBoxComponent', () => {
  let component: FixedRateAssociateControlBoxComponent;
  let fixture: ComponentFixture<FixedRateAssociateControlBoxComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        HttpClientTestingModule,
      ],
      providers: [
        sharedServiceStub,
      ],
      declarations: [
        FixedRateAssociateControlBoxComponent,
        imgCdnStub,
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(FixedRateAssociateControlBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should inactivate calendarButton when list empty', () => {
    component.pageItemsList = null;
    expect(component.calendarButtonConfig.isDisabled).toBeTruthy();
  });
});
