import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { RateHistoryHeaderDto } from '../../rate-history/models/rate-history-header-dto';
import { RateHistoryService } from '@app/revenue-management/rate-history/services/rate-history.service';

@Component({
  selector: 'app-fixed-rate-associate-control-box',
  templateUrl: './fixed-rate-associate-control-box.component.html',
  styleUrls: ['./fixed-rate-associate-control-box.component.css']
})
export class FixedRateAssociateControlBoxComponent implements OnInit, OnChanges {

  @ViewChild('apliedToCell') apliedToCell: TemplateRef<any>;
  @ViewChild('historyCell') historyCell: TemplateRef<any>;

  // Config
  public calendarButtonConfig: ButtonConfig;
  public insertButtonConfig: ButtonConfig;

  // table
  public pageItemsList: Array<any>;
  public columns: Array<any>;

  public isListFiltered = false;

  @Input() historyList: Array<RateHistoryHeaderDto>;

  @Output() seeCalendarClicked = new EventEmitter();
  @Output() addFixedRateClicked = new EventEmitter();
  @Output() seeHistoryClicked = new EventEmitter();

  constructor(
    private sharedService: SharedService,
    public rateHistoryService: RateHistoryService
  ) {}

  ngOnInit() {
    this.configButtons();
    this.setTableConfig();
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    this.setPageItens(simpleChanges.historyList.currentValue);
  }

  private setPageItens(list: Array<any>) {
    this.pageItemsList = list;
    if (this.pageItemsList) {
      this.pageItemsList = this.rateHistoryService.headerHistoryMapper(this.pageItemsList);
    }
    this.controlCalendarButtonVisibility();
  }

  private controlCalendarButtonVisibility() {
    if ( this.calendarButtonConfig ) {
      this.calendarButtonConfig.isDisabled = !(this.pageItemsList && this.pageItemsList.length > 0);
    }
  }

  public setTableConfig() {
    this.columns = [];

    this.columns.push({name: 'label.vigence', prop: 'vigence'});
    this.columns.push({name: 'label.currency', prop: 'currencyName'});
    this.columns.push({name: 'label.apliedTo', prop: 'appliedTo', cellTemplate: this.apliedToCell});
    this.columns.push({
      name: 'label.seeHistory', prop: 'id',
      cellTemplate: this.historyCell, headerClass: 'thf-u-text-center', cellClass: 'thf-u-text-center'
    });
  }

  public configButtons() {
    this.calendarButtonConfig = this.sharedService.getButtonConfig(
      'calendar-button-config',
      this.goToCalendar,
      'label.seeCalendar',
      ButtonType.Secondary,
    );
    this.controlCalendarButtonVisibility();

    this.insertButtonConfig = this.sharedService.getButtonConfig(
      'insert-rate-button-config',
      this.addFixedRate,
      'label.addRate',
      ButtonType.Secondary
    );
  }

  public goToCalendar = () => {
    this.seeCalendarClicked.emit();
  }

  public addFixedRate = () => {
    this.addFixedRateClicked.emit();
  }

  public seeHistory(id: string) {
    this.seeHistoryClicked.emit(id);
  }

  public updateList(list) {
    this.isListFiltered = !((this.historyList && list) && this.historyList.length == list.length);
    this.setPageItens(list);
  }

  public filterHistory(list) {
    this.pageItemsList = [...list];
    this.isListFiltered = true;
  }

}
