import { AfterViewInit, Component, DoCheck, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { TranslateService } from '@ngx-translate/core';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { DataTableGlobals } from '../../../shared/models/DataTableGlobals';
import { RatePlanUhTypeDto } from '../../../shared/models/dto/rate-plan/rate-plan-uh-type-dto';
import { RateTypeDto } from '../../../shared/models/dto/rate-type-dto';
import { ComissionTypeEnum } from '../../../shared/models/rate-plan/comission-type-enum';
import { UhTypeAssociateDto } from '../../../shared/models/dto/rate-plan/uh-type-associate-dto';
import * as _ from 'lodash';
import { CurrencyService } from '../../../shared/services/shared/currency-service.service';

@Component({
  selector: 'uh-type-associate-modal',
  templateUrl: './uh-type-associate-modal.component.html',
  styleUrls: ['./uh-type-associate-modal.component.css'],
})
export class UhTypeAssociateModalComponent implements OnInit, DoCheck {
  @Input() modalId: string;

  private _uhTpeList: Array<RatePlanUhTypeDto>;
  @Input()
  set uhTypeList(list: Array<RatePlanUhTypeDto>) {
    if (list) {
      this._uhTpeList = _.orderBy(list, ['id'], ['asc']);
    }
    this.originalUhTypeList = _.clone(this._uhTpeList);
  }
  get uhTypeList() {
    return this._uhTpeList;
  }

  @Output() shouldCloseModal: EventEmitter<any> = new EventEmitter();
  @Output() confirmAssociation: EventEmitter<Array<UhTypeAssociateDto>> = new EventEmitter<Array<UhTypeAssociateDto>>();

  // Info
  public rateTypeList: Array<RateTypeDto>;
  public rateTypeSelected: number;
  public isDecreased: boolean;
  public showTable = false;
  public rateVarianty: number;

  // Config
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  // Table
  public columns: Array<any> = [];
  public uhTypeSelectedList: Array<RatePlanUhTypeDto> = [];
  private originalUhTypeList: Array<RatePlanUhTypeDto>;

  constructor(
    private sharedService: SharedService,
    public datatableGlobals: DataTableGlobals,
    private translateService: TranslateService,
    private commomService: CommomService,
    private modalToggle: ModalEmitToggleService,
    private currencyService: CurrencyService,
  ) {}

  // Lifecycle
  ngOnInit() {
    this.setButtonsConfig();
    this.setTableConfig();
    this.setRateType();
    this.modalToggle.changeEmitted$.subscribe(result => {
      setTimeout(() => {
        this.showTable = !this.showTable;
      }, 5);
      this.cleanFields();
    });
  }

  ngDoCheck(): void {
    this.buttonConfirmConfig.isDisabled = !(this.uhTypeSelectedList.length > 0 && this.rateVarianty >= 0);
  }

  // Actions
  public cancel() {
    this.shouldCloseModal.emit();
  }

  public confirm() {
    const confirmationUhsList = [];
    this.uhTypeSelectedList.forEach(uhType => {
      const uhTypeAssociate = new UhTypeAssociateDto();
      uhTypeAssociate.id = uhType.id;
      uhTypeAssociate.roomTypeId = !uhType.roomTypeId ? uhType.id : uhType.roomTypeId;
      uhTypeAssociate.abbreviation = uhType.abbreviation;
      uhTypeAssociate.name = uhType.name;
      uhTypeAssociate.percentualAmmount = this.rateVarianty;
      uhTypeAssociate.isDecreased = this.isDecreased;
      uhTypeAssociate.publishOnline = true; // todo verificar se esse propriedade deveria estar aqui
      confirmationUhsList.push(uhTypeAssociate);
    });
    this.confirmAssociation.emit(confirmationUhsList);
  }

  public searchByTerm(term) {
    if (!term) {
      this._uhTpeList = _.clone(this.originalUhTypeList);
    } else {
      const filteredList = _.clone(this.uhTypeList).filter(item => {
        if (item.abbreviation.toLowerCase().indexOf(term.toLowerCase()) > -1 || item.name.toLowerCase().indexOf(term.toLowerCase()) > -1) {
          return item;
        }
      });
      this._uhTpeList = [...filteredList];
    }
  }

  public verifyCommissionPercent(value) {
    this.rateVarianty = this.currencyService.verifyPecentageField(value, this.rateVarianty);
  }
  // Config
  public setRateType() {
    const rateList = [];
    const rateIncrease = new RateTypeDto();
    rateIncrease.id = ComissionTypeEnum.INCREASE;
    rateIncrease.name = 'label.increase';
    rateList.push(rateIncrease);

    const rateDecrease = new RateTypeDto();
    rateDecrease.id = ComissionTypeEnum.DECREATE;
    rateDecrease.name = 'label.decrease';
    rateList.push(rateDecrease);

    this.rateTypeList = this.commomService.toOption(rateList, 'id', 'name');
    this.rateTypeSelected = ComissionTypeEnum.INCREASE;
  }

  public setButtonsConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancelModalUhType',
      this.cancel.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirmModalUhType',
      this.confirm.bind(this),
      'action.confirm',
      ButtonType.Primary,
    );
  }

  public setTableConfig() {
    this.columns = [];

    this.translateService.get('label.abbreviation').subscribe(value => {
      this.columns.push({ name: value, prop: 'abbreviation', flexGrow: 1 });
    });
    this.translateService.get('label.description').subscribe(value => {
      this.columns.push({ name: value, prop: 'name', flexGrow: 1 });
    });
  }

  public cleanFields() {
    this.rateVarianty = 0;
    this.uhTypeSelectedList = [];
  }

  public changeVariationType(value) {
    this.isDecreased = value == ComissionTypeEnum.DECREATE;
  }
}
