import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '../../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { UhTypeAssociateModalComponent } from './uh-type-associate-modal.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('UhTypeAssociateModalComponent', () => {
  let component: UhTypeAssociateModalComponent;
  let fixture: ComponentFixture<UhTypeAssociateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [UhTypeAssociateModalComponent],
      providers: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UhTypeAssociateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
