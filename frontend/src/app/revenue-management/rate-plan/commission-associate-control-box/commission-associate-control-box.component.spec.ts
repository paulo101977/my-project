import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CommissionAssociateControlBoxComponent } from './commission-associate-control-box.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';

describe('CommissionAssociateControlBoxComponent', () => {
  let component: CommissionAssociateControlBoxComponent;
  let fixture: ComponentFixture<CommissionAssociateControlBoxComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
      ],
      declarations: [CommissionAssociateControlBoxComponent],
      providers: [
        sharedServiceStub,
        modalToggleServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(CommissionAssociateControlBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
