import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { TranslateService } from '@ngx-translate/core';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { RatePlanBillingItemDto } from 'app/shared/models/dto/rate-plan/rate-plan-billing-item-dto';
import { RatePlanComissionDto } from 'app/shared/models/dto/rate-plan/rate-plan-comission-dto';
import { SearchableItems } from 'app/shared/models/filter-search/searchable-item';
import { TableRowTypeEnum } from 'app/shared/models/table-row-type-enum';
import * as _ from 'lodash';

@Component({
  selector: 'commission-associate-control-box',
  templateUrl: './commission-associate-control-box.component.html',
  styleUrls: ['./commission-associate-control-box.component.css'],
})
export class CommissionAssociateControlBoxComponent implements OnInit {
  @ViewChild('percentCell') percentCell: TemplateRef<any>;

  public readonly MODAL_ASSOCIATE_COMISSION_ID = 'id_rate_plan_modal_assotiate_comission';

  @Input() billingItemTypeList: Array<RatePlanBillingItemDto>;

  @Input()
  set associatedComissions(list: Array<any>) {
    this.pageItemsList = list;
    this.associatedComissionsChange.emit(list);
  }
  get associatedComissions(): Array<any> {
    return this.pageItemsList;
  }
  @Output() associatedComissionsChange = new EventEmitter();

  // Table
  public columns: Array<any>;
  public pageItemsList: Array<any>;

  // General
  public associateButtonConfig: ButtonConfig;
  public showTable = false;
  public searchBarConfig: SearchableItems;

  constructor(
    private sharedService: SharedService,
    private modalToggle: ModalEmitToggleService,
    private translateService: TranslateService,
  ) {}

  // Lifecycle
  ngOnInit() {
    this.setButtonConfig();
    this.setTableConfig();
    this.setSeachConfig();
  }

  // Actions
  public prepareToAssociate() {
    this.modalToggle.emitToggleWithRef(this.MODAL_ASSOCIATE_COMISSION_ID);
  }

  public cancel() {
    this.modalToggle.emitToggleWithRef(this.MODAL_ASSOCIATE_COMISSION_ID);
  }

  public addComission(comission: RatePlanComissionDto) {
    this.pageItemsList.push(comission);
    this.associatedComissions = [...this.pageItemsList];
    this.modalToggle.emitToggleWithRef(this.MODAL_ASSOCIATE_COMISSION_ID);
  }

  public removeComissionAssociation(element) {
    const indexOfElement: number = this.pageItemsList.indexOf(element);
    this.pageItemsList.splice(indexOfElement, 1);
    this.associatedComissions = _.clone(this.pageItemsList);
  }

  public updateList(filteredList) {
    this.pageItemsList = filteredList;
  }

  // Config
  public setButtonConfig() {
    this.associateButtonConfig = this.sharedService.getButtonConfig(
      'associateComission',
      this.prepareToAssociate.bind(this),
      'label.associateComissions',
      ButtonType.Secondary,
    );
    this.associateButtonConfig.extraClass = 'thf-u-width-flex';
  }

  public setTableConfig() {
    if (!this.pageItemsList) {
      this.pageItemsList = [];
    }
    this.columns = [];

    this.translateService.get('label.service').subscribe(value => {
      this.columns.push({ name: value, prop: 'billingItemName' });
    });
    this.translateService.get('label.comissionPercent').subscribe(value => {
      this.columns.push({ name: value, prop: 'commissionPercentualAmount', cellTemplate: this.percentCell });
      this.showTable = true;
    });
  }

  public setSeachConfig() {
    this.searchBarConfig = new SearchableItems();
    this.searchBarConfig.items = this.pageItemsList;
    this.searchBarConfig.tableRowTypeEnum = TableRowTypeEnum.RatePlanComission;
    this.searchBarConfig.placeholderSearchFilter = 'placeholder.searchByService';
  }
}
