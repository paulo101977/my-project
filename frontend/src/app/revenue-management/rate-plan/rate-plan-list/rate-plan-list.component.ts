import { Component, ComponentRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';

// Resouces
import { RatePlanResource } from '../../../shared/resources/rate-plan/rate-plan.resource';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { DateService } from '../../../shared/services/shared/date.service';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import { PrintHelperService } from '../../../shared/services/shared/print-helper.service';

@Component({
  selector: 'app-rate-plan-list',
  templateUrl: './rate-plan-list.component.html',
  styleUrls: ['./rate-plan-list.component.css'],
})
export class RatePlanListComponent implements OnInit {
  // header
  public configHeaderPage: ConfigHeaderPageNew;
  public searchableItems: SearchableItems;

  // Table dependencies
  public columns: Array<any>;
  public pageItemsList: Array<any>;

  // General
  public propertyId: number;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private ratePlaResource: RatePlanResource,
    private toastEmitter: ToasterEmitService,
    private translateService: TranslateService,
    private dateService: DateService,
    private loaderService: LoadPageEmitService,
    private printService: PrintHelperService,
  ) {}

  // Lifecycle
  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.configTable();
    this.setConfigHeaderPage();
    this.setSearchableItems();
    this.loadRatePlans();
  }

  // integration
  private loadRatePlans() {
    this.loaderService.emitChange(ShowHide.show);
    this.ratePlaResource.getAll(this.propertyId).subscribe(result => {
      if (result && result.items) {
        result.items.forEach(item => {
          item['startDateStr'] = this.dateService.convertDateToStringViewFormat(item.startDate);
          item['endDateStr'] = this.dateService.convertDateToStringViewFormat(item.endDate);
        });
        this.pageItemsList = result.items;
        this.searchableItems.items = this.pageItemsList;
      }
      this.loaderService.emitChange(ShowHide.hide);
    });
  }

  // Table methods
  public configTable() {
    this.columns = [];
    this.translateService.get('label.ratePlanName').subscribe(value => {
      this.columns.push({ name: value, prop: 'agreementName' });
    });
    this.translateService.get('label.ratePlanType').subscribe(value => {
      this.columns.push({ name: value, prop: 'agreementTypeName' });
    });
    this.translateService.get('label.ratePlan_rigs').subscribe(value => {
      this.columns.push({ name: value, prop: 'distributionCode' });
    });
    this.translateService.get('label.initVigence').subscribe(value => {
      this.columns.push({ name: value, prop: 'startDateStr' });
    });
    this.translateService.get('label.endVigence').subscribe(value => {
      this.columns.push({ name: value, prop: 'endDateStr' });
    });
  }

  public updateStatus(element) {
    this.ratePlaResource.toggleStatus(element.id).subscribe(
      result => {
        this.showToastStatus(SuccessError.success, element.isActive);
      },
      error => {
        this.showToastStatus(SuccessError.error, element.isActive);
        element.isActive = !element.isActive;
      },
    );
  }

  public goToEditPage(element) {
    this.router.navigate(['edit', element.id], {
      relativeTo: this.route,
    });
  }

  // Configuration
  public goToNewRatePlanPage = () => {
    this.router.navigate(['new'], {
      relativeTo: this.route,
    });
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      keepButtonActive: true,
      callBackFunction: this.goToNewRatePlanPage,
    };
  }

  private setSearchableItems(): void {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.pageItemsList;
    this.searchableItems.placeholderSearchFilter = 'placeholder.ratePlanSeachbar';
    this.searchableItems.tableRowTypeEnum = TableRowTypeEnum.RatePlan;
  }

  public updateList(items: Array<any>) {
    this.pageItemsList = items;
  }

  // Utils
  public showToastStatus(typeMessage: SuccessError, isActive) {
    let message;

    if (typeMessage == SuccessError.success && isActive) {
      message = 'label.ratePlanstatusActivateSuccess';
    }
    if (typeMessage == SuccessError.success && !isActive) {
      message = 'label.ratePlanstatusInactivateSuccess';
    }
    if (typeMessage == SuccessError.error && isActive) {
      message = 'label.ratePlanstatusActivateError';
    }
    if (typeMessage == SuccessError.error && !isActive) {
      message = 'label.ratePlanstatusInactivateError';
    }

    this.toastEmitter.emitChangeTwoMessages(typeMessage, this.translateService.instant(message), '');
  }
}
