import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RatePlanListComponent } from './rate-plan-list.component';
import { InterceptorHandlerService, ThexApiModule, configureApi, ApiEnvironment } from '@inovacaocmnet/thx-bifrost';

describe('RatePlanListComponent', () => {
  let component: RatePlanListComponent;
  let fixture: ComponentFixture<RatePlanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, TranslateModule.forRoot(), ThexApiModule],
      declarations: [RatePlanListComponent],
      providers: [ InterceptorHandlerService,
        configureApi({
          environment: ApiEnvironment.Development
        })
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatePlanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
