import { Component, DoCheck, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { CompanyClientCategoryDto } from 'app/shared/models/dto/rate-plan/company-client-category-dto';
import { CompanyClientDto } from 'app/shared/models/dto/rate-plan/company-client-dto';
import * as _ from 'lodash';

@Component({
  selector: 'thx-customer-associate-modal',
  templateUrl: './customer-associate-modal.component.html',
  styleUrls: ['./customer-associate-modal.component.css'],
})
export class CustomerAssociateModalComponent implements OnInit, DoCheck {
  @Input() modalId: string;
  private _companyClientList: Array<CompanyClientDto>;
  @Input()
  set companyClientList(list: Array<CompanyClientDto>) {
    if (list) {
      this._companyClientList = _.orderBy(list, ['id'], ['asc']);
    }
    this.originalCustorList = _.clone(this._companyClientList);
  }
  get companyClientList() {
    return this._companyClientList;
  }

  private _companyClientCategoryList: Array<CompanyClientCategoryDto>;
  @Input()
  set companyClientCategoryList(list: Array<CompanyClientCategoryDto>) {
    if (list) {
      this._companyClientCategoryList = list;
    }
    this.setCustomerCategory();
  }
  get companyClientCategoryList() {
    return this._companyClientCategoryList;
  }

  @Output() shouldCloseModal: EventEmitter<any> = new EventEmitter();
  @Output() confirmAssociation: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

  // Info
  public customerCategoryList: Array<any>;
  public rateTypeSelected: number;
  public showTable = false;

  // Config
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  // Table
  public columns: Array<any> = [];
  public customerSelectedList: Array<any> = [];
  private originalCustorList: Array<any>;

  constructor(
    private sharedService: SharedService,
    private translateService: TranslateService,
    private commomService: CommomService,
    private modalToggle: ModalEmitToggleService,
  ) {}

  // Lifecycle
  ngOnInit() {
    this.setButtonsConfig();
    this.setTableConfig();
    this.modalToggle.changeEmitted$.subscribe(() => {
      setTimeout(() => {
        this.showTable = !this.showTable;
      }, 5);
      this.cleanFilds();
    });
  }

  ngDoCheck(): void {
    this.buttonConfirmConfig.isDisabled = this.customerSelectedList.length <= 0;
  }

  // Actions
  public cancel() {
    this.shouldCloseModal.emit();
  }

  public confirm() {
    this.confirmAssociation.emit(this.customerSelectedList);
  }

  public searchByTerm(term) {
    if (!term) {
      this._companyClientList = _.clone(this.originalCustorList);
    } else {
      this._companyClientList = [
        ...this.companyClientList.filter(item => {
          if (item.document.toLowerCase().indexOf(term.toLowerCase()) > -1 || item.name.toLowerCase().indexOf(term.toLowerCase()) > -1) {
            return item;
          }
        }),
      ];
    }
  }

  public filterCustomersByCategory(id) {
    if (!id) {
      this._companyClientList = _.clone(this.originalCustorList);
    } else {
      this._companyClientList = [
        ..._.clone(this.originalCustorList).filter(item => {
          if (item.clientCategoryId == id) {
            return item;
          }
        }),
      ];
    }
  }

  // Config
  public setCustomerCategory() {
    this.customerCategoryList = this.commomService.toOption(
      this.companyClientCategoryList,
      'id',
      'propertyCompanyClientCategoryName',
    );
  }

  public setButtonsConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancelModalCustomer',
      this.cancel.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirmModalCustomer',
      this.confirm.bind(this),
      'action.confirm',
      ButtonType.Primary,
    );
  }

  public setTableConfig() {
    this.columns = [];
    this.translateService.get('label.name').subscribe(value => {
      this.columns.push({ name: value, prop: 'name', flexGrow: 1 });
    });
    this.translateService.get('label.cnpj').subscribe(value => {
      this.columns.push({ name: value, prop: 'document', flexGrow: 1 });
    });
    this.translateService.get('label.category').subscribe(value => {
      this.columns.push({ name: value, prop: 'clientCategoryName', flexGrow: 1 });
    });
  }

  public cleanFilds() {
    this.customerSelectedList = [];
  }
}
