import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CustomerAssociateModalComponent } from './customer-associate-modal.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';

describe('CustomerAssociateModalComponent', () => {
  let component: CustomerAssociateModalComponent;
  let fixture: ComponentFixture<CustomerAssociateModalComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
      ],
      declarations: [
        CustomerAssociateModalComponent
      ],
      providers: [
        sharedServiceStub,
        commomServiceStub,
        modalToggleServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerAssociateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
