export enum RoundEnum {
  NotRound = 1,
  Default = 2,
  RoundUp = 3,
  RoundDown = 4
}
