import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { RatePlanRoutingModule } from './rate-plan-routing.module';
import { MyDatePickerModule } from 'mydatepicker';
import { RatePlanListComponent } from './rate-plan-list/rate-plan-list.component';
import { RatePlanCreateEditComponent } from './rate-plan-create-edit/rate-plan-create-edit.component';
import { UhTypeControlBoxComponent } from './uh-type-control-box/uh-type-control-box.component';
import { UhTypeAssociateModalComponent } from './uh-type-associate-modal/uh-type-associate-modal.component';
import { CustomerAssociateControlBoxComponent } from './customer-associate-control-box/customer-associate-control-box.component';
import { CommissionAssociateControlBoxComponent } from './commission-associate-control-box/commission-associate-control-box.component';
import { CustomerAssociateModalComponent } from './customer-associate-modal/customer-associate-modal.component';
import { ComissionAssociateModalComponent } from './comission-associate-modal/comission-associate-modal.component';
import { PrintRpsComponent } from './print-rps/print-rps.component';
import { FixedRateAssociateControlBoxComponent } from './fixed-rate-associate-control-box/fixed-rate-associate-control-box.component';
import { RateHistoryModule } from '@app/revenue-management/rate-history/rate-history.module';
import { InfoTooltipModule } from '@inovacao-cmnet/thx-ui';

@NgModule({
  imports: [CommonModule,
    SharedModule,
    RouterModule,
    RatePlanRoutingModule,
    MyDatePickerModule,
    RateHistoryModule,
    InfoTooltipModule],
  declarations: [
    RatePlanListComponent,
    RatePlanCreateEditComponent,
    UhTypeControlBoxComponent,
    UhTypeAssociateModalComponent,
    CustomerAssociateControlBoxComponent,
    CommissionAssociateControlBoxComponent,
    CustomerAssociateModalComponent,
    ComissionAssociateModalComponent,
    FixedRateAssociateControlBoxComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class RatePlanModule {}
