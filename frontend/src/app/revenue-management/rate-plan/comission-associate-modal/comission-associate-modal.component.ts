//  Angular imports
import { Component, DoCheck, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { RatePlanBillingItemDto } from '../../../shared/models/dto/rate-plan/rate-plan-billing-item-dto';
import { RatePlanComissionDto } from '../../../shared/models/dto/rate-plan/rate-plan-comission-dto';
import { CurrencyService } from '../../../shared/services/shared/currency-service.service';

@Component({
  selector: 'thx-comission-associate-modal',
  templateUrl: './comission-associate-modal.component.html',
  styleUrls: ['./comission-associate-modal.component.css'],
})
export class ComissionAssociateModalComponent implements OnInit, DoCheck {
  @Input() modalId: string;

  private _billingItemTypeList: Array<RatePlanBillingItemDto>;
  @Input()
  set billingItemTypeList(list: Array<RatePlanBillingItemDto>) {
    this._billingItemTypeList = list;
    this.serviceList = this.commomService.toOption(
      this._billingItemTypeList,
      'id',
      'billingItemName',
    );
  }
  get billingItemTypeList() {
    return this._billingItemTypeList;
  }

  @Output() shouldCloseModal = new EventEmitter();
  @Output() confirmAssociation: EventEmitter<RatePlanComissionDto> = new EventEmitter<RatePlanComissionDto>();

  public serviceList: Array<any>;
  public serviceSelected: number;

  public comissionForm: FormGroup;

  // Config
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  constructor(
    private sharedService: SharedService,
    private commomService: CommomService,
    private formBuilder: FormBuilder,
    private toggleModal: ModalEmitToggleService,
    private curerncyService: CurrencyService,
  ) {}

  // Lifecycle
  ngOnInit() {
    this.toggleModal.changeEmitted$.subscribe(result => {
      this.setForm();
    });
    this.setForm();
    this.setButtonsConfig();
  }

  ngDoCheck(): void {
    if (this.buttonConfirmConfig && this.comissionForm) {
      this.buttonConfirmConfig.isDisabled = !this.comissionForm.valid;
    }
  }

  // Actions
  public cancel() {
    this.shouldCloseModal.emit();
  }

  public confirm() {
    const comission = new RatePlanComissionDto();
    comission.billingItemId = this.comissionForm.get('billingItemId').value;
    comission.billingItemName = this.getBillingItemName(comission.billingItemId);
    comission.commissionPercentualAmount = this.comissionForm.get('commissionPercentualAmount').value;

    this.confirmAssociation.emit(comission);
  }

  public verifyCommissionPercent(value) {
    const returnedValue = this.curerncyService.verifyPecentageField(value, this.comissionForm.get('commissionPercentualAmount').value);
    this.comissionForm.get('commissionPercentualAmount').setValue(returnedValue);
  }

  // Config
  public setButtonsConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancelModalAssociate',
      this.cancel.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirmModalAssociate',
      this.confirm.bind(this),
      'action.confirm',
      ButtonType.Primary,
    );
  }

  public setForm() {
    this.comissionForm = this.formBuilder.group({
      billingItemId: [null, Validators.required],
      commissionPercentualAmount: [null, Validators.required],
    });
  }

  public getBillingItemName(billingItemId) {
    return this.billingItemTypeList.filter(item => item.id == billingItemId)[0].billingItemName;
  }
}
