import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComissionAssociateModalComponent } from './comission-associate-modal.component';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { currencyServiceStub } from 'app/shared/services/shared/testing/currency-service';
import { createAccessorComponent } from '../../../../../testing';
import { configureTestSuite } from 'ng-bullet';


const thxError = createAccessorComponent('thx-error');

describe('ComissionAssociateModalComponent', () => {
  let component: ComissionAssociateModalComponent;
  let fixture: ComponentFixture<ComissionAssociateModalComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [
        ComissionAssociateModalComponent,
        thxError
      ],
      providers: [
        sharedServiceStub,
        commomServiceStub,
        modalToggleServiceStub,
        currencyServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ComissionAssociateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
