import { TestBed } from '@angular/core/testing';

import { PremiseRateService } from './premise-rate.service';
import {
  COL_REFERENCE,
  CONFIGURATION_PROPERTY,
  DATA,
  FILLED_WITH_AMOUNT,
  PAX_PROPERTY,
  PREMISE_BY_ID_PROPERTY,
  PROPERTY_PREMISE_LIST_TO_RECEIVE_PROPERTY,
  PROPERTY_PREMISE_LIST_TO_SEND_PROPERTY
} from 'app/revenue-management/premises/mock-data';
import { Operators } from 'app/revenue-management/premises/models/operators';


describe('PremiseRateService', () => {
  let service;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PremiseRateService]
    });
  });

  beforeEach(() => {
    service = new PremiseRateService();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test getPropertyOrNull', () => {
    let item = {};

    expect(service.getPropertyOrNull(item, 'value')).toEqual(null);

    item = { value: 0 };

    expect(service.getPropertyOrNull(item, 'value')).toEqual(0);

    item = { value: 1.01};

    expect(service.getPropertyOrNull(item, 'value')).toEqual(1.01);
  });

  it('should test getBaseRateConfigurationRoomType', () => {
    const result = service.getBaseRateConfigurationRoomType(DATA[PROPERTY_PREMISE_LIST_TO_SEND_PROPERTY]);

    expect(result).toEqual(DATA[PROPERTY_PREMISE_LIST_TO_RECEIVE_PROPERTY]);
  });

  it('should test countChildren', () => {
    const toCount = [];
    const falsy = { isActive: false };
    const truely = { isActive: true };

    expect(service['countChildren'](toCount)).toEqual(0);

    toCount.push(falsy);

    expect(service['countChildren'](toCount)).toEqual(0);

    toCount.push(truely);

    expect(service['countChildren'](toCount)).toEqual(1);

    toCount.push(falsy);

    expect(service['countChildren'](toCount)).toEqual(1);

    toCount.push(truely);

    expect(service['countChildren'](toCount)).toEqual(2);

  });

  it('should test getAdultCapacity', () => {

    const row = {
      roomTypeId: 1,
      adultCapacity: 3
    };

    const items = [
      {
        id: 1,
        adultCapacity: 3
      },
      {
        id: 2,
        adultCapacity: 5
      }
    ];

    expect(service['getAdultCapacity'](null, null)).toEqual(0);

    expect(service['getAdultCapacity'](row, items)).toEqual(3);

    row.roomTypeId = 2;
    expect(service['getAdultCapacity'](row, items)).toEqual(5);
  });

  describe('on calculateValue', () => {
    // TODO: add new operator on new version
    it('should test calculateValue sum', () => {

      expect(service['calculateValue'](1, 2, Operators.sum)).toEqual(3);
    });

    it('should test calculateValue minus', () => {
      expect(service['calculateValue'](2, 1, Operators.minus)).toEqual(1);
      expect(service['calculateValue'](11, 1, Operators.minus)).toEqual(10);
    });

    it('should test calculateValue fixed', () => {
      expect(service['calculateValue'](2, 100, Operators.fixed)).toEqual(100);
      expect(service['calculateValue'](11, 101, Operators.fixed)).toEqual(101);
    });
  });

  it('should test getPropertyNames', () => {

    expect(service['getPropertyNames'](3)).toEqual(DATA[PAX_PROPERTY][0]);

    expect(service['getPropertyNames'](6)).toEqual(DATA[PAX_PROPERTY][1]);

    expect(service['getPropertyNames'](8)).toEqual(DATA[PAX_PROPERTY][1]);
  });

  it('should test setColReference', () => {
    const premiseList = DATA[PREMISE_BY_ID_PROPERTY].propertyPremiseList;

    service['setColReference'](premiseList, 2, 1, 100);

    expect(service['colReference']).toEqual(DATA[COL_REFERENCE]);
  });

  it('should test setRow', () => {
    const premiseList = JSON.parse(JSON.stringify(DATA[PREMISE_BY_ID_PROPERTY].propertyPremiseList));
    const config = DATA[CONFIGURATION_PROPERTY].roomTypeList.items;
    const children = DATA[CONFIGURATION_PROPERTY].propertyParameterList;

    const paxReference = 1;

    service['setColReference'](premiseList, 2, paxReference, 50);

    service['setRow'](
      config,
      paxReference,
      premiseList,
      children
    );

    expect(premiseList).toEqual(DATA[FILLED_WITH_AMOUNT]);
  });


  it('should test setPremiseValues', () => {
    const propertyPremiseList = JSON.parse(JSON.stringify(DATA[PREMISE_BY_ID_PROPERTY].propertyPremiseList));
    const roomTypeWithBaseRateList = DATA[CONFIGURATION_PROPERTY].roomTypeList.items;
    const propertyParameterList = DATA[CONFIGURATION_PROPERTY].propertyParameterList;

    const paxReference = 1;

    const selectedPremise = { roomTypeReferenceId: 2, paxReference };


    service['setPremiseValues'](
      propertyParameterList,
      roomTypeWithBaseRateList,
      selectedPremise,
      propertyPremiseList,
      50,
      true
    );

    expect(propertyPremiseList).toEqual(DATA[FILLED_WITH_AMOUNT]);
  });
});
