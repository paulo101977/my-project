import { Injectable } from '@angular/core';
import { Operators } from 'app/revenue-management/premises/models/operators';

@Injectable({
  providedIn: 'root'
})
export class PremiseRateService {
  private readonly ADDITIONAL = 'Additional';
  private colReference = [];

  constructor() { }

  public setPremiseValues(
    propertyParameterList,
    roomTypeWithBaseRateList,
    selectedPremise,
    propertyPremiseList,
    currentPremiseValue,
    premiseId
  ) {
    if ( selectedPremise
      && propertyPremiseList
      && currentPremiseValue
      && premiseId ) {

      const { roomTypeReferenceId, paxReference } = selectedPremise;


      this.setColReference(
        propertyPremiseList,
        roomTypeReferenceId,
        paxReference,
        currentPremiseValue
      );

      this.setRow(
        roomTypeWithBaseRateList,
        paxReference,
        propertyPremiseList,
        propertyParameterList
      );
    }
  }

  private setColReference(
    propertyPremiseList,
    roomTypeReferenceId,
    paxReference,
    currentPremiseValue
  ) {
    this.colReference = []; // clean any

    propertyPremiseList.forEach(item => {
      const property = this.getPropertyNames(paxReference);

      if ( item.roomTypeId ===  roomTypeReferenceId) {
        item[property.paxIndexStr] = currentPremiseValue;
        this.colReference.push({
          ref: currentPremiseValue,
          roomTypeId: item.roomTypeId
        });
      } else {
        const value = this.calculateValue(
          currentPremiseValue,
          item[property.paxValueStr],
          item[property.paxOperatorStr]
        );

        item[property.paxIndexStr] = value;

        this.colReference.push({
          ref: value,
          roomTypeId: item.roomTypeId
        });
      }

    });
  }

  private getPropertyNames(paxReference) {
    return {
      paxIndexStr: paxReference > 5 ? `pax${this.ADDITIONAL}Amount` : `pax${paxReference}Amount`,
      paxValueStr: paxReference > 5 ? `pax${this.ADDITIONAL}` : `pax${paxReference}`,
      paxOperatorStr: paxReference > 5 ? `pax${this.ADDITIONAL}Operator` : `pax${paxReference}Operator`,
    };
  }


  private calculateValue(currentPremiseValue, value, operator) {
    switch (operator) {
      case Operators.sum:
        return currentPremiseValue + value;
      case Operators.minus:
        return currentPremiseValue - value;
      case Operators.fixed:
        return value;
      // TODO: remove after second stage of project
      // case Operators.multiple:
      //   return currentPremiseValue * value;
      // case Operators.divide:
      //   return currentPremiseValue / value;
      default:
        return value;
    }
  }

  private setRow(
    roomTypeWithBaseRateList,
    paxReference,
    propertyPremiseList,
    propertyParameterList
  ) {
    const { colReference } = this;
    const childCount = this.countChildren(propertyParameterList);


    if ( colReference ) {
      colReference.forEach( colRef => {
        const adultCapacity = this.getAdultCapacity(colRef, roomTypeWithBaseRateList);
        const finded = propertyPremiseList.find( item => item.roomTypeId === colRef.roomTypeId);

        if (finded) {
          for (let i = 1; i <= adultCapacity && i <= 5; i++) {
            if (i !== paxReference ) {
              const property = this.getPropertyNames(i);
              const value = this.calculateValue(
                colRef.ref,
                finded[property.paxValueStr],
                finded[property.paxOperatorStr]
              );

              finded[property.paxIndexStr] = value;
            }
          }

          if ( paxReference <= 5 && adultCapacity > 5) {
            const value = this.calculateValue(
              colRef.ref,
              finded[`paxAdditional`],
              finded[`paxAdditionalOperator`]
            );

            finded[`paxAdditionalValue`] = value;
          }

          for (let i = 1; i <= childCount; i++) {
            const value = this.calculateValue(
              colRef.ref,
              finded[`child${i}`],
              finded[`child${i}Operator`]
            );

            finded[`child${i}Amount`] = value;
          }
        }
      });
    }
  }

  private getAdultCapacity(row, roomTypeWithBaseRateList) {
    if ( row && roomTypeWithBaseRateList) {
      const finded = roomTypeWithBaseRateList.find( item => row.roomTypeId === item.id);

      if ( finded ) {
        return finded.adultCapacity;
      }

      return 0;
    }

    return 0;
  }

  private countChildren(propertyParameterList) {
    let count = 0;

    if ( propertyParameterList ) {
      count = propertyParameterList.reduce( (p, l) => l.isActive ? ++p : p, 0 );
    }

    return count;
  }

  public getBaseRateConfigurationRoomType(propertyPremiseList) {
    if ( propertyPremiseList ) {
      return propertyPremiseList.map( item => {
        return {
          name: item['name'] ? item['name'] : '',
          roomTypeId: this.getPropertyOrNull(item, 'roomTypeId'),
          pax1Amount: this.getPropertyOrNull(item, 'pax1Amount'),
          pax2Amount: this.getPropertyOrNull(item, 'pax2Amount'),
          pax3Amount: this.getPropertyOrNull(item, 'pax3Amount'),
          pax4Amount: this.getPropertyOrNull(item, 'pax4Amount'),
          pax5Amount: this.getPropertyOrNull(item, 'pax5Amount'),
          paxAdditionalAmount: this.getPropertyOrNull(item, 'paxAdditionalValue'),
          child1Amount: this.getPropertyOrNull(item, 'child1Amount'),
          child2Amount: this.getPropertyOrNull(item, 'child2Amount'),
          child3Amount: this.getPropertyOrNull(item, 'child3Amount')
        };
      });
    }

    return [];
  }

  public getPropertyOrNull(item, propertyName) {
    return item[propertyName]
      || ( (item[propertyName] !== undefined || item[propertyName] !== null ) && (+item[propertyName]) === 0)
        ? item[propertyName] : null;
  }
}
