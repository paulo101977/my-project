import { BaseRateConfigurationMealPlanType } from '@app/shared/models/revenue-management/meal-type-fix-rate';
import { RateLevelService } from 'app/revenue-management/rate-level/services/rate-level/rate-level.service';
import { Option } from 'app/shared/models/option';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing';

export const rateLevelProviderStub = createServiceStub(RateLevelService, {
  getAllLevelsAvailable (initialDate: string, endDate: string, currencyId: string):
    Observable<{ levelList: Option[]; levelRateMealPlanTypeList: BaseRateConfigurationMealPlanType[] }> {
    return of({levelList: [], levelRateMealPlanTypeList: []});
  },
  getLevelRateByDate (initialDate: string, endDate: string): Observable<any> {
    return of({levelList: [], mealPlanTypeList: []});
  }
});
