import {DaysEnum} from 'app/shared/models/days.enum';

export class BaseRateLevel {
  levelRateIdList: Array<string>;
  initialDate: string;
  endDate: string;
  daysOfWeekDto: Array<DaysEnum>;
}
