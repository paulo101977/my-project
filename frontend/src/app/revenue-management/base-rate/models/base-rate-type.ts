export enum BaseRateType {
  Amount = 1,
  Variation = 2,
  Premise = 3,
  Level = 4
}
