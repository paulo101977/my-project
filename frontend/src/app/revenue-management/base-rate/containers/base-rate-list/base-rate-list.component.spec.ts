import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { BaseRateListComponent } from './base-rate-list.component';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { configureTestSuite } from 'ng-bullet';
import { ActivatedRouteStub, imgCdnStub } from '../../../../../../testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThexApiModule, configureApi, ApiEnvironment } from '@inovacaocmnet/thx-bifrost';


describe('BaseRateListComponent', () => {
  let component: BaseRateListComponent;
  let fixture: ComponentFixture<BaseRateListComponent>;
  let listMealPlan;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(),
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        FormsModule,
        ThexApiModule
      ],
      declarations: [
        BaseRateListComponent,
        imgCdnStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ActivatedRouteStub,
          useValue: {
            snapshot: {
              params: {property: 1}
            }
          }
        },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ],
    });
    fixture = TestBed.createComponent(BaseRateListComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  describe('lifecycle', () => {

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should call init functions', () => {
      spyOn<any>(component, 'setButtonConfig');
      spyOn<any>(component, 'setForm');
      spyOn<any>(component, 'setHeaderConfig');
      spyOn<any>(component, 'setColumns');
      spyOn<any>(component, 'loadBaseRateList');
      spyOn<any>(component, 'loadMealPlanList');

      component.ngOnInit();

      expect(component['setButtonConfig']).toHaveBeenCalled();
      expect(component['setForm']).toHaveBeenCalled();
      expect(component.setHeaderConfig).toHaveBeenCalled();
      expect(component['setColumns']).toHaveBeenCalled();
      expect(component['loadBaseRateList']).toHaveBeenCalled();
      expect(component['loadMealPlanList']).toHaveBeenCalled();
    });

  });

  describe('mapping content', () => {

    it('should call mapper after receive request info', fakeAsync(() => {
      component.propertyId = 1;
      spyOn(component['rateHistoryResource'], 'gelAllByPropertyId').and.returnValue(of([]));
      spyOn(component['rateHistoryService'], 'headerHistoryMapper').and.returnValue([]);

      component['loadBaseRateList']();
      tick();
      expect(component['rateHistoryService'].headerHistoryMapper).toHaveBeenCalledWith([]);
    }));

    it('should convertObject after receive mealPlanRequest info', fakeAsync(() => {
      listMealPlan = [{mealPlanTypeId: 1, propertyMealPlanTypeName: 'algo'}];
      component.propertyId = 1;
      spyOn(component['commomService'], 'convertObjectToSelectOptionObjectByIdAndValueProperties').and.callFake(() => {
      });

      spyOn(component['mealPlanTypeResource'], 'getAllMealPlanTypeList').and.returnValue(of({
        hasNext: true,
        items: listMealPlan
      }));
      component['loadMealPlanList']();
      tick();
      expect(component['commomService'].convertObjectToSelectOptionObjectByIdAndValueProperties)
        .toHaveBeenCalledWith(listMealPlan, 'mealPlanTypeId', 'propertyMealPlanTypeName');
    }));

  });

  describe('filter', () => {

    it('should set filtered result', () => {
      component.baseRateHistoryOriginalList = [
        {
          initialDate: '2018-01-01T00:00:00',
          finalDate: '2018-01-10T00:00:00',
          mealPlanTypeDefaultId: 1
        },
        {
          initialDate: '2018-02-01T00:00:00',
          finalDate: '2018-02-10T00:00:00',
          mealPlanTypeDefaultId: 2
        }
      ];
      component.baseRateHistoryList = [...component.baseRateHistoryOriginalList];
      component.searchForm.patchValue({vigence: '2018-02-01T00:00:00', mealPlan: 2});


      component.filterHistory();

      expect(component.baseRateHistoryList.length).toEqual(1);
      expect(component.filterEnabled).toBe(true);
    });

    it('should clean filter', () => {
      component.baseRateHistoryOriginalList = [
        {
          initialDate: '2018-01-01T00:00:00',
          finalDate: '2018-01-10T00:00:00',
          mealPlanTypeDefaultId: 1
        },
        {
          initialDate: '2018-02-01T00:00:00',
          finalDate: '2018-02-10T00:00:00',
          mealPlanTypeDefaultId: 2
        }
      ];
      component.baseRateHistoryList = [];
      component.filterEnabled = true;

      component.cleanFilter();

      expect(component.baseRateHistoryList).toEqual(component.baseRateHistoryOriginalList);
      expect(component.filterEnabled).toEqual(false);
    });

  });

  describe('router', () => {

    it('should redirect to history', () => {
      spyOn<any>(component['router'], 'navigate').and.callFake(() => {});

      component.seeHistory('1');

      expect(component['router'].navigate).toHaveBeenCalledWith([
        '..', 'history', '1'],
        {relativeTo: component['route']}
      );
    });

    it('should redirect to new base Rate', () => {
      spyOn<any>(component['router'], 'navigate').and.callFake(() => {});

      component.goToNewBaseRate();

      expect(component['router'].navigate).toHaveBeenCalledWith(
        [ 'p', component.propertyId, 'rm', 'base-rate', 'new' ]
      );
    });

  });

});
