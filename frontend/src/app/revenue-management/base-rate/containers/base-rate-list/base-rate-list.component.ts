import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigHeaderPageNew } from '@app/shared/components/header-page-new/config-header-page-new';
import { RateHistoryResource } from '@app/revenue-management/rate-history/resources/rate-history.resource';
import { RateHistoryService } from '@app/revenue-management/rate-history/services/rate-history.service';
import { SharedService } from '@app/shared/services/shared/shared.service';
import { ButtonConfig, ButtonType } from '@app/shared/models/button-config';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MealPlanTypeResource } from '@app/shared/resources/meal-plan-type/meal-plan-type.resource';
import { MealPlanType } from '@app/shared/models/revenue-management/meal-plan-type';
import { CommomService } from '@app/shared/services/shared/commom.service';

@Component({
  selector: 'app-base-rate-list',
  templateUrl: './base-rate-list.component.html',
  styleUrls: ['./base-rate-list.component.css']
})
export class BaseRateListComponent implements OnInit {

  @ViewChild('apliedToCell') apliedToCell: TemplateRef<any>;
  @ViewChild('historyCell') historyCell: TemplateRef<any>;

  // Header
  public configHeaderPage: ConfigHeaderPageNew;

  // Table
  public columns: Array<any>;
  public baseRateHistoryOriginalList: Array<any>;
  public baseRateHistoryList: Array<any>;

  // Form
  public filterButtonConfig: ButtonConfig;
  public cleanButtonConfig: ButtonConfig;
  public searchForm: FormGroup;
  public mealPlanList: Array<MealPlanType>;
  public mealPlanOptions: Array<any>;

  // Info
  public propertyId: number;
  public filterEnabled: boolean;

  constructor(
    public  router: Router,
    private route: ActivatedRoute,
    private rateHistoryResource: RateHistoryResource,
    public  rateHistoryService: RateHistoryService,
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private mealPlanTypeResource: MealPlanTypeResource,
    private commomService: CommomService,
  ) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setButtonConfig();
    this.setForm();

    this.setHeaderConfig();
    this.setColumns();
    this.loadBaseRateList();
    this.loadMealPlanList();
  }

  private loadMealPlanList() {
    this.mealPlanTypeResource
      .getAllMealPlanTypeList(this.propertyId)
      .subscribe(result => {
        this.mealPlanList = result.items;
        this.mealPlanOptions = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
          this.mealPlanList,
          'mealPlanTypeId',
          'propertyMealPlanTypeName',
        );
      });
  }

  private loadBaseRateList() {
    this.rateHistoryResource.gelAllByPropertyId(this.propertyId).subscribe(result => {
      this.baseRateHistoryList = this.rateHistoryService.headerHistoryMapper(result);
      this.baseRateHistoryOriginalList = [...this.baseRateHistoryList];
    });
  }

  public filterHistory = () => {
    this.filterEnabled = true;
    let filteredList = [];
    if (this.searchForm.get('vigence').value) {
      const vigence = this.searchForm.get('vigence').value;
      filteredList = this.rateHistoryService.filterByVigence(vigence, this.baseRateHistoryOriginalList);
    } else {
      filteredList = this.baseRateHistoryOriginalList;
    }
    if (this.searchForm.get('mealPlan').value) {
      const mealPlanId = this.searchForm.get('mealPlan').value;
      filteredList = [...filteredList.filter( item => item.mealPlanTypeDefaultId == mealPlanId)];
    }

    this.baseRateHistoryList = [...filteredList];
  }

  public cleanFilter = () => {
    this.searchForm.reset();
    this.filterEnabled = false;
    this.baseRateHistoryList = [...this.baseRateHistoryOriginalList];
  }

  public seeHistory(historyItemId) {
    this.router.navigate(['..', 'history', historyItemId], {relativeTo: this.route});
  }

  public goToNewBaseRate = () => {
    this.router.navigate([
      'p',
      this.propertyId,
      'rm',
      'base-rate',
      'new',
    ]);
  }

  // Config
  public setHeaderConfig() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
      keepTitleButton: true,
      callBackFunction: this.goToNewBaseRate,
    };
  }

  private setColumns() {
    this.columns = [
      {name: 'label.vigence', prop: 'vigence'},
      {name: 'label.currency', prop: 'currencyName', flexGrow: 0.3},
      {name: 'label.defaultMealPlan', prop: 'mealPlanTypeDefaultName'},
      {name: 'label.apliedTo', prop: 'appliedTo', cellTemplate: this.apliedToCell},
      {name: 'label.seeHistory', prop: 'id', cellTemplate: this.historyCell, headerClass: 'thf-u-text-right'},
    ];
  }

  private setButtonConfig() {
    this.filterButtonConfig = this.sharedService.getButtonConfig(
      'filterButton',
      this.filterHistory,
      'label.search',
      ButtonType.Primary
    );
    this.filterButtonConfig.extraClass = 'thf-u-margin-top--3';

    this.cleanButtonConfig = this.sharedService.getButtonConfig(
      'cleanButton',
      this.cleanFilter,
      'commomData.clean',
      ButtonType.Secondary
    );
    this.cleanButtonConfig.extraClass = 'thf-u-margin-top--3';
  }

  private setForm() {
    this.searchForm = this.formBuilder.group({
      vigence: [null],
      mealPlan: [null]
    });
  }

}
