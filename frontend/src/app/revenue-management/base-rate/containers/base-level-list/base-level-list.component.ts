import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BaseLevelRate } from 'app/revenue-management/rate-level/models/base-level-rate';
import { LevelRate } from 'app/revenue-management/rate-level/models/level-rate';
import { LevelRateCurrency } from 'app/revenue-management/rate-level/models/level-rate-currency';
import { RateLevelService } from 'app/revenue-management/rate-level/services/rate-level/rate-level.service';
import { Option } from 'app/shared/models/option';
import { Parameter } from 'app/shared/models/parameter';
import { RoomTypeParametersConfiguration } from 'app/shared/models/revenue-management/room-type-parameters-configuration';
import { CommomService } from 'app/shared/services/shared/commom.service';
import * as moment from 'moment';

@Component({
  selector: 'app-base-level-list',
  templateUrl: './base-level-list.component.html',
  styleUrls: ['./base-level-list.component.scss']
})
export class BaseLevelListComponent implements OnInit, OnChanges {

  @Input() initialDate: string;
  @Input() lastDate: string;
  @Input() currencyId: string;
  @Input() propertyParameterList: Array<Parameter>;
  @Input() roomTypeList: Array<RoomTypeParametersConfiguration>;
  @Output() isvalid = new EventEmitter();
  @Output() levelRateIdList = new EventEmitter();
  @Output() weekSelected = new EventEmitter();
  @Output() dateOutside = new EventEmitter();

  @ViewChild('mealDefault') mealDefault: TemplateRef<any>;
  public baseLevelRate: Array<BaseLevelRate>;
  public levelRateForm: FormGroup;
  public levelOptionsList: Option[];
  public viewLevelRate = false;
  public viewConfigLevel = false;
  public columnsLevelRate: Array<any>;
  public columnsMealPlan: Array<any>;
  public itemsLevelRate: Array<any> = [];
  public itemsMealPlan: Array<any> = [];
  public levelRateVigence: Array<LevelRate>;
  public ageRange: string;
  public ageRange1: string;
  public ageRange2: string;
  public isCurrencyDisable = true;
  public currencyList: Option[];
  public levelRateCurrencyList: Array<LevelRateCurrency>;
  public dateListOutside: string;

  constructor(
    private formBuilder: FormBuilder,
    private rateLevelService: RateLevelService,
    private commomService: CommomService,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
    this.setForm();
    this.setRangeChild();
    this.setColumn();
  }

  ngOnChanges (changes: SimpleChanges): void {
    if (this.initialDate && this.lastDate) {
      this.loadData();
    } else {
      this.isvalid.emit(false);
    }
  }

  public setRangeChild() {
    if (this.propertyParameterList) {
      const [firstAge, secondAge, thirdAge] = this.propertyParameterList
        .map((age) => parseInt(age.propertyParameterValue, 10));

      this.ageRange = `0 - ${firstAge}`;
      this.ageRange1 = `${firstAge + 1} - ${secondAge}`;
      this.ageRange2 = `${secondAge + 1} - ${thirdAge}`;
    }
  }

  public setForm() {
    this.levelRateForm = this.formBuilder.group({
      levelRate: [null, Validators.required],
      daysOfWeek: [[], Validators.required],
      rateCurrency: [null, Validators.required]
    });

    this.levelRateForm.get('levelRate').valueChanges.subscribe(item => {
      this.getCurrencyByLevel(item);
      if (this.levelRateForm.get('rateCurrency').value) {
        this.loadLevelRateList();
      }
    });
    this.levelRateForm.get('rateCurrency').valueChanges.subscribe(item => {
      if (item && this.levelRateForm.get('levelRate').value) {
        this.loadLevelRateVigence(item);
      }
    });
  }

  public weekDaysSelected(weekDays) {
    this.weekSelected.emit(weekDays);
    this.levelRateForm.get('daysOfWeek').setValue(weekDays);
  }

  public loadData() {
    this.rateLevelService.getLevelRateByDate(this.initialDate, this.lastDate).subscribe( ({items}) => {
      this.baseLevelRate = items;

      if (this.baseLevelRate && this.baseLevelRate.length > 0) {
        this.levelOptionsList = this
          .commomService
          .toOption(this.baseLevelRate, 'levelId', 'levelName');
        this.viewConfigLevel = true;
      } else {
        this.clearScreen();
      }
    }, error => {
      this.clearScreen();
    });
  }

  public clearScreen() {
    this.levelOptionsList = null;
    this.viewConfigLevel = false;
    this.viewLevelRate = false;
    this.isvalid.emit(false);
  }

  public getCurrencyByLevel(id: string) {
    const levelRateCurrency = this.baseLevelRate.find(item => item.levelId === id);

    if (levelRateCurrency && levelRateCurrency.levelRateCurrencyList) {
      this.levelRateCurrencyList = levelRateCurrency.levelRateCurrencyList;
      this.currencyList = this
        .commomService
        .toOption(levelRateCurrency.levelRateCurrencyList, 'currencyId', 'currencyName');
      this.isCurrencyDisable = false;
      this.levelRateForm.get('rateCurrency').setValue(null);
    } else {
      this.isCurrencyDisable = true;
      this.currencyList = null;
    }
  }

  public loadLevelRateVigence(currencyId: string) {
    const {levelRateList, dateListOutsideRateList} = this.levelRateCurrencyList.find(item => item.currencyId === currencyId);

    if (levelRateList) {
      this.levelRateVigence = levelRateList;
      this.loadLevelRateList();
      this.viewLevelRate = true;
      this.isvalid.emit(true);
      this.setLevelRateIdSelected(levelRateList);
    } else {
      this.isvalid.emit(false);
    }
    if (dateListOutsideRateList && dateListOutsideRateList.length > 0) {
      dateListOutsideRateList.map( dates => {
        const date = moment(dates).format('L');
        this.dateListOutside = this.dateListOutside
          ? this.dateListOutside + ', ' + date
          : date;
      });
      this.dateOutside.emit(this.dateListOutside);
    }
  }

  public setLevelRateIdSelected(levelRateList) {
    let levelList = Array<string>();
    if (levelRateList && levelRateList.length > 0) {
      levelList = levelRateList.map(level =>
        level.levelRateHeader.id
      );
      this.levelRateIdList.emit(levelList);
    }
  }

  public loadLevelRateList() {
    // TODO: SERÁ IMPLEMENTADO DEPOIS UMA COMBO PARA SELECIONAR A TARIFA
    const selectedRate = this.levelRateVigence[0];

    if (selectedRate.levelRateList && selectedRate.levelRateList.length > 0) {
      this.itemsLevelRate = selectedRate.levelRateList.map(level => {
        const roomType = this.roomTypeList.find(item => item.id === level.roomTypeId);

        return {
          'abbreviation': roomType.abbreviation,
          ...level
        };
      });
    }

    this.itemsMealPlan = selectedRate.levelRateMealPlanTypeList;
  }

  public setColumn() {
    this.columnsLevelRate = [
      {
        name: 'label.typeUHComplete',
        prop: 'abbreviation'
      },
      {
        name: 'label.paxOne',
        prop: 'pax1Amount'
      },
      {
        name: 'label.paxTwo',
        prop: 'pax2Amount'
      },
      {
        name: 'label.paxThree',
        prop: 'pax3Amount'
      },
      {
        name: 'label.paxFour',
        prop: 'pax4Amount'
      },
      {
        name: 'label.paxFive',
        prop: 'pax5Amount'
      },
      {
        name: 'label.paxAdditional',
        prop: 'paxAdditionalAmount'
      },
    ];

    this.columnsMealPlan = [
      {
        name: 'label.defaultMealPlan',
        cellTemplate: this.mealDefault,
        sortable: false,
        flexGrow: 0.9,
      },
      {
        name: 'label.mealType',
        prop: 'mealPlanTypeName'
      },
      {
        name: 'label.adult',
        prop: 'adultMealPlanAmount'
      }
    ];

    // TODO: Create child template
    if (this.propertyParameterList && this.propertyParameterList[0].isActive) {
      const child = {
        name: this.translateService.instant('variable.ageChildRange', {age: this.ageRange}),
        headerTemplate:  undefined,
        flexGrow: 0.9,
        cssClass: ''
      };

      this.columnsLevelRate.push(
        {
          prop: 'child1Amount',
          ...child
        }
      );

      this.columnsMealPlan.push({
          prop: 'child1MealPlanAmount',
          ...child
        }
      );
    }
    if (this.propertyParameterList && this.propertyParameterList[1].isActive) {
      const child1 = {
        name: this.translateService.instant('variable.ageChildRange', {age: this.ageRange1}),
        headerTemplate: undefined,
        flexGrow: 0.9
      };
      this.columnsLevelRate.push(
        {
          prop: 'child2Amount',
          ...child1
        }
      );
      this.columnsMealPlan.push(
        {
          prop: 'child2MealPlanAmount',
          ...child1
        }
      );
    }
    if (this.propertyParameterList && this.propertyParameterList[2].isActive) {
      const child2 =    {
        name: this.translateService.instant('variable.ageChildRange', {age: this.ageRange2}),
        headerTemplate: undefined,
        flexGrow: 0.9
      };

      this.columnsLevelRate.push(
        {
          prop: 'child3Amount',
          ...child2
        }
      );
      this.columnsMealPlan.push(
        {
          prop: 'child3MealPlanAmount',
          ...child2
        }
      );
    }
  }

  public reset() {
    if (this.levelRateForm.pristine) {
      return;
    }

    this.levelRateForm.reset();
    this.itemsLevelRate = [];
    this.itemsMealPlan = [];
    this.viewLevelRate = false;
  }
}
