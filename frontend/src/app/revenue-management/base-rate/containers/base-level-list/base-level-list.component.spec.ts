import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BaseLevelListComponent } from './base-level-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NO_ERRORS_SCHEMA, SimpleChange, SimpleChanges} from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { RateLevelService } from 'app/revenue-management/rate-level/services/rate-level/rate-level.service';
import { configureTestSuite } from 'ng-bullet';
import { rateLevelProviderStub} from 'app/revenue-management/base-rate/services/testing';
import {SharedModule} from 'app/shared/shared.module';
import {commomServiceStub} from 'app/shared/services/shared/testing/common-service';
import { DATA, PROPERTY_PARAMETER_LIST, BASE_LEVEL_RATE} from 'app/revenue-management/base-rate/mock-data';
import {of} from 'rxjs/internal/observable/of';

describe('BaseLevelListComponent', () => {
  let component: BaseLevelListComponent;
  let fixture: ComponentFixture<BaseLevelListComponent>;
  let rateLevelService: RateLevelService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        BaseLevelListComponent,
      ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
      ],
      providers: [
        commomServiceStub,
        rateLevelProviderStub
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    rateLevelService = TestBed.get(RateLevelService);
    fixture = TestBed.createComponent(BaseLevelListComponent);
    component = fixture.componentInstance;

    component.initialDate = '2019-08-01T00:00:00';
    component.lastDate = '2019-08-14T00:00:00';
    component.propertyParameterList = DATA[PROPERTY_PARAMETER_LIST];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn(component, 'setForm');
    spyOn(component, 'setRangeChild');
    spyOn(component, 'setColumn');
    component.ngOnInit();

    expect(component.setForm).toHaveBeenCalled();
    expect(component.setRangeChild).toHaveBeenCalled();
    expect(component.setColumn).toHaveBeenCalled();
  });

  it('should ngOnChanges', () => {
    spyOn(component, 'loadData');
    const changes: SimpleChanges = {
      initialDate: new SimpleChange({}, component.initialDate, true),
      lastDate: new SimpleChange({}, component.lastDate, true)
    };
    component.ngOnChanges(changes);
    expect(component.loadData).toHaveBeenCalled();
  });

  it('should setRangeChild', () => {
    component.setRangeChild();
    expect(component.ageRange).toEqual('0 - 6');
  });

  it('should loadData', () => {
    spyOn(component['rateLevelService'], 'getLevelRateByDate').and.returnValue(of({ items: DATA[BASE_LEVEL_RATE]} ));
    component.loadData();
    expect(component.baseLevelRate).toEqual(DATA[BASE_LEVEL_RATE]);
    expect(component.viewConfigLevel).toEqual(true);
  });

  it('should getCurrencyByLevel', () => {
    component.getCurrencyByLevel('54d8e57d-db36-456e-a437-058006790952');

    expect(component.levelRateCurrencyList[0]).toEqual(DATA[BASE_LEVEL_RATE][0].levelRateCurrencyList[0]);
    expect(component.isCurrencyDisable).toEqual(false);
  });

  it('should loadLevelRateVigence', () => {
    spyOn<any>(component, 'loadLevelRateList').and.callFake(f => f);
    component.loadLevelRateVigence( '779d25a8-135f-496b-a2c9-7d5734c6d38c');

    expect(component.viewLevelRate).toEqual(true);
    expect(component.loadLevelRateList).toHaveBeenCalled();
    expect(component.levelRateVigence).toEqual(DATA[BASE_LEVEL_RATE][0].levelRateCurrencyList[0].levelRateList);
  });

  describe('#reset', () => {
    it('should not run when the levelRateForm is pristine', () => {
      component.itemsLevelRate = [2];
      component.itemsMealPlan = [2];
      component.viewLevelRate = true;

      component.levelRateForm.reset();
      expect(component.levelRateForm.pristine).toBe(true);

      component.reset();

      expect(component.itemsLevelRate.length).toBe(1);
      expect(component.itemsMealPlan.length).toBe(1);
      expect(component.viewLevelRate).toBe(true);
    });

    it('should not run when the levelRateForm is pristine', () => {
      component.itemsLevelRate = [2];
      component.itemsMealPlan = [2];
      component.viewLevelRate = true;

      component.levelRateForm.markAsDirty();
      expect(component.levelRateForm.pristine).toBe(false);

      component.reset();

      expect(component.levelRateForm.pristine).toBe(true);
      expect(component.itemsLevelRate.length).toBe(0);
      expect(component.itemsMealPlan.length).toBe(0);
      expect(component.viewLevelRate).toBe(false);
    });
  });
});
