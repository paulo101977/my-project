import { BaseRateConfigurationMealPlanType } from 'app/shared/models/revenue-management/meal-type-fix-rate';
import {Parameter} from 'app/shared/models/parameter';
import {Option} from 'app/shared/models/option';
import {BaseLevelRate} from 'app/revenue-management/rate-level/models/base-level-rate';
import {LevelRateCurrency} from 'app/revenue-management/rate-level/models/level-rate-currency';

export const BASE_RATE_CONFIG_MEAL_PLAN_TYPE = 'BASE_RATE_CONFIG_MEAL_PLAN_TYPE';
export const PROPERTY_PARAMETER_LIST = 'PROPERTY_PARAMETER_LIST';
export const BASE_LEVEL_RATE = 'BASE_LEVEL_RATE';
export const BASE_LEVEL_OPTIONS = 'BASE_LEVEL_OPTIONS';

export class DATA {
  public static readonly BASE_RATE_CONFIG_MEAL_PLAN_TYPE = <BaseRateConfigurationMealPlanType[]>[
    {
      id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
      isMealPlanTypeDefault: false,
      mealPlanTypeName: 'Meia Pensão',
      mealPlanTypeId: 3,
      adultMealPlanAmount: 50,
      child1MealPlanAmount: 50,
      child2MealPlanAmount: 50,
      child3MealPlanAmount: 50
    },
    {
      id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
      isMealPlanTypeDefault: true,
      mealPlanTypeName: 'Pensão Completa',
      mealPlanTypeId: 6,
      adultMealPlanAmount: 100,
      child1MealPlanAmount: 100,
      child2MealPlanAmount: 100,
      child3MealPlanAmount: 100
    },
    {
      id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
      isMealPlanTypeDefault: false,
      mealPlanTypeName: 'Meia Pensão Jantar',
      mealPlanTypeId: 5,
      adultMealPlanAmount: 50,
      child1MealPlanAmount: 50,
      child2MealPlanAmount: 50,
      child3MealPlanAmount: 50
    },
    {
      id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
      isMealPlanTypeDefault: false,
      mealPlanTypeName: 'Café da Manhã',
      mealPlanTypeId: 2,
      adultMealPlanAmount: 50,
      child1MealPlanAmount: 50,
      child2MealPlanAmount: 50,
      child3MealPlanAmount: 50
    },
    {
      id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
      isMealPlanTypeDefault: false,
      mealPlanTypeName: 'Meia Pensão Almoço',
      mealPlanTypeId: 4,
      adultMealPlanAmount: 50,
      child1MealPlanAmount: 50,
      child2MealPlanAmount: 50,
      child3MealPlanAmount: 50
    },
    {
      id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
      isMealPlanTypeDefault: false,
      mealPlanTypeName: 'Nenhuma',
      mealPlanTypeId: 1,
      adultMealPlanAmount: 0,
      child1MealPlanAmount: 0,
      child2MealPlanAmount: 0,
      child3MealPlanAmount: 0
    },
    {
      id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
      isMealPlanTypeDefault: false,
      mealPlanTypeName: 'Tudo Incluso',
      mealPlanTypeId: 7,
      adultMealPlanAmount: 100,
      child1MealPlanAmount: 100,
      child2MealPlanAmount: 100,
      child3MealPlanAmount: 100
    }
  ];

  public static readonly PROPERTY_PARAMETER_LIST = <Parameter[]> [
    {
      applicationParameterId: 7,
      featureGroupId: 3,
      id: 'd4237c24-d20c-4adb-acda-a354fba5722a',
      isActive: true,
      parameterDescription: 'Criança 1',
      parameterTypeName: 'Number',
      propertyId: 11,
      propertyParameterMaxValue: '9',
      propertyParameterMinValue: '0',
      propertyParameterValue: '6'
    },
    {
      applicationParameterId: 8,
      featureGroupId: 3,
      id: 'ad6c8266-8766-4f19-bbf3-7f078af8ca81',
      isActive: true,
      parameterDescription: 'Criança 2',
      parameterTypeName: 'Number',
      propertyId: 11,
      propertyParameterMaxValue: '8',
      propertyParameterMinValue: '7',
      propertyParameterValue: '8'
    },
    {
      applicationParameterId: 9,
      featureGroupId: 3,
      id: '77549693-60b5-423c-9738-829719c860c6',
      isActive: true,
      parameterDescription: 'Criança 3',
      parameterTypeName: 'Number',
      propertyId: 11,
      propertyParameterMaxValue: '17',
      propertyParameterMinValue: '9',
      propertyParameterValue: '10'
    }
  ];

  public static readonly BASE_LEVEL_RATE = <BaseLevelRate[]> [
    {
      levelId: '54d8e57d-db36-456e-a437-058006790952',
      levelName: 'Nível 1',
      levelRateCurrencyList: [
       {
         currencyId: '779d25a8-135f-496b-a2c9-7d5734c6d38c',
         currencyName: 'Dólar',
         levelRateList: [
             {
               levelRateHeader: {
                 currencyId: '00000000-0000-0000-0000-000000000000',
                 endDate: '2019-08-04T00:00:00',
                 id: '84075f3f-44bc-4def-9ca4-3d8746e186f1',
                 initialDate: '2019-08-01T00:00:00',
                 isActive: true,
                 levelId: '9f42ba2b-8ee7-4a13-9135-627b244d3929',
                 levelName: 'Nível 2',
                 propertyMealPlanTypeRateHeaderId: 'fc29d4f8-f3ed-495b-9668-07765f240a2f',
                 rateName: 'tarifa de teste'
               },
               levelRateList: null
           }
         ]
       }
     ]
    },
    {
      levelId: '9f42ba2b-8ee7-4a13-9135-627b244d3929',
      levelName: 'Nível 2'
    }
  ];

  public static readonly BASE_LEVEL_OPTIONS = <Option[]> [
    {
      key: '54d8e57d-db36-456e-a437-058006790952',
      value: 'Nível 1'
    },
    {
      key: '9f42ba2b-8ee7-4a13-9135-627b244d3929',
      value: 'Nível 2'
    }
  ];
}
