import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseRateComponent } from './components/base-rate/base-rate.component';
import { BaseRateListComponent } from './containers/base-rate-list/base-rate-list.component';

export const propertyBaseRoutes: Routes = [
  { path: '', component: BaseRateComponent },
  { path: 'new', component: BaseRateComponent },
  { path: 'list', component: BaseRateListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(propertyBaseRoutes)],
  exports: [RouterModule],
})
export class BaseRateRoutingModule {}
