import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { RoomTypeParametersConfiguration } from '../../../../shared/models/revenue-management/room-type-parameters-configuration';
import { Parameter } from '../../../../shared/models/parameter';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { BaseRateService } from '../../../../shared/services/base-rate/base-rate.service';
import { BaseRateMapper } from '../../../../shared/mappers/base-rate-mapper';

@Component({
  selector: 'base-rate-table',
  templateUrl: 'base-rate-table.component.html',
})
export class BaseRateTableComponent implements OnInit, OnChanges {
  @Output() roomTypeWithBaseRateUpdatedList = new EventEmitter();
  @Input() roomTypeWithBaseRateList: Array<RoomTypeParametersConfiguration>;
  @Input() propertyParameterList: Array<Parameter>;
  @Input() footerHeight = 50;

  public rows: Array<RoomTypeParametersConfiguration>;
  public ageRange1: number;
  public ageRange2: number;
  public optionsCurrencyMask: any;
  public allItemsAreChecked: boolean;

  constructor(private sharedService: SharedService, private baseRateService: BaseRateService, private baseRateMapper: BaseRateMapper) {}

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes: any) {
    if (this.roomTypeWithBaseRateList || this.rows) {
      this.rows = [...this.roomTypeWithBaseRateList];
    }
  }

  setVars() {
    this.allItemsAreChecked = false;
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
    this.ageRange1 = parseInt(this.propertyParameterList[0].propertyParameterValue, 10) + 1;
    this.ageRange2 = parseInt(this.propertyParameterList[1].propertyParameterValue, 10) + 1;

    if (this.roomTypeWithBaseRateList) {
      this.roomTypeWithBaseRateList.forEach(roomTypeWithBaseRate => {
        roomTypeWithBaseRate.paxLimitInputs = this.sharedService.createRange(roomTypeWithBaseRate.adultCapacity);
        roomTypeWithBaseRate.childLimitInputs = this.sharedService.createRange(roomTypeWithBaseRate.childCapacity);
      });
    }
  }

  public updateBaseRate() {
    const roomTypeListWithBaseRateValid = {
      roomTypeList: this.baseRateService.hasBaseRateConfigWithValue(this.rows),
      validBaseRate: this.baseRateService.getBaseRateValueListIsValid(this.baseRateMapper.getBaseRateTableListModel(this.rows)),
    };
    this.roomTypeWithBaseRateUpdatedList.emit(roomTypeListWithBaseRateValid);
  }

  public getRowClass(row: RoomTypeParametersConfiguration) {
    return { 'thf-u-background-color--blue-very-soft': row.isChecked };
  }

}
