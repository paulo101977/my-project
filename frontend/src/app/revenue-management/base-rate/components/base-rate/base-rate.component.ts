import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { BaseLevelListComponent } from 'app/revenue-management/base-rate/containers/base-level-list/base-level-list.component';
import { DateService } from 'app/shared/services/shared/date.service';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { BaseRateService } from 'app/shared/services/base-rate/base-rate.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { BaseRateMapper } from 'app/shared/mappers/base-rate-mapper';
import { BaseRateResource } from 'app/shared/resources/base-rate/base-rate.resource';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { RightButtonTop } from 'app/shared/models/right-button-top';
import { MealPlanType } from 'app/shared/models/revenue-management/meal-plan-type';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { CurrencyBaseRateConfiguration } from 'app/shared/models/revenue-management/currency-base-rate';
import { BaseRateTable, PropertyRate } from 'app/shared/models/revenue-management/property-base-rate';
import { Parameter } from 'app/shared/models/parameter';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { ButtonSize } from 'app/shared/models/button-config';
import { RoomTypeParametersConfiguration } from 'app/shared/models/revenue-management/room-type-parameters-configuration';
import { ConfirmModalDataConfig } from 'app/shared/models/confirm-modal-data-config';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import * as moment from 'moment';
import { WeekDayCheckListComponent } from 'app/shared/components/week-day-check-list/week-day-check-list.component';
import { BaseRateConfigurationMealPlanType } from 'app/shared/models/revenue-management/meal-type-fix-rate';
import { RatePlanResource } from 'app/shared/resources/rate-plan/rate-plan.resource';
import { RatePlanDto } from 'app/shared/models/dto/rate-plan/rate-plan-dto';
import { MealPlanTypeEnum } from 'app/shared/models/revenue-management/meal-plan-type.enum';
import { AssetsService } from '@app/core/services/assets.service';
import { ButtonBarConfig } from 'app/shared/components/button-bar/button-bar-config';
import { ButtonBarColor } from 'app/shared/components/button-bar/button-bar-color.enum';
import { ButtonBarButtonStatus } from 'app/shared/components/button-bar/button-bar-button-status.enum';
import { PremiseResource } from 'app/revenue-management/premises/resources/premise.resource';
import { PropertyPremiseHeader } from 'app/revenue-management/premises/models/property-premise-header';
import { PremiseRateService } from 'app/revenue-management/base-rate/services/premise-rate.service';
import {BaseRateType} from 'app/revenue-management/base-rate/models/base-rate-type';
import {BaseRateLevel} from 'app/revenue-management/base-rate/models/base-rate-level';
import { merge } from 'rxjs';

@Component({
  selector: 'base-rate',
  templateUrl: 'base-rate.component.html',
  styleUrls: ['base-rate.component.css'],
})
export class BaseRateComponent implements OnInit {

  @ViewChild(WeekDayCheckListComponent)
  private weekDayComponent: WeekDayCheckListComponent;
  @ViewChild('baseRateLevel') baseRateLevel: BaseLevelListComponent;

  public readonly MIN_PREMISE_VALUE = 0.01;
  public paymentTypeForm: FormGroup;
  public premiseForm: FormGroup;
  public mealTypeList: Array<MealPlanType>;
  public optionsRateCurrencyList: Array<CurrencyBaseRateConfiguration>;
  public mealTypeOptionListToSelect: Array<SelectObjectOption>;
  public currencyOptionListToSelect: Array<SelectObjectOption>;
  public dateInvalid: boolean;
  public useMealTypeListDefault: boolean;
  public roomTypeWithBaseRateList: Array<RoomTypeParametersConfiguration>;
  private propertyRate: PropertyRate;
  public propertyParameterList: Array<Parameter>;
  private baseRateTableList: Array<BaseRateTable>;
  public confirmModalDataConfig: ConfirmModalDataConfig;
  public showConfirmModal: boolean;
  private propertyId: number;
  public dateInvalidPassedPeriodAllowed: boolean;
  public numberOfDays: number;
  public textInvalidPeriod: string;
  private returnUrl: string;
  private readonly PERIOD_ALLOWED_DAYS = 1000;
  private ratePlanId: any;
  private mealPlanId: any;
  private ratePlan: RatePlanDto;
  private roomTypeWithBaseRateListValid: boolean;
  public premiseList: Array<any>;
  public isValue = true;
  public isPremise = false;
  public isLevel = false;
  public hasTablePremise: boolean;
  public selectedPremise: PropertyPremiseHeader;
  public currentPremise: PropertyPremiseHeader;
  public propertyPremiseHeaderList: Array<PropertyPremiseHeader>;
  public currentPremiseValue: string;
  public levelIdList: Array<string>;
  public dateOutsideList: string;

  // HeaderComponent config
  public configHeaderPage: ConfigHeaderPageNew;
  public topButtonsListConfig: Array<RightButtonTop>;
  public title: string;

  // Buttons
  public cancelButtonConfig: ButtonConfig;
  public saveButtonConfig: ButtonConfig;
  public premiseApplyButton: ButtonConfig;

  // App Button Bar
  public buttonBarConfig: ButtonBarConfig;


  constructor(
    public router: Router,
    private activeRouter: ActivatedRoute,
    public route: ActivatedRoute,
    public formBuilder: FormBuilder,
    private dateService: DateService,
    private baseRateResource: BaseRateResource,
    private toasterEmitService: ToasterEmitService,
    private commomService: CommomService,
    private sharedService: SharedService,
    private location: Location,
    private baseRateMapper: BaseRateMapper,
    private baseRateService: BaseRateService,
    private translateService: TranslateService,
    private modalEmitToggleService: ModalEmitToggleService,
    private sessionParameterService: SessionParameterService,
    private ratePlanResource: RatePlanResource,
    private assetsService: AssetsService,
    private premiseResource: PremiseResource,
    private premiseRateService: PremiseRateService,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.setVars();
  }

  public getPremiseList(): void {
    let startDate = this.paymentTypeForm.get('initialDate').value || '';
    let lastDate = this.paymentTypeForm.get('lastDate').value || '';
    if (startDate == '' && lastDate == '') { return; }

    if (startDate != '') {
      startDate = startDate.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$3-$2-$1');
    }

    if (lastDate != '') {
      lastDate = lastDate.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$3-$2-$1');
    }

    this
      .premiseResource
      .getPropertyPremiseHeaderByDate(startDate, lastDate, this.propertyRate.currencyId)
      .subscribe(({ items }) => {
        if ( items ) {

          this.propertyPremiseHeaderList = <Array<PropertyPremiseHeader>>items;

          // Array.filter return [] or not empty array
          const itemsList = items.filter( item => item.isActive );
          this.premiseList = this
            .commomService
            .toOption(itemsList, 'id', 'name');
          if ( itemsList.length === 1) {
            this.currentPremise = itemsList[0];
            this.premiseForm.removeControl('premiseId');
          }
        }
      });
  }

  public changeView(view: string) {
    switch (view) {
      case 'isPremise': {
        this.isPremise = true;
        this.isLevel = false;
        this.isValue = false;
        break;
      }
      case 'isLevel': {
        this.isLevel = true;
        this.isPremise = false;
        this.isValue = false;
        break;
      }
      default: {
        this.isValue = true;
        this.isPremise = false;
        this.isLevel = false;
        break;
      }
    }
  }

  public validSaveButton(valid) {
    this.saveButtonConfig.isDisabled = !valid;
  }

  public setConfigButtonBar() {
    this.buttonBarConfig = {
      color: ButtonBarColor.blue,
      buttons: [
        {
          title: 'text.value',
          status: ButtonBarButtonStatus.active,
          callBackFunction: () => this.changeView(''),
          cssClass: 'button-bar-size'
        },
        {
          title: 'text.premise',
          callBackFunction: () => this.changeView('isPremise'),
          cssClass: 'button-bar-size'
        },
        {
          title: 'label.level',
          callBackFunction: () => this.changeView('isLevel'),
          cssClass: 'button-bar-size'
        },
      ],
    };
  }

  public setVars() {
    this.route.params.subscribe(async params => {
      this.propertyId = +params.property;

      this.showConfirmModal = false;
      this.propertyRate = new PropertyRate();
      this.propertyRate.propertyId = this.route.snapshot.params.property;
      this.propertyRate.baseRateConfigurationRoomTypeList = [];
      this.propertyRate.baseRateConfigurationMealPlanTypeList = [];
      this.confirmModalDataConfig = new ConfirmModalDataConfig();
      this.returnUrl = this.route.snapshot.queryParams.returnUrl;
      this.ratePlanId = this.route.snapshot.queryParams.ratePlanId;
      this.mealPlanId = this.route.snapshot.queryParams.mealPlanId;
      this.setConfigButtonBar();
      this.setPremiseForm();

      if (this.isFixRate()) {
        this.setRatePlan();
        this.setReturnUrl();
      }

      this.setConfigHeaderPage();
      this.setTopButtonsListConfig();
      this.setButtonConfig();
      this.setPaymentForm();
      await this.getBaseRateConfiguration();
      this.getPremiseList();
    });
  }

  private setPaymentForm() {
    if (this.isFixRate()) {
      this.setMealType(this.mealPlanId);
    }

    if (this.paymentTypeForm) {
      this.paymentTypeForm.get('initialDate').setValue(null);
      this.paymentTypeForm.get('lastDate').setValue(null);
      this.paymentTypeForm.get('rateCurrencySelect.rateCurrency').setValue('');
      this.paymentTypeForm.get('daysOfWeek').setValue([]);
    } else {
      this.paymentTypeForm = this.formBuilder.group({
        initialDate: [null, [Validators.required]],
        lastDate: [null, [Validators.required]],
        rateCurrencySelect: this.formBuilder.group({
          rateCurrency: ['', [Validators.required]],
        }),
        daysOfWeek: [[], Validators.required],
      });
    }
    this.setListener();
    this.setInitialDatePicker();
  }

  public setPremiseForm() {
    this.premiseForm = this.formBuilder.group({
      premiseId: [ null, Validators.required ],
      premiseAmount: [0,  [Validators.required, Validators.min(this.MIN_PREMISE_VALUE)] ],
    });

    this
      .premiseForm
      .valueChanges
      .subscribe( (values) => {
        const {
          propertyPremiseHeaderList,
        } = this;

        this.premiseApplyButton.isDisabled = true;

        if ( values.premiseId && propertyPremiseHeaderList ) {
          this.currentPremise = propertyPremiseHeaderList
            .find( item => item.id === values.premiseId);
        }

        if (
          this.premiseForm.valid
          && values.premiseAmount
        ) {
          this.premiseApplyButton.isDisabled = false;
          this.cdRef.detectChanges();
        }

        this.checkPremiseRate();
      });
  }

  private setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      () => this.goBackPreviewPage(),
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.saveButtonConfig = this.sharedService.getButtonConfig(
      'save',
      () => this.saveBaseRateToRoomTypeByDay(),
      'action.save',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );

    this.premiseApplyButton = this.sharedService.getButtonConfig(
      'premise',
      this.premiseApply,
      'action.apply',
      ButtonType.Secondary,
      ButtonSize.Normal,
      true
    );
  }

  public premiseApply = () => {
    const { propertyParameterList, roomTypeWithBaseRateList } = this;
    this.currentPremiseValue = this.premiseForm.get('premiseAmount').value;

    if ( this.currentPremise ) {
      this.hasTablePremise = true;
      // deep clone
      this.selectedPremise = JSON.parse(JSON.stringify(this.currentPremise));

      const { propertyPremiseList, id } = this.selectedPremise;
      this
        .premiseRateService
        .setPremiseValues(
          propertyParameterList,
          roomTypeWithBaseRateList,
          this.selectedPremise,
          propertyPremiseList,
          this.currentPremiseValue,
          id
        );

      this.checkPremiseRate();
    }
  }

  private setOptionsMealTypeList(mealPlanTypeList: any) {
    this.mealTypeList = [...mealPlanTypeList.items];
    for (const mealType of this.mealTypeList) {
      mealType.propertyMealPlanTypeName = mealType.propertyMealPlanTypeName || mealType.mealPlanTypeName;
    }
    this.mealTypeOptionListToSelect = new Array<SelectObjectOption>();
    this.mealTypeOptionListToSelect = this.commomService.toOption(
      mealPlanTypeList,
      'mealPlanTypeId',
      'propertyMealPlanTypeName',
    );

    if (this.route.snapshot.queryParams.mealType) {
      this.setMealType(this.route.snapshot.queryParams.mealType);
    }
  }

  private hasCurrencyDefault = (optionsRateCurrency: SelectObjectOption) => {
    return optionsRateCurrency.key
      === this.sessionParameterService
        .getParameterValue(this.propertyId, ParameterTypeEnum.DefaultCurrency);
  }

  private setOptionsRateCurrencyList(currencyList: any) {
    this.optionsRateCurrencyList = [...currencyList.items];
    this.currencyOptionListToSelect = new Array<SelectObjectOption>();
    this.currencyOptionListToSelect = this.commomService.toOption(
      currencyList,
      'id',
      'currencyName',
    );

    const hasCurrencyDefault = this.currencyOptionListToSelect.find(this.hasCurrencyDefault);
    if (this.route.snapshot.queryParams.rateCurrency) {
      this.paymentTypeForm.get('rateCurrencySelect.rateCurrency').setValue(this.route.snapshot.queryParams.rateCurrency);
    } else if (hasCurrencyDefault && hasCurrencyDefault.key) {
      this.paymentTypeForm.get('rateCurrencySelect.rateCurrency').setValue(hasCurrencyDefault.key);
    }
    this.paymentTypeForm.get('rateCurrencySelect.rateCurrency').updateValueAndValidity();
  }

  private setPropertyParameterList(propertyParamsList: any) {
    this.propertyParameterList = [...propertyParamsList];
  }

  private setRoomTypeWithBaseRateList(roomTypeList: any) {
    this.roomTypeWithBaseRateList = [...roomTypeList.items];
  }

  redirectBack = () => {
    if (this.isFixRate()) {
      this.router.navigate(['p', this.propertyId, 'rm', 'rate-plan', 'edit', this.ratePlanId]);
    } else {
      this.goBackPreviewPage();
    }
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
      backPreviewPageCallBackFunction: this.redirectBack
    };
    const insertLabel = this.translateService.instant('action.insert');
    const rateLabel = this.isFixRate() ?
      this.translateService.instant('title.fixedRate') :
      this.translateService.instant('title.baseRate');
    this.title = `${insertLabel} ${rateLabel}`;
  }

  private setInitialDatePicker() {
    this.propertyRate.startDate = moment(this.dateService.removeTime(this.route.snapshot.queryParams.startDate)).toDate();
    this.paymentTypeForm.get('initialDate').setValue(this.propertyRate.startDate.toStringUniversal());

    if (this.route.snapshot.queryParams.endDate) {
      this.propertyRate.finalDate = moment(this.dateService.removeTime(this.route.snapshot.queryParams.endDate)).toDate();
      this.paymentTypeForm.get('lastDate').setValue(this.propertyRate.finalDate.toStringUniversal());
    }
  }

  private saveBaseRateToRoomTypeByDay = () => {
    if (!this.hasMealPlanDefault() && !this.isLevel && !this.useMealTypeListDefault) {
      const mealTypeDefaultRequired = this.translateService.instant('text.mealTypeDefaultRequired');
      this.toasterEmitService
        .emitChange(
          SuccessError.error,
          `${mealTypeDefaultRequired} ${this.getMealTypeDefaultName()}`
        );
      return;
    }

    this.setConfirmModalConfig();
    this.modalEmitToggleService.emitToggleWithRef('confirm-insert-base-rate');
  }

  private setConfirmModalConfig() {
    this.showConfirmModal = true;
    this.confirmModalDataConfig.textConfirmModal = 'text.confirmInsertionBaseRate';
    this.confirmModalDataConfig.alertTextToConfirmModal = 'text.confirmToInsertionBaseRate';
    this.confirmModalDataConfig.alertSecondParagraph = 'variable.datesNotInclude';
    this.confirmModalDataConfig.dates = this.dateOutsideList;
    this.confirmModalDataConfig.actionCancelButton = this.sharedService.getButtonConfig(
      'cancelModal',
      this.closeConfirmationModal,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.confirmModalDataConfig.actionConfirmButton = this.sharedService.getButtonConfig(
      'confirmModal',
      this.isLevel ? this.insertBaseRateLevel : this.insertBaseRateData,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
    );
  }

  public closeConfirmationModal = () => {
    this.modalEmitToggleService.emitToggleWithRef('confirm-insert-base-rate');
  }

  public getLevelId(list) {
    this.levelIdList = list;
  }

  public getWeekSelectedLevel(week) {
    this.paymentTypeForm.get('daysOfWeek').setValue(week);
  }

  public getDateOutside(date) {
    this.dateOutsideList = date;
  }

  public insertBaseRateLevel = () => {
    const baseLevel = <BaseRateLevel>{
      daysOfWeekDto: this.paymentTypeForm.get('daysOfWeek').value,
      levelRateIdList: this.levelIdList,
      initialDate: this.propertyRate.startDate.toString(),
      endDate: this.propertyRate.finalDate.toString()
    };

    this.closeConfirmationModal();
    this
      .baseRateResource
      .sendConfigurationDataLevel(baseLevel)
      .subscribe(() => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbSaveSuccessF', {
            labelName: this.translateService.instant('title.rateLevel'),
          }),
        );
        this.paymentTypeForm.reset();
        this.premiseForm.reset();
        this.setInitialConfigPage();
        this.hasTablePremise = false;

        if (this.returnUrl) {
          this.router.navigateByUrl(this.returnUrl);
        }
      });
  }

  public toggleDefaultMealTypeList() {
    this.useMealTypeListDefault = !this.useMealTypeListDefault;

    if ( this.isPremise ) {
      this.checkPremiseRate();
    } else if (!this.isLevel) {
      this.checkPropertyRate();
    }
  }

  public insertBaseRateData = () => {
    const { selectedPremise, isPremise, propertyRate } = this;

    if ( isPremise && selectedPremise ) {
      propertyRate.propertyBaseRateTypeId = BaseRateType.Premise;
      const { propertyPremiseList } = selectedPremise;

      propertyRate.baseRateConfigurationRoomTypeList = this
        .premiseRateService
        .getBaseRateConfigurationRoomType(
          propertyPremiseList
        );
    } else {
      propertyRate.propertyBaseRateTypeId = BaseRateType.Amount;
    }

    propertyRate.propertyMealPlanTypeRate = this.useMealTypeListDefault;

    if (!this.useMealTypeListDefault && this.mealTypeList) {
      propertyRate.mealPlanTypeDefaultId = this.mealTypeList.find((item: any) => item.isMealPlanTypeDefault).mealPlanTypeId;
    }

    this.closeConfirmationModal();
    const roomTypeNameList = propertyRate.baseRateConfigurationRoomTypeList.map(
      baseRateConfigurationRoomType => baseRateConfigurationRoomType.name,
    );
    const roomTypeStringList = this.translateService.instant('text.roomTypeList', {
      roomTypeList: roomTypeNameList.join(),
    });
    const roomTypeString = this.translateService.instant('text.oneRoomType', {
      roomTypeList: roomTypeNameList.join(),
    });

    this
      .baseRateResource
      .sendConfigurationDataParameters(propertyRate, (isPremise && selectedPremise != null))
      .subscribe(() => {
        if (roomTypeNameList.length > 1) {
          this.toasterEmitService.emitChangeTwoMessages(
            SuccessError.success,
            roomTypeStringList,
            this.translateService.instant('variable.saveSuccessMPlural'),
          );
        } else {
          this.toasterEmitService.emitChangeTwoMessages(
            SuccessError.success,
            roomTypeString,
            this.translateService.instant('variable.saveSuccessM'),
          );
        }

        this.paymentTypeForm.reset();
        this.premiseForm.reset();
        this.setInitialConfigPage();
        this.hasTablePremise = false;

        if (this.returnUrl) {
          this.router.navigateByUrl(this.returnUrl);
        }
    });
  }

  private setInitialConfigPage() {
    this.roomTypeWithBaseRateList = null;
    this.setPaymentForm();
    this.getBaseRateConfiguration();
    if (!this.isLevel) {
      this.weekDayComponent.onInputValueSelected();
    }
  }

  private setTopButtonsListConfig(): void {
    this.topButtonsListConfig = [];

    const listBaseRateType = new RightButtonTop();
    listBaseRateType.title = 'label.baseRateList';
    listBaseRateType.callback = this.goToBaseRateListPage.bind(this);
    listBaseRateType.iconDefault = this.assetsService.getImgUrlTo('base_rate_list.svg');
    listBaseRateType.iconAlt = 'label.baseRateList';

    const topButtonMealType = new RightButtonTop();
    topButtonMealType.title = 'label.mealType';
    topButtonMealType.callback = this.goToMeanTypePage.bind(this);
    topButtonMealType.iconDefault = this.assetsService.getImgUrlTo('tipo-pensao-icon.min.svg');
    topButtonMealType.iconAlt = 'label.mealType';

    const topButtonRatePlan = new RightButtonTop();
    topButtonRatePlan.title = 'label.ratePlan';
    topButtonRatePlan.callback = this.goToRatePlanPage.bind(this);
    topButtonRatePlan.iconDefault = this.assetsService.getImgUrlTo('swap-horiz.min.svg');
    topButtonRatePlan.iconAlt = 'label.ratePlan';


    const topButtonPremise = new RightButtonTop();
    topButtonPremise.title = 'label.configPremise';
    topButtonPremise.callback = this.goToPremise;
    topButtonPremise.iconDefault = this.assetsService.getImgUrlTo('tag-faces-premises.svg');
    topButtonPremise.iconAlt = 'label.configPremise';

    this.topButtonsListConfig.push(listBaseRateType);
    this.topButtonsListConfig.push(topButtonPremise);
    this.topButtonsListConfig.push(topButtonMealType);
    this.topButtonsListConfig.push(topButtonRatePlan);
  }

  public goToBaseRateListPage() {
    this.router.navigate(['p', this.propertyId, 'rm', 'base-rate', 'list']);
  }


  public goToPremise = () => {
    this.router.navigate(['p', this.propertyId, 'rm', 'premises']);
  }

  private getBaseRateConfiguration(): Promise<any> {
    return new Promise<any>( resolve => {
      this.baseRateResource
        .getConfigurationDataParameters(this.propertyRate.propertyId)
        .subscribe(configurationDataParameters => {
          this.setOptionsMealTypeList(configurationDataParameters.mealPlanTypeList);
          this.setOptionsRateCurrencyList(configurationDataParameters.currencyList);
          this.setRoomTypeWithBaseRateList(configurationDataParameters.roomTypeList);
          this.setPropertyParameterList(configurationDataParameters.propertyParameterList);

          resolve(null);
        });
    });
  }

  private setTextForInvalidPeriod() {
    if (this.dateInvalidPassedPeriodAllowed) {
      this.textInvalidPeriod = this.translateService.instant('text.invalidPeriodLimit', {
        dias: this.numberOfDays,
      });
      this.toasterEmitService.emitChange(SuccessError.error, this.textInvalidPeriod);
    }
  }

  private setListener() {
    this.paymentTypeForm.get('rateCurrencySelect.rateCurrency').valueChanges.subscribe(value => {
      this.propertyRate.currencyId = value;
    });
    this.paymentTypeForm.valueChanges.subscribe(() => {
      if (this.paymentTypeForm.get('initialDate').value && this.paymentTypeForm.get('lastDate').value) {
        const date1 = this.dateService
          .convertStringToDate(this.paymentTypeForm.get('initialDate').value, DateService.DATE_FORMAT_UNIVERSAL);
        const date2 = this.dateService
          .convertStringToDate(this.paymentTypeForm.get('lastDate').value, DateService.DATE_FORMAT_UNIVERSAL);

        this.dateInvalid = this.dateService.date1IsMajorThanDate2(date1, date2);
        this.numberOfDays = this.dateService.diffDays(date1, date2);
        this.dateInvalidPassedPeriodAllowed =
          this.numberOfDays > this.PERIOD_ALLOWED_DAYS;
        this.propertyRate.startDate = this.paymentTypeForm.get('initialDate').value;
        this.propertyRate.finalDate = this.paymentTypeForm.get('lastDate').value;
        this.setTextForInvalidPeriod();
      }

      if ( this.isPremise ) {
        this.checkPremiseRate();
      } else if (!this.isLevel) {
        this.checkPropertyRate();
      }
    });

    merge(
      this.paymentTypeForm.get('initialDate').valueChanges,
      this.paymentTypeForm.get('lastDate').valueChanges
    ).subscribe(() => {
      if (this.isLevel && this.baseRateLevel) {
        this.baseRateLevel.reset();
      }
    });
  }

  public goToMeanTypePage() {
    this.router.navigate(['p', this.propertyId, 'rm', 'meal-plan-type']);
  }

  public goToRatePlanPage() {
    this.router.navigate(['p', this.propertyId, 'rm', 'rate-plan']);
  }

  public weekDaysSelected(weekDays) {
    this.propertyRate.daysOfWeekDto = weekDays;
    this.paymentTypeForm.get('daysOfWeek').setValue(weekDays);
    this.checkPropertyRate();
  }

  private goBackPreviewPage = () => {
    this.location.back();
  }

  private checkPropertyRate() {
    this.saveButtonConfig.isDisabled = this.paymentTypeForm.invalid ||
      this.dateInvalid ||
      this.dateInvalidPassedPeriodAllowed ||
      this.isPremise ||
      !this.roomTypeWithBaseRateListValid;

    this.checkMealPlanValues();
  }

  private checkPremiseRate() {
    this.saveButtonConfig.isDisabled = this.paymentTypeForm.invalid ||
      this.dateInvalid ||
      this.dateInvalidPassedPeriodAllowed ||
      !this.isPremise ||
      this.premiseForm.invalid ||
      !this.selectedPremise;

      this.checkMealPlanValues();
  }

  private checkMealPlanValues() {
    if (this.mealTypeList) {
      const validate = this.mealTypeList.filter((item: any) => {
        return item.isMealPlanTypeDefault
          && (
            item.adultMealPlanAmount != null ||
            item.child1MealPlanAmount != null ||
            item.child2MealPlanAmount != null ||
            item.child3MealPlanAmount != null
          );
      });

      if (!this.useMealTypeListDefault) {
        this.saveButtonConfig.isDisabled = !validate.length;
      }
    }
  }

  public setRoomTypeWithBaseRateUpdatedList(configRoomTypeWithBaseRateValidate?: any) {
    if (this.baseRateService.hasBaseRateConfigWithValue(configRoomTypeWithBaseRateValidate.roomTypeList)) {
      this.baseRateTableList = this.baseRateMapper.getBaseRateTableListModel(configRoomTypeWithBaseRateValidate.roomTypeList) || [];
      this.propertyRate.baseRateConfigurationRoomTypeList = [...this.baseRateTableList];
      this.roomTypeWithBaseRateListValid = configRoomTypeWithBaseRateValidate.validBaseRate;
    }
    this.checkPropertyRate();
  }

  public updatedListMealTypeWithBaseRate(mealPlanTypeWithBaseRateList?: BaseRateConfigurationMealPlanType[]) {
    this.propertyRate.baseRateConfigurationMealPlanTypeList = mealPlanTypeWithBaseRateList;

    this.checkPropertyRate();
  }

  public isFixRate(): boolean {
    return !!(this.ratePlanId && this.mealPlanId);
  }

  private setRatePlan() {
    this.ratePlanResource.getRatePlanById(this.ratePlanId).subscribe(response => {
      this.ratePlan = response;
      this.propertyRate.mealPlanTypeDefaultId = this.mealPlanId;
      this.propertyRate.ratePlanId = this.ratePlanId;

      this.setMealType(this.mealPlanId);
    });
  }

  private setMealType(id) {
    if (this.mealTypeList && id) {
      const myItem = <any>this.mealTypeList.filter(item => item.mealPlanTypeId == id);

      if (myItem) {
        myItem.isMealPlanTypeDefault = true;
        this.mealTypeList = [...this.mealTypeList];
      }
    }
  }

  private setReturnUrl() {
    this.returnUrl = `p/${this.propertyId}/rm/rate-plan/edit/${this.ratePlanId}`;
  }

  private hasMealPlanDefault() {
    return this.propertyRate.mealPlanTypeDefaultId == MealPlanTypeEnum.None ||
      this.propertyRate.baseRateConfigurationMealPlanTypeList
        .some(mp => mp.isMealPlanTypeDefault);
  }

  private getMealTypeDefaultName() {
    if (!this.mealTypeList) {
      return '';
    }

    const mealDefault = this.mealTypeList.find((mt: any) => mt.isMealPlanTypeDefault);

    return (mealDefault) ? mealDefault.mealPlanTypeName : '';
  }
}
