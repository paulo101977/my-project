import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BaseRateTableComponent } from 'app/revenue-management/base-rate/components/base-rate-table/base-rate-table.component';
import { BaseRateComponent } from 'app/revenue-management/base-rate/components/base-rate/base-rate.component';
import { DATA, PREMISE_BY_ID_PROPERTY } from 'app/revenue-management/premises/mock-data';
import { PropertyPremiseHeader } from 'app/revenue-management/premises/models/property-premise-header';
import { SharedModule } from 'app/shared/shared.module';
import { configureTestSuite } from 'ng-bullet';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { of } from 'rxjs';

describe('BaseRateComponent', () => {
  let component: BaseRateComponent;
  let fixture: ComponentFixture<BaseRateComponent>;
  let fb: FormBuilder;

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        imports: [
          TranslateModule.forRoot(),
          CommonModule,
          ReactiveFormsModule,
          FormsModule,
          CurrencyMaskModule,
          SharedModule,
          RouterTestingModule,
          HttpClientTestingModule,
        ],
        declarations: [BaseRateComponent, BaseRateTableComponent],
        schemas: [ NO_ERRORS_SCHEMA ]
      });

    fixture = TestBed.createComponent(BaseRateComponent);
    component = fixture.componentInstance;

    fb = new FormBuilder();

    spyOn(component, 'getPremiseList').and.callThrough();
    spyOn<any>(component, 'checkPremiseRate').and.callThrough();
    spyOn(component['premiseRateService'], 'setPremiseValues').and.callThrough();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test getPremiseList with one item', () => {
    const data = { items: [ DATA[PREMISE_BY_ID_PROPERTY] ] };
    component.paymentTypeForm.patchValue({lastDate: '2019-07-06T00:00:00'});
    spyOn<any>(component['premiseResource'], 'getPropertyPremiseHeaderByDate')
      .and.returnValue(of(data));

    component.getPremiseList();

    expect(component.currentPremise).toEqual(data.items[0]);
  });

  it('should test getPremiseList with more items item', () => {
    const { isActive , ...dataInative } = DATA[PREMISE_BY_ID_PROPERTY];
    dataInative['isActive'] = false;

    const data = {
      items: [
        DATA[PREMISE_BY_ID_PROPERTY],
        DATA[PREMISE_BY_ID_PROPERTY],
        dataInative,
        dataInative
    ]};
    component.paymentTypeForm.patchValue({lastDate: '2019-07-06T00:00:00'});
    spyOn<any>(component['premiseResource'], 'getPropertyPremiseHeaderByDate')
      .and.returnValue(of(data));

    component.getPremiseList();

    expect(component.premiseList.length).toEqual(2);
  });

  it('should test app-button-bar toggle functions', () => {
    const appButtonBar = fixture.debugElement.nativeElement.querySelector('app-button-bar');
    const allButtons = appButtonBar.querySelectorAll('button');

    expect(allButtons[0].innerText).toEqual('text.value');
    expect(allButtons[1].innerText).toEqual('text.premise');
    expect(allButtons[2].innerText).toEqual('label.level');
  });

  describe('should changeView', () => {
    it('isValue ', () => {
      component.changeView('');
      expect(component.isValue).toEqual(true);
      expect(component.isPremise).toEqual(false);
      expect(component.isLevel).toEqual(false);
    });

    it('isPremise ', () => {
      component.changeView('isPremise');
      expect(component.isValue).toEqual(false);
      expect(component.isPremise).toEqual(true);
      expect(component.isLevel).toEqual(false);
    });

    it('isLevel ', () => {
      component.changeView('isLevel');
      expect(component.isValue).toEqual(false);
      expect(component.isPremise).toEqual(false);
      expect(component.isLevel).toEqual(true);
    });
  });

  it('should test setPremiseForm', fakeAsync(() => {
    const dataEl =  {...DATA[PREMISE_BY_ID_PROPERTY]};
    delete dataEl['includes'];


    const dataEl2 = {...dataEl};
    dataEl2.id = '';

    const data = [
      dataEl,
      dataEl2,
    ];

    component['cdRef'].detectChanges = () => {};

    component.propertyPremiseHeaderList = <PropertyPremiseHeader[]>data;
    component.setPremiseForm();

    tick();

    expect(component.premiseApplyButton.isDisabled).toEqual(true);

    component['premiseForm'].patchValue(
      { premiseId: dataEl.id , premiseAmount: 10}
      );

    tick();

    expect(component.premiseApplyButton.isDisabled).toEqual(false);
    expect(component['checkPremiseRate']).toHaveBeenCalled();
    expect(component.currentPremise).toEqual(dataEl);
  }));

  it('should test premiseApply', fakeAsync(() => {
    const dataEl =  {...DATA[PREMISE_BY_ID_PROPERTY]};
    delete dataEl['includes'];

    component.currentPremise = dataEl;
    component['premiseForm'].patchValue(
      { premiseId: dataEl.id , premiseAmount: 10}
    );

    component.premiseApply();

    tick();


    expect(component.hasTablePremise).toEqual(true);
    expect(component.selectedPremise.propertyPremiseList[0]['pax1Amount']).toEqual(11);
    expect(component['checkPremiseRate']).toHaveBeenCalled();
  }));

  it('should test checkPremiseRate', () => {
    component['paymentTypeForm'] = fb.group({
      value: [null]
    });
    let dataEl =  {...DATA[PREMISE_BY_ID_PROPERTY]};
    delete dataEl['includes'];
    dataEl = <PropertyPremiseHeader>{...dataEl};


    component.selectedPremise = <PropertyPremiseHeader>dataEl;
    component['premiseForm'].patchValue(
      { premiseId: '1111' , premiseAmount: 10}
    );

    component.isPremise = true;

    component['checkPremiseRate']();

    component.dateInvalid = false;
    component.dateInvalidPassedPeriodAllowed = false;
    component.isPremise = false;

    component['checkPremiseRate']();

    expect(component.saveButtonConfig.isDisabled).toEqual(true);
  });
});
