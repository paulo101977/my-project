import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MealPlanTableComponent } from './meal-plan-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CommonModule } from '@angular/common';
import { BASE_RATE_CONFIG_MEAL_PLAN_TYPE, DATA } from 'app/revenue-management/base-rate/mock-data';
import { propertyParameterDataList } from 'app/shared/mock-data/property-parameter-data';

describe('MealPlanTableComponent', () => {
  let component: MealPlanTableComponent;
  let fixture: ComponentFixture<MealPlanTableComponent>;

  const listWithoutMealTypeNone = [
    DATA[BASE_RATE_CONFIG_MEAL_PLAN_TYPE][0],
    DATA[BASE_RATE_CONFIG_MEAL_PLAN_TYPE][1],
    DATA[BASE_RATE_CONFIG_MEAL_PLAN_TYPE][2],
    DATA[BASE_RATE_CONFIG_MEAL_PLAN_TYPE][3],
    DATA[BASE_RATE_CONFIG_MEAL_PLAN_TYPE][4],
    DATA[BASE_RATE_CONFIG_MEAL_PLAN_TYPE][6],
  ];

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed
      .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: [
          TranslateModule.forRoot(),
          CommonModule,
          ReactiveFormsModule,
          FormsModule,
          CurrencyMaskModule
        ],
        declarations: [MealPlanTableComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      });

    fixture = TestBed.createComponent(MealPlanTableComponent);
    component = fixture.componentInstance;
    component.propertyParameterList = propertyParameterDataList;
    spyOn(component['mealPlanTypeService'], 'setConfigMealPlanTypeList')
      .and.returnValue(listWithoutMealTypeNone);
    component.mealTypeList = DATA[BASE_RATE_CONFIG_MEAL_PLAN_TYPE];
    component.rows = listWithoutMealTypeNone;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should setVars', () => {
    expect(component.optionsCurrencyMask).toEqual(
      {prefix: '', thousands: '.', decimal: ',', align: 'left'}
    );
    expect(component.ageRange1).toEqual(
      parseInt(component.propertyParameterList[0].propertyParameterValue, 10) + 1
    );
    expect(component.ageRange2).toEqual(
      parseInt(component.propertyParameterList[1].propertyParameterValue, 10) + 1
    );
  });

  it('should emit new mealTypeList', () => {
    spyOn(component.updatedList, 'emit');

    component.updateMealType(DATA[BASE_RATE_CONFIG_MEAL_PLAN_TYPE]);

    expect(component.updatedList.emit)
      .toHaveBeenCalledWith(component['mealPlanTypeService']
        .mapperValidMealTypeList(DATA[BASE_RATE_CONFIG_MEAL_PLAN_TYPE]));
  });

  it('should setMealPlanTypeDefault', () => {
    spyOn(component['mealPlanTypeService'], 'uptateMealPlanTypeDefault')
      .and.returnValue(listWithoutMealTypeNone);
    spyOn(component, 'updateMealType');

    component.setMealPlanTypeDefault(listWithoutMealTypeNone[0]);

    expect(component['mealPlanTypeService'].uptateMealPlanTypeDefault)
      .toHaveBeenCalledWith(component.rows, listWithoutMealTypeNone[0]);
    expect(component.updateMealType).toHaveBeenCalledWith(component.rows);
  });

});
