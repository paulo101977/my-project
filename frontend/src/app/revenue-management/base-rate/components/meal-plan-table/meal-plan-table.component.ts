import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { MealPlanTypeService } from 'app/revenue-management/meal-plan-type/services/meal-plan-type.service';
import { Parameter } from '../../../../shared/models/parameter';
import { BaseRateConfigurationMealPlanType } from '../../../../shared/models/revenue-management/meal-type-fix-rate';

@Component({
  selector: 'meal-plan-table',
  templateUrl: './meal-plan-table.component.html'
})
export class MealPlanTableComponent implements OnInit, OnChanges {

  @Input() mealTypeList: BaseRateConfigurationMealPlanType[];
  @Input() propertyParameterList: Parameter[];
  @Input() canChoseDefault: boolean;
  @Input() isDisabled: boolean;
  @Output() updatedList = new EventEmitter();

  public rows: BaseRateConfigurationMealPlanType[];
  public ageRange1: number;
  public ageRange2: number;
  public optionsCurrencyMask: any;

  constructor(
    private mealPlanTypeService: MealPlanTypeService
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes: any) {
    if (this.mealTypeList) {
      this.rows = this.mealPlanTypeService
        .setConfigMealPlanTypeList(this.mealTypeList);
    }
  }

  setVars() {
    this.optionsCurrencyMask = {prefix: '', thousands: '.', decimal: ',', align: 'left'};
    this.ageRange1 = parseInt(this.propertyParameterList[0].propertyParameterValue, 10) + 1;
    this.ageRange2 = parseInt(this.propertyParameterList[1].propertyParameterValue, 10) + 1;
  }

  public updateMealType(rows) {
    this.updatedList.emit(rows);
  }

  public setMealPlanTypeDefault(row: BaseRateConfigurationMealPlanType) {
    this.rows = [...this.mealPlanTypeService
      .uptateMealPlanTypeDefault(this.rows, row)];
    this.updateMealType(this.rows);
  }

}
