import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-premise-rate-table',
  templateUrl: './premise-rate-table.component.html',
  styleUrls: ['./premise-rate-table.component.scss']
})
export class PremiseRateTableComponent implements OnInit {

  public _children: Array<any>;
  public ageText1: string;
  public ageText2: string;
  public ageText3: string;
  public _selectedPremise: any;
  public _currentPremiseValue: number;
  public roomTypeReferenceId: number;
  public paxReference: string;

  @Input() roomTypeWithBaseRateList: Array<any>;
  @Input() set propertyParameterList (_children) {
    this._children = _children;

    this.setColumns();
  }
  @Input() set selectedPremise(_selectedPremise) {
    this._selectedPremise = _selectedPremise;

    this.normalizeData();
  }

  @Input() set currentPremiseValue(_currentPremiseValue: string | number) {
    this._currentPremiseValue = (+_currentPremiseValue);

    this.normalizeData();
  }

  @ViewChild('header1') header1: TemplateRef<any>;
  @ViewChild('header2') header2: TemplateRef<any>;
  @ViewChild('header3') header3: TemplateRef<any>;
  @ViewChild('firstRowInfo') firstRowInfo: TemplateRef<any>;
  @ViewChild('paxOne') paxOne: TemplateRef<any>;
  @ViewChild('paxTwo') paxTwo: TemplateRef<any>;
  @ViewChild('paxThree') paxThree: TemplateRef<any>;
  @ViewChild('paxFour') paxFour: TemplateRef<any>;
  @ViewChild('paxFive') paxFive: TemplateRef<any>;
  @ViewChild('paxAdditional') paxAdditional: TemplateRef<any>;
  @ViewChild('child1') child1: TemplateRef<any>;
  @ViewChild('child2') child2: TemplateRef<any>;
  @ViewChild('child3') child3: TemplateRef<any>;

  public itemsList: Array<any>;

  public columns: Array<any>;

  public optionsCurrencyMask: any;


  constructor(
    private translateService: TranslateService,
  ) { }

  public setColumns() {
    const {_children} = this;

    this.columns = [
      {
        name: 'label.typeUHComplete',
        cellTemplate: this.firstRowInfo,
        sortable: false
      },
      {
        name: 'label.paxOne',
        cellTemplate: this.paxOne,
        sortable: false
      },
      {
        name: 'label.paxTwo',
        cellTemplate: this.paxTwo,
        sortable: false
      },
      {
        name: 'label.paxThree',
        cellTemplate: this.paxThree,
        sortable: false
      },
      {
        name: 'label.paxFour',
        cellTemplate: this.paxFour,
        sortable: false
      },
      {
        name: 'label.paxFive',
        cellTemplate: this.paxFive,
        sortable: false
      },
      {
        name: 'label.paxAdditional',
        cellTemplate: this.paxAdditional,
        sortable: false
      },
    ];

    if (_children) {
      _children.forEach((child, i) => {
        const index = i + 1;
        if (child.isActive) {
          this.columns.push({
            headerTemplate: this[`header${ index }`],
            prop: `child${ index }`,
            cellTemplate: this[`child${ index }`],
            sortable: false
          });

          this[`ageText${ index }`] = this
            .translateService
            .instant('label.ageByYears', {
              min: child.propertyParameterMinValue,
              max: child.propertyParameterValue
            });
        }
      });
    }
  }

  public normalizeData() {
    const {
      roomTypeWithBaseRateList,
      _selectedPremise,
      _currentPremiseValue
    } = this;

    if (
      roomTypeWithBaseRateList
        && _selectedPremise
        && _selectedPremise.propertyPremiseList
        && _currentPremiseValue
    ) {
      const {
        propertyPremiseList,
        paxReference,
        roomTypeReferenceId
      } = _selectedPremise;

      this.roomTypeReferenceId = roomTypeReferenceId;
      this.paxReference = paxReference;

      this.itemsList = roomTypeWithBaseRateList.map( item => {
        const { minimumRate, maximumRate } = item;
        const finded = propertyPremiseList.find(tItem => item.id === tItem.roomTypeId) || {};

        item.isRowReference = finded.roomTypeId === roomTypeReferenceId;

        return {
          ...item,
          ...finded,
          roomTypeListMinMaxRate: this
            .translateService
            .instant('label.roomTypeListMinMaxRate', { minimumRate, maximumRate })
        };
      });
    }
  }

  public modelHasBeenUpdated(event) {
    if ( this._selectedPremise ) {
      this._selectedPremise.propertyPremiseList = [...this.itemsList];
    }
  }

  ngOnInit() {
    this.setColumns();
    this.setInputMask();
    this.normalizeData();
  }

  public setInputMask() {
    // the input number mask
    this.optionsCurrencyMask = {
      prefix: '',
      thousands: '.',
      decimal: ',',
      align: 'center',
      allowZero: true,
      allowNegative: false,
      precision: 2,
    };
  }

}
