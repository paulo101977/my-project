import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { PremiseRateTableComponent } from './premise-rate-table.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'app/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CONFIGURATION_PROPERTY, DATA, NORMALIZED_DATA, PREMISE_BY_ID_PROPERTY } from 'app/revenue-management/premises/mock-data';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { currencyFormatPipe } from '../../../../../../testing';

describe('PremiseRateTableComponent', () => {
  let component: PremiseRateTableComponent;
  let fixture: ComponentFixture<PremiseRateTableComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        PremiseRateTableComponent,
        currencyFormatPipe,
      ],
      imports: [
        TranslateTestingModule,
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    });

    fixture = TestBed.createComponent(PremiseRateTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(component, 'setColumns').and.callThrough();
    spyOn(component, 'setInputMask');
    spyOn(component, 'normalizeData').and.callThrough();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', () => {
    component.ngOnInit();

    expect(component.setColumns).toHaveBeenCalled();
    expect(component.setInputMask).toHaveBeenCalled();
    expect(component.normalizeData).toHaveBeenCalled();
  });

  it('should test normalizeData', fakeAsync(() => {
    const premiseList = JSON.parse(JSON.stringify(DATA[PREMISE_BY_ID_PROPERTY].propertyPremiseList));

    component.roomTypeWithBaseRateList = DATA[CONFIGURATION_PROPERTY].roomTypeList.items;

    component._selectedPremise = {
      propertyPremiseList: premiseList,
      paxReference: 1,
      roomTypeReferenceId: 2
    };

    component._currentPremiseValue = 50;

    component.ngOnInit();

    component.propertyParameterList = DATA[CONFIGURATION_PROPERTY].propertyParameterList;

    expect(component.itemsList[1]).toEqual(DATA[NORMALIZED_DATA][0]);
  }));
});
