import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { MyDatePickerModule } from 'mydatepicker';
import { BaseRateComponent } from './components/base-rate/base-rate.component';
import { BaseRateTableComponent } from './components/base-rate-table/base-rate-table.component';
import { BaseRateResource } from '../../shared/resources/base-rate/base-rate.resource';
import { BaseRateRoutingModule } from './base-rate-routing.module';
import { MealPlanTableComponent } from './components/meal-plan-table/meal-plan-table.component';
import { BaseRateListComponent } from './containers/base-rate-list/base-rate-list.component';
import { PremiseRateTableComponent } from './components/premise-rate-table/premise-rate-table.component';
import { BaseLevelListComponent } from './containers/base-level-list/base-level-list.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MyDatePickerModule,
    BaseRateRoutingModule
  ],
  providers: [BaseRateResource],
  declarations: [
    BaseRateComponent,
    BaseRateTableComponent,
    MealPlanTableComponent,
    BaseRateListComponent,
    PremiseRateTableComponent,
    BaseLevelListComponent],
  exports: [
    MealPlanTableComponent,
    BaseRateTableComponent
    ],
  // TODO: remova all CUSTOM_ELEMENTS_SCHEMA [DEBIT: 7141]
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class BaseRateModule {}
