export class RateStrategy {
  id: string;
  isActive: boolean;
  name: string;
  startDate: string;
  endDate: string;
  roomTypeAbbreviationList: string[];
  propertyRateStrategyRoomTypeList: RateStrategyRoomType[];
}

export enum VariationRate {
  percentage,
  currency
}

export class RateStrategyRoomType {
  id: string;
  propertyRateStrategyId: string;
  roomTypeId: number;
  roomTypeName: string;
  minOccupationPercentual: number;
  maxOccupationPercentual: number;
  isDecreased: boolean;
  percentualAmount: number;
  incrementAmount: number;

  occupationPercent?: string;
  isDecreasedName?: string;
  amount?: number;
  variation?: VariationRate;
  amountView?: string;
  isEdit?: boolean;

  constructor() {
    this.roomTypeId = 0;
    this.isDecreased = true;
    this.minOccupationPercentual = 0;
    this.maxOccupationPercentual = 0;
    this.percentualAmount = 0;
    this.incrementAmount = 0;
    this.amount = 0;
    this.variation = VariationRate.percentage;
    this.amountView = VariationRate.percentage
      ? this.percentualAmount + ' %'
      : `${this.incrementAmount}`;
  }
}
