import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RateStrategyRoutingModule } from 'app/revenue-management/rate-strategy/rate-strategy-routing.module';
import { RateStrategyListComponent } from './components/rate-strategy-list/rate-strategy-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { RateStrategyCreateEditComponent } from './components/rate-strategy-create-edit/rate-strategy-create-edit.component';
import { ThxErrorFormModule } from '@inovacao-cmnet/thx-ui';
import { OccupationParameterManagerComponent } from './components/occupation-parameter-manager/occupation-parameter-manager.component';

@NgModule({
  imports: [
    CommonModule,
    RateStrategyRoutingModule,
    SharedModule,
    ThxErrorFormModule
  ],
  declarations: [RateStrategyListComponent, RateStrategyCreateEditComponent, OccupationParameterManagerComponent]
})
export class RateStrategyModule { }
