import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RateStrategyListComponent } from './components/rate-strategy-list/rate-strategy-list.component';
import { RateStrategyCreateEditComponent } from './components/rate-strategy-create-edit/rate-strategy-create-edit.component';

export const rateStrategyRoutes: Routes = [
  {path: '', component: RateStrategyListComponent},
  {path: 'new', component: RateStrategyCreateEditComponent},
  {path: 'edit/:id', component: RateStrategyCreateEditComponent},
];

@NgModule({
  imports: [RouterModule.forChild(rateStrategyRoutes)],
  exports: [RouterModule],
})
export class RateStrategyRoutingModule {
}
