import { RateStrategy, RateStrategyRoomType, VariationRate } from 'app/revenue-management/rate-strategy/models/rate-strategy';

export const RATE_STRATEGY_ROOM_TYPE: RateStrategyRoomType = {
  id: 'c5bb96d6-9796-4fd2-9d2e-2f67f7f4dddf',
  propertyRateStrategyId: '127fbeee-7d99-4b6e-8c8c-b84aad6d3196',
  roomTypeId: 1,
  minOccupationPercentual: 0,
  maxOccupationPercentual: 100,
  isDecreased: true,
  percentualAmount: 25,
  incrementAmount: 0,
  roomTypeName: 'LUXO',
  variation: VariationRate.percentage
};

export const RATE_STRATEGY_DATA: RateStrategy = {
  id: '324343243432',
  name: 'Estratégia 01',
  startDate: '2019-01-01T00:00:00',
  endDate: '2019-01-04T00:00:00',
  isActive: true,
  roomTypeAbbreviationList: [],
  propertyRateStrategyRoomTypeList: [RATE_STRATEGY_ROOM_TYPE]
};

export const RATE_STRATEGY_LIST_DATA: RateStrategy[] = [
  RATE_STRATEGY_DATA
];


