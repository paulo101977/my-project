import { Injectable } from '@angular/core';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';
import { RateStrategy } from 'app/revenue-management/rate-strategy/models/rate-strategy';

@Injectable({
  providedIn: 'root'
})
export class RateStrategyResource {

  constructor(private thexApi: ThexApiService) {
  }

  public gelAllByPropertyId(): Observable<any> {
    return this.thexApi.get(`PropertyRateStrategy/getallbypropertyid`);
  }

  public getById(id: string): Observable<RateStrategy> {
    return this.thexApi.get(`PropertyRateStrategy/${id}`);
  }

  public updateStatus(id: string): Observable<any> {
    return this.thexApi.patch(`PropertyRateStrategy/${id}/toggleactivation`);
  }

  public create(rateStrategy: RateStrategy): Observable<any> {
    return this.thexApi.post(`PropertyRateStrategy/`, rateStrategy);
  }

  public update(rateStrategy: RateStrategy): Observable<any> {
    return this.thexApi.put(`PropertyRateStrategy/${rateStrategy.id}`, rateStrategy);
  }
}
