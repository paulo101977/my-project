import { TestBed } from '@angular/core/testing';
import { RateStrategyService } from 'app/revenue-management/rate-strategy/services/rate-strategy.service';
import { RATE_STRATEGY_ROOM_TYPE } from 'app/revenue-management/rate-strategy/mock/rate-strategy-mock';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { RateStrategyRoomType, VariationRate } from 'app/revenue-management/rate-strategy/models/rate-strategy';

describe('RateStrategyService', () => {
  let service: RateStrategyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule],
      providers: [RateStrategyService]
    });

    service = TestBed.get(RateStrategyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should mapperParamToTable', () => {
    const rateStrategyRoomType = new RateStrategyRoomType();
    rateStrategyRoomType.minOccupationPercentual = 20;
    rateStrategyRoomType.maxOccupationPercentual = 80;
    rateStrategyRoomType.isDecreased = true;
    rateStrategyRoomType.variation = VariationRate.percentage;
    rateStrategyRoomType['amount'] = 25;

    service.mapperParamToTable(rateStrategyRoomType);

    expect(rateStrategyRoomType['amountView']).toEqual('25 %');
    expect(rateStrategyRoomType['isDecreasedName']).toEqual(service.i18n.labels.discount);
    expect(rateStrategyRoomType['occupationPercent']).toEqual(`20 ${service.i18n.labels.until} 80`);
  });

  it('should mapperParamsToTable', () => {
    const rateStrategyRoomType = new RateStrategyRoomType();
    rateStrategyRoomType.minOccupationPercentual = 0;
    rateStrategyRoomType.maxOccupationPercentual = 100;
    rateStrategyRoomType.isDecreased = true;
    rateStrategyRoomType.variation = VariationRate.currency;
    rateStrategyRoomType['amount'] = 50;

    const result = service.mapperParamsToTable([rateStrategyRoomType]);

    expect(result[0]['amountView']).toEqual('50 R$');
    expect(result[0]['isDecreasedName']).toEqual(service.i18n.labels.discount);
    expect(result[0]['occupationPercent']).toEqual(`0 ${service.i18n.labels.until} 100`);
  });

  it('should rateStrategyRoomTypeIsValid', () => {
    RATE_STRATEGY_ROOM_TYPE.amount = RATE_STRATEGY_ROOM_TYPE.percentualAmount;
    const result = service.rateStrategyRoomTypeIsValid(RATE_STRATEGY_ROOM_TYPE);

    expect(result).toBeTruthy();
  });

  it('should amountIsValid#valid', () => {
    const result = service.amountIsValid(RATE_STRATEGY_ROOM_TYPE);

    expect(result).toBeTruthy();
  });

  it('should amountIsValid#invalid', () => {
    RATE_STRATEGY_ROOM_TYPE.amount = 0;
    const result = service.amountIsValid(RATE_STRATEGY_ROOM_TYPE);

    expect(result).toBeFalsy();
  });

  it('should minOccupationPercentual#valid', () => {
    RATE_STRATEGY_ROOM_TYPE.minOccupationPercentual = 20;
    RATE_STRATEGY_ROOM_TYPE.maxOccupationPercentual = 80;
    const result = service.minOccupationPercentual(RATE_STRATEGY_ROOM_TYPE);

    expect(result).toBeTruthy();
  });

  it('should minOccupationPercentual#invalid', () => {
    RATE_STRATEGY_ROOM_TYPE.minOccupationPercentual = -1;
    let result = service.minOccupationPercentual(RATE_STRATEGY_ROOM_TYPE);
    expect(result).toBeFalsy();

    RATE_STRATEGY_ROOM_TYPE.minOccupationPercentual = 150;
    result = service.minOccupationPercentual(RATE_STRATEGY_ROOM_TYPE);
    expect(result).toBeFalsy();

    RATE_STRATEGY_ROOM_TYPE.minOccupationPercentual = 50;
    RATE_STRATEGY_ROOM_TYPE.maxOccupationPercentual = 40;
    result = service.minOccupationPercentual(RATE_STRATEGY_ROOM_TYPE);
    expect(result).toBeFalsy();
  });

  it('should maxOccupationPercentual#valid', () => {
    RATE_STRATEGY_ROOM_TYPE.minOccupationPercentual = 20;
    RATE_STRATEGY_ROOM_TYPE.maxOccupationPercentual = 80;
    const result = service.maxOccupationPercentual(RATE_STRATEGY_ROOM_TYPE);

    expect(result).toBeTruthy();
  });

  it('should maxOccupationPercentual#invalid', () => {
    RATE_STRATEGY_ROOM_TYPE.maxOccupationPercentual = -1;
    let result = service.maxOccupationPercentual(RATE_STRATEGY_ROOM_TYPE);
    expect(result).toBeFalsy();

    RATE_STRATEGY_ROOM_TYPE.maxOccupationPercentual = 150;
    result = service.maxOccupationPercentual(RATE_STRATEGY_ROOM_TYPE);
    expect(result).toBeFalsy();

    RATE_STRATEGY_ROOM_TYPE.minOccupationPercentual = 50;
    RATE_STRATEGY_ROOM_TYPE.maxOccupationPercentual = 40;
    result = service.maxOccupationPercentual(RATE_STRATEGY_ROOM_TYPE);
    expect(result).toBeFalsy();
  });

});
