import { RateStrategyRoomType, VariationRate } from 'app/revenue-management/rate-strategy/models/rate-strategy';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class RateStrategyService {

  // Translate
  public i18n: any;

  constructor(
    private translate: TranslateService,
  ) {
    this.setTranslate();
  }

  private setTranslate() {
    this.i18n = {
      labels: {
        discount: this.translate.instant('label.discount'),
        increase: this.translate.instant('label.increase'),
        until: this.translate.instant('text.until')
      }
    };
  }

  public mapperParamToTable(param: RateStrategyRoomType) {
    param.occupationPercent = `${param.minOccupationPercentual} ${this.i18n.labels.until} ${param.maxOccupationPercentual}`;
    param.isDecreasedName = param.isDecreased
      ? this.i18n.labels.discount
      : this.i18n.labels.increase;
    param.isEdit = false;

    this.setAmountVariation(param);
    return param;
  }

  private setAmountVariation(param: RateStrategyRoomType) {
    if (param.hasOwnProperty('variation') && param.hasOwnProperty('amount')) {
      if (param.variation == VariationRate.percentage) {
        param.incrementAmount = null;
        param.percentualAmount = param.amount;
        param.amountView = param.percentualAmount + ' %';
        param.amount = param.percentualAmount;
      } else {
        param.percentualAmount = null;
        param.incrementAmount = param.amount;
        param.amountView = param.incrementAmount + ' R$';
        param.amount = param.incrementAmount;
      }
    } else {
      if (param.percentualAmount) {
        param.variation = VariationRate.percentage;
        param.amountView = param.percentualAmount + ' %';
        param.amount = param.percentualAmount;
      } else {
        param.variation = VariationRate.currency;
        param.amountView = param.incrementAmount + ' R$';
        param.amount = param.incrementAmount;
      }
    }
  }

  public mapperParamsToTable(items: RateStrategyRoomType[]): RateStrategyRoomType[] {
    return items.map(param => {
      return this.mapperParamToTable(param);
    });
  }

  public rateStrategyRoomTypeIsValid(param: RateStrategyRoomType) {
    return param.roomTypeId != null
      && this.amountIsValid(param)
      && this.minOccupationPercentual(param)
      && this.maxOccupationPercentual(param);
  }

  public amountIsValid(param: RateStrategyRoomType) {
    return param.isDecreased
      ? param.variation == VariationRate.percentage
        ? param.amount > 0 && param.amount <= 99.99
        : param.amount > 0
      : param.amount > 0;
  }

  public minOccupationPercentual(param) {
    return param.minOccupationPercentual >= 0
      && param.minOccupationPercentual <= 100
      && param.minOccupationPercentual < param.maxOccupationPercentual;
  }

  public maxOccupationPercentual(param) {
    return param.maxOccupationPercentual >= 0
      && param.maxOccupationPercentual <= 100
      && param.maxOccupationPercentual > param.minOccupationPercentual;
  }

}
