import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccupationParameterManagerComponent } from './occupation-parameter-manager.component';
import { ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { RATE_STRATEGY_ROOM_TYPE } from 'app/revenue-management/rate-strategy/mock/rate-strategy-mock';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { VariationRate } from 'app/revenue-management/rate-strategy/models/rate-strategy';
import { FormBuilder, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

xdescribe('OccupationParameterManagerComponent', () => {
  let component: OccupationParameterManagerComponent;
  let fixture: ComponentFixture<OccupationParameterManagerComponent>;

  @Pipe({name: 'imgCdn'})
  class ImgCdnPipeStub implements PipeTransform {
    transform(value: any, ...args: any[]): any {
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule, FormsModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [OccupationParameterManagerComponent, ImgCdnPipeStub],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {params: {property: '1'}},
          },
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccupationParameterManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setButtonConfig', () => {
      component['setButtonConfig']();

      expect(component.newParamButton.id).toEqual('newParam');
      expect(component.newParamButton.textButton).toEqual(component.i18n.actions.newParam);
      expect(component.newParamButton.buttonType).toEqual(ButtonType.Primary);
      expect(component.newParamButton.buttonSize).toEqual(ButtonSize.Small);
    });

    it('should setColumns', () => {
      component['setColumns']();

      expect(component.columns[0].name).toEqual(component.i18n.labels.roomType);
      expect(component.columns[0].prop).toEqual('roomTypeName');
      expect(component.columns[1].name).toEqual(component.i18n.labels.occupationPercent);
      expect(component.columns[1].prop).toEqual('occupationPercent');
      expect(component.columns[2].name).toEqual(component.i18n.labels.adjustmentType);
      expect(component.columns[2].prop).toEqual('isDecreasedName');
      expect(component.columns[3].name).toEqual(component.i18n.labels.rateVariation);
      expect(component.columns[3].prop).toEqual('amount');
      expect(component.columns[4].name).toEqual(component.i18n.labels.actions);
      expect(component.columns[4].prop).toEqual('');
    });
  });

  describe('resources', () => {
    it('should getRoomTypeSelectList', () => {
      spyOn<any>(component['roomTypeResource'], 'getRoomTypesByPropertyId').and.returnValue(of({items: []}));

      component.getRoomTypeSelectList();

      expect(component['roomTypeResource'].getRoomTypesByPropertyId).toHaveBeenCalledWith(component.propertyId);
    });

    it('should getAdjustSelectList', () => {
      component.getAdjustSelectList();

      expect(component.adjustSelectList).toEqual([
        {key: true, value: component.i18n.labels.discount},
        {key: false, value: component.i18n.labels.increase}
      ]);
    });

    it('should getRateVariationSelectList', () => {
      component.getRateVariationSelectList();

      expect(component.rateVariationSelectList).toEqual([
        {key: VariationRate.percentage, value: component.i18n.labels.percentage},
        {key: VariationRate.currency, value: component.i18n.labels.real}
      ]);
    });
  });

  describe('update Row Value', () => {
    it('should updateRoomType', () => {
      component.roomTypeSelectList = [{key: 10, value: 'LUXO'}];
      component.updateRoomType(10, RATE_STRATEGY_ROOM_TYPE);

      expect(RATE_STRATEGY_ROOM_TYPE.roomTypeId).toEqual(10);
      expect(RATE_STRATEGY_ROOM_TYPE.roomTypeName).toEqual('LUXO');
    });

    it('should updateMin', () => {
      spyOn(component, 'paramRowIsValid');
      const event = {
        target: {
          value: '30'
        }
      };

      component.updateMin(event, RATE_STRATEGY_ROOM_TYPE);

      expect(RATE_STRATEGY_ROOM_TYPE.minOccupationPercentual).toEqual(30);
      expect(component.paramRowIsValid).toHaveBeenCalledWith(RATE_STRATEGY_ROOM_TYPE);
    });

    it('should updateMax', () => {
      spyOn(component, 'paramRowIsValid');
      const event = {
        target: {
          value: '100'
        }
      };

      component.updateMax(event, RATE_STRATEGY_ROOM_TYPE);

      expect(RATE_STRATEGY_ROOM_TYPE.maxOccupationPercentual).toEqual(100);
      expect(component.paramRowIsValid).toHaveBeenCalledWith(RATE_STRATEGY_ROOM_TYPE);
    });

    it('should updateAdjust', () => {

      component.updateAdjust('false', RATE_STRATEGY_ROOM_TYPE);

      expect(RATE_STRATEGY_ROOM_TYPE.isDecreased).toBeFalsy();
    });

    it('should updateRate', () => {

      component.updateRate('40', RATE_STRATEGY_ROOM_TYPE);

      expect(RATE_STRATEGY_ROOM_TYPE.variation).toEqual(40);
    });

    it('should updateAmount', () => {
      const event = {
        target: {
          value: '50'
        }
      };
      component.updateAmount(event, RATE_STRATEGY_ROOM_TYPE);

      expect(RATE_STRATEGY_ROOM_TYPE['amount']).toEqual(50);
    });
  });

  describe('actions', () => {
    it('should newParam', () => {
      spyOn(component, 'changeParams');
      component.paramsList = [RATE_STRATEGY_ROOM_TYPE];

      component.newParam();

      expect(component.paramsList.length).toEqual(2);
      expect(component.changeParams).toHaveBeenCalled();
    });

    it('should changeParams', () => {
      spyOn(component.paramsChanged$, 'emit');

      component.changeParams();

      expect(component.paramsChanged$.emit).toHaveBeenCalled();
    });

    it('should editRow', () => {
      spyOn(component, 'changeParams');

      component.editRow(RATE_STRATEGY_ROOM_TYPE);

      expect(RATE_STRATEGY_ROOM_TYPE.isEdit).toBeTruthy();
      expect(component.rowEditing).toEqual(RATE_STRATEGY_ROOM_TYPE);
      expect(component.changeParams).toHaveBeenCalled();
    });

    it('should saveRow', () => {
      spyOn(component['rateStrategyService'], 'mapperParamToTable');
      spyOn(component, 'changeParams');

      component.saveRow(RATE_STRATEGY_ROOM_TYPE);

      expect(component['rateStrategyService'].mapperParamToTable).toHaveBeenCalledWith(RATE_STRATEGY_ROOM_TYPE);
      expect(component.changeParams).toHaveBeenCalled();
    });

    it('should cancelRow#cancel editing', () => {
      spyOn(component, 'changeParams');
      component.rowEditing = RATE_STRATEGY_ROOM_TYPE;

      component.cancelRow(RATE_STRATEGY_ROOM_TYPE);

      expect(RATE_STRATEGY_ROOM_TYPE.isEdit).toBeFalsy();
      expect(component.rowEditing).toBeNull();
      expect(component.changeParams).toHaveBeenCalled();
    });

    it('should cancelRow#cancel creating', () => {
      component.paramsList = [RATE_STRATEGY_ROOM_TYPE];
      component.rowEditing = null;

      component.cancelRow(RATE_STRATEGY_ROOM_TYPE);

      expect(RATE_STRATEGY_ROOM_TYPE).not.toContain(RATE_STRATEGY_ROOM_TYPE);
    });

    it('should removeRow', () => {
      spyOn(component, 'changeParams');
      component.paramsList = [RATE_STRATEGY_ROOM_TYPE];

      component.removeRow(RATE_STRATEGY_ROOM_TYPE);

      expect(RATE_STRATEGY_ROOM_TYPE).not.toContain(RATE_STRATEGY_ROOM_TYPE);
      expect(component.changeParams).toHaveBeenCalled();
    });
  });

  describe('validation', () => {
    it('should paramRowIsValid', () => {
      spyOn(component['rateStrategyService'], 'rateStrategyRoomTypeIsValid').and.returnValue(true);

      const result = component.paramRowIsValid(RATE_STRATEGY_ROOM_TYPE);

      expect(result).toBeTruthy();
      expect(component['rateStrategyService'].rateStrategyRoomTypeIsValid).toHaveBeenCalledWith(RATE_STRATEGY_ROOM_TYPE);
    });

    it('should paramsListIsInvalid# there is no row like edit mode', () => {
      component.paramsList = [RATE_STRATEGY_ROOM_TYPE];

      const result = component.paramsListIsInvalid();

      expect(result).toBeFalsy();
    });

    it('should maxIsValid', () => {
      spyOn(component['rateStrategyService'], 'maxOccupationPercentual');

      const result = component.maxIsValid(RATE_STRATEGY_ROOM_TYPE);

      expect(component['rateStrategyService'].maxOccupationPercentual).toHaveBeenCalledWith(RATE_STRATEGY_ROOM_TYPE);
    });

    it('should minIsValid', () => {
      spyOn(component['rateStrategyService'], 'minOccupationPercentual');

      const result = component.minIsValid(RATE_STRATEGY_ROOM_TYPE);

      expect(component['rateStrategyService'].minOccupationPercentual).toHaveBeenCalledWith(RATE_STRATEGY_ROOM_TYPE);
    });

    it('should amountIsValid', () => {
      spyOn(component['rateStrategyService'], 'amountIsValid');

      const result = component.amountIsValid(RATE_STRATEGY_ROOM_TYPE);

      expect(component['rateStrategyService'].amountIsValid).toHaveBeenCalledWith(RATE_STRATEGY_ROOM_TYPE);
    });
  });
});
