import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { RateStrategyRoomType, VariationRate } from 'app/revenue-management/rate-strategy/models/rate-strategy';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { RateStrategyService } from 'app/revenue-management/rate-strategy/services/rate-strategy.service';
import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { ActivatedRoute } from '@angular/router';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';

@Component({
  selector: 'app-occupation-parameter-manager',
  templateUrl: './occupation-parameter-manager.component.html',
  styleUrls: ['./occupation-parameter-manager.component.css']
})
export class OccupationParameterManagerComponent implements OnInit, OnChanges {

  // Any
  public propertyId: number;

  // Translate
  public i18n: any;

  // Table Params
  public columns: any[];
  public newParamButton: ButtonConfig;
  public rowEditing: RateStrategyRoomType;

  // Select
  public roomTypeSelectList: any[];
  public adjustSelectList: any[];
  public rateVariationSelectList: any[];

  public currencyName: any;

  // Actions
  public paramsList: RateStrategyRoomType[] = [];
  @Input() params: RateStrategyRoomType[];
  @Output() paramsChanged$ = new EventEmitter();

  @ViewChild('actionsTemplate') actionsTemplate: ElementRef;
  @ViewChild('roomTypeSelect') roomTypeSelect: ElementRef;
  @ViewChild('occupation') occupation: ElementRef;
  @ViewChild('isDecreasedSelect') isDecreasedSelect: ElementRef;
  @ViewChild('amount') amount: ElementRef;

  constructor(
    private translate: TranslateService,
    private sharedService: SharedService,
    private rateStrategyService: RateStrategyService,
    private roomTypeResource: RoomTypeResource,
    private route: ActivatedRoute,
    private commomService: CommomService,
    private sessionService: SessionParameterService
  ) {
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.currencyName = this.sessionService.extractSelectedOptionOfPossibleValues(
      this.sessionService.getParameter( this.propertyId, ParameterTypeEnum.DefaultCurrency));
    if (this.currencyName) {
      this.currencyName = this.currencyName.title;
    }
    this.setTranslate();
    this.setButtonConfig();
    this.setColumns();
    this.getRoomTypeSelectList();
    this.getAdjustSelectList();
    this.getRateVariationSelectList();
  }

  ngOnChanges() {
    this.paramsList = [...this.params];
  }

  private setTranslate() {
    this.i18n = {
      title: this.translate.instant('label.parameters'),
      actions: {
        newParam: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.parameter')
        }),
      },
      labels: {
        actions: this.translate.instant('label.actions'),
        info: this.translate.instant('img.info'),
        roomType: this.translate.instant('label.roomType'),
        occupationPercent: this.translate.instant('label.occupationPercent'),
        adjustmentType: this.translate.instant('label.adjustmentType'),
        rateVariation: this.translate.instant('label.rateVariation'),
        until: this.translate.instant('text.until'),
        discount: this.translate.instant('label.discount'),
        increase: this.translate.instant('label.increase'),
        percentage: this.translate.instant('label.percentage'),
        real: this.translate.instant('label.value'),
      },
      table: {
        emptyMessage: {
          title: this.translate.instant('text.noneParameterInStrategy'),
          description: this.translate.instant('text.noneParameterInStrategyInsert')
        }
      },
    };
  }

  private setButtonConfig() {
    this.newParamButton = this.sharedService.getButtonConfig(
      'newParam',
      () => this.newParam()
      ,
      this.i18n.actions.newParam,
      ButtonType.Primary,
      ButtonSize.Small
    );
  }

  private setColumns() {
    this.columns = [
      {
        name: this.i18n.labels.roomType,
        prop: 'roomTypeName',
        cellTemplate: this.roomTypeSelect,
        cellClass: 'thf-u-padding-vertical--05'
      },
      {
        name: this.i18n.labels.occupationPercent,
        prop: 'occupationPercent',
        cellTemplate: this.occupation,
        cellClass: 'thf-u-padding-vertical--05'
      },
      {
        name: this.i18n.labels.adjustmentType,
        prop: 'isDecreasedName',
        cellTemplate: this.isDecreasedSelect,
        cellClass: 'thf-u-padding-vertical--05'
      },
      {
        name: this.i18n.labels.rateVariation,
        prop: 'amount',
        cellTemplate: this.amount,
        cellClass: 'thf-u-padding-vertical--05'
      },
      {
        name: this.i18n.labels.actions,
        prop: '',
        cellTemplate: this.actionsTemplate,
        cellClass: 'thf-u-padding-vertical--05',
        flexGrow: 0.6
      }
    ];
  }

  public getRoomTypeSelectList() {
    this.roomTypeResource.getRoomTypesByPropertyId(this.propertyId)
      .subscribe((items) => {
        if (items) {
          this.roomTypeSelectList = [
            {key: 0, value: this.translate.instant('label.entires')},
            ...this.commomService
              .convertObjectToSelectOptionObjectByIdAndValueProperties(items, 'id', 'abbreviation')
          ];
        }
      });
  }

  public getAdjustSelectList() {
    this.adjustSelectList = [
      {key: true, value: this.i18n.labels.discount},
      {key: false, value: this.i18n.labels.increase}
    ];
  }

  public getRateVariationSelectList() {
    this.rateVariationSelectList = [
      {key: VariationRate.percentage, value: this.i18n.labels.percentage},
      {key: VariationRate.currency, value: this.i18n.labels.real}
    ];
  }

  // Update Row Value
  public updateRoomType(event, row: RateStrategyRoomType) {
    if (event) {
      row.roomTypeId = event;
      const roomType = this.roomTypeSelectList.find(r => r.key == event);
      if (roomType) {
        row.roomTypeName = roomType.value;
      }
      this.paramRowIsValid(row);
    } else {
      row.roomTypeId = this.roomTypeSelectList[0].key;
      row.roomTypeName = this.roomTypeSelectList[0].value;
    }
  }

  public updateMin(event, row: RateStrategyRoomType) {
    if (event) {
      row.minOccupationPercentual = +event.target.value;
      this.paramRowIsValid(row);
    }
  }

  public updateMax(event, row: RateStrategyRoomType) {
    if (event) {
      row.maxOccupationPercentual = +event.target.value;
      this.paramRowIsValid(row);
    }
  }

  public updateAdjust(event, row: RateStrategyRoomType) {
    if (event) {
      row.isDecreased = (String(event) == 'true');
      this.paramRowIsValid(row);
    }
  }

  public updateRate(event, row: RateStrategyRoomType) {
    if (event) {
      row.variation = +event || VariationRate.percentage;
      this.paramRowIsValid(row);
    }
  }

  public updateAmount(event, row: RateStrategyRoomType) {
    if (event) {
      row['amount'] = +event.target.value;
      this.paramRowIsValid(row);
    }
  }

  // Actions
  public newParam() {
    if (this.paramsList) {
      const newParam = new RateStrategyRoomType();
      this.rateStrategyService.mapperParamToTable(newParam);
      newParam.isEdit = true;
      this.paramsList.push(newParam);
      this.paramsList = [...this.paramsList];
      this.changeParams();
    }
  }

  public changeParams() {
    this.paramsChanged$.emit(this.paramsList);
  }

  public editRow(row: RateStrategyRoomType) {
    row.isEdit = true;
    this.rowEditing = row;
    this.changeParams();
  }

  public saveRow(row: RateStrategyRoomType) {
    this.rateStrategyService.mapperParamToTable(row);
    this.changeParams();
  }

  public cancelRow(row: RateStrategyRoomType) {
    if (this.rowEditing) {
      row = this.rowEditing;
      row.isEdit = false;
      this.rowEditing = null;
    } else {
      this.removeRow(row);
    }
    this.changeParams();
  }

  public removeRow(row: RateStrategyRoomType) {
    if (this.paramsList) {
      const index: number = this.paramsList.indexOf(row);
      if (index !== -1) {
        this.paramsList.splice(index, 1);
        this.paramsList = [...this.paramsList];
      }
      this.changeParams();
    }
  }

  // Validation
  public paramRowIsValid(row: RateStrategyRoomType): boolean {
    return this.rateStrategyService.rateStrategyRoomTypeIsValid(row);
  }

  public paramsListIsInvalid(): boolean {
    return this.paramsList.some(param => param.isEdit);
  }

  public maxIsValid(row) {
    return this.rateStrategyService.maxOccupationPercentual(row);
  }

  public minIsValid(row) {
    return this.rateStrategyService.minOccupationPercentual(row);
  }

  public amountIsValid(row) {
    return this.rateStrategyService.amountIsValid(row);
  }

}
