import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateStrategyListComponent } from './rate-strategy-list.component';
import { RateStrategyResource } from 'app/revenue-management/rate-strategy/resources/rate-strategy-resource';
import { of } from 'rxjs';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RATE_STRATEGY_DATA, RATE_STRATEGY_LIST_DATA } from 'app/revenue-management/rate-strategy/mock/rate-strategy-mock';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('RateStrategyListComponent', () => {
  let component: RateStrategyListComponent;
  let fixture: ComponentFixture<RateStrategyListComponent>;

  let rateStrategyResource;
  let toasterEmitService;

  const rateStrategyResourceStub: Partial<RateStrategyResource> = {
    gelAllByPropertyId: () => of(<any>{}),
    updateStatus: () => of(<any>{})
  };
  const toasterEmitServiceStub: Partial<ToasterEmitService> = {
    emitChange: () => {}
  };
  const translateStubService: Partial<TranslateService> = {
    instant: () => {}
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [RateStrategyListComponent],
      providers: [
        {
          provide: RateStrategyResource,
          useValue: rateStrategyResourceStub
        },
        {
          provide: ToasterEmitService,
          useValue: toasterEmitServiceStub
        },
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        InterceptorHandlerService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
    rateStrategyResource = TestBed.get(RateStrategyResource);
    toasterEmitService = TestBed.get(ToasterEmitService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateStrategyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setHeaderConfig', () => {
      component['setHeaderConfig']();

      expect(component.configHeaderPage).toEqual({
        hasBackPreviewPage: true,
        keepTitleButton: true,
        callBackFunction: jasmine.any(Function),
      });
    });

    it('should setColumns', () => {
      component['setColumns']();

      expect(component.columns).toEqual([
        {name: 'label.strategies', prop: 'name'},
        {name: 'label.uhType', cellTemplate: component.tags},
        {name: 'label.duration', prop: 'duration'},
      ]);
    });
  });

  describe('actions', () => {
    it('should updateList', () => {
      component.rateStrategyList = RATE_STRATEGY_LIST_DATA;

      component.updateList('01');

      expect(component.filteredItems).toEqual([RATE_STRATEGY_DATA]);
    });

    it('should goToNewRateStrategy', () => {
      spyOn(component['router'], 'navigate');

      component.goToNewRateStrategy();

      expect(component['router'].navigate).toHaveBeenCalledWith(['new'], {relativeTo: component['route']});
    });

    it('should goToEditRateStrategy', () => {
      spyOn(component['router'], 'navigate');

      component.goToEditRateStrategy(RATE_STRATEGY_DATA);

      expect(component['router'].navigate).toHaveBeenCalledWith(['edit', RATE_STRATEGY_DATA.id], {relativeTo: component['route']});
    });
  });

  describe('resources', () => {
    it('should getRateStrategy', () => {
      spyOn(rateStrategyResource, 'gelAllByPropertyId').and.returnValue(of({items: []}));
      spyOn<any>(component, 'mapperToTable').and.returnValue([]);

      component.getRateStrategy();

      expect(rateStrategyResource.gelAllByPropertyId).toHaveBeenCalled();
      expect(component['mapperToTable']).toHaveBeenCalledWith([]);
      expect(component.rateStrategyList).toEqual([]);
      expect(component.filteredItems).toEqual([]);
    });

    it('should updateStatus#active', () => {
      spyOn(rateStrategyResource, 'updateStatus').and.returnValue(of(null));
      spyOn(toasterEmitService, 'emitChange');

      RATE_STRATEGY_DATA.isActive = false;
      component.updateStatus(RATE_STRATEGY_DATA);

      expect(rateStrategyResource.updateStatus).toHaveBeenCalledWith(RATE_STRATEGY_DATA.id);
      expect(toasterEmitService.emitChange)
        .toHaveBeenCalledWith(SuccessError.success, component.i18n.actions.activatedWithSuccess);
    });

    it('should updateStatus#inactive', () => {
      spyOn(rateStrategyResource, 'updateStatus').and.returnValue(of(null));
      spyOn(toasterEmitService, 'emitChange');

      RATE_STRATEGY_DATA.isActive = true;
      component.updateStatus(RATE_STRATEGY_DATA);

      expect(rateStrategyResource.updateStatus).toHaveBeenCalledWith(RATE_STRATEGY_DATA.id);
      expect(toasterEmitService.emitChange)
        .toHaveBeenCalledWith(SuccessError.success, component.i18n.actions.inactivatedWithSuccess);
    });
  });

});
