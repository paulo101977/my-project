import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { RateStrategy } from 'app/revenue-management/rate-strategy/models/rate-strategy';
import { RateStrategyResource } from 'app/revenue-management/rate-strategy/resources/rate-strategy-resource';
import { DateService } from 'app/shared/services/shared/date.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-rate-strategy-list',
  templateUrl: './rate-strategy-list.component.html',
  styleUrls: ['./rate-strategy-list.component.css']
})
export class RateStrategyListComponent implements OnInit {

  // Translate
  public i18n: any;

  // Header
  public configHeaderPage: ConfigHeaderPageNew;

  // Table
  public columns: any[];
  public rateStrategyList: RateStrategy[];
  public filteredItems: RateStrategy[];

  @ViewChild('tags') tags: ElementRef;

  constructor(
    private rateStrategyResource: RateStrategyResource,
    private dateService: DateService,
    private toasterEmitService: ToasterEmitService,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.setTranslate();
    this.setHeaderConfig();
    this.setColumns();
    this.getRateStrategy();
  }

  private setTranslate() {
    this.i18n = {
      title: this.translate.instant('title.rateStrategyConsult'),
      placeholder: this.translate.instant('placeholder.searchByStrategyNameDateRoomType'),
      table: {
        emptyMessage: {
          title: this.translate.instant('variable.emptyContentF.title', {
            labelName: this.translate.instant('label.rateStrategy')
          }),
          description: this.translate.instant('variable.emptyContentF.textDescription', {
            labelName: this.translate.instant('label.rateStrategy')
          })
        }
      },
      actions: {
        new: this.translate.instant('action.new'),
        activatedWithSuccess: this.translate.instant('variable.lbActivatedWithSuccessF', {
          labelName: this.translate.instant('label.rateStrategy')
        }),
        inactivatedWithSuccess: this.translate.instant('variable.lbInactivatedWithSuccessF', {
          labelName: this.translate.instant('label.rateStrategy')
        })
      }
    };
  }

  public setHeaderConfig() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
      keepTitleButton: true,
      callBackFunction: this.goToNewRateStrategy,
    };
  }

  private setColumns() {
    this.columns = [
      {name: 'label.strategies', prop: 'name'},
      {name: 'label.uhType', cellTemplate: this.tags},
      {name: 'label.duration', prop: 'duration'},
    ];
  }

  public goToNewRateStrategy = () => {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  public goToEditRateStrategy = (rateStrategy: RateStrategy) => {
    this.router.navigate(['edit', rateStrategy.id], {relativeTo: this.route});
  }

  public getRateStrategy() {
    this.rateStrategyResource.gelAllByPropertyId()
      .subscribe(response => {
        if (response.items) {
          this.rateStrategyList = this.filteredItems = this.mapperToTable(response.items);
        }
      });
  }

  public updateStatus(item: RateStrategy) {
    this.rateStrategyResource.updateStatus(item.id)
      .subscribe(() => {
        const message = item.isActive
          ? this.i18n.actions.activatedWithSuccess
          : this.i18n.actions.inactivatedWithSuccess;
        this.toasterEmitService.emitChange(SuccessError.success, message);
      });
  }

  private mapperToTable(items: RateStrategy[]) {
    return items.map((item: RateStrategy) => {
      item['duration'] = this.dateService
          .convertUniversalDateToViewFormat(item.startDate)
        + ' - '
        + this.dateService
          .convertUniversalDateToViewFormat(item.endDate);
      return item;
    });
  }

  public updateList(searchData: string) {
    this.filteredItems = this.rateStrategyList.filter(item => {
      return (
        item.name.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1
        || item.roomTypeAbbreviationList.some(roomType => {
          return roomType.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1;
        })
      );
    });
  }

}
