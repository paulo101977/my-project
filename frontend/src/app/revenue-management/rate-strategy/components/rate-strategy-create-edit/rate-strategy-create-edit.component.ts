import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RateStrategy, RateStrategyRoomType } from 'app/revenue-management/rate-strategy/models/rate-strategy';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { RateStrategyService } from 'app/revenue-management/rate-strategy/services/rate-strategy.service';
import {
  OccupationParameterManagerComponent
} from 'app/revenue-management/rate-strategy/components/occupation-parameter-manager/occupation-parameter-manager.component';
import { RateStrategyResource } from 'app/revenue-management/rate-strategy/resources/rate-strategy-resource';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';

@Component({
  selector: 'app-rate-strategy-create-edit',
  templateUrl: './rate-strategy-create-edit.component.html',
  styleUrls: ['./rate-strategy-create-edit.component.css']
})
export class RateStrategyCreateEditComponent implements OnInit {

  // Info
  private rateStrategyId: string;
  private rateStrategy: RateStrategy;

  // Translate
  public i18n: any;

  // Buttons
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;

  // Form
  public form: FormGroup;

  // Table Params
  public columns: any[];
  public paramsList: RateStrategyRoomType[] = [];
  public paramsToEditList: RateStrategyRoomType[] = [];
  public filteredItems: RateStrategyRoomType[] = [];
  public newParamButton: ButtonConfig;

  // Modal
  public modalTitle: string;
  public readonly MODAL_ID = 'params_modal';
  public cancelModalButton: ButtonConfig;
  public saveModalButton: ButtonConfig;

  public propertyId: string;
  public currencyName: any;

  @ViewChild('occupationParameter') occupationParameter: OccupationParameterManagerComponent;
  @ViewChild('amount') templateAmount: TemplateRef<any>;

  constructor(
    private translate: TranslateService,
    private sharedService: SharedService,
    private location: Location,
    private fb: FormBuilder,
    private modalToggle: ModalEmitToggleService,
    private rateStrategyService: RateStrategyService,
    private rateStrategyResource: RateStrategyResource,
    private route: ActivatedRoute,
    private router: Router,
    private toaster: ToasterEmitService,
    private sessionService: SessionParameterService
  ) {
  }

  ngOnInit() {
    this.rateStrategyId = this.route.snapshot.params.id;
    this.propertyId = this.route.snapshot.params.property;
    this.currencyName = this.sessionService.extractSelectedOptionOfPossibleValues(
      this.sessionService.getParameter( +this.propertyId, ParameterTypeEnum.DefaultCurrency));
    if (this.currencyName) {
      this.currencyName = this.currencyName.title;
    }
    this.setTranslate();
    this.setButtonConfig();
    this.setForm();
    this.setColumns();
    if (this.rateStrategyId) {
      this.getRateStrategy();
    }
    this.setModalTitle();
  }

  private setTranslate() {
    this.i18n = {
      title: this.translate.instant('action.lbNew', {
        labelName: this.translate.instant('label.occupationStrategy')
      }),
      titleModalParamNew: this.translate.instant('action.lbNew', {
        labelName: this.translate.instant('label.occupationParam')
      }),
      titleModalParamEdit: this.translate.instant('action.lbEdit', {
        labelName: this.translate.instant('label.occupationParam')
      }),
      groups: {
        general: {
          title: this.translate.instant('label.generalSettings'),
          label: {
            active: this.translate.instant('label.active'),
            name: this.translate.instant('label.name'),
            validityPeriod: this.translate.instant('label.validityPeriod'),
          },
        },
        params: {
          title: this.translate.instant('label.occupationStrategyParams'),
          label: {
            active: this.translate.instant('label.active'),
            name: this.translate.instant('label.name'),
            validityPeriod: this.translate.instant('label.validityPeriod'),
          },
          placeholder: `${this.translate.instant('action.search')} ${this.translate.instant('label.roomType')}`,
        }
      },
      actions: {
        cancel: this.translate.instant('action.cancel'),
        confirm: this.translate.instant('action.confirm'),
        newParam: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.parameter')
        }),
        editParam: this.translate.instant('action.lbEdit', {
          labelName: this.translate.instant('label.parameter')
        }),
        createStrategy: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.strategy')
        }),
        create: this.translate.instant('variable.lbSaveSuccessF', {
          labelName: this.translate.instant('label.rateStrategy')
        }),
        update: this.translate.instant('variable.lbEditSuccessF', {
          labelName: this.translate.instant('label.rateStrategy')
        })
      },
      required: this.translate.instant('validationError.required'),
      alertRateStrategy: this.translate.instant('alert.alertRateStrategy'),
      labels: {
        attention: this.translate.instant('label.attention'),
        info: this.translate.instant('img.info'),
        roomType: this.translate.instant('label.roomType'),
        occupationPercent: this.translate.instant('label.occupationPercent'),
        adjustmentType: this.translate.instant('label.adjustmentType'),
        rateVariation: this.translate.instant('label.rateVariation'),
      },
      table: {
        emptyMessage: {
          title: this.translate.instant('text.noneParameterInStrategy'),
          description: this.translate.instant('text.noneParameterInStrategyInsert')
        }
      },
    };
  }

  private setForm() {
    this.form = this.fb.group({
      isActive: [true, [Validators.required]],
      name: ['', [Validators.required]],
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]],
      id: [null],
    });

    this.form.valueChanges
      .subscribe(form => {
        this.confirmButtonConfig.isDisabled = !this.form.valid;
      });
  }

  private setDataToForm(rateStrategy: RateStrategy) {
    this.form.patchValue({
      isActive: rateStrategy.isActive,
      name: rateStrategy.name,
      startDate: rateStrategy.startDate,
      endDate: rateStrategy.endDate,
      id: rateStrategy.id,
    });
  }

  private setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      () => this.location.back(),
      this.i18n.actions.cancel,
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'save',
      () => this.saveStrategy(),
      this.i18n.actions.createStrategy,
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );

    this.cancelModalButton = this.sharedService.getButtonConfig(
      'cancelModal',
      () => this.cancelParamsList(),
      this.i18n.actions.cancel,
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.saveModalButton = this.sharedService.getButtonConfig(
      'saveModal',
      () => this.saveParamsList(),
      this.i18n.actions.confirm,
      ButtonType.Primary,
      ButtonSize.Normal,
    );
    this.setParamButtonConfig();
  }

  public saveStrategy() {
    this.rateStrategy = this.mapperToSave();

    if (this.rateStrategy) {
      const message = this.rateStrategy.id
        ? this.i18n.actions.update
        : this.i18n.actions.create;

      const resource = this.rateStrategy.id
        ? this.rateStrategyResource.update(this.rateStrategy)
        : this.rateStrategyResource.create(this.rateStrategy);

      const router = this.rateStrategy.id
        ? ['../../']
        : ['../'];

      resource
        .subscribe(() => {
          this.toaster.emitChange(SuccessError.success, message);
          this.router.navigate(router, { relativeTo: this.route});
        });
    }
  }

  public saveParam(newList) {
    this.paramsToEditList = newList;
    this.saveModalButton.isDisabled = this.occupationParameter.paramsListIsInvalid();
  }

  public cancelParamsList() {
    this.paramsList = [...this.paramsList];
    this.modalToggle.emitToggleWithRef(this.MODAL_ID);
    this.setParamButtonConfig();
    this.setModalTitle();
  }

  public saveParamsList() {
    this.filteredItems = this.paramsToEditList;
    this.paramsList = this.paramsToEditList;
    this.modalToggle.emitToggleWithRef(this.MODAL_ID);
    this.setParamButtonConfig();
    this.setModalTitle();
  }

  public newParam() {
    this.modalToggle.emitToggleWithRef(this.MODAL_ID);
  }

  private setColumns() {
    this.columns = [
      {name: this.i18n.labels.roomType, prop: 'roomTypeName'},
      {name: this.i18n.labels.occupationPercent, prop: 'occupationPercent'},
      {name: this.i18n.labels.adjustmentType, prop: 'isDecreasedName'},
      {name: this.i18n.labels.rateVariation, prop: 'amountView', cellTemplate: this.templateAmount},
    ];
  }

  public getRateStrategy() {
    this.rateStrategyResource.getById(this.rateStrategyId)
      .subscribe(result => {
        this.rateStrategy = result;
        this.setDataToForm(result);
        this.filteredItems = this.paramsToEditList = this.paramsList = this.rateStrategyService
          .mapperParamsToTable(this.rateStrategy.propertyRateStrategyRoomTypeList);
        this.setParamButtonConfig();
        this.setModalTitle();
      });
  }

  public updateList(searchData: string) {
    this.filteredItems = this.paramsList.filter(item => {
      return item.roomTypeName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1;
    });
  }

  private hasParams() {
    return this.paramsList && this.paramsList.length;
  }

  private setParamButtonConfig() {
    this.newParamButton = this.sharedService.getButtonConfig(
      this.hasParams() ? 'editParam' : 'newParam',
      () => this.modalToggle.emitToggleWithRef(this.MODAL_ID),
      this.hasParams() ? this.i18n.actions.editParam : this.i18n.actions.newParam,
      ButtonType.Secondary,
      ButtonSize.Small
    );
  }

  private setModalTitle() {
    this.modalTitle = this.hasParams()
      ? this.i18n.titleModalParamEdit
      : this.i18n.titleModalParamNew;
  }

  private mapperToSave() {
    const rateStrategy = <RateStrategy>this.form.getRawValue();
    rateStrategy.propertyRateStrategyRoomTypeList = this.paramsList;
    return rateStrategy;
  }
}
