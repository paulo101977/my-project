import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateStrategyCreateEditComponent } from './rate-strategy-create-edit.component';
import { ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { Component, CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ThxErrorFormModule } from '@inovacao-cmnet/thx-ui';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { DatepickerComponent } from 'app/shared/components/datepicker/datepicker.component';
import { RATE_STRATEGY_DATA, RATE_STRATEGY_ROOM_TYPE } from 'app/revenue-management/rate-strategy/mock/rate-strategy-mock';
import { of } from 'rxjs';
import { RateStrategyResource } from 'app/revenue-management/rate-strategy/resources/rate-strategy-resource';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

xdescribe('RateStrategyCreateEditComponent', () => {
  let component: RateStrategyCreateEditComponent;
  let fixture: ComponentFixture<RateStrategyCreateEditComponent>;

  let rateStrategyResource;

  const locationStub: Partial<Location> = {
    back: () => {
    }
  };

  @Pipe({name: 'imgCdn'})
  class ImgCdnPipeStub implements PipeTransform {
    transform(value: any, ...args: any[]): any {
    }
  }

  @Component({selector: 'app-occupation-parameter-manager', template: ''})
  class OccupationParameterManagerStubComponent {
    public paramsListIsInvalid() {
      return true;
    }
  }

  const rateStrategyResourceStub: Partial<RateStrategyResource> = {
    getById: () => of(<any>{}),
    create: () => of(<any>{}),
    update: () => of(<any>{}),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MyDatePickerModule,
        RouterTestingModule,
        HttpClientTestingModule,
        ThxErrorFormModule,
        TranslateTestingModule,
      ],
      declarations: [
        RateStrategyCreateEditComponent,
        DatepickerComponent,
        ImgCdnPipeStub,
        OccupationParameterManagerStubComponent
      ],
      providers: [
        {
          provide: RateStrategyResource,
          useValue: rateStrategyResourceStub
        },
        {
          provide: Location,
          useValue: locationStub
        },
        FormBuilder,
        InterceptorHandlerService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
    rateStrategyResource = TestBed.get(RateStrategyResource);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateStrategyCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setButtonConfig', () => {
      component['setButtonConfig']();

      expect(component.cancelButtonConfig.id).toEqual('cancel');
      expect(component.cancelButtonConfig.textButton).toEqual(component.i18n.actions.cancel);
      expect(component.cancelButtonConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.cancelButtonConfig.buttonSize).toEqual(ButtonSize.Normal);

      expect(component.confirmButtonConfig.id).toEqual('save');
      expect(component.confirmButtonConfig.textButton).toEqual(component.i18n.actions.createStrategy);
      expect(component.confirmButtonConfig.buttonType).toEqual(ButtonType.Primary);
      expect(component.confirmButtonConfig.buttonSize).toEqual(ButtonSize.Normal);
      expect(component.confirmButtonConfig.isDisabled).toBeTruthy();

      expect(component.newParamButton.id).toEqual('newParam');
      expect(component.newParamButton.textButton).toEqual(component.i18n.actions.newParam);
      expect(component.newParamButton.buttonType).toEqual(ButtonType.Secondary);
      expect(component.newParamButton.buttonSize).toEqual(ButtonSize.Small);

      expect(component.cancelModalButton.id).toEqual('cancelModal');
      expect(component.cancelModalButton.textButton).toEqual(component.i18n.actions.cancel);
      expect(component.cancelModalButton.buttonType).toEqual(ButtonType.Secondary);
      expect(component.cancelModalButton.buttonSize).toEqual(ButtonSize.Normal);

      expect(component.saveModalButton.id).toEqual('saveModal');
      expect(component.saveModalButton.textButton).toEqual(component.i18n.actions.confirm);
      expect(component.saveModalButton.buttonType).toEqual(ButtonType.Primary);
      expect(component.saveModalButton.buttonSize).toEqual(ButtonSize.Normal);
    });

    it('should setForm', () => {
      component['setForm']();

      expect(component.form.contains('isActive')).toBeTruthy();
      expect(component.form.contains('name')).toBeTruthy();
      expect(component.form.contains('startDate')).toBeTruthy();
      expect(component.form.contains('endDate')).toBeTruthy();
    });

    it('should setColumns', () => {
      component['setColumns']();

      expect(component.columns[0].name).toEqual(component.i18n.labels.roomType);
      expect(component.columns[0].prop).toEqual('roomTypeName');
      expect(component.columns[1].name).toEqual(component.i18n.labels.occupationPercent);
      expect(component.columns[1].prop).toEqual('occupationPercent');
      expect(component.columns[2].name).toEqual(component.i18n.labels.adjustmentType);
      expect(component.columns[2].prop).toEqual('isDecreasedName');
      expect(component.columns[3].name).toEqual(component.i18n.labels.rateVariation);
      expect(component.columns[3].prop).toEqual('amountView');
    });
  });

  describe('resources', () => {
    it('should getRateStrategy', () => {
      spyOn(rateStrategyResource, 'getById').and
        .returnValue(of(RATE_STRATEGY_DATA));
      spyOn(component['rateStrategyService'], 'mapperParamsToTable').and
        .returnValue(RATE_STRATEGY_DATA.propertyRateStrategyRoomTypeList);

      component.getRateStrategy();

      expect(rateStrategyResource.getById)
        .toHaveBeenCalledWith(component['rateStrategyId']);
      expect(component['rateStrategyService'].mapperParamsToTable)
        .toHaveBeenCalledWith(component['rateStrategy'].propertyRateStrategyRoomTypeList);
      expect(component.filteredItems[0]).toEqual(RATE_STRATEGY_ROOM_TYPE);
    });

    it('should saveStrategy', () => {
      // TODO to implement
    });
  });

  describe('actions', () => {
    it('should updateList#filter found', () => {
      component.paramsList = [RATE_STRATEGY_ROOM_TYPE];

      component.updateList('Lux');

      expect(component.filteredItems.length).toEqual(1);
    });

    it('should updateList#filter not found', () => {
      component.paramsList = [RATE_STRATEGY_ROOM_TYPE];

      component.updateList('AA');

      expect(component.filteredItems.length).toEqual(0);
    });

    it('should newParam', () => {
      spyOn(component['modalToggle'], 'emitToggleWithRef');

      component.newParam();

      expect(component['modalToggle'].emitToggleWithRef).toHaveBeenCalled();
    });

    it('should saveParam', () => {
      spyOn(component.occupationParameter, 'paramsListIsInvalid').and.returnValue(true);

      component.saveParam([RATE_STRATEGY_ROOM_TYPE]);

      expect(component.saveModalButton.isDisabled).toBeTruthy();
      expect(component.paramsToEditList).toEqual([RATE_STRATEGY_ROOM_TYPE]);
      expect(component.occupationParameter.paramsListIsInvalid).toHaveBeenCalled();
    });

    it('should cancelParamsList', () => {
      spyOn(component['modalToggle'], 'emitToggleWithRef');
      spyOn<any>(component, 'setParamButtonConfig');
      spyOn<any>(component, 'setModalTitle');

      component.cancelParamsList();

      expect(component['modalToggle'].emitToggleWithRef).toHaveBeenCalledWith(component.MODAL_ID);
      expect(component['setParamButtonConfig']).toHaveBeenCalled();
      expect(component['setModalTitle']).toHaveBeenCalled();
      expect(component.paramsToEditList).toEqual(component.paramsList);
    });

    it('should saveParamsList', () => {
      spyOn(component['modalToggle'], 'emitToggleWithRef');
      spyOn<any>(component, 'setParamButtonConfig');
      spyOn<any>(component, 'setModalTitle');

      component.saveParamsList();

      expect(component['modalToggle'].emitToggleWithRef).toHaveBeenCalledWith(component.MODAL_ID);
      expect(component['setParamButtonConfig']).toHaveBeenCalled();
      expect(component['setModalTitle']).toHaveBeenCalled();
      expect(component.filteredItems).toEqual(component.paramsToEditList);
      expect(component.paramsList).toEqual(component.paramsToEditList);
    });
  });
});
