import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetsService } from '@app/core/services/assets.service';
import { PopoverOption, RateScheduleCell, RateScheduleLabels, ThxRateScheduleComponent } from '@inovacao-cmnet/thx-ui';
import { TranslateService } from '@ngx-translate/core';
import { PremiseRateModalEditComponent } from '../premise-rate-modal-edit/premise-rate-modal-edit.component';
import { ButtonBarConfig } from 'app/shared/components/button-bar/button-bar-config';
import { AutocompleteConfig } from 'app/shared/models/autocomplete/autocomplete-config';
import { Client } from 'app/shared/models/client';
import { ConfigHeaderPage } from 'app/shared/models/config-header-page';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { RateCalendarDayDto } from 'app/shared/models/revenue-management/rate-calendar-day-dto';
import { RateCalendarFilter } from 'app/shared/models/revenue-management/rate-calendar-filter';
import { RightButtonTop } from 'app/shared/models/right-button-top';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { ClientResource } from 'app/shared/resources/client/client.resource';
import { RateCalendarResource } from 'app/shared/resources/rate-calendar/rate-calendar.resource';
import { RatePlanResource } from 'app/shared/resources/rate-plan/rate-plan.resource';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { PropertyService } from 'app/shared/services/property/property.service';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { ValidatorFormService } from 'app/shared/services/shared/validator-form.service';
import { DayPilot } from 'daypilot-pro-angular';
import { ChildTextConfig } from '@inovacao-cmnet/thx-ui';
import { LevelRateModalEditComponent } from '../../containers/level-rate-modal-edit/level-rate-modal-edit.component';
import { DayDetailComponent } from '../day-detail/day-detail.component';
import { Currency } from '@app/shared/models/currency';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
})
export class CalendarComponent implements OnInit {
  @ViewChild(DayDetailComponent) public dayDetailComponent: DayDetailComponent;
  @ViewChild(ThxRateScheduleComponent) public rateScheduleComponent: ThxRateScheduleComponent;
  @ViewChild('modalLevelEdit') modalLevelEdit: LevelRateModalEditComponent;
  @ViewChild('modalPremiseEdit') modalPremiseEdit: PremiseRateModalEditComponent;

  // Filter Items
  public propertyId: number;
  public calendarFilter: RateCalendarFilter;
  public configHeaderPage: ConfigHeaderPage;
  public calendarFilterForm: FormGroup;
  public buttonBarConfig: ButtonBarConfig;
  public filterDate: string;
  private defaultCurrency: string;

  public calendarDate: DayPilot.Date;

  // Filter Lists
  public currencyItemList: Currency[];
  public currencyList: SelectObjectOption[];
  public ratePlanList: SelectObjectOption[];
  public mealPlanTypeList: SelectObjectOption[];
  public roomTypeList: any[];
  public propertyParameterList: SelectObjectOption[];

  public autocompleteClientConfig: AutocompleteConfig;
  public autoCompleteClientResultList: any[];

  public dayList: RateScheduleCell[];
  public popoverOptionList: PopoverOption[];
  public labels: RateScheduleLabels;
  public topButtonsListConfig: RightButtonTop[];
  public maxAges: number;
  public childTextConfig: ChildTextConfig;

  public selectedDay: any;
  public startDateSelected: string;
  public endDateSelected: string;

  // Modal Data
  public modalMealType: string;


  constructor(
    public translateSvc: TranslateService,
    public router: Router,
    public route: ActivatedRoute,
    public dateService: DateService,
    public commonService: CommomService,
    public formBuilder: FormBuilder,
    public rateCalendarResource: RateCalendarResource,
    public sessionParameterService: SessionParameterService,
    public currencyService: CurrencyService,
    public validatorFormService: ValidatorFormService,
    public clientResource: ClientResource,
    public translateService: TranslateService,
    public ratePlanResource: RatePlanResource,
    public location: Location,
    private assetsService: AssetsService,
    private propertyService: PropertyService
  ) {}

  ngOnInit() {
    this.setVars();
    this.setTopButtonsListConfig();
    this.setFilterForm();
    this.fetchParamData();
    this.loadFilters();
  }

  public setVars() {
    this.propertyId = this.route.snapshot.params.property;
    this.maxAges =  this.propertyService.getChildAgeOfProperty(this.propertyId).length - 1;
    this.defaultCurrency = this.sessionParameterService.getParameterValue(this.propertyId, ParameterTypeEnum.DefaultCurrency);

    const labels = [
      'label.showDetail',
      'label.insertBaseRate',
      'label.stopSales',
      'label.noRate',
      'label.overbook',
      'label.occupPercentual',
      'label.today',
      'label.applyLevelRate',
      'label.applyPremiseRate'
    ];

    this.translateSvc.get(labels).subscribe(data => {
      this.popoverOptionList = [
        {
          label: data['label.showDetail'],
          callback: (start, end) => this.showDetail(start, end),
          multipleCells: false,
        },
        {
          label: data['label.insertBaseRate'],
          callback: (start, end) => this.changeBaseRate(start, end),
          multipleCells: true,
        },
        {
          label: data['label.applyLevelRate'],
         callback: (start, end) => this.applyLevelRate(start, end),
          multipleCells: true,
        },
        {
           label: data['label.applyPremiseRate'],
           callback: (start, end) => this.applyPremiseRate(start, end),
           multipleCells: true,
        }/* TODO: ,
        {
          label: data['label.stopSales'],
          callback: (start, end) => this.stopSales(start, end),
          multipleCells: true
        }*/
      ];

      this.labels = {
        noRate: data['label.noRate'],
        overbook: data['label.overbook'],
        occupation: data['label.occupPercentual'],
        today: data['label.today'],
      };
    });

    this.configChildText();
  }

  private setTopButtonsListConfig(): void {
    this.topButtonsListConfig = [
      // TODO: Descomentar quando restrição tarifária estiver disponível
      /*{
        title: 'label.tariffRestriction',
        iconDefault: this.assetsService.getImgUrlTo('lock-outline.svg'),
        iconAlt: 'label.statement',
        callback: () => this.router.navigate(['p', this.propertyId, 'rm', 'tariff-restriction'])
      },*/
      {
        title: 'label.mealType',
        iconDefault: this.assetsService.getImgUrlTo('tipo-pensao-icon.min.svg'),
        iconAlt: 'label.statement',
        callback: () => this.router.navigate(['p', this.propertyId, 'rm', 'meal-plan-type']),
      },
      {
        title: 'label.baseRate',
        iconDefault: this.assetsService.getImgUrlTo('tarifa.svg'),
        iconAlt: 'label.baseRate',
        callback: () => this.router.navigate(['p', this.propertyId, 'rm', 'base-rate']),
      },
      {
        title: 'title.ratePlan',
        iconDefault: this.assetsService.getImgUrlTo('swap-horiz.min.svg'),
        iconAlt: 'title.ratePlan',
        callback: () => this.router.navigate(['p', this.propertyId, 'rm', 'rate-plan']),
      },
    ];
  }

  private configChildText() {
    this.childTextConfig = new ChildTextConfig();
    this.childTextConfig.title = this.translateService.instant('title.childList');
    this.childTextConfig.cancelButton = this.translateService.instant('action.cancel');
    this.childTextConfig.okButton = 'ok';
  }

  private setFilterForm() {
    const today = new DayPilot.Date().getDatePart().toDateLocal();
    this.calendarFilterForm = this.formBuilder.group({
      date: [today, [Validators.required]],
      adultQuantity: [
        0,
        [Validators.required, this.validatorFormService.capacityNumberIsMajorThanOne, this.validatorFormService.setNumberToZero],
      ],
      childAges: [null],
      roomType: ['', [Validators.required]],
      clientName: [''],
      clientId: [''],
      ratePlan: [{ value: '', disabled: true }],
      currency: ['', [Validators.required]],
      mealType: ['', [Validators.required]],
    });

    this.calendarFilterForm.patchValue({ adultQuantity: 1 }); // necessary because "setNumberToZero" throws error if I start with 1

    this.filterDate = today.toISOString();
  }

  private fetchParamData() {
    const queryParams = <RateCalendarFilter>this.route.snapshot.queryParams;

    if (Object.keys(queryParams).length > 0) {
      const date = new DayPilot.Date(queryParams.date).getDatePart().toDateLocal();
      this.calendarFilterForm.patchValue({
        date: date,
        adultQuantity: queryParams.adultCount,
        childAges: queryParams.childrenAges,
        roomType: queryParams.roomTypeId,
        clientName: queryParams.companyClientName,
        clientId: queryParams.companyClientId,
        ratePlan: queryParams.ratePlanId,
        currency: queryParams.currencyId,
        mealType: queryParams.mealPlanTypeId,
      });
    }
  }

  private loadFilters() {
    this.rateCalendarResource.getConfigurationDataParameters(this.propertyId).subscribe(calendarParameters => {
      this.setMealPlanTypeList(calendarParameters.mealPlanTypeList);
      this.setCurrencyList(calendarParameters.currencyList);
      this.setRoomTypeList(calendarParameters.roomTypeList);
      this.setPropertyParameterList(calendarParameters.propertyParameterList);
      this.setRatePlanList();

      if (this.calendarFilterForm.valid) {
        this.filter(this.calendarFilterForm);
      }
    });

    this.autocompleteClientConfig = new AutocompleteConfig();
    this.autocompleteClientConfig.dataModel = 'clientName';
    this.autocompleteClientConfig.resultList = [];
    this.autocompleteClientConfig.callbackToSearch = $event => this.searchClient($event);
    this.autocompleteClientConfig.callbackToGetSelectedValue = data => this.setClient(data);
    this.autocompleteClientConfig.fieldObjectToDisplay = 'tradeName';
    this.translateService.get('placeholder.searchClientByDocuments').subscribe(value => {
      this.autocompleteClientConfig.placeholder = value;
    });
  }

  private setMealPlanTypeList(mealPlanTypeList: any) {
    this.mealPlanTypeList = mealPlanTypeList.items.map(mealPlanType => {
      return { key: mealPlanType.mealPlanTypeId, value: mealPlanType.propertyMealPlanTypeName || mealPlanType.mealPlanTypeName };
    });

    this.calendarFilterForm.get('mealType').setValue(this.mealPlanTypeList[0].key);
  }

  private setCurrencyList(currencyList: any) {
    this.currencyItemList = currencyList.items;
    this.currencyList = this.commonService.toOption(currencyList, 'id', 'currencyName');
    const hasDefaultCurrency = this.currencyList.find(currency => currency.key === this.defaultCurrency);

    if (hasDefaultCurrency) {
      this.calendarFilterForm.get('currency').setValue(hasDefaultCurrency.key);
    }
  }

  private setRoomTypeList(roomTypeList: any) {
    this.roomTypeList = this.commonService.toOption(roomTypeList, 'id', 'abbreviation');
    this.calendarFilterForm.get('roomType').setValue(this.roomTypeList[0].key);
  }

  private setRatePlanList() {
    this.ratePlanResource.getAll(this.propertyId, { companyClientId: this.calendarFilterForm.get('clientId').value }).subscribe(data => {
      this.ratePlanList = this.commonService.toOption(data, 'id', 'agreementName');

      if (!this.ratePlanList.some(ratePlan => ratePlan.key == this.calendarFilterForm.get('ratePlan').value)) {
        this.calendarFilterForm.get('ratePlan').setValue('');
      }

      if (this.ratePlanList && this.ratePlanList.length > 0) {
        this.calendarFilterForm.get('ratePlan').enable();
      } else {
        this.calendarFilterForm.get('ratePlan').disable();
      }
    });
  }

  private setPropertyParameterList(propertyParameterList: any) {
    this.propertyParameterList = propertyParameterList;
  }

  private searchClient($event: any) {
    const query: string = $event.query;
    if (query && query.length >= 3) {
      this.clientResource.getClientsBySearchData(this.propertyId, query).subscribe(data => (this.autoCompleteClientResultList = data));
    }
  }

  private setClient(data: Client) {
    this.calendarFilterForm.get('clientId').setValue(data.id);
    this.setRatePlanList();
  }

  public filter({ value, valid }: { value: any; valid: boolean }, event? ) {
    if (event) {
      event.preventDefault();
    }
    if (valid) {
      this.calendarDate = new DayPilot.Date(value.date).getDatePart();

      const initialDate = this.calendarDate.addDays(1 - this.calendarDate.getDayOfWeek()).toString();
      const finalDate = this.calendarDate.addDays(28 - this.calendarDate.getDayOfWeek()).toString();

      this.calendarFilter = {
        date: this.calendarDate.toString(),
        initialDate: initialDate,
        finalDate: finalDate,
        adultCount: value.adultQuantity,
        childrenAges: value.childAges ? value.childAges.childrenAges : null,
        roomTypeId: value.roomType,
        currencyId: value.currency,
        mealPlanTypeId: value.mealType,
        ratePlanId: value.ratePlan,
        companyClientId: value.clientId,
        companyClientName: value.clientName,
      };

      this.getCalendarData(value.mealType);
    }
  }

  public getCalendarData(mealType) {
    this.rateCalendarResource.getCalendar(this.propertyId, this.calendarFilter).subscribe(data => {
      this.dayList = this.mapCalendarCells(data.items);

      this.modalMealType = this.mealPlanTypeList.find(mealPlan => mealPlan.key == mealType).value;
      this.selectedDay = this.dayList.find(day => day.date == this.calendarFilter.initialDate);
    });
  }

  public reloadCalendar() {
    const initialDate = this.calendarDate.addDays(1 - this.calendarDate.getDayOfWeek()).toString();
    const finalDate = this.calendarDate.addDays(28 - this.calendarDate.getDayOfWeek()).toString();
    const mealType = this.calendarFilterForm.get('mealType').value;

    this.calendarFilter = {
      date: this.calendarDate.toString(),
      initialDate: initialDate,
      finalDate: finalDate,
      adultCount: this.calendarFilterForm.get('adultQuantity').value,
      childrenAges: null,
      roomTypeId: this.calendarFilterForm.get('roomType').value,
      currencyId: this.calendarFilterForm.get('currency').value,
      mealPlanTypeId: mealType,
      ratePlanId: this.calendarFilterForm.get('ratePlan').value,
      companyClientId: '',
      companyClientName: null,
    };

    this.getCalendarData(mealType);
  }

  public onInitialDateChange($event: DayPilot.Date) {
    const date = {
      date: {
        year: $event.getYear(),
        month: $event.getMonth() + 1,
        day: $event.getDay(),
      },
      jsdate: $event.toDateLocal(),
    };

    this.calendarFilterForm.get('date').setValue(date);
    this.filter(this.calendarFilterForm);
  }

  private mapCalendarCells(dayList: RateCalendarDayDto[]) {
    return dayList.map(day => {
      const currentValue = day.baseRateWithRatePlanAmount ? day.baseRateWithRatePlanAmount : day.baseRateAmount;
      const oldValue = day.baseRateWithRatePlanAmount ? day.baseRateAmount : null;

      return {
        date: day.dateFormatted,
        oldValue: oldValue,
        oldValueString: oldValue && this.currencyService.formatCurrencyToString(oldValue, day.currencySymbol),
        currentValue: currentValue,
        currentValueString: currentValue && this.currencyService.formatCurrencyToString(currentValue, day.currencySymbol),
        occupation: day.occupation,
        occupiedRooms: day.occupiedRooms,
        roomTotal: day.roomTotal,
        levelCode: day.levelCode,
        isLevel: day.isLevel,
      };
    });
  }

  private showDetail(start, end) {
    this.dayDetailComponent.openModal(start.toString());
  }

  private changeBaseRate(start: DayPilot.Date, end: DayPilot.Date) {
    this.router.navigate([], { queryParams: this.calendarFilter }).then(() => {
      const baseRateParams = {
        startDate: start,
        endDate: end.addDays(-1),
        rateCurrency: this.calendarFilterForm.get('currency').value,
        mealType: this.calendarFilterForm.get('mealType').value,
        returnUrl: this.location.path(),
      };

      this.router.navigate(['p', this.propertyId, 'rm', 'base-rate'], { queryParams: baseRateParams });
    });
  }

  private applyLevelRate(start: DayPilot.Date, end: DayPilot.Date) {
    this.startDateSelected = start['value'];
    this.endDateSelected = (end.addDays(-1))['value'];

    this.modalLevelEdit.toggleModal(this.startDateSelected, this.endDateSelected, this.calendarFilterForm.get('currency').value);
  }

  private applyPremiseRate(start: DayPilot.Date, end: DayPilot.Date) {
    this.startDateSelected = start['value'];
    this.endDateSelected = (end.addDays(-1))['value'];
    const currencyId = this.calendarFilterForm.get('currency');

    if (this.currencyItemList && currencyId) {
      const currency = this.currencyItemList.find(x => x.id == currencyId.value);
      this.modalPremiseEdit.toggleModal(this.startDateSelected, this.endDateSelected, currency);
    }
  }

  private stopSales(start, end) {
    console.log('stopSales', start, end);
  }

  public goToday() {
    this.rateScheduleComponent.goToday();
  }
}
