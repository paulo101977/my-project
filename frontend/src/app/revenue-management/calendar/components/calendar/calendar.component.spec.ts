import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'ngx-moment';
import { CalendarComponent } from './calendar.component';
import { MyDatePickerModule } from 'mydatepicker';
import { ErrorComponent } from '@app/shared/components/error/error.component';
import { CurrencyFormatPipe } from '@app/shared/pipes/currency-format.pipe';
import { DatepickerComponent } from '@app/shared/components/datepicker/datepicker.component';
import { CoreModule } from '../../../../core/core.module';
import { ThxRateScheduleModule, ThxChildSelectModule } from '@inovacao-cmnet/thx-ui';
import {DayPilot} from 'daypilot-pro-angular';
import {configureTestSuite} from 'ng-bullet';

describe('CalendarComponent', () => {
  let component: CalendarComponent;
  let fixture: ComponentFixture<CalendarComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarComponent, ErrorComponent, DatepickerComponent],
      imports: [
        CommonModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        MyDatePickerModule,
        HttpClientTestingModule,
        MomentModule,
        CoreModule,
        ThxRateScheduleModule,
        ThxChildSelectModule
      ],
      providers: [CurrencyFormatPipe],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(CalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reloadCalendar', () => {
    component.calendarDate = new DayPilot.Date(new Date()).getDatePart();
    component.calendarFilterForm.get('mealType').setValue(4);

    spyOn(component, 'getCalendarData');
    component.reloadCalendar();
    expect(component.getCalendarData).toHaveBeenCalled();
    expect(component.getCalendarData).toHaveBeenCalledWith(4);
  });
});
