import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { configureTestSuite } from 'ng-bullet';
import { createServiceStub } from 'testing';

import { PremiseRateModalEditComponent } from './premise-rate-modal-edit.component';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'ngx-moment';
import { ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PremiseResource } from '@app/revenue-management/premises/resources/premise.resource';
import { BaseRateResource } from '@app/shared/resources/base-rate/base-rate.resource';
import { of } from 'rxjs';

describe('PremiseRateModalEditComponent', () => {
  let component: PremiseRateModalEditComponent;
  let fixture: ComponentFixture<PremiseRateModalEditComponent>;

  let modalToggleService: ModalEmitToggleService;

  const modalEmitToggleProvider = createServiceStub(ModalEmitToggleService, {
    emitToggleWithRef(modalId: any): void {}
  });

  const premiseResourceProvider = createServiceStub(PremiseResource, {});
  const baseRateResourceProvider = createServiceStub(BaseRateResource, {
    getConfigurationDataParameters: (propertyId: number) => of( null ),
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiseRateModalEditComponent ],
      providers: [
        modalEmitToggleProvider,
        FormBuilder,
        premiseResourceProvider,
        baseRateResourceProvider
      ],
      imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        MomentModule,
        HttpClientTestingModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    modalToggleService = TestBed.get(ModalEmitToggleService);

    fixture = TestBed.createComponent(PremiseRateModalEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggleModal', () => {
    spyOn<any>(component, 'loadData').and.callFake( f => f);
    spyOn(modalToggleService, 'emitToggleWithRef');
    component.toggleModal(
        '2019-01-10T00:00:00',
        '2019-01-11T00:00:00',
        'c5ef2cfa-a05e-44ed-ad2e-089b26471432'
    );

    expect(component.initialDate).toEqual('2019-01-10T00:00:00');
    expect(component.endDate).toEqual('2019-01-11T00:00:00');
    expect(component.currency).toEqual('c5ef2cfa-a05e-44ed-ad2e-089b26471432');
    expect(component.loadData).toHaveBeenCalled();
    expect(modalToggleService.emitToggleWithRef).toHaveBeenCalled();
  });
});
