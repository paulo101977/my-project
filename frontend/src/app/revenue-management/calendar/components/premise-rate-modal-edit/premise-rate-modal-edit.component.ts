import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ButtonConfig, SharedService, ButtonSize, ButtonType } from '@inovacao-cmnet/thx-ui';
import { PremiseResource } from '@app/revenue-management/premises/resources/premise.resource';
import { PropertyPremiseHeader } from '@app/revenue-management/premises/models/property-premise-header';
import { Option } from '@app/shared/models/option';
import { CommomService } from '@app/shared/services/shared/commom.service';
import { Parameter } from '@app/shared/models/parameter';
import {
  RoomTypeParametersConfiguration
} from '@app/shared/models/revenue-management/room-type-parameters-configuration';
import { BaseRateResource } from '@app/shared/resources/base-rate/base-rate.resource';
import { ActivatedRoute, Router } from '@angular/router';
import { PremiseRateService } from '@app/revenue-management/base-rate/services/premise-rate.service';
import { BaseRateType } from '@app/revenue-management/base-rate/models/base-rate-type';
import { PropertyRate } from '@app/shared/models/revenue-management/property-base-rate';
import { ConfirmModalDataConfig } from '@app/shared/models/confirm-modal-data-config';
import { SuccessError } from '@app/shared/models/success-error-enum';
import { ToasterEmitService } from '@app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

@Component({
  selector: 'app-premise-rate-modal-edit',
  templateUrl: './premise-rate-modal-edit.component.html',
  styleUrls: ['./premise-rate-modal-edit.component.css']
})
export class PremiseRateModalEditComponent implements OnInit {

  @Output() reloadCalendar = new EventEmitter();

  public readonly MODAL_ID = 'premise-rate-modal';
  public readonly MIN_PREMISE_VALUE = 0.01;

  private propertyRate: PropertyRate;
  public hasPremise: boolean;
  public showConfirmModal: boolean;
  public premiseForm: FormGroup;
  public premiseList: PropertyPremiseHeader[];
  public selectedPremise: PropertyPremiseHeader;
  public premiseOptionList: Option[];
  public initialDate: string;
  public endDate: string;
  public currency: any;
  public propertyId: number;
  public returnUrl: string;

  public propertyParameterList: Array<Parameter>;
  public roomTypeWithBaseRateList: Array<RoomTypeParametersConfiguration>;

  public cancelButtonConfig: ButtonConfig;
  public saveButtonConfig: ButtonConfig;
  public confirmModalDataConfig: ConfirmModalDataConfig;

  constructor(
    public formBuilder: FormBuilder,
    private sharedService: SharedService,
    private commomService: CommomService,
    private modalEmitToggleService: ModalEmitToggleService,
    private premiseResource: PremiseResource,
    private baseRateResource: BaseRateResource,
    private premiseRateService: PremiseRateService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    public route: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    this.setButtonConfig();
    this.setForm();
    this.setVars();
  }

  private setVars() {
    this.route.params.subscribe(async params => {
      this.propertyId = +params.property;

      this.confirmModalDataConfig = new ConfirmModalDataConfig();
      this.propertyRate = new PropertyRate();
      this.propertyRate.propertyId = this.propertyId;
      this.propertyRate.baseRateConfigurationRoomTypeList = [];
      this.propertyRate.baseRateConfigurationMealPlanTypeList = [];
      this.propertyRate.daysOfWeekDto = [0, 1, 2, 3, 4, 5, 6];
      this.propertyRate.propertyMealPlanTypeRate = true;

      this.returnUrl = this.route.snapshot.queryParams.returnUrl;

      await this.getBaseRateConfiguration();
      this.setConfirmModalConfig();
    });
  }

  private setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      () => this.cancel(),
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.saveButtonConfig = this.sharedService.getButtonConfig(
      'save',
      () => this.save(),
      'action.save',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  private setConfirmModalConfig() {
    this.showConfirmModal = true;
    this.confirmModalDataConfig.textConfirmModal = 'text.confirmInsertionBaseRate';
    this.confirmModalDataConfig.alertTextToConfirmModal = 'text.confirmToInsertionBaseRate';
    this.confirmModalDataConfig.alertSecondParagraph = 'variable.datesNotInclude';
    this.confirmModalDataConfig.actionCancelButton = this.sharedService.getButtonConfig(
      'cancelModal',
      this.closeConfirmationModal,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.confirmModalDataConfig.actionConfirmButton = this.sharedService.getButtonConfig(
      'confirmModal',
      this.insertBaseRateData,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
    );
  }

  public closeConfirmationModal = () => {
    this.modalEmitToggleService.emitCloseWithRef('confirm-insert-base-rate');
  }

  private setForm() {
    this.premiseForm = this.formBuilder.group({
      currency: [0,  [Validators.required, Validators.min(this.MIN_PREMISE_VALUE)] ],
      premiseId: ['', [Validators.required]],
    });

    this.premiseForm.valueChanges.subscribe(values => {
      this.saveButtonConfig.isDisabled = true;
      this.changePremise();

      if (
        this.premiseForm.valid
        && values.currency
      ) {
        this.saveButtonConfig.isDisabled = false;
      }
    });
  }

  public closePremiseModal() {
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_ID);
  }

  private cancel() {
    this.closePremiseModal();
  }

  private save = () => {
    this.modalEmitToggleService.emitToggleWithRef('confirm-insert-base-rate');
  }

  private getBaseRateConfiguration(): Promise<any> {
    return new Promise<any>( resolve => {
      this.baseRateResource
        .getConfigurationDataParameters(this.propertyId)
        .subscribe(configurationDataParameters => {
          this.setRoomTypeWithBaseRateList(configurationDataParameters.roomTypeList);
          this.setPropertyParameterList(configurationDataParameters.propertyParameterList);

          resolve(null);
        });
    });
  }

  private setPropertyParameterList(propertyParamsList: any) {
    this.propertyParameterList = [...propertyParamsList];
  }

  private setRoomTypeWithBaseRateList(roomTypeList: any) {
    this.roomTypeWithBaseRateList = [...roomTypeList.items];
  }

  public changePremise() {
    const id = this.premiseForm.get('premiseId').value;

    if (id && this.premiseList) {
      this.selectedPremise = this.premiseList.find(x => x.id == id);
      this.currency = this.selectedPremise.currency;
      this.propertyRate.currencyId = this.currency.id;
    }
  }

  public premiseApply = () => {
    const { propertyParameterList, roomTypeWithBaseRateList } = this;

    if ( this.selectedPremise ) {
      const { propertyPremiseList, id } = this.selectedPremise;
      this
        .premiseRateService
        .setPremiseValues(
          propertyParameterList,
          roomTypeWithBaseRateList,
          this.selectedPremise,
          propertyPremiseList,
          this.premiseForm.get('currency').value,
          id
        );
    }
  }

  public insertBaseRateData = () => {
    const { selectedPremise, propertyRate } = this;

    if ( selectedPremise ) {
      this.premiseApply();

      const { propertyPremiseList } = selectedPremise;
      propertyRate.propertyBaseRateTypeId = BaseRateType.Premise;

      propertyRate.baseRateConfigurationRoomTypeList = this
        .premiseRateService
        .getBaseRateConfigurationRoomType(
          propertyPremiseList
        );
    } else {
      propertyRate.propertyBaseRateTypeId = BaseRateType.Amount;
    }

    this.closeConfirmationModal();
    this
      .baseRateResource
      .sendConfigurationDataParameters(propertyRate, true)
      .subscribe(() => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this
            .translateService
            .instant('text.rateSucess'),
        );

        this.closePremiseModal();
        this.reloadCalendar.emit();
    });
  }

  public toggleModal(initialDate, endDate, currency) {
    this.initialDate = initialDate;
    this.endDate = endDate;
    this.currency = currency;

    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_ID);
    this.loadData();
  }

  public loadData() {
    this.hasPremise = false;
    if (this.initialDate && this.endDate && this.currency) {
      this.propertyRate.startDate = moment(this.initialDate).toDate();
      this.propertyRate.finalDate = moment(this.endDate).toDate();

      this.premiseResource.getPropertyPremiseHeaderByDate(this.initialDate, this.endDate, this.currency.id).subscribe(({items}) => {
        if (items && items.length > 0) {
          this.hasPremise = true;
          this.selectedPremise = items[0];

          this.premiseForm.get('premiseId').setValue(this.selectedPremise.id);
          this.premiseList = items;
          this.premiseOptionList = this
            .commomService
            .toOption(this.premiseList, 'id', 'name');
        }
      });
    }
  }
}
