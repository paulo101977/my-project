import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { DateFormatPipe, MomentModule } from 'ngx-moment';
import { DayDetailComponent } from './day-detail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CurrencyFormatPipe } from '../../../../shared/pipes/currency-format.pipe';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('DayDetailComponent', () => {
  let component: DayDetailComponent;
  let fixture: ComponentFixture<DayDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DayDetailComponent],
      imports: [TranslateModule.forRoot(), MomentModule, HttpClientTestingModule, RouterTestingModule],
      providers: [DateFormatPipe, CurrencyFormatPipe, InterceptorHandlerService],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DayDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
