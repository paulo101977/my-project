import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DayPilot } from 'daypilot-pro-angular';
import { ModalEmitToggleService } from '../../../../shared/services/shared/modal-emit-toggle.service';
import { RateCalendarResource } from '../../../../shared/resources/rate-calendar/rate-calendar.resource';
import { RateCalendarFilter } from '../../../../shared/models/revenue-management/rate-calendar-filter';
import { ActivatedRoute } from '@angular/router';
import { RateCalendarDayDto } from '../../../../shared/models/revenue-management/rate-calendar-day-dto';
import { CurrencyService } from '../../../../shared/services/shared/currency-service.service';
import * as moment from 'moment';

@Component({
  selector: 'app-day-detail',
  templateUrl: './day-detail.component.html',
  styleUrls: ['./day-detail.component.css'],
})
export class DayDetailComponent implements OnInit {
  private MODAL_REF = 'modal-day-detail';

  public _dayList: any[] = [];

  @Input() public mealPlanType: string;

  @Input() public calendarFilter: RateCalendarFilter;

  @Output() public navigate: EventEmitter<undefined> = new EventEmitter();

  private propertyId;
  private currentIndex: number;
  public selectedDay: any;

  public blankDayList: void[];
  public weekDayList: string[];

  @Input()
  public set dayList(dayList) {
    this._dayList = dayList;
    this.blankDayList = [];
    if (dayList && dayList.length > 0) {
      const firstDayOfWeek = new DayPilot.Date(dayList[0].date).getDayOfWeek();
      for (let i = 0; i < firstDayOfWeek; i++) {
        this.blankDayList.push(null);
      }
    }
  }

  public get dayList() {
    return this._dayList;
  }

  constructor(
    public modalEmitToggleService: ModalEmitToggleService,
    public rateCalendarResource: RateCalendarResource,
    public currencyService: CurrencyService,
    public activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  setVars() {
    this.propertyId = this.activatedRoute.snapshot.params.property;
    this.weekDayList = moment.weekdaysMin();
  }

  public toggleModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_REF);
  }

  public openModal(date: string) {
    this.navigateTo(date);
    this.toggleModal();
  }

  public closeModal() {
    this.toggleModal();
  }

  public navigateNext() {
    if (this.currentIndex < this.dayList.length - 1) {
      this.currentIndex++;
      this.selectedDay = this.dayList[this.currentIndex];
    } else {
      this.navigateTo(new DayPilot.Date(this.selectedDay.date).addDays(1).toString());
    }
  }

  public navigatePrevious() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
      this.selectedDay = this.dayList[this.currentIndex];
    } else {
      this.navigateTo(new DayPilot.Date(this.selectedDay.date).addDays(-1).toString());
    }
  }

  public navigateToday() {
    this.navigateTo(new DayPilot.Date().getDatePart().toString());
  }

  private navigateTo(date: string) {
    const index = this.dayList.findIndex(day => day.date == date);

    if (index >= 0) {
      this.currentIndex = index;
      this.selectedDay = this.dayList[this.currentIndex];
    } else {
      this.calendarFilter.initialDate = new DayPilot.Date(date).addDays(-14).toString();
      this.calendarFilter.finalDate = new DayPilot.Date(date).addDays(14).toString();

      this.rateCalendarResource.getCalendar(this.propertyId, this.calendarFilter).subscribe(data => {
        this.dayList = this.mapCalendarCells(data.items);
        this.navigateTo(date);
      });
    }
  }

  private mapCalendarCells(dayList: RateCalendarDayDto[]) {
    return dayList.map(day => {
      const currentValue = day.baseRateWithRatePlanAmount ? day.baseRateWithRatePlanAmount : day.baseRateAmount;
      const oldValue = day.baseRateWithRatePlanAmount ? day.baseRateAmount : null;

      return {
        date: day.dateFormatted,
        oldValue: oldValue,
        oldValueString: oldValue && this.currencyService.formatCurrencyToString(oldValue),
        currentValue: currentValue,
        currentValueString: currentValue && this.currencyService.formatCurrencyToString(currentValue),
        occupation: day.occupation,
        occupiedRooms: day.occupiedRooms,
        roomTotal: day.roomTotal,
      };
    });
  }
}
