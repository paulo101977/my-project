import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalendarComponent } from './components/calendar/calendar.component';

export const calendarRevenueManagementRoutingRoutes: Routes = [{ path: '', component: CalendarComponent }];

@NgModule({
  imports: [RouterModule.forChild(calendarRevenueManagementRoutingRoutes)],
  exports: [RouterModule],
})
export class CalendarRoutingModule {}
