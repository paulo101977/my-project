import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {configureTestSuite} from 'ng-bullet';
import {ThexApiTestingModule} from '@inovacaocmnet/thx-bifrost';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateTestingModule} from 'app/shared/mock-test/translate-testing.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import { LevelRateModalEditComponent } from './level-rate-modal-edit.component';
import {amDateFormatPipeStub, createServiceStub} from '../../../../../../testing';
import {BaseRateResource} from 'app/shared/resources/base-rate/base-rate.resource';
import {BaseRateLevel} from 'app/revenue-management/base-rate/models/base-rate-level';
import {RateLevelService} from 'app/revenue-management/rate-level/services/rate-level/rate-level.service';
import {Observable} from 'rxjs';
import {of} from 'rxjs/internal/observable/of';
import {BASE_LEVEL_RATE, BASE_LEVEL_OPTIONS, DATA} from 'app/revenue-management/base-rate/mock-data';
import {MomentModule} from 'ngx-moment';

describe('LevelRateModalEditComponent', () => {
  let component: LevelRateModalEditComponent;
  let fixture: ComponentFixture<LevelRateModalEditComponent>;
  let rateLevelService: RateLevelService;

  const baseRateProvider = createServiceStub(BaseRateResource, {
    sendConfigurationDataLevel (baseRateConfiguration: BaseRateLevel): Observable<any> {
      return null;
    }
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        HttpClientTestingModule,
        ThexApiTestingModule,
        MomentModule
      ],
      declarations: [
        LevelRateModalEditComponent,
        amDateFormatPipeStub
      ],
      providers: [
        FormBuilder,
        baseRateProvider,
        ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(LevelRateModalEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    rateLevelService = TestBed.get(RateLevelService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn(component, 'setForm');
    spyOn<any>(component, 'setButtonConfig');
    component.ngOnInit();
    expect(component.setForm).toHaveBeenCalled();
    expect(component['setButtonConfig']).toHaveBeenCalled();
  });

  it('should toggleModal', () => {
    spyOn<any>(component, 'loadData').and.callFake( f => f);
    component.toggleModal('2019-01-10T00:00:00', '2019-01-11T00:00:00', 'c5ef2cfa-a05e-44ed-ad2e-089b26471432');

    expect(component.initialDate).toEqual('2019-01-10T00:00:00');
    expect(component.endDate).toEqual('2019-01-11T00:00:00');
    expect(component.currencyId).toEqual('c5ef2cfa-a05e-44ed-ad2e-089b26471432');
    expect(component.loadData).toHaveBeenCalled();
  });

  it('should loadData', () => {
    component.initialDate = '2019-01-10T00:00:00';
    component.endDate = '2019-01-10T00:00:00';
    component.currencyId = 'c5ef2cfa-a05e-44ed-ad2e-089b26471432';

    spyOn(component['rateLevelService'], 'getLevelRateByDate').and.returnValue(of({ items: DATA[BASE_LEVEL_RATE]}));
    component.loadData();

    expect(component.baseLevelRate).toEqual(DATA[BASE_LEVEL_RATE]);
    expect(component.levelOptionsList).toEqual(DATA[BASE_LEVEL_OPTIONS]);
  });

  it('should applyLevelRate', () => {
    spyOn(component, 'closeConfirmationModal');
    spyOn(component['baseRateResource'], 'sendConfigurationDataLevel').and.returnValue(of(null));
    spyOn(component, 'closeLevelModal');
    spyOn(component['reloadCalendar'], 'emit');
    component.applyLevelRate();

    expect(component.closeConfirmationModal).toHaveBeenCalled();
    expect(component.closeLevelModal).toHaveBeenCalled();
    expect(component['reloadCalendar'].emit).toHaveBeenCalled();
  });


});
