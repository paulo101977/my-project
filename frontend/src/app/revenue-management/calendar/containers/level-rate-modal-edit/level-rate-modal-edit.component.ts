import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RateLevelService} from 'app/revenue-management/rate-level/services/rate-level/rate-level.service';
import {CommomService} from 'app/shared/services/shared/commom.service';
import {Option} from 'app/shared/models/option';
import {BaseLevelRate} from 'app/revenue-management/rate-level/models/base-level-rate';
import {ButtonConfig, ButtonSize, ButtonType} from 'app/shared/models/button-config';
import {SharedService} from 'app/shared/services/shared/shared.service';
import {BaseRateLevel} from 'app/revenue-management/base-rate/models/base-rate-level';
import {BaseRateResource} from 'app/shared/resources/base-rate/base-rate.resource';
import {ConfirmModalDataConfig} from 'app/shared/models/confirm-modal-data-config';
import * as moment from 'moment';
import {SuccessError} from 'app/shared/models/success-error-enum';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-level-rate-modal-edit',
  templateUrl: './level-rate-modal-edit.component.html',
  styleUrls: ['./level-rate-modal-edit.component.css']
})
export class LevelRateModalEditComponent implements OnInit {

  @Output() reloadCalendar = new EventEmitter();

  public initialDate: string;
  public endDate: string;
  public currencyId: string;
  public levelOptionsList: Option[];
  public cancelButtonConfig: ButtonConfig;
  public saveButtonConfig: ButtonConfig;
  public confirmModalDataConfig: ConfirmModalDataConfig;
  private readonly LEVEL_MODAL = 'level-rate-modal';
  public levelForm: FormGroup;
  public hasLevel = false;
  public showConfirmModal = false;
  public baseLevelRate: Array<BaseLevelRate>;
  public levelIdList: Array<string>;
  public dateListOutside: string;

  constructor(
    private modalEmitToggleService: ModalEmitToggleService,
    private formBuilder: FormBuilder,
    private rateLevelService: RateLevelService,
    private commomService: CommomService,
    private sharedService: SharedService,
    private baseRateResource: BaseRateResource,
    private toasterEmitService: ToasterEmitService,
    public translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.setButtonConfig();
    this.setForm();
  }

  public toggleModal(initialDate, endDate, currencyId) {
    this.initialDate = initialDate;
    this.endDate = endDate;
    this.currencyId =  currencyId;

    this.modalEmitToggleService.emitToggleWithRef(this.LEVEL_MODAL);
    this.loadData();
  }

  public setForm() {
    this.levelForm = this.formBuilder.group(
      {
        levelRate: [null, [Validators.required]]
      }
    );

    this.levelForm.valueChanges.subscribe( () => {
      this.saveButtonConfig.isDisabled = !this.levelForm.valid;
    });
  }

  public loadData() {
    if (this.initialDate && this.endDate && this.currencyId) {
      this.rateLevelService.getLevelRateByDate(this.initialDate, this.endDate, this.currencyId).subscribe(({items}) => {

        if (items && items.length > 0) {
          this.hasLevel = true;

          this.baseLevelRate = items;
          this.levelOptionsList = this
            .commomService
            .toOption(this.baseLevelRate, 'levelId', 'levelName');
        }
      });
    }
  }

  private setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      () => this.closeLevelModal(),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.saveButtonConfig = this.sharedService.getButtonConfig(
      'apply',
      () => this.setConfirmModalConfig(),
      'action.apply',
      ButtonType.Primary
    );
    this.saveButtonConfig.isDisabled = true;
  }

  public closeLevelModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.LEVEL_MODAL);
  }

  public closeConfirmationModal() {
    this.modalEmitToggleService.emitToggleWithRef('confirm-insert-level-rate');
  }

  public applyLevelRate() {
    const daysWeeks = [0, 1, 2, 3, 4, 5, 6];

    const baseLevel = <BaseRateLevel>{
      daysOfWeekDto: daysWeeks,
      levelRateIdList: this.levelIdList,
      initialDate: this.initialDate,
      endDate: this.endDate
    };

    this.closeConfirmationModal();
    this
      .baseRateResource
      .sendConfigurationDataLevel(baseLevel)
      .subscribe( (item) => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this
            .translateService
            .instant('text.rateSucess'),
        );

        this.closeLevelModal();
        this.reloadCalendar.emit();
      });
  }

  private setConfirmModalConfig() {
    this.getLevelRateHeader();
    this.confirmModalDataConfig = new ConfirmModalDataConfig();
    this.showConfirmModal = true;
    this.confirmModalDataConfig.textConfirmModal = 'text.confirmInsertionBaseRate';
    this.confirmModalDataConfig.alertTextToConfirmModal = 'text.confirmToInsertionBaseRate';
    this.confirmModalDataConfig.alertSecondParagraph = 'variable.datesNotInclude';
    this.confirmModalDataConfig.dates = this.dateListOutside;
    this.confirmModalDataConfig.actionCancelButton = this.sharedService.getButtonConfig(
      'cancelModal',
      () => this.closeConfirmationModal(),
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.confirmModalDataConfig.actionConfirmButton = this.sharedService.getButtonConfig(
      'confirmModal',
      () => this.applyLevelRate(),
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
    );
    this.modalEmitToggleService.emitToggleWithRef('confirm-insert-level-rate');
  }

  public getLevelRateHeader() {
    const {levelRateCurrencyList} = this.baseLevelRate.find(item => item.levelId === this.levelForm.get('levelRate').value);
    const {levelRateList, dateListOutsideRateList} = levelRateCurrencyList.find(item => item.currencyId === this.currencyId);
    if (levelRateList && levelRateList.length > 0) {
      this.levelIdList = levelRateList.map(level =>
        level.levelRateHeader.id
      );
    }

    if (dateListOutsideRateList && dateListOutsideRateList.length > 0) {
      dateListOutsideRateList.map( dates => {
        if (this.dateListOutside) {
          this.dateListOutside = this.dateListOutside + ', ' + moment(dates).format('L');
        } else {
          this.dateListOutside = moment(dates).format('L');
        }
      });
    }
  }
}
