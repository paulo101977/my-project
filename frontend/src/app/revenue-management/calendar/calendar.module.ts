import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ThxChildSelectModule, ThxRateScheduleModule } from '@inovacao-cmnet/thx-ui';
import { MomentModule } from 'ngx-moment';
import { MyDatePickerModule } from 'mydatepicker';
import { SharedModule } from '../../shared/shared.module';
import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarComponent } from './components/calendar/calendar.component';
import { DayDetailComponent } from './components/day-detail/day-detail.component';
import { PremiseRateModalEditComponent } from './components/premise-rate-modal-edit/premise-rate-modal-edit.component';
import { LevelRateModalEditComponent } from './containers/level-rate-modal-edit/level-rate-modal-edit.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    SharedModule,
    MyDatePickerModule,
    MomentModule,
    CalendarRoutingModule,
    ThxRateScheduleModule,
    ThxChildSelectModule
  ],
  declarations: [
    CalendarComponent,
    DayDetailComponent,
    LevelRateModalEditComponent,
    PremiseRateModalEditComponent
  ],
})
export class CalendarModule {}
