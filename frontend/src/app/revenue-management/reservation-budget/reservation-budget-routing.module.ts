import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservationBudgetManageComponent } from './components-containers/reservation-budget-manage/reservation-budget-manage.component';

export const reservationBudgetRoutes: Routes = [{ path: '', component: ReservationBudgetManageComponent }];

@NgModule({
  imports: [RouterModule.forChild(reservationBudgetRoutes)],
  exports: [RouterModule],
})
export class ReservationBudgetRoutingModule {}
