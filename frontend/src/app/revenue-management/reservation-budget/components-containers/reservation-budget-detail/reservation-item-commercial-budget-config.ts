import {
  ReservationItemCommercialBudget
} from '../../../../shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { SelectObjectOption } from '../../../../shared/models/selectModel';
import { Input } from '@angular/core';
import {
  ReservationItemCommercialBudgetDay
} from '../../../../shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-day';
import { ReservationBudget } from '../../../../shared/models/revenue-management/reservation-budget/reservation-budget';
import { MealPlanType } from '../../../../shared/models/revenue-management/meal-plan-type';

export class ReservationItemCommercialBudgetConfig {
  reservationItemCommercialBudgetCurrent: ReservationItemCommercialBudget;
  reservationBudgetTableList: Array<ReservationBudget>;
  selectReservationItemCommercialBudgetList: Array<SelectObjectOption>;
  isGratuityType: boolean;
  gratuityTypeId: number;
  averageDaily: number;
  dateListFromPeriod: Array<Date>;
  dateListFromPeriodChanged: Array<Date>;
  gratuityTypeSelectOptionsList: Array<SelectObjectOption>;
  wasConfirmed: boolean;
  reservationItemCode: string;
  reservationItemId: number;
  roomTypeId: number;
  companyClientId: string;
  adultQuantity: number;
  childCount: number;
}

export class BudgetTableConfig {
  reservationItemCommercialBudgetDayList: Array<ReservationItemCommercialBudgetDay>;
  mealPlanTypeList: Array<MealPlanType>;
  mealPlanTypeCurrent: number;
  reservationBudgetList: Array<ReservationBudget>;
  enableAllInputs: boolean;
  isGratuityType: boolean;
}
