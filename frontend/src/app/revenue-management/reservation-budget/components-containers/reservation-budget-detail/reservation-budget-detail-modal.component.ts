import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BudgetTableConfig, ReservationItemCommercialBudgetConfig } from './reservation-item-budget-config';
import {
  ReservationItemCommercialBudget
} from '../../../../shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { SelectObjectOption } from '../../../../shared/models/selectModel';
import { MealPlanType } from '../../../../shared/models/revenue-management/meal-plan-type';
import { SuccessError } from '../../../../shared/models/success-error-enum';
import { ShowHide } from '../../../../shared/models/show-hide-enum';
import { LoadPageEmitService } from '../../../../shared/services/shared/load-emit.service';
import { MealPlanTypeResource } from '../../../../shared/resources/meal-plan-type/meal-plan-type.resource';
import { ActivatedRoute } from '@angular/router';
import { ToasterEmitService } from '../../../../shared/services/shared/toaster-emit.service';
import {
  ReservationBudget,
  ReservationWithBudget,
} from '../../../../shared/models/revenue-management/reservation-budget/reservation-budget';
import * as _ from 'lodash';
import { ReservationBudgetService } from '../../../../shared/services/reservation-budget/reservation-budget.service';
import { BudgetService } from '@app/shared/services/budget/budget.service';
import { GratuityTypeIdEnum } from '../../../../shared/models/gratuity-type-id-enum';
import { GratuityType } from '../../../../shared/models/revenue-management/reservation-budget/gratuity-type';
import { TranslateService } from '@ngx-translate/core';
import { CommomService } from '../../../../shared/services/shared/commom.service';
import { ButtonConfig, ButtonType } from '../../../../shared/models/button-config';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { SessionParameterService } from '../../../../shared/services/parameter/session-parameter.service';

@Component({
  selector: 'reservation-budget-detail-modal',
  templateUrl: 'reservation-budget-detail-modal.component.html',
  styleUrls: ['./reservation-budget-detail-modal.component.css'],
})
export class ReservationBudgetDetailModalComponent implements OnInit {
  @Input() reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig;
  @Input() reservationItemCommercialBudgetChanged: ReservationItemCommercialBudgetConfig;
  @Input() showHeader = true;
  @Input() showFooter = true;
  @Output() closeModal = new EventEmitter();
  @Output() insert = new EventEmitter();
  @Output() sendReservationWithBudget = new EventEmitter();
  @Output() updateBudgetList = new EventEmitter();

  public enableAllInputs: boolean;
  public mealPlanTypeList: Array<MealPlanType>;
  public propertyId: number;
  public budgetTableConfig: BudgetTableConfig;
  public averageDaily: number;
  public hasSymbol: boolean;
  public gratuityTypeList: Array<GratuityType>;
  public reservationBudgetTableListWithGratuityTypeEnable: Array<ReservationBudget>;
  public reservationBudgetTableListWithGratuityTypeDisabled: Array<ReservationBudget>;

  // Buttons
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;

  constructor(
    private loadPageEmitService: LoadPageEmitService,
    private mealPlanTypeResource: MealPlanTypeResource,
    private route: ActivatedRoute,
    private budgetService: BudgetService,
    private sessionParameter: SessionParameterService,
    private reservationBudgetService: ReservationBudgetService,
    private translateService: TranslateService,
    private commonService: CommomService,
    private sharedService: SharedService,
    public toasterEmitService: ToasterEmitService,
  ) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.hasSymbol = this.reservationBudgetService.showCurrencySymbol(this.reservationItemCommercialBudget,
      this.reservationItemCommercialBudget.isGratuityType);

    if (this.reservationItemCommercialBudgetChanged) {
      this.reservationItemCommercialBudget = _.cloneDeep(this.reservationItemCommercialBudgetChanged);
    }
    this.setEnableAllInputs();
    this.setMealPlanTypeList();
    this.setGratuityTypeList();
    if (
      !this.reservationItemCommercialBudget.isGratuityType &&
      !this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.mealPlanTypeId
      || this.reservationItemCommercialBudget.isGratuityType &&
      !this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent
    ) {
      this.setReservationBudgetListDefault();
    }
    this.budgetTableConfig = new BudgetTableConfig();
    this.reservationBudgetTableListWithGratuityTypeEnable = this.reservationItemCommercialBudget.reservationBudgetTableList;

    this.setButtonConfig();
    if (this.reservationItemCommercialBudget.reservationItemId) {

      this.setSelectCommercialBudget();
    }

    if (this.reservationItemCommercialBudget.isGratuityType) {
      this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent = null;
      this.changeEnableGratuityType();
    }
  }

  private setSelectCommercialBudget() {
    if ( this.reservationItemCommercialBudget
      && this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent
      && this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.mealPlanTypeId) {
      const optionSelected = this.reservationItemCommercialBudget.selectReservationItemCommercialBudgetList.find(
        reservationItemBudget => {
          return (
            reservationItemBudget.key.mealPlanTypeId ===
            this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.mealPlanTypeId
          );
        },
      );
      this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent = optionSelected ? optionSelected.key : null;
    }
  }

  private setMealPlanTypeList(): void {
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.mealPlanTypeResource.getAllMealPlanTypeList(this.propertyId).subscribe(
      response => {
        this.budgetTableConfig.mealPlanTypeList = [...response.items];
        this.loadPageEmitService.emitChange(ShowHide.hide);
      },
      error => {
        this.toasterEmitService.emitChange(SuccessError.error, 'text.errorMessage');
        this.loadPageEmitService.emitChange(ShowHide.hide);
      },
    );
  }

  private setEnableAllInputs() {
    this.enableAllInputs = !this.reservationItemCommercialBudget.isGratuityType;
  }

  public setReservationItemCommercialBudgetCurrent(reservationItemCommercialBudget: ReservationItemCommercialBudget) {
    if (!this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent
      || this.hasMealPlanTypeIdDifferent(reservationItemCommercialBudget)) {
      this.updateReservationItemCommercialBudgetCurrent(reservationItemCommercialBudget);
    }

    this.setEnableAllInputs();
    this.updateIsDisabledOfConfirmButtonConfig();
    this.reservationBudgetTableListWithGratuityTypeDisabled = this.reservationItemCommercialBudget.reservationBudgetTableList;
    this.setTotals();
  }

  private hasMealPlanTypeIdDifferent(reservationItemCommercialBudget: ReservationItemCommercialBudget) {
    const commercialBudgetCurrent = this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent;
    const isRatePlan = reservationItemCommercialBudget.ratePlanId;
    if (this.reservationItemCommercialBudget && commercialBudgetCurrent && reservationItemCommercialBudget) {
      return commercialBudgetCurrent.mealPlanTypeId !== reservationItemCommercialBudget.mealPlanTypeId
        || isRatePlan && !commercialBudgetCurrent.ratePlanId
        || isRatePlan !== commercialBudgetCurrent.ratePlanId
        || !isRatePlan && commercialBudgetCurrent.ratePlanId
        || commercialBudgetCurrent.mealPlanTypeId === reservationItemCommercialBudget.mealPlanTypeId
        && commercialBudgetCurrent.totalBudget !== reservationItemCommercialBudget.totalBudget;
    }

    return false;
  }

  private updateReservationItemCommercialBudgetCurrent(reservationItemCommercialBudget: ReservationItemCommercialBudget) {
    this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent = reservationItemCommercialBudget;
    this.reservationItemCommercialBudget.reservationBudgetTableList = new Array<ReservationBudget>();

    reservationItemCommercialBudget.commercialBudgetDayList.forEach(commercialBudget => {
      const reservationBudget = new ReservationBudget();
      const reservationBudgetByMapper = this.reservationBudgetService.getReservationBudgetByCommercialBudgetDay(
        reservationBudget,
        commercialBudget,
        this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.commercialBudgetName,
        this.reservationItemCommercialBudget.isGratuityType,
      );
      this.reservationItemCommercialBudget.reservationBudgetTableList.push(reservationBudgetByMapper);
    });

    this.budgetTableConfig.reservationItemCommercialBudgetDayList =
      this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.commercialBudgetDayList;
    this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.totalBudget =
      this.reservationBudgetService.getDailiesTotalsOfAgreement(
        this.reservationItemCommercialBudget,
      );
    this.setTotals();

    if (
      this.reservationItemCommercialBudget &&
      this.reservationItemCommercialBudget.reservationBudgetTableList &&
      this.reservationItemCommercialBudget.reservationBudgetTableList.length > 0
    ) {
      this.setBaseRateChanged(false);
    }
  }

  public updateGratuityType(gratuityType: any) {
    if (typeof gratuityType === 'string') {
      this.reservationItemCommercialBudget.gratuityTypeId = parseInt(gratuityType, 0);
    }

    this.updateIsDisabledOfConfirmButtonConfig();
  }

  public changeEnableGratuityType(event?: any) {
    this.setEnableAllInputs();
    this.setAverageDaily();
    this.hasSymbol = this.reservationBudgetService.showCurrencySymbol(this.reservationItemCommercialBudget,
      this.reservationItemCommercialBudget.isGratuityType);
    if (this.reservationItemCommercialBudget.isBudgetManagePage) {
      this.validForBudgetPage();
    } else {
      this.validForBudgetModal();
    }

    this.setTotals();
    this.updateIsDisabledOfConfirmButtonConfig();
  }

  private validForBudgetModal() {
    if (!this.reservationItemCommercialBudget.isGratuityType) {
      this.reservationItemCommercialBudget.reservationBudgetTableList = !this.reservationBudgetTableListWithGratuityTypeDisabled
        ? this.reservationBudgetTableListWithGratuityTypeEnable
        : this.reservationBudgetTableListWithGratuityTypeDisabled;
      this.setSelectCommercialBudget();
      if (this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent) {
        this.updateReservationItemCommercialBudgetCurrent(this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent);
      }
      this.reservationItemCommercialBudget.gratuityTypeId = null;
      this.reservationItemCommercialBudget.gratuityTypeSelectOptionsList = new Array<SelectObjectOption>();
    } else {
      this.reservationBudgetTableListWithGratuityTypeDisabled =
        _.cloneDeep(this.reservationItemCommercialBudget.reservationBudgetTableList);
      this.setReservationBudgetListDefault();
      this.reservationBudgetTableListWithGratuityTypeEnable = this.reservationItemCommercialBudget.reservationBudgetTableList;
      this.reservationItemCommercialBudget.reservationBudgetTableList = this.reservationItemCommercialBudgetChanged
        ? this.reservationItemCommercialBudgetChanged.reservationBudgetTableList
        : this.reservationBudgetTableListWithGratuityTypeEnable;
    }

    this.setGratuityTypeList();
  }

  private validForBudgetPage() {
    if (this.reservationItemCommercialBudget.isGratuityType) {
      if (!(this.reservationItemCommercialBudget.reservationBudgetTableList[0].agreementRate == 0)) {
        this.setReservationBudgetListDefault();
        this.reservationBudgetTableListWithGratuityTypeEnable = this.reservationItemCommercialBudget.reservationBudgetTableList;
        this.reservationItemCommercialBudget.reservationBudgetTableList = this.reservationBudgetTableListWithGratuityTypeEnable;
      }
    }

    if (!this.reservationItemCommercialBudget.isGratuityType
      && !this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent) {
      this.reservationBudgetTableListWithGratuityTypeDisabled =
        _.cloneDeep(this.reservationItemCommercialBudget.reservationBudgetTableList);
      this.setReservationBudgetListDefault();
      this.reservationItemCommercialBudget.reservationBudgetTableList = this.reservationBudgetTableListWithGratuityTypeDisabled;
      this.resetGratuityList();
    }

    if (!this.reservationItemCommercialBudget.isGratuityType
      && this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent
      && this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.totalBudget > 0) {
      this.updateReservationItemCommercialBudgetCurrent(this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent);
      this.resetGratuityList();
    }

    this.setGratuityTypeList();
  }

  private resetGratuityList() {
    this.reservationItemCommercialBudget.gratuityTypeId = null;
    this.reservationItemCommercialBudget.gratuityTypeSelectOptionsList = new Array<SelectObjectOption>();
  }

  private setButtonConfig(): void {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      this.cancelChangesReservationBudget,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'confirm',
      this.saveReservationBudget,
      'action.confirm',
      ButtonType.Primary,
    );
    this.updateIsDisabledOfConfirmButtonConfig();
  }

  private updateIsDisabledOfConfirmButtonConfig() {
    if (
      this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent &&
      !this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.currencyId
    ) {
      this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent = null;
    }
    if (this.reservationItemCommercialBudget && this.reservationItemCommercialBudget.reservationBudgetTableList) {
      this.confirmButtonConfig.isDisabled =
        this.reservationItemCommercialBudget.reservationBudgetTableList.find(
          reservationBudget =>
            !_.isNumber(reservationBudget.manualRate) ||
            isNaN(reservationBudget.manualRate) ||
            !_.isNumber(reservationBudget.discount) ||
            isNaN(reservationBudget.discount) ||
            !_.isNumber(reservationBudget.mealPlanTypeId) ||
            isNaN(reservationBudget.mealPlanTypeId),
        ) ||
        (this.reservationItemCommercialBudget.isGratuityType && !this.reservationItemCommercialBudget.gratuityTypeId) ||
        (!this.reservationItemCommercialBudget.isGratuityType &&
          !this.reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent)
          ? true
          : false;
    }
  }

  public reservationBudgetListHasAgreementRateChanged(): boolean {
    if (this.reservationItemCommercialBudget && this.reservationItemCommercialBudget.reservationBudgetTableList) {
      return this.reservationItemCommercialBudget.reservationBudgetTableList.some(r => r.baseRateChanged);
    }
  }

  public cancelChangesReservationBudget = () => {
    this.setBaseRateChanged(false);
    this.reservationItemCommercialBudgetChanged = null;
    this.closeModal.emit(this.reservationItemCommercialBudget);
  }

  private setBaseRateChanged(wasChanged: boolean) {
    if (this.reservationItemCommercialBudget && this.reservationItemCommercialBudget.reservationBudgetTableList) {
      this.reservationItemCommercialBudget.reservationBudgetTableList.forEach(reservationBudget => {
        reservationBudget.baseRateChanged = wasChanged;
      });
    }
  }

  private saveReservationBudget = () => {
    this.reservationItemCommercialBudget.wasConfirmed = true;
    this.setBaseRateChanged(false);
    const reservationWithBudget = new ReservationWithBudget();
    reservationWithBudget.reservationBudgetList = this.reservationItemCommercialBudget.reservationBudgetTableList;
    reservationWithBudget.reservationItemCommercialBudget = this.reservationItemCommercialBudget;
    reservationWithBudget.gratuityTypeId = this.reservationItemCommercialBudget.gratuityTypeId;
    this.sendReservationWithBudget.emit(reservationWithBudget);
  }

  public updateReservationBudgetList(reservationBudgetList: Array<ReservationBudget>): void {
    this.reservationItemCommercialBudget.reservationBudgetTableList = reservationBudgetList;
    this.updateBudgetList.emit(this.reservationItemCommercialBudget);
  }

  public updateValuesByMealPlanType(reservationBudget: ReservationBudget): void {}

  private setReservationBudgetListDefault(): void {
    this.reservationItemCommercialBudget =
      this.budgetService.getReservationBudgetListDefault(
        this.reservationItemCommercialBudget,
        this.propertyId
      );
    this.enableAllInputs = false;
    this.setTotals();
  }

  private setGratuityTypeList(): void {
    this.gratuityTypeList = [];
    this.reservationItemCommercialBudget.gratuityTypeSelectOptionsList = new Array<SelectObjectOption>();
    this.translateService.get('text.internalUsage').subscribe(text => {
      const gratuityType1 = new GratuityType();
      gratuityType1.gratuityTypeId = GratuityTypeIdEnum.InternalUsage;
      gratuityType1.gratuityTypeName = text;

      const gratuityType2 = new GratuityType();
      gratuityType2.gratuityTypeId = GratuityTypeIdEnum.Cortesy;
      gratuityType2.gratuityTypeName = this.translateService.instant('text.courtesy');

      this.gratuityTypeList.push(gratuityType1);
      this.gratuityTypeList.push(gratuityType2);
    });

    this.reservationItemCommercialBudget.gratuityTypeSelectOptionsList =
      this.commonService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        this.gratuityTypeList,
        'gratuityTypeId',
        'gratuityTypeName',
      );
  }

  private setTotals(): void {
    return this.reservationBudgetService.setTotals(this.reservationItemCommercialBudget);
  }

  private setAverageDaily() {
    return this.reservationBudgetService.setAverageDaily(this.reservationItemCommercialBudget);
  }

  private getRateVariation() {
    return this.reservationBudgetService.getRateVariation(this.reservationItemCommercialBudget);
  }
}
