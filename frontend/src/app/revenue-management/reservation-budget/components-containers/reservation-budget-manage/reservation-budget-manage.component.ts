import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/shared/services/auth-service/auth.service';
import * as moment from 'moment';
import { ConfigHeaderPageNew } from './../../../../shared/components/header-page-new/config-header-page-new';
import { RightButtonTop } from './../../../../shared/models/right-button-top';
import { ReservationBudgetHeader } from './../../../../shared/models/revenue-management/reservation-budget/reservation-budget-header';
import { ReservationBudgetCard } from './../../../../shared/models/revenue-management/reservation-budget/reservation-budget-card';
import { ButtonConfig, ButtonType } from './../../../../shared/models/button-config';
import { ToasterEmitService } from '../../../../shared/services/shared/toaster-emit.service';
import { SharedService } from './../../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from './../../../../shared/services/shared/modal-emit-toggle.service';
import { DateService } from './../../../../shared/services/shared/date.service';
import { ReservationBudgetResource } from './../../../../shared/resources/reservation-budget/reservation-budget.resource';
import { SessionParameterService } from './../../../../shared/services/parameter/session-parameter.service';
import { TranslateService } from '@ngx-translate/core';
import {
  ReservationBudget,
  ReservationBudgetDtoList
} from '../../../../shared/models/revenue-management/reservation-budget/reservation-budget';
import { ReservationBudgetService } from '../../../../shared/services/reservation-budget/reservation-budget.service';
import {
  ReservationItemsBudget
} from '../../../../shared/models/revenue-management/reservation-budget/reservation-items-budget';
import {
  ReservationItemCommercialBudget
} from '../../../../shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { BudgetService } from '@app/shared/services/budget/budget.service';
import { ReservationItemBudget } from '../../../../shared/models/revenue-management/reservation-budget/reservation-item-budget';
import { SuccessError } from '../../../../shared/models/success-error-enum';
import { ReservationItemCommercialBudgetConfig } from '../reservation-budget-detail/reservation-item-budget-config';
import {
  ReservationItemCommercialBudgetDay
} from '../../../../shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-day';
import { SelectObjectOption } from '../../../../shared/models/selectModel';
import { Location } from '@angular/common';
import * as _ from 'lodash';
import { CurrencyFormatPipe } from '../../../../shared/pipes/currency-format.pipe';
import { CurrencyService } from '../../../../shared/services/shared/currency-service.service';
import { AssetsService } from '@app/core/services/assets.service';
import { Proforma } from '../../../../shared/models/proforma';
import { ProformaResource } from '../../../../shared/resources/proforma/proforma.resource';
import { CountryCodeEnum } from '../../../../shared/models/country-code-enum';
import {ModalEmitService} from 'app/shared/services/shared/modal-emit.service';
import { FiscalDocumentsService } from '../../../../shared/services/billing-account/fiscal-documents.service';
import { PdfPrintService } from '../../../../shared/services/pdf-print/pdf-print.service';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'reservation-budget-manage',
  templateUrl: './reservation-budget-manage.component.html',
  styleUrls: ['./reservation-budget-manage.component.css'],
})
export class ReservationBudgetManageComponent implements OnInit {
  // Parameters Url
  private propertyId: number;
  private reservationId: number;

  // Header
  public configHeaderPage: ConfigHeaderPageNew;
  public topButtonsListConfig: Array<RightButtonTop>;

  // Component dependencies
  public reservationBudgetHeader: ReservationBudgetHeader;
  public reservationBudgetCardList: Array<ReservationBudgetCard>;
  public reservationItem: ReservationItemsBudget;
  public dateListFromPeriod: Array<Date>;
  public reservationBudgetList: Array<ReservationBudgetDtoList>;
  public commercialBudgetCurrent: ReservationItemCommercialBudget;
  public budgetPageConfig: ReservationItemCommercialBudgetConfig;
  public budgetPageConfigChanged: ReservationItemCommercialBudgetConfig;
  public showBudgetDetail: boolean;
  private readonly MEAL_PLAN_TYPE_ID_DEFAULT = 2;
  private propertyCountryCode: string;

  // Buttons
  public cancelRecalculateReservationButtonConfig: ButtonConfig;
  public confirmRecalculateReservationButtonConfig: ButtonConfig;

  // new Budget
  public reservationItemStatus: number;
  private reservationItemIsMigrated: boolean;
  public period: any;
  public commercialBudgetList: Array<ReservationItemCommercialBudget>;
  public commercialBudgetSelectedItemKey: any;
  public selectedReservationBudgetList: Array<ReservationBudget>;
  public originalReservationBudgetList: Array<ReservationBudget>;
  public gratuityType: number;


  // Modal dependencies
  public recalculateReservationModalId: string;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private reservationBudgetResource: ReservationBudgetResource,
    private toasterEmitService: ToasterEmitService,
    private currencyFormatPipe: CurrencyFormatPipe,
    private budgetService: BudgetService,
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private dateService: DateService,
    private sessionParameterService: SessionParameterService,
    private translateService: TranslateService,
    private location: Location,
    private currencyService: CurrencyService,
    private reservationBudgetService: ReservationBudgetService,
    private assetsService: AssetsService,
    private proFormaResource: ProformaResource,
    private propertyService: PropertyStorageService,
    private modalEmitService: ModalEmitService,
    private fiscalDocumentService: FiscalDocumentsService,
    private pdfPrintService: PdfPrintService,
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  private setVars(): void {
    this.showBudgetDetail = false;
    this.budgetPageConfig = new ReservationItemCommercialBudgetConfig();
    this.budgetPageConfig.reservationBudgetTableList = new Array<ReservationBudget>();
    this.recalculateReservationModalId = 'recalculate-reservation-modal-id';
    this.propertyId = this.propertyService.getCurrentPropertyId();
    this.reservationId = this.route.snapshot.params.reservationId;
    this.propertyCountryCode = this.authService.getPropertyCountryCode();
    this.setConfigHeaderPage();
    this.setButtonConfig();
    this.setTopButtonsListConfig();
    this.setReservationBudgetHeaderAndCardList();
  }

  private setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  private setButtonConfig(): void {
    this.cancelRecalculateReservationButtonConfig = this.sharedService.getButtonConfig(
      'cancel-modal-recalculate-reservation',
      this.closeModalRecalculateReservation,
      'action.no',
      ButtonType.Secondary,
    );
    this.confirmRecalculateReservationButtonConfig = this.sharedService.getButtonConfig(
      'confirm-modal-recalculate-reservation',
      this.recalculateReservation,
      'action.yes',
      ButtonType.Primary,
    );
  }

  private closeModalRecalculateReservation = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.recalculateReservationModalId);
  }

  // Recalculate reservation
  private recalculateReservation = () => {
    this.reservationBudgetResource.recalculateReservation(this.propertyId, this.reservationId).subscribe(result => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('variable.recalculateSuccessM', {
          labelName: this.translateService.instant('label.reservationBudget'),
        }),
      );
      this.modalEmitToggleService.emitToggleWithRef(this.recalculateReservationModalId);

      this.recalculateReservationValues();
    });
  }

  private recalculateReservationValues() {
    this.showBudgetDetail = false;
    this.budgetPageConfig = new ReservationItemCommercialBudgetConfig();
    this.budgetPageConfig.reservationBudgetTableList = new Array<ReservationBudget>();
    this.setReservationBudgetHeaderAndCardList();
  }

  private setTopButtonsListConfig(): void {
    this.topButtonsListConfig = [
      {
        title: 'text.recalculateReservation',
        callback: this.openModalRecalculateReservation,
        iconDefault: this.assetsService.getImgUrlTo('recalcular-reserva-btn.svg'),
        iconAlt: 'img.recalculateReservation',
      }
    ];

    if (this.propertyCountryCode == CountryCodeEnum.PT) {
      this.topButtonsListConfig.push(
        {
          title: 'label.issueProformaOfGroup',
          callback: this.issueProforma,
          iconDefault: this.assetsService.getImgUrlTo('emitir-proforma-topo-icon.svg'),
          iconAlt: 'img.issueProformaOfGroup',
        }
      );
    }
  }

  private openModalRecalculateReservation = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.recalculateReservationModalId);
  }

  private setReservationBudgetHeaderAndCardList(): void {
    this.getReservationBudgetHeader();
    this.getReservationBudgetCardListAndSetReservationItemCurrent();
  }

  private getReservationBudgetHeader(): void {
    this.reservationBudgetHeader = new ReservationBudgetHeader();
    this.reservationBudgetResource.getReservationHeader(this.reservationId).subscribe(response => {
      this.reservationBudgetHeader = response;
    });
  }

  private getReservationBudgetCardListAndSetReservationItemCurrent(): void {
    this.reservationBudgetCardList = [];
    this.reservationBudgetResource.getAllReservationItems(this.propertyId, this.reservationId).subscribe(response => {
      this.setReservationItemListOfCardList(response.items);
      this.setCardToActive(this.reservationBudgetCardList[0].reservationItem.id);
      this.setReservationBudgetCurrent(this.reservationBudgetCardList[0].reservationItem);
    });
  }

  private setReservationItemListOfCardList(items: ReservationItemsBudget[]): void {
    items.forEach(reservationItem => {
      const reservationBudgetCard = new ReservationBudgetCard();
      reservationBudgetCard.reservationItem = reservationItem;
      this.reservationBudgetCardList.push(reservationBudgetCard);
    });
  }

  private setCardToActive(reservationItemId: number): void {
    const billingAccountCardActive = this.reservationBudgetCardList.find(b => b.reservationItem.id == reservationItemId);
    if (billingAccountCardActive) {
      billingAccountCardActive.isActive = true;
    } else {
      this.reservationBudgetCardList[0].isActive = true;
    }
  }

  private getPeriod(item) {
    if (item) {
      const { estimatedArrivalDate, estimatedDepartureDate } = item;
      return {
        beginDate: estimatedArrivalDate,
        endDate: estimatedDepartureDate
      };
    }
  }
  public setReservationBudgetCurrent(reservationItem: ReservationItemsBudget): void {

    // set currentInfo
    this.reservationItemIsMigrated = reservationItem.isMigrated;
    this.reservationItemStatus = reservationItem.statusId;
    this.period = this.getPeriod(reservationItem);


    this.showBudgetDetail = false;
    reservationItem.estimatedArrivalDate = moment(reservationItem.estimatedArrivalDate).toDate();
    reservationItem.estimatedDepartureDate = moment(reservationItem.estimatedDepartureDate).toDate();
    this.dateListFromPeriod = this.dateService.convertPeriodToDateList(
      reservationItem.estimatedArrivalDate,
      reservationItem.estimatedDepartureDate
    );
    this.reservationItem = reservationItem;

    this.getReservationBudgetList(reservationItem);
  }

  private getReservationBudgetList(reservationItem: ReservationItemsBudget) {
    const reservationItemCommercialBudgetSearch = this.reservationBudgetService.getReservationItemCommercialBudgetSearch(
      this.propertyId,
      reservationItem.requestedRoomTypeId,
      this.dateListFromPeriod,
      null,
      reservationItem.adultCount,
      null,
      reservationItem.id,
    );

    this.reservationBudgetResource.getReservationItemBudget(reservationItemCommercialBudgetSearch).subscribe(reservationItemBudget => {

      // new Budget
      this.commercialBudgetList = reservationItemBudget.commercialBudgetList;
      this.commercialBudgetSelectedItemKey = this.reservationBudgetService
        .prepareAndReturnBudgetKey( { roomTypeId: this.reservationItem.requestedRoomTypeId , ...reservationItemBudget},
          reservationItemBudget.commercialBudgetList );

      this.selectedReservationBudgetList = reservationItemBudget.reservationBudgetDtoList;
      this.originalReservationBudgetList = [...reservationItemBudget.reservationBudgetDtoList];

      this.setCommercialBudgetList(this.budgetPageConfig, reservationItemBudget);
      this.budgetPageConfig.wasConfirmed = true;
      this.budgetPageConfig.isBudgetManagePage = true;
      this.budgetPageConfig.reservationItemCode = reservationItemBudget.reservationItemCode;
      this.budgetPageConfig.gratuityTypeId = reservationItemBudget.gratuityTypeId;
      this.budgetPageConfig.isGratuityType = !!reservationItemBudget.gratuityTypeId;
      this.budgetPageConfig.reservationItemId = reservationItemBudget.reservationItemId;
      this.budgetPageConfig.reservationItemCommercialBudgetCurrent = new ReservationItemCommercialBudget();
      this.budgetPageConfig.reservationItemCommercialBudgetCurrent.ratePlanId = reservationItemBudget.ratePlanId;
      this.budgetPageConfig.reservationItemCommercialBudgetCurrent.currencyId = reservationItemBudget.currencyId;
      this.budgetPageConfig.reservationItemCommercialBudgetCurrent.mealPlanTypeId = reservationItemBudget.mealPlanTypeId;
      this.budgetPageConfig.reservationItemCommercialBudgetCurrent.commercialBudgetDayList = [];
      this.budgetPageConfig.reservationItemCommercialBudgetCurrent.totalBudget = 0;
      this.budgetPageConfig.reservationBudgetTableList = new Array<ReservationBudget>();
      if (reservationItemBudget.reservationBudgetDtoList) {
        reservationItemBudget.reservationBudgetDtoList.forEach(reservationBudget => {
          this.budgetPageConfig.reservationBudgetTableList.push(reservationBudget);
          this.budgetPageConfig.reservationItemCommercialBudgetCurrent.commercialBudgetDayList.push(
            this.getCommercialBudgetDay(reservationBudget)
          );
        });
      }

      this.showBudgetDetail = true;
    });
  }

  private setCommercialBudgetList(reservationItemCommercialBudgetConfig: ReservationItemCommercialBudgetConfig,
                                  reservationItemBudget: ReservationItemBudget): void {
    this.budgetPageConfig.selectReservationItemCommercialBudgetList = new Array<SelectObjectOption>();
    this.budgetPageConfig =
    this.budgetService.getCommercialBudgetList(reservationItemCommercialBudgetConfig, reservationItemBudget);
  }

  private getCommercialBudgetDay(reservationBudget: ReservationBudget) {
    const commercialBudgetDay = new ReservationItemCommercialBudgetDay();
    commercialBudgetDay.day = reservationBudget.budgetDay;
    commercialBudgetDay.baseRateAmount = reservationBudget.baseRate;
    commercialBudgetDay.baseRateWithRatePlanAmount = reservationBudget.agreementRate;
    commercialBudgetDay.mealPlanTypeId = reservationBudget.mealPlanTypeId;
    commercialBudgetDay.currencySymbol = reservationBudget.currencySymbol;
    commercialBudgetDay.currencyId = reservationBudget.currencyId;
    commercialBudgetDay.childRateAmount = reservationBudget.childRate;
    return commercialBudgetDay;
  }

  // Recalculate ReservationItem
  public recalculateBudgetReservationItem(reservationItemBudget: ReservationItemsBudget) {
    const elementFound: ReservationItemCommercialBudget = this.commercialBudgetList
      .find( item => _.isEqual(this.reservationBudgetService.getReservationItemCommercialBudgetIdentifier(item),
        this.commercialBudgetSelectedItemKey));
    if (elementFound) {
      this.selectedReservationBudgetList = this.reservationBudgetService
        .transformCommercialBudgetDayListInReservationBudget(elementFound.commercialBudgetDayList,
          reservationItemBudget.gratuityTypeId,
          elementFound.commercialBudgetName, null);
      this.selectedReservationBudgetList.forEach(item => {
        item.separatedRate = 0;
      });
      this.originalReservationBudgetList = [...this.selectedReservationBudgetList];
    }
  }

  public cancelBudgetModal() {
    this.location.back();
  }

  public confirmBudgetModal(event) {
    const { ratePlanId, currencyId, mealPlanTypeId } = event.commercialBudgetItem;
    const { gratuityTypeId, reservationBudgetList } = event;

    reservationBudgetList.forEach(budget => {
      budget.id = 0;
    });

    const reservationItemBudgetChanged = <ReservationItemBudget>{
      gratuityTypeId,
      currencyId,
      ratePlanId,
      mealPlanTypeId,
      reservationBudgetDtoList: reservationBudgetList
    };
    this.reservationBudgetResource
      .updateBudgetReservationItem(this.propertyId, this.reservationItem.id, reservationItemBudgetChanged)
      .subscribe(result => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.recalculateSuccessM', {
            labelName: this.translateService.instant('label.reservationItemBudget'),
          }),
        );

        // update header info
        this.getReservationBudgetHeader();
      });
  }

  private issueProforma = () => {

    this.modalEmitToggleService.closeAllModals();
    this.modalEmitService.emitSimpleModal(
      this.translateService.instant(`label.emitProForma`),
      this.translateService.instant('text.tourismTaxInProforma'),
      () => this.emitProforma(true), null, () => this.emitProforma()  );

  }

  public emitProforma(withTourism = false) {
    const proforma = new Proforma();
    proforma.reservationId = this.reservationId;
    proforma.launchTourismTax = withTourism;
    this.proFormaResource.issueProforma(proforma)
      .subscribe(response => {
        this.printProforma(proforma);
      });
  }

  private printProforma(proforma: Proforma) {
    this
      .fiscalDocumentService
      .getProforma(proforma)
      .subscribe(
      (byteCodes: any) => {
        if ( byteCodes ) {
          this.pdfPrintService.printPdfFromByteCode(byteCodes);
        }
      }, () => {
        this.showMessagePrintInvoiceError();
      });
  }

  private showMessagePrintInvoiceError() {
    this
      .toasterEmitService
      .emitChange(
        SuccessError.error,
        this.translateService.instant('alert.errorPrintInvoice')
      );
  }


}
