import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonConfig, ButtonType } from './../../../../shared/models/button-config';
import { SharedService } from './../../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from './../../../../shared/services/shared/modal-emit-toggle.service';
import { ReservationItemsBudget } from '../../../../shared/models/revenue-management/reservation-budget/reservation-items-budget';

@Component({
  selector: 'reservation-budget-detail-header',
  templateUrl: './reservation-budget-detail-header.component.html',
})
export class ReservationBudgetDetailHeaderComponent implements OnInit {
  @Input() reservationItem: ReservationItemsBudget;

  @Output() public shouldRecalculate: EventEmitter<ReservationItemsBudget> = new EventEmitter();

  // Buttons
  public openModalRecalculateReservationItemButtonConfig: ButtonConfig;
  public cancelRecalculateReservationItemButtonConfig: ButtonConfig;
  public confirmRecalculateReservationItemButtonConfig: ButtonConfig;

  // Modal dependencies
  public recalculateReservationItemModalId: string;

  constructor(private sharedService: SharedService, private modalEmitToggleService: ModalEmitToggleService) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars(): void {
    this.recalculateReservationItemModalId = 'recalculate-reservation-item-modal-id';
    this.setButtonConfig();
  }

  private setButtonConfig(): void {
    this.openModalRecalculateReservationItemButtonConfig = this.sharedService.getButtonConfig(
      'recalculate-reservation-item',
      this.openModalRecalculateReservationItem,
      'text.recalculateReservationItem',
      ButtonType.Primary,
    );
    this.cancelRecalculateReservationItemButtonConfig = this.sharedService.getButtonConfig(
      'cancel-modal-recalculate-reservation-item',
      this.closeModalRecalculateReservationItem,
      'action.no',
      ButtonType.Secondary,
    );
    this.confirmRecalculateReservationItemButtonConfig = this.sharedService.getButtonConfig(
      'confirm-modal-recalculate-reservation-item',
      this.recalculateBudgetReservationItem,
      'action.yes',
      ButtonType.Primary,
    );
  }

  private openModalRecalculateReservationItem = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.recalculateReservationItemModalId);
  }

  private closeModalRecalculateReservationItem = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.recalculateReservationItemModalId);
  }

  private recalculateBudgetReservationItem = () => {
    this.closeModalRecalculateReservationItem();
    this.shouldRecalculate.emit(this.reservationItem);
  }
}
