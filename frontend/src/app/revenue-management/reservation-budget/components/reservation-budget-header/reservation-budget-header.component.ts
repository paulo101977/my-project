import { Component, OnInit, Input } from '@angular/core';
import { ReservationBudgetHeader } from './../../../../shared/models/revenue-management/reservation-budget/reservation-budget-header';

@Component({
  selector: 'reservation-budget-header',
  templateUrl: './reservation-budget-header.component.html',
  styleUrls: ['./reservation-budget-header.component.css'],
})
export class ReservationBudgetHeaderComponent implements OnInit {
  @Input() reservationBudgetHeader: ReservationBudgetHeader;

  constructor() {}

  ngOnInit() {}
}
