import { Component, OnInit, Input } from '@angular/core';
import { ReservationBudgetCard } from './../../../../shared/models/revenue-management/reservation-budget/reservation-budget-card';

@Component({
  selector: 'reservation-budget-card',
  templateUrl: './reservation-budget-card.component.html',
  styleUrls: ['./reservation-budget-card.component.css'],
})
export class ReservationBudgetCardComponent implements OnInit {
  @Input() public reservationBudgetCard: ReservationBudgetCard;

  constructor() {}

  ngOnInit() {}

  getStartDate() {
    if (this.reservationBudgetCard && this.reservationBudgetCard.reservationItem) {
      let startDate = this.reservationBudgetCard.reservationItem.checkIn;
      if (!startDate) {
        startDate = this.reservationBudgetCard.reservationItem.estimatedArrivalDate;
      }
      return startDate;
    }
  }
}
