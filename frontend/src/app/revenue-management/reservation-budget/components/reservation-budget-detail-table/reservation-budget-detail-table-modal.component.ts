import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {
  BudgetTableConfig,
  ReservationItemCommercialBudgetConfig
} from '../../components-containers/reservation-budget-detail/reservation-item-budget-config';
import { SelectObjectOption } from '../../../../shared/models/selectModel';
import { CommomService } from '../../../../shared/services/shared/commom.service';
import { ReservationBudget } from '../../../../shared/models/revenue-management/reservation-budget/reservation-budget';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { ReservationBudgetService } from '../../../../shared/services/reservation-budget/reservation-budget.service';
import {
  ReservationItemCommercialBudgetDay
} from '../../../../shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-day';
import { ModalEmitToggleService } from '../../../../shared/services/shared/modal-emit-toggle.service';

@Component({
  selector: 'reservation-budget-detail-table-modal',
  templateUrl: 'reservation-budget-detail-table-modal.component.html',
  styleUrls: ['reservation-budget-detail-table-modal.component.css']
})

export class ReservationBudgetDetailTableModalComponent implements AfterViewInit, OnInit, OnChanges {

  public readonly MODAL_UPDATE_DAY_VALUE = 'manual_rate_change_modal';

  @ViewChild(DatatableComponent) myTable: DatatableComponent;

  @Input() budgetTableConfig: BudgetTableConfig;
  @Input() enableAllInputs: boolean;
  @Input() reservationItemCommercialBudgetConfig: ReservationItemCommercialBudgetConfig;
  @Input() isGratuityType: boolean;
  @Input() reservationBudgetTableList: Array<ReservationBudget>;

  @Output() totalsChange = new EventEmitter();
  @Output() reservationBudgetListChange = new EventEmitter();

  private rowIndexChoose: number;
  private mealPlanChoose: any;

  public optionsCurrencyMask: any;
  public mealPlanTypeSelectList: Array<SelectObjectOption>;
  public hasSymbol: boolean;

  constructor(
    public commomService: CommomService,
    private sharedService: SharedService,
    private reservationBudgetService: ReservationBudgetService,
    private modalToggleService: ModalEmitToggleService
  ) {}

  ngAfterViewInit() {
    this.myTable.rowHeight = 60;
    this.myTable.offset = 0;
  }

  ngOnInit() {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
    this.setMealPlanTypeSelectList();
  }

  ngOnChanges() {
    this.hasSymbol = this.reservationBudgetService.showCurrencySymbol(this.reservationItemCommercialBudgetConfig, this.isGratuityType);
  }

  private setMealPlanTypeSelectList(): void {
    this.mealPlanTypeSelectList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
      this.budgetTableConfig.mealPlanTypeList,
      'mealPlanTypeId',
      'propertyMealPlanTypeName'
    );
  }

  public discountChange(rowIndex: number): void {
    this.reservationBudgetService.setDiscount(this.reservationBudgetTableList, rowIndex);
    this.totalsChange.emit();
    this.reservationBudgetListChange.emit(this.reservationBudgetTableList);
  }



  public manualRateChange(rowIndex: number): void {

    if (isNaN(this.reservationBudgetTableList[rowIndex].manualRate)) {
      this.reservationBudgetTableList[rowIndex].manualRate = 0;
    }

    if (this.reservationBudgetTableList[rowIndex].manualRate < this.reservationBudgetTableList[rowIndex].agreementRate) {
      this.reservationBudgetTableList[rowIndex].discount = this.sharedService.getDiscountPercentage(
        this.reservationBudgetTableList[rowIndex].manualRate,
        this.reservationBudgetTableList[rowIndex].agreementRate
      );
    } else {
      this.reservationBudgetTableList[rowIndex].discount = 0;
    }
    this.totalsChange.emit();
    this.reservationBudgetListChange.emit(this.reservationBudgetTableList);
  }

  public updateMealPlanTypeIdCurrent(mealPlanTypeId: any, rowIndex: number): void {
    this.updateManualRateBasedOnMealPlan(rowIndex, mealPlanTypeId);
  }

  private updateManualRateBasedOnMealPlan(rowIndex: number, mealPlanTypeId: any) {

    const originalList = this.reservationItemCommercialBudgetConfig.selectReservationItemCommercialBudgetOriginalList;
    const current = this.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent;
    if (current) {
      // filter by mealplanId and ratePlan
      const element = originalList.filter(item => item.mealPlanTypeId == mealPlanTypeId && item.ratePlanId == current.ratePlanId)[0];
      if (element) {
        const day = this.reservationBudgetTableList[rowIndex].budgetDay;
        const dayInfo: ReservationItemCommercialBudgetDay = element.commercialBudgetDayList.filter(d => d.day == day)[0];
        this.reservationBudgetTableList[rowIndex].manualRate = dayInfo.total;
        this.reservationBudgetTableList[rowIndex].agreementRate = dayInfo.total;
        this.reservationBudgetTableList[rowIndex].discount = 0;

      }
    }
    this.totalsChange.emit();
    this.reservationBudgetListChange.emit(this.reservationBudgetTableList);
  }

  public mantainSameValue() {
    this.modalToggleService.emitCloseWithRef(this.MODAL_UPDATE_DAY_VALUE);
  }

  public updateValue() {
    this.modalToggleService.emitCloseWithRef(this.MODAL_UPDATE_DAY_VALUE);
    this.updateManualRateBasedOnMealPlan(this.rowIndexChoose, this.mealPlanChoose);
  }

  public getRowClass(row) {
    return { 'thf-datatable-overbooking-bg': row.overbooking };
  }
}
