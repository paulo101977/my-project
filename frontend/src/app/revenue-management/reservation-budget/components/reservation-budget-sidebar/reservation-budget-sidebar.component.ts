import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ReservationBudgetCard } from './../../../../shared/models/revenue-management/reservation-budget/reservation-budget-card';
import { ReservationItemsBudget } from '../../../../shared/models/revenue-management/reservation-budget/reservation-items-budget';

@Component({
  selector: 'reservation-budget-sidebar',
  templateUrl: './reservation-budget-sidebar.component.html',
  styleUrls: ['./reservation-budget-sidebar.component.css'],
})
export class ReservationBudgetSidebarComponent implements OnInit {
  @Input() reservationBudgetCardList: Array<ReservationBudgetCard>;
  @Output() clickReservationBudget = new EventEmitter<ReservationItemsBudget>();

  // Filter
  public filterGuestName: string;
  public filterParameters: any;
  public filterParentObjectName: string;

  constructor() {}

  ngOnInit() {
    this.setVars();
  }

  private setVars(): void {
    this.filterParentObjectName = 'reservationItem';
    this.filterParameters = ['searchableNames', 'roomNumber', 'roomTypeAbbreviation', 'roomTypeName', 'reservationItemStatus'];
  }

  public chooseItem(reservationBudgetCard: ReservationBudgetCard): void {
    this.clickReservationBudget.emit(reservationBudgetCard.reservationItem);
    this.resetActiveList();
    reservationBudgetCard.isActive = true;
  }

  public resetActiveList() {
    this.reservationBudgetCardList.forEach(card => (card.isActive = false));
  }
}
