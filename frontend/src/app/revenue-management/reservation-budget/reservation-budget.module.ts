import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ReservationBudgetManageComponent } from './components-containers/reservation-budget-manage/reservation-budget-manage.component';
import { ReservationBudgetSidebarComponent } from './components/reservation-budget-sidebar/reservation-budget-sidebar.component';
import { ReservationBudgetCardComponent } from './components/reservation-budget-card/reservation-budget-card.component';
import { ReservationBudgetHeaderComponent } from './components/reservation-budget-header/reservation-budget-header.component';
import {
  ReservationBudgetDetailHeaderComponent
} from './components/reservation-budget-detail-header/reservation-budget-detail-header.component';
import {
  ReservationBudgetDetailModalComponent
} from './components-containers/reservation-budget-detail/reservation-budget-detail-modal.component';
import {
  ReservationBudgetDetailTableModalComponent
} from './components/reservation-budget-detail-table/reservation-budget-detail-table-modal.component';
import { ReservationBudgetRoutingModule } from './reservation-budget-routing.module';

@NgModule({
  imports: [CommonModule, SharedModule, FormsModule, ReservationBudgetRoutingModule],
  declarations: [
    ReservationBudgetManageComponent,
    ReservationBudgetSidebarComponent,
    ReservationBudgetCardComponent,
    ReservationBudgetHeaderComponent,
    ReservationBudgetDetailModalComponent,
    ReservationBudgetDetailHeaderComponent,
    ReservationBudgetDetailTableModalComponent
  ],
  exports: [
    ReservationBudgetManageComponent,
    ReservationBudgetDetailModalComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ReservationBudgetModule {}
