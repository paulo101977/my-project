import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { HttpParams } from '@angular/common/http';
import { MealPlan, MealPlanList } from 'app/revenue-management/meal-plan/models/meal-plan';
import { ItemsResponse } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class MealPlanResource {
  private url = 'PropertyMealPlanTypeRates';

  constructor(private thexApiService: ThexApiService) {}


  public getMealPlanById( mealPlanId: string ): Observable<MealPlan> {
    return this
      .thexApiService
      .get<MealPlan>(`${this.url}/${mealPlanId}`);
  }


  public addMealPlanRate( item: MealPlan ): Observable<any> {
    return this
      .thexApiService
      .post(`${this.url}`, item);
  }


  public getAllMealPlanRate(queryParams?: any): Observable<ItemsResponse<MealPlanList>> {
    let  params = new HttpParams();

    if ( queryParams ) {
      const { currencyId, startDate, endDate } = queryParams;
      params = new HttpParams()
        .set('currencyId', currencyId)
        .set('startDate', startDate)
        .set('endDate', endDate);
    }

    return this
      .thexApiService
      .get<ItemsResponse<MealPlanList>>(
        `${this.url}/getAll`,
        { params }
      );
  }
}
