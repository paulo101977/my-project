import { createServiceStub } from '../../../../../../testing';
import { MealPlanResource } from 'app/revenue-management/meal-plan/resources/meal-plan-resource';
import { MealPlan } from 'app/revenue-management/meal-plan/models/meal-plan';
import { of } from 'rxjs';

export const mealPlanResourceStub = createServiceStub(MealPlanResource, {
    addMealPlanRate: (item: MealPlan) => of( null ),

    getAllMealPlanRate: (queryParams?: any) => of(null),

    getMealPlanById: (mealPlanId: string) => of(null),
});

