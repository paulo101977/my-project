export enum Weekdays {
  sunday = 0,
  monday = 1,
  tuesday = 2,
  wednesday = 3,
  thursday = 4,
  friday = 5,
  saturday = 6
}

export class MealCurrency {
  public currencyName: string;
  public id: string;
  public symbol: string;
}

export class MealPlan {
  public currencyId: string;
  public finalDate: string;
  public initialDate: string;
  public propertyMealPlanTypeRateHistoryList: Array<PropertyMealPlanTypeRateHistory>;
  public id?: string;
  public propertyId?: number;
  public currency?: MealCurrency;
}

export class PropertyMealPlanTypeRateHistory {
  public adultMealPlanAmount?: number;
  public child1MealPlanAmount?: number;
  public child2MealPlanAmount?: number;
  public child3MealPlanAmount?: number;
  public mealPlanTypeId: number;
  public friday: boolean;
  public saturday: boolean;
  public sunday: boolean;
  public thursday: boolean;
  public tuesday: boolean;
  public wednesday: boolean;
  public monday: boolean;
  public mealPlanTypeDefault: boolean;
  public daysOfWeekDto: number[];
}

export interface  MealPlanList extends  MealPlan, PropertyMealPlanTypeRateHistory {}
