export enum Guests {
  ADULT = 'adult',
  CHILD_ONE = 'child1',
  CHILD_TWO = 'child2',
  CHILD_THREE = 'child3'
}
