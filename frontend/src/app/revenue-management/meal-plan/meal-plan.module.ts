import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MealPlanNewAddComponent } from './containers/meal-plan-new-add/meal-plan-new-add.component';
import { MealPlanRoutingModule } from './meal-plan-routing.module';
import { SharedModule } from 'app/shared/shared.module';
import { MealPlanListComponent } from './containers/meal-plan-list/meal-plan-list.component';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    MealPlanRoutingModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    MealPlanNewAddComponent,
    MealPlanListComponent
  ]
})
export class MealPlanModule { }
