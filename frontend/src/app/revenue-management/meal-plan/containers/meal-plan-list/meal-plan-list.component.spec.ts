import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { MealPlanListComponent } from './meal-plan-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import {
  CONFIGURATION_PROPERTY,
  DATA,
  DEFAUL_MEAL_PLAN_LIST_RESULT,
  DEFAULT_MEAL_PLAN_LIST
} from 'app/revenue-management/mock-data';
import { of } from 'rxjs';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { createAccessorComponent } from '../../../../../../testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { currencyExchangeResourceStub } from 'app/shared/resources/currency-exchange/testing';
import { sessionParameterServiceStub } from 'app/shared/services/parameter/testing';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { mealPlanResourceStub } from 'app/revenue-management/meal-plan/resources/testing';

const thxDatepicker = createAccessorComponent('thx-date-range-picker');
const thxError = createAccessorComponent('thx-error');

describe('MealPlanListComponent', () => {
  let component: MealPlanListComponent;
  let fixture: ComponentFixture<MealPlanListComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        MealPlanListComponent,
        thxError,
        thxDatepicker,
      ],
      imports: [
        TranslateTestingModule,
        ReactiveFormsModule,
        FormsModule,
        RouterTestingModule,
      ],
      providers: [
        sharedServiceStub,
        currencyExchangeResourceStub,
        sessionParameterServiceStub,
        dateServiceStub,
        mealPlanResourceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(MealPlanListComponent);
    component = fixture.componentInstance;

    spyOn(component, 'setDefaultCurrency').and.callThrough();
    spyOn(component, 'fillForm').and.callThrough();
    spyOn(component, 'getCurrencies').and.callThrough();
    spyOn<any>(component['router'], 'navigate').and.callFake( f => f );
    spyOn<any>(component['currencyExchangeResource'], 'getCurrencies')
        .and.returnValue(of(DATA[CONFIGURATION_PROPERTY].currencyList));
    spyOn(component['sessionParameterService'], 'getParameterValue')
        .and.returnValue(DATA[CONFIGURATION_PROPERTY].currencyList.items[1].id);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', fakeAsync(() => {
    spyOn(component, 'setButtonConfig');
    spyOn(component, 'setForm');
    spyOn(component, 'setColumns');
    component['route'].snapshot.params = { propertyId: 1 };

    component.ngOnInit();

    tick();

    expect(component.setDefaultCurrency).toHaveBeenCalled();
    expect(component.setButtonConfig).toHaveBeenCalled();
    expect(component.setForm).toHaveBeenCalled();
    expect(component.setColumns).toHaveBeenCalled();
    expect(component.getCurrencies).toHaveBeenCalled();
    expect(component.fillForm).toHaveBeenCalled();
  }));

  it('should getCurrencies', fakeAsync(() => {
    const currencyList = DATA[CONFIGURATION_PROPERTY].currencyList.items;
    const { currencyName, symbol } = currencyList[2];
    component.getCurrencies();

    tick();

    expect(component.currencyList[2].value).toEqual(`${currencyName} ${symbol}`);
  }));

  it('should test setDefaultCurrency', fakeAsync(() => {
    component.setForm();
    component.setDefaultCurrency();

    tick();

    expect(component['form'].get('currencyId').value).toEqual(DATA[CONFIGURATION_PROPERTY].currencyList.items[1].id);
  }));

  it('should test fillForm', () => {
    const params = {
      currencyId: '1',
      beginDate: '2',
      endDate: '3',
    };

    component['route'].snapshot.queryParams = params;
    component.fillForm();

    const formResult =  component['form'].getRawValue();

    expect(formResult.currencyId).toEqual(params.currencyId);
    expect(formResult.range.beginDate).toEqual(params.beginDate);
    expect(formResult.range.endDate).toEqual(params.endDate);
  });

  it('should test search', fakeAsync(() => {
    const params = {
      currencyId: '1',
      startDate: '2',
      endDate: '3',
    };

    spyOn<any>(component['mealPlanResource'], 'getAllMealPlanRate')
      .and.returnValue(of({ items: null }));
    spyOn<any>(component, 'filterDuplicateData').and.callFake( () => params);
    spyOn(component, 'mapItemsToMealPlanList').and.callFake( (p) => p);

    component['form'].setValue({
      range: { beginDate: params.startDate, endDate: params.endDate },
      currencyId: params.currencyId
    });

    component.search();

    tick();

    expect(component['mealPlanResource'].getAllMealPlanRate).toHaveBeenCalledWith(params);
  }));

  it('should test getWeekList', () => {
    const item = {
      sunday: false,
      monday: true,
      tuesday: true,
      wednesday: false,
      thursday: true,
      friday: true,
      saturday: false,
    };

    expect(component.getWeekList(item)).toEqual([1, 2, 4, 5]);

  });



  it('should test mapItemsToMealPlanList', () => {
    const items = [...DATA[DEFAULT_MEAL_PLAN_LIST]];
    const result = component.mapItemsToMealPlanList(items);

    expect(result).toEqual(DATA[DEFAUL_MEAL_PLAN_LIST_RESULT]);
  });

  it('should test filterDuplicateData', () => {
    const items = [...DATA[DEFAULT_MEAL_PLAN_LIST]];
    const result = component.filterDuplicateData(items);

    expect(result.length).toEqual(1);
  });
});
