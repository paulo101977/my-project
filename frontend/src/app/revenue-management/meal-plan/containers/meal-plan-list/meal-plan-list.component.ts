import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { CurrencyExchangeResource } from 'app/shared/resources/currency-exchange/currency-exchange.resource';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MealPlanResource } from 'app/revenue-management/meal-plan/resources/meal-plan-resource';
import { MealPlanService } from 'app/revenue-management/meal-plan/services/meal-plan.service';
import { Weekdays } from 'app/revenue-management/meal-plan/models/meal-plan';
import { DateService } from 'app/shared/services/shared/date.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-meal-plan-list',
  templateUrl: './meal-plan-list.component.html',
  styleUrls: ['./meal-plan-list.component.scss']
})
export class MealPlanListComponent implements OnInit {

  public form: FormGroup;
  public searchButtonConfig: ButtonConfig;
  public currencyId: string;
  public propertyId: number;
  public currencyList: Array<any>;
  public columns: any[];


  public mealplanList: Array<any>;

  @ViewChild('weekday') weekday: TemplateRef<any>;


  constructor(
    private fb: FormBuilder,
    private sharedService: SharedService,
    private currencyExchangeResource: CurrencyExchangeResource,
    private sessionParameterService: SessionParameterService,
    private route: ActivatedRoute,
    private mealPlanResource: MealPlanResource,
    private router: Router,
    private dateService: DateService,
    private mealPlanService: MealPlanService,
    private translateService: TranslateService,
  ) { }

  public setButtonConfig() {
    this.searchButtonConfig = this.sharedService.getButtonConfig(
      'search',
      this.search,
      'action.searchOnly',
      ButtonType.Primary,
    );

    this.searchButtonConfig.isDisabled = true;
  }

  public setDefaultCurrency() {
    const currencyId = this
      .sessionParameterService
      .getParameterValue(this.propertyId, ParameterTypeEnum.DefaultCurrency);

    if ( currencyId ) {
      this.form.patchValue({currencyId});
    }
  }

  public getCurrencies(): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .currencyExchangeResource
        .getCurrencies()
        .subscribe( ({ items }) => {
          if ( items ) {
            this.currencyList = items.map( item => {
              const { id, currencyName, symbol } = item;
              return {
                key: id,
                value: `${currencyName} ${symbol}`
              };
            });
          }
          resolve(null);
        });
    });

  }


  public getWeekList(item) {
    const keysNames = this.mealPlanService.getWeekNames();

    return keysNames.filter( key => {
      return item[key];
    }).map( key => {
      return Weekdays[key];
    });
  }


  public mapItemsToMealPlanList(items) {
    const { dateService } = this;
    if ( items ) {
      return items.map( item => {
        const {
          propertyMealPlanTypeRateHeaderId,
          propertyMealPlanTypeRateHeader: {
            initialDate,
            finalDate,
            currency: {
              symbol,
              currencyName
            },
          },
          mealPlanType: {
            mealPlanTypeName
          },
        } = item;

        const dateBegin = dateService.convertUniversalDateToViewFormat(initialDate);
        const endDate = dateService.convertUniversalDateToViewFormat(finalDate);
        const validity = this.translateService.instant(
          'label.historicMealPlanDateToDate',
          {
            dateBegin, endDate
          });


        return {
          headerId: propertyMealPlanTypeRateHeaderId,
          weekList: this.getWeekList(item),
          validity,
          currency: `${ currencyName } ${ symbol}`,
          defaultMealPlan: mealPlanTypeName
        };
      });
    }

    return [];
  }


  public search = () => {
    const {
      range: { beginDate, endDate }, currencyId
    } = this.form.getRawValue();

    const params = {
      currencyId,
      startDate: beginDate,
      endDate
    };

    this
      .mealPlanResource
      .getAllMealPlanRate(params)
      .subscribe( ({items}) => {
        const noDuplicateItems = this.filterDuplicateData(items);

        this.mealplanList = this.mapItemsToMealPlanList( noDuplicateItems );
    });
  }

  public filterDuplicateData( items ) {
    const result = [];

    items.forEach( item => {
      const found = result.find(fItem => {
        return item.propertyMealPlanTypeRateHeaderId ===
          fItem.propertyMealPlanTypeRateHeaderId;
      });

      if ( !found && item.mealPlanTypeDefault ) {
        result.push(item);
      }
    });

    return result;
  }

  public setColumns() {
    this.columns = [{
        name: 'label.validity',
        prop: 'validity',
      },
      {
        name: 'label.currency',
        prop: 'currency',
      },
      {
        name: 'label.defaultMealPlan',
        prop: 'defaultMealPlan',
      },
      {
        name: 'label.applyToDays',
        cellTemplate: this.weekday,
      }
    ];
  }

  public setForm() {
    this.form = this.fb.group({
      range: [ null, Validators.required ],
      currencyId: [ null, Validators.required ]
    });

    this.form.valueChanges.subscribe( () => {
      this.searchButtonConfig.isDisabled = !this.form.valid;
    });
  }


  public goToView = (row) => {
    this.router.navigate(['../add-view', row.headerId], {relativeTo: this.route});
  }

  public fillForm() {
    const {
      currencyId,
      beginDate,
      endDate,
    } = this.route.snapshot.queryParams;

    if ( currencyId && beginDate && endDate ) {
      this.form.patchValue({
        currencyId,
        range: {
          beginDate,
          endDate,
        }
      });
    }
  }


  ngOnInit() {
    this.setForm();
    this.setButtonConfig();
    this.setColumns();

    this.route.params.subscribe(async params => {
      this.propertyId = +params.property;

      await this.getCurrencies();
      this.setDefaultCurrency();
      this.fillForm();
    });
  }

}
