import { Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { IMyDate } from 'mydaterangepicker';
import { BaseRateResource } from 'app/shared/resources/base-rate/base-rate.resource';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { Guests } from 'app/revenue-management/meal-plan/models/guests';
import { Location } from '@angular/common';
import { MealPlanResource } from 'app/revenue-management/meal-plan/resources/meal-plan-resource';
import { AssetsService } from 'app/core/services/assets.service';
import { Weekdays } from 'app/revenue-management/meal-plan/models/meal-plan';
import { MealPlanService } from 'app/revenue-management/meal-plan/services/meal-plan.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ModalEmitToggleService } from '@app/shared/services/shared/modal-emit-toggle.service';

@Component({
  selector: 'app-guest-house-new-add',
  templateUrl: './meal-plan-new-add.component.html',
  styleUrls: ['./meal-plan-new-add.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MealPlanNewAddComponent implements OnInit {

  public readonly GUEST = Guests;
  public readonly MEAL_PLAN_AMOUNT = 'MealPlanAmount';
  public readonly modalErrorId = 'ERROR_MODAL';
  public readonly modalConfirmId = 'CONFIRM_MODAL';

  public form: FormGroup;
  public title = 'title.configDefaultGuestHouse';
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;
  public modalButtonCancelConfig: ButtonConfig;
  public modalButtonConfirmConfig: ButtonConfig;
  public backButtonConfig: ButtonConfig;
  public disableToday: IMyDate;
  public propertyId: number;
  public currencyList: Array<any>;
  public mealPlanTypeList: Array<any>;
  public propertyParameterList: Array<any>;
  public optionsCurrencyMask: any;
  public columns: any[];
  public selectedWeekDays: Array<number>;
  public isReadonly = false;
  public currentMealPlan: any;
  public topButtonsListConfig: any;
  public selectedMealPlan: any;

  public modalButtonCloseConfig: ButtonConfig;
  public modalErrorMessage: string;

  @ViewChild('radio') radio: TemplateRef<any>;
  @ViewChild('adult') adult: TemplateRef<any>;
  @ViewChild('child1') child1: TemplateRef<any>;
  @ViewChild('child2') child2: TemplateRef<any>;
  @ViewChild('child3') child3: TemplateRef<any>;
  @ViewChild('header1') header1: TemplateRef<any>;
  @ViewChild('header2') header2: TemplateRef<any>;
  @ViewChild('header3') header3: TemplateRef<any>;


  constructor(
    private fb: FormBuilder,
    private sharedService: SharedService,
    private dateService: DateService,
    private route: ActivatedRoute,
    private baseRateResource: BaseRateResource,
    private translateService: TranslateService,
    private sessionParameterService: SessionParameterService,
    private _location: Location,
    private mealPlanResource: MealPlanResource,
    private assetsService: AssetsService,
    private router: Router,
    private mealPlanService: MealPlanService,
    private toasterEmitter: ToasterEmitService,
    private modalToggler: ModalEmitToggleService,
  ) { }

  public setForm() {
    this.form = this.fb.group({
      range: [ null, Validators.required ],
      currencyId: [ null, Validators.required ]
    });

    this.form.valueChanges.subscribe( () => {
      this.validate();
    });
  }


  public goToHistory = () => {
    const { range , currencyId } = this.form.getRawValue();
    let queryParams = {};

    if ( range && currencyId ) {
      const { beginDate, endDate } = range;

      queryParams = {
        currencyId,
        beginDate,
        endDate,
      };
    }

    this.navigateToList(queryParams);
  }

  public navigateToList(queryParams) {
    this.router.navigate(
      ['../list'],
      {
        relativeTo: this.route,
        queryParams
      });
  }

  public setTopButtonConfig(mealPlanId: string) {
    this.topButtonsListConfig = [];

    if (!mealPlanId) {
      this.topButtonsListConfig = [{
        title: 'label.historicMealPlan',
        callback: this.goToHistory,
        iconDefault: this.assetsService.getImgUrlTo('time_schedule.svg'),
        iconAlt: 'label.historicMealPlan'
      }];
    }
  }

  public validate() {
    const { valid } = this.form;
    const { selectedWeekDays } = this;

    this.confirmButtonConfig.isDisabled = !valid
      && ( selectedWeekDays && selectedWeekDays.length !== 0 );
  }

  public setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel-button',
      this.cancel,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'save-button',
      () => this.openConfirmModal(),
      'action.confirm',
      ButtonType.Primary,
    );
    this.backButtonConfig = this.sharedService.getButtonConfig(
      'back-button',
      this.back,
      'action.back',
      ButtonType.Primary,
    );

    this.modalButtonCloseConfig =  this.sharedService.getButtonConfig(
      'modal-cancel-button',
      () => this.closeErrorModal(),
      'action.close',
      ButtonType.Secondary
    );

    this.modalButtonCancelConfig =  this.sharedService.getButtonConfig(
      'modal-cancel-button',
      () => this.closeConfirmModal(),
      'action.no',
      ButtonType.Secondary
    );

    this.modalButtonConfirmConfig = this.sharedService.getButtonConfig(
      'modal-confirm-button',
      () => this.confirm(),
      'action.yes',
      ButtonType.Secondary
    );

    this.confirmButtonConfig.isDisabled = true;
  }

  public openConfirmModal() {
    this.modalToggler.emitOpenWithRef(this.modalConfirmId);
  }

  public closeConfirmModal() {
    this.modalToggler.emitCloseWithRef(this.modalConfirmId);
  }

  public openErrorModal() {
    this.modalToggler.emitOpenWithRef(this.modalErrorId);
  }

  public closeErrorModal() {
    this.modalToggler.emitCloseWithRef(this.modalErrorId);
  }

  public back = () => {
    this._location.back();
  }

  public cancel = () => {
    this.back();
  }

  public unsetAllWeekDay(obj: any) {
    const names = this.mealPlanService.getWeekNames();

    names.forEach( item => {
      obj[item] = false;
    });
  }

  public makePropertyMealPlanTypeRateHistoryList(
    objToSender,
    mealPlanTypeList,
    selectedMealPlan
  ) {

    if ( mealPlanTypeList ) {
      objToSender.propertyMealPlanTypeRateHistoryList = [];
      mealPlanTypeList.forEach( item => {
        if (
          item.adultMealPlanAmount != null || item.child1MealPlanAmount != null || item.child2MealPlanAmount != null ||
          item.child3MealPlanAmount != null
        ) {
          const {
            id,
            isSelected,
            mealPlanTypeName,
            propertyId,
            propertyMealPlanTypeCode,
            propertyMealPlanTypeName,
            ...propertyToSend
          } = item;

          const myItem = {
            ...propertyToSend,
            daysOfWeekDto: [0, 1, 2, 3, 4, 5, 6],
            mealPlanTypeDefault: selectedMealPlan.mealPlanTypeId === item.mealPlanTypeId
          };

          objToSender.propertyMealPlanTypeRateHistoryList.push(myItem);
        }
      });
    }
  }

  public validateTableList(objToSender): boolean {
    if (!objToSender.propertyMealPlanTypeRateHistoryList.length) {
      this.modalErrorMessage = this.translateService.instant('alert.fillOneMeal');
      this.openErrorModal();
      return false;
    }

    const main = objToSender.propertyMealPlanTypeRateHistoryList.find(x => x.mealPlanTypeDefault);
    if (!main || (
        main && main.adultMealPlanAmount == null && main.child1MealPlanAmount == null &&
        main.child2MealPlanAmount == null && main.child3MealPlanAmount == null
       )
    ) {
      this.modalErrorMessage = this.translateService.instant('alert.fillDefaultMeal');
      this.openErrorModal();
      return false;
    }

    return true;
  }

  public confirm = () => {
    this.closeConfirmModal();

    const {
      range: {
        beginDate, endDate
      },
      currencyId
    } = this.form.getRawValue();
    const { selectedMealPlan, mealPlanTypeList } = this;

    const objToSender = {
      initialDate: beginDate,
      finalDate: endDate,
      currencyId,
      propertyMealPlanTypeRateHistoryList: []
    };

    this.makePropertyMealPlanTypeRateHistoryList(
      objToSender,
      mealPlanTypeList,
      selectedMealPlan
    );

    if (!this.validateTableList(objToSender)) {
      return;
    }

    this
      .mealPlanResource
      .addMealPlanRate(objToSender)
      .subscribe( () => {
        const mealPlanMessage = this.translateService.instant('label.mealPlan');
        const message = this.translateService.instant(
          'variable.lbSaveSuccessF',
          { labelName: mealPlanMessage}
        );
        this.toasterEmitter.emitChange(SuccessError.success, message);
        this.navigateToList({});
    });
  }

  public weekDaysSelected(event) {
    this.selectedWeekDays = event;
    this.validate();
  }


  public valueChanged(value, guestField, row ) {
    row[guestField] = value;

    if ( isNaN(value) ) {
      delete row[guestField];
    }
  }

  public selectionChanged(event) {
    this.selectedMealPlan = event;
  }

  public setCurrencyList( currencyList ) {
    if ( currencyList ) {
      this.currencyList = currencyList.map( item => {
        const { id, currencyName, symbol } = item;
        return {
          key: id,
          value: `${currencyName} ${symbol}`
        };
      });
    }
  }

  public setDefaultCurrency() {
    const currencyId = this
      .sessionParameterService
      .getParameterValue(this.propertyId, ParameterTypeEnum.DefaultCurrency);

    if ( currencyId ) {
      this.form.patchValue({currencyId});
    }
  }

  public setMealPlanTypeList(mealPlanTypeList) {
    this.mealPlanTypeList = mealPlanTypeList;

    if ( this.mealPlanTypeList && this.mealPlanTypeList.length ) {
      this.mealPlanTypeList.map((item, index) => {
        if (item.mealPlanTypeId == 1) {
          this.mealPlanTypeList.splice(index, 1);
        }
      });

      this.mealPlanTypeList[0].isSelected = true;
      this.selectedMealPlan = this.mealPlanTypeList[0];
    }
  }

  public getBaseRateConfiguration(): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .baseRateResource
        .getConfigurationDataParameters(this.propertyId)
        .subscribe(items => {
          const { currencyList, mealPlanTypeList, propertyParameterList } = items;

          this.setCurrencyList( currencyList['items'] );
          this.setMealPlanTypeList( mealPlanTypeList['items'] );
          this.propertyParameterList = propertyParameterList;
          resolve(null);
        });
    });
  }

  public setInputMask() {
    // the input number mask
    this.optionsCurrencyMask = {
      prefix: '',
      thousands: '.',
      decimal: ',',
      align: 'center',
      allowZero: true,
      allowNegative: false
    };
  }

  public setColumns() {
    const { propertyParameterList } = this;

    this.columns = [
      {
        name: 'label.defaultMealPlan',
        cellTemplate: this.radio,
        sortable: false,
      },
      {
        name: 'label.mealType',
        prop: 'mealPlanTypeName',
        sortable: false,
      },
      {
        name: 'label.adult',
        cellTemplate: this.adult,
        sortable: false,
      }
    ];

    if ( propertyParameterList ) {
      propertyParameterList.forEach( (item, i) => {
        const index = i + 1;
        if ( item.isActive ) {
          this.columns.push({
            headerTemplate: this[`header${ index }`],
            prop: `child${ index }`,
            cellTemplate: this[`child${ index }`],
            sortable: false
          });

          this[`ageText${ index }`] = this
            .translateService
            .instant('label.ageByYears', {
              min: item.propertyParameterMinValue,
              max: item.propertyParameterValue
            });
        }
      });
    }
  }

  public getWeekListByNames(itemList: Array<any>): Array<number> {
    if ( itemList && itemList.length ) {
      const obj = itemList[0];
      const keysNames = this.mealPlanService.getWeekNames();

      return keysNames.filter( key => {
        return obj[key];
      }).map( key => {
        return Weekdays[key];
      });
    }

    return [];
  }

  public getMealPlanListById(mealPlanId: string): Promise<any> {
    return new Promise( resolve => {
      this
        .mealPlanResource
        .getMealPlanById(mealPlanId)
        .subscribe( item => {

          const {
            currency: { symbol, currencyName },
            initialDate,
            finalDate
          } = item;

          const weekList = this.getWeekListByNames(item.propertyMealPlanTypeRateHistoryList);

          const dateBegin = this.dateService.convertUniversalDateToViewFormat(initialDate);
          const endDate = this.dateService.convertUniversalDateToViewFormat(finalDate);

          const currentMealPlan = {
            currency: `${ currencyName } ${ symbol }`,
            validity: `${ dateBegin } - ${ endDate }`,
            range: weekList
          };


          this.currentMealPlan = currentMealPlan;
          this.setValuesOnEditMode( item.propertyMealPlanTypeRateHistoryList );
          resolve(null);
        });
    });
  }

  public setValuesOnEditMode(mealPlanListWithValues) {
    if ( mealPlanListWithValues ) {
      this.mealPlanTypeList = this.mealPlanTypeList.map( item => {
        const found = mealPlanListWithValues.find( mItem => item.mealPlanTypeId === mItem.mealPlanTypeId ) || {};

        return {
          ...item,
          ...found,
        };
      });
    }
  }

  ngOnInit() {
    const mealPlanId = this.route.snapshot.params['mealPlanId'];
    let systemDate;
    let dateObj;
    this.setInputMask();
    this.setTopButtonConfig(mealPlanId);

    this.route.params.subscribe(async params => {
      this.propertyId = +params.property;

      systemDate = this.dateService.getSystemDate(this.propertyId);

      dateObj = this
        .dateService
        .convertUniversalDateToIMyDate(systemDate);

      this.disableToday = dateObj ? dateObj.date : {};

      this.setForm();
      this.setButtonConfig();
      await this.getBaseRateConfiguration();

      this.setColumns();
      this.setDefaultCurrency();


      if ( mealPlanId ) {
        this.isReadonly = true;

        await this.getMealPlanListById( mealPlanId );
      }
    });
  }
}
