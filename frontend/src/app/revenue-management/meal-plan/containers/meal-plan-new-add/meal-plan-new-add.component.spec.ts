import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { MealPlanNewAddComponent } from './meal-plan-new-add.component';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import {
  CONFIGURATION_PROPERTY,
  DATA,
  DATE_PROPERTY,
  SELECTED_MEAL_PLAN_ID
} from 'app/revenue-management/mock-data';

describe('MealPlanNewAddComponent', () => {
  let component: MealPlanNewAddComponent;
  let fixture: ComponentFixture<MealPlanNewAddComponent>;
  let data;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ MealPlanNewAddComponent ],
      imports: [
        TranslateModule.forRoot(),
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        SharedModule,
        RouterTestingModule,
      ]
    });

    fixture = TestBed.createComponent(MealPlanNewAddComponent);
    component = fixture.componentInstance;

    spyOn(component, 'getBaseRateConfiguration').and.callThrough();
    spyOn(component, 'setColumns').and.callThrough();
    spyOn(component, 'setDefaultCurrency').and.callThrough();
    spyOn(component['dateService'], 'getTodayIMydate').and.returnValue(DATA[DATE_PROPERTY]);
    spyOn<any>(component['baseRateResource'], 'getConfigurationDataParameters')
      .and.returnValue(of( DATA[CONFIGURATION_PROPERTY]));
    spyOn(component, 'setMealPlanTypeList').and.callThrough();
    spyOn(component, 'setCurrencyList').and.callThrough();
    spyOn(component, 'getMealPlanListById').and.callThrough();
    spyOn(component, 'setValuesOnEditMode').and.callThrough();
    spyOn<any>(component['mealPlanResource'], 'getMealPlanById').and.returnValue(of(DATA[SELECTED_MEAL_PLAN_ID]));
    spyOn(component['sessionParameterService'], 'getParameterValue')
      .and.returnValue(DATA[CONFIGURATION_PROPERTY].currencyList.items[1].id);

    data = {...DATA[CONFIGURATION_PROPERTY]};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngInit', fakeAsync(() => {
    spyOn(component, 'setButtonConfig');
    spyOn(component, 'setForm');

    component['route'].snapshot.params = { mealPlanId: 1 };

    component.ngOnInit();


    tick();

    expect(component.setButtonConfig).toHaveBeenCalled();
    expect(component.setForm).toHaveBeenCalled();
    expect(component.setButtonConfig).toHaveBeenCalled();
    expect(component.getBaseRateConfiguration).toHaveBeenCalled();
    expect(component.setDefaultCurrency).toHaveBeenCalled();
    expect(component.getMealPlanListById).toHaveBeenCalledWith( 1 );
    expect(component.isReadonly).toEqual(true);
  }));

  it('should test getBaseRateConfiguration', fakeAsync(() => {
    component.getBaseRateConfiguration();

    tick();

    expect(component.setMealPlanTypeList).toHaveBeenCalledWith(data.mealPlanTypeList.items);
    expect(component.setCurrencyList).toHaveBeenCalledWith(data.currencyList.items);
    expect(component.propertyParameterList).toEqual(data.propertyParameterList);
  }));

  it('should test setDefaultCurrency', fakeAsync(() => {
    component.setForm();
    component.setDefaultCurrency();

    tick();

    expect(component['form'].get('currencyId').value).toEqual(DATA[CONFIGURATION_PROPERTY].currencyList.items[1].id);
  }));

  it('should test setMealPlanTypeList', () => {
    const mealPlanList = data.mealPlanTypeList.items;


    component.setMealPlanTypeList(mealPlanList);

    expect(component.mealPlanTypeList).toEqual(mealPlanList);
    expect(component.mealPlanTypeList[0]['isSelected']).toEqual(true);
    expect(component.selectedMealPlan).toEqual(component.mealPlanTypeList[0]);
  });

  it('should test setCurrencyList', () => {
    const currencyList = data.currencyList.items;
    const { currencyName, symbol } = currencyList[1];

    component.setCurrencyList(currencyList);

    expect(component.currencyList[1].value).toEqual(`${currencyName} ${symbol}`);
  });

  it('should test setColumns', () => {
    component.propertyParameterList = data.propertyParameterList;

    component.setColumns();

    expect(component.columns[4]['prop']).toEqual('child2');
    expect(component.columns[5]).toEqual(undefined);
  });


  it('should test getMealPlanListById', fakeAsync(() => {
    const item = DATA[SELECTED_MEAL_PLAN_ID];

    component.getMealPlanListById(item.id);

    tick();

    expect(component['mealPlanResource'].getMealPlanById).toHaveBeenCalledWith(item.id);
    expect(component.setValuesOnEditMode).toHaveBeenCalledWith(item.propertyMealPlanTypeRateHistoryList);
  }));


  it('should test setValuesOnEditMode', () => {
    const item = DATA[SELECTED_MEAL_PLAN_ID];

    component.setValuesOnEditMode(item.propertyMealPlanTypeRateHistoryList);

    expect(JSON.stringify(component.mealPlanTypeList[3])).toContain('adult');
    expect(component.mealPlanTypeList[3].child2MealPlanAmount)
      .toEqual(item.propertyMealPlanTypeRateHistoryList[3].child2MealPlanAmount);
  });

  it('should test getWeekListByNames', () => {
    const item = DATA[SELECTED_MEAL_PLAN_ID];

    const weekList = component.getWeekListByNames(item.propertyMealPlanTypeRateHistoryList);


    expect(weekList).toEqual([0, 1, 2, 3, 4]);
  });

  it('should test valueChanged', () => {
    let value: string | number = 'pppp';

    const row = { };

    component.valueChanged(value, 'adult', row );

    expect(row['adult']).toEqual(undefined);


    value = 1.11;

    component.valueChanged(value, 'adult', row );

    expect(row['adult']).toEqual(1.11);
  });
});
