import { MealPlanModule } from './meal-plan.module';

describe('MealPlanModule', () => {
  let guestHouseModule: MealPlanModule;

  beforeEach(() => {
    guestHouseModule = new MealPlanModule();
  });

  it('should create an instance', () => {
    expect(guestHouseModule).toBeTruthy();
  });
});
