import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MealPlanNewAddComponent } from './containers/meal-plan-new-add/meal-plan-new-add.component';
import { MealPlanListComponent } from './containers/meal-plan-list/meal-plan-list.component';


export const propertyBaseRoutes: Routes = [
  { path: 'list', component: MealPlanListComponent},
  { path: 'add-view', component: MealPlanNewAddComponent},
  { path: 'add-view/:mealPlanId', component: MealPlanNewAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(propertyBaseRoutes)],
  exports: [RouterModule],
})
export class MealPlanRoutingModule {}
