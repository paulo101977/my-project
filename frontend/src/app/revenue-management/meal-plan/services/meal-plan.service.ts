import { Injectable } from '@angular/core';
import { Weekdays } from 'app/revenue-management/meal-plan/models/meal-plan';

@Injectable({
  providedIn: 'root'
})
export class MealPlanService {

  constructor() { }

  public getWeekKeys() {
    return Object.keys(Weekdays).filter( item => !isNaN(Number(item)));
  }

  public getWeekNames() {
    return Object.keys(Weekdays).filter( item => isNaN(Number(item)));
  }
}
