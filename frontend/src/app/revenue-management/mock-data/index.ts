
// the base config property
export const DATE_PROPERTY = 'DATE';

// the array of items
export const PROPERTY_PREMISE_LIST_PROPERTY = 'PROPERTY_PREMISE_LIST';

// the children array list property
export const CHILDREN_LIST_PROPERTY = 'CHILDREN_LIST';

// the base config property
export const CONFIGURATION_PROPERTY = 'CONFIGURATION';

// obj premise property by id
export const PREMISE_BY_ID_PROPERTY = 'PREMISE_BY_ID';

// obj premise property by id
export const RESULT_FILL_TABLE_PROPERTY = 'RESULT_FILL_TABLE';


export const CHILDREN_SESSION_LIST_PROPERTY = 'CHILDREN_SESSION_LIST';

export const FORM_PREMISE_INSERT_EDIT_PROPERTY = 'FORM_PREMISE_INSERT_EDIT';


export const TEXT_MESSAGE_SUCCESS_PROPERTY = 'TEXT_MESSAGE_SUCCESS';

export const FILL_TABLE_DATA_PROPERTY = 'FILL_TABLE_DATA';

export const FILL_TABLE_ITEM_CHILD_PROPERTY = 'FILL_TABLE_ITEM_CHILD';

export const FORM_DATA_PROPERTY = 'FORM_DATA';

export const DEFAULT_MEAL_PLAN_LIST = 'DEFAULT_MEAL_PLAN_LIST';

export const SELECTED_MEAL_PLAN_ID = 'SELECTED_MEAL_PLAN_ID';

export const DEFAUL_MEAL_PLAN_LIST_RESULT = 'DEFAUL_MEAL_PLAN_LIST_RESULT';

export class DATA {
  public static readonly DATE = { day: 29,
    month: 1,
    year: 2019,
  };
  public static readonly PROPERTY_PREMISE_LIST = [
    {
      id: 1,
      propertyId: 1,
      roomTypeId: 1,
      roomTypeName: 'LXO',
      abbreviation: 'LXO',
      roomTypeListMinMaxRate: '100 - 200 max',
      minimumRate: 100,
      maximumRate: 200,
      pax1: 100.0,
      pax2: 55.5,
      pax3: 30.0,
      pax4: 120.0,
      pax5: 130.0,
      child1: 0,
      child2: 0,
      child3: 400.0,
      paxAdditional: 120,
      pax1Operator: '+',
      pax2Operator: '-',
      pax3Operator: '/',
      pax4Operator: '*',
      pax5Operator: '+',
      paxAdditionalOperator: '*',
      child1Operator: '+',
      child2Operator: '-',
      child3Operator: '*',
      hasPax1: true,
      hasPax2: true,
      hasPax3: true,
      hasPax4: true,
      hasPax5: true,
      hasPaxAdditional: true,
      hasChild1: true,
      hasChild2: true,
      hasChild3: true,
    },
    {
      id: 2,
      propertyId: 1,
      roomTypeId: 2,
      roomTypeName: 'STD',
      abbreviation: 'STD',
      roomTypeListMinMaxRate: '300 - 400 max',
      minimumRate: 300,
      maximumRate: 400,
      pax1: 310.0,
      pax2: 400.5,
      pax3: 30.0,
      child1: 45,
      child2: 420,
      child3: 5,
      paxAdditional: 100,
      pax1Operator: '+',
      pax2Operator: '-',
      pax3Operator: '/',
      child1Operator: '+',
      child2Operator: '-',
      child3Operator: '*',
      hasPax1: true,
      hasPax2: true,
      hasPax3: true,
      hasChild1: true,
      hasChild2: true
    }
  ];
  public static readonly CHILDREN_LIST = [
    {
      isActive: true,
      propertyParameterMinValue: 0,
      propertyParameterValue: 4
    },
    {
      isActive: true,
      propertyParameterMinValue: 5,
      propertyParameterValue: 6
    },
    {
      isActive: false,
      propertyParameterMinValue: 7,
      propertyParameterValue: 10
    }
  ];
  public static readonly CONFIGURATION = {
    currencyList: {
      items: [
        {
          id: 'c5ef2cfa-a05e-44ed-ad2e-089b26471432',
          countrySubdivision: '',
          currencyName: 'Dólar',
          alphabeticCode: 'USD',
          numnericCode: '2',
          twoLetterIsoCode: '',
          symbol: 'USD',
          minorUnit: 2,
          exchangeRate: 1,
          countrySubdivisionId: 0,
          billingAccountItemList: [],
          currencyExchangeList: [],
          propertyBaseRateList: [],
          propertyCurrencyExchangeList: [],
          ratePlanList: [],
          reservationBudgetList: [],
        },
        {
          id: '67838acb-9525-4aeb-b0a6-127c1b986c48',
          countrySubdivision: '',
          currencyName: 'Real',
          alphabeticCode: 'BRL',
          numnericCode: '2',
          symbol: 'R$',
          twoLetterIsoCode: '',
          countrySubdivisionId: 0,
          exchangeRate: 3.82,
          minorUnit: 2,
          billingAccountItemList: [],
          currencyExchangeList: [],
          propertyBaseRateList: [],
          propertyCurrencyExchangeList: [],
          ratePlanList: [],
          reservationBudgetList: [],
        },
        {
          id: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
          countrySubdivision: '',
          currencyName: 'Euro',
          alphabeticCode: 'EUR',
          numnericCode: '2',
          symbol: '€',
          twoLetterIsoCode: '',
          minorUnit: 2,
          exchangeRate: 0.88,
          countrySubdivisionId: 0,
          billingAccountItemList: [],
          currencyExchangeList: [],
          propertyBaseRateList: [],
          propertyCurrencyExchangeList: [],
          ratePlanList: [],
          reservationBudgetList: [],
        },
        {
          id: '100326fd-450d-48af-a756-f28aaf7d87da',
          countrySubdivision: '',
          currencyName: 'Peso Argentino',
          alphabeticCode: 'ARS',
          numnericCode: '2',
          symbol: '$',
          twoLetterIsoCode: '',
          minorUnit: 2,
          countrySubdivisionId: 0,
          exchangeRate: 37.12,
          billingAccountItemList: [],
          currencyExchangeList: [],
          propertyBaseRateList: [],
          propertyCurrencyExchangeList: [],
          ratePlanList: [],
          reservationBudgetList: [],
        }
      ]
    },
    mealPlanTypeList: {
      items: [
        {
          mealPlanTypeName: 'Meia Pensão Almoço',
          id: 'e9ad9cc8-3cdd-44b6-9257-206459dc8085',
          propertyMealPlanTypeCode: 'MAPA',
          propertyMealPlanTypeName: 'Meia Pensão Almoço',
          propertyId: 1,
          mealPlanTypeId: 4,

        },
        {
          mealPlanTypeName: 'Pensão Completa',
          id: 'ad43eba7-d883-4637-928a-20779887a308',
          propertyMealPlanTypeCode: 'FAP',
          propertyMealPlanTypeName: 'Pensão Completa',
          propertyId: 1,
          mealPlanTypeId: 6,

        },
        {
          mealPlanTypeName: 'Nenhuma',
          id: 'a580d115-31e0-4e36-97ba-2927abce17bd',
          propertyMealPlanTypeCode: 'SAP',
          propertyMealPlanTypeName: 'Sem Pensão',
          propertyId: 1,
          mealPlanTypeId: 1,

        },
        {
          mealPlanTypeName: 'Café da Manhã',
          id: 'a2d7e6b9-1ee5-4f7b-b28d-71ff4af771e9',
          propertyMealPlanTypeCode: 'CM',
          propertyMealPlanTypeName: 'Café da Manha',
          propertyId: 1,
          mealPlanTypeId: 2,

        },
        {
          mealPlanTypeName: 'Meia Pensão',
          id: 'f9112356-d56e-4197-bb2f-abe294a2d7bd',
          propertyMealPlanTypeCode: 'MAP',
          propertyMealPlanTypeName: 'Meia Pensão',
          propertyId: 1,
          mealPlanTypeId: 3,

        },
        {
          mealPlanTypeName: 'Tudo Incluso',
          id: '23fc77cd-058c-4836-b382-b226889a8459',
          propertyMealPlanTypeCode: 'TI',
          propertyMealPlanTypeName: 'Tudo Incluso',
          propertyId: 1,
          mealPlanTypeId: 7,

        },
        {
          mealPlanTypeName: 'Meia Pensão Jantar',
          id: 'd9e47425-3c0e-4ec5-a20f-c4eaad8e1fd8',
          propertyMealPlanTypeCode: 'MAPJ',
          propertyMealPlanTypeName: 'Meia Pensão Jantar',
          propertyId: 1,
          mealPlanTypeId: 5,

        }
      ]
    },
    roomTypeList: {
      items: [
        {
          id: 2,
          propertyId: 1,
          order: 20,
          name: 'Luxo',
          abbreviation: 'LXO',
          adultCapacity: 5,
          childCapacity: 2,
          freeChildQuantity1: 0,
          freeChildQuantity2: 0,
          freeChildQuantity3: 0,
          isActive: true,
          maximumRate: 500,
          minimumRate: 100,
          distributionCode: 'CD',
          roomTypeBedTypeList: [],
        },
        {
          id: 3,
          propertyId: 1,
          order: 3,
          name: 'Presidential',
          abbreviation: 'PSD',
          adultCapacity: 2,
          childCapacity: 2,
          freeChildQuantity1: 0,
          freeChildQuantity2: 0,
          freeChildQuantity3: 0,
          isActive: true,
          maximumRate: 500,
          minimumRate: 100,
          distributionCode: 'CD',
          roomTypeBedTypeList: [],
        },
        {
          id: 1,
          propertyId: 1,
          order: 1,
          name: 'Standard',
          abbreviation: 'STD',
          adultCapacity: 2,
          childCapacity: 1,
          freeChildQuantity1: 0,
          freeChildQuantity2: 0,
          freeChildQuantity3: 0,
          isActive: true,
          maximumRate: 500,
          minimumRate: 100,
          distributionCode: 'CD',
          roomTypeBedTypeList: [],
        },
        {
          id: 11,
          propertyId: 1,
          order: 4,
          name: 'Suíte Automatizada',
          abbreviation: 'AlT',
          adultCapacity: 3,
          childCapacity: 3,
          freeChildQuantity1: 2,
          freeChildQuantity2: 0,
          freeChildQuantity3: 0,
          isActive: true,
          maximumRate: 450,
          minimumRate: 120,
          distributionCode: 'Aut0001',
          roomTypeBedTypeList: [],
        },
        {
          id: 6,
          propertyId: 1,
          order: 4,
          name: 'Suíte Automatizada aaaaaaa aaaaaaaaaaa',
          abbreviation: 'AUT',
          adultCapacity: 3,
          childCapacity: 3,
          freeChildQuantity1: 2,
          freeChildQuantity2: 0,
          freeChildQuantity3: 0,
          isActive: true,
          maximumRate: 450,
          minimumRate: 120,
          distributionCode: 'Aut0001',
          roomTypeBedTypeList: [],

        },
        {
          id: 12,
          propertyId: 1,
          order: 10,
          name: 'Teste Rafael',
          abbreviation: 'TTeste',
          adultCapacity: 1,
          childCapacity: 1,
          freeChildQuantity1: 1,
          freeChildQuantity2: 1,
          freeChildQuantity3: 1,
          isActive: true,
          maximumRate: 150,
          minimumRate: 100,
          distributionCode: 'teste',
          roomTypeBedTypeList: [],
        },
        {
          id: 31,
          propertyId: 1,
          order: 10,
          name: 'Teste Tarifa',
          abbreviation: 'TT',
          adultCapacity: 1,
          childCapacity: 0,
          freeChildQuantity1: 0,
          freeChildQuantity2: 0,
          freeChildQuantity3: 0,
          isActive: true,
          maximumRate: 0,
          minimumRate: 0,
          distributionCode: '',
          roomTypeBedTypeList: [],
        }
      ]
    },
    propertyParameterList: [
      {
        parameterTypeName: 'Number',
        parameterDescription: 'Criança 1',
        featureGroupId: 3,
        id: 'd4237c24-d20c-4adb-acda-a354fba5722a',
        propertyId: 1,
        applicationParameterId: 7,
        propertyParameterValue: '2',
        propertyParameterMinValue: '0',
        propertyParameterMaxValue: '6',
        isActive: true,
      },
      {
        parameterTypeName: 'Number',
        parameterDescription: 'Criança 2',
        featureGroupId: 3,
        id: 'ad6c8266-8766-4f19-bbf3-7f078af8ca81',
        propertyId: 1,
        applicationParameterId: 8,
        propertyParameterValue: '7',
        propertyParameterMinValue: '3',
        propertyParameterMaxValue: '17',
        isActive: true,
      },
      {
        parameterTypeName: 'Number',
        parameterDescription: 'Criança 3',
        featureGroupId: 3,
        id: '77549693-60b5-423c-9738-829719c860c6',
        propertyId: 1,
        applicationParameterId: 9,
        propertyParameterValue: '0',
        propertyParameterMinValue: '0',
        propertyParameterMaxValue: '0',
        isActive: false,

      }
    ],
  };
  public static readonly PREMISE_BY_ID = {
    id: 'bbe77a49-826b-41b6-b887-d480e35a35ce',
    propertyPremiseId: '00000000-0000-0000-0000-000000000000',
    propertyPremiseHeaderId: 'bbe77a49-826b-41b6-b887-d480e35a35ce',
    propertyId: 1,
    initialDate: '2019-01-18T19:04:56',
    finalDate: '2019-01-18T19:04:56',
    currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    isActive: true,
    isDeleted: false,
    creationTime: '2019-01-18T19:04:56',
    creatorUserId: 'ae45fef3-81f5-4d15-ed6e-08d648aa4156',
    lastModificationTime: '2019-01-31T13:40:43.96',
    lastModifierUserId: 'abee3fae-ff86-4eb3-a333-3e757dd01f53',
    deletionTime: '2019-01-31T12:13:44.9',
    deleterUserId: 'abee3fae-ff86-4eb3-a333-3e757dd01f53',
    name: 'Tester',
    paxReference: 1,
    roomTypeReferenceId: 1,
    roomTypeReferenceName: 'STD',
    roomTypeId: 1,
    currencyName: 'Real',
    currencyAlphabeticCode: 'BRL',
    currencySymbol: 'R$',
    propertyPremiseList: [
      {
        id: '00000000-0000-0000-0000-000000000000',
        propertyPremiseId: '00000000-0000-0000-0000-000000000000',
        propertyId: 0,
        roomTypeId: 2,
        roomTypeName: 'LXO',
        pax1: 1,
        pax2: 1,
        pax3: 1,
        pax4: 1,
        pax5: 1,
        pax1Operator: '+',
        pax2Operator: '+',
        pax3Operator: '+',
        pax4Operator: '+',
        pax5Operator: '+',
        paxAdditional: 1,
        paxAdditionalOperator: '1',
        child1: 1,
        child2: 1,
        child3: 1,
        child1Operator: '+',
        child2Operator: '+',
        child3Operator: '+',
        tenantId: '00000000-0000-0000-0000-000000000000',
        propertyPremiseHeaderId: '00000000-0000-0000-0000-000000000000',
        isDeleted: false,
        creationTime: '0001-01-01T00:00:00',
        creatorUserId: '00000000-0000-0000-0000-000000000000'
      },
      {
        id: '00000000-0000-0000-0000-000000000000',
        propertyPremiseId: '00000000-0000-0000-0000-000000000000',
        propertyId: 0,
        roomTypeId: 1,
        roomTypeName: 'STD',
        pax1: 1,
        pax2: 1,
        pax3: 1,
        pax4: 1,
        pax5: 1,
        pax1Operator: '+',
        pax2Operator: '-',
        pax3Operator: '+',
        pax4Operator: '+',
        pax5Operator: '+',
        paxAdditional: 1,
        paxAdditionalOperator: '1',
        child1: 1,
        child2: 1,
        child3: 1,
        child1Operator: '+',
        child2Operator: '+',
        child3Operator: '+',
        tenantId: '00000000-0000-0000-0000-000000000000',
        propertyPremiseHeaderId: '00000000-0000-0000-0000-000000000000',
        isDeleted: false,
        creationTime: '0001-01-01T00:00:00',
        creatorUserId: '00000000-0000-0000-0000-000000000000'
      }
    ]
  };
  public static readonly RESULT_FILL_TABLE = [
    {
      abbreviation: 'STD',
      hasPax1: true,
      hasPax2: true,
      maximumRate: 500,
      minimumRate: 100,
      pax1Operator: '+',
      pax2Operator: '+',
      roomTypeId: 1,
      roomTypeListMinMaxRate: 'label.roomTypeListMinMaxRate',
      tenantId: '123',
    },
    {
      abbreviation: 'TTeste',
      hasPax1: true,
      maximumRate: 150,
      minimumRate: 100,
      pax1Operator: '+',
      roomTypeId: 12,
      roomTypeListMinMaxRate: 'label.roomTypeListMinMaxRate',
      tenantId: '123',
    }
  ];
  public static readonly CHILDREN_SESSION_LIST = [
    {
    applicationParameterId: 7,
    canInactive: false,
    featureGroupId: 3,
    id: 'd4237c24-d20c-4adb-acda-a354fba5722a',
    isActive: true,
    parameterDescription: 'Criança 1',
    parameterTypeName: 'Number',
    propertyId: 1,
    propertyParameterMaxValue: '6',
    propertyParameterMinValue: '0',
    propertyParameterValue: '2'
  },
    {
    applicationParameterId: 8,
    canInactive: true,
    featureGroupId: 3,
    id: 'ad6c8266-8766-4f19-bbf3-7f078af8ca81',
    isActive: true,
    parameterDescription: 'Criança 2',
    parameterTypeName: 'Number',
    propertyId: 1,
    propertyParameterMaxValue: 9,
    propertyParameterMinValue: 3,
    propertyParameterValue: 7,
  },
    {
    applicationParameterId: 9,
    canInactive: true,
    featureGroupId: 3,
    id: '77549693-60b5-423c-9738-829719c860c6',
    isActive: true,
    parameterDescription: 'Criança 3',
    parameterTypeName: 'Number',
    propertyId: 1,
    propertyParameterMaxValue: 17,
    propertyParameterMinValue: 8,
    propertyParameterValue: 10,
  }];
  public static readonly FORM_PREMISE_INSERT_EDIT = {
    currencyId: 'c5ef2cfa-a05e-44ed-ad2e-089b26471432',
    name: 'Teste',
    paxReference: '1',
    period: {
      beginDate: '2019-01-30T00:00:00',
      endDate: '2019-02-02T00:00:00',
    },
    roomTypeReferenceId: '3'
  };
  public static readonly TEXT_MESSAGE_SUCCESS = [
    'text.premiseSavedSuccess',
    'text.premiseEditedSuccess'
  ];
  public static readonly FILL_TABLE_DATA = [
    {
      abbreviation: 'LXO',
      child1: 150,
      child1Operator: '+',
      child2: 400.05,
      child2Operator: '-',
      hasPax1: true,
      hasPax2: true,
      hasPax3: true,
      hasPax4: true,
      hasPax5: true,
      id: 'string',
      maximumRate: 500,
      minimumRate: 100,
      pax1: 120,
      pax1Operator: '+',
      pax2: 120,
      pax2Operator: '-',
      pax3: 230.34,
      pax3Operator: '+',
      pax4: 10,
      pax4Operator: '*',
      pax5: 230,
      pax5Operator: '/',
      propertyId: 1,
      roomTypeId: 2,
      roomTypeListMinMaxRate: 'label.roomTypeListMinMaxRate',
      roomTypeName: 'string',
      tenantId: '123',
    },
    {
      abbreviation: 'PSD',
      child1: 150,
      child1Operator: '+',
      child2: 400.05,
      child2Operator: '-',
      hasPax1: true,
      hasPax2: true,
      id: 'string',
      maximumRate: 500,
      minimumRate: 100,
      pax1: 120,
      pax1Operator: '+',
      pax2: 120,
      pax2Operator: '-',
      propertyId: 1,
      roomTypeId: 3,
      roomTypeListMinMaxRate: 'label.roomTypeListMinMaxRate',
      roomTypeName: 'PSD',
      tenantId: '123',
    }
  ];
  public static readonly FILL_TABLE_ITEM_CHILD = [
    {
      abbreviation: 'LXO',
      child1: 1,
      child1Operator: '+',
      child2: 1,
      child2Operator: '+',
      child3: 1,
      child3Operator: '+',
      creationTime: '0001-01-01T00:00:00',
      creatorUserId: '00000000-0000-0000-0000-000000000000',
      hasPax1: true,
      hasPax2: true,
      hasPax3: true,
      hasPax4: true,
      hasPax5: true,
      id: '00000000-0000-0000-0000-000000000000',
      isDeleted: false,
      maximumRate: 500,
      minimumRate: 100,
      pax1: 1,
      pax1Operator: '+',
      pax2: 1,
      pax2Operator: '+',
      pax3: 1,
      pax3Operator: '+',
      pax4: 1,
      pax4Operator: '+',
      pax5: 1,
      pax5Operator: '+',
      paxAdditional: 1,
      paxAdditionalOperator: '1',
      propertyId: 0,
      propertyPremiseHeaderId: '00000000-0000-0000-0000-000000000000',
      propertyPremiseId: '00000000-0000-0000-0000-000000000000',
      roomTypeId: 2,
      roomTypeListMinMaxRate: 'label.roomTypeListMinMaxRate',
      roomTypeName: 'LXO',
      tenantId: '00000000-0000-0000-0000-000000000000'
    },
    {
      abbreviation: 'PSD',
      hasPax1: true,
      hasPax2: true,
      maximumRate: 500,
      minimumRate: 100,
      pax1Operator: '+',
      pax2Operator: '+',
      roomTypeId: 3,
      roomTypeListMinMaxRate: 'label.roomTypeListMinMaxRate',
      tenantId: '123'
    }
  ];
  public static readonly FORM_DATA = {
    currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
    name: 'Tester',
    paxReference: 1,
    period: {
      beginDate: '2019-01-18T19:04:56',
      endDate: '2019-01-18T19:04:56',
    },
    roomTypeReferenceId: 1,
  };
  public static readonly DEFAULT_MEAL_PLAN_LIST = [
    {
      propertyMealPlanTypeRateHeaderId: '2',
      propertyMealPlanTypeRateHeader: {
        initialDate: '2019-01-18T19:04:56',
        finalDate: '2019-01-18T19:04:56',
        currency: {
          symbol: 'R$',
          currencyName: 'Real'
        }
      },
      mealPlanType: {
        mealPlanTypeName: 'My Name 1',
      },
      sunday: true,
      monday: false,
      tuesday: true,
      wednesday: false,
      thursday: true,
      friday: true,
      saturday: false,
      mealPlanTypeDefault: true
    },
    {
      propertyMealPlanTypeRateHeaderId: '2',
      propertyMealPlanTypeRateHeader: {
        initialDate: '2019-01-17T19:04:56',
        finalDate: '2019-02-18T19:04:56',
        currency: {
          symbol: '$',
          currencyName: 'Dolar'
        }
      },
      mealPlanType: {
        mealPlanTypeName: 'My Name 2',
      },
      sunday: true,
      monday: true,
      tuesday: false,
      wednesday: false,
      thursday: false,
      friday: false,
      saturday: false,
      mealPlanTypeDefault: false
    }
  ];
  public static readonly DEFAUL_MEAL_PLAN_LIST_RESULT = [{
    currency: 'Real R$',
    headerId: '2',
    validity: 'label.historicMealPlanDateToDate',
    weekList: [0, 2, 4, 5],
    defaultMealPlan: 'My Name 1'
  }, {
    currency: 'Dolar $',
    headerId: '2',
    validity: 'label.historicMealPlanDateToDate',
    weekList: [0, 1],
    defaultMealPlan: 'My Name 2'
  }];
  // TODO: change when backend
  public static readonly SELECTED_MEAL_PLAN_ID = {
    currencyId: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
    finalDate: '2019-02-28T00:00:00',
    id: '9c9b137b-5c12-41fd-b460-283b6a60a1ff',
    initialDate: '2019-02-27T00:00:00',
    propertyId: 1,
    currency: {
      currencyName: 'Euro',
      id: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
      symbol: '€',
    },
    propertyMealPlanTypeRateHistoryList: [
      {
        adultMealPlanAmount: 11.11,
        child1MealPlanAmount: 11.11,
        child2MealPlanAmount: 11.11,
        mealPlanTypeId: 3,
        friday: false,
        saturday: false,
        sunday: true,
        thursday: true,
        tuesday: true,
        wednesday: true,
        monday: true,
        mealPlanTypeDefault: false,
      },
      {
        adultMealPlanAmount: 11.11,
        child1MealPlanAmount: 11.11,
        child2MealPlanAmount: 11.11,
        mealPlanTypeId: 2,
        friday: false,
        saturday: false,
        sunday: true,
        thursday: true,
        tuesday: true,
        wednesday: true,
        monday: true,
        mealPlanTypeDefault: false,
      },
      {
        adultMealPlanAmount: 11.11,
        child1MealPlanAmount: 11.11,
        child2MealPlanAmount: 11.11,
        mealPlanTypeId: 3,
        friday: false,
        saturday: false,
        sunday: true,
        thursday: true,
        tuesday: true,
        wednesday: true,
        monday: true,
        mealPlanTypeDefault: false,
      },
      {
        adultMealPlanAmount: 11.11,
        child1MealPlanAmount: 11.11,
        child2MealPlanAmount: 11.11,
        mealPlanTypeId: 3,
        friday: false,
        saturday: false,
        sunday: true,
        thursday: true,
        tuesday: true,
        wednesday: true,
        monday: true,
        mealPlanTypeDefault: false,
      },
      {
        adultMealPlanAmount: 11.11,
        child1MealPlanAmount: 11.11,
        child2MealPlanAmount: 11.11,
        mealPlanTypeId: 3,
        friday: false,
        saturday: false,
        sunday: true,
        thursday: true,
        tuesday: true,
        wednesday: true,
        monday: true,
        mealPlanTypeDefault: false,
      },
      {
        adultMealPlanAmount: 11.11,
        child1MealPlanAmount: 11.11,
        child2MealPlanAmount: 11.11,
        mealPlanTypeId: 3,
        friday: false,
        saturday: false,
        sunday: true,
        thursday: true,
        tuesday: true,
        wednesday: true,
        monday: true,
        mealPlanTypeDefault: false,
      },
      {
        adultMealPlanAmount: 11.11,
        child1MealPlanAmount: 11.11,
        child2MealPlanAmount: 11.11,
        mealPlanTypeId: 3,
        friday: false,
        saturday: false,
        sunday: true,
        thursday: true,
        tuesday: true,
        wednesday: true,
        monday: true,
        mealPlanTypeDefault: false,
      }
    ]
  };
}
