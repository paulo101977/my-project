import {BaseRateConfigurationMealPlanType} from 'app/shared/models/revenue-management/meal-type-fix-rate';
import {BaseRateTable} from 'app/shared/models/revenue-management/property-base-rate';
import { LevelRateItem } from './level-rate-item';

export class LevelRate {
  levelRateHeader: LevelRateItem;
  levelRateList: Array<BaseRateTable>;
  levelRateMealPlanTypeList: Array<BaseRateConfigurationMealPlanType>;
  hasMealPlanTypes: boolean;
}
