import {LevelRateCurrency} from './level-rate-currency';


export class BaseLevelRate {
  levelId: string;
  levelName: string;
  levelRateCurrencyList: Array<LevelRateCurrency>;
}
