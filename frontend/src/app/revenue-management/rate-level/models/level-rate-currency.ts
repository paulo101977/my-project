import {LevelRate} from './level-rate';

export class LevelRateCurrency {
  currencyId: string;
  currencyName: string;
  levelRateList: Array<LevelRate>;
  dateListOutsideRateList: Array<string>;
}
