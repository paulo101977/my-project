export class LevelRateItem {
  id: string;
  levelId: string;
  isActive: boolean;
  rateName: string;
  currencyId: string;
  initialDate: string;
  endDate: string;
  levelName: string;
  currencyName: string;
  currencySymbol: string;
  vigenceDate: string;
  currency: string;
  propertyMealPlanTypeRate: boolean;
}
