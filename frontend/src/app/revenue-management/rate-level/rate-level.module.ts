import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  RateLevelModalDeleteComponent
} from 'app/revenue-management/rate-level/components/rate-level-modal-delete/rate-level-modal-delete.component';
import { RateLevelRoutingModule } from './rate-level-routing.module';
import { RateLevelComponent } from './components/rate-level/rate-level.component';
import { RateLevelNewEditComponent } from './components/rate-level-new-edit/rate-level-new-edit.component';
import {SharedModule} from 'app/shared/shared.module';
import {BaseRateModule} from 'app/revenue-management/base-rate/base-rate.module';

@NgModule({
  imports: [
    CommonModule,
    RateLevelRoutingModule,
    SharedModule,
    BaseRateModule
  ],
  declarations: [
    RateLevelComponent,
    RateLevelNewEditComponent,
    RateLevelModalDeleteComponent
  ]
})
export class RateLevelModule { }
