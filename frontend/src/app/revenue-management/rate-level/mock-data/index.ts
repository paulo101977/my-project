import {LevelRateItem} from '../models/level-rate-item';
import {LevelModel} from 'app/revenue-management/level/models/level.model';
import {LevelRate} from '../models/level-rate';

export const RATE_LEVEL_LIST = 'RATE_LEVEL_LIST';
export const CURRENCY_LIST = 'CURRENCY_LIST';
export const LEVEL_LIST = 'LEVEL_LIST';
export const LEVEL_LIST_OPTION = 'LEVEL_LIST_OPTION';
export const CURRENCY_LIST_OPTION = 'CURRENCY_LIST_OPTION';
export const RATE_LEVEL_HEADER = 'RATE_LEVEL_HEADER';

export class DATA {

  public static readonly RATE_LEVEL_LIST = <LevelRateItem[]>[
    {
      id: '1',
      isActive: true,
      levelName: 'Level 1',
      rateName: 'Alta temporada B1',
      currencySymbol: 'R$',
      currencyName: 'Real',
      initialDate: '2019-01-10T13:52:30.057',
      endDate: '2019-01-10T13:52:30.057',
      levelId: '64084C54-CFF5-47A0-A5B4-271273791766',
      currencyId: '00000000-0000-0000-0000-000000000000'
    },
    {
      id: '2',
      isActive: true,
      levelName: 'Level 1',
      rateName: 'Alta temporada B1',
      currencySymbol: 'R$',
      currencyName: 'Real',
      initialDate: '2019-02-10T13:52:30.057',
      endDate: '2019-02-10T13:52:30.057'
    },
    {
      id: '3',
      isActive: false,
      levelName: 'Level 2',
      rateName: 'Alta temporada B2',
      currencySymbol: 'R$',
      currencyName: 'Real',
      initialDate: '2019-05-10T13:52:30.057',
      endDate: '2019-05-10T13:52:30.057'
    }
  ];

  public static readonly CURRENCY_LIST = [
    {
      id: 'c5ef2cfa-a05e-44ed-ad2e-089b26471432',
      symbol: '$',
      currencyName: 'Dólar'
    },
    {
      id: '67838acb-9525-4aeb-b0a6-127c1b986c48',
      symbol: 'R$',
      currencyName: 'Real'
    },
    {
      id: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
      symbol: '€',
      currencyName: 'Euro'
    },
    {
      id: '100326fd-450d-48af-a756-f28aaf7d87da',
      symbol: '$',
      currencyName: 'Peso Argentino'
    }
  ];

  public static readonly LEVEL_LIST = <LevelModel[]>[
    {
      id: '123',
      order: 1,
      isActive: true,
      levelCode: 1,
      description: 'Best Available Rate 1'
    },
    {
      id: '456',
      order: 2,
      isActive: false,
      levelCode: 2,
      description: 'Best Available Rate 2'
    }
  ];

  public static readonly LEVEL_LIST_OPTION = [
    {
      key: '123',
      value: 'Best Available Rate 1'
    },
    {
      key: '456',
      value: 'Best Available Rate 2'
    }
  ];

  public static readonly CURRENCY_LIST_OPTION = [
    {
      key: 'c5ef2cfa-a05e-44ed-ad2e-089b26471432',
      symbol: '$',
      value: 'Dólar $'
    },
    {
      key: '67838acb-9525-4aeb-b0a6-127c1b986c48',
      symbol: 'R$',
      value: 'Real R$'
    }
  ];

  public static readonly RATE_LEVEL_HEADER = <LevelRate> {
    levelRateHeader: {
      id: '12340000-0000-0000-0000-000000000000',
      rateName: '$ 1',
      initialDate: '2019-05-10T13:52:30.058Z',
      endDate: '2019-05-15T13:52:30.058Z',
      levelId: '8dd833ef-c63c-4fde-86dc-84bf746a12e5',
      currencyId: '67838ACB-9525-4AEB-B0A6-127C1B986C48'
    },
    levelRateList: [
      {
        id: '00000000-0000-0000-0000-000000000000',
        pax1Amount: 170,
        pax2Amount: 170,
        pax3Amount: 170,
        pax4Amount: 170,
        pax5Amount: 170,
        paxAdditionalAmount: 170,
        child1Amount: 170,
        child2Amount: 170,
        child3Amount: 170,
        roomTypeId: 1
      },
      {
        id: '00000000-0000-0000-0000-000000000000',
        pax1Amount: 190,
        pax2Amount: 190,
        pax3Amount: 190,
        pax4Amount: 190,
        pax5Amount: 190,
        paxAdditionalAmount: 190,
        child1Amount: 190,
        child2Amount: 190,
        child3Amount: 190,
        roomTypeId: 2
      },
      {
        id: '00000000-0000-0000-0000-000000000000',
        pax1Amount: 250,
        pax2Amount: 250,
        pax3Amount: 250,
        pax4Amount: 250,
        pax5Amount: 250,
        paxAdditionalAmount: 220,
        child1Amount: 220,
        child2Amount: 220,
        child3Amount: 220,
        roomTypeId: 3
      }
    ],
    levelRateMealPlanTypeList: [
      {
        id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
        isMealPlanTypeDefault: false,
        mealPlanTypeName: 'Meia Pensão',
        mealPlanTypeId: 3,
        adultMealPlanAmount: 50,
        child1MealPlanAmount: 50,
        child2MealPlanAmount: 50,
        child3MealPlanAmount: 50
      },
      {
        id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
        isMealPlanTypeDefault: true,
        mealPlanTypeName: 'Pensão Completa',
        mealPlanTypeId: 6,
        adultMealPlanAmount: 100,
        child1MealPlanAmount: 100,
        child2MealPlanAmount: 100,
        child3MealPlanAmount: 100
      },
      {
        id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
        isMealPlanTypeDefault: false,
        mealPlanTypeName: 'Meia Pensão Jantar',
        mealPlanTypeId: 5,
        adultMealPlanAmount: 50,
        child1MealPlanAmount: 50,
        child2MealPlanAmount: 50,
        child3MealPlanAmount: 50
      },
      {
        id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
        isMealPlanTypeDefault: false,
        mealPlanTypeName: 'Café da Manhã',
        mealPlanTypeId: 2,
        adultMealPlanAmount: 50,
        child1MealPlanAmount: 50,
        child2MealPlanAmount: 50,
        child3MealPlanAmount: 50
      },
      {
        id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
        isMealPlanTypeDefault: false,
        mealPlanTypeName: 'Meia Pensão Almoço',
        mealPlanTypeId: 4,
        adultMealPlanAmount: 50,
        child1MealPlanAmount: 50,
        child2MealPlanAmount: 50,
        child3MealPlanAmount: 50
      },
      {
        id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
        isMealPlanTypeDefault: false,
        mealPlanTypeName: 'Nenhuma',
        mealPlanTypeId: 1,
        adultMealPlanAmount: 0,
        child1MealPlanAmount: 0,
        child2MealPlanAmount: 0,
        child3MealPlanAmount: 0
      },
      {
        id: 'cf4cf561-b2fd-492d-9c88-bf7e5c97f5e6',
        isMealPlanTypeDefault: false,
        mealPlanTypeName: 'Tudo Incluso',
        mealPlanTypeId: 7,
        adultMealPlanAmount: 100,
        child1MealPlanAmount: 100,
        child2MealPlanAmount: 100,
        child3MealPlanAmount: 100
      }
    ]
  };
}
