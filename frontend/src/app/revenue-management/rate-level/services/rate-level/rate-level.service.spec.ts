import { TestBed } from '@angular/core/testing';
import {
  DATA,
  LEVEL_LIST_OPTION,
  RATE_LEVEL_HEADER,
  RATE_LEVEL_LIST
} from 'app/revenue-management/rate-level/mock-data';
import { RateLevelResource } from 'app/revenue-management/rate-level/resource/rate-level.resource';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing';

import { RateLevelService } from './rate-level.service';

const RateLevelResourceProvider = createServiceStub(RateLevelResource, {
  getAllLevelsAvailable(initialDate: string, endDate: string, currencyId: string): Observable<any> {
    return of(null);
  },
  getById(id: string): Observable<any> {
    return of(null);
  }
});

const SessionParameterProvider = createServiceStub(SessionParameterService);

describe('RateLevelService', () => {
  let service: RateLevelService;
  let resource: RateLevelResource;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RateLevelService,
        RateLevelResourceProvider,
        // Dependencia do DateService
        SessionParameterProvider
      ]
    });

    service = TestBed.get(RateLevelService);
    resource = TestBed.get(RateLevelResource);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should searchByNameLevelVigenceRate', () => {
    const array = DATA[RATE_LEVEL_LIST];
    const result = service.searchByNameLevelVigenceRate(array, 'Level 2');

    expect(result.length).toEqual(1);
    expect(result[0].levelName).toEqual('Level 2');
  });

  it('should updateForm', () => {
    const array = DATA[RATE_LEVEL_LIST];
    const result = service.updateForm(array);

    expect(result.length).toEqual(3);
    expect(result[0].vigenceDate).toEqual('10/01/2019 - 10/01/2019');
    expect(result[0].currency).toEqual('R$ Real');
  });

  it('should getAllLevelsAvailable', () => {
    const levelList = [
      {
        id: '1234',
        description: '4321'
      },
    ];
    const levelRateMealPlanTypeList = [];
    spyOn(resource, 'getAllLevelsAvailable').and.returnValue(of({levelList, levelRateMealPlanTypeList}));

    service.getAllLevelsAvailable('2019-04-12T00:00:00', '2019-04-13T00:00:00', '1')
      .subscribe((data) => {
        expect(data.levelList).toEqual([{key: levelList[0].id, value: levelList[0].description}]);
        expect(data.levelRateMealPlanTypeList).toEqual(levelRateMealPlanTypeList);
      });
  });

  it('should getCompleteLevelRate', () => {
    const id = '123';
    const levelRate = DATA[RATE_LEVEL_HEADER];
    const configuration = {
      levelList: DATA[LEVEL_LIST_OPTION],
      levelRateMealPlanTypeList: DATA[RATE_LEVEL_HEADER].levelRateMealPlanTypeList
    };

    spyOn(resource, 'getById').and.returnValue(of(levelRate));
    spyOn(service, 'getAllLevelsAvailable').and.returnValue(of(configuration));

    service.getCompleteLevelRate(id).subscribe(data => {
      expect(levelRate).toEqual(data.levelRate);
      expect(configuration).toEqual(data.levelRateConfig);
    });
  });

  describe('#getBaseRateRoomTableData', () => {
    it('should return the configList when there is no levelRateList', () => {
      const configList = [{foo: 'foo'}];
      const list = service.getBaseRateRoomTableData(configList, []);
      expect(list).toEqual(configList);
    });

    it('should run #normalizeToBaseRateTable when there is levelRateList', () => {
      const configList = [{id: '123'}];
      const levelRateList = [
        {id: '000-00', roomTypeId: '123'},
        {id: '111-11', roomTypeId: '124'},
      ];
      spyOn(service, 'normalizeToBaseRateTable').and.callThrough();

      service.getBaseRateRoomTableData(configList, levelRateList);
      expect(service.normalizeToBaseRateTable).toHaveBeenCalledWith({
        ...configList[0],
        ...levelRateList[0],
        id: configList[0].id
      });
    });
  });

  describe('#getBaseRateMealTableData', () => {
    it('should return the configList when there is no levelRateMealPlanTypeList', () => {
      const configList = [{foo: 'foo'}];
      const list = service.getBaseRateMealTableData(configList, []);
      expect(list).toEqual(configList);
    });

    it('should return a new list when there is levelRateMealPlanTypeList', () => {
      const configList = [{mealPlanTypeId: '123'}];
      const levelRateMealPlanTypeList = [
        {id: '000-00', mealPlanTypeId: '123'},
        {id: '111-11', mealPlanTypeId: '124'}
      ];

      const list = service.getBaseRateMealTableData(configList, levelRateMealPlanTypeList);
      expect(list).toEqual([{
        ...configList[0],
        ...levelRateMealPlanTypeList[0]
      }]);
    });
  });

  it('should normalizeToBaseRateTable', () => {
    const levelRate = {
      foo: 'foo',
      pax1Amount: '123',
      pax2Amount: '223',
      pax3Amount: '323',
      pax4Amount: '423',
      pax5Amount: '523',
      paxAdditionalAmount: '523',
    };
    const normalizedLevelRate = service.normalizeToBaseRateTable(levelRate);

    expect(normalizedLevelRate).toEqual({
      ...levelRate,
      paxOne: levelRate.pax1Amount,
      paxTwo: levelRate.pax2Amount,
      paxThree: levelRate.pax3Amount,
      paxFour: levelRate.pax4Amount,
      paxFive: levelRate.pax5Amount,
      paxAdditional: levelRate.paxAdditionalAmount
    });
  });
});
