import { DATA, LEVEL_LIST_OPTION, RATE_LEVEL_HEADER } from 'app/revenue-management/rate-level/mock-data';
import { LevelRate } from 'app/revenue-management/rate-level/models/level-rate';
import { RateLevelService } from 'app/revenue-management/rate-level/services/rate-level/rate-level.service';
import { Option } from 'app/shared/models/option';
import { BaseRateConfigurationMealPlanType } from 'app/shared/models/revenue-management/meal-type-fix-rate';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../../testing';
import { LevelRateItem } from 'app/revenue-management/rate-level/models/level-rate-item';

export const rateLevelProvider = createServiceStub(RateLevelService, {
  getAllLevelsAvailable(initialDate: string, endDate: string, currencyId: string):
    Observable<{levelList: Option[], levelRateMealPlanTypeList: BaseRateConfigurationMealPlanType[]}> {
    return of({levelList: [], levelRateMealPlanTypeList: []});
  },
  createRateLevel (data: LevelRate): Observable<any> {
    return of(null);
  },
  getBaseRateRoomTableData(configList, levelRateList): any {
    return of([]);
  },
  getBaseRateMealTableData(configList, levelRateMealPlanTypeList): any {
    return of([]);
  },
  getCompleteLevelRate(id: string): Observable<any> {
    const levelRate = DATA[RATE_LEVEL_HEADER];
    const levelRateConfig = {
      levelList: DATA[LEVEL_LIST_OPTION],
      levelRateMealPlanTypeList: DATA[RATE_LEVEL_HEADER].levelRateMealPlanTypeList
    };

    return of({levelRate, levelRateConfig});
  },
  editLevelRate(id: string, data: LevelRate): Observable<any> {
    return of(null);
  },

  updateForm: (array: Array<LevelRateItem>) => [],
});
