import { Injectable } from '@angular/core';
import { swapKeys, toOptions } from '@inovacaocmnet/thx-bifrost';
import { LevelRate } from 'app/revenue-management/rate-level/models/level-rate';
import { LevelRateItem } from 'app/revenue-management/rate-level/models/level-rate-item';
import { RateLevelResource } from 'app/revenue-management/rate-level/resource/rate-level.resource';
import { Option } from 'app/shared/models/option';
import { BaseRateConfigurationMealPlanType } from 'app/shared/models/revenue-management/meal-type-fix-rate';
import { DateService } from 'app/shared/services/shared/date.service';
import { forkJoin, from, Observable, of } from 'rxjs';
import { map, switchMap, toArray } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class RateLevelService {

  constructor(
    private dateService: DateService,
    private rateLevelResource: RateLevelResource,
  ) {}

  public searchByNameLevelVigenceRate(array: Array<LevelRateItem>, search): Array<LevelRateItem> {
    const regexp = new RegExp(search, 'i');
    if (search != '' && search.length > 1) {
      return array
        .filter(function (v) {
          return regexp.test(v.rateName)
            || regexp.test(v.levelName.toString())
            || regexp.test(v.vigenceDate);
        });
    } else {
      return array;
    }
  }

  public updateForm(array: Array<LevelRateItem>): Array<LevelRateItem> {
    array.forEach( data => {
      const initial = this.dateService.convertUniversalDateToViewFormat(data.initialDate);
      const end = this.dateService.convertUniversalDateToViewFormat(data.endDate);

      data.vigenceDate = `${initial} - ${end}`;
      data.currency = `${data.currencySymbol} ${data.currencyName}`;
    });

    return array;
  }

  public getAllLevelsAvailable(initialDate: string, endDate: string, currencyId: string):
    Observable<{levelList: Option[], levelRateMealPlanTypeList: BaseRateConfigurationMealPlanType[]}> {
    const formattedInitialDate = this.dateService.clearTime(initialDate);
    const formattedEndDate = this.dateService.clearTime(endDate);

    return this.rateLevelResource
      .getAllLevelsAvailable(formattedInitialDate, formattedEndDate, currencyId)
      .pipe(
        switchMap(({levelList, levelRateMealPlanTypeList}) => {
          const levelList$ = <Observable<Option[]>> from(levelList).pipe(
            toOptions('id', 'description'),
            swapKeys(['label'], ['value']),
            toArray()
          );
          return forkJoin(levelList$, of(levelRateMealPlanTypeList));
        }),
        map(([levelList, levelRateMealPlanTypeList]) => ({levelList, levelRateMealPlanTypeList}))
      );
  }

  public createRateLevel(data: LevelRate):
    Observable<any> {
    return this.rateLevelResource.createRateLevel(data);
  }

  public getCompleteLevelRate(id: string): Observable<any> {
    return this.rateLevelResource.getById(id)
      .pipe(
        switchMap((levelRate) => {
          const { initialDate, endDate, currencyId } = levelRate.levelRateHeader;
          const configuration$ = this.getAllLevelsAvailable(initialDate, endDate, currencyId);
          return forkJoin(of(levelRate), configuration$);
        }),
        map(([levelRate, levelRateConfig]) => ({ levelRate, levelRateConfig }))
      );
  }

  public getBaseRateRoomTableData(configList, levelRateList) {
    if (!levelRateList || !levelRateList.length) {
      return configList.map(item => ({...item}));
    }

    return configList
      .map((roomType) => {
        return this.normalizeToBaseRateTable({
          ...roomType,
          ...levelRateList.find((levelRate) => levelRate.roomTypeId == roomType.id),
          id: roomType.id
        });
      });
  }

  public getBaseRateMealTableData(configList, levelRateMealPlanTypeList) {
    if (!configList || !configList.length) { return []; }

    if (!levelRateMealPlanTypeList || !levelRateMealPlanTypeList.length) {
      return configList.map(item => ({...item}));
    }

    return configList
      .map((mealType) => {
        return {
          ...mealType,
          ...levelRateMealPlanTypeList.find(mealPlanType => mealType.mealPlanTypeId == mealPlanType.mealPlanTypeId)
        };
      });
  }

  public normalizeToBaseRateTable(levelRate) {
    return {
      ...levelRate,
      paxOne: levelRate.pax1Amount,
      paxTwo: levelRate.pax2Amount,
      paxThree: levelRate.pax3Amount,
      paxFour: levelRate.pax4Amount,
      paxFive: levelRate.pax5Amount,
      paxAdditional: levelRate.paxAdditionalAmount
    };
  }

  public editLevelRate(id: string, data: LevelRate):
    Observable<any> {
    return this.rateLevelResource.editLevelRate(id, data);
  }

  public getLevelRateByDate(initialDate: string, endDate: string, currencyId?: string):
    Observable<any> {
    return this.rateLevelResource.getAllLevelRatesAvailable(initialDate, endDate, currencyId);
  }

  public cleanRoomTypeList(roomTypeList) {
    if (!roomTypeList.length) {
      return [];
    }

    return roomTypeList.map(roomType => ({
      ...roomType,
      paxOne: null,
      paxTwo: null,
      paxThree: null,
      paxFour: null,
      paxFive: null,
      paxAdditional: null,
      child1Amount: null,
      child2Amount: null,
      child3Amount: null,
    }));
  }

  public cleanMealPlanTypeList(mealPlanTypeList) {
    if (!mealPlanTypeList.length) {
      return [];
    }

    return mealPlanTypeList.map(mealPlanType => ({
      ...mealPlanType,
      adultMealPlanAmount: null,
      child1MealPlanAmount: null,
      child2MealPlanAmount: null,
      child3MealPlanAmount: null,
      isMealPlanTypeDefault: false,
    }));
  }
}
