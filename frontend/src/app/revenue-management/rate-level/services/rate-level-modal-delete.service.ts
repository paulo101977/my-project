import { Injectable } from '@angular/core';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RateLevelModalDeleteService {
  private modalId = 'LEVEL_DELETE';
  private modalData = new Subject<any>();
  public sentModalData$ = this.modalData.asObservable();

  constructor(
    private modalToggle: ModalEmitToggleService,
  ) { }

  public getModalId() {
    return this.modalId;
  }

  public openModal(rateLevelData: any) {
    this.modalToggle.emitOpenWithRef(this.modalId);
    this.modalData.next(rateLevelData);
  }

  public closeModal() {
    this.modalToggle.emitCloseWithRef(this.modalId);
  }
}
