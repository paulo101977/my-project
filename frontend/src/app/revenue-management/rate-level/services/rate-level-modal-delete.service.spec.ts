import { TestBed, inject } from '@angular/core/testing';

import { RateLevelModalDeleteService } from './rate-level-modal-delete.service';

describe('RateLevelModalDeleteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RateLevelModalDeleteService]
    });
  });

  it('should be created', inject([RateLevelModalDeleteService], (service: RateLevelModalDeleteService) => {
    expect(service).toBeTruthy();
  }));
});
