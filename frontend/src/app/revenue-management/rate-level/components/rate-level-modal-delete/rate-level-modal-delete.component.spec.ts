import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

import { RateLevelModalDeleteComponent } from './rate-level-modal-delete.component';
import {createServiceStub} from '../../../../../../testing';
import {Observable, of} from 'rxjs';
import {RateLevelResource} from 'app/revenue-management/rate-level/resource/rate-level.resource';


describe('RateLevelModalDeleteComponent', () => {
  let component: RateLevelModalDeleteComponent;
  let fixture: ComponentFixture<RateLevelModalDeleteComponent>;

  const RateLevelResourceStub = createServiceStub(RateLevelResource, {
    deleteRateLevel(id: string): Observable<any> { return of(null); }
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ RateLevelModalDeleteComponent ],
      imports: [
        TranslateTestingModule
      ],
      providers: [
        RateLevelResourceStub
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateLevelModalDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
