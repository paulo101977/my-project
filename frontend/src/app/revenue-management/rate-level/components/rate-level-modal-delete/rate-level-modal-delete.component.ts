import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { RateLevelModalDeleteService } from 'app/revenue-management/rate-level/services/rate-level-modal-delete.service';
import {ButtonConfig, ButtonType} from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import {RateLevelResource} from 'app/revenue-management/rate-level/resource/rate-level.resource';
import {LevelRateItem} from 'app/revenue-management/rate-level/models/level-rate-item';

@Component({
  selector: 'app-rate-level-modal-delete',
  templateUrl: './rate-level-modal-delete.component.html',
  styleUrls: ['./rate-level-modal-delete.component.css']
})
export class RateLevelModalDeleteComponent implements OnInit {
  @Output() public deleted = new EventEmitter();

  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;
  public selectedLevel: LevelRateItem;

  constructor(
    public levelModalDeleteService: RateLevelModalDeleteService,
    private sharedService: SharedService,
    private rateLevelResource: RateLevelResource
  ) { }

  ngOnInit() {
    this.setButtonsConfigs();
    this.levelModalDeleteService.sentModalData$.subscribe( data => {
      this.selectedLevel = data;
    });
  }

  private setButtonsConfigs() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'level-modal-delete-cancel',
      () => this.closeModal(),
      'action.no',
      ButtonType.Secondary,
    );

    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'level-modal-delete-confirm',
      () => this.deleteRateLevel(),
      'action.yes',
      ButtonType.Primary,
    );
  }

  public deleteRateLevel() {
    this.rateLevelResource.deleteRateLevel(this.selectedLevel.id).subscribe( () => {
      this.deleted.emit();
    });
  }

  public closeModal() {
    this.levelModalDeleteService.closeModal();
  }
}
