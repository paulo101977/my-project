import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ThexApiService, ThexApiTestingModule } from '@inovacaocmnet/thx-bifrost';
import { RateLevelService } from 'app/revenue-management/rate-level/services/rate-level/rate-level.service';
import { rateLevelProvider } from 'app/revenue-management/rate-level/services/rate-level/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { BaseRateTable } from 'app/shared/models/revenue-management/property-base-rate';
import { RoomTypeParametersConfiguration } from 'app/shared/models/revenue-management/room-type-parameters-configuration';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { baseRateResourceProvider } from 'app/shared/resources/base-rate/testing/base-rate.resource.provider';
import { BaseRateService } from 'app/shared/services/base-rate/base-rate.service';
import { locale } from 'moment';
import { configureTestSuite } from 'ng-bullet';
import { of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing';
import {
  DATA,
  CURRENCY_LIST_OPTION,
  LEVEL_LIST_OPTION,
  RATE_LEVEL_HEADER
} from '../../mock-data/index';

import { RateLevelNewEditComponent } from './rate-level-new-edit.component';

describe('RateLevelNewEditComponent', () => {
  let component: RateLevelNewEditComponent;
  let fixture: ComponentFixture<RateLevelNewEditComponent>;
  locale('pt-BR');

  let rateLevelService: RateLevelService;
  let thexApiService: ThexApiService;

  const baseRateProvider = createServiceStub(BaseRateService, {
    getBaseRateTableList(roomTypeWithBaseRateList: Array<RoomTypeParametersConfiguration>): Array<BaseRateTable> {
      return [];
    },
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        HttpClientTestingModule,
        ThexApiTestingModule
      ],
      providers: [
        FormBuilder,
        baseRateResourceProvider,
        rateLevelProvider,
        baseRateProvider,
      ],
      declarations: [
        RateLevelNewEditComponent
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    rateLevelService = TestBed.get(RateLevelService);
    thexApiService = TestBed.get(ThexApiService);

    fixture = TestBed.createComponent(RateLevelNewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn(component, 'setForms');
    spyOn<any>(component, 'buttonConfig');
    spyOn<any>(component, 'getBaseRateConfiguration').and.callThrough();

    component.ngOnInit();

    expect(component.setForms).toHaveBeenCalled();
    expect(component['buttonConfig']).toHaveBeenCalled();
    expect(component['getBaseRateConfiguration']).toHaveBeenCalled();
    expect(component.pageTitle).toEqual('title.lbInsert');
  });

  describe('when applying header', () => {
    it('should do nothing if header form is invalid', () => {
      component.headerForm.get('vigenceDate').setValue(null);
      component.headerApply();
      expect(component.configRateLevel).toEqual(false);
    });

    it('should select the currency and load the level configuration', () => {
      spyOn(rateLevelService, 'getAllLevelsAvailable').and.returnValue(of({
        levelList: [],
        levelRateMealPlanTypeList: []
      }));
      component.currencyList = DATA[CURRENCY_LIST_OPTION];
      component.headerForm.get('vigenceDate').setValue({beginDate: '2019-05-01T00:00:00', endDate: '2019-05-03T00:00:00'});
      component.headerForm.get('currency').setValue('67838acb-9525-4aeb-b0a6-127c1b986c48');

      component.headerApply();

      expect(component.configRateLevel).toEqual(true);
      expect(component.buttonApplyConfig.isDisabled).toEqual(true);
      expect(component.rangeDate).toEqual('01/05/2019 - 03/05/2019');
      expect(component.levelRateForm.get('nameRate').value).toEqual('R$');
      expect(component.levelOptionsList).toEqual([]);
      expect(component['mealTypeDefaultList']).toEqual([]);
    });
  });

  describe('should showToastMessage', () => {
    it('variable lbSaveSuccessF', () => {
      spyOn(component['toastEmitter'], 'emitChange');
      component.showToastMessage('variable.lbSaveSuccessF');
      expect(component['toastEmitter'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'variable.lbSaveSuccessF'
      );
    });

    it('variable lbEditSuccessF', () => {
      spyOn(component['toastEmitter'], 'emitChange');
      component.showToastMessage('variable.lbEditSuccessF');
      expect(component['toastEmitter'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'variable.lbEditSuccessF'
      );
    });
  });

  it('should loadLevelRateData', () => {
    const levelRate = DATA[RATE_LEVEL_HEADER];
    const levelList = DATA[LEVEL_LIST_OPTION];
    spyOn(component, 'populateLevelRateForm');
    spyOn(component, 'populateLevelRateHeader');

    spyOn<any>(rateLevelService, 'getAllLevelsAvailable').and.returnValue(of({
      levelList: DATA[RATE_LEVEL_HEADER],
      levelRateConfig: []
    }));

    component.loadLevelRateData('1').subscribe();

    expect(component.populateLevelRateForm).toHaveBeenCalled();
    expect(component.populateLevelRateHeader).toHaveBeenCalled();
  });

  it('should populateLevelRateHeader', () => {
    spyOn(component, 'setMealTableData');
    const level = DATA[RATE_LEVEL_HEADER];
    const levelRate = {
      initialDate: '2019-05-10T13:52:30.058',
      endDate: '2019-05-15T13:52:30.058',
      levelName: '',
      levelId: '1'
    };
    const config = {
      levelList: DATA[LEVEL_LIST_OPTION],
      levelRateMealPlanTypeList: level.levelRateMealPlanTypeList
    };

    component.populateLevelRateHeader(levelRate, config);

    expect(component.rangeDate).toEqual('10/05/2019 - 15/05/2019');
    expect(component.levelOptionsList).toEqual(DATA[LEVEL_LIST_OPTION]);
    expect(component.setMealTableData).toHaveBeenCalled();
  });
});
