import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LevelRate } from 'app/revenue-management/rate-level/models/level-rate';
import { RateLevelService } from 'app/revenue-management/rate-level/services/rate-level/rate-level.service';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { Option } from 'app/shared/models/option';
import { Parameter } from 'app/shared/models/parameter';
import { MealPlanType } from 'app/shared/models/revenue-management/meal-plan-type';
import { BaseRateConfigurationMealPlanType } from 'app/shared/models/revenue-management/meal-type-fix-rate';
import { PropertyRate } from 'app/shared/models/revenue-management/property-base-rate';
import { RoomTypeParametersConfiguration } from 'app/shared/models/revenue-management/room-type-parameters-configuration';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { BaseRateResource } from 'app/shared/resources/base-rate/base-rate.resource';
import { BaseRateService } from 'app/shared/services/base-rate/base-rate.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { forkJoin, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-rate-level-new-edit',
  templateUrl: './rate-level-new-edit.component.html',
  styleUrls: ['./rate-level-new-edit.component.scss']
})
export class RateLevelNewEditComponent implements OnInit {
  public modalId = 'CLEAN_MODAL';
  public configHeaderPage = <ConfigHeaderPageNew>{
    hasBackPreviewPage: true,
  };
  public pageTitle: string;
  public isNew: boolean;
  public headerForm: FormGroup;
  public levelRateForm: FormGroup;
  public levelRateId: string;
  public currencyList: Array<any>;
  public levelOptionsList: Option[];
  public buttonApplyConfig: ButtonConfig;
  public buttonCleanConfig: ButtonConfig;
  public modalButtonCancelConfig: ButtonConfig;
  public modalButtonConfirmConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;
  public buttonCancelConfig: ButtonConfig;
  public rangeDate: string;
  public propertyParameterList: Array<Parameter>;
  public useMealTypeListDefault: boolean;
  public useMealTypeListFromBase: boolean;
  public propertyId: number;
  public hasBaseRateConfiguration: boolean;
  public configRateLevel = false;
  public roomTypeList: RoomTypeParametersConfiguration[];
  public roomTypeConfigList: RoomTypeParametersConfiguration[];
  public mealTypeList: Array<MealPlanType>;
  public mealPlanTypeConfigList: MealPlanType[];
  public mealTypeDefaultList: Array<BaseRateConfigurationMealPlanType> = [];
  public mealTypeBackList: Array<BaseRateConfigurationMealPlanType> = [];
  public isDefaultMealPlanTypeToggleDisabled: boolean;

  private roomTypeWithBaseRateListValid: boolean;
  private propertyRate: PropertyRate;

  constructor(
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private toastEmitter: ToasterEmitService,
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private modalToggler: ModalEmitToggleService,
    private dateService: DateService,
    private baseRateResource: BaseRateResource,
    private baseRateService: BaseRateService,
    private rateLevelService: RateLevelService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.setVars();
    this.setForms();
    this.buttonConfig();
    this.setRouterParamsListener();
  }

  private setRouterParamsListener() {
    const {rateId, property} = this.route.snapshot.params;
    this.propertyId = +property;
    this.levelRateId = rateId;
    this.isNew = !rateId;

    const mode = rateId ? 'title.lbEdit' : 'title.lbInsert';
    this.pageTitle = this.translateService.instant(mode, {
      labelName: this.translateService.instant('title.rateLevel'),
    });

    this.loadPageData(rateId);
  }

  private loadPageData(rateId: string) {
    const baseRateConfiguration$ = this.getBaseRateConfiguration();
    const levelRateData$ = !!rateId ? this.loadLevelRateData(rateId) : of(null);

    forkJoin([ baseRateConfiguration$, levelRateData$ ])
      .subscribe(([_, levelRateData]) => {
        const levelRate = levelRateData ? levelRateData.levelRate : {};
        const roomTypeList = this.rateLevelService
          .getBaseRateRoomTableData(this.roomTypeConfigList, levelRate.levelRateList);

        this.mealTypeBackList = levelRate.levelRateMealPlanTypeList || [];
        this.propertyRate.baseRateConfigurationRoomTypeList = this.baseRateService.getBaseRateTableList(roomTypeList);

        this.setRoomTableData(levelRate.levelRateList);
        this.setMealTableData();
        this.checkPropertyRate();
      });
  }

  public setVars() {
    this.propertyRate = new PropertyRate();
    this.propertyRate.propertyId = this.route.snapshot.params.property;
    this.propertyRate.baseRateConfigurationRoomTypeList = [];
    this.propertyRate.baseRateConfigurationMealPlanTypeList = [];
  }

  public setForms() {
    this.headerForm = this.formBuilder.group({
      vigenceDate: [null, [Validators.required]],
      currency: [null, [Validators.required]],
    });

    this.levelRateForm = this.formBuilder.group({
      level: [null, [Validators.required]],
      nameRate: [null, [Validators.required]]
    });

    this.levelRateForm.valueChanges.subscribe(() => {
      this.checkPropertyRate();
    });
  }

  public loadRateLevelConfiguration(initialDate: string, endDate: string, currencyId: string) {
    return this.rateLevelService.getAllLevelsAvailable(initialDate, endDate, currencyId);
  }

  private setHeaderDisableState(isDisabled: boolean) {
    this.buttonApplyConfig.isDisabled = isDisabled;
    const vigenceDateControl = this.headerForm.get('vigenceDate');
    const currencyControl = this.headerForm.get('currency');

    if (isDisabled) {
      vigenceDateControl.disable();
      currencyControl.disable();
      return;
    }

    vigenceDateControl.enable();
    currencyControl.enable();
  }

  private buttonConfig() {
    this.buttonApplyConfig = this.sharedService.getButtonConfig(
      'save-button',
      () => this.headerApply(),
      'action.apply',
      ButtonType.Secondary
    );

    this.buttonCleanConfig = this.sharedService.getButtonConfig(
      'clean-button',
      () => this.openCleanModal(),
      'action.cleanEverything',
      ButtonType.Secondary
    );

    this.modalButtonCancelConfig =  this.sharedService.getButtonConfig(
      'modal-cancel-button',
      () => this.closeCleanModal(),
      'action.no',
      ButtonType.Secondary
    );

    this.modalButtonConfirmConfig = this.sharedService.getButtonConfig(
      'modal-confirm-button',
      () => this.confirmCleanModal(),
      'action.yes',
      ButtonType.Secondary
    );

    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-button',
      () => this.goToLevelRate(),
      'action.cancel',
      ButtonType.Secondary
    );

    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'save-button',
      () => this.isNew ? this.saveLevelRate() : this.editLevelRate(),
      'action.save',
      ButtonType.Primary
    );
  }

  public openCleanModal() {
    this.modalToggler.emitOpenWithRef(this.modalId);
  }

  public closeCleanModal() {
    this.modalToggler.emitCloseWithRef(this.modalId);
  }

  public headerApply() {
    const { vigenceDate, currency } = this.headerForm.value;
    if (!vigenceDate || !currency) {
     return;
    }

    const { beginDate: initialDate, endDate } = vigenceDate;

    this.loadRateLevelConfiguration(initialDate, endDate, currency)
      .subscribe((levelRateConfig) => {
        const selectedCurrency = this.currencyList.find(c => c.key === currency);
        if (selectedCurrency) {
          this.levelRateForm.patchValue({nameRate: selectedCurrency.symbol});
        }

        this.setHeaderDisableState(true);
        this.populateLevelRateHeader({initialDate, endDate}, levelRateConfig);
      });
  }

  public populateLevelRateHeader({initialDate, endDate, levelName = '', levelId = ''}, {levelList, levelRateMealPlanTypeList}) {
    this.rangeDate = this.dateService.formatVigenceString(initialDate, endDate);
    this.configRateLevel = true;
    this.levelOptionsList = [
      ...levelList,
      ...(!this.isNew ? [{key: levelId, value: levelName}] : [])
    ];
    // this.mealTypeDefaultList = levelRateMealPlanTypeList;
    //  TODO uncomment when there is a solution for multiple default meal plans
    this.mealTypeDefaultList = [];
    this.isDefaultMealPlanTypeToggleDisabled = levelRateMealPlanTypeList && !levelRateMealPlanTypeList.length;
    this.setMealTableData();
  }

  private populateHeaderForm({initialDate, endDate}, currency, propertyMealPlanTypeRate) {
    this.headerForm.patchValue({
      vigenceDate: {
        beginDate: initialDate,
        endDate
      },
      currency: currency || this.headerForm.get('currency')
    });

    if (propertyMealPlanTypeRate) {
      this.toggleDefaultMealTypeList();
    }
  }

  public populateLevelRateForm({ levelRateHeader }) {
    const { levelId, rateName } = levelRateHeader;

    this.levelRateForm.patchValue({
      nameRate: rateName,
      level: levelId
    });
  }

  public loadLevelRateData(id: string) {
    return this.rateLevelService.getCompleteLevelRate(id)
      .pipe(
        tap(({levelRate, levelRateConfig}) => {
          this.populateHeaderForm(levelRate.levelRateHeader,
            levelRate.levelRateHeader.currencyId, levelRate.levelRateHeader.propertyMealPlanTypeRate);
          this.populateLevelRateHeader(levelRate.levelRateHeader, levelRateConfig);
          this.populateLevelRateForm(levelRate);
        })
      );
  }

  public setRoomTableData(levelRateList) {
    this.roomTypeList = this.rateLevelService
      .getBaseRateRoomTableData(this.roomTypeConfigList, levelRateList);
  }

  public setMealTableData() {
    const mealTypeList = this.useMealTypeListDefault
      ? [...this.mealTypeDefaultList]
      : [...this.mealTypeBackList];

    this.mealTypeList = this.rateLevelService
      .getBaseRateMealTableData(this.mealPlanTypeConfigList, mealTypeList);
  }

  public updatedListMealType(mealPlanTypeList?: BaseRateConfigurationMealPlanType[]) {
    this.mealTypeBackList = [...mealPlanTypeList];

    this.checkPropertyRate();
  }

  public confirmCleanModal() {
    this.closeCleanModal();
    this.configRateLevel = false;
    this.setHeaderDisableState(false);
    this.levelRateForm.reset();
    this.roomTypeList = this.rateLevelService.cleanRoomTypeList(this.roomTypeList);
    this.mealTypeBackList = this.rateLevelService.cleanMealPlanTypeList(this.mealTypeBackList);
  }

  private getBaseRateConfiguration() {
    return this.baseRateResource
      .getConfigurationDataParameters(this.propertyId)
      .pipe(
        tap( config => {
          this.hasBaseRateConfiguration = true;
          this.propertyParameterList = [...config.propertyParameterList];
          this.roomTypeConfigList = [...config.roomTypeList.items];
          this.mealPlanTypeConfigList = [...config.mealPlanTypeList.items];

          this.setCurrencyList(config.currencyList.items);
        })
      );
  }

  public setCurrencyList(list) {
    this.currencyList = list.map((item) => {
      const {id, currencyName, symbol} = item;
      return {
        key: id,
        value: `${currencyName} ${symbol}`,
        symbol: symbol
      };
    });
  }

  public setRoomTypeWithBaseRateUpdatedList(config?: any) {
    if (config.roomTypeList) {
      this.propertyRate.baseRateConfigurationRoomTypeList = this.baseRateService.getBaseRateTableList(config.roomTypeList);
      this.roomTypeWithBaseRateListValid = config.validBaseRate;
    }
    this.checkPropertyRate();
  }

  private checkPropertyRate() {
    const { levelRateList, levelRateMealPlanTypeList } = this.getLevelRateData();

    this.buttonSaveConfig.isDisabled = !(
      levelRateList.length
      && (levelRateMealPlanTypeList.length || this.useMealTypeListDefault)
      && this.levelRateForm.valid
    );
  }

  private saveLevelRate() {
    const data = this.getLevelRateData();

    this.rateLevelService.createRateLevel(data).subscribe( () => {
        this.showToastMessage('variable.lbSaveSuccessF');
        this.router.navigate([`p/${this.propertyId}/rm/rate-level`]);
      });
  }

  private editLevelRate() {
    const data = this.getLevelRateData();

    this.rateLevelService.editLevelRate(this.levelRateId, data).subscribe( () => {
      this.showToastMessage('variable.lbEditSuccessF');
      this.router.navigate([`p/${this.propertyId}/rm/rate-level`]);
    });
  }

  private goToLevelRate() {
    this.router.navigate([`p/${this.propertyId}/rm/rate-level`]);
  }

  public showToastMessage(message: string) {
    this.toastEmitter.emitChange(SuccessError.success,
      this.translateService.instant(message, {
      labelName: this.translateService.instant('label.rateLevel')
    }));
  }

  public toggleDefaultMealTypeList() {
    this.useMealTypeListDefault = !this.useMealTypeListDefault;
    this.checkPropertyRate();
  }

  private getLevelRateData(): LevelRate {
    const { vigenceDate, currency } = this.headerForm.value;
    const { beginDate = null, endDate = null } = vigenceDate || {};
    const { level, nameRate } = this.levelRateForm.value;
    const {
      baseRateConfigurationRoomTypeList: roomTypeList
    } = this.propertyRate;
    const mealPlanTypeList = this.useMealTypeListDefault ? this.mealTypeDefaultList : this.mealTypeBackList;
    const propertyMealPlanTypeRateHeaderId = mealPlanTypeList.length && mealPlanTypeList[0].id;

    const levelRateHeader = {
      initialDate: beginDate,
      endDate: endDate,
      rateName: nameRate,
      levelId: level,
      currencyId: currency,
      propertyMealPlanTypeRate: this.useMealTypeListDefault
    };

    return {
      levelRateMealPlanTypeList: mealPlanTypeList.filter(mealPlanType => mealPlanType.adultMealPlanAmount),
      levelRateList: roomTypeList.filter(roomType => roomType.pax1Amount),
      levelRateHeader
    } as LevelRate;
  }
}
