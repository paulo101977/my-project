import { HttpClientTestingModule } from '@angular/common/http/testing';
import {  NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { DATA, RATE_LEVEL_LIST } from 'app/revenue-management/rate-level/mock-data';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { of } from 'rxjs';
import { ActivatedRouteStub, routerStub } from '../../../../../../testing';

import { RateLevelComponent } from './rate-level.component';
import { configureTestSuite } from 'ng-bullet';
import { ActivatedRoute } from '@angular/router';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { rateLevelResourceStub } from 'app/revenue-management/rate-level/resource/testing';
import { rateLevelProvider } from 'app/revenue-management/rate-level/services/rate-level/testing';

describe('RateLevelComponent', () => {
  let component: RateLevelComponent;
  let fixture: ComponentFixture<RateLevelComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        HttpClientTestingModule,
        FormsModule
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        dateServiceStub,
        toasterEmitServiceStub,
        rateLevelResourceStub,
        rateLevelProvider,
        routerStub,
      ],
      declarations: [
        RateLevelComponent
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(RateLevelComponent);
    component = fixture.componentInstance;

    spyOn<any>(component['router'], 'navigate').and.callFake( f => f );
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn<any>(component, 'setHeaderConfig');
    spyOn<any>(component, 'setColumnsName');
    spyOn(component, 'loadData');

    component.ngOnInit();

    expect(component['setHeaderConfig']).toHaveBeenCalled();
    expect(component['setColumnsName']).toHaveBeenCalled();
    expect(component.loadData).toHaveBeenCalled();
    expect(component.itemsOption[0]['title']).toEqual('commomData.edit');
  });

  it('should setColumnsName', () => {
    component['setColumnsName']();

    expect(component.columns).not.toBeNull();
    expect(component.columns[0]).toEqual({ name: 'variable.nameOf.F', prop: 'rateName' });
    expect(component.columns[1]).toEqual({ name: 'label.level', prop: 'levelName' });
    expect(component.columns[2]).toEqual({ name: 'label.currency', prop: 'currency' });
    expect(component.columns[3]).toEqual({ name: 'label.vigence', prop: 'vigenceDate' });
  });

  it('should gotoNewRateLevel', fakeAsync( () => {
    component['propertyId'] = '1';
    component.gotoNewRateLevel();
    expect(component['router'].navigate)
      .toHaveBeenCalledWith(['p/1/rm/rate-level/rate']);
  }));

  it('should gotoEdit', fakeAsync( () => {
    component['propertyId'] = '1';
    component.gotoEdit({id: 1});
    expect(component['router'].navigate)
      .toHaveBeenCalledWith(['p/1/rm/rate-level/rate/1']);
  }));

  describe('should showToastStatus', () => {
    it('true', () => {
      spyOn(component['toastEmitter'], 'emitChange');
      component.showToastStatus(true);
      expect(component['toastEmitter'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'variable.lbActivatedWithSuccessM'
      );
    });

    it('false', () => {
      spyOn(component['toastEmitter'], 'emitChange');
      component.showToastStatus(false);
      expect(component['toastEmitter'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'variable.lbInactivatedWithSuccessM'
      );
    });
  });

  it('should loadData', fakeAsync(() => {
    const item = { items: DATA[RATE_LEVEL_LIST]};
    item.items[0].currency = 'R$ Real';
    item.items[0].vigenceDate = '10/01/2019 - 10/01/2019';
    spyOn(component['rateLevelResource'], 'getAllRateLevel').and.returnValue(of(item));
    spyOn(component['rateLevelService'], 'updateForm').and.returnValue(item.items);

    component.loadData();

    tick();

    expect(component.rateLevelSearchList).toEqual(item.items);
    expect(component.rateLevelSearchList[0].rateName).toEqual('Alta temporada B1');
    expect(component.rateLevelSearchList[0].levelName).toEqual('Level 1');
    expect(component.rateLevelSearchList[0].currency).toEqual('R$ Real');
    expect(component.rateLevelSearchList[0].vigenceDate).toEqual('10/01/2019 - 10/01/2019');
  }));

  it('should updateStatus', fakeAsync(() => {
    spyOn(component, 'showToastStatus');
    spyOn(component['rateLevelResource'], 'toogleRateLevel').and.returnValue(of(null));
    component.updateStatus(DATA[RATE_LEVEL_LIST][0]);
    tick();
    expect(component.showToastStatus).toHaveBeenCalled();
  }));

  it('should deleteCompleted', () => {
    spyOn(component['toastEmitter'], 'emitChange');
    spyOn(component, 'loadData').and.callFake(() => {} );
    component.deleteCompleted();
    expect(component['toastEmitter'].emitChange).toHaveBeenCalledWith(
      SuccessError.success,
      'variable.lbDeleteSuccessM'
    );
  });
});
