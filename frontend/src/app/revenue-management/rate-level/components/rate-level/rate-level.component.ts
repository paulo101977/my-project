import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

import {ConfigHeaderPageNew} from 'app/shared/components/header-page-new/config-header-page-new';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {DateService} from 'app/shared/services/shared/date.service';
import {SuccessError} from 'app/shared/models/success-error-enum';

import {RateLevelModalDeleteService} from '../../services/rate-level-modal-delete.service';
import {RateLevelService} from '../../services/rate-level/rate-level.service';
import {RateLevelResource} from '../../resource/rate-level.resource';
import {LevelRateItem} from '../../models/level-rate-item';

@Component({
  selector: 'app-rate-level',
  templateUrl: './rate-level.component.html',
  styleUrls: ['./rate-level.component.css']
})
export class RateLevelComponent implements OnInit {

  public configHeaderPage: ConfigHeaderPageNew;
  public columns: Array<any>;
  public itemsOption: Array<any>;

  public rateLevelSearchList: Array<LevelRateItem>;
  public rateLevelList: Array<LevelRateItem>;
  private propertyId: string;

  constructor(
    private translateService: TranslateService,
    private toastEmitter: ToasterEmitService,
    private router: Router,
    private route: ActivatedRoute,
    private rateLevelModalDeleteService: RateLevelModalDeleteService,
    private rateLevelResource: RateLevelResource,
    private dateService: DateService,
    private rateLevelService: RateLevelService
  ) { }

  ngOnInit() {
    this.setHeaderConfig();
    this.setColumnsName();
    this.loadData();
    this.propertyId = this.route.snapshot.params.property;

    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: (row) => this.gotoEdit(row) },
      { title: 'commomData.delete', callbackFunction: (row) => this.callExclude(row) },
    ];
  }

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: false,
      keepTitleButton: true,
      callBackFunction: () => this.gotoNewRateLevel()
    };
  }

  private setColumnsName(): void {
    const currencySymbol = 1;
    this.columns = [
      {
        name: this.translateService.instant('variable.nameOf.F', {
            labelName: this.translateService.instant('label.taxRate')
        }),
        prop: 'rateName',
      },
      {
        name: 'label.level',
        prop: 'levelName',
      },
      {
        name: 'label.currency',
        prop: 'currency'
      },
      {
        name: 'label.vigence',
        prop: 'vigenceDate',
      }
    ];
  }

  public gotoNewRateLevel() {
    this.router.navigate([`p/${this.propertyId}/rm/rate-level/rate`]);
  }

  public gotoEdit(row) {
    this.router.navigate([`p/${this.propertyId}/rm/rate-level/rate/${row.id}`]);
  }

  public callExclude($event) {
    this.rateLevelModalDeleteService.openModal($event);
  }

  public updateStatus($event): void {
    const { isActive, id } = <LevelRateItem>$event;

    this.rateLevelResource
      .toogleRateLevel(id).subscribe( () => {
      this.showToastStatus(isActive);
    });
  }

  public showToastStatus(modify: boolean) {
    const typeMessage = SuccessError.success;

    let message = 'variable.lbInactivatedWithSuccessM';

    if ( modify ) {
      message = 'variable.lbActivatedWithSuccessM';
    }

    this
      .toastEmitter
      .emitChange(typeMessage, this.translateService.instant(message, {
          labelName: this.translateService.instant('label.rateLevel')
        }
      ));
  }

  public rowItemClicked(row): void {
    this.gotoEdit(row);
  }

  public loadData() {
    this.rateLevelResource.getAllRateLevel().subscribe( ({items})  => {
      items = this.rateLevelService.updateForm(items);
      this.rateLevelSearchList = items;
      this.rateLevelList = items;
    });
  }

  public search(item: any) {
    const { value } = item.target;
      this.rateLevelSearchList = this
        .rateLevelService
        .searchByNameLevelVigenceRate(this.rateLevelList, value);
  }

  public deleteCompleted() {
    this
      .toastEmitter
      .emitChange(SuccessError.success,
        this.translateService.instant('variable.lbDeleteSuccessM', {
      labelName: this.translateService.instant('label.rateLevel')
    })
      );

    this.rateLevelModalDeleteService.closeModal();
    this.loadData();
  }
}
