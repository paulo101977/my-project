import { createServiceStub } from '../../../../../../testing';
import { RateLevelResource } from 'app/revenue-management/rate-level/resource/rate-level.resource';
import { of } from 'rxjs';
import { LevelRate } from 'app/revenue-management/rate-level/models/level-rate';

export const rateLevelResourceStub = createServiceStub(RateLevelResource, {
    createRateLevel: (data: LevelRate) => of( null ),

    deleteRateLevel: (id: string) => of( null ),

    editLevelRate: (id: string, item: LevelRate) => of( null ),

    getAllLevelRatesAvailable: (initialDate: string, endDate: string, currencyId?: string) => of( null ),

    getAllRateLevel: () => of( null ),

    getAllLevelsAvailable: (initialDate: string, endDate: string, currencyId: string) => of( null ),


    getById: (id: string) => of( null ),

    toogleRateLevel: (id: string) => of( null ),
});
