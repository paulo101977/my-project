import { TestBed } from '@angular/core/testing';
import { ThexApiTestingModule, ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { RateLevelResource } from './rate-level.resource';
import { DATA, RATE_LEVEL_HEADER } from '../mock-data/index';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RateLevelResourceService', () => {
  let service: RateLevelResource;
  let thexApiService: ThexApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RateLevelResource
      ],
      imports: [
        ThexApiTestingModule
        ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    service = TestBed.get(RateLevelResource);
    thexApiService = TestBed.get(ThexApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getAllRateLevel', () => {
    it('should call ThexApiService#get', () => {
      spyOn(thexApiService, 'get');

      service.getAllRateLevel();
      expect(thexApiService.get).toHaveBeenCalledWith(service['url']);
    });
  });

  describe('#deleteRateLevel', () => {
    it('should call ThexApiService#delete', () => {
      const id = '1234';
      spyOn(thexApiService, 'delete');

      service.deleteRateLevel(id);
      expect(thexApiService.delete).toHaveBeenCalledWith(`${service['url']}/${id}`);
    });
  });

  describe('#getById', () => {
    it('should call ThexApiService#get', () => {
      const id = '1234';
      spyOn(thexApiService, 'get');

      service.getById(id);
      expect(thexApiService.get).toHaveBeenCalledWith(`${service['url']}/${id}`);
    });
  });

  describe('#createRateLevel', () => {
    it('should call ThexApiService#post', () => {
      spyOn(thexApiService, 'post');

      service.createRateLevel(DATA[RATE_LEVEL_HEADER]);
      expect(thexApiService.post).toHaveBeenCalledWith(`${service['url']}`, DATA[RATE_LEVEL_HEADER]);
    });
  });

  describe('#editLevelRate', () => {
    it('should call ThexApiService#put', () => {
      const id = '12340000-0000-0000-0000-000000000000';

      spyOn(thexApiService, 'put');

      service.editLevelRate(id, DATA[RATE_LEVEL_HEADER]);
      expect(thexApiService.put).toHaveBeenCalledWith(`${service['url']}/${id}`, DATA[RATE_LEVEL_HEADER]);
    });
  });

  describe('#toogleRateLevel', () => {
    it('should call ThexApiService#patch', () => {
      const id = '1234';
      spyOn(thexApiService, 'patch');

      service.toogleRateLevel(id);
      expect(thexApiService.patch).toHaveBeenCalledWith(`${service['url']}/toggleisactive/${id}`);
    });
  });

  describe('#getAllLevelsAvailable', () => {
    it('should call ThexApiService#get', () => {
      const mock = {
        initialDate: '1212',
        endDate: '1212',
        currencyId: '1212'
      };
      spyOn(thexApiService, 'get');

      service.getAllLevelsAvailable(mock.initialDate, mock.endDate, mock.currencyId);
      expect(thexApiService.get).toHaveBeenCalledWith(`${service['url']}/getalllevelsavailable`, { params: mock });
    });
  });
});
