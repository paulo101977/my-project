import { Injectable } from '@angular/core';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { LevelRate } from 'app/revenue-management/rate-level/models/level-rate';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class RateLevelResource {

  private url = 'LevelRate';

  constructor (private thexApiService: ThexApiService) { }

  public getAllRateLevel(): Observable<any> {
    return this.thexApiService.get(this.url);
  }

  public deleteRateLevel(id: string):
    Observable<any> {
    return this
      .thexApiService
      .delete<any>(`${this.url}/${id}`);
  }

  public getById(id: string):
    Observable<any> {
    return this
      .thexApiService
      .get(`${this.url}/${id}`);
  }

  public createRateLevel(data: LevelRate):
    Observable<any> {
    return this
      .thexApiService
      .post(`${this.url}`, data);
  }

  public editLevelRate( id: string, item: LevelRate ): Observable<any> {
    return this
      .thexApiService
      .put(`${this.url}/${id}`, item);
  }

  public toogleRateLevel(id: string):
    Observable<any> {
    return this
      .thexApiService
      .patch(`${this.url}/toggleisactive/${id}`);
  }

  public getAllLevelsAvailable(initialDate: string, endDate: string, currencyId: string) {
    const params = {
      initialDate,
      endDate,
      currencyId
    };

    return this
      .thexApiService
      .get(`${this.url}/getalllevelsavailable`, { params });
  }

  public getAllLevelRatesAvailable(initialDate: string, endDate: string, currencyId?: string) {
    const params = {
      initialDate,
      endDate,
      currencyId
    };

    return this
      .thexApiService
      .get(`${this.url}/getalllevelratesavailable`, { params });
  }
}
