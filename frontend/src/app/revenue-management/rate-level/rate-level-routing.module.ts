import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RateLevelComponent} from './components/rate-level/rate-level.component';
import {RateLevelNewEditComponent} from './components/rate-level-new-edit/rate-level-new-edit.component';

export const propertyBaseRoutes: Routes = [
  { path: '', component: RateLevelComponent },
  { path: 'rate', component: RateLevelNewEditComponent }, // add
  { path: 'rate/:rateId', component: RateLevelNewEditComponent }, // edit
];

@NgModule({
  imports: [RouterModule.forChild(propertyBaseRoutes)],
  exports: [RouterModule],
})
export class RateLevelRoutingModule {}
