import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MealPlanTypeManageComponent } from './components-containers/meal-plan-type-manage/meal-plan-type-manage.component';

export const mealPlanTypeRoutes: Routes = [{ path: '', component: MealPlanTypeManageComponent }];

@NgModule({
  imports: [RouterModule.forChild(mealPlanTypeRoutes)],
  exports: [RouterModule],
})
export class MealPlanTypeRoutingModule {}
