import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { MealPlanTypeManageComponent } from './meal-plan-type-manage.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of as observableOf } from 'rxjs';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';
import { ThexApiModule, configureApi, ApiEnvironment } from '@inovacaocmnet/thx-bifrost';

describe('MealPlanTypeManageComponent', () => {
  let component: MealPlanTypeManageComponent;
  let fixture: ComponentFixture<MealPlanTypeManageComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed
      .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: [
          TranslateModule.forRoot(),
          BrowserDynamicTestingModule,
          ReactiveFormsModule,
          FormsModule,
          HttpClientTestingModule,
          RouterTestingModule,
          ThexApiModule
        ],
        declarations: [MealPlanTypeManageComponent],
        providers: [ InterceptorHandlerService,
          configureApi({
            environment: ApiEnvironment.Development
          })
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      });

    fixture = TestBed.createComponent(MealPlanTypeManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('onInit', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should call resource change status and toaster message', () => {
      spyOn<any>(component['mealPlanTypeResource'], 'toggleActivation').and.returnValue(observableOf({}));
      spyOn(component['toasterEmitService'], 'emitChange');

      component.changeStatus('99999999999999999999999');

      expect(component['mealPlanTypeResource'].toggleActivation).toHaveBeenCalledWith('99999999999999999999999');
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        component['translateService'].instant('variable.lbEditSuccessM', {
          labelName: component['translateService'].instant('label.mealPlanType')
        })
      );
    });
  });

});
