import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MealPlanType } from '../../../../shared/models/revenue-management/meal-plan-type';
import { ConfigHeaderPageNew } from '../../../../shared/components/header-page-new/config-header-page-new';
import { SuccessError } from '../../../../shared/models/success-error-enum';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../../shared/models/button-config';
import { ToasterEmitService } from '../../../../shared/services/shared/toaster-emit.service';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { MealPlanTypeResource } from '../../../../shared/resources/meal-plan-type/meal-plan-type.resource';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-meal-plan-type-manage',
  styleUrls: ['./meal-plan-type-manage.component.css'],
  templateUrl: './meal-plan-type-manage.component.html',
})
export class MealPlanTypeManageComponent implements OnInit {
  private propertyId: number;
  public mealPlanTypeList: Array<MealPlanType>;
  public configHeaderPage: ConfigHeaderPageNew;

  // Buttons
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;

  constructor(
    private mealPlanTypeResource: MealPlanTypeResource,
    private toasterEmitService: ToasterEmitService,
    private sharedService: SharedService,
    private route: ActivatedRoute,
    private location: Location,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setMealPlanTypeList();
    this.setConfigHeaderPage();
    this.setButtonConfig();
  }

  private setMealPlanTypeList(): void {
    this.mealPlanTypeList = [];
    this.mealPlanTypeResource.getAllMealPlanTypeList(this.propertyId).subscribe(
      response => {
        this.mealPlanTypeList = [...response.items];
      },
      error => {
        this.toasterEmitService.emitChange(SuccessError.error, 'text.errorMessage');
      },
    );
  }

  private setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  private setButtonConfig(): void {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      this.goBackPreviewPage,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'confirm',
      this.updateMealPlanTypeList,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
    );
  }

  private goBackPreviewPage = () => {
    this.location.back();
  }

  private updateMealPlanTypeList = () => {
    for (let i = 0; i < this.mealPlanTypeList.length; i++) {
      if (!this.mealPlanTypeList[i].propertyMealPlanTypeName || !this.mealPlanTypeList[i].propertyMealPlanTypeCode) {
        this.toasterEmitService.emitChange(SuccessError.error, 'text.completeAllFields');
        return;
      } else if (
        this.sharedService.isEmptyOrWhiteSpace(this.mealPlanTypeList[i].propertyMealPlanTypeName) ||
        this.sharedService.isEmptyOrWhiteSpace(this.mealPlanTypeList[i].propertyMealPlanTypeCode)
      ) {
        this.toasterEmitService.emitChange(SuccessError.error, 'text.completeAllFields');
        return;
      }
    }
    this.mealPlanTypeResource.updateMealPlanType(this.mealPlanTypeList).subscribe(response => {
      this.toasterEmitService.emitChange(SuccessError.success, 'variable.saveSuccessM');
    });
  }

  public changeStatus(id: string) {
    this.mealPlanTypeResource.toggleActivation(id)
      .subscribe(() => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbEditSuccessM', {
            labelName: this.translateService.instant('label.mealPlanType')
          })
        );
      });
  }
}
