import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './../../shared/shared.module';
import { MealPlanTypeManageComponent } from './components-containers/meal-plan-type-manage/meal-plan-type-manage.component';
import { MealPlanTypeRoutingModule } from './meal-plan-type-routing.module';

@NgModule({
  imports: [CommonModule, SharedModule, FormsModule, MealPlanTypeRoutingModule],
  declarations: [MealPlanTypeManageComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MealPlanTypeModule {}
