import { Injectable } from '@angular/core';
import { BaseRateConfigurationMealPlanType } from 'app/shared/models/revenue-management/meal-type-fix-rate';
import { MealPlanTypeEnum } from 'app/shared/models/revenue-management/meal-plan-type.enum';

@Injectable({
  providedIn: 'root'
})
export class MealPlanTypeService {

  constructor() {
  }

  public setMealPlanTypeDefault(item: BaseRateConfigurationMealPlanType)
    : BaseRateConfigurationMealPlanType {
    if (item) {
      item.isMealPlanTypeDefault = true;
      return item;
    }
    return null;
  }

  public resetMealPlanTypeDefaultList(list: BaseRateConfigurationMealPlanType[])
    : BaseRateConfigurationMealPlanType[] {
    if (list) {
      return list.map(item => {
        item.isMealPlanTypeDefault = false;
        return item;
      });
    }
    return [];
  }

  public uptateMealPlanTypeDefault(list: BaseRateConfigurationMealPlanType[], item: BaseRateConfigurationMealPlanType)
  : BaseRateConfigurationMealPlanType[] {
    const newList = this.resetMealPlanTypeDefaultList(list);
    if (newList) {
      return newList.map(mealPlanType => {
        if (mealPlanType.mealPlanTypeId == item.mealPlanTypeId) {
          mealPlanType = this.setMealPlanTypeDefault(item);
        }
        return mealPlanType;
      });
    }
    return [];
  }

  public setConfigMealPlanTypeList(list: BaseRateConfigurationMealPlanType[])
    : BaseRateConfigurationMealPlanType[] {
    if (list) {
      return list
        .filter(baseRateMealType => {
          return baseRateMealType.mealPlanTypeId != MealPlanTypeEnum.None;
        });
    }
    return [];
  }

  public mapperValidMealTypeList(list: BaseRateConfigurationMealPlanType[]): BaseRateConfigurationMealPlanType[] {
    if (list) {
      return list.filter(ml => {
        if (ml.adultMealPlanAmount != null) {
          return ml;
        }
      });
    }
    return [];
  }

}
