import { TestBed, inject } from '@angular/core/testing';

import { MealPlanTypeService } from './meal-plan-type.service';

describe('MealPlanTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MealPlanTypeService]
    });
  });

  it('should be created', inject([MealPlanTypeService], (service: MealPlanTypeService) => {
    expect(service).toBeTruthy();
  }));
});
