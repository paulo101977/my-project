export interface Overview {
    propertyName: string;
    chainName: string;
    totalCheckIn: number;
    totalCheckOut: number;
    totalWalkIn: number;
    totalDayUse: number;
    occupationPercentage: number;
    totalOccupiedRoom: number;
    totalAvailableRoom: number;
}

export interface CheckInList {
    guestReservationItemId: number;
    guestName: string;
    reservationItemId: number;
    reservationItemCode: string;
    roomType: string;
    guestStatusId: number;
    isMigrated: boolean;
    isGroupReservation: boolean;
}

export interface CheckOutList {
    guestReservationItemId: number;
    guestName: string;
    guestDocumentTypeName: string;
    guestDocument: string;
    billingAccountId: string;
    reservationItemId: number;
    reservationItemCode: string;
    roomType: string;
    guestStatusId: number;
    isMigrated: boolean;
    isGroupReservation: boolean;
}

export interface DashboardItem {
    overview: Overview;
    checkInList: CheckInList[];
    checkOutList: CheckOutList[];
}
