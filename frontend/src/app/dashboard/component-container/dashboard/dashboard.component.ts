import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { GovernanceRoomBlockComponent } from 'app/governance/components/governance-room-block/governance-room-block.component';
import { ConfigHeaderPage } from 'app/shared/models/config-header-page';
import { ButtonBarConfig } from 'app/shared/components/button-bar/button-bar-config';
import { AvailabilityGridPeriodEnum } from 'app/shared/models/availability-grid/availability-grid-period.enum';
import { DateService } from 'app/shared/services/shared/date.service';
import { AvailabilityGridResource } from 'app/shared/resources/availability-grid/availability-grid.resource';
import { RoomBlock } from 'app/shared/models/dto/housekeeping/room-block';
import { RoomBlockResource } from 'app/shared/resources/room-block/room-block.resource';
import {
  Availability,
  AvailabilityGridLabels,
  Footer,
  Reservation,
  Room,
  RoomType,
  ThxAvailabilityGridComponent
} from '@inovacao-cmnet/thx-ui';

import { ReservationResource } from 'app/shared/resources/reservation/reservation.resource';
import { SearchReservationDto } from 'app/shared/models/dto/search-reservation-dto';
import { ReservationSearchDto } from 'app/shared/models/dto/reservation-search-dto';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';
import { PropertyStorageService, APP_NAME } from '@inovacaocmnet/thx-bifrost';
import { ReportsResource } from 'app/interactive-report/resources/reports.resource';
import { CheckInList, CheckOutList } from '@app/dashboard/model/dashboard-item';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @ViewChild('grid') public availabilityGridComponent: ThxAvailabilityGridComponent;

  @ViewChild(GovernanceRoomBlockComponent) public governanceRoomBlock: GovernanceRoomBlockComponent;

  // Filter Items
  public propertyId: number;
  public configHeaderPage: ConfigHeaderPage;
  public availabilityGridFilterForm: FormGroup;
  public buttonBarConfig: ButtonBarConfig;

  // Reservations
  public searchReservationDto: SearchReservationDto;
  public reservationList: Reservation[] = [];
  public reservationItemsList: Array<ReservationSearchDto>;
  public totalGuests: number;
  public totalReservations: number;

  public isFindingCheckin: boolean;
  public isFindingCheckout: boolean;

  // Grid Items
  public columnsCheckin: Array<any>;
  public columnsCheckout: Array<any>;
  public roomTypeList: RoomType[];
  public blockedRoomList: RoomBlock[];
  public availabilityList: Availability[];
  public footerList: Footer[];
  public reservationCheckinList: CheckInList[] = [];
  public reservationCheckoutList: CheckOutList[] = [];
  public reservationCheckinListBack: CheckInList[] = [];
  public reservationCheckoutListBack: CheckOutList[] = [];
  public roomList: Room[];

  public labels: AvailabilityGridLabels;
  public totals: any = {};
  public chainName: string;
  public propertyName: string;

  public startDate: string;
  public days: number;
  public expandedRoomType: number;
  public validateReservationMove: Function;


  constructor(
    @Inject(APP_NAME) private appName,
    public route: ActivatedRoute,
    public router: Router,
    public formBuilder: FormBuilder,
    public dateService: DateService,
    public translateService: TranslateService,
    public gridResource: AvailabilityGridResource,
    public roomBlockResource: RoomBlockResource,
    private reservationResource: ReservationResource,
    private reservationService: ReservationService,
    private propertyManager: PropertyStorageService,
    private reportResource: ReportsResource
  ) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setVars();
    this.loadDashboard();
  }

  private setVars() {
    const property = this.propertyManager.getProperty(this.appName, this.propertyId);
    this.setAvailabilityGridFilter();
    this.setLabels();
    this.setGridReservation();

    this.searchReservationDto = new SearchReservationDto();

    this.totals = {
      'checkin': 0,
      'checkout': 0,
      'walkin': 0,
      'dayuse': 0,
      'pendings': 0,
      'busy': 0,
      'avaiables': 0,
      'percent': 0,
    };
  }

  private setGridReservation() {
    this.columnsCheckin = [];
    this.columnsCheckout = [];

    this.translateService.get('label.name').subscribe(value => {
      this.columnsCheckin.push({ name: value, prop: 'guestName' });
      this.columnsCheckout.push({ name: value, prop: 'guestName' });
    });

    this.translateService.get('label.type').subscribe(value => {
      this.columnsCheckin.push({ name: value, prop: 'roomType' });
    });

    this.translateService.get('commomData.document').subscribe(value => {
      this.columnsCheckout.push({ name: value, prop: 'guestDocument' });
    });

    this.translateService.get('label.reservationSingle').subscribe(value => {
      this.columnsCheckin.push({ name: value, prop: 'reservationItemCode' });
      this.columnsCheckout.push({ name: value, prop: 'reservationItemCode' });
    });
  }

  private setLabels() {
    const labels = [
      'propertyModule.availabilityGrid.labels.overbook',
      'propertyModule.availabilityGrid.labels.legend',
      'propertyModule.availabilityGrid.labels.checkin',
      'propertyModule.availabilityGrid.labels.checkout',
      'propertyModule.availabilityGrid.labels.toConfirm',
      'propertyModule.availabilityGrid.labels.confirmed',
      'propertyModule.availabilityGrid.labels.showToday',
      'propertyModule.availabilityGrid.labels.reservationCode',
      'propertyModule.availabilityGrid.labels.roomType',
      'propertyModule.availabilityGrid.labels.status',
      'propertyModule.availabilityGrid.labels.period',
      'propertyModule.availabilityGrid.labels.groupName',
      'propertyModule.availabilityGrid.labels.reservationDetails',
      'propertyModule.availabilityGrid.labels.newReservation',
      'propertyModule.availabilityGrid.labels.cornerLabel',
      'propertyModule.availabilityGrid.labels.rateProposal'
    ];

    this.translateService.get(labels).subscribe(data => {
      this.labels = {
        overbook: data['propertyModule.availabilityGrid.labels.overbook'],
        legend: data['propertyModule.availabilityGrid.labels.legend'],
        checkin: data['propertyModule.availabilityGrid.labels.checkin'],
        checkout: data['propertyModule.availabilityGrid.labels.checkout'],
        toConfirm: data['propertyModule.availabilityGrid.labels.toConfirm'],
        confirmed: data['propertyModule.availabilityGrid.labels.confirmed'],
        showToday: data['propertyModule.availabilityGrid.labels.showToday'],
        reservationCode: data['propertyModule.availabilityGrid.labels.reservationCode'],
        roomType: data['propertyModule.availabilityGrid.labels.roomType'],
        status: data['propertyModule.availabilityGrid.labels.status'],
        period: data['propertyModule.availabilityGrid.labels.period'],
        groupName: data['propertyModule.availabilityGrid.labels.groupName'],
        reservationDetails: data['propertyModule.availabilityGrid.labels.reservationDetails'],
        newReservation: 'Reservar período',
        cornerLabel: data['propertyModule.availabilityGrid.labels.cornerLabel'],
        unlockRoom: 'Desbloquear UH',
        blockRoom: 'Bloquear UH',
        reservationRateProposal: data['propertyModule.availabilityGrid.labels.rateProposal'],
      };
    });
  }

  private setAvailabilityGridFilter() {
    this.filter(AvailabilityGridPeriodEnum.weekly);
  }

  private loadDashboard() {
    this.reportResource.getDashBoardItems()
        .subscribe(item => {
          this.reservationCheckinList = item.checkInList;
          this.reservationCheckinListBack = item.checkInList;

          this.reservationCheckoutList = item.checkOutList;
          this.reservationCheckoutListBack = item.checkOutList;

          this.chainName = item.overview.chainName;
          this.propertyName = item.overview.propertyName;

          this.totals = {
            'checkin': item.overview.totalCheckIn,
            'checkout': item.overview.totalCheckOut,
            'walkin': item.overview.totalWalkIn,
            'dayuse': item.overview.totalDayUse,
            'busy': item.overview.totalOccupiedRoom,
            'avaiables': item.overview.totalAvailableRoom,
            'percent': item.overview.occupationPercentage,
          };
        });
  }

  public search(isCheckin: boolean, term: string): void {
    const regex = new RegExp(term, 'i');

    if (isCheckin) {
      this.reservationCheckinList = this.reservationCheckinListBack.filter(
        item =>
          regex.test(item.guestName)
          || regex.test(item.roomType)
          || regex.test(item.reservationItemCode)
      );

      this.isFindingCheckin = (term.length > 0);
    } else {
      this.reservationCheckoutList = this.reservationCheckoutListBack.filter(
        item =>
          regex.test(item.guestName)
          || regex.test(item.guestDocument)
          || regex.test(item.reservationItemCode)
      );

      this.isFindingCheckout = (term.length > 0);
    }
  }

  public goToCheckinPage(row) {
    this.reservationService.goToEditCheckinPage(row.reservationItemId, row.reservationItemCode, `${this.propertyId}`);
  }

  public goToBillingAccount(row) {
    this.reservationService.goToBillingAccount(row.billingAccountId, `${this.propertyId}`);
  }

  public filter(period: number) {
    const initialDate = new Date();
    this.startDate = moment(initialDate)
      .format(DateService.DATE_FORMAT_UNIVERSAL)
      .toString();
    this.days = period;

    this.getRoomTypes(this.propertyId, this.startDate, this.days);
  }

  public getRoomTypes(propertyId: number, startDate: string, days: number) {
    this.gridResource.getRoomTypes(propertyId, startDate, days).subscribe(data => {
      this.roomTypeList = data.roomTypeList;

      this.availabilityList = data.availabilityList;
      this.footerList = data.footerList;
      this.blockedRoomList = data.roomsBlocked;
    });
  }

  public onRoomBlocked() {
    this.getRoomTypes(this.propertyId, this.startDate, this.days);
  }

}
