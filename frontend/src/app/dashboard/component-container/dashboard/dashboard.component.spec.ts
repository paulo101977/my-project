import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { AvailabilityGridResource } from '@app/shared/resources/availability-grid/availability-grid.resource';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateTestingModule } from '@app/shared/mock-test/translate-testing.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ThexApiModule, ApiEnvironment, configureApi, configureAppName } from '@inovacaocmnet/thx-bifrost';
import { FormBuilder } from '@angular/forms';
import { amDateFormatPipeStub } from 'testing';
import { NO_ERRORS_SCHEMA, Component } from '@angular/core';
import { AvailabilityGridPeriodEnum } from '@app/shared/models/availability-grid/availability-grid-period.enum';
import {
  AvailabilityResourcesDtoData,
  AvailabilityRoomTypeDtoData,
  AvailabilityDtoData,
  AvailabilityFooterDtoData,
} from '@app/shared/mock-data/availability-grid-data';
import { Observable } from 'rxjs';
import { configureTestSuite } from 'ng-bullet';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let resource: AvailabilityGridResource;
  let httpMock: HttpTestingController;

  @Component({ selector: 'thx-availability-grid', template: '' })
  class ThxAvailabilityGridStubComponent {
    setRooms(roomTypeId, roomList) {
    }
  }

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        imports: [
          TranslateTestingModule,
          RouterTestingModule,
          HttpClientTestingModule,
          ThexApiModule
        ],
        providers: [
          FormBuilder,
          configureApi({
            environment: ApiEnvironment.Development
          }),
          configureAppName('pms')
        ],
        declarations: [
          DashboardComponent,
          ThxAvailabilityGridStubComponent,
          amDateFormatPipeStub
        ],
        schemas: [NO_ERRORS_SCHEMA],
      });

    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    resource = TestBed.get(AvailabilityGridResource);
    httpMock = TestBed.get(HttpTestingController);

    fixture.detectChanges();
  });

  describe('on init', () => {
    it(
      'should create component with settings initials',
      () => {
        expect(component).toBeTruthy();
      }
    );
  });

  it('should execute filter', () => {
    const period = AvailabilityGridPeriodEnum.weekly;

    component.filter(period);
    expect(component.days).toEqual(period);
  });

  it('should get roomTypes', done => {
    const spy = spyOn(component['gridResource'], 'getRoomTypes').and.returnValue(
      new Observable(observer => {
        setTimeout(() => {
          observer.next(AvailabilityResourcesDtoData);
        }, 200);
      }),
    );

    component.getRoomTypes(1, '2017-12-10', 7);

    spy.calls.mostRecent().returnValue.subscribe(() => {
      expect(component.roomTypeList).toEqual([AvailabilityRoomTypeDtoData]);
      expect(component.availabilityList).toEqual([AvailabilityDtoData]);
      expect(component.footerList).toEqual([AvailabilityFooterDtoData]);
      done();
    });
  });
});
