import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './component-container/dashboard/dashboard.component';
import { GovernanceModule } from '@app/governance/governance.module';
import { ThxAvailabilityGridModule, ThxImgModule, ThxProgressBarV2Module } from '@inovacao-cmnet/thx-ui';
import { AvailabilityChartGridModule } from '@app/availability/avaialability-chart-grid/availability-chart-grid.module';
import { SharedModule } from '@app/shared/shared.module';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    GovernanceModule,
    ThxAvailabilityGridModule,
    AvailabilityChartGridModule,
    ThxImgModule,
    ThxProgressBarV2Module
  ]
})
export class DashboardModule { }
