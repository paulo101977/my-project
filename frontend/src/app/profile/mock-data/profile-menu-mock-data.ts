import { MenuItems, Items } from 'app/profile/model/menuItems';

export const MENU_LIST_DATA_1 = new MenuItems();
MENU_LIST_DATA_1.title = 'title.userProfileManagement';
MENU_LIST_DATA_1.imgSrc = '';
MENU_LIST_DATA_1.items =  <Items[]>[
  {
    title: 'title.userInformations',
    iconClass: 'icon-user',
    routerLink: 'info'
  },
  {
    title: 'title.profilePermissions',
    iconClass: 'icon-document-filled',
    routerLink: 'permission'
  },
  {
    title: 'title.changePassword',
    iconClass: 'icon-lock',
    routerLink: 'password'
  }
];

export const MENU_LIST_DATA: MenuItems [] = [
  MENU_LIST_DATA_1
];
