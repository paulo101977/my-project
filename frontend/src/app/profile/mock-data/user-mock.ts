import { Brand } from 'app/shared/models/brand';
import { Company } from 'app/shared/models/company';
import { Location } from 'app/shared/models/location';
import { Contact } from 'app/shared/models/contact';
import { DocumentForLegalAndNaturalPerson } from 'app/shared/models/document';
import { PropertyStatusEnum } from 'app/shared/models/property-status-enum';

export class UserMock {

  public userData = {
      id: 'abee3fae-ff86-4eb3-a333-3e757dd01f53',
      userUid: '1',
      personId: '1286f3d0-432c-4de0-9a83-d6bcbe7f7722',
      tenantId: '1',
      userLogin: 'thex@totvscmnet.com.br',
      preferredLanguage: 'pt-br',
      preferredCulture: 'pt-br',
      isActive: true,
      isAdmin:  true,
      name: 'Thex Bematech',
      email: 'thex@totvscmnet.com.br',
      lastLink: '',
      roleIdList: null,
      phoneNumber: '123',
      dateOfBirth: '',
      nickName: '',
      photoBase64: '',
      photoUrl: ''
    };

  public userPasswordForm = {
    currentPassword: '1',
    newPassword: '1',
    confirmPassword: '1',
  };

  public property = {
    brand: null,
    brandId: 1,
    chainId: 0,
    company: null,
    id: 1,
    locationList: null,
    name: 'Hotel Totvs Resorts Rio de Janeiro',
    propertyStatusId: 19,
    propertyType: 1,
    propertyUId: '6146da81-9cc3-46ef-b855-825551594bfe',
    reservationList: null,
    roomList: null,
    roomTypeList: null,
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    totalOfRooms: 129,
    contactList: null,
    documentList: null,
    cnpj: '',
    inscEst: '',
    inscMun: '',
    contactPhone: '',
  contactWebSite: '',
  shortName: '',
  tradeName: ''
  };
}
