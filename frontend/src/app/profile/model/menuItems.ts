export class MenuItems {
  title: string;
  imgSrc: string;
  items: Items[];
}
export interface Items {
  title?: string;
  iconClass?: string;
  routerLink?: string;
}

