import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordComponent } from 'app/profile/components/change-password/change-password.component';
import { ProfilePermissionsComponent } from 'app/profile/components/profile-permissions/profile-permissions.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { GetInitialsNamePipe } from '../../app/user/pipes/get-initials-name.pipe';
import { ThxImgModule, ThxPageTitleModule, ThxSidebardCardContainerModule } from '@inovacao-cmnet/thx-ui';
import { UserPhotoUploadComponent } from 'app/profile/components/user-photo-upload/user-photo-upload.component';
import { UserProfileListComponent } from 'app/profile/components/user-profile-list/user-profile-list.component';
import { UserCardComponent } from 'app/profile/components/user-card/user-card.component';
import { UserProfileManagementComponent } from 'app/profile/components/user-profile-management/user-profile-management.component';
import { UserInfoComponent } from 'app/profile/components/user-info/user-info.component';
import { ProfileIconPipe } from './shared/pipes/profile-icon.pipe';
import { ProfileSelectorComponent } from './shared/components/profile-selector/profile-selector.component';
import { ProfileAcronymPipe } from './shared/pipes/profile-acronym.pipe';

@NgModule({
  declarations: [
    UserProfileManagementComponent,
    UserCardComponent,
    UserProfileListComponent,
    UserPhotoUploadComponent,
    GetInitialsNamePipe,
    ChangePasswordComponent,
    UserInfoComponent,
    ProfilePermissionsComponent,
    ProfileIconPipe,
    ProfileSelectorComponent,
    ProfileAcronymPipe,
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    TranslateModule.forChild(),
    SharedModule,
    RouterModule,
    ThxImgModule,
    ThxPageTitleModule,
    ThxSidebardCardContainerModule,
  ],
  exports: [
    UserCardComponent,
    UserProfileListComponent,
    UserPhotoUploadComponent,
    GetInitialsNamePipe,
    ProfileIconPipe,
    ProfileSelectorComponent,
    ProfileAcronymPipe,
  ]
})
export class ProfileModule { }
