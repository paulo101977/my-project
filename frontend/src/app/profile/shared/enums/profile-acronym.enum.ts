export enum ProfileAcronymEnum {
  'CFA00683-9308-448C-9BED-D2FF29259AD5' = 'profile.admin.acronym',
  '113EBD17-3B6E-4A2E-A3D0-444C276DB57C' = 'profile.roomMaid.acronym',
  '84B76BD5-6E78-4A64-9AFC-D972C9488850' = 'profile.employee.acronym'
}
