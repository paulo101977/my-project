import { TestBed } from '@angular/core/testing';
import { AdminApiTestingModule } from '@inovacaocmnet/thx-bifrost';

import { ProfileResourceService } from './profile-resource.service';

describe('ProfileResourceService', () => {
  let service: ProfileResourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AdminApiTestingModule]
    });
    service = TestBed.get(ProfileResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call AdminApiService#get', () => {
      spyOn<any>(service['api'], 'get');

      service.list();
      expect(service['api'].get).toHaveBeenCalled();
    });
  });
});
