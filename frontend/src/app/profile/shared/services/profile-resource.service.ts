import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: 'root'
})
export class ProfileResourceService {

  constructor(
    private api: AdminApiService
  ) { }

  public list(params?: any) {
    return this.api.get('Role', { params });
  }
}
