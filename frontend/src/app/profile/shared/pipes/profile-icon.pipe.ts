import { Pipe, PipeTransform } from '@angular/core';
import { PermissionsEnum } from 'app/profile/components/profile-permissions/permissionsEnum';

@Pipe({
  name: 'profileIcon'
})
export class ProfileIconPipe implements PipeTransform {
  private permissionsEnum = PermissionsEnum;

  transform(permissionId: string): string {
    if (permissionId == null) {
      return 'icon-perfil-default.svg';
    }

    return this.permissionsEnum[permissionId.toUpperCase()] || 'icon-perfil-default.svg';
  }

}
