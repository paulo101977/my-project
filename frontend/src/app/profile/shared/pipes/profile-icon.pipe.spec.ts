import { ProfileIconPipe } from './profile-icon.pipe';

describe('ProfileIconPipe', () => {
  it('create an instance', () => {
    const pipe = new ProfileIconPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return a default profile icon if no profile is passed', () => {
    const pipe = new ProfileIconPipe();
    const image = pipe.transform(null);

    expect(image).toBe('icon-perfil-default.svg');
  });

  it('should return a default profile icon if the profile don\'t match an image', () => {
    const pipe = new ProfileIconPipe();
    const image = pipe.transform('doesnt-exist');

    expect(image).toBe('icon-perfil-default.svg');
  });

  it('should return the profile icon', () => {
    const pipe = new ProfileIconPipe();
    const image = pipe.transform('CFA00683-9308-448C-9BED-D2FF29259AD5');

    expect(image).toBe('admin-icon.svg');
  });
});
