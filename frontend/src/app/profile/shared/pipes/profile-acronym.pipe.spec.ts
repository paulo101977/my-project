import { ProfileAcronymPipe } from './profile-acronym.pipe';

describe('ProfileAcronymPipe', () => {
  it('create an instance', () => {
    const pipe = new ProfileAcronymPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return a default profile acronym  if no profile is passed', () => {
    const pipe = new ProfileAcronymPipe();
    const image = pipe.transform(null);

    expect(image).toBe('profile.undefined.acronym');
  });

  it('should return a default profile acronym if the profile don\'t match an ID', () => {
    const pipe = new ProfileAcronymPipe();
    const image = pipe.transform('doesnt-exist');

    expect(image).toBe('profile.undefined.acronym');
  });

  it('should return the profile acronym', () => {
    const pipe = new ProfileAcronymPipe();
    const image = pipe.transform('CFA00683-9308-448C-9BED-D2FF29259AD5');

    expect(image).toBe('profile.admin.acronym');
  });
});
