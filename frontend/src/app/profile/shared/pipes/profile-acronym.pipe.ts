import { Pipe, PipeTransform } from '@angular/core';
import { ProfileAcronymEnum } from 'app/profile/shared/enums/profile-acronym.enum';

@Pipe({
  name: 'profileAcronym'
})
export class ProfileAcronymPipe implements PipeTransform {

  transform(profileId: string): string {
    if (profileId == null) {
      return 'profile.undefined.acronym';
    }

    return ProfileAcronymEnum[profileId.toUpperCase()] || 'profile.undefined.acronym';
  }

}
