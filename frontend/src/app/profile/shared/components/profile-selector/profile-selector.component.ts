import {Component, EventEmitter, Input, Output} from '@angular/core';
import {SelectOptionItem} from '@inovacao-cmnet/thx-ui';

@Component({
  selector: 'app-profile-selector',
  templateUrl: './profile-selector.component.html',
  styleUrls: ['./profile-selector.component.scss']
})
export class ProfileSelectorComponent {
  @Input() list: Array<SelectOptionItem>;
  @Output() selectUserType = new EventEmitter();
  private selectedType: SelectOptionItem;

  clear() {
    this.selectedType = null;
  }

  selectUser(type) {
    this.selectedType = type;
    this.selectUserType.emit(type);
  }

}
