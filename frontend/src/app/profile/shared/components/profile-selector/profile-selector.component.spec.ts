import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileSelectorComponent} from './profile-selector.component';
import {ImgCdnTestingModule} from '@inovacao-cmnet/thx-ui';
import {configureTestSuite} from 'ng-bullet';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {profileIconPipeStub} from 'app/profile/testing/profile-icon.pipe.stub';

describe('ProfileSelectorComponent', () => {
  let component: ProfileSelectorComponent;
  let fixture: ComponentFixture<ProfileSelectorComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProfileSelectorComponent,
        profileIconPipeStub
      ],
      imports: [ ImgCdnTestingModule ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    fixture = TestBed.createComponent(ProfileSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
