import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangePasswordComponent } from 'app/profile/components/change-password/change-password.component';
import { ProfilePermissionsComponent } from 'app/profile/components/profile-permissions/profile-permissions.component';
import { UserInfoComponent } from 'app/profile/components/user-info/user-info.component';
import { UserProfileManagementComponent } from 'app/profile/components/user-profile-management/user-profile-management.component';

export const profileRoutes: Routes = [
  { path: '',
    component: UserProfileManagementComponent,
    children: [
      { path: 'info', component: UserInfoComponent},
      { path: 'permission', component: ProfilePermissionsComponent},
      { path: 'password', component: ChangePasswordComponent },
  ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(profileRoutes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
