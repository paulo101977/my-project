import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { UserProfileManagementComponent } from './user-profile-management.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { imgCdnStub } from '../../../../../testing';

describe('UserProfileManagementComponent', () => {
  let component: UserProfileManagementComponent;
  let fixture: ComponentFixture<UserProfileManagementComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserProfileManagementComponent,
        imgCdnStub
      ],
      imports: [
        TranslateTestingModule
        ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(UserProfileManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
