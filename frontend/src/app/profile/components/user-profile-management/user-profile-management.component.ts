import {Component,  OnInit} from '@angular/core';
import { MENU_LIST_DATA } from 'app/profile/mock-data/profile-menu-mock-data';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'thx-user-profile-management',
  templateUrl: './user-profile-management.component.html',
  styleUrls: ['./user-profile-management.component.scss']
})
export class UserProfileManagementComponent implements OnInit {
  constructor(private translateService: TranslateService) { }
  public menuList: any;
  public title: string;

  ngOnInit() {
    this.menuList = MENU_LIST_DATA;
    this.title = this.translateService.instant('title.userProfileManagement');
  }

}
