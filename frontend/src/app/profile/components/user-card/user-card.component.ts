import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { APP_NAME, LocalUserManagerService, PropertyStorageService, User } from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { UserResource } from 'app/shared/resources/user/user.resource';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})

export class UserCardComponent implements OnInit {

  public myUser: User;
  public propertyId: number;
  public propertyName: string;
  public photoUrl = '';
  public selectedImage: string;

  @ViewChild('inputBarCode') inputBarCode: ElementRef;

  constructor(
    @Inject(APP_NAME) private appName: string,
    private userResource: UserResource,
    public propertyResource: PropertyResource,
    private propertyService: PropertyStorageService,
    private modalEmitToggleService: ModalEmitToggleService,
    private toasterEmitService: ToasterEmitService,
    public translateService: TranslateService,
    private localUserManager: LocalUserManagerService,
  ) { }

  ngOnInit(): void {
    this.setUser();
    this.propertyId =  this.propertyService.getCurrentPropertyId();
    this.setPropertyName(this.propertyId);
  }

  public setUser(): void {
    this.localUserManager.localUser$
      .subscribe(_ => {
        this.myUser = this.localUserManager.getUser(this.appName);
        this.photoUrl = this.myUser.photoUrl;
      });
  }

  public setPropertyName(property: number): void {
    this.propertyResource.getProperty(property).subscribe(data => {
      this.propertyName = data.name;
    });
  }

  public translateBarcode(imgBase64) {
    const _self = this;
    this.myUser.photoBase64 = imgBase64.substr(imgBase64.indexOf('base64,') + 7);
    this.userResource.saveUserImage(this.myUser)
      .subscribe((res) => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbEditSuccessF', {
            labelName: this.translateService.instant('label.photo'),
          })
        );
        this.photoUrl = res.photoUrl;
      });

    this.toggleModalBarcode();
  }

  public cancelModalBarcode() {
    this
      .modalEmitToggleService
      .emitToggleWithRef('user-photo-modal-upload');
  }

  public toggleModalBarcode() {
    this
      .modalEmitToggleService
      .emitToggleWithRef('user-photo-modal-upload');
  }

  public barcodeClick = () => {
    this.inputBarCode.nativeElement.click();
  }

  public imageBarCodeUpload($event) {
    const file: File = $event.target.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (loadEvent: any) => {
      this.selectedImage = loadEvent.target.result;
        this.inputBarCode.nativeElement.value = '';
        this.toggleModalBarcode();
    };

    myReader.readAsDataURL(file);
  }
}
