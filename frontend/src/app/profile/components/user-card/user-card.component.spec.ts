import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { UserCardComponent } from './user-card.component';
import { TruncatePipe } from 'app/shared/pipes/truncate.pipe';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GetInitialsNamePipe } from 'app/user/pipes/get-initials-name.pipe';
import { userResourceStub } from 'app/shared/resources/user/testing';
import { propertyResourceStub } from 'app/shared/resources/property/testing';
import { propertyServiceStub } from 'app/shared/services/property/testing';
import { LocalUserManagerServiceStub } from '@bifrost/storage/services/testing/local-user-manager-service.stub';
import { propertyStorageServiceStub } from '@bifrost/storage/services/testing/property-storage-service.stub';
import { APP_NAME } from '@inovacaocmnet/thx-bifrost';
import { of } from 'rxjs/internal/observable/of';
import { UserMock } from 'app/profile/mock-data/user-mock';


describe('UserCardComponent', () => {
  let component: UserCardComponent;
  let fixture: ComponentFixture<UserCardComponent>;
  const mockData = new UserMock();

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserCardComponent,
        TruncatePipe,
        GetInitialsNamePipe
      ],
      imports: [ TranslateTestingModule ],
      providers: [
        userResourceStub,
        propertyResourceStub,
        propertyServiceStub,
        LocalUserManagerServiceStub,
        propertyStorageServiceStub,
        { provide: APP_NAME, useValue: {} },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(UserCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn(component, 'setUser').and.callFake(() => {});
    spyOn(component, 'setPropertyName');
    spyOn(component['propertyService'], 'getCurrentPropertyId').and.returnValue(0);
    component.ngOnInit();

    expect(component.setUser).toHaveBeenCalled();
    expect(component.setPropertyName).toHaveBeenCalled();
    expect(component.propertyId).toEqual(0);
  });

  it('should setUser', () => {
    spyOn<any>(component['localUserManager'], 'localUser$').and.returnValue(of(null));
    spyOn(component['localUserManager'], 'getUser').and.returnValue(mockData.userData);

    component.setUser();

    expect(component.myUser).toEqual(mockData.userData);
    expect(component.myUser.photoUrl).toEqual(mockData.userData.photoUrl);
  });

  it('should setPropertyName', () => {
    spyOn<any>(component['propertyResource'], 'getProperty').and.returnValue(of(mockData.property));
    component.setPropertyName(1);

    expect(component.propertyName).toEqual(mockData.property.name);
  });

  it('should cancelModalBarcode', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    component.cancelModalBarcode();
    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('user-photo-modal-upload');
  });

  it('should toggleModalBarcode', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    component.toggleModalBarcode();
    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('user-photo-modal-upload');
  });

  it('should translateBarcode', () => {
    component.photoUrl = 'photo';
    spyOn(component['userResource'], 'saveUserImage').and.returnValue(of(mockData.userData));
    spyOn(component['translateService'], 'instant');
    component.translateBarcode('image/png;base64,AAANSUhE');

    expect(component.myUser.photoBase64).toEqual('AAANSUhE');
    expect(component['translateService'].instant).toHaveBeenCalledWith(
      'variable.lbEditSuccessF', {
        labelName: component['translateService'].instant('label.photo')
    });
  });
});
