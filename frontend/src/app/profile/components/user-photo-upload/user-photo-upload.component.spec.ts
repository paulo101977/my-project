import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { UserPhotoUploadComponent } from './user-photo-upload.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('UserPhotoUploadComponent', () => {
  let component: UserPhotoUploadComponent;
  let fixture: ComponentFixture<UserPhotoUploadComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPhotoUploadComponent ],
      imports: [TranslateTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(UserPhotoUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', () => {
    spyOn(component, 'configCropper').and.callFake( () => {});
    component.ngOnInit();

    expect(component.configCropper).toHaveBeenCalled();
  });

});
