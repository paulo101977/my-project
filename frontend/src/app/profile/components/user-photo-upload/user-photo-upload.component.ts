import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ImageCropperComponent, CropperSettings } from 'ngx-img-cropper';

@Component({
  selector: 'app-user-photo-upload',
  templateUrl: './user-photo-upload.component.html',
  styleUrls: ['./user-photo-upload.component.scss']
})
export class UserPhotoUploadComponent implements OnInit {
  @ViewChild('cropper') cropper: ImageCropperComponent;

  item: string;
  cropperSettings: CropperSettings;
  data: any;

  @Input()
  get selectedImage() {
    return this.item;
  }

  set selectedImage(val) {
    if (val) {
      this.item = val;
      this.loadImage();
    }
  }

  @Output() dataChange = new EventEmitter<string>();
  @Output() exitChange = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.configCropper();
  }

  public configCropper() {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.width = 100;
    this.cropperSettings.height = 100;
    this.cropperSettings.croppedWidth = 100;
    this.cropperSettings.croppedHeight = 100;
    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;
    this.cropperSettings.canvasWidth = 400;
    this.cropperSettings.canvasHeight = 300;
    this.cropperSettings.rounded = true;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 1;

    this.data = {};
  }

  public loadImage() {
    const image: any = new Image();

    if ( this.selectedImage ) {
      image.src = this.selectedImage;
      image.onload = () => {
        this.cropper.setImage(image);
      };
    }
  }

  public cancelRequest() {
    this.exitChange.emit();
  }

  public changeData(): void {
    this.dataChange.emit(this.data.image);
  }

}
