import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { UserProfileListComponent } from './user-profile-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import {imgCdnStub} from '../../../../../testing';
import {TranslateTestingModule} from 'app/shared/mock-test/translate-testing.module';

describe('UserProfileListComponent', () => {
  let component: UserProfileListComponent;
  let fixture: ComponentFixture<UserProfileListComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserProfileListComponent,
        imgCdnStub
      ],
      imports: [
        RouterTestingModule,
        TranslateTestingModule
      ]
    });
    fixture = TestBed.createComponent(UserProfileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
