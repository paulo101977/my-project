import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-profile-list',
  templateUrl: './user-profile-list.component.html',
  styleUrls: ['./user-profile-list.component.scss']
})

export class UserProfileListComponent implements OnInit {

  @Input() menuList: any;
  constructor() { }

  ngOnInit() {
  }

}
