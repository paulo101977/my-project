import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { User, APP_NAME } from '@inovacaocmnet/thx-bifrost';
import { UserResource } from 'app/shared/resources/user/user.resource';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '@inovacaocmnet/thx-bifrost';
import { DateService } from 'app/shared/services/shared/date.service';
import { IMyDate } from 'mydaterangepicker';
import { ValidatorFormService } from 'app/shared/services/shared/validator-form.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  public userItems: User;
  public userForm: FormGroup;
  public buttonSaveConfig: ButtonConfig;
  public disableToday: IMyDate;

  constructor(
    @Inject(APP_NAME) private appName,
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private userResource: UserResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private dateService: DateService,
    private authService: AuthService,
    private validatorFormService: ValidatorFormService,
  ) { }

  ngOnInit() {
    this.configForm();
    this.buttonConfig();
    this.getUsers();
    this.disableToday = this.dateService.getTodayIMydate();
  }

  private configForm(): void {
    this.userForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(60)]],
      nickname: ['', [Validators.maxLength(60), this.validatorFormService.regexValidator(/^(\s)/g)]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: [''],
      dateOfBirth: ['', [Validators.required]],
    });

    this.userForm.valueChanges.subscribe( () => {
      if (this.verifyUserReplace()) {
        this.buttonSaveConfig.isDisabled = !this.userForm.valid;
      }
    });
  }

  private buttonConfig() {
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'reservationModule-confirm-cancel-modal',
      () => this.saveForm(),
      'action.save',
      ButtonType.Primary
    );
    this.buttonSaveConfig.extraClass = 'button-bold-size';
    this.buttonSaveConfig.isDisabled = true;
  }

  public saveForm() {
    this.loadUserData();

    this.userResource.updateUserInfo(this.userItems).subscribe(() => {
      this.saveLocalStorage();
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('variable.lbEditSuccessM', {
          labelName: this.translateService.instant('label.user')
        })
      );
    });
  }

  private saveLocalStorage() {
    this.authService.saveUser(this.appName, this.userItems);
  }

  public loadUserData() {
    this.userItems = {
      ...this.userItems,
      email: this.userForm.get('email').value,
      name: this.userForm.get('name').value,
      dateOfBirth: this.userForm.get('dateOfBirth').value,
      nickName: this.userForm.get('nickname').value,
      phoneNumber: this.userForm.get('phoneNumber').value
    };
  }

  public verifyUserReplace() {
    let verify = false;
    if (this.userForm.get('email').value !== this.userItems.email) {
      verify = true;
    } else if (this.userForm.get('name').value !== this.userItems.name) {
      verify = true;
    } else if (this.userForm.get('dateOfBirth').value !== this.userItems.dateOfBirth) {
      verify = true;
    } else if (this.userForm.get('nickname').value !== this.userItems.nickName) {
      verify = true;
    } else if (this.userForm.get('phoneNumber').value !== this.userItems.phoneNumber) {
      verify = true;
    }
    return verify;
  }

  public getUsers(): void {
    this.authService.getUser().subscribe(data => {
      this.userItems = data;
      if (this.userForm && this.userItems) {
        this.loadFormData();
      }
    });
  }

  public loadFormData() {
    if (this.userItems) {
      const date = this
        .dateService
        .convertStringToDate(this.userItems.dateOfBirth, DateService.DATE_FORMAT_UNIVERSAL);

      this.userForm.patchValue({
        email: this.userItems.email,
        name: this.userItems.name,
        dateOfBirth: date,
        nickname: this.userItems.nickName,
        phoneNumber: this.userItems.phoneNumber
      });
    }
  }
}
