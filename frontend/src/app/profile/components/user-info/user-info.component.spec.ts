import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { createAccessorComponent } from '../../../../../testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { configureTestSuite } from 'ng-bullet';

import { UserInfoComponent } from './user-info.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { RouterTestingModule } from '@angular/router/testing';

import { AdminApiTestingService, AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { of } from 'rxjs/internal/observable/of';
import { UserMock } from 'app/profile/mock-data/user-mock';
import { APP_NAME } from '@inovacaocmnet/thx-bifrost';


describe('UserInfoComponent', () => {
  let component: UserInfoComponent;
  let fixture: ComponentFixture<UserInfoComponent>;
  const thxError = createAccessorComponent('thx-error');
  const thxDatePicker = createAccessorComponent('thx-datepicker');
  const mockData = new UserMock();

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserInfoComponent,
        thxError,
        thxDatePicker,
      ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [
        AdminApiTestingService,
        {provide: AdminApiService, useExisting: AdminApiTestingService},
        dateServiceStub,
        { provide: APP_NAME, useValue: {} }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(UserInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should ngOnInit', () => {
    spyOn<any>(component, 'configForm');
    spyOn<any>(component, 'buttonConfig');
    spyOn<any>(component, 'getUsers');
    const today = new Date();
    const disableToday = {
      year: today.getFullYear(),
      month: today.getMonth() + 1,
      day: today.getDate() - 1,
    };

    component.ngOnInit();
    expect(component['configForm']).toHaveBeenCalled();
    expect(component['buttonConfig']).toHaveBeenCalled();
    expect(component['getUsers']).toHaveBeenCalled();
    expect(component.disableToday).toEqual(disableToday);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should loadData', () => {
    component.userItems = mockData.userData;
    component.loadFormData();
    expect(component.userForm.get('email').value).toEqual(component.userItems.email);
    expect(component.userForm.get('name').value).toEqual(component.userItems.name);
    expect(component.userForm.get('dateOfBirth').value).toEqual(null);
    expect(component.userForm.get('nickname').value).toEqual(component.userItems.nickName);
    expect(component.userForm.get('phoneNumber').value).toEqual(component.userItems.phoneNumber);
  });

  it('should saveForm', () => {
    spyOn(component, 'loadUserData');
    spyOn(component['userResource'], 'updateUserInfo').and.returnValue(of(true));
    spyOn(component['toasterEmitService'], 'emitChange');

    const message = component['translateService'].instant('variable.lbEditSuccessM', {
      labelName: component['translateService'].instant('label.user'),
    });

    component.saveForm();
    expect(component.loadUserData).toHaveBeenCalled();
    expect(component['userResource'].updateUserInfo).toHaveBeenCalled();
    expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(SuccessError.success, message);
  });

  it('form invalid when empty', () => {
    expect(component.userForm.valid).toBeFalsy();
  });

  it('should verify nickName invalid with one empty space char', () => {
    component.userForm.controls['nickname'].setValue(' ');
    expect(component.userForm.controls['nickname'].valid).toBeFalsy();
  });

  it('should verify nickName is valid', () => {
    component.userForm.controls['nickname'].setValue('Nick');
    expect(component.userForm.controls['nickname'].valid).toBeTruthy();
  });

  it('should loadUserData', () => {
    component.loadUserData();
    expect(component.userItems.email).toEqual(component.userForm.get('email').value);
    expect(component.userItems.phoneNumber).toEqual(component.userForm.get('phoneNumber').value);
  });

  it('should getUsers', fakeAsync(() => {
    const userData = {...mockData.userData, dateOfBirth: null, };

    spyOn<any>(component, 'loadFormData');
    spyOn<any>(component['authService'], 'getUser').and.returnValue(of(userData));

    component.getUsers();
    tick();

    expect(component.userItems).toEqual(userData);
    expect(component.loadFormData).toHaveBeenCalled();
  }));
});
