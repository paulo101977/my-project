import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserResourceService } from '@inovacaocmnet/thx-bifrost';
import { profileIconPipeStub } from 'app/profile/testing/profile-icon.pipe.stub';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { Observable, of } from 'rxjs';
import { createServiceStub, imgCdnStub } from '../../../../../testing';
import { ProfilePermissionsComponent } from './profile-permissions.component';

describe('ProfilePermissionsComponent', () => {
  let component: ProfilePermissionsComponent;
  let fixture: ComponentFixture<ProfilePermissionsComponent>;
  const userResourceService = createServiceStub(UserResourceService, {
    getProfilePermissionList(): Observable<any> {
      return of(null);
    }
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProfilePermissionsComponent,
        imgCdnStub,
        profileIconPipeStub,
      ],
      imports: [ TranslateTestingModule ],
      providers: [userResourceService]
    });
    fixture = TestBed.createComponent(ProfilePermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getProfileList', () => {
    it('test getProfileList', () => {
      spyOn(component['userResourceService'], 'getProfilePermissionList').and.returnValue(of({items: [
          {
            permissionId: 'cfa00683-9308-448c-9bed-d2ff29259ad5',
            listPermission: []
          }
        ]}));
      component.getProfileList();
      expect(component.permissionId).toBe('CFA00683-9308-448C-9BED-D2FF29259AD5');
    });
  });
});
