import { Component, OnInit } from '@angular/core';
import { UserResourceService } from '@inovacaocmnet/thx-bifrost';
import { map, pluck } from 'rxjs/operators';

@Component({
  selector: 'app-profile-permissions',
  templateUrl: './profile-permissions.component.html',
  styleUrls: ['./profile-permissions.component.scss']
})
export class ProfilePermissionsComponent implements OnInit {

  public permissionList: any[];
  public permissionId: string;
  public permission: string;

  constructor(private userResourceService: UserResourceService) {
  }

  ngOnInit() {
    this.getProfileList();
  }

  getProfileList() {
    this.userResourceService.getProfilePermissionList()
      .pipe(
        pluck('items'),
        map(data => {
          return data[0];
        })
      ).subscribe(data => {
      this.permissionId = data.permissionId.toUpperCase();
      this.permission = data.permission;
      this.permissionList = data.permissionList;
    });
  }
}
