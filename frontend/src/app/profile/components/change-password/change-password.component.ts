import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { UserResource } from 'app/shared/resources/user/user.resource';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { confirmPasswordValidator } from 'app/shared/validators/confirm-password-validator';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  public buttonSaveConfig: ButtonConfig;
  public passwordForm: FormGroup;
  public userId: string;

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private translateService: TranslateService,
    private toasterEmitService: ToasterEmitService,
    private userResource: UserResource
  ) { }

  ngOnInit() {
    this.createForm();
    this.configButton();
    this.getUserData();
  }

  public getUserData() {
    this.userResource.getCurrentUser().subscribe(user => {
      this.userId = user.id;
    });
  }

  public configButton() {
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'reservationModule-confirm-cancel-modal',
      () => this.save(),
      'action.save',
      ButtonType.Primary
    );
  }

  public createForm() {
    this.passwordForm = this.formBuilder.group({
      currentPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    }, {
      validator: [confirmPasswordValidator('newPassword', 'confirmPassword')]
    });

    this.passwordForm.statusChanges.subscribe(() => {
      this.buttonSaveConfig.isDisabled = this.passwordForm.invalid;
    });
  }

  save() {
    if (this.passwordForm.invalid) {
      return;
    }

    const {currentPassword, newPassword} = this.passwordForm.value;

    this.passwordForm.reset();
    this.userResource.updatePassword(this.userId, currentPassword, newPassword)
      .subscribe(() => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbEditSuccessF', {
            labelName: this.translateService.instant('label.password')
          })
        );
      });
  }

}
