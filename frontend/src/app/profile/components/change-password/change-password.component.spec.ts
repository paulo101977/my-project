import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ChangePasswordComponent } from './change-password.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { sharedServiceStub } from '../../../../../testing';
import { userResourceStub } from 'app/shared/resources/user/testing';
import { of } from 'rxjs';
import { UserMock } from 'app/profile/mock-data/user-mock';


describe('ChangePasswordComponent', () => {
  let component: ChangePasswordComponent;
  let fixture: ComponentFixture<ChangePasswordComponent>;
  const mockData = new UserMock();

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ChangePasswordComponent,
      ],
      imports: [
        TranslateTestingModule,
      ],
      providers: [
        sharedServiceStub,
        userResourceStub,
        FormBuilder
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(ChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn(component, 'createForm');
    spyOn(component, 'configButton');
    spyOn(component, 'getUserData');

    component.ngOnInit();
    expect(component.createForm).toHaveBeenCalled();
    expect(component.configButton).toHaveBeenCalled();
    expect(component.getUserData).toHaveBeenCalled();
  });

  it('should getUserData', () => {
    spyOn(component['userResource'], 'getCurrentUser').and.returnValue(of(mockData.userData));
    component.getUserData();
    expect(component.userId).toEqual(mockData.userData.id);
  });

  it('should save', () => {
    spyOn(component['userResource'], 'updatePassword').and.returnValue(of(null));
    component.passwordForm.patchValue(mockData.userPasswordForm);

    component.save();

    expect(component['userResource'].updatePassword)
      .toHaveBeenCalledWith(component.userId, mockData.userPasswordForm.currentPassword, mockData.userPasswordForm.newPassword);
  });
});
