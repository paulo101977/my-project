import { ProfileResourceService } from 'app/profile/shared/services/profile-resource.service';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../testing';

export const profileResourceStub = createServiceStub(ProfileResourceService, {
  list(params?: any): Observable<any> {
    return of(null);
  }
});
