import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvailabilityRoutingModule } from './availability-routing.module';
import { PropertyAvailabilityGridComponent } from './property-availability-grid/property-availability-grid.component';
import { SharedModule } from '../shared/shared.module';
import { GovernanceModule } from '../governance/governance.module';
import { ThxAvailabilityGridModule } from '@inovacao-cmnet/thx-ui';
// @ts-ignore
import { AvailabilityChartGridModule } from '@app/availability/avaialability-chart-grid/availability-chart-grid.module';

@NgModule({
  imports: [
    CommonModule,
    AvailabilityRoutingModule,
    SharedModule,
    GovernanceModule,
    ThxAvailabilityGridModule,
    AvailabilityChartGridModule
  ],
  declarations: [
    PropertyAvailabilityGridComponent
  ]
})
export class AvailabilityModule { }
