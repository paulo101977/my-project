import { NgModule } from '@angular/core';
import { AvailabilityChartGridComponent } from './components/availability-chart-grid/availability-chart-grid.component';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { ThxGroupedBarChartWidgetModule, ThxPieChartWidgetModule } from '@inovacao-cmnet/thx-ui';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ThxPieChartWidgetModule,
    ThxGroupedBarChartWidgetModule,
  ],
  declarations: [AvailabilityChartGridComponent],
  exports: [AvailabilityChartGridComponent]
})
export class AvailabilityChartGridModule { }
