import { ReceptionItem } from '@app/availability/avaialability-chart-grid/models/reception-item';

export class ReportReceptionStatus {
  checkInReturn: ReceptionItem;
  checkOutReturn: ReceptionItem;
  walkInReturn: ReceptionItem;
}
