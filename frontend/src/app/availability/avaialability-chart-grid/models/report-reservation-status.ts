import { ReportReservationItem } from '@app/availability/avaialability-chart-grid/models/report-reservation-item';

export class ReportReservationStatus {
  canceled: ReportReservationItem;
  checkin: ReportReservationItem;
  checkout: ReportReservationItem;
  confirmed: ReportReservationItem;
  noShow: ReportReservationItem;
  pending: ReportReservationItem;
  reservationProposal: ReportReservationItem;
  toConfirm: ReportReservationItem;
  waitList: ReportReservationItem;
}
