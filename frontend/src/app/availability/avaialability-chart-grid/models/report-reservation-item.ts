export class ReportReservationItem {
  id: number;
  name: string;
  total: number;
}
