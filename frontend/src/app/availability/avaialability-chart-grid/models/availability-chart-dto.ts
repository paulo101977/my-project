import { ReportUhSituation } from '@app/availability/avaialability-chart-grid/models/report-uh-situation';
import { ReportReceptionStatus } from '@app/availability/avaialability-chart-grid/models/report-reception-status';
import { ReportReservationStatus } from '@app/availability/avaialability-chart-grid/models/report-reservation-status';
import { HousekeepingStatusResolve } from 'app/availability/avaialability-chart-grid/models/HousekeepingStatusResolve';

export class AvailabilityChartDto {
  reportUhSituationReturnDto: ReportUhSituation;
  reportHousekeepingStatusReturnDto: HousekeepingStatusResolve;
  reportCheckStatusReturnDto: ReportReceptionStatus;
  reportReservationStatusReturnDto: ReportReservationStatus;
}

