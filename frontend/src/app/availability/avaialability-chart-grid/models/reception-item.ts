export class ReceptionItem {
  accomplished: number;
  estimated: number;
  dayUse: number;
}
