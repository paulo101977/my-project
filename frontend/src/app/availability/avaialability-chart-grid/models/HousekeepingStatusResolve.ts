export class HousekeepingStatusResolve {
  reportHousekeepingStatusResolveDtos: Array<HousekeepingStatusResolveItem>;
}

export class HousekeepingStatusResolveItem {
  id: number;
  name: string;
  total: number;
  color: string;
}
