import { Injectable } from '@angular/core';
import { HttpService } from '@app/core/services/http/http.service';
import { Observable } from 'rxjs';
import { AvailabilityChartDto } from '@app/availability/avaialability-chart-grid/models/availability-chart-dto';

@Injectable({
  providedIn: 'root'
})
export class AvailabilityChartResource {

  constructor(private httpClient: HttpService) {}

  getChartInfoFromProperty( propertyId: number): Observable<AvailabilityChartDto> {
    return this.httpClient.get<AvailabilityChartDto>(`ReservationReport/${propertyId}/Grafics`);
  }

}
