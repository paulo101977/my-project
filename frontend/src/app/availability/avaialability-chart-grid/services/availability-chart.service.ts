import { Injectable } from '@angular/core';
import { AvailabilityChartDto } from '@app/availability/avaialability-chart-grid/models/availability-chart-dto';
import { ChartItem, GroupChartItem, PieChartItem } from '@inovacao-cmnet/thx-ui';
import { TranslateService } from '@ngx-translate/core';
import { StatusStyleService } from '@app/shared/services/reservation/status-style.service';
import {
  HousekeepingStatusResolveItem
} from 'app/availability/avaialability-chart-grid/models/HousekeepingStatusResolve';

@Injectable({
  providedIn: 'root'
})
export class AvailabilityChartService {

  constructor(private translateService: TranslateService,
              private statusStyleService: StatusStyleService) {
  }

  public populateUhStatusFrom(chartDto: AvailabilityChartDto) {
    if (chartDto) {
      const uhStatusList = [];
      const uhSituation = chartDto.reportUhSituationReturnDto;

      uhStatusList.push(
        this.factoryItem(this.translateService.instant('label.uhBlocked'),
          uhSituation.blocked,
          '#FF6969'));

      uhStatusList.push(
        this.factoryItem(this.translateService.instant('label.uhVague'),
          uhSituation.vague,
          '#00B28E'));


      uhStatusList.push(
        this.factoryItem(this.translateService.instant('label.UhBusy'),
          uhSituation.busy,
          '#B7BEBF'));

      return uhStatusList;
    }
    return [];
  }

  public populateHousekeepingStatusFrom(chartDto: AvailabilityChartDto) {
    if (chartDto) {
      const uhHousekeepingStatusList = [];
      const report: Array<HousekeepingStatusResolveItem> = chartDto.reportHousekeepingStatusReturnDto.reportHousekeepingStatusResolveDtos;

      for (const item of report) {
        if (item) {
          uhHousekeepingStatusList.push(
            this.factoryItem(item.name, item.total, item.color));
        }
      }
      return uhHousekeepingStatusList;
    }
    return [];
  }

  public populateReceptionStatusFrom(chartDto: AvailabilityChartDto) {
    if (chartDto) {
      const groupItemList = [];
      const reportCheckStatus = chartDto.reportCheckStatusReturnDto;
      for (const key in reportCheckStatus) {
        if (reportCheckStatus.hasOwnProperty(key)) {
          const groupItem = new GroupChartItem();
          groupItem.name = this.translateService.instant(`label.${key}`);
          const series = [];
          if (reportCheckStatus[key]) {
            for (const keyItem in reportCheckStatus[key]) {
              if (reportCheckStatus[key].hasOwnProperty(keyItem)) {
                const item = new ChartItem();
                item.name = this.translateService.instant(`label.${keyItem}`);
                item.value = chartDto.reportCheckStatusReturnDto[key][keyItem];
                series.push(item);
              }
            }
            groupItem.series = series;
            groupItemList.push(groupItem);
          }
        }
      }
      return groupItemList;
    }
    return [];
  }

  public populateReportReservation(chartDto: AvailabilityChartDto) {
    if (chartDto) {
      const reportReservation = chartDto.reportReservationStatusReturnDto;
      const reportReservationList = [];
      for (const key in reportReservation) {
        if (reportReservation.hasOwnProperty(key)) {
          const status = reportReservation[key];
            reportReservationList.push(
              this.factoryItem(status.name,
                status.total,
                this.statusStyleService.getStatusColor(status.id)));
        }
      }

      return reportReservationList;
    }
    return [];
  }

  public getReceptionColorList() {
    return ['#CCD1D1', '#ABEBC6', '#AED6F1'];
  }

  private factoryItem(name: string, value: number, color?: string): PieChartItem {
    const item = new PieChartItem();
    item.name = name;
    item.value = value;
    item.color = color.indexOf('#') >= 0 ? color : `#${color}`;
    return item;
  }
}
