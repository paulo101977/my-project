import { inject, TestBed } from '@angular/core/testing';
import { AvailabilityChartService } from './availability-chart.service';
import { PieChartItem } from '@inovacao-cmnet/thx-ui';
import { TranslateModule } from '@ngx-translate/core';
import { StatusStyleService } from '@app/shared/services/reservation/status-style.service';

describe('AvailabilityChartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [AvailabilityChartService, PieChartItem, StatusStyleService]
    });
  });

  it('should be created', inject([AvailabilityChartService], (service: AvailabilityChartService) => {
    expect(service).toBeTruthy();
  }));

  it('should return Object PieChartItem',
    inject([AvailabilityChartService], (service: AvailabilityChartService) => {
    const pieChartItem = new PieChartItem();
    pieChartItem.name = 'name';
    pieChartItem.value = 10;
    pieChartItem.color = '#000000';

    const item = service['factoryItem'](pieChartItem.name, pieChartItem.value, pieChartItem.color);

    expect(item).toEqual(pieChartItem);
  }));

  it('should return 3 colors',
    inject([AvailabilityChartService], (service: AvailabilityChartService) => {
    const colorList = service.getReceptionColorList();

    expect(colorList.length).toEqual(3);
  }));

  it('should return 3 colors',
    inject([AvailabilityChartService], (service: AvailabilityChartService) => {
    const colorList = service.getReceptionColorList();

    expect(colorList.length).toEqual(3);
  }));

  describe('Populate chart methods', () => {

    let chartDto;

    beforeEach(() => {
      chartDto = {
        'reportUhSituationReturnDto': {'vague': 11, 'busy': 1, 'blocked': 0},
        'reportHousekeepingStatusReturnDto': {
          'reportHousekeepingStatusResolveDtos': [
            {
              'id': 1,
              'name': 'algo',
              'total': 10,
              'color': '#000000'
            },
            {
              'id': 2,
              'name': 'algo 2',
              'total': 1,
              'color': '#000000'
            }
          ]
        },
        'reportCheckStatusReturnDto': {
          'checkInReturn': {'estimated': 0, 'accomplished': 0, 'dayUse': 0},
          'checkOutReturn': {'estimated': 0, 'accomplished': 0, 'dayUse': 0},
          'walkInReturn': {'estimated': 0, 'accomplished': 0, 'dayUse': 0}
        },
        'reportReservationStatusReturnDto': {
          'toConfirm': {'id': 0, 'name': 'A Confirmar', 'total': 10},
          'confirmed': {'id': 1, 'name': 'Confirmada', 'total': 10},
          'checkin': {'id': 2, 'name': 'Check-In', 'total': 1},
          'checkout': {'id': 3, 'name': 'Check-Out', 'total': 1},
          'pending': {'id': 4, 'name': 'Pendente', 'total': 1},
          'noShow': {'id': 5, 'name': 'No-Show', 'total': 1},
          'canceled': {'id': 6, 'name': 'Cancelada', 'total': 1},
          'waitList': {'id': 7, 'name': 'Waiting List', 'total': 2},
          'reservationProposal': {'id': 8, 'name': 'Em tentativa', 'total': 1}
        }
      };
    });

    it('should call populateUhStatusFrom and return items',
      inject([AvailabilityChartService], (service: AvailabilityChartService) => {
      const uhStatus = service.populateUhStatusFrom(chartDto);
        expect(uhStatus.length).toEqual(3);
    }));

    it('should call populateHousekeepingStatusFrom and return items',
      inject([AvailabilityChartService], (service: AvailabilityChartService) => {
      const housekeepingStatus = service.populateHousekeepingStatusFrom(chartDto);
        expect(housekeepingStatus.length).toEqual(2);
    }));

    it('should call populateReceptionStatusFrom',
      inject([AvailabilityChartService], (service: AvailabilityChartService) => {
      const receptionStatusList = service.populateReceptionStatusFrom(chartDto);

      expect(receptionStatusList.length).toEqual(3);
    }));

    it('should call populateReportReservation  and return items than 0',
      inject([AvailabilityChartService], (service: AvailabilityChartService) => {
      const reportReservation = service.populateReportReservation(chartDto);

      expect(reportReservation.length).toEqual(9);
    }));



  });
});
