import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AvailabilityChartGridComponent } from './availability-chart-grid.component';
import { TranslateModule } from '@ngx-translate/core';
import { ThxGroupedBarChartWidgetModule, ThxPieChartWidgetModule } from '@inovacao-cmnet/thx-ui';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AvailabilityChartService } from '@app/availability/avaialability-chart-grid/services/availability-chart.service';
import { AvailabilityChartResource } from '@app/availability/avaialability-chart-grid/resources/availability-chart.resource';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('AvailabilityChartGridComponent', () => {
  let component: AvailabilityChartGridComponent;
  let fixture: ComponentFixture<AvailabilityChartGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateModule.forRoot(),
        ThxPieChartWidgetModule,
        RouterTestingModule,
        BrowserAnimationsModule,
      HttpClientTestingModule,
      ThxGroupedBarChartWidgetModule],
      declarations: [ AvailabilityChartGridComponent ],
      providers: [AvailabilityChartService, AvailabilityChartResource, InterceptorHandlerService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityChartGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
