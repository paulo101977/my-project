import { Component, OnInit } from '@angular/core';
import { GroupChartItem, PieChartItem } from '@inovacao-cmnet/thx-ui';
import { AvailabilityChartResource } from '@app/availability/avaialability-chart-grid/resources/availability-chart.resource';
import { AvailabilityChartDto } from '@app/availability/avaialability-chart-grid/models/availability-chart-dto';
import { AvailabilityChartService } from '@app/availability/avaialability-chart-grid/services/availability-chart.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-availability-chart-grid',
  templateUrl: './availability-chart-grid.component.html',
  styleUrls: ['./availability-chart-grid.component.css']
})
export class AvailabilityChartGridComponent implements OnInit {

  constructor(private availabilityChartResource: AvailabilityChartResource,
              private chartService: AvailabilityChartService,
              public  route: ActivatedRoute ) { }

  private availabilityChartDto: AvailabilityChartDto;

  public uhStatusList: Array<PieChartItem> = [];
  public uhHouseKeepingStatusList: Array<PieChartItem> = [];
  public receptionList: Array<GroupChartItem> = [];
  public receptionChartColorList: Array<string> = [];
  public reservationStatusList: Array<PieChartItem> = [];
  public propertyId: number;

  ngOnInit() {

    this.propertyId = this.route.snapshot.params.property;

    this.availabilityChartResource
      .getChartInfoFromProperty(this.propertyId)
      .subscribe( result => {
        this.availabilityChartDto = result;
        this.uhStatusList = this.chartService.populateUhStatusFrom(this.availabilityChartDto);
        this.uhHouseKeepingStatusList = this.chartService.populateHousekeepingStatusFrom(this.availabilityChartDto);
        this.receptionList = this.chartService.populateReceptionStatusFrom(this.availabilityChartDto);
        this.receptionChartColorList = this.chartService.getReceptionColorList();
        this.reservationStatusList = this.chartService.populateReportReservation(this.availabilityChartDto);
      });
  }

}
