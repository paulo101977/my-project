import { TestBed, inject } from '@angular/core/testing';

import { PropertyAvailabilityGridService } from './property-availability-grid.service';

describe('PropertyAvailabilityGridService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PropertyAvailabilityGridService]
    });
  });

  it('should be created', inject([PropertyAvailabilityGridService], (service: PropertyAvailabilityGridService) => {
    expect(service).toBeTruthy();
  }));
});
