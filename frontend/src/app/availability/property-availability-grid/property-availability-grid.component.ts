
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { GovernanceRoomBlockComponent } from '../../governance/components/governance-room-block/governance-room-block.component';
import { ConfigHeaderPage } from '../../shared/models/config-header-page';
import { ButtonBarConfig } from '../../shared/components/button-bar/button-bar-config';
import { ButtonBarButtonStatus } from '../../shared/components/button-bar/button-bar-button-status.enum';
import { ButtonBarColor } from '../../shared/components/button-bar/button-bar-color.enum';
import { ButtonBarButtonConfig } from '../../shared/components/button-bar/button-bar-button-config';
import { AvailabilityGridFilter } from '../../shared/models/availability-grid/availability-grid-filter';
import { AvailabilityGridPeriodEnum } from '../../shared/models/availability-grid/availability-grid-period.enum';
import { AvailabilityReservationChangeDto } from '../../shared/models/dto/availability-grid/availability-reservation-dto';
import { DateService } from '../../shared/services/shared/date.service';
import { AvailabilityGridResource } from '../../shared/resources/availability-grid/availability-grid.resource';
import { RoomBlock } from '../../shared/models/dto/housekeeping/room-block';
import { ModalEmitService } from '../../shared/services/shared/modal-emit.service';
import { PropertyResource } from '../../shared/resources/property/property.resource';
import { SuccessError } from '../../shared/models/success-error-enum';
import { RoomBlockResource } from '../../shared/resources/room-block/room-block.resource';
import { ToasterEmitService } from '../../shared/services/shared/toaster-emit.service';
import {
  Availability,
  AvailabilityGridLabels,
  Footer,
  Reservation,
  Room,
  RoomType,
  ThxAvailabilityGridComponent,
  TimeRange
} from '@inovacao-cmnet/thx-ui';
import { PropertyAvailabilityGridService } from 'app/availability/property-availability-grid/property-availability-grid.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';

@Component({
  selector: 'property-config-container',
  templateUrl: './property-availability-grid.component.html',
  styleUrls: ['./property-availability-grid.component.css'],
})
export class PropertyAvailabilityGridComponent implements OnInit {
  @ViewChild('grid') public availabilityGridComponent: ThxAvailabilityGridComponent;

  @ViewChild(GovernanceRoomBlockComponent) public governanceRoomBlock: GovernanceRoomBlockComponent;

  // Filter Items
  public propertyId: number;
  public configHeaderPage: ConfigHeaderPage;
  public availabilityGridFilterForm: FormGroup;
  public buttonBarConfig: ButtonBarConfig;

  // Grid Items
  public roomTypeList: RoomType[];
  public blockedRoomList: RoomBlock[];
  public availabilityList: Availability[];
  public footerList: Footer[];
  public reservationList: Reservation[] = [];
  public roomList: Room[];

  public labels: AvailabilityGridLabels;

  public startDate: string;
  public days: number;
  public expandedRoomType: number;
  public validateReservationMove: Function;
  public propertyParams: { checkin: string, checkout: string };


  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public formBuilder: FormBuilder,
    public dateService: DateService,
    public translateService: TranslateService,
    public modalEmitService: ModalEmitService,
    public gridResource: AvailabilityGridResource,
    public roomBlockResource: RoomBlockResource,
    public toastEmitService: ToasterEmitService,
    public propertyResource: PropertyResource,
    public propertyGridService: PropertyAvailabilityGridService,
    public sessionParameterService: SessionParameterService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.propertyId = +params['property'];
    });
    this.setVars();
  }

  private setVars() {
    const checkinHour = this.sessionParameterService.getParameterValue(this.propertyId, ParameterTypeEnum.Checkin);
    const checkoutHour = this.sessionParameterService.getParameterValue(this.propertyId, ParameterTypeEnum.Checkout);
    this.propertyParams = {checkin: checkinHour, checkout: checkoutHour};
    this.setAvailabilityGridFilterForm();
    this.setButtonBarConfig();

    const labels = [
      'propertyModule.availabilityGrid.labels.overbook',
      'propertyModule.availabilityGrid.labels.legend',
      'propertyModule.availabilityGrid.labels.checkin',
      'propertyModule.availabilityGrid.labels.checkout',
      'propertyModule.availabilityGrid.labels.toConfirm',
      'propertyModule.availabilityGrid.labels.confirmed',
      'propertyModule.availabilityGrid.labels.showToday',
      'propertyModule.availabilityGrid.labels.reservationCode',
      'propertyModule.availabilityGrid.labels.roomType',
      'propertyModule.availabilityGrid.labels.status',
      'propertyModule.availabilityGrid.labels.period',
      'propertyModule.availabilityGrid.labels.groupName',
      'propertyModule.availabilityGrid.labels.reservationDetails',
      'propertyModule.availabilityGrid.labels.newReservation',
      'propertyModule.availabilityGrid.labels.cornerLabel',
      'propertyModule.availabilityGrid.labels.rateProposal'
    ];

    this.translateService.get(labels).subscribe(data => {
      this.labels = {
        overbook: data['propertyModule.availabilityGrid.labels.overbook'],
        legend: data['propertyModule.availabilityGrid.labels.legend'],
        checkin: data['propertyModule.availabilityGrid.labels.checkin'],
        checkout: data['propertyModule.availabilityGrid.labels.checkout'],
        toConfirm: data['propertyModule.availabilityGrid.labels.toConfirm'],
        confirmed: data['propertyModule.availabilityGrid.labels.confirmed'],
        showToday: data['propertyModule.availabilityGrid.labels.showToday'],
        reservationCode: data['propertyModule.availabilityGrid.labels.reservationCode'],
        roomType: data['propertyModule.availabilityGrid.labels.roomType'],
        status: data['propertyModule.availabilityGrid.labels.status'],
        period: data['propertyModule.availabilityGrid.labels.period'],
        groupName: data['propertyModule.availabilityGrid.labels.groupName'],
        reservationDetails: data['propertyModule.availabilityGrid.labels.reservationDetails'],
        newReservation: 'Reservar período',
        cornerLabel: data['propertyModule.availabilityGrid.labels.cornerLabel'],
        unlockRoom: 'Desbloquear UH',
        blockRoom: 'Bloquear UH',
        reservationRateProposal: data['propertyModule.availabilityGrid.labels.rateProposal'],
      };
    });

    // ValidateReservationMove callback
    this.validateReservationMove = reservation => {
      const reservationDto = new AvailabilityReservationChangeDto();
      reservationDto.initialDate = reservation.start;
      reservationDto.finalDate = reservation.end;
      reservationDto.roomTypeId = reservation.roomTypeId;
      reservationDto.roomId = reservation.roomId;
      reservationDto.id = reservation.reservationItemId;

      return this.gridResource.changeReservationPeriod(this.propertyId, reservationDto);
    };
  }

  private setAvailabilityGridFilterForm() {
    const today = new Date();
    this.availabilityGridFilterForm = this.formBuilder.group({
      initialDate: [ today , [Validators.required]],
      period: [AvailabilityGridPeriodEnum.weekly, [Validators.required]],
    });

    this.availabilityGridFilterForm.valueChanges.subscribe(data => this.filter(this.availabilityGridFilterForm));

    this.filter(this.availabilityGridFilterForm);
  }

  private setButtonBarConfig() {
    this.buttonBarConfig = {
      color: ButtonBarColor.black,
      buttons: [
        {
          id: AvailabilityGridPeriodEnum.weekly,
          title: 'propertyModule.availabilityGrid.periodTypes.weekly',
          status: ButtonBarButtonStatus.active,
          callBackFunction: () => this.setPeriod(AvailabilityGridPeriodEnum.weekly),
        } as ButtonBarButtonConfig,
        {
          title: 'propertyModule.availabilityGrid.periodTypes.fortnightly',
          callBackFunction: () => this.setPeriod(AvailabilityGridPeriodEnum.fortnightly),
        } as ButtonBarButtonConfig,
        {
          title: 'propertyModule.availabilityGrid.periodTypes.monthly',
          callBackFunction: () => this.setPeriod(AvailabilityGridPeriodEnum.monthly),
          cssClass: 'hidden-sm-down',
        } as ButtonBarButtonConfig,
      ],
    };
  }

  private setPeriod(period: number) {
    this.availabilityGridFilterForm.patchValue({ period: period });
    this.buttonBarConfig.buttons.map(
      button => (button.status = button.id == period ? ButtonBarButtonStatus.active : ButtonBarButtonStatus.default),
    );
  }

  public filter({ value }: { value: AvailabilityGridFilter }) {
    const myData = this.dateService.convertUniversalDateToIMyDate( value.initialDate );
    const year = myData.date.year;
    const month = myData.date.month;
    const day = myData.date.day;

    const initialDate = new Date(year, month - 1, day);

    this.startDate = moment(initialDate)
      .format(DateService.DATE_FORMAT_UNIVERSAL)
      .toString();
    this.days = value.period == 30 ? this.dateService.getTotalDaysFromMonth(initialDate) : value.period;

    this.getRoomTypes(this.propertyId, this.startDate, this.days);
  }

  public getRoomTypes(propertyId: number, startDate: string, days: number) {
    this.gridResource.getRoomTypes(propertyId, startDate, days).subscribe(data => {
      if (this.expandedRoomType) {
        const expandedRoomType = data.roomTypeList.find(roomType => roomType.id == this.expandedRoomType);
        expandedRoomType.expanded = true;
        this.getRooms(this.expandedRoomType, startDate, this.days);
      }

      this.roomTypeList = data.roomTypeList;

      this.availabilityList = data.availabilityList;
      this.footerList = data.footerList;
      this.blockedRoomList = data.roomsBlocked;
    });
  }

  public getRooms(roomTypeId: number, startDate: string, days: number) {
    this.expandedRoomType = roomTypeId;

    this.gridResource.getRooms(this.propertyId, roomTypeId, startDate, days).subscribe(data => {
      this.availabilityGridComponent.setRooms(roomTypeId, data.roomList);
      this.roomList = data.roomList;
      this.reservationList = data.reservationList.map( item => {
        return this.propertyGridService.ajustReservationHour(item);
      });
    });
  }



  public onInitialDateChange(date: string) {
    this.startDate = date;
    this.getRoomTypes(this.propertyId, date, this.days);
  }

  public goToEditReservationPage(reservation: Reservation) {
    this.router.navigate(['p', this.propertyId, 'reservation', 'new'], { queryParams: { reservation: reservation.reservationId } });
  }

  public goToNewReservationPage(reservation: Reservation) {
    this.router.navigate(['p', this.propertyId, 'reservation', 'new'], { queryParams: reservation });
  }

  public goToRateProposalPage(timeRange: TimeRange) {
    this.router.navigate(['p', this.propertyId, 'reservation', 'rate-proposal'], { queryParams: timeRange });
  }

  public onBlockRoomClick(timeRange: TimeRange) {
    const item = {
      id: timeRange.roomId,
      roomNumber: timeRange.roomName,
      roomTypeId: timeRange.roomTypeId,
      roomTypeName: timeRange.roomTypeName,
      blockingStartDate: timeRange.start,
      blockingEndDate: timeRange.end,
    };
    this.governanceRoomBlock.selectedItens = [item];
    this.governanceRoomBlock.callBlock();
  }

  public onUnlockRoomClick(timeRange: TimeRange) {
    const title = this.translateService.instant('alert.unblockUH', { name: ` - ${timeRange.roomTypeName} ${timeRange.roomName}` });
    const message = this.translateService.instant('alert.unblockUHConfirmation', {
      name: ` ${timeRange.roomTypeName} ${timeRange.roomName}`,
    });

    this.modalEmitService.emitSimpleModal(title, message, () => this.unblock(timeRange), []);
  }

  unblock(timeRange: any) {
    this.roomBlockResource.unblockRoom(timeRange.roomBlock.id).subscribe(data => {
      this.getRoomTypes(this.propertyId, this.startDate, this.days);
      this.toastEmitService.emitChange(SuccessError.success, this.translateService.instant('alert.roomsUnblockedSuccess'));
    });
  }

  public onRoomBlocked() {
    this.getRoomTypes(this.propertyId, this.startDate, this.days);
  }

}
