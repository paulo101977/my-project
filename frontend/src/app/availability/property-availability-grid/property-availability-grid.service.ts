import { Injectable } from '@angular/core';
import { AvailabilityReservationDto } from 'app/shared/models/dto/availability-grid/availability-reservation-dto';
import * as moment from 'moment';
import { DateService } from 'app/shared/services/shared/date.service';

@Injectable({
  providedIn: 'root'
})
export class PropertyAvailabilityGridService {
  constructor() { }

  /**
   * workaround para mostrar uma pontinha da reserva no dia correto, quando o horário de saída é 00:00
   * @param item
   */
  public ajustReservationHour(item: AvailabilityReservationDto) {
    const endDate = moment(item.endFormatted, DateService.DATE_FORMAT_UNIVERSAL);
    if (endDate.isSame(endDate.startOf('hours'))) {
      item.end = endDate.add(120, 'minute')
        .format(DateService.DATE_FORMAT_UNIVERSAL);
    }
    return item;
  }
}

