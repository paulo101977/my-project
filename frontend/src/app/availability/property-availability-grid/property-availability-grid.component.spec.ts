import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { PropertyAvailabilityGridComponent } from './property-availability-grid.component';
import { AvailabilityGridPeriodEnum } from 'app/shared/models/availability-grid/availability-grid-period.enum';
import { ButtonBarButtonStatus } from 'app/shared/components/button-bar/button-bar-button-status.enum';
import {
  AvailabilityDtoData,
  AvailabilityFooterDtoData,
  AvailabilityReservationDtoData,
  AvailabilityResourcesDtoData,
  AvailabilityRoomsDtoData,
  AvailabilityRoomTypeDtoData,
} from 'app/shared/mock-data/availability-grid-data';
import { AvailabilityGridResource } from 'app/shared/resources/availability-grid/availability-grid.resource';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DateService } from 'app/shared/services/shared/date.service';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { amDateFormatPipeStub } from '../../../../testing';
import { ApiEnvironment, configureApi, ThexApiModule } from '@inovacaocmnet/thx-bifrost';
import { configureTestSuite } from 'ng-bullet';
import { dateServiceStub } from '@app/shared/services/shared/testing/date-service';

describe('PropertyAvailabilityGridComponent', () => {
  let component: PropertyAvailabilityGridComponent;
  let fixture: ComponentFixture<PropertyAvailabilityGridComponent>;
  let resource: AvailabilityGridResource;
  let httpMock: HttpTestingController;

  @Component({ selector: 'thx-availability-grid', template: '' })
  class ThxAvailabilityGridStubComponent {
    setRooms(roomTypeId, roomList) {
    }
  }

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        imports: [
          TranslateTestingModule,
          RouterTestingModule,
          HttpClientTestingModule,
          ThexApiModule
        ],
        providers: [
          FormBuilder,
          configureApi({
            environment: ApiEnvironment.Development
          })
        ],
        declarations: [
          PropertyAvailabilityGridComponent,
          ThxAvailabilityGridStubComponent,
          amDateFormatPipeStub
        ],
        schemas: [NO_ERRORS_SCHEMA],
      });

    fixture = TestBed.createComponent(PropertyAvailabilityGridComponent);
    component = fixture.componentInstance;
    resource = TestBed.get(AvailabilityGridResource);
    httpMock = TestBed.get(HttpTestingController);

    spyOn<any>(component.route.params, 'subscribe').and.callFake(success => {
      success({ property: '1' });
    });

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should set ButtonBar config', () => {
      component['setButtonBarConfig']();

      expect(component.buttonBarConfig.buttons).not.toBeNull();
      expect(component.buttonBarConfig.buttons.length).toEqual(3);
      expect(component.buttonBarConfig.buttons[0].title).toEqual('propertyModule.availabilityGrid.periodTypes.weekly');
      expect(component.buttonBarConfig.buttons[0].status).toEqual(ButtonBarButtonStatus.active);
      expect(component.buttonBarConfig.buttons[0].callBackFunction).not.toBeNull();
      expect(component.buttonBarConfig.buttons[1].title).toEqual('propertyModule.availabilityGrid.periodTypes.fortnightly');
      expect(component.buttonBarConfig.buttons[1].callBackFunction).not.toBeNull();
      expect(component.buttonBarConfig.buttons[2].title).toEqual('propertyModule.availabilityGrid.periodTypes.monthly');
      expect(component.buttonBarConfig.buttons[2].callBackFunction).not.toBeNull();
    });

    it(
      'should create component with settings initials',
      fakeAsync(() => {
        tick();

        expect(component).toBeTruthy();
        expect(component.propertyId).toEqual(1);
        expect(component.availabilityGridFilterForm.get('initialDate').value).toEqual(jasmine.any(Date));
        expect(component.availabilityGridFilterForm.get('period').value).toEqual(AvailabilityGridPeriodEnum.weekly);
      }),
    );

    it('should set AvailabilityGridFilter form', () => {
      component['setAvailabilityGridFilterForm']();
      expect(component.availabilityGridFilterForm).toBeTruthy();
    });
  });

  it('should set period on form correctly', () => {
    component['setPeriod'](AvailabilityGridPeriodEnum.monthly);
    expect(component.availabilityGridFilterForm.get('period').value).toEqual(AvailabilityGridPeriodEnum.monthly);

    for (const button of component.buttonBarConfig.buttons) {
      if (button.id == AvailabilityGridPeriodEnum.monthly) {
        expect(button.status).toEqual(ButtonBarButtonStatus.active);
      } else {
        expect(button.status).toEqual(ButtonBarButtonStatus.default);
      }
    }
  });

  it('should execute filter', () => {
    const todayMock = new Date(2018, 12 - 1, 24).toStringUniversal();
    const initialDateString = moment(todayMock)
      .format(DateService.DATE_FORMAT_UNIVERSAL)
      .toString();

    component.availabilityGridFilterForm.patchValue({
      initialDate: initialDateString,
      period: AvailabilityGridPeriodEnum.monthly,
    });

    component.filter(component.availabilityGridFilterForm);

    expect(component.startDate).toEqual(initialDateString);
    expect(component.days).toEqual(31);
  });

  it('should goToEditReservationPage', () => {
    spyOn(component.router, 'navigate');
    const reservation = AvailabilityReservationDtoData;

    component.goToEditReservationPage(reservation);
    expect(component.router.navigate).toHaveBeenCalledWith(['p', 1, 'reservation', 'new'], {
      queryParams: { reservation: reservation.reservationId },
    });
  });

  it('should get roomTypes', done => {
    const spy = spyOn(component['gridResource'], 'getRoomTypes').and.returnValue(
      new Observable(observer => {
        setTimeout(() => {
          observer.next(AvailabilityResourcesDtoData);
        }, 200);
      }),
    );

    component.getRoomTypes(1, '2017-12-10', 7);

    spy.calls.mostRecent().returnValue.subscribe(() => {
      expect(component.roomTypeList).toEqual([AvailabilityRoomTypeDtoData]);
      expect(component.availabilityList).toEqual([AvailabilityDtoData]);
      expect(component.footerList).toEqual([AvailabilityFooterDtoData]);
      done();
    });
  });

  it('should get roomList/reservationList', done => {
    const spy = spyOn(component['gridResource'], 'getRooms').and.returnValue(
      new Observable(observer => {
        setTimeout(() => {
          observer.next(AvailabilityRoomsDtoData);
        }, 200);
      }),
    );

    spyOn(component['availabilityGridComponent'], 'setRooms');

    component.propertyId = 1;
    component.startDate = '2017-12-10';
    component.days = 7;

    component.getRooms(1, component.startDate, component.days);

    spy.calls.mostRecent().returnValue.subscribe(() => {
      expect(component.roomList).toEqual(AvailabilityRoomsDtoData.roomList);
      expect(component.reservationList).toEqual(AvailabilityRoomsDtoData.reservationList);
      done();
    });
  });

  it('should get roomTypeList when navigating', done => {
    spyOn(component, 'getRooms');

    const spy = spyOn<any>(component, 'getRoomTypes').and.returnValue(
      new Observable(observer => {
        setTimeout(() => {
          observer.next({});
        }, 200);
      }),
    );

    const startDate = '2018-12-29';
    component.onInitialDateChange(startDate);

    spy.calls.mostRecent().returnValue.subscribe(() => {
      expect(component.startDate).toEqual(startDate);
      expect(component['getRoomTypes']).toHaveBeenCalled();
      done();
    });
  });

  it('should get roomTypeList and roomList when navigating', done => {
    spyOn(component, 'getRooms');

    const spy = spyOn<any>(component, 'getRoomTypes').and.returnValue(
      new Observable(observer => {
        setTimeout(() => {
          observer.next({});
        }, 200);
      }),
    );

    const startDate = '2018-12-29';
    component.expandedRoomType = 1;
    component.onInitialDateChange(startDate);

    spy.calls.mostRecent().returnValue.subscribe(() => {
      expect(component.startDate).toEqual(startDate);
      expect(component['getRoomTypes']).toHaveBeenCalled();
      done();
    });
  });
});
