import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PropertyAvailabilityGridComponent } from './property-availability-grid/property-availability-grid.component';

const routes: Routes = [
  { path: '', component: PropertyAvailabilityGridComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AvailabilityRoutingModule { }
