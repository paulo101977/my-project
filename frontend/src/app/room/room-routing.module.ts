import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoomListComponent } from './components/room-list/room-list.component';
import { RoomCreateComponent } from './components/room-create/room-create.component';
import { RoomEditComponent } from './components/room-edit/room-edit.component';

export const roomRoutes: Routes = [
  { path: '', component: RoomListComponent },
  { path: 'new', component: RoomCreateComponent },
  { path: 'edit/:id', component: RoomEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(roomRoutes)],
  exports: [RouterModule],
})
export class RoomRoutingModule {}
