// import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
// import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { MockBackend, MockConnection } from '@angular/http/testing';
// import { TranslateModule, TranslateService } from '@ngx-translate/core';
// import { RouterTestingModule } from '@angular/router/testing';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { By } from '@angular/platform-browser';
// import { HttpClientTestingModule } from '@angular/common/http/testing';
// import { HttpClientModule } from '@angular/common/http';
// import { RoomEditComponent } from './room-edit.component';
// import { Room } from '../../../shared/models/room';
// import { RoomType } from '../../../shared/models/room-type/room-type';
// import { BedTypeEnum } from '../../../shared/models/bed-type';
// import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
// import { SuccessError } from '../../../shared/models/success-error-enum';
// import { RoomTypeService } from '../../../shared/services/room-type/room-type.service';
// import { RoomResource } from '../../../shared/resources/room/room.resource';
// import { FilterListPipe } from '../../../shared/pipes/filter-select.pipe';
//
// xdescribe('RoomEditComponent', () => {
//   let component: RoomEditComponent;
//   let fixture: ComponentFixture<RoomEditComponent>;
//   let elemHelper, roomResourceMock, mockEndpoint, roomTypeServiceMock, translateServiceMock;
//   const propertyId = 1;
//
//   const roomTypesMockList = new Array<RoomType>();
//
//   const roomTypeMock = new RoomType();
//   roomTypeMock.abbreviation = 'URG';
//   roomTypeMock.adultCapacity = 2;
//   roomTypeMock.childCapacity = 1;
//   roomTypeMock.roomTypeBedTypeList = [
//     {
//       bedType: BedTypeEnum.Double,
//       count: 2,
//       optionalLimit: 0,
//     },
//     {
//       bedType: BedTypeEnum.Reversible,
//       count: 4,
//       optionalLimit: 0,
//     },
//     {
//       bedType: BedTypeEnum.Single,
//       count: 6,
//       optionalLimit: 0,
//     },
//     {
//       bedType: BedTypeEnum.Crib,
//       count: 0,
//       optionalLimit: 10,
//     },
//     {
//       bedType: BedTypeEnum.Single,
//       count: 0,
//       optionalLimit: 33,
//     },
//   ];
//
//   roomTypesMockList.push(roomTypeMock);
//
//   const roomMock = new Room();
//   roomMock.id = 1;
//   roomMock.roomNumber = '265';
//   roomMock.isActive = true;
//   roomMock.remarks = 'OBS';
//   roomMock.building = 'A';
//   roomMock.floor = 'B';
//   roomMock.wing = 'C';
//   roomMock.roomType = roomTypeMock;
//   roomMock.childRoomIdList = [roomMock.id, roomMock.id];
//
//   const conjugateRoomMockList = new Array<Room>();
//   conjugateRoomMockList.push(roomMock);
//
//   const roomsMock = new Array<Room>();
//   roomsMock.push(roomMock);
//
//   beforeAll(() => {
//     TestBed.resetTestEnvironment();
//
//     TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
//       imports: [FormsModule, ReactiveFormsModule, TranslateModule.forRoot(), RouterTestingModule, HttpClientTestingModule],
//       declarations: [RoomEditComponent, FilterListPipe],
//       providers: [],
//       schemas: [CUSTOM_ELEMENTS_SCHEMA],
//     });
//
//     fixture = TestBed.createComponent(RoomEditComponent);
//     component = fixture.componentInstance;
//     elemHelper = fixture.debugElement;
//     fixture.detectChanges();
//   });
//
//   beforeEach(() => {
//     roomTypeServiceMock = new RoomTypeService();
//   });
//
//   it('should create RoomEditComponent', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should set formGroupEditRoom', () => {
//     expect(Object.keys(component.formGroupEditRoom.controls)).toEqual([
//       'roomType',
//       'roomNumber',
//       'floor',
//       'wing',
//       'building',
//       'remarks',
//       'filterRoomType',
//       'filterConjugateRoom',
//     ]);
//   });
//
//   it('should set ConfigHeaderPage', () => {
//     component.room = new Room();
//     component.room.roomNumber = '10';
//
//     component.setConfigHeaderPage();
//
//     expect(component.configHeaderPage.headerPageEnum).toEqual(HeaderPageEnum.Edit);
//     expect(component.configHeaderPage.titlePage).toEqual('hotelPanelModule.roomEdit.title');
//     expect(component.configHeaderPage.confirmDelete).toEqual(component.confirmDeleteRoom);
//     expect(component.configHeaderPage.nameOfItemUpdated).toEqual(component.room.roomNumber);
//   });
//
//   it('should loadRoomTypeOfRoom', () => {
//     component.room = roomMock;
//     const doubleBedCapacity = roomTypeServiceMock.getQuantityBedList(roomMock.roomType, BedTypeEnum.Double);
//     const reversibleBedCapacity = roomTypeServiceMock.getQuantityBedList(roomMock.roomType, BedTypeEnum.Reversible);
//     const singleBedCapacity = roomTypeServiceMock.getQuantityBedList(roomMock.roomType, BedTypeEnum.Single);
//     const cribBedCapacity = roomTypeServiceMock.getQuantityOptionalBedList(roomMock.roomType, BedTypeEnum.Crib);
//     const extraBedCapacity = roomTypeServiceMock.getQuantityOptionalBedList(roomMock.roomType, BedTypeEnum.Single);
//
//     component.loadRoomTypeOfRoom();
//
//     expect(component.formGroupEditRoom.get('roomType.adultCapacity').value).toEqual(roomMock.roomType.adultCapacity);
//     expect(component.formGroupEditRoom.get('roomType.childCapacity').value).toEqual(roomMock.roomType.childCapacity);
//     expect(component.formGroupEditRoom.get('roomType.doubleBedCapacity').value).toEqual(doubleBedCapacity);
//     expect(component.formGroupEditRoom.get('roomType.reversibleBedCapacity').value).toEqual(reversibleBedCapacity);
//     expect(component.formGroupEditRoom.get('roomType.singleBedCapacity').value).toEqual(singleBedCapacity);
//     expect(component.formGroupEditRoom.get('roomType.cribBedCapacity').value).toEqual(cribBedCapacity);
//     expect(component.formGroupEditRoom.get('roomType.extraBedCapacity').value).toEqual(extraBedCapacity);
//   });
//
//   it(
//     'should loadChildrenConjugateRoomsOfRoom',
//     fakeAsync(() => {
//       component.room = roomMock;
//       component.chosenConjugatedRooms = null;
//
//       const promiseChildrenRoomResource = spyOn(component['roomResource'], 'getChildrenRoomByRoomId').and.callFake(() => {
//         return new Promise((resolve, reject) => resolve(conjugateRoomMockList));
//       });
//
//       promiseChildrenRoomResource().then(prop => {
//         component.chosenConjugatedRooms = prop;
//       });
//       tick();
//
//       expect(component.chosenConjugatedRooms).not.toBeNull();
//       expect(component.chosenConjugatedRooms.length).toBeGreaterThanOrEqual(1);
//     }),
//   );
//
//   it(
//     'should loadChildrenConjugateRoomsOfRoom without children',
//     fakeAsync(() => {
//       component.room = roomMock;
//       component.chosenConjugatedRooms = null;
//       component.room.childRoomIdList = null;
//
//       component['loadChildrenConjugateRoomsOfRoom']();
//
//       expect(component.chosenConjugatedRooms).toBeNull();
//     }),
//   );
//
//   it('should loadRoomFormControls', () => {
//     component.room = roomMock;
//
//     component.loadRoomFormControls();
//
//     expect(component.formGroupEditRoom.get('roomNumber').value).toEqual(roomMock.roomNumber);
//     expect(component.formGroupEditRoom.get('floor').value).toEqual(roomMock.floor);
//     expect(component.formGroupEditRoom.get('wing').value).toEqual(roomMock.wing);
//     expect(component.formGroupEditRoom.get('building').value).toEqual(roomMock.building);
//     expect(component.formGroupEditRoom.get('remarks').value).toEqual(roomMock.remarks);
//   });
//
//   it('should run _location.back', () => {
//     spyOn(component._location, 'back');
//
//     component.goBackPreviewPage();
//
//     expect(component._location.back).toHaveBeenCalled();
//   });
//
//   it(
//     'should fill select field with RoomTypes',
//     fakeAsync(() => {
//       component.room = new Room();
//       const promiseRoomTypeResource = spyOn(component['roomTypeResource'], 'getRoomTypesByPropertyId').and.callFake(() => {
//         return new Promise((resolve, reject) => resolve(roomTypesMockList));
//       });
//
//       promiseRoomTypeResource(propertyId).then(prop => {
//         component.roomTypeList = prop;
//       });
//       tick();
//
//       expect(component.roomTypeList).toEqual(roomTypesMockList);
//     }),
//   );
//
//   it(
//     'should fill select field with Conjugate Rooms',
//     fakeAsync(() => {
//       const promiseConjugateRoomResource = spyOn(component['roomResource'], 'getRoomsByPropertyId').and.callFake(() => {
//         return new Promise((resolve, reject) => resolve(conjugateRoomMockList));
//       });
//       promiseConjugateRoomResource(component.propertyId).then(prop => {
//         component.conjugatedRoomList = prop;
//       });
//       tick();
//       expect(component.conjugatedRoomList).toEqual(conjugateRoomMockList);
//     }),
//   );
//
//   it(
//     'should return empty RoomTypes and show message to user',
//     fakeAsync(() => {
//       const promiseRoomTypeResource = spyOn(component['roomTypeResource'], 'getRoomTypesByPropertyId').and.callFake(() => {
//         return new Promise((resolve, reject) => resolve([]));
//       });
//
//       promiseRoomTypeResource(propertyId).then(prop => {
//         component.roomTypeList = prop;
//       });
//       tick();
//
//       expect(component.roomTypeList).toEqual([]);
//       expect(elemHelper.query(By.css('.thf-empty-content-header-title')).nativeElement.innerText).toBe(
//         'hotelPanelModule.roomTypeList.errorMessage',
//       );
//     }),
//   );
//
//   it(
//     'should return empty Conjugate Rooms and show message to user',
//     fakeAsync(() => {
//       const promiseRoomResource = spyOn(component['roomResource'], 'getRoomsByPropertyId').and.callFake(() => {
//         return new Promise((resolve, reject) => resolve([]));
//       });
//       promiseRoomResource(component.propertyId).then(prop => {
//         component.conjugatedRoomList = prop;
//       });
//       tick();
//       expect(component.conjugatedRoomList).toEqual([]);
//     }),
//   );
//
//   it('should fill room with roomType after select it', () => {
//     component.room = new Room();
//
//     component.changeRoomTypeInSelect(roomTypesMockList[0]);
//
//     expect(component.showDataRoomType).toEqual(true);
//     expect(component.formGroupEditRoom.get('roomType.adultCapacity').value).toEqual(roomTypesMockList[0].adultCapacity);
//     expect(component.formGroupEditRoom.get('roomType.childCapacity').value).toEqual(roomTypesMockList[0].childCapacity);
//   expect(component.formGroupEditRoom.get('roomType.doubleBedCapacity').value).toEqual(roomTypesMockList[0].roomTypeBedTypeList[0].count);
//     expect(component.formGroupEditRoom.get('roomType.reversibleBedCapacity').value).toEqual(
//       roomTypesMockList[0].roomTypeBedTypeList[1].count,
//     );
//   expect(component.formGroupEditRoom.get('roomType.singleBedCapacity').value).toEqual(roomTypesMockList[0].roomTypeBedTypeList[2].count);
//     expect(component.formGroupEditRoom.get('roomType.cribBedCapacity').value).toEqual(
//       roomTypesMockList[0].roomTypeBedTypeList[3].optionalLimit,
//     );
//     expect(component.formGroupEditRoom.get('roomType.extraBedCapacity').value).toEqual(
//       roomTypesMockList[0].roomTypeBedTypeList[4].optionalLimit,
//     );
//   });
//
//   it('should remove conjugate Room from chosenConjugatedRooms', () => {
//     const chosenConjugatedRoomsMock = new Array<Room>();
//     chosenConjugatedRoomsMock.push(roomMock);
//     component.chosenConjugatedRooms = chosenConjugatedRoomsMock;
//     component.conjugatedRoomList = [];
//
//     component.removeConjugatedRoomFromSelect(roomMock);
//
//     expect(component.chosenConjugatedRooms).toEqual([]);
//     expect(component.chosenConjugatedRooms.length).toEqual(0);
//     expect(component.conjugatedRoomList.length).toEqual(1);
//   });
//
//   it('should insert conjugate Room in chosenConjugatedRooms', () => {
//     spyOn(component, 'toogleConjugatedItemsAreaSelect');
//     component.chosenConjugatedRooms = [];
//     component.conjugatedRoomList = [roomMock];
//
//     component.chooseConjugateRoom(roomMock);
//
//     expect(component.chosenConjugatedRooms.length).toEqual(1);
//     expect(component.chosenConjugatedRooms).toContain(roomMock);
//     expect(component.conjugatedRoomList.length).toEqual(0);
//     expect(component.toogleConjugatedItemsAreaSelect).toHaveBeenCalled();
//   });
//
//   it('should insert conjugate Room in chosenConjugatedRooms when this list is null', () => {
//     spyOn(component, 'toogleConjugatedItemsAreaSelect');
//     component.chosenConjugatedRooms = null;
//     component.conjugatedRoomList = [roomMock];
//
//     component.chooseConjugateRoom(roomMock);
//
//     expect(component.chosenConjugatedRooms.length).toEqual(1);
//     expect(component.chosenConjugatedRooms).toContain(roomMock);
//     expect(component.conjugatedRoomList.length).toEqual(0);
//     expect(component.toogleConjugatedItemsAreaSelect).toHaveBeenCalled();
//   });
//
//   it('should show items of select when click to open it', () => {
//     component.showItemsSelect = false;
//     expect(component.showItemsSelect).toBeFalsy();
//     component.toogleItemsAreaSelect();
//     expect(component.showItemsSelect).toBeTruthy();
//   });
//
//   it('should show items of select conjugate rooms when click to open it', () => {
//     expect(component.showConjugatedItemsSelect).toBeFalsy();
//     component.toogleConjugatedItemsAreaSelect();
//     expect(component.showConjugatedItemsSelect).toBeTruthy();
//   });
//
//   it('should change value property Filter.Abbreviation after change value of field filter from Form', () => {
//     component.formGroupEditRoom.get('filterRoomType').setValue('teste');
//     component.observeFilterRoomTypeSelect();
//     expect(component.filterRoomType).toEqual('teste');
//   });
//
//   it('should count lenght remarks field', () => {
//     spyOn(component.countService, 'setFieldToCount');
//     spyOn(component.countService, 'getLenght').and.returnValue({ subscribe: () => {} });
//
//     component.countLenghtRemarksField();
//
//     expect(component.countService.setFieldToCount).toHaveBeenCalled();
//     expect(component.countService.getLenght).toHaveBeenCalled();
//   });
//
//   it('should show conjugateSelect after user check this option', () => {
//     component.showConjugatedSelect = false;
//     spyOn(component, 'loadConjugatedRoomToShowInSelect');
//
//     component.toogleConjugatedSelect();
//
//     expect(component.showConjugatedSelect).toBeTruthy();
//     expect(component['loadConjugatedRoomToShowInSelect']).toHaveBeenCalled();
//   });
//
//   it('should hidden conjugateSelect after user uncheck this option', () => {
//     component.showConjugatedSelect = true;
//     component.chosenConjugatedRooms = conjugateRoomMockList;
//     spyOn(component, 'loadConjugatedRoomToShowInSelect');
//
//     component.toogleConjugatedSelect();
//
//     expect(component.showConjugatedSelect).toBeFalsy();
//     expect(component.chosenConjugatedRooms).toBeNull();
//   });
//
//   it('should find room in edition in conjugateRoomList', () => {
//     component.room = roomMock;
//     const conjugatedRooms = new Array<Room>();
//     conjugatedRooms.push(roomMock);
//
//     expect(component['roomInEditionIsInConjugateRoomList'](conjugatedRooms)).toBeTruthy();
//   });
//
//   it('should not find room in edition in conjugateRoomList', () => {
//     component.room = roomMock;
//     const conjugatedRooms = new Array<Room>();
//
//     expect(component['roomInEditionIsInConjugateRoomList'](conjugatedRooms)).toBeFalsy();
//   });
//
//   it('should remove room in edition from conjugateRoomList', () => {
//     component.room = roomMock;
//     const conjugatedRooms = new Array<Room>();
//     conjugatedRooms.push(roomMock);
//
//     component['removeRoomInEditionFromConjugateRoomList'](conjugatedRooms);
//
//     expect(conjugatedRooms).toEqual([]);
//   });
//
//   describe('on loadRoom', () => {
//     beforeEach(() => {
//       TestBed.configureTestingModule({
//         imports: [FormsModule, ReactiveFormsModule, TranslateModule.forRoot(), RouterTestingModule, HttpClientModule],
//         declarations: [RoomEditComponent, FilterListPipe],
//         providers: [],
//         schemas: [CUSTOM_ELEMENTS_SCHEMA],
//       });
//
//       fixture = TestBed.createComponent(RoomEditComponent);
//       component = fixture.componentInstance;
//       elemHelper = fixture.debugElement;
//       fixture.detectChanges();
//     });
//
//     beforeEach(inject([MockBackend, RoomResource], (mockBackend: MockBackend, resourceMock: RoomResource) => {
//       mockEndpoint = mockBackend;
//       roomResourceMock = resourceMock;
//       component.room = roomMock;
//     }));
//
//     it(
//       'should execute loadRoomTypeOfRoom, loadRoomTypesToShowInSelect, loadRoomFormControls and setConfigHeaderPage',
//       fakeAsync(() => {
//         spyOn(component, 'loadRoomTypeOfRoom');
//         spyOn(component, 'loadRoomTypesToShowInSelect');
//         spyOn(component, 'loadRoomFormControls');
//         spyOn(component, 'setConfigHeaderPage');
//         mockEndpoint.connections.subscribe((connection: MockConnection) => {
//           const options = new ResponseOptions({
//             body: JSON.stringify({
//               result: roomMock,
//             }),
//           });
//           connection.mockRespond(new Response(options));
//         });
//         tick();
//
//         component.loadRoom();
//
//         roomResourceMock.getRoomById(roomMock.id).then(resp => {
//           expect(component.loadRoomTypeOfRoom).toHaveBeenCalled();
//           expect(component.loadRoomTypesToShowInSelect).toHaveBeenCalledWith(component.propertyId);
//           expect(component.loadRoomFormControls).toHaveBeenCalled();
//           expect(component.setConfigHeaderPage).toHaveBeenCalled();
//         });
//       }),
//     );
//   });
//
//   describe('on saveRoom', () => {
//     beforeEach(() => {
//       TestBed.configureTestingModule({
//         imports: [FormsModule, ReactiveFormsModule, TranslateModule.forRoot(), RouterTestingModule, HttpClientModule],
//         declarations: [RoomEditComponent, FilterListPipe],
//         providers: [],
//         schemas: [CUSTOM_ELEMENTS_SCHEMA],
//       });
//
//       fixture = TestBed.createComponent(RoomEditComponent);
//       component = fixture.componentInstance;
//       elemHelper = fixture.debugElement;
//       fixture.detectChanges();
//     });
//
//     beforeEach(inject(
//       [MockBackend, RoomResource, TranslateService],
//       (mockBackend: MockBackend, resourceMock: RoomResource, translateService: TranslateService) => {
//         mockEndpoint = mockBackend;
//         roomResourceMock = resourceMock;
//         translateServiceMock = translateService;
//         component.roomTypeSelected = roomTypeMock;
//       },
//     ));
//
//     it(
//       'should execute emitChange toasterEmitService',
//       fakeAsync(() => {
//         spyOn(component.toasterEmitService, 'emitChange');
//         spyOn(component.router, 'navigate');
//         spyOn(component.formGroupEditRoom, 'reset');
//         spyOn(component['roomMapper'], 'getRoomUpdatedByFormGroupToSendServer').and.returnValue(roomMock);
//         mockEndpoint.connections.subscribe((connection: MockConnection) => {
//           const options = new ResponseOptions({
//             body: JSON.stringify({
//               result: roomMock,
//             }),
//           });
//           connection.mockRespond(new Response(options));
//         });
//         tick();
//
//         component.propertyId = propertyId;
//         component.saveRoom();
//
//         roomResourceMock.editRoom(roomMock).then(resp => {
//           expect(component.router.navigate).toHaveBeenCalledWith(['../../'], { relativeTo: component['route'] });
//           expect(component.toasterEmitService.emitChange).toHaveBeenCalledWith(
//             SuccessError.success,
//             translateServiceMock.instant('hotelPanelModule.roomEdit.editRoomSuccessMessage'),
//           );
//         });
//       }),
//     );
//
//     it(
//       'should show error message',
//       fakeAsync(() => {
//         spyOn(component.toasterEmitService, 'emitChange');
//         spyOn(component.roomService, 'conjugatedRoomListIsInvalid').and.returnValue(true);
//
//         component.showConjugatedSelect = true;
//         component.room = roomMock;
//         component.room.childRoomIdList = null;
//         component.saveRoom();
//
//         expect(component.toasterEmitService.emitChange).toHaveBeenCalledWith(
//           SuccessError.error,
//           translateServiceMock.instant('hotelPanelModule.commomData.errorInConjugatedRoomList'),
//         );
//       }),
//     );
//   });
//
//   describe('on delete Room', () => {
//     beforeEach(() => {
//       TestBed.configureTestingModule({
//         imports: [FormsModule, ReactiveFormsModule, TranslateModule.forRoot(), RouterTestingModule, HttpClientModule],
//         declarations: [RoomEditComponent, FilterListPipe],
//         providers: [],
//         schemas: [CUSTOM_ELEMENTS_SCHEMA],
//       });
//
//       fixture = TestBed.createComponent(RoomEditComponent);
//       component = fixture.componentInstance;
//       elemHelper = fixture.debugElement;
//       fixture.detectChanges();
//     });
//
//     beforeEach(inject([MockBackend, RoomResource], (mockBackend: MockBackend, resourceMock: RoomResource) => {
//       mockEndpoint = mockBackend;
//       roomResourceMock = resourceMock;
//       component.roomTypeSelected = roomTypeMock;
//     }));
//
//     it('should call modal to delete room', () => {
//       spyOn(component.modalEmitService, 'emitSimpleModal');
//       component.room = roomMock;
//       component.roomTypeSelected = roomTypeMock;
//
//       component.propertyId = propertyId;
//       component.confirmDeleteRoom();
//
//       expect(component.modalEmitService.emitSimpleModal).toHaveBeenCalledWith(
//         'commomData.delete',
//         'hotelPanelModule.roomEdit.confirmDeleteRoom ' + component.room.roomNumber + '?',
//         component.deleteRoom,
//         [],
//       );
//     });
//
//     it(
//       'should call delete room',
//       fakeAsync(() => {
//         spyOn(component.router, 'navigate');
//         spyOn(component.toasterEmitService, 'emitChange');
//         component.room = roomMock;
//
//         mockEndpoint.connections.subscribe((connection: MockConnection) => {
//           const options = new ResponseOptions({
//             body: JSON.stringify({
//               result: roomMock,
//             }),
//           });
//           connection.mockRespond(new Response(options));
//         });
//
//         tick();
//
//         component.deleteRoom();
//
//         roomResourceMock.deleteRoom(roomMock).subscribe(
//           resp => {
//             expect(component.router.navigate).toHaveBeenCalledWith(['./room'], { relativeTo: component['route'] });
//             expect(component.toasterEmitService.emitChange).toHaveBeenCalledWith(
//               SuccessError.success,
//               'hotelPanelModule.roomEdit.deleteRoomTypeSuccessMessage',
//             );
//           },
//           error => {
//             expect(component.toasterEmitService.emitChange).toHaveBeenCalledWith(SuccessError.error, 'commomData.errorMessage');
//           },
//         );
//       }),
//     );
//
//     it('should goToRoomTypeNewPage', () => {
//       spyOn(component.router, 'navigate');
//       component.goToRoomTypeNewPage();
//       expect(component.router.navigate).toHaveBeenCalledWith(['../room-type', 'new'], { relativeTo: component['route'] });
//     });
//   });
// });
