import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { Room } from '../../../shared/models/room';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { BedTypeEnum } from '../../../shared/models/bed-type';
import { ConfigHeaderPage } from '../../../shared/models/config-header-page';
import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { CountService } from '../../../shared/services/shared/count.service';
import { RoomService } from '../../../shared/services/room/room.service';
import { RoomMapper } from 'app/shared/mappers/room-mapper';
import { RoomResource } from '../../../shared/resources/room/room.resource';
import { RoomTypeService } from '../../../room-type/shared/services/room-type/room-type.service';
import { RoomTypeResource } from '../../../room-type/shared/services/room-type-resource/room-type.resource';

@Component({
  selector: 'app-room-edit',
  templateUrl: './room-edit.component.html',
})
export class RoomEditComponent implements OnInit {
  public configHeaderPage: ConfigHeaderPage;
  public roomId: number;
  public room: Room;
  public roomTypeSelected: RoomType;
  public chosenConjugatedRooms: Array<Room>;
  public formGroupEditRoom: FormGroup;
  public showDataRoomType: boolean;
  public roomTypeList: Array<RoomType>;
  public conjugatedRoomList: Array<Room>;
  public filterRoomType: string;
  public filterConjugateRoom: string;
  public parametersfilterRoomType: Array<string>;
  public parametersConjugateRoom: Array<string>;
  public showItemsSelect = false;
  public showConjugatedSelect = false;
  public showConjugatedItemsSelect = false;
  public count = 0;
  public limitLenghtFieldRemarks = 4000;
  public propertyId: number;

  constructor(
    public translateService: TranslateService,
    public formBuilder: FormBuilder,
    public route: ActivatedRoute,
    public router: Router,
    public toasterEmitService: ToasterEmitService,
    public _location: Location,
    private roomTypeService: RoomTypeService,
    public roomResource: RoomResource,
    public roomService: RoomService,
    private roomTypeResource: RoomTypeResource,
    public countService: CountService,
    public modalEmitService: ModalEmitService,
    private roomMapper: RoomMapper,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  setVars() {
    this.route.params.subscribe(params => {
      this.roomId = +params['id'];
      this.loadRoom();
    });

    this.formGroupEditRoom = this.formBuilder.group({
      roomType: this.formBuilder.group({
        adultCapacity: ['', [Validators.required]],
        childCapacity: ['', [Validators.required]],
        doubleBedCapacity: ['', [Validators.required]],
        reversibleBedCapacity: ['', [Validators.required]],
        singleBedCapacity: ['', [Validators.required]],
        cribBedCapacity: ['', [Validators.required]],
        extraBedCapacity: ['', [Validators.required]],
      }),
      roomNumber: ['', [Validators.required, Validators.maxLength(10), Validators.pattern('[a-zA-Z0-9]+')]],
      floor: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]+')]],
      wing: [''],
      building: [''],
      remarks: ['', [Validators.maxLength(4000)]],
      filterRoomType: [''],
      filterConjugateRoom: [''],
    });

    this.setConfigHeaderPage();
    this.observeFilterRoomTypeSelect();
    this.parametersfilterRoomType = ['abbreviation', 'name'];
    this.parametersConjugateRoom = ['roomNumber'];
  }

  public setConfigHeaderPage() {
    this.configHeaderPage = new ConfigHeaderPage(HeaderPageEnum.Create);
    this.configHeaderPage.titlePage = 'hotelPanelModule.roomEdit.title';
    this.configHeaderPage.nameOfItemUpdated = this.getRoomNumber();
  }

  private getRoomNumber() {
    return this.room == undefined ? '' : this.room.roomNumber;
  }

  public loadRoom() {
    this.roomResource.getRoomById(this.roomId).subscribe(room => {
      this.room = room;
      this.propertyId = this.room.propertyId;
      this.count = this.room.remarks != null ? this.room.remarks.length : 0;
      this.loadRoomTypeOfRoom();
      this.loadRoomTypesToShowInSelect(this.propertyId);
      this.loadChildrenConjugateRoomsOfRoom();
      this.loadRoomFormControls();
      this.setConfigHeaderPage();
      this.countLenghtRemarksField();
    });
  }

  public loadRoomTypeOfRoom() {
    this.roomTypeSelected = this.room.roomType;
    this.showDataRoomType = true;

    this.formGroupEditRoom.get('roomType.adultCapacity').setValue(this.roomTypeSelected.adultCapacity);
    this.formGroupEditRoom.get('roomType.childCapacity').setValue(this.roomTypeSelected.childCapacity);
    this.formGroupEditRoom
      .get('roomType.doubleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(this.roomTypeSelected, BedTypeEnum.Double));
    this.formGroupEditRoom
      .get('roomType.reversibleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(this.roomTypeSelected, BedTypeEnum.Reversible));
    this.formGroupEditRoom
      .get('roomType.singleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(this.roomTypeSelected, BedTypeEnum.Single));
    this.formGroupEditRoom
      .get('roomType.cribBedCapacity')
      .setValue(this.roomTypeService.getQuantityOptionalBedList(this.roomTypeSelected, BedTypeEnum.Crib));
    this.formGroupEditRoom
      .get('roomType.extraBedCapacity')
      .setValue(this.roomTypeService.getQuantityOptionalBedList(this.roomTypeSelected, BedTypeEnum.Single));
  }

  public loadRoomFormControls() {
    this.formGroupEditRoom.get('roomNumber').setValue(this.room.roomNumber);
    this.formGroupEditRoom.get('floor').setValue(this.room.floor);
    this.formGroupEditRoom.get('wing').setValue(this.room.wing);
    this.formGroupEditRoom.get('building').setValue(this.room.building);
    const remarks = this.room.remarks != null ? this.room.remarks : '';
    this.formGroupEditRoom.get('remarks').setValue(remarks);
  }

  public loadRoomTypesToShowInSelect(propertyId: number) {
    this.roomTypeResource.getRoomTypesByPropertyId(propertyId).subscribe(roomTypes => {
      this.roomTypeList = roomTypes;
    });
  }

  private loadChildrenConjugateRoomsOfRoom() {
    if (this.hasChildren(this.room)) {
      this.roomResource.getChildrenRoomByRoomId(this.room.id).subscribe(rooms => {
        this.chosenConjugatedRooms = rooms;
      });

      this.toogleConjugatedSelect();
    }
  }

  private hasChildren(room: Room): boolean {
    return room.childRoomIdList != null && room.childRoomIdList.length >= 2;
  }

  private loadConjugatedRoomToShowInSelect(hotelId: number) {
    this.roomResource.getRoomsWithoutParentAndChildrenByPropertyId(this.propertyId).subscribe(conjugatedRooms => {
      if (conjugatedRooms.length > 0) {
        if (this.roomInEditionIsInConjugateRoomList(conjugatedRooms)) {
          this.removeRoomInEditionFromConjugateRoomList(conjugatedRooms);
        }

        this.conjugatedRoomList = conjugatedRooms;
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.error,
          this.translateService.instant('hotelPanelModule.conjugatedRoom.thereIsNoConjugatedRoom'),
        );
      }
    });
  }

  private roomInEditionIsInConjugateRoomList(conjugatedRooms: Array<Room>): boolean {
    if (conjugatedRooms.find(conjugatedRoom => conjugatedRoom.id === this.room.id)) {
      return true;
    }
    return false;
  }

  private removeRoomInEditionFromConjugateRoomList(conjugatedRooms: Array<Room>) {
    conjugatedRooms.forEach((room, index) => {
      if (room.id == this.room.id) {
        conjugatedRooms.splice(index, 1);
      }
    });
  }

  public changeRoomTypeInSelect(roomType: RoomType) {
    this.showDataRoomType = true;

    this.formGroupEditRoom.get('roomType.adultCapacity').setValue(roomType.adultCapacity);
    this.formGroupEditRoom.get('roomType.childCapacity').setValue(roomType.childCapacity);
    this.formGroupEditRoom
      .get('roomType.doubleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(roomType, BedTypeEnum.Double));
    this.formGroupEditRoom
      .get('roomType.reversibleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(roomType, BedTypeEnum.Reversible));
    this.formGroupEditRoom
      .get('roomType.singleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(roomType, BedTypeEnum.Single));
    this.formGroupEditRoom
      .get('roomType.cribBedCapacity')
      .setValue(this.roomTypeService.getQuantityOptionalBedList(roomType, BedTypeEnum.Crib));
    this.formGroupEditRoom
      .get('roomType.extraBedCapacity')
      .setValue(this.roomTypeService.getQuantityOptionalBedList(roomType, BedTypeEnum.Single));

    this.roomTypeSelected = roomType;
    this.toogleItemsAreaSelect();
  }

  public removeRoomTypeFromSelect() {
    this.formGroupEditRoom.get('roomType').reset();
    this.roomTypeSelected = null;
  }

  public chooseConjugateRoom(room: Room) {
    if (this.chosenConjugatedRooms == null) {
      this.chosenConjugatedRooms = new Array<Room>();
    }
    this.chosenConjugatedRooms.push(room);

    if (this.conjugatedRoomList.find(conjugatedRoom => conjugatedRoom.id === room.id)) {
      this.conjugatedRoomList.splice(this.conjugatedRoomList.indexOf(room), 1);
    }

    this.toogleConjugatedItemsAreaSelect();
  }

  public removeConjugatedRoomFromSelect(room: Room) {
    if (this.chosenConjugatedRooms.find(chosenConjugatedRoom => chosenConjugatedRoom.id === room.id)) {
      this.chosenConjugatedRooms.splice(this.chosenConjugatedRooms.indexOf(room), 1);

      if (this.conjugatedRoomList != null) {
        this.conjugatedRoomList.push(room);
      }
    }
  }

  public toogleItemsAreaSelect() {
    this.showItemsSelect = !this.showItemsSelect;
  }

  public toogleConjugatedSelect() {
    this.showConjugatedSelect = !this.showConjugatedSelect;

    if (this.showConjugatedSelect) {
      this.loadConjugatedRoomToShowInSelect(this.propertyId);
    } else {
      this.chosenConjugatedRooms = null;
    }
  }

  public toogleConjugatedItemsAreaSelect() {
    this.showConjugatedItemsSelect = !this.showConjugatedItemsSelect;
  }

  public observeFilterRoomTypeSelect() {
    const control = this.formGroupEditRoom.get('filterRoomType');
    control.valueChanges.subscribe(value => {
      this.filterRoomType = value;
    });
  }

  public saveRoom() {
    if (this.showConjugatedSelect && this.roomService.conjugatedRoomListIsInvalid(this.chosenConjugatedRooms)) {
      this.toasterEmitService.emitChange(
        SuccessError.error,
        this.translateService.instant('hotelPanelModule.commomData.errorInConjugatedRoomList'),
      );
    } else {
      this.setRoomToSendServer();
      this.roomResource.editRoom(this.room).subscribe(room => {
        this.router.navigate(['../../'], { relativeTo: this.route });
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('hotelPanelModule.roomEdit.editRoomSuccessMessage'),
        );
      });
    }
  }

  private setRoomToSendServer() {
    this.room = this.roomMapper.getRoomUpdatedByFormGroupToSendServer(this.room, this.formGroupEditRoom, this.chosenConjugatedRooms);
    this.room.roomType = this.roomTypeSelected;
    this.room.roomTypeId = this.roomTypeSelected.id;
  }

  public countLenghtRemarksField() {
    this.countService.setFieldToCount(this.formGroupEditRoom.get('remarks'));
    this.countService.getLenght().subscribe(response => {
      this.count = response.count;
    });
  }

  public goBackPreviewPage() {
    this._location.back();
  }

  public goToRoomTypeNewPage() {
    this.router.navigate(['../../../room-type', 'new'], { relativeTo: this.route });
  }
}
