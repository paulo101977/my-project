import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ConfigHeaderPage, SizeOfSearchButtonEnum } from '../../../shared/models/config-header-page';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { BedTypeService } from '../../../room-type/shared/services/bed-type/bed-type.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { RoomResource } from '../../../shared/resources/room/room.resource';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
import { Room } from '../../../shared/models/room';
import { Column } from '../../../shared/models/table/column';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { DataTableGlobals } from 'app/shared/models/DataTableGlobals';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
})
export class RoomListComponent implements OnInit {
  public tableRowTypeEnum: TableRowTypeEnum;
  public configHeaderPage: ConfigHeaderPage;
  public hasRooms: boolean;
  public propertyId: number;
  public allRoomList: Array<Room>;
  public roomWithoutParentList: Array<Room>;
  public parentRoomList: Array<Room>;
  public hasParentRoomList: boolean;
  public columns: Array<Column>;
  public quantityOfItemsToDisplayPerPage: number;
  public searchableItems: SearchableItems;
  public showRoomWithoutParentList: boolean;

  // Modal Inactive dependencies
  public readonly MODAL_TITLE = 'inactive-uh';
  public formModalInactive: FormGroup;
  public roomNumber: string;
  public roomBlocked: Room;
  public btnCancel: ButtonConfig;
  public btnConfirm: ButtonConfig;

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public roomResource: RoomResource,
    public translateService: TranslateService,
    public bedTypeService: BedTypeService,
    public toasterEmitService: ToasterEmitService,
    public loadPageEmitService: LoadPageEmitService,
    private dataTableGlobals: DataTableGlobals,
    public modalToggle: ModalEmitToggleService,
    public fb: FormBuilder,
    public sharedService: SharedService,
  ) {}

  ngOnInit() {
    this.setVars();
    this.getRoomsRegistered();
  }

  setVars() {
    this.route.params.subscribe(params => {
      this.propertyId = +params['property'];
    });
    this.columns = [];
    this.hasRooms = false;
    this.allRoomList = [];
    this.quantityOfItemsToDisplayPerPage = 10;
    this.tableRowTypeEnum = TableRowTypeEnum.Room;
    this.setConfigHeaderPage();
    this.setModalInactiveConfig();
  }

  public setConfigHeaderPage() {
    this.configHeaderPage = new ConfigHeaderPage(HeaderPageEnum.List);
    this.configHeaderPage.titlePage = 'hotelPanelModule.roomList.title';
    this.configHeaderPage.titleCreateButton = 'hotelPanelModule.roomList.newRoom';
    this.configHeaderPage.goToCreatePage = this.goToCreatePage;
    this.configHeaderPage.sizeOfSearchButtonEnum = SizeOfSearchButtonEnum.Medium;
  }

  public goToCreatePage = () => {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

  public showAllRoomList() {
    this.allRoomList = this.roomWithoutParentList;
    this.searchableItems.items = this.allRoomList;
    this.showRoomWithoutParentList = true;
  }

  public showParentRoomList() {
    this.allRoomList = this.parentRoomList;
    this.searchableItems.items = this.allRoomList;
    this.showRoomWithoutParentList = false;
  }

  public getRoomsRegistered() {
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.roomResource.getRoomsWithoutChildByPropertyId(this.propertyId).subscribe(rooms => {
      this.roomWithoutParentList = rooms;
      this.showRoomWithoutParentList = true;
      this.allRoomList = this.roomWithoutParentList;
      this.setSearchableItems();
      this.hasRooms = rooms.length > 0 ? true : false;
      this.bedTypeService.setBedTypeCountForRooms(this.allRoomList);
      this.setColumnsName();
      this.loadParentRoomList();
    });
  }

  public setSearchableItems() {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.allRoomList;
    this.searchableItems.tableRowTypeEnum = TableRowTypeEnum.Room;
    this.searchableItems.placeholderSearchFilter = 'hotelPanelModule.roomList.filterSearchPlaceholder';
  }

  public setColumnsName() {
    this.columns = new Array<Column>();
    this.columns.push(new Column(this.translateService.instant('commomData.tableColumns.active'), 1, 'center'));
    this.columns.push(new Column(this.translateService.instant('hotelPanelModule.roomList.tableColumns.uhNum'), 1));
    this.columns.push(new Column(this.translateService.instant('hotelPanelModule.roomList.tableColumns.roomType'), 1));
    this.columns.push(new Column(this.translateService.instant('commomData.tableColumns.description'), 2));
    this.columns.push(new Column(this.translateService.instant('hotelPanelModule.roomList.tableColumns.observation'), 2, 'center'));
    this.columns.push(new Column(this.translateService.instant('commomData.tableColumns.bedsQuantity'), 2, 'center'));
    this.columns.push(new Column('', 1));
  }

  public loadParentRoomList() {
    this.roomResource.getParentRoomsByPropertyId(this.propertyId).subscribe(rooms => {
      this.parentRoomList = rooms;
      this.hasParentRoomList = this.parentRoomListIsEmpty() ? false : true;
      if (this.hasParentRoomList) {
        this.bedTypeService.setBedTypeCountForRooms(this.parentRoomList);
      }
      this.loadPageEmitService.emitChange(ShowHide.hide);
    });
  }

  private parentRoomListIsEmpty() {
    if (this.parentRoomList == null) {
      return true;
    }
    if (this.parentRoomList.length == 0) {
      return true;
    }
    return false;
  }

  public updateRoomStatus(room: any) {
    this.roomResource.updateRoomStatusById(room.id)
      .subscribe(() => {
      if (room.isActive) {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('hotelPanelModule.roomList.roomEnabledWithSuccess'),
        );
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('hotelPanelModule.roomList.roomInactivatedWithSuccess'),
        );
        this.formModalInactive.reset();
        this.modalToggle.emitToggleWithRef(this.MODAL_TITLE);
      }
    });
  }

  public updateRoomList(items: Array<any>) {
    if (items != undefined) {
      this.hasRooms = items.length > 0 ? true : false;
      this.allRoomList = items;
    }
  }

  public goEditRoomPage(roomSelected) {
    this.router.navigate(['./edit', roomSelected.id], { relativeTo: this.route });
  }

  private setModalInactiveConfig() {
    this.setFormToggleInactive();
    this.setBtnModalInactive();
  }

  private setFormToggleInactive() {
    this.formModalInactive = this.fb.group({
      roomNumber: ['', this.matchUhName()]
    });

    this.formModalInactive.statusChanges
      .subscribe(values => {
         this.btnConfirm.isDisabled = this.formModalInactive.invalid;
      });
  }

  public setBtnModalInactive() {
    this.btnCancel = this.sharedService.getButtonConfig(
      'cancel-modal-inactive-uh',
      () => this.canceModal(),
      'commomData.cancel',
      ButtonType.Secondary
    );

    this.btnConfirm = this.sharedService.getButtonConfig(
      'confirm-modal-inactive-uh',
      () => this.updateRoomStatus(this.roomBlocked),
      'commomData.confirm',
      ButtonType.Primary,
      null,
      true
    );
  }

  private matchUhName() {
    return (control) => {
      return !(this.roomNumber == control.value) ? {'name': {value: 'Invalid'}} : null;
    };
  }

  private canceModal() {
    this.roomBlocked.isActive = !this.roomBlocked.isActive;
    this.formModalInactive.reset();
    this.modalToggle.emitToggleWithRef(this.MODAL_TITLE);
  }

  public toggleModal = (room: Room) => {
    if (!room.isActive) {
      this.roomBlocked = room ? room : null;
      this.roomNumber = room.roomNumber;
      this.modalToggle.emitToggleWithRef(this.MODAL_TITLE);
    } else {
      this.updateRoomStatus(room);
    }
  }

}
