// import { async, ComponentFixture, inject, TestBed, fakeAsync, tick } from '@angular/core/testing';
// import { TranslateModule } from '@ngx-translate/core';
// import { RouterTestingModule } from '@angular/router/testing';
// import { FormsModule } from '@angular/forms';
// import { By } from '@angular/platform-browser';
// import { Http, BaseRequestOptions, ResponseOptions, Response } from '@angular/http';
// import { MockBackend, MockConnection } from '@angular/http/testing';
// import { TranslateService } from '@ngx-translate/core';
// import { roomRoutes as RoomRoutes } from '../../room-routing.module';
// import { appRoutes as AppRoutes } from '../../../app-routing.module';
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { Room } from '../../../shared/models/room';
// import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
// import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
// import { SizeOfSearchButtonEnum } from '../../../shared/models/config-header-page';
// import { SuccessError } from '../../../shared/models/success-error-enum';
// import { RoomListComponent } from './room-list.component';
// import { TableComponent } from '../../../shared/components/table/table.component';
// import { TableHeaderComponent } from '../../../shared/components/table-header/table-header.component';
// import { TableFooterComponent } from '../../../shared/components/table-footer/table-footer.component';
// import { TableRowRoomComponent } from '../../../shared/components/table-row-room/table-row-room.component';
// import { HeaderPageComponent } from '../../../shared/components/header-page/header-page.component';
// import { HeaderListPageComponent } from '../../../shared/components/header-list-page/header-list-page.component';
// import { HeaderCreatePageComponent } from '../../../shared/components/header-create-page/header-create-page.component';
// import { HeaderEditPageComponent } from '../../../shared/components/header-edit-page/header-edit-page.component';
// import { RoomTypeService } from '../../../shared/services/room-type/room-type.service';
// import { BedTypeService } from '../../../shared/services/bed-type/bed-type.service';
// import { SharedService } from '../../../shared/services/shared/shared.service';
// import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
// import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
// import { RoomResource } from '../../../shared/resources/room/room.resource';
// import { DataTableGlobals } from '../../../shared/models/DataTableGlobals';
// import { NgxDatatableModule } from '@swimlane/ngx-datatable';
// import {HttpClientModule} from '@angular/common/http';
//
// describe('RoomListComponent', () => {
//   let component: RoomListComponent;
//   let fixture: ComponentFixture<RoomListComponent>;
//   let element, elemHelper, roomResourceMock, mockEndpoint, translateServiceMock;
//
//   const propertyId = 1;
//
//   const room = new Room();
//   const rooms = new Array<Room>();
//   rooms.push(room);
//
//   const roomDataWithItems = {
//     items: [
//       {
//         id: 1,
//         roomNumber: '265',
//         isActive: true,
//         parentRoom: {},
//         property: {},
//         roomType: {},
//         roomPosition: {},
//         bedType: []
//       },
//       {
//         id: 2,
//         roomNumber: '267',
//         isActive: false,
//         parentRoom: {},
//         property: {},
//         roomType: {},
//         roomPosition: {},
//         bedType: []
//       }
//     ]
//   };
//
//   const roomData = [
//     {
//       id: 1,
//       roomNumber: '265',
//       isActive: true,
//       parentRoom: {},
//       property: {},
//       roomType: {},
//       roomPosition: {},
//       bedType: []
//     },
//     {
//       id: 2,
//       roomNumber: '267',
//       isActive: false,
//       parentRoom: {},
//       property: {},
//       roomType: {},
//       roomPosition: {},
//       bedType: []
//     }
//   ];
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         TranslateModule.forRoot(),
//         RouterTestingModule,
//         FormsModule,
//
//         NgxDatatableModule,
//         HttpClientModule
//       ],
//       declarations: [
//         RoomListComponent,
//         TableComponent,
//         TableFooterComponent,
//         TableHeaderComponent,
//         TableRowRoomComponent,
//         HeaderPageComponent,
//         HeaderListPageComponent,
//         HeaderCreatePageComponent,
//         HeaderEditPageComponent
//       ],
//       providers: [],
//       schemas: [NO_ERRORS_SCHEMA]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(RoomListComponent);
//     component = fixture.componentInstance;
//     element = fixture.nativeElement;
//     elemHelper = fixture.debugElement;
//
//     spyOn(component.route.params, 'subscribe').and.callFake((success) => {
//       success({ property: '1' });
//     });
//
//     fixture.detectChanges();
//   });
//
//   it('should create component with settings initials', fakeAsync(() => {
//     const promiseRoomResource = spyOn(component.roomResource, 'getRoomsByPropertyId').and.callFake(() => {
//       return new Promise((resolve, reject) => resolve(roomData));
//     });
//
//     promiseRoomResource(component.propertyId).then(prop => {
//       component.allRoomList = prop;
//     });
//
//     tick();
//
//     expect(component).toBeTruthy();
//     expect(component.propertyId).toEqual(1);
//     expect(element.querySelector('h1').innerText).toBe('hotelPanelModule.roomList.errorMessage');
//     expect(elemHelper.query(By.css('.thf-empty-content-message')).nativeElement.innerText)
// .toBe('commomData.emptyContent.followTo commomData.emptyContent.registerScreen hotelPanelModule.roomList.messageEmptyContent');
//     expect(component.columns).toEqual([]);
//     expect(component.hasRooms).toEqual(false);
//     expect(component.allRoomList).toEqual(roomData);
//     expect(component.quantityOfItemsToDisplayPerPage).toEqual(10);
//     expect(component.tableRowTypeEnum).toEqual(TableRowTypeEnum.Room);
//   }));
//
//   it('should config HeaderPage', fakeAsync(() => {
//     expect(component.configHeaderPage.headerPageEnum).toEqual(HeaderPageEnum.List);
//     expect(component.configHeaderPage.titlePage).toEqual('hotelPanelModule.roomList.title');
//     expect(component.configHeaderPage.titleCreateButton).toEqual('hotelPanelModule.roomList.newRoom');
//     expect(component.configHeaderPage.sizeOfSearchButtonEnum).toEqual(SizeOfSearchButtonEnum.Medium);
//     expect(component.configHeaderPage.goToCreatePage).toBeTruthy();
//   }));
//
//   it('should showAllRoomList', fakeAsync(() => {
//     component.roomWithoutParentList = rooms;
//
//     component.setSearchableItems();
//     component.showAllRoomList();
//
//     expect(component.allRoomList).toEqual(rooms);
//     expect(component.searchableItems.items).toEqual(rooms);
//     expect(component.showRoomWithoutParentList).toEqual(true);
//   }));
//
//   it('should showParentRoomList', fakeAsync(() => {
//     component.parentRoomList = rooms;
//
//     component.setSearchableItems();
//     component.showParentRoomList();
//
//     expect(component.allRoomList).toEqual(rooms);
//     expect(component.searchableItems.items).toEqual(rooms);
//     expect(component.showRoomWithoutParentList).toEqual(false);
//   }));
//
//   it('should goToCreatePage', fakeAsync(() => {
//     spyOn(component.router, 'navigate');
//     component.goToCreatePage();
//     expect(component.router.navigate).toHaveBeenCalledWith(['new'], { relativeTo: component.route });
//   }));
//
//   it('should set searchableItems', fakeAsync(() => {
//     component.setSearchableItems();
//     expect(component.searchableItems.items).toEqual(component.allRoomList);
//     expect(component.searchableItems.tableRowTypeEnum).toEqual(TableRowTypeEnum.Room);
//     expect(component.searchableItems.placeholderSearchFilter).toEqual('hotelPanelModule.roomList.filterSearchPlaceholder');
//   }));
//
//   it('should updateRoomList empty', () => {
//     component.updateRoomList([]);
//     expect(component.hasRooms).toEqual(false);
//     expect(component.allRoomList).toEqual([]);
//   });
//
//   it('should updateRoomList with data', () => {
//     component.updateRoomList(roomData);
//     expect(component.hasRooms).toEqual(true);
//     expect(component.allRoomList).toEqual(roomData);
//   });
//
//   describe('on getRoomsRegistered', () => {
//     beforeEach(inject([MockBackend, RoomResource, TranslateService], (mockBackend: MockBackend, resourceMock: RoomResource,
// translateService: TranslateService) => {
//       mockEndpoint = mockBackend;
//       roomResourceMock = resourceMock;
//       translateServiceMock = translateService;
//     }));
//
//     it('should execute setBedTypeCountForRooms', fakeAsync(() => {
//       spyOn(component.bedTypeService, 'setBedTypeCountForRooms');
//       spyOn(component, 'setSearchableItems');
//       spyOn(component, 'loadParentRoomList');
//       spyOn(component, 'setColumnsName');
//       const rooms = new Array<Room>();
//       const room = new Room();
//       room.id = 100;
//       rooms.push(room);
//       spyOn(component['roomResource'], 'getRoomsWithoutChildByPropertyId').and.callFake(() => {
//         return new Promise((resolve, reject) => resolve(rooms));
//       });
//
//       component.getRoomsRegistered();
//       tick();
//
//       expect(component.allRoomList).toEqual(rooms);
//       expect(component.hasRooms).toEqual(true);
//       expect(component.bedTypeService.setBedTypeCountForRooms).toHaveBeenCalledWith(component.allRoomList);
//       expect(component.setColumnsName).toHaveBeenCalled();
//       expect(component.setSearchableItems).toHaveBeenCalled();
//       expect(component.loadParentRoomList).toHaveBeenCalled();
//     }));
//   });
//
//   describe('on loadParentRoomList', () => {
//     beforeEach(inject([MockBackend, RoomResource, TranslateService], (mockBackend: MockBackend, resourceMock: RoomResource,
// translateService: TranslateService) => {
//       mockEndpoint = mockBackend;
//       roomResourceMock = resourceMock;
//       translateServiceMock = translateService;
//     }));
//
//     it('should execute emitChange and hasParentRoomList should be true', fakeAsync(() => {
//       spyOn(component.loadPageEmitService, 'emitChange');
//       spyOn(component.roomResource, 'getParentRoomsByPropertyId').and.callFake(() => {
//         return new Promise((resolve, reject) => resolve(roomData));
//       });
//
//       component.loadParentRoomList();
//       tick();
//
//       roomResourceMock.getParentRoomsByPropertyId(propertyId).then((resp) => {
//         expect(component.parentRoomList).toEqual(resp);
//         expect(component.hasParentRoomList).toEqual(true);
//         expect(component.loadPageEmitService.emitChange).toHaveBeenCalled();
//       });
//     }));
//
//     it('should execute emitChange and hasParentRoomList should be false', fakeAsync(() => {
//       spyOn(component.loadPageEmitService, 'emitChange');
//
//       mockEndpoint.connections.subscribe((connection: MockConnection) => {
//         const options = new ResponseOptions({
//           body: JSON.stringify({
//             'result': {}
//           })
//         });
//         connection.mockRespond(new Response(options));
//       });
//       tick();
//
//       component.loadParentRoomList();
//
//       roomResourceMock.getParentRoomsByPropertyId(propertyId).then((resp) => {
//         expect(component.parentRoomList).toEqual(resp);
//         expect(component.hasParentRoomList).toEqual(false);
//         expect(component.loadPageEmitService.emitChange).toHaveBeenCalled();
//       });
//     }));
//
//   });
//
//   describe('on updateRoomStatus', () => {
//
//     beforeEach(inject([MockBackend, RoomResource], (mockBackend: MockBackend, resourceMock: RoomResource) => {
//       mockEndpoint = mockBackend;
//       roomResourceMock = resourceMock;
//     }));
//
//     it('should update room status if isActive true', fakeAsync(() => {
//       spyOn(component.loadPageEmitService, 'emitChange');
//       spyOn(component.toasterEmitService, 'emitChange');
//
//       mockEndpoint.connections.subscribe((connection: MockConnection) => {
//         const options = new ResponseOptions({
//           body: JSON.stringify({
//             'data': roomData[0]
//           })
//         });
//         connection.mockRespond(new Response(options));
//       });
//
//       tick();
//
//       component.updateRoomStatus(roomData[0]);
//       roomResourceMock.updateRoomStatusById(roomData[0].id).then((resp) => {
//         expect(component.toasterEmitService.emitChange).toHaveBeenCalledWith(
//           SuccessError.success,
//           'hotelPanelModule.roomList.roomEnabledWithSuccess'
//         );
//         expect(component.loadPageEmitService.emitChange).toHaveBeenCalled();
//       });
//     }));
//
//     it('should update room status if isActive false', fakeAsync(() => {
//       spyOn(component.loadPageEmitService, 'emitChange');
//       spyOn(component.toasterEmitService, 'emitChange');
//
//       mockEndpoint.connections.subscribe((connection: MockConnection) => {
//         const options = new ResponseOptions({
//           body: JSON.stringify({
//             'data': roomData[1]
//           })
//         });
//         connection.mockRespond(new Response(options));
//       });
//
//       tick();
//
//       component.updateRoomStatus(roomData[1]);
//       roomResourceMock.updateRoomStatusById(roomData[1].id).then((resp) => {
//         expect(component.toasterEmitService.emitChange).toHaveBeenCalledWith(
//           SuccessError.success,
//           'hotelPanelModule.roomList.roomInactivatedWithSuccess'
//         );
//         expect(component.loadPageEmitService.emitChange).toHaveBeenCalled();
//       });
//     }));
//   });
// });
