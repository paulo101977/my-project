// import { tick, async, fakeAsync, inject, ComponentFixture, TestBed } from '@angular/core/testing';
// import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { TranslateModule } from '@ngx-translate/core';
// import { RouterTestingModule } from '@angular/router/testing';
// import { Http, BaseRequestOptions, ResponseOptions, Response } from '@angular/http';
// import { MockBackend, MockConnection } from '@angular/http/testing';
// import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup } from '@angular/forms';
// import { By } from '@angular/platform-browser';
// import { Location } from '@angular/common';
// import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { TranslateService } from '@ngx-translate/core';
// import { roomRoutes as RoomRoutes } from '../../room-routing.module';
// import { roomTypeRoutes as RoomTypeRoutes } from '../../../room-type/room-type-routing.module';
// import { appRoutes as AppRoutes } from '../../../app-routing.module';
// import { RoomCreateComponent } from './room-create.component';
// import { Room } from '../../../shared/models/room';
// import { RoomType } from '../../../shared/models/room-type/room-type';
// import { BedTypeEnum } from '../../../shared/models/bed-type';
// import { ConfigHeaderPage } from '../../../shared/models/config-header-page';
// import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
// import { ShowHide } from '../../../shared/models/show-hide-enum';
// import { SuccessError } from '../../../shared/models/success-error-enum';
// import { BedType } from '../../../shared/models/bed-type';
// import { RoomMapper } from '../../../shared/mappers/room-mapper';
// import { RoomService } from '../../../shared/services/room/room.service';
// import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
// import { SharedService } from '../../../shared/services/shared/shared.service';
// import { RoomTypeService } from '../../../shared/services/room-type/room-type.service';
// import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
// import { CountService } from '../../../shared/services/shared/count.service';
// import { RoomResource } from '../../../shared/resources/room/room.resource';
// import { RoomTypeResource } from '../../../shared/resources/room-type/room-type.resource';
// import { FilterListPipe } from '../../../shared/pipes/filter-select.pipe';
// import { HttpClientModule } from '@angular/common/http';
//
//
// xdescribe('RoomCreateComponent', () => {
//   let component: RoomCreateComponent;
//   let fixture: ComponentFixture<RoomCreateComponent>;
//   let elemHelper, roomResourceMock, mockEndpoint, translateServiceMock;
//   const propertyId = 1;
//   const bedTypeDouble = {
//     bedType: BedTypeEnum.Double,
//     count: 2,
//     optionalLimit: 0
//   };
//
//   const bedTypeReversible = {
//     bedType: BedTypeEnum.Reversible,
//     count: 4,
//     optionalLimit: 0
//   };
//
//   const bedTypeSingle = {
//     bedType: BedTypeEnum.Single,
//     count: 6,
//     optionalLimit: 0
//   };
//
//   const bedTypeCrib = {
//     bedType: BedTypeEnum.Crib,
//     count: 0,
//     optionalLimit: 10
//   };
//
//   const bedTypeExtra = {
//     bedType: BedTypeEnum.Single,
//     count: 0,
//     optionalLimit: 33
//   };
//   const roomTypeMock = new RoomType();
//   roomTypeMock.adultCapacity = 2;
//   roomTypeMock.roomTypeBedTypeList = new Array<BedType>();
//   roomTypeMock.roomTypeBedTypeList.push(bedTypeDouble);
//   roomTypeMock.roomTypeBedTypeList.push(bedTypeReversible);
//   roomTypeMock.roomTypeBedTypeList.push(bedTypeSingle);
//   roomTypeMock.roomTypeBedTypeList.push(bedTypeCrib);
//   roomTypeMock.roomTypeBedTypeList.push(bedTypeExtra);
//   roomTypeMock.childCapacity = 1;
//
//   const roomTypesMockList = new Array<RoomType>();
//   roomTypesMockList.push(roomTypeMock);
//
//   const room = new Room();
//   room.roomNumber = '2017 Totvs';
//   room.floor = 'Andar 1';
//   room.wing = 'Ala A';
//   room.building = 'Bloco A';
//   room.remarks = 'observação';
//   room.isActive = true;
//   room.parentRoom = null;
//   room.roomType = roomTypeMock;
//
//   const conjugateRoomMockList = new Array<Room>();
//   conjugateRoomMockList.push(room);
//
//   beforeAll(() => {
//     TestBed.resetTestEnvironment();
//
//     TestBed
//       .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
//       .configureTestingModule({
//         imports: [
//
//           FormsModule,
//           ReactiveFormsModule,
//           TranslateModule.forRoot(),
//           RouterTestingModule,
//           HttpClientModule
//
//         ],
//         declarations: [RoomCreateComponent, FilterListPipe],
//         providers: [],
//         schemas: [
//           CUSTOM_ELEMENTS_SCHEMA
//         ]
//       });
//
//     fixture = TestBed.createComponent(RoomCreateComponent);
//     component = fixture.componentInstance;
//     elemHelper = fixture.debugElement;
//     fixture.detectChanges();
//   });
//
//   beforeEach(() => {
//     spyOn(component._route.params, 'subscribe').and.callFake((success) => {
//       success({ property: '1' });
//     });
//   });
//
//   it('should create RoomCreateComponent', () => {
//     expect(component).toBeTruthy();
//
//     expect(component.parametersfilterRoomType).toEqual(['abbreviation', 'name']);
//     expect(component.parametersConjugateRoom).toEqual(['roomNumber']);
//   });
//
//   it('should set ConfigHeaderPage', () => {
//     component.setConfigHeaderPage();
//
//     expect(component.configHeaderPage.headerPageEnum).toEqual(HeaderPageEnum.Create);
//     expect(component.configHeaderPage.titlePage).toEqual('hotelPanelModule.roomCreate.title');
//   });
//
//   it('should back preview page', () => {
//     spyOn(component._location, 'back');
//
//     component.goBackPreviewPage();
//
//     expect(component._location.back).toHaveBeenCalled();
//   });
//
//   it('should set formGroupCreateRoom', () => {
//     expect(Object.keys(component.formGroupCreateRoom.controls)).toEqual([
//       'roomType', 'roomNumber', 'floor', 'wing', 'building', 'remarks', 'filterRoomType', 'filterConjugateRoom'
//     ]);
//   });
//
//   it('should fill select field with RoomTypes', fakeAsync(() => {
//     const promiseRoomTypeResource = spyOn(component['roomTypeResource'], 'getRoomTypesByPropertyId').and.callFake(() => {
//       return new Promise((resolve, reject) => resolve(roomTypesMockList));
//     });
//     promiseRoomTypeResource(component.propertyId).then(prop => {
//       component.roomTypeList = prop;
//     });
//     tick();
//     expect(component.roomTypeList).toEqual(roomTypesMockList);
//   }));
//
//   it('should fill select field with Conjugate Rooms', fakeAsync(() => {
//     const promiseConjugateRoomResource = spyOn(component['roomResource'], 'getRoomsByPropertyId').and.callFake(() => {
//       return new Promise((resolve, reject) => resolve(conjugateRoomMockList));
//     });
//     promiseConjugateRoomResource(component.propertyId).then(prop => {
//       component.conjugatedRoomList = prop;
//     });
//     tick();
//     expect(component.conjugatedRoomList).toEqual(conjugateRoomMockList);
//   }));
//
//   it('should return empty RoomTypes and show message to user', fakeAsync(() => {
//     const promiseRoomTypeResource = spyOn(component['roomTypeResource'], 'getRoomTypesByPropertyId').and.callFake(() => {
//       return new Promise((resolve, reject) => resolve([]));
//     });
//     promiseRoomTypeResource(component.propertyId).then(prop => {
//       component.roomTypeList = prop;
//     });
//     tick();
//     expect(component.roomTypeList).toEqual([]);
//     expect(elemHelper.query(By.css('.thf-empty-content-header-title')).nativeElement.innerText)
//       .toBe('hotelPanelModule.roomTypeList.errorMessage');
//   }));
//
//   it('should return empty Conjugate Rooms and show message to user', fakeAsync(() => {
//     const promiseRoomResource = spyOn(component['roomResource'], 'getRoomsByPropertyId').and.callFake(() => {
//       return new Promise((resolve, reject) => resolve([]));
//     });
//     promiseRoomResource(component.propertyId).then(prop => {
//       component.conjugatedRoomList = prop;
//     });
//     tick();
//     expect(component.conjugatedRoomList).toEqual([]);
//   }));
//
//   it('should fill formCreateRoom with roomType and save roomType in room after select it ', () => {
//     component.changeRoomTypeInSelect(roomTypesMockList[0]);
//
//     expect(component.formGroupCreateRoom.get('roomType.adultCapacity').value).toEqual(roomTypesMockList[0].adultCapacity);
//     expect(component.formGroupCreateRoom.get('roomType.childCapacity').value).toEqual(roomTypesMockList[0].childCapacity);
//     expect(component.formGroupCreateRoom.get('roomType.doubleBedCapacity').value)
//       .toEqual(roomTypesMockList[0].roomTypeBedTypeList[0].count);
//     expect(component.formGroupCreateRoom.get('roomType.reversibleBedCapacity').value)
//       .toEqual(roomTypesMockList[0].roomTypeBedTypeList[1].count);
//     expect(component.formGroupCreateRoom.get('roomType.singleBedCapacity').value)
//       .toEqual(roomTypesMockList[0].roomTypeBedTypeList[2].count);
//     expect(component.formGroupCreateRoom.get('roomType.cribBedCapacity').value)
//       .toEqual(roomTypesMockList[0].roomTypeBedTypeList[3].optionalLimit);
//     expect(component.formGroupCreateRoom.get('roomType.extraBedCapacity').value)
//       .toEqual(roomTypesMockList[0].roomTypeBedTypeList[4].optionalLimit);
//
//     expect(component.roomTypeSelected).toEqual(roomTypesMockList[0]);
//   });
//
//   it('should remove roomType from formCreateRoom and remove roomType from room after select it ', () => {
//     component.removeRoomTypeFromSelect();
//
//     expect(component.formGroupCreateRoom.get('roomType.adultCapacity').value).toBeNull();
//     expect(component.formGroupCreateRoom.get('roomType.childCapacity').value).toBeNull();
//     expect(component.formGroupCreateRoom.get('roomType.doubleBedCapacity').value).toBeNull();
//     expect(component.formGroupCreateRoom.get('roomType.reversibleBedCapacity').value).toBeNull();
//     expect(component.formGroupCreateRoom.get('roomType.singleBedCapacity').value).toBeNull();
//     expect(component.formGroupCreateRoom.get('roomType.cribBedCapacity').value).toBeNull();
//     expect(component.formGroupCreateRoom.get('roomType.extraBedCapacity').value).toBeNull();
//
//     expect(component.roomTypeSelected).toBeNull();
//   });
//
//   it('should remove conjugate Room from chosenConjugatedRooms', () => {
//     const chosenConjugatedRoomsMock = new Array<Room>();
//     chosenConjugatedRoomsMock.push(room);
//     component.chosenConjugatedRooms = chosenConjugatedRoomsMock;
//     component.conjugatedRoomList = [];
//
//     component.removeConjugatedRoomFromSelect(room);
//
//     expect(component.chosenConjugatedRooms).toEqual([]);
//     expect(component.chosenConjugatedRooms.length).toEqual(0);
//     expect(component.conjugatedRoomList.length).toEqual(1);
//   });
//
//   it('should insert conjugate Room in chosenConjugatedRooms', () => {
//     component.chosenConjugatedRooms = [];
//     const conjugatedRoomListMock = new Array<Room>();
//     conjugatedRoomListMock.push(room);
//     component.conjugatedRoomList = conjugatedRoomListMock;
//
//     component.chooseConjugateRoom(room);
//
//     expect(component.chosenConjugatedRooms.length).toEqual(1);
//     expect(component.chosenConjugatedRooms).toContain(room);
//     expect(component.conjugatedRoomList.length).toEqual(0);
//   });
//
//   describe('on saveRoom', () => {
//     beforeEach(() => {
//       TestBed.configureTestingModule({
//         imports: [
//
//           FormsModule,
//           ReactiveFormsModule,
//           TranslateModule.forRoot(),
//           RouterTestingModule,
//           HttpClientModule
//         ],
//         declarations: [RoomCreateComponent, FilterListPipe],
//         providers: [],
//         schemas: [
//           CUSTOM_ELEMENTS_SCHEMA
//         ]
//       });
//
//       fixture = TestBed.createComponent(RoomCreateComponent);
//       component = fixture.componentInstance;
//       elemHelper = fixture.debugElement;
//       fixture.detectChanges();
//     });
//
//     beforeEach(inject([MockBackend, RoomResource, TranslateService], (mockBackend: MockBackend, resourceMock: RoomResource,
// translateService: TranslateService) => {
//       mockEndpoint = mockBackend;
//       roomResourceMock = resourceMock;
//       translateServiceMock = translateService;
//       component.roomTypeSelected = roomTypeMock;
//     }));
//
//     it('should save Room', fakeAsync(() => {
//       spyOn(component.loadPageEmitService, 'emitChange');
//       spyOn(component.toasterEmitService, 'emitChange');
//       spyOn(component.route, 'navigate');
//       mockEndpoint.connections.subscribe((connection: MockConnection) => {
//         const options = new ResponseOptions({
//           body: JSON.stringify({
//             'result': room
//           })
//         });
//         connection.mockRespond(new Response(options));
//       });
//       tick();
//
//       component.propertyId = propertyId;
//       component.showConjugatedSelect = false;
//       component.saveRoom();
//
//       roomResourceMock.createRoom(room).then((resp) => {
//         expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.hide);
//         expect(component.route.navigate).toHaveBeenCalledWith(['../'], { relativeTo: component['_route'] });
//         expect(component.toasterEmitService.emitChange).toHaveBeenCalledWith(
//           SuccessError.success,
//           translateServiceMock.instant('hotelPanelModule.roomCreate.createRoomSuccessMessage')
//         );
//       });
//     }));
//
//     it('should show error message', fakeAsync(() => {
//       spyOn(component.toasterEmitService, 'emitChange');
//       spyOn(component.roomService, 'conjugatedRoomListIsInvalid').and.returnValue(true);
//
//       component.showConjugatedSelect = true;
//       component.room = room;
//       component.room.childRoomIdList = null;
//       component.saveRoom();
//
//       expect(component.toasterEmitService.emitChange).toHaveBeenCalledWith(
//         SuccessError.error,
//         translateServiceMock.instant('hotelPanelModule.commomData.errorInConjugatedRoomList')
//       );
//
//     }));
//
//   });
//
//   it('should show items of select when click to open it', () => {
//     expect(component.showItemsSelect).toBeFalsy();
//     component.toogleItemsAreaSelect();
//     expect(component.showItemsSelect).toBeTruthy();
//   });
//
//   it('should show items of select conjugate rooms when click to open it', () => {
//     expect(component.showConjugatedItemsSelect).toBeFalsy();
//     component.toogleConjugatedItemsAreaSelect();
//     expect(component.showConjugatedItemsSelect).toBeTruthy();
//   });
//
//   it('should change value property Filter.Abbreviation after change value of field filter from Form', () => {
//     component.formGroupCreateRoom.get('filterRoomType').setValue('teste');
//     component.observeFilterRoomTypeSelect();
//     expect(component.filterRoomType).toEqual('teste');
//   });
//
//   it('should change value property Filter.RoomNumber after change value of field filter from Form', () => {
//     component.formGroupCreateRoom.get('filterConjugateRoom').setValue('501A');
//     component.observeFilterConjugateRoomSelect();
//     expect(component.filterConjugateRoom).toEqual('501A');
//   });
//
//   it('should count lenght remarks field', () => {
//     spyOn(component.countService, 'setFieldToCount');
//     spyOn(component.countService, 'getLenght').and.returnValue({subscribe: () => { }});
//
//     component.countLenghtRemarksField();
//
//     expect(component.countService.setFieldToCount).toHaveBeenCalled();
//     expect(component.countService.getLenght).toHaveBeenCalled();
//   });
//
//   it('should goToRoomTypeNewPage', () => {
//     spyOn(component.route, 'navigate');
//     component.goToRoomTypeNewPage();
//     expect(component.route.navigate).toHaveBeenCalledWith(['../../room-type', 'new'], { relativeTo: component['_route'] });
//   });
//
//   it('should show conjugateSelect after user check this option', () => {
//     component.showConjugatedSelect = false;
//     spyOn(component, 'loadConjugatedRoomToShowInSelect');
//
//     component.toogleConjugatedSelect();
//
//     expect(component.showConjugatedSelect).toEqual(true);
//     expect(component['loadConjugatedRoomToShowInSelect']).toHaveBeenCalled();
//   });
//
//   it('should not show conjugateSelect after user check this option', () => {
//     component.showConjugatedSelect = true;
//     spyOn(component, 'loadConjugatedRoomToShowInSelect');
//
//     component.toogleConjugatedSelect();
//
//     expect(component.showConjugatedSelect).toEqual(false);
//     expect(component['loadConjugatedRoomToShowInSelect']).not.toHaveBeenCalled();
//   });
//
// });
