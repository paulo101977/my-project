import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { CountService } from '../../../shared/services/shared/count.service';
import { RoomService } from '../../../shared/services/room/room.service';
import { RoomTypeService } from '../../../room-type/shared/services/room-type/room-type.service';
import { RoomMapper } from '../../../shared/mappers/room-mapper';
import { RoomTypeResource } from '../../../room-type/shared/services/room-type-resource/room-type.resource';
import { RoomResource } from '../../../shared/resources/room/room.resource';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { Room } from '../../../shared/models/room';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { BedTypeEnum } from '../../../shared/models/bed-type';
import { ConfigHeaderPage } from '../../../shared/models/config-header-page';
import { HeaderPageEnum } from '../../../shared/models/header-page-enum';

@Component({
  selector: 'app-room-create',
  templateUrl: './room-create.component.html',
})
export class RoomCreateComponent implements OnInit {
  public propertyId: number;
  public room: Room;
  public roomTypeSelected: RoomType;
  public chosenConjugatedRooms: Array<Room>;
  public formGroupCreateRoom: FormGroup;
  public configHeaderPage: ConfigHeaderPage;
  public roomTypeList: Array<RoomType>;
  public conjugatedRoomList: Array<Room>;
  public filterRoomType: string;
  public filterConjugateRoom: string;
  public parametersfilterRoomType: Array<string>;
  public parametersConjugateRoom: Array<string>;
  public showItemsSelect = false;
  public showConjugatedSelect = false;
  public showConjugatedItemsSelect = false;
  public count: 0;
  public limitLenghtFieldRemarks = 4000;

  constructor(
    public translateService: TranslateService,
    public formBuilder: FormBuilder,
    public _route: ActivatedRoute,
    public route: Router,
    public _location: Location,
    public toasterEmitService: ToasterEmitService,
    private roomTypeService: RoomTypeService,
    public roomService: RoomService,
    private roomTypeResource: RoomTypeResource,
    private roomResource: RoomResource,
    public countService: CountService,
    private roomMapper: RoomMapper,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  setVars() {
    this.propertyId = this._route.snapshot.params.property;
    this.loadRoomTypesToShowInSelect(this.propertyId);

    this.formGroupCreateRoom = this.formBuilder.group({
      roomType: this.formBuilder.group({
        adultCapacity: ['', [Validators.required]],
        childCapacity: ['', [Validators.required]],
        doubleBedCapacity: ['', [Validators.required]],
        reversibleBedCapacity: ['', [Validators.required]],
        singleBedCapacity: ['', [Validators.required]],
        cribBedCapacity: ['', [Validators.required]],
        extraBedCapacity: ['', [Validators.required]],
      }),
      roomNumber: ['', [Validators.required, Validators.maxLength(10), Validators.pattern('[a-zA-Z0-9]+')]],
      floor: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]+')]],
      wing: [''],
      building: [''],
      remarks: ['', [Validators.maxLength(4000)]],
      filterRoomType: [''],
      filterConjugateRoom: [''],
    });

    this.setConfigHeaderPage();
    this.observeFilterRoomTypeSelect();
    this.observeFilterConjugateRoomSelect();
    this.countLenghtRemarksField();
    this.parametersfilterRoomType = ['abbreviation', 'name'];
    this.parametersConjugateRoom = ['roomNumber'];
  }

  public setConfigHeaderPage() {
    this.configHeaderPage = new ConfigHeaderPage(HeaderPageEnum.Create);
    this.configHeaderPage.titlePage = 'hotelPanelModule.roomCreate.title';
  }

  private loadRoomTypesToShowInSelect(hotelId: number) {
    this.roomTypeResource.getRoomTypesByPropertyId(this.propertyId).subscribe(roomTypes => {
      if (roomTypes.length > 0) {
        this.roomTypeList = roomTypes;
      }
    });
  }

  private loadConjugatedRoomToShowInSelect(hotelId: number) {
    this.roomResource.getRoomsWithoutParentAndChildrenByPropertyId(this.propertyId).subscribe(conjugatedRooms => {
      if (conjugatedRooms.length > 0) {
        this.conjugatedRoomList = conjugatedRooms;
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.error,
          this.translateService.instant('hotelPanelModule.conjugatedRoom.thereIsNoConjugatedRoom'),
        );
      }
    });
  }

  public changeRoomTypeInSelect(roomType: RoomType) {
    this.formGroupCreateRoom.get('roomType.adultCapacity').setValue(roomType.adultCapacity);
    this.formGroupCreateRoom.get('roomType.childCapacity').setValue(roomType.childCapacity);
    this.formGroupCreateRoom
      .get('roomType.doubleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(roomType, BedTypeEnum.Double));
    this.formGroupCreateRoom
      .get('roomType.reversibleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(roomType, BedTypeEnum.Reversible));
    this.formGroupCreateRoom
      .get('roomType.singleBedCapacity')
      .setValue(this.roomTypeService.getQuantityBedList(roomType, BedTypeEnum.Single));
    this.formGroupCreateRoom
      .get('roomType.cribBedCapacity')
      .setValue(this.roomTypeService.getQuantityOptionalBedList(roomType, BedTypeEnum.Crib));
    this.formGroupCreateRoom
      .get('roomType.extraBedCapacity')
      .setValue(this.roomTypeService.getQuantityOptionalBedList(roomType, BedTypeEnum.Single));

    this.roomTypeSelected = roomType;
    this.toogleItemsAreaSelect();
  }

  public chooseConjugateRoom(room: Room) {
    if (this.chosenConjugatedRooms == null) {
      this.chosenConjugatedRooms = new Array<Room>();
    }
    this.chosenConjugatedRooms.push(room);
    if (this.conjugatedRoomList.find(conjugatedRoom => conjugatedRoom.id === room.id)) {
      this.conjugatedRoomList.splice(this.conjugatedRoomList.indexOf(room), 1);
    }
    this.toogleConjugatedItemsAreaSelect();
  }

  public removeRoomTypeFromSelect() {
    this.formGroupCreateRoom.get('roomType').reset();
    this.roomTypeSelected = null;
  }

  public removeConjugatedRoomFromSelect(room: Room) {
    if (this.chosenConjugatedRooms.find(chosenConjugatedRoom => chosenConjugatedRoom.id === room.id)) {
      this.chosenConjugatedRooms.splice(this.chosenConjugatedRooms.indexOf(room), 1);
      if (this.conjugatedRoomList != null) {
        this.conjugatedRoomList.push(room);
      }
    }
  }

  public toogleItemsAreaSelect() {
    this.showItemsSelect = !this.showItemsSelect;
  }

  public toogleConjugatedSelect() {
    this.showConjugatedSelect = !this.showConjugatedSelect;

    if (this.showConjugatedSelect) {
      this.loadConjugatedRoomToShowInSelect(this.propertyId);
    } else {
      this.chosenConjugatedRooms = null;
    }
  }

  public toogleConjugatedItemsAreaSelect() {
    this.showConjugatedItemsSelect = !this.showConjugatedItemsSelect;
  }

  public observeFilterRoomTypeSelect() {
    const control = this.formGroupCreateRoom.get('filterRoomType');
    control.valueChanges.subscribe(value => {
      this.filterRoomType = value;
    });
  }

  public observeFilterConjugateRoomSelect() {
    const control = this.formGroupCreateRoom.get('filterConjugateRoom');
    control.valueChanges.subscribe(value => {
      this.filterConjugateRoom = value;
    });
  }

  public saveRoom() {
    if (this.showConjugatedSelect && this.roomService.conjugatedRoomListIsInvalid(this.chosenConjugatedRooms)) {
      this.toasterEmitService.emitChange(
        SuccessError.error,
        this.translateService.instant('hotelPanelModule.commomData.errorInConjugatedRoomList'),
      );
    } else {
      this.setRoomToSendServer();
      this.roomResource.createRoom(this.room).subscribe(room => {
        this.route.navigate(['../'], { relativeTo: this._route });
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('hotelPanelModule.roomCreate.createRoomSuccessMessage'),
        );
      });
    }
  }

  private setRoomToSendServer() {
    this.room = this.roomMapper.getNewRoomByFormGroupToSendServer(this.propertyId, this.formGroupCreateRoom, this.chosenConjugatedRooms);
    this.room.roomType = this.roomTypeSelected;
    this.room.roomTypeId = this.roomTypeSelected.id;
  }

  public countLenghtRemarksField() {
    this.countService.setFieldToCount(this.formGroupCreateRoom.get('remarks'));
    this.countService.getLenght().subscribe(response => {
      this.count = response.count;
    });
  }

  public goBackPreviewPage() {
    this._location.back();
  }

  public goToRoomTypeNewPage() {
    this.route.navigate(['../../room-type', 'new'], { relativeTo: this._route });
  }
}
