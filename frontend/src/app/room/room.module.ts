import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { RoomRoutingModule } from './room-routing.module';
import { SharedModule } from '../shared/shared.module';

import { RoomListComponent } from './components/room-list/room-list.component';
import { RoomCreateComponent } from './components/room-create/room-create.component';
import { RoomEditComponent } from './components/room-edit/room-edit.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, FormsModule, RouterModule, SharedModule, RoomRoutingModule],
  declarations: [RoomListComponent, RoomCreateComponent, RoomEditComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class RoomModule {}
