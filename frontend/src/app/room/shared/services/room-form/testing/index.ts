import { createServiceStub } from 'testing';
import { RoomFormService } from '../room-form.service';
import { FormGroup } from '@angular/forms';
import { Room } from '@app/shared/models/room';

export const roomFormStub = createServiceStub(RoomFormService, {
    form: new FormGroup({}),
    getRoom: (propertyId: number) => new Room()
});
