import { TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { configureTestSuite } from 'ng-bullet';
import { RoomFormService } from './room-form.service';
import { roomFormStub } from './testing';

describe('RoomFormService', () => {
  let service: RoomFormService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [roomFormStub, FormBuilder],
    });

    service = TestBed.get(RoomFormService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should getRoom', () => {
    const propertyId = 1;
    const getRoom = service.getRoom(propertyId);

    expect(getRoom).toBeTruthy();
  });
});
