import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NoWhitespaceValidator } from 'app/shared/validators/no-whitespace-validator';
import { Room } from '@app/shared/models/room';


@Injectable({ providedIn: 'root' })
export class RoomFormService {
  public form: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.form = this.getForm();
  }

  private getForm(): FormGroup {
    return this.formBuilder.group({
      uhType: [null, [Validators.required]],
      roomNumber: [null, [Validators.required, Validators.maxLength(10), NoWhitespaceValidator.match]],
      floor: [null, [Validators.required, Validators.maxLength(10), NoWhitespaceValidator.match]],
      building: [null],
      wing: [null],
      remarks: [null, [Validators.maxLength(200)]]
    });
  }

  public getRoom(propertyId: number): Room {
    const { uhType, roomNumber, floor, building, wing, remarks } = this.form.value;

    return {
      ...uhType,
      roomTypeId: uhType.id,
      propertyId,
      roomNumber,
      floor,
      building,
      wing,
      remarks,
      childRoomIdList: [],
      parentRoom: null,
      isActive: true,
      isInUse: false,
      roomType: uhType
    };
  }
}
