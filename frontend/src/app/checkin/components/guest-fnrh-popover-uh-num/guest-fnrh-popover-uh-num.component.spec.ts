
import {throwError as observableThrowError, of as observableOf,  Observable } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { GuestFnrhPopoverUhNumComponent } from './guest-fnrh-popover-uh-num.component';
import { CheckInNumUhsDto } from '../../../shared/models/dto/checkin/checkin-num-uhs-dto';
import { CheckInHeaderDto } from '../../../shared/models/dto/checkin/checkin-header-dto';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { CheckInRoomTypesDto } from '../../../shared/models/dto/checkin/checkin-room-types-dto';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CoreModule } from '@app/core/core.module';

describe('GuestFnrhPopoverUhNumComponent', () => {
  let component: GuestFnrhPopoverUhNumComponent;
  let fixture: ComponentFixture<GuestFnrhPopoverUhNumComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), RouterTestingModule, HttpClientTestingModule, CoreModule],
      declarations: [GuestFnrhPopoverUhNumComponent],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(GuestFnrhPopoverUhNumComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      spyOn<any>(component['showHideContentEmitService'], 'changeEmitted$').and.callFake(() => {});

      expect(component).toBeTruthy();
      expect(component.togglePopoverHidden).toBeTruthy();
    });

    it('should togglePopover', () => {
      const response = {
        items: [
          {
            id: 1,
            roomNumber: '100',
            roomTypeId: 2,
          },
        ],
      };
      spyOn<any>(component['checkinResource'], 'getUhNums').and.returnValue(observableOf(response));

      component.roomTypeActive = new CheckInRoomTypesDto();
      component.roomTypeActive.id = 1;
      component.checkInHeaderDto = new CheckInHeaderDto();

      component.togglePopover();

      expect(component.checkInHeaderDto.receivedRoomTypeId).toEqual(component.roomTypeActive.id);
      expect<any>(component.roomList).toEqual(response.items);
      expect<any>(component.roomList).toEqual(response.items);
      expect(component.hasRoomList).toBeTruthy();
      expect(component.togglePopoverHidden).toBeFalsy();
    });

    it('should clickNumUhList and save with success', () => {
      spyOn(document, 'querySelector').and.returnValue(null);
      spyOn<any>(component['checkinResource'], 'associateRoomToReservationItem').and.returnValue(observableOf({}));
      spyOn(component['toasterEmitService'], 'emitChange');
      spyOn(component['checkinService'], 'uhChosen');

      component.checkInHeaderDto = new CheckInHeaderDto();
      const $event = {
        target: {
          classList: {
            add: function() {},
          },
        },
      };

      const item = new CheckInNumUhsDto();
      item.roomNumber = '1121212';

      component.clickNumUhList($event, item);
      expect(component.uhActive).toEqual(item);
      expect(component['checkinResource'].associateRoomToReservationItem).toHaveBeenCalledWith(
        component['propertyId'],
        component.checkInHeaderDto.reservationItemId,
        component.uhActive.roomTypeId,
        component.uhActive.id,
      );
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(SuccessError.success, 'checkinModule.header.successMessage');
      expect(component['checkinService'].uhChosen).toHaveBeenCalledWith(component.uhActive);
    });

    it('should clickNumUhList and save with error', () => {
      spyOn(document, 'querySelector').and.returnValue(null);
      spyOn(component['checkinResource'], 'associateRoomToReservationItem').and.returnValue(observableThrowError({}));
      spyOn(component['toasterEmitService'], 'emitChange');
      spyOn(component['checkinService'], 'uhChosen');

      const errorMessage = 'Mensagem de erro';
      spyOn<any>(component, 'getErrorMessage').and.returnValue(errorMessage);

      component.checkInHeaderDto = new CheckInHeaderDto();
      const $event = {
        target: {
          classList: {
            add: function() {},
          },
        },
      };

      const item = new CheckInNumUhsDto();
      item.roomNumber = '1121212';

      component.clickNumUhList($event, item);
      expect(component.uhActive).toEqual(item);
      expect(component['checkinResource'].associateRoomToReservationItem).toHaveBeenCalledWith(
        component['propertyId'],
        component.checkInHeaderDto.reservationItemId,
        component.uhActive.roomTypeId,
        component.uhActive.id,
      );
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(SuccessError.error, errorMessage);
      expect(component['checkinService'].uhChosen).toHaveBeenCalledWith(component.uhActive);
    });

    it('should keepPopoverOpen', () => {
      component.keepPopoverOpen();

      expect(component.togglePopoverHidden).toBeFalsy();
    });
  });

  it('should ngOnChanges', () => {
    component.checkInHeaderDto = new CheckInHeaderDto();
    component.checkInHeaderDto.roomNumber = '200';

    component.ngOnChanges({});

    expect(component.uhActive.roomNumber).toEqual(component.checkInHeaderDto.roomNumber);
  });
});
