import { Component, OnInit, Output, Input, EventEmitter, OnChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { CheckInNumUhsDto } from '../../../shared/models/dto/checkin/checkin-num-uhs-dto';
import { CheckInHeaderDto } from '../../../shared/models/dto/checkin/checkin-header-dto';
import { CheckInRoomTypesDto } from '../../../shared/models/dto/checkin/checkin-room-types-dto';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ErrorMessageHandlerAPI } from '../../../shared/models/error-message/error-message-handler-api';
import { ShowHideContentEmitService } from '../../../shared/services/shared/show-hide-content-emit-service';
import { CheckinService } from 'app/shared/services/checkin/checkin.service';
import { CheckinResource } from '../../../shared/resources/checkin/checkin.resource';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';

@Component({
  selector: 'app-guest-fnrh-popover-uh-num',
  templateUrl: './guest-fnrh-popover-uh-num.component.html',
  styleUrls: ['./guest-fnrh-popover-uh-num.component.css'],
})
export class GuestFnrhPopoverUhNumComponent implements OnInit, OnChanges {
  @Input() roomTypeActive: CheckInRoomTypesDto;
  @Input() checkInHeaderDto: CheckInHeaderDto;
  @Input() isWalkin: boolean;

  public roomList: Array<CheckInNumUhsDto>;
  public uhActive: CheckInNumUhsDto;
  public togglePopoverHidden: boolean;
  public hasRoomList: boolean;

  private propertyId: number;

  constructor(
    private checkinResource: CheckinResource,
    private toasterEmitService: ToasterEmitService,
    private showHideContentEmitService: ShowHideContentEmitService,
    private translateService: TranslateService,
    private checkinService: CheckinService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.showHideContentEmitService.changeEmitted$.subscribe(response => {
      this.togglePopoverHidden = true;
    });
    this.setVars();
  }

  ngOnChanges(changes: any) {
    if (this.checkInHeaderDto) {
      if (this.checkInHeaderDto.roomNumber) {
        this.uhActive = new CheckInNumUhsDto();
        this.uhActive.roomNumber = this.checkInHeaderDto.roomNumber;
        this.checkinService.uhChosen(this.uhActive);
      } else {
        this.setUhActiveToEmpty();
      }
    }
  }

  private setVars() {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.setUhActiveToEmpty();
    });
  }

  private setUhActiveToEmpty(): void {
    this.uhActive = new CheckInNumUhsDto();
    this.togglePopoverHidden = true;
    this.translateService.get('checkinModule.popovers.uhNums.titlePopover').subscribe(value => {
      this.uhActive.roomNumber = value;
    });
    this.checkinService.uhChosen(this.uhActive);
  }

  public clickNumUhList($event, uh: CheckInNumUhsDto) {
    const elementListActive = document.querySelector('.header-details-popover-list--numuhs.active');

    if (elementListActive) {
      elementListActive.classList.remove('active');
    }

    $event.target.classList.add('active');
    this.uhActive = uh;
    this.togglePopoverHidden = !this.togglePopoverHidden;
    this.associateRoomToReservationItem();
    this.checkinService.uhChosen(this.uhActive);
  }

  private getNumUhs() {
    this.checkInHeaderDto.receivedRoomTypeId = this.roomTypeActive.id;
    this.checkinResource.getUhNums(this.checkInHeaderDto, this.propertyId).subscribe(response => {
      this.roomList = response.items;
      this.hasRoomList = this.roomList && this.roomList.length > 0;
    });
  }

  private checkIfInputCheckInHeaderDtoWasModified(changes) {
    // if (changes.checkInHeaderDto && !changes.checkInHeaderDto.firstChange) {
    //   if (changes.checkInHeaderDto.currentValue.roomNumber !== '') {
    //     this.uhActive = changes.checkInHeaderDto.currentValue;
    //     if (this.isWalkin) {
    //       this.setVars();
    //     }
    //   }
    //   this.checkinService.uhChosen(this.uhActive);
    // }
  }

  private associateRoomToReservationItem(): void {
    this.checkinResource
      .associateRoomToReservationItem(this.propertyId, this.checkInHeaderDto.reservationItemId, this.uhActive.roomTypeId, this.uhActive.id)
      .subscribe(
        response => {
          this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant('checkinModule.header.successMessage'));
        },
        error => {
          const errorMessage = this.getErrorMessage(error);
          this.toasterEmitService.emitChange(SuccessError.error, errorMessage);
        },
      );
  }

  private getErrorMessage(error): string {
    return new ErrorMessageHandlerAPI(error).getErrorMessage();
  }

  public togglePopover() {
    if (this.togglePopoverHidden) {
      this.getNumUhs();
    }

    this.togglePopoverHidden = !this.togglePopoverHidden;
  }

  public keepPopoverOpen(): void {
    this.togglePopoverHidden = false;
  }
}
