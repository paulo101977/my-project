import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GuestFnrhSidebarComponent } from '../sidebar/guest-fnrh-sidebar/guest-fnrh-sidebar.component';
import { CheckInRoomTypesDto } from 'app/shared/models/dto/checkin/checkin-room-types-dto';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';
import { ReservationRemark } from 'app/shared/models/reserves/reservation-remark';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { CheckInNumUhsDto } from 'app/shared/models/dto/checkin/checkin-num-uhs-dto';
import { ErrorMessageHandlerAPI } from 'app/shared/models/error-message/error-message-handler-api';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';
import { ModalRemarksFormEmitService } from 'app/shared/services/shared/modal-remarks-form-emit.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { CheckinService } from 'app/shared/services/checkin/checkin.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { ReservationResource } from 'app/shared/resources/reservation/reservation.resource';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';

@Component({
  selector: 'app-guest-fnrh-header',
  templateUrl: './guest-fnrh-header.component.html',
  styleUrls: ['./guest-fnrh-header.component.scss']
})
export class GuestFnrhHeaderComponent implements OnInit, OnChanges {
  private MODAL_TYPE_UH_TAX = 'modalTypeUhTax';

  private ID_CHEKIN_STATUS = 2;
  private _dataHeader: CheckInHeaderDto;
  @Input()
  set dataHeader(data: CheckInHeaderDto) {
    this._dataHeader = data;
    if (data) {
      this._dataHeader.overnight = this.dateService.calculateOvernightStay(data.arrivalDate, data.departureDate);
      if (data.reservationItemBudgetHeaderDto && data.reservationItemBudgetHeaderDto.commercialBudgetList) {
        if (data.reservationItemBudgetHeaderDto.commercialBudgetList.length > 0) {
          this.agreementName = data.reservationItemBudgetHeaderDto.commercialBudgetList[0].commercialBudgetName;
          this.agreementTotal = data.reservationItemBudgetHeaderDto.commercialBudgetList[0].totalBudget;
          this.agreementCurrencySymbol = data.reservationItemBudgetHeaderDto.commercialBudgetList[0].currencySymbol;
        }
      }
    }
  }
  get dataHeader(): CheckInHeaderDto {
    return this._dataHeader;
  }

  @Input() isWalkin: boolean;
  @Input() sidebar: GuestFnrhSidebarComponent;
  @Output() reservationBudgetClicked = new EventEmitter<any>();

  private _checkInHeaderDto: CheckInHeaderDto;

  set checkInHeaderDto(checkin: CheckInHeaderDto) {
    this._checkInHeaderDto = checkin;
    if (this.checkInHeaderDto.roomNumber && this.checkInHeaderDto.roomNumber != '') {
      this.numberRoomElementSelected = new CheckInNumUhsDto();
      this.numberRoomElementSelected.roomNumber = this.checkInHeaderDto.roomNumber;
      this.dataHeader.overnight = this.dateService.calculateOvernightStay(
        this.checkInHeaderDto.arrivalDate,
        this.checkInHeaderDto.departureDate
      );
    } else {
      this.selectedOptionTypeNumber = null;
      this.setUhActiveToEmpty();
    }
  }
  get checkInHeaderDto(): CheckInHeaderDto {
    return this._checkInHeaderDto;
  }
  public privateRoomTypeActive: CheckInRoomTypesDto;
  public roomTypesList: Array<CheckInRoomTypesDto>;
  public roomNumbersList: Array<CheckInNumUhsDto>;
  public selectRoomTypesList: Array<SelectObjectOption>;
  public selectedOptionTypeUHId: number;
  public selectedOptionTypeNumber: string;
  public selectRoomNumberList: Array<SelectObjectOption>;
  public hasZeroRemarks: boolean;
  public hasOneRemarks: boolean;
  public hasTwoRemarks: boolean;
  public roomTypeSelected: CheckInRoomTypesDto;
  public roomNumberSelected: any;
  public propertyId: number;
  public isDisabled: boolean;
  public buttonModalOk: ButtonConfig;

  public agreementName = '-';
  public agreementTotal: number;
  public agreementCurrencySymbol: string;

  public numberRoomElementSelected: CheckInNumUhsDto;

  constructor(
    private reservationService: ReservationService,
    private modalRemarksFormEmitService: ModalRemarksFormEmitService,
    private modalToggleService: ModalEmitToggleService,
    private reservationResource: ReservationResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private checkinResource: CheckinResource,
    private activateRoute: ActivatedRoute,
    public commomService: CommomService,
    private checkinService: CheckinService,
    private sharedService: SharedService,
    private dateService: DateService,
  ) {
    this.roomTypesList = [];
    this.selectRoomTypesList = [];
    this.selectRoomNumberList = [];
  }

  ngOnInit() {
    this.propertyId = this.activateRoute.snapshot.params.property;
    this.setvars();
    this.buttonModalOk = this.sharedService.getButtonConfig(
      'modal-tax',
      this.toggleModal.bind(this),
      'commomData.ok',
      ButtonType.Secondary
    );
  }

  ngOnChanges(changes: any) {
    this.setvars();
  }

  public setvars() {
    if (this.dataHeader) {
      this.checkInHeaderDto = this.dataHeader;
      this.isDisabled = this.dataHeader.reservationItemStatusId == this.ID_CHEKIN_STATUS;
      this.loadUHtypes();
      this.setRemarksQuantity();
    }
  }

  private setRemarksQuantity(): void {
    this.hasZeroRemarks = this.checkInHeaderDto.numberObservations == 0;
    this.hasOneRemarks = this.checkInHeaderDto.numberObservations == 1;
    this.hasTwoRemarks = this.checkInHeaderDto.numberObservations == 2;
  }

  public subscribeRoomTypeActive(item: CheckInRoomTypesDto) {
    this.privateRoomTypeActive = item;
  }

  public getStatusColor(): any {
    if (this.checkInHeaderDto) {
      const status = <ReservationStatusEnum>this.checkInHeaderDto.reservationItemStatusId;
      return this.reservationService.getStatusColorByReservationStatus(status);
    }
  }

  public showRemarksModal(): void {
    this.modalRemarksFormEmitService
      .emitSimpleModal(
        this.checkInHeaderDto.internalComments,
        this.checkInHeaderDto.externalComments,
        this.checkInHeaderDto.partnerComments);
  }

  public saveInternalCommentsAndExternalComments(reservationRemark: ReservationRemark): void {
    this.reservationResource.updateRemarks(this.checkInHeaderDto.id, reservationRemark).subscribe(response => {
      this.checkInHeaderDto.externalComments = reservationRemark.externalComments;
      this.checkInHeaderDto.internalComments = reservationRemark.internalComments;
      this.checkInHeaderDto.numberObservations = this.reservationService.getRemarksQuantityOfReservation(
        this.checkInHeaderDto.externalComments,
        this.checkInHeaderDto.internalComments
      );
      this.setRemarksQuantity();
      this.toasterEmitService
        .emitChange(SuccessError.success, this.translateService.instant('checkinModule.header.remark.successMessage'));
    });
  }

  public loadUHtypes() {
    this.checkinResource.getRoomTypes(this.propertyId, this.checkInHeaderDto)
      .subscribe((result) => {
        this.roomTypesList = result.items;
        this.selectRoomTypesList = this.roomTypesList.map( item => {
          const option = new SelectObjectOption();
          option.key = item.id;
          option.value = item.abbreviation ;
          return option;
        });

        if (this.selectedOptionTypeUHId && this.selectedOptionTypeUHId === this.dataHeader.receivedRoomTypeId) {
          this.UHTypeSelected(this.selectedOptionTypeUHId);
        } else {
          this.selectedOptionTypeUHId = this.dataHeader.receivedRoomTypeId;
        }
      }, error => {
        const option = new SelectObjectOption();
        option.key = this.dataHeader.receivedRoomTypeId;
        option.value = this.dataHeader.receivedRoomTypeAbbreviation;

        const checkinRoomType = new CheckInRoomTypesDto();
        checkinRoomType.id = this.dataHeader.receivedRoomTypeId;
        checkinRoomType.name = this.dataHeader.receivedRoomTypeAbbreviation;

        this.selectRoomTypesList = [option];
        this.roomTypesList = [checkinRoomType];
        this.selectedOptionTypeUHId = this.dataHeader.receivedRoomTypeId;
      }
    );
  }

  public UHTypeSelected(optionSelected) {
    if (optionSelected) {

      if (this.roomTypeSelected && this.roomTypeSelected.id != optionSelected) {
        this.setUhActiveToEmpty();
      }

      this.roomTypeSelected = this.roomTypesList.filter(item => item.id == optionSelected)[0];
      this.loadUHsFromType(this.roomTypeSelected);
    } else {
      this.roomTypeSelected = null;
      this.cleanRoomNumberSelection();
    }
  }

  private loadUHsFromType(type) {
    this.checkinResource
      .getUhNumsByTypeAndPeriod(type.id,
        this.checkInHeaderDto.arrivalDate,
        this.checkInHeaderDto.departureDate,
        this.propertyId,
        this.dataHeader.reservationItemId)
      .subscribe(response => {
        this.notifyTaxUnmodified(type.id);
        this.roomNumbersList = response.items;
        this.selectedOptionTypeNumber = this.dataHeader.roomNumber;
        this.selectRoomNumberList = this.roomNumbersList.map(item => {
          const option = new SelectObjectOption();
          option.key = item.roomNumber;
          option.value =
            '<div class="thf-u-width-flex" >' +
            item.roomNumber +
            '<span class="float-right  "  style="color: #' +
            item.color +
            '  " >' +
            item.housekeepingStatusName +
            '</span></div>';
          return option;
        });
      });
  }

  private notifyTaxUnmodified(typeId) {
    if (this.checkInHeaderDto.receivedRoomTypeId && this.checkInHeaderDto.receivedRoomTypeId != typeId) {
      this.modalToggleService.emitToggleWithRef(this.MODAL_TYPE_UH_TAX);
    }
  }

  public toggleModal() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_TYPE_UH_TAX);
  }

  public associateRoomToReservationItem(element): void {
    this.numberRoomElementSelected = this.roomNumbersList && this.roomNumbersList.length > 0
    ? this.roomNumbersList.filter(item => item.roomNumber == element)[0] : null;

    this.checkinService.uhChosen(this.numberRoomElementSelected);

    if (this.checkInHeaderDto.roomNumber != element) {
      this.checkinResource
        .associateRoomToReservationItem(
          this.propertyId,
          this.checkInHeaderDto.reservationItemId,
          this.roomTypeSelected.id,
          this.numberRoomElementSelected.id
        )
        .subscribe(
          response => {
            this.toasterEmitService
              .emitChange(SuccessError.success, this.translateService.instant('checkinModule.header.successMessage'));
          },
          error => {
            const errorMessage = this.getErrorMessage(error);
            this.toasterEmitService.emitChange(SuccessError.error, errorMessage);
          }
        );
    }
  }

  private getErrorMessage(error): string {
    return new ErrorMessageHandlerAPI(error).getErrorMessage();
  }

  private setUhActiveToEmpty(): void {
    this.numberRoomElementSelected = new CheckInNumUhsDto();
    this.numberRoomElementSelected.roomNumber = null;
    this.checkinService.uhChosen(this.numberRoomElementSelected);
  }

  public cleanRoomNumberSelection() {
    this.selectRoomNumberList = [];
    this.roomNumberSelected = null;
    this.roomNumbersList = [];
  }

  public isCheckinStatus(checkinStatus: number) {
    return checkinStatus == ReservationStatusEnum.Checkin;
  }

  public goToReservationBudget() {
    this.reservationBudgetClicked.next();
  }
}
