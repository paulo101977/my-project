import { ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {of} from 'rxjs';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MomentModule} from 'ngx-moment';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  GuestFnrhHeaderComponent
} from 'app/checkin/components/guest-fnrh-header/guest-fnrh-header.component';
import {
  GuestFnrhSidebarComponent
} from 'app/checkin/components/sidebar/guest-fnrh-sidebar/guest-fnrh-sidebar.component';
import {
  DATA,
  GUEST_RESERVATION_BUDGET,
  ROOM_TYPES_LIST,
  UHS_FROM_TYPE_LIST
} from 'app/checkin/mock-data';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import {
  ActivatedRouteStub,
  createPipeStub,
  createServiceStub, currencyFormatPipe, imgCdnStub,
  modalRemarksFormEmitServiceStub,
  modalToggleServiceStub, padStub,
  sharedServiceStub,
  toasterEmitServiceStub
} from '../../../../../testing';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';
import { ReservationResource } from 'app/shared/resources/reservation/reservation.resource';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import { ActivatedRoute } from '@angular/router';
import { CheckinService } from 'app/shared/services/checkin/checkin.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { ReservationRemark } from 'app/shared/models/reserves/reservation-remark';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { CheckInNumUhsDto } from 'app/shared/models/dto/checkin/checkin-num-uhs-dto';

// TODO: create and move to respective folder [DEBIT: 7506]
const reservationServiceStub = createServiceStub(ReservationService, {
  getStatusColorByReservationStatus: (status: ReservationStatusEnum | number) => ({color: ''}),
  getRemarksQuantityOfReservation: (...comments) => 12
});

const reservationResourceStub = createServiceStub(ReservationResource, {
  updateRemarks: (reservationId: number, reservationRemark: ReservationRemark) => of(null)
});

const checkinResourceStub =  createServiceStub(CheckinResource, {
  getRoomTypes: (propertyId: number, data: CheckInHeaderDto) => of( null ),
  getUhNumsByTypeAndPeriod: (
    roomType: number,
    arrivalData: any,
    departureDate: any,
    propertyId: number,
    reservationItemId?: number
  ) => of( null )
});

const dateServiceStub = createServiceStub(DateService, {
  calculateOvernightStay: (startDate, endDate) => 0
});

const checkinServiceStub = createServiceStub(CheckinService, {
  uhChosen(uh: CheckInNumUhsDto) {}
});


describe('GuestFnrhHeaderComponent', () => {
  let component: GuestFnrhHeaderComponent;
  let fixture: ComponentFixture<GuestFnrhHeaderComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        GuestFnrhHeaderComponent,
        imgCdnStub,
        padStub,
        currencyFormatPipe
      ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        MomentModule,
      ],
      providers: [
        reservationServiceStub,
        modalToggleServiceStub,
        toasterEmitServiceStub,
        sharedServiceStub,
        modalRemarksFormEmitServiceStub,
        reservationResourceStub,
        checkinResourceStub,
        dateServiceStub,
        checkinServiceStub,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  beforeAll(() => {
    fixture = TestBed.createComponent(GuestFnrhHeaderComponent);
    component = fixture.componentInstance;

    component.sidebar = <GuestFnrhSidebarComponent>{
      sidebarReal: null
    };
    component.propertyId = 1;
    component.dataHeader = DATA[GUEST_RESERVATION_BUDGET];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn<any>(component, 'setvars');
    component.ngOnInit();
    expect(component.setvars).toHaveBeenCalled();
  });

  it('should setvars', () => {
    spyOn<any>(component, 'loadUHtypes');
    spyOn<any>(component, 'setRemarksQuantity');
    component.setvars();
    expect(component.checkInHeaderDto).toEqual(component.dataHeader);
    expect(component.isDisabled).toEqual(true);
    expect(component.loadUHtypes).toHaveBeenCalled();
    expect(component['setRemarksQuantity']).toHaveBeenCalled();
  });

  describe('should loadUHtypes', () => {
    it('receivedRoomTypeId === selectedOptionTypeUHId', fakeAsync (() => {
      const response = {
        items: DATA[ROOM_TYPES_LIST]
      };

      spyOn<any>(component, 'UHTypeSelected');
      spyOn<any>(component['checkinResource'], 'getRoomTypes').and.returnValue(of(response));

      component.dataHeader.receivedRoomTypeId = component.selectedOptionTypeUHId = 2;
      component.loadUHtypes();
      tick();

      expect(component.roomTypesList.length).toEqual(2);
      expect(component.selectRoomTypesList[1].value).toEqual('BGR');
      expect(component.UHTypeSelected).toHaveBeenCalled();
    }));

    it('receivedRoomTypeId != selectedOptionTypeUHId', fakeAsync (() => {
      const response = {
        items: DATA[ROOM_TYPES_LIST]
      };

      spyOn<any>(component, 'UHTypeSelected');
      spyOn<any>(component['checkinResource'], 'getRoomTypes').and.returnValue(of(response));

      component.selectedOptionTypeUHId = null;

      component.loadUHtypes();
      tick();

      expect(component.UHTypeSelected).not.toHaveBeenCalled();
    }));
  });

  describe('should UHTypeSelected', () => {
    it('optionSelected is null', () => {
      spyOn<any>(component, 'cleanRoomNumberSelection');

      component.UHTypeSelected(null);

      expect(component.roomTypeSelected).toEqual(null);
      expect(component.cleanRoomNumberSelection).toHaveBeenCalled();
    });

    it('have object and roomTypeSelected != null', fakeAsync(() => {
      spyOn<any>(component, 'loadUHsFromType');
      spyOn<any>(component, 'cleanRoomNumberSelection');
      spyOn<any>(component, 'setUhActiveToEmpty');

      const roomType = DATA[ROOM_TYPES_LIST];
      component.roomTypesList = roomType;
      component.roomTypeSelected = roomType[1];
      component.UHTypeSelected(roomType[0].id);

      expect(component['setUhActiveToEmpty']).toHaveBeenCalled();
      expect(component.roomTypeSelected.abbreviation).toEqual('LXO');
      expect(component['loadUHsFromType']).toHaveBeenCalled();
      expect(component.cleanRoomNumberSelection).not.toHaveBeenCalled();
    }));

    it('have object and roomTypeSelected == null', fakeAsync(() => {
      spyOn<any>(component, 'loadUHsFromType');
      spyOn<any>(component, 'setUhActiveToEmpty');

      const roomType = DATA[ROOM_TYPES_LIST];
      component.roomTypesList = roomType;
      component.roomTypeSelected = null;
      component.UHTypeSelected(roomType[0].id);

      expect(component['setUhActiveToEmpty']).not.toHaveBeenCalled();
      expect(component['loadUHsFromType']).toHaveBeenCalled();
    }));
  });

  it('should loadUHsFromType', fakeAsync(() => {
    const uhFromType = { items: DATA[UHS_FROM_TYPE_LIST]};
    spyOn<any>(component['checkinResource'], 'getUhNumsByTypeAndPeriod').and.returnValue(of(uhFromType));

    component['loadUHsFromType'](DATA[ROOM_TYPES_LIST][1]);
    tick();

    expect(component.roomNumbersList.length).toEqual(2);
    expect(component.roomNumbersList[0].roomNumber).toEqual('100');
    expect(component.selectedOptionTypeNumber).toEqual('501');
    expect(component.selectRoomNumberList[0].key).toEqual('100');
    expect(component.selectRoomNumberList[0].value).toEqual
    ('<div class="thf-u-width-flex" >100<span class="float-right  "  style="color: #FF6969  " >Sujo</span></div>');
  }));

  it('should setUhActiveToEmpty', fakeAsync(() => {
    spyOn<any>(component['checkinService'], 'uhChosen');

    component['setUhActiveToEmpty']();
    tick();

    expect(component['checkinService'].uhChosen).toHaveBeenCalledWith(component.numberRoomElementSelected);
  }));
});

