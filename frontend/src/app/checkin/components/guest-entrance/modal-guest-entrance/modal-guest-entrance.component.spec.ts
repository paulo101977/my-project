import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { ModalGuestEntranceComponent } from './modal-guest-entrance.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  DATA,
  GUEST_FORM,
  GUEST_FORM_MODAL,
  GUEST_RESERVATION_BUDGET,
  GUEST_RESOURCE_OBJECT,
  HEADER_BUDGET,
  RESERVATION_ARRAY
} from 'src/app/checkin/mock-data/index';
import * as moment from 'moment';
import { PaymentTypeEnum } from 'app/shared/models/reserves/payment-type-enum';
import { of } from 'rxjs';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import {
  ActivatedRouteStub,
  createServiceStub,
  currencyFormatPipe,
  imgCdnStub,
  routerStub,
  sharedServiceStub,
  toasterEmitServiceStub
} from '../../../../../../testing';
import { GuestEntranceResource } from 'app/shared/resources/guest-entrance/guest-entrance.resource';
import { ActivatedRoute } from '@angular/router';
import { DateService } from 'app/shared/services/shared/date.service';
import { ReservationBudgetService } from 'app/shared/services/reservation-budget/reservation-budget.service';
import { ReservationBudget } from 'app/shared/models/revenue-management/reservation-budget/reservation-budget';
import {
  ReservationItemCommercialBudgetDay
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-day';
import {
  ReservationItemCommercialBudgetSearch
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-search';

const data = DATA[GUEST_RESOURCE_OBJECT];


const dateServiceStub = createServiceStub(DateService, {
  convertDateToString: (date: Date) => '2019-06-01T19:48:35',
  getSystemDate: (propertyId: number) => new Date('2019-06-01T19:48:35'),
  diffDaily: (date1: Date, date2: Date) => -1,
  convertStringToDate: (dateString: string, format?: string) => new Date('2019-05-31T00:00:00'),
  getSystemDateWithoutFormat: (propertyId: number) => '2019-06-01T19:48:35'
});

const reservationBudgetServiceStub = createServiceStub(ReservationBudgetService, {
  getReservationBudgetByCommercialBudgetDay: (
    reservationBudget: ReservationBudget,
    commercialBudgetDay: ReservationItemCommercialBudgetDay,
    commercialBudgetName: string,
    isGratuityType: boolean = false
  ) => null
});


const guestEntranceResourceStub = createServiceStub(GuestEntranceResource, {
  confirmEntrance: (guestReservation: Array<any>) => of( null ),
  getGuestEntrance: (
    reservationItemCommercialBudgetSearch: ReservationItemCommercialBudgetSearch
  ) => of( null )
});

describe('ModalGuestEntranceComponent', () => {
  let component: ModalGuestEntranceComponent;
  let fixture: ComponentFixture<ModalGuestEntranceComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ModalGuestEntranceComponent,
        currencyFormatPipe,
        imgCdnStub,
      ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        MomentModule,
      ],
      providers: [
        sharedServiceStub,
        routerStub,
        toasterEmitServiceStub,
        dateServiceStub,
        reservationBudgetServiceStub,
        guestEntranceResourceStub,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(ModalGuestEntranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.textConfirm = 'Confirmar';
    component.propertyId = 1;
    component.budget = {... DATA[GUEST_RESERVATION_BUDGET]};
    component['formModal'].setValue(DATA[GUEST_FORM_MODAL]);

    spyOn(component, 'getDate').and.callThrough();
    spyOn(component, 'setReservationBudget').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit functions', () => {
    spyOn(component, 'setForm');
    spyOn(component, 'setBtnModal');

    component.ngOnInit();
    expect(component.setForm).toHaveBeenCalled();
    expect(component.setBtnModal).toHaveBeenCalled();
  });

  it( 'should onSelectSeparate', () => {
    const event = {
      target: {
        checked: false
      }
    };
    component.onSelectSeparate(event);
    expect(component.isSelectedSeparate).toEqual(false);
    expect(component.formModal.get('textConfirm').value).toEqual('');
  });

  it( 'should test onSelectGratuity', () => {
    const makeEvent = (isChecked: boolean) => {
      return {
        target: {
          checked: isChecked
        }
      };
    };

    spyOn(component, 'newBudget').and.callFake( () => {});

    // case selected
    component.onSelectGratuity(makeEvent(true));


    expect(component.isSelectedGratuity).toEqual(true);
    expect(component.isDisabledSeparate).toEqual(true);
    expect(component.isSelectedSeparate).toEqual(false);
    expect(component.formModal.get('textConfirm').value).toEqual('');
    expect(component.budgetEstimated).toEqual(500);

    // case false
    component.onSelectGratuity(makeEvent(false));

    expect(component.isDisabledSeparate).toEqual(false);
    expect(component.newBudget).toHaveBeenCalled();
  });

  it('should textCompare and form change', () => {
    const control = { value: component.textConfirm };
    component['textCompare']()(control);
    expect(component['textCompare']()(control)).toEqual( null );
    expect(component.btnConfirm.isDisabled).toEqual(component.formModal.invalid);
  });

  it('should onInit call setVars', () => {
    spyOn<any>(component, 'setBudgetConfig');

    component.setVars();

    expect(component.isSelectedGratuity).toBeTruthy();
    expect(component.hasCourtesyOrInternalUsage).toBeTruthy();
    expect(component.isDisabledSeparate).toBeTruthy();
    expect(component['setBudgetConfig']).toHaveBeenCalled();
  });

  it('should setBudgetConfig', () => {
    spyOn(component, 'isCourtesyOrInternalUsage').and.returnValue(false);
    spyOn(component, 'newBudget');

    component['setBudgetConfig']();

    expect(component.budget.receivedRoomTypeId).toEqual(2);
    expect(component.budget.reservationItemId).toEqual(4);
    expect(component.budget.arrivalDate).toEqual('2019-01-01T00:00:00');
    expect(component.budget.departureDate).toEqual('2019-03-03T12:00:00');
    expect(component.budget.adultCount).toEqual(1);
    expect(component.isSelectedGratuity).toBeFalsy();
    expect(component.hasCourtesyOrInternalUsage).toBeFalsy();
    expect(component.isDisabledSeparate).toBeFalsy();
    expect(component['isCourtesyOrInternalUsage']).toHaveBeenCalled();
    expect(component.newBudget).toHaveBeenCalled();
  });

  it('should test newBudget', fakeAsync(() => {
    spyOn<any>(component['guestEntranceResource'], 'getGuestEntrance')
      .and
      .returnValue(of(data));
    spyOn<any>(component['reservationBudgetService'], 'getReservationBudgetByCommercialBudgetDay')
      .and.returnValue(DATA[RESERVATION_ARRAY][0]);

    const totalBudget = data.commercialBudgetList[0].totalBudget;

    component.guestForm = <Array<CheckinGuestReservationItemDto>>[DATA[GUEST_FORM]];

    component.newBudget();

    tick();

    expect(component.getDate).toHaveBeenCalled();
    expect(component.setReservationBudget).toHaveBeenCalled();
    expect<any>(component.reservationBudgetArray[0]).toEqual(DATA[RESERVATION_ARRAY][0]);
    expect(component.budgetEstimated).toEqual(totalBudget);
    expect(component.newHeaderBudget).toEqual(data);
  }));

  it('should getDate without diff', () => {
    const date = '2019-06-01T19:48:35.562Z';
    let systemDate = '';
    spyOn(component['dateService'], 'getSystemDateWithoutFormat').and.returnValue(date);
    spyOn(component['dateService'], 'convertStringToDate').and.returnValue( new Date(date));
    spyOn(component['dateService'], 'diffDaily').and.returnValue(1);

    component.budget.departureDate = date;
    component.getDate();

    systemDate = new Date(component.systemDate).toStringUniversal();

    expect(
      component.lastDate.toStringUniversal().split('T')[0]
    ).toEqual(
      systemDate.split('T')[0]
    );
  });

  it('should getDate with diff', () => {
    const date = '2019-06-01T19:48:35.562Z';
    let lastDate = null;
    spyOn(component['dateService'], 'getSystemDateWithoutFormat').and.callFake(() => date);

    component.budget.departureDate = '2019-06-01T19:48:35.562Z';
    component.getDate();

    lastDate = moment(component.budget.departureDate)
      .add(-1, 'days')
      .startOf('day')
      .toDate();
    expect(component.lastDate.toStringUniversal()).toEqual(lastDate.toStringUniversal());
  });

  it('should check isCourtesyOrInternalUsage method', () => {
    expect(component.isCourtesyOrInternalUsage()).toEqual(false);

    component.budget.paymentTypeId = PaymentTypeEnum.InternalUsage;

    expect(component.isCourtesyOrInternalUsage()).toEqual(true);

    component.budget.paymentTypeId = 999999;

    expect(component.isCourtesyOrInternalUsage()).toEqual(false);
  });

  it('should check clearForm method', () => {
    const value = 'TEST1';

    component['formModal'].patchValue({ textConfirm: value });

    expect(component['formModal'].get('textConfirm').value).toEqual(value);

    component.clearForm();

    expect(component['formModal'].get('textConfirm').value).toEqual('');
  });


  it( 'should test setObjectGuestEntrance method', fakeAsync(() => {
    spyOn<any>(component['guestEntranceResource'], 'getGuestEntrance')
      .and
      .returnValue(of(DATA[GUEST_RESOURCE_OBJECT]));

    component.guestForm = <Array<CheckinGuestReservationItemDto>>[DATA[GUEST_FORM]];

    // reset
    component.budget = {... DATA[GUEST_RESERVATION_BUDGET]};
    component.newHeaderBudget = {... DATA[HEADER_BUDGET]};

    component.guestForm = <Array<CheckinGuestReservationItemDto>>[DATA[GUEST_FORM]];

    component.reservationBudgetArray = null;

    component.setObjectGuestEntrance();

    expect(
      component
        .guestEntrance
        .reservationItemBudgetHeaderDto
    ).toEqual (
      component
        .budget
        .reservationItemBudgetHeaderDto
    );

    // gratuity
    component.isSelectedGratuity = true;
    component.budgetEstimated = 0;

    component.setObjectGuestEntrance();

    expect(
      component
        .guestEntrance
        .reservationItemBudgetHeaderDto
    ).toEqual (null );

    // separated
    component.isSelectedSeparate = true;
    component.isSelectedGratuity = false;
    component.newBudget();

    tick();

    component.setObjectGuestEntrance();

    expect(component.guestEntrance.reservationItemBudgetHeaderDto.isSeparated).toEqual(true);
    expect(component.guestEntrance.isGratuity).toEqual(false);
    expect(component.guestEntrance.reservationItemBudgetHeaderDto).toEqual(component.budget.reservationItemBudgetHeaderDto);
    expect(component.guestEntrance.guestRegistrationId).toEqual(component.guestForm[0].guestRegistrationId);
    expect(component.guestEntrance.reservationItemId).toEqual(component.reservationItemId);
  }));

  it('should check confirmEntrance method', fakeAsync(() => {
    spyOn(component['guestEntranceResource'], 'confirmEntrance').and.callThrough();
    spyOn(component, 'changeRoute').and.callFake(() => {});
    spyOn(component, 'setObjectGuestEntrance').and.callFake(() => {});
    spyOn(component['toasterEmitService'], 'emitChange').and.callFake(() => {});

    component.btnConfirm.isDisabled = false;

    component.confirmEntrance();

    tick();

    expect(component.btnConfirm.isDisabled).toEqual(true);
    expect(
      component['toasterEmitService']
        .emitChange
    ).toHaveBeenCalledWith( SuccessError.success, 'label.confirmentrance' );
    expect(component.changeRoute).toHaveBeenCalled();
    expect(component.setObjectGuestEntrance).toHaveBeenCalled();
  }));

  it('should check onModalClose method', () => {
    spyOn(component, 'clearForm').and.callFake( () => {});

    component.onModalClose();

    expect(component.clearForm).toHaveBeenCalled();
  });
});

