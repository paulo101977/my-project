import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { TranslateService } from '@ngx-translate/core';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { DateService } from 'app/shared/services/shared/date.service';
import * as moment from 'moment';
import { GuestEntranceResource } from 'app/shared/resources/guest-entrance/guest-entrance.resource';
import {
  ReservationItemCommercialBudgetSearch
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-search';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { PaymentTypeEnum } from 'app/shared/models/reserves/payment-type-enum';
import { CheckinReservationItemBudgetDto } from 'app/shared/models/dto/checkin/checkin-reservation-item-budget-dto';
import { ReservationBudgetService } from 'app/shared/services/reservation-budget/reservation-budget.service';
import { ReservationBudget } from 'app/shared/models/revenue-management/reservation-budget/reservation-budget';
import {
  ReservationItemCommercialBudgetDay
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-day';

@Component({
  selector: 'app-modal-guest-entrance',
  templateUrl: './modal-guest-entrance.component.html',
  styleUrls: ['./modal-guest-entrance.component.scss']
})
export class ModalGuestEntranceComponent implements OnInit, OnChanges {

  public readonly MODAL_REF_ID = 'modalGuestEntrance';
  public formModal: FormGroup;
  public btnConfirm: ButtonConfig;
  public textConfirm: string;
  public checkinDate: any;
  public checkoutDate: any;
  public systemDate: Date;
  public isSelectedGratuity: boolean;
  public isSelectedSeparate: boolean;
  public isDisabledSeparate: boolean;
  public propertyId: number;
  public lastDate: Date;
  public budgetEstimated = 0;
  public guestEntrance: any;
  public hasCourtesyOrInternalUsage: boolean;
  public newHeaderBudget: CheckinReservationItemBudgetDto;

  // Set Vars
  public roomTypeId: number;
  public reservationItemId: number;
  public adultCount: number;
  public childCount: number;
  public reservationItemCommercialBudgetSearch = new ReservationItemCommercialBudgetSearch();
  public reservationBudgetArray: ReservationBudget[];

  // Inputs
  @Input() reservationItemCode: string;
  @Input() budget: CheckInHeaderDto;
  @Input() guestForm: Array<CheckinGuestReservationItemDto>;
  @Input() closeCallbackFunction: Function;

  constructor(
    public fb: FormBuilder,
    public sharedService: SharedService,
    public translateService: TranslateService,
    public guestEntranceResource: GuestEntranceResource,
    public activateRoute: ActivatedRoute,
    public router: Router,
    private dateService: DateService,
    private toasterEmitService: ToasterEmitService,
    private reservationBudgetService: ReservationBudgetService,
  ) {
  }

  ngOnInit() {
    this.propertyId = this.activateRoute.snapshot.params.property;
    this.textConfirm = this.translateService.instant('action.confirm');
    this.setForm();
    this.setBtnModal();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setVars();
  }

  public isCourtesyOrInternalUsage() {
    const {budget} = this;

    return budget && (
      budget.paymentTypeId === PaymentTypeEnum.InternalUsage
      || budget.paymentTypeId === PaymentTypeEnum.Courtesy
    );
  }

  public setForm() {
    this.formModal = this.fb.group({
      textConfirm: ['', this.textCompare()]
    });

    this.formModal.statusChanges
      .subscribe(() => {
        this.btnConfirm.isDisabled = this.formModal.invalid;
      });
  }

  private textCompare() {
    return (control) => {
      return (this.textConfirm == control.value) ? null : {'name': {value: 'Invalid'}};
    };
  }

  public setBtnModal() {
    this.btnConfirm = this.sharedService.getButtonConfig(
      'confirm-modal',
      () => this.confirmEntrance(),
      'action.confirm',
      ButtonType.Primary,
      null,
      true
    );
  }

  public confirmEntrance() {
    // disable on confirm
    this.btnConfirm.isDisabled = true;

    this.setObjectGuestEntrance();
    this.guestEntranceResource.confirmEntrance(this.guestEntrance)
      .subscribe(() => {
          this.toasterEmitService.emitChange(
            SuccessError.success,
            this.translateService.instant('label.confirmentrance')
          );
          this.changeRoute();
        },
        () => {
          this.btnConfirm.isDisabled = false;
        });
  }

  public setVars() {
    this.isSelectedGratuity = true;
    this.hasCourtesyOrInternalUsage = true;
    this.isDisabledSeparate = true;

    this.setBudgetConfig();
  }

  private setBudgetConfig() {
    if (this.budget) {
      this.roomTypeId = this.budget.receivedRoomTypeId;
      this.reservationItemId = this.budget.reservationItemId;
      this.checkinDate = this.budget.arrivalDate;
      this.checkoutDate = this.budget.departureDate;
      this.adultCount = this.budget.adultCount;

      if (!this.isCourtesyOrInternalUsage()) {
        this.isSelectedGratuity = false;
        this.isDisabledSeparate = false;
        this.hasCourtesyOrInternalUsage = false;

        this.newBudget();
      }
    }
  }

  public newBudget() {
    this.getDate();
    this.setReservationBudget();

    this
      .guestEntranceResource
      .getGuestEntrance(this.reservationItemCommercialBudgetSearch)
      .subscribe(item => {
        const {commercialBudgetList, reservationBudgetDtoList} = item;

        if (commercialBudgetList.length > 0 && reservationBudgetDtoList.length > 0) {
          const commercialBudgetItem = commercialBudgetList[0];

          this.budgetEstimated = commercialBudgetItem.totalBudget;

          this.reservationBudgetArray =
            <Array<ReservationBudget>>commercialBudgetItem
              .commercialBudgetDayList.map(budgetDay => {
                const findedItem = reservationBudgetDtoList
                  .find(el => el.budgetDay === budgetDay.day) || {};
                return <ReservationBudget>{
                  ...this
                    .reservationBudgetService
                    .getReservationBudgetByCommercialBudgetDay(
                      new ReservationBudget(),
                      this.mapObjectToReservationItemCommercialBudgetDay(budgetDay),
                      commercialBudgetItem.commercialBudgetName,
                    ),
                  id: findedItem['id']
                };
              });
        }

        this.newHeaderBudget = item;
      });
  }

  public mapObjectToReservationItemCommercialBudgetDay(obj): ReservationItemCommercialBudgetDay {
    return <ReservationItemCommercialBudgetDay>{
      baseRateAmount: obj['baseRateAmount'],
      baseRateHeaderHistoryId: obj['baseRateHeaderHistoryId'],
      childMealPlanAmount: obj['childMealPlanAmount'],
      childRateAmount: obj['childRateAmount'],
      childRateAmout: null,
      currencyId: obj['currencyId'],
      currencySymbol: obj['currencySymbol'],
      day: obj['day'],
      id: obj['id'],
      maximumRate: obj['maximumRate'],
      mealPlanAmount: obj['mealPlanAmount'],
      mealPlanTypeId: obj['mealPlanTypeId'],
      minimumRate: obj['minimumRate'],
      overbooking: obj['overbooking'],
      overbookingLimit: obj['overbookingLimit'],
      rateLimitExceeded: obj['rateLimitExceeded'],
      total: obj['total'],
      childRateAmountWithRatePlan: obj['childRateAmountWithRatePlan'],
      baseRateAmountWithRatePlan: obj['baseRateAmountWithRatePlan'],
      mealPlanAmountWithRatePlan: obj['mealPlanAmountWithRatePlan'],
      baseRateWithRatePlanAmount: obj['baseRateWithRatePlanAmount'],
      childMealPlanAmountWithRatePlan: obj['childMealPlanAmountWithRatePlan'],
      ratePlanId: obj['ratePlanId'],
      baseRateChanged: obj['baseRateChanged'],
    };
  }

  public getDate() {
    const {departureDate} = this.budget;
    let systemDate = this.dateService.getSystemDate(this.propertyId);

    if (!systemDate || (systemDate && isNaN(systemDate.getDay()))) {
      systemDate = moment().toDate();
    }

    if (departureDate && systemDate) {
      const departure = new Date(this.budget.departureDate);
      const diff = this.dateService.diffDaily(departure, systemDate);

      this.lastDate = this.dateService.convertStringToDate(this.budget.departureDate);

      if (diff < 0) {
        this.lastDate = moment(this.budget.departureDate)
          .add(-1, 'days')
          .startOf('day')
          .toDate();
      }
    }

    this.systemDate = systemDate;
  }

  public onSelectGratuity(event) {
    this.isSelectedGratuity = false;
    this.formModal.get('textConfirm').setValue('');

    if (event.target.checked) {
      this.isSelectedGratuity = true;
      this.isDisabledSeparate = true;
      this.isSelectedSeparate = false;
      this.budgetEstimated = this.budget.reservationItemBudgetHeaderDto.commercialBudgetList[0].totalBudget;
    } else {
      this.newBudget();
      this.isDisabledSeparate = false;
    }
  }

  public onSelectSeparate(event) {
    const {checked} = event.target;
    this.formModal.get('textConfirm').setValue('');
    this.isSelectedSeparate = checked;
  }

  public changeRoute() {
    this.router.navigate(
      [`/p/${this.propertyId}/reservation`],
      {relativeTo: this.activateRoute}
    );
  }

  public setReservationBudget() {
    let verifyAdult = 0;
    if (this.guestForm) {
      this.guestForm.forEach(item => {
        if (!item.isChild) {
          this.adultCount++;
          verifyAdult++;
        }
      });
    }

    const adultCount = this.budget.adultCount + verifyAdult;

    this.reservationItemCommercialBudgetSearch = {
      propertyId: this.propertyId,
      roomTypeId: this.budget.requestedRoomTypeId,
      systemDate: this.dateService.convertDateToString(this.systemDate),
      initialDate: this.systemDate,
      finalDate: this.lastDate,
      adultCount: adultCount,
      reservationItemId: this.reservationItemId,
      mealPlanTypeId: null,
      companyClientId: null,
      childrenAge: this.budget.childrenAge,
      ratePlanId: this.budget.ratePlanId
    };
  }

  public setObjectGuestEntrance() {
    const {reservationBudgetDtoList} = this.budget.reservationItemBudgetHeaderDto;
    const {reservationBudgetArray} = this;
    const {isSelectedGratuity} = this;
    const reservationBudgetArrayClone = reservationBudgetArray ? [...reservationBudgetArray] : reservationBudgetDtoList;

    if (this.newHeaderBudget) {
      this.budget.reservationItemBudgetHeaderDto = this.newHeaderBudget;
    }
    this.budget.reservationItemBudgetHeaderDto.isSeparated = this.isSelectedSeparate;
    this.budget.reservationItemBudgetHeaderDto.reservationBudgetDtoList = reservationBudgetArrayClone;

    if (!this.isSelectedGratuity) {
      this.budget.reservationItemBudgetHeaderDto.commercialBudgetList[0].totalBudget = this.budgetEstimated;
    }
    this.guestEntrance = {
      reservationItemBudgetHeaderDto: !isSelectedGratuity ? this.budget.reservationItemBudgetHeaderDto : null,
      guestRegistrationId: this.guestForm[0].guestRegistrationId,
      reservationItemId: this.reservationItemId,
      isGratuity: this.isSelectedGratuity
    };
  }

  public clearForm() {
    if (this.formModal) {
      this.formModal.patchValue({textConfirm: ''});
    }

    // clear radio and toggle
    this.isSelectedGratuity = false;
    this.isSelectedSeparate = false;
  }

  public onModalClose = () => {
    this.clearForm();
  }
}
