import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import * as _ from 'lodash';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { CheckinService } from 'app/shared/services/checkin/checkin.service';

@Component({
  selector: 'app-guest-entrance-fnrh-sidebar',
  templateUrl: './guest-entrance-fnrh-sidebar.component.html',
  styleUrls: ['./guest-entrance-fnrh-sidebar.component.scss'],
})
export class GuestEntranceFnrhSidebarComponent implements OnInit {

  public _guestCapacity: any;
  public totalGuestsCapacity: number;
  public freeGuestsCount: number;
  public guestCards: Array<GuestCard>;
  public guestCardsReadonly: Array<GuestCard>;
  public guestReservationCheckedForCheckinList: Array<CheckinGuestReservationItemDto>;
  // TODO: check if it is necessary
  private guestCardCurrent: GuestCard;
  public guestCardCurrentIndex: number;

  @ViewChild('sidebarReal') sidebarReal: ElementRef;

  @Input() set readonlyDataSidebar( _dateSideBar: Array<CheckinGuestReservationItemDto>) {
    if ( _dateSideBar ) {
      this
        .setGuestList(
          [..._dateSideBar],
          'guestCardsReadonly',
          true
        );
      this.setFreeGuestsCount();
    }
  }

  @Input() set dataSidebar(_dateSideBar: Array<CheckinGuestReservationItemDto>) {
    if ( _dateSideBar ) {
      this.setGuestList([..._dateSideBar], 'guestCards');
      this.setFreeGuestsCount();
    }
  }

  @Input() set guestCapacity(_guestCapacity: any) {
    this._guestCapacity = _guestCapacity;
    this.setTotalGuestsCapacity(_guestCapacity);
  }

  @Output() clickGuestCard = new EventEmitter<GuestCard>();
  @Output() setGuestReservationItemListForCheckinEvent = new EventEmitter();


  constructor(
    // TODO: check if it is necessary
    private checkinService: CheckinService,
  ) {}

  ngOnInit() {
    this.initGuestList();
  }

  public setTotalGuestsCapacity(_guestCapacity) {
    this.totalGuestsCapacity = 0;

    if ( _guestCapacity ) {
      const { adultCapacity, childCapacity } = _guestCapacity;
      this.totalGuestsCapacity =  (+adultCapacity) + (+childCapacity);
    }
  }

  public setFreeGuestsCount() {
    const { guestCards, guestCardsReadonly } = this;

    this.freeGuestsCount = 0;

    if ( guestCards &&  guestCardsReadonly ) {
      this.freeGuestsCount = guestCards.length + guestCardsReadonly.length;
    }
  }

  // TODO: implement and add tests
  public removeGuest() {
    console.log('removeGuest');
  }

  // TODO: implement and add tests
  public addNewGuest() {
    console.log('addNewGuest');
  }

  public initGuestList(): void {
    this.guestCards = [];
    this.guestCardsReadonly = [];
  }


  public setGuestList(dataSidebar, propertyListName: string, isReadonly?: boolean) {
    if (dataSidebar && this[propertyListName]) {
      // reset card
      this[propertyListName] = [];

      dataSidebar.forEach(item => {
        this[propertyListName].push(<GuestCard>{
          guest: item,
          active: false,
          isValid: item.isValid,
          isSelected: true,
          isReadonly
        });
      });

      // sort based on isMain, ReduxId, isChild
      this[propertyListName] =
        _.orderBy(
          this[propertyListName],
          ['isMain', 'isChild'],
          ['asc', 'asc', 'desc']
        );
    }
  }

  public resetActiveList() {
    const { guestCards } = this;

    if ( guestCards ) {
      this.guestCards.forEach(card => (card.active = false));
    }
  }

  // TODO: implement or verify if is necessary and add tests
  public chooseItem(guestCard: GuestCard) {
    // this.guestCardCurrentIndex = this.guestCards.indexOf(guestCard) + 1;
    // this.guestCardCurrent = guestCard;
    // this.guestCardCurrent.number = this.guestCardCurrentIndex;
    // this.clickGuestCard.emit(this.guestCardCurrent);
    // this.resetActiveList();
    // guestCard.active = true;
  }


  public isAllChecked() {
    const { guestReservationCheckedForCheckinList , guestCards } = this;

    if ( guestReservationCheckedForCheckinList && guestCards ) {
      return guestReservationCheckedForCheckinList
        .length === guestCards.length;
    }
    return false;
  }
}
