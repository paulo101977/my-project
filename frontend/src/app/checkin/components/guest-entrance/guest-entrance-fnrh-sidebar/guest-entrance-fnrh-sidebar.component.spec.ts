import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GuestEntranceFnrhSidebarComponent } from './guest-entrance-fnrh-sidebar.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';
import {
  DATA,
  GUEST_CAPACITY,
  GUEST_CARD_LIST,
  GUEST_LIST
} from 'app/checkin/mock-data';
import { padStub, routerStub } from '../../../../../../testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';


describe('GuestEntranceFnrhSidebarComponent', () => {
  let component: GuestEntranceFnrhSidebarComponent;
  let fixture: ComponentFixture<GuestEntranceFnrhSidebarComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        GuestEntranceFnrhSidebarComponent,
        padStub,
      ],
      imports: [
        TranslateTestingModule,
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [
        InterceptorHandlerService,
        routerStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(GuestEntranceFnrhSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(component, 'setGuestList').and.callThrough();
    spyOn(component, 'setTotalGuestsCapacity').and.callThrough();
    spyOn(component, 'setFreeGuestsCount').and.callThrough();


  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should test initGuestList', () => {
    component.initGuestList();

    expect(component.guestCards).not.toBeNull();
  });

  it('should test guestCapacity', () => {
    component.guestCapacity = DATA[GUEST_CAPACITY];

    expect(component.setTotalGuestsCapacity)
      .toHaveBeenCalledWith(component._guestCapacity);
    expect(component.totalGuestsCapacity)
      .toEqual(9);
  });

  it('should test setFreeGuestsCount', () => {
    component.guestCards = [...DATA[GUEST_CARD_LIST]];
    component.readonlyDataSidebar = [...DATA[GUEST_LIST]];

    component.setFreeGuestsCount();

    expect(component.freeGuestsCount)
      .toEqual(4);
  });

  it('should test readonlyDataSidebar', () => {
    const list = [{...DATA[GUEST_LIST][0]}];

    component.readonlyDataSidebar = list;

    expect(component.setGuestList)
      .toHaveBeenCalledWith(list, 'guestCardsReadonly', true);
    expect(component.setFreeGuestsCount)
      .toHaveBeenCalled();
  });

  it('should test dataSidebar', () => {
    const list = [{...DATA[GUEST_LIST][1]}];

    component.dataSidebar = list;

    expect(component.setGuestList)
      .toHaveBeenCalledWith(list, 'guestCards');
    expect(component.setFreeGuestsCount)
      .toHaveBeenCalled();
  });

  it('should test setGuestList', () => {
    let list = [{...DATA[GUEST_CARD_LIST][1]}];
    let result: any;

    component.guestCards = null;
    component.guestCardsReadonly = null;

    component.setGuestList(null, null, null);

    expect(component.guestCards)
      .toEqual(null);
    expect(component.guestCardsReadonly)
      .toEqual(null);

    component.guestCardsReadonly = [];
    component
      .setGuestList(list, 'guestCardsReadonly', true);

    result  = JSON.stringify(component.guestCardsReadonly);

    expect( component.guestCardsReadonly[0].isReadonly ).toEqual(true);


    list = [...DATA[GUEST_CARD_LIST]];
    component.guestCards = [];
    component
      .setGuestList(list, 'guestCards');

    expect( result ).toContain('isValid');
    expect( component.guestCards[0].isReadonly ).toEqual(undefined);
    expect( component.guestCards.length ).toEqual(2);
  });


  it('should resetActiveList', () => {
    component.guestCards = DATA[GUEST_CARD_LIST];

    component.resetActiveList();

    const found = component.guestCards.find(card => card.active == true);

    expect(found).toBeFalsy();
  });

  // TODO: change the chooseItem logic and create the test
  // it('should chooseItem', () => {
  //   spyOn(component.clickGuestCard, 'emit');
  //
  //   const guestCard = new GuestCard();
  //   guestCard.guest = null;
  //   guestCard.active = false;
  //
  //   component.chooseItem(guestCard);
  //
  //   expect(component.clickGuestCard.emit).toHaveBeenCalled();
  //   expect(guestCard.active).toBeTruthy();
  // });

});
