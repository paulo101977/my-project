import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { GuestStatus } from 'app/shared/models/reserves/guest';

@Component({
  selector: 'app-card-entrance-guest-fnrh',
  templateUrl: './card-entrance-guest-fnrh.component.html',
})
export class CardEntranceGuestFnrhComponent implements OnInit {
  public guestCard: GuestCard;


  @ViewChild('checkbox') checkbox;


  @Input() guest: any;


  @Output() removeGuestEvent = new EventEmitter();
  @Output() seeGuestEventInfo = new EventEmitter();

  constructor(private translateService: TranslateService) {}

  ngOnInit() {
    const { guest } = this;
    this.guestCard = guest;
    this.fillGuestInformation(guest);
  }

  public removeGuest(guest) {
    this.removeGuestEvent.emit(guest);
  }

  public seeGuestInfo(guest) {
    this.seeGuestEventInfo.emit(guest);
  }


  // TODO: check it method
  private fillGuestInformation(item: GuestCard) {
    const guest = item.guest;
    guest.guestReservationItemName = guest.guestReservationItemName
      ? guest.guestReservationItemName
      : this.translateService.instant('checkinModule.guestFnrhcreateEdit.guestList.guestReservationItemName');
    guest.subdivision = guest.subdivision
      ? guest.subdivision
      : this.translateService.instant('checkinModule.guestFnrhcreateEdit.guestList.subdivision');
    guest.division = guest.division
      ? guest.division
      : this.translateService.instant('checkinModule.guestFnrhcreateEdit.guestList.division');
    guest.country = guest.country ? guest.country : this.translateService.instant('checkinModule.guestFnrhcreateEdit.guestList.country');
    guest.businessSourceName = guest.businessSourceName
      ? guest.businessSourceName
      : this.translateService.instant('checkinModule.guestFnrhcreateEdit.guestList.businessSourceName');
  }


  public hasStatusCheckin(card) {
    if (card && card.guest && card.guest.guestStatusId == GuestStatus.Checkin) {
      return true;
    }
    return false;
  }
}
