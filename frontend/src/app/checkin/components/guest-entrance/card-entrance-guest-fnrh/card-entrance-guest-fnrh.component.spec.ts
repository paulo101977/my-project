import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'ngx-moment';
import { CardEntranceGuestFnrhComponent } from './card-entrance-guest-fnrh.component';
import { CoreModule } from 'app/core/core.module';
import { configureTestSuite } from 'ng-bullet';


describe('CardEntranceGuestFnrhComponent', () => {
  let component: CardEntranceGuestFnrhComponent;
  let fixture: ComponentFixture<CardEntranceGuestFnrhComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        MomentModule, CoreModule
      ],
      declarations: [CardEntranceGuestFnrhComponent],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(CardEntranceGuestFnrhComponent);
    component = fixture.componentInstance;

    component.guest = {
      guest: {
        guestReservationItemName: '',
        fnrhDocumentBirthDate: new Date('2017/12/4'),
        documentTypeName: 'CPF',
        documentInformation: '343.343.554-55',
        subdivision: '',
        division: 'RJ',
        country: 'Brasil',
        businessSourceName: 'booking.com',
        isValid: false,
      },
      active: false,
      isValid: false,
    };

    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should removeGuest', () => {
    spyOn(component['removeGuestEvent'], 'emit');

    component.removeGuest(component.guestCard.guest);

    expect(component['removeGuestEvent'].emit)
      .toHaveBeenCalledWith(component.guestCard.guest);
  });

  it('should seeGuestInfo', () => {
    spyOn(component['seeGuestEventInfo'], 'emit');

    component.seeGuestInfo(component.guestCard.guest);

    expect(component['seeGuestEventInfo'].emit)
      .toHaveBeenCalledWith(component.guestCard.guest);
  });
});
