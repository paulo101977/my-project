import { Component, OnInit, ViewEncapsulation, Input, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GuestFnrhSidebarComponent } from '../../sidebar/guest-fnrh-sidebar/guest-fnrh-sidebar.component';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';
import { ReservationRemark } from 'app/shared/models/reserves/reservation-remark';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { CheckInNumUhsDto } from 'app/shared/models/dto/checkin/checkin-num-uhs-dto';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';
import { ModalRemarksFormEmitService } from 'app/shared/services/shared/modal-remarks-form-emit.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { ReservationResource } from 'app/shared/resources/reservation/reservation.resource';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';

@Component({
  selector: 'app-guest-entrance-fnrh-header',
  templateUrl: './guest-fnrh-entrance-header.component.html',
  styleUrls: ['./guest-fnrh-entrance-header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GuestFnrhEntranceHeaderComponent implements OnInit, OnChanges {
  private MODAL_TYPE_UH_TAX = 'modalTypeUhTax';

  @ViewChild('menuHeader') menuHeader: ElementRef;


  private ID_CHEKIN_STATUS = 2;
  private _dataHeader: CheckInHeaderDto;

  @Input() estimatedBudget: number;
  @Input()
  set dataHeader(data: CheckInHeaderDto) {
    this._dataHeader = data;
    if (data) {
      const { reservationItemBudgetHeaderDto: { commercialBudgetList } } = data;
      this._dataHeader.overnight = this.dateService.calculateOvernightStay(data.arrivalDate, data.departureDate);
      if ( commercialBudgetList && commercialBudgetList.length ) {
        this.agreementName = commercialBudgetList[0].commercialBudgetName;
        this.agreementTotal = commercialBudgetList[0].totalBudget;
      }
    }
  }
  get dataHeader(): CheckInHeaderDto {
    return this._dataHeader;
  }


  @Input() sidebar: GuestFnrhSidebarComponent;

  private _checkInHeaderDto: CheckInHeaderDto;

  set checkInHeaderDto(checkin: CheckInHeaderDto) {
    this._checkInHeaderDto = checkin;
    if (this.checkInHeaderDto.roomNumber && this.checkInHeaderDto.roomNumber != '') {
      this.numberRoomElementSelected = new CheckInNumUhsDto();
      this.numberRoomElementSelected.roomNumber = this.checkInHeaderDto.roomNumber;
      this.dataHeader.overnight = this.dateService.calculateOvernightStay(
        this.checkInHeaderDto.arrivalDate,
        this.checkInHeaderDto.departureDate
      );
    }
  }
  get checkInHeaderDto(): CheckInHeaderDto {
    return this._checkInHeaderDto;
  }

  public hasZeroRemarks: boolean;
  public hasOneRemarks: boolean;
  public hasTwoRemarks: boolean;
  public propertyId: number;
  public isDisabled: boolean;
  public buttonModalOk: ButtonConfig;

  public agreementName = '-';
  public agreementTotal: number;

  public numberRoomElementSelected: CheckInNumUhsDto;

  constructor(
    private reservationService: ReservationService,
    private modalRemarksFormEmitService: ModalRemarksFormEmitService,
    private modalToggleService: ModalEmitToggleService,
    private reservationResource: ReservationResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private activateRoute: ActivatedRoute,
    private sharedService: SharedService,
    private dateService: DateService,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.propertyId = this.activateRoute.snapshot.params.property;
    this.setvars();
    this.buttonModalOk = this.sharedService.getButtonConfig(
      'modal-tax',
      this.toggleModal.bind(this),
      'commomData.ok',
      ButtonType.Secondary
    );
  }

  ngOnChanges(changes: any) {
    this.setvars();
  }

  public setvars() {
    if (this.dataHeader) {
      this.checkInHeaderDto = this.dataHeader;
      this.isDisabled = this.dataHeader.reservationItemStatusId === this.ID_CHEKIN_STATUS;
    }
  }

  private setRemarksQuantity(): void {
    this.hasZeroRemarks = this.checkInHeaderDto.numberObservations === 0;
    this.hasOneRemarks = this.checkInHeaderDto.numberObservations === 1;
    this.hasTwoRemarks = this.checkInHeaderDto.numberObservations === 2;
  }

  public getStatusColor(): any {
    if (this.checkInHeaderDto) {
      const status = <ReservationStatusEnum>this.checkInHeaderDto.reservationItemStatusId;
      return this.reservationService.getStatusColorByReservationStatus(status);
    }
  }

  public showRemarksModal(): void {
    this.modalRemarksFormEmitService.emitSimpleModal(
        this.checkInHeaderDto.internalComments,
        this.checkInHeaderDto.externalComments,
        this.checkInHeaderDto.partnerComments);
  }

  public saveInternalCommentsAndExternalComments(reservationRemark: ReservationRemark): void {
    this.reservationResource.updateRemarks(this.checkInHeaderDto.id, reservationRemark).subscribe(response => {
      this.checkInHeaderDto.externalComments = reservationRemark.externalComments;
      this.checkInHeaderDto.internalComments = reservationRemark.internalComments;
      this.checkInHeaderDto.numberObservations = this.reservationService.getRemarksQuantityOfReservation(
        this.checkInHeaderDto.externalComments,
        this.checkInHeaderDto.internalComments
      );
      this.setRemarksQuantity();
      this.toasterEmitService
        .emitChange(
          SuccessError.success,
          this
            .translateService
            .instant('checkinModule.header.remark.successMessage')
        );
    });
  }

  private notifyTaxUnmodified(typeId) {
    if (this.checkInHeaderDto.receivedRoomTypeId && this.checkInHeaderDto.receivedRoomTypeId != typeId) {
      this.modalToggleService.emitToggleWithRef(this.MODAL_TYPE_UH_TAX);
    }
  }

  public toggleModal() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_TYPE_UH_TAX);
  }


  public isCheckinStatus(checkinStatus: number) {
    return checkinStatus == ReservationStatusEnum.Checkin;
  }

  public goToReservationBudget() {
    this.router.navigate(['p', this.propertyId, 'rm', 'reservation-budget', this.dataHeader.id]);
  }
}
