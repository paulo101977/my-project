import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MomentModule} from 'ngx-moment';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CoreModule} from '../../../../core/core.module';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CurrencyFormatPipe} from '../../../../shared/pipes/currency-format.pipe';
import {DATA, RESERVATION_ENTRANCE_BUDGET} from 'src/app/checkin/mock-data/index';
import {GuestFnrhEntranceHeaderComponent} from './guest-fnrh-entrance-header.component';
import {PadPipe} from 'app/shared/pipe/pad.pipe';

describe('GuestFnrhEntranceHeaderComponent', () => {
  let component: GuestFnrhEntranceHeaderComponent;
  let fixture: ComponentFixture<GuestFnrhEntranceHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GuestFnrhEntranceHeaderComponent,
        CurrencyFormatPipe,
        PadPipe
      ],
      imports: [
        TranslateModule.forRoot(),
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        MomentModule,
        CoreModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestFnrhEntranceHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.dataHeader = (DATA[RESERVATION_ENTRANCE_BUDGET]);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set Vars', () => {
    component.setvars();
    expect(component.isDisabled).toEqual(true);
    expect(component.checkInHeaderDto).toEqual(component.dataHeader);
  });

  it('should header', () => {
    expect(component.dataHeader.arrivalDate).toEqual('2019-01-08T21:00:00');
    expect(component.dataHeader.departureDate).toEqual('2019-01-10T12:00:00');
    expect(component.dataHeader.adultCount).toEqual(7);
    expect(component.dataHeader.childCount).toEqual(5);
    expect(component.dataHeader.reservationItemStatusName).toEqual('Check-In');
    expect(component.dataHeader.receivedRoomTypeAbbreviation).toEqual('BGR');
    expect(component.dataHeader.roomNumber).toEqual('5520');
    expect(component.dataHeader.overnight).toEqual(2);
  });
});

