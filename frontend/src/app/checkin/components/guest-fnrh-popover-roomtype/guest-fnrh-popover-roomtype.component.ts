import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CheckInRoomTypesDto } from '../../../shared/models/dto/checkin/checkin-room-types-dto';
import { CheckInHeaderDto } from '../../../shared/models/dto/checkin/checkin-header-dto';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import { ShowHideContentEmitService } from '../../../shared/services/shared/show-hide-content-emit-service';
import { CheckinResource } from '../../../shared/resources/checkin/checkin.resource';

@Component({
  selector: 'app-guest-fnrh-popover-roomtype',
  templateUrl: './guest-fnrh-popover-roomtype.component.html',
  styleUrls: ['./guest-fnrh-popover-roomtype.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class GuestFnrhPopoverRoomtypeComponent implements OnInit, OnChanges {
  @Output() roomTypeActive = new EventEmitter<CheckInRoomTypesDto>();
  @Input() checkInHeaderDto: CheckInHeaderDto;
  @Input() propertyId: number;

  public roomTypeItemActive: string;
  public togglePopoverHidden: boolean;
  public roomTypesList: Array<CheckInRoomTypesDto>;

  constructor(
    private checkinResource: CheckinResource,
    private showHideContentEmitService: ShowHideContentEmitService,
    private translateService: TranslateService,
  ) {
    this.roomTypesList = [];
  }

  ngOnInit() {
    this.showHideContentEmitService.changeEmitted$.subscribe(response => {
      this.togglePopoverHidden = true;
    });
    this.setVars();
  }

  ngOnChanges(changes: any) {
    if (this.checkInHeaderDto) {
      this.roomTypeItemActive = this.checkInHeaderDto.receivedRoomTypeAbbreviation;
      this.emitRoomType(changes);
    }
  }

  private emitRoomType(changes: any): void {
    const checkInRoomTypeDto = new CheckInRoomTypesDto();
    checkInRoomTypeDto.id = this.checkInHeaderDto.receivedRoomTypeId;
    checkInRoomTypeDto.name = this.checkInHeaderDto.receivedRoomTypeName;
    checkInRoomTypeDto.abbreviation = this.checkInHeaderDto.receivedRoomTypeAbbreviation;
    this.roomTypeActive.emit(checkInRoomTypeDto);
  }

  private setVars(): void {
    this.togglePopoverHidden = true;
    this.translateService.get('checkinModule.popovers.roomType.titlePopover').subscribe(value => {
      this.roomTypeItemActive = value;
    });
  }

  private getRoomTypes() {
    this.checkinResource.getRoomTypes(this.propertyId, this.checkInHeaderDto)
    .subscribe((result) => {
      this.roomTypesList = result.items;
    });
  }

  public clickRoomType($event, item: CheckInRoomTypesDto): void {
    const elementListActive = document.querySelector('.header-details-popover-list--roomtype.active');

    if (elementListActive) {
      elementListActive.classList.remove('active');
    }

    $event.target.classList.add('active');
    this.roomTypeItemActive = item.abbreviation;
    this.roomTypeActive.emit(item);
    this.togglePopoverHidden = !this.togglePopoverHidden;
    if (this.togglePopoverHidden) {
      this.closeOthersPopovers();
    }
  }

  public togglePopover(): void {
    if (this.togglePopoverHidden) {
      this.getRoomTypes();
    }

    this.togglePopoverHidden = !this.togglePopoverHidden;
    if (this.togglePopoverHidden) {
      this.closeOthersPopovers();
    }
  }

  private closeOthersPopovers(): void {
    this.showHideContentEmitService.emitChange(ShowHide.hide);
  }

  public keepPopoverOpen(): void {
    this.togglePopoverHidden = false;
  }
}
