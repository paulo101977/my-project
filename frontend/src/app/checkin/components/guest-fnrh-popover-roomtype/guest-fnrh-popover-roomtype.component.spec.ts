import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { GuestFnrhPopoverRoomtypeComponent } from './guest-fnrh-popover-roomtype.component';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { CheckInRoomTypesDto } from 'app/shared/models/dto/checkin/checkin-room-types-dto';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';

describe('GuestFnrhPopoverRoomtypeComponent', () => {
  let component: GuestFnrhPopoverRoomtypeComponent;
  let fixture: ComponentFixture<GuestFnrhPopoverRoomtypeComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, RouterTestingModule, CoreModule],
      declarations: [GuestFnrhPopoverRoomtypeComponent],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(GuestFnrhPopoverRoomtypeComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      spyOn<any>(component['showHideContentEmitService'], 'changeEmitted$').and.callFake(() => {});

      expect(component).toBeTruthy();
      expect(component.togglePopoverHidden).toBeTruthy();
    });

    it('should togglePopover', () => {
      component.togglePopoverHidden = false;

      component.togglePopover();

      expect(component.togglePopoverHidden).toBeTruthy();
    });

    it('should clickRoomType when  togglePopoverHidden is false', () => {
      spyOn(document, 'querySelector').and.returnValue(null);
      spyOn(component.roomTypeActive, 'emit');
      spyOn(component['showHideContentEmitService'], 'emitChange');

      const $event = {
        target: {
          classList: {
            add: function() {},
          },
        },
      };
      const item = new CheckInRoomTypesDto();
      component.togglePopoverHidden = false;

      component.clickRoomType($event, item);

      expect(component.roomTypeActive.emit).toHaveBeenCalled();
      expect(component.togglePopoverHidden).toBeTruthy();
      expect(component['showHideContentEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
    });

    it('should clickRoomType when  togglePopoverHidden is false', () => {
      spyOn(document, 'querySelector').and.returnValue(null);
      spyOn(component.roomTypeActive, 'emit');
      spyOn(component['showHideContentEmitService'], 'emitChange');

      const $event = {
        target: {
          classList: {
            add: function() {},
          },
        },
      };
      const item = new CheckInRoomTypesDto();
      component.togglePopoverHidden = true;

      component.clickRoomType($event, item);

      expect(component.roomTypeActive.emit).toHaveBeenCalled();
      expect(component.togglePopoverHidden).toBeFalsy();
      expect(component['showHideContentEmitService'].emitChange).not.toHaveBeenCalledWith(ShowHide.hide);
    });

    it('should keepPopoverOpen', () => {
      component.keepPopoverOpen();

      expect(component.togglePopoverHidden).toBeFalsy();
    });
  });

  describe('on changes', () => {
    it('should emitRoomType', () => {
      spyOn(component['roomTypeActive'], 'emit');
      component.checkInHeaderDto = new CheckInHeaderDto();
      const receivedRoomTypeAbbreviation = 'STD';
      component.checkInHeaderDto.receivedRoomTypeAbbreviation = receivedRoomTypeAbbreviation;

      component.checkInHeaderDto = new CheckInHeaderDto();

      component.ngOnChanges({});

      expect(component.roomTypeItemActive).toEqual(component.checkInHeaderDto.receivedRoomTypeAbbreviation);
      expect(component['roomTypeActive'].emit).toHaveBeenCalled();
    });
  });
});
