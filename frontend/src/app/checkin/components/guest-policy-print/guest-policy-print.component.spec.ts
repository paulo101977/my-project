import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestPolicyPrintComponent } from './guest-policy-print.component';
import { ThxImgModule } from '@inovacao-cmnet/thx-ui';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('GuestPolicyPrintComponent', () => {
  let component: GuestPolicyPrintComponent;
  let fixture: ComponentFixture<GuestPolicyPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateTestingModule, ThxImgModule ],
      declarations: [ GuestPolicyPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestPolicyPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
