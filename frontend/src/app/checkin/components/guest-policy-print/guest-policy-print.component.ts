import { Component, Input } from '@angular/core';
import { GuestPolicyPrint } from 'app/policy/models/guest-policy';

@Component({
  selector: 'guest-policy-print',
  templateUrl: './guest-policy-print.component.html',
  styleUrls: ['./guest-policy-print.component.css']
})
export class GuestPolicyPrintComponent {

  @Input() guestPolicy: GuestPolicyPrint;

  constructor() {
  }

}
