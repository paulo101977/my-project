import { Component, Input, ViewEncapsulation } from '@angular/core';
import { FnrhToPrintDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { DateService } from 'app/shared/services/shared/date.service';
import * as moment from 'moment';
import { GuestPolicyPrint } from 'app/policy/models/guest-policy';
import { PropertyService } from 'app/shared/services/property/property.service';

@Component({
  selector: 'app-print-single-guest-registration',
  templateUrl: './print-single-guest-registration.component.html',
  styleUrls: ['./print-single-guest-registration.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PrintSingleGuestRegistrationComponent {
  private _frnh: FnrhToPrintDto;
  public today = moment().toDate();
  public hotelCity = '';
  public hotelName = '';
  public needResponsable = false;
  public age: number;
  public guestPolicyList: GuestPolicyPrint[];
  public reservationItemCode: string;

  @Input() occupationName: string;
  @Input() transportName: string;
  @Input() reasonForTravel: string;
  @Input() addressTypeName: string;
  @Input() genderName: string;
  @Input() guestTypeName: string;
  @Input() nationalityName: string;
  @Input() showSignature = true;
  @Input() showHeader = true;

  @Input()
  set frnh(fnrh: FnrhToPrintDto) {
    this._frnh = fnrh;
    this.reservationItemCode = fnrh.reservationItemCode ? ` - ${fnrh.reservationItemCode}` : '';

    // todo checar se a ficha necessida de responsável
    if (fnrh.isLegallyIncompetent) {
      this.needResponsable = true;
    }
    this.age = this.dateService.calculateAgeByDate(this.fnrh.birthDate);
    if (fnrh) {
      const property = this.propertyService.getPropertySession(fnrh.propertyId);
      this.hotelName = property.name;
      this.hotelCity = property.locationList[0].subdivision;
      if (this.fnrh.guestPolicyPrint) {
        this.guestPolicyList = this.fnrh.guestPolicyPrint.guestPolicyList;
      }
    }
  }

  get fnrh() {
    return this._frnh;
  }

  @Input() text: string;

  constructor
  (private dateService: DateService,
   private propertyService: PropertyService) {
  }


}
