import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { PrintSingleGuestRegistrationComponent } from './print-single-guest-registration.component';
import { TranslateModule } from '@ngx-translate/core';
import { DateFormatPipe } from 'ngx-moment';
import { CoreModule } from '@app/core/core.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('PrintSingleGuestRegistrationComponent', () => {
  let component: PrintSingleGuestRegistrationComponent;
  let fixture: ComponentFixture<PrintSingleGuestRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientTestingModule, RouterTestingModule, CoreModule],
      declarations: [PrintSingleGuestRegistrationComponent, DateFormatPipe],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintSingleGuestRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
