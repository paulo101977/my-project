import { Component, Input } from '@angular/core';
import { FnrhSaveDto } from '../../../shared/models/dto/checkin/fnrh-save-dto';
import { SelectObjectOption } from '../../../shared/models/selectModel';

@Component({
  selector: 'app-print-all-guest-registration',
  templateUrl: './print-all-guest-registration.component.html',
  styleUrls: ['./print-all-guest-registration.component.css'],
})
export class PrintAllGuestRegistrationComponent {
  @Input() fnrhList: Array<FnrhSaveDto>;
  @Input() genderList: Array<SelectObjectOption>;
  @Input() ocupationList: Array<SelectObjectOption>;
  @Input() transportList: Array<SelectObjectOption>;
  @Input() reasonList: Array<SelectObjectOption>;
  @Input() nationalitylist: Array<SelectObjectOption>;
  @Input() guestTypeList: Array<SelectObjectOption>;


  public getOptionValueByKey(array: Array<SelectObjectOption>, key: string | number ): string {
    if (array) {
      const elements = array.filter(item => item.key == key);
      if (elements && elements.length > 0) {
        return elements[0].value;
      }
    }
    return '';
  }
}
