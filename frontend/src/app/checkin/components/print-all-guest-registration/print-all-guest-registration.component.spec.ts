import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PrintAllGuestRegistrationComponent } from './print-all-guest-registration.component';
import { PrintSingleGuestRegistrationComponent } from '../print-single-guest-registration/print-single-guest-registration.component';
import { SharedModule } from 'primeng/shared';
import { TranslateModule } from '@ngx-translate/core';
import { DateFormatPipe } from 'ngx-moment';
import { CoreModule } from '@app/core/core.module';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { DATA } from 'app/checkin/components/print-all-guest-registration/mock-data';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import * as moment from 'moment';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('PrintAllGuestRegistrationComponent', () => {
  let component: PrintAllGuestRegistrationComponent;
  let fixture: ComponentFixture<PrintAllGuestRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        TranslateModule.forRoot(),
        CoreModule,
        HttpClientTestingModule,
        HttpClientModule,
        RouterTestingModule,
      ],
      declarations: [PrintAllGuestRegistrationComponent, PrintSingleGuestRegistrationComponent, DateFormatPipe],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(PrintAllGuestRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it( 'should test getOptionValueByKey', () => {
    const arr = [
      new SelectObjectOption('1', '2'),
      new SelectObjectOption('3', '4'),
      new SelectObjectOption('5', '6')
    ];

    expect(component.getOptionValueByKey(arr, 1)).toEqual('2');
    expect(component.getOptionValueByKey(arr, 3)).toEqual('4');
    expect(component.getOptionValueByKey(arr, 5)).toEqual('6');
  });

  describe('on render method', () => {
    const toDateStr = (date: string) => moment(date).format('L');
    let fnrhList;
    beforeEach( () => {
      const propertyId = 1;
      fnrhList = component.fnrhList = DATA.FNRH_DTO_LIST;
      component.genderList = DATA.GENDER_LIST;
      component.guestTypeList = DATA.GUEST_TYPE_LIST;
      component.nationalitylist = DATA.NATIONALITY_LIST;
      component.ocupationList = DATA.OCCUPATION_LIST;
      component.reasonList = DATA.REASON_LIST;
      component.transportList = DATA.TRANSPORT_LIST;
      spyOn(sessionStorage, 'getItem').and.returnValue( DATA.SESSION_STORAGE_RETURN );

      fixture.detectChanges();
    });

    it('should render the print component', () => {
      const componentContent = fixture.nativeElement.textContent;

      expect(fixture.nativeElement.children.length).toEqual(fnrhList.length);
      expect(componentContent).toContain(
        component.getOptionValueByKey(
          component.ocupationList, fnrhList[0].occupationId
        )
      );
      expect(componentContent).toContain(
        component.getOptionValueByKey(
          component.guestTypeList, fnrhList[0].guestTypeId
        )
      );
      expect(componentContent).toContain(
        component.getOptionValueByKey(
          component.genderList, fnrhList[1].gender
        )
      );
      expect(componentContent).toContain(
        component.getOptionValueByKey(
          component.nationalitylist, fnrhList[0].nationality
        )
      );
      expect(componentContent).toContain(
        component.getOptionValueByKey(
          component.reasonList, fnrhList[1].purposeOfTrip
        )
      );
      expect(componentContent).toContain(
        component.getOptionValueByKey(
          component.transportList, fnrhList[1].arrivingBy
        )
      );
      expect(componentContent).toContain(toDateStr(fnrhList[0].birthDate));
      expect(componentContent).toContain(toDateStr(fnrhList[1].birthDate));
    });
  });
});
