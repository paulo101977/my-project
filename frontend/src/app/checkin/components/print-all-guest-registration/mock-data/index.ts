
import { FnrhSaveDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { SelectObjectOption } from 'app/shared/models/selectModel';


export  class DATA {
  public static readonly FNRH_DTO_LIST = <Array<FnrhSaveDto>>[
    {
      guestId: 1,
      personId: '1',
      firstName: 'Jose',
      lastName: 'Arriba',
      email: 'g.g@com',
      fullName: 'Jose Arriba',
      phoneNumber: '22222222',
      mobilePhoneNumber: '222222',
      birthDate: new Date('2018-11-05T14:48:00.000Z'),
      age: 12,
      gender: 'M',
      arrivingBy: 1,
      additionalInformation: 'blabla',
      nextDestinationText: 'blabla',
      arrivingFromText: 'blabla',
      occupationId: 1,
      guestTypeId: 1,
      nationality: 1,
      purposeOfTrip: 1,
      socialName: 'blabla',
      healthInsurance: 'blabla',
      document: null,
      location: null,
      person: null,
      guestReservationItemId: 1,
      id: '234',
      propertyId: 1,
      tenantId: '1234567',
      responsibleGuestRegistrationId: '45678',
      responsableName: 'blabla',
      isLegallyIncompetent: true,
      isChild: true,
    },
    {
      guestId: 2,
      personId: '1',
      firstName: 'Jose',
      lastName: 'Arriba',
      email: 'g.g@com',
      fullName: 'Jose Arriba',
      phoneNumber: '22222222',
      mobilePhoneNumber: '222222',
      birthDate: new Date('2019-11-05T14:48:00.000Z'),
      age: 12,
      gender: 'F',
      arrivingBy: 2,
      additionalInformation: 'blabla',
      nextDestinationText: 'blabla',
      arrivingFromText: 'blabla',
      occupationId: 2,
      guestTypeId: 2,
      nationality: 2,
      purposeOfTrip: 2,
      socialName: 'blabla',
      healthInsurance: 'blabla',
      document: null,
      location: null,
      person: null,
      guestReservationItemId: 2,
      id: '234',
      propertyId: 1,
      tenantId: '1234567',
      responsibleGuestRegistrationId: '45678',
      responsableName: 'blabla',
      isLegallyIncompetent: true,
      isChild: true,
    }
  ];
  public static readonly GENDER_LIST = <Array<SelectObjectOption>>[
    {
      key: 'M',
      value: 'M'
    },
    {
      key: 'F',
      value: 'F'
    }
  ];
  public static readonly OCCUPATION_LIST = <Array<SelectObjectOption>>[
    {
      key: '1',
      value: 'Pedreiro'
    },
    {
      key: '2',
      value: 'Marceneiro'
    }
    ];
  public static readonly TRANSPORT_LIST = <Array<SelectObjectOption>>[
    {
      key: '1',
      value: 'Rio'
    },
    {
      key: '4',
      value: 'Nova Iguaçu'
    },
    {
      key: '5',
      value: 'Buzio'
    },
    {
      key: '2',
      value: 'Friburgo'
    }
    ];
  public static readonly REASON_LIST = <Array<SelectObjectOption>>[
    {
      key: '1',
      value: 'Passeio'
    },
    {
      key: '4',
      value: 'Viagem'
    },
    {
      key: '2',
      value: 'Trabalho'
    },
    {
      key: '5',
      value: 'Outros'
    }
  ];
  public static readonly NATIONALITY_LIST = <Array<SelectObjectOption>>[
    {
      key: '0',
      value: 'Brasileiro'
    },
    {
      key: '2',
      value: 'Português'
    },
    {
      key: '1',
      value: 'Francês'
    },
    {
      key: '5',
      value: 'Japonês'
    }
  ];
  public static readonly GUEST_TYPE_LIST = <Array<SelectObjectOption>>[
    {
      key: '10',
      value: 'Outros'
    },
    {
      key: '1',
      value: 'Vip'
    },
    {
      key: '2',
      value: 'Normal'
    },
    {
      key: '3',
      value: 'Outros'
    }
  ];
  public static readonly SESSION_STORAGE_RETURN = JSON.stringify({
    name: 'Hotel blablabla',
    locationList: [
      {
        subdivision: 'Bangu'
      }
    ]
  });

}
