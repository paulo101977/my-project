import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GuestFnrhSidebarComponent } from './guest-fnrh-sidebar.component';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { padStub } from '../../../../../../testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { checkinServiceStub } from 'app/shared/services/checkin/testing';


describe('GuestFnrhSidebarComponent', () => {
  let component: GuestFnrhSidebarComponent;
  let fixture: ComponentFixture<GuestFnrhSidebarComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
      ],
      declarations: [
        GuestFnrhSidebarComponent,
        padStub
      ],
      providers: [
        checkinServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(GuestFnrhSidebarComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', () => {
    spyOn<any>(component, 'initGuestList').and.callFake( () => {} );
    spyOn<any>(component, 'setGuestList').and.callFake( () => {} );

    component.ngOnInit();

    expect(component.initGuestList).toHaveBeenCalled();
    expect(component['setGuestList']).toHaveBeenCalled();
  });

  it('should test chooseItem', () => {
    spyOn<any>(component, 'resetActiveList').and.callFake( () => {} );
    spyOn<any>(component, 'setCardActive').and.callFake( () => {} );

    const index = 1;
    const checkinGuestReservationItemDto1 = new CheckinGuestReservationItemDto();
    checkinGuestReservationItemDto1.guestReservationItemId = 1;
    const checkinGuestReservationItemDto2 = new CheckinGuestReservationItemDto();
    checkinGuestReservationItemDto2.guestReservationItemId = 2;

    component.guestCards = [
      <GuestCard>{ active: false, isValid: false, guest: checkinGuestReservationItemDto1 },
      <GuestCard>{ active: false, isValid: false, guest: checkinGuestReservationItemDto2 },
    ];

    const guestCard = component.guestCards[index];
    component.chooseItem(guestCard);

    expect(component.guestCardCurrentIndex).toEqual(index + 1);
    expect(component['guestCardCurrent']).toEqual({
      ...guestCard,
      number: index + 1
    });

    expect(component.resetActiveList).toHaveBeenCalled();
    expect(component['setCardActive']).toHaveBeenCalledWith(guestCard);
  });

  describe('on toggleAllCheckin', () => {
    beforeEach( () => {
      component.guestReservationCheckedForCheckinList = new Array<CheckinGuestReservationItemDto>();
      const checkinGuestReservationItemDto1 = new CheckinGuestReservationItemDto();
      checkinGuestReservationItemDto1.guestReservationItemId = 1;
      const checkinGuestReservationItemDto2 = new CheckinGuestReservationItemDto();
      checkinGuestReservationItemDto2.guestReservationItemId = 2;

      component.guestReservationCheckedForCheckinList.push(checkinGuestReservationItemDto1);
      component.guestReservationCheckedForCheckinList.push(checkinGuestReservationItemDto2);

      component.guestCards = [
        <GuestCard>{ active: false, isValid: false, guest: checkinGuestReservationItemDto1 },
        <GuestCard>{ active: false, isValid: false, guest: checkinGuestReservationItemDto2 },
      ];

      spyOn<any>(component, 'setAllCardsSelected').and.callFake( () => {} );
    });


    it('should test on guestCard.length == guestReservationCheckedForCheckinList.length',
      () => {

      component.toggleAllCheckin();

      expect(component.guestReservationCheckedForCheckinList.length).toEqual(0);
      expect(component['setAllCardsSelected']).toHaveBeenCalledWith(false);
    });
    it('should test otherwise', () => {
      spyOn<any>(component, 'setGuestReservationCheckedForCheckinList').and.callFake( f => f );

      component.guestReservationCheckedForCheckinList.pop();
      component.toggleAllCheckin();

      expect(component['setGuestReservationCheckedForCheckinList']).toHaveBeenCalled();
      expect(component['setAllCardsSelected']).toHaveBeenCalledWith(true);
    });
  });

  it('should on changes', () => {
    spyOn<any>(component, 'chooseItem');
    spyOn<any>(component['checkinService'], 'fnrhListValid');

    component.dataSidebar = new Array<CheckinGuestReservationItemDto>();

    const checkinGuestReservationItemDto = new CheckinGuestReservationItemDto();
    checkinGuestReservationItemDto.guestReservationItemId = 1;
    component.guestCards = [
      <GuestCard>{
        active: false,
        isValid: false,
        guest: checkinGuestReservationItemDto,
      },
    ];
    component.dataSidebar.push(checkinGuestReservationItemDto);

    component.ngOnChanges({});

    // expect(component.chooseItem).toHaveBeenCalledWith(component.guestCards[0]);
    expect(component.guestReservationCheckedForCheckinList[0].guestReservationItemId).toEqual(
      checkinGuestReservationItemDto.guestReservationItemId,
    );
    expect(component.guestReservationCheckedForCheckinList.length).toEqual(1);
    expect(component['checkinService'].fnrhListValid).toHaveBeenCalledWith(component.guestCards);
  });

  it('should initGuestList', () => {
    component.initGuestList();

    expect(component.guestCards).not.toBeNull();
  });

  it('should getGuestList', () => {
    spyOn<any>(component.setGuestReservationItemListForCheckinEvent, 'emit');
    component.dataSidebar = new Array<CheckinGuestReservationItemDto>();
    const checkinGuestReservationItemDto = new CheckinGuestReservationItemDto();
    checkinGuestReservationItemDto.guestReservationItemId = 1;
    component.dataSidebar.push(checkinGuestReservationItemDto);

    component['setGuestList']();
    const list = component.getGuestList();

    expect(list.length).toEqual(1);
    expect(component.setGuestReservationItemListForCheckinEvent.emit).toHaveBeenCalledWith(component.guestReservationCheckedForCheckinList);
  });

  it('should resetActiveList', () => {
    component.resetActiveList();

    const found = component.guestCards.find(card => card.active == true);

    expect(found).toBeFalsy();
  });

  describe('on insertOrRemoveGuestReservationItemForCheckin', () => {
    beforeEach(() => {
      component.guestReservationCheckedForCheckinList = new Array<CheckinGuestReservationItemDto>();
      const checkinGuestReservationItemDto1 = new CheckinGuestReservationItemDto();
      checkinGuestReservationItemDto1.guestReservationItemId = 1;
      const checkinGuestReservationItemDto2 = new CheckinGuestReservationItemDto();
      checkinGuestReservationItemDto2.guestReservationItemId = 2;

      component.guestReservationCheckedForCheckinList.push(checkinGuestReservationItemDto1);
      component.guestReservationCheckedForCheckinList.push(checkinGuestReservationItemDto2);

      component.guestCards = [
        <GuestCard>{ active: false, isValid: false, guest: checkinGuestReservationItemDto1 },
        <GuestCard>{ active: false, isValid: false, guest: checkinGuestReservationItemDto2 },
      ];
    });

    it('should insert checkinGuestReservationItemDto in insertOrRemoveGuestReservationItemForCheckin', () => {
      spyOn<any>(component.setGuestReservationItemListForCheckinEvent, 'emit');
      const checkinGuestReservationItemDto3 = new CheckinGuestReservationItemDto();
      checkinGuestReservationItemDto3.guestReservationItemId = 3;

      component.guestCards.push(
        <GuestCard>{ active: false, isValid: false, guest: checkinGuestReservationItemDto3 }
      );
      component.insertOrRemoveGuestReservationItemForCheckin(checkinGuestReservationItemDto3);


      expect(component.guestReservationCheckedForCheckinList[2].guestReservationItemId).toEqual(3);
      expect(component.guestReservationCheckedForCheckinList.length).toEqual(3);
      expect(component.setGuestReservationItemListForCheckinEvent.emit).toHaveBeenCalledWith(
        component.guestReservationCheckedForCheckinList,
      );
    });

    it('should remove checkinGuestReservationItemDto in insertOrRemoveGuestReservationItemForCheckin', () => {
      spyOn<any>(component.setGuestReservationItemListForCheckinEvent, 'emit');
      const checkinGuestReservationItemDto3 = new CheckinGuestReservationItemDto();
      checkinGuestReservationItemDto3.guestReservationItemId = 1;

      component.insertOrRemoveGuestReservationItemForCheckin(checkinGuestReservationItemDto3);

      expect(component.guestReservationCheckedForCheckinList.length).toEqual(1);
      expect(component.setGuestReservationItemListForCheckinEvent.emit).toHaveBeenCalledWith(
        component.guestReservationCheckedForCheckinList,
      );
    });
  });

  describe('on NextGuestCard', () => {
      // it('should to call chooseItem when have next guestCard valid and checked to checkin', () => {
      //
      //   spyOn<any>(component, 'chooseItem');
      //
      //   const checkinGuestReservationItemDto = new CheckinGuestReservationItemDto();
      //   checkinGuestReservationItemDto.guestReservationItemId = 1;
      //
      //   const checkinGuestReservationItemDto2 = new CheckinGuestReservationItemDto();
      //   checkinGuestReservationItemDto2.guestReservationItemId = 2;
      //   component.guestCards = [
      //     <GuestCard>{ active: false, isValid: false, guest: checkinGuestReservationItemDto },
      //     <GuestCard>{ active: false, isValid: false, guest: checkinGuestReservationItemDto2 },
      //   ];
      //   component.guestReservationCheckedForCheckinList = new Array<CheckinGuestReservationItemDto>();
      //   component.guestReservationCheckedForCheckinList.push(component.guestCards[0].guest);
      //
      //   component.setGuestList();
      //
      //   expect(component.chooseItem).toHaveBeenCalledWith(component.guestCards[1]);
      // });

    it('should not to call chooseItem when no have next guestCard valid and checked to checkin', () => {
      spyOn<any>(component, 'chooseItem');
      const checkinGuestReservationItemDto = new CheckinGuestReservationItemDto();
      checkinGuestReservationItemDto.guestReservationItemId = 1;
      component.guestCards = [<GuestCard>{ active: false, isValid: false, guest: checkinGuestReservationItemDto }];
      component.guestReservationCheckedForCheckinList = new Array<CheckinGuestReservationItemDto>();
      component.guestReservationCheckedForCheckinList.push(component.guestCards[0].guest);

      expect(component.chooseItem).not.toHaveBeenCalledWith(component.guestCards[1]);
    });
  });
});
