import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { CheckinService } from 'app/shared/services/checkin/checkin.service';
import { GuestStatus } from 'app/shared/models/reserves/guest';

@Component({
  selector: 'app-guest-fnrh-sidebar',
  templateUrl: './guest-fnrh-sidebar.component.html',
  styleUrls: ['./guest-fnrh-sidebar.component.css'],
})
export class GuestFnrhSidebarComponent implements OnInit, OnChanges {
  @ViewChild('sidebarReal') sidebarReal: ElementRef;
  @Input() dataSidebar: Array<CheckinGuestReservationItemDto>;
  @Output() canChooseGuestCard = new EventEmitter<any>();
  @Output() setGuestReservationItemListForCheckinEvent = new EventEmitter();
  public guestCards: Array<GuestCard>;
  public guestReservationCheckedForCheckinList: Array<CheckinGuestReservationItemDto>;
  private guestCardCurrent: GuestCard;
  public guestCardCurrentIndex: number;

  constructor(private checkinService: CheckinService) {}

  ngOnInit() {
    this.initGuestList();
    this.setGuestList();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.dataSidebar) {
      this.setGuestList();
      if (!this.guestCardCurrent) {
        this.canChooseGuestCard.emit({...this.guestCards[0], isFirsTime: true});
        this.setCardActive(this.guestCards[0]);
      }
    }
    this.checkinService.fnrhListValid(this.guestCards);
  }

  public initGuestList(): void {
    this.guestCards = new Array<GuestCard>();
  }

  public setGuestList() {
    if (this.dataSidebar) {
      this.guestCards = Array<GuestCard>();
      this.dataSidebar.forEach(item => {
        this.guestCards.push(<GuestCard>{
          guest: item,
          active: false,
          isValid: item.isValid,
          isSelected: true,
        });
      });

      // sort based on isMain, ReduxId, isChild
      this.guestCards = _.orderBy(this.guestCards, ['isMain', 'isChild'], ['asc', 'desc']);

      if (this.guestCardCurrent) {
        const guestCard = this.guestCards.find(
            g => g.guest.guestReservationItemId == this.guestCardCurrent.guest.guestReservationItemId
        );

        if (guestCard) {
          guestCard.active = true;
        } else {
          this.chooseItem(this.guestCards[0]);
        }
      }
      this.setGuestReservationCheckedForCheckinList();
    }
  }

  private setGuestReservationCheckedForCheckinList() {
    this.guestReservationCheckedForCheckinList = this.guestCards
      .filter(guestCard => guestCard.isSelected)
      .map(guestCard => guestCard.guest);
    this.setGuestReservationItemListForCheckinEvent.emit(this.guestReservationCheckedForCheckinList);
  }

  public getGuestList() {
    return this.guestCards;
  }

  public resetActiveList() {
    this.guestCards.forEach(card => (card.active = false));
  }

  public canChoose(guestCard: GuestCard) {
    this.canChooseGuestCard.emit(guestCard);
  }

  public chooseItem(guestCard: GuestCard) {
    this.guestCardCurrentIndex = this.guestCards.indexOf(guestCard) + 1;
    this.guestCardCurrent = guestCard;
    this.guestCardCurrent.number = this.guestCardCurrentIndex;
    this.resetActiveList();
    this.setCardActive(guestCard);
  }

  public insertOrRemoveGuestReservationItemForCheckin(checkinGuest: CheckinGuestReservationItemDto) {
    const index = _.findIndex(this.guestReservationCheckedForCheckinList, [
      'guestReservationItemId',
      checkinGuest.guestReservationItemId,
    ]);
    if (index < 0) {
      if (checkinGuest.guestStatusId != GuestStatus.Checkin) {
        this.guestReservationCheckedForCheckinList.push(checkinGuest);
        const indexToSetTrue = _.findIndex(this.getGuestList(), ['guest', checkinGuest]);
        this.getGuestList()[indexToSetTrue].isSelected = true;
      }
    } else {
      _.remove(this.guestReservationCheckedForCheckinList, function(g) {
        return g.guestReservationItemId == checkinGuest.guestReservationItemId;
      });
      const indexToSetFalse = _.findIndex(this.getGuestList(), ['guest', checkinGuest]);
      this.getGuestList()[indexToSetFalse].isSelected = false;
    }
    this.setGuestReservationItemListForCheckinEvent.emit(this.guestReservationCheckedForCheckinList);
  }

  public toggleAllCheckin() {
    if (this.getGuestList().length == this.guestReservationCheckedForCheckinList.length) {
      this.guestReservationCheckedForCheckinList = [];
      this.setAllCardsSelected(false);
    } else {
      this.setGuestReservationCheckedForCheckinList();
      this.setAllCardsSelected(true);
    }
  }

  public setAllCardsSelected(status) {
    for (const card of this.guestCards) {
      card.isSelected = status;
    }
  }

  public isAllChecked() {
    if (this.guestReservationCheckedForCheckinList && this.getGuestList()) {
      return this.guestReservationCheckedForCheckinList.length == this.getGuestList().length;
    }
    return false;
  }

  public setCardActive(guestCard: GuestCard) {
    guestCard.active = true;
  }
}
