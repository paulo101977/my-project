import { forkJoin, of } from 'rxjs';

export const RESULT = 'RESULT';

export class DATA {
  public static readonly DOCUMENT_TYPE_ARR = [
    {
      countrySubdivisionId: 1,
      id: 1,
      isMandatory: true,
      name: 'CPF',
      personType: 'N',
    }
  ];

  public static readonly GUEST_TYPE_ARR = [
    {
      code: '0001',
      guestTypeName: 'NORMAL',
      id: 1,
      isActive: true,
      isIncognito: false,
      isVip: false,
      propertyId: 1,
    }
  ];

  public static readonly TRANSPORTATION_ARR = [
    {
      id: 1,
      transportationTypeName: 'Avião',
    },
  ];

  public static readonly NATIONALITY_ARR = [
    {
      countrySubdivisionId: 1,
      id: 1,
      languageIsoCode: 'pt-BR',
      name: 'Brasil',
      nationality: 'Brasileira',
    }
  ];

  public static readonly REASON_ARR = [
    {
      chainId: 1,
      id: 62,
      isActive: true,
      propertyId: 0,
      reasonCategoryId: 7,
      reasonName: 'AAA',
    }
  ];

  public static readonly OCCUPATION_ARR = [
    {
      id: 1,
      occupation: 'Contador',
    }
  ];

  public static readonly CHECKIN_HEADER_OBJ = {
    accomodations: 0,
    adultCount: 1,
    arrivalDate: '2018-08-02T00:00:00',
    childCount: 0,
    childrenAge: [],
    color: 'FF6969',
    currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
    currencySymbol: 'R$',
    departureDate: '2019-01-19T00:00:00',
    externalComments: '',
    housekeepingStatusName: 'Sujo',
    housekeepingStatusPropertyId: '42c17b94-2190-46a6-8cfc-3bcdd7d38019',
    id: 124,
    internalComments: '',
    mealPlanTypeId: 2,
    numberObservations: 0,
    overnight: 170,
    paymentType: 'Uso da Casa',
    paymentTypeId: 1,
    propertyId: 1,
    quantityDouble: 0,
    quantitySingle: 3,
    receivedRoomTypeAbbreviation: 'STD',
    receivedRoomTypeId: 1,
    receivedRoomTypeName: 'Standard',
    requestedRoomTypeAbbreviation: 'STD',
    requestedRoomTypeId: 1,
    requestedRoomTypeName: 'Standard',
    reservationBudget: 13140,
    reservationCode: 'VRN9QFCQN',
    reservationItemBudgetHeaderDto: {
      commercialBudgetList: [
        {
          commercialBudgetDayList: [
            {
              baseRateAmount: 100,
              childRateAmount: 0,
              currencyId: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
              currencySymbol: '€',
              id: 0,
              mealPlanTypeId: 2,
            }
          ],
          commercialBudgetName: 'Tarifa base',
          currencyId: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
          currencySymbol: '€',
          id: 0,
          mealPlanTypeDefaultId: 2,
          mealPlanTypeDefaultName: 'Café da Manha',
          mealPlanTypeId: 2,
          mealPlanTypeName: 'Café da Manha',
          rateVariation: 0,
          roomTypeAbbreviation: 'BGR',
          roomTypeId: 63,
          totalBudget: 900
        }
      ],
      isSeparated: true,
      mealPlanTypeId: 2,
      reservationItemCode: '4JDF93BT7-0001',
      reservationItemId: 3696,
      reservationBudgetDtoList: [
        {
          agreementRate: 150,
          baseRate: 100,
          childMealPlanTypeRate: 0,
          childRate: 0,
          currencyId: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
          currencySymbol: '€',
          discount: 0,
          id: 17916,
          manualRate: 150,
          mealPlanTypeBaseRate: 50,
          mealPlanTypeId: 2,
          propertyBaseRateHeaderHistoryId: '96f17cec-42b7-42f1-bbd4-b23ca80e7115',
          rateVariation: 0,
          reservationBudgetUid: '00000000-0000-0000-0000-000000000000',
          reservationItemId: 0,
        }
      ]
    },
    reservationItemId: 158,
    reservationItemStatusId: 2,
    reservationItemStatusName: 'Check-In',
    roomId: 1,
    roomLayoutId: '88C38586-5C4B-489B-8BB9-0AA7D78DEA99',
    roomNumber: '101',
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
  };

  public static readonly RESERVATION_ITEM_LIST = [
    {
      businessSourceName: 'Nova origem',
      country: 'Brasil',
      division: 'RJ',
      documentInformation: '259.251.124-58',
      documentTypeName: 'CPF',
      guestRegistrationDocumentBirthDate: '1990-10-10T00:00:00',
      guestRegistrationId: '2f016f4f-b32d-42de-2ab3-08d6119f40e6',
      guestReservationItemId: 204,
      guestReservationItemName: 'Teste Grupo 2',
      guestStatusId: 2,
      id: 124,
      isChild: false,
      isLegallyIncompetent: false,
      isMain: true,
      isValid: true,
      personId: '96ee1826-1a71-4d85-a4b4-4581b9242289',
      reservationItemCode: 'VRN9QFCQN-0002',
      reservationItemId: 158,
      reservationItemStatusId: 2,
      subdivision: 'Rio de Janeiro',
    }
  ];

  public static readonly RESPONSABLE_ARR = [
    {
      age: 124,
      arrivingFromText: 'Rio de Janeiro',
      birthDate: '1894-06-08T00:00:00',
      checkinDate: '2018-07-30T18:46:08.927',
      documentTypeId: 0,
      email: 'william@email.com',
      firstName: 'William',
      fullName: 'William',
      gender: 'M',
      guestId: 19,
      guestReservationItemId: 45,
      guestTypeId: 1,
      id: '825a1564-699f-4b9e-bce9-08d5f6486835',
      isChild: false,
      isLegallyIncompetent: true,
      isMain: true,
      lastName: 'William',
      nationality: 1,
      nextDestinationText: 'Rio de Janeiro',
      person: {
        companyClientContactPersonDtoList: [],
        companyContactPersonList: [],
        contactInformationList: [],
        documentList: [],
        employeeList: [],
        guestList: [],
        id: '00000000-0000-0000-0000-000000000000',
        locationList: [],
        personInformationList: [],
        propertyContactPersonList: [],
      },
      phoneNumber: '+55 21 9976-6508',
      propertyId: 1,
      socialName: 'William',
      tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    }
  ];

  public static readonly ROOM_TYPE_ARR = [
    {
      abbreviation: 'STD',
      adultCapacity: 2,
      childCapacity: 1,
      hasConjugatedRooms: false,
      id: 1,
      name: 'Standard',
      overbooking: true,
      overbookingLimit: true,
      roomTypeBedTypeList: [
        {
          bedType: 1,
          bedTypeId: 1,
          count: 1,
          id: 0,
          optionalLimit: 0,
          roomTypeId: 1,
          typeName: 'Single'
        }
      ],
      totalConjugatedRooms: 0
    }
  ];

  public static readonly RESULT = forkJoin([
    of({
      items: DATA.DOCUMENT_TYPE_ARR
    }),
    of({
      items: DATA.GUEST_TYPE_ARR,
    }),
    of({
      items: DATA.TRANSPORTATION_ARR
    }),
    of({
      items: DATA.NATIONALITY_ARR
    }),
    of({
      items: DATA.REASON_ARR
    }),
    of({
      items: DATA.OCCUPATION_ARR,
    }),
    of(DATA.CHECKIN_HEADER_OBJ),
    of({
      items: DATA.RESERVATION_ITEM_LIST,
    }),
    of({
      items: DATA.RESPONSABLE_ARR
    }),
    of({
      items: DATA.ROOM_TYPE_ARR,
    }),
  ]);
}
