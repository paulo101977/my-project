import { Component, OnInit } from '@angular/core';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { FnrhConfigInterface } from 'app/shared/components/fnrh/fnrh-config.interface';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { ActivatedRoute } from '@angular/router';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import { LoadPageEmitService } from 'app/shared/services/shared/load-emit.service';
import { ReservationItemViewService } from 'app/shared/services/reservation-item-view/reservation-item-view.service';
import * as moment from 'moment';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { FnrhService } from 'app/shared/services/fnrh/fnrh.service';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { UserService } from 'app/shared/services/user/user.service';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import {GuestFnrhService} from 'app/checkin/service/guest-fnrh.service';

@Component({
  selector: 'app-guest-fnrh-edit',
  templateUrl: './guest-fnrh-edit.component.html',
  styleUrls: ['./guest-fnrh-edit.component.css']
})
export class GuestFnrhEditComponent implements OnInit {

  public propertyId: number;
  public guestReservationItemId: number;

  // config view itens
  public configHeaderPage: ConfigHeaderPageNew;

  // Fnrh dependencies
  public config = <FnrhConfigInterface>{};
  private documentTypesAutocomplete;
  private guestTypeList;
  private nationalityList;
  private vehicleList;
  private reasonList;
  private occupationList;
  public responsableList;

  public reservationItemListDto: Array<CheckinGuestReservationItemDto>;
  private reservationItemId: number;
  public reservationCode: string;
  public reservationItemCode: string;
  public reservationId: number;
  public checkInHeaderDto: CheckInHeaderDto;
  public addressToApplyToAll: any;
  public guest: GuestCard;


  constructor(
    public _route: ActivatedRoute,
    public checkinResource: CheckinResource,
    public loadPageEmitService: LoadPageEmitService,
    private reservationItemViewService: ReservationItemViewService,
    private commomService: CommomService,
    private userService: UserService,
    private fnrhService: FnrhService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private guestFnrhService: GuestFnrhService
  ) {}

  ngOnInit() {
    const params = this._route.snapshot.params;
    this.propertyId = params.property;
    this.guestReservationItemId = params.guestReservationItemId;
    this.setConfigHeaderPage();

    if (params.reservationItemId) {
      this.reservationItemId = params.reservationItemId;
      this.reservationItemCode = this
        .reservationItemViewService
        .reduceAndGetCodeOfReservationItem(params.reservationItemCode);
      this.loadCompleteInfo();
    } else if (params.reservationId) {
      this.reservationId = params.reservationId;
    }
  }

  public setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
      callBackFunction: this.printFnrh
    };
  }

  public getResponsables() {
    let reservationId = this.reservationId;
    if (!this.reservationId) {
      reservationId = this.getReservationIdFromChildHeader();
    }
    this.checkinResource.getResponsables(this.propertyId, reservationId).subscribe(response => {
      this.responsableList = [...response.items];
    });
  }

  public loadCompleteInfo() {
    let reservationId = this.reservationId;
    if (!this.reservationId) {
      reservationId = this.getReservationIdFromChildHeader();
    }
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.checkinResource.getCompleteAuxInfo(
      this.propertyId,
      this.reservationItemId,
      reservationId,
      moment().locale()
    ).subscribe(result => {
      this.documentTypesAutocomplete = result[0].items;

      this.guestTypeList = this.commomService
        .toOption(result[1], 'id', 'guestTypeName');
      this.vehicleList = this.commomService
        .toOption(result[2], 'id', 'transportationTypeName');
      this.nationalityList = result[3].items;
      this.reasonList = this.commomService
        .toOption(result[4], 'id', 'reasonName');
      this.occupationList = this.commomService
        .toOption(result[5], 'id', 'occupation');
      this.checkInHeaderDto = result[6];
      this.reservationItemListDto = [];
      this.reservationItemListDto = result[7].items;

      if (this.reservationItemListDto) {
        this.guest = this.getGuestToFnrh();
      }

      this.responsableList = [...result[8].items];

      this.setFnrhToView();

      this.loadPageEmitService.emitChange(ShowHide.hide);
    });
  }

  public getReservationIdFromChildHeader(): number {
    return this.checkInHeaderDto ? this.checkInHeaderDto.id : 0;
  }

  public setFnrhToView() {
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.checkinResource.getFnrh(
      this.guestReservationItemId,
      this.propertyId,
      this.userService.getUserpreferredLanguage() || navigator.language.toLocaleLowerCase()
    ).subscribe(fnrhDtoResponse => {
      this.fnrhMapper(this.guestReservationItemId, fnrhDtoResponse);
      this.loadPageEmitService.emitChange(ShowHide.hide);
      this.getResponsables();
    });
  }

  public fnrhMapper(guestReservationItemId: number, fnrhFromGuest: FnrhDto) {
    const dataReservation = this.guestFnrhService.getDataReservation(this.checkInHeaderDto);
    const { reservationItemCode } = this;

    this.config = <FnrhConfigInterface>{
      guestReservationItemId: guestReservationItemId,
      fnrh: fnrhFromGuest,
      genderList: [
        <SelectObjectOption>{key: 'M', value: 'commomData.gender.male'},
        <SelectObjectOption>{key: 'F', value: 'commomData.gender.female'},
      ],
      occupationList: this.occupationList,
      guestTypeList: this.guestTypeList,
      nationalityList: this.nationalityList,
      vehicleList: this.vehicleList,
      reasonList: this.reasonList,
      documentTypesAutocomplete: this.documentTypesAutocomplete,
      guest: this.guest,
      dataReservation,
      reservationItemCode
    };
  }

  private getGuestToFnrh(): GuestCard {
    const guest = this.reservationItemListDto.find(
      g => g.guestReservationItemId == this.guestReservationItemId
    );
    if (guest) {
      return <GuestCard>{
        guest: guest,
        isSelected: true,
        isValid: true,
        active: true
      };
    }
  }

  // actions
  public printFnrh = () => {
    this.fnrhService.printFnrh();
  }

  public fnrhSaved(fnrhData) {
    let resource, message;

    if (!fnrhData.id || fnrhData.id == GuidDefaultData) {
      resource = this.checkinResource.createFnrh(fnrhData);
      message = this.translateService.instant('commomData.fnrh.actionsMessages.save');
    } else {
      resource = this.checkinResource.updateFnrh(fnrhData);
      message = this.translateService.instant('commomData.fnrh.actionsMessages.update');
    }

    resource
      .subscribe(() => {
        this.toasterEmitService.emitChange(SuccessError.success, message);
      });
  }

}
