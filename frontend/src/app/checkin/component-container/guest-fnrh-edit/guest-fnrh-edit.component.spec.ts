import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';

import { GuestFnrhEditComponent } from './guest-fnrh-edit.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { fnrhList } from 'app/shared/reducers/fnrh.reducer';
import { StoreModule } from '@ngrx/store';
import { of } from 'rxjs';
import {
  DATA,
  RESULT
} from './mock-data';
import * as moment from 'moment';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import {
  CheckinGuestReservationItemDto
} from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ActivatedRoute } from '@angular/router';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import { ReservationItemViewService } from 'app/shared/services/reservation-item-view/reservation-item-view.service';
import { FnrhService } from 'app/shared/services/fnrh/fnrh.service';
import { ActivatedRouteStub, createServiceStub, loadPageEmitServiceStub, userServiceStub } from '../../../../../testing';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';

const data$ = DATA[RESULT];
const result = <ItemsResponse<CheckinGuestReservationItemDto>>{
  items: [
    {
      ...new CheckinGuestReservationItemDto()
    },
    {
      ...new CheckinGuestReservationItemDto()
    }
  ],
  hasNext: false,
};
const response = new FnrhDto();
response.id = '123678';
const reservationItemCode = '123';

const checkinResourceStub =  createServiceStub(CheckinResource, {
  getCompleteAuxInfo: (propertyId, reservationItemId, reservationId, language) =>  data$ ,
  getResponsables: (propertyId: number, reservationId) => of ( result ),
  getFnrh: (guestReservationItemId: number, propertyId: number, languageIsoCode: string) => of( response )
});

const toasterEmitServiceStub = createServiceStub(ToasterEmitService, {
  emitChange(isSuccess: SuccessError, message: string) {}
});

const reservationItemViewServiceStub = createServiceStub(ReservationItemViewService, {
  reduceAndGetCodeOfReservationItem: (code: string) => reservationItemCode
});

export const fnrhServiceStub = createServiceStub(FnrhService, {
  fnrhAddressToApply: null
});

describe('GuestFnrhEditComponent', () => {
  let component: GuestFnrhEditComponent;
  let fixture: ComponentFixture<GuestFnrhEditComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        StoreModule.forRoot({ fnrhList }),
        RouterTestingModule,
        HttpClientTestingModule,
      ],
      declarations: [ GuestFnrhEditComponent ],
      providers: [
        loadPageEmitServiceStub,
        userServiceStub,
        checkinResourceStub,
        reservationItemViewServiceStub,
        fnrhServiceStub,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestFnrhEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on ngOnInit', () => {
    beforeEach( () => {
      spyOn<any>(component, 'setConfigHeaderPage').and.callFake( () => {} );
    });

    it('should check common methods', () => {
      component.ngOnInit();

      expect(component['setConfigHeaderPage']).toHaveBeenCalled();
    });

    it('should init with params reservationItemId', () => {
      const reservationItemId = 123;

      spyOn<any>(component['reservationItemViewService'], 'reduceAndGetCodeOfReservationItem')
        .and.returnValue(reservationItemCode);
      spyOn<any>(component, 'loadCompleteInfo').and.callFake(() => {} );

      component._route.snapshot.params = {
        reservationItemId,
        reservationItemCode
      };

      component.ngOnInit();

      expect(component['reservationItemId']).toEqual(reservationItemId);
      expect(component['reservationItemCode']).toEqual(reservationItemCode);
      expect(component['reservationItemViewService'].reduceAndGetCodeOfReservationItem).toHaveBeenCalledWith(
        reservationItemCode
      );
      expect(component['loadCompleteInfo']).toHaveBeenCalled();
    });

    it('should init with params reservationId', () => {
      const reservationId = 12356;

      component._route.snapshot.params = {
        reservationId
      };

      component.ngOnInit();

      expect(component['reservationId']).toEqual(reservationId);
    });
  });

  describe('on getResponsables', () => {
    let propertyId;
    let reservationId;


    beforeEach( () => {
      reservationId = 6;
      propertyId = component['propertyId'] = 11;
      spyOn<any>(component, 'getReservationIdFromChildHeader')
        .and.callFake( () => reservationId );
      spyOn<any>(component['checkinResource'], 'getResponsables').and.callThrough();

    });

    it('should test without reservationId', fakeAsync(() => {
      component.reservationId = null;
      component['getResponsables']();
      tick();

      expect(component['getReservationIdFromChildHeader']).toHaveBeenCalled();
      expect(component['checkinResource'].getResponsables).toHaveBeenCalledWith(propertyId, reservationId);
      expect(component.responsableList).toEqual(result.items);
    }));

    it('should test with reservationId', fakeAsync(() => {
      reservationId = component['reservationId'] = 100;
      component['getResponsables']();
      tick();

      expect(component['getReservationIdFromChildHeader']).not.toHaveBeenCalled();
      expect(component['checkinResource'].getResponsables)
        .toHaveBeenCalledWith(propertyId, reservationId);
      expect(component.responsableList).toEqual(result.items);
    }));

  });

  describe('on loadCompleteInfo', () => {
    let reservationItemId;
    let reservationId;

    beforeEach( () => {
      reservationId = 6;
      reservationItemId = component['reservationItemId'] = 123;
      spyOn<any>(component, 'getReservationIdFromChildHeader')
        .and.callFake( () => reservationId );
      spyOn<any>(component['checkinResource'], 'getCompleteAuxInfo').and.callThrough();
      spyOn<any>(component, 'loadCompleteInfo').and.callThrough();
      spyOn<any>(component['loadPageEmitService'], 'emitChange').and.callFake( f => f );
    });

    it('should test without reservationId', fakeAsync(() => {
      const propertyId = component['propertyId'] = 1;
      const locale = moment().locale();

      component.reservationId = null;
      component['loadCompleteInfo']();

      tick();

      expect(component['getReservationIdFromChildHeader']).toHaveBeenCalled();
      expect(component['checkinResource'].getCompleteAuxInfo).toHaveBeenCalledWith(
        propertyId,
        reservationItemId,
        reservationId,
        locale
      );
      expect( component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
    }));

    it('should test with reservationId', fakeAsync(() => {
      const propertyId = component['propertyId'] = 2;
      const locale = moment().locale();

      reservationId = 77;
      component.reservationId = reservationId;
      component['loadCompleteInfo']();

      tick();

      expect(component['getReservationIdFromChildHeader']).not.toHaveBeenCalled();
      expect(component['checkinResource'].getCompleteAuxInfo).toHaveBeenCalledWith(
        propertyId,
        reservationItemId,
        reservationId,
        locale
      );
      expect( component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
    }));

  });

  it('on setFnrhToView', fakeAsync(() => {
    const userLanguage = component['userService'].getUserpreferredLanguage() || navigator.language.toLocaleLowerCase();
    const propertyId = component['propertyId'] = 1456;
    const guestReservationItemId = component['guestReservationItemId'] = 678;

    spyOn<any>(component['loadPageEmitService'], 'emitChange').and.callFake( f => f );
    spyOn<any>(component['checkinResource'], 'getFnrh').and.callThrough();
    spyOn<any>(component, 'fnrhMapper').and.callFake( f => f );
    spyOn<any>(component, 'getResponsables').and.callFake( () => {} );

    component.setFnrhToView();
    tick();

    expect(component['checkinResource'].getFnrh).toHaveBeenCalledWith(
      guestReservationItemId,
      propertyId,
      userLanguage
    );

    expect(component['fnrhMapper']).toHaveBeenCalledWith(guestReservationItemId, response);
    expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledTimes(2);
    expect(component['getResponsables']).toHaveBeenCalled();
  }));

  it('on getGuestToFnrh', () => {
    const guestReservationItemId = component.guestReservationItemId = 12345678;
    const reservationItemListDto = [
      new CheckinGuestReservationItemDto(),
      new CheckinGuestReservationItemDto(),
    ];

    reservationItemListDto[1].guestReservationItemId = guestReservationItemId;

    component.reservationItemListDto = reservationItemListDto;

    const res = component['getGuestToFnrh']();

    expect(res.guest).toEqual(reservationItemListDto[1]);
    expect(res.isSelected).toBeTruthy();
    expect(res.isValid).toBeTruthy();
    expect(res.active).toBeTruthy();
  });
});
