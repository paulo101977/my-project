import { forkJoin, of } from 'rxjs';
import { ReservationDto } from 'app/shared/models/dto/reservation-dto';
import { Location as Address } from 'app/shared/models/location';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { FnrhSaveDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';

export const RESULT = 'RESULT';
export const DOCUMENT_TYPE_ARR = 'DOCUMENT_TYPE_ARR';
export const GUEST_TYPE_ARR = 'GUEST_TYPE_ARR';
export const TRANSPORTATION_ARR = 'TRANSPORTATION_ARR';
export const NATIONALITY_ARR = 'NATIONALITY_ARR';
export const REASON_ARR = 'REASON_ARR';
export const OCCUPATION_ARR = 'OCCUPATION_ARR';
export const CHECKIN_HEADER_OBJ = 'CHECKIN_HEADER_OBJ';
export const RESERVATION_ITEM_LIST = 'RESERVATION_ITEM_LIST';
export const RESPONSABLE_ARR = 'RESPONSABLE_ARR';
export const ROOM_TYPE_ARR = 'ROOM_TYPE_ARR';
export const RESERVATION_RESULT = 'RESERVATION_RESULT';
export const ADDRESS = 'ADDRESS';
export const GUEST_RESERVATION = 'GUEST_RESERVATION';
export const GUEST_RESERVATION_RESULT = 'GUEST_RESERVATION_RESULT';

export class DATA {
  public static readonly DOCUMENT_TYPE_ARR = [
    {
      countrySubdivisionId: 1,
      id: 1,
      isMandatory: true,
      name: 'CPF',
      personType: 'N',
    }
  ];

  public static readonly GUEST_TYPE_ARR = [
    {
      code: '0001',
      guestTypeName: 'NORMAL',
      id: 1,
      isActive: true,
      isIncognito: false,
      isVip: false,
      propertyId: 1,
    }
  ];

  public static readonly TRANSPORTATION_ARR = [
    {
      id: 1,
      transportationTypeName: 'Avião',
    },
  ];

  public static readonly NATIONALITY_ARR = [
    {
      countrySubdivisionId: 1,
      id: 1,
      languageIsoCode: 'pt-BR',
      name: 'Brasil',
      nationality: 'Brasileira',
    }
  ];

  public static readonly REASON_ARR = [
    {
      chainId: 1,
      id: 62,
      isActive: true,
      propertyId: 0,
      reasonCategoryId: 7,
      reasonName: 'AAA',
    }
  ];

  public static readonly OCCUPATION_ARR = [
    {
      id: 1,
      occupation: 'Contador',
    }
  ];

  public static readonly CHECKIN_HEADER_OBJ = {
    accomodations: 0,
    adultCount: 1,
    arrivalDate: '2018-08-02T00:00:00',
    childCount: 0,
    childrenAge: [],
    color: 'FF6969',
    currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
    currencySymbol: 'R$',
    departureDate: '2019-01-19T00:00:00',
    externalComments: '',
    housekeepingStatusName: 'Sujo',
    housekeepingStatusPropertyId: '42c17b94-2190-46a6-8cfc-3bcdd7d38019',
    id: 124,
    internalComments: '',
    mealPlanTypeId: 2,
    numberObservations: 0,
    overnight: 170,
    paymentType: 'Uso da Casa',
    paymentTypeId: 1,
    propertyId: 1,
    quantityDouble: 0,
    quantitySingle: 3,
    receivedRoomTypeAbbreviation: 'STD',
    receivedRoomTypeId: 1,
    receivedRoomTypeName: 'Standard',
    requestedRoomTypeAbbreviation: 'STD',
    requestedRoomTypeId: 1,
    requestedRoomTypeName: 'Standard',
    reservationBudget: 13140,
    reservationCode: 'VRN9QFCQN',
    reservationItemBudgetHeaderDto: {
      commercialBudgetList: [
        {
          commercialBudgetDayList: [
            {
              baseRateAmount: 100,
              childRateAmount: 0,
              currencyId: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
              currencySymbol: '€',
              id: 0,
              mealPlanTypeId: 2,
            }
          ],
          commercialBudgetName: 'Tarifa base',
          currencyId: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
          currencySymbol: '€',
          id: 0,
          mealPlanTypeDefaultId: 2,
          mealPlanTypeDefaultName: 'Café da Manha',
          mealPlanTypeId: 2,
          mealPlanTypeName: 'Café da Manha',
          rateVariation: 0,
          roomTypeAbbreviation: 'BGR',
          roomTypeId: 63,
          totalBudget: 900
        }
      ],
      isSeparated: true,
      mealPlanTypeId: 2,
      reservationItemCode: '4JDF93BT7-0001',
      reservationItemId: 3696,
      reservationBudgetDtoList: [
        {
          agreementRate: 150,
          baseRate: 100,
          childMealPlanTypeRate: 0,
          childRate: 0,
          currencyId: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
          currencySymbol: '€',
          discount: 0,
          id: 17916,
          manualRate: 150,
          mealPlanTypeBaseRate: 50,
          mealPlanTypeId: 2,
          propertyBaseRateHeaderHistoryId: '96f17cec-42b7-42f1-bbd4-b23ca80e7115',
          rateVariation: 0,
          reservationBudgetUid: '00000000-0000-0000-0000-000000000000',
          reservationItemId: 0,
        }
      ]
    },
    reservationItemId: 158,
    reservationItemStatusId: 2,
    reservationItemStatusName: 'Check-In',
    roomId: 1,
    roomLayoutId: '88C38586-5C4B-489B-8BB9-0AA7D78DEA99',
    roomNumber: '101',
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
  };

  public static readonly RESERVATION_ITEM_LIST = [
    {
      businessSourceName: 'Nova origem',
      country: 'Brasil',
      division: 'RJ',
      documentInformation: '259.251.124-58',
      documentTypeName: 'CPF',
      guestRegistrationDocumentBirthDate: '1990-10-10T00:00:00',
      guestRegistrationId: '2f016f4f-b32d-42de-2ab3-08d6119f40e6',
      guestReservationItemId: 204,
      guestReservationItemName: 'Teste Grupo 2',
      guestStatusId: 2,
      id: 124,
      isChild: false,
      isLegallyIncompetent: false,
      isMain: true,
      isValid: true,
      personId: '96ee1826-1a71-4d85-a4b4-4581b9242289',
      reservationItemCode: 'VRN9QFCQN-0002',
      reservationItemId: 158,
      reservationItemStatusId: 2,
      subdivision: 'Rio de Janeiro',
    }
  ];

  public static readonly RESPONSABLE_ARR = [
    {
      age: 124,
      arrivingFromText: 'Rio de Janeiro',
      birthDate: '1894-06-08T00:00:00',
      checkinDate: '2018-07-30T18:46:08.927',
      documentTypeId: 0,
      email: 'william@email.com',
      firstName: 'William',
      fullName: 'William',
      gender: 'M',
      guestId: 19,
      guestReservationItemId: 45,
      guestTypeId: 1,
      id: '825a1564-699f-4b9e-bce9-08d5f6486835',
      isChild: false,
      isLegallyIncompetent: true,
      isMain: true,
      lastName: 'William',
      nationality: 1,
      nextDestinationText: 'Rio de Janeiro',
      person: {
        companyClientContactPersonDtoList: [],
        companyContactPersonList: [],
        contactInformationList: [],
        documentList: [],
        employeeList: [],
        guestList: [],
        id: '00000000-0000-0000-0000-000000000000',
        locationList: [],
        personInformationList: [],
        propertyContactPersonList: [],
      },
      phoneNumber: '+55 21 9976-6508',
      propertyId: 1,
      socialName: 'William',
      tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    }
  ];

  public static readonly ROOM_TYPE_ARR = [
    {
      abbreviation: 'STD',
      adultCapacity: 2,
      childCapacity: 1,
      hasConjugatedRooms: false,
      id: 1,
      name: 'Standard',
      overbooking: true,
      overbookingLimit: true,
      roomTypeBedTypeList: [
        {
          bedType: 1,
          bedTypeId: 1,
          count: 1,
          id: 0,
          optionalLimit: 0,
          roomTypeId: 1,
          typeName: 'Single'
        }
      ],
      totalConjugatedRooms: 0
    }
  ];

  public static readonly RESULT = forkJoin([
    of({
      items: DATA.DOCUMENT_TYPE_ARR
    }),
    of({
      items: DATA.GUEST_TYPE_ARR,
    }),
    of({
      items: DATA.TRANSPORTATION_ARR
    }),
    of({
      items: DATA.NATIONALITY_ARR
    }),
    of({
      items: DATA.REASON_ARR
    }),
    of({
      items: DATA.OCCUPATION_ARR,
    }),
    of(DATA.CHECKIN_HEADER_OBJ),
    of({
      items: DATA.RESERVATION_ITEM_LIST,
    }),
    of({
      items: DATA.RESPONSABLE_ARR
    }),
    of({
      items: DATA.ROOM_TYPE_ARR,
    }),
  ]);

  public static readonly RESERVATION_RESULT = <ReservationDto>{
    partnerComments: 'blalblaal',
    companyClientId: '2345',
    companyClientContactPersonId: '5678',
    companyClient: null,
    reservationConfirmation: null,
    walkin: true,
    contactId: 123,
    'id': 3491,
    'reservationUid': 'e0611efb-6795-4e4b-b131-02875367731c',
    'reservationCode': '74JHK1V55',
    'reservationVendorCode': '001',
    'propertyId': 1,
    'contactName': 'Paulo Teste 123',
    'contactEmail': 'pauloteste123@teste.com',
    'contactPhone': '',
    'internalComments': '',
    'externalComments': '',
    'groupName': '',
    'unifyAccounts': false,
    'businessSourceId': 4,
    'marketSegmentId': 33,
    'walkIn': false,
    launchDaily: true,
    'reservationItemList': [
      {
        'id': 3692,
        'reservationItemUid': 'ffcac2c7-52e3-4d14-ab91-8e0de4bc69b4',
        'reservationId': 3491,
        'estimatedArrivalDate': '2019-03-20T14:00:00',
        'checkInDate': '2019-01-08T21:00:00',
        'estimatedDepartureDate': '2019-03-21T12:00:00',
        'reservationItemCode': '74JHK1V55-0001',
        'requestedRoomTypeId': 63,
        'receivedRoomTypeId': 63,
        'roomId': 502,
        'adultCount': 2,
        'childCount': 3,
        'reservationItemStatusId': 2,
        'roomLayoutId': '56576890-480a-4763-a8d1-151fbf3c1689',
        'extraBedCount': 0,
        'extraCribCount': 0,
        'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
        'mealPlanTypeId': 2,
        'isMigrated': false,
        'commercialBudgetName': 'Tarifa base',
        'guestReservationItemList': [],
        'reservationBudgetList': [],
        'reservationItemStatusFormatted': 'Check-In',
        checkOutDate: '23/10/2019',
        requestedRoomType: null,
        receivedRoomType: null,
        roomLayout: null,
        gratuityTypeId: 123,
        room: null,
        currencySymbol: 'R$',
        ratePlanId: '123',
        externalReservationNumber: '4567',
        partnerReservationNumber: '7890',
      }
    ],
  };

  public static readonly ADDRESS = <Address>{
    complement: 'Bloco..',
    district: 'Rio de Janeiro',
    locality: 'Rua Doutor Alfredo Backer',
    number: '989',
    postalCode: 24452001,
    selectValueType: {
      type: 3
    },
    recordIdentification: '1',
    latitude: 12.33,
    longitude: 23.44,
    streetName: 'Rua do Matoso',
    streetNumber: '125',
    additionalAddressDetails: 'Próximo ao Mundial',
    countryCode: 'BR',
    ownerId: '345',
    browserLanguage: 'pt-br',
    neighborhood: 'Tijuca',
    cityId: 444,
    city: 'Rio de Janeiro',
    locationCategory: null,
    locationCategoryId: 2,
    addressTranslation: 'blabla',
    completeAddress: 'Apt 420',
    subdivision: 'Praça da Bandeira',
    division: 'Tijuca',
    country: 'Br',
    countrySubdivisionId: 'Sudeste',
    id: 1
  };

  public static readonly GUEST_RESERVATION = <Array<CheckinGuestReservationItemDto>>[
    {...new CheckinGuestReservationItemDto()},
    {...new CheckinGuestReservationItemDto()}
  ];

  public static readonly GUEST_RESERVATION_RESULT = <ItemsResponse<FnrhSaveDto>>{
    items: [
      {
        age: 44,
        occupationId: 1,
        socialName: '',
        responsibleGuestRegistrationId: '1',
        responsableName: '',
        healthInsurance: '',
        purposeOfTrip: 1,
        additionalInformation: '',
        mobilePhoneNumber: '',
        arrivingBy: 1,
        phoneNumber: '',
        arrivingFromText: 'Rio de Janeiro',
        birthDate: new Date('1974-11-19T00:00:00'),
        document: null,
        email: 'paulo01@email.com',
        firstName: 'Paulo',
        fullName: 'Paulo 1',
        gender: 'M',
        guestId: 107,
        guestReservationItemId: 4145,
        guestTypeId: 1,
        id: 'c5b783a3-5ade-4f65-fddc-08d6be9ff623',
        isChild: false,
        isLegallyIncompetent: false,
        lastName: '1',
        location: null,
        nationality: 5593,
        nextDestinationText: 'Rio de Janeiro',
        person: null,
        personId: '73E6578B-B3EE-4B2F-BBCD-ACE44981C710',
        propertyId: 1,
        tenantId: '00000000-0000-0000-0000-000000000000',
      },
      {
        age: 44,
        occupationId: 1,
        purposeOfTrip: 1,
        responsableName: '',
        healthInsurance: '',
        responsibleGuestRegistrationId: '1',
        socialName: '',
        additionalInformation: '',
        phoneNumber: '',
        mobilePhoneNumber: '',
        arrivingBy: 1,
        arrivingFromText: 'Rio de Janeiro',
        birthDate: new Date('1974-11-19T00:00:00'),
        document: null,
        email: 'paulo01@email.com',
        firstName: 'Paulo',
        fullName: 'Paulo 1',
        gender: 'M',
        guestId: 107,
        guestReservationItemId: 4145,
        guestTypeId: 1,
        id: 'c5b783a3-5ade-4f65-fddc-08d6be9ff624',
        isChild: false,
        isLegallyIncompetent: false,
        lastName: '1',
        location: null,
        nationality: 5593,
        nextDestinationText: 'Rio de Janeiro',
        person: null,
        personId: '73E6578B-B3EE-4B2F-BBCD-ACE44981C710',
        propertyId: 1,
        tenantId: '00000000-0000-0000-0000-000000000000',
      }
    ],
    hasNext: false,
  };
}

