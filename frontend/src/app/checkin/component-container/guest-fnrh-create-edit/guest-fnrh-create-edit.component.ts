import { Component, ComponentRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {
  DocumentTypeLegalEnum,
  DocumentTypeNaturalEnum
} from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subscription } from 'rxjs';
import { Location } from '@angular/common';
import * as _ from 'lodash';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { FnrhConfigInterface } from 'app/shared/components/fnrh/fnrh-config.interface';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { ReservationItemView } from 'app/shared/models/reserves/reservation-item-view';
import { PrintComunicator } from 'app/shared/interfaces/print-comunicator';
import { FnrhSaveDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { GuestFnrhSidebarComponent } from './../../components/sidebar/guest-fnrh-sidebar/guest-fnrh-sidebar.component';
import { GuestFnrhHeaderComponent } from 'app/checkin/components/guest-fnrh-header/guest-fnrh-header.component';
import { PrintAllGuestRegistrationComponent } from '../../components/print-all-guest-registration/print-all-guest-registration.component';
import { ShowHideContentEmitService } from 'app/shared/services/shared/show-hide-content-emit-service';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { ReservationItemViewService } from 'app/shared/services/reservation-item-view/reservation-item-view.service';
import { CheckinService } from 'app/shared/services/checkin/checkin.service';
import { ReservationItemStatusService } from 'app/shared/services/reservation-item-status/reservation-item-status.service';
import { GuestReservationItemService } from 'app/shared/services/guest-reservation-item/guest-reservation-item.service';
import { FnrhService } from 'app/shared/services/fnrh/fnrh.service';
import { LoadPageEmitService } from 'app/shared/services/shared/load-emit.service';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import { DocumentTypeResource } from 'app/shared/resources/document-type/document-type.resource';
import { ReservationResource } from 'app/shared/resources/reservation/reservation.resource';
import * as moment from 'moment';
import { UserService } from 'app/shared/services/user/user.service';
import { FnrhComponent } from 'app/shared/components/fnrh/fnrh.component';
import { AddressType } from 'app/shared/models/address-type-enum';
import { Location as Address } from 'app/shared/models/location';
import { TourismTaxResource } from 'app/tax/resources/tourism-tax.resource';
import { TourismTaxService } from 'app/tax/services/tourism-tax.service';
import { GuestReservationItemIdToSend } from 'app/tax/models/tourism-tax';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import {GuestFnrhService} from 'app/checkin/service/guest-fnrh.service';

@Component({
  selector: 'app-guest-fnrh-create-edit',
  templateUrl: './guest-fnrh-create-edit.component.html',
})
export class GuestFnrhCreateEditComponent implements OnInit, PrintComunicator, OnDestroy {
  public readonly MODAL_PRINT_ALL = 'modalPrintAll';
  private finishCheckin = false;

  private propertyId: number;
  public configHeaderPage: ConfigHeaderPageNew;
  public reservationItemListDto: Array<CheckinGuestReservationItemDto>;
  public checkInHeaderDto: CheckInHeaderDto;

  public guestReservationCheckedForCheckinList: Array<CheckinGuestReservationItemDto> = [];
  private invalidGuestReservationItemList: Array<CheckinGuestReservationItemDto>;
  public isWalkin: boolean;
  private reservationItemViewList: Array<ReservationItemView>;
  private reservationItemId: number;
  public totalReservationItem: number;
  public numberOfReservationItemThatAreDoingCheckin: number;

  public reservationCode: string;
  public reservationItemCode: string;


  public buttonConfirmCheckinDisabled: boolean;

  public fnrhList: Array<FnrhSaveDto>;

  // Fnrh dependencies
  public config = <FnrhConfigInterface>{};
  private documentTypesAutocomplete;
  private guestTypeList;
  private nationalityList;
  private vehicleList;
  private reasonList;
  private genderList;
  private occupationList;
  public responsableList;

  public reservationId: number;
  // Buttons dependencies
  public buttonPrintAllConfig: ButtonConfig;
  public buttonNotPrintConfig: ButtonConfig;
  public closePrintAllModalCallback: any;

  public buttonCancelConfig: ButtonConfig;

  @ViewChild(GuestFnrhHeaderComponent) childHeader;
  @ViewChild('sidebar') childSidebar: GuestFnrhSidebarComponent;
  @ViewChild('fnrhComponent') fnrhComponent: FnrhComponent;

  private fnrhIsModified: boolean;
  public readonly MODAL_CONFIRM_EDIT_FNRH = 'MODAL_CONFIRM_EDIT_FNRH';
  public readonly MODAL_CONFIRM_TOURISM_TAX = 'MODAL_CONFIRM_TOURISM_TAX';
  public guestCardCurrent: GuestCard;
  private lastFnrh: FnrhDto;
  private isRedirectToBudgetPage: boolean;

  // Subscriptions
  private uhChosenSubscription: Subscription;
  private fnrhListChangeSubscription: Subscription;
  private headerSidebarSubscription: Subscription;
  private nextGuestCardSubscription: Subscription;

  constructor(
    private checkinResource: CheckinResource,
    public _route: ActivatedRoute,
    private showHideContentEmitService: ShowHideContentEmitService,
    private commomService: CommomService,
    private documentTypeResource: DocumentTypeResource,
    private reservationResource: ReservationResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private route: Router,
    private reservationItemViewService: ReservationItemViewService,
    private reservationItemStatusService: ReservationItemStatusService,
    private guestReservationItemService: GuestReservationItemService,
    private checkinService: CheckinService,
    private location: Location,
    private fnrhService: FnrhService,
    private loadPageEmitService: LoadPageEmitService,
    private printService: PrintHelperService,
    private modalToggleService: ModalEmitToggleService,
    private userService: UserService,
    private tourismTaxService: TourismTaxService,
    private tourismTaxResource: TourismTaxResource,
    private guestFnrhService: GuestFnrhService
  ) {}

  ngOnInit(): void {
    const params = this._route.snapshot.params;
    this.propertyId = params.property;
    this.setConfigHeaderPage();
    this.setButtonConfig();

    this.isWalkin = false;
    this.reservationItemViewList = new Array<ReservationItemView>();


    if (params.reservationItemId) {
      this.reservationItemId = params.reservationItemId;
      this.reservationItemCode = this.reservationItemViewService.reduceAndGetCodeOfReservationItem(params.reservationItemCode);
      this.loadCompleteInfo();
    } else if (params.reservationId) {
      this.reservationId = params.reservationId;
      this.setDataWhenCheckinIsWalkin(params);
    }

    this.canConfirmCheckin();
    this.subscribeFnrhToUpdateHeaderAndSidebar();
    this.subscribeFnrhToNextGuestCard();
  }

  ngOnDestroy() {
    this.uhChosenSubscription.unsubscribe();
    this.fnrhListChangeSubscription.unsubscribe();
    this.headerSidebarSubscription.unsubscribe();
    this.nextGuestCardSubscription.unsubscribe();
  }

  private loadCompleteInfo() {
    let reservationId = this.reservationId;
    if (!this.reservationId) {
      reservationId = this.getReservationIdFromChildHeader();
    }

    this
      .checkinResource
      .getCompleteAuxInfo(
        this.propertyId,
        this.reservationItemId,
        reservationId,
        moment().locale()
      )
      .subscribe(result => {
        this.documentTypesAutocomplete = result[0].items;

        this.guestTypeList = this.commomService
          .toOption(result[1], 'id', 'guestTypeName');

        this.vehicleList = this.commomService
          .toOption(result[2], 'id', 'transportationTypeName');

        this.nationalityList = result[3].items;

        this.reasonList = this.commomService.toOption(result[4], 'id', 'reasonName');

        this.occupationList = this.commomService.toOption(result[5], 'id', 'occupation');

        this.checkInHeaderDto = result[6];

        this.reservationItemListDto = [];
        this.reservationItemListDto = result[7].items;

        this.responsableList = [...result[8].items];
      });
  }

  private getResponsables() {
    let reservationId = this.reservationId;
    if (!this.reservationId) {
      reservationId = this.getReservationIdFromChildHeader();
    }
    this.checkinResource.getResponsables(this.propertyId, reservationId).subscribe(response => {
      this.responsableList = [...response.items];
    });
  }

  private setDataWhenCheckinIsWalkin(params: Params): void {
    this.isWalkin = true;
    const reservationId = parseInt(params.reservationId, 10);
    this
      .reservationResource
      .getReservationWithOnlyReservationItemListById(reservationId)
      .subscribe(reservation => {
        this.reservationCode = reservation.reservationCode;
        reservation.reservationItemViewList.forEach(reservationItem => {
          if (this.reservationItemStatusService.canDoCheckin(reservationItem.status)) {
            this.reservationItemViewList.push(reservationItem);
          }
        });
        this.setPaginationDataOfHeader();

        if ( this.reservationItemViewList && this.reservationItemViewList.length > 0 ) {
          this.reservationItemId = this.reservationItemViewList[0].id;
          this.reservationItemCode = this.reservationItemViewService.reduceAndGetCodeOfReservationItem(
            reservation.reservationItemViewList[0].code,
          );
        }
        this.loadCompleteInfo();
      });
  }

  private setPaginationDataOfHeader(): void {
    this.totalReservationItem = this.reservationItemViewList.length;
    this.numberOfReservationItemThatAreDoingCheckin = 1;
  }

  private setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  private setButtonConfig() {
    this.buttonPrintAllConfig = new ButtonConfig();
    this.buttonPrintAllConfig.textButton = 'commomData.yes';
    this.buttonPrintAllConfig.buttonType = ButtonType.Primary;
    this.buttonPrintAllConfig.callback = this.printAllFnrh.bind(this);
    this.buttonPrintAllConfig.id = 'print-all';

    this.buttonNotPrintConfig = new ButtonConfig();
    this.buttonNotPrintConfig.textButton = 'commomData.not';
    this.buttonNotPrintConfig.buttonType = ButtonType.Secondary;
    this.buttonNotPrintConfig.callback = this.closeModalPrintAll.bind(this);
    this.buttonNotPrintConfig.id = 'not-print-all';

    this.buttonCancelConfig = new ButtonConfig();
    this.buttonCancelConfig.textButton = 'commomData.cancel';
    this.buttonCancelConfig.buttonType = ButtonType.Secondary;
    this.buttonCancelConfig.callback = this.goBackPreviewPage;
    this.buttonCancelConfig.id = 'fnrh-create-edit-cancel';

    this.closePrintAllModalCallback = this.closeModalPrintAll.bind(this);
  }

  private goBackPreviewPage = () => {
    this.location.back();
  }

  public showModalTourismTax() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_CONFIRM_TOURISM_TAX);
  }

  public confirmCheckin = () => {
    if (this.guestReservationItemListcheckedIsValid()) {
      this.saveCheckin().subscribe(() => {
        this.finishCheckin = true;

        if ( this.tourismTaxService.couldShow()) {
          this.showModalTourismTax();
        } else {
          this.showModalPrintAll();
        }
      });
    } else {
      this.showGuestListInvalidMessage();
    }
  }

  private showModalPrintAll() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_PRINT_ALL);
  }

  private closeModalPrintAll() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_PRINT_ALL);
    this.redirectFromCheckin();
  }

  @HostListener('window:afterprint', ['$event'])
  private redirectFromCheckin() {
    if (this.finishCheckin) {
      if (this.isWalkin) {
        this.confirmCheckinWhenIsWalkin();
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          `${this.translateService.instant('checkinModule.header.accommodationCheckin')} ${
            this.reservationItemCode
          } ${this.translateService.instant('commomData.successfullyDone')}`,
        );
        this.route.navigate(['p', this.propertyId, 'reservation']);
      }
    }
  }

  private saveCheckin(): Observable<any> {
    const guestForCheckinIdList = this.getCheckinIdListFromChildSidebar();
    const reservationId = this.getReservationIdFromChildHeader();
    return this.checkinResource.confirmCheckin(guestForCheckinIdList, reservationId, this.propertyId);
  }

  private getReservationIdFromChildHeader(): number {
    return this.childHeader.checkInHeaderDto ? this.childHeader.checkInHeaderDto.id : 0;
  }

  private getCheckinIdListFromChildSidebar(): Array<GuestReservationItemIdToSend> {
    if (this.childSidebar.guestReservationCheckedForCheckinList) {
      return this.childSidebar.guestReservationCheckedForCheckinList.map(guest => {
        return <GuestReservationItemIdToSend>{guestReservationItemId: guest.guestReservationItemId};
      });
    }

    return [];
  }

  private guestReservationItemListcheckedIsValid(): boolean {
    this.invalidGuestReservationItemList = this.guestReservationItemService
      .getInvalidGuestReservationItemList(this.guestReservationCheckedForCheckinList);
    return this.invalidGuestReservationItemList.length == 0;
  }

  private showGuestListInvalidMessage(): void {
    const formattedMessage = this
      .guestReservationItemService
      .getFormattedMessageOfGuestInvalidList(
        this.invalidGuestReservationItemList
      );
    if (this.invalidGuestReservationItemList.length > 1) {
      this.toasterEmitService.emitChange(
        SuccessError.error,
        `${this.translateService.instant(
          'checkinModule.header.errorValidationGuestListPartOne',
        )} ${formattedMessage} ${this.translateService.instant('checkinModule.header.errorValidationGuestListPartTwo')}`,
      );
    } else {
      this.toasterEmitService.emitChange(
        SuccessError.error,
        `${this.translateService.instant(
          'checkinModule.header.errorValidationGuestPartOne',
        )} ${formattedMessage} ${this.translateService.instant('checkinModule.header.errorValidationGuestPartTwo')}`,
      );
    }
  }

  private confirmCheckinWhenIsWalkin(): void {
    _.remove(this.reservationItemViewList, (reservationItemView, index) => {
      return index == 0;
    });
    if (this.reservationItemViewList.length === 0) {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        `${this.translateService.instant('checkinModule.header.reservationCheckin')} ${
          this.reservationCode
        } ${this.translateService.instant('commomData.successfullyDone')}`,
      );
      this.route.navigate(['p', this.propertyId, 'reservation', 'new'], { queryParams: { iswalkin: 1 } });
    } else {
      this.reservationItemId = this.reservationItemViewList[0].id;
      this.reservationItemCode = this
        .reservationItemViewService
        .reduceAndGetCodeOfReservationItem(
          this.reservationItemViewList[0].code
        );
      this.numberOfReservationItemThatAreDoingCheckin++;
      this.loadCompleteInfo();
    }
  }

  private getReservationItems(reservationItemId: number): void {
    this.checkinResource.getReservationGuestList(this.propertyId, reservationItemId)
      .subscribe((response) => {
        this.reservationItemListDto = [];
        this.reservationItemListDto = response.items;
      });
  }

  public hideAllPopovers(): void {
    this.showHideContentEmitService.emitChange(ShowHide.hide);
  }

  public setFnrhToView(guestCard: any) {
    this.guestCardCurrent = guestCard;
    this.isRedirectToBudgetPage = false;
    if (guestCard.isFirsTime) {
      this.changeCard(guestCard);
    } else {
      this.fnrhIsModified
        ? this.modalToggleService.emitToggleWithRef(this.MODAL_CONFIRM_EDIT_FNRH)
        : this.changeCard(guestCard);
    }
  }

  private fnrhMapper(guestCard: GuestCard, fnrhFromGuest: FnrhDto) {
    const dataReservation = this.guestFnrhService.getDataReservation(this.checkInHeaderDto);
    const { reservationItemCode } = this;

    this.config = <FnrhConfigInterface>{
      guestReservationItemId: guestCard.guest.guestReservationItemId,
      fnrh: this.prepareFnrh(fnrhFromGuest),
      genderList: [
        <SelectObjectOption>{ key: 'M', value: 'commomData.gender.male' },
        <SelectObjectOption>{ key: 'F', value: 'commomData.gender.female' },
      ],
      occupationList: this.occupationList,
      guestTypeList: this.guestTypeList,
      nationalityList: this.nationalityList,
      vehicleList: this.vehicleList,
      reasonList: this.reasonList,
      documentTypesAutocomplete: this.documentTypesAutocomplete,
      guest: guestCard,
      dataReservation,
      reservationItemCode,
    };
  }

  public setAddressToApply(addressToApply: Address) {
    if (addressToApply) {
      this.fnrhService.fnrhAddressToApply = addressToApply;
    }
  }

  public setGuestReservationItemListForCheckin(
    guestReservationCheckedForCheckinList: Array<CheckinGuestReservationItemDto>
  ): void {
    this.guestReservationCheckedForCheckinList = guestReservationCheckedForCheckinList;
  }

  private canConfirmCheckin(): void {
    let uhOk = false;
    let fnrhOk = false;
    this.buttonConfirmCheckinDisabled = true;

    this.uhChosenSubscription = this.checkinService.uhChosenChangeEmitted$
      .subscribe(uhChosen => {
        uhOk = uhChosen;
        this.buttonConfirmCheckinDisabled = !(uhOk && fnrhOk);
      });
    this.fnrhListChangeSubscription = this.checkinService.fnrhListChangeEmitted$
      .subscribe(hasFnrhValid => {
        fnrhOk = hasFnrhValid;
        this.buttonConfirmCheckinDisabled = !(uhOk && fnrhOk);
      });
  }

  private subscribeFnrhToUpdateHeaderAndSidebar(): void {
    this.headerSidebarSubscription = this.checkinService.changeEmitted$
      .subscribe(() => {
        this.getReservationItems(this.reservationItemId);
      });
  }

  public printAllFnrh() {
    this.modalToggleService.emitToggleWithRef(this.modalToggleService);

    if (this.guestReservationCheckedForCheckinList.length === 0) {
      this
        .toasterEmitService
        .emitChange(
          SuccessError.error,
          this.translateService.instant('alert.noneFnrhSelected')
        );
      return;
    } else {
      this.reservationResource
        .getGuestRegistrationByReservationitem(
          this.config.guest.guest.reservationItemId,
          this.propertyId,
          navigator.language.toLocaleLowerCase()
        )
        .subscribe(result => {
          this.fnrhList = result.items.filter(item => {
            const items = this
              .guestReservationCheckedForCheckinList
              .filter(element => element.guestRegistrationId == item.id);
            return ( items && items.length );
          });
          this.fnrhList.forEach( (item: any) => {
            item.propertyId = this.propertyId;
            if (this.config.fnrh) {
              item.guestPolicyPrint = this.config.fnrh.guestPolicyPrint;
            }

            item.document = {
              documentTypeName: DocumentTypeNaturalEnum[item.documentTypeId] || DocumentTypeLegalEnum[item.documentTypeId],
              documentInformation: item.document
            };
          });
          this.printService.printComponent(PrintAllGuestRegistrationComponent, this);
        });
    }
  }

  populateComponent(component: ComponentRef<any>) {
    (<PrintAllGuestRegistrationComponent>component.instance).fnrhList = this.fnrhList;
    (<PrintAllGuestRegistrationComponent>component.instance).genderList = this.config.genderList;
    (<PrintAllGuestRegistrationComponent>component.instance).ocupationList = this.config.occupationList;
    (<PrintAllGuestRegistrationComponent>component.instance).transportList = this.config.vehicleList;
    (<PrintAllGuestRegistrationComponent>component.instance).reasonList = this.config.reasonList;
    (<PrintAllGuestRegistrationComponent>(
      component.instance
    )).nationalitylist = this.commomService.toOption(
      this.config.nationalityList,
      'id',
      'name',
    );
    (<PrintAllGuestRegistrationComponent>component.instance).guestTypeList = this.config.guestTypeList;
  }

  public fnrhChange(fnrh) {
    if (this.lastFnrh && fnrh) {
      this.fnrhIsModified = this.fnrhIsPropertiesModified(this.lastFnrh, fnrh);
    }
  }

  public cancelNotSaveFnrh() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_CONFIRM_EDIT_FNRH);
  }

  public cancelTourismTax() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_CONFIRM_TOURISM_TAX);
    this.showModalPrintAll();
  }

  public confirmTourismTax() {
    const guestForCheckinIdList = this.getCheckinIdListFromChildSidebar();
    this.modalToggleService.emitToggleWithRef(this.MODAL_CONFIRM_TOURISM_TAX);
    this
      .tourismTaxResource
      .launchTourismTax(guestForCheckinIdList)
      .subscribe( () => {
        this.showModalPrintAll();
      }, () => {
        this.showModalPrintAll();
      });
  }

  public confirmNotToSaveFnrh() {
    this.isRedirectToBudgetPage
      ? this.redirectToBdgetPage()
      : this.changeCard(this.guestCardCurrent);
    this.modalToggleService.emitToggleWithRef(this.MODAL_CONFIRM_EDIT_FNRH);
  }

  private changeFnrh(guestCard: GuestCard) {
    if (guestCard && guestCard.guest) {
      this.fnrhComponent.resetFnrh();
      this.checkinResource.getFnrh(
        guestCard.guest.guestReservationItemId,
        this.propertyId,
        this.userService.getUserpreferredLanguage() || navigator.language.toLocaleLowerCase()
      )
        .subscribe(fnrhDtoResponse => {
          this.lastFnrh = this.prepareFnrh(fnrhDtoResponse);
          this.fnrhMapper(guestCard, fnrhDtoResponse);
          this.loadPageEmitService.emitChange(ShowHide.hide);
          this.getResponsables();
        });
    }
  }

  private changeCard(guestCard: GuestCard) {
    this.childSidebar.chooseItem(guestCard);
    this.changeFnrh(guestCard);
  }

  public subscribeFnrhToNextGuestCard(): void {
    this.nextGuestCardSubscription = this.checkinService.changeEmitted$
      .subscribe(guestReservationItemId => {
        this.nextGuestCardToCheckin(guestReservationItemId);
      });
  }

  public nextGuestCardToCheckin(guestReservationItemId: number) {
    const nextGuestToCheckin = this.guestReservationCheckedForCheckinList.find(
      guestToCheckin => guestToCheckin.guestReservationItemId != guestReservationItemId && !guestToCheckin.isValid,
    );
    if (nextGuestToCheckin) {
      const guestCard = this.childSidebar.guestCards
        .find(card => card.guest.guestReservationItemId == nextGuestToCheckin.guestReservationItemId);
      if (guestCard) {
        this.changeCard(guestCard);
      }
    } else {
      const guestCard = this.childSidebar.guestCards.find(
        card => card.guest.guestReservationItemId == guestReservationItemId
      );
      this.changeCard(guestCard);
    }
  }

  private fnrhIsPropertiesModified(last, current) {
    return (last.document != current.basicData.document)
      || (last.documentTypeId != current.basicData.documentTypeId)
      || (last.email != current.basicData.email)
      || (last.fullName != current.basicData.fullName)
      || (last.birthDate != current.basicData.birthDate)
      || (last.gender != current.basicData.gender)
      || (last.guestTypeId != current.basicData.guestType)
      || (last.nationality != current.basicData.nationality)
      || (last.responsable != current.basicData.responsable)
      || (last.incapable != current.basicData.incapable)
      || (last.phoneNumber != current.basicData.phone)
      || (last.mobilePhoneNumber != current.basicData.cellPhone)
      || (last.occupationId != current.complementaryInformation.occupation)
      || (last.arrivingBy != current.complementaryInformation.vehicle)
      || (last.arrivingFromText != current.complementaryInformation.lastAddress)
      || (last.nextDestinationText != current.complementaryInformation.nextAddress)
      || (last.purposeOfTrip != current.complementaryInformation.reasonForTravel)
      || (last.healthInsurance != current.complementaryInformation.medicalAgreement)
      || (last.socialName != current.complementaryInformation.socialName)
      || (last.additionalInformation != current.additionalInformation.remarks)
      || (last.location.postalCode != current.address.postalCode)
      || (last.location.streetName != current.address.streetName)
      || (last.location.streetNumber != current.address.streetNumber)
      || (last.location.additionalAddressDetails != current.address.additionalAddressDetails)
      || (last.location.division != current.address.division)
      || (last.location.subdivision != current.address.subdivision)
      || (last.location.neighborhood != current.address.neighborhood)
      || (last.location.latitude != current.address.latitude)
      || (last.location.longitude != current.address.longitude)
      || (last.location.countryCode != current.address.countryCode)
      || (last.location.country != current.address.country)
      || (last.location.completeAddress != current.address.completeAddress)
      || (last.location.locationCategoryId != current.address.locationCategoryId);
  }

  private prepareFnrh(fnrh: FnrhDto) {

    return <FnrhDto> {
      id: fnrh.id,
      receiveOffers: fnrh['person'].receiveOffers,
      sharePersonData: fnrh['person'].sharePersonData,
      allowContact: fnrh['person'].allowContact,
      personId: fnrh.personId,
      document: fnrh.document,
      documentTypeId: fnrh.documentTypeId ? fnrh.documentTypeId : 1,
      guestReservationItemId: fnrh.guestReservationItemId,
      firstName: fnrh.firstName,
      socialName: fnrh.socialName,
      lastName: fnrh.lastName,
      fullName: fnrh.fullName,
      email: fnrh.email,
      birthDate: fnrh.birthDate,
      age: fnrh.age,
      guestId: fnrh.guestId,
      gender: fnrh.gender,
      occupationId: fnrh.occupationId,
      guestTypeId: fnrh.guestTypeId,
      phoneNumber: fnrh.phoneNumber,
      mobilePhoneNumber: fnrh.mobilePhoneNumber,
      nationality: fnrh.nationality,
      purposeOfTrip: fnrh.purposeOfTrip,
      arrivingBy: fnrh.arrivingBy,
      arrivingFrom: fnrh.arrivingFrom,
      arrivingFromText: fnrh.arrivingFromText,
      nextDestination: fnrh.nextDestination,
      nextDestinationText: fnrh.nextDestinationText,
      additionalInformation: fnrh.additionalInformation,
      healthInsurance: fnrh.healthInsurance,
      location: {
        ownerId: fnrh.location.ownerId,
        latitude: fnrh.location.latitude,
        longitude: fnrh.location.longitude,
        streetName: fnrh.location.streetName,
        streetNumber: fnrh.location.streetNumber,
        additionalAddressDetails: fnrh.location.additionalAddressDetails ? fnrh.location.additionalAddressDetails : null,
        postalCode: fnrh.location.postalCode,
        countryCode: fnrh.location.countryCode,
        locationCategory: fnrh.location.locationCategory,
        neighborhood: fnrh.location.neighborhood,
        cityId: fnrh.location.cityId,
        city: fnrh.location.city,
        addressTranslation: fnrh.location.addressTranslation,
        completeAddress: fnrh.location.completeAddress == ',  -  -  -  - ' ? null : fnrh.location.completeAddress,
        browserLanguage: fnrh.location.browserLanguage,
        locationCategoryId: fnrh.location.locationCategoryId || AddressType.Residencial,
        subdivision: fnrh.location.subdivision,
        division: fnrh.location.division,
        country: fnrh.location.country,
        id: fnrh.location.id,
      },
      incapable: this.getIncapable(fnrh),
      responsable: this.getReponsable(fnrh),
      isChild: fnrh.isChild,
      isMain: fnrh.isMain,
      childAge: fnrh.childAge,
      guestRegistrationDocumentList: fnrh.guestRegistrationDocumentList,
      guestPolicyPrint: fnrh.guestPolicyPrint
    };
  }

  private getIncapable(fnrh) {
    let incapable = fnrh.incapable ? fnrh.incapable : false;
    if (!incapable) {
      incapable = fnrh.isLegallyIncompetent ? fnrh.isLegallyIncompetent : false;
    }
    return incapable;
  }

  private getReponsable(fnrh) {
    let idReponsable = fnrh.responsibleGuestRegistrationId ? fnrh.responsibleGuestRegistrationId : null;
    if (!idReponsable) {
      idReponsable = fnrh.responsable ? fnrh.responsable : null;
    }
    return idReponsable;
  }

  public goToReservationBudget() {
    this.isRedirectToBudgetPage = true;
    this.fnrhIsModified
      ? this.modalToggleService.emitToggleWithRef(this.MODAL_CONFIRM_EDIT_FNRH)
      : this.redirectToBdgetPage();
  }

  private redirectToBdgetPage() {
    this.route
      .navigate(['p', this.propertyId, 'rm', 'reservation-budget', this.checkInHeaderDto.id]);
  }

  public fnrhSaved(fnrhData) {
    let resource, message;

    if (!fnrhData.id || fnrhData.id == GuidDefaultData) {
      resource = this.checkinResource.createFnrh(fnrhData);
      message = this.translateService.instant('commomData.fnrh.actionsMessages.save');
    } else {
      resource = this.checkinResource.updateFnrh(fnrhData);
      message = this.translateService.instant('commomData.fnrh.actionsMessages.update');
    }

    resource
      .subscribe(() => {
        this.toasterEmitService.emitChange(SuccessError.success, message);
        this.checkinService.nextCardGuest(this.config.guestReservationItemId);
      });
  }
}



