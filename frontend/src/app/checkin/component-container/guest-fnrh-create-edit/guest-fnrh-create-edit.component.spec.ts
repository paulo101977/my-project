import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { GuestFnrhCreateEditComponent } from './guest-fnrh-create-edit.component';
import { GuestFnrhSidebarComponent } from './../../components/sidebar/guest-fnrh-sidebar/guest-fnrh-sidebar.component';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { ReasonDto } from 'app/shared/models/dto/checkin/reason-dto';
import { NationalityDto } from 'app/shared/models/dto/checkin/nationality-dto';
import { PropertyGuestTypeDto } from 'app/shared/models/dto/property-guest-type-dto';
import { TransportationTypeDto } from 'app/shared/models/dto/checkin/transportation-type-dto';
import { documentTypeListData } from 'app/shared/mock-data/document-type-data';
import { NationalityDtoData } from 'app/shared/mock-data/nationality-data';
import { TransportationTypeDtoData } from 'app/shared/mock-data/transportation-type-dto';
import { PropertyGuestTypeDtoData } from 'app/shared/mock-data/guest-type-data';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { ReasonDtoData } from 'app/shared/mock-data/reason-travel-dto';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { CheckinGuestReservationItemDtoData } from 'app/shared/mock-data/guest-reservation-item-dto-data';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { FnrhDtoData } from 'app/shared/mock-data/fnrh-dto-data';
import { Reservation } from 'app/shared/models/reserves/reservation';
import { ReservationItemView } from 'app/shared/models/reserves/reservation-item-view';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { CheckinGuestReservationItemDtoDataList } from 'app/shared/mock-data/checkin-guest-reservation-item-dto';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { OccupationDto } from 'app/shared/models/dto/checkin/occupation-dto';
import { OccupationDtoData } from 'app/shared/mock-data/occupation-dto-data';
import { of, throwError } from 'rxjs';
import {
  ADDRESS,
  DATA,
  GUEST_RESERVATION,
  GUEST_RESERVATION_RESULT,
  RESERVATION_RESULT,
  RESULT
} from './mock-data';
import * as moment from 'moment';
import {
  PrintAllGuestRegistrationComponent
} from 'app/checkin/components/print-all-guest-registration/print-all-guest-registration.component';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';
import {
  ActivatedRouteStub,
  createServiceStub,
  fnrhServiceStub,
  locationStub,
  modalToggleServiceStub,
  userServiceStub,
  showHideContentEmitServiceStub,
  routerStub
} from '../../../../../testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import { DocumentTypeResource } from 'app/shared/resources/document-type/document-type.resource';
import { ClientDocumentTypesDto } from 'app/shared/models/dto/client/client-document-types';
import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import { ReservationResource } from 'app/shared/resources/reservation/reservation.resource';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { ReservationItemViewService } from 'app/shared/services/reservation-item-view/reservation-item-view.service';
import { ReservationItemStatusService } from 'app/shared/services/reservation-item-status/reservation-item-status.service';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';
import { GuestReservationItemService } from 'app/shared/services/guest-reservation-item/guest-reservation-item.service';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { DATA as DATACHECKIN, GUEST_CARD_LIST } from 'app/checkin/mock-data';
import { tourismTaxResourceStub, tourismTaxServiceStub } from 'app/tax/testing/tax-tourism';
import { GuestReservationItemIdToSend } from 'app/tax/models/tourism-tax';


@Component({
  selector: 'app-fnrh',
  template: ''
}) class FnrhWrapperComponent {
  resetFnrh() {}
}

@Component({
  selector: 'app-guest-fnrh-sidebar',
  template: ''
}) class SidebarWrapperComponent {}

const reservationItemListDtoResponse = <ItemsResponse<any>>{
  items: []
};
const documentTypes: ItemsResponse<ClientDocumentTypesDto> = {
  items: documentTypeListData,
  hasNext: false,
};
const guestTypes: ItemsResponse<PropertyGuestTypeDto> = {
  items: [PropertyGuestTypeDtoData],
  hasNext: false,
};
const nationalities: ItemsResponse<NationalityDto> = {
  items: [NationalityDtoData],
  hasNext: false,
};
const vehicles: ItemsResponse<TransportationTypeDto> = {
  items: [TransportationTypeDtoData],
  hasNext: false,
};
const reasons: ItemsResponse<ReasonDto> = {
  items: [ReasonDtoData],
  hasNext: false,
};
const occupations: ItemsResponse<OccupationDto> = {
  items: [OccupationDtoData],
  hasNext: false,
};
const data$ = DATA[RESULT];
const result = <ItemsResponse<CheckinGuestReservationItemDto>>{
  items: [
    {
      ...new CheckinGuestReservationItemDto()
    },
    {
       ...new CheckinGuestReservationItemDto()
    }
  ],
  hasNext: false,
};
const response = new FnrhDto();
response.id = '123678';
const reservation = new Reservation();
reservation.normalizeResponse(DATA[RESERVATION_RESULT]);
const guestData = {...DATA[GUEST_RESERVATION_RESULT]};
const reservationItemCode = '123';

// TODO: move to stub folder: [DEBIT: 7506]
const checkinResourceStub: Partial<CheckinResource> =  {
  getGuestTypesByPropertyId: (propertyId: number) => of( guestTypes ),
  getNationalities: (languageIsoCode: string) => of( nationalities ),
  getTransportationType: () => of( vehicles ),
  getReasonTravel: (propertyId: number, reasonCategoryId: number) => of( reasons ),
  getHeader: (reservationItemId: number) => of( null ),
  getReservationGuestList: (propertyId: number, reservationItemId: number) => of( reservationItemListDtoResponse ),
  getOccupations: () => of( occupations ),
  getCompleteAuxInfo: (propertyId, reservationItemId, reservationId, language) =>  data$ ,
  getResponsables: (propertyId: number, reservationId) => of ( result ),
  confirmCheckin: (guestReservationItemIdList: Array<any>, reservationId: number, propertyId: number) => of( null ),
  getFnrh: (guestReservationItemId: number, propertyId: number, languageIsoCode: string) => of( response )
};

const documentTypeResource = createServiceStub(DocumentTypeResource, {
  getDocumentTypesByPersonTypeAndCountryCode: (personType: string, countryCode: string) => of( documentTypes )
});

const reservationResourceStub = createServiceStub(ReservationResource, {
  getReservationWithOnlyReservationItemListById: (reservationId: number) => of ( reservation),
  getGuestRegistrationByReservationitem: (reservationItemId, propertyId, language) => of ( guestData )
});

const toasterEmitServiceStub = createServiceStub(ToasterEmitService, {
  emitChange(isSuccess: SuccessError, message: string) {}
});

const reservationItemViewService = createServiceStub(ReservationItemViewService, {
  reduceAndGetCodeOfReservationItem: (code: string) => reservationItemCode
});

const reservationItemStatusServiceStub = createServiceStub(ReservationItemStatusService, {
  canDoCheckin: (reservationStatusEnum: ReservationStatusEnum) =>  true
});

const guestReservationItemServiceStub = createServiceStub(GuestReservationItemService, {
  getFormattedMessageOfGuestInvalidList: (checkinGuestReservationItemList: Array<CheckinGuestReservationItemDto>) => '',
  getInvalidGuestReservationItemList: (list: CheckinGuestReservationItemDto[]) => []
});



describe('GuestFnrhCreateEditComponent', () => {
  let component: GuestFnrhCreateEditComponent;
  let fixture: ComponentFixture<GuestFnrhCreateEditComponent>;
  let checkinResource;

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        imports: [
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateTestingModule,
        ],
        declarations: [
          GuestFnrhCreateEditComponent,
          FnrhWrapperComponent,
          SidebarWrapperComponent,
        ],
        providers: [
          FormBuilder,
          documentTypeResource,
          reservationResourceStub,
          toasterEmitServiceStub,
          reservationItemViewService,
          reservationItemStatusServiceStub,
          guestReservationItemServiceStub,
          locationStub,
          fnrhServiceStub,
          modalToggleServiceStub,
          userServiceStub,
          showHideContentEmitServiceStub,
          routerStub,
          tourismTaxResourceStub,
          tourismTaxServiceStub,
          { provide: CheckinResource, useValue: checkinResourceStub},
          { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        ],
        schemas: [NO_ERRORS_SCHEMA]
      });

    fixture = TestBed.createComponent(GuestFnrhCreateEditComponent);
    component = fixture.componentInstance;

    checkinResource =  fixture.debugElement.injector.get(CheckinResource);
    spyOn<any>(component['documentTypeResource'], 'getDocumentTypesByPersonTypeAndCountryCode');
    spyOn(checkinResource, 'getGuestTypesByPropertyId').and.callThrough();
    spyOn(checkinResource, 'getNationalities').and.callThrough();
    spyOn(checkinResource, 'getTransportationType').and.callThrough();
    spyOn(checkinResource, 'getReasonTravel').and.callThrough();
    spyOn(checkinResource, 'getHeader').and.callThrough();
    spyOn(checkinResource, 'getReservationGuestList').and.callThrough();
    spyOn(checkinResource, 'getOccupations').and.callThrough();


    component.checkInHeaderDto = new CheckInHeaderDto();
    component.checkInHeaderDto.id = 1;
    component.checkInHeaderDto.roomNumber = '602';

    component.childSidebar['chooseItem'] = () => {};
    fixture.detectChanges();
  });

  describe('on init', () => {
    beforeEach( () => {
      spyOn<any>(component, 'setConfigHeaderPage').and.callFake( () => {} );
      spyOn<any>(component, 'setButtonConfig').and.callFake( () => {} );
      spyOn<any>(component, 'canConfirmCheckin').and.callFake( () => {} );
      spyOn<any>(component, 'subscribeFnrhToUpdateHeaderAndSidebar').and.callFake( () => {} );
      spyOn<any>(component, 'subscribeFnrhToNextGuestCard').and.callFake( () => {} );
    });

    it('should check common methods', () => {
      component.ngOnInit();

      expect(component['setConfigHeaderPage']).toHaveBeenCalled();
      expect(component['setButtonConfig']).toHaveBeenCalled();
      expect(component['canConfirmCheckin']).toHaveBeenCalled();
      expect(component['subscribeFnrhToUpdateHeaderAndSidebar']).toHaveBeenCalled();
      expect(component['subscribeFnrhToNextGuestCard']).toHaveBeenCalled();
      expect(component['isWalkin']).toBeFalsy();
    });

    it('should init with params reservationItemId', () => {
      const reservationItemId = 123;

      spyOn(component['reservationItemViewService'], 'reduceAndGetCodeOfReservationItem').and.callThrough();
      spyOn<any>(component, 'loadCompleteInfo').and.callFake(() => {} );

      component._route.snapshot.params = {
        reservationItemId,
        reservationItemCode
      };

      component.ngOnInit();


      expect(component['reservationItemId']).toEqual(reservationItemId);
      expect(component['reservationItemCode']).toEqual(reservationItemCode);
      expect(component['reservationItemViewService'].reduceAndGetCodeOfReservationItem).toHaveBeenCalledWith(
        reservationItemCode
      );
      expect(component['loadCompleteInfo']).toHaveBeenCalled();
    });

    it('should init with params reservationId', () => {
      const reservationId = 123;

      const params = component._route.snapshot.params = {
        reservationId,
      };

      spyOn<any>(component, 'setDataWhenCheckinIsWalkin').and.callFake(f => f );

      component.ngOnInit();

      expect(component['reservationId']).toEqual(reservationId);
      expect(component['setDataWhenCheckinIsWalkin']).toHaveBeenCalledWith(params);
    });

  });

  describe('on loadCompleteInfo', () => {
    let reservationItemId;
    let reservationId;

    beforeEach( () => {
      reservationId = 6;
      reservationItemId = component['reservationItemId'] = 123;
      spyOn<any>(component, 'getReservationIdFromChildHeader').and.callFake( () => reservationId );
      spyOn(component['checkinResource'], 'getCompleteAuxInfo').and.callThrough();
      spyOn<any>(component, 'loadCompleteInfo').and.callThrough();
    });

    it('should test without reservationId', fakeAsync(() => {
      const propertyId = component['propertyId'] = 1;
      const locale = moment().locale();

      component.reservationId = null;
      component['loadCompleteInfo']();

      tick();

      expect(component['getReservationIdFromChildHeader']).toHaveBeenCalled();
      expect(component['checkinResource'].getCompleteAuxInfo).toHaveBeenCalledWith(
        propertyId,
        reservationItemId,
        reservationId,
        locale
      );
    }));

    it('should test with reservationId', fakeAsync(() => {
      const propertyId = component['propertyId'] = 2;
      const locale = moment().locale();

      reservationId = 77;
      component.reservationId = reservationId;
      component['loadCompleteInfo']();

      tick();

      expect(component['getReservationIdFromChildHeader']).not.toHaveBeenCalled();
      expect(component['checkinResource'].getCompleteAuxInfo).toHaveBeenCalledWith(
        propertyId,
        reservationItemId,
        reservationId,
        locale
      );
    }));

  });

  describe('on getResponsables', () => {
    let propertyId;
    let reservationId;

    beforeEach( () => {
      reservationId = 6;
      propertyId = component['propertyId'] = 11;
      spyOn<any>(component, 'getReservationIdFromChildHeader').and.callFake( () => reservationId );
      spyOn(component['checkinResource'], 'getResponsables').and.callThrough();

    });

    it('should test without reservationId', fakeAsync(() => {
      component.reservationId = null;
      component['getResponsables']();
      tick();

      expect(component['getReservationIdFromChildHeader']).toHaveBeenCalled();
      expect(component['checkinResource'].getResponsables).toHaveBeenCalledWith(propertyId, reservationId);
      expect(component.responsableList).toEqual(result.items);
    }));

    it('should test with reservationId', fakeAsync(() => {
      reservationId = component['reservationId'] = 100;
      component['getResponsables']();
      tick();

      expect(component['getReservationIdFromChildHeader']).not.toHaveBeenCalled();
      expect(component['checkinResource'].getResponsables).toHaveBeenCalledWith(propertyId, reservationId);
      expect(component.responsableList).toEqual(result.items);
    }));

  });

  describe('on setDataWhenCheckinIsWalkin',  () => {
    const params = { reservationId: '123' };

    beforeEach( () => {
      spyOn<any>(component, 'loadCompleteInfo').and.callFake(() => {} );
      spyOn<any>(component['reservationResource'], 'getReservationWithOnlyReservationItemListById')
        .and
        .callThrough();
      spyOn<any>(component, 'setPaginationDataOfHeader').and.callFake(() => {} );
      spyOn(component['reservationItemViewService'], 'reduceAndGetCodeOfReservationItem')
        .and
        .callThrough();
    });

    it( 'should test without status', fakeAsync(() => {
      spyOn(component['reservationItemStatusService'], 'canDoCheckin')
        .and.returnValue(false );
      component['reservationItemViewList'] = [];

      component['setDataWhenCheckinIsWalkin'](params);

      tick();

      expect(component['isWalkin']).toEqual(true);
      expect(component['reservationCode']).toEqual(reservation.reservationCode);
      expect(component['loadCompleteInfo']).toHaveBeenCalled();
      expect(component['setPaginationDataOfHeader']).toHaveBeenCalled();
      expect(
        component['reservationItemViewService'].reduceAndGetCodeOfReservationItem
      ).not.toHaveBeenCalled();
    }));

    it( 'should test setDataWhenCheckinIsWalkin with status', fakeAsync(() => {
      spyOn(component['reservationItemStatusService'], 'canDoCheckin')
        .and.returnValue(true );
      reservation.normalizeResponse(DATA[RESERVATION_RESULT]);
      component['reservationItemViewList'] = [];

      component['setDataWhenCheckinIsWalkin'](params);

      tick();

      expect(component['isWalkin']).toEqual(true);
      expect(component['reservationItemCode']).toEqual(reservationItemCode);
      expect(component['reservationCode']).toEqual(reservation.reservationCode);
      expect(component['loadCompleteInfo']).toHaveBeenCalled();
      expect(component['setPaginationDataOfHeader']).toHaveBeenCalled();
      expect(
        component['reservationItemViewService'].reduceAndGetCodeOfReservationItem
      ).toHaveBeenCalledWith(reservation.reservationItemViewList[0].code);

      expect(component['reservationItemId']).toEqual(reservation.reservationItemViewList[0].id);
    }));
  });

  describe('on confirmCheckin',  () => {
    beforeEach( () => {
      spyOn<any>(component, 'saveCheckin').and.returnValue(of(null));
      spyOn<any>(component, 'showModalPrintAll').and.callFake( () => {} );
      spyOn<any>(component, 'showGuestListInvalidMessage').and.callFake( () => {} );
      spyOn<any>(component, 'showModalTourismTax');
      component['token'] = '123';
    });


    it( 'should test if call methods when guestReservationItemListcheckedIsValid return true and not couldShow',
      fakeAsync(() => {

        spyOn<any>(component, 'guestReservationItemListcheckedIsValid')
          .and.returnValue(true );

        component['finishCheckin'] = false;
        component.confirmCheckin();
        tick();

        expect(component['finishCheckin']).toBeTruthy();
        expect(component['showModalPrintAll']).toHaveBeenCalled();
        expect(component['saveCheckin']).toHaveBeenCalled();
        expect(component['guestReservationItemListcheckedIsValid']).toHaveBeenCalled();
        expect(component['showModalTourismTax']).not.toHaveBeenCalled();
        expect(component['showGuestListInvalidMessage']).not.toHaveBeenCalled();
    }));

    it( 'should test if call methods when guestReservationItemListcheckedIsValid return true and couldShow', fakeAsync(() => {
      spyOn<any>(component, 'guestReservationItemListcheckedIsValid')
        .and.returnValue(true );
      spyOn(component['tourismTaxService'], 'couldShow').and.returnValue(true);

      component['finishCheckin'] = false;
      component.confirmCheckin();
      tick();

      expect(component['finishCheckin']).toBeTruthy();
      expect(component['showModalPrintAll']).not.toHaveBeenCalled();
      expect(component['saveCheckin']).toHaveBeenCalled();
      expect(component['guestReservationItemListcheckedIsValid']).toHaveBeenCalled();
      expect(component['showModalTourismTax']).toHaveBeenCalled();
      expect(component['showGuestListInvalidMessage']).not.toHaveBeenCalled();
    }));

    it( 'should test if call methods when guestReservationItemListcheckedIsValid return false', () => {
      spyOn<any>(component, 'guestReservationItemListcheckedIsValid')
        .and.returnValue(false );

      component['finishCheckin'] = false;
      component.confirmCheckin();

      expect(component['finishCheckin']).toBeFalsy();
      expect(component['showModalPrintAll']).not.toHaveBeenCalled();
      expect(component['saveCheckin']).not.toHaveBeenCalled();
      expect(component['guestReservationItemListcheckedIsValid']).toHaveBeenCalled();
      expect(component['showGuestListInvalidMessage']).toHaveBeenCalled();
      expect(component['showModalTourismTax']).not.toHaveBeenCalled();
    });
  });

  describe('on redirectFromCheckin',  () => {
    beforeEach( () => {
      component['finishCheckin'] = true;
    });

    it( 'should test if call methods when isWalkin', () => {
      spyOn<any>(component, 'confirmCheckinWhenIsWalkin').and.callFake( () => {} );

      component['isWalkin'] = true;
      component['redirectFromCheckin']();

      expect(component['confirmCheckinWhenIsWalkin']).toHaveBeenCalled();
    });

    it( 'should test if call methods when guestReservationItemListcheckedIsValid return false', () => {
      spyOn<any>(component['toasterEmitService'], 'emitChange');
      spyOn<any>(component['route'], 'navigate').and.callFake(
          (commands ) => new Promise<boolean>(resolve => resolve(true) )
      );

      component['isWalkin'] = false;
      const propertyId = component['propertyId'] = 1236;
      component['redirectFromCheckin']();

      expect(component['toasterEmitService'].emitChange).toHaveBeenCalled();
      expect(component['route'].navigate).toHaveBeenCalledWith(['p', propertyId, 'reservation']);
    });
  });

  it( 'should test saveCheckin', () => {
    const arr = [new GuestReservationItemIdToSend(), new GuestReservationItemIdToSend()];
    const reservationId = 129;
    const propertyId = component['propertyId'] = 456;

    spyOn<any>(component, 'getCheckinIdListFromChildSidebar').and.returnValue( arr );
    spyOn<any>(component, 'getReservationIdFromChildHeader').and.returnValue(reservationId );
    spyOn<any>(component['checkinResource'], 'confirmCheckin');

    component['saveCheckin']();

    expect(component['checkinResource'].confirmCheckin)
      .toHaveBeenCalledWith( arr, reservationId, propertyId );
  });

  describe('on confirmCheckinWhenIsWalkin', () => {
    const reservation1 = new ReservationItemView();
    const reservation2 = new ReservationItemView();

    let reservationArr: ReservationItemView[];

    beforeEach( () => {
      reservationArr = [];
      reservation1.id = 1;
      reservation2.id = 2;

      reservationArr.push(reservation1);
      reservationArr.push(reservation2);
    });

    it('test when reservationItemViewList not empty', () => {
      spyOn<any>(component, 'loadCompleteInfo').and.callFake( () => {} );
      spyOn(
        component['reservationItemViewService'],
        'reduceAndGetCodeOfReservationItem'
      ).and.callThrough();

      component['reservationItemViewList'] = [...reservationArr];
      component['numberOfReservationItemThatAreDoingCheckin'] = 1;
      component['confirmCheckinWhenIsWalkin']();

      expect(component['reservationItemViewList'].length).toEqual(1);
      expect(component['reservationItemId']).toEqual(reservation2.id);
      expect(component['reservationItemCode']).toEqual(reservationItemCode);
      expect(component['loadCompleteInfo']).toHaveBeenCalled();
      expect(component['numberOfReservationItemThatAreDoingCheckin']).toEqual(2);
    });

    it('test when reservationItemViewList empty', () => {
      spyOn(component['toasterEmitService'], 'emitChange');
      spyOn(component['route'], 'navigate').and.callFake(
          (commands ) => new Promise<boolean>(resolve => resolve(true))
      );

      const propertyId = component['propertyId'] = 1000;
      component['reservationItemViewList'] = [];
      component['confirmCheckinWhenIsWalkin']();

      expect(component['toasterEmitService'].emitChange).toHaveBeenCalled();
      expect(component['route'].navigate).toHaveBeenCalledWith(
        ['p', propertyId, 'reservation', 'new'], { queryParams: { iswalkin: 1 } }
      );
    });
  });

  describe('on Click Guest Card', () => {
    let userLanguage;

    beforeEach( () => {
      spyOn<any>(component['checkinResource'], 'getFnrh').and.callThrough();
      spyOn<any>(component, 'changeCard').and.callFake( () => {} );
      spyOn<any>(component['modalToggleService'], 'emitToggleWithRef').and.callFake( f => f );

      userLanguage =
        component['userService'].getUserpreferredLanguage() || navigator.language.toLocaleLowerCase();
    });

    it('should get Fnrh data and set to view with isFirsTime', () => {
      const guestCard = new GuestCard();
      const propertyId = component['propertyId'] = 1;

      guestCard.guest = CheckinGuestReservationItemDtoData;
      component.setFnrhToView(guestCard);

      expect(component['changeCard'])
        .toHaveBeenCalledWith(guestCard);
    });

    it('should get Fnrh not isFirsTime not fnrhIsModified', () => {
      const propertyId = component['propertyId'] = 1;
      const guestCard = new GuestCard();

      guestCard.guest = CheckinGuestReservationItemDtoData;
      guestCard['isFirsTime'] = false;
      component['fnrhIsModified'] = false;
      component.setFnrhToView(guestCard);

      expect(component['changeCard']).toHaveBeenCalledWith(guestCard);
      expect(component['modalToggleService'].emitToggleWithRef).not.toHaveBeenCalled();
    });

    it('should get Fnrh not isFirsTime fnrhIsModified', () => {
      const guestCard = new GuestCard();

      component['propertyId'] = 1;

      guestCard.guest = CheckinGuestReservationItemDtoData;
      guestCard['isFirsTime'] = false;
      component['fnrhIsModified'] = true;
      component.setFnrhToView(guestCard);

      expect(component['changeCard']).not.toHaveBeenCalled();
      expect(component['modalToggleService'].emitToggleWithRef)
        .toHaveBeenCalledWith(component.MODAL_CONFIRM_EDIT_FNRH);
    });

    it('should mapper fnrh', () => {
      const guestCard = new GuestCard();
      spyOn<any>(component, 'prepareFnrh').and.returnValue(FnrhDtoData);

      guestCard.guest = CheckinGuestReservationItemDtoData;
      component['fnrhMapper'](guestCard, FnrhDtoData);

      expect(component.config.fnrh).toEqual(FnrhDtoData);
      expect(component.config.genderList).toEqual([
        <SelectObjectOption>{ key: 'M', value: 'commomData.gender.male' },
        <SelectObjectOption>{ key: 'F', value: 'commomData.gender.female' }
      ]);
      expect(component.config.occupationList).toEqual(component['occupationList']);
      expect(component.config.guestTypeList).toEqual(component['guestTypeList']);
      expect(component.config.nationalityList).toEqual(component['nationalityList']);
      expect(component.config.vehicleList).toEqual(component['vehicleList']);
      expect(component.config.reasonList).toEqual(component['reasonList']);
      expect(component.config.documentTypesAutocomplete).toEqual(component['documentTypesAutocomplete']);
      expect(component.config.guestReservationItemId).toEqual(CheckinGuestReservationItemDtoData.guestReservationItemId);
    });
  });

  it('should test setAddressToApply', () => {
    const address = DATA[ADDRESS];

    component.setAddressToApply(address);


    expect(component['fnrhService']['fnrhAddressToApply']).toEqual(address);
  });

  describe( 'on canConfirmCheckin method', () => {
    it('should uhChosenChangeEmitted$ return true', fakeAsync(() => {
      component['canConfirmCheckin']();
      component['checkinService']['emitChangeUhChosen'].next(true);
      component['checkinService']['emitChangeFnrhList'].next(false);
      tick();

      expect(component.buttonConfirmCheckinDisabled).toBeTruthy();
    }));


    it('should fnrhListChangeEmitted$ return true', fakeAsync(() => {
      component['canConfirmCheckin']();
      component['checkinService']['emitChangeUhChosen'].next(false);
      component['checkinService']['emitChangeFnrhList'].next(true);
      tick();

      expect(component.buttonConfirmCheckinDisabled).toBeTruthy();
    }));

    it('should fnrhListChangeEmitted$ and uhChosenChangeEmitted$ return true', fakeAsync(() => {
      component['canConfirmCheckin']();
      component['checkinService']['emitChangeUhChosen'].next(true);
      component['checkinService']['emitChangeFnrhList'].next(true);
      tick();

      expect(component.buttonConfirmCheckinDisabled).toBeFalsy();
    }));
  });

  it('should subscribeFnrhToUpdateHeaderAndSidebar', fakeAsync(() => {
    const reservationItemId = 666;

    spyOn<any>(component, 'getReservationItems').and.callFake( f => f );

    component['reservationItemId'] = reservationItemId;
    component['subscribeFnrhToUpdateHeaderAndSidebar']();
    component['checkinService']['emitChangeSource'].next(null);
    tick();

    expect(component['getReservationItems']).toHaveBeenCalledWith(reservationItemId);
  }));

  describe( 'on printAllFnrh method', () => {
    beforeEach( () => {
      spyOn<any>(component['modalToggleService'], 'emitToggleWithRef');
    });

    it('should guestReservationCheckedForCheckinList whith length 0', () => {
      spyOn<any>(component['toasterEmitService'], 'emitChange');
      component.guestReservationCheckedForCheckinList = [];

      component['printAllFnrh']();

      expect(component['modalToggleService'].emitToggleWithRef).toHaveBeenCalledWith(
        component['modalToggleService']
      );
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.error,
        'alert.noneFnrhSelected'
      );
    });

    it('should print all components', fakeAsync(() => {
      const propertyId = component['propertyId'] = 11;


      spyOn<any>(component['printService'], 'printComponent').and.callFake( f => f );

      component.guestReservationCheckedForCheckinList = DATA[GUEST_RESERVATION];
      component.guestReservationCheckedForCheckinList[0].guestRegistrationId = guestData.items[0].id;
      component.guestReservationCheckedForCheckinList[1].guestRegistrationId = guestData.items[1].id;

      guestData.items[0].propertyId = propertyId;
      guestData.items[1].propertyId = propertyId;

      component['printAllFnrh']();
      tick();

      expect(component.fnrhList.length).toEqual(2);
      expect(component.fnrhList).toEqual( guestData.items );
      expect(component['modalToggleService'].emitToggleWithRef).toHaveBeenCalledWith(
        component['modalToggleService']
      );
      expect(component['printService'].printComponent).toHaveBeenCalledWith(
        PrintAllGuestRegistrationComponent,
        component
      );
    }));
  });

  describe( 'on confirmNotToSaveFnrh method', () => {
    beforeEach( () => {
      spyOn<any>(component['modalToggleService'], 'emitToggleWithRef').and.callFake( f => f );
    });
    it('should test with isRedirectToBudgetPage', fakeAsync(() => {
      spyOn<any>( component, 'redirectToBdgetPage').and.callFake( () => {} );

      component['isRedirectToBudgetPage'] = true;
      component.confirmNotToSaveFnrh();

      expect( component['redirectToBdgetPage']).toHaveBeenCalled();
      expect( component['modalToggleService'].emitToggleWithRef )
        .toHaveBeenCalledWith(component.MODAL_CONFIRM_EDIT_FNRH);
    }));

    it('should test without isRedirectToBudgetPage', fakeAsync(() => {
      spyOn<any>( component, 'changeCard').and.callFake( f => f );

      const guestCard = new GuestCard();
      guestCard.active = true;
      guestCard.isReadonly = true;

      component.guestCardCurrent = guestCard;
      component['isRedirectToBudgetPage'] = false;
      component.confirmNotToSaveFnrh();

      expect( component['changeCard']).toHaveBeenCalledWith(guestCard);
      expect( component['modalToggleService'].emitToggleWithRef )
        .toHaveBeenCalledWith(component.MODAL_CONFIRM_EDIT_FNRH);
    }));
  });

  it( 'should test changeFnrh', fakeAsync(() => {
    const guestCard = new GuestCard();
    const guestReservationItemId = 456;
    const propertyId = component['propertyId'] = 12;
    const language = 'pt-br';
    const lastFnrh = new FnrhDto();

    spyOn<any>(component['fnrhComponent'], 'resetFnrh').and.callFake( () => {} );
    spyOn(component['userService'], 'getUserpreferredLanguage')
      .and.returnValue( language );
    spyOn<any>(component['checkinResource'], 'getFnrh').and.callThrough();
    spyOn<any>(component, 'prepareFnrh').and.returnValue( lastFnrh );
    spyOn<any>(component, 'getResponsables').and.callFake( () => {} );
    spyOn<any>(component['loadPageEmitService'], 'emitChange').and.callFake( f => f );


    guestCard.active = true;
    guestCard.isReadonly = true;
    guestCard.guest = new CheckinGuestReservationItemDto();
    guestCard.guest.guestReservationItemId = guestReservationItemId;

    component['changeFnrh'](guestCard);
    tick();

    expect(component['fnrhComponent'].resetFnrh).toHaveBeenCalled();
    expect(component['checkinResource'].getFnrh).toHaveBeenCalledWith(
        guestReservationItemId,
        propertyId,
      language
    );
    expect(component['prepareFnrh']).toHaveBeenCalledWith(response);
    expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
    expect(component['getResponsables']).toHaveBeenCalled();
  }));

  it( 'should test subscribeFnrhToNextGuestCard', fakeAsync(() => {
    const guestReservationItemId = 456;

    spyOn<any>(component, 'nextGuestCardToCheckin').and.callFake( f => f );

    component['subscribeFnrhToNextGuestCard']();
    component['checkinService']['emitChangeSource'].next(guestReservationItemId);
    tick();

    expect(component['nextGuestCardToCheckin']).toHaveBeenCalledWith(guestReservationItemId);
  }));

  it('should getReservationIdFromChildHeader', async () => {
    const id = 10;
    const childHeader  = component['childHeader'] = {};
    childHeader['checkInHeaderDto'] = { id };
    expect(component['getReservationIdFromChildHeader']()).toEqual( id );
  });

  it('should getCheckinIdListFromChildSidebar', async () => {
      const childSidebar: GuestFnrhSidebarComponent = fixture.componentInstance.childSidebar;
      childSidebar.guestReservationCheckedForCheckinList = CheckinGuestReservationItemDtoDataList;
      expect(component['getCheckinIdListFromChildSidebar']())
        .toEqual([{ guestReservationItemId: 1 }, { guestReservationItemId: 2 }]);
    });

  it('should setGuestReservationItemListForCheckin', () => {
    const checkinGuestReservationItemList = new Array<CheckinGuestReservationItemDto>();
    checkinGuestReservationItemList.push(new CheckinGuestReservationItemDto());

    component.setGuestReservationItemListForCheckin(checkinGuestReservationItemList);

    expect(component.guestReservationCheckedForCheckinList).toEqual(checkinGuestReservationItemList);
  });

  describe( 'on nextGuestCardToCheckin method', () => {
    beforeEach( () => {
      spyOn<any>(component, 'changeCard').and.callFake( f => f );
    });
    it('should test without nextGuestToCheckin', () => {
      const guestReservationItemId = 1323;
      let guestCards;

      component.guestReservationCheckedForCheckinList = new Array<CheckinGuestReservationItemDto>();
      guestCards = component.childSidebar.guestCards = DATACHECKIN[GUEST_CARD_LIST];
      guestCards[0]['guest'] = { guestReservationItemId: 3456789 };
      guestCards[1]['guest'] = { guestReservationItemId };

      component.nextGuestCardToCheckin(guestReservationItemId);

      expect(component['changeCard']).toHaveBeenCalledWith(guestCards[1]);
    });

    it('should test with nextGuestToCheckin', () => {
      const guestReservationItemId = 1323;
      const diffGuestReservationItemId = 1325;
      let guestCards, guestReservationCheckedForCheckinList: CheckinGuestReservationItemDto[];

      guestReservationCheckedForCheckinList
        = component.guestReservationCheckedForCheckinList = DATA[GUEST_RESERVATION];
      guestReservationCheckedForCheckinList[0].isValid = false;
      guestReservationCheckedForCheckinList[0].guestReservationItemId = diffGuestReservationItemId;

      guestReservationCheckedForCheckinList[1].isValid = true;
      guestReservationCheckedForCheckinList[1].guestReservationItemId = guestReservationItemId;

      guestCards = component.childSidebar.guestCards = DATACHECKIN[GUEST_CARD_LIST];

      guestCards[0]['guest'] = { guestReservationItemId: diffGuestReservationItemId };
      guestCards[1]['guest'] = { guestReservationItemId };

      component.nextGuestCardToCheckin(guestReservationItemId);

      expect(component['changeCard']).toHaveBeenCalledWith(guestCards[0]);
    });
  });

  describe( 'on getIncapable method', () => {
    it('should test if incapable and isLegallyIncompetent', () => {
      const fnrh = {
        incapable: true,
        isLegallyIncompetent: true
      };

      expect(component['getIncapable'](fnrh)).toBeTruthy();
    });

    it('should test if incapable and not isLegallyIncompetent', () => {
      const fnrh = {
        incapable: true,
        isLegallyIncompetent: false
      };

      expect(component['getIncapable'](fnrh)).toBeTruthy();
    });

    it('should test if not incapable and isLegallyIncompetent', () => {
      const fnrh = {
        incapable: false,
        isLegallyIncompetent: true
      };

      expect(component['getIncapable'](fnrh)).toBeTruthy();
    });

    it('should test if not incapable and not isLegallyIncompetent', () => {
      const fnrh = {
        incapable: false,
        isLegallyIncompetent: false
      };

      expect(component['getIncapable'](fnrh)).toBeFalsy();
    });
  });

  describe( 'on getReponsable method', () => {
    it('should test if responsibleGuestRegistrationId and responsable', () => {
      const fnrh = {
        responsibleGuestRegistrationId: 123,
        responsable: true
      };

      expect(component['getReponsable'](fnrh)).toBeTruthy();
    });

    it('should test if responsibleGuestRegistrationId and not responsable', () => {
      const fnrh = {
        responsibleGuestRegistrationId: 123,
        responsable: false
      };

      expect(component['getReponsable'](fnrh)).toBeTruthy();
    });

    it('should test if not responsibleGuestRegistrationId and responsable', () => {
      const fnrh = {
        responsibleGuestRegistrationId: null,
        responsable: true
      };

      expect(component['getReponsable'](fnrh)).toBeTruthy();
    });

    it('should test if not responsibleGuestRegistrationId and not responsable', () => {
      const fnrh = {
        responsibleGuestRegistrationId: null,
        responsable: false
      };

      expect(component['getReponsable'](fnrh)).toBeFalsy();
    });
  });

  describe( 'on goToReservationBudget method', () => {
    it('should test if fnrhIsModified', () => {
      spyOn(component['modalToggleService'], 'emitToggleWithRef');

      component['fnrhIsModified'] = true;
      component['isRedirectToBudgetPage'] = false;
      component['goToReservationBudget']();

      expect(component['isRedirectToBudgetPage']).toBeTruthy();
      expect(component['modalToggleService'].emitToggleWithRef)
        .toHaveBeenCalledWith(component.MODAL_CONFIRM_EDIT_FNRH);
    });

    it('should test if not fnrhIsModified', () => {
      spyOn<any>(component, 'redirectToBdgetPage').and.callFake( () => {} );
      component['fnrhIsModified'] = false;
      component['isRedirectToBudgetPage'] = false;

      component['goToReservationBudget']();

      expect(component['isRedirectToBudgetPage']).toBeTruthy();
      expect(component['redirectToBdgetPage']).toHaveBeenCalled();
    });
  });

  describe( 'on confirmTourismTax', () => {
    const arr = [ new GuestReservationItemIdToSend(), new GuestReservationItemIdToSend() ];
    beforeEach(() => {
      spyOn<any>(component, 'getCheckinIdListFromChildSidebar').and.returnValue( arr );
      spyOn<any>(component, 'showModalPrintAll');
      spyOn<any>(component['modalToggleService'], 'emitToggleWithRef');
    });

    it('on error', fakeAsync(() => {
      spyOn<any>(component['tourismTaxResource'], 'launchTourismTax').and.returnValue(throwError(null));

      component.confirmTourismTax();
      tick();

      expect(component['showModalPrintAll']).toHaveBeenCalled();
      expect(component['tourismTaxResource'].launchTourismTax).toHaveBeenCalledWith(arr);
    }));

    it('on success', fakeAsync(() => {
      spyOn<any>(component['tourismTaxResource'], 'launchTourismTax').and.callThrough();

      component.confirmTourismTax();
      tick();

      expect(component['showModalPrintAll']).toHaveBeenCalled();
      expect(component['tourismTaxResource'].launchTourismTax).toHaveBeenCalledWith(arr);
    }));
  });
});
