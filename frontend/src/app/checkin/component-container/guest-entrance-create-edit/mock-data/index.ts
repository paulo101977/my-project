import { forkJoin, of } from 'rxjs';
import { ReservationDto } from 'app/shared/models/dto/reservation-dto';
import { FnrhSaveDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { CheckInGetRoomTypesDto } from 'app/shared/models/dto/checkin/checkin-get-room-types-dto';
import { BedTypeEnum } from 'app/shared/models/bed-type';
import { CheckinReservationItemBudgetDto } from 'app/shared/models/dto/checkin/checkin-reservation-item-budget-dto';

export const RESULT = 'RESULT';
export const DOCUMENT_TYPE_ARR = 'DOCUMENT_TYPE_ARR';
export const GUEST_TYPE_ARR = 'GUEST_TYPE_ARR';
export const TRANSPORTATION_ARR = 'TRANSPORTATION_ARR';
export const NATIONALITY_ARR = 'NATIONALITY_ARR';
export const REASON_ARR = 'REASON_ARR';
export const OCCUPATION_ARR = 'OCCUPATION_ARR';
export const HEADER_BUDGET = 'HEADER_BUDGET';
export const RESERVATION_ITEM_LIST = 'RESERVATION_ITEM_LIST';
export const RESPONSABLE_ARR = 'RESPONSABLE_ARR';
export const ROOM_TYPE_ARR = 'ROOM_TYPE_ARR';
export const RESERVATION_RESULT = 'RESERVATION_RESULT';
export const FNRH_ENTRANCE_RESULT = 'FNRH_ENTRANCE_RESULT';
export const GET_FORM_METHOD_RESULT = 'GET_FORM_METHOD_RESULT';
export const RESERVATION_ITEM_LIST_DTO = 'RESERVATION_ITEM_LIST_DTO';

export class DATA {
  public static readonly DOCUMENT_TYPE_ARR = [
    {
      countrySubdivisionId: 1,
      id: 1,
      isMandatory: true,
      name: 'CPF',
      personType: 'N',
    }
  ];

  public static readonly GUEST_TYPE_ARR = [
    {
      code: '0001',
      guestTypeName: 'NORMAL',
      id: 1,
      isActive: true,
      isIncognito: false,
      isVip: false,
      propertyId: 1,
    }
  ];

  public static readonly TRANSPORTATION_ARR = [
    {
      id: 1,
      transportationTypeName: 'Avião',
    },
  ];

  public static readonly NATIONALITY_ARR = [
    {
      countrySubdivisionId: 1,
      id: 1,
      languageIsoCode: 'pt-BR',
      name: 'Brasil',
      nationality: 'Brasileira',
    }
  ];

  public static readonly REASON_ARR = [
    {
      chainId: 1,
      id: 62,
      isActive: true,
      propertyId: 0,
      reasonCategoryId: 7,
      reasonName: 'AAA',
    }
  ];

  public static readonly OCCUPATION_ARR = [
    {
      id: 1,
      occupation: 'Contador',
    }
  ];

  public static readonly HEADER_BUDGET = <CheckinReservationItemBudgetDto>{
    id: 0,
    reservationItemId: 3692,
    reservationItemCode: '74JHK1V55-0001',
    mealPlanTypeId: 2,
    currencyId: null,
    reservationBudgetCurrencySymbol: 'R$',
    isSeparated: false,
    ratePlanId: null,
    gratuityTypeId: null,
    commercialBudgetList: [
      {
        id: 0,
        ratePlanId: null,
        companyClientId: null,
        currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
        roomTypeId: 63,
        mealPlanTypeId: 2,
        roomTypeAbbreviation: 'BGR',
        mealPlanTypeName: 'Café da Manha',
        commercialBudgetName: 'Tarifa base',
        currencySymbol: 'R$',
        rateVariation: 0.0,
        totalBudget: 24549.0000,
        mealPlanTypeDefaultId: 2,
        mealPlanTypeDefaultName: 'Café da Manha',
        commercialBudgetDayList: [
          {
            id: 0,
            day: new Date('2019-01-09T00:00:00'),
            baseRateAmount: 150.0000,
            childRateAmount: 30.0000,
            currencySymbol: 'R$',
            currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
            mealPlanTypeId: 2,
            baseRateWithRatePlanAmount: null,
            ratePlanId: null,
          },
          {
            id: 0,
            day: new Date('2019-01-10T00:00:00'),
            baseRateAmount: 150.0000,
            childRateAmount: 30.0000,
            currencySymbol: 'R$',
            currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
            mealPlanTypeId: 2,
            baseRateWithRatePlanAmount: null,
            ratePlanId: null,
          },
        ],
      }
    ],
    reservationBudgetDtoList: [
      {
        id: 17687,
        reservationBudgetUid: '00000000-0000-0000-0000-000000000000',
        reservationItemId: 0,
        budgetDay: new Date('2019-01-09T00:00:00'),
        baseRate: 100.0000,
        childRate: 0.0000,
        mealPlanTypeId: 2,
        rateVariation: 0.0000,
        agreementRate: 150.0000,
        manualRate: 282.0000,
        discount: 0.0000,
        currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
        currencySymbol: 'R$',
        mealPlanTypeBaseRate: 50.0000,
        childMealPlanTypeRate: 0.0000,
        childAgreementRate: 0.0000,
        mealPlanTypeAgreementRate: 0.0000,
        childMealPlanTypeAgreementRate: 0.0000,
        propertyBaseRateHeaderHistoryId: '237ac6ef-6478-4212-a31a-dcd9a419afa4',
        previousAgreementRate: null,
        previousManualRate: null,
        separatedRate: null,
        commercialBudgetName: null,
        baseRateChanged: null,
        originalManualRate: null,
        overbooking: null,
      },
      {
        id: 17688,
        reservationBudgetUid: '00000000-0000-0000-0000-000000000000',
        reservationItemId: 0,
        budgetDay: new Date('2019-01-10T00:00:00'),
        baseRate: 100.0000,
        childRate: 0.0000,
        mealPlanTypeId: 2,
        rateVariation: 0.0000,
        agreementRate: 150.0000,
        manualRate: 282.0000,
        discount: 0.0000,
        currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
        currencySymbol: 'R$',
        mealPlanTypeBaseRate: 50.0000,
        childMealPlanTypeRate: 0.0000,
        childAgreementRate: 0.0000,
        mealPlanTypeAgreementRate: 0.0000,
        childMealPlanTypeAgreementRate: 0.0000,
        propertyBaseRateHeaderHistoryId: '237ac6ef-6478-4212-a31a-dcd9a419afa4',
        previousAgreementRate: null,
        previousManualRate: null,
        separatedRate: null,
        commercialBudgetName: null,
        baseRateChanged: null,
        originalManualRate: null,
        overbooking: null,
      },
    ],
  };

  public static readonly RESERVATION_ITEM_LIST = [
    {
      businessSourceName: 'Nova origem',
      country: 'Brasil',
      division: 'RJ',
      documentInformation: '259.251.124-58',
      documentTypeName: 'CPF',
      guestRegistrationDocumentBirthDate: '1990-10-10T00:00:00',
      guestRegistrationId: '2f016f4f-b32d-42de-2ab3-08d6119f40e6',
      guestReservationItemId: 204,
      guestReservationItemName: 'Teste Grupo 2',
      guestStatusId: 2,
      id: 124,
      isChild: false,
      isLegallyIncompetent: false,
      isMain: true,
      isValid: true,
      personId: '96ee1826-1a71-4d85-a4b4-4581b9242289',
      reservationItemCode: 'VRN9QFCQN-0002',
      reservationItemId: 158,
      reservationItemStatusId: 2,
      subdivision: 'Rio de Janeiro',
    }
  ];

  public static readonly RESPONSABLE_ARR = [
    {
      age: 124,
      arrivingFromText: 'Rio de Janeiro',
      birthDate: '1894-06-08T00:00:00',
      checkinDate: '2018-07-30T18:46:08.927',
      documentTypeId: 0,
      email: 'william@email.com',
      firstName: 'William',
      fullName: 'William',
      gender: 'M',
      guestId: 19,
      guestReservationItemId: 45,
      guestTypeId: 1,
      id: '825a1564-699f-4b9e-bce9-08d5f6486835',
      isChild: false,
      isLegallyIncompetent: true,
      isMain: true,
      lastName: 'William',
      nationality: 1,
      nextDestinationText: 'Rio de Janeiro',
      person: {
        companyClientContactPersonDtoList: [],
        companyContactPersonList: [],
        contactInformationList: [],
        documentList: [],
        employeeList: [],
        guestList: [],
        id: '00000000-0000-0000-0000-000000000000',
        locationList: [],
        personInformationList: [],
        propertyContactPersonList: [],
      },
      phoneNumber: '+55 21 9976-6508',
      propertyId: 1,
      socialName: 'William',
      tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    }
  ];

  public static readonly ROOM_TYPE_ARR: CheckInGetRoomTypesDto = {
    items: [
      {
        name: '',
        abbreviation: '',
        id: 123,
        overbooking: false,
        adultCapacity: 10,
        childCapacity: 2,
        roomTypeBedTypeList: [
          {
            bedType: BedTypeEnum.Double,
            count: 2,
            optionalLimit: 1,
          }
        ],
      }
    ],
    hasNext: false,
    total: 0
  };

  public static readonly RESULT = forkJoin([
    of({
      items: DATA.DOCUMENT_TYPE_ARR
    }),
    of({
      items: DATA.GUEST_TYPE_ARR,
    }),
    of({
      items: DATA.TRANSPORTATION_ARR
    }),
    of({
      items: DATA.NATIONALITY_ARR
    }),
    of({
      items: DATA.REASON_ARR
    }),
    of({
      items: DATA.OCCUPATION_ARR,
    }),
    of(DATA.HEADER_BUDGET),
    of({
      items: DATA.RESERVATION_ITEM_LIST,
    }),
    of({
      items: DATA.RESPONSABLE_ARR
    }),
    of({
      items: DATA.ROOM_TYPE_ARR.items,
    }),
  ]);

  public static readonly RESERVATION_RESULT = <ReservationDto>{
    partnerComments: 'blalblaal',
    companyClientId: '2345',
    companyClientContactPersonId: '5678',
    companyClient: null,
    reservationConfirmation: null,
    walkin: true,
    contactId: 123,
    'id': 3491,
    'reservationUid': 'e0611efb-6795-4e4b-b131-02875367731c',
    'reservationCode': '74JHK1V55',
    'reservationVendorCode': '001',
    'propertyId': 1,
    'contactName': 'Paulo Teste 123',
    'contactEmail': 'pauloteste123@teste.com',
    'contactPhone': '',
    'internalComments': '',
    'externalComments': '',
    'groupName': '',
    'unifyAccounts': false,
    'businessSourceId': 4,
    'marketSegmentId': 33,
    'walkIn': false,
    'launchDaily': true,
    'reservationItemList': [
      {
        'id': 3692,
        'reservationItemUid': 'ffcac2c7-52e3-4d14-ab91-8e0de4bc69b4',
        'reservationId': 3491,
        'estimatedArrivalDate': '2019-03-20T14:00:00',
        'checkInDate': '2019-01-08T21:00:00',
        'estimatedDepartureDate': '2019-03-21T12:00:00',
        'reservationItemCode': '74JHK1V55-0001',
        'requestedRoomTypeId': 63,
        'receivedRoomTypeId': 63,
        'roomId': 502,
        'adultCount': 2,
        'childCount': 3,
        'reservationItemStatusId': 2,
        'roomLayoutId': '56576890-480a-4763-a8d1-151fbf3c1689',
        'extraBedCount': 0,
        'extraCribCount': 0,
        'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
        'mealPlanTypeId': 2,
        'isMigrated': false,
        'commercialBudgetName': 'Tarifa base',
        'guestReservationItemList': [],
        'reservationBudgetList': [],
        'reservationItemStatusFormatted': 'Check-In',
        checkOutDate: '23/10/2019',
        requestedRoomType: null,
        receivedRoomType: null,
        roomLayout: null,
        gratuityTypeId: 123,
        room: null,
        currencySymbol: 'R$',
        ratePlanId: '123',
        externalReservationNumber: '4567',
        partnerReservationNumber: '7890',
      }
    ],
  };

  public static readonly FNRH_ENTRANCE_RESULT = <FnrhSaveDto> {
    responsableName: 'Paulo',
    responsibleGuestRegistrationId: '3rrw',
    healthInsurance: 'rrrr',
    socialName: 'bllbal',
    purposeOfTrip: 2,
    occupationId: 234,
    additionalInformation: 'Blabala blala',
    age: 41,
    arrivingBy: 1,
    phoneNumber: '21996397564',
    mobilePhoneNumber: '21996397564',
    'isChild': false,
    'id': '3faf564d-f077-41a1-0f00-08d6b2c8bbb7',
    'firstName': 'Paulo',
    'lastName': '22 Doidao',
    'fullName': 'Paulo 22 Doidao',
    'email': 'paulo22@doidao.com',
    'birthDate': new Date('1942-02-21T00:00:00'),
    'gender': 'M',
    'guestTypeId': 1,
    'nationality': 1,
    'arrivingFromText': 'Rio de Janeiro',
    'nextDestinationText': 'Rio de Janeiro',
    'propertyId': 1,
    'tenantId': '00000000-0000-0000-0000-000000000000',
    'personId': '607975df-0ef9-47a7-82e7-6b81ce711b2b',
    'guestId': 272,
    'isLegallyIncompetent': false,
    'documentTypeId': 1,
    'documentInformation': '854.479.070-47',
    'document': {
      'id': 0,
      'ownerId': '607975df-0ef9-47a7-82e7-6b81ce711b2b',
      'documentTypeId': 1,
      'documentInformation': '854.479.070-47',
      documentTypeName: 'blabla'
    },
    'location': {
      'id': 0,
      'ownerId': '00000000-0000-0000-0000-000000000000',
      'browserLanguage': 'pt-BR',
      'locationCategoryId': 4,
      'latitude': -22.897355,
      'longitude': -43.180278,
      'streetName': 'Avenida Rio Branco',
      'streetNumber': '1',
      'additionalAddressDetails': '',
      'neighborhood': 'Centro',
      'cityId': 3269,
      'postalCode': 20040001,
      'countryCode': 'BR',
      'stateId': 20,
      'countryId': 1,
      'completeAddress': 'Avenida Rio Branco, 1 -  - Centro',
      'subdivision': 'Rio de Janeiro',
      'division': 'RJ',
      'country': 'Brasil',
      city: 'Rio de Janeiro'
    },
    'person': {
      'id': '607975df-0ef9-47a7-82e7-6b81ce711b2b',
      'personType': 'N',
      'firstName': 'Paulo',
      'lastName': 'Doidao',
      'fullName': 'Paulo 22 Doidao',
      'countrySubdivisionId': 1,
      'documentList': [],
      'locationList': [],
      'companyContactPersonList': [],
      'contactInformationList': [],
      'employeeList': [],
      'guestList': [],
      'personInformationList': [],
      'propertyContactPersonList': [],
      'companyClientContactPersonDtoList': [],
      receiveOffers: true,
      sharePersonData: true,
      allowContact: true,
    },
    'guestReservationItemId': 3934,
    'guestRegistrationDocumentList': []
  };

  public static readonly GET_FORM_METHOD_RESULT = [{
    guestReservationItemId: 3934,
    personId: '607975df-0ef9-47a7-82e7-6b81ce711b2b',
    guestRegistrationId: '3faf564d-f077-41a1-0f00-08d6b2c8bbb7',
    isChild: false,
    guestId: 272,
    guestName: 'Paulo 22 Doidao',
    guestEmail: 'paulo22@doidao.com',
    documentInformation: '854.479.070-47',
    documentTypeId: 1,
    guestDocumentTypeId: 1,
    guestDocument: '854.479.070-47',
    guestTypeId: 1,
    preferredName: 'Paulo 22 Doidao',
    guestCitizenship: 'Brasil',
    guestLanguage: 'pt-BR',
    propertyId: 1,
    childAge: undefined,
    currentBudget: 13140
  }];

  public static readonly RESERVATION_ITEM_LIST_DTO = <Array<CheckinGuestReservationItemDto>> [
    {
      socialName: '',
      childAge: undefined,
      documentTypeId: 1,
      guestId: 12,
      guestName: 'Paulo',
      guestEmail: 'paulo@p.com',
      guestDocumentTypeId: 2,
      guestDocument: '',
      estimatedArrivalDate: new Date('1942-02-21T00:00:00'),
      checkInDate: new Date('2019-02-21T00:00:00'),
      estimatedDepartureDate: new Date('2019-03-21T00:00:00'),
      checkOutDate:  new Date('2019-04-21T00:00:00'),
      guestTypeId: 678,
      preferredName: 'Paulo',
      guestCitizenship: 'Rio',
      guestLanguage: 'br',
      propertyId: 11,
      currentBudget: 1,
      businessSourceName: 'Nova origem',
      country: 'Brasil',
      division: 'RJ',
      documentInformation: '259.251.124-58',
      documentTypeName: 'CPF',
      guestRegistrationDocumentBirthDate: new Date('1990-10-10T00:00:00'),
      guestRegistrationId: '2f016f4f-b32d-42de-2ab3-08d6119f40e6',
      guestReservationItemId: 204,
      guestReservationItemName: 'Teste Grupo 2',
      guestStatusId: 2,
      id: 124,
      isChild: false,
      isMain: true,
      isValid: true,
      personId: '96ee1826-1a71-4d85-a4b4-4581b9242289',
      reservationItemCode: 'VRN9QFCQN-0002',
      reservationItemId: 158,
      reservationItemStatusId: 2,
      subdivision: 'Rio de Janeiro',
    }
  ];
}

