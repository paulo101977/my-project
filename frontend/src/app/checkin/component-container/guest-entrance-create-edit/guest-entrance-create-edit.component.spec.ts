import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { ReasonDto } from 'app/shared/models/dto/checkin/reason-dto';
import { NationalityDto } from 'app/shared/models/dto/checkin/nationality-dto';
import { PropertyGuestTypeDto } from 'app/shared/models/dto/property-guest-type-dto';
import { TransportationTypeDto } from 'app/shared/models/dto/checkin/transportation-type-dto';
import { NationalityDtoData } from 'app/shared/mock-data/nationality-data';
import { TransportationTypeDtoData } from 'app/shared/mock-data/transportation-type-dto';
import { PropertyGuestTypeDtoData } from 'app/shared/mock-data/guest-type-data';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { ReasonDtoData } from 'app/shared/mock-data/reason-travel-dto';
import { Reservation } from 'app/shared/models/reserves/reservation';
import { OccupationDto } from 'app/shared/models/dto/checkin/occupation-dto';
import { OccupationDtoData } from 'app/shared/mock-data/occupation-dto-data';
import {
  GuestEntranceCreateEditComponent
} from 'app/checkin/component-container/guest-entrance-create-edit/guest-entrance-create-edit.component';
import { of } from 'rxjs';
import {
  DATA,
  HEADER_BUDGET,
  FNRH_ENTRANCE_RESULT,
  GET_FORM_METHOD_RESULT,
  RESERVATION_RESULT,
  RESULT,
  ROOM_TYPE_ARR
} from './mock-data';
import { FnrhSaveDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { configureTestSuite } from 'ng-bullet';

import {
  ActivatedRouteStub,
  assetsServiceStub,
  commomServiceStub,
  createServiceStub,
  locationStub,
  modalToggleServiceStub, routerStub,
  toasterEmitServiceStub
} from '../../../../../testing';

import { ActivatedRoute } from '@angular/router';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import { ReservationItemViewService } from 'app/shared/services/reservation-item-view/reservation-item-view.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { GuestEntranceResource } from 'app/shared/resources/guest-entrance/guest-entrance.resource';
import {
  ReservationItemCommercialBudgetSearch
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-search';

const guestTypes: ItemsResponse<PropertyGuestTypeDto> = {
  items: [PropertyGuestTypeDtoData],
  hasNext: false,
};
const nationalities: ItemsResponse<NationalityDto> = {
  items: [NationalityDtoData],
  hasNext: false,
};
const vehicles: ItemsResponse<TransportationTypeDto> = {
  items: [TransportationTypeDtoData],
  hasNext: false,
};
const reasons: ItemsResponse<ReasonDto> = {
  items: [ReasonDtoData],
  hasNext: false,
};
const occupations: ItemsResponse<OccupationDto> = {
  items: [OccupationDtoData],
  hasNext: false,
};
const reservationItemListDtoResponse: ItemsResponse<CheckinGuestReservationItemDto> = {
  items: [],
  hasNext: false,
};
const data$ = DATA[RESULT];
const response = new FnrhDto();
response.id = '123678';
const reservation = new Reservation();
reservation.normalizeResponse(DATA[RESERVATION_RESULT]);
const reservationItemCode = '123';
const childrenAge = 12;

// TODO: move to stub folder: [DEBIT: 7506]
const checkinResourceStub = createServiceStub(CheckinResource, {
  getGuestTypesByPropertyId: (propertyId: number) => of( guestTypes ),
  getNationalities: (languageIsoCode: string) => of( nationalities ),
  getTransportationType: () => of( vehicles ),
  getReasonTravel: (propertyId: number, reasonCategoryId: number) => of( reasons ),
  getReservationGuestList: (propertyId: number, reservationItemId: number) => of( reservationItemListDtoResponse ),
  getOccupations: () => of( occupations ),
  getCompleteAuxInfo: (propertyId, reservationItemId, reservationId, language) => of( data$ ),
  getRoomTypes: (propertyId: number, data: CheckInHeaderDto) =>  of(DATA[ROOM_TYPE_ARR])
});

const guestEntranceResourceStub = createServiceStub(GuestEntranceResource, {
  confirmEntrance: (guestReservation: Array<any>) => of({}),
  createFnrh: (fnrh: FnrhSaveDto) => of(DATA[FNRH_ENTRANCE_RESULT]),
  getGuestEntrance: (reservationItemCommercialBudgetSearch: ReservationItemCommercialBudgetSearch) => of(DATA[HEADER_BUDGET]),
  updateFnrhEntrance: (fnrh: FnrhSaveDto) => of(DATA[FNRH_ENTRANCE_RESULT]),
});

const reservationItemViewServiceStub = createServiceStub(ReservationItemViewService, {
  reduceAndGetCodeOfReservationItem: (code: string) => reservationItemCode
});

const dateServiceStub = createServiceStub(DateService, {
  calculateAgeByDate: (date: Date) => childrenAge
});


describe('GuestEntranceCreateEditComponent', () => {
  let component: GuestEntranceCreateEditComponent;
  let fixture: ComponentFixture<GuestEntranceCreateEditComponent>;

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        imports: [
          RouterTestingModule,
          HttpClientTestingModule,
          TranslateTestingModule,
        ],
        declarations: [
          GuestEntranceCreateEditComponent,
        ],
        providers: [
          locationStub,
          modalToggleServiceStub,
          assetsServiceStub,
          checkinResourceStub,
          commomServiceStub,
          toasterEmitServiceStub,
          reservationItemViewServiceStub,
          dateServiceStub,
          { provide: ActivatedRoute, useClass: ActivatedRouteStub },
          guestEntranceResourceStub,
          routerStub,
        ],
        schemas: [
          NO_ERRORS_SCHEMA
        ]
      });

    fixture = TestBed.createComponent(GuestEntranceCreateEditComponent);
    component = fixture.componentInstance;

    component.checkInHeaderDto = new CheckInHeaderDto();
    component.checkInHeaderDto.id = 1;
    component.checkInHeaderDto.roomNumber = '602';

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create with params reservationItemId', fakeAsync(() => {
      const reservationItemId = component._route.snapshot.params.reservationItemId = 1;
      component._route.snapshot.params.reservationItemCode = reservationItemCode;
      spyOn<any>(
        component['reservationItemViewService'],
        'reduceAndGetCodeOfReservationItem'
      ).and.callThrough();
      spyOn<any>(component, 'loadCompleteInfo');

      component.ngOnInit();

      tick();

      expect(component).toBeTruthy();
      expect(component.reservationItemCode).toEqual(reservationItemCode);
      expect(component['reservationItemId']).toEqual(reservationItemId);
      expect(component['loadCompleteInfo']).toHaveBeenCalled();
    }));

    it('should hideAllPopovers', () => {
      spyOn<any>(component['showHideContentEmitService'], 'emitChange');

      component.hideAllPopovers();

      expect(component['showHideContentEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
    });
  });

  describe('on reservationItemId', () => {
    let reservationItemId;
    let reservationId;

    beforeEach( () => {
      reservationId = 6;
      reservationItemId = component['reservationItemId'] = 123;
      spyOn<any>(component['checkinResource'], 'getCompleteAuxInfo').and.returnValue( data$ );
      spyOn<any>(component, 'loadCompleteInfo').and.callThrough();
      spyOn<any>(component, 'configSelect').and.callFake( f => f );
      spyOn<any>(component['checkinResource'], 'getRoomTypes').and.returnValue( of(DATA[ROOM_TYPE_ARR]) );
    });

  });

  describe('on goToReservationBudget',  () => {
    const reservationId = 123;
    const propertyId = 12;
    const params = [
      'p',
      propertyId,
      'rm',
      'reservation-budget',
      reservationId
    ];

    beforeEach( () => {
      spyOn<any>(component['router'], 'navigate').and.callFake( f => f );
    });

    it( 'should goToReservationBudget', () => {
      params[4] = component.reservationId = 2345;
      component['propertyId'] = propertyId;
      component['goToReservationBudget']();

      expect(component['router'].navigate).toHaveBeenCalledWith(params);
    });
  });

  describe('on getForm' , () => {
    let data;
    let formResult;

    beforeEach(() => {
      data = DATA[FNRH_ENTRANCE_RESULT];
      formResult = DATA[GET_FORM_METHOD_RESULT];
      component.checkInHeaderDto.reservationBudget = formResult[0].currentBudget;
    });

    it('should test person type equal N', () => {
      const result = component.getForm(data);

      expect(result).toEqual(formResult);
    });

    it('should test insert children', () => {
      const nData = <FnrhSaveDto>JSON.parse(JSON.stringify(data));
      nData['isChild'] = true;
      const result = component.getForm(nData);

      expect(result[0]['childAge']).toEqual(childrenAge);
    });
  });

});
