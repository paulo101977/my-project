import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, of } from 'rxjs';
import { Location } from '@angular/common';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { FnrhConfigInterface } from 'app/shared/components/fnrh/fnrh-config.interface';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { FnrhSaveDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { switchMap } from 'rxjs/operators';
import { ShowHideContentEmitService } from 'app/shared/services/shared/show-hide-content-emit-service';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { ReservationItemViewService } from 'app/shared/services/reservation-item-view/reservation-item-view.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import * as moment from 'moment';
import { RightButtonTop } from 'app/shared/models/right-button-top';
import { AssetsService } from 'app/core/services/assets.service';
import { ModalGuestEntranceComponent } from '../../components/guest-entrance/modal-guest-entrance/modal-guest-entrance.component';
import { DateService } from 'app/shared/services/shared/date.service';
import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { GuestEntranceResource } from 'app/shared/resources/guest-entrance/guest-entrance.resource';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import {GuestFnrhService} from 'app/checkin/service/guest-fnrh.service';

@Component({
  selector: 'app-guest-fnrh-create-edit',
  templateUrl: './guest-entrance-create-edit.component.html',
})
export class GuestEntranceCreateEditComponent implements OnInit {

  private propertyId: number;
  public checkInHeaderDto: CheckInHeaderDto;
  public guestForm: Array<CheckinGuestReservationItemDto>;
  private reservationItemId: number;

  public reservationCode: string;
  public reservationItemCode: string;
  public systemDate: any;

  // Fnrh dependencies
  public config = <FnrhConfigInterface>{};
  private documentTypesAutocomplete;
  private guestTypeList;
  private nationalityList;
  private vehicleList;
  private reasonList;
  private occupationList;
  public responsableList;
  public guestCapacity: {adultCapacity: number, childCapacity: number};
  public reservationId: number;

  // Buttons dependencies
  public buttonCancelConfig: ButtonConfig;
  public topButtonsListConfig: any[];
  public buttonCustomSave: { textButton: string, buttonType: ButtonType };

  @ViewChild('MODAL_REF_ID') modalGuestEntrance: ModalGuestEntranceComponent;

  constructor(
    private checkinResource: CheckinResource,
    public _route: ActivatedRoute,
    private showHideContentEmitService: ShowHideContentEmitService,
    private commonService: CommomService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private reservationItemViewService: ReservationItemViewService,
    private location: Location,
    private modalToggleService: ModalEmitToggleService,
    private router: Router,
    private assetsService: AssetsService,
    private dateService: DateService,
    private guestEntranceResource: GuestEntranceResource,
    private guestFnrhService: GuestFnrhService
  ) {}

  ngOnInit(): void {
    const params = this._route.snapshot.params;
    const {
      reservationItemId,
      reservationItemCode,
      property
    } = params;

    this.propertyId = +property;
    this.setButtonConfig();
    this.setTopButtonsListConfig();

    if (reservationItemId) {
      this.reservationItemId = reservationItemId;
      this.reservationItemCode = this
        .reservationItemViewService
        .reduceAndGetCodeOfReservationItem(reservationItemCode);
      this.loadCompleteInfo();
    }
  }

  public loadCompleteInfo() {
    this
      .checkinResource
      .getCompleteAuxInfo(
        this.propertyId,
        this.reservationItemId,
        this.reservationId,
        moment().locale()
      )
      .pipe(
        switchMap((sources) => {
          const roomTypes$ = this.checkinResource.getRoomTypes(this.propertyId, sources[6]);
          const sources$ = [...sources.map(source => of(source)), roomTypes$];
          return forkJoin(sources$);
        }),
      )
      .subscribe(result => {
        this.documentTypesAutocomplete = result[0].items;
        this.guestTypeList = this.commonService
          .toOption(result[1], 'id', 'guestTypeName');
        this.vehicleList = this.commonService
          .toOption(result[2], 'id', 'transportationTypeName');
        this.nationalityList = result[3].items;
        this.reasonList = this.commonService
          .toOption(result[4], 'id', 'reasonName');
        this.occupationList = this.commonService
          .toOption(result[5], 'id', 'occupation');
        this.checkInHeaderDto = result[6];
        this.reservationId = result[6].id;
        this.responsableList = [...result[8].items];
        const roomTypeList = result[9].items;
        this.guestCapacity = roomTypeList
          .find( roomType => roomType.id === this.checkInHeaderDto.receivedRoomTypeId);
        this.configSelect();
      });
  }

  public goToReservationBudget = () => {
    const reservationId = this.reservationId;
    this
      .router
      .navigate(
        [
          'p',
          this.propertyId,
          'rm',
          'reservation-budget',
          reservationId
        ]);
  }

  private setTopButtonsListConfig(): void {
    const topReservationBudget = new RightButtonTop();
    topReservationBudget.iconAlt = 'label.reservationBudget';
    topReservationBudget.title = 'label.reservationBudget';
    topReservationBudget.callback = this.goToReservationBudget;
    topReservationBudget.iconDefault = this.assetsService.getImgUrlTo('icon_dollar_sign.svg');

    this.topButtonsListConfig = [topReservationBudget];
  }

  private setButtonConfig() {
    this.buttonCustomSave = {
      textButton: 'action.confirm',
      buttonType: ButtonType.Primary
    };
    this.buttonCancelConfig = new ButtonConfig();
    this.buttonCancelConfig.textButton = 'commomData.cancel';
    this.buttonCancelConfig.buttonType = ButtonType.Secondary;
    this.buttonCancelConfig.callback = () => this.location.back();
    this.buttonCancelConfig.id = 'fnrh-entrance-edit-cancel';
  }

  public hideAllPopovers(): void {
    this
      .showHideContentEmitService
      .emitChange(ShowHide.hide);
  }

  public configSelect() {
    const dataReservation = this.guestFnrhService.getDataReservation(this.checkInHeaderDto);
    const { reservationItemCode } = this;

    this.config = <FnrhConfigInterface>{
      reservationItemCode,
      genderList: [
        <SelectObjectOption>{ key: 'M', value: 'commomData.gender.male' },
        <SelectObjectOption>{ key: 'F', value: 'commomData.gender.female' },
      ],
      occupationList: this.occupationList,
      guestTypeList: this.guestTypeList,
      nationalityList: this.nationalityList,
      vehicleList: this.vehicleList,
      reasonList: this.reasonList,
      documentTypesAutocomplete: this.documentTypesAutocomplete,
      fnrh: new FnrhDto(),
      validateAvailableSlots: {
        adultCapacity: this.guestCapacity.adultCapacity,
        adultCount: this.checkInHeaderDto.adultCount,
        childCapacity: this.guestCapacity.childCapacity,
        childCount: this.checkInHeaderDto.childCount
      },
      dataReservation
    };
  }

  public getForm(data: FnrhSaveDto) {
    if (data) {
      let guestTypeId, newAge;

      if (data.person.personType &&  data.person.personType === 'N') {
        guestTypeId = 1;
      } else if (data.person.personType) {
        guestTypeId = 2;
      }

      if (data.isChild) {
        newAge = this.dateService
          .calculateAgeByDate(data.birthDate);
        !this.checkInHeaderDto.childrenAge
          ? this.checkInHeaderDto.childrenAge = [newAge]
          : this.checkInHeaderDto.childrenAge.push(newAge);
      }

      const docTypeId = data.document.documentTypeId === 0
        ? null
        : data.document.documentTypeId;

      return [
        <CheckinGuestReservationItemDto>{
          guestReservationItemId: data.guestReservationItemId,
          personId: data.personId,
          guestRegistrationId: data.id,
          isChild: data.isChild,
          guestId: data.guestId,
          guestName: data.fullName,
          guestEmail: data.email,
          documentInformation: data.document.documentInformation,
          documentTypeId: docTypeId,
          guestDocumentTypeId: docTypeId,
          guestDocument: data.document.documentInformation,
          guestTypeId: guestTypeId,
          preferredName: data.fullName,
          guestCitizenship: data.location.country,
          guestLanguage: data.location.browserLanguage,
          propertyId: data.propertyId,
          childAge: newAge,
          currentBudget: this.checkInHeaderDto.reservationBudget
        }
      ];
    }
  }

  public openModal() {
    this.modalToggleService.emitToggleWithRef(this.modalGuestEntrance.MODAL_REF_ID);
  }

  public fnrhSaved(fnrhData) {
    let resource, message;

    if (!fnrhData.id || fnrhData.id == GuidDefaultData) {
      resource = this.guestEntranceResource.createFnrh(fnrhData);
      message = this.translateService.instant('commomData.fnrh.actionsMessages.save');
    } else {
      resource = this.guestEntranceResource.updateFnrhEntrance(fnrhData);
      message = this.translateService.instant('commomData.fnrh.actionsMessages.update');
    }

    resource
      .subscribe((fnrhSaved) => {
        this.toasterEmitService.emitChange(SuccessError.success, message);
        if (fnrhSaved) {
          this.guestForm = this.getForm(fnrhSaved);
          this.openModal();
        }
      });
  }

}
