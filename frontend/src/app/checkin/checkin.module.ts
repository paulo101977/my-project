import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { CheckinResource } from '../shared/resources/checkin/checkin.resource';
import { CheckinRoutingModule } from './checkin-routing.module';
import { GuestFnrhEditComponent } from './component-container/guest-fnrh-edit/guest-fnrh-edit.component';

@NgModule({
  imports: [CommonModule, SharedModule, CheckinRoutingModule],
  providers: [CheckinResource],
  schemas: [NO_ERRORS_SCHEMA],
  declarations: [GuestFnrhEditComponent]
})
export class CheckinModule {}
