import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { FnrhConfigInterface } from 'app/shared/components/fnrh/fnrh-config.interface';
import { CheckinReservationItemBudgetDto } from 'app/shared/models/dto/checkin/checkin-reservation-item-budget-dto';
import { ResponsableDto } from 'app/checkin/models/responsable-dto';
import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import {FnrhSaveDto} from 'app/shared/models/dto/checkin/fnrh-save-dto';

export const GUEST_LIST = 'GUEST_LIST';
export const GUEST_CAPACITY = 'GUEST_CAPACITY';
export const GUEST_CARD_LIST = 'GUEST_CARD_LIST';
export const GUEST_RESERVATION_BUDGET = 'RESERVATIONBUDGET';
export const GUEST_FORM_MODAL = 'FORM_MODAL';
export const RESERVATION_ENTRANCE_BUDGET = 'RESERVATIONENTRANCEBUDGET';
export const GUEST_ADDRESS_LIST = 'GUEST_ADDRESS';
export const GUEST_FORM_FNRH_CONFIG = 'FNRH_CONFIG';
export const GUEST_RESOURCE_OBJECT = 'GUEST_RESOURCE_OBJECT';
export const GUEST_FORM = 'GUEST_FORM';
export const RESERVATION_ARRAY = 'RESERVATION_ARRAY';
export const HEADER_BUDGET = 'HEADER_BUDGET';
export const FNRH_GUEST_RESPONSABLE = 'GUEST_RESPONSABLE';
export const GUEST_FNRH_AUTOCOMPLETE = 'GUEST_FNRH';
export const ROOM_TYPES_LIST = 'ROOM_TYPES';
export const UHS_FROM_TYPE_LIST = 'UHS_FROM_TYPE';
export const RECEIVE_FNRH_LIST = 'RECEIVE_FNRH';
export const FNRH_CONFIG_RECEIVE_LIST = 'FNRH_CONFIG_RECEIVE';
export const GUEST_FNRH_PRINT = 'GUEST_FNRH_PRINT';
export const GUEST_HEADER_PRINT = 'GUEST_HEADER_PRINT';

export class DATA {
  public static readonly GUEST_LIST = <CheckinGuestReservationItemDto[]>[
    {
      businessSourceName: 'my source',
      reservationItemId: 1,
      reservationItemCode: 'my code',
      reservationItemStatusId: 2,
      guestReservationItemId: 4,
      guestReservationItemName: 'my item name',
      guestStatusId: 3,
      socialName: '',
      personId: '1',
      documentInformation: '',
      documentTypeName: '',
      guestRegistrationId: '',
      guestRegistrationDocumentBirthDate: new Date('2019-02-26T20:02:44.820Z'),
      division: '',
      subdivision: '',
      country: '',
      isValid: true,
      id: 123,
      isChild: false,
      childAge: 2,
      isMain: true
    },
    {
      businessSourceName: 'my source',
      reservationItemId: 2,
      reservationItemCode: 'my code',
      reservationItemStatusId: 3,
      guestReservationItemId: 5,
      guestReservationItemName: 'my item name 2',
      guestStatusId: 6,
      socialName: '',
      personId: '2',
      documentInformation: '',
      documentTypeName: '',
      guestRegistrationId: '',
      guestRegistrationDocumentBirthDate: new Date('2019-02-26T20:02:44.820Z'),
      division: '',
      subdivision: '',
      country: '',
      isValid: true,
      id: 124,
      isChild: true,
      childAge: 3,
      isMain: true
    }
  ];
  public static readonly GUEST_CAPACITY = {
    adultCapacity: '5',
    childCapacity: '4',
  };
  public static readonly GUEST_CARD_LIST = <GuestCard[]>[
    {
      guest: <CheckinGuestReservationItemDto>DATA.GUEST_LIST[1],
      active: true,
      isValid: true,
      number: 102,
      isSelected: true,
      isReadonly: false,
    }, {
      guest: <CheckinGuestReservationItemDto>DATA.GUEST_LIST[0],
      active: true,
      isValid: true,
      number: 103,
      isSelected: false,
      isReadonly: true,
    }
  ];

  public static readonly RESERVATIONBUDGET = <CheckInHeaderDto>{
    reservationCode: 'MUZ7KC6RK',
    adultCount: 1,
    arrivalDate: '2019-01-01T00:00:00',
    childCount: 0,
    childrenAge: [3],
    color: '1DC29D',
    departureDate: '2019-03-03T12:00:00',
    externalComments: '',
    housekeepingStatusName: 'Limpo',
    id: 4,
    internalComments: '',
    numberObservations: 0,
    overnight: 61,
    paymentType: 'Hotel',
    paymentTypeId: 4,
    quantityDouble: 1,
    quantitySingle: 1,
    receivedRoomTypeAbbreviation: 'LXO',
    receivedRoomTypeId: 2,
    receivedRoomTypeName: 'Luxo',
    requestedRoomTypeAbbreviation: 'LXO',
    requestedRoomTypeId: 2,
    requestedRoomTypeName: 'Luxo',
    reservationBudget: 0,
    reservationItemBudgetHeaderDto: {
      commercialBudgetList: [{
        totalBudget: 500
      }],
      isSeparated: true
    },
    reservationItemId: 4,
    reservationItemStatusId: 2,
    reservationItemStatusName: 'Check-In',
    roomLayoutId: '74BC15D2-25CB-44E9-B43C-036FE8E5207E',
    roomNumber: '501',
    comercialDeal: ''
  };

  public static readonly FORM_MODAL = {
    textConfirm: 'Confirmar'
  };

  public static readonly RESERVATIONENTRANCEBUDGET = <CheckInHeaderDto>{
    reservationBudgetCurrencySymbol: '€',
    adultCount: 7,
    arrivalDate: '2019-01-08T21:00:00',
    childCount: 5,
    childrenAge: [2],
    color: 'FF6969',
    departureDate: '2019-01-10T12:00:00',
    externalComments: '',
    housekeepingStatusName: 'Sujo',
    id: 3495,
    internalComments: '',
    mealPlanTypeId: 2,
    numberObservations: 0,
    overnight: 2,
    paymentType: 'Hotel',
    paymentTypeId: 4,
    quantityDouble: 4,
    quantitySingle: 9,
    receivedRoomTypeAbbreviation: 'BGR',
    receivedRoomTypeId: 63,
    receivedRoomTypeName: 'Big Room',
    requestedRoomTypeAbbreviation: 'BGR',
    requestedRoomTypeId: 63,
    requestedRoomTypeName: 'Big Room',
    reservationBudget: 600,
    reservationCode: '4JDF93BT7',
    reservationItemBudgetHeaderDto: {
      commercialBudgetList: [
        {
          commercialBudgetDayList: [
            {
              baseRateAmount: 100,
              childRateAmount: 0,
              currencyId: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
              currencySymbol: '€',
              id: 0,
              mealPlanTypeId: 2,
            }
          ],
          commercialBudgetName: 'Tarifa base',
          currencyId: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
          currencySymbol: '€',
          id: 0,
          mealPlanTypeDefaultId: 2,
          mealPlanTypeDefaultName: 'Café da Manha',
          mealPlanTypeId: 2,
          mealPlanTypeName: 'Café da Manha',
          rateVariation: 0,
          roomTypeAbbreviation: 'BGR',
          roomTypeId: 63,
          totalBudget: 900
        }
      ],
      isSeparated: true,
      mealPlanTypeId: 2,
      reservationItemCode: '4JDF93BT7-0001',
      reservationItemId: 3696,
      reservationBudgetDtoList: [
        {
          agreementRate: 150,
          baseRate: 100,
          childMealPlanTypeRate: 0,
          childRate: 0,
          currencyId: '2fb77beb-dd9d-4e0b-8319-3482c77e60c6',
          currencySymbol: '€',
          discount: 0,
          id: 17916,
          manualRate: 150,
          mealPlanTypeBaseRate: 50,
          mealPlanTypeId: 2,
          propertyBaseRateHeaderHistoryId: '96f17cec-42b7-42f1-bbd4-b23ca80e7115',
          rateVariation: 0,
          reservationBudgetUid: '00000000-0000-0000-0000-000000000000',
          reservationItemId: 0,
        }
      ]
    },
    reservationItemId: 3696,
    reservationItemStatusId: 2,
    reservationItemStatusName: 'Check-In',
    roomLayoutId: '56576890-480A-4763-A8D1-151FBF3C1689',
    roomNumber: '5520',
  };

  public static readonly GUEST_RESOURCE_OBJECT = {
    'id': 0,
    'reservationItemId': 3692,
    'reservationItemCode': '74JHK1V55-0001',
    'mealPlanTypeId': 2,
    'currencyId': 123,
    'reservationBudgetCurrencySymbol': 'R$',
    'isSeparated': false,
    ratePlanId: 1,
    gratuityTypeId: 1,
    'commercialBudgetList': [
      {
        'id': 0,
        ratePlanId: 1,
        companyClientId: 1,
        'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
        'roomTypeId': 63,
        'mealPlanTypeId': 2,
        'roomTypeAbbreviation': 'BGR',
        'mealPlanTypeName': 'Café da Manha',
        'commercialBudgetName': 'Tarifa base',
        'currencySymbol': 'R$',
        'rateVariation': 0.0,
        'totalBudget': 24549.0000,
        'mealPlanTypeDefaultId': 2,
        'mealPlanTypeDefaultName': 'Café da Manha',
        'commercialBudgetDayList': [
          {
            'id': 0,
            'day': '2019-01-09T00:00:00',
            'baseRateAmount': 150.0000,
            'childRateAmount': 30.0000,
            'currencySymbol': 'R$',
            'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
            'mealPlanTypeId': 2,
            baseRateWithRatePlanAmount: 1,
            ratePlanId: 1,
          },
          {
            'id': 0,
            'day': '2019-01-10T00:00:00',
            'baseRateAmount': 150.0000,
            'childRateAmount': 30.0000,
            'currencySymbol': 'R$',
            'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
            'mealPlanTypeId': 2,
            baseRateWithRatePlanAmount: 1,
            ratePlanId: 1,
          },
        ],
      }
    ],
    'reservationBudgetDtoList': [
      {
        'id': 17687,
        'reservationBudgetUid': '00000000-0000-0000-0000-000000000000',
        'reservationItemId': 0,
        'budgetDay': '2019-01-09T00:00:00',
        'baseRate': 100.0000,
        'childRate': 0.0000,
        'mealPlanTypeId': 2,
        'rateVariation': 0.0000,
        'agreementRate': 150.0000,
        'manualRate': 282.0000,
        'discount': 0.0000,
        'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
        'currencySymbol': 'R$',
        'mealPlanTypeBaseRate': 50.0000,
        'childMealPlanTypeRate': 0.0000,
        'childAgreementRate': 0.0000,
        'mealPlanTypeAgreementRate': 0.0000,
        'childMealPlanTypeAgreementRate': 0.0000,
        'propertyBaseRateHeaderHistoryId': '237ac6ef-6478-4212-a31a-dcd9a419afa4',
        previousManualRate: 1,
        previousAgreementRate: 1,
        separatedRate: 1,
        commercialBudgetName: '',
        baseRateChanged: false,
        originalManualRate: 1,
        overbooking: false,
      },
      {
        'id': 17688,
        'reservationBudgetUid': '00000000-0000-0000-0000-000000000000',
        'reservationItemId': 0,
        'budgetDay': '2019-01-10T00:00:00',
        'baseRate': 100.0000,
        'childRate': 0.0000,
        'mealPlanTypeId': 2,
        'rateVariation': 0.0000,
        'agreementRate': 150.0000,
        'manualRate': 282.0000,
        'discount': 0.0000,
        'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
        'currencySymbol': 'R$',
        'mealPlanTypeBaseRate': 50.0000,
        'childMealPlanTypeRate': 0.0000,
        'childAgreementRate': 0.0000,
        'mealPlanTypeAgreementRate': 0.0000,
        'childMealPlanTypeAgreementRate': 0.0000,
        'propertyBaseRateHeaderHistoryId': '237ac6ef-6478-4212-a31a-dcd9a419afa4',
        previousManualRate: 1,
        previousAgreementRate: 1,
        separatedRate: 1,
        commercialBudgetName: '',
        baseRateChanged: false,
        originalManualRate: 1,
        overbooking: false,
      },
    ],
  };

  public static readonly GUEST_FORM = {
    businessSourceName: 'Origem Automatizada Ativada',
    childAge: undefined,
    country: 'Afeganistão',
    currentBudget: 20304,
    division: 'RJ',
    documentInformation: '021.554.861-20',
    documentTypeId: 1,
    documentTypeName: 'CPF',
    guestCitizenship: 'Brasil',
    guestDocument: '021.554.861-20',
    guestDocumentTypeId: 1,
    guestEmail: 'paulo01@email.com',
    guestId: 107,
    guestLanguage: 'pt-BR',
    guestName: 'Paulo 1',
    guestRegistrationDocumentBirthDate: new Date('1971-03-25T00:00:00Z'),
    guestRegistrationId: 'cb7c383d-d59c-497a-a7ea-08d6b38aaf41',
    guestReservationItemId: 4066,
    guestReservationItemName: 'Paulo Teste 123',
    guestStatusId: 2,
    guestTypeId: 1,
    id: 3491,
    isChild: false,
    isLegallyIncompetent: false,
    isMain: true,
    isValid: true,
    personId: '73e6578b-b3ee-4b2f-bbcd-ace44981c710',
    preferredName: 'Paulo 1',
    propertyId: 1,
    reservationItemCode: '74JHK1V55-0001',
    reservationItemId: 3692,
    reservationItemStatusId: 2,
    subdivision: 'Rio de Janeiro',
    socialName: '',
    estimatedArrivalDate: null,
    checkInDate: null,
    estimatedDepartureDate: null,
    checkOutDate: null,
  };

  public static readonly RESERVATION_ARRAY = [
    {
      agreementRate: 333,
      baseRate: 150,
      budgetDay: '2019-01-09T00:00:00',
      childAgreementRate: undefined,
      childMealPlanTypeAgreementRate: undefined,
      childMealPlanTypeRate: 3,
      childRate: 30,
      commercialBudgetName: 'Tarifa base',
      currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
      currencySymbol: 'R$',
      discount: 0,
      id: 17687,
      manualRate: 333,
      mealPlanTypeAgreementRate: undefined,
      mealPlanTypeBaseRate: 150,
      mealPlanTypeId: 2,
      overbooking: false,
      propertyBaseRateHeaderHistoryId: '55868952-e3e9-4eab-bbba-709693952f9e',
      reservationBudgetUid: '00000000-0000-0000-0000-000000000000',
      reservationItemId: 0,
      rateVariation: 1,
      previousManualRate: 1,
      previousAgreementRate: 1,
      separatedRate: 1,
      baseRateChanged: false,
      originalManualRate: 1,
    },
    {
      agreementRate: 333,
      baseRate: 150,
      budgetDay: '2019-01-10T00:00:00',
      childAgreementRate: undefined,
      childMealPlanTypeAgreementRate: undefined,
      childMealPlanTypeRate: 3,
      childRate: 30,
      commercialBudgetName: 'Tarifa base',
      currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
      currencySymbol: 'R$',
      discount: 0,
      id: 17688,
      manualRate: 333,
      mealPlanTypeAgreementRate: undefined,
      mealPlanTypeBaseRate: 150,
      mealPlanTypeId: 2,
      overbooking: false,
      propertyBaseRateHeaderHistoryId: '55868952-e3e9-4eab-bbba-709693952f9e',
      reservationBudgetUid: '00000000-0000-0000-0000-000000000000',
      reservationItemId: 0,
      rateVariation: 1,
      previousManualRate: 1,
      previousAgreementRate: 1,
      separatedRate: 1,
      baseRateChanged: false,
      originalManualRate: 1,
    }
  ];

  public static readonly HEADER_BUDGET = <CheckinReservationItemBudgetDto>{
    'id': 0,
    'reservationItemId': 3692,
    'reservationItemCode': '74JHK1V55-0001',
    'mealPlanTypeId': 2,
    'currencyId': null,
    'reservationBudgetCurrencySymbol': 'R$',
    'isSeparated': false,
    ratePlanId: null,
    gratuityTypeId: null,
    'commercialBudgetList': [
      {
        'id': 0,
        ratePlanId: null,
        companyClientId: null,
        'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
        'roomTypeId': 63,
        'mealPlanTypeId': 2,
        'roomTypeAbbreviation': 'BGR',
        'mealPlanTypeName': 'Café da Manha',
        'commercialBudgetName': 'Tarifa base',
        'currencySymbol': 'R$',
        'rateVariation': 0.0,
        'totalBudget': 24549.0000,
        'mealPlanTypeDefaultId': 2,
        'mealPlanTypeDefaultName': 'Café da Manha',
        'commercialBudgetDayList': [
          {
            'id': 0,
            'day': new Date('2019-01-09T00:00:00'),
            'baseRateAmount': 150.0000,
            'childRateAmount': 30.0000,
            'currencySymbol': 'R$',
            'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
            'mealPlanTypeId': 2,
            baseRateWithRatePlanAmount: null,
            ratePlanId: null,
          },
          {
            'id': 0,
            'day': new Date('2019-01-10T00:00:00'),
            'baseRateAmount': 150.0000,
            'childRateAmount': 30.0000,
            'currencySymbol': 'R$',
            'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
            'mealPlanTypeId': 2,
            baseRateWithRatePlanAmount: null,
            ratePlanId: null,
          },
        ],
      }
    ],
    'reservationBudgetDtoList': [
      {
        'id': 17687,
        'reservationBudgetUid': '00000000-0000-0000-0000-000000000000',
        'reservationItemId': 0,
        'budgetDay': new Date('2019-01-09T00:00:00'),
        'baseRate': 100.0000,
        'childRate': 0.0000,
        'mealPlanTypeId': 2,
        'rateVariation': 0.0000,
        'agreementRate': 150.0000,
        'manualRate': 282.0000,
        'discount': 0.0000,
        'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
        'currencySymbol': 'R$',
        'mealPlanTypeBaseRate': 50.0000,
        'childMealPlanTypeRate': 0.0000,
        'childAgreementRate': 0.0000,
        'mealPlanTypeAgreementRate': 0.0000,
        'childMealPlanTypeAgreementRate': 0.0000,
        'propertyBaseRateHeaderHistoryId': '237ac6ef-6478-4212-a31a-dcd9a419afa4',
        previousAgreementRate: null,
        previousManualRate: null,
        separatedRate: null,
        commercialBudgetName: null,
        baseRateChanged: null,
        originalManualRate: null,
        overbooking: null,
      },
      {
        'id': 17688,
        'reservationBudgetUid': '00000000-0000-0000-0000-000000000000',
        'reservationItemId': 0,
        'budgetDay': new Date('2019-01-10T00:00:00'),
        'baseRate': 100.0000,
        'childRate': 0.0000,
        'mealPlanTypeId': 2,
        'rateVariation': 0.0000,
        'agreementRate': 150.0000,
        'manualRate': 282.0000,
        'discount': 0.0000,
        'currencyId': '67838acb-9525-4aeb-b0a6-127c1b986c48',
        'currencySymbol': 'R$',
        'mealPlanTypeBaseRate': 50.0000,
        'childMealPlanTypeRate': 0.0000,
        'childAgreementRate': 0.0000,
        'mealPlanTypeAgreementRate': 0.0000,
        'childMealPlanTypeAgreementRate': 0.0000,
        'propertyBaseRateHeaderHistoryId': '237ac6ef-6478-4212-a31a-dcd9a419afa4',
        previousAgreementRate: null,
        previousManualRate: null,
        separatedRate: null,
        commercialBudgetName: null,
        baseRateChanged: null,
        originalManualRate: null,
        overbooking: null,
      },
    ],
  };

  public static readonly FNRH_CONFIG = <FnrhConfigInterface>{
    guestReservationItemId: 1,
    genderList: null,
    occupationList: null,
    guestTypeList: null,
    nationalityList: [
      {
        countrySubdivisionId: 1,
        id: 1,
        languageIsoCode: 'pt-BR',
        name: 'Brasil',
        nationality: 'Brasileira'
      },
      {
        countrySubdivisionId: 5593,
        id: 5593,
        languageIsoCode: 'pt-BR',
        name: 'Portugal',
        nationality: 'Portuguesa'
      }
    ],
    vehicleList: null,
    reasonList: null,
    documentTypesAutocomplete: [
      {
        countrySubdivisionId: 1,
        id: 1,
        isMandatory: true,
        name: 'CPF',
        personType: 'N',
      },
      {
        countrySubdivisionId: 5684,
        id: 3,
        isMandatory: true,
        name: 'DNI',
        personType: 'N'
      }
    ],
    guest: {
      guest: {
        guestId: 1
      }
    },
    fnrh: {
      id: '1',
      isChild: false
    }
  };

  public static readonly GUEST_RESPONSABLE = <ResponsableDto[]>[
    {
      fullName: 'William',
      gender: 'M',
      guestId: 19,
      guestReservationItemId: 45,
      guestTypeId: 1,
      id: '1234'
    },
    {
      fullName: 'Teste da Silva Sauro',
      gender: 'M',
      guestId: 61,
      guestReservationItemId: 196,
      guestTypeId: 1,
      id: '124'
    }
  ];

  public static readonly GUEST_FNRH = {
    id: '24ee9a21-9dde-4545-1369-08d6b578a78c',
    isLegallyIncompetent: true,
    guestReservationItemId: 4075,
    guestId: 305,
    personId: '0369F90E',
    gender: 'Masculino',
    guestTypeId: 'N',
    responsibleGuestRegistrationId: '1234',
    fullName: 'asdasd asdasd',
    documentTypeId: 1,
    document: '191.000.000-00',
    birthDate: '1998-04-14T00:00:00',
    email: 'william@email.com',
    nationality: 1,
    phoneNumber: '+55 21 9976-6508',
    location: 'Brasil'
  };

  public static readonly ROOM_TYPES = [
    {
      abbreviation: 'LXO',
      adultCapacity: 2,
      childCapacity: 1,
      hasConjugatedRooms: false,
      id: 2,
      name: 'Luxo',
      overbooking: false,
      overbookingLimit: true,
      roomTypeBedTypeList: []
    },
    {
      abbreviation: 'BGR',
      adultCapacity: 10,
      childCapacity: 6,
      hasConjugatedRooms: false,
      id: 63,
      name: 'Big Room',
      overbooking: false,
      overbookingLimit: true,
      roomTypeBedTypeList: []
    }
  ];

  public static readonly UHS_FROM_TYPE = [
    {
      building: '2',
      childRoomIdList: [],
      color: 'FF6969',
      floor: '2',
      housekeepingStatusName: 'Sujo',
      id: 504,
      isActive: true,
      propertyId: 1,
      remarks: '',
      roomNumber: '100',
      roomTypeId: 74,
      wing: '2'
    },
    {
      building: '3',
      childRoomIdList: [],
      color: 'FF6969',
      floor: '3',
      housekeepingStatusName: 'Sujo',
      id: 505,
      isActive: true,
      propertyId: 1,
      remarks: '',
      roomNumber: '101',
      roomTypeId: 74,
      wing: '3'
    }
  ];

  public static readonly RECEIVE_FNRH = <FnrhDto>{
    additionalInformation: undefined,
    age: 43,
    arrivingBy: undefined,
    arrivingFrom: undefined,
    arrivingFromText: undefined,
    birthDate: '1976-03-19T00:00:00',
    childAge: undefined,
    document: '123.643.610-52',
    documentTypeId: 1,
    email: 'fgg@g.com',
    firstName: 'fgg',
    fullName: 'fgg',
    gender: undefined,
    guestId: 462,
    guestReservationItemId: 686,
    guestTypeId: 1,
    healthInsurance: undefined,
    id: 'a813e8ca-0524-4391-8d23-08d6bd0ccc70',
    incapable: false,
    isChild: false,
    isMain: true,
    lastName: 'fgg',
    location: {
      additionalAddressDetails: null,
      browserLanguage: 'pt-BR',
      completeAddress: 'Avenida das Américas, 8888 -  - Barra da Tijuca - Rio de Janeiro - RJ - ',
      countryCode: 'BR',
      division: 'RJ',
      id: 5656,
      latitude: -22.99876,
      locationCategoryId: 4,
      longitude: -43.397209,
      neighborhood: 'Barra da Tijuca',
      postalCode: '22793081',
      streetName: 'Avenida das Américas',
      streetNumber: '8888',
      subdivision: 'Rio de Janeiro'
    },
    nationality: 1,
    phoneNumber: '21998956047',
    mobilePhoneNumber: '21998956047',
    responsable: 19
  };

  public static readonly GUEST_ADDRESS = {
    additionalAddressDetails: null,
    browserLanguage: 'pt-BR',
    completeAddress: 'Avenida das Américas, 8888 -  - Barra da Tijuca - Rio de Janeiro - RJ - ',
    country: 'Brasil',
    countryCode: 'BR',
    countrySubdivisionId: null,
    division: 'RJ',
    id: 5656,
    latitude: -22.99876,
    locationCategoryId: 4,
    longitude: -43.9567045,
    neighborhood: 'Barra da Tijuca',
    ownerId: '00000000-0000-0000-0000-000000000000',
    postalCode: '22793081',
    streetName: 'Avenida das Américas',
    streetNumber: '8888',
    subdivision: 'Rio de Janeiro',
    valid: false
  };

  public static readonly FNRH_CONFIG_RECEIVE = <FnrhConfigInterface>{
    guestReservationItemId: 1,
    genderList: null,
    occupationList: null,
    guestTypeList: null,
    nationalityList: [
      {
        countrySubdivisionId: 1,
        id: 1,
        languageIsoCode: 'pt-BR',
        name: 'Brasil',
        nationality: 'Brasileira'
      },
      {
        countrySubdivisionId: 5593,
        id: 5593,
        languageIsoCode: 'pt-BR',
        name: 'Portugal',
        nationality: 'Portuguesa'
      }
    ],
    vehicleList: null,
    reasonList: null,
    documentTypesAutocomplete: [
      {
        countrySubdivisionId: 1,
        id: 1,
        isMandatory: true,
        name: 'CPF',
        personType: 'N',
      },
      {
        countrySubdivisionId: 5684,
        id: 3,
        isMandatory: true,
        name: 'DNI',
        personType: 'N'
      }
    ],
    guest: {
      guest: {
        guestId: 1
      }
    },
    fnrh: DATA.RECEIVE_FNRH
  };

  public static readonly GUEST_FNRH_PRINT = <FnrhSaveDto>{
    adultCount: 1,
    childCount: 2,
    arrivalDate: '2019-01-10 00:10:00',
    departureDate: '2019-01-20 00:10:00',
    roomTypeAbbreviation: 'STD',
    roomNumber: 102
  };

  public static readonly GUEST_HEADER_PRINT = <CheckInHeaderDto> {
    adultCount: 1,
    childCount: 2,
    arrivalDate: '2019-01-10 00:10:00',
    departureDate: '2019-01-20 00:10:00',
    receivedRoomTypeAbbreviation: 'STD',
    roomNumber: '102'
  };
}
