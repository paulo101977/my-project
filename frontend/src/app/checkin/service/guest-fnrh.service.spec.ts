import { TestBed } from '@angular/core/testing';
import {GuestFnrhService} from './guest-fnrh.service';
import {DATA, GUEST_FNRH_PRINT, GUEST_HEADER_PRINT} from 'app/checkin/mock-data';

describe('GuestFnrhService', () => {
  let service;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GuestFnrhService]
    });
    service = new GuestFnrhService;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should getDataReservation', () => {
    expect(service['getDataReservation'](DATA[GUEST_HEADER_PRINT])).toEqual(DATA[GUEST_FNRH_PRINT]);
  });
});
