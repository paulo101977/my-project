import { Injectable } from '@angular/core';
import {FnrhSaveDto} from 'app/shared/models/dto/checkin/fnrh-save-dto';
import {CheckInHeaderDto} from 'app/shared/models/dto/checkin/checkin-header-dto';

@Injectable({
  providedIn: 'root'
})
export class GuestFnrhService {

  constructor() { }

  public getDataReservation(items: CheckInHeaderDto) {
    return <FnrhSaveDto>{
      adultCount: items.adultCount,
      childCount: items.childCount,
      arrivalDate: items.arrivalDate,
      departureDate: items.departureDate,
      roomTypeAbbreviation: items.receivedRoomTypeAbbreviation,
      roomNumber: +items.roomNumber
    };
  }
}
