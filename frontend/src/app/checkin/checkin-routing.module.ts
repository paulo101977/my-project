import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuestFnrhCreateEditComponent } from './component-container/guest-fnrh-create-edit/guest-fnrh-create-edit.component';
import { GuestFnrhEditComponent } from './component-container/guest-fnrh-edit/guest-fnrh-edit.component';
import {
  GuestEntranceCreateEditComponent
} from 'app/checkin/component-container/guest-entrance-create-edit/guest-entrance-create-edit.component';

export const guestFnrhRoutes: Routes = [
  { path: 'new/:reservationId', component: GuestFnrhCreateEditComponent },
  { path: 'edit/:reservationItemId/:reservationItemCode', component: GuestFnrhCreateEditComponent },
  { path: 'edit/:reservationItemId/:reservationItemCode/guest/:guestReservationItemId', component: GuestFnrhEditComponent },
  {
    path: 'edit/entrance/:reservationItemId/:reservationItemCode',
    component: GuestEntranceCreateEditComponent
  },
];


@NgModule({
  imports: [RouterModule.forChild(guestFnrhRoutes)],
  exports: [RouterModule],
})
export class CheckinRoutingModule {}
