import {Person} from 'app/shared/models/person';

export class ResponsableDto {
  age: number;
  arrivingFromText: string;
  birthDate: Date;
  checkinDate: Date;
  documentTypeId: 0;
  email: string;
  firstName: string;
  fullName: string;
  gender: string;
  guestId: number;
  guestReservationItemId: number;
  guestTypeId: number;
  id: string;
  isChild: false;
  isLegallyIncompetent: false;
  isMain: true;
  lastName: string;
  nationality: number;
  nextDestinationText: string;
  person: Person;
  propertyId: number;
  socialName: string;
  tenantId: string;
}
