import { GuestPolicy } from 'app/policy/models/guest-policy';

export const GUEST_POLICY_BR: GuestPolicy = {
  countryId: 1,
  countryCode: 'br',
  description: 'Mussum Ipsum, cacilds vidis litro abertis. Quem manda na minha terra sou euzis! Per aumento ' +
    'de cachacis, eu reclamis. Viva Forevis aptent taciti sociosqu ad litora torquent. Mé faiz elementum ' +
    'girarzis, nisi eros vermeio.'
};

export const GUEST_POLICY_BR_2: GuestPolicy = {
  countryId: 2,
  countryCode: 'br',
  description: 'TESTING EDITED'
};

export const GUEST_POLICY_AR: GuestPolicy = {
  countryId: 3,
  countryCode: 'ar',
  description: `Ipsum, cacilds vidis litro abertis. Manduma pindureta quium dia nois paga. Per aumento de cachacis,
  eu reclamis. Quem num gosta di mim que vai caçá sua turmis! Delegadis gente finis,
  bibendum egestas augue arcu ut est.`
};

export const GUEST_POLICY_PT: GuestPolicy = {
  countryId: 4,
  countryCode: 'pt',
  description: 'O hóspede, mediante a aceitação da cláusula apresentada na frente do Registro de Hóspede, autoriza' +
    'expressamente que os seus dados, recebidos através do presente impresso, ou de agências de viagens, ' +
    'operadores, serviços de reserva pela internet ou serviços gerias de reserva com os quais o Hotel ' +
    'Trup Lisboa Caparica Mar colabora ou outros que surjam da relação connosco, sejam recolhidos e ' +
    'tratados em ficheiros, propriedade de sociedade do Grupo Hoti Hoteis, com a finalidade de lhe prestar ' +
    'o melhor serviço como Cliente, fazer-lhe chegar informação dos nossos produtos e serviços, assim como ' +
    'das Empresas Cessionários ou de Terceiros. O Cliente poderá exercer o seu direito de acesso, retificação ' +
    'cancelamento e oposição dirigindo-se ao Encarregado de Protecção de Dados da Hoti Hoteis por email: ' +
    'dadospessoais@hotihoteis.com.'
};

export const GUEST_POLICY_US: GuestPolicy = {
  countryId: 5,
  countryCode: 'us',
  description: 'The guest, through the acceptance of the clause presented in front of the Guest Register, ' +
    'expressly authorizes that his data, received through this form, or of travel agencies, operators, ' +
    'reservation services through the Internet or general reservation services with which the Hotel Trup ' +
    'Lisboa Caparica Mar collaborates or others that arise from the relationship with us, are collected ' +
    'and processed in files owned by the Hoti Hotel Group, in order to provide you with the best service ' +
    'as a Customer, to bring you information about our products and services, as well as the Assigned ' +
    'Companies or Third Parties. The Customer may exercise its right of access, rectification, cancellation ' +
    'and opposition by contacting the Data Protection Officer of Hoti Hotels by email: personal data@hotihoteis.com.'
};

export const GUEST_POLICY_BRAND_LIST = [
  { countryCode: 'br', countryId: 1},
  { countryCode: 'pt', countryId: 5593},
  { countryCode: 'us', countryId: 5630},
  { countryCode: 'ar', countryId: 5684},
];
