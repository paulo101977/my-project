import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PolicyListComponent } from './components/policy-list/policy-list.component';

export const policyRoutes: Routes = [{ path: '', component: PolicyListComponent }];

@NgModule({
  imports: [RouterModule.forChild(policyRoutes)],
  exports: [RouterModule],
})
export class PolicyRoutingModule {}
