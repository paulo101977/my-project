import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { GuestPolicySaveComponent } from './guest-policy-save.component';
import { ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { GUEST_POLICY_BR, GUEST_POLICY_BRAND_LIST } from 'app/policy/mock/data';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { CoreModule } from 'app/core/core.module';
import { of } from 'rxjs';
import { GuestPolicy } from 'app/policy/models/guest-policy';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('GuestPolicySaveComponent', () => {
  let component: GuestPolicySaveComponent;
  let fixture: ComponentFixture<GuestPolicySaveComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule, HttpClientTestingModule, CoreModule, RouterTestingModule],
      declarations: [GuestPolicySaveComponent],
      providers: [FormBuilder],
      schemas: [NO_ERRORS_SCHEMA]
    });
    fixture = TestBed.createComponent(GuestPolicySaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('resources', () => {
    it('should getCountryBrandList', () => {
      component['getCountryBrandList']();

      expect(component.countryBrandList).toEqual(GUEST_POLICY_BRAND_LIST);
    });
  });

  describe('set config', () => {
    it('should setForm', () => {
      component['setForm']();

      expect(Object.keys(component.form.controls)).toContain(
        'countryId',
        'description'
      );
    });

    it('should setButtonConfig', () => {
      component['setButtonConfig']();

      expect(component.buttonCancelModalConfig.id).toEqual('policy-cancel-modal');
      expect(component.buttonCancelModalConfig.textButton).toEqual('commomData.cancel');
      expect(component.buttonCancelModalConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.buttonSaveModalConfig.id).toEqual('policy-save-modal');
      expect(component.buttonSaveModalConfig.textButton).toEqual('commomData.confirm');
      expect(component.buttonSaveModalConfig.buttonType).toEqual(ButtonType.Primary);
      expect(component.buttonSaveModalConfig.buttonSize).toEqual(ButtonSize.Normal);
      expect(component.buttonSaveModalConfig.isDisabled).toBeTruthy();
    });
  });

  describe('on actions', () => {
    it('should patchForm', () => {
      component['patchForm'](GUEST_POLICY_BR);

      expect(component.form.controls['countryId'].value).toEqual(GUEST_POLICY_BR.countryId);
      expect(component.form.controls['description'].value).toEqual(GUEST_POLICY_BR.description);
    });

    it('should exit', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');

      component['exit']();

      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.id);
    });

    it('should resetForm', () => {
      component['resetForm']();

      expect(component.form.controls['countryId'].value).toEqual(null);
      expect(component.form.controls['description'].value).toEqual(null);
    });

    describe('#confirm', () => {
      let policy: GuestPolicy;
      beforeEach(() => {
        spyOn(component, 'exit');
        spyOn<any>(component, 'resetForm');
        spyOn(component.saved, 'emit');
        spyOn(component['policyResource'],  'createGuestPolicy').and.returnValue(of(null));
        spyOn(component['policyResource'],  'editGuestPolicy').and.returnValue(of(null));

        component.form.patchValue({
          countryId: GUEST_POLICY_BR.countryId,
          description: GUEST_POLICY_BR.description
        });

        const { countryId, description } = component.form.value;
        policy = { countryId, description};
      });

      it('should confirm call createGuestPolicy', fakeAsync(() => {
        component.edit = null;
        component['confirm']();
        tick();

        expect(component['policyResource'].createGuestPolicy).toHaveBeenCalledWith(policy);
      }));

      it('should confirm call createGuestPolicy', fakeAsync(() => {
        component.edit = <GuestPolicy> { countryId: 1, description: '', id: '123'};
        component['confirm']();
        tick();

        policy.id = '123';
        policy.isActive = true;

        expect(component['policyResource'].editGuestPolicy).toHaveBeenCalledWith(policy);
        expect(component.saved.emit).toHaveBeenCalledWith(policy);
        expect(component.exit).toHaveBeenCalled();
        expect(component['resetForm']).toHaveBeenCalled();
      }));

      it('should confirm call close methods after request', fakeAsync(() => {
        component.edit = null;
        component['confirm']();
        tick();

        expect(component.saved.emit).toHaveBeenCalledWith(policy);
        expect(component.exit).toHaveBeenCalled();
        expect(component['resetForm']).toHaveBeenCalled();
      }));


    });

    it('should set countryBrandList', () => {
      spyOn(component['guestPolicyService'], 'getBrandGuestPolicies');

      component['guestPolicyService'].getBrandGuestPolicies();

      expect(component.countryBrandList).toEqual(GUEST_POLICY_BRAND_LIST);
    });
  });
});
