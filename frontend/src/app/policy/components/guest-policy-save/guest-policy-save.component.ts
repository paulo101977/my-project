import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { GuestPolicy } from 'app/policy/models/guest-policy';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { GuestPolicyService } from 'app/policy/services/guest-policy.service';
import { PolicyResource } from 'app/shared/resources/policy/policy.resource';
import { Observable } from 'rxjs';

@Component({
  selector: 'guest-policy-save',
  templateUrl: './guest-policy-save.component.html',
  styleUrls: ['./guest-policy-save.component.css']
})
export class GuestPolicySaveComponent implements OnInit, OnChanges {

  // Modal dependencies
  public form: FormGroup;
  public modalCreateEditTitle: string;
// Button dependencies
  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;
  // Select dependencies
  public countryBrandList: Array<any>;

  @Input() id: string;
  @Input() edit: GuestPolicy;
  @Output() saved = new EventEmitter<GuestPolicy>();

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private modalEmitToggleService: ModalEmitToggleService,
    private guestPolicyService: GuestPolicyService,
    private policyResource: PolicyResource
  ) {
  }

  ngOnInit() {
    this.getCountryBrandList();
    this.setButtonConfig();
    this.setForm();
  }

  ngOnChanges() {
    if (this.edit) {
      this.modalCreateEditTitle = 'policyModule.createEdit.titleEdit';
      this.patchForm(this.edit);
    } else {
      this.modalCreateEditTitle = 'policyModule.createEdit.titleCreate';
      this.setForm();
    }
  }

  private getCountryBrandList() {
    this.countryBrandList = this.guestPolicyService.getBrandGuestPolicies();
  }

  private setForm() {
    this.form = this.formBuilder.group({
      countryId: ['', [Validators.required]],
      description: ['', [Validators.required, Validators.maxLength(4000)]],
    });
    this.form.valueChanges.subscribe(() => {
      this.buttonSaveModalConfig.isDisabled = this.form.invalid;
    });
  }

  private setButtonConfig() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'policy-cancel-modal',
      this.exit,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
      'policy-save-modal',
      this.confirm,
      'commomData.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
  }

  private patchForm(policy: GuestPolicy) {
    this.form.patchValue({
      countryId: policy.countryId,
      description: policy.description,
    });
  }

  public exit = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.id);
  }

  private resetForm() {
    this.form.reset();
  }

  public confirm = () => {
    const { countryId, description } = this.form.value;
    const policy: GuestPolicy = { countryId, description};
    let request: Observable<any>;
    if (this.edit) {
      policy.id = this.edit.id;
      policy.isActive = true;
      request = this.policyResource.editGuestPolicy(policy);
    } else {
      request = this.policyResource.createGuestPolicy(policy);
    }
    request.subscribe(() => {
      this.saved.emit(policy);
    }, null, () => {
      this.exit();
      this.resetForm();
    });


  }
}
