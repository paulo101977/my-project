import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PolicyListComponent } from './policy-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TruncatePipe } from '../../../shared/pipes/truncate.pipe';
import { ErrorComponent } from '../../../shared/components/error/error.component';
import { CoreModule } from '@app/core/core.module';
import { of } from 'rxjs';

describe('PolicyListComponent', () => {
  let component: PolicyListComponent;
  let fixture: ComponentFixture<PolicyListComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [FormsModule, RouterTestingModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule, CoreModule],
      declarations: [PolicyListComponent, TruncatePipe, ErrorComponent],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(PolicyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
