import { Component, OnInit } from '@angular/core';
import { PolicyResource } from '../../../shared/resources/policy/policy.resource';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { Policy, PolicyTypeEnum } from '../../../shared/models/dto/policy';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../shared/models/button-config';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { KeysPipe } from '../../../shared/pipes/keys.pipe';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { GuestPolicy } from 'app/policy/models/guest-policy';

@Component({
  selector: 'app-policy-list',
  templateUrl: './policy-list.component.html',
})
export class PolicyListComponent implements OnInit {
  private propertyId: number;
  public policyToRemove: Policy;
  // Header dependencies
  public configHeaderPage: ConfigHeaderPageNew;
  public searchableItems: SearchableItems;
  // Button dependencies
  public buttonNewConfig: ButtonConfig;
  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;
  public buttonCancelRemoveConfig: ButtonConfig;
  public buttonConfirmRemoveConfig: ButtonConfig;
  // Modal dependencies
  public formModalNew: FormGroup;
  public modalCreateEditTitle: string;
  // Select dependencies
  public policyOptionList: Array<SelectObjectOption>;
  // Table dependencies
  public rows: Array<Policy> = [];
  public itemsOption: Array<any>;

  constructor(
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private modalEmitToggleService: ModalEmitToggleService,
    private policyResource: PolicyResource,
    private commomService: CommomService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.setConfigHeaderPage();
      this.setButtonConfig();
      this.setFormModalPolicy();
      this.getPolicyList();
      this.itemsOption = [{title: 'commomData.edit'}, {title: 'commomData.delete'}];
    });
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      callBackFunction: this.toggleNew,
    };
  }

  private setButtonConfig() {
    this.buttonNewConfig = this.sharedService.getButtonConfig(
      'policy-new',
      this.toggleNew,
      'policyModule.listPage.header.buttonNew',
      ButtonType.Secondary,
      ButtonSize.Small,
    );
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'policy-cancel-modal',
      this.toggleNew,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
      'policy-save-modal',
      this.confirmSave,
      'commomData.confirm',
      ButtonType.Primary,
      null,
      true,
    );
    this.buttonCancelRemoveConfig = this.sharedService.getButtonConfig(
      'policy-remove-cancel-modal',
      this.toggleRemoveModal,
      'commomData.not',
      ButtonType.Secondary,
    );
    this.buttonConfirmRemoveConfig = this.sharedService.getButtonConfig(
      'policy-remove-confirm-modal',
      this.deleteRow,
      'commomData.yes',
      ButtonType.Primary,
    );
  }

  private setFormModalPolicy() {
    this.formModalNew = this.formBuilder.group({
      id: '',
      isActive: true,
      description: [null, [Validators.required, Validators.maxLength(4000)]],
      type: [null, [Validators.required]],
      typeName: null,
    });

    this.modalCreateEditTitle = 'policyModule.createEdit.titleCreate';

    this.formModalNew.valueChanges.subscribe(value => {
      this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
        'policy-save-modal',
        this.confirmSave,
        'commomData.confirm',
        ButtonType.Primary,
      );
      this.buttonSaveModalConfig.isDisabled = this.formModalNew.invalid;
      this.modalCreateEditTitle = value.id != '' ? 'policyModule.createEdit.titleEdit' : 'policyModule.createEdit.titleCreate';
    });
  }

  public toggleNew = () => {
    this.modalEmitToggleService.emitToggleWithRef('policy-new');
    this.setFormModalPolicy();
  }

  public toggleRemoveModal = () => {
    this.modalEmitToggleService.emitToggleWithRef('policy-remove');
  }

  public editRow(row: Policy) {
    this.toggleNew();
    this.formModalNew.patchValue({
      id: row.id,
      isActive: row.isActive,
      description: row.description,
      type: row.policyTypeId,
      typeName: row.policyTypeName,
    });
  }

  public confirmRemoveRow(row: Policy) {
    this.policyToRemove = row;
    this.toggleRemoveModal();
  }

  public deleteRow = () => {
    this.policyResource.deletePolicy(this.policyToRemove.id).subscribe(() => {
      this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant('policyModule.listPage.removedWithSuccess'));
      this.toggleRemoveModal();
      this.getPolicyList();
    });
  }

  public rowAction(event, row: Policy) {
    if (event.title == 'commomData.delete') {
      this.confirmRemoveRow(row);
    } else if (event.title == 'commomData.edit') {
      this.editRow(row);
    }
  }

  private getPolicyList() {
    this.policyResource.getAllPolicyList(this.propertyId).subscribe(response => {
      if (response) {
        this.rows = response.items;
        this.getPolicyOptionList();
      }
    });
  }

  public confirmSave = () => {
    const policy = new Policy();

    if (this.formModalNew.get('id').value != '') {
      policy.id = this.formModalNew.get('id').value;
    }

    policy.description = this.formModalNew.get('description').value;
    policy.isActive = this.formModalNew.get('isActive').value;
    policy.policyTypeId = this.formModalNew.get('type').value;
    policy.policyTypeName = this.formModalNew.get('typeName').value;
    policy.propertyId = this.propertyId;

    this.savePolicy(policy);
    this.toggleNew();
  }

  private getPolicyOptionList() {
    this.policyOptionList = new KeysPipe().transform(PolicyTypeEnum);
    const valuesTranslated = this.sharedService
      .tranlasteEnumToView(PolicyTypeEnum, 'policyModule.policyTypeEnum');
    this.policyOptionList.forEach((option, index) => {
      option['value'] = valuesTranslated[index];
    });
  }

  public itemWasUpdated(policy: Policy) {
    this.policyResource.updatePolicyStatus(policy).subscribe(() => {
      if (policy.isActive) {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('policyModule.listPage.activatedWithSuccess'),
        );
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('policyModule.listPage.inactivatedWithSuccess'),
        );
      }
      this.getPolicyList();
    });
  }

  private savePolicy(policy: Policy) {
    const message = 'policyModule.createEdit.saveMessageSuccess';

    if (policy.id) {
      this.policyResource.updatePolicy(policy).subscribe(() => {
        this.getPolicyList();
        this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant(message));
      });
    } else {
      this.policyResource.createPolicy(policy).subscribe(() => {
        this.getPolicyList();
        this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant(message));
      });
    }
  }

}
