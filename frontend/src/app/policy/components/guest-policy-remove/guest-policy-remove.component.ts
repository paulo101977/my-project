import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { GuestPolicy } from 'app/policy/models/guest-policy';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { PolicyResource } from 'app/shared/resources/policy/policy.resource';

@Component({
  selector: 'guest-policy-remove',
  templateUrl: './guest-policy-remove.component.html',
  styleUrls: ['./guest-policy-remove.component.css']
})
export class GuestPolicyRemoveComponent implements OnInit {

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  @Input() id: string;
  @Input() remove: GuestPolicy;
  @Output() removed = new EventEmitter<GuestPolicy>();

  constructor(
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private policyResource: PolicyResource
  ) {
  }

  ngOnInit() {
    this.setButtonConfig();
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'guest-policy-remove-cancel-modal',
      () => this.exit(),
      'commomData.not',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'guest-policy-remove-confirm-modal',
      () => this.confirm(),
      'commomData.yes',
      ButtonType.Primary,
    );
  }

  public exit() {
    this.modalEmitToggleService.emitToggleWithRef(this.id);
  }

  public confirm() {
    if (this.remove) {
      this.policyResource.deleteGuestPolicy(this.remove.id).subscribe(() => {
        this.removed.emit(this.remove);
        this.exit();
      });
    }
  }

}
