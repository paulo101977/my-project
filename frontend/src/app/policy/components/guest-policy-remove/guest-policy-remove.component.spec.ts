import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { GuestPolicyRemoveComponent } from './guest-policy-remove.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ButtonType } from 'app/shared/models/button-config';
import { GUEST_POLICY_BR } from 'app/policy/mock/data';
import { configureTestSuite } from 'ng-bullet';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CoreModule } from 'app/core/core.module';
import { TruncatePipe } from 'app/shared/pipes/truncate.pipe';
import { of } from 'rxjs';

describe('GuestPolicyRemoveComponent', () => {
  let component: GuestPolicyRemoveComponent;
  let fixture: ComponentFixture<GuestPolicyRemoveComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule, HttpClientTestingModule, CoreModule],
      declarations: [ GuestPolicyRemoveComponent, TruncatePipe],
      schemas: [NO_ERRORS_SCHEMA]
    });
    fixture = TestBed.createComponent(GuestPolicyRemoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set config', () => {
    it('should setButtonConfig', () => {
      component['setButtonConfig']();

      expect(component.buttonCancelConfig.id).toEqual('guest-policy-remove-cancel-modal');
      expect(component.buttonCancelConfig.textButton).toEqual('commomData.not');
      expect(component.buttonCancelConfig.buttonType).toEqual(ButtonType.Secondary);

      expect(component.buttonConfirmConfig.id).toEqual('guest-policy-remove-confirm-modal');
      expect(component.buttonConfirmConfig.textButton).toEqual('commomData.yes');
      expect(component.buttonConfirmConfig.buttonType).toEqual(ButtonType.Primary);
    });
  });

  describe('on actions', () => {
    it('should exit', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');

      component['exit']();

      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.id);
    });

    it('should confirm and call deleteGuestPolicy and close', fakeAsync(() => {
      spyOn(component['policyResource'], 'deleteGuestPolicy').and.returnValue(of(null));
      spyOn(component, 'exit');
      spyOn(component.removed, 'emit');

      component.remove = GUEST_POLICY_BR;
      component['confirm']();
      tick();

      expect(component['policyResource'].deleteGuestPolicy).toHaveBeenCalledWith(GUEST_POLICY_BR.id);
      expect(component.exit).toHaveBeenCalled();
      expect(component.removed.emit).toHaveBeenCalledWith(component.remove);
    }));
  });
});
