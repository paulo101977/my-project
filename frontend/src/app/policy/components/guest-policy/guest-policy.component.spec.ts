import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { GuestPolicyComponent } from './guest-policy.component';
import { ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ImgCdnPipeStub } from '@inovacao-cmnet/thx-ui';
import { FormBuilder } from '@angular/forms';
import { GUEST_POLICY_BR } from 'app/policy/mock/data';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CoreModule } from 'app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('GuestPolicyComponent', () => {
  let component: GuestPolicyComponent;
  let fixture: ComponentFixture<GuestPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule, HttpClientTestingModule, CoreModule, RouterTestingModule],
      declarations: [
        GuestPolicyComponent,
        ImgCdnPipeStub,
      ],
      providers: [FormBuilder],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set config', () => {
    it('should setGuestPolicyColumns', () => {
      component['setGuestPolicyColumns']();
      expect(component.columns).toEqual([
        {
          name: 'label.country',
          prop: 'countryCode',
          cellTemplate: component.countryBrand,
          cellClass: 'thf-u-padding-vertical--1',
          flexGrow: 0.2
        },
        {name: 'label.description', prop: 'description'}
      ]);
    });

    it('should setGuestPolicyOptions', () => {
      component['setGuestPolicyOptions']();
      expect(component.itemsOption).toEqual([
        {title: 'commomData.edit', callbackFunction: jasmine.any(Function)},
        {title: 'commomData.delete', callbackFunction: jasmine.any(Function)}
      ]);
    });

    it('should setButtonConfig', () => {
      component['setButtonConfig']();
      expect(component.buttonNewConfig.id).toEqual('guest-policy-new');
      expect(component.buttonNewConfig.textButton).toEqual('policyModule.listPage.header.buttonNew');
      expect(component.buttonNewConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.buttonNewConfig.buttonSize).toEqual(ButtonSize.Small);
    });
  });

  describe('on actions', () => {

    beforeEach(() => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    });

    it('should toggle new', () => {
      component.createEdit();

      expect(component.guestPolicyEdit).toBeNull();
      expect(component['modalEmitToggleService'].emitToggleWithRef)
        .toHaveBeenCalledWith(component.GUEST_POLICY_NEW);
    });

    it('should toggle edit', () => {
      component.createEdit(GUEST_POLICY_BR);

      expect(component.guestPolicyEdit).not.toBeNull();
      expect(component['modalEmitToggleService'].emitToggleWithRef)
        .toHaveBeenCalledWith(component.GUEST_POLICY_NEW);
    });
  });

  describe('Guest Policy', () => {
    const policyList = [
      {countryCode: 'br', description: 'AAAAAAAAAAAAAAAAAAA'},
      {countryCode: 'ar', description: 'BBBBBBBBBBBBBBBBBBB'},
      {countryCode: 'pt', description: 'CCCCCCCCCCCCCCCCCCC'},
      {countryCode: 'us', description: 'DDDDDDDDDDDDDDDDDDD'},
    ];

    beforeEach( () => {
      spyOn(component['policyResource'], 'getAllGuestPolicies').and.returnValue(of(policyList));
    });

    it('should setGuestPolicyList call resource', () => {
      component['loadGuestPolicyList']();

      expect(component['policyResource'].getAllGuestPolicies).toHaveBeenCalled();
    });

    it('should setGuestPolicyList populate guestPolicyList', fakeAsync(() => {
      component['loadGuestPolicyList']();
      tick();

      expect<any>(component.guestPolicyList).toEqual(policyList);
    }));
  });
});
