import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { TableColumn } from '@swimlane/ngx-datatable';
import { GuestPolicy } from 'app/policy/models/guest-policy';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { GuestPolicyService } from 'app/policy/services/guest-policy.service';
import { PolicyResource } from 'app/shared/resources/policy/policy.resource';

@Component({
  selector: 'guest-policy',
  templateUrl: './guest-policy.component.html',
  styleUrls: ['./guest-policy.component.css']
})
export class GuestPolicyComponent implements OnInit {

  // Table dependencies
  public columns: TableColumn[];
  public itemsOption: any[];
  // Modal dependencies
  public readonly GUEST_POLICY_NEW = 'GUEST_POLICY_NEW';
  public readonly GUEST_POLICY_REMOVE = 'GUEST_POLICY_REMOVE';
  // Info
  public guestPolicyEdit: GuestPolicy;
  public formModalNew: FormGroup;
  public guestPolicyRemove: GuestPolicy;
  public guestPolicyList: GuestPolicy[] = [];
  // Button dependencies
  public buttonNewConfig: ButtonConfig;

  @ViewChild('countryBrand') countryBrand: TemplateRef<any>;

  constructor(
    private modalEmitToggleService: ModalEmitToggleService,
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private guestPolicyService: GuestPolicyService,
    private policyResource: PolicyResource,
  ) {
  }

  ngOnInit() {
    this.setGuestPolicyTableConfig();
    this.setButtonConfig();
    this.loadGuestPolicyList();
  }

  public loadGuestPolicyList() {
    this.policyResource.getAllGuestPolicies().subscribe(policyList => {
      this.guestPolicyList = policyList;
    });
  }

  private setGuestPolicyTableConfig() {
    this.setGuestPolicyOptions();
    this.setGuestPolicyColumns();
  }

  private setGuestPolicyColumns() {
    this.columns = [
      {
        name: 'label.country',
        prop: 'countryCode',
        cellTemplate: this.countryBrand,
        cellClass: 'thf-u-padding-vertical--1',
        flexGrow: 0.2
      },
      {name: 'label.description', prop: 'description'}
    ];
  }

  private setGuestPolicyOptions() {
    this.itemsOption = [
      {title: 'commomData.edit', callbackFunction: (policy) => this.createEdit(policy)},
      {title: 'commomData.delete', callbackFunction: (policy) => this.remove(policy)}
    ];
  }

  private setButtonConfig() {
    this.buttonNewConfig = this.sharedService.getButtonConfig(
      'guest-policy-new',
      () => this.createEdit(),
      'policyModule.listPage.header.buttonNew',
      ButtonType.Secondary,
      ButtonSize.Small,
    );
  }

  public createEdit(policy?: GuestPolicy) {
    this.guestPolicyEdit = null;
    if (policy) {
      this.guestPolicyEdit = policy;
    }
    this.modalEmitToggleService.emitToggleWithRef(this.GUEST_POLICY_NEW);
  }

  public remove(policy: GuestPolicy) {
    this.guestPolicyRemove = null;
    if (policy) {
      this.guestPolicyRemove = policy;
    }
    this.modalEmitToggleService.emitToggleWithRef(this.GUEST_POLICY_REMOVE);
  }

}
