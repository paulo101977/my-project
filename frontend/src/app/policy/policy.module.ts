import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { PolicyListComponent } from './components/policy-list/policy-list.component';
import { PolicyRoutingModule } from './policy-routing.module';
import { GuestPolicyComponent } from './components/guest-policy/guest-policy.component';
import { GuestPolicySaveComponent } from './components/guest-policy-save/guest-policy-save.component';
import { ThxImgModule } from '@inovacao-cmnet/thx-ui';
import { GuestPolicyRemoveComponent } from './components/guest-policy-remove/guest-policy-remove.component';

@NgModule({
  imports: [CommonModule, SharedModule, PolicyRoutingModule, ThxImgModule],
  declarations: [PolicyListComponent, GuestPolicyComponent, GuestPolicySaveComponent, GuestPolicyRemoveComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class PolicyModule {
}
