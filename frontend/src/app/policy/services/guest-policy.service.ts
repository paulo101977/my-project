import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class GuestPolicyService {

  constructor() {}

  /**
   * There is an agreement with backend and frontend to let this only in front.
   * No api required
   */
  public getBrandGuestPolicies() {
    return [
      { countryCode: 'br', countryId: 1},
      { countryCode: 'pt', countryId: 5593},
      { countryCode: 'us', countryId: 5630},
      { countryCode: 'ar', countryId: 5684},
    ];
  }}
