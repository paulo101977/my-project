import {inject, TestBed} from '@angular/core/testing';

import {GuestPolicyService} from './guest-policy.service';
import {GUEST_POLICY_BRAND_LIST} from 'app/policy/mock/data';

describe('GuestPolicyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GuestPolicyService]
    });
  });

  it('should be created', inject([GuestPolicyService], (service: GuestPolicyService) => {
    expect(service).toBeTruthy();
  }));

  it('should return getBrandGuestPolicies', inject([GuestPolicyService], (service: GuestPolicyService) => {
    const list = service.getBrandGuestPolicies();
    expect(list).toEqual(GUEST_POLICY_BRAND_LIST);
  }));
});
