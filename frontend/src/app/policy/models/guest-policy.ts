export interface GuestPolicy {
  id?: string;
  countryId: number;
  countryCode?: string;
  description: string;
  isActive?: boolean;
}

export interface GuestPolicyPrint {
  countryCode: string;
  descriptionList: string[];
}
