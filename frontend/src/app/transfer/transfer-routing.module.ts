import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillingAccountTransferComponent } from './components-container/billing-account-transfer/billing-account-transfer.component';
import { CurrencyFormatPipe } from '../shared/pipes/currency-format.pipe';

export const transferRoutes: Routes = [{ path: '', component: BillingAccountTransferComponent }];

@NgModule({
  imports: [RouterModule.forChild(transferRoutes)],
  providers: [CurrencyFormatPipe],
  exports: [RouterModule],
})
export class TransferRoutingModule {}
