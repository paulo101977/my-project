import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'account-table-footer',
  templateUrl: './account-table-footer.component.html',
  styleUrls: ['./account-table-footer.component.css'],
})
export class AccountTableFooterComponent implements OnInit {
  @Input() quantity: number;

  @Input() priceTotal: number;

  @Input() accountType: string;

  @Input() currencySymbol: string;

  constructor() {}

  ngOnInit() {}
}
