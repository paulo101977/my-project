import { Component, OnInit, OnChanges, Input, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SlimboxConfig } from '../../../shared/models/slimbox-config';
import { BillingAccountWithAccount } from './../../../shared/models/billing-account/billing-account-with-account';
import { ShowHide } from './../../../shared/models/show-hide-enum';
import { BillingAccountTypeEnum } from './../../../shared/models/billing-account/billing-account-type-enum';
import { KeyValue } from '../../../shared/models/key-value';
import { ConfigLinkToTab } from '../../../shared/models/config-link-to-tab';
import { LoadPageEmitService } from './../../../shared/services/shared/load-emit.service';
import { BillingAccountItemService } from './../../../shared/services/billing-account-item/billing-account-item.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { BillingAccountResource } from './../../../shared/resources/billing-account/billing-account.resource';
import { BillingAccountWithEntry } from '../../../shared/models/billing-account/billing-account-with-entry';

@Component({
  selector: 'origin-account',
  templateUrl: './origin-account.component.html',
  styleUrls: ['./origin-account.component.css'],
})
export class OriginAccountComponent implements OnInit, OnChanges, OnDestroy {
  @Input() transferWasCompleted: boolean;

  private billingAccountId: string;
  public billingAccountWithAccount: BillingAccountWithAccount;
  public referenceToBillingAccountTypeEnum = BillingAccountTypeEnum;
  public quantityOfBillingAccountItemChecked: number;
  public priceTotalOfBillingAccountItemChecked: number;
  public currentBillingAccount: BillingAccountWithEntry;
  public currencySymbol: string;

  // Slimbox dependencies
  public slimboxConfig: SlimboxConfig;

  // Tab dependencies
  public itemsTab: KeyValue[];
  public configLinkToAdd: ConfigLinkToTab;

  public currentIndex: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private billingAccountResource: BillingAccountResource,
    private loadPageEmitService: LoadPageEmitService,
    private translateService: TranslateService,
    private billingAccountItemService: BillingAccountItemService,
    private sharedService: SharedService,
  ) {
    window.onbeforeunload = () => {
      this.billingAccountItemService.persistData();
    };
    this.billingAccountItemService.recoverPersistedData();
  }

  ngOnInit() {
    this.setVars();
    this.setListeners();
  }

  ngOnChanges(changes: any) {
    if (this.transferWasCompleted) {
      const billingAccountWithAccountWithouEntryCheckedList = this.billingAccountWithAccount;
      billingAccountWithAccountWithouEntryCheckedList.billingAccounts.forEach(billingAccountWithEntry => {
        billingAccountWithEntry.billingAccountItems.forEach(billingAccountItemEntry => {
          if (billingAccountItemEntry.isChecked) {
            this.sharedService.removeElementFromList(billingAccountItemEntry, billingAccountWithEntry.billingAccountItems);
          }
        });
        // DEIXA pois vou mexer nesse códio no próximo PR
        // const newList =  billingAccountWithEntry.billingAccountItems.filter(b => !b.isChecked);
        // const newBillingAccount = new BillingAccountWithEntry();
        // newBillingAccount.billingAccountId = billingAccountWithEntry.billingAccountId;
        // newBillingAccount.billingAccountItemName = billingAccountWithEntry.billingAccountItemName;
        // newBillingAccount.isBlocked = billingAccountWithEntry.isBlocked;
        // newBillingAccount.total = billingAccountWithEntry.total;
        // newBillingAccount.billingAccountItems = newList;
        // billingAccountWithEntry = newBillingAccount;
      });
      this.setBillingAccount();
      this.billingAccountWithAccount = billingAccountWithAccountWithouEntryCheckedList;
    }
  }

  private setVars(): void {
    this.quantityOfBillingAccountItemChecked = 0;
    this.priceTotalOfBillingAccountItemChecked = 0;
    this.slimboxConfig = <SlimboxConfig>{
      showFooter: false,
      showHeader: true,
    };
    this.configLinkToAdd = <ConfigLinkToTab>{
      hasHeightLimit: true,
    };

    this.route.queryParams.subscribe(params => {
      this.billingAccountId = params['billingAccountId'];
      this.setBillingAccount();
    });
  }

  private setListeners(): void {
    this.billingAccountItemService.billingAccountItemEntryListObservable.subscribe(list => {
      if (this.billingAccountWithAccount) {
        this.setIsCheckedOfBillingAccountItemEntryList();
      }
    });
  }

  private setItemsTab(): void {
    this.itemsTab = [];

    this.billingAccountWithAccount.billingAccounts.forEach((billingAccount, index) => {
      if (billingAccount.billingAccountItemName) {
        this.itemsTab.push(new KeyValue(index, billingAccount.billingAccountItemName));
      }
    });
    this.currentIndex = 0;
  }

  private setBillingAccount(): void {
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.billingAccountResource.getBillingAccountWithAccountById(this.billingAccountId).subscribe(response => {
      if (response) {
        this.billingAccountWithAccount = response;
        this.setBillingAccountWithEntryCurrent();
        this.setIsCheckedOfBillingAccountItemEntryList();
        this.loadPageEmitService.emitChange(ShowHide.hide);
      }
    });
  }

  private setBillingAccountWithEntryCurrent(): void {
    if (this.billingAccountWithAccount) {
      this.selectAccount(0);
      this.setItemsTab();
    }
  }

  private setIsCheckedOfBillingAccountItemEntryList(): void {
    let quantity = 0;
    let priceTotal = 0;
    this.billingAccountWithAccount.billingAccounts.forEach(billingAccountWithAccount => {
      billingAccountWithAccount.billingAccountItems.forEach(billingAccountItem => {
        this.billingAccountItemService.billingAccountWithEntryList.forEach(billingAccountItemEntry => {
          billingAccountItemEntry.billingAccountItems.forEach(billingAccountItemEntryChecked => {
            if (billingAccountItemEntryChecked.billingAccountItemId == billingAccountItem.billingAccountItemId) {
              billingAccountItem.isChecked = true;
              quantity++;
              priceTotal += billingAccountItemEntryChecked.total;
            }
          });
        });
      });
    });
    this.priceTotalOfBillingAccountItemChecked = priceTotal;
    this.quantityOfBillingAccountItemChecked = quantity;
  }

  public updateIsCheckedOfBillingAccountItemEntryList(): void {
    const billingAccountItemCheckedList = this.billingAccountItemService.getBillingAccountWithEntryCheckedList(
      this.billingAccountWithAccount,
    );
    this.billingAccountItemService.addBillingAccountItemListInStore(billingAccountItemCheckedList);
  }

  public selectAccount(index) {
    this.currentIndex = index;
    if (this.billingAccountWithAccount && this.billingAccountWithAccount.billingAccounts) {
      this.currentBillingAccount = this.billingAccountWithAccount.billingAccounts[index];
      if (this.currentBillingAccount && this.currentBillingAccount.billingAccountItems
        && this.currentBillingAccount.billingAccountItems.length > 0) {
        this.currencySymbol = this.currentBillingAccount.billingAccountItems[index].currencySymbol;
      }
    }
  }

  ngOnDestroy() {
    this.billingAccountItemService.cleanPersistedData();
  }
}
