import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnChanges } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BillingAccountWithEntry } from '../../../shared/models/billing-account/billing-account-with-entry';
import { BillingAccountItemEntry } from '../../../shared/models/billing-account/billing-account-item-entry';
import { BillingAccountItemTypeEnum } from '../../../shared/models/billing-account/billing-account-item-type-enum';
import { SharedService } from './../../../shared/services/shared/shared.service';
import { DateService } from './../../../shared/services/shared/date.service';

@Component({
  selector: 'account-table',
  templateUrl: './account-table.component.html',
  styleUrls: ['./account-table.component.css'],
})
export class AccountTableComponent implements OnInit, OnChanges {
  @Input() transferWasCompleted: boolean;
  @Input() isOriginAccount: boolean;
  @Input() billingAccountWithEntry: BillingAccountWithEntry;
  @Output() isCheckedOfBillingAccountItemEntryListUpdated = new EventEmitter<BillingAccountWithEntry>();

  public allItemsAreChecked: boolean;
  public referenceToDisplayAutocompleteEnum = BillingAccountItemTypeEnum;
  private testeA: boolean;

  // Table dependencies
  @ViewChild('myTable') table: DatatableComponent;
  widthColumns = Array<number>();
  public rows = [];

  constructor(private sharedService: SharedService, private dateService: DateService) {}

  ngOnInit() {}

  ngOnChanges(changes: any) {
    if (this.billingAccountWithEntry) {
      this.rows = [...this.billingAccountWithEntry.billingAccountItems];
    }
  }

  public setAllBillingAccountItemEntryToTrueOrFalse(): void {
    if (this.allItemsAreChecked) {
      this.billingAccountWithEntry.billingAccountItems.forEach(item => (item.isChecked = false));
    } else {
      this.billingAccountWithEntry.billingAccountItems.forEach(item => {
        if (item.billingAccountItemTypeId != BillingAccountItemTypeEnum.Reverse) {
          item.isChecked = true;
        }
      });
    }
    this.fireEventIsCheckedOfBillingAccountItemEntryListUpdated();
  }

  public setBillingAccountItemEntryToTrueOrFalse(billingAccountItemEntry: BillingAccountItemEntry) {
    if (billingAccountItemEntry.isChecked) {
      billingAccountItemEntry.isChecked = false;
    } else {
      billingAccountItemEntry.isChecked = true;
    }
    this.fireEventIsCheckedOfBillingAccountItemEntryListUpdated();
  }

  public getRowClass = (row: BillingAccountItemEntry) => {
    return {
      'thf-u-background-color--grey-sauvignon': row.billingAccountItemTypeId == BillingAccountItemTypeEnum.Reverse,
      'thf-u-background-color--blue-very-soft':
        (row.isChecked && this.transferWasCompleted) || (row.isChecked && !this.transferWasCompleted && this.isOriginAccount),
      'thf-u-background-color--grey-pattens': row.isChecked && !this.transferWasCompleted && !this.isOriginAccount,
      'thf-u-color--grey-submarine': row.isChecked && !this.transferWasCompleted && !this.isOriginAccount,
      'thf-u-border-bottom-dotted--2': row.isChecked && !this.transferWasCompleted && !this.isOriginAccount,
      'thf-u-border-top-dotted--2': row.isChecked && !this.transferWasCompleted && !this.isOriginAccount,
    };
  }

  public toggleExpandRow(row, expanded) {
    this.table.rowDetail.toggleExpandRow(row);
    this.widthColumns = this.table.headerComponent.columns.map(c => c.width);
  }

  private fireEventIsCheckedOfBillingAccountItemEntryListUpdated(): void {
    this.isCheckedOfBillingAccountItemEntryListUpdated.emit();
  }

  public getHoursAndMinutes(date: Date): string {
    if (!date) {
      return '';
    }
    const dateData = new Date(date);
    return this.dateService.getHoursAndMinutes(dateData);
  }

  public getCellClass({ row, column, value }): any {
    return {
      'thf-u-text-decoration--line-through': row.billingAccountItemTypeId == BillingAccountItemTypeEnum.Reverse,
    };
  }
}
