import { Component, OnInit, ViewChild, AfterViewInit, Output, EventEmitter, Input } from '@angular/core';
import { ThxPopoverDirective } from '@inovacao-cmnet/thx-ui';
import { SlimboxConfig } from '../../../shared/models/slimbox-config';
import { AutocompleteConfig } from '../../../shared/models/autocomplete/autocomplete-config';
import { ActivatedRoute } from '@angular/router';
import { BillingAccountDestination } from '../../../shared/models/billing-account/billing-account-destination';
import { ButtonConfig } from '../../../shared/models/button-config';
import { ButtonType } from '../../../shared/models/button-config';
import { BillingAccountItemService } from '../../../shared/services/billing-account-item/billing-account-item.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { BillingAccountResource } from '../../../shared/resources/billing-account/billing-account.resource';
import { TranslateService } from '@ngx-translate/core';
import { CurrencyFormatPipe } from '../../../shared/pipes/currency-format.pipe';

@Component({
  selector: 'destination-account',
  templateUrl: 'destination-account.component.html',
  styleUrls: ['./destination-account.component.css'],
})
export class DestinationAccountComponent implements OnInit, AfterViewInit {
  @ViewChild(ThxPopoverDirective) public destinationAccountInformationsPopover: ThxPopoverDirective;

  @Input() transferWasCompleted: boolean;
  @Output() billingAccountWithAccountWasSelected = new EventEmitter<string>();

  public autocompleteConfig: AutocompleteConfig;
  public billingAccountWithAccountByIdSelected: BillingAccountDestination;
  private propertyId: number;
  private billingAccountId: string;
  public guestUHAccount: any[];
  public accountDestinationInitialState: boolean;

  // Slimbox dependencies
  public slimboxConfig: SlimboxConfig;

  // Button dependencies
  public buttonConfirmConfig: ButtonConfig;

  constructor(
    private route: ActivatedRoute,
    private billingAccountItemService: BillingAccountItemService,
    public billingAccountResource: BillingAccountResource,
    private sharedService: SharedService,
    private translateService: TranslateService,
    private currencyFormatPipe: CurrencyFormatPipe,
  ) {}

  ngOnInit() {
    this.accountDestinationInitialState = true;
    this.route.queryParams.subscribe(params => {
      this.billingAccountId = params['billingAccountId'];
    });
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.getUHAccounts();
    });
    this.setSlimboxConfig();
    this.setAutoCompleteConfig();
    this.setButtonConfig();
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.destinationAccountInformationsPopover.onClick(), 1000);
  }

  private setButtonConfig(): void {
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'close-popover-account-destination',
      this.closeDestinationAccountInformationsPopover,
      'action.conclude',
      ButtonType.Widget,
    );
  }

  private getUHAccounts() {
    this.billingAccountResource.getOpenAccounts(this.propertyId, this.billingAccountId).subscribe(response => {
      if (response) {
        this.billingAccountItemService.billingAccountWithEntryList.forEach(billingAccountEntry => {
          response.items = response.items.filter(
            itemWithEntry => itemWithEntry.billingAccountId.toUpperCase() != billingAccountEntry.billingAccountId.toUpperCase(),
          );
        });
        this.guestUHAccount = response.items;
      }
    });
  }

  private search = event => {
    const query: string = event.query;
    if (query && query.length >= 3) {
      this.billingAccountResource.getSearchAccountOpen(this.propertyId, this.billingAccountId, event.query).subscribe(accountResponse => {
        this.autocompleteConfig.resultList = [...accountResponse.items];
      });
    }
  }

  private setSlimboxConfig() {
    this.slimboxConfig = <SlimboxConfig>{
      showFooter: false,
      showHeader: false,
    };
  }

  private setAutoCompleteConfig() {
    this.autocompleteConfig = new AutocompleteConfig();
    this.autocompleteConfig.dataModel = 'option';
    this.autocompleteConfig.callbackToSearch = this.search;
    this.autocompleteConfig.callbackToGetSelectedValue = this.getSelectedValue;
    this.autocompleteConfig.fieldObjectToDisplay = 'resultFormatted';
    this.translateService.get('label.placeholder.autocompleteDestinationAccount').subscribe(value => {
      this.autocompleteConfig.placeholder = value;
    });
  }

  private getSelectedValue = event => {
    this.addDestinationAccount(event);
  }

  public addDestinationAccount(account) {
    this.billingAccountWithAccountWasSelected.emit(account.billingAccountId);
    this.billingAccountResource.getBillingAccountWithAccountById(account.billingAccountId).subscribe(response => {
      if (response) {
        response.billingAccounts.forEach((billingAccountWithEntry, index) => {
          if (account.id == billingAccountWithEntry.billingAccountId) {
            this.billingAccountWithAccountByIdSelected = new BillingAccountDestination();
            this.billingAccountWithAccountByIdSelected.accountTypeId = account.accountTypeId;
            this.billingAccountWithAccountByIdSelected.isHolder = response.isHolder;
            this.billingAccountWithAccountByIdSelected.holderName = account.holderName;
            this.billingAccountWithAccountByIdSelected.accountTypeName = response.accountTypeName;
            this.billingAccountWithAccountByIdSelected.statusId = account.statusId;
            this.billingAccountWithAccountByIdSelected.statusName = response.statusName;
            this.billingAccountWithAccountByIdSelected.departureDate = response.departureDate;
            this.billingAccountWithAccountByIdSelected.arrivalDate = response.arrivalDate;
            this.billingAccountWithAccountByIdSelected.billingAccountItemName = account.billingAccountName;
            this.billingAccountWithAccountByIdSelected.billingAccountId = account.id;
            this.billingAccountWithAccountByIdSelected.billingAccountItems = billingAccountWithEntry.billingAccountItems;
            this.billingAccountWithAccountByIdSelected.total = billingAccountWithEntry.total;
            this.billingAccountItemService.billingAccountWithEntryList.forEach(billingAccountWithEntryChecked => {
              billingAccountWithEntryChecked.billingAccountItems.forEach(billingAccountItemEntryChecked => {
                this.billingAccountWithAccountByIdSelected.billingAccountItems.push(billingAccountItemEntryChecked);
              });
            });
          }
          this.accountDestinationInitialState = false;
        });
      }
    });
  }

  public closeDestinationAccountInformationsPopover = () => {
    this.destinationAccountInformationsPopover.hidePopover();
  }

  public backToAccountDestinationInitialState(): void {
    this.accountDestinationInitialState = true;
    this.billingAccountWithAccountWasSelected.emit(null);
  }

  public formatBalance(account) {
    const balance = this.currencyFormatPipe.transform(account.balance);
    return account.resultFormatted.replace(account.balanceFormatted, balance);
  }
}
