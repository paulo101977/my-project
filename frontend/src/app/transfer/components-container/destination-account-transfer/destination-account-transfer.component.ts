import { Component, Input, OnInit, ViewChild, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ThxPopoverDirective } from '@inovacao-cmnet/thx-ui';
import { BillingAccountTypeEnum } from '../../../shared/models/billing-account/billing-account-type-enum';
import { SlimboxConfig } from '../../../shared/models/slimbox-config';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import { KeyValue } from '../../../shared/models/key-value';
import { ButtonConfig } from '../../../shared/models/button-config';
import { ButtonType } from '../../../shared/models/button-config';
import { ConfigLinkToTab } from '../../../shared/models/config-link-to-tab';
import { BillingAccountDestination } from '../../../shared/models/billing-account/billing-account-destination';
import { TranslateService } from '@ngx-translate/core';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { BillingAccountItemService } from '../../../shared/services/billing-account-item/billing-account-item.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { BillingAccountResource } from '../../../shared/resources/billing-account/billing-account.resource';

@Component({
  selector: 'destination-account-transfer',
  templateUrl: 'destination-account-transfer.component.html',
  styleUrls: ['./destination-account-transfer.component.css'],
})
export class DestinationAccountTransferComponent implements OnInit, AfterViewInit {
  @ViewChild(ThxPopoverDirective) public changeDestinationAccountPopover: ThxPopoverDirective;

  @Input() transferWasCompleted: boolean;
  @Input() billingAccountDestination: BillingAccountDestination;
  @Output() closeBillinAccountDestination = new EventEmitter();

  public referenceToBillingAccountTypeEnum = BillingAccountTypeEnum;
  public quantityOfBillingAccountItemChecked: number;
  public priceTotalOfBillingAccountItemChecked: number;
  public priceTotal: number;
  public currencySymbol: string;

  // Slimbox dependencies
  public slimboxConfig: SlimboxConfig;

  // Tab dependencies
  public itemsTab: KeyValue[];
  public itemAllTab: KeyValue[];
  public configLinkToAdd: ConfigLinkToTab;

  // Button dependencies
  public buttonConfirmConfig: ButtonConfig;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private billingAccountResource: BillingAccountResource,
    private loadPageEmitService: LoadPageEmitService,
    private translateService: TranslateService,
    private billingAccountItemService: BillingAccountItemService,
    private sharedService: SharedService,
  ) {}

  ngOnInit() {
    this.setVars();
    this.setButtonConfig();
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.changeDestinationAccountPopover.onClick(), 1000);
  }

  private setButtonConfig(): void {
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'close-popover-account-destination',
      this.closeDestinationAccountInformationsPopover,
      'action.conclude',
      ButtonType.Widget,
    );
  }

  private setVars(): void {
    this.quantityOfBillingAccountItemChecked = 0;
    this.priceTotalOfBillingAccountItemChecked = 0;
    this.slimboxConfig = <SlimboxConfig>{
      showFooter: false,
      showHeader: true,
    };

    this.configLinkToAdd = <ConfigLinkToTab>{
      hasHeightLimit: true,
    };

    this.setBillingAccount();
  }

  private setItemsTab(): void {
    this.itemsTab = [];
    this.itemAllTab = [];
    this.translateService.get('label.all').subscribe(value => {
      const mainAccountTitle = value;
      const billingAccountItemName = this.billingAccountDestination.billingAccountItemName;

      if (typeof billingAccountItemName !== 'undefined' && billingAccountItemName !== null) {
        this.itemsTab.push(this.sharedService.getKeyValue(this.billingAccountDestination.billingAccountItemName));
      } else {
        this.itemsTab.push(this.sharedService.getKeyValue(mainAccountTitle));
      }
    });

    this.itemAllTab.push(this.itemsTab[0]);
  }

  private setBillingAccount(): void {
    if (this.billingAccountDestination) {
      this.setItemsTab();
    }
    this.setIsCheckedOfBillingAccountItemEntryList();
    this.loadPageEmitService.emitChange(ShowHide.hide);
  }

  private setIsCheckedOfBillingAccountItemEntryList(): void {
    let quantity = 0;
    let priceTotal = 0;
    if (this.billingAccountDestination.billingAccountItems && this.billingAccountDestination.billingAccountItems.length > 0) {
      this.currencySymbol = this.billingAccountDestination.billingAccountItems[0].currencySymbol;
      this.billingAccountDestination.billingAccountItems.forEach(billingAccountItem => {
        this.billingAccountItemService.billingAccountWithEntryList.forEach(billingAccountItemEntry => {
          billingAccountItemEntry.billingAccountItems.forEach(billingAccountItemEntryChecked => {
            if (billingAccountItemEntryChecked.billingAccountItemId == billingAccountItem.billingAccountItemId) {
              billingAccountItem.isChecked = true;
              quantity++;
              priceTotal += billingAccountItemEntryChecked.total;
            }
          });
        });
      });
    }
    this.priceTotalOfBillingAccountItemChecked = priceTotal;
    this.quantityOfBillingAccountItemChecked = quantity;
    this.priceTotal = this.billingAccountDestination.total + this.priceTotalOfBillingAccountItemChecked;
  }

  private closeDestinationAccountInformationsPopover = () => {
    this.changeDestinationAccountPopover.hidePopover();
  }

  public closeAccountDestination(): void {
    this.closeBillinAccountDestination.emit();
    this.changeDestinationAccountPopover.hidePopover();
  }
}
