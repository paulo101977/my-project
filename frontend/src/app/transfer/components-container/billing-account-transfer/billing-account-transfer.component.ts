import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ButtonSize } from './../../../shared/models/button-config';
import { SharedService } from './../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from './../../../shared/services/shared/modal-emit-toggle.service';
import { BillingAccountItemService } from './../../../shared/services/billing-account-item/billing-account-item.service';
import { BillingAccountItemTransferMapper } from './../../../shared/mappers/billing-account-item-transfer-mapper';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { BillingAccountItemResource } from './../../../shared/resources/billing-account-item/billing-account-item.resource';
import { BillingAccountWithEntry } from '../../../shared/models/billing-account/billing-account-with-entry';

@Component({
  selector: 'app-billing-account-transfer',
  templateUrl: './billing-account-transfer.component.html',
})
export class BillingAccountTransferComponent implements OnInit {
  public configHeaderPage: ConfigHeaderPageNew;
  public billingAccountIdOrigin: string;
  public billingAccountWithEntryListToTransfer: Array<BillingAccountWithEntry>;
  public billingAccountIdDestination: string;
  public transferWasCompleted: boolean;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonTransferConfig: ButtonConfig;
  public buttonBackToAccountConfig: ButtonConfig;
  public buttonCloseTransferModalConfig: ButtonConfig;
  public buttonCloseUndoTransferModalConfig: ButtonConfig;
  public buttonConfirmCancelTransferModalConfig: ButtonConfig;
  public buttonConfirmUndoTransferModalConfig: ButtonConfig;

  // Modal dependencies
  public cancelTransferModalId: string;
  public undoTransferModalId: string;

  constructor(
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private translateService: TranslateService,
    private location: Location,
    private billingAccountItemService: BillingAccountItemService,
    private billingAccountItemTransferMapper: BillingAccountItemTransferMapper,
    private billingAccountItemResource: BillingAccountItemResource,
    private toasterEmitService: ToasterEmitService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.setConfigHeaderPage();
    this.setButtonConfig();
    this.cancelTransferModalId = 'cancelTransfer';
    this.undoTransferModalId = 'undolTransfer';
    this.billingAccountIdOrigin = this.route.snapshot.queryParams['billingAccountId'];
  }

  public setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  private setButtonConfig(): void {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-account-destination',
      this.toggleCancelTransferModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonTransferConfig = this.sharedService.getButtonConfig(
      'transfer-account-destination',
      this.confirmTransfer,
      'action.transfer',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
    this.buttonBackToAccountConfig = this.sharedService.getButtonConfig(
      'back-to-account',
      this.goToBillingAccountPage,
      'action.backToAccount',
      ButtonType.Secondary,
    );
    this.buttonCloseTransferModalConfig = this.sharedService.getButtonConfig(
      'close-transfer-modal',
      this.toggleCancelTransferModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonCloseUndoTransferModalConfig = this.sharedService.getButtonConfig(
      'close-undo-transfer-modal',
      this.toggleUndoTransferModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmCancelTransferModalConfig = this.sharedService.getButtonConfig(
      'confirm-transfer-modal',
      this.cancelTransfer,
      'action.confirm',
      ButtonType.Primary,
    );
    this.buttonConfirmUndoTransferModalConfig = this.sharedService.getButtonConfig(
      'confirm-undo-transfer-modal',
      this.undoTransfer,
      'action.confirm',
      ButtonType.Primary,
    );
  }

  private toggleCancelTransferModal = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.cancelTransferModalId);
  }

  public toggleUndoTransferModal = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.undoTransferModalId);
  }

  private goToBillingAccountPage = () => {
    this.backToPreviousPage();
  }

  public cancelTransfer = () => {
    this.backToPreviousPage();
  }

  private backToPreviousPage(): void {
    this.location.back();
  }

  public setBillingAccountIdOrigin(billingAccountId: string): void {
    this.billingAccountIdOrigin = billingAccountId;
  }

  public setBillingAccountIdDestinationToTransfer(billingAccountId: string): void {
    this.billingAccountIdDestination = billingAccountId;
    if (!this.billingAccountIdDestination) {
      this.buttonTransferConfig = this.sharedService.getButtonConfig(
        'transfer-account-destination',
        this.confirmTransfer,
        'action.transfer',
        ButtonType.Primary,
        ButtonSize.Normal,
        true,
      );
    } else {
      this.buttonTransferConfig = this.sharedService.getButtonConfig(
        'transfer-account-destination',
        this.confirmTransfer,
        'action.transfer',
        ButtonType.Primary,
      );
    }
  }

  public confirmTransfer = () => {

    this.billingAccountWithEntryListToTransfer = this.billingAccountItemService.billingAccountWithEntryList;
    if (!this.billingAccountWithEntryListToTransfer || this.billingAccountWithEntryListToTransfer.length == 0) {
      this.toasterEmitService.emitChange(SuccessError.error, 'text.errorTransferAccount');
    } else {
      const billingAccountItemTransfer = this.billingAccountItemTransferMapper.toBillingAccountItemTransfer(
        this.billingAccountWithEntryListToTransfer,
        this.billingAccountIdDestination,
      );
      this.billingAccountItemResource.transferBillingAccountItem(billingAccountItemTransfer).subscribe(response => {
        this.toasterEmitService.emitChange(SuccessError.success, 'alert.successfulTransfer');
        this.transferWasCompleted = true;
      });
    }
  }

  private undoTransfer = () => {

    if (!this.billingAccountWithEntryListToTransfer || this.billingAccountWithEntryListToTransfer.length == 0) {
      this.toasterEmitService.emitChange(SuccessError.error, 'text.errorTransferAccount');
    } else {
      const billingAccountItemUndoTransfer = this.billingAccountItemTransferMapper.toBillingAccountItemUndoTransfer(
        this.billingAccountWithEntryListToTransfer,
      );
      this.billingAccountItemResource.undoTransferBillingAccountItem(billingAccountItemUndoTransfer).subscribe(response => {
        this.toasterEmitService.emitChange(SuccessError.success, 'alert.successfullUndoTransfer');
        this.goToBillingAccountPage();
      });
    }
  }
}
