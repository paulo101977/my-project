import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferRoutingModule } from './transfer-routing.module';
import { SharedModule } from '../shared/shared.module';
import { BillingAccountTransferComponent } from './components-container/billing-account-transfer/billing-account-transfer.component';
import { OriginAccountComponent } from './components/origin-account/origin-account.component';
import { DestinationAccountComponent } from './components/destination-account/destination-account.component';
import { AccountTableComponent } from './components/account-table/account-table.component';
import { AccountTableFooterComponent } from './components/origin-account-table-footer/account-table-footer.component';
import {
  DestinationAccountTransferComponent
} from './components-container/destination-account-transfer/destination-account-transfer.component';

@NgModule({
  imports: [CommonModule, SharedModule, TransferRoutingModule],
  declarations: [
    BillingAccountTransferComponent,
    OriginAccountComponent,
    DestinationAccountComponent,
    AccountTableComponent,
    AccountTableFooterComponent,
    DestinationAccountTransferComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class TransferModule {}
