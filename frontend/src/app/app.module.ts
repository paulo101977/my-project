import { HttpClient, HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TextMaskModule } from 'angular2-text-mask';
import { F1Service } from 'app/core/services/f1/f1.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { HttpService } from './core/services/http/http.service';
import { PropertyModule } from './property/property.module';
import { billingAccountWithEntryList } from './shared/reducers/billing-account/billing-account.reducer';
import { fnrhList } from './shared/reducers/fnrh.reducer';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// TODO: remove after deploy to npm
// barcode scanner
import { DeviceDetectorModule } from 'ngx-device-detector';
import { NgQrScannerModule } from 'angular2-qrscanner';
import { LocationManagerModule, configureAppName } from '@inovacaocmnet/thx-bifrost';
import { ProductTributesListComponent } from './tributes/components-container/product-tributes-list/product-tributes-list.component';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpService) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return function (state, action) {
    return reducer(state, action);
  };
}

const f1Factory = (f1Service: F1Service) => {
  return (): Promise<any> => {
    return f1Service.init();
  };
};

export const metaReducers: MetaReducer<any>[] = [debug];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    StoreModule.forRoot({
      fnrhList,
      billingAccountWithEntryList
    }, {metaReducers}),
    CoreModule,
    AppRoutingModule,
    PropertyModule,
    TextMaskModule,
    DeviceDetectorModule.forRoot(),
    LocationManagerModule,
    NgQrScannerModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    configureAppName('pms'),
    {
      provide: APP_INITIALIZER,
      useFactory: f1Factory,
      deps: [F1Service],
      multi: true
    }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {
}
