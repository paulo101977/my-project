// TODO: add tests [DEBIT: 7517]

import { Component, ComponentRef, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { BillingAccountItemTypeEnum } from 'app/shared/models/billing-account/billing-account-item-type-enum';
import { BillingAccountTypeEnum } from 'app/shared/models/billing-account/billing-account-type-enum';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { BillingAccountWithAccount } from 'app/shared/models/billing-account/billing-account-with-account';
import { PaymentTypeWithBillingItem } from 'app/shared/models/billingType/payment-type-with-billing-item';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { BillingAccountWithEntry } from 'app/shared/models/billing-account/billing-account-with-entry';
import { BillingAccountForClosure } from 'app/shared/models/billing-account/billing-account-for-closure';
import { GetAllBillingAccountForClosure } from 'app/shared/models/billing-account/get-all-billing-account-for-closure';
import { BillingAccountClosureResult } from 'app/shared/models/billing-account/billing-account-closure-result';
import { BillingAccountStatusEnum } from 'app/shared/models/billing-account/billing-account-status-enum';
import { BillingAccountClosure } from 'app/shared/models/billing-account/billing-account-closure';
import { BillingAccountCheckedListWithTotal } from 'app/shared/models/billing-account/billing-account-checked-list-with-total';
import { BillingAccountEntriesComplete } from 'app/shared/models/billing-account/billing-account-entries-complete';
import { BillingAccountOwner } from 'app/shared/models/billing-account/billing-account-owner';
import { RightButtonTop } from 'app/shared/models/right-button-top';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { BillingAccountItemMapper } from 'app/shared/mappers/billing-account-item-mapper';
import { BillingAccountClosureMapper } from 'app/shared/mappers/billing-account-closure-mapper';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { BillingAccountService } from 'app/shared/services/billing-account/billing-account.service';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { BillingAccountResource } from 'app/shared/resources/billing-account/billing-account.resource';
import { BillingAccountClosureResource } from 'app/shared/resources/billing-account/billing-account-closure.resource';
import { CheckoutResource } from 'app/shared/resources/checkout/checkout.resource';
import { ReservationItemResource } from 'app/shared/resources/reservation-item/reservation-item.resource';
import { ReservationItem } from 'app/shared/models/reserves/reservation-item';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';
import { BillingAccountItem } from 'app/shared/models/billing-account/billing-account-item';
import { StatementThermalPrinterComponent } from 'app/shared/components/statement-thermal-printer/statement-thermal-printer.component';
import { PrintComunicator } from 'app/shared/interfaces/print-comunicator';
import { AssetsService } from '@app/core/services/assets.service';

import { InternationalizationService } from 'app/shared/services/shared/internationalization.service';
import { UserService } from 'app/shared/services/user/user.service';
import { PrintRpsComponent } from 'app/revenue-management/rate-plan/print-rps/print-rps.component';
import * as moment from 'moment';
import { BillingAccountClosureInvoice } from 'app/shared/models/billing-account/billing-account-closure-invoice';
import { FiscalDocumentsService } from 'app/shared/services/billing-account/fiscal-documents.service';
import { BillingInvoiceTypeEnum } from 'app/shared/models/dto/invoice/billing-invoice-type-enum';
import { Location } from '@angular/common';



@Component({
  selector: 'statement-detail',
  templateUrl: './statement-detail.component.html',
  styleUrls: ['./statement-detail.component.css'],
  entryComponents: [StatementThermalPrinterComponent]
})
export class StatementDetailComponent implements OnInit, PrintComunicator {
  public readonly MODAL_AFTER_PAYMENT = 'model_payment_ok';
  public readonly MODAL_CHECKOUT = 'model_checkout';
  public readonly MODAL_EARLY_CHECKOUT = 'model_early_checkout';
  private propertyId: number;
  public statement: BillingAccountWithAccount;
  public reservationItem: ReservationItem;
  public statementList: Array<BillingAccountWithAccount>;
  public configHeaderPage: ConfigHeaderPageNew;
  public referenceToBillingAccountTypeEnum = BillingAccountTypeEnum;
  public billingAccountCurrentId: string;
  private groupKey: string;
  public billingAccountForClosureList: Array<BillingAccountForClosure>;
  public getAllBillingAccountForClosureData: GetAllBillingAccountForClosure;
  public paymentForm: FormGroup;
  public paymentTypeWithBillingItemList: Array<PaymentTypeWithBillingItem>;
  public billingAccountSelectedToPayment: BillingAccountWithEntry;
  public holderName: string;
  public billingAccountWithAccountList: Array<BillingAccountWithEntry>;
  public totalOfBillingAccountWithAccountList: number;
  private billingAccountForClosureCheckedList: Array<BillingAccountForClosure>;
  private isConsolidateAndPay: boolean;
  private paymentWasMade: boolean;
  private goToNextBillingAccount: boolean;
  public statementTotal: BillingAccountEntriesComplete;
  public isIndividualStatement: boolean;
  public billingAccountOwnerList: Array<BillingAccountOwner>;
  private billingAccountOwnerToPayment: BillingAccountOwner;
  public topButtonsListConfig: Array<RightButtonTop>;
  public openAccounts = 0;
  public checkoutEnabled = false;
  public completeCheckoutMode: string;
  private invoiceForPrintList: Array<BillingAccountClosureInvoice>;
  private keepPrintIframe: boolean;
  public currencySymbol: string;

  // Button dependencies
  public buttonPrintConfig: ButtonConfig;
  public buttonPayConfig: ButtonConfig;
  public okButtonConfig: ButtonConfig;
  public buttonCancelModalCheckoutConfig: ButtonConfig;
  public buttonSaveModalCheckoutConfig: ButtonConfig;
  public buttonEarlyCancelModalCheckoutConfig: ButtonConfig;
  public buttonEarlySaveModalCheckoutConfig: ButtonConfig;

  // Modal dependencies
  public optionBillingAccountClosureModalTitle: string;
  public optionBillingAccountClosureModalId: string;
  public billingAccountClosurePersonModalId: string;
  public multipleBillingAccountClosurePersonModalId: string;
  public statementModalId: string;
  public paymentModalId: string;

  public responseOfPayment: any;
  private reservationItemId: number;

  @ViewChild('statemant', { read: ElementRef }) componentToPrint: ElementRef;

  constructor(
    private sharedService: SharedService,
    private _route: ActivatedRoute,
    private router: Router,
    private billingAccountResource: BillingAccountResource,
    private modalEmitToggleService: ModalEmitToggleService,
    private formBuilder: FormBuilder,
    private billingAccountItemMapper: BillingAccountItemMapper,
    private billingAccountClosureMapper: BillingAccountClosureMapper,
    private toasterEmitService: ToasterEmitService,
    private billingAccountClosureResource: BillingAccountClosureResource,
    private translateService: TranslateService,
    private billingAccountService: BillingAccountService,
    private printService: PrintHelperService,
    private checkoutResource: CheckoutResource,
    private reservationItemResource: ReservationItemResource,
    private assetsService: AssetsService,
    private userService: UserService,
    private internationalizationService: InternationalizationService,
    private fiscalDocumentService: FiscalDocumentsService,
    private location: Location
  ) {}

  ngOnInit() {
    const params = this._route.snapshot.params;
    const queryParams = this._route.snapshot.queryParams;
    this.propertyId = +params.property;
    this.reservationItemId = queryParams.reservationItemId;
    this.billingAccountCurrentId = queryParams.id;
    this.groupKey = queryParams.groupKey ? queryParams.groupKey : queryParams.id;
    this.statementList = new Array<BillingAccountWithAccount>();
    if (queryParams.statementIndividual == 'true') {
      this.isIndividualStatement = true;
    } else {
      this.isIndividualStatement = false;
      this.getStatementUhTotal();
    }

    this.getStatement();
    this.setVars();
  }

  private setVars(): void {
    this.statement = new BillingAccountWithAccount();
    this.billingAccountWithAccountList = new Array<BillingAccountWithEntry>();
    this.getAllBillingAccountForClosureData = new GetAllBillingAccountForClosure();
    this.setConfigHeaderPage();
    this.setTopButtonsListConfig();
    this.setButtonConfig();
    this.optionBillingAccountClosureModalId = 'modal-option-billing-account-closure';
    this.billingAccountClosurePersonModalId = 'modal-billing-account-closure-person';
    this.multipleBillingAccountClosurePersonModalId = 'modal-multiple-billing-account-closure-person';
    this.statementModalId = 'modal-statement';
    this.setPaymentForm();
    this.goToNextBillingAccount = true;
    this.optionBillingAccountClosureModalTitle = this.isIndividualStatement ? 'label.chooseAnOption' : 'text.closure';
  }

  private setPaymentForm(): void {
    this.paymentForm = this.formBuilder.group({
      paymentTypeId: [null, [Validators.required]],
      card: this.formBuilder.group({
        plasticBrandPropertyId: null,
        nsuCode: null,
        installmentsNumber: 1,
      }),
      bankCheck: this.formBuilder.group({
        number: null,
      }),
      price: [null, [Validators.required]],
    });
  }

  public async getStatement(): Promise<void> {
    return this.billingAccountResource.getStatementByBillingAccountId(this.billingAccountCurrentId)
      .toPromise()
      .then(statement => {
        this.statement = statement;
        if (this.statement) {
          this.currencySymbol = this.statement.currencySymbol;
        }
        if (this.statement.accountTypeId != BillingAccountTypeEnum.Sparse && this.reservationItemId) {
          this.getReservationItenInfo();
        }
        this.statement.billingAccounts.forEach((billingAccount) => {
          let items = [];
          billingAccount.billingAccountItems
            .filter(statementItem => statementItem.billingAccountItemTypeId != BillingAccountItemTypeEnum.Reverse)
            .forEach(item => items = items
              .concat(item.billingAccountChildrenItems ? item.billingAccountChildrenItems
                  .filter(childItem => childItem.billingAccountItemTypeId != BillingAccountItemTypeEnum.Reverse)
                : [item]));
          billingAccount.billingAccountItems = items;
        });

        const accountDefault = this.statement.billingAccounts
          .find(billingAccount => billingAccount.billingAccountItemName == null);
        this.statement.billingAccounts = this.statement.billingAccounts
          .filter(billingAccount => billingAccount.billingAccountItemName != null);

        if (accountDefault) {
          this.statement.billingAccounts.unshift(accountDefault);
        }

        if (this.paymentWasMade) {
          this.setIsConsolidateAndPayToFalseAndBillingAccountSelected();
        }
        this.getBillingAccountForClosureList();

        if (this.statementList.length == 0) {
          this.statementList.push(this.statement);
        }
        this.setVisibilityPaymentButton();
        this.setHolderName();
      });

  }

  public getReservationItenInfo() {
    this.reservationItemResource.getReservationItemById(this.reservationItemId).subscribe( result => {
      this.reservationItem = result;

      const queryParams = this._route.snapshot.queryParams;
      if (queryParams.hasOwnProperty('checkout')  && queryParams.checkout) {
        this.completePayment();
      }
    });
  }

  private async getStatementUhTotal(): Promise<void> {
    return this.billingAccountResource.getStatementUhTotal(this.billingAccountCurrentId)
      .toPromise()
      .then(accountrientriecomplete => {
        this.statementTotal = accountrientriecomplete;
        this.statementList = accountrientriecomplete.billingAccountWithAccounts;
        this.setVisibilityPaymentButton();
      });
  }

  private getBillingAccountForClosureList(openPayment?: boolean): void {
    const allSimiliarAccounts = !this.isIndividualStatement;
    this.billingAccountResource
      .getForClosure(this.billingAccountCurrentId, allSimiliarAccounts)
      .subscribe(getAllBillingAccountForClosure => {
        getAllBillingAccountForClosure.billingAccountForClosureList = this.billingAccountService.getBillingAccountOpenForClosureList(
          this.statement,
          getAllBillingAccountForClosure.billingAccountForClosureList,
        );
        this.getAllBillingAccountForClosureData = getAllBillingAccountForClosure;
        this.setBillingAccountOwnerList();
        this.setIsCheckedOfBillingAccountForClosureListToTrue();
        this.setBillingAccountForClosureList();
        if (!this.isIndividualStatement && !this.paymentWasMade) {
          this.setInitialBillingAccountCurrentIdWhenIsNotIndividualStatement();
        }
        if (openPayment) {
          this.removeBillingAccountCurrentOfClosureList();
          this.openModalPaymentWithNewBillingAccountCurrentOrCloseModals();
          this.openModalOptionBillingAccountClosure();
        }
      });
  }

  private setBillingAccountOwnerList(): void {
    this.billingAccountOwnerList = this.getAllBillingAccountForClosureData.ownerList;
  }

  private setIsCheckedOfBillingAccountForClosureListToTrue(): void {
    this.getAllBillingAccountForClosureData.billingAccountForClosureList.forEach(b => (b.isChecked = true));
  }

  private setBillingAccountForClosureList(): void {
    this.billingAccountForClosureList = this.getAllBillingAccountForClosureData.billingAccountForClosureList;
  }

  private setInitialBillingAccountCurrentIdWhenIsNotIndividualStatement(): void {
    const firstBillingAccountForClosure = this.billingAccountForClosureList[0];
    if (firstBillingAccountForClosure) {
      this.billingAccountCurrentId = firstBillingAccountForClosure.id;
    }
  }

  public setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  private setButtonConfig() {
    this.buttonPrintConfig = this.sharedService.getButtonConfig(
      'action.print',
      () => this.printAccountStatement(),
      'action.print',
      ButtonType.Secondary,
    );
    this.buttonPayConfig = this.sharedService.getButtonConfig(
      'action.pay',
      this.openModalOptionBillingAccountClosure,
      'action.pay',
      ButtonType.Primary,
    );
    this.okButtonConfig = this.sharedService.getButtonConfig(
      'action.okPayment',
      () => this.completePayment(),
      'action.gotIt',
      ButtonType.Primary
    );
    this.buttonCancelModalCheckoutConfig = this.sharedService.getButtonConfig(
      'action.modalCancelCheckout',
      () => this.closeCheckoutModal(),
      'action.no',
      ButtonType.Secondary,
    );
    this.buttonSaveModalCheckoutConfig = this.sharedService.getButtonConfig(
      'action.modalSaveCheckout',
      () => this.completeCheckoutNormal(),
      'action.yes',
      ButtonType.Primary,
    );
    this.buttonEarlyCancelModalCheckoutConfig = this.sharedService.getButtonConfig(
      'action.modalCancelCheckout',
      () => this.closeEarlyCheckoutModal(),
      'action.no',
      ButtonType.Secondary,
    );
    this.buttonEarlySaveModalCheckoutConfig = this.sharedService.getButtonConfig(
      'action.modalSaveCheckout',
      () => this.completeCheckoutEarly(),
      'action.yes',
      ButtonType.Primary,
    );
  }

  private setTopButtonsListConfig(): void {
    this.topButtonsListConfig = [];

    const topButtonStatementPrint = new RightButtonTop();
    topButtonStatementPrint.title = 'action.print';
    topButtonStatementPrint.callback = this.printAccountStatement;
    topButtonStatementPrint.iconDefault = this.assetsService.getImgUrlTo('print.svg');
    topButtonStatementPrint.iconAlt = 'action.print';

    const topButtonStatementThermalPrint = new RightButtonTop();
    topButtonStatementThermalPrint.title = 'action.printThermal';
    topButtonStatementThermalPrint.callback = this.printAccountStatementThermal;
    topButtonStatementThermalPrint.iconDefault = this.assetsService.getImgUrlTo('thermal-printer.svg');
    topButtonStatementThermalPrint.iconAlt = 'action.print';

    const topButtonConsolidatePayment = new RightButtonTop();
    topButtonConsolidatePayment.title = 'action.finish';
    topButtonConsolidatePayment.callback = this.openModalOptionBillingAccountClosure;
    topButtonConsolidatePayment.iconDefault = this.assetsService.getImgUrlTo('credit-card.svg');
    topButtonConsolidatePayment.iconAlt = 'action.pay';

    this.topButtonsListConfig.push(topButtonStatementThermalPrint);
    this.topButtonsListConfig.push(topButtonStatementPrint);
    this.topButtonsListConfig.push(topButtonConsolidatePayment);
  }

  private printAccountStatement = () => {
    this.printService.printHtml(this.componentToPrint.nativeElement.innerHTML);
  }

  private printAccountStatementThermal = () => {
    this.printService.printComponent(StatementThermalPrinterComponent, this, true);
  }

  public openModalOptionBillingAccountClosure = () => {
    this.setBillingAccountSelectedToPaymentAndPaymentModalId();
    this.modalEmitToggleService.closeAllModals();
    this.modalEmitToggleService.emitToggleWithRefIfNoModalShowing(this.optionBillingAccountClosureModalId);
  }

  public openModalPayment(): void {
    if (!this.billingAccountSelectedToPayment) {
      this.toasterEmitService.emitChange(SuccessError.error, 'text.selectAccountInManageAccount');
    } else if (this.billingAccountSelectedToPayment.statusId == BillingAccountStatusEnum.Closed) {
      this.toasterEmitService.emitChange(
        SuccessError.error,
        `${this.translateService.instant('text.theAccount')} "${
          this.billingAccountSelectedToPayment.billingAccountItemName
        }" ${this.translateService.instant('text.isClosed')}`,
      );
    } else if (this.billingAccountSelectedToPayment.total > 0) {
      let msg = `${this.translateService.instant('text.accountBalanceP')} ${this.translateService
        .instant('text.isPositiveAndDoesNotAllowPayment')}`;
      if (this.billingAccountSelectedToPayment && this.billingAccountSelectedToPayment.billingAccountItemName) {
        msg = `${this.translateService.instant('text.accountBalance')} "${
          this.billingAccountSelectedToPayment.billingAccountItemName
          }" ${this.translateService.instant('text.isPositiveAndDoesNotAllowPayment')}`;
      }
      this.toasterEmitService.emitChange(
        SuccessError.error,
        msg,
      );
    } else if (this.billingAccountSelectedToPayment.total == 0) {
     this.closeEmptyAccount(this.billingAccountSelectedToPayment.billingAccountId, this.propertyId);
    } else {
      this.clearDataOfPaymentForm();
      this.clearValidatorsInPaymentForm();
      this.setHolderName();
      this.modalEmitToggleService.closeAllModals();
      this.modalEmitToggleService.emitToggleWithRefIfNoModalShowing(this.paymentModalId);
    }
  }

  public closeEmptyAccount(billingAccountId, propertyId) {

    let billingAccountClosure;
    if (this.isConsolidateAndPay) {
      const billingAccountItem: BillingAccountItem = new BillingAccountItem();
      billingAccountItem.id = this.billingAccountItemMapper.guidDefault;
      billingAccountItem.billingAccountId = this.billingAccountItemMapper.guidDefault;
      billingAccountItem.propertyId = propertyId;
      billingAccountItem.amount = 0;

      billingAccountClosure = this.prepareToCloseConsolidateAccount(billingAccountClosure, billingAccountItem);
      billingAccountClosure.credit = null;
    } else {
      billingAccountClosure = this.billingAccountClosureMapper.toBillingAccountClosureWhenTotalIsZero(billingAccountId, propertyId);
    }
    this.billingAccountClosureResource.payBillingAccount(billingAccountClosure).subscribe(
      response => {
        this.paymentSuccess(response);
      },
      () => {
        this.redirectToBillingAccountPage();
      },
    );
  }

  public openStatementModal(billingAccountCheckedListWithTotal: BillingAccountCheckedListWithTotal) {
    this.billingAccountWithAccountList = new Array<BillingAccountWithEntry>();
    this.billingAccountResource
      .showStatementOfBillingAccountList(billingAccountCheckedListWithTotal.billingAccountCheckedList)
      .subscribe(billingAccountWithAccount => {
        billingAccountWithAccount.items.forEach(billingAccountWithAccountData => {
          billingAccountWithAccountData.billingAccounts.forEach(billingAccountWithEntryData => {
            this.billingAccountWithAccountList.push(billingAccountWithEntryData);
          });
        });

        this.totalOfBillingAccountWithAccountList = billingAccountCheckedListWithTotal.total;
        this.modalEmitToggleService.emitToggleWithRef(this.statementModalId);
      });
  }

  private clearDataOfPaymentForm(): void {
    this.paymentForm.get('card.plasticBrandPropertyId').setValue(null);
    this.paymentForm.get('card.nsuCode').setValue(null);
    this.paymentForm.get('bankCheck.number').setValue(null);
  }

  private clearValidatorsInPaymentForm(): void {
    this.paymentForm.get('card.plasticBrandPropertyId').clearValidators();
  }

  public openModalBillingAccountClosurePerson(): void {
    this.modalEmitToggleService.closeAllModals();
    this.modalEmitToggleService.emitToggleWithRefIfNoModalShowing(this.billingAccountClosurePersonModalId);
  }

  public openModalMultipleBillingAccountClosurePerson(): void {
    this.modalEmitToggleService.closeAllModals();
    this.modalEmitToggleService.emitToggleWithRefIfNoModalShowing(this.multipleBillingAccountClosurePersonModalId);
  }

  public closeSelectBillingAccountModal() {
    this.modalEmitToggleService.emitCloseWithRef(this.billingAccountClosurePersonModalId);
  }

  public closeSelectMultipleBillingAccountModal() {
    this.modalEmitToggleService.emitCloseWithRef(this.multipleBillingAccountClosurePersonModalId);
  }

  public closeOptionBillingAccountClosureModal() {
    this.modalEmitToggleService.emitCloseWithRef(this.optionBillingAccountClosureModalId);
  }

  public makePayment(): void {
    const billingAccountClosure = this.getBillingAccountClosureToPayment();
    this
      .billingAccountClosureResource
      .payBillingAccount(billingAccountClosure)
      .subscribe((response: BillingAccountClosureResult) => {
          this.paymentSuccess(response, true);
      }, () => {
        this.redirectToBillingAccountPage();
      });
  }

  private paymentSuccess(response: BillingAccountClosureResult, paymentIsShowing?) {
    let account;
    this.statementList.forEach( i => {
      if (!account) {
        account = i.billingAccounts.find( item => item.billingAccountId == response.id);
      }
    });
    this.toasterEmitService.emitChangeTwoMessages(
      SuccessError.success,
      this.translateService.instant('text.payment'),
      this.translateService.instant('variable.doneSuccessM')
    );

    if (response.accountBalance < 0) {
      return this.reopenPayment();
    }

    this.paymentWasMade = true;
    this.responseOfPayment = response;

    if (this.openAccounts > 0) {
      this.openAccounts--;
    }
    if (this.checkoutEnabled && this.openAccounts <= 0) {
      this.checkoutResource.chekcoutAcomodation(this.propertyId, this.reservationItemId).subscribe(
        () => {
          this.resumePayment(this.responseOfPayment);
        },
        () => {
          this
            .toasterEmitService
            .emitChange(
              SuccessError.error, this.translateService.instant('label.messageErrorCheckout'));
              this.resumePayment(this.responseOfPayment);
        },
      );
    } else if (!this.checkoutEnabled) {
      this.modalEmitToggleService.closeAllModals();
      if (!account || (account && account.total < 0)) {
        this.printRps(response.invoiceList);
      } else {
        this.completePayment();
      }
    } else {
      this.completeCheckout();
    }
  }

  private async reopenPayment() {
    this.modalEmitToggleService.closeAllModals();
    this.paymentForm.reset();
    this.statementList = new Array<BillingAccountWithAccount>();

    if (!this.isIndividualStatement) {
      await this.getStatementUhTotal();
    }

    await this.getStatement();

    this.setBillingAccountSelectedToPaymentAndPaymentModalId();
    this.openModalPayment();
  }

  public resumePayment(response: BillingAccountClosureResult) {
    if (this.isConsolidateAndPay) {
      this.resolveResponseWhenPaymentIsConsolidate(response);
    } else {
      this.resolveResponseWhenPaymentIsNotConsolidate(response);
    }
  }

  private getCountry() {
    const user = this.userService.getUserLocalStorage();
    if (user && user.preferredLanguage) {
      return this.internationalizationService.getCountry(user.preferredLanguage);
    }

    // TODO: verify or remove default country
    return 'br';
  }

  private getBillingAccountClosureToPayment(): BillingAccountClosure {
    const billingAccountItem = this.billingAccountItemMapper.toBillintAccountItemForPayment(
      this.paymentForm,
      this.propertyId,
      this.billingAccountCurrentId,
      this.paymentTypeWithBillingItemList,
    );
    let billingAccountClosure: BillingAccountClosure;
    if (this.isConsolidateAndPay) {
      billingAccountClosure = this.prepareToCloseConsolidateAccount(billingAccountClosure, billingAccountItem);
    } else {
      billingAccountClosure = this.billingAccountClosureMapper.toBillingAccountClosure(
        [this.billingAccountCurrentId],
        this.propertyId,
        billingAccountItem,
      );
    }

    billingAccountClosure.twoLetterIsoCodeCountry = this.getCountry();

    return billingAccountClosure;
  }

  private prepareToCloseConsolidateAccount(billingAccountClosure: BillingAccountClosure, billintAccountItem) {
    const billingAccountIdList = new Array<string>();
    this.billingAccountForClosureCheckedList.forEach(billingAccount => {
      billingAccountIdList.push(billingAccount.id);
    });
    if (this.billingAccountOwnerToPayment) {
      const ownerIdDestination = this.billingAccountOwnerToPayment.ownerId;
      const billingAccountForClosureDestination = this.sharedService.getElementInList(
        null,
        this.billingAccountForClosureCheckedList,
        b => b.ownerId == ownerIdDestination,
      );
      billingAccountClosure = this.billingAccountClosureMapper.toBillingAccountClosure(
        billingAccountIdList,
        this.propertyId,
        billintAccountItem,
        billingAccountForClosureDestination,
        this.billingAccountOwnerToPayment,
      );
    } else {
      billingAccountClosure = this.billingAccountClosureMapper.toBillingAccountClosure(
        billingAccountIdList,
        this.propertyId,
        billintAccountItem,
        this.billingAccountForClosureCheckedList[0],
      );
    }
    this.translateService.get('text.consolidatedF').subscribe(text => {
      billingAccountClosure.billingAccountName = text;
    });
    return billingAccountClosure;
  }

  public redirectToBillingAccountPage() {
    this.location.back();
  }

  private resolveResponseWhenPaymentIsConsolidate(response: BillingAccountClosureResult): void {
    if (response) {
      this.billingAccountCurrentId = response.id;
      if (response.accountBalance == 0) {
        this.closeAllModalsAndRedirectToBillingAccountPage();
      } else {
        this.billingAccountSelectedToPayment = new BillingAccountWithEntry();
        this.billingAccountSelectedToPayment.total = response.accountBalance;
        // Preciso colocar isConsolidateAndPay como false,
        // pois o próximo pagamento será de uma conta simples (a conta consolidada que foi criada)
        this.isConsolidateAndPay = false;
        this.goToNextBillingAccount = false;
      }
    } else {
      this.closeAllModalsAndRedirectToBillingAccountPage();
    }
  }

  private resolveResponseWhenPaymentIsNotConsolidate(response: BillingAccountClosureResult): void {
    if (response) {
      if (response.accountBalance == 0 && !this.goToNextBillingAccount && this.openAccounts <= 0) {
        this.closeAllModalsAndRedirectToBillingAccountPage();
      } else if (response.accountBalance == 0 && this.billingAccountForClosureList.length > 0) {
        this.removeBillingAccountCurrentOfClosureList();
        this.openModalPaymentWithNewBillingAccountCurrentOrCloseModals();
      } else {
        this.billingAccountSelectedToPayment.total = response.accountBalance;
      }
    } else {
      this.closeAllModalsAndRedirectToBillingAccountPage();
    }
  }

  private removeBillingAccountCurrentOfClosureList(): void {
    const billingAccountToRemovedOfClosureList = this.billingAccountForClosureList.find(b => b.id == this.billingAccountCurrentId);
    this.sharedService.removeElementFromList(billingAccountToRemovedOfClosureList, this.billingAccountForClosureList);
  }

  private openModalPaymentWithNewBillingAccountCurrentOrCloseModals(): void {
    const billingAccountListWithStatusOpen = this.billingAccountForClosureList.filter(b => b.statusId == BillingAccountStatusEnum.Open);
    if (billingAccountListWithStatusOpen && billingAccountListWithStatusOpen.length > 0) {
      this.billingAccountCurrentId = billingAccountListWithStatusOpen[0].id;
      this.billingAccountOwnerToPayment = null;
      this.setBillingAccountSelectedToPaymentAndPaymentModalId();
      this.openModalPayment();
    } else {
      this.closeAllModalsAndRedirectToBillingAccountPage();
    }
  }

  public updatePaymentTypeWithBillingItemList(paymentTypeWithBillingItemList: Array<PaymentTypeWithBillingItem>): void {
    this.paymentTypeWithBillingItemList = paymentTypeWithBillingItemList;
  }

  public updateBillingAccountForClosureCheckedList(billingAccountForClosureCheckedList: Array<BillingAccountForClosure>): void {
    this.billingAccountForClosureCheckedList = billingAccountForClosureCheckedList;
  }

  public setIsConsolidateAndPayToTrue(): void {
    this.isConsolidateAndPay = true;
  }

  public setIsConsolidateAndPayToFalseAndBillingAccountSelected(): void {
    this.setBillingAccountSelectedToPaymentAndPaymentModalId();
    this.isConsolidateAndPay = false;
  }

  private setBillingAccountSelectedToPaymentAndPaymentModalId(): void {
    this.statementList.forEach(billingAccountWithAccount => {
      billingAccountWithAccount.billingAccounts.forEach(billingAccount => {
        if (billingAccount.billingAccountId == this.billingAccountCurrentId) {
          this.billingAccountSelectedToPayment = billingAccount;
          this.billingAccountSelectedToPayment.total = billingAccount.total;
        }
      });
    });
    if (this.billingAccountSelectedToPayment) {
      this.setBillingAccountItemName();
      this.paymentModalId = this.billingAccountSelectedToPayment.billingAccountId;
    }
  }

  private setHolderName(): void {
    if (this.billingAccountOwnerToPayment) {
      this.holderName = this.billingAccountOwnerToPayment.ownerName;
    } else {
      this.statementList.forEach(billingAccountWithAccount => {
        billingAccountWithAccount.billingAccounts.forEach(billingAccount => {
          if (billingAccount.billingAccountId == this.billingAccountCurrentId) {
            this.holderName = billingAccountWithAccount.holderName;
            return;
          }
        });
      });
    }
  }

  private setBillingAccountItemName(): void {
    if (!this.billingAccountSelectedToPayment.billingAccountItemName) {
      this.billingAccountSelectedToPayment.billingAccountItemName = this.translateService.instant('label.all');
    }
  }

  public setBillingAccountSelectedToPaymentWhenIsConsolidate(): void {
    this.translateService.get('text.consolidatedF').subscribe(text => {
      this.paymentModalId = text + this.billingAccountCurrentId;
    });
    this.billingAccountSelectedToPayment = new BillingAccountWithEntry();
    this.billingAccountSelectedToPayment.total = 0;
    this.billingAccountForClosureCheckedList.forEach(billingAccount => {
      this.billingAccountSelectedToPayment.total += billingAccount.total;
    });
  }

  public setBillingAccountOwner(billingAccountOwnerToPayment: BillingAccountOwner): void {
    this.billingAccountOwnerToPayment = billingAccountOwnerToPayment;
    if (this.billingAccountOwnerToPayment) {
      this.holderName = this.billingAccountOwnerToPayment.ownerName;
    }
  }

  public closeAllModalsAndRedirectToBillingAccountPage() {
    this.modalEmitToggleService.closeAllModals();
    this.redirectToBillingAccountPage();
  }

  public completePayment() {
    if (this.statement.accountTypeId != BillingAccountTypeEnum.Sparse &&
      (this.reservationItem &&  this.reservationItem.reservationItemStatusId >= ReservationStatusEnum.Checkin &&
        this.reservationItem.reservationItemStatusId != ReservationStatusEnum.Checkout &&
        this.reservationItem.reservationItemStatusId != ReservationStatusEnum.Pending)) {
      this.checkoutResource.checkEarlyCheckout(this.propertyId, this.reservationItemId).subscribe(result => {
        this.modalEmitToggleService.closeAllModals();
        if (result && result.isEarlyCheckout) {
          this.completeCheckoutMode = this.MODAL_EARLY_CHECKOUT;
          this.modalEmitToggleService.emitToggleWithRefIfNoModalShowing(this.MODAL_EARLY_CHECKOUT);
        } else {
          this.completeCheckoutMode = this.MODAL_CHECKOUT;
          this.modalEmitToggleService.emitToggleWithRefIfNoModalShowing(this.MODAL_CHECKOUT);
        }
      });
    } else {
      this.resumePayment(this.responseOfPayment);
    }
  }

  public completeCheckoutNormal = () => {
    this.completeCheckout();
  }

  public completeCheckoutEarly = () => {
    this.completeCheckout();
  }

  public completeCheckout = () => {
    this.checkoutEnabled = true;
    let closeAccountsToCheckout = false;
    let openAccounts = 0;
    this.billingAccountResource.getAllSidebarByBillingAccountId(this.billingAccountCurrentId).subscribe(response => {
      if (response) {
        for (const item of response.items) {
          if (!item.isClosed) {
            openAccounts++;
            closeAccountsToCheckout = true;
          }
        }
        this.openAccounts = openAccounts;
        if (closeAccountsToCheckout) {
          this.modalEmitToggleService.closeAllModals();

          this.isIndividualStatement = false;
          this.getStatementUhTotal();
          this.getStatement();
          this.getBillingAccountForClosureList(true);
        } else {
          this.checkoutResource.chekcoutAcomodation(this.propertyId, this.reservationItemId).subscribe(
            () => {
              this.resumePayment(this.responseOfPayment);
            },
            () => {
              this
                .toasterEmitService
                .emitChange(
                  SuccessError.error,
                  this.translateService.instant('label.messageErrorCheckout')
                );
              this.resumePayment(this.responseOfPayment);
            },
          );
        }
      }
    });
  }

  public closeCheckoutModal = () => {
    if (this.isIndividualStatement) {
      this.goToNextBillingAccount = false;
    }
    this.resumePayment(this.responseOfPayment);
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_CHECKOUT);
  }

  public closeEarlyCheckoutModal = () => {
    this.resumePayment(this.responseOfPayment);
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_EARLY_CHECKOUT);
  }

  private setVisibilityPaymentButton() {
    const openAccountsQtd = this.statementList.filter( item => item.statusId == BillingAccountStatusEnum.Open).length;
    this.buttonPayConfig.isDisabled = !this.statementList || openAccountsQtd <= 0;
    if (!this.statementList || openAccountsQtd <= 0) {
       this.topButtonsListConfig = this.topButtonsListConfig.splice(0, 1);
    }
  }

  populateComponent(component: ComponentRef<any>) {
    (<StatementThermalPrinterComponent>component.instance).statementList = this.statementList;
    (<StatementThermalPrinterComponent>component.instance).propertyId = this.propertyId;
  }

  private printRps(invoiceList: Array<BillingAccountClosureInvoice> = null) {
    if (invoiceList && invoiceList.length > 0 && (invoiceList.filter(
      i => i.type == BillingInvoiceTypeEnum.NFCE || i.type == BillingInvoiceTypeEnum.Factura
    ).length > 0)) {
      this.printFiscalDocument(invoiceList);
    }

    if (!this.invoiceForPrintList || (this.invoiceForPrintList && this.invoiceForPrintList.length == 0)) {
      this.invoiceForPrintList = invoiceList.filter(i => i.type == BillingInvoiceTypeEnum.NFSE);
    }

    if (this.invoiceForPrintList && this.invoiceForPrintList.length > 0) {
      this.checkoutResource.getInvoiceInfo(this.propertyId, this.invoiceForPrintList[0].invoiceId, moment.locale()).subscribe(result => {
        this.invoiceForPrintList.shift();
        this.printService.printComponent(PrintRpsComponent, <PrintComunicator>{
          populateComponent(component) {
            (<PrintRpsComponent>component.instance).rpsPrint = result;
          },
        });
      });
    }
  }

  private printFiscalDocument(invoiceList: Array<BillingAccountClosureInvoice>) {
    this.keepPrintIframe = true;
    const invoiceItem = invoiceList.find(i =>
      i.type == BillingInvoiceTypeEnum.NFCE || i.type == BillingInvoiceTypeEnum.Factura
    );

    if ( invoiceItem ) {
      this.fiscalDocumentService.getNfse(invoiceItem.invoiceId).subscribe(
        (response: any) => {
          const printIframe = document.createElement('iframe');
          const blobUrl = URL.createObjectURL(response);
          // TODO: insert inside html and get the reference by ViewChild
          // TODO: css iframe display none
          printIframe.style.display = 'none';
          printIframe.src = blobUrl;
          document.body.appendChild(printIframe);
          printIframe.contentWindow.print();
          this.keepPrintIframe = false;
          this.redirectCompletePayment();
          printIframe.contentWindow.onafterprint = () => {
            document.body.removeChild(printIframe);
          };
        }, () => {
          this.toasterEmitService.emitChange(
            SuccessError.error,
            this.translateService.instant('alert.errorPrintInvoice')
          );
          this.keepPrintIframe = false;
          this.redirectToBillingAccountPage();
        });
    }
  }

  @HostListener('window:afterprint', ['$event'])
  private redirectCompletePayment() {
      if (this.invoiceForPrintList && this.invoiceForPrintList.length > 0) {
        this.printRps();
      } else if ((!this.invoiceForPrintList && !this.keepPrintIframe) ||
        ((this.invoiceForPrintList && this.invoiceForPrintList.length == 0) && !this.keepPrintIframe)) {
        this.completePayment();
      }
  }
}
