import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { BillingAccountWithEntry } from '../../../shared/models/billing-account/billing-account-with-entry';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';

@Component({
  selector: 'statement-account-table',
  templateUrl: 'statement-account-table.component.html',
})
export class StatementAccountTableComponent implements OnChanges, OnInit {
  @Input() itemStatementList: Array<BillingAccountWithEntry>;
  @Input() hasSubtotal: boolean;
  @Input() propertyId: number;

  public currencySymbol: string;
  constructor( private currencyService: CurrencyService) { }

  ngOnChanges(changes) {
    if (changes.itemStatementList && changes.itemStatementList.currentValue) {
      this.itemStatementList = changes.itemStatementList.currentValue;
    }
  }

  ngOnInit(): void {
    this.currencySymbol = this.currencyService.getDefaultCurrencySymbol(this.propertyId);
  }
}
