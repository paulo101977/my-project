import { Component, Input, OnInit } from '@angular/core';
import { BillingAccountEntriesComplete } from '../../../shared/models/billing-account/billing-account-entries-complete';

@Component({
  selector: 'statement-footer-total',
  templateUrl: 'statement-footer-total.component.html',
})
export class StatementTotalFooterComponent {
  @Input() statementTotal: BillingAccountEntriesComplete;
  @Input() propertyId: number;
  @Input() currencySymbol: string;
}
