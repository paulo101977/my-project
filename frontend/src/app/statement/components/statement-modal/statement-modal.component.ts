import { Component, Input } from '@angular/core';
import { BillingAccountWithEntry } from '../../../shared/models/billing-account/billing-account-with-entry';

@Component({
  selector: 'statement-modal',
  templateUrl: 'statement-modal.component.html',
  styleUrls: ['statement-modal.component.css'],
})
export class StatementModalComponent {
  @Input() statementList: Array<BillingAccountWithEntry>;
  @Input() total: number;
}
