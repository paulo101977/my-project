import { Component, Input } from '@angular/core';
import { BillingAccountWithAccount } from '../../../shared/models/billing-account/billing-account-with-account';
import { BillingAccountTypeEnum } from '../../../shared/models/billing-account/billing-account-type-enum';
import { BillingAccountStatusEnum } from '../../../shared/models/billing-account/billing-account-status-enum';

@Component({
  selector: 'statement-template',
  templateUrl: 'statement-template.component.html',
})
export class StatementTemplateComponent {
  @Input() statement: BillingAccountWithAccount;
  @Input() isIndividualStatement: boolean;
  @Input() showsIfOpen = false;
  @Input() propertyId: number;
  @Input() currencySymbol: string;

  public referenceToBillingAccountTypeEnum = BillingAccountTypeEnum;
  public referenceToBillingAccountStatusEnum = BillingAccountStatusEnum;

  public canShow() {
    return (this.showsIfOpen && this.statement.statusId == BillingAccountStatusEnum.Open) || !this.showsIfOpen;
  }
}
