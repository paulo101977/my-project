import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { BillingAccountItemEntry } from '../../../shared/models/billing-account/billing-account-item-entry';
import { BillingAccountItemTypeEnum } from '../../../shared/models/billing-account/billing-account-item-type-enum';
import { BillingAccountItemStatement } from '../../../shared/models/billing-account/billing-account-item-statement';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';

@Component({
  selector: 'statement-table',
  templateUrl: 'statement-table.component.html',
})
export class StatementTableComponent implements OnChanges, OnInit {
  @Input() billingAccountItems: Array<BillingAccountItemEntry>;
  @Input() propertyId: number;

  public currencySymbol: any;
  constructor( private currencyService: CurrencyService ) {}

  ngOnInit() {
    this.currencySymbol = this.currencyService.getDefaultCurrencySymbol(this.propertyId);
  }

  ngOnChanges(changes) {
    if (changes.billingAccountItems && changes.billingAccountItems.currentValue) {
      this.onBillingAccountItemsChange(changes.billingAccountItems.currentValue);
    }
  }

  private onBillingAccountItemsChange(billingAccountItems) {
    const accountItemList = this.mapperCreditToShowCategoryName(billingAccountItems);
    if (!accountItemList.length) {
      return;
    }

    this.billingAccountItems = accountItemList.reduce((list, accountItem) => {
      const { billingAccountChildrenItems } = accountItem;

      return [
        ...list,
        ...(billingAccountChildrenItems ? billingAccountChildrenItems : [accountItem])
      ];
    }, []);
  }

  public getRowClass(billingAccountItemStatement: BillingAccountItemStatement) {
    return {
      'thf-u-text-decoration--line-through': billingAccountItemStatement.billingAccountItemTypeId == BillingAccountItemTypeEnum.Reverse,
      'thf-u-background-color--grey-lighter': billingAccountItemStatement.billingAccountItemTypeId == BillingAccountItemTypeEnum.Reverse,
    };
  }

  private mapperCreditToShowCategoryName(items: BillingAccountItemEntry[]) {
    return items.map(item => {
      if (item.billingAccountItemTypeId == BillingAccountItemTypeEnum.Credit) {
        item.billingItemName = item.billingItemCategoryName;
      }
      return item;
    });
  }
}
