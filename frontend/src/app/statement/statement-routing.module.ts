import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatementDetailComponent } from './component-container/statement-detail/statement-detail.component';

export const statementRoutes: Routes = [{ path: '', component: StatementDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(statementRoutes)],
  exports: [RouterModule],
})
export class StatementRoutingModule {}
