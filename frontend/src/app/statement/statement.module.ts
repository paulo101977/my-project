import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { StatementRoutingModule } from './statement-routing.module';
import { BillingAccountModule } from 'app/billing-account/billing-account.module';
import { StatementDetailComponent } from './component-container/statement-detail/statement-detail.component';
import { StatementTableComponent } from './components/statement-table/statement-table.component';
import { StatementAccountTableComponent } from './components/statement-account-table/statement-account-table.component';
import { StatementModalComponent } from './components/statement-modal/statement-modal.component';
import { StatementTemplateComponent } from './components/statement-template/statement-template.component';
import { StatementTotalFooterComponent } from './components/statement-footer-total/statement-total-footer.component';
import { CommonModule } from '@angular/common';
import { StatementThermalPrinterComponent } from '../shared/components/statement-thermal-printer/statement-thermal-printer.component';

@NgModule({
  imports: [CommonModule, RouterModule, SharedModule, StatementRoutingModule, BillingAccountModule],
  declarations: [
    StatementDetailComponent,
    StatementTableComponent,
    StatementAccountTableComponent,
    StatementModalComponent,
    StatementTemplateComponent,
    StatementTotalFooterComponent,
  ],
  exports: [
    StatementDetailComponent,
    StatementTableComponent,
    StatementAccountTableComponent,
    StatementModalComponent,
    StatementTemplateComponent,
    StatementTotalFooterComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class StatementModule {}
