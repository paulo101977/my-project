import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { PropertyTokenInterceptorService } from '@inovacaocmnet/thx-bifrost';
import { ApiCultureInterceptor } from 'app/core/interceptors/api-culture.interceptor';
import { ErrorHandlerInterceptor } from 'app/core/interceptors/error-handler.interceptor';
import { LoaderInterceptor } from 'app/core/interceptors/loader.interceptor';

export function addInterceptor(interceptor: any) {
  return {
    provide: HTTP_INTERCEPTORS,
    useClass: interceptor,
    multi: true
  };
}



export const httpInterceptorProviders = [
  // TODO: não remover enquanto não substituir todas as chamadas de API
  // addInterceptor(ApiPrefixInterceptor),
  addInterceptor(LoaderInterceptor),
  addInterceptor(PropertyTokenInterceptorService),
  addInterceptor(ErrorHandlerInterceptor),
  addInterceptor(ApiCultureInterceptor),
];
