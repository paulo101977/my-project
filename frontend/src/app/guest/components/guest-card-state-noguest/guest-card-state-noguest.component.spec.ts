import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GuestCardStateNoguestComponent } from './guest-card-state-noguest.component';
import { GuestCardRoomlist } from '../../../shared/models/guest-card-roomlist';
import { RouterTestingModule } from '@angular/router/testing';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';
import { GuestState } from 'app/shared/models/reserves/guest';

describe('GuestCardStateNoguestComponent', () => {
  let component: GuestCardStateNoguestComponent;
  let fixture: ComponentFixture<GuestCardStateNoguestComponent>;
  const formBuilder = new FormBuilder();

  beforeAll(() => {
    TestBed.resetTestEnvironment();


    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [GuestCardStateNoguestComponent],
      providers: [ InterceptorHandlerService ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(GuestCardStateNoguestComponent);
    component = fixture.componentInstance;

    component.guest = new GuestCardRoomlist();
    component.guest.isChild = true;
    component.guest.state = GuestState.NoGuest;
    component.guest.lastState = GuestState.CreatingGuest;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on init', () => {
    it('should change status guestCard to GuestCreating after editGuest', () => {
      spyOn(component['guestService'], 'setEditGuestForm');

      component.editGuest();

      expect(component['guestService'].setEditGuestForm).toHaveBeenCalledWith(component.guest);
    });

    it('should return isChild from guestCard', () => {
      spyOn(component['guestService'], 'isChild').and.returnValue(true);

      component.isChild();

      expect(component['guestService'].isChild).toHaveBeenCalledWith(component.guest);
    });
  });
});
