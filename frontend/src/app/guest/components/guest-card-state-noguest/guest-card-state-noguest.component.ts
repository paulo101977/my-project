import { Component, Input } from '@angular/core';
import { GuestCardRoomlist } from '../../../shared/models/guest-card-roomlist';
import { GuestService } from '../../../shared/services/guest/guest.service';

@Component({
  selector: 'app-guest-card-state-noguest',
  templateUrl: './guest-card-state-noguest.component.html',
})
export class GuestCardStateNoguestComponent {
  @Input() guest: GuestCardRoomlist;

  constructor(private guestService: GuestService) {}

  public isChild(): boolean {
    return this.guestService.isChild(this.guest);
  }

  public editGuest() {
    this.guestService.setEditGuestForm(this.guest);
  }
}
