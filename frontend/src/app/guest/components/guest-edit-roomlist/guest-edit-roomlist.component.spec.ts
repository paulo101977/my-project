import { async, ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { FormArray, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MomentModule } from 'ngx-moment';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { GuestEditRoomlistComponent } from './guest-edit-roomlist.component';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { RoomListResultDto } from 'app/shared/models/dto/room-list-result-dto';
import { ReservationData, ReservationItemGuestViewData } from 'app/shared/mock-data/reservation-data';
import { ReservationDtoData, ReservationItemDtoData } from 'app/shared/mock-data/reservation-dto-data';
import { FilterListPipe } from 'app/shared/pipes/filter-select.pipe';
import { SharedModule } from 'app/shared/shared.module';
import { HttpService } from 'app/core/services/http/http.service';

xdescribe('GuestEditRoomlistComponent', () => {
  let component: GuestEditRoomlistComponent;
  let fixture: ComponentFixture<GuestEditRoomlistComponent>;
  const formBuilder = new FormBuilder();

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MomentModule,
        SharedModule,
      ],
      declarations: [GuestEditRoomlistComponent, FilterListPipe],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(GuestEditRoomlistComponent);
    component = fixture.componentInstance;

    spyOn(component, 'loadRoomListFromReserve');

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should onInit call setVars', () => {
      spyOn(component, 'setConfigHeaderPage');
      spyOn(component, 'initFormRoomList');

      component['setVars']();

      expect(component['propertyId']).toEqual(1);
      expect(component.filterParameters).toEqual(['code', 'searchableNames']);
      expect(component.elementParentFixedTop).toEqual(0);
      expect(component.isFixed).toEqual(false);
      expect(component.setConfigHeaderPage).toHaveBeenCalled();
      expect(component.initFormRoomList).toHaveBeenCalled();
    });

    it('should initFormRoomList', () => {
      component.initFormRoomList();
      expect(component.editGuestRoomListAccommodationForm.contains('reservationItems')).toBeTruthy();
    });
  });

  describe('on load backend', () => {
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
          TranslateModule.forRoot(),
          RouterTestingModule,
          FormsModule,
          ReactiveFormsModule,
          HttpClientModule,
          HttpClientTestingModule,
          MomentModule,
          SharedModule,
        ],
        declarations: [GuestEditRoomlistComponent, FilterListPipe],
        providers: [],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();

      fixture = TestBed.createComponent(GuestEditRoomlistComponent);
      component = fixture.componentInstance;

      spyOn<any>(component._route.params, 'subscribe').and.callFake(function(success) {
        success({ reservationId: '1', property: '1' });
      });

      fixture.detectChanges();
    }));

    it(
      'should loadRoomListFromReserve',
      fakeAsync(
        inject([HttpClient, HttpTestingController], (http: HttpService, backend: HttpTestingController) => {
          spyOn(component['loadPageEmitService'], 'emitChange');
          spyOn<any>(component, 'setReservationItemsToForm');
          spyOn<any>(component, 'setFirstReservationItem');

          const roomListResultDto = new RoomListResultDto();
          roomListResultDto.id = 1;

          const roomListResultDtoList = new Array<RoomListResultDto>();
          roomListResultDtoList.push(roomListResultDto);
          roomListResultDtoList.push(roomListResultDto);
          roomListResultDtoList.push(roomListResultDto);

          backend.expectOne(JSON.stringify({ items: roomListResultDtoList }));

          component.loadRoomListFromReserve(component.reservationId);
          tick();

          expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.show);
        }),
      ),
    );

    it(
      'should loadGuestsFromReservationItem',
      fakeAsync(() => {
        spyOn<any>(component['reservationResource'], 'getReservationItemWithGuestsByReservationId').and.returnValue(
          Promise.resolve(ReservationDtoData),
        );
        spyOn(component['loadPageEmitService'], 'emitChange');
        spyOn(component, 'getReservationItemInReservation').and.returnValue(ReservationItemGuestViewData);

        const id = 1;

        component.loadGuestsFromReservationItem(id);
        tick();

        expect(component['reservationResource'].getReservationItemWithGuestsByReservationId).toHaveBeenCalledWith(id);
        expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
        expect(component.getReservationItemInReservation).toHaveBeenCalledWith(ReservationDtoData, id);
      }),
    );
  });

  describe('on guest form', () => {
    it(
      'should chooseReservationItem',
      fakeAsync(() => {
        spyOn<any>(component, 'loadGuestsFromReservationItem')
            .and.returnValue(Promise.resolve(ReservationItemGuestViewData));
        spyOn(component, 'markInBlackReservationItemSelected');
        spyOn<any>(component, 'validateGuestsOfReservationItem');

        spyOn(component['guestService'], 'addGuestsToreservationItemForm');
        spyOn<any>(component, 'getReservationItemInReservation').and.returnValue(ReservationItemDtoData);

        const form = formBuilder.group({
          id: 1,
        });

        component.chooseReservationItem(form);
        tick();

        expect(component.loadGuestsFromReservationItem).toHaveBeenCalledWith(form.get('id').value);
        expect(component.markInBlackReservationItemSelected).toHaveBeenCalledWith(form.get('id').value);
        expect(component['validateGuestsOfReservationItem']).toHaveBeenCalledWith(form);
      }),
    );

    it('should return Room from Guest', () => {
      spyOn(component['guestService'], 'getRoomNumberFromReservationItemGuestView').and.returnValue('1000');

      const reservationItemForm = formBuilder.group({});

      component.getRoomNumber(reservationItemForm);

      expect(component['guestService'].getRoomNumberFromReservationItemGuestView).toHaveBeenCalledWith(reservationItemForm.value);
      expect(component.getRoomNumber(reservationItemForm)).toEqual('1000');
    });

    it('should return empty Room from Guest', () => {
      spyOn(component['guestService'], 'getRoomNumberFromReservationItemGuestView').and.returnValue('');

      const reservationItemForm = formBuilder.group({});

      component.getRoomNumber(reservationItemForm);

      expect(component['guestService'].getRoomNumberFromReservationItemGuestView).toHaveBeenCalledWith(reservationItemForm.value);
      expect(component.getRoomNumber(reservationItemForm)).toEqual('commomData.room');
    });

    it('should return RoomType from Guest', () => {
      spyOn(component['guestService'], 'getRoomTypeFromReservationItemGuestView').and.returnValue('XPTO');

      const reservationItemForm = formBuilder.group({});

      component.getRoomType(reservationItemForm);

      expect(component['guestService'].getRoomTypeFromReservationItemGuestView).toHaveBeenCalledWith(reservationItemForm.value);
      expect(component.getRoomType(reservationItemForm)).toEqual('XPTO');
    });

    it('should return empty RoomType from Guest', () => {
      spyOn(component['guestService'], 'getRoomTypeFromReservationItemGuestView').and.returnValue('');

      const reservationItemForm = formBuilder.group({});

      component.getRoomType(reservationItemForm);

      expect(component['guestService'].getRoomTypeFromReservationItemGuestView).toHaveBeenCalledWith(reservationItemForm.value);
      expect(component.getRoomType(reservationItemForm)).toEqual('');
    });

    it('should call roomListFixed', () => {
      spyOn(component, 'roomListFixed');

      component.roomListFixed();
      expect(component.roomListFixed).toHaveBeenCalled();
    });

    it('should getReservationItemInReservation', () => {
      expect(component.getReservationItemInReservation(ReservationData, ReservationItemGuestViewData.id)).toEqual(
        ReservationItemGuestViewData,
      );
    });

    it('should hasGuestsInReservationItem', () => {
      expect(component.hasGuests).toBeTruthy();
    });

    it('should validate guests and set reservationItem to valid', () => {
      const reservationItemForm = formBuilder.group({
        isValid: false,
      });

      const guest = formBuilder.group({
        guestName: '',
        isChild: false,
        childAge: 0,
      });

      component.guestsFromReservationItem = formBuilder.group({
        guests: formBuilder.array([guest]),
      });

      component['validateGuestsOfReservationItem'](reservationItemForm);

      guest.get('guestName').setValue('Caio');

      expect(reservationItemForm.get('isValid').value).toBeTruthy();
    });

    it('should setFirstReservationItem', () => {
      spyOn(component, 'chooseReservationItem');

      const reservationItem = formBuilder.group({});
      const array = <FormArray>component.editGuestRoomListAccommodationForm.get('reservationItems');
      array.push(reservationItem);

      component['setFirstReservationItem']();

      expect(component.chooseReservationItem).toHaveBeenCalledWith(reservationItem);
    });
  });
});
