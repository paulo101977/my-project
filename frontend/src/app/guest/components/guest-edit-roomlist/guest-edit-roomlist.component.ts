import { map } from 'rxjs/operators';
import { AfterContentInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ReservationItemGuestView } from '../../../shared/models/reserves/reservation-item-guest-view';
import { GuestService } from '../../../shared/services/guest/guest.service';
import { ReservationResource } from '../../../shared/resources/reservation/reservation.resource';
import { Reservation } from '../../../shared/models/reserves/reservation';
import { Observable } from 'rxjs';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-guest-edit-roomlist',
  templateUrl: './guest-edit-roomlist.component.html',
  styleUrls: ['./guest-edit-roomlist.component.css'],
})
export class GuestEditRoomlistComponent implements OnInit, AfterContentInit {
  @ViewChild('roomList') roomList;
  @ViewChild('roomListContainer') roomListContainer;

  public configHeaderPage: ConfigHeaderPageNew;
  public reservationId: number;
  public paramSub: any;
  public guestsFromReservationItem: FormGroup;
  public editGuestRoomListAccommodationForm: FormGroup;
  public isFixed: boolean;
  public elementParentFixedTop: number;
  private propertyId: number;
  public reservationItemGuestViewQuantity = 0;
  public hasGuests: boolean;
  public filter: string;
  public widthRoomListItems: string;

  public filterParameters: Array<any>;

  constructor(
    public _route: ActivatedRoute,
    private reservationResource: ReservationResource,
    private guestService: GuestService,
    private formBuilder: FormBuilder,
    public translateService: TranslateService,
    private propertyService: PropertyStorageService
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars(): void {
    this.filterParameters = ['code', 'searchableNames'];
    this.elementParentFixedTop = 0;
    this.isFixed = false;
    this.propertyId = this.propertyService.getCurrentPropertyId();
    this.paramSub = this._route.params.subscribe(params => {
      this.reservationId = +params['reservationId'];
      this.setConfigHeaderPage();
      this.initFormRoomList();
      this.loadRoomListFromReserve(this.reservationId);
    });
  }

  public ngAfterContentInit() {
    this.widthRoomListItems = `${this.roomList.nativeElement.clientWidth + 2}px`;
  }

  public setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  public initFormRoomList(): void {
    this.editGuestRoomListAccommodationForm = this.formBuilder.group({
      reservationItems: this.formBuilder.array([]),
    });
  }

  public loadRoomListFromReserve(reserveId: number): void {
    this.reservationResource.getReservationItemsByReservationIdAndPropertyId(reserveId, this.propertyId).subscribe(response => {
      const reservationItemGuestViewList = response;
      if (reservationItemGuestViewList) {
        this.reservationItemGuestViewQuantity = reservationItemGuestViewList.length;
        this.setReservationItemsToForm(reservationItemGuestViewList);
        this.setFirstReservationItem();
      }
    });
  }

  private setReservationItemsToForm(reservationItemGuestViewList: Array<ReservationItemGuestView>): void {
    reservationItemGuestViewList.forEach((reservationItemGuestView, index) => {
      this.guestService.addReservationItemToForm(this.editGuestRoomListAccommodationForm, reservationItemGuestView);
    });
    const formToUse = this.getReservationItems(this.editGuestRoomListAccommodationForm)[0];
    this.chooseReservationItem(formToUse);
  }

  public loadGuestsFromReservationItem(reservationItemId: number): Observable<ReservationItemGuestView> {
    this.hasGuests = false;
    return this.reservationResource.getReservationItemWithGuestsByReservationId(reservationItemId).pipe(map(reservation => {
      return this.getReservationItemInReservation(reservation, reservationItemId);
    }));
  }

  public getReservationItemInReservation(reservation: Reservation, reservationItemId: number): ReservationItemGuestView {
    return reservation.reservationItemGuestViewList.find(r => r.id == reservationItemId);
  }

  public chooseReservationItem(reservationItemForm: FormGroup): void {
    if (reservationItemForm && reservationItemForm.get('id').value) {
      this.loadGuestsFromReservationItem(reservationItemForm.get('id').value).subscribe(reservationItem => {
        this.guestsFromReservationItem = this.guestService.initReservationItem(reservationItem);
        reservationItem.guestReservationItemList.forEach(guest => {
          this.guestService.addGuestsToreservationItemForm(this.guestsFromReservationItem, guest);
        });
        this.hasGuests = !!this.guestsFromReservationItem;
      });
      this.validateGuestsOfReservationItem(reservationItemForm);
      this.markInBlackReservationItemSelected(reservationItemForm.get('id').value);
    }
  }

  private setFirstReservationItem() {
    const reservationItemArray = <FormArray>this.editGuestRoomListAccommodationForm.get('reservationItems');
    this.chooseReservationItem(<FormGroup>reservationItemArray.at(0));
  }

  public markInBlackReservationItemSelected(reservationItemId: number) {
    const reservationItemArray = <FormArray>this.editGuestRoomListAccommodationForm.get('reservationItems');

    reservationItemArray.controls.forEach(reservationItemForm => {
      reservationItemForm.get('active').setValue(false);
      if (reservationItemForm.get('id').value === reservationItemId) {
        reservationItemForm.get('active').setValue(true);
      }
    });
  }

  private validateGuestsOfReservationItem(reservationItem: FormGroup) {
    if (this.guestsFromReservationItem) {
      this.guestsFromReservationItem.valueChanges.subscribe(changes => {
        let valid = true;
        changes['guests'].forEach(guest => {
          if (!this.guestIsValid(guest)) {
            valid = false;
          }
        });
        reservationItem.get('isValid').setValue(valid);
      });
    }
  }

  private guestIsValid(guest: any): boolean {
    if (!guest['guestName'] || guest['guestName'] == '' || (guest['isChild'] && guest['childAge'] <= 0)) {
      return false;
    }
    return true;
  }

  public roomListFixed(): void {
    this.elementParentFixedTop = this.roomListContainer.nativeElement.getBoundingClientRect().top;

    if (this.elementParentFixedTop <= 20) {
      this.isFixed = true;
    } else if (this.elementParentFixedTop >= 21) {
      this.isFixed = false;
    }
  }

  public getReservationItems(form) {
    return form.get('reservationItems').controls;
  }

  public getRoomNumber(reservationItem: FormGroup) {
    const roomNumber = this.guestService.getRoomNumberFromReservationItemGuestView(reservationItem.value);
    if (roomNumber) {
      return roomNumber;
    }
    return this.translateService.instant('commomData.room');
  }

  public getRoomType(reservationItem: FormGroup) {
    return this.guestService.getRoomTypeFromReservationItemGuestView(reservationItem.value);
  }
}
