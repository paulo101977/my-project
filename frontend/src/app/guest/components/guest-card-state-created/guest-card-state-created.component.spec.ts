/*
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { Http, BaseRequestOptions, ResponseOptions, Response } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { MomentModule } from 'ngx-moment';
import { GuestCardStateCreatedComponent } from './guest-card-state-created.component';
import { DocumentForLegalAndNaturalPerson } from '../../../shared/models/document';
import { DocumentType } from '../../../shared/models/document-type';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { PropertyGuestTypeDtoDataList } from 'app/shared/mock-data/guest-type-data';
import { GuestCardRoomlist } from '../../../shared/models/guest-card-roomlist';
import { GuestService } from '../../../shared/services/guest/guest.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { DateService } from '../../../shared/services/shared/date.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { PersonResource } from '../../../shared/resources/person/person.resource';
import { ReservationResource } from '../../../shared/resources/reservation/reservation.resource';

describe('GuestCardStateCreatedComponent', () => {
  let component: GuestCardStateCreatedComponent;
  let fixture: ComponentFixture<GuestCardStateCreatedComponent>;
  const formBuilder = new FormBuilder();
  let mockBackend: MockBackend;
  let resourceMock: ReservationResource;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed
      .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: [
          TranslateModule.forRoot(),
          FormsModule,
          ReactiveFormsModule,

          HttpClientModule,
          MomentModule
        ],
        declarations: [GuestCardStateCreatedComponent],
        providers: [],
        schemas: [NO_ERRORS_SCHEMA]
      });

    fixture = TestBed.createComponent(GuestCardStateCreatedComponent);
    component = fixture.componentInstance;
    component.guest = new GuestCardRoomlist();
    component.guest.guestReservationItemId = 1;
    component.dataInput = {};


    fixture.detectChanges();
    mockBackend = TestBed.get(MockBackend);
    resourceMock = TestBed.get(ReservationResource);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on init', () => {
    it('should change status guestCard to GuestCreating after editGuest', () => {
      spyOn(component['guestService'], 'setEditGuestForm');

      component.editGuest();

      expect(component['guestService'].setEditGuestForm).toHaveBeenCalledWith(component.guest);
    });

    it('should clean guestForm', fakeAsync(() => {
      spyOn(component['guestService'], 'setCleanGuestForm');
      spyOn(component['loadPageEmitService'], 'emitChange');
      spyOn(component['toasterEmitService'], 'emitChange');

      mockBackend.connections.subscribe((connection: MockConnection) => {
        connection.mockRespond(new Response(<ResponseOptions>{
          body: null
        }));
      });

      component.guestToBeClean = component.guest;

      component.cleanGuest();
      tick();

      resourceMock.disassociateGuestFromReservationItem(component.guestToBeClean.guestReservationItemId).then(
        resp => {
          expect(component['guestService'].setCleanGuestForm).toHaveBeenCalledWith(component.guestToBeClean);
          expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
          expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
            SuccessError.success,
            'guestModule.edit.disassociateGuestFromReservationItemSuccessMessage',
          );
        }
      );

      expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.show);
    }));

    it('should show Modal asking to clean guestForm', () => {
      spyOn(component['modalEmitService'], 'emitSimpleModal');

      component.askCleanGuest();

      expect(component.guestToBeClean).toEqual(component.guest);
      expect(component['modalEmitService'].emitSimpleModal).toHaveBeenCalledWith(
        component.translateService.instant('commomData.clean'),
        component.translateService.instant('guestModule.edit.confirmCleanGuest?'),
        jasmine.any(Function),
        []
      );
    });

    it('should return name of document type from Guest', () => {
      const documentType = new DocumentType();
      documentType.name = 'CPF';
      const document = new DocumentForLegalAndNaturalPerson();
      document.documentType = documentType;

      component.guest.guestDocument = document;

      expect(component.getDocumentTypeName()).toEqual('CPF');
    });

    it('should return document information from Guest', () => {
      const document = new DocumentForLegalAndNaturalPerson();
      document.documentInformation = '999.999.999-99';

      component.guest.guestDocument = document;

      expect(component.getDocumentInformation().documentInformation).toEqual('999.999.999-99');
    });

    it('should return Guest Type Name from Guest', () => {
      component.dataInput.guestTypes = PropertyGuestTypeDtoDataList;

      component.guest.type = 1;

      expect(component.getGuestTypeName()).toEqual('NORMAL');
    });

  });
});
*/
