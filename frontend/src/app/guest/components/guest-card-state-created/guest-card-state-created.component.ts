import { Component, Input, OnInit } from '@angular/core';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ReservationStatusEnum } from '../../../shared/models/reserves/reservation-status-enum';
import { GuestService } from '../../../shared/services/guest/guest.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { ReservationResource } from '../../../shared/resources/reservation/reservation.resource';
import { GuestCardRoomlist } from '../../../shared/models/guest-card-roomlist';
import { CurrencyService } from '../../../shared/services/shared/currency-service.service';

@Component({
  selector: 'app-guest-card-state-created',
  templateUrl: './guest-card-state-created.component.html',
})
export class GuestCardStateCreatedComponent implements OnInit {
  @Input() guest: GuestCardRoomlist;
  @Input() dataInput;
  public guestToBeClean: GuestCardRoomlist;
  public currencyCountry: string;
  public reservationStatusEnum = ReservationStatusEnum;

  constructor(
    private guestService: GuestService,
    public translateService: TranslateService,
    public modalEmitService: ModalEmitService,
    private reservationResource: ReservationResource,
    private toasterEmitService: ToasterEmitService,
    public currencyService: CurrencyService
  ) {}

  ngOnInit() {
    this.currencyCountry = window.navigator.language;
  }

  public getDocumentTypeName(): string {
    return this.guest.guestDocument && this.guest.guestDocument.documentType ? this.guest.guestDocument.documentType.name : '';
  }

  public getDocumentInformation() {
    return this.guest.guestDocument;
  }

  public editGuest() {
    this.guestService.setEditGuestForm(this.guest);
  }

  public askCleanGuest() {
    this.guestToBeClean = this.guest;

    this.modalEmitService.emitSimpleModal(
      this.translateService.instant('commomData.clean'),
      this.translateService.instant('guestModule.edit.confirmCleanGuest') + '?',
      this.cleanGuest,
      []
    );
  }

  public cleanGuest = () => {

    const guestReservationItemId = this.guestToBeClean.guestReservationItemId;

    if (guestReservationItemId) {
      this.reservationResource.disassociateGuestFromReservationItem(guestReservationItemId).subscribe(result => {
        this.guestService.setCleanGuestForm(this.guestToBeClean);

        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('guestModule.edit.disassociateGuestFromReservationItemSuccessMessage'),
        );
      });
    }
  }

  public toogleShowDetails() {
    this.guestService.toogleShowDetailsForm(this.guest);
  }

  public getGuestTypeName() {
    if (this.dataInput.guestTypes) {
      const guestType = this.dataInput.guestTypes.find(t => t.id == this.guest.type);
      return guestType ? guestType.guestTypeName : '-';
    }
  }
}
