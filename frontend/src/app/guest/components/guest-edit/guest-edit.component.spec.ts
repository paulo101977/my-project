// import { async, ComponentFixture, TestBed, inject, tick, fakeAsync } from '@angular/core/testing';
// import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { TranslateModule, TranslateService } from '@ngx-translate/core';
// import { RouterTestingModule } from '@angular/router/testing';
// import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
// import { Http, BaseRequestOptions, ResponseOptions, Response } from '@angular/http';
// import { MockBackend, MockConnection } from '@angular/http/testing';
// import { HttpClientModule } from '@angular/common/http';
// import { MomentModule } from 'ngx-moment';
// import { GuestEditComponent } from './guest-edit.component';
// import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
// import { ShowHide } from '../../../shared/models/show-hide-enum';
// import { Reservation } from '../../../shared/models/reserves/reservation';
// import { GuestReservationItemDto } from '../../../shared/models/dto/guest-reservation-item-dto';
// import { ReservationItemDto } from '../../../shared/models/dto/reservation-item-dto';
// import { ReservationDto } from '../../../shared/models/dto/reservation-dto';
// import { ReservationDtoData } from '../../../shared/mock-data/reservation-dto-data';
// import { DateService } from '../../../shared/services/shared/date.service';
// import { SharedService } from '../../../shared/services/shared/shared.service';
// import { GuestService } from '../../../shared/services/guest/guest.service';
// import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
// import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
// import { ReservationResource } from '../../../shared/resources/reservation/reservation.resource';
// import { PersonResource } from '../../../shared/resources/person/person.resource';
// import { ActivatedRoute } from '@angular/router';
// import { Observable } from 'rxjs/Observable';
//
//
// describe('GuestEditComponent', () => {
//   let component: GuestEditComponent;
//   let fixture: ComponentFixture<GuestEditComponent>;
//   const formBuilder = new FormBuilder();
//   let mockBackend: MockBackend;
//   let resourceMock: ReservationResource;
//
//   beforeAll(() => {
//     TestBed.resetTestEnvironment();
//
//     TestBed
//       .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
//       .configureTestingModule({
//         imports: [
//           TranslateModule.forRoot(),
//           RouterTestingModule,
//           FormsModule,
//           ReactiveFormsModule,
//
//           HttpClientModule,
//           MomentModule
//         ],
//         declarations: [GuestEditComponent],
//         providers: [],
//         schemas: [CUSTOM_ELEMENTS_SCHEMA]
//       });
//
//     fixture = TestBed.createComponent(GuestEditComponent);
//     component = fixture.componentInstance;
//
//     spyOn(component._route.params, 'subscribe').and.callFake(function (success) {
//       success({ reservationItemId: '1' });
//     });
//
//     fixture.detectChanges();
//     mockBackend = TestBed.get(MockBackend);
//     resourceMock = TestBed.get(ReservationResource);
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   describe('on init', () => {
//     it('should setConfigs', () => {
//       spyOn(component, 'setConfigHeaderPage');
//
//       component.setConfigs();
//
//       expect(component.setConfigHeaderPage).toHaveBeenCalled();
//     });
//
//     it('should setConfigHeaderPage', () => {
//       component.setConfigHeaderPage();
//
//       expect(component.configHeaderPage.titlePage).toEqual('guestModule.edit.title');
//       expect(component.configHeaderPage.headerPageEnum).toEqual(HeaderPageEnum.Create);
//     });
//
//     it('should set editGuestAccommodationForm', () => {
//       expect(Object.keys(component.editGuestsReservationItemForm.controls)).toEqual([
//         'reservationItems'
//       ]);
//     });
//
//   });
//
//   describe('on after init', () => {
//
//     it('should loadReservationItem', fakeAsync(() => {
//       spyOn(component['loadPageEmitService'], 'emitChange');
//       spyOn(component['guestService'], 'addReservationItemToForm');
//
//       const reservationDto = ReservationDtoData;
//
//       mockBackend.connections.subscribe((connection: MockConnection) => {
//         connection.mockRespond(new Response(<ResponseOptions>{
//           body: JSON.stringify(reservationDto),
//         }));
//       });
//
//       component.loadReservationItem(1);
//       tick();
//
//       resourceMock.getReservationItemWithGuestsByReservationId(1).then(
//         resp => {
//           expect(component['guestService'].addReservationItemToForm)
// .toHaveBeenCalledWith(component.editGuestsReservationItemForm, resp.reservationItemGuestViewList[0]);
//           expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
//         }
//       );
//
//       expect(component.guestsFromReservationItem).not.toBeNull();
//       expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.show);
//     }));
//
//     it('should return Room from Guest', () => {
//       spyOn(component['guestService'], 'getRoomNumberFromReservationItemGuestView').and.returnValue('1000');
//       const reservationItemForm = formBuilder.group({});
//       component.getRoomNumber(reservationItemForm);
//
//       expect(component['guestService'].getRoomNumberFromReservationItemGuestView).toHaveBeenCalledWith(reservationItemForm.value);
//       expect(component.getRoomNumber(reservationItemForm)).toEqual('1000');
//     });
//
//     it('should return empty Room from Guest', () => {
//       spyOn(component['guestService'], 'getRoomNumberFromReservationItemGuestView').and.returnValue('');
//       const reservationItemForm = formBuilder.group({});
//       component.getRoomNumber(reservationItemForm);
//
//       expect(component['guestService'].getRoomNumberFromReservationItemGuestView).toHaveBeenCalledWith(reservationItemForm.value);
//       expect(component.getRoomNumber(reservationItemForm)).toEqual('commomData.room');
//     });
//
//     it('should return RoomType from Guest', () => {
//       spyOn(component['guestService'], 'getRoomTypeFromReservationItemGuestView').and.returnValue('XPTO');
//       const reservationItemForm = formBuilder.group({});
//       component.getRoomType(reservationItemForm);
//
//       expect(component['guestService'].getRoomTypeFromReservationItemGuestView).toHaveBeenCalledWith(reservationItemForm.value);
//       expect(component.getRoomType(reservationItemForm)).toEqual('XPTO');
//     });
//
//     it('should return empty RoomType from Guest', () => {
//       spyOn(component['guestService'], 'getRoomTypeFromReservationItemGuestView').and.returnValue('');
//       const reservationItemForm = formBuilder.group({});
//       component.getRoomType(reservationItemForm);
//
//       expect(component['guestService'].getRoomTypeFromReservationItemGuestView).toHaveBeenCalledWith(reservationItemForm.value);
//       expect(component.getRoomType(reservationItemForm)).toEqual('');
//     });
//   });
//
// });
