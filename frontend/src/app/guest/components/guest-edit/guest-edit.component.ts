import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ConfigHeaderPage } from '../../../shared/models/config-header-page';
import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
import { ShowHide } from '../../../shared/models/show-hide-enum';
import { GuestService } from '../../../shared/services/guest/guest.service';
import { ReservationResource } from '../../../shared/resources/reservation/reservation.resource';

@Component({
  selector: 'app-guest-edit',
  templateUrl: './guest-edit.component.html',
  styleUrls: ['./guest-edit.component.css'],
})
export class GuestEditComponent {
  public configHeaderPage: ConfigHeaderPage;
  public reservationItemId: number;
  public paramSub: any;
  public guestsFromReservationItem: FormGroup;
  public editGuestsReservationItemForm: FormGroup;

  constructor(
    public _route: ActivatedRoute,
    private reservationResource: ReservationResource,
    private guestService: GuestService,
    private formBuilder: FormBuilder,
    public translateService: TranslateService,
  ) {
    this.setConfigs();

    this.paramSub = this._route.params.subscribe(params => {
      this.reservationItemId = +params['reservationItemId'];
    });

    this.editGuestsReservationItemForm = this.formBuilder.group({
      reservationItems: this.formBuilder.array([]),
    });

    this.loadReservationItem(this.reservationItemId);
  }

  public setConfigs() {
    this.setConfigHeaderPage();
  }

  public setConfigHeaderPage() {
    this.configHeaderPage = new ConfigHeaderPage(HeaderPageEnum.Create);
    this.configHeaderPage.titlePage = 'guestModule.edit.title';
  }

  public loadReservationItem(reservationItemId: number) {
    this.reservationResource.getReservationItemWithGuestsByReservationId(reservationItemId).subscribe(response => {
      const reservation = response;
      if (reservation && reservation.reservationItemGuestViewList) {
        this.guestService.addReservationItemToForm(this.editGuestsReservationItemForm, reservation.reservationItemGuestViewList[0]);
        const reservationItemArray = this.editGuestsReservationItemForm.get('reservationItems') as FormArray;
        this.guestsFromReservationItem = reservationItemArray.at(0) as FormGroup;
      }
    });
  }

  public getRoomNumber(reservationItem: FormGroup): string {
    if (reservationItem) {
      const roomNumber = this.guestService.getRoomNumberFromReservationItemGuestView(reservationItem.value);
      if (roomNumber) {
        return roomNumber;
      }
    }
    return this.translateService.instant('commomData.room');
  }

  public getRoomType(reservationItem: FormGroup): string {
    if (reservationItem) {
      return this.guestService.getRoomTypeFromReservationItemGuestView(reservationItem.value);
    }
    return '';
  }
}
