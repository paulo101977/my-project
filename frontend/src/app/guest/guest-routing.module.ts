import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GuestEditComponent } from './components/guest-edit/guest-edit.component';
import { GuestEditRoomlistComponent } from './components/guest-edit-roomlist/guest-edit-roomlist.component';

export const guestRoutes: Routes = [
  { path: 'edit/:reservationItemId', component: GuestEditComponent },
  { path: 'edit/room-list/:reservationId', component: GuestEditRoomlistComponent },
  { path: 'fnrh', loadChildren: '../checkin/checkin.module#CheckinModule' },
];

@NgModule({
  imports: [RouterModule.forChild(guestRoutes)],
  exports: [RouterModule],
})
export class GuestRoutingModule {}
