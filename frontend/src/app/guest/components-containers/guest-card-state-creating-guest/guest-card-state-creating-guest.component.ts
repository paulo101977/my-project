import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { SearchPersonsResultDto } from 'app/shared/models/person/search-persons-result-dto';
import { DocumentType } from 'app/shared/models/document-type';
import { GuestReservationItem } from 'app/shared/models/reserves/guest-reservation-item';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';
import { GuestService } from 'app/shared/services/guest/guest.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { PersonResource } from 'app/shared/resources/person/person.resource';
import { ReservationResource } from 'app/shared/resources/reservation/reservation.resource';
import { GuestCardRoomlist } from 'app/shared/models/guest-card-roomlist';
import { NumberRangeValidator } from 'app/shared/validators/number-range-validator';
import { DateService } from 'app/shared/services/shared/date.service';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { AutocompleteConfig } from 'app/shared/models/autocomplete/autocomplete-config';
import { DocumentTypeMaskService } from 'app/shared/services/shared/document-type-mask.service';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { DocumentTypeResource } from 'app/shared/resources/document-type/document-type.resource';
import { GuestState } from 'app/shared/models/reserves/guest';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';

@Component({
  selector: 'app-guest-card-state-creating-guest',
  templateUrl: './guest-card-state-creating-guest.component.html',
})
export class GuestCardStateCreatingGuestComponent implements OnInit {
  @Input() guest: GuestCardRoomlist;
  @Input() dataInput;
  @Input() guestsForm;
  @Input() propertyId;
  @Output() activeForm = new EventEmitter();

  private readonly MAX_AGE = 18;
  public guestFormGroup: FormGroup;
  public optionDocument: DocumentType;
  public possibleAgesOfProperty: Array<any>;
  public currencyCountry: string;

  // AutoComplete Dependencies
  public autoCompleteDocumentConfig: AutocompleteConfig;
  public autoCompleteEmailConfig: AutocompleteConfig;
  public documentOptionSelectList: Array<SelectObjectOption>;

  constructor(
    private guestService: GuestService,
    public translateService: TranslateService,
    private personResource: PersonResource,
    private reservationResource: ReservationResource,
    private toasterEmitService: ToasterEmitService,
    private formBuilder: FormBuilder,
    private dateService: DateService,
    private toasterService: ToasterEmitService,
    private sessionParameter: SessionParameterService,
    private documentMaskService: DocumentTypeMaskService,
    private commonService: CommomService,
    private documentTypeResource: DocumentTypeResource,
    private currencyService: CurrencyService,
  ) {}

  ngOnInit() {
    this.possibleAgesOfProperty = Array
      .from(Array(this.MAX_AGE).keys())
      .map( i => ({key: `${i}`, value: `${i}`}));
    this.setVars();
  }

  private setVars() {
    this.currencyCountry = this.currencyService.getDefaultCurrencySymbol(this.propertyId);
    this.setOptionDocument();
    this.setForm();
    this.getDocumentTypeList();
    this.setAutoCompleteDocumentConfig();
    this.setAutoCompleteEmailConfig();
  }

  private setOptionDocument() {
    this.optionDocument = new DocumentType();
    this.optionDocument.id = +this.guest.guestDocumentTypeId || 1;
  }

  private setForm() {
    const periodOfStay = this.dateService.getDateRangePicker(this.guest.estimatedArrivalDate, this.guest.estimatedDepartureDate);
    const cardState = this.guest.guestName ? GuestState.GuestCreated : GuestState.NoGuest;

    this.guestFormGroup = this.formBuilder.group({
      guestReservationItemId: this.guest.guestReservationItemId,
      reservationItemId: this.guest.reservationItemId,
      guestId: this.guest.guestId,
      personId: this.guest.personId,
      guestName: [this.guest.guestName, [Validators.required]],
      preferredName: this.guest.preferredName,
      childAge: [this.guest.childAge],
      guestEmail: this.guest.guestEmail,
      type: [this.guest.type, [Validators.required]],
      isIncognito: this.guest.isIncognito,
      isChild: this.guest.isChild,
      isMain: this.guest.isMain,
      guestCitizenship: this.guest.guestCitizenship,
      language: this.guest.language,
      uHPreferably: '',
      lastUH: '',
      lastDailyValue: 0,
      periodOfStay: periodOfStay,
      checkinDate: [this.guest.checkinDate],
      checkoutDate: [this.guest.checkoutDate],
      pctDailyRate: [this.guest.pctDailyRate, [Validators.required, NumberRangeValidator.range(0, 100)]],
      remarks: '',
      state: cardState,
      lastState: cardState,
      guestStatusId: this.guest.guestStatusId,
      showDetails: false,
      guestDocument: [this.guest.guestDocument],
      guestDocumentTypeId: this.optionDocument.id,
      estimatedArrivalDate: [this.guest.estimatedArrivalDate, [Validators.required]],
      estimatedDepartureDate: [this.guest.estimatedDepartureDate, [Validators.required]],
      completeAddress: this.guest.completeAddress,
    });

    this
      .guestFormGroup
      .get('guestDocumentTypeId')
      .valueChanges
      .subscribe(value => {
        this.optionDocument.id = value;
        this.apllyMaskToDocument(this.guestFormGroup.get('guestDocument').value);
    });

    this.setValidators();
  }

  private setAutoCompleteDocumentConfig() {
    this.autoCompleteDocumentConfig = new AutocompleteConfig();
    this.autoCompleteDocumentConfig.dataModel = 'document';
    this.autoCompleteDocumentConfig.callbackToSearch = this.searchItemGuestDocument;
    this.autoCompleteDocumentConfig.callbackToGetSelectedValue = this.updateItemGuestDocument;
    this.autoCompleteDocumentConfig.fieldObjectToDisplay = 'document';
    this.autoCompleteDocumentConfig.displayIcon = false;
    this.autoCompleteDocumentConfig.extraClasses = 'thf-u-padding--1';
  }

  private setAutoCompleteEmailConfig() {
    this.autoCompleteEmailConfig = new AutocompleteConfig();
    this.autoCompleteEmailConfig.dataModel = 'document';
    this.autoCompleteEmailConfig.callbackToSearch = this.searchGuestByEmail;
    this.autoCompleteEmailConfig.callbackToGetSelectedValue = this.updateItemGuestEmail;
    this.autoCompleteEmailConfig.fieldObjectToDisplay = 'email';
    this.autoCompleteEmailConfig.displayIcon = false;
    this.autoCompleteEmailConfig.extraClasses = 'thf-u-padding--1';
  }

  public saveGuest(guestForm: FormGroup) {
    const guestReservationItem = this.mapperGuestFormToGuestReservationItem(guestForm);

    this.reservationResource
      .updateAndAssociateGuestToReservationItem(guestReservationItem.guestReservationItemId, guestReservationItem)
      .subscribe(result => {
        this.guestService.setSaveGuestForm(this.guest, guestForm);
        this.activeForm.emit(guestForm);

        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('guestModule.edit.associateAndUpdatedGuestReservatioItemSuccessMessage'),
        );
      });
  }

  private mapperGuestFormToGuestReservationItem(guestForm: FormGroup): GuestReservationItem {
    const guestReservationItem = new GuestReservationItem();

    guestReservationItem.guestReservationItemId = guestForm.get('guestReservationItemId').value;
    guestReservationItem.reservationItemId = guestForm.get('reservationItemId').value;
    guestReservationItem.guestId = guestForm.get('guestId').value;
    guestReservationItem.personId = guestForm.get('personId').value;
    guestReservationItem.guestName = guestForm.get('guestName').value;
    guestReservationItem.guestEmail = guestForm.get('guestEmail').value;
    guestReservationItem.guestDocument = guestForm.get('guestDocument').value;
    guestReservationItem.guestDocumentTypeId = guestForm.get('guestDocumentTypeId').value;
    guestReservationItem.checkinDate = guestForm.get('checkinDate').value;
    guestReservationItem.checkoutDate = guestForm.get('checkoutDate').value;
    guestReservationItem.estimatedArrivalDate = guestForm.get('estimatedArrivalDate').value;
    guestReservationItem.estimatedDepartureDate = guestForm.get('estimatedDepartureDate').value;
    guestReservationItem.preferredName = guestForm.get('preferredName').value;
    guestReservationItem.guestCitizenship = guestForm.get('guestCitizenship').value;
    guestReservationItem.guestLanguage = guestForm.get('language').value;
    guestReservationItem.pctDailyRate = guestForm.get('pctDailyRate').value;
    guestReservationItem.isIncognito = guestForm.get('isIncognito').value;
    guestReservationItem.isChild = guestForm.get('isChild').value;
    guestReservationItem.childAge = guestForm.get('childAge').value;
    guestReservationItem.isMain = guestForm.get('isMain').value;
    guestReservationItem.guestTypeId = guestForm.get('type').value;
    guestReservationItem.guestStatusId = guestForm.get('guestStatusId').value;
    guestReservationItem.estimatedArrivalDate = guestForm.get('periodOfStay').value.beginDate;
    guestReservationItem.estimatedDepartureDate = guestForm.get('periodOfStay').value.endDate;

    return guestReservationItem;
  }

  public cancelEdit() {
    this.guestService.cancelEditForm(this.guest);
  }

  public toogleShowDetails() {
    this.guestService.toogleShowDetailsForm(this.guest);
  }

  public isChild(): boolean {
    return this.guestService.isChild(this.guest);
  }

  public searchItemGuestDocument = (searchData: any) => {
    this.personResource.getPersonsByCriteria(searchData.query, this.optionDocument).subscribe(persons => {
      this.autoCompleteDocumentConfig.resultList = persons;
    });
  }

  public updateItemGuestDocument = (itemData: SearchPersonsResultDto) => {
    this.guestService.updateGuestForm(itemData, this.guestFormGroup);
  }

  public searchGuestByEmail = (searchData: any) => {
    if (searchData) {
      this.personResource.getPersonsByEmail(searchData.query).subscribe(persons => {
        this.autoCompleteEmailConfig.resultList = persons;
      });
    }
  }

  public updateItemGuestEmail = (itemData: SearchPersonsResultDto) => {
    this.guestService.updateGuestForm(itemData, this.guestFormGroup);
  }

  private calculatePctDailyValidator(pctDailyRate: number) {
    const total = 100;
    let sum = 0;
    if (this.guestsForm) {
      const guests = this.guestsForm.get('guests').value;
      guests.forEach(guest => {
        const guestToSum: GuestCardRoomlist = guest;
        if (guestToSum.guestId != this.guest.guestId) {
          sum += guestToSum.pctDailyRate;
        }
      });
      if (sum + pctDailyRate > total) {
        this.toasterService.emitChange(SuccessError.error, `A soma do '% da Diária' dos hóspedes não pode ultrapassar 100%`);
        this.guestFormGroup.get('pctDailyRate').setErrors({ pctDailyRateIsInvalid: sum + pctDailyRate });
      }
    }
  }

  private setValidators() {
    this.guestFormGroup.get('guestName')[this.dataInput['status'] != ReservationStatusEnum.Checkin ? 'enable' : 'disable']();

    if (this.guest.isChild) {
      this.guestFormGroup.get('childAge').setValidators(Validators.required);
    }

    this.guestFormGroup.get('pctDailyRate').valueChanges.subscribe(value => {
      this.calculatePctDailyValidator(value);
    });
  }

  public mapGuestTypes(guestTypes) {
    return guestTypes ? this.commonService.toOption(guestTypes, 'id', 'guestTypeName') : [];
  }

  public maskDocument(event) {
    const documentString = event.target.value;
    const backspaceCode = 8;
    if (event.keyCode != backspaceCode) {
      this.apllyMaskToDocument(documentString);
    }
  }

  public apllyMaskToDocument(documentString) {
    if (documentString) {
      this.guestFormGroup
        .get('guestDocument')
        .setValue(
          this.documentMaskService
            .maskDocumentStringByDocumentTypeId(documentString, this.guestFormGroup.get('guestDocumentTypeId').value)
        );
    }
  }

  public getDocumentTypeList() {
    this.documentTypeResource.getAllDocumentTypes().subscribe(response => {
      this.documentOptionSelectList = this.commonService.toOption(
        response.items,
        'id',
        'name',
      );
    });
  }

}
