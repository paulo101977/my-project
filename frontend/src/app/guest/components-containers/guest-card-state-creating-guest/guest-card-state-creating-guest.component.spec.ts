// TODO: create tests [DEBIT: 7517]

/*
import { ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { Http, BaseRequestOptions, ResponseOptions, Response } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { GuestCardStateCreatingGuestComponent } from './guest-card-state-creating-guest.component';
import { DocumentType } from '../../../shared/models/document-type';
import { SearchPersonsResultDto } from '../../../shared/models/person/search-persons-result-dto';
import { UpdateItemResponse } from '../../../shared/models/autocomplete/update-item-response';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { ShowHide } from './../../../shared/models/show-hide-enum';
import { GuestReservationItemData } from './../../../shared/mock-data/guest-reservation-item-data';
import { SuccessError } from './../../../shared/models/success-error-enum';
import { GuestCardRoomlist } from '../../../shared/models/guest-card-roomlist';
import { GuestService } from '../../../shared/services/guest/guest.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { DateService } from '../../../shared/services/shared/date.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { ReservationResource } from '../../../shared/resources/reservation/reservation.resource';
import { PersonResource } from '../../../shared/resources/person/person.resource';
import { DocumentForLegalAndNaturalPerson } from '../../../shared/models/document';

describe('GuestCardStateCreatingGuestComponent', () => {
  let component: GuestCardStateCreatingGuestComponent;
  let fixture: ComponentFixture<GuestCardStateCreatingGuestComponent>;
  const formBuilder = new FormBuilder();
  let mockBackend: MockBackend;
  let resourceMock: ReservationResource;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed
      .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: [
          TranslateModule.forRoot(),
          FormsModule,
          ReactiveFormsModule,

          HttpClientModule,
          MyDateRangePickerModule
        ],
        declarations: [GuestCardStateCreatingGuestComponent],
        providers: [],
        schemas: [NO_ERRORS_SCHEMA]
      });

    fixture = TestBed.createComponent(GuestCardStateCreatingGuestComponent);
    component = fixture.componentInstance;

    // Inputs
    component.dataInput = {};

    const documentType = new DocumentType();
    documentType.name = 'CPF';
    const document = new DocumentForLegalAndNaturalPerson();
    document.documentType = documentType;

    component.guest = new GuestCardRoomlist();
    component.guest.guestReservationItemId = 1;
    component.guest.reservationItemId = 1;
    component.guest.guestId = 1;
    component.guest.personId = 1;
    component.guest.guestName = '';
    component.guest.preferredName = '';
    component.guest.childAge = '';
    component.guest.guestEmail = '';
    component.guest.type = 1;
    component.guest.isIncognito = true;
    component.guest.isChild = false;
    component.guest.isMain = true;
    component.guest.guestCitizenship = '';
    component.guest.language = '';
    component.guest.uHPreferably = '';
    component.guest.lastUH = '';
    component.guest.lastDailyValue = 100;
    component.guest.periodOfStay = {
      beginDate: { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() },
      endDate: { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() },
      beginJsDate: new Date(),
      endJsDate: new Date()
    };
    component.guest.checkinDate = new Date();
    component.guest.checkoutDate = new Date();
    component.guest.pctDailyRate = 100;
    component.guest.remarks = '';
    component.guest.status = 1;
    component.guest.lastStatus = 1;
    component.guest.guestStatusId = 1;
    component.guest.showDetails = true;
    component.guest.guestDocument = document;
    component.guest.guestDocumentTypeId = '';
    component.guest.estimatedArrivalDate = new Date();
    component.guest.estimatedDepartureDate = new Date();
    component.guest.completeAddress = '';
    // End Inputs

    fixture.detectChanges();
    mockBackend = TestBed.get(MockBackend);
    resourceMock = TestBed.get(ReservationResource);
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();

      const possibleAgesOfProperty = [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17
      ];
      expect(component.possibleAgesOfProperty).toEqual(possibleAgesOfProperty);
    });

    it('should save GuestReservationItem and change status guestCard to GuestCreated after saveGuest', fakeAsync(() => {
      spyOn(component['guestService'], 'setSaveGuestForm');
      spyOn(component.activeForm, 'emit');
      spyOn(component['loadPageEmitService'], 'emitChange');
      spyOn(component['toasterEmitService'], 'emitChange');

      mockBackend.connections.subscribe((connection: MockConnection) => {
        connection.mockRespond(new Response(<ResponseOptions>{
          body: JSON.stringify(GuestReservationItemData)
        }));
      });

      component.saveGuest(component.guestFormGroup);
      tick();

      resourceMock.updateAndAssociateGuestToReservationItem(1, GuestReservationItemData).then(
        resp => {
          expect(component['guestService'].setSaveGuestForm).toHaveBeenCalledWith(component.guest, component.guestFormGroup);
          expect(component.activeForm.emit).toHaveBeenCalledWith(component.guestFormGroup);
          expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
          expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
            SuccessError.success,
            'guestModule.edit.associateAndUpdatedGuestReservatioItemSuccessMessage',
          );
        }
      );

      expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.show);
    }));

    it('should change status guestCard to LastStatus after cancelEdit', () => {
      spyOn(component['guestService'], 'cancelEditForm');

      component.cancelEdit();

      expect(component['guestService'].cancelEditForm).toHaveBeenCalledWith(component.guest);
    });

    it('should change showDetails', () => {
      spyOn(component['guestService'], 'toogleShowDetailsForm');


      component.toogleShowDetails();

      expect(component['guestService'].toogleShowDetailsForm).toHaveBeenCalledWith(component.guest);
    });
  });

  describe('on document autocomplete with select', () => {

    it('should call updateGuestForm', () => {
      spyOn(component['guestService'], 'updateGuestForm');

      const item = new UpdateItemResponse<SearchPersonsResultDto>();
      item.completeItem = new SearchPersonsResultDto();
      item.choosenOption = 'ABC';
      const guest = formBuilder.group({});

      component.updateItemGuestDocument(item, guest);

      expect(component['guestService'].updateGuestForm).toHaveBeenCalledWith(item, guest);
    });

    it('should call updateAutocompleteItemsGuestDocument', () => {
      spyOn(component, 'searchItemGuestDocument');

      const option = new DocumentType();
      option.name = 'CPF';

      component.searchDataGestDocument = '';

      component.updateAutocompleteItemsGuestDocument(option);

      expect(component.optionDocument).toEqual(option);
      expect(component.searchItemGuestDocument).toHaveBeenCalledWith(component.searchDataGestDocument);
    });

    it('should searchItemGuestDocument', fakeAsync(() => {
      const search = 'abcde';

      const searchPersonsResult = new SearchPersonsResultDto();
      searchPersonsResult.id = '1';
      const searchPersonsResult2 = new SearchPersonsResultDto();
      searchPersonsResult2.id = '2';
      const searchPersonsResult3 = new SearchPersonsResultDto();
      searchPersonsResult3.id = '3';

      const personsResult = new Array<SearchPersonsResultDto>();
      personsResult.push(searchPersonsResult);
      personsResult.push(searchPersonsResult2);
      personsResult.push(searchPersonsResult3);

      const promise = spyOn(component['personResource'], 'getPersonsByCriteria').and.callFake(() => {
        return new Promise((resolve, reject) => resolve(personsResult));
      });
      promise(search, this.optionDocument).then(persons => component.autocompleItemsGuestsDocument = persons);
      tick();

      component.searchItemGuestDocument(search);

      expect(component.searchDataGestDocument).toEqual(search);
      expect(component.autocompleItemsGuestsDocument).toEqual(personsResult);
    }));
  });

  describe('on email autocomplete', () => {

    it('should call updateEmailGuestForm', () => {
      spyOn(component['guestService'], 'updateEmailGuestForm');

      const item = new UpdateItemResponse<SearchPersonsResultDto>();
      item.completeItem = new SearchPersonsResultDto();
      item.choosenOption = 'caio.regis@bematech.com.br';
      const guest = formBuilder.group({});

      component.updateItemGuestEmail(item, guest);

      expect(component['guestService'].updateEmailGuestForm).toHaveBeenCalledWith(item, guest);
    });

    it('should searchGuestByEmail', fakeAsync(() => {
      const search = 'abcde';

      const searchPersonsResult = new SearchPersonsResultDto();
      searchPersonsResult.id = '1';
      const searchPersonsResult2 = new SearchPersonsResultDto();
      searchPersonsResult2.id = '2';
      const searchPersonsResult3 = new SearchPersonsResultDto();
      searchPersonsResult3.id = '3';

      const personsResult = new Array<SearchPersonsResultDto>();
      personsResult.push(searchPersonsResult);
      personsResult.push(searchPersonsResult2);
      personsResult.push(searchPersonsResult3);

      const promise = spyOn(component['personResource'], 'getPersonsByEmail').and.callFake(() => {
        return new Promise((resolve, reject) => resolve(personsResult));
      });
      promise(search).then(persons => component.autocompleItemsGuestsEmail = persons);
      tick();

      component.searchGuestByEmail(search);

      expect(component.autocompleItemsGuestsEmail).toEqual(personsResult);
    }));
  });

});
*/
