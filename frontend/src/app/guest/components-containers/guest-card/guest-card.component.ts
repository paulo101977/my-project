import { Component, OnInit, Input, ViewChildren, QueryList } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { GuestService } from 'app/shared/services/guest/guest.service';
import { GuestResource } from 'app/shared/resources/guest/guest.resource';
import { AddressResource } from 'app/shared/resources/address/address.resource';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { GuestCardStateCreatedComponent } from '../../components/guest-card-state-created/guest-card-state-created.component';
import { GuestState } from 'app/shared/models/reserves/guest';
import { Country } from 'app/shared/models/country';
import { ThexAddressApiService, PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-guest-card',
  templateUrl: './guest-card.component.html',
})
export class GuestCardComponent implements OnInit {
  @Input() guestsFromReservationItem: FormGroup;

  public referenceGuestState = GuestState;
  public propertyId: number;
  public dataInput = {};
  public countryInfoList: Array<Country>;

  public currencyCountry: string;

  @ViewChildren(GuestCardStateCreatedComponent) cardsStateCreated: QueryList<GuestCardStateCreatedComponent>;

  constructor(
    private guestService: GuestService,
    private guestResource: GuestResource,
    private addressResource: AddressResource,
    private commomService: CommomService,
    private route: ActivatedRoute,
    private thexAddressApiService: ThexAddressApiService,
    private propertyService: PropertyStorageService
  ) {}

  ngOnInit() {
    this.currencyCountry = window.navigator.language;
    this.propertyId = this.propertyService.getCurrentPropertyId();
    this.route.params.subscribe(params => {
      this.getGuestTypes();
      this.getNationalities();
      this.dataInput['configDateRangePicker'] = this.guestsFromReservationItem.get('configDateRangePicker').value;
      this.dataInput['status'] = this.guestsFromReservationItem.get('status').value;
    });
  }

  public getGuestsForm(form): any {
    return form.get('guests').value;
  }

  public activeNextGuest(formList: FormGroup): void {
    const guestList = this.getGuestsForm(formList);
    for (const guest of guestList) {
      if (guest.state === this.referenceGuestState.NoGuest) {
        this.guestService.setEditGuestForm(guest);
        break;
      }
    }
  }

  private getGuestTypes() {
    this.guestResource.getGuestTypesByPropertyId(this.propertyId).subscribe(response => {
      if (response.items) {
        this.dataInput['guestTypes'] = response.items;
      }
    });
  }

  private getNationalities(): void {
    this.thexAddressApiService.getNationalities().subscribe(({items}) => {
      this.countryInfoList = items;
      this.dataInput['nationalities'] = this.commomService.toOption(
        items,
        'name',
        'nationality',
      );
    });
  }
}
