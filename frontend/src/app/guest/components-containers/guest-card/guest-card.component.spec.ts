import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormArray, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { GuestCardComponent } from './guest-card.component';
import { PropertyGuestTypeDtoDataList } from 'app/shared/mock-data/guest-type-data';
import { GuestResource } from 'app/shared/resources/guest/guest.resource';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';
import { GuestState } from 'app/shared/models/reserves/guest';

xdescribe('GuestCardComponent', () => {
  let component: GuestCardComponent;
  let fixture: ComponentFixture<GuestCardComponent>;
  const formBuilder = new FormBuilder();
  let httpMock: HttpTestingController;
  let resource: GuestResource;
  let formList;
  let formArray;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), FormsModule, ReactiveFormsModule, HttpClientTestingModule, RouterTestingModule],
      declarations: [GuestCardComponent],
      providers: [ InterceptorHandlerService ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(GuestCardComponent);
    component = fixture.componentInstance;
    component.guestsFromReservationItem = formBuilder.group({
      guests: formBuilder.array([]),
      configDateRangePicker: '',
      status: 2,
    });

    resource = TestBed.get(GuestResource);
    httpMock = TestBed.get(HttpTestingController);

    formList = formBuilder.group({
      guests: formBuilder.array([formBuilder.group({ state: GuestState.NoGuest })]),
    });
    formArray = <FormArray>formList.controls.guests;

    spyOn<any>(component, 'getGuestTypes');
    spyOn<any>(component, 'getNationalities');

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should change status guest', () => {
      spyOn(component['guestService'], 'setEditGuestForm');
      component.activeNextGuest(formList);
      expect(component['guestService'].setEditGuestForm).toHaveBeenCalledWith(formArray.at(0).value);
    });

    it('should noChange status guest', () => {
      spyOn(component['guestService'], 'setEditGuestForm');
      formList
        .get('guests')
        .at(0)
        .get('state')
        .setValue(GuestState.CreatingGuest);
      component.activeNextGuest(formList);
      expect(component['guestService'].setEditGuestForm).not.toHaveBeenCalled();
    });

    it('should get GuestTypes from Property', () => {
      component.propertyId = 1;

      resource.getGuestTypesByPropertyId(component.propertyId).subscribe(result => {
        expect(PropertyGuestTypeDtoDataList).toEqual(result.items);
      });

      const req = httpMock.expectOne('PropertyGuestType/1/byproperty');

      req.flush({
        hasNext: false,
        items: PropertyGuestTypeDtoDataList,
        total: 1,
      });

      httpMock.verify();
    });
  });
});
