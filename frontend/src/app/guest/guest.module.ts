import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { GuestRoutingModule } from './guest-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MyDateRangePickerModule } from 'mydaterangepicker';

import { GuestEditComponent } from './components/guest-edit/guest-edit.component';
import { GuestEditRoomlistComponent } from './components/guest-edit-roomlist/guest-edit-roomlist.component';
import { GuestCardComponent } from './components-containers/guest-card/guest-card.component';
import { GuestCardStateCreatedComponent } from './components/guest-card-state-created/guest-card-state-created.component';
import { GuestCardStateNoguestComponent } from './components/guest-card-state-noguest/guest-card-state-noguest.component';
import {
  GuestCardStateCreatingGuestComponent
} from './components-containers/guest-card-state-creating-guest/guest-card-state-creating-guest.component';
import {
  PrintSingleGuestRegistrationComponent
} from '../checkin/components/print-single-guest-registration/print-single-guest-registration.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    GuestRoutingModule,
    SharedModule,
    MyDateRangePickerModule,
    ReactiveFormsModule,
    FormsModule],
  declarations: [
    GuestEditComponent,
    GuestEditRoomlistComponent,
    GuestCardComponent,
    GuestCardStateCreatedComponent,
    GuestCardStateNoguestComponent,
    GuestCardStateCreatingGuestComponent,
  ],
  exports: [GuestEditComponent, GuestEditRoomlistComponent],
  entryComponents: [PrintSingleGuestRegistrationComponent],
})
export class GuestModule {}
