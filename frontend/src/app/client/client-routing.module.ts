import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientListComponent } from './components/client-list/client-list.component';
import { ClientCreateEditComponent } from './component-container/client-create-edit/client-create-edit.component';

export const clientRoutes: Routes = [
  { path: '', component: ClientListComponent },
  { path: 'new', component: ClientCreateEditComponent },
  { path: 'edit/:id', component: ClientCreateEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(clientRoutes)],
  exports: [RouterModule],
})
export class ClientRoutingModule {}
