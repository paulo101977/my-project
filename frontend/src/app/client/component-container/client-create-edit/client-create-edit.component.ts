import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { ConfigHeaderPage } from '../../../shared/models/config-header-page';
import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ClientDto } from '../../../shared/models/dto/client/client-dto';
import { ClientDocumentTypeEnum } from '../../../shared/models/dto/client/client-document-types';
import { LocationCategoryEnum } from '../../../shared/models/location-category-enum';
import { ClientService } from '../../../shared/services/client/client.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ClientResource } from '../../../shared/resources/client/client.resource';
import { GuidDefaultData } from '../../../shared/mock-data/guid-default-data';
import { CustomerResource } from '../../../shared/resources/consult-customer/customer.resource';
import { CustomerStation } from '../../../shared/models/integration/customerStation';
import { ChannelResource } from '../../../shared/resources/channel/channel.resource';
import { ClientLocationDto } from 'app/shared/models/dto/client/client-location-dto';

@Component({
  selector: 'app-client-create-edit',
  templateUrl: './client-create-edit.component.html',
})
export class ClientCreateEditComponent implements OnInit {
  private readonly BRASIL_COUNTRY_SUBDIVISION = 1;
  private isEdition;

  public configHeaderPage: ConfigHeaderPage;
  public formDataGroup: FormGroup;
  public isPersonTypeL: boolean;
  public dataSend: ClientDto;
  public checklistValid: any;
  public isInvalid: boolean;
  public comercialAddress: ClientLocationDto;


  constructor(
    public translateService: TranslateService,
    public _route: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder,
    private clientResource: ClientResource,
    private customerResource: CustomerResource,
    private channelResource: ChannelResource,
    private clientService: ClientService,
    private toasterEmitService: ToasterEmitService
  ) {
    this.isInvalid = true;
    this.checklistValid = {
      formDataGroup: false,
      locationList: false,
      documentList: false,
    };
  }

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.dataSend = new ClientDto();
    this.isPersonTypeL = true;
    this.setConfigHeader();
    this.setFormConfig();

    this._route.params.subscribe(params => {
      if (params.id) {
        this.isEdition = true;
        this.configHeaderPage.titlePage = 'clientModule.edit.pageHeader.title';
        this.getClientWithId(params.id);
      }
    });
  }

  private setConfigHeader() {
    this.configHeaderPage = new ConfigHeaderPage(HeaderPageEnum.Create);
    this.configHeaderPage.titlePage = 'clientModule.new.pageHeader.title';
  }

  private setFormConfig(): void {
    this.formDataGroup = this.formBuilder.group({
      companyId: 0,
      personId: GuidDefaultData,
      propertyId: _.parseInt(this._route.snapshot.params.property),
      personType: [ClientDocumentTypeEnum.Legal, [Validators.required]],
      shortName: ['', [Validators.required]],
      tradeName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      homePage: '',
      phoneNumber: '',
      cellPhoneNumber: '',
      isActive: [true, [Validators.required]],
      propertyCompanyClientCategoryId: [GuidDefaultData, [Validators.required]],
      externalCode: null,
    });

    this.updateValidatorsFromPersonType(ClientDocumentTypeEnum.Legal);
    this.formDataGroup.get('personType').valueChanges.subscribe(personType => {
      this.isPersonTypeL = this.formDataGroup.get('personType').value === ClientDocumentTypeEnum.Legal;
      this.updateValidatorsFromPersonType(this.formDataGroup.get('personType').value);
      if (this.personTypeIsDifferent(personType) && this.hasDocumentListOrContactList()) {
        this.clearDocumentListAndContactList();
      }
    });
  }

  private getClientWithId(id: string): void {
    this.clientResource.getClientById(id).subscribe(async result => {
      this.dataSend = result;
      this.setClientDataBasic(result);
    });
  }

  async getListCustomer(): Promise<CustomerStation[]> {
    return new Promise<CustomerStation[]>(resolve => {
      this
        .customerResource
        .getAllList()
        .subscribe( item => resolve(item) );
    });
  }

  private setClientDataBasic(client: ClientDto) {
    this.formDataGroup.patchValue({
      companyId: client.companyId,
      personId: client.personId,
      propertyId: client.propertyId,
      personType: client.personType,
      shortName: client.shortName,
      tradeName: client.tradeName,
      email: client.email,
      homePage: client.homePage,
      phoneNumber: client.phoneNumber,
      cellPhoneNumber: client.cellPhoneNumber,
      isActive: client.isActive,
      propertyCompanyClientCategoryId: client.propertyCompanyClientCategoryId,
      externalCode: client.externalCode,
    });
  }

  public checkIsValid(): boolean {
    this.isInvalid = true;
    const dataEnd = this.clientService.parsingDataEndtoSave(this.dataSend, this.formDataGroup.value);

    this.checklistValid.formDataGroup = this.formDataGroup.valid;
    this.checklistValid.locationList = dataEnd.locationList.some(loc => loc.locationCategoryId == LocationCategoryEnum.Comercial);

    if (this.comercialAddress && this.comercialAddress.countrySubdivisionId == this.BRASIL_COUNTRY_SUBDIVISION) {
      if (this.isPersonTypeL) {
        this.checklistValid.documentList = dataEnd.documentList.some(doc => doc.documentTypeName == 'CNPJ');
      } else {
        this.checklistValid.documentList = dataEnd.documentList.some(doc => doc.documentTypeName == 'CPF');
      }
    } else {
      this.checklistValid.documentList = dataEnd.documentList.length > 0;
    }

    if (this.checklistValid.formDataGroup && this.checklistValid.locationList && this.checklistValid.documentList) {
      this.isInvalid = false;
    }

    return this.isInvalid;
  }

  public saveFormAction(): void {
    const data = this.clientService.parsingDataEndtoSave(this.dataSend, this.formDataGroup.value);
    let request,
      path,
      message = null;

    if (this.isEdition) {
      request = this.clientResource.editClient(data);
      path = '../../';
      message = this.translateService.instant('variable.lbEditSuccessM', {
        labelName: this.translateService.instant('label.client'),
      });
    } else {
      request = this.clientResource.createClient(data);
      path = '../';
      message = this.translateService.instant('variable.lbSaveSuccessM', {
        labelName: this.translateService.instant('label.client'),
      });
    }

    request.subscribe(result => {
      this.toasterEmitService.emitChange(SuccessError.success, message);
      this.router.navigate([path], { relativeTo: this._route });
    });
  }

  public cancelFormAction() {
    this.isEdition
      ? this.router.navigate(['../../'], {relativeTo: this._route})
      : this.router.navigate(['../'], {relativeTo: this._route});
  }

  private updateValidatorsFromPersonType(personType): void {
    if (personType == ClientDocumentTypeEnum.Legal) {
      this.formDataGroup.get('tradeName').setValidators(Validators.required);
      this.formDataGroup.get('tradeName').updateValueAndValidity();
      this.formDataGroup.get('shortName').setValidators(Validators.required);
      this.formDataGroup.get('shortName').updateValueAndValidity();
    } else if (personType == ClientDocumentTypeEnum.Natural) {
      this.formDataGroup.get('tradeName').clearValidators();
      this.formDataGroup.get('tradeName').updateValueAndValidity();
      this.formDataGroup.get('shortName').clearValidators();
      this.formDataGroup.get('shortName').updateValueAndValidity();
    }
  }

  private clearDocumentListAndContactList() {
    this.dataSend.documentList = [];
    this.dataSend.companyClientContactPersonList = [];
  }

  private personTypeIsDifferent(personType: string) {
    return this.dataSend.personType.toString() != personType;
  }

  private hasDocumentListOrContactList() {
    return this.dataSend.documentList.length > 0 || this.dataSend.companyClientContactPersonList.length > 0;
  }

  public updateComercialAddress(address: ClientLocationDto) {
    this.comercialAddress = address;
  }
}
