
import {of as observableOf,  Observable } from 'rxjs';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClientDocumentTypeEnum } from '../../../shared/models/dto/client/client-document-types';
import { ClientCreateEditComponent } from './client-create-edit.component';
import { ThexApiTestingModule,
  InterceptorHandlerService,
  configureApi,
  ApiEnvironment} from '@inovacaocmnet/thx-bifrost';

describe('ClientCreateEditComponent', () => {
  let component: ClientCreateEditComponent;
  let fixture: ComponentFixture<ClientCreateEditComponent>;
  const formBuilder = new FormBuilder();

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
      imports: [TranslateModule.forRoot(),
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        RouterTestingModule,
        ThexApiTestingModule],
      declarations: [ClientCreateEditComponent],
      providers: [
        InterceptorHandlerService,
        configureApi({
          environment: ApiEnvironment.Development
        })
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(ClientCreateEditComponent);
    component = fixture.componentInstance;

    component.checklistValid = {
      formDataGroup: false,
      locationList: false,
      documentList: false,
    };

    component.formDataGroup = formBuilder.group({
      companyId: '',
      id: '',
      personId: '',
      propertyId: '',
      personType: '',
      shortName: '',
      tradeName: '',
      email: '',
      homePage: '',
      phoneNumber: '',
      cellPhoneNumber: '',
      isActive: '',
      isAcquirer: '',
      dateOfBirth: '',
      countrySubdivisionId: '',
      group: '',
      propertyCompanyClientCategoryId: '',
      externalCode: ''
    });

    component.dataSend = {
      companyId: 0,
      id: '',
      personId: '',
      propertyId: 0,
      personType: ClientDocumentTypeEnum.Legal,
      shortName: '',
      tradeName: '',
      email: '',
      homePage: '',
      phoneNumber: '',
      cellPhoneNumber: '',
      isActive: null,
      propertyCompanyClientCategoryId: '',
      locationList: [],
      documentList: [],
      companyClientContactPersonList: [],
      customerStation: [],
      companyClientChannelList: [],
      externalCode: ''
    };

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should call initFormConfig', () => {
      component['setFormConfig']();

      expect(Object.keys(component.formDataGroup.controls)).toEqual([
        'companyId',
        'personId',
        'propertyId',
        'personType',
        'shortName',
        'tradeName',
        'email',
        'homePage',
        'phoneNumber',
        'cellPhoneNumber',
        'isActive',
        'propertyCompanyClientCategoryId',
        'externalCode'
      ]);
    });

    it('should call saveFormAction', () => {
      spyOn(component['toasterEmitService'], 'emitChange');

      spyOn(component['clientResource'], 'createClient').and.returnValue(observableOf({}));
      component.saveFormAction();

      expect(component['clientResource'].createClient).toHaveBeenCalled();
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalled();
    });

    it('should call saveFormAction edition', () => {
      spyOn(component['toasterEmitService'], 'emitChange');

      spyOn(component['clientResource'], 'editClient').and.returnValue(observableOf({}));
      component['isEdition'] = true;
      component.saveFormAction();

      expect(component['clientResource'].editClient).toHaveBeenCalled();
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalled();
    });

    xit('should checkIsValid if isEdition TRUE', () => {
      component['isEdition'] = true;
      component.checkIsValid();

      expect(component.isInvalid).toBeFalsy();
      component['isEdition'] = false;
    });

    xit('should checkIsValid personType "Legal"', () => {
      component.formDataGroup.get('shortName').setValue('test');
      component.formDataGroup.get('tradeName').setValue('test');
      component.formDataGroup.get('email').setValue('test');
      component.formDataGroup.get('homePage').setValue('test');
      component.formDataGroup.get('address.postalCode').setValue('12312312');
      component.formDataGroup.get('address.locality').setValue('test');
      component.formDataGroup.get('address.number').setValue('test');
      component.formDataGroup.get('address.federativeUnit').setValue('test');
      component.formDataGroup.get('address.selectValueType.type').setValue('test');
      component.formDataGroup.get('address.city').setValue('test');
      component.formDataGroup.get('address.district').setValue('test');

      component.checkIsValid();

      expect(component.checklistValid.locationList).toBeFalsy();
      expect(component.checklistValid.documentList).toBeFalsy();

      // component.dataSend['locationList'].push({
      //   locationCategoryId: 1
      // });
      //
      // component.dataSend['documentList'].push({
      //   documentTypeName: 'CNPJ'
      // });

      component.checkIsValid();

      expect(component.checklistValid.formDataGroup).toBeTruthy();
      expect(component.checklistValid.locationList).toBeTruthy();
      expect(component.checklistValid.documentList).toBeTruthy();
      expect(component.isInvalid).toBeFalsy();
    });
  });
});
