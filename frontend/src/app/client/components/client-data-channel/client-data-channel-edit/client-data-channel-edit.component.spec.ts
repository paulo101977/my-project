import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { CHANNEL_COMPANY_ASSOCIATION_DATA } from 'app/integration/components/mock/channel-data';
import { TranslateModule } from '@ngx-translate/core';
import {
  ClientDataChannelEditComponent
} from 'app/client/components/client-data-channel/client-data-channel-edit/client-data-channel-edit.component';

describe('ClientDataChannelEditComponent', () => {
  let component: ClientDataChannelEditComponent;
  let fixture: ComponentFixture<ClientDataChannelEditComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: [TranslateModule.forRoot()],
        providers: [FormBuilder],
        declarations: [ClientDataChannelEditComponent],
        schemas: [NO_ERRORS_SCHEMA],
      });

    fixture = TestBed.createComponent(ClientDataChannelEditComponent);
    component = fixture.componentInstance;
    component.selectedItem = CHANNEL_COMPANY_ASSOCIATION_DATA;
    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('set settings', () => {
    it('should setFormChannelModal', () => {
      component['setFormConfig']();

      expect(component.form.contains('id')).toBeTruthy();
      expect(component.form.contains('channelCodeName')).toBeTruthy();
      expect(component.form.contains('channelDescription')).toBeTruthy();
      expect(component.form.contains('businessSourceDescription')).toBeTruthy();
      expect(component.form.contains('companyId')).toBeTruthy();
      expect(component.form.contains('channelId')).toBeTruthy();
      expect(component.form.contains('isActive')).toBeTruthy();
    });

    it('should setBtnConfig', () => {
      component['setBtnConfig']();

      expect(component.cancelBtnConfig.id).toEqual('cancel');
      expect(component.cancelBtnConfig.textButton).toEqual('action.cancel');
      expect(component.cancelBtnConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.cancelBtnConfig.buttonSize).toEqual(ButtonSize.Normal);

      expect(component.saveBtnConfig.id).toEqual('save');
      expect(component.saveBtnConfig.textButton).toEqual('action.confirm');
      expect(component.saveBtnConfig.buttonType).toEqual(ButtonType.Primary);
      expect(component.saveBtnConfig.buttonSize).toEqual(ButtonSize.Normal);
      expect(component.saveBtnConfig.isDisabled).toBeTruthy();
    });
  });

  describe('actions', () => {
    it('should fieldFormData', () => {
      component['fieldFormData'](CHANNEL_COMPANY_ASSOCIATION_DATA);

      expect(component.form.get('channelCodeName').value).toEqual('CODE');
    });

    it('should save', () => {
      spyOn(component.dataChange, 'emit');
      spyOn(component.form, 'reset');

      component.form.patchValue(CHANNEL_COMPANY_ASSOCIATION_DATA);
      component['save'](component.form);

      expect(component.dataChange.emit).toHaveBeenCalledWith(component.form.value);
      expect(component.form.reset).toHaveBeenCalled();
    });

    it('should save#not save - not valid', () => {
      spyOn(component.dataChange, 'emit');
      spyOn(component.form, 'reset');

      component.form.patchValue({
        companyId: ''
      });
      component['save'](component.form);

      expect(component.dataChange.emit).not.toHaveBeenCalledWith(component.form.value);
      expect(component.form.reset).not.toHaveBeenCalled();
    });

    it('should cancel', () => {
      spyOn(component.exitChange, 'emit');
      spyOn(component.form, 'reset');

      component['cancel']();

      expect(component.exitChange.emit).toHaveBeenCalled();
      expect(component.form.reset).toHaveBeenCalled();
    });
  });

});
