import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChannelClientAssociation } from '../../../../shared/models/integration/channel';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';

@Component({
  selector: 'app-client-data-channel-edit',
  templateUrl: './client-data-channel-edit.component.html',
})
export class ClientDataChannelEditComponent implements OnInit, OnChanges {

  @Input() selectedItem: ChannelClientAssociation;
  @Output() dataChange = new EventEmitter<ChannelClientAssociation>();
  @Output() exitChange = new EventEmitter();

  public form: FormGroup;

  // Button dependencies
  public cancelBtnConfig: ButtonConfig;
  public saveBtnConfig: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.selectedItem.currentValue) {
      const channel: ChannelClientAssociation = changes.selectedItem.currentValue;
      this.fieldFormData(channel);
    }
  }

  private setVars() {
    this.setFormConfig();
    this.setBtnConfig();
  }

  private setFormConfig() {
    this.form = this.formBuilder.group({
      id: null,
      channelCodeName: null,
      channelDescription: null,
      businessSourceDescription: null,
      companyId: ['', [Validators.required]],
      channelId: null,
      isActive: true,
    });

    this.form.valueChanges
      .subscribe(() => {
        this.saveBtnConfig.isDisabled = this.form.invalid;
      });
  }

  private setBtnConfig() {
    this.cancelBtnConfig = this.sharedService.getButtonConfig(
      'cancel',
      () => this.cancel(),
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal
    );

    this.saveBtnConfig = this.sharedService.getButtonConfig(
      'save',
      () => this.save(this.form),
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  private fieldFormData(item: ChannelClientAssociation) {
    if (item) {
      this.form.patchValue(item);
    }
  }

  private cancel() {
    this.form.reset();
    this.exitChange.emit();
  }

  private save({value, valid}: { value: ChannelClientAssociation; valid: boolean }): void {
    if (valid) {
      this.dataChange.emit(value);
      this.form.reset();
    }
  }

}
