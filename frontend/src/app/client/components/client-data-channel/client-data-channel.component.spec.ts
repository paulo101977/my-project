import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClientDataChannelComponent } from 'app/client/components/client-data-channel/client-data-channel.component';
import { CHANNEL_COMPANY_ASSOCIATION_DATA } from 'app/integration/components/mock/channel-data';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';

describe('ClientDataChannelComponent', () => {
  let component: ClientDataChannelComponent;
  let fixture: ComponentFixture<ClientDataChannelComponent>;

  const modalEmitToggleServiceStub: Partial<ModalEmitToggleService> = {
    emitToggleWithRef(modalId: any): void {}
  };
  @Pipe({name: 'imgCdn'})
  class ImgCdnPipeStub implements PipeTransform {
    transform(value: any, ...args: any[]): any {
    }
  }

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), ReactiveFormsModule, FormsModule],
      providers: [
        {
          provide: ModalEmitToggleService,
          useValue: modalEmitToggleServiceStub
        }
      ],
      declarations: [
        ClientDataChannelComponent,
        ImgCdnPipeStub
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(ClientDataChannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('set settings', () => {
    it('should setColumnsName', () => {
      component['setColumnsName']();

      expect(component.columns[0]).toEqual({name: 'label.code', prop: 'channelCodeName'});
      expect(component.columns[1]).toEqual({name: 'label.description', prop: 'channelDescription'});
      expect(component.columns[2]).toEqual({name: 'label.origin', prop: 'businessSourceDescription'});
      expect(component.columns[3]).toEqual({name: 'label.companyId', prop: 'companyId'});
      expect(component.columns[4]).toEqual({
        cellTemplate: component.editDeleteTemplate,
        name: '',
        cellClass: 'thf-u-no-padding',
        width: 50,
        resizeable: false,
        draggable: false,
        canAutoResize: false
      });
    });
  });

  describe('actions', () => {
    it('should toggleModal', () => {
      spyOn(component['modal'], 'emitToggleWithRef');

      component.toggleModal();

      expect(component.modalTitle).toEqual(component['translate'].instant('action.lbAssociate', {
        labelName: component.labelName
      }));
      expect(component['modal'].emitToggleWithRef).toHaveBeenCalledWith('newDataChannel');
    });

    it('should toggleModalEdit', () => {
      spyOn(component['modal'], 'emitToggleWithRef');

      component.toggleModalEdit();

      expect(component['modal'].emitToggleWithRef).toHaveBeenCalledWith('editDataChannel');
    });

    it('should goToEdit', () => {
      spyOn(component, 'toggleModalEdit');

      component.channelsList = [CHANNEL_COMPANY_ASSOCIATION_DATA];

      component.goToEdit(CHANNEL_COMPANY_ASSOCIATION_DATA);

      expect(component.modalTitle).toEqual(component['translate'].instant('action.lbEdit', {
        labelName: component.labelName
      }));
      expect(component.itemSelected).toEqual(CHANNEL_COMPANY_ASSOCIATION_DATA);
      expect(component.toggleModalEdit).toHaveBeenCalled();
    });

    it('should changeDataSavedFromModel', () => {
      spyOn(component, 'toggleModal');

      component.channels = [CHANNEL_COMPANY_ASSOCIATION_DATA];

      component.changeDataSavedFromModel([CHANNEL_COMPANY_ASSOCIATION_DATA]);

      expect(component.channels.length).toEqual(2);
      expect(component.toggleModal).toHaveBeenCalled();
    });

    it('should changeDataEditedFromModel', () => {
      spyOn(component, 'toggleModalEdit');

      component.channels = [CHANNEL_COMPANY_ASSOCIATION_DATA];
      CHANNEL_COMPANY_ASSOCIATION_DATA.channelCodeName = 'TESTE CODE';

      component.changeDataEditedFromModel(CHANNEL_COMPANY_ASSOCIATION_DATA);

      expect(component.channels[0].channelCodeName).toEqual('TESTE CODE');
      expect(component.toggleModalEdit).toHaveBeenCalled();
    });

    it('should search#found', () => {
      CHANNEL_COMPANY_ASSOCIATION_DATA.channelCodeName = 'TESTE CODE';
      component.channelsListBack = [CHANNEL_COMPANY_ASSOCIATION_DATA];

      component.search('TESTE CODE');

      expect(component.channelsList.length).toEqual(1);
    });

    it('should search#NOT found', () => {
      CHANNEL_COMPANY_ASSOCIATION_DATA.channelCodeName = 'TESTE CODE';
      component.channelsListBack = [CHANNEL_COMPANY_ASSOCIATION_DATA];

      component.search('TESTA CODA');

      expect(component.channelsList.length).toEqual(0);
    });

    it('should delete', () => {
      component.channels = [CHANNEL_COMPANY_ASSOCIATION_DATA];

      component.delete(CHANNEL_COMPANY_ASSOCIATION_DATA);

      expect(component.channels.length).toEqual(0);
    });

    it('should hasAssociation', () => {
      const hasAssociation = component.hasAssociation(CHANNEL_COMPANY_ASSOCIATION_DATA);

      expect(hasAssociation).toBeTruthy();
    });

    it('should hasAssociation#NOT', () => {
      CHANNEL_COMPANY_ASSOCIATION_DATA.id = GuidDefaultData;
      const hasAssociation = component.hasAssociation(CHANNEL_COMPANY_ASSOCIATION_DATA);

      expect(hasAssociation).toBeFalsy();
    });
  });

});
