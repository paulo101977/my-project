import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Channel } from '../../../../shared/models/integration/channel';

@Component({
  selector: 'app-client-data-channel-remove',
  templateUrl: './client-data-channel-remove.component.html',
})
export class ClientDataChannelRemoveComponent implements OnInit {
  private item: Channel;

  @Input()
  get selectedItem() {
    return this.item;
  }

  set selectedItem(val) {
    if (val) {
      this.item = val;
    }
  }

  @Output() dataChange = new EventEmitter<Channel>();
  @Output() exitChange = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  public cancelRequest() {
    this.exitChange.emit();
  }

  public deleteData(): void {
    this.dataChange.emit(this.selectedItem);
  }
}
