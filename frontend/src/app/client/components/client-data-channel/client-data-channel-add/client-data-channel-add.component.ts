import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AutocompleteConfig } from '../../../../shared/models/autocomplete/autocomplete-config';
import { ChannelResource } from '../../../../shared/resources/channel/channel.resource';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { Channel, ChannelClientAssociation } from 'app/shared/models/integration/channel';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { ChannelService } from 'app/integration/services/channel.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';

@Component({
  selector: 'app-client-data-channel-add',
  templateUrl: './client-data-channel-add.component.html',
})
export class ClientDataChannelAddComponent implements OnInit {

  @Input() channelsList: ChannelClientAssociation[];
  @Output() dataChange = new EventEmitter<Array<ChannelClientAssociation>>();
  @Output() exitChange = new EventEmitter();

  // Info
  public i18n: any;

  public columns: any[];
  public formDataGroupChannel: FormGroup;

  // Autcomplete dependencies
  public autocompleteCodeConfig: AutocompleteConfig;
  public autoCompleteCodeResultList: any[];

  // Modal Dependencies
  public modalTitle: string;
  public dataModalList: ChannelClientAssociation[] = [];
  public itemSelected: Channel;

  // Button dependencies
  public associateBtnConfig: ButtonConfig;
  public cancelBtnConfig: ButtonConfig;
  public saveBtnConfig: ButtonConfig;

  constructor(
    public translate: TranslateService,
    public channelResource: ChannelResource,
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private channelService: ChannelService,
    private toaster: ToasterEmitService,
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  private setTranslate() {
    this.i18n = {
      table: {
        emptyMessage: this.translate.instant('variable.emptyContentM.title', {
          labelName: this.translate.instant('label.channel')
        })
      },
      actions: {
        cancel: this.translate.instant('action.cancel'),
        confirm: this.translate.instant('action.confirm'),
        associate: this.translate.instant('action.associate')
      },
      labels: {
        code: this.translate.instant('label.code'),
        description: this.translate.instant('label.description'),
        origin: this.translate.instant('label.origin'),
        channel: this.translate.instant('label.channel'),
        companyID: this.translate.instant('label.companyId')
      },
      placeholders: {
        codeOrDescription: this.translate.instant('placeholder.searchByCodeDescription')
      }
    };
  }

  private setVars() {
    this.setTranslate();
    this.setColumnsName();
    this.configureAutoComplete();
    this.setFormChannelModal();
    this.setBtnConfig();
  }

  private configureAutoComplete(): void {
    this.autocompleteCodeConfig = new AutocompleteConfig();
    this.autocompleteCodeConfig.dataModel = 'code';
    this.autocompleteCodeConfig.resultList = [];
    this.autocompleteCodeConfig.callbackToSearch = $event => this.getChannelsBySearch($event);
    this.autocompleteCodeConfig.callbackToGetSelectedValue = data => this.itemSelected = data;
    this.autocompleteCodeConfig.fieldObjectToDisplay = 'templateCode';
    this.autocompleteCodeConfig.placeholder = this.i18n.placeholders.codeOrDescription;
  }

  public getChannelsBySearch($event) {
    const term: string = $event.query;
    this.channelResource
      .getAllChannelList({searchData: term})
      .subscribe(({items}) =>
        this.autoCompleteCodeResultList = items
          .map(item => {
            item['templateCode'] = `${item.description} - ${item.channelCodeName}`;
            return item;
          })
      );
  }

  private setColumnsName(): void {
    this.columns = [
      {name: this.i18n.labels.code, prop: 'channelCodeName'},
      {name: this.i18n.labels.description, prop: 'channelDescription'},
      {name: this.i18n.labels.origin, prop: 'businessSourceDescription'},
      {name: this.i18n.labels.companyID, prop: 'companyId'}
    ];
  }

  private setFormChannelModal() {
    this.formDataGroupChannel = this.formBuilder.group({
      channelCode: ['', [Validators.required]],
      companyId: ['', [Validators.required]],
    });

    this.formDataGroupChannel.valueChanges
      .subscribe(() => {
        this.associateBtnConfig.isDisabled = !this.formDataGroupChannel.valid;
      });
  }

  private setBtnConfig() {
    this.associateBtnConfig = this.sharedService.getButtonConfig(
      'associate',
      () => this.associate(),
      this.i18n.actions.associate,
      ButtonType.Secondary,
      ButtonSize.Normal,
      true
    );

    this.cancelBtnConfig = this.sharedService.getButtonConfig(
      'cancel',
      () => this.cancel(),
      this.i18n.actions.cancel,
      ButtonType.Secondary,
      ButtonSize.Normal
    );

    this.saveBtnConfig = this.sharedService.getButtonConfig(
      'save',
      () => this.save(),
      this.i18n.actions.confirm,
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  public save(): void {
    if (this.dataModalList.length) {
      this.dataChange.emit([...this.dataModalList]);
      this.resetModal();
    }
  }

  public cancel() {
    this.exitChange.emit();
    this.resetModal();
  }

  public deleteChannel(item: ChannelClientAssociation) {
    const removeIndex = this.dataModalList.indexOf(item);
    this.dataModalList.splice(removeIndex, 1);
    this.dataModalList = [...this.dataModalList];
    this.setStatusSaveButton();
  }

  public associate(): void {
    const {valid} = this.formDataGroupChannel;

    if (valid && this.itemSelected) {
      const companyId = this.formDataGroupChannel.get('companyId').value;
      const channelCompany = this.parsingDataFromBackend(this.itemSelected, companyId);

      this.channelResource.checkAvailableChannelAssociation(channelCompany.channelId, companyId)
        .subscribe(response => {
            if (this.channelService.channelAlreadyExists(channelCompany, this.dataModalList)
              || this.channelService.channelAlreadyExists(channelCompany, this.channelsList)
            ) {
              this.toaster.emitChange(SuccessError.error, 'alert.channelAlreadyExistsInClient');
              return;
            }

            // shalow copy
            this.dataModalList = [channelCompany, ...this.dataModalList];

            this.resetItemSelected();
            this.formDataGroupChannel.reset();
            this.setStatusSaveButton();
          });
    }
  }

  private parsingDataFromBackend(channel: Channel, companyId) {
    return <ChannelClientAssociation>{
      id: GuidDefaultData,
      companyId: companyId,
      channelId: channel.id,
      channelCodeName: channel.channelCodeName,
      channelDescription: channel.description,
      businessSourceDescription: channel.businessSourceName,
      isActive: true
    };
  }

  private resetItemSelected() {
    this.itemSelected = null;
  }

  private resetDataModalList() {
    this.dataModalList = [];
  }

  private resetModal() {
    this.resetDataModalList();
    this.resetItemSelected();
    this.formDataGroupChannel.reset();
  }

  private setStatusSaveButton() {
    this.saveBtnConfig.isDisabled = !this.dataModalList.length;
  }

}
