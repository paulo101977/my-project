import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {
  ClientDataChannelAddComponent
} from 'app/client/components/client-data-channel/client-data-channel-add/client-data-channel-add.component';
import { ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { of } from 'rxjs';
import { ChannelResource } from 'app/shared/resources/channel/channel.resource';
import { CHANNEL_COMPANY_ASSOCIATION_DATA, channelData } from 'app/integration/components/mock/channel-data';
import { TranslateModule } from '@ngx-translate/core';
import { ChannelService } from 'app/integration/services/channel.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';

describe('ClientDataChannelAddComponent', () => {
  let component: ClientDataChannelAddComponent;
  let fixture: ComponentFixture<ClientDataChannelAddComponent>;

  let channelResource;
  let channelService;
  let toaster;

  const channelResourceStub: Partial<ChannelResource> = {
    getAllChannelList: ({searchData}) => of(<any>{}),
    checkAvailableChannelAssociation: () => of(<boolean>{})
  };
  const channelServiceStub: Partial<ChannelService> = {
    channelAlreadyExists: () => <boolean>{},
  };
  const toasterStub: Partial<ToasterEmitService> = {
    emitChange: () => {},
  };

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: [TranslateModule.forRoot()],
        providers: [
          {
            provide: ChannelResource,
            useValue: channelResourceStub
          },
          {
            provide: ChannelService,
            useValue: channelServiceStub
          },
          {
            provide: ToasterEmitService,
            useValue: toasterStub
          },
          FormBuilder
        ],
        declarations: [ClientDataChannelAddComponent],
        schemas: [NO_ERRORS_SCHEMA],
      });

    fixture = TestBed.createComponent(ClientDataChannelAddComponent);
    component = fixture.componentInstance;

    channelResource = TestBed.get(ChannelResource);
    channelService = TestBed.get(ChannelService);
    toaster = TestBed.get(ToasterEmitService);

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('set settings', () => {
    it('should configureAutoComplete', () => {
      component['configureAutoComplete']();

      expect(component.autocompleteCodeConfig.dataModel).toEqual('code');
      expect(component.autocompleteCodeConfig.resultList).toEqual([]);
      expect(component.autocompleteCodeConfig.callbackToSearch).toEqual(jasmine.any(Function));
      expect(component.autocompleteCodeConfig.callbackToGetSelectedValue).toEqual(jasmine.any(Function));
      expect(component.autocompleteCodeConfig.fieldObjectToDisplay).toEqual('templateCode');
      expect(component.autocompleteCodeConfig.placeholder).toEqual(component.i18n.placeholders.codeOrDescription);
    });

    it('should setColumnsName', () => {
      component['setColumnsName']();

      expect(component.columns[0]).toEqual({name: component.i18n.labels.code, prop: 'channelCodeName'});
      expect(component.columns[1]).toEqual({name: component.i18n.labels.description, prop: 'channelDescription'});
      expect(component.columns[2]).toEqual({name: component.i18n.labels.origin, prop: 'businessSourceDescription'});
      expect(component.columns[3]).toEqual({name: component.i18n.labels.companyID, prop: 'companyId'});
    });

    it('should setFormChannelModal', () => {
      component['setFormChannelModal']();

      expect(component.formDataGroupChannel.contains('channelCode')).toBeTruthy();
      expect(component.formDataGroupChannel.contains('companyId')).toBeTruthy();
    });

    it('should setBtnConfig', () => {
      component['setBtnConfig']();

      expect(component.associateBtnConfig.id).toEqual('associate');
      expect(component.associateBtnConfig.textButton).toEqual(component.i18n.actions.associate);
      expect(component.associateBtnConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.associateBtnConfig.buttonSize).toEqual(ButtonSize.Normal);
      expect(component.associateBtnConfig.isDisabled).toBeTruthy();

      expect(component.cancelBtnConfig.id).toEqual('cancel');
      expect(component.cancelBtnConfig.textButton).toEqual(component.i18n.actions.cancel);
      expect(component.cancelBtnConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.cancelBtnConfig.buttonSize).toEqual(ButtonSize.Normal);

      expect(component.saveBtnConfig.id).toEqual('save');
      expect(component.saveBtnConfig.textButton).toEqual(component.i18n.actions.confirm);
      expect(component.saveBtnConfig.buttonType).toEqual(ButtonType.Primary);
      expect(component.saveBtnConfig.buttonSize).toEqual(ButtonSize.Normal);
      expect(component.saveBtnConfig.isDisabled).toBeTruthy();
    });
  });

  describe('actions', () => {
    it('should getChannelsBySearch#found filter', () => {
      spyOn(channelResource, 'getAllChannelList').and.returnValue(of({items: [channelData]}));

      component.getChannelsBySearch({query: '000'});

      expect(channelResource.getAllChannelList).toHaveBeenCalled();
      expect(component.autoCompleteCodeResultList).toEqual([channelData]);
    });

    it('should getChannelsBySearch#NOT found filter', () => {
      spyOn(channelResource, 'getAllChannelList').and.returnValue(of({items: []}));

      component.getChannelsBySearch({query: '111'});

      expect(channelResource.getAllChannelList).toHaveBeenCalledWith({searchData: '111'});
      expect(component.autoCompleteCodeResultList).toEqual([]);
    });

    it('should associate', () => {
      spyOn<any>(component, 'resetItemSelected');
      spyOn(component.formDataGroupChannel, 'reset');
      spyOn<any>(component, 'setStatusSaveButton');
      spyOn(channelService, 'channelAlreadyExists').and.returnValue(false);
      spyOn(channelResource, 'checkAvailableChannelAssociation').and.returnValue(of(true));

      component.formDataGroupChannel.patchValue({
        channelCode: 'CODE 000',
        companyId: 'COMPANY-f8f2'
      });
      component.itemSelected = channelData;
      component.channelsList = [];
      component.dataModalList = [];

      component.associate();

      expect(channelService.channelAlreadyExists).toHaveBeenCalled();
      expect(component['resetItemSelected']).toHaveBeenCalled();
      expect(component.formDataGroupChannel.reset).toHaveBeenCalled();
      expect(component['setStatusSaveButton']).toHaveBeenCalled();
      expect(channelResource.checkAvailableChannelAssociation)
        .toHaveBeenCalledWith('3751359d-f8f2-4d0d-9456-13cbd750d1ed', 'COMPANY-f8f2');
      expect(component.dataModalList[0].channelCodeName).toEqual('CODE 000');
      expect(component.dataModalList[0].companyId).toEqual('COMPANY-f8f2');
    });

    it('should associate#NOT associate - channel already exists in client', () => {
      spyOn(component['toaster'], 'emitChange');

      component.formDataGroupChannel.patchValue({
        channelCode: 'CODE 000',
        companyId: 'COMPANY-f8f2'
      });
      component.itemSelected = channelData;
      CHANNEL_COMPANY_ASSOCIATION_DATA.channelId = channelData.id;
      component.channelsList = [CHANNEL_COMPANY_ASSOCIATION_DATA];

      component.associate();

      expect(component['toaster'].emitChange).toHaveBeenCalledWith(SuccessError.error, 'alert.channelAlreadyExistsInClient');
    });

    it('should save', () => {
      spyOn(component.dataChange, 'emit');
      spyOn<any>(component, 'resetModal');

      component.dataModalList = [CHANNEL_COMPANY_ASSOCIATION_DATA];

      component.save();

      expect(component.dataChange.emit).toHaveBeenCalledWith(component.dataModalList);
      expect(component['resetModal']).toHaveBeenCalled();
    });

    it('should save#not save - empty list', () => {
      spyOn(component.dataChange, 'emit');
      spyOn<any>(component, 'resetModal');

      component.dataModalList = [];

      component.save();

      expect(component.dataChange.emit).not.toHaveBeenCalledWith(component.dataModalList);
      expect(component['resetModal']).not.toHaveBeenCalled();
    });

    it('should cancel', () => {
      spyOn(component.exitChange, 'emit');
      spyOn<any>(component, 'resetModal');

      component.cancel();

      expect(component.exitChange.emit).toHaveBeenCalled();
      expect(component['resetModal']).toHaveBeenCalled();
    });

    it('should deleteChannel', () => {
      spyOn<any>(component, 'setStatusSaveButton');

      component.dataModalList = [CHANNEL_COMPANY_ASSOCIATION_DATA];

      component.deleteChannel(CHANNEL_COMPANY_ASSOCIATION_DATA);

      expect(component.dataModalList.length).toEqual(0);
      expect(component['setStatusSaveButton']).toHaveBeenCalled();
    });
  });

  describe('actions config', () => {
    it('should resetItemSelected', () => {
      component['resetItemSelected']();

      expect(component.itemSelected).toBeNull();
    });

    it('should resetDataModalList', () => {
      component['resetDataModalList']();

      expect(component.dataModalList.length).toEqual(0);
    });

    it('should resetModal', () => {
      spyOn<any>(component, 'resetDataModalList');
      spyOn<any>(component, 'resetItemSelected');
      spyOn(component.formDataGroupChannel, 'reset');

      component['resetModal']();

      expect(component['resetDataModalList']).toHaveBeenCalled();
      expect(component['resetItemSelected']).toHaveBeenCalled();
      expect(component.formDataGroupChannel.reset).toHaveBeenCalled();
    });

    it('should setStatusSaveButton#disabled', () => {
      component.dataModalList = [];

      component['setStatusSaveButton']();

      expect(component.saveBtnConfig.isDisabled).toBeTruthy();
    });

    it('should setStatusSaveButton#NOT disabled', () => {
      component.dataModalList = [CHANNEL_COMPANY_ASSOCIATION_DATA];

      component['setStatusSaveButton']();

      expect(component.saveBtnConfig.isDisabled).toBeFalsy();
    });
  });

});
