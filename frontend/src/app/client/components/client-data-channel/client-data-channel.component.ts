import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import * as _ from 'lodash';
import { ChannelClientAssociation } from '../../../shared/models/integration/channel';
import { TranslateService } from '@ngx-translate/core';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';

@Component({
  selector: 'app-client-data-channel',
  templateUrl: './client-data-channel.component.html',
})
export class ClientDataChannelComponent implements OnInit {

  // Info
  public labelName: string;

  // Table dependencies
  public channelsList: Array<ChannelClientAssociation> = [];
  public channelsListBack: Array<ChannelClientAssociation> = [];

  public hasData: boolean;
  public columns: Array<any>;
  public itemsOption: Array<any>;

  // Modal dependencies
  public modalTitle: string;
  public itemSelected: ChannelClientAssociation;
  public itemIndex: number;

  @Input()
  get channels() {
    return this.channelsList;
  }

  @Output() channelsChange = new EventEmitter();

  set channels(channels) {
    if (channels) {
      this.channelsList = channels;
      this.channelsListBack = this.channelsList;
      this.hasData = this.channelsList.length > 0;
      this.channelsChange.emit(this.channelsList);
    }
  }

  @ViewChild('editDeleteTemplate') editDeleteTemplate: ElementRef;

  constructor(
    public modal: ModalEmitToggleService,
    private translate: TranslateService
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  public setVars() {
    this.setColumnsName();
    this.itemsOption = [
      {title: 'commomData.edit', callbackFunction: this.goToEdit},
      {title: 'commomData.delete', callbackFunction: this.delete},
    ];
    this.labelName = this.translate.instant('label.channel');
  }

  public setColumnsName(): void {
    this.columns = [
      {name: 'label.code', prop: 'channelCodeName'},
      {name: 'label.description', prop: 'channelDescription'},
      {name: 'label.origin', prop: 'businessSourceDescription'},
      {name: 'label.companyId', prop: 'companyId'},
      {
        cellTemplate: this.editDeleteTemplate,
        name: '',
        cellClass: 'thf-u-no-padding',
        width: 50,
        resizeable: false,
        draggable: false,
        canAutoResize: false
      }
    ];
  }

  public toggleModal(): void {
    this.modalTitle = this.translate.instant('action.lbAssociate', {
      labelName: this.labelName
    });
    this.modal.emitToggleWithRef('newDataChannel');
  }

  public toggleModalEdit(): void {
    this.itemSelected = null;
    this.modal.emitToggleWithRef('editDataChannel');
  }

  public goToEdit = (item: ChannelClientAssociation) => {
    this.modalTitle = this.translate.instant('action.lbEdit', {
      labelName: this.labelName
    });
    this.toggleModalEdit();
    this.itemSelected = item;
  }

  public changeDataSavedFromModel(data: ChannelClientAssociation[]) {
    this.channels = this.channels.concat(data);
    this.toggleModal();
  }

  public changeDataEditedFromModel(data) {
    const itemIndex = _.findIndex(this.channelsList, channel => channel.id == data.id);
    this.channels.splice(itemIndex, 1, <ChannelClientAssociation>data);
    this.channels = [...this.channels];

    this.toggleModalEdit();
  }

  public search(term: string): void {
    const regex = new RegExp(term, 'i');
    this.channelsList = this.channelsListBack.filter(
      item =>
        regex.test(item.channelDescription)
        || regex.test(item.channelCodeName)
        || regex.test(item.businessSourceDescription)
    );
  }

  public hasAssociation(channel: ChannelClientAssociation) {
    return channel.id != GuidDefaultData;
  }

  public delete = (item: ChannelClientAssociation) => {
    const removeIndex = this.channels.indexOf(item);
    this.channels.splice(removeIndex, 1);
    this.channels = [...this.channels];
  }

}
