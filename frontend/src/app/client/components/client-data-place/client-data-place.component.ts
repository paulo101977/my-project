import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { CustomerStation } from '../../../shared/models/integration/customerStation';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-client-data-place',
  templateUrl: './client-data-place.component.html',
})
export class ClientDataPlaceComponent implements OnInit {
  dataList: Array<CustomerStation> = [];
  dataListBack: Array<CustomerStation> = [];

  // to navigate to other route
  private propertyId;

  @Input()
  get places() {
    return this.dataList;
  }

  @Output() dataChange = new EventEmitter();

  set places(val) {
    if (val) {
      this.dataList = val;
      this.dataListBack = val;
      this.hasData = this.dataList.length > 0;
      this.dataChange.emit(this.dataList);
    }
  }

  public hasData: boolean;
  public columns: Array<any>;
  public itemsOption: Array<any>;
  public formDataGroup: FormGroup;

  private editingItemIndex: number;

  // Modal Dependencies
  public modalTitle: string;
  public itemToRemove: CustomerStation;

  constructor(
    public translateService: TranslateService,
    public modalEmitToggleService: ModalEmitToggleService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.setVars();

    this.route.params.subscribe( params => {
      this.propertyId = +params.property;
    });
  }

  private setVars() {
    this.setColumnsName();
    this.setFormModal();
    this.editingItemIndex = -1;

    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.goToEdit },
      { title: 'commomData.delete', callbackFunction: this.askRemoveItem },
    ];
  }

  private setColumnsName(): void {
    this.columns = [
      { name: 'label.stationName', prop: 'stationName' },
      { name: 'label.category', prop: 'category'},
      { name: 'label.contact', prop: 'contact'},
      { name: 'label.email', prop: 'email' },
      { name: 'label.phone', prop: 'phone' }
    ];
  }

  public search(term: any): void {
    if (typeof term === 'string' && term !== '') {
      this.dataList = this.dataList.filter(
        item =>
          new RegExp(term, 'i').test(item.stationName) ||
              new RegExp(term, 'i').test(item.client)
      );
    } else {
      this.dataList = this.dataListBack;
    }
  }

  private setFormModal() {
    this.formDataGroup = this.formBuilder.group({
      stationName: ['', [Validators.required]],
      category: ['', [Validators.required]],
      contact: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone: ['', [Validators.required]]
    });
  }

  public toggleModal(): void {
    this.router.navigate(
      [
        `/p/${this.propertyId}/integration/customer/new`, { mode: 'insert' }
      ],
      { relativeTo: this.route }
    );
  }

  public goToEdit = (item: CustomerStation) => {
    const row = <CustomerStation>item;
    const { id } = row;

    if (typeof id !== 'undefined') {
      this.router.navigate(
        [
          `/p/${this.propertyId}/integration/customer/edit`, { mode: 'edit', customerId:  id }
        ],
        { relativeTo: this.route}
      );
    } else {
      console.error('id undefined');
    }
  }


  public toggleRemoveItem() {
    this.itemToRemove = null;
    this.modalEmitToggleService.emitToggleWithRef('removePlace');
  }

  public askRemoveItem = item => {
    this.toggleRemoveItem();
    this.itemToRemove = item;
  }

  public removeItem() {
    const removeIndex = this.places.indexOf(this.itemToRemove);
    this.places.splice(removeIndex, 1);
    this.places = [...this.places];
    this.toggleRemoveItem();
  }

}
