import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { ClientContactDto } from '../../../shared/models/dto/client/client-contact-dto';
import { GuidDefaultData } from '../../../shared/mock-data/guid-default-data';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';

@Component({
  selector: 'app-client-data-contact',
  templateUrl: './client-data-contact.component.html',
})
export class ClientDataContactComponent implements OnInit {
  contactList: ClientContactDto[] = [];
  contactListBack: Array<ClientContactDto> = [];

  @Input()
  get contacts() {
    return this.contactList;
  }

  @Output() contactsChange = new EventEmitter();

  set contacts(val) {
    if (val) {
      this.contactList = val;
      this.contactListBack = val;
      this.hasDataContact = this.contactList.length > 0;
      this.contactsChange.emit(this.contactList);
    }
  }

  public hasDataContact: boolean;
  public columns: Array<any>;
  public itemsOption: Array<any>;
  public formDataGroup: FormGroup;

  private editingContactIndex: number;

  // Modal Dependencies
  public modalTitle: string;
  public itemToRemove: ClientContactDto;

  constructor(
    public translateService: TranslateService,
    public modalEmitToggleService: ModalEmitToggleService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.setColumnsName();
    this.setFormModal();
    this.editingContactIndex = -1;

    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.goToEdit },
      { title: 'commomData.delete', callbackFunction: this.askRemoveItem },
    ];
  }

  private setColumnsName(): void {
    this.columns = [
      { name: 'clientModule.edit.contacts.columns.contactName', prop: 'name' },
      { name: 'clientModule.edit.contacts.columns.function', prop: 'occupationName' },
      { name: 'clientModule.edit.contacts.columns.phone', prop: 'phoneNumber' },
      { name: 'clientModule.edit.contacts.columns.email', prop: 'email' },
    ];
  }

  private setFormModal() {
    this.formDataGroup = this.formBuilder.group({
      personId: GuidDefaultData,
      name: ['', [Validators.required]],
      occupationName: ['', [Validators.required]],
      phoneNumber: '',
      email: ['', [Validators.required]],
    });
  }

  public search(term: any): void {

    if (typeof term === 'string' && term !== '') {
      this.contactList = this.contactList.filter(
        item =>
          new RegExp(term, 'i').test(item.name) ||
          new RegExp(term, 'i').test(item.email) ||
          new RegExp(term, 'i').test(item.phoneNumber)
      );
    } else {
      // the original resource
      this.contactList = this.contactListBack;
    }
  }

  public toggleModal(): void {
    this.modalTitle = 'clientModule.edit.contacts.new';
    this.editingContactIndex = -1;
    this.setFormModal();
    this.modalEmitToggleService.emitToggleWithRef('newDataContact');
  }

  public goToEdit = (item: ClientContactDto) => {
    this.toggleModal();
    this.modalTitle = 'clientModule.edit.contacts.edit';
    this.editingContactIndex = _.findIndex(this.contactList, item);

    this.formDataGroup.patchValue({
      personId: item.personId,
      name: item.name,
      occupationName: item.occupationName,
      phoneNumber: item.phoneNumber,
      email: item.email,
    });
  }

  public saveDataContact({ value, valid }: { value: ClientContactDto; valid: boolean }): void {
    if (valid) {
      if (this.editingContactIndex != -1) {
        this.parsingOldDataFromNewValues(value);
      } else {
        this.contacts = [this.parsingDataFromBackend(value), ...this.contacts];
      }

      this.formDataGroup.reset();
      this.toggleModal();
    }
  }

  public toggleRemoveItem() {
    this.itemToRemove = null;
    this.modalEmitToggleService.emitToggleWithRef('removeContact');
  }

  public askRemoveItem = item => {
    this.toggleRemoveItem();
    this.itemToRemove = item;
  }

  public removeItem() {
    const removeIndex = this.contacts.indexOf(this.itemToRemove);
    this.contacts.splice(removeIndex, 1);
    this.contacts = [...this.contacts];
    this.toggleRemoveItem();
  }

  public parsingDataFromBackend(dataContact): ClientContactDto {
    return {
      personId: dataContact.personId,
      name: dataContact.name,
      occupationName: dataContact.occupationName,
      phoneNumber: dataContact.phoneNumber,
      email: dataContact.email,
    };
  }

  public parsingOldDataFromNewValues(dataContact: any): void {
    const newContact = this.parsingDataFromBackend(dataContact);
    this.contacts.splice(this.editingContactIndex, 1, newContact);
    this.contacts = [...this.contacts];
  }
}
