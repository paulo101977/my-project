import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as _ from 'lodash';
import { ClientDataContactComponent } from './client-data-contact.component';
import { ClientContactDto } from 'app/shared/models/dto/client/client-contact-dto';

describe('ClientDataContactComponent', () => {
  let component: ClientDataContactComponent;
  let fixture: ComponentFixture<ClientDataContactComponent>;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), ReactiveFormsModule, FormsModule],
      providers: [],
      declarations: [ClientDataContactComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(ClientDataContactComponent);
    component = fixture.componentInstance;

    component.formDataGroup = formBuilder.group({
      id: 0,
      name: '',
      email: '',
      occupationId: '',
      occupationName: '',
      personId: '',
      phoneNumber: '',
    });

    component.contacts = [
      <ClientContactDto>{
        personId: '5608fd69-741a-4700-9d36-c1f5b899f1f4',
        name: 'Test Santos 1',
        occupationId: 1,
        occupationName: 'GERENTE',
        phoneNumber: '22010111',
        email: 'test1@email.com',
        id: 0,
      },
      <ClientContactDto>{
        personId: '5608fd69-741a-4700-9d36-c1f5b899f1f4',
        name: 'Test Santos 2',
        occupationId: 1,
        occupationName: 'GERENTE',
        phoneNumber: '22010111',
        email: 'test24@email.com',
        id: 1,
      },
    ];

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should setVars', () => {
      spyOn<any>(component, 'setColumnsName');
      spyOn<any>(component, 'setFormModal');

      component['setVars']();

      expect(component['editingContactIndex']).toEqual(-1);
      expect(component.contactList).toEqual(component.contacts);
      expect(component.hasDataContact).toBeTruthy();
      expect(component['setColumnsName']).toHaveBeenCalled();
      expect(component['setFormModal']).toHaveBeenCalled();
    });

    it('should setColumnsName', () => {
      component['setColumnsName']();

      expect(component.columns[0]).toEqual({ name: 'clientModule.edit.contacts.columns.contactName', prop: 'name' });
      expect(component.columns[1]).toEqual({
        name: 'clientModule.edit.contacts.columns.function',
        prop: 'occupationName',
      });
      expect(component.columns[2]).toEqual({ name: 'clientModule.edit.contacts.columns.phone', prop: 'phoneNumber' });
      expect(component.columns[3]).toEqual({ name: 'clientModule.edit.contacts.columns.email', prop: 'email' });
    });

    it('should toggleModal', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
      spyOn<any>(component, 'setFormModal');

      component.toggleModal();

      expect(component.modalTitle).toEqual('clientModule.edit.contacts.new');
      expect(component['editingContactIndex']).toEqual(-1);
      expect(component['setFormModal']).toHaveBeenCalled();
      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('newDataContact');
    });

    it('should goToEdit', () => {
      const indexTest = 0;
      spyOn(component, 'toggleModal');
      spyOn(_, 'findIndex').and.returnValue(indexTest);
      const item = component.contactList[indexTest];

      component.goToEdit(item);

      expect(component.modalTitle).toEqual('clientModule.edit.contacts.edit');
      expect(component['editingContactIndex']).toEqual(indexTest);
      expect(component.formDataGroup.get('name').value).toEqual(component.contactList[0].name);
      expect(component.formDataGroup.get('email').value).toEqual(component.contactList[0].email);
      expect(component.formDataGroup.get('occupationName').value).toEqual(component.contactList[0].occupationName);
      expect(component.formDataGroup.get('personId').value).toEqual(component.contactList[0].personId);
      expect(component.formDataGroup.get('phoneNumber').value).toEqual(component.contactList[0].phoneNumber);
      expect(component.toggleModal).toHaveBeenCalled();
    });

    it('should saveDataDocument', () => {
      spyOn<any>(component, 'parsingDataFromBackend').and.returnValue(component.contactList[0]);
      spyOn(component.formDataGroup, 'reset');
      spyOn(component, 'toggleModal');

      component['editingContactIndex'] = -1;

      component.saveDataContact(component.formDataGroup);

      expect(component['parsingDataFromBackend']).toHaveBeenCalledWith(component.formDataGroup.value);
      expect(component.formDataGroup.reset).toHaveBeenCalled();
      expect(component.toggleModal).toHaveBeenCalled();
      component.removeItem();
    });

    it('should editDataDocument', () => {
      spyOn<any>(component, 'parsingOldDataFromNewValues');
      spyOn(component.formDataGroup, 'reset');
      spyOn(component, 'toggleModal');

      component['editingContactIndex'] = 0;

      component.saveDataContact(component.formDataGroup);

      expect(component['parsingOldDataFromNewValues']).toHaveBeenCalledWith(component.formDataGroup.value);
      expect(component.formDataGroup.reset).toHaveBeenCalled();
      expect(component.toggleModal).toHaveBeenCalled();
    });

    it('should removeItem', () => {
      component.removeItem();

      expect(component.contactList.length).toEqual(1);
      expect(component.contactList).not.toContain(component.contactList[1]);
    });
  });
});
