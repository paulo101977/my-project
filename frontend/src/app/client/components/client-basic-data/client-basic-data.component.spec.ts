import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ClientBasicDataComponent } from './client-basic-data.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';

describe('ClientBasicDataComponent', () => {
  let component: ClientBasicDataComponent;
  let fixture: ComponentFixture<ClientBasicDataComponent>;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), ReactiveFormsModule, FormsModule, HttpClientTestingModule, RouterTestingModule, CoreModule],
      declarations: [ClientBasicDataComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ClientBasicDataComponent);
    component = fixture.componentInstance;

    component.formDataGroup = formBuilder.group({
      companyId: '',
      id: '',
      personId: '',
      propertyId: '',
      personType: '',
      shortName: '',
      tradeName: '',
      email: '',
      homePage: '',
      phoneNumber: '',
      cellPhoneNumber: '',
      isActive: '',
      isAcquirer: '',
      dateOfBirth: '',
      countrySubdivisionId: '',
      group: '',
      propertyCompanyClientCategoryId: '',
      externalCode: ''
    });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
