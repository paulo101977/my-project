import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { PropertyCompanyClientCategory } from '../../../shared/models/property-company-client-category';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { CategoryResource } from '../../../shared/resources/category/category.resource';

@Component({
  selector: 'app-client-basic-data',
  templateUrl: './client-basic-data.component.html',
})
export class ClientBasicDataComponent implements OnInit {
  @Input() formDataGroup: FormGroup;
  @Input() isPersonTypeL: boolean;

  public categoryOptionList: PropertyCompanyClientCategory[];

  constructor(private categoryResource: CategoryResource, private commonService: CommomService) {}

  ngOnInit() {
    this.getCategories();
  }

  public getCategories() {
    const propertyId = this.formDataGroup.get('propertyId').value;
    this.categoryResource.getCategory(propertyId).subscribe(response => {
      this.categoryOptionList = this.commonService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        response.items,
        'id',
        'propertyCompanyClientCategoryName',
      );
    });
  }
}
