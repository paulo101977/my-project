
import {of as observableOf,  Observable } from 'rxjs';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ClientListComponent } from './client-list.component';
import { ClientInfoData } from '../../../shared/mock-data/client-data';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
import { SizeOfSearchButtonEnum } from '../../../shared/models/config-header-page';
import { ClientResource } from '../../../shared/resources/client/client.resource';
import { ActivatedRoute } from '@angular/router';
import { environment } from '@environment';
import { InterceptorHandlerService,
  configureApi,
  ApiEnvironment,
  ThexApiTestingModule
} from '@inovacaocmnet/thx-bifrost';

describe('ClientListComponent', () => {
  let component: ClientListComponent;
  let fixture: ComponentFixture<ClientListComponent>;
  let httpMock: HttpTestingController;
  let resource: ClientResource;
  let client;

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    client = ClientInfoData;
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(),
        RouterTestingModule,
        HttpClientTestingModule,
        ThexApiTestingModule
      ],
      declarations: [ClientListComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: observableOf({ property: 1 }),
          },
        },
        InterceptorHandlerService,
        configureApi({
          environment: ApiEnvironment.Development
        })
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ClientListComponent);
    component = fixture.componentInstance;

    resource = TestBed.get(ClientResource);
    httpMock = TestBed.get(HttpTestingController);

    spyOn<any>(component, 'getClients');

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should setVars', () => {
      spyOn<any>(component, 'setConfigHeaderPage');
      spyOn<any>(component, 'setConfigTable');

      component['setVars']();

      expect(component.propertyId).toEqual(1);
      expect(component['setConfigHeaderPage']).toHaveBeenCalled();
      expect(component['setConfigTable']).toHaveBeenCalled();
      expect(component['getClients']).toHaveBeenCalled();
    });

    it('should setConfigTable', () => {
      spyOn<any>(component, 'setColumnsName');

      component['setConfigTable']();

      expect(component['setColumnsName']).toHaveBeenCalled();
    });

    it('should config HeaderPage', () => {
      component['setConfigHeaderPage']();

      expect(component.configHeaderPage.headerPageEnum).toEqual(HeaderPageEnum.List);
      expect(component.configHeaderPage.titlePage).toEqual('clientModule.list.pageHeader.title');
      expect(component.configHeaderPage.titleCreateButton).toEqual('clientModule.list.client.createNewClient');
      expect(component.configHeaderPage.sizeOfSearchButtonEnum).toEqual(SizeOfSearchButtonEnum.Medium);
      expect(component.configHeaderPage.goToCreatePage).toBeTruthy();
    });

    it('should goToCreatePage', () => {
      spyOn(component.router, 'navigate');
      component.goToCreatePage();
      expect(component.router.navigate).toHaveBeenCalledWith(['new'], { relativeTo: component['route'] });
    });

    it('should goToEditPage', () => {
      const item = { id: '9ae937da-4df3-4f2a-843d-b49286f41dfd' };
      spyOn(component.router, 'navigate');

      component.goToEditPage(item);

      expect(component.router.navigate).toHaveBeenCalledWith(['edit', item.id], { relativeTo: component['route'] });
    });

    it('should setSearchableItems', () => {
      component['setSearchableItems']();

      expect(component.searchableItems).not.toBeNull();
      expect(component.searchableItems.placeholderSearchFilter).toEqual('clientModule.list.client.filterSearchPlaceholder');
    });

    it('should setColumnsName', () => {
      component['setColumnsName']();

      expect(component.columns).not.toBeNull();
      expect(component.columns[0]).toEqual({ name: 'clientModule.list.collumns.type', prop: 'type' });
      expect(component.columns[1]).toEqual({ name: 'clientModule.list.collumns.name', prop: 'name' });
      expect(component.columns[2]).toEqual({ name: 'clientModule.list.collumns.document', prop: 'document' });
      expect(component.columns[3]).toEqual({ name: 'clientModule.list.collumns.email', prop: 'email' });
      expect(component.columns[4]).toEqual({ name: 'clientModule.list.collumns.phone', prop: 'phone' });
      expect(component.columns[5]).toEqual({ name: 'clientModule.list.collumns.category', prop: 'clientCategoryName' });
    });

    it('should updateList', () => {
      const clients = [];
      clients.push(ClientInfoData);

      component.updateList(clients);

      expect(component.hasClient).toBeTruthy();
      expect(component.pageItemsList).toEqual(clients);
    });
  });

  describe('on resources', () => {
    xit('should getClients', () => {
      const clients = [];
      clients.push(ClientInfoData);

      resource.getAllCompanyClientResultDto(1).subscribe(result => {
        expect(result.items.length).toEqual(1);
        expect(result.items[0]).toEqual(ClientInfoData);
      });

      const req = httpMock.expectOne(environment.urlBase + 'CompanyClient?PropertyId=1');
      req.flush({
        hasNext: false,
        items: clients,
        total: 1,
      });

      httpMock.verify();
    });

    it('should call loadpage e success message on change status Origin', () => {
      spyOn(component['clientResource'], 'updateClientStatus').and.returnValue(observableOf(client));
      spyOn(component['toasterEmitService'], 'emitChange');

      component.updateStatus(client);

      const message = component.translateService.instant('variable.lbActivatedWithSuccessM', {
        labelName: component.translateService.instant('label.client'),
      });

      expect(component['clientResource'].updateClientStatus).toHaveBeenCalledWith(client);
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(SuccessError.success, message);
    });
  });
});
