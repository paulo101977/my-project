import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GetAllCompanyClientResultDto } from '../../../shared/models/dto/client/get-all-company-client-result-dto';
import { HeaderPageEnum } from '../../../shared/models/header-page-enum';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { ConfigHeaderPage } from '../../../shared/models/config-header-page';
import { SizeOfSearchButtonEnum } from '../../../shared/models/config-header-page';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ClientResource } from '../../../shared/resources/client/client.resource';
import * as _ from 'lodash';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
})
export class ClientListComponent implements OnInit {
  public configHeaderPage: ConfigHeaderPage;
  public searchableItems: SearchableItems;

  public columns: Array<any>;
  public pageItemsList: Array<GetAllCompanyClientResultDto>;

  public hasClient = false;
  public propertyId: number;

  constructor(
    public translateService: TranslateService,
    private toasterEmitService: ToasterEmitService,
    private clientResource: ClientResource,
    public router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.setConfigHeaderPage();
    this.setConfigTable();
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.getClients();
    });
  }

  private setConfigTable() {
    this.setColumnsName();
  }

  private setConfigHeaderPage(): void {
    this.configHeaderPage = new ConfigHeaderPage(HeaderPageEnum.List);
    this.configHeaderPage.titlePage = 'clientModule.list.pageHeader.title';
    this.configHeaderPage.titleCreateButton = 'clientModule.list.client.createNewClient';
    this.configHeaderPage.goToCreatePage = this.goToCreatePage;
    this.configHeaderPage.sizeOfSearchButtonEnum = SizeOfSearchButtonEnum.Medium;
  }

  public goToCreatePage = () => {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

  public goToEditPage(item) {
    this.router.navigate(['edit', item.id], { relativeTo: this.route });
  }

  private setSearchableItems(): void {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.pageItemsList;
    this.searchableItems.placeholderSearchFilter = 'clientModule.list.client.filterSearchPlaceholder';
    this.searchableItems.tableRowTypeEnum = TableRowTypeEnum.Client;
  }

  private setColumnsName(): void {
    this.columns = [];

    this.translateService.get('clientModule.list.collumns.type').subscribe(value => {
      this.columns.push({ name: value, prop: 'type' });
    });
    this.translateService.get('clientModule.list.collumns.name').subscribe(value => {
      this.columns.push({ name: value, prop: 'name' });
    });
    this.translateService.get('clientModule.list.collumns.document').subscribe(value => {
      this.columns.push({ name: value, prop: 'document' });
    });
    this.translateService.get('clientModule.list.collumns.email').subscribe(value => {
      this.columns.push({ name: value, prop: 'email' });
    });
    this.translateService.get('clientModule.list.collumns.phone').subscribe(value => {
      this.columns.push({ name: value, prop: 'phone' });
    });
    this.translateService.get('clientModule.list.collumns.category').subscribe(value => {
      this.columns.push({ name: value, prop: 'clientCategoryName' });
    });
    this.translateService.get('clientModule.list.collumns.code').subscribe(value => {
      this.columns.push({ name: value, prop: 'externalCode' });
    });
  }

  private getClients(): void {
    this.clientResource.getAllCompanyClientResultDto(this.propertyId).subscribe(result => {
      this.pageItemsList = result.items;
      this.hasClient = !_.isEmpty(result.items);
      this.setSearchableItems();
    });
  }

  public updateStatus(clientItem: GetAllCompanyClientResultDto) {
    let message: string;
    if (clientItem.isActive) {
      message = this.translateService.instant('variable.lbActivatedWithSuccessM', {
        labelName: this.translateService.instant('label.client'),
      });
    } else {
      message = this.translateService.instant('variable.lbInactivatedWithSuccessM', {
        labelName: this.translateService.instant('label.client'),
      });
    }
    this.clientResource.updateClientStatus(clientItem).subscribe(result => {
      this.toasterEmitService.emitChange(SuccessError.success, message);
    });
  }

  public updateList(items: Array<GetAllCompanyClientResultDto>) {
    if (items) {
      this.hasClient = items.length > 0;
      this.pageItemsList = items;
    }
  }
}
