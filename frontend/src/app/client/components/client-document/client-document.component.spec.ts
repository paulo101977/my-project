import { of  } from 'rxjs';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule } from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA, SimpleChange} from '@angular/core';
import { FormBuilder, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { TextMaskModule } from 'angular2-text-mask';
import * as _ from 'lodash';
import { ClientDocumentComponent } from './client-document.component';
import { documentTypeListData } from 'app/shared/mock-data/client-document-type-data';
import { ClientDocumentDto } from 'app/shared/models/dto/client/client-document-dto';
import {ClientDocumentTypeEnum, ClientDocumentTypesDto} from 'app/shared/models/dto/client/client-document-types';
import { InterceptorHandlerService, SupportApiTestingModule } from '@inovacaocmnet/thx-bifrost';
import { createAccessorComponent } from '../../../../../testing';
import {DocumentTypeNaturalEnum} from 'app/shared/models/document-type';
import {CountryCodeEnum} from 'app/shared/models/country-code-enum';

describe('ClientDocumentComponent', () => {
  let component: ClientDocumentComponent;
  let fixture: ComponentFixture<ClientDocumentComponent>;

  const thxErrorStub = createAccessorComponent('thx-error');

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: [
          TranslateModule.forRoot(),
          ReactiveFormsModule,
          FormsModule,
          HttpClientModule,
          RouterTestingModule,
          TextMaskModule,
          SupportApiTestingModule,
        ],
        providers: [
          InterceptorHandlerService,
          FormBuilder
        ],
        declarations: [
          ClientDocumentComponent,
          thxErrorStub
        ],
        schemas: [NO_ERRORS_SCHEMA],
      });

    fixture = TestBed.createComponent(ClientDocumentComponent);
    component = fixture.componentInstance;

    spyOn<any>(component, 'parsingDataFromBackend').and.callFake((item) => {
      return item;
    });

    component.documents = [
      <ClientDocumentDto>{
        ownerId: 'eeae6861-e4f9-498e-8066-1e77f5db2305',
        documentTypeId: 1,
        documentTypeName: 'NIT',
        documentInformation: '134.001.222-00',
        id: 0,
      },
      <ClientDocumentDto>{
        ownerId: 'eeae6861-e4f9-498e-8066-1e77f5db2305',
        documentTypeId: 13,
        documentTypeName: 'Inscrição Estadual',
        documentInformation: '135.001.222-00',
        id: 0,
      },
    ];
    component.personType = new FormControl(ClientDocumentTypeEnum.Natural);
    component.countrySubdivisionId = 1;

    spyOn(component['documentResource'], 'getAll').and.returnValue(
      of({
        items: documentTypeListData,
        hasNext: false,
        total: documentTypeListData.length,
      }),
    );

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should setVars', () => {
      spyOn<any>(component, 'setColumnsName');
      spyOn<any>(component, 'setFormModal');

      component['setVars']();

      expect(component.editingDocumentIndex).toEqual(-1);
      expect(component.personType.value).toEqual('N');
      expect(component.documentsList).toEqual(component.documents);
      expect(component.allDocList).toEqual(documentTypeListData);
      expect(component.optionsDocListOriginal[0]).toEqual({
        key: documentTypeListData[0].id,
        value: documentTypeListData[0].name,
      });
      expect(component['setColumnsName']).toHaveBeenCalled();
      expect(component['setFormModal']).toHaveBeenCalled();
    });
  });

  describe('on methods', () => {

    it('should call nifValidation on ngChange', () => {
      spyOn<any>(component, 'nifValidation');

      component.ngOnChanges({
        countrySubdivisionId: new SimpleChange(null, 110, false)
      });

      expect(component['nifValidation']).toHaveBeenCalled();
    });

    it('should toggleModal edition', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
      spyOn<any>(component, 'resetForm');
      spyOn<any>(component, 'nonRepeatedDocuments');

      component.toggleModal();

      expect(component.modalTitle).toEqual('clientModule.edit.documents.new');
      expect(component.editingDocumentIndex).toEqual(-1);
      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('newDocument');
      expect(component['resetForm']).toHaveBeenCalled();
      expect(component['nonRepeatedDocuments']).toHaveBeenCalled();
    });

    it('should goToEdit', () => {
      const indexTest = 1;
      spyOn(component, 'toggleModal');
      spyOn(_, 'findIndex').and.returnValue(indexTest);
      const item = component.documents[indexTest];

      component.goToEdit(item);

      const {value} = component.formDataGroup;

      expect(component.modalTitle).toEqual('clientModule.edit.documents.edit');
      expect(component.editingDocumentIndex).toEqual(indexTest);
      expect(value.id).toEqual(item.id);
      expect(value.ownerId).toEqual(item.ownerId);
      expect(value.documentTypeId).toEqual(item.documentTypeId);
      expect(value.documentTypeName).toEqual(item.documentTypeName);
      expect(value.documentInformation).toEqual(item.documentInformation);
      expect(component.toggleModal).toHaveBeenCalled();
    });

    it('should saveDataDocument', () => {
      spyOn<any>(component, 'parsingOldDocumentDataFromNewValues');
      spyOn(component.formDataGroup, 'reset');
      spyOn(component, 'toggleModal');

      component.editingDocumentIndex = 0;

      component.saveDataDocument(component.formDataGroup);

      expect(component['parsingOldDocumentDataFromNewValues']).toHaveBeenCalledWith(component.formDataGroup.value);
      expect(component.formDataGroup.reset).toHaveBeenCalled();
      expect(component.toggleModal).toHaveBeenCalled();
    });

    it('should removeItem', () => {
      spyOn(component, 'toggleRemoveItem');
      component.itemToRemove = component.documentsList[1];

      component.removeItem();

      expect(component.documentsList.length).toEqual(1);
      expect(component.documentsList).not.toContain(component.documentsList[1]);
      expect(component.toggleRemoveItem).toHaveBeenCalled();
    });
  });

  describe('NIF validation', () => {
    describe('Pt-PT', () => {
      beforeEach(() => {
        component.allDocList = <ClientDocumentTypesDto[]>[
          {
            personType: null,
            countryCode: CountryCodeEnum.PT,
            countrySubdivisionId: 1,
            name: 'Port',
            stringFormatMask: null,
            regexValidationExpression: null,
            isMandatory: false,
            id: 1
          }
        ];
        component.countryCode = CountryCodeEnum.PT;

        component.formDataGroup.get('documentTypeId').setValue(DocumentTypeNaturalEnum.NIF);
      });

      it('should return invalid form nifValidation with no info', () => {
        component['nifValidation']();

        expect(component.formDataGroup.get('documentInformation').invalid).toBeTruthy();
      });

      it('should return invalid form nifValidation with wrong info', () => {
        component.formDataGroup.get('documentInformation').setValue('222',
          { eventEmit: false, onlySelf: true} );
        component['nifValidation']();

        expect(component.formDataGroup.get('documentInformation').invalid).toBeTruthy();
      });

      it('should return valid form nifValidation', () => {
        component.formDataGroup.get('documentInformation').setValue('258275812',
          { eventEmit: false, onlySelf: true} );
        component['nifValidation']();

        expect(component.formDataGroup.get('documentInformation').valid).toBeTruthy();
      });

    });

  });
});
