import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { ClientDocumentDto } from '../../../shared/models/dto/client/client-document-dto';
import { GuidDefaultData } from '../../../shared/mock-data/guid-default-data';
import { ClientDocumentTypesDto } from '../../../shared/models/dto/client/client-document-types';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { TranslateService } from '@ngx-translate/core';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { DocumentTypeResource } from '../../../shared/resources/document-type/document-type.resource';
import { DocumentTypeMaskService } from 'app/shared/services/shared/document-type-mask.service';
import { DocumentService } from '@inovacaocmnet/thx-bifrost';
import { NifValidatorService } from 'app/shared/services/nif-validator/nif-validator.service';
import { ValidatorFormService } from 'app/shared/services/shared/validator-form.service';

@Component({
  selector: 'app-client-document',
  templateUrl: './client-document.component.html',
})
export class ClientDocumentComponent implements OnInit, OnChanges {
  documentsList = [];

  @Output() documentsChange = new EventEmitter();

  get documents() {
    return this.documentsList;
  }

  @Input()
  set documents(val) {
    if (val) {
      this.documentsList = val.map(doc => this.parsingDataFromBackend(doc));
      this.documentsChange.emit(this.documentsList);
    }
  }

  @Input() personType: FormControl;
  @Input() countrySubdivisionId: number;
  @Input() countryCode: string;
  @Input() isRequired = true;

  public columns: Array<any>;
  public itemsOption: Array<any>;
  public formDataGroup: FormGroup;

  public allDocListOriginal: ClientDocumentTypesDto[];
  public allDocList: ClientDocumentTypesDto[];
  public optionsDocList = [];
  public optionsDocListOriginal = [];
  public editingDocumentIndex: number;

  // Modal Dependencies
  public modalTitle: string;
  public itemToRemove: ClientDocumentDto;

  constructor(
    private modalEmitToggleService: ModalEmitToggleService,
    private formBuilder: FormBuilder,
    private documentResource: DocumentTypeResource,
    private translateService: TranslateService,
    private toasterEmitService: ToasterEmitService,
    private commomService: CommomService,
    private documentMaskService: DocumentTypeMaskService,
    private documentService: DocumentService,
    private nifService: NifValidatorService,
    private validatorFormService: ValidatorFormService,
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty('countrySubdivisionId')) {
      this.updateDocumentTypes();
      this.nifValidation();
    }
  }

  private nifValidation() {
    if (this.formDataGroup) {
      const documentControl = this.formDataGroup.get('documentInformation');
      this.nifService.setNifValidatorRequired(
        this.formDataGroup,
        documentControl,
        this.countryCode,
        this.formDataGroup.get('documentTypeId').value,
        this.validatorFormService.invalidPortugueseNif
      );
    }
  }

  private setVars(): void {
    this.setColumnsName();
    this.setFormModal();
    this.editingDocumentIndex = -1;
    this.getDocumentTypes();
    this.personType.valueChanges.subscribe(() => {
      this.updateDocumentTypes();
    });

    this.itemsOption = [
      {title: 'commomData.edit', callbackFunction: (item) => this.goToEdit(item)},
      {title: 'commomData.delete', callbackFunction: (item) => this.askRemoveItem(item)},
    ];
  }


  private updateDocumentTypes() {
    this.allDocList = this.filterDocumentTypeListBy(this.allDocListOriginal, this.personType.value, this.countrySubdivisionId);
    this.optionsDocListOriginal = this.commomService.toOption(this.allDocList, 'id', 'name');
  }

  private getDocumentTypes(): void {
    this.documentResource.getAll().subscribe(result => {
      this.allDocListOriginal = result.items;
      this.allDocList = this.filterDocumentTypeListBy(result.items, this.personType.value, this.countrySubdivisionId);
      this.optionsDocListOriginal = this.commomService.toOption(this.allDocList, 'id', 'name');
      this.nonRepeatedDocuments();
    });
  }

  private filterDocumentTypeListBy(documentTypeList: Array<ClientDocumentTypesDto>,
                                   personType,
                                   countrySubdivision?): Array<ClientDocumentTypesDto> {
    if (documentTypeList) {
      let filteredResult = documentTypeList
        .filter(docType => docType.personType === personType);
      if (countrySubdivision) {
        filteredResult = filteredResult
          .filter( doc => doc.countrySubdivisionId == countrySubdivision || !doc.countrySubdivisionId);
      }

      return filteredResult;
    }
    return [];
  }

  private setColumnsName(): void {
    this.columns = [
      {name: 'clientModule.edit.documents.columns.documentType', prop: 'documentTypeName'},
      {name: 'clientModule.edit.documents.columns.document', prop: 'documentInformation'}
    ];
  }

  private setFormModal() {
    this.formDataGroup = this.formBuilder.group({
      id: 0,
      ownerId: GuidDefaultData,
      documentTypeId: ['', [Validators.required]],
      documentTypeName: '',
      documentInformation: ['', [Validators.required, (c) => this.documentIsValid(c)]],
    });

    this.observeDocumentType();
  }

  private observeDocumentType() {
    this.formDataGroup.get('documentTypeId').valueChanges
      .subscribe(documentTypeId => {
        if (documentTypeId) {
          const documentTypeFound = this.allDocList.find(documentType => documentType.id == documentTypeId);
          if (documentTypeFound) {
            this.formDataGroup.get('documentTypeName').setValue(documentTypeFound.name);
            this.formDataGroup.get('documentInformation').reset('');
            this.nifValidation();
          }
        }
      });
  }

  public toggleModal(): void {
    this.modalTitle = 'clientModule.edit.documents.new';
    this.editingDocumentIndex = -1;
    this.resetForm();
    this.modalEmitToggleService.emitToggleWithRef('newDocument');
    this.nonRepeatedDocuments();
  }

  private resetForm() {
    this.formDataGroup.reset({
      id: 0,
      ownerId: GuidDefaultData,
      documentTypeId: '',
      documentTypeName: '',
      documentInformation: '',
    });
  }

  public goToEdit(item: ClientDocumentDto) {
    this.toggleModal();
    this.modalTitle = 'clientModule.edit.documents.edit';
    this.editingDocumentIndex = _.findIndex(this.documentsList, <any>item);

    const optToEdit = this.optionsDocListOriginal
      .find(opt => opt.key == item.documentTypeId);

    if (optToEdit) {
      if (!this.optionsDocList.some(opt => opt.key == optToEdit.key)) {
        this.optionsDocList = [...this.optionsDocList, optToEdit];
      }

      this.formDataGroup.patchValue({
        id: item.id,
        ownerId: item.ownerId,
        documentTypeId: +item.documentTypeId,
        documentTypeName: item.documentTypeName,
        documentInformation: item.documentInformation,
      });
    }
  }

  public saveDataDocument({value, valid}: { value: ClientDocumentDto; valid: boolean }): void {
    if (valid) {
      if (this.editingDocumentIndex != -1) {
        this.parsingOldDocumentDataFromNewValues(value);
      } else {
        this.documents = [this.parsingDataFromBackend(value), ...this.documents];
      }

      this.formDataGroup.reset();
      this.toggleModal();
    }
  }

  public maskDocument(event) {
    const documentString = event.target.value;
    const backspaceCode = 8;
    if (event.keyCode != backspaceCode) {
      this.applyMaskToDocument(documentString);
    }
  }

  public applyMaskToDocument(documentString) {
    if (documentString) {
      const documentTypeId = this.formDataGroup.get('documentTypeId').value;
      if (documentTypeId) {
        this.formDataGroup
          .get('documentInformation')
          .setValue(
            this.documentMaskService
              .maskDocumentStringByDocumentTypeId(documentString, documentTypeId),
          );
      }
    }
  }
  public toggleRemoveItem() {
    this.itemToRemove = null;
    this.modalEmitToggleService.emitToggleWithRef('removeDocument');
  }

  public askRemoveItem(item) {
    this.toggleRemoveItem();
    this.itemToRemove = item;
  }

  public removeItem() {
    if (this.itemToRemove) {
      this.documents = this.documents
        .filter(doc => doc.documentTypeId != this.itemToRemove.documentTypeId);
    }
    this.toggleRemoveItem();
  }

  private parsingDataFromBackend(dataDocument): ClientDocumentDto {
    if (this.allDocList) {
      const docChosen = this.allDocList
        .find(doc => doc.id == dataDocument.documentTypeId);

      if (docChosen) {
        return <ClientDocumentDto>{
          id: dataDocument.id,
          ownerId: dataDocument.ownerId,
          documentTypeId: +dataDocument.documentTypeId,
          documentTypeName: docChosen.name || '',
          documentInformation: dataDocument.documentInformation,
        };
      }
    }
  }

  private parsingOldDocumentDataFromNewValues(document: ClientDocumentDto): void {
    const newDocument = this.parsingDataFromBackend(document);
    this.documents.splice(this.editingDocumentIndex, 1, newDocument);
    this.documents = [...this.documents];
  }

  private nonRepeatedDocuments() {
    this.optionsDocList = this.optionsDocListOriginal
      .filter(opt => {
      return !this.documentsList.some(doc => doc.documentTypeId == opt.key);
    });
  }

  private documentIsValid(c) {
    return this.formDataGroup && this.documentService
      .cpfControlIsValid(this.formDataGroup.get('documentTypeId').value, c.value);
  }
}
