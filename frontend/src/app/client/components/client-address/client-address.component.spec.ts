import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule } from '@ngx-translate/core';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as _ from 'lodash';
import { ClientAddressComponent } from './client-address.component';
import { Location } from 'app/shared/models/location';

xdescribe('ClientAddressComponent', () => {

  @Component({selector: 'app-address', template: ''})
  class AddressStubComponent {
    addressForm: FormGroup;
    clearAddress = () => {};
  }

  let component: ClientAddressComponent;
  let fixture: ComponentFixture<ClientAddressComponent>;
  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), FormsModule, ReactiveFormsModule],
      declarations: [ClientAddressComponent, AddressStubComponent],
      providers: [FormBuilder],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(ClientAddressComponent);
    component = fixture.componentInstance;

    component.locationNewEdit = new Location();

    component.addresses = [
      {
        ownerId: '9999999999999999999',
        subdivision: 'Rio de Janeiro',
        division: 'RJ',
        additionalAddressDetails: 'Av',
        streetName: 'Pebleu',
        streetNumber: '23',
        postalCode: '89218001',
        neighborhood: 'pavuna',
        locationCategoryId: 1,
        locationCategory: {
          id: 1,
          name: 'Comercial',
        },
      },
      {
        ownerId: '9999999999999999999',
        subdivision: 'Joinville',
        division: 'SC',
        additionalAddressDetails: 'Av',
        streetName: 'Pebleu',
        streetNumber: '23',
        postalCode: '89218001',
        neighborhood: 'pavuna',
        locationCategoryId: 3,
        locationCategory: {
          id: 3,
          name: 'Correspondência',
        },
      },
    ];

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should setVars', () => {
      spyOn<any>(component, 'setColumnsName');
      spyOn<any>(component, 'setFormModal');

      component['setVars']();

        expect(component.editingAddressIndex).toEqual(-1);
      expect(component.hasAddresses).toBeFalsy();
      expect(component.columns).toEqual([
        { name: 'clientModule.edit.address.columns.addressType', prop: 'locationCategory.name' },
        { name: 'clientModule.edit.address.columns.country', prop: 'country' },
        { name: 'clientModule.edit.address.columns.postalCode', prop: 'postalCode' },
        { name: 'clientModule.edit.address.columns.uf', prop: 'division' },
        { name: 'clientModule.edit.address.columns.city', prop: 'subdivision' },
        { name: 'clientModule.edit.address.columns.neighborhood', prop: 'neighborhood' },
        { name: 'clientModule.edit.address.columns.locality', prop: 'streetName' },
        { name: 'clientModule.edit.address.columns.number', prop: 'streetNumber' },
        { name: 'clientModule.edit.address.columns.complement', prop: 'additionalAddressDetails' },
      ]);
      expect(component['setColumnsName']).toHaveBeenCalled();
      expect(component['setFormModal']).toHaveBeenCalled();
    });

    it('should toggleModal', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
      spyOn<any>(component, 'setFormModal');

      component.toggleModal();

      expect(component.modalTitle).toEqual('clientModule.edit.address.new');
      expect(component['editingAddressIndex']).toEqual(-1);
      expect(component['setFormModal']).toHaveBeenCalled();
      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('newAddress');
    });

    it('should getAllInfoGoogleAddress', () => {
      component.getAllInfoGoogleAddress({});

      expect(component.allPlaceAddress).toEqual({});
    });
  });

  it('should goToEdit', () => {
    const indexTest = 0;
    spyOn(component, 'toggleModal');
    spyOn(_, 'findIndex').and.returnValue(indexTest);
    const item = component.addressesList[indexTest];

    component.goToEdit(item);

    expect(component.modalTitle).toEqual('clientModule.edit.address.edit');
    expect(component['editingAddressIndex']).toEqual(indexTest);
    expect(component.toggleModal).toHaveBeenCalled();
  });

  it('should saveDataAddress', () => {
    spyOn<any>(component, 'parsingDataFromBackend').and.returnValue(component.addressesList[0]);
    spyOn(component, 'toggleModal');

    component.editingAddressIndex = -1;

    component.saveDataAddress(component.locationNewEdit);

    expect(component.toggleModal).toHaveBeenCalled();
    component.removeItem();
  });

  it('should editDataAddress', () => {
    spyOn<any>(component, 'parsingAddressOldToNew');
    spyOn(component, 'toggleModal');

    component.editingAddressIndex = 0;
    component.allPlaceAddress = {};

    component.saveDataAddress(component.locationNewEdit);

    expect(component.toggleModal).toHaveBeenCalled();
  });

  it('should removeItem', () => {
    component.removeItem();

    expect(component.addressesList.length).toEqual(1);
    expect(component.addressesList).not.toContain(component.addressesList[1]);
  });
});
