import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { AddressComponent } from '../../../shared/components/address/address.component';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { LocationCategoryEnum } from '../../../shared/models/location-category-enum';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { AddressType } from 'app/shared/models/address-type-enum';
import { Location } from 'app/shared/models/location';

@Component({
  selector: 'app-client-address',
  templateUrl: './client-address.component.html',
})
export class ClientAddressComponent implements OnInit {
  @ViewChild(AddressComponent) addressComponent: AddressComponent;

  public disabledAddressesTypeList: Array<AddressType>;
  public comercialAddress: any;
  addressesList = [];

  @Input()
  get addresses() {
    return this.addressesList;
  }

  @Output() updateComercialAddress = new EventEmitter();
  @Output() addressesChange = new EventEmitter();

  set addresses(val) {
    if (val) {
      this.addressesList = val;
      this.controlComercialAddressType();
      this.hasAddresses = this.addressesList.length > 0;
      this.addressesChange.emit(this.addressesList);
    }
  }

  public hasAddresses: boolean;
  public columns: Array<any>;
  public itemsOption: Array<any>;

  public allPlaceAddress: any;
  public editingAddressIndex: number;

  // Modal Dependencies
  public modalTitle: string;
  public itemToRemove: any;
  public locationNewEdit: Location;

  constructor(
    private translateService: TranslateService,
    private toasterEmitService: ToasterEmitService,
    private modalEmitToggleService: ModalEmitToggleService,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars(): void {
    this.setColumnsName();

    this.editingAddressIndex = -1;
    this.allPlaceAddress = {};
    this.hasAddresses = false;

    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.goToEdit },
      { title: 'commomData.delete', callbackFunction: this.askRemoveItem },
    ];
  }

  private controlComercialAddressType() {

    if (this.addressesList
      .filter(address => address.locationCategoryId === AddressType.Comercial).length > 0 && this.editingAddressIndex < 0) {
      this.disabledAddressesTypeList = [AddressType.Comercial];
    } else {
      this.disabledAddressesTypeList = null;
    }
  }

  private setColumnsName(): void {
    this.columns = [];
    this.translateService.get('clientModule.edit.address.columns.addressType').subscribe(value => {
      this.columns.push({ name: value, prop: 'locationCategory.name' });
    });
    this.translateService.get('clientModule.edit.address.columns.country').subscribe(value => {
      this.columns.push({ name: value, prop: 'country' });
    });
    this.translateService.get('clientModule.edit.address.columns.postalCode').subscribe(value => {
      this.columns.push({ name: value, prop: 'postalCode' });
    });
    this.translateService.get('clientModule.edit.address.columns.uf').subscribe(value => {
      this.columns.push({ name: value, prop: 'division' });
    });
    this.translateService.get('clientModule.edit.address.columns.city').subscribe(value => {
      this.columns.push({ name: value, prop: 'subdivision' });
    });
    this.translateService.get('clientModule.edit.address.columns.neighborhood').subscribe(value => {
      this.columns.push({ name: value, prop: 'neighborhood' });
    });
    this.translateService.get('clientModule.edit.address.columns.locality').subscribe(value => {
      this.columns.push({ name: value, prop: 'streetName' });
    });
    this.translateService.get('clientModule.edit.address.columns.number').subscribe(value => {
      this.columns.push({ name: value, prop: 'streetNumber' });
    });
    this.translateService.get('clientModule.edit.address.columns.complement').subscribe(value => {
      this.columns.push({ name: value, prop: 'additionalAddressDetails' });
    });
  }

  public toggleModal(): void {
    this.modalTitle = 'clientModule.edit.address.new';
    this.editingAddressIndex = -1;
    this.controlComercialAddressType();
    this.modalEmitToggleService.emitToggleWithRef('newAddress');
  }

  public getAllInfoGoogleAddress(place: any) {
    this.allPlaceAddress = place;
  }

  public setValueAddressByEventEmitted(address) {
    this.locationNewEdit = address;
  }

  public goToEdit = item => {
    this.toggleModal();
    this.modalTitle = 'clientModule.edit.address.edit';
    this.editingAddressIndex = _.findIndex(this.addressesList, item);
    this.controlComercialAddressType();
    this.locationNewEdit = item;
  }

  public saveDataAddress(location: Location): void {
    if (this.editingAddressIndex != -1) {
      this.parsingAddressOldToNew(this.locationNewEdit);
    } else {
      this.addresses = [this.locationNewEdit, ...this.addresses];
    }

    if (location.locationCategoryId == AddressType.Comercial) {
      this.comercialAddress = this.locationNewEdit;
      this.updateComercialAddress.emit(this.comercialAddress);
    }
    this.locationNewEdit = null;
    this.addressComponent && this.addressComponent.addressForm.reset();
    this.addressComponent && this.addressComponent.clearAddress();
    this.toggleModal();
  }

  public toggleRemoveItem() {
    this.itemToRemove = null;
    this.modalEmitToggleService.emitToggleWithRef('removeAddress');
  }

  public askRemoveItem = item => {
    if (item.locationCategoryId == LocationCategoryEnum.Comercial && this.countComercialAddress() == 1) {
      this.toasterEmitService.emitChange(SuccessError.error, this.translateService.instant('clientModule.edit.address.cannotDelete'));
    } else {
      this.toggleRemoveItem();
      this.itemToRemove = item;
    }
  }

  public removeItem(): void {
    const removeIndex = this.addresses.indexOf(this.itemToRemove);
    this.addresses.splice(removeIndex, 1);
    this.addresses = [...this.addresses];
    this.toggleRemoveItem();
  }

  private parsingAddressOldToNew(dataAddress: any): void {
    this.addresses.splice(this.editingAddressIndex, 1, dataAddress);
    this.addresses = [...this.addresses];
  }

  private countComercialAddress() {
    return _.filter(this.addressesList, ['locationCategoryId', LocationCategoryEnum.Comercial]).length;
  }
}
