import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { ClientCreateEditComponent } from './component-container/client-create-edit/client-create-edit.component';
import { ClientListComponent } from './components/client-list/client-list.component';
import { ClientAddressComponent } from './components/client-address/client-address.component';
import { ClientBasicDataComponent } from './components/client-basic-data/client-basic-data.component';
import { ClientDataContactComponent } from './components/client-data-contact/client-data-contact.component';
import { ClientService } from '../shared/services/client/client.service';
import { ClientDataDocumentService } from '../shared/services/shared/client-data-document.service';
import { ClientRoutingModule } from './client-routing.module';
import { ClientDataPlaceComponent } from './components/client-data-place/client-data-place.component';
import { ClientDataChannelComponent } from './components/client-data-channel/client-data-channel.component';
import {
  ClientDataChannelAddComponent
} from './components/client-data-channel/client-data-channel-add/client-data-channel-add.component';
import {
  ClientDataChannelEditComponent
} from './components/client-data-channel/client-data-channel-edit/client-data-channel-edit.component';
import {
  ClientDataChannelRemoveComponent
} from './components/client-data-channel/client-data-channel-remove/client-data-channel-remove.component';

@NgModule({
  imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule, ClientRoutingModule],
  providers: [ClientService, ClientDataDocumentService],
  declarations: [
    ClientCreateEditComponent,
    ClientListComponent,
    ClientAddressComponent,
    ClientBasicDataComponent,
    ClientDataContactComponent,
    ClientDataPlaceComponent,
    ClientDataChannelComponent,
    ClientDataChannelAddComponent,
    ClientDataChannelEditComponent,
    ClientDataChannelRemoveComponent
  ],
  exports: [
    ClientCreateEditComponent,
    ClientListComponent,
    ClientAddressComponent,
    ClientBasicDataComponent,
    ClientDataContactComponent,
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class ClientModule {}
