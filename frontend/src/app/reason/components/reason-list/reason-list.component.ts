import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Reason } from '../../../shared/models/dto/reason';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ReasonService } from '../../../shared/services/shared/reason.service';
import { ReasonResource } from '../../../shared/resources/reason/reason.resource';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reason-list',
  templateUrl: './reason-list.component.html',
})
export class ReasonListComponent implements OnInit {
  private propertyId: number;
  public reasonToRemove: Reason;
  private isEdit: boolean;
  // Header dependencies
  public configHeaderPage: ConfigHeaderPageNew;
  public searchableItems: SearchableItems;
  // Button dependencies
  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;
  public buttonCancelRemoveConfig: ButtonConfig;
  public buttonConfirmRemoveConfig: ButtonConfig;
  // Modal dependencies
  public modaCreateEditTitle: string;
  public formModalNew: FormGroup;
  // Select dependencies
  public reasonOptionList: Array<SelectObjectOption>;
  // Table dependencies
  public rows: Array<Reason> = [];
  public filteredRows: Array<Reason> = [];
  public itemsOption: Array<any>;

  constructor(
    private sharedService: SharedService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private modalEmitToggleService: ModalEmitToggleService,
    private reasonResource: ReasonResource,
    private reasonService: ReasonService,
    private commomService: CommomService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.propertyId = +this.route.snapshot.params.property;

    this.setConfigHeaderPage();
    this.setButtonConfig();
    this.setFormModalReason();
    this.getReasonList();
    this.getReasonOptionList();
    this.itemsOption = [{ title: 'commomData.edit' }, { title: 'commomData.delete' }];
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      callBackFunction: this.toggleNew,
    };
  }

  private setButtonConfig() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'reason-cancel-modal',
      this.toggleNew,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
      'reason-save-modal',
      this.confirmSave,
      'commomData.confirm',
      ButtonType.Primary,
      null,
      true,
    );
    this.buttonCancelRemoveConfig = this.sharedService.getButtonConfig(
      'reason-remove-cancel-modal',
      this.toggleRemoveModal,
      'commomData.not',
      ButtonType.Secondary,
    );
    this.buttonConfirmRemoveConfig = this.sharedService.getButtonConfig(
      'reason-remove-confirm-modal',
      this.deleteRow,
      'commomData.yes',
      ButtonType.Primary,
    );
  }

  public updateList(searchData: any) {
    this.filteredRows = this.reasonService.filterBySearchData(searchData, this.rows);
  }

  private setFormModalReason() {
    this.formModalNew = this.formBuilder.group({
      id: 0,
      isActive: true,
      name: [null, [Validators.required]],
      category: [null, [Validators.required]],
      propertyId: this.propertyId,
    });

    this.formModalNew.valueChanges.subscribe(value => {
      this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
        'reason-save-modal',
        this.confirmSave,
        'commomData.confirm',
        ButtonType.Primary,
      );
      this.buttonSaveModalConfig.isDisabled = this.formModalNew.valid ? false : true;
    });
  }

  public toggleNew = () => {
    this.modaCreateEditTitle = this.translateService.instant('action.lbNew', {
      labelName: this.translateService.instant('label.reason')
    });
    this.modalEmitToggleService.emitToggleWithRef('reason-new');
    this.setFormModalReason();
  }

  public toggleRemoveModal = () => {
    this.modalEmitToggleService.emitToggleWithRef('reason-remove');
  }

  public editRow(row: Reason) {
    this.toggleNew();
    this.modaCreateEditTitle = this.translateService.instant('action.lbEdit', {
      labelName: this.translateService.instant('label.reason')
    });
    this.formModalNew.patchValue({
      id: row.id,
      isActive: row.isActive,
      name: row.reasonName,
      category: row.reasonCategoryId,
      propertyId: row.propertyId,
    });
  }

  public confirmRemoveRow(row: Reason) {
    this.reasonToRemove = row;
    this.toggleRemoveModal();
  }

  public deleteRow = () => {
    this.reasonResource.deleteReason(this.reasonToRemove.id).subscribe(response => {
      if (response) {
        this.toasterEmitService
          .emitChange(
            SuccessError.success,
            this.translateService.instant('variable.lbDeleteSuccessM', {
              labelName: this.translateService.instant('label.reason')
            })
          );
        this.toggleRemoveModal();
        this.getReasonList();
      }
    });
  }

  public rowAction(event, row: Reason) {
    if (event.title == 'commomData.delete') {
      this.confirmRemoveRow(row);
    } else if (event.title == 'commomData.edit') {
      this.editRow(row);
    }
  }

  private getReasonList() {
    this.reasonResource.getAllReasonList(this.propertyId).subscribe(response => {
      if (response) {
        this.rows = response.items;
        this.filteredRows = response.items;
      }
    });
  }

  public confirmSave = () => {
    const reason = new Reason();
    reason.id = this.formModalNew.get('id').value;
    reason.reasonName = this.formModalNew.get('name').value;
    reason.isActive = this.formModalNew.get('isActive').value;
    reason.reasonCategoryId = this.formModalNew.get('category').value;
    reason.propertyId = this.propertyId;
    this.saveReason(reason);
    this.toggleNew();
  }

  private getReasonOptionList() {
    this.reasonResource.getAllReasonCategoryList().subscribe(response => {
      if (response) {
        this.reasonOptionList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
          response,
          'id',
          'reasonCategoryName',
        );
        this.rows.forEach(r => (r.reasonCategoryName = response.items.find(ro => ro.id == r.reasonCategoryId).reasonCategoryName));
      }
    });
  }

  public itemWasUpdated(reason: Reason) {
    this.reasonResource.updateReasonStatus(reason).subscribe(response => {
      if (reason.isActive) {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbActivatedWithSuccessM', {
            labelName: this.translateService.instant('label.reason')
          })
        );
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbInactivatedWithSuccessM', {
            labelName: this.translateService.instant('label.reason')
          })
        );
      }
      this.getReasonList();
    });
  }

  private saveReason(reason: Reason) {
    const message = this.translateService.instant('variable.lbSaveSuccessM', {
      labelName: this.translateService.instant('label.reason')
    });

    if (reason.id) {
      this.reasonResource.updateReason(reason).subscribe(response => {
        this.getReasonList();
        this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant(message));
      });
    } else {
      this.reasonResource.createReason(reason).subscribe(response => {
        this.getReasonList();
        this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant(message));
      });
    }
  }
}
