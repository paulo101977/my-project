import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReasonListComponent } from './components/reason-list/reason-list.component';

export const reasonRoutes: Routes = [{ path: '', component: ReasonListComponent }];

@NgModule({
  imports: [RouterModule.forChild(reasonRoutes)],
  exports: [RouterModule],
})
export class ReasonRoutingModule {}
