import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ReasonListComponent } from './components/reason-list/reason-list.component';
import { ReasonRoutingModule } from './reason-routing.module';

@NgModule({
  imports: [CommonModule, SharedModule, ReasonRoutingModule],
  declarations: [ReasonListComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class ReasonModule {}
