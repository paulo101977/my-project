import { PmsMenuFinancialPermissions } from './pms-menu-financial-permissions';
import { PmsMenuGovernancePermissions } from './pms-menu-governance-permissions';
import { PmsMenuHomePermissions } from './pms-menu-home-permissions';
import { PmsMenuHostingPermissions } from './pms-menu-hosting-permissions';
import { PmsMenuInteractiveReportPermissions } from './pms-menu-interactive-report-permissions';
import { PmsMenuReservePermissions } from './pms-menu-reserve-permissions';
import { PmsMenuRevenueManagementPermissions } from './pms-menu-revenue-management-permissions';
import { PmsMenuSettingsPermissions } from './pms-menu-settings-permissions';
import { PmsMenuIntegrationPermissions } from './pms-menu-integration-permissions';

export const PmsMenuPermissions = {
  ...PmsMenuHomePermissions,
  ...PmsMenuHostingPermissions,
  ...PmsMenuFinancialPermissions,
  ...PmsMenuReservePermissions,
  ...PmsMenuGovernancePermissions,
  ...PmsMenuSettingsPermissions,
  ...PmsMenuRevenueManagementPermissions,
  ...PmsMenuInteractiveReportPermissions,
  ...PmsMenuIntegrationPermissions
};
