export const PmsMenuInteractiveReportPermissions = {
  PMS_Menu_Interactive_Report: {
    img: 'report-blue-V2.svg'
  },
  PMS_Menu_Reports: {
    routerLink: 'interactive-report/reports/all'
  },
  PMS_Menu_Dashboards: {
    routerLink: 'interactive-report/dashboard'
  },
  PMS_Menu_SibaIntegration: {
    countries: ['PT'],
    routerLink: 'interactive-report/siba-integration'
  }
};
