export const PmsMenuFinancialPermissions = {
  PMS_Menu_Cash_Operation: {
    routerLink: 'billing-account?openedAccounts=true',
    img: 'finance-blue-V2.svg'
  }
};
