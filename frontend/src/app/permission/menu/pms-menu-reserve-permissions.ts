export const PmsMenuReservePermissions = {
  PMS_Menu_Reserve: {
    img: 'booking-blue-V2.svg'
  },
  PMS_Menu_Consult_Reservation: {
    routerLink: 'reservation',
  },
  PMS_Menu_New_Booking: {
    routerLink: 'reservation/new'
  },
  PMS_Menu_Availability: {
    routerLink: 'availability-grid'
  },
  PMS_Menu_Tariff_Suggestion: {
    routerLink: 'reservation/rate-proposal'
  }
};
