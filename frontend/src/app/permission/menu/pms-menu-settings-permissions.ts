import { UserPermissionReferenceList } from '@thex/security';

export const PmsMenuSettingsPermissions: UserPermissionReferenceList = {
  PMS_Menu_Settings: {
    img: 'register-blue-V2.svg'
  },
  PMS_Menu_Hotel_Management: {},
  PMS_Menu_Hotel_Contract: {
    routerLink: 'config'
  },
  PMS_Menu_Hotel_Parameters: {
    routerLink: 'config/parameter'
  },
  PMS_Menu_Hotel_Policies: {
    routerLink: 'config/policies'
  },
  PMS_Menu_Type_of_Guest: {
    routerLink: 'config/guest-type'
  },
  PMS_Menu_Hotel_GovernanceStatus: {
    routerLink: 'config/governance-status'
  },
  PMS_Menu_Currency_Quotation: {
    routerLink: 'currency-exchange-manage'
  },
  PMS_Menu_Kind_of_UH: {
    routerLink: 'room-type'
  },
  PMS_Menu_UH: {
    routerLink: 'room'
  },
  PMS_Menu_Network_Management: {},
  PMS_Menu_Reason: {
    routerLink: 'reason'
  },
  PMS_Menu_Segment: {
    routerLink: 'market-segment'
  },
  PMS_Menu_Origin: {
    routerLink: 'origin'
  },
  PMS_Menu_Client: {
    routerLink: 'client'
  },
  PMS_Menu_Customer_Category: {
    routerLink: 'config/category'
  },
  PMS_Menu_Release_Type: {},
  PMS_Menu_Service: {},
  PMS_Menu_Service_Group_Registration: {
    routerLink: 'entry-type/group'
  },
  PMS_Menu_Service_Registration: {
    routerLink: 'entry-type/service'
  },
  PMS_Menu_Rate: {},
  PMS_Menu_Create_Rate: {
    routerLink: 'entry-type/tax'
  },
  PMS_Menu_Product: {},
  PMS_Menu_POS_Register: {
    routerLink: 'pos'
  },
  PMS_Menu_Product_Group_Registration: {
    routerLink: 'products/group'
  },
  PMS_Menu_Product_Registration: {
    routerLink: 'products/list'
  },
  PMS_Menu_Forms_of_Payment: {
    routerLink: 'payment-type'
  },
  PMS_Menu_User_Management: {
    routerLink: 'config/user'
  },
  PMS_Menu_Invoice_Emissions: {},
  PMS_Menu_Tributes: {},
  PMS_Menu_Tributes_Service: {
    countries: ['BR'],
    routerLink: 'tributes/services'
  },
  PMS_Menu_Tributes_Product: {
    countries: ['BR'],
    routerLink: 'tributes/products'
  },
  PMS_Menu_ISS_Withheld: {
    countries: ['BR'],
    routerLink: 'tributes/client/withheld'
  },
  PMS_Menu_Iva_Services_And_Products: {
    countries: ['PT'],
    routerLink: 'tributes/iva-associate'
  },
  PMS_Menu_Fiscal_Documents: {
    routerLink: 'config/fiscal-documents/parameter'
  },
  PMS_Menu_Download_XML_NFCE: {
    countries: ['BR'],
    routerLink: 'billing-account/invoice-nfce-download'
  }
};
