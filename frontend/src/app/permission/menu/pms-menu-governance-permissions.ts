export const PmsMenuGovernancePermissions = {
  PMS_Menu_Governance: {
    img: 'governance-blue-V2.svg'
  },
  PMS_Menu_See_Governance: {
    routerLink: 'governance/room'
  },
  PMS_Menu_See_Management_of_UH: {
    routerLink: 'governance/room/association'
  }
};
