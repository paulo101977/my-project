export const PmsMenuHostingPermissions = {
  PMS_Menu_Hosting: {
    img: 'accommodation-blue-V2.svg'
  },
  PMS_Menu_Checkin: {
    routerLink: 'reservation?filter=0'
  },
  PMS_Menu_Walkin: {
    routerLink: 'reservation/new?iswalkin=1'
  },
  PMS_Menu_Change_Of_UH: {
    routerLink: 'room-change'
  },
  PMS_Menu_Audit: {
    routerLink: 'audit'
  }
};
