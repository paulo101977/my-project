export const PmsMenuRevenueManagementPermissions = {
  PMS_Menu_Revenue_Management: {
    img: 'rm-blue-V2.svg'
  },
  PMS_Menu_Pension_Register: {
    routerLink: 'rm/meal-plan-type'
  },
  PMS_Menu_Insert_Base_Rate: {
    routerLink: 'rm/base-rate'
  },
  PMS_Menu_Trade_Agreement: {
    routerLink: 'rm/rate-plan'
  },
  PMS_Menu_Tariff_Schedule: {
    routerLink: 'rm/calendar'
  },
  PMS_Menu_Rate_Strategy: {
    routerLink: 'rm/rate-strategy'
  },
  PMS_Menu_PropertyMealPlanTypeRate: {
    routerLink: 'rm/meal-plan/add-view'
  },
  PMS_Menu_PropertyPremise: {
    routerLink: 'rm/premises'
  },
  PMS_Menu_Level: {
    routerLink: 'rm/level'
  },
  PMS_Menu_Level_Rate: {
    routerLink: 'rm/rate-level'
  },
  PMS_Menu_Level_Tariff: {
  }
};
