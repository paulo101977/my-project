import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';
import { SearchReservationResultDto } from 'app/shared/models/dto/search-reservation-result-dto';

export const searchForm: SearchReservationResultDto = {
  reservationCode: '',
  reservationItemCode: '',
  reservationItemId: 1,
  reservationItemStatusId: ReservationStatusEnum.Checkin,
  arrivalDate: null,
  departureDate: null,
  cancellationDate: null,
  roomNumber: 101,
  requestedRoomTypeId: 1,
  requestedRoomTypeName: '',
  requestedRoomTypeAbbreviation: '',
  receivedRoomTypeId: 101,
  receivedRoomTypeName: 'Room',
  receivedRoomTypeAbbreviation: 'R',
  guestReservationItemStatusId: 1,
  guestReservationItemStatusName: 'Room',
  guestId: 1,
  guestReservationItemId: 1,
  guestIsIncognito: false,
  guestName: 'Room',
  guestLanguage: 'pt-BR',
  client: null,
  groupName: null,
  creationTime: null,
  reservationBy: null,
  deadline: null,
  guestDocument: null,
  total: null,
  reservationItemStatusName: null,
  id: null,
  isShow: false,
};

