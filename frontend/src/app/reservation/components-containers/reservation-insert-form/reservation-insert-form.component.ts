import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';
import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, Renderer2, SimpleChanges, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Reservation } from 'app/shared/models/reserves/reservation';
import { IMyDate } from 'mydaterangepicker';
import { DateService } from 'app/shared/services/shared/date.service';
import { ReservationHour } from 'app/shared/models/reserves/reservation-hour';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';
import { ReservationItemView } from 'app/shared/models/reserves/reservation-item-view';
import { RoomType } from 'app/room-type/shared/models/room-type';
import { ReservationBudget, ReservationWithBudget } from 'app/shared/models/revenue-management/reservation-budget/reservation-budget';
import { ValidatorFormService } from 'app/shared/services/shared/validator-form.service';
import { Masks } from 'app/shared/models/masks-enum';
import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { ReservationBudgetService } from 'app/shared/services/reservation-budget/reservation-budget.service';
import { ReservationItemBudget } from 'app/shared/models/revenue-management/reservation-budget/reservation-item-budget';
import { ReservationBudgetResource } from 'app/shared/resources/reservation-budget/reservation-budget.resource';
import { PropertyService } from 'app/shared/services/property/property.service';
import {
  ReservationItemCommercialBudgetSearch
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-search';
import {
  ReservationItemCommercialBudgetConfig
} from 'app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';
import { GratuityType } from 'app/shared/models/revenue-management/reservation-budget/gratuity-type';
import { TranslateService } from '@ngx-translate/core';
import { CommomService } from 'app/shared/services/shared/commom.service';
import {
  ReservationItemCommercialBudget
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { distinctUntilChanged } from 'rxjs/operators';
import { BudgetService } from '@app/shared/services/budget/budget.service';
import { RoomTypeService } from 'app/room-type/shared/services/room-type/room-type.service';
import { RateProposalStorageItem } from 'app/reservation/rate-proposal/models/rate-proposal-storage-item';

@Component({
  selector: 'reservation-insert-form',
  templateUrl: 'reservation-insert-form.component.html',
  styleUrls: ['reservation-insert-form.component.css'],
})
export class ReservationInsertFormComponent implements OnInit, OnChanges {

  @Input() rateProposalStorage: RateProposalStorageItem;
  @Input() reservationData: Reservation;
  @Input() roomTypeDataList: Array<RoomType>;
  @Input() propertyId: number;
  @Input() companyClientId: string;
  @Input() reservationItemId: number;
  @Input() budgetChildAgeList: Array<number>;
  @Input('filterWord')
  set filterWord (filter: string) {
    this.searchData = filter;
    this.filterWordChange.emit(this.searchData);
  }
  get filterWord(): string {
    return this.searchData;
  }

  @Output() filterWordChange = new EventEmitter();

  @Output() setFocusOnReservationItem = new EventEmitter();
  @Output() setListenersOnReservationItem = new EventEmitter();
  @Output() showCancelOrReactivateButtonOnReservation = new EventEmitter();
  @Output() setStatusColorFooterOnReservationItem = new EventEmitter();
  @Output() setTotalsOfReservation = new EventEmitter();
  @Output() setReservationGroupFormOnReservation = new EventEmitter();
  @Output() setReservationBudget = new EventEmitter();

  @ViewChild('childrenPopoverContent') childrenPopoverContent: ElementRef;


  // Component properties
  public searchData: string;
  public reservationHour: ReservationHour;
  public averagePrice: number;
  public reservationBudgetListCurrent: Array<ReservationBudget>;
  public reservationGroup: FormGroup;
  public schedulePopoverIsVisible: boolean;
  public childrenPopoverIsVisible: boolean;
  public maskHourAndMinute: Masks = Masks.HourAndMinute;
  private roomTypesOfProperty: Array<RoomType>;
  public periodSelected: boolean;
  public childrenQuantity: number;
  public showReservationBudgetDetail: boolean;
  public reservationItemBudget: ReservationItemBudget;
  public childAgeList: Array<any>;
  public showRatePlanOfNewReservationItem: boolean;
  public reservationItemViewWhichIsUpdating: ReservationItemView;
  public selectReservationItemCommercialBudgetList: Array<SelectObjectOption>;
  public reservationItemCommercialBudget: ReservationItemCommercialBudgetSearch;
  public reservationItemCommercialBudgetConfig: ReservationItemCommercialBudgetConfig;
  public reservationItemBudgetConfigForModal: ReservationItemCommercialBudgetConfig;
  public gratuityTypeList: Array<GratuityType>;
  public commercialBudgetNameOfNewReservationItem: string;
  public currencySymbolOfNewReservationItem: string;
  public budgetedAmountOfNewReservationItem: number;
  public teste: ReservationItemCommercialBudgetConfig;
  public insertBtnIsAble: boolean;

  // Budget dependences
  public dateListFromPeriod: Array<Date>;
  public showBudgetButton: boolean;
  public budgetModalId: string;
  public gratuityTypeSelectOptionsList: Array<SelectObjectOption>;

  // Url params received from availability grid
  private roomTypeId;
  private roomId;

  public disableUntil: IMyDate;
  public oldChildQtdValue = 0;

  // novo orçamento
  public currentReservationWithBudget: ReservationWithBudget;

  constructor(
    private dateService: DateService,
    private reservationService: ReservationService,
    private sessionParameterService: SessionParameterService,
    public formBuilder: FormBuilder,
    private toasterEmitService: ToasterEmitService,
    private validatorFormService: ValidatorFormService,
    private roomTypeResource: RoomTypeResource,
    private renderer: Renderer2,
    public modalEmitToggleService: ModalEmitToggleService,
    public route: ActivatedRoute,
    public reservationBudgetService: ReservationBudgetService,
    public reservationBudgetResource: ReservationBudgetResource,
    private translateService: TranslateService,
    private commonService: CommomService,
    private budgetService: BudgetService,
    private currencyFormatPipe: CurrencyFormatPipe,
    private propertyService: PropertyService,
    private roomTypeService: RoomTypeService,
  ) {
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (this.companyClientId) {
      const period = this.reservationGroup.get('period').value;
      const roomTypeId = this.reservationGroup.get('roomType').value.id;
      if (this.propertyId && period && roomTypeId && this.dateListFromPeriod && this.reservationGroup.get('adultQuantity').value) {
        this.showBudgetButtonAndOptionsOnInsertForm();
      }
    }
    if (simpleChanges.hasOwnProperty('companyClientId')) {
      if (simpleChanges['companyClientId'].previousValue != simpleChanges['companyClientId'].currentValue
        && !simpleChanges['companyClientId'].isFirstChange()) {
        this.showBudgetButtonAndOptionsOnInsertForm();
      }
    }
    if (simpleChanges.hasOwnProperty('propertyId')) {
      const propertyChange = simpleChanges.propertyId;
      if (propertyChange.previousValue != propertyChange.currentValue) {
        this.getRoomType(this.reservationGroup.get('period').value);
      }
    }

    if (simpleChanges.hasOwnProperty('rateProposalStorage')) {
      if (simpleChanges.rateProposalStorage.currentValue) {
        this.setRateProposalInForm();
        this.setReservationDataFromUrlParams();
        this.loadChildAgeList();
      }
    }

  }

  private setRateProposalInForm() {
    if (this.reservationGroup && this.rateProposalStorage) {
      this.reservationGroup.patchValue({
        roomQuantity: this.rateProposalStorage.rateProposalSearchModel.roomQtd,
        period: this.getPeriodFromRateProposal(),
        adultQuantity: this.rateProposalStorage.rateProposalSearchModel.adultCount,
        childrenQuantity: this.rateProposalStorage.rateProposalSearchModel.childrenAge ?
          this.rateProposalStorage.rateProposalSearchModel.childrenAge.length : 0,
        childrenAgeList: this.prepareRateProposalChildList()
      });

      if ( this.rateProposalStorage ) {
        this.periodSelected = true;
        this.roomTypeId = this.rateProposalStorage.rateProposalSearchModel.roomTypeIdSort;
        this.getRoomTypesByPeriod(this.getPeriodFromRateProposal());
        this.reservationItemCommercialBudgetConfig = this.rateProposalStorage.rateProposalTypeItemItem.budgetConfig;

        this.setRateProposalCurrentItens();
      }
    }
  }

  ngOnInit() {
    this.setFormInsertReservation();
    this.setCheckinAndCkeckOutLabelToDefault();
    this.setListeners();
    this.setReservationDataFromUrlParams();
    this.setVars();
    this.loadChildAgeList();

    this.disableUntil = this.dateService.getTodayIMydate();
  }


  private setVars() {
    this.schedulePopoverIsVisible = false;
    this.childrenPopoverIsVisible = false;
    this.showReservationBudgetDetail = false;
    this.budgetModalId = 'reservation-budget-modal-id';
    this.insertBtnIsAble = false;
  }

  public showOrHideSchedulePopoverReservationForm(event) {
    event.stopPropagation();
    const currentValue = this.schedulePopoverIsVisible;
    this.schedulePopoverIsVisible = !currentValue;
  }

  public keepSchedulePopoverReservationFormVisible(event) {
    event.stopPropagation();
    this.schedulePopoverIsVisible = true;
  }

  public hideChildrenAndSchedulePopoverReservationForm() {
    this.schedulePopoverIsVisible = false;
  }

  public hideChildrenPopoverReservationForm(event, type?) {
    event.stopPropagation();
    this.childrenPopoverIsVisible = false;
    if (type == 'cancel') {
      this.reservationGroup.get('childrenQuantity').setValue(this.oldChildQtdValue, { eventEmit: false, onlySelf: true});
    } else {
      this.oldChildQtdValue = this.reservationGroup.get('childrenQuantity').value;
      this.setBudgetModal(); // must recalculate child
    }
  }

  public keepChildrenPopoverReservationFormVisible(event) {
    event.stopPropagation();
    this.childrenPopoverIsVisible = true;
  }

  public setCheckinAndCkeckOutLabelToDefault() {
    this.reservationHour = new ReservationHour();
    this.reservationHour.checkinHour = this.sessionParameterService.getParameterValue(this.propertyId, ParameterTypeEnum.Checkin);
    this.reservationHour.checkoutHour = this.sessionParameterService.getParameterValue(this.propertyId, ParameterTypeEnum.Checkout);
    this.reservationGroup.get('checkinHour').setValue(this.reservationHour.checkinHour);
    this.reservationGroup.get('checkoutHour').setValue(this.reservationHour.checkoutHour);
  }

  private setListeners() {
    this.observePeriod();
    this.observeRoomType();
    this.observeCheckin();
    this.observeCheckout();
    this.observeChildren();
    this.observeAdult();
  }

  private observeRoomType() {
    this.reservationGroup.get('roomType').valueChanges.subscribe(value => {
      const childrenCapacity = this.reservationGroup.get('childrenQuantity').value;
      if (value && childrenCapacity > value.childCapacity) {
        this.reservationGroup.get('childrenQuantity').setValidators([Validators.max(value.childCapacity)]);
        this.toasterEmitService.emitChange(
          SuccessError.error,
          this.translateService.instant('label.maxChildPerRoomType', {qtd: value.childCapacity}),
        );
      } else if (!value) {
        this.reservationGroup.get('childrenQuantity').clearValidators();
        this.reservationGroup.get('childrenQuantity').setValidators(Validators.min(0));
      }
      this.reservationGroup.get('childrenQuantity').updateValueAndValidity({
        onlySelf: true,
        emitEvent: false,
      });

      if (value) {
        if (
          this.reservationItemCommercialBudgetConfig &&
          this.reservationItemCommercialBudgetConfig.reservationBudgetTableList &&
          this.reservationItemCommercialBudgetConfig.reservationBudgetTableList.length > 0
        ) {
          this.reservationItemCommercialBudgetConfig.reservationBudgetTableList.forEach(reservationBudget => {
            this.showRatePlanOfNewReservationItem = !!reservationBudget.mealPlanTypeId;
          });
        }
        this.showBudgetButton = this.reservationGroup.get('period').value && value;
        if (this.reservationGroup.get('period').value && value) {
          this.showBudgetButtonAndOptionsOnInsertForm();
        }
      }
    });
  }

  public closeBudgetModal(reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig) {
    if (reservationItemCommercialBudget.wasConfirmed) {
      this.showRatePlanOfNewReservationItem = true;
      this.insertBtnIsAble = true;
    } else {
      this.showBudgetButtonAndOptionsOnInsertForm();
      this.showRatePlanOfNewReservationItem = false;
      this.showReservationBudgetDetail = false;
      this.insertBtnIsAble = false;
    }
    this.modalEmitToggleService.emitToggleWithRef(this.budgetModalId);
  }

  private showBudgetButtonAndOptionsOnInsertForm() {
    this.showReservationBudgetDetail = false;
    this.showRatePlanOfNewReservationItem = false;
    this.insertBtnIsAble = false;
    this.reservationItemCommercialBudgetConfig = new ReservationItemCommercialBudgetConfig();

    const childrenAgeControlList = this.reservationGroup.get('childrenAgeList') as FormArray;
    const childrenAgeList = this.reservationService.getChildrenAgeListOfFormArray(childrenAgeControlList, 'childAge');
    if (this.reservationGroup.get('period').value) {
        this.dateListFromPeriod = this.reservationService.getDateListFromPeriod(this.reservationGroup.get('period').value);
    }

    this.reservationItemCommercialBudget = this.reservationBudgetService.getReservationItemCommercialBudgetSearch(
      this.propertyId,
      this.reservationGroup.get('roomType').value.id,
      this.dateListFromPeriod,
      this.companyClientId,
      this.reservationGroup.get('adultQuantity').value,
      childrenAgeList,
      this.reservationItemId,
    );

    if (this.reservationGroup.get('period').value &&
      this.reservationGroup.get('roomType').value &&
      this.reservationGroup.get('roomType').value != '') {
      this.reservationBudgetResource.getReservationItemBudget(this.reservationItemCommercialBudget).subscribe(response => {
        this.reservationItemBudget = response;
        this.setCommercialBudgetList();

        this.reservationItemCommercialBudgetConfig
          .selectReservationItemCommercialBudgetList = this.selectReservationItemCommercialBudgetList;
        this.reservationItemCommercialBudgetConfig
          .selectReservationItemCommercialBudgetOriginalList = this.reservationItemBudget.commercialBudgetList;


        this.reservationItemCommercialBudgetConfig.dateListFromPeriod = this.dateListFromPeriod;
        this.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent = new ReservationItemCommercialBudget();
        this.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent.totalBudget = 0;
        this.reservationItemCommercialBudgetConfig.isGratuityType = false;
        this.reservationItemCommercialBudgetConfig.gratuityTypeId = null;
        this.reservationItemCommercialBudgetConfig.averageDaily = 0;
        this.reservationItemCommercialBudgetConfig.wasConfirmed = false;

        this.reservationItemCommercialBudgetConfig.reservationBudgetTableList = this.reservationItemBudget.reservationBudgetDtoList;
        this.reservationItemCommercialBudgetConfig.gratuityTypeSelectOptionsList = new Array<SelectObjectOption>();

        this.showReservationBudgetDetail = !!response;
        if (this.rateProposalStorage) {
          this.reservationItemCommercialBudgetConfig = this.rateProposalStorage.rateProposalTypeItemItem.budgetConfig;
          this.reservationItemBudgetConfigForModal = this.rateProposalStorage.rateProposalTypeItemItem.budgetConfig;
          this.showRatePlanOfNewReservationItem = true;
          this.setRateProposalCurrentItens();
          this.insertBtnIsAble = true;
          this.insertReservationItemViewToReservation(); // insere os cards da sugestão tarifária
        }
      });
    }
  }

  private setRateProposalCurrentItens() {
    this.currentReservationWithBudget = new ReservationWithBudget();
    this.currentReservationWithBudget.commercilBudgetItemList =
      this.rateProposalStorage.rateProposalTypeItemItem.commercialBudgetList;
    this.currentReservationWithBudget.commercialBudgetItemKey =
      this.reservationBudgetService.getReservationItemCommercialBudgetIdentifier(
        this.rateProposalStorage.rateProposalTypeItemItem.budgetConfig.reservationItemCommercialBudgetCurrent
      );
    this.currentReservationWithBudget.gratuityTypeId = this.rateProposalStorage.rateProposalTypeItemItem.budgetConfig.gratuityTypeId;
    this.currentReservationWithBudget.reservationBudgetList =
      this.rateProposalStorage.rateProposalTypeItemItem.budgetConfig.reservationBudgetTableList;
    this.currentReservationWithBudget.reservationItemCommercialBudget =
      this.rateProposalStorage.rateProposalTypeItemItem.budgetConfig;
  }

  private observePeriod() {
    this.reservationGroup.get('period').valueChanges.subscribe(value => {
      this.getRoomType(value);
    });
  }

  private getRoomType(value) {
    this.showBudgetButton = this.reservationGroup.get('roomType').value.id && value;
    this.roomTypesOfProperty = this.getRoomTypesByPeriod(this.reservationGroup.get('period').value);
    this.roomTypeDataList = this.roomTypesOfProperty;
    this.reservationBudgetListCurrent = new Array<ReservationBudget>();
    this.reservationItemCommercialBudgetConfig = new ReservationItemCommercialBudgetConfig();
    this.periodSelected = !!value;
    if (this.reservationGroup.get('roomType').value.id && value) {
      this.showBudgetButtonAndOptionsOnInsertForm();
    }
    if (this.roomTypeService.hasOverbooking(this.roomTypeDataList)) {
      this.toasterEmitService.emitChange(SuccessError.error, this.translateService.instant('text.hasOverbooking'));
    }
  }

  private observeChildren() {
    const controlChildrenQuantity = this.reservationGroup.get('childrenQuantity');

    controlChildrenQuantity.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((value) => {
        this.showChildrenPopoverReservationForm(event);
        this.setNewRoomTypeListByGuestQuantity(value, this.reservationGroup.get('adultQuantity').value);
      });
  }

  private setBudgetModal() {
    if (this.reservationGroup.get('roomType').value.id && this.reservationGroup.get('period').value) {
      this.showBudgetButtonAndOptionsOnInsertForm();
    } else {
      this.showBudgetButtonAndOptionsOnInsertForm();
      this.showRatePlanOfNewReservationItem = false;
      this.showReservationBudgetDetail = false;
      this.insertBtnIsAble = false;
    }
  }

  private setNewRoomTypeListByGuestQuantity(childrenQuantity: number, adultQuantity: number) {
    this.roomTypeDataList = this.getRoomTypesOfProperty();
    const newRoomTypes = this.reservationService.getNewRoomTypesByGuestCapacity(this.roomTypeDataList, adultQuantity, childrenQuantity);
    this.roomTypeDataList = new Array<RoomType>();
    if (
      this.reservationGroup.get('roomType').value &&
      ((<RoomType>this.reservationGroup.get('roomType').value).childCapacity < childrenQuantity ||
        (<RoomType>this.reservationGroup.get('roomType').value).adultCapacity < adultQuantity)
    ) {
      this.reservationGroup.get('roomType').setValue('');
    }
    this.roomTypeDataList = newRoomTypes;
  }

  private getRoomTypesOfProperty() {
    return this.roomTypesOfProperty;
  }

  public sendReservationWithBudgetToReservationCreate(reservationWithBudget: ReservationWithBudget) {
    this.reservationItemBudgetConfigForModal = new ReservationItemCommercialBudgetConfig();
    this.budgetedAmountOfNewReservationItem = 0;
    reservationWithBudget.reservationItemCommercialBudget.reservationBudgetTableList = new Array<ReservationBudget>();
    reservationWithBudget.reservationItemCommercialBudget.reservationBudgetTableList.push(...reservationWithBudget.reservationBudgetList);
    this.reservationItemBudgetConfigForModal = reservationWithBudget.reservationItemCommercialBudget;

    this.reservationBudgetListCurrent = reservationWithBudget.reservationBudgetList;
    this.reservationBudgetListCurrent.forEach(reservationBudget => {
      this.commercialBudgetNameOfNewReservationItem = reservationBudget.commercialBudgetName
        ? reservationBudget.commercialBudgetName
        : this.translateService.instant('text.gratuitous');
      this.currencySymbolOfNewReservationItem = reservationBudget.currencySymbol;
      this.budgetedAmountOfNewReservationItem = this.budgetedAmountOfNewReservationItem + reservationBudget.manualRate;
    });
    if (reservationWithBudget && reservationWithBudget.gratuityTypeId) {
      reservationWithBudget.reservationItemCommercialBudget.gratuityTypeId = reservationWithBudget.gratuityTypeId;
      reservationWithBudget.reservationItemCommercialBudget.isGratuityType = true;
    }

    this.showRatePlanOfNewReservationItem = true;
    this.insertBtnIsAble = true;
    this.setReservationBudget.emit(reservationWithBudget);
    this.modalEmitToggleService.emitToggleWithRef(this.budgetModalId);
  }

  public showChildrenPopoverReservationForm(event) {
    event.stopPropagation();
    this.childrenQuantity = this.reservationGroup.get('childrenQuantity').value;
    if (this.childrenQuantity <= 0) {
      (<FormGroup>this.reservationGroup).setControl('childrenAgeList', this.formBuilder.array([]));
    } else {
      (<FormGroup>this.reservationGroup).setControl(
        'childrenAgeList',
        this.formBuilder.array(this.initChildAgeFormArrayWithDefaultValue(this.childrenQuantity)),
      );
      this.renderer.removeStyle(this.childrenPopoverContent.nativeElement, 'height');
      this.renderer.removeStyle(this.childrenPopoverContent.nativeElement, 'overflow');
      if (this.childrenQuantity > 3) {
        this.renderer.setStyle(this.childrenPopoverContent.nativeElement, 'height', '202px');
        this.renderer.setStyle(this.childrenPopoverContent.nativeElement, 'overflow', 'auto');
      }
    }
  }

  private initChildAgeFormArrayWithDefaultValue(size: number): Array<AbstractControl> {
    return this.reservationService.getChildAgeListFormGroupWithValueDefault(size);
  }

  private observeAdult() {
    const controlAdultQuantity = this.reservationGroup.get('adultQuantity');
    controlAdultQuantity.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe(value => {
        this.setNewRoomTypeListByGuestQuantity(this.reservationGroup.get('childrenQuantity').value, value);
        this.setBudgetModal();
      });
  }

  private getRoomTypesByPeriod(period: any): Array<RoomType> {
    if (period) {
      const roomTypes = new Array<RoomType>();
      const initialDate = period.beginDate;
      const endDate = period.endDate;
      this.roomTypeResource
        .getRoomTypesWithOverBookingByPropertyId(this.propertyId, initialDate, endDate, true)
        .subscribe(roomTypesOfProperty => {
          roomTypesOfProperty.forEach(roomtypeObject => roomTypes.push(roomtypeObject));
          this.roomTypeDataList = roomTypes;
          this.setRoomTypeDataFromUrlParams(roomTypes);
          if (this.roomTypeService.hasOverbooking(roomTypesOfProperty)) {
            this.toasterEmitService.emitChange(SuccessError.error, this.translateService.instant('text.hasOverbooking'));
          }
        });

      return roomTypes;
    }

    return this.roomTypeDataList;
  }

  private setRoomTypeDataFromUrlParams(roomTypes: RoomType[]) {
    this.route.queryParams.subscribe(queryParams => {
      if (this.roomTypeId && queryParams.roomTypeId) {
        const selectedRoomType = roomTypes.find(roomType => roomType.id == queryParams.roomTypeId);
        this.reservationGroup.patchValue({
          roomType: selectedRoomType,
        });
        this.roomTypeId = null;
      } else if (this.rateProposalStorage) {
        this.reservationGroup.patchValue({
          roomType: roomTypes.find( item =>
            item.id == this.rateProposalStorage.rateProposalTypeItemItem.budgetConfig.reservationItemCommercialBudgetCurrent.roomTypeId ),
        });
      }
    });

  }

  private setFormInsertReservation() {
    this.reservationGroup = this.formBuilder.group({
      period: [this.rateProposalStorage ? this.getPeriodFromRateProposal() : null , Validators.required],
      checkinHour: ['', [Validators.required, this.reservationService.checkinHourAndMinutesValid()]],
      checkoutHour: ['', [Validators.required, this.reservationService.checkinHourAndMinutesValid()]],
      roomQuantity: [
        this.rateProposalStorage ? this.rateProposalStorage.rateProposalSearchModel.roomQtd :  1,
        [Validators.required, this.validatorFormService.capacityNumberIsMajorThanOne, this.validatorFormService.setNumberToZero],
      ],
      adultQuantity: [
        this.rateProposalStorage ? this.rateProposalStorage.rateProposalSearchModel.adultCount :  1,
        [Validators.required, this.validatorFormService.capacityNumberIsMajorThanOne, this.validatorFormService.setNumberToZero],
      ],
      childrenQuantity: [
        this.rateProposalStorage ?
          (this.rateProposalStorage.rateProposalSearchModel.childrenAge ?
            this.rateProposalStorage.rateProposalSearchModel.childrenAge.length : 0 ) :  0
        , [this.validatorFormService.setNumberToZero]],
      roomType: [''],
      roomTypeRequested: null,
      childrenAgeList: this.formBuilder.array(
        this.rateProposalStorage ?
          ( this.rateProposalStorage.rateProposalSearchModel.childrenAge ?
            this.prepareRateProposalChildList() : [] ) : [] ),
    });

    if (this.rateProposalStorage) {
      this.periodSelected = true;
      this.roomTypeId = this.rateProposalStorage.rateProposalSearchModel.roomTypeIdSort;
      this.getRoomTypesByPeriod(this.getPeriodFromRateProposal());
      this.reservationItemCommercialBudgetConfig = this.rateProposalStorage.rateProposalTypeItemItem.budgetConfig;
    }
  }

  private prepareRateProposalChildList(): Array<any> {
    if (this.rateProposalStorage && this.rateProposalStorage.rateProposalSearchModel.childrenAge) {
      this.reservationService.getChildAgeListFormGroupWithValue( this.rateProposalStorage.rateProposalSearchModel.childrenAge );
    }
    return [];
  }

  private getPeriodFromRateProposal() {
    return {
      beginDate: this.rateProposalStorage.rateProposalSearchModel.initialDate,
      endDate: this.rateProposalStorage.rateProposalSearchModel.finalDate
    };
  }

  public observeCheckin() {
    this.reservationGroup.get('checkinHour').valueChanges.subscribe(value => {
      this.reservationHour.errorMessage = this.reservationService.getMessageError(this.reservationGroup.get('checkinHour'));
      if (!this.reservationHour.errorMessage) {
        this.reservationHour.checkinHour = value;
      }
    });
  }

  public observeCheckout() {
    this.reservationGroup.get('checkoutHour').valueChanges.subscribe(value => {
      this.reservationHour.errorMessage = this.reservationService.getMessageError(this.reservationGroup.get('checkoutHour'));
      if (!this.reservationHour.errorMessage) {
        this.reservationHour.checkoutHour = value;
      }
    });
  }

  private setReservationFormToDefault() {
    this.reservationGroup.get('period').setValue(null);
    this.reservationGroup.get('roomQuantity')
      .setValue(this.rateProposalStorage ? this.rateProposalStorage.rateProposalSearchModel.roomQtd : 1);
    this.reservationGroup.get('roomType').setValue('');
    this.reservationGroup.get('adultQuantity').setValue(1);
    this.reservationGroup.get('childrenQuantity').setValue(0);

    this.dateListFromPeriod = new Array<Date>();
    this.averagePrice = 0;

    this.setCheckinAndCkeckOutLabelToDefault();
  }

  public insertReservationItemViewToReservation() {
    this.averagePrice = 0;

    const reservationItemViewList = this.reservationService.getReservationItemViewList(
      this.reservationGroup,
      this.roomTypeDataList,
      this.averagePrice,
      this.reservationItemBudgetConfigForModal,
    );

    // Novo orçamento
    if (reservationItemViewList) {
      reservationItemViewList.forEach( item => {
        item.budgetCommercialBudgetItemKey = this.currentReservationWithBudget.commercialBudgetItemKey;
        item.budgetGratuityTypeId = this.currentReservationWithBudget.gratuityTypeId;
        item.budgetReservationBudgetList = JSON.parse(JSON.stringify(this.currentReservationWithBudget.reservationBudgetList));
        item.budgetCommercialBudgetList = this.currentReservationWithBudget.commercilBudgetItemList;
      });
    }
    // --

    let currencyId, mealPlanTypeId, ratePlanId;
    if (this.currentReservationWithBudget && this.currentReservationWithBudget.commercialBudgetItemKey) {
      currencyId = this.currentReservationWithBudget.commercialBudgetItemKey.currencyId;
      mealPlanTypeId = this.currentReservationWithBudget.commercialBudgetItemKey.mealPlanTypeId;
      ratePlanId = this.currentReservationWithBudget.commercialBudgetItemKey.ratePlanId;
    }
    const currencyIdDefault = this.sessionParameterService.getParameterValue(this.propertyId, ParameterTypeEnum.DefaultCurrency);
    reservationItemViewList.forEach(reservationItemView => {
      reservationItemView.currencyId = currencyId ? currencyId : currencyIdDefault;
      reservationItemView.mealPlanTypeId = mealPlanTypeId ? mealPlanTypeId : null;
      reservationItemView.ratePlanId = ratePlanId ? ratePlanId : null;
      reservationItemView.gratuityTypeId = this.reservationItemBudgetConfigForModal.gratuityTypeId;
      reservationItemView.extraCribCount = 0;
      reservationItemView.extraBedCount = 0;
    });

    if (reservationItemViewList) {
      const reservationItemViewLists = {
        oldList: this.reservationData.reservationItemViewList,
        newList: reservationItemViewList,
      };
      this.setFocusOnReservationItem.emit(reservationItemViewLists);
      this.reservationData.reservationItemViewList.push(...reservationItemViewList);
      this.reservationData.reservationItemViewList = [...this.reservationData.reservationItemViewList];

      this.setConfigReservationItemView(this.reservationData.reservationItemViewList);
      this.reservationData.reservationItemViewList.reverse();
      this.reservationService.checkStatusReservationItemCanceledToReorderReservationItemList(this.reservationData.reservationItemViewList);

      this.setListenersOnReservationItem.emit(reservationItemViewList);
    }

    this.showCancelOrReactivateButtonOnReservation.emit();
    this.setStatusColorFooterOnReservationItem.emit();
    this.showReservationBudgetDetail = false;
    this.showRatePlanOfNewReservationItem = false;
    this.insertBtnIsAble = false;
    this.periodSelected = false;

    if (this.roomId && this.reservationData.reservationItemViewList && this.reservationData.reservationItemViewList[0]) {
      const selectedRoom = this.reservationData.reservationItemViewList[0].rooms.find(room => room.id == this.roomId);
      this.reservationData.reservationItemViewList[0].formCard.patchValue({roomNumberCard: selectedRoom});
      this.roomId = null;
    }
    this.rateProposalStorage = null;
  }

  public deleteChildren(index) {
    (<Array<FormGroup>>this.getControlsOfChildAgeListForm()).splice(index, 1);
    this.reservationGroup.get('childrenQuantity').setValue((<Array<FormGroup>>this.getControlsOfChildAgeListForm()).length);
  }

  private setConfigReservationItemView(reservationItemViewList: Array<ReservationItemView>) {
    this.reservationService.generateCodesForReservationItemViewList(reservationItemViewList);
    this.setTotalsOfReservation.emit(reservationItemViewList);
    this.setReservationFormToDefault();
  }

  private setReservationDataFromUrlParams() {
    this.route.queryParams.subscribe(queryParams => {
      if (queryParams.start && queryParams.end) {
        this.reservationGroup.patchValue({
          period: this.dateService.getDateRangePicker(moment(queryParams.start).toDate(), moment(queryParams.end).toDate()),
        });
      }

      this.roomTypeId = queryParams.roomTypeId;
      this.roomId = queryParams.roomId;
    });
  }

  public getControlsOfChildAgeListForm(): any {
    return (<FormGroup>this.reservationGroup.get('childrenAgeList')).controls;
  }

  private loadChildAgeList(): void {
    this.childAgeList = this.propertyService.getChildAgeOfProperty(this.propertyId);
  }

  public showBudgetModalOfNewReservationItem(): void {
    if (this.reservationItemCommercialBudgetConfig.wasConfirmed) {
      this.reservationItemCommercialBudgetConfig.reservationBudgetTableList = [
        ...this.reservationItemCommercialBudgetConfig.reservationBudgetTableList,
      ];
    }
    this.setCommercialBudgetList();
    this.reservationItemViewWhichIsUpdating = null;

    this.modalEmitToggleService.emitToggleWithRef(this.budgetModalId);
  }

  private setCommercialBudgetList(): void {
    const itemCommercialBudgetList =
    this.budgetService.getCommercialBudgetList(this.reservationItemCommercialBudgetConfig, this.reservationItemBudget);
    this.selectReservationItemCommercialBudgetList = itemCommercialBudgetList.selectReservationItemCommercialBudgetList;
  }

  public informSeachChange(inputEvent) {
    this.filterWord = inputEvent;
  }

  public cancelBudgetModal() {
    this.modalEmitToggleService.emitCloseWithRef(this.budgetModalId);
  }

  public confirmBudgetModal( item ) {
    const reservationWithBudget: ReservationWithBudget = new ReservationWithBudget();
    reservationWithBudget.gratuityTypeId = item.gratuityTypeId;
    reservationWithBudget.reservationBudgetList = item.reservationBudgetList;
    reservationWithBudget.reservationItemCommercialBudget = this.reservationItemCommercialBudgetConfig;
    reservationWithBudget.commercialBudgetItemKey = item.commercialBudgetItemKey;
    reservationWithBudget.commercilBudgetItemList = item.commercialBudgetList;
    this.currentReservationWithBudget = reservationWithBudget;
    this.sendReservationWithBudgetToReservationCreate(reservationWithBudget);
  }
}
