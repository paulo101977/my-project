import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReservationHour } from '../../../shared/models/reserves/reservation-hour';
import { ReservationItemView } from '../../../shared/models/reserves/reservation-item-view';
import { IMyDrpOptions } from 'mydaterangepicker';
import { Masks } from '../../../shared/models/masks-enum';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { Guest, GuestState, GuestStatus } from '../../../shared/models/reserves/guest';
import { PropertyService } from '../../../shared/services/property/property.service';
import { ReservationService } from '../../../shared/services/reservation/reservation.service';
import { DateService } from '../../../shared/services/shared/date.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { GuestService } from '../../../shared/services/guest/guest.service';
import { TranslateService } from '@ngx-translate/core';
import { ReservationItemViewService } from '../../../shared/services/reservation-item-view/reservation-item-view.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { PersonResource } from '../../../shared/resources/person/person.resource';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { ReservationBudgetService } from '../../../shared/services/reservation-budget/reservation-budget.service';
import { ReservationBudgetResource } from '../../../shared/resources/reservation-budget/reservation-budget.resource';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { RoomTypeResource } from '../../../room-type/shared/services/room-type-resource/room-type.resource';
import { ReservationItemBudget } from '../../../shared/models/revenue-management/reservation-budget/reservation-item-budget';
import * as _ from 'lodash';
import { RoomTypeService } from '../../../room-type/shared/services/room-type/room-type.service';
import { BedTypeEnum } from '../../../shared/models/bed-type';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../shared/models/button-config';
import { ConfirmModalDataConfig } from '../../../shared/models/confirm-modal-data-config';
import { Room } from '../../../shared/models/room';
import { SearchResultDto } from '../../../shared/models/dto/search-result-dto';
import { SearchPersonAndGuestDto } from '../../../shared/models/dto/search-person-and-guest-dto';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CurrencyFormatPipe } from '../../../shared/pipes/currency-format.pipe';
import { BudgetService } from '@app/shared/services/budget/budget.service';
import { ReservationServiceEmit } from '../../services/reservation-service-emit';
import { Subscription } from 'rxjs';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'reservation-item-card-body',
  templateUrl: 'reservation-item-card-body.component.html',
  styleUrls: ['reservation-item-card-body.component.css'],
})
export class ReservationItemCardBodyComponent implements OnInit {
  private propertyId: number;
  public myDateRangePickerOptions: IMyDrpOptions;
  public reserveHour: ReservationHour;
  public selectReservationItemCommercialBudgetList: Array<SelectObjectOption>;
  public maskHourAndMinute: Masks = Masks.HourAndMinute;
  public autocompleItemsGuests: SearchResultDto;
  public clearField: boolean;
  public childAgeList: Array<any>;
  public reservationItemBudget: ReservationItemBudget;
  public checkinHourCardValue: any;
  public checkoutHourCardValue: any;
  public referenceGuestState = GuestState;
  public referenceGuestStatus = GuestStatus;

  // Modal dependencies
  public confirmModalDataConfig: ConfirmModalDataConfig;
  public confirmFinishModalButtonConfig: ButtonConfig;
  public cancelFinishModalButtonConfig: ButtonConfig;

  // Subscriptions
  private unsubscribe: Subscription;
  private canEditIsMigrated = true;

  // Form dependencies

  @Input() isRoomTypeWithoutBaseRate: boolean;
  @Input() companyClientId: string;
  @Input() reservationItemView: ReservationItemView;
  @Input() readonly: boolean;
  @Output() saveReservation: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() setNewValuesForBudgetConfig = new EventEmitter();

  constructor(
    private propertyService: PropertyService,
    private formBuilder: FormBuilder,
    private dateService: DateService,
    private roomTypeService: RoomTypeService,
    private modalEmitToggleService: ModalEmitToggleService,
    private roomTypeResource: RoomTypeResource,
    private reservationService: ReservationService,
    private budgetService: BudgetService,
    private reservationBudgetService: ReservationBudgetService,
    private reservationBudgetResource: ReservationBudgetResource,
    private guestService: GuestService,
    private reservationItemViewService: ReservationItemViewService,
    private toasterEmitService: ToasterEmitService,
    private personResource: PersonResource,
    private translateService: TranslateService,
    private currencyFormatPipe: CurrencyFormatPipe,
    private sharedService: SharedService,
    private modalEmitService: ModalEmitService,
    private route: ActivatedRoute,
    private reservationServiceEmit: ReservationServiceEmit,
    private router: Router,
    private sessionParameter: SessionParameterService,
    private propertyStorageService: PropertyStorageService
  ) {
  }

  ngOnInit() {
    this.setVars();
    this.getCanEdit();
    this.setRoomLayout();
    this.setListeners();

  }

  private getCanEdit() {
    const value = this.sessionParameter
      .getParameterValue(this.propertyId, ParameterTypeEnum.EditMigrated);
    this.canEditIsMigrated =  value == 'true';
    this.setFormValidity();

  }

  private setVars() {
    this.propertyId = this.propertyStorageService.getCurrentPropertyId();
    this.reserveHour = new ReservationHour();
    this.myDateRangePickerOptions = this.dateService.getConfigDateRangePicker();
    this.autocompleItemsGuests = new SearchResultDto();
    this.clearField = true;
    this.reservationItemView.guestQuantity.forEach(guest => this.observeGuest(guest));
    this.checkinHourCardValue = this.reservationItemView.formCard.get('checkinHourCard').value;
    this.checkoutHourCardValue = this.reservationItemView.formCard.get('checkoutHourCard').value;
    this.loadChildAgeList();

    this.reservationItemViewService.roomlistRequested$.subscribe(reservationItemView => {
      this.goToGuestPage(reservationItemView);
    });
    this.setConfirmModalDataConfig();
    this.setRoomTypeConfig();
    this.setFormValidity();
  }

  private setFormValidity() {
    if (this.reservationItemIsCheckin()) {
      this.reservationItemView.formCard.get('checkinHourCard').disable();
      this.reservationItemView.formCard.get('roomTypeCard').disable();
      this.reservationItemView.formCard.get('roomTypeRequestedCard').disable();
      this.reservationItemView.formCard.get('roomNumberCard').disable();
    }
    if ( ! this.canEdit() ) {
      this.reservationItemView.formCard.get('checkinHourCard').disable();
      this.reservationItemView.formCard.get('checkoutHourCard').disable();
      this.reservationItemView.formCard.get('roomTypeCard').disable();
      this.reservationItemView.formCard.get('roomTypeRequestedCard').disable();
      this.reservationItemView.formCard.get('periodCard').disable();
      this.reservationItemView.formCard.get('adultQuantityCard').disable();
      this.reservationItemView.formCard.get('childrenQuantityCard').disable();
    }
  }

  private setRoomTypeConfig() {
    if (this.reservationItemView.formCard) {
      this.reservationItemView.formCard.get('hasCribBedTypeCard')
        .setValue(this.roomTypeQuantityCribBedType(this.reservationItemView.formCard.get('roomTypeCard').value) > 0);
      this.reservationItemView.formCard.get('hasExtraBedTypeCard')
        .setValue(this.roomTypeQuantityExtraBedType(this.reservationItemView.formCard.get('roomTypeCard').value) > 0);
    }
  }

  private setListeners() {
    if (this.reservationItemView.formCard) {

      this.reservationItemView.formCard.get('periodCard').valueChanges.subscribe(periodValue => {
        if (periodValue && this.reservationItemView.initialPeriod !== periodValue &&
          this.reservationItemView.formCard.get('emitBudgetEvent').value) {
          this.setNewRoomTypeList();
          this.showNewValuesToBudgetModal();
        }
      });

      this.reservationItemView.formCard.get('endDate').valueChanges
        .subscribe(endNewDate => {
          if (this.reservationItemIsCheckin()) {
            this.reservationItemView.formCard.get('periodCard').setValue({
              beginDate: this.reservationItemView.initialPeriod.beginDate,
              endDate: endNewDate
            });
          }
        });


      this.reservationItemView.formCard.get('roomTypeCard').valueChanges.subscribe(roomTypeValue => {
        if (roomTypeValue && this.reservationItemView.formCard.get('periodCard').value &&
          this.reservationItemView.formCard.get('emitBudgetEvent').value) {

          this.reservationItemView.rooms = roomTypeValue.roomList ? roomTypeValue.roomList : [];
          this.setValidityRoomNumberCard();

          this.reservationItemView.formCard.get('cribBedQuantityCard').reset();
          this.reservationItemView.formCard.get('extraBedQuantityCard').reset();
          this.reservationItemView.formCard.get('hasCribBedTypeCard').setValue(this.roomTypeQuantityCribBedType(roomTypeValue) > 0);
          this.reservationItemView.formCard.get('hasExtraBedTypeCard').setValue(this.roomTypeQuantityExtraBedType(roomTypeValue) > 0);
          this.reservationItemView.extraBedQuantityList = this.reservationService.setBedOptionalQuantity(roomTypeValue, BedTypeEnum.Single);
          this.reservationItemView.cribBedQuantityList = this.reservationService.setBedOptionalQuantity(roomTypeValue, BedTypeEnum.Crib);
          this.reservationService.getRoomLayoutList(this.reservationItemView).subscribe(roomLayoutList => {
            this.reservationItemView.roomLayoutList = roomLayoutList;
            this.reservationItemView.formCard
              .get('roomLayoutCard')
              .setValue(this.reservationService.getRoomLayoutCardOfTheReservationItemView(this.reservationItemView));
            this.reservationItemView.formCard
              .get('roomLayoutCard')
              .setValue(this.reservationService.getRoomLayoutCardOfTheReservationItemView(this.reservationItemView));
          });
        }
      });

      this.reservationItemView.formCard.get('checkinHourCard').valueChanges.subscribe(value => {
        this.checkinHourCardValue = value;
      });

      this.reservationItemView.formCard.get('checkoutHourCard').valueChanges.subscribe(value => {
        this.checkoutHourCardValue = value;
      });

      this.reservationItemView.formCard.get('adultQuantityCard').valueChanges.subscribe(value => {
        this.setNewRoomTypeListByGuestQuantityCard(
          this.reservationItemView,
          this.reservationItemView.formCard.get('childrenQuantityCard').value,
          value);
        if (this.reservationItemView.formCard.get('emitBudgetEvent').value) {
          this.showNewValuesToBudgetModal();
        }
      });

      this.reservationItemView.formCard.get('roomTypeRequestedCard').valueChanges.subscribe(roomTypeRequestedCardValue => {
        if (roomTypeRequestedCardValue && this.reservationItemView.initialRequestedRoomType.id !== roomTypeRequestedCardValue.id &&
          this.reservationItemView.formCard.get('emitBudgetEvent').value) {
          this.callConfirmModalRoomTypeChanged();
        }
      });

      this.reservationItemView.formCard.get('roomNumberCard').valueChanges.subscribe(room => {
        if (room) {
          this.reservationItemView.room = room;
        }
      });
    }
  }

  private setValidityRoomNumberCard() {
    this.reservationItemView.rooms.length
      ? this.reservationItemView.formCard.get('roomNumberCard').enable()
      : this.reservationItemView.formCard.get('roomNumberCard').disable();

    if (this.reservationItemIsCheckin()) {
      this.reservationItemView.formCard.get('roomNumberCard').disable();
    }
  }

  private setRoomLayout() {
    this.reservationService.getRoomLayoutList(this.reservationItemView).subscribe(roomLayoutList => {
      this.reservationItemView.roomLayoutList = roomLayoutList;
      this.reservationItemView.formCard
        .get('roomLayoutCard')
        .setValue(this.reservationService.getRoomLayoutCardOfTheReservationItemView(this.reservationItemView));
      this.reservationItemView.formCard
        .get('roomLayoutCard')
        .setValue(this.reservationService.getRoomLayoutCardOfTheReservationItemView(this.reservationItemView));
    });
  }

  private setNewRoomTypeListByGuestQuantityCard(reservationItemView: ReservationItemView, childrenQuantity: number, adultQuantity: number) {
    const newRoomTypes = this.reservationService.getNewRoomTypesByGuestCapacity(
      reservationItemView.roomTypes,
      adultQuantity,
      childrenQuantity,
    );
    if (
      !this.roomTypeService.hasRoomTypeAtRoomTypeList(reservationItemView.roomTypes, reservationItemView.formCard.get('roomTypeCard').value)
    ) {
      this.setNewRoomTypeListOfReservationItemView(reservationItemView, newRoomTypes);

      if (reservationItemView.roomTypes && reservationItemView.formCard.controls.roomTypeCard.value) {
        reservationItemView.formCard
          .get('roomTypeCard')
          .setValue(
            this.reservationService.getRoomTypeSelected(
              reservationItemView.roomTypes,
              reservationItemView.formCard.controls.roomTypeCard.value,
            ),
          );
      }
    }
  }

  private setNewRoomTypeListOfReservationItemView(reservationItemView: ReservationItemView, newRoomTypeList: Array<RoomType>) {
    const roomTypeSelected = reservationItemView.formCard.get('roomTypeCard').value;
    const cribQuantity = reservationItemView.formCard.get('cribBedQuantityCard').value;
    const extraQuantity = reservationItemView.formCard.get('extraBedQuantityCard').value;

    reservationItemView.roomTypes = new Array<RoomType>();
    reservationItemView.roomTypes = newRoomTypeList;
    reservationItemView.rooms = new Array<Room>();

    const roomType = reservationItemView.roomTypes.find(roomTypeItem => roomTypeItem.id === roomTypeSelected.id);
    reservationItemView.formCard.get('roomTypeCard').setValue(roomType);
    reservationItemView.formCard.get('roomTypeRequestedCard').setValue(roomType);
    reservationItemView.formCard.get('cribBedQuantityCard').setValue(parseInt(cribQuantity, 0));
    reservationItemView.formCard.get('extraBedQuantityCard').setValue(parseInt(extraQuantity, 0));
  }

  public roomTypeQuantityCribBedType(roomType: RoomType): number {
    return this.roomTypeService.getQuantityOptionalBedList(roomType, BedTypeEnum.Crib);
  }

  public roomTypeQuantityExtraBedType(roomType: RoomType): number {
    return this.roomTypeService.getQuantityOptionalBedList(roomType, BedTypeEnum.Single);
  }

  public callConfirmModalRoomTypeChanged() {
    this.modalEmitToggleService.emitOpenWithRef('confirmModalId');
  }

  private setConfirmModalDataConfig(): void {
    this.cancelFinishModalButtonConfig = this.sharedService.getButtonConfig(
      'cancel-finish',
      this.closeConfirmModal,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.confirmFinishModalButtonConfig = this.sharedService.getButtonConfig(
      'confirm-finish',
      this.confirmRoomTypeChanged,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
    );
    this.confirmModalDataConfig = new ConfirmModalDataConfig();
    this.confirmModalDataConfig.textConfirmModal = 'text.wantChangeBudget';
    this.confirmModalDataConfig.actionCancelButton = this.cancelFinishModalButtonConfig;
    this.confirmModalDataConfig.actionConfirmButton = this.confirmFinishModalButtonConfig;
  }

  public closeConfirmModal = () => {
    this.modalEmitToggleService.emitToggleWithRef('confirmModalId');
    this.reservationItemView.formCard.get('roomTypeRequestedCard')
      .setValue(this.reservationItemView.initialRequestedRoomType, {emitEvent: false});
  }

  public confirmRoomTypeChanged = () => {
    this.modalEmitToggleService.emitToggleWithRef('confirmModalId');
    this.showNewValuesToBudgetModal();
  }

  private setNewRoomTypeList() {
    const roomTypeCardSetted = this.reservationItemView.formCard.get('roomTypeCard').value;
    const newRoomTypelist = this.getRoomTypesByPeriod(this.reservationItemView.formCard.get('periodCard').value);
    this.reservationItemView.roomTypes = _.union(newRoomTypelist, this.reservationItemView.roomTypes);
    this.reservationItemView.roomTypes.forEach(roomType => {
      if (roomType.id === roomTypeCardSetted.id) {
        this.reservationItemView.formCard.get('roomTypeCard').setValue(roomTypeCardSetted);
      }
    });
    if (this.roomTypeService.hasOverbooking(this.reservationItemView.roomTypes)) {
      this.toasterEmitService.emitChange(SuccessError.error, this.translateService.instant('text.hasOverbooking'));
    }
  }

  private getRoomTypesByPeriod(period: any): Array<RoomType> {
    if (period) {
      const roomTypes = new Array<RoomType>();
      const initialDate = period.beginDate;
      const endDate = period.endDate;
      this.roomTypeResource
        .getRoomTypesWithOverBookingByPropertyId(this.propertyId, initialDate, endDate, true)
        .subscribe(roomTypesOfProperty => {
          roomTypesOfProperty.forEach(roomtypeObject => roomTypes.push(roomtypeObject));
          if (!this.roomTypeService.hasRoomTypeAtRoomTypeList(roomTypes, this.reservationItemView.formCard.get('roomTypeCard').value)) {
            this.setNewRoomTypeListOfReservationItemView(this.reservationItemView, roomTypes);
          }
        });

      return roomTypes;
    }

    return this.reservationItemView.roomTypes;
  }

  private showNewValuesToBudgetModal() {
    this.setNewValuesForBudgetConfig.emit(this.reservationItemView);
  }

  private loadChildAgeList(): void {
    this.childAgeList = this.propertyService.getChildAgeOfProperty(this.propertyId);
  }

  public showOrHideSchedulePopoverCard(event, reservationItemCard) {
    if (this.canEdit()) {
      event.stopPropagation();
      reservationItemCard.schedulePopoverIsVisible = !reservationItemCard.schedulePopoverIsVisible;
    }
  }

  public keepSchedulePopoverCardVisible(event, reservationItemCard) {
    event.stopPropagation();
    reservationItemCard.schedulePopoverIsVisible = true;
  }

  public keepChildrenPopoverCardVisible(reservationItemCard) {
    if (this.canEdit()) {
      reservationItemCard.childrenPopoverIsVisible = true;
    }
  }

  public hideChildrenPopoverCardVisible(reservationItemCard) {
    reservationItemCard.childrenPopoverIsVisible = false;
  }

  public showOrHideBedsIncludedPopoverCard(event, reservationItemCard) {
    if (this.canEdit()) {
      event.stopPropagation();
      reservationItemCard.bedsIncludedPopoverIsVisible = !reservationItemCard.bedsIncludedPopoverIsVisible;
    }
  }

  public keepBedsIncludedPopoverCardVisible(event, reservationItemCard) {
    event.stopPropagation();
    reservationItemCard.bedsIncludedPopoverIsVisible = true;
  }

  public cancelChildrenAgeList(reservationItemView: ReservationItemView) {
    if (this.canEdit()) {
      reservationItemView.formCard.get('childrenQuantityCard').setValue(reservationItemView.formCard.get('childrenQuantityCard').value - 1);
      reservationItemView.guestQuantity.pop();
      const childrenAgeList = <FormArray>this.reservationItemView.formCard.get('childrenAgeListCard');
      childrenAgeList.removeAt(childrenAgeList.length - 1);
      this.hideChildrenPopoverCardVisible(reservationItemView);
    }
  }

  public saveChildrenAgeList(reservationItemCard): void {
    if (this.canEdit()) {
      this.hideChildrenPopoverCardVisible(reservationItemCard);

      this.setNewRoomTypeListByGuestQuantityCard(
        this.reservationItemView,
        this.reservationItemView.formCard.get('childrenQuantityCard').value,
        this.reservationItemView.formCard.get('adultQuantityCard').value
      );
      if (this.reservationItemView.formCard.get('emitBudgetEvent').value) {
        this.showNewValuesToBudgetModal();
      }
    }
  }

  public validTimeValue(checkHour, reservationItemCard) {
    if (checkHour === 'checkoutHourCard') {
      this.reserveHour.errorMessage = this.reservationService.getMessageError(reservationItemCard.formCard.controls.checkoutHourCard);
    }
    if (checkHour === 'checkinHourCard') {
      this.reserveHour.errorMessage = this.reservationService.getMessageError(reservationItemCard.formCard.controls.checkinHourCard);
    }
  }

  public addGuestAtReservationItem(reservationItemViewSelected: ReservationItemView, formControlName: string) {
    if (!this.reservationItemIsCheckin() && this.canEdit()) {
      let guest;
      const isAdult = this.objectHas(formControlName, 'adult');
      const isChildren = this.objectHas(formControlName, 'child');

      if (isAdult) {
        guest = this.guestService.getAdultGuestDefaultValues();
      }
      if (isChildren) {
        guest = this.guestService.getChildGuestDefaultValues();
      }
      this.validToAddOrShowMessageErrorForGuestCapacityMajorThanRoomTypeCapacity(
        reservationItemViewSelected,
        formControlName,
        isAdult,
        isChildren,
        guest,
      );

      this.validGuestLinkIsDisabled(reservationItemViewSelected);
    }
  }

  public guestHandle(event, guestSelected, isEdit, index) {
    event.stopPropagation();
    guestSelected.state = this.guestService.getGuestStateCreatingGuest();

    setTimeout(() => {
      const element = document.getElementById('guestInput-' + index);
      if (element) {
        element.focus();
      }
    }, 0);

    if (isEdit === 'edit') {
      this.clearField = false;
    } else {
      this.clearField = true;
    }
  }

  public removeGuestAtReservationItemView(reservationItemView, indexGuest) {
    if (this.canEdit()) {
      const guestDeleted = reservationItemView.guestQuantity[indexGuest];

      if (guestDeleted.isChild) {
        reservationItemView.formCard.get('childrenQuantityCard')
          .setValue(reservationItemView.formCard.get('childrenQuantityCard').value - 1);
        const childrenAgeList = <FormArray>this.reservationItemView.formCard.get('childrenAgeListCard');
        childrenAgeList.removeAt(childrenAgeList.length - 1);
        if (this.reservationItemView.formCard.get('emitBudgetEvent').value) {
          this.showNewValuesToBudgetModal();
        }
      }
      if (!guestDeleted.isChild) {
        reservationItemView.formCard.get('adultQuantityCard').setValue(reservationItemView.formCard.get('adultQuantityCard').value - 1);
      }
      reservationItemView.guestQuantity.splice(indexGuest, 1);
    }
  }

  public keepGuestInputOpen(event) {
    event.stopPropagation();
  }

  private objectHas(formControlName: string, text: string) {
    return formControlName.indexOf(text) > -1;
  }

  private validToAddOrShowMessageErrorForGuestCapacityMajorThanRoomTypeCapacity(
    reservationItemViewSelected: ReservationItemView,
    formControlName: string,
    isAdult: boolean,
    isChildren: boolean,
    guest: Guest,
  ) {
    if (
      isAdult &&
      this.reservationItemViewService.isRoomTypeCapacityMajorThanGuestCapacity(
        reservationItemViewSelected.formCard.get(formControlName).value,
        reservationItemViewSelected.formCard.get('roomTypeCard').value.adultCapacity,
      )
    ) {
      this.setGuestQuantityAtAccommodation(reservationItemViewSelected, formControlName, guest);
    } else if (
      isAdult &&
      !this.reservationItemViewService.isRoomTypeCapacityMajorThanGuestCapacity(
        reservationItemViewSelected.formCard.get(formControlName).value,
        reservationItemViewSelected.formCard.get('roomTypeCard').value.adultCapacity,
      )
    ) {
      this.toasterEmitService.emitChange(
        SuccessError.error,
        this.translateService.instant('reservationModule.commomData.guestAdultCapacityMajorRoomTypeCapacityError'),
      );
    }

    if (
      isChildren &&
      this.reservationItemViewService.isRoomTypeCapacityMajorThanGuestCapacity(
        reservationItemViewSelected.formCard.get(formControlName).value,
        reservationItemViewSelected.formCard.get('roomTypeCard').value.childCapacity,
      )
    ) {
      this.reservationItemView.childrenPopoverIsVisible = true;
      this.setGuestQuantityAtAccommodation(reservationItemViewSelected, formControlName, guest);
    } else if (
      isChildren &&
      !this.reservationItemViewService.isRoomTypeCapacityMajorThanGuestCapacity(
        reservationItemViewSelected.formCard.get(formControlName).value,
        reservationItemViewSelected.formCard.get('roomTypeCard').value.childCapacity,
      )
    ) {
      this.toasterEmitService.emitChange(
        SuccessError.error,
        this.translateService.instant('reservationModule.commomData.guestChildrenCapacityMajorRoomTypeCapacityError'),
      );
    }
  }

  private validGuestLinkIsDisabled(data) {
    data.guestQuantity.forEach((guest, index) => {
      if (this.guestService.guestNameIsEmpty(data)) {
        guest.isDisabledGuest = index != 0 ? true : false;
      } else {
        guest.isDisabledGuest = false;
      }
    });
  }

  private setGuestQuantityAtAccommodation(reservationItemViewSelected: ReservationItemView, formControlName: string, guest: Guest) {
    reservationItemViewSelected.formCard.get(formControlName).setValue(reservationItemViewSelected.formCard.get(formControlName).value + 1);
    reservationItemViewSelected.guestQuantity.push(guest);
    if (guest.isChild) {
      const childrenAgeList = <FormArray>this.reservationItemView.formCard.get('childrenAgeListCard');
      childrenAgeList.push(this.formBuilder.group({
        childAge: 0
      }));
    }
    this.observeGuest(guest);
  }

  private observeGuest(guest: Guest) {
    guest.formGuest
      .get('guestName')
      .valueChanges
      .pipe(
        debounceTime(400),
        distinctUntilChanged()
      )
      .subscribe(guestName => {
        if (this.reservationItemView.formCard.get('emitBudgetEvent').value) {
          guest.quantityClicksOutInputAndAutocomplete += 1;
          if (guestName && guestName.length > 3) {
            guest.showAutocompleItemsGuests = true;
            if (this.doesNotClickInOptionOfTheAutocomplete(guest)) {
              guest.personId = null;
              guest.guestId = 0;
              guest.fullName = '';
              this.setMainGuestName(guestName);
            } else {
              guest.showAutocompleItemsGuests = false;
              this.setMainGuestName(guestName);
            }
            this.personResource.getCategorizedPersonByName(guestName).subscribe(guests => {
              if (guests) {
                this.autocompleItemsGuests = guests;
              } else {
                guest.personId = null;
                this.clearAutocompleItemsGuests();
              }
            });
          } else {
            guest.personId = null;
            guest.showAutocompleItemsGuests = false;
            this.setMainGuestName(guestName);
            this.clearAutocompleItemsGuests();
          }
          guest.quantityClicksOutInputAndAutocomplete = 0;
        }
      });
  }

  private setMainGuestName(guestName: string) {
    this.reservationItemView.mainGuestName = guestName;
  }

  private doesNotClickInOptionOfTheAutocomplete(guest: Guest) {
    return guest.quantityClicksOutInputAndAutocomplete == 1;
  }

  private clearAutocompleItemsGuests(): void {
    this.autocompleItemsGuests = new SearchResultDto();
  }

  public createRange(number: number): Array<number> {
    return this.sharedService.createRange(number);
  }

  // Depois de executar esse método, ele vai para o observe de Guest, pois altero o seu valor.
  public chooseGuest(event, guest: Guest, searchPersonsResultDto: SearchPersonAndGuestDto): void {
    event.stopPropagation();
    guest.showAutocompleItemsGuests = false;
    guest.fullName = searchPersonsResultDto.name;
    guest.guestId = searchPersonsResultDto.guestId;
    guest.personId = searchPersonsResultDto.personId;
    guest.quantityClicksOutInputAndAutocomplete += 1;
    guest.formGuest.get('guestName').setValue(searchPersonsResultDto.name);
  }

  public goToGuestPage(reservationItemView?: ReservationItemView) {
    if (this.canEdit()) {
      this.saveReservation.emit(false);
      this.unsubscribe = this.reservationServiceEmit.reservationSaved$.subscribe(reservation => {
        const reservationItemViewRedirect = reservation.reservationItemList
          .find(riv => riv.reservationItemCode == reservationItemView.code);
        this.reservationService.goToRoomList(reservationItemViewRedirect.id, 1, this.propertyId)
          .then( () => {
            return this.unsubscribe.unsubscribe();
          });
      });
    }
  }

  public getControlsOfChildAgeListForm(): any {
    return (<FormGroup>this.reservationItemView.formCard.get('childrenAgeListCard')).controls;
  }

  public hasGuestsInAutocomplete(autocompleItemsGuests) {
    return (
      autocompleItemsGuests &&
      (autocompleItemsGuests.searchResultCategory &&
        (autocompleItemsGuests.searchResultCategory.length > 0 &&
          (autocompleItemsGuests.searchResultCategory[0].searchPersonAndGuest.length > 0 &&
            autocompleItemsGuests.searchResultCategory[1].searchPersonAndGuest.length > 0)))
    );
  }

  public canRemove(guest) {
    return guest
      && (guest.status == this.referenceGuestStatus.ToConfirm
        || guest.status == this.referenceGuestStatus.Confirmed
        || guest.state == this.referenceGuestState.NoGuest) && this.canEdit();
  }

  public reservationItemIsCheckin() {
    return this.reservationItemView.status == ReservationStatusEnum.Checkin;
  }

  public canEdit() {
    if (this.readonly) {
      return false;
    }
    if (this.reservationItemView.isMigrated) {
      return this.canEditIsMigrated;
    }
    return true;
  }

  public getOvernights() {
    return this.dateService
      .convertPeriodToDateList(this.reservationItemView.period.beginDate, this.reservationItemView.period.endDate, true).length;
  }

}
