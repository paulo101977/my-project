// import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { FormsModule, ReactiveFormsModule, FormBuilder, FormArray } from '@angular/forms';
// import { HttpClientModule } from '@angular/common/http/';
// import { ActivatedRoute } from '@angular/router';
// import { RouterTestingModule } from '@angular/router/testing';
// import { TranslateModule } from '@ngx-translate/core';
// import { ReservationHour } from '../../../shared/models/reserves/reservation-hour';
// import { MyDateRangePickerModule, IMyDrpOptions } from 'mydaterangepicker';
// import { TextMaskModule } from 'angular2-text-mask';
// import { SuccessError } from '../../../shared/models/success-error-enum';
// import { ReservationItemCardBodyComponent } from './reservation-item-card-body.component';
// import { Guest } from '../../../shared/models/reserves/guest';
// import { BedType } from '../../../shared/models/bed-type';
// import { RoomType } from '../../../shared/models/room-type/room-type';
// import { Room } from '../../../shared/models/room';
// import { ReservationItemView } from '../../../shared/models/reserves/reservation-item-view';
// import { GuestStatus } from '../../../shared/models/reserves/guest';
// import { SearchPersonsResultDto } from '../../../shared/models/person/search-persons-result-dto';
// import { ReservationStatusEnum } from '../../../shared/models/reserves/reservation-status-enum';
// import { ReservationItemViewData } from '../../../shared/mock-data/reservationItem-data';
// import { PropertyService } from '../../../shared/services/property/property.service';
// import { DateService } from '../../../shared/services/shared/date.service';
// import { ReservationService } from '../../../shared/services/reservation/reservation.service';
// import { ValidatorFormService } from '../../../shared/services/shared/validator-form.service';
// import { RoomTypeService } from '../../../shared/services/room-type/room-type.service';
// import { SharedService } from '../../../shared/services/shared/shared.service';
// import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
// import { ReservationItemViewService } from '../../../shared/services/reservation-item-view/reservation-item-view.service';
// import { GuestService } from '../../../shared/services/guest/guest.service';
// import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
// import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
// import { StatusStyleService } from '../../../shared/services/reservation/status-style.service';
// import { PersonResource } from '../../../shared/resources/person/person.resource';
// import { RoomLayoutResource } from '../../../shared/resources/room-layout/room-layout.resource';
//
// describe('ReservationItemBodyCardComponent', () => {
//   let component: ReservationItemCardBodyComponent;
//   let fixture: ComponentFixture<ReservationItemCardBodyComponent>;
//   const roomTypeMock = new RoomType();
//   const roomTypeMock2 = new RoomType();
//   const formBuilder = new FormBuilder();
//   const personIdMock = '23232323';
//   let bedTypeCrib;
//   let bedTypeSingle;
//   const guest = new Guest();
//   const guestList = [];
//   const roomMock2 = new Room();
//   const roomMock = new Room();
//   let options: IMyDrpOptions;
//   const name = 'Abc', email = 'abc@email.com', phone = '1223333';
//   const newReservationForm = new FormBuilder().group({
//     contactGroup: new FormBuilder().group({
//       name: name,
//       email: email,
//       phone: phone
//     }),
//     selectValuePaymentType: formBuilder.group({
//       paymentType: ['']
//     }),
//     selectOrigin: new FormBuilder().group({
//       origin: ['']
//     }),
//     selectMarketSegment: new FormBuilder().group({
//         marketSegment: ['']
//     }),
//     moneyGroup: new FormBuilder().group({
//       moneyValue: ['']
//     }),
//     deadlineGroup: new FormBuilder().group({
//       deadline: [ {
//         date: {
//           year: new Date().getFullYear(),
//           month: new Date().getMonth() + 1,
//           day: new Date().getDate() - 1
//         }
//       }]
//     }),
//     clientToBillGroup: new FormBuilder().group({
//       clientId: [''],
//       clientName: ['']
//     }),
//     creditCardGroup: formBuilder.group({
//       holder: [''],
//       plasticNumber: [''],
//       expirationDate: [''],
//       csc: [''],
//       selectValuebanner: formBuilder.group({
//         banner: ['']
//       })
//     }),
//   });
//
//   function setGuest() {
//     guest.status = GuestStatus.NoGuest;
//     guest.formGuest = formBuilder.group({
//       guestName: ['']
//     });
//     guest.personId = personIdMock;
//     guestList.push(guest);
//   }
//
//   function setRoom() {
//     roomMock.roomNumber = '101';
//     roomMock2.roomNumber = '209';
//   }
//
//   function setBedType() {
//     bedTypeCrib = new BedType();
//     bedTypeCrib.count = 0;
//     bedTypeCrib.type = 5;
//     bedTypeCrib.optionalLimit = 2;
//
//     bedTypeSingle = new BedType();
//     bedTypeSingle.count = 2;
//     bedTypeSingle.type = 1;
//     bedTypeSingle.optionalLimit = 1;
//   }
//
//   function setRoomType() {
//     roomTypeMock.id = 1;
//     roomTypeMock.name = 'Quarto Standard';
//     roomTypeMock.abbreviation = 'SPDC';
//     roomTypeMock.order = 1;
//     roomTypeMock.adultCapacity = 2;
//     roomTypeMock.childCapacity = 1;
//     roomTypeMock.roomTypeBedTypeList = new Array<BedType>();
//     roomTypeMock.roomTypeBedTypeList.push(bedTypeSingle);
//     roomTypeMock.roomTypeBedTypeList.push(bedTypeCrib);
//     roomTypeMock.isActive = true;
//     roomTypeMock.isInUse = false;
//     roomTypeMock.roomList = new Array<Room>();
//     roomTypeMock.roomList.push(roomMock);
//
//     roomTypeMock2.id = 2;
//     roomTypeMock2.name = 'Quarto Luxo';
//     roomTypeMock2.abbreviation = 'LXO';
//     roomTypeMock2.order = 1;
//     roomTypeMock2.adultCapacity = 3;
//     roomTypeMock2.childCapacity = 0;
//     roomTypeMock2.roomTypeBedTypeList = new Array<BedType>();
//     roomTypeMock2.roomTypeBedTypeList.push(bedTypeSingle);
//     roomTypeMock2.roomTypeBedTypeList.push(bedTypeCrib);
//     roomTypeMock2.isActive = true;
//     roomTypeMock2.isInUse = false;
//     roomTypeMock2.roomList = new Array<Room>();
//     roomTypeMock2.roomList.push(roomMock2);
//   }
//
//   function setReservationItem() {
//     setGuest();
//     setRoom();
//     setBedType();
//     setRoomType();
//
//     const reservationItemCardForm = formBuilder.group({
//       periodCard: '12/01/2017 - 14/01/2017',
//       adultQuantityCard: [1],
//       childrenQuantityCard: [0],
//       childrenAgeCard: [''],
//       roomTypeCard: [''],
//       checkinHourCard: [''],
//       checkoutHourCard: [''],
//       roomNumberCard: [''],
//       roomLayoutCard: [''],
//       extraBedQuantityCard: [''],
//       cribBedQuantityCard: [''],
//       hasCribBedTypeCard: [false],
//       hasExtraBedTypeCard: [false],
//       childrenAgeListCard: formBuilder.array([formBuilder.group({
//         childAgeCard: [0]
//       })])
//     });
//
//     component.parent = reservationItemCardForm;
//     component.reservationItemView = new ReservationItemView();
//     component.reservationItemView.code = '';
//     component.reservationItemView.room = new Room();
//     component.reservationItemView.period = '12/01/2017 - 14/01/2017';
//     component.reservationItemView.adultQuantity = 2;
//     component.reservationItemView.childrenQuantity = 1;
//     component.reservationItemView.status = ReservationStatusEnum.ToConfirm;
//     component.reservationItemView.guestQuantity = guestList;
//     component.reservationItemView.receivedRoomType = new RoomType();
//     component.reservationItemView.receivedRoomType.id = 1;
//     component.reservationItemView.receivedRoomType.name = 'Quarto Standard';
//     component.reservationItemView.receivedRoomType.abbreviation = 'SPDC';
//     component.reservationItemView.receivedRoomType.order = 1;
//     component.reservationItemView.receivedRoomType.adultCapacity = 2;
//     component.reservationItemView.receivedRoomType.childCapacity = 1;
//     component.reservationItemView.receivedRoomType.roomTypeBedTypeList = new Array<BedType>();
//     component.reservationItemView.receivedRoomType.roomTypeBedTypeList.push(bedTypeSingle);
//     component.reservationItemView.receivedRoomType.roomTypeBedTypeList.push(bedTypeCrib);
//     component.reservationItemView.receivedRoomType.isActive = true;
//     component.reservationItemView.receivedRoomType.isInUse = false;
//     component.reservationItemView.receivedRoomType.roomList = new Array<Room>();
//     component.reservationItemView.receivedRoomType.roomList.push(roomMock2);
//     component.reservationItemView.roomTypes = new Array<RoomType>();
//     component.reservationItemView.roomTypes.push(roomTypeMock);
//     component.reservationItemView.roomTypes.push(roomTypeMock2);
//     component.reservationItemView.dailyNumber = 3;
//     component.reservationItemView.dailyPrice = 1200.00;
//     component.reservationItemView.totalDaily = component.reservationItemView.dailyPrice * component.reservationItemView.dailyNumber;
//     component.reservationItemView.total = 3;
//     component.reservationItemView.room.roomNumber = 'ABCDEFG';
//     component.reservationItemView.focus = false;
//     component.reservationItemView.schedulePopoverIsVisible = false;
//     component.reservationItemView.formCard = reservationItemCardForm;
//     component.reservationItemView.formCard.controls.roomTypeCard.setValue(component.reservationItemView.receivedRoomType);
//   }
//
//   beforeAll(() => {
//     TestBed.resetTestEnvironment();
//     TestBed
//       .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
//       .configureTestingModule({
//         imports: [
//           TranslateModule.forRoot(),
//           RouterTestingModule,
//           FormsModule,
//
//           ReactiveFormsModule,
//           TextMaskModule,
//           MyDateRangePickerModule,
//           HttpClientModule
//         ],
//         declarations: [
//           ReservationItemCardBodyComponent
//         ],
//         providers: [],
//         schemas: [
//           CUSTOM_ELEMENTS_SCHEMA
//         ]
//       });
//     fixture = TestBed.createComponent(ReservationItemCardBodyComponent);
//     component = fixture.componentInstance;
//     component.newReservationForm = newReservationForm;
//     component.reservationItemView = ReservationItemViewData;
//     setReservationItem();
//     options = {
//       dateFormat: 'dd/mm/yyyy',
//       firstDayOfWeek: 'mo',
//       sunHighlight: false,
//       markCurrentDay: true,
//       height: '44px',
//       width: '100%',
//       inline: false,
//       showClearBtn: false,
//       showApplyBtn: false,
//       showSelectDateText: false,
//       alignSelectorRight: false,
//       indicateInvalidDateRange: true,
//       showClearDateRangeBtn: false,
//       editableDateRangeField: false,
//       openSelectorOnInputClick: true,
//       showSelectorArrow: false,
//       disableUntil: {
//         year: 2017,
//         month: 9,
//         day: 12
//       }
//     };
//     spyOn(component['dateService'], 'getConfigDateRangePicker').and.returnValue(options);
//     spyOn(component['personResource'], 'getPersonsByName').and.callFake(() => {
//       return new Promise((resolve, reject) => resolve({}));
//     });
//
//     fixture.detectChanges();
//   });
//
//   describe('on init', () => {
//     it('should set vars', () => {
//       const childAgeList = [{ value: 0, text: '00' }];
//       spyOn(component['propertyService'], 'getChildAgeOfProperty').and.returnValue(childAgeList);
//
//       const reserveHour = new ReservationHour();
//       expect(component.reserveHour).toEqual(reserveHour);
//       expect(component.myDateRangePickerOptions).toEqual(options);
//       expect(component.autocompleItemsGuests).toEqual(new Array<SearchPersonsResultDto>());
//       expect(component.childAgeList[0].value).toEqual(childAgeList[0].value);
//       expect(component.childAgeList[0].text).toEqual(childAgeList[0].text);
//     });
//   });
//
//   describe('on popovers', () => {
//     it('should set reservationItem.schedulePopoverIsVisible for true', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//       const reservationItem = {
//         schedulePopoverIsVisible: false
//       };
//
//       component.showOrHideSchedulePopoverCard(event, reservationItem);
//
//       expect(reservationItem.schedulePopoverIsVisible).toEqual(true);
//     });
//
//     it('should set reservationItem.schedulePopoverIsVisible for false in showOrHideSchedulePopover', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//       const reservationItem = {
//         schedulePopoverIsVisible: true
//       };
//
//       component.showOrHideSchedulePopoverCard(event, reservationItem);
//
//       expect(reservationItem.schedulePopoverIsVisible).toEqual(false);
//     });
//
//     it('should set schedulePopoverIsVisible for true when accomodation is true in keepSchedulePopoverCardVisible', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//       const accomodation = {
//         schedulePopoverIsVisible: true
//       };
//
//       component.keepSchedulePopoverCardVisible(event, accomodation);
//
//       expect(accomodation.schedulePopoverIsVisible).toEqual(true);
//     });
//
//     it('should set schedulePopoverIsVisible for true when accomodation is false in keepSchedulePopoverCardVisible', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//       const accomodation = {
//         schedulePopoverIsVisible: false
//       };
//
//       component.keepSchedulePopoverCardVisible(event, accomodation);
//
//       expect(accomodation.schedulePopoverIsVisible).toEqual(true);
//     });
//
//     it('should set childrenPopoverIsVisible for false in hideChildrenPopoverCardVisible', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//
//       const accommodation = {
//         childrenPopoverIsVisible: true
//       };
//
//       component.hideChildrenPopoverCardVisible(event, accommodation);
//
//       expect(accommodation.childrenPopoverIsVisible).toBeFalsy();
//     });
//
//     it('should createRange for bedTypeList extra', () => {
//       spyOn(component['sharedService'], 'createRange');
//       const numberToRange = 4;
//       component.createRange(numberToRange);
//       expect(component['sharedService'].createRange).toHaveBeenCalledWith(numberToRange);
//     });
//   });
//
//   describe('on valid time', () => {
//     it('should set reserveHour.errorMessage is equal required field', () => {
//       spyOn(component['reservationService'], 'getMessageError').and.returnValue('Campos obrigatórios');
//       const accomodation = {
//         formCard: {
//           controls: {
//             periodCard: [''],
//             adultQuantityCard: [1],
//             childrenQuantityCard: [0],
//             childrenAgeCard: [''],
//             roomTypeCard: [''],
//             checkinHourCard: [''],
//             checkoutHourCard: [''],
//             roomNumberCard: [''],
//             roomLayoutCard: [''],
//             extraBedQuantityCard: [''],
//             cribBedQuantityCard: [''],
//             hasCribBedTypeCard: [false],
//             hasExtraBedTypeCard: [false]
//           }
//         }
//       };
//
//       component.validTimeValue('checkinHourCard', accomodation);
//
//       expect(component.reserveHour.errorMessage).toEqual('Campos obrigatórios');
//
//     });
//
//     it('should set reserveHour.errorMessage to time invalid', () => {
//       spyOn(component['reservationService'], 'getMessageError').and.returnValue('A hora e o minuto estão inválidos');
//       const accomodation = {
//         formCard: {
//           controls: {
//             periodCard: [''],
//             adultQuantityCard: [1],
//             childrenQuantityCard: [0],
//             childrenAgeCard: [''],
//             roomTypeCard: [''],
//             checkinHourCard: [''],
//             checkoutHourCard: '45:45',
//             roomNumberCard: [''],
//             roomLayoutCard: [''],
//             extraBedQuantityCard: [''],
//             cribBedQuantityCard: [''],
//             hasCribBedTypeCard: [false],
//             hasExtraBedTypeCard: [false]
//           }
//         }
//       };
//
//       component.validTimeValue('checkinHourCard', accomodation);
//
//       expect(component.reserveHour.errorMessage).toEqual('A hora e o minuto estão inválidos');
//
//     });
//
//     it('should set reserveHour.errorMessage to required field', () => {
//       spyOn(component['reservationService'], 'getMessageError').and.returnValue('Campos obrigatórios');
//       const accomodation = {
//         formCard: {
//           controls: {
//             periodCard: [''],
//             adultQuantityCard: [1],
//             childrenQuantityCard: [0],
//             childrenAgeCard: [''],
//             roomTypeCard: [''],
//             checkinHourCard: [''],
//             checkoutHourCard: [''],
//             roomNumberCard: [''],
//             roomLayoutCard: [''],
//             extraBedQuantityCard: [''],
//             cribBedQuantityCard: [''],
//             hasCribBedTypeCard: [false],
//             hasExtraBedTypeCard: [false]
//           }
//         }
//       };
//
//       component.validTimeValue('checkoutHourCard', accomodation);
//
//       expect(component.reserveHour.errorMessage).toEqual('Campos obrigatórios');
//
//     });
//
//     it('should set reserveHour.errorMessage to time invalid', () => {
//       spyOn(component['reservationService'], 'getMessageError').and.returnValue('A hora e o minuto estão inválidos');
//       const accomodation = {
//         formCard: {
//           controls: {
//             periodCard: [''],
//             adultQuantityCard: [1],
//             childrenQuantityCard: [0],
//             childrenAgeCard: [''],
//             roomTypeCard: [''],
//             checkinHourCard: [''],
//             checkoutHourCard: '45:45',
//             roomNumberCard: [''],
//             roomLayoutCard: [''],
//             extraBedQuantityCard: [''],
//             cribBedQuantityCard: [''],
//             hasCribBedTypeCard: [false],
//             hasExtraBedTypeCard: [false]
//           }
//         }
//       };
//
//       component.validTimeValue('checkoutHourCard', accomodation);
//
//       expect(component.reserveHour.errorMessage).toEqual('A hora e o minuto estão inválidos');
//     });
//   });
//
//   describe('on Guest', () => {
//     it('should add new property to reservationItemView', () => {
//       component.reservationItemView.guestQuantity[0].formGuest.get('guestName').setValue('Marcelo');
//
//       expect(component.reservationItemView.mainGuestName).toEqual('Marcelo');
//     });
//
//     it('should add new property to reservationItemView when quantityClicksOutInputAndAutocomplete is 0', () => {
//       component.reservationItemView.guestQuantity[0].quantityClicksOutInputAndAutocomplete = 0;
//       component.reservationItemView.guestQuantity[0].formGuest.get('guestName').setValue('Marcelo');
//
//       expect(component.reservationItemView.mainGuestName).toEqual('Marcelo');
//     });
//
//     it('should addGuestAtReservationItem Adult when roomTypeCapacity allowed', () => {
//       const guestAdult = new Guest();
//       guestAdult.status = GuestStatus.NoGuest;
//       guestAdult.formGuest = formBuilder.group({
//         guestName: ['']
//       });
//       guestAdult.isChild = false;
//
//       spyOn(component['guestService'], 'getAdultGuestDefaultValues').and.returnValue(guestAdult);
//       spyOn(component['guestService'], 'getChildGuestDefaultValues');
//
//       const reservationItem = new ReservationItemView();
//       reservationItem.formCard = formBuilder.group({
//         adultQuantityCard: [1],
//         roomTypeCard: [{
//           value: new RoomType()
//         }]
//       });
//       reservationItem.formCard.controls.roomTypeCard.value.adultCapacity = 2;
//       reservationItem.formCard.controls.roomTypeCard.value.childCapacity = 1;
//       reservationItem.guestQuantity = new Array<Guest>();
//
//
//       component.addGuestAtReservationItem(reservationItem, 'adultQuantityCard');
//
//       expect(component['guestService'].getAdultGuestDefaultValues).toHaveBeenCalled();
//       expect(component['guestService'].getChildGuestDefaultValues).not.toHaveBeenCalled();
//       expect(reservationItem.formCard.value.adultQuantityCard).toEqual(2);
//     });
//
//     it('should addGuestAtReservationItem Adult when roomTypeCapacity not allowed', () => {
//       const guestAdult = new Guest();
//       guestAdult.status = GuestStatus.NoGuest;
//       guestAdult.formGuest = formBuilder.group({
//         guestName: ['']
//       });
//       guestAdult.isChild = false;
//
//       spyOn(component['guestService'], 'getAdultGuestDefaultValues').and.returnValue(guestAdult);
//       spyOn(component['guestService'], 'getChildGuestDefaultValues');
//
//       const reservationItem = new ReservationItemView();
//       reservationItem.formCard = formBuilder.group({
//         adultQuantityCard: [2],
//         roomTypeCard: [{
//           value: new RoomType()
//         }]
//       });
//       reservationItem.formCard.controls.roomTypeCard.value.adultCapacity = 2;
//       reservationItem.formCard.controls.roomTypeCard.value.childCapacity = 1;
//       reservationItem.guestQuantity = new Array<Guest>();
//
//
//       component.addGuestAtReservationItem(reservationItem, 'adultQuantityCard');
//
//       expect(component['guestService'].getAdultGuestDefaultValues).toHaveBeenCalled();
//       expect(component['guestService'].getChildGuestDefaultValues).not.toHaveBeenCalled();
//       expect(reservationItem.formCard.value.adultQuantityCard).toEqual(2);
//     });
//
//     it('should addGuestAtReservationItem child when roomTypeCapacity allowed', () => {
//       const guestChild = new Guest();
//       guestChild.status = GuestStatus.NoGuest;
//       guestChild.formGuest = formBuilder.group({
//         guestName: ['']
//       });
//       guestChild.isChild = true;
//
//       spyOn(component['guestService'], 'getAdultGuestDefaultValues');
//       spyOn(component['guestService'], 'getChildGuestDefaultValues').and.returnValue(guestChild);
//
//       const reservationItem = new ReservationItemView();
//       reservationItem.formCard = formBuilder.group({
//         childrenQuantityCard: [0],
//         roomTypeCard: [{
//           value: new RoomType()
//         }]
//       });
//       reservationItem.formCard.controls.roomTypeCard.value.adultCapacity = 2;
//       reservationItem.formCard.controls.roomTypeCard.value.childCapacity = 1;
//       reservationItem.guestQuantity = new Array<Guest>();
//       reservationItem.guestQuantity = new Array<Guest>();
//
//       component.addGuestAtReservationItem(reservationItem, 'childrenQuantityCard');
//
//       expect(component['guestService'].getChildGuestDefaultValues).toHaveBeenCalled();
//       expect(component['guestService'].getAdultGuestDefaultValues).not.toHaveBeenCalled();
//       expect(reservationItem.formCard.value.childrenQuantityCard).toEqual(1);
//     });
//
//     it('should addGuestAtReservationItem child when roomTypeCapacity not allowed grow up guest capacity', () => {
//       const guestChild = new Guest();
//       guestChild.status = GuestStatus.NoGuest;
//       guestChild.formGuest = formBuilder.group({
//         guestName: ['']
//       });
//       guestChild.isChild = true;
//
//       spyOn(component['guestService'], 'getAdultGuestDefaultValues');
//       spyOn(component['guestService'], 'getChildGuestDefaultValues').and.returnValue(guestChild);
//
//       const reservationItem = new ReservationItemView();
//       reservationItem.formCard = formBuilder.group({
//         childrenQuantityCard: [1],
//         roomTypeCard: [{
//           value: new RoomType()
//         }]
//       });
//       reservationItem.formCard.controls.roomTypeCard.value.adultCapacity = 2;
//       reservationItem.formCard.controls.roomTypeCard.value.childCapacity = 1;
//       reservationItem.guestQuantity = new Array<Guest>();
//       reservationItem.guestQuantity = new Array<Guest>();
//
//       component.addGuestAtReservationItem(reservationItem, 'childrenQuantityCard');
//
//       expect(component['guestService'].getChildGuestDefaultValues).toHaveBeenCalled();
//       expect(component['guestService'].getAdultGuestDefaultValues).not.toHaveBeenCalled();
//       expect(reservationItem.formCard.value.childrenQuantityCard).toEqual(1);
//     });
//     it('should guestHandle and set false clearField', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//       spyOn(component['guestService'], 'getGuestStatusCreatingGuest');
//       const dummyElement = document.createElement('input');
//       document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
//
//       component.guestHandle(event, guest, 'edit', 0);
//
//       expect(component['guestService'].getGuestStatusCreatingGuest).toHaveBeenCalledWith();
//       expect(component.clearField).toBeFalsy();
//     });
//
//     it('should guestHandle', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//
//       spyOn(component['guestService'], 'getGuestStatusCreatingGuest');
//
//       component.guestHandle(event, guest, 'create', 1);
//
//       expect(component['guestService'].getGuestStatusCreatingGuest).toHaveBeenCalledWith();
//       expect(component.clearField).toBeTruthy();
//     });
//
//     it('should removeGuestAtReservationItemView', () => {
//       spyOn(component, 'observeChildrenToShowOrHide');
//       const guestAdult = new Guest();
//       guestAdult.status = GuestStatus.NoGuest;
//       guestAdult.formGuest = formBuilder.group({
//         guestName: ['']
//       });
//       guestAdult.isChild = false;
//
//       const indexGuestArray = 1;
//       const accommodation = new ReservationItemView();
//       accommodation.formCard = formBuilder.group({
//         adultQuantityCard: [1]
//       });
//       accommodation.guestQuantity = new Array<Guest>();
//
//       accommodation.guestQuantity.push(guestAdult);
//       accommodation.guestQuantity.push(guestAdult);
//
//       component.removeGuestAtReservationItemView(accommodation, indexGuestArray);
//
//       expect(accommodation.guestQuantity[1]).toBeUndefined();
//       expect(accommodation.formCard.value.adultQuantityCard).toEqual(0);
//       expect(component['observeChildrenToShowOrHide']).toHaveBeenCalled();
//     });
//
//     it('should removeGuestAtReservationItemView', () => {
//       spyOn(component, 'observeChildrenToShowOrHide');
//       const guestChild = new Guest();
//       guestChild.status = GuestStatus.NoGuest;
//       guestChild.formGuest = formBuilder.group({
//         guestName: ['']
//       });
//       guestChild.isChild = false;
//
//       const indexGuestArray = 1;
//       const accommodation = new ReservationItemView();
//       accommodation.formCard = formBuilder.group({
//         adultQuantityCard: [3]
//       });
//       accommodation.guestQuantity = new Array<Guest>();
//
//       accommodation.guestQuantity.push(guestChild);
//       accommodation.guestQuantity.push(guestChild);
//
//       component.removeGuestAtReservationItemView(accommodation, indexGuestArray);
//
//       expect(accommodation.guestQuantity[1]).toBeUndefined();
//       expect(accommodation.formCard.value.adultQuantityCard).toEqual(2);
//     });
//
//     it('should keepGuestInputOpen', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//       spyOn(event, 'stopPropagation');
//
//       component.keepGuestInputOpen(event);
//
//       expect(event.stopPropagation).toHaveBeenCalledWith();
//     });
//
//     it('should chooseGuest', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//       const searchPersonsResultDto = new SearchPersonsResultDto();
//       searchPersonsResultDto.guestId = 112;
//       searchPersonsResultDto.id = personIdMock;
//       searchPersonsResultDto.name = 'Abc';
//
//       setGuest();
//
//       spyOn(component['guestService'], 'getGuestStatusCreatingGuest');
//
//       component.chooseGuest(event, guest, searchPersonsResultDto);
//
//       expect(guest.showAutocompleItemsGuests).toBeFalsy();
//       expect(guest.personId).toEqual(searchPersonsResultDto.id);
//       expect(guest.formGuest.get('guestName').value).toEqual(searchPersonsResultDto.name);
//     });
//
//     it('should saveReservation and redirect to guestDetailsPage', () => {
//       spyOn(component['reservationItemViewService'], 'isAccommodationSaved').and.returnValue(false);
//       spyOn(component['modalEmitService'], 'emitSimpleModal');
//       spyOn(component['reservationService'], 'getModalContent').and.returnValue(component['translateService']
// .instant('reservationModule.reserveCreate.confirmReserveSaveAndGoToRoomList'));
//       spyOn(component['reservationService'], 'getStatusIsPossibleToSaveReserve').and.returnValue(false);
//       spyOn(component['reservationService'], 'reservationConfirmationGroupIsInvalid').and.returnValue(false);
//
//       const accommodation = new ReservationItemView();
//       accommodation.adultQuantity = 1;
//       accommodation.code = '123';
//
//       component.goToGuestPage(accommodation);
//
//       expect(component['modalEmitService'].emitSimpleModal).toHaveBeenCalledWith(
//         component['translateService'].instant('commomData.warning'),
//         component['translateService'].instant('reservationModule.reserveCreate.confirmReserveSaveAndGoToRoomList'),
//         jasmine.any(Function),
//         []
//       );
//     });
//
//     it('should emit toaster when all fields is not filled', () => {
//       spyOn(component['reservationItemViewService'], 'isAccommodationSaved').and.returnValue(false);
//       spyOn(component['reservationService'], 'getStatusIsPossibleToSaveReserve').and.returnValue(true);
//       spyOn(component['reservationService'], 'reservationConfirmationGroupIsInvalid').and.returnValue(true);
//       spyOn(component['toasterEmitService'], 'emitChange');
//
//       const accommodation = new ReservationItemView();
//       accommodation.adultQuantity = 1;
//       accommodation.code = '123';
//       component.newReservationForm = newReservationForm;
//
//       component.goToGuestPage(accommodation);
//
//       expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
//         SuccessError.error,
//         component['translateService'].instant('reservationModule.commomData.errorMessageToSaveReserve')
//       );
//     });
//
//     it('should show ModalConfirm if change values', () => {
//       spyOn(component['reservationItemViewService'], 'isAccommodationSaved').and.returnValue(true);
//       spyOn(component['reservationService'], 'getStatusIsPossibleToSaveReserve').and.returnValue(false);
//       spyOn(component['reservationService'], 'reservationConfirmationGroupIsInvalid').and.returnValue(false);
//       spyOn(component, 'openConfirmReservationSaveModal');
//
//       component.reservationItemView.formCard.markAsDirty();
//       const reservationItem = new ReservationItemView();
//       component.goToGuestPage(reservationItem);
//
//       expect(component['reservationService'].getStatusIsPossibleToSaveReserve).toHaveBeenCalled();
//       expect(component['openConfirmReservationSaveModal']).toHaveBeenCalled();
//     });
//
//     it('should redirect to guestDetailsPage', () => {
//       spyOn(component, 'isCheckShowModal').and.returnValue(false);
//       spyOn(component['reservationItemViewService'], 'isAccommodationSaved').and.returnValue(true);
//       spyOn(component.saveReservation, 'emit');
//
//       const accommodation = new ReservationItemView();
//       accommodation.adultQuantity = 1;
//       accommodation.code = '123';
//       component.goToGuestPage(accommodation);
//
//       const hasRedirect = true;
//       expect(component.saveReservation.emit).toHaveBeenCalledWith(hasRedirect);
//     });
//
//     it('should showChildrenPopoverCard when childrenQuantityCard is greather than zero', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//
//       const reservationItemView = new ReservationItemView();
//       reservationItemView.formCard = formBuilder.group({
//         childrenQuantityCard: [1],
//         childrenAgeListCard: formBuilder.array([formBuilder.group({
//           childAgeCard: [0]
//         })])
//       });
//
//       component.showChildrenPopoverCard(event, reservationItemView);
//
//       reservationItemView.formCard.get('childrenQuantityCard').setValue(1);
//       const childrenQuantityCardFormArray = <FormArray>reservationItemView.formCard.get('childrenAgeListCard');
//
//       expect(reservationItemView.childrenPopoverIsVisible).toBeTruthy();
//       expect(childrenQuantityCardFormArray.controls[0].get('childAgeCard').value).toEqual(0);
//     });
//
//     it('should showChildrenPopoverCard when childrenQuantityCard is equals zero', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//
//       const reservationItemView = new ReservationItemView();
//       reservationItemView.formCard = formBuilder.group({
//         childrenQuantityCard: [1],
//         childrenAgeListCard: formBuilder.array([formBuilder.group({
//           childAgeCard: [0]
//         })])
//       });
//
//       component.showChildrenPopoverCard(event, reservationItemView);
//
//       reservationItemView.formCard.get('childrenQuantityCard').setValue(0);
//       const childrenQuantityCardFormArray = <FormArray>reservationItemView.formCard.get('childrenAgeListCard');
//
//       expect(reservationItemView.childrenPopoverIsVisible).toBeFalsy();
//       expect(childrenQuantityCardFormArray.controls.length).toEqual(0);
//     });
//
//   });
//
//   describe('on actions', () => {
//     it('should keepChildrenPopoverCardVisible', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//
//       component.keepChildrenPopoverCardVisible(event, ReservationItemViewData);
//
//       expect(ReservationItemViewData.childrenPopoverIsVisible).toBeTruthy();
//     });
//
//     it('should showOrHideBedsIncludedPopoverCard', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//
//       component.showOrHideBedsIncludedPopoverCard(event, ReservationItemViewData);
//
//       expect(ReservationItemViewData.bedsIncludedPopoverIsVisible).toBeTruthy();
//     });
//
//     it('should keepBedsIncludedPopoverCardVisible', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function () { }
//       };
//
//       component.keepBedsIncludedPopoverCardVisible(event, ReservationItemViewData);
//
//       expect(ReservationItemViewData.bedsIncludedPopoverIsVisible).toBeTruthy();
//     });
//   });
// });
