import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ReservationItemView } from '@app/shared/models/reserves/reservation-item-view';
import { ReservationStatusEnum } from '@app/shared/models/reserves/reservation-status-enum';
import { SuccessError } from '@app/shared/models/success-error-enum';
import { GuestService } from '@app/shared/services/guest/guest.service';
import { ToasterEmitService } from '@app/shared/services/shared/toaster-emit.service';
import { ReservationItemViewService } from '@app/shared/services/reservation-item-view/reservation-item-view.service';
import { StatusStyleService } from '@app/shared/services/reservation/status-style.service';
import { ReservationService } from '@app/shared/services/reservation/reservation.service';
import { ReservationBudget, ReservationWithBudget } from '@app/shared/models/revenue-management/reservation-budget/reservation-budget';
import { ModalEmitToggleService } from '@app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from '@app/shared/services/shared/shared.service';
import { ReservationItemBudget } from '@app/shared/models/revenue-management/reservation-budget/reservation-item-budget';
import { ReservationBudgetService } from '@app/shared/services/reservation-budget/reservation-budget.service';
import { BudgetService } from '@app/shared/services/budget/budget.service';
import { ActivatedRoute } from '@angular/router';
import { ReservationBudgetResource } from '@app/shared/resources/reservation-budget/reservation-budget.resource';
import { CurrencyFormatPipe } from '@app/shared/pipes/currency-format.pipe';
import {
  ReservationItemCommercialBudget
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import {
  OldAndNew,
  ReservationBudgetStatusChange,
  ReservationMantainStatusService
} from 'app/reservation/services/reservation-mantain-status.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { FormBuilder } from '@angular/forms';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'reservation-item-card',
  templateUrl: 'reservation-item-card.component.html'
})

export class ReservationItemCardComponent implements OnInit {
  public searchParams: Array<string>;
  public propertyId: number;
  public reservationItemCardBudgetModalId = 'budgetReservationItem';
  public showReservationItemCardBudgetDetail: boolean;
  public reservationItemBudget: ReservationItemBudget;

  // TODO novo orçamento
  public currentIdentifier: string;
  public currentCommercialBudgetList: Array<ReservationItemCommercialBudget>;
  public currentCommercialBudgetSelectedItemKey: any;
  public currentSelectedReservationBudgetList: Array<ReservationBudget>; // contem os valores modificados pelo usuário
  public currentPeriod: any;
  public currentGratuityType: number;
  public currentOriginalReservationBudgetList: Array<ReservationBudget>; // contém os valores do ultimo que foi salvo / edição
  public currentReservationItem: ReservationItemView;

  public callModal = true;

  _reservationItemViewList: ReservationItemView[];
  @Input() set reservationItemViewList(val: Array<any>) {
    this._reservationItemViewList = val;
    if (val && val.length) {
      val.forEach(item => {
        const mantain = new ReservationBudgetStatusChange();
        mantain.period = new OldAndNew({...item.period});
        mantain.adultQuantity = new OldAndNew(item.adultQuantity);
        mantain.childrenQuantity = new OldAndNew(item.childrenQuantity);
        mantain.childAges = new OldAndNew([...item.childrenListCard]);
        mantain.uhType = new OldAndNew({...item.requestedRoomType});
        mantain.commercialBudget = new OldAndNew(item.budgetCommercialBudgetList);
        mantain.commercialBudgetKey = new OldAndNew(item.budgetCommercialBudgetItemKey);
        mantain.reservationBudget = new OldAndNew(item.budgetReservationBudgetList);
        mantain.reservationBudgetOriginal = new OldAndNew([...item.budgetReservationBudgetList]);
        mantain.guestList = new OldAndNew([...item.guestQuantity]);
        mantain.gratuityType = new OldAndNew(item.gratuityTypeId);
        this.reservationMantainService.setValue(`${item.reservationItemUid}+${item.code}`, mantain);
      });
    }
  }

  get reservationItemViewList() {
    return this._reservationItemViewList;
  }

  @Input() isRoomTypeWithoutBaseRate: boolean;
  @Input() searchData: string;
  @Input() companyClientId: string;
  @Input() reservationItemId: number;
  @Input() readonly: boolean;
  @Output() createReserve: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();
  @Output() showBudgetModal = new EventEmitter<ReservationItemView>();
  @Output() reactivateReservationItemEvent: EventEmitter<ReservationItemView> = new EventEmitter<ReservationItemView>();
  @Output() sendBudgetForReservationCreate = new EventEmitter();
  @Output() setTotalsOfReservation = new EventEmitter();

  constructor(
    private guestService: GuestService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private reservationBudgetService: ReservationBudgetService,
    private route: ActivatedRoute,
    private budgetService: BudgetService,
    private reservationBudgetResource: ReservationBudgetResource,
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private reservationItemViewService: ReservationItemViewService,
    private reservationService: ReservationService,
    private currencyFormatPipe: CurrencyFormatPipe,
    private statusStyleService: StatusStyleService,
    private reservationMantainService: ReservationMantainStatusService,
    private dateService: DateService,
    private formBuilder: FormBuilder,
    private propertyStorageService: PropertyStorageService
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  setVars() {
    this.propertyId = this.propertyStorageService.getCurrentPropertyId();
    this.searchParams = ['code', 'mainGuestName'];
    this.setStatusColorFooter();
    this.reservationService.checkStatusReservationItemCanceledToReorderReservationItemList(this.reservationItemViewList);
  }

  public setReservationItemCommercialBudgetAndCallBudgetModal(reservationItemView: ReservationItemView, index, id?) {
    this.currentReservationItem = reservationItemView;
    this.isUpdatingReservation(reservationItemView, id);
  }

  private openModal(indentifier) {
    const element = this.reservationMantainService.getValue(indentifier);

    this.currentIdentifier = indentifier;
    this.currentPeriod = element.period.newValue;
    this.currentCommercialBudgetList = element.commercialBudget.newValue;
    this.currentCommercialBudgetSelectedItemKey = this.reservationBudgetService
      .prepareAndReturnBudgetKey({...element.commercialBudgetKey.newValue}, element.commercialBudget.newValue );
    this.currentSelectedReservationBudgetList = element.reservationBudget.newValue;
    this.currentGratuityType = element.gratuityType.newValue;
    this.currentOriginalReservationBudgetList = element.reservationBudgetOriginal.newValue;


    setTimeout(() => {
      this.modalEmitToggleService.emitOpenWithRef(this.reservationItemCardBudgetModalId);
    }, 500);
  }

  private isUpdatingReservation(reservationItemView?, id?) {
    const reservationIdentifier = `${reservationItemView.reservationItemUid}+${reservationItemView.code}`;
    const {
      periodCard, adultQuantityCard, roomTypeRequestedCard, childrenAgeListCard
    } = reservationItemView.formCard.getRawValue();
    const { companyClientId } = this;

    const childAgeArray = [];
    if (childrenAgeListCard) {
      childrenAgeListCard.forEach( value => {
        childAgeArray.push(value.childAge);
      });
    }
    this.currentIdentifier = reservationIdentifier;

    const dateList = this.dateService.convertPeriodToDateList(periodCard.beginDate, periodCard.endDate);
    const reservationItemCommercialBudget = this.reservationBudgetService.getReservationItemCommercialBudgetSearch(
      this.propertyId,
      roomTypeRequestedCard.id,
      dateList,
      reservationItemView.companyClientId ? reservationItemView.companyClientId : companyClientId,
      adultQuantityCard,
      childAgeArray,
      reservationItemView.id
    );

    if (reservationItemView.id) {
      this.reservationBudgetResource.getReservationItemBudget(reservationItemCommercialBudget).subscribe(reservationItemBudget => {

        if (id != 'header') {
          this.reservationMantainService.newValueIn(reservationIdentifier, 'reservationBudget', undefined);
          this.reservationMantainService.newValueIn(reservationIdentifier, 'period', periodCard);
          this.reservationMantainService.newValueIn(reservationIdentifier, 'uhType', roomTypeRequestedCard);
          this.reservationMantainService.newValueIn(reservationIdentifier, 'adultQuantity', adultQuantityCard);
          this.reservationMantainService.newValueIn(reservationIdentifier, 'childAges', childrenAgeListCard);
          this.reservationMantainService.newValueIn(reservationIdentifier,
            'commercialBudgetKey',
            reservationItemView.budgetCommercialBudgetItemKey);
          this.reservationMantainService.newValueIn(reservationIdentifier,
            'guestList',
            [...reservationItemView.guestQuantity]);
        }

        const saved = this.reservationMantainService.getValue(reservationIdentifier);
        if (!saved.commercialBudget.oldValue && !saved.commercialBudget.newValue) {
          saved.commercialBudget = new OldAndNew(reservationItemBudget.commercialBudgetList);
          this.reservationMantainService.setValue(reservationIdentifier, saved);
        } else {

          this.reservationMantainService.newValueIn(reservationIdentifier,
            'commercialBudget',
            reservationItemBudget.commercialBudgetList);
        }

        this.openModal(reservationIdentifier);
      });
    } else {

      this.reservationBudgetResource.getReservationItemBudget(reservationItemCommercialBudget).subscribe(reservationItemBudget => {
        if (id != 'header') {
          this.reservationMantainService.newValueIn(reservationIdentifier, 'period', periodCard);
          this.reservationMantainService.newValueIn(reservationIdentifier, 'uhType', roomTypeRequestedCard);
          this.reservationMantainService.newValueIn(reservationIdentifier, 'adultQuantity', adultQuantityCard);
          this.reservationMantainService.newValueIn(reservationIdentifier, 'childAges', childrenAgeListCard);
          this.reservationMantainService.newValueIn(reservationIdentifier,
            'commercialBudget', reservationItemBudget.commercialBudgetList);
          this.reservationMantainService.newValueIn(reservationIdentifier,
            'commercialBudgetKey',
            reservationItemView.budgetCommercialBudgetItemKey);
          this.reservationMantainService.newValueIn(reservationIdentifier,
            'guestList',
            [...reservationItemView.guestQuantity]);
          this.reservationMantainService.newValueIn(reservationIdentifier,
            'reservationBudget', undefined);
        }
        this.openModal(reservationIdentifier);
      });

    }
  }

  private setStatusColorFooter() {
    this.reservationItemViewList = this.reservationService.getReservationItemViewListWithStatusColorFooter(this.reservationItemViewList);
  }

  public saveReservation(openResume) {
    this.createReserve.emit(openResume);
  }

  public setFocusCard(accommodationSelected, index) {
    accommodationSelected.focus = !accommodationSelected.focus;
    this.reservationItemViewList.forEach((accommodation, i) => {
      if (index !== i) {
        accommodation.focus = false;
      }
    });
  }

  public onSendBudgetForReservationCreate(reservationWithBudget: ReservationWithBudget) {
    this.sendBudgetForReservationCreate.emit(reservationWithBudget);
  }

  public hidePopoversCard(reservationItemCard) {
    reservationItemCard.schedulePopoverIsVisible = false;
    reservationItemCard.childrenPopoverIsVisible = false;
    reservationItemCard.bedsIncludedPopoverIsVisible = false;
    reservationItemCard.extrasBedPopoverIsVisible = false;
  }

  public setStatusAndDisableOrEnableGuests(reservationItemView: ReservationItemView) {
    reservationItemView.guestQuantity.forEach((guest) => {
      guest.showAutocompleItemsGuests = false;
      guest.state = this.guestService.getGuestState(guest);
    });
    this.disableOrEnableGuests(this.reservationItemViewList);
  }

  private disableOrEnableGuests(reservationItemViewList) {
    reservationItemViewList.forEach((data) => {
      this.validGuestLinkIsDisabled(data);
    });
  }

  private validGuestLinkIsDisabled(data) {
    data.guestQuantity.forEach((guest, index) => {
      if (this.guestService.guestNameIsEmpty(data)) {
        guest.isDisabledGuest = index != 0 ? true : false;
      } else {
        guest.isDisabledGuest = false;
      }
    });
  }

  public showBudgetModalForReservationItemViewExistent(reservationItemView: ReservationItemView) {
    this.showBudgetModal.emit(reservationItemView);
  }

  public deleteReservationItem(reservationItem: ReservationItemView) {
    this.reservationItemViewList.forEach((reservationItemOfList, index) => {
      if (reservationItemOfList.code === reservationItem.code) {
        if (reservationItemOfList.id > 0) {
          this.checkIfHasSetReservationItemStatusCanceled(reservationItem);
        } else {
          this.reservationItemViewList.splice(index, 1);
          this.setTotalsOfReservation.emit(this.reservationItemViewList);
        }
      }
    });
    this.reservationService.checkStatusReservationItemCanceledToReorderReservationItemList(this.reservationItemViewList);
    this.setStatusColorFooter();
  }

  public reactivateReservationItem(reservationItem: ReservationItemView) {
    this.reservationService.checkStatusReservationItemCanceledToReorderReservationItemList(this.reservationItemViewList);
    this.setStatusColorFooter();
    this.reactivateReservationItemEvent.emit(reservationItem);
  }

  private checkIfHasSetReservationItemStatusCanceled(reservationItem: ReservationItemView) {
    if (!this.reservationItemViewService.hasStatusChanged(reservationItem.status)) {
      this.toasterEmitService.emitChange(
        SuccessError.error,
        this.translateService.instant('commomData.status.errorUnchangedStatus')
      );
    } else {
      reservationItem.status = ReservationStatusEnum.Canceled;
      reservationItem.isCanceledStatus = this.statusStyleService.isCanceledStatus(reservationItem.status);
      reservationItem.focus = false;
    }
  }

  public cancelBudgetModal(item) {
    this.callModal = false;
    const reservationUID = this.currentIdentifier.split('+')[0];
    const reservationCode = this.currentIdentifier.split('+')[1];
    const element = this.reservationMantainService.getValue(this.currentIdentifier);

    const reservationItemView = this.reservationItemViewList
      .find(reservation => reservation.reservationItemUid == reservationUID && reservation.code == reservationCode);

    reservationItemView.formCard.get('emitBudgetEvent').setValue(false);
    const childrenAgesList = element.childAges ? element.childAges.oldValue : [];
    const options = {emitEvent: false, onlySelf: true};
    reservationItemView.formCard.get('periodCard').setValue(element.period.oldValue, options);
    reservationItemView.formCard.get('endDate').setValue(element.period.oldValue.endDate, options);
    reservationItemView.formCard
      .setControl('childrenAgeListCard', this.formBuilder.array(this.reservationService
        .getChildAgeListFormGroupWithValue(childrenAgesList.map(childrenAge => childrenAge.childAge))));
    reservationItemView.formCard.get('adultQuantityCard').setValue(element.adultQuantity.oldValue, options);
    reservationItemView.formCard.get('childrenQuantityCard').setValue(childrenAgesList.length, options);

    if (element.uhType.oldValue) {
      const roomType = reservationItemView.roomTypes.find( rt => rt.id == element.uhType.oldValue.id );
      reservationItemView.formCard.get('roomTypeRequestedCard').setValue(roomType , options);
    }
    reservationItemView.guestQuantity = [...element.guestList.oldValue];
    reservationItemView.formCard.get('emitBudgetEvent').setValue(true);

    this.reservationMantainService.resetToOldValues(this.currentIdentifier);
    this.cleanCurrentData();

    this.modalEmitToggleService.emitCloseWithRef(this.reservationItemCardBudgetModalId);
  }

  private cleanCurrentData() {
    this.currentIdentifier = null;
    this.currentPeriod = null;
    this.currentCommercialBudgetList = null;
    this.currentCommercialBudgetSelectedItemKey = null;
    this.currentSelectedReservationBudgetList = null;
    this.currentGratuityType = null;
  }

  public confirmBudgetModal(item) {
    const reservationUID = this.currentIdentifier.split('+')[0];
    const reservationCode = this.currentIdentifier.split('+')[1];

    const reservationItemView = this.reservationItemViewList
      .find(reservation => reservation.reservationItemUid == reservationUID && reservation.code == reservationCode);

    const key = {
      currencyId: item.commercialBudgetItemKey.currencyId,
      mealPlanTypeId: item.commercialBudgetItemKey.mealPlanTypeId,
      roomTypeId: item.commercialBudgetItemKey.roomTypeId,
      ratePlanId: item.commercialBudgetItemKey.ratePlanId
    };
    this.reservationMantainService.newValueIn(this.currentIdentifier, 'adultQuantityCard',
      reservationItemView.formCard.get('adultQuantityCard').value);
    this.reservationMantainService.newValueIn(this.currentIdentifier, 'gratuityType', item.gratuityTypeId);
    this.reservationMantainService.newValueIn(this.currentIdentifier, 'commercialBudgetKey', key);
    this.reservationMantainService.newValueIn(this.currentIdentifier, 'reservationBudget', item.reservationBudgetList);
    this.reservationMantainService.newValueIn(this.currentIdentifier, 'reservationBudgetOriginal', [...item.reservationBudgetList]);

    reservationItemView.reservationItemCommercialBudgetConfig.reservationBudgetTableList = item.reservationBudgetList;
    reservationItemView.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent = item.commercialBudgetItem;
    reservationItemView.budgetReservationBudgetList = item.reservationBudgetList;
    reservationItemView.budgetCommercialBudgetList = item.commercilBudgetItemList;
    reservationItemView.budgetCommercialBudgetItemKey = item.commercialBudgetItemKey;
    reservationItemView.budgetGratuityTypeId = item.gratuityTypeId;
    reservationItemView.period = reservationItemView.formCard.get('periodCard').value;
    reservationItemView.initialPeriod = reservationItemView.formCard.get('periodCard').value;
    reservationItemView.mealPlanTypeId = item.commercialBudgetItemKey.mealPlanTypeId;
    reservationItemView.ratePlanId = item.commercialBudgetItemKey.ratePlanId;
    reservationItemView.gratuityTypeId = item.gratuityTypeId;
    reservationItemView.adultQuantity = reservationItemView.formCard.get('adultQuantityCard').value;
    reservationItemView.childrenQuantity = reservationItemView.formCard.get('childrenQuantityCard').value;
    reservationItemView.currencyId = item.commercialBudgetItem.currencyId;


    this.reservationMantainService.resetToNewValues(this.currentIdentifier);

    const reservationWithBudget = new ReservationWithBudget();
    reservationWithBudget.reservationItemCommercialBudget = item.reservationBudgetList;
    reservationWithBudget.gratuityTypeId = item.gratuityTypeId;
    reservationWithBudget.reservationItemCommercialBudget = reservationItemView
      .reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent;
    reservationWithBudget.commercialBudgetItemKey = item.commercialBudgetItemKey;
    reservationWithBudget.commercilBudgetItemList = item.commercilBudgetItemList;

    this.sendBudgetForReservationCreate.emit(reservationWithBudget);
    this.setTotalsOfReservation.emit(this.reservationItemViewList);
    this.showReservationItemCardBudgetDetail = false;

    this.modalEmitToggleService.emitCloseWithRef(this.reservationItemCardBudgetModalId);
    this.cleanCurrentData();
  }
}
