// TODO: uncomment [DEBIT: 7517]

// import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http/';
// import { RouterTestingModule } from '@angular/router/testing';
// import { TranslateModule } from '@ngx-translate/core';
// import { SharedModule } from '../../../shared/shared.module';
// import { ReservationItemCardComponent } from './reservation-item-card.component';
// import { Guest } from '../../../shared/models/reserves/guest';
// import { ReservationItemView } from '../../../shared/models/reserves/reservation-item-view';
// import { ReservationItemViewData,
// ReservationItemViewData2,
// ReservationItemViewData3,
// ReservationItemViewToConfirmData,
// ReservationItemViewCheckinData, ReservationItemViewToConfirmIdZeroData } from '../../../shared/mock-data/reservationItem-data';
// import { ReservationStatusEnum } from '../../../shared/models/reserves/reservation-status-enum';
// import { SuccessError } from 'app/shared/models/success-error-enum';
// import { GuestService } from '../../../shared/services/guest/guest.service';
// import { DateService } from '../../../shared/services/shared/date.service';
// import { SharedService } from '../../../shared/services/shared/shared.service';
// import { ReservationItemViewService } from '../../../shared/services/reservation-item-view/reservation-item-view.service';
// import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
// import { StatusStyleService } from '../../../shared/services/reservation/status-style.service';
// import { ReservationService } from '../../../shared/services/reservation/reservation.service';
// import { ValidatorFormService } from '../../../shared/services/shared/validator-form.service';
// import { RoomTypeService } from '../../../shared/services/room-type/room-type.service';
// import { PersonResource } from '../../../shared/resources/person/person.resource';
// import { RoomLayoutResource } from '../../../shared/resources/room-layout/room-layout.resource';
// import { FilterListPipe } from '../../../shared//pipes/filter-select.pipe';
//
// describe('ReservationItemCardComponent', () => {
//     let component: ReservationItemCardComponent;
//     let fixture: ComponentFixture<ReservationItemCardComponent>;
//     const  formBuilder = new FormBuilder();
//
//     beforeAll(() => {
//         TestBed.resetTestEnvironment();
//         TestBed
//           .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
//           .configureTestingModule({
//             imports: [
//                 TranslateModule.forRoot(),
//                 RouterTestingModule,
//                 FormsModule,
//                 HttpModule,
//                 ReactiveFormsModule,
//                 HttpClientModule
//             ],
//             declarations: [
//                 ReservationItemCardComponent,
//                 FilterListPipe
//             ],
//             providers: [
//                 GuestService,
//                 DateService,
//                 PersonResource,
//                 SharedService,
//                 ReservationItemViewService,
//                 ToasterEmitService,
//                 StatusStyleService,
//                 ReservationService,
//                 ValidatorFormService,
//                 RoomLayoutResource,
//                 RoomTypeService
//             ],
//             schemas: [
//                 CUSTOM_ELEMENTS_SCHEMA
//             ]
//           });
//         fixture = TestBed.createComponent(ReservationItemCardComponent);
//         component = fixture.componentInstance;
//         component.reservationItemViewList = new Array<ReservationItemView>();
//         component.reservationItemViewList.push(ReservationItemViewData);
//         component.searchData = '';
//         component.newReservationForm = new FormBuilder().group({
//             contactGroup: formBuilder.group({
//               name: 'name',
//               email: 'email',
//               phone: 'phone'
//             }),
//             selectValuePaymentType: formBuilder.group({
//               paymentType: ['']
//             }),
//         });
//
//         spyOn(component, 'createReserve').and.callThrough();
//         spyOn(component, 'showBudgetModal').and.callThrough();
//         spyOn(component['reservationService'], 'checkStatusReservationItemCanceledToReorderReservationItemList');
//
//         fixture.detectChanges();
//     });
//
//     describe('on init', () => {
//         it('should set vars', () => {
//             expect(component.searchParams).toEqual(['code', 'mainGuestName']);
//
//             const statusColor = {
//                 'thf-u-background-color--cancel': true,
//                 'thf-u-background-color--confirmed': false,
//                 'thf-u-background-color--to-confirm': false,
//                 'thf-u-background-color--no-show': false,
//                 'thf-u-background-color--checkin': false,
//                 'thf-u-background-color--pending': false,
//                 'thf-u-background-color--checkout': false,
//                 'thf-u-background-color--wait-list': false
//             };
//             component.reservationItemViewList[0].statusColorFooter = statusColor;
//             spyOn(component['reservationService'],
// 'getReservationItemViewListWithStatusColorFooter').and.returnValue(component.reservationItemViewList);
//
//             component['setVars']();
//
//             expect(component.reservationItemViewList[0].statusColorFooter).toEqual(statusColor);
//         });
//     });
//
//     describe('on form methods', () => {
//         it('should setFocusCard', () => {
//             component.setFocusCard(ReservationItemViewData , 1);
//             expect(ReservationItemViewData.focus).toBeFalsy();
//         });
//
//         it('should set popoversCard is visible for false', () => {
//             const reservationItemCard = {
//                 schedulePopoverIsVisible: true,
//                 childrenPopoverIsVisible: true,
//                 bedsIncludedPopoverIsVisible: true,
//                 extrasBedPopoverIsVisible: true
//             };
//
//             component.hidePopoversCard(reservationItemCard);
//
//             expect(reservationItemCard.schedulePopoverIsVisible).toEqual(false);
//             expect(reservationItemCard.childrenPopoverIsVisible).toEqual(false);
//             expect(reservationItemCard.bedsIncludedPopoverIsVisible).toEqual(false);
//             expect(reservationItemCard.extrasBedPopoverIsVisible).toEqual(false);
//         });
//
//         it('should setStatusAndDisableOrEnableGuests', () => {
//             const guest = new Guest();
//
//             spyOn(component['guestService'], 'getGuestStatus');
//             spyOn(component['guestService'], 'guestNameIsEmpty').and.returnValue(true);
//
//             component.reservationItemViewList = new Array<ReservationItemView>();
//             component.reservationItemViewList[0] = new ReservationItemView();
//             component.reservationItemViewList[0].guestQuantity = new Array<Guest>();
//             component.reservationItemViewList[0].guestQuantity.push(guest);
//             component.reservationItemViewList[0].guestQuantity.push(guest);
//
//             component.setStatusAndDisableOrEnableGuests(component.reservationItemViewList[0]);
//
//             expect(component['guestService'].getGuestStatus).toHaveBeenCalledWith(guest);
//             expect(guest.showAutocompleItemsGuests).toBeFalsy();
//         });
//
//         it('should setStatusAndDisableOrEnableGuests', () => {
//             const guest = new Guest();
//
//             spyOn(component['guestService'], 'getGuestStatus');
//             spyOn(component['guestService'], 'guestNameIsEmpty').and.returnValue(false);
//
//             component.reservationItemViewList = new Array<ReservationItemView>();
//             component.reservationItemViewList[0] = new ReservationItemView();
//             component.reservationItemViewList[0].guestQuantity = new Array<Guest>();
//             component.reservationItemViewList[0].guestQuantity.push(guest);
//             component.reservationItemViewList[0].guestQuantity.push(guest);
//
//             component.setStatusAndDisableOrEnableGuests(component.reservationItemViewList[0]);
//
//             expect(component['guestService'].getGuestStatus).toHaveBeenCalledWith(guest);
//             expect(guest.showAutocompleItemsGuests).toBeFalsy();
//             expect(guest.isDisabledGuest).toBeFalsy();
//         });
//
//         it('should emit event to saveReservation', () => {
//             spyOn(component.createReserve, 'emit');
//             const hasRedirect = true;
//             component.saveReservation(hasRedirect);
//             expect(component.createReserve.emit).toHaveBeenCalledWith(hasRedirect);
//         });
//
//         it('should emit event to showBudgetModal', () => {
//             spyOn(component.showBudgetModal, 'emit');
//             const reservationItemView = new ReservationItemView();
//
//             component.showBudgetModalForReservationItemViewExistent(reservationItemView);
//
//             expect(component.showBudgetModal.emit).toHaveBeenCalledWith(reservationItemView);
//         });
//     });
//
//     describe('on actions to reservationItem', () => {
//         it('should not changed status to canceled when the status is different ToConfirm/Confirmed and EditPage', () => {
//             component.reservationItemViewList = new Array<ReservationItemView>();
//             component.reservationItemViewList.push(ReservationItemViewCheckinData);
//             component.reservationItemViewList.push(ReservationItemViewData2);
//             component.reservationItemViewList.push(ReservationItemViewData3);
//             spyOn(component['reservationItemViewService'], 'hasStatusChanged').and.returnValue(false);
//             spyOn(component['toasterEmitService'], 'emitChange');
//
//             component.deleteReservationItem(ReservationItemViewCheckinData);
//
//             expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
//                 SuccessError.error,
//                 component['translateService'].instant('commomData.status.errorUnchangedStatus'));
//             expect(component.reservationItemViewList).toContain(ReservationItemViewCheckinData);
//             expect(component.reservationItemViewList).toContain(ReservationItemViewData2);
//             expect(component.reservationItemViewList).toContain(ReservationItemViewData3);
//             expect(component['reservationService']['checkStatusReservationItemCanceledToReorderReservationItemList'])
// .toHaveBeenCalledWith(component.reservationItemViewList);
//         });
//
//         it('should changed status to canceled when the status is equal ToConfirm/Confirmed and EditPage', () => {
//             component.reservationItemViewList = new Array<ReservationItemView>();
//             component.reservationItemViewList.push(ReservationItemViewToConfirmData);
//             component.reservationItemViewList.push(ReservationItemViewToConfirmIdZeroData);
//             spyOn(component['reservationItemViewService'], 'hasStatusChanged').and.returnValue(true);
//
//             const reservationItem = ReservationItemViewToConfirmData;
//             component.deleteReservationItem(ReservationItemViewToConfirmData);
//             expect(component.reservationItemViewList).toContain(reservationItem);
//             expect(component.reservationItemViewList.length).toEqual(1);
//         });
//
//         it('should delete reservationItem when is not EditPage', () => {
//             component.reservationItemViewList = new Array<ReservationItemView>();
//             component.reservationItemViewList.push(ReservationItemViewCheckinData);
//             component.reservationItemViewList.push(ReservationItemViewData2);
//             component.reservationItemViewList.push(ReservationItemViewData3);
//             spyOn(component['reservationItemViewService'], 'isEditPageReservationItem').and.returnValue(false);
//
//             const reservationItem = ReservationItemViewData;
//             const reservationItem2 = ReservationItemViewData2;
//
//             component.deleteReservationItem(reservationItem);
//
//             expect(component['reservationService']['checkStatusReservationItemCanceledToReorderReservationItemList'])
// .toHaveBeenCalledWith(component.reservationItemViewList);
//             expect(component.reservationItemViewList).not.toContain(reservationItem);
//             expect(component.reservationItemViewList).toContain(reservationItem2);
//         });
//     });
//
//     it('should reactivateReservationItem', () => {
//         spyOn(component, 'setStatusColorFooter');
//         spyOn(component.reactivateReservationItemEvent, 'emit');
//
//         component.reactivateReservationItem(ReservationItemViewData);
//
//         expect(component['reservationService']['checkStatusReservationItemCanceledToReorderReservationItemList'])
// .toHaveBeenCalledWith(component.reservationItemViewList);
//         expect(component['setStatusColorFooter']).toHaveBeenCalled();
//         expect(component.reactivateReservationItemEvent.emit).toHaveBeenCalled();
//     });
//
// });
