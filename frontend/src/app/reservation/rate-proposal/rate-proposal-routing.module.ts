import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RateProposalComponent } from 'app/reservation/rate-proposal/container/rate-proposal.component';

export const rateProposalRoutes: Routes = [
  {path: '', component: RateProposalComponent},
];

@NgModule({
  imports: [RouterModule.forChild(rateProposalRoutes)],
  exports: [RouterModule],
})
export class RateProposalModuleRoutingModule {
}
