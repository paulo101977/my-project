import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';

export class RateProposalStorageItem {
  public  rateProposalSearchModel: RateProposalSearchModel;
  public rateProposalTypeItemItem: RateProposalRateTypeItem;
}

