import {
  ReservationItemCommercialBudgetConfig
} from 'app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';
import {
  ReservationItemCommercialBudget
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { SelectObjectOption } from 'app/shared/models/selectModel';

export class RateProposalRateTypeItem extends ReservationItemCommercialBudget {
  public commercialBudgetList: Array<ReservationItemCommercialBudget>;
  public budgetConfig: ReservationItemCommercialBudgetConfig;
  public mealPlanTypeListOptions: Array<SelectObjectOption>;

}
