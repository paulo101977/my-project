export class RateProposalSearchModel {
  customerClientId: string;
  customerClientName: string;
  initialDate: string;
  finalDate: string;
  roomTypeIdSort: number;
  adultCount: number;
  childrenAge: Array<number>;
  companyClientId: string;
  roomQtd: number;
  roomTypeId?: number;
}
