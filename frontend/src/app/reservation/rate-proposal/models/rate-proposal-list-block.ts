import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';

export class RateProposalListBlock {
  public roomTypeId: number;
  public roomTypeName: string;
  public roomTypeOrder: number;
  public rateList: Array<RateProposalRateTypeItem>;
  public overbooking: boolean;

  constructor(roomTypeId?: number, roomTypeName?: string, roomTypeOrder?: number) {
    this.roomTypeId = roomTypeId ? roomTypeId : 0;
    this.roomTypeName = roomTypeName ? roomTypeName : null;
    this.roomTypeOrder = roomTypeOrder ? roomTypeOrder : null;
  }
}
