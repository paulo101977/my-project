import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureAppName } from '@inovacaocmnet/thx-bifrost';

import { RateProposalCardListComponent } from './rate-proposal-card-list.component';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { reservationBudgetServiceStub } from 'app/shared/services/reservation-budget/testing';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { reservationServiceStub } from 'app/shared/services/reservation/testing';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { currencyFormatPipe } from '../../../../../../testing';
import { propertyStorageServiceStub } from '@bifrost/storage/services/testing';
import { mealPlanTypeResourceStub } from 'app/shared/resources/meal-plan-type/testing';

describe('RateProposalCardListComponent', () => {
  let component: RateProposalCardListComponent;
  let fixture: ComponentFixture<RateProposalCardListComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        RateProposalCardListComponent,
        currencyFormatPipe,
      ],
      imports: [
        TranslateTestingModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({property: '1'}) ,
            snapshot: { params: { property: 1}}
          },
        },
        configureAppName('pms'),
        commomServiceStub,
        mealPlanTypeResourceStub,
        reservationBudgetServiceStub,
        modalToggleServiceStub,
        reservationServiceStub,
        dateServiceStub,
        propertyStorageServiceStub,
      ],
      schemas: [
        NO_ERRORS_SCHEMA,
      ]
    });

    fixture = TestBed.createComponent(RateProposalCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit rateInformed when selectRate fired', () => {
    const rate = new RateProposalRateTypeItem();
    spyOn(component.rateProposalSelected, 'emit');

    component.selectRate(rate);

    expect(component.rateProposalSelected.emit).toHaveBeenCalledWith(rate);
  });
});
