import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RateProposalListBlock } from 'app/reservation/rate-proposal/models/rate-proposal-list-block';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ReservationBudgetService } from 'app/shared/services/reservation-budget/reservation-budget.service';
import {
  ReservationItemCommercialBudgetConfig
} from 'app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';
import { MealPlanTypeResource } from 'app/shared/resources/meal-plan-type/meal-plan-type.resource';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-rate-proposal-card-list',
  templateUrl: './rate-proposal-card-list.component.html',
  styleUrls: ['./rate-proposal-card-list.component.css']
})
export class RateProposalCardListComponent implements OnInit {


  public readonly MODAL_CHANGE_PERIOD = 'change-period-modal';
  public propertyId: number;

  public beginDate: any;
  public endDate: any;

  public rateProposalSelect: RateProposalRateTypeItem;

  @Input() rateProposalList: Array<RateProposalListBlock>;
  @Output() rateProposalSelected: EventEmitter<RateProposalRateTypeItem> = new EventEmitter();

  constructor(
    private route: ActivatedRoute,
    private mealPlanTypeResource: MealPlanTypeResource,
    private reservationBudgetService: ReservationBudgetService,
    private commomService: CommomService,
    private modalToggleService: ModalEmitToggleService,
    private reservationService: ReservationService,
    private dateService: DateService,
    private propertyService: PropertyStorageService
  ) { }

  ngOnInit() {
    this.propertyId = this.propertyService.getCurrentPropertyId();
  }

  public mealPlanChanged(mealPlanId, rateItem: RateProposalRateTypeItem) {
    const roomItem = this.getRateProposalListBlockByRoomType(rateItem.roomTypeId);
    const commercialBudget = roomItem.rateList
      .filter( item => item.ratePlanId == rateItem.ratePlanId && item.commercialBudgetName == rateItem.commercialBudgetName)[0];
    const mealPlanItems = commercialBudget.commercialBudgetList.filter( item => item.mealPlanTypeId == mealPlanId);
    if ( mealPlanItems && mealPlanItems.length > 0 ) {
      rateItem.budgetConfig.reservationItemCommercialBudgetCurrent = mealPlanItems[0];
      this.reservationBudgetService.recalculateBudgetReservationItem(rateItem.budgetConfig);
    }
  }

  private getRateProposalListBlockByRoomType(roomTypeId) {
    return this.rateProposalList.filter(item => item.roomTypeId == roomTypeId)[0];
  }

  public selectRate(rate) {
    this.prepareDates(rate);

    this.rateProposalSelect = rate;
    this.rateProposalSelected.emit(rate);
  }

  private prepareDates(rate) {
    if (rate.commercialBudgetDayList) {
      let bDate = this.dateService
        .convertStringToDate(rate.commercialBudgetDayList[0].day, DateService.DATE_FORMAT_UNIVERSAL);
      bDate = this.dateService.addDays(bDate, -1);

      this.beginDate = this.dateService
        .setDatePickerPeriod(bDate).date;

      let eDate = this.dateService
        .convertStringToDate(rate.commercialBudgetDayList[rate.commercialBudgetDayList.length - 1].day,
          DateService.DATE_FORMAT_UNIVERSAL);
      eDate = this.dateService.addDays(eDate, 1);

      this.endDate = this.dateService
        .setDatePickerPeriod(eDate).date;
    }
  }

  public getSelectId(rate) {
    return `${rate.roomTypeId}${rate.mealPlanTypeId}${rate.ratePlanId ? rate.ratePlanId : '0'}${rate.currencyId}`;
  }

  public updateBudget(updateBudget: ReservationItemCommercialBudgetConfig) {
    const mealPlanIdSelected = updateBudget.reservationBudgetTableList.reduce(  (tot, current) => {
      const mealplansFiltered = tot.filter(item => item == current.mealPlanTypeId);
      if ( mealplansFiltered && mealplansFiltered.length === 0) {
        tot.push(+current.mealPlanTypeId);
      }
      return tot;
    }, [] );
    const rateProposal = <RateProposalListBlock> this.getRateProposalListBlockByRoomType(updateBudget
      .reservationItemCommercialBudgetCurrent.roomTypeId);

    const ratePlan = <RateProposalRateTypeItem>rateProposal.rateList.filter( item => item.commercialBudgetName == updateBudget
        .reservationItemCommercialBudgetCurrent.commercialBudgetName
      && item.ratePlanId == updateBudget.reservationItemCommercialBudgetCurrent.ratePlanId)[0];

    const multiple = ratePlan.mealPlanTypeListOptions.filter( item => item.key == -1);
    if (mealPlanIdSelected.length > 1) { // se maior do que 1, existe mais de um tipo de pensão selecionada
      if (!multiple || multiple.length == 0) {
        ratePlan.mealPlanTypeListOptions.push({key: -1, value: 'label.multipleMealPlan'});
      }
      ratePlan.mealPlanTypeId = -1;
    } else {
      if (multiple && multiple.length > 0) {
        ratePlan.mealPlanTypeListOptions
          .splice(ratePlan.mealPlanTypeListOptions.indexOf(multiple[0]), 1);
      }
      ratePlan.mealPlanTypeId = mealPlanIdSelected[0];
    }
  }

  public openPeriodModal() {
    this.modalToggleService.emitOpenWithRef(this.MODAL_CHANGE_PERIOD);
  }

  public cancelRateProposalChange() {
    this.modalToggleService.emitCloseWithRef(this.MODAL_CHANGE_PERIOD);
  }

  public confirmRateProposalChange(formValue, budgetConfig) {
    if (budgetConfig.reservationItemCommercialBudgetCurrent &&
      budgetConfig.reservationItemCommercialBudgetCurrent.commercialBudgetDayList) {

      const periodList = this.reservationService.getDateListFromPeriod(formValue.period, false);

      const filtered = budgetConfig.reservationBudgetTableList
        .filter( item => periodList
          .filter(item2 => (<Date>item2).toStringUniversal() === item.budgetDay).length >= 1);

      filtered.forEach( element => {
        element.manualRate = formValue.amountValue;
        element.mealPlanTypeId = +formValue.mealPlanType;
        element.discount = formValue.discount;
        this.reservationBudgetService.setDiscount(budgetConfig
          .reservationBudgetTableList, budgetConfig.reservationBudgetTableList.indexOf(element));
      });
      this.reservationBudgetService.setTotals(budgetConfig);
      this.updateBudget(budgetConfig);
    }
    this.modalToggleService.emitCloseWithRef(this.MODAL_CHANGE_PERIOD);
  }
}
