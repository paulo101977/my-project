import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AutocompleteConfig } from 'app/shared/models/autocomplete/autocomplete-config';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { ClientResource } from 'app/shared/resources/client/client.resource';
import { PropertyService } from 'app/shared/services/property/property.service';
import { ChildTextConfig, ChildSelect } from '@inovacao-cmnet/thx-ui';
import { RoomType } from 'app/room-type/shared/models/room-type';
import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-rate-proposal-search-form',
  templateUrl: './rate-proposal-search-form.component.html',
})
export class RateProposalSearchFormComponent implements OnInit, OnDestroy {

  private readonly ROOM_QTD_DEFAULT = 1;
  private readonly ADULT_QTD_DEFAULT = 2;
  private readonly ALL_UH_TYPE_DEFAULT = 'all';

  /* Config */
  public  childMaxAge: number;
  public  buttonConfig: ButtonConfig;
  public  autoCompleteConfig: AutocompleteConfig;
  public  childTextConfig: ChildTextConfig;
  public  searchForm: FormGroup;
  public queryParams: any;

  /* Info */
  private propertyId: string;
  public clientList: Array<any>;
  public roomTypeList: Array<RoomType>;
  public roomTypeOptions: Array<SelectObjectOption>;
  public propertySubscription: Subscription;

  @Output() search: EventEmitter<RateProposalSearchModel> = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private clientResource: ClientResource,
    private propertyService: PropertyService,
    private roomTypeResource: RoomTypeResource,
    private commomService: CommomService,
    private sharedService: SharedService,
    private propertyStorageService: PropertyStorageService,
  ) {

    this.propertySubscription = this.propertyStorageService.currentPropertyChangeEmitted$.subscribe( () => {
      this.propertyId = `${this.propertyStorageService.getCurrentPropertyId()}`;
      this.loadInfo();
      this.search.emit( null );
    });
  }

  ngOnInit() {
    this.propertyId = `${this.propertyStorageService.getCurrentPropertyId()}`;
    this.loadInfo();
    this.queryParams = this.route.snapshot.queryParams;
    this.setForm();
    this.setAutoCompleteConfig();
    this.configChildText();
    this.setButtonConfig();

    // the page needs to load info automatically when params informed
    if (this.queryParams.start && this.queryParams.end) {
      this.searchClicked();
    }
  }

  private loadInfo() {
    this.childMaxAge = this.propertyService.getHigherChildAge(+this.propertyId);
    this.getRoomTypes();
  }

  private searchClicked = () => {
    this.search.emit( this.mapFormValueToSearchModel(this.searchForm.value) );
  }

  private mapFormValueToSearchModel(value: any) {
    const rateProposalSearchModel = new RateProposalSearchModel();
    rateProposalSearchModel.companyClientId = value.customerClientId;
    rateProposalSearchModel.customerClientName = value.client;
    rateProposalSearchModel.adultCount = value.adultQtd;
    rateProposalSearchModel.childrenAge = value.childQtd ? (<ChildSelect>value.childQtd).childrenAges : null;
    rateProposalSearchModel.initialDate = value.period ? value.period.beginDate : '';
    rateProposalSearchModel.finalDate = value.period ? value.period.endDate : '';
    rateProposalSearchModel.roomTypeIdSort = value.uhType == this.ALL_UH_TYPE_DEFAULT ? null : value.uhType;
    rateProposalSearchModel.roomQtd = value.roomQtd;
    rateProposalSearchModel.roomTypeId = rateProposalSearchModel.roomTypeIdSort;

    return rateProposalSearchModel;
  }

  private getRoomTypes() {
    this.roomTypeResource.getRoomTypesByPropertyId(+this.propertyId).subscribe(result => {
      this.roomTypeList = result;
      this.roomTypeOptions = this.commomService
        .toOption(result, 'id', 'name');
      this.roomTypeOptions = [
        {key: this.ALL_UH_TYPE_DEFAULT, value: this.translateService.instant('label.entiresF')} ,
        ...this.roomTypeOptions
      ];
    });
  }

  /* SEARCH CLIENT*/
  public searchItemsClient = (searchData: any) => {
    this.searchForm.get('client').setValue(searchData.query);

    this.clientResource.getClientsBySearchData(+this.propertyId, searchData.query).subscribe( result => {
      this.clientList = result.map( item => {
        item.templateClientSearch = `${item.tradeName} ${item.document}`;
        return item;
      });
    });
  }

  private updateItemClient = (client) => {
    if (this.searchForm) {
      this.searchForm.get('customerClientId').setValue(client.id);
    }
  }

  /* CONFIG */
  private setAutoCompleteConfig() {
    this.autoCompleteConfig = new AutocompleteConfig();
    this.autoCompleteConfig.dataModel = this.searchForm.get('client').value;
    this.autoCompleteConfig.callbackToSearch = this.searchItemsClient;
    this.autoCompleteConfig.extraClasses = 'thf-u-font-input-default';
    this.autoCompleteConfig.callbackToGetSelectedValue = this.updateItemClient;
    this.autoCompleteConfig.fieldObjectToDisplay = 'templateClientSearch';
    this.autoCompleteConfig.resultList = this.clientList;
    this.translateService.get('label.placeholder.clientSearch').subscribe(value => {
      this.autoCompleteConfig.placeholder = value;
    });
  }

  private configChildText() {
    this.childTextConfig = new ChildTextConfig();
    this.childTextConfig.title = this.translateService.instant('title.childList');
    this.childTextConfig.cancelButton = this.translateService.instant('action.cancel');
    this.childTextConfig.okButton = 'ok';
  }

  private setButtonConfig() {
    this.buttonConfig = this.sharedService.getButtonConfig(
      'search',
      this.searchClicked,
      this.translateService.instant('action.searchOnly'),
      ButtonType.Primary,
      ButtonSize.Normal
    );
  }

  private setForm() {
    this.searchForm = this.formBuilder.group({
      client: null,
      customerClientId: null,
      period: [this.getQueryParamsPeriod(), [Validators.required]],
      roomQtd: [this.ROOM_QTD_DEFAULT, [Validators.required, Validators.min(1)]],
      adultQtd: [this.ADULT_QTD_DEFAULT, [Validators.required, Validators.min(1)]],
      childQtd: [null],
      uhType: [this.queryParams.roomTypeId ? this.queryParams.roomTypeId : this.ALL_UH_TYPE_DEFAULT, [Validators.required]],
    });

    this.searchForm.valueChanges.subscribe(() => {
      if (this.buttonConfig) {
        this.buttonConfig.isDisabled = !this.searchForm.valid;
      }
    });
  }

  private getQueryParamsPeriod() {
    if (this.queryParams && this.queryParams.start &&  this.queryParams.end) {
      return { beginDate: `${this.queryParams.start}T00:00:00`, endDate: `${this.queryParams.end}T00:00:00` };
    }
    return null;
  }

  ngOnDestroy(): void {
    if (this.propertySubscription) {
      this.propertySubscription.unsubscribe();
    }
  }
}
