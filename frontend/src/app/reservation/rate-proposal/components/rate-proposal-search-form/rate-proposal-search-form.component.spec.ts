import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureAppName } from '@inovacaocmnet/thx-bifrost';
import { roomTypeResourceStub } from 'app/room-type/shared/services/room-type-resource/testing';

import { RateProposalSearchFormComponent } from './rate-proposal-search-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { RoomType } from 'app/room-type/shared/models/room-type';
import { of } from 'rxjs';
import { Client } from 'app/shared/models/client';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { ActivatedRoute } from '@angular/router';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { clientResourceStub } from 'app/shared/resources/client/testing';
import { propertyServiceStub } from 'app/shared/services/property/testing';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { createAccessorComponent } from '../../../../../../testing';

const dateRangeComponent = createAccessorComponent('thx-date-range-picker');
const childSelectComponent = createAccessorComponent('thx-child-select');

describe('RateProposalSearchFormComponent', () => {
  let component: RateProposalSearchFormComponent;
  let fixture: ComponentFixture<RateProposalSearchFormComponent>;

  let roomTypeList: Array<RoomType>;
  let clientList: Array<Client>;

  let formValueMock;
  let rateProposalSearchMock: RateProposalSearchModel;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        TranslateTestingModule,
      ],
      declarations: [
        RateProposalSearchFormComponent,
        dateRangeComponent,
        childSelectComponent,
      ],
      providers: [
        sharedServiceStub,
        clientResourceStub,
        propertyServiceStub,
        commomServiceStub,
        roomTypeResourceStub,
        configureAppName('pms'),
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              queryParams: {start: '2018-01-05T00:00:00', end: '2018-01-05T00:00:00'},
              params: {
                property: '1'
              }
            },
            params: of({property: '1'} )
          },
        },
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    fixture = TestBed.createComponent(RateProposalSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component['propertyId'] = '1';

    roomTypeList = [];
    const roomType = new RoomType();
    roomType.abbreviation = 'abc';
    roomType.id = 1;
    roomType.name = 'abc';
    roomTypeList.push(roomType);


    spyOn(component['roomTypeResource'], 'getRoomTypesByPropertyId').and.returnValue(of(roomTypeList));


    formValueMock = {
      adultQtd: 2,
      childQtd: null,
      client: null,
      customerClientId: null,
      period: {beginDate: '2018-12-05T00:00:00', endDate: '2018-12-07T00:00:00'},
      roomQtd: 1,
      uhType: 'all'
    };
    rateProposalSearchMock = new RateProposalSearchModel();
    rateProposalSearchMock.companyClientId = null;
    rateProposalSearchMock.customerClientName = null;
    rateProposalSearchMock.adultCount = 2;
    rateProposalSearchMock.childrenAge = null;
    rateProposalSearchMock.initialDate = '2018-12-05T00:00:00';
    rateProposalSearchMock.finalDate = '2018-12-07T00:00:00';
    rateProposalSearchMock.roomTypeIdSort = null;
    rateProposalSearchMock.roomQtd = 1;
    rateProposalSearchMock.roomTypeId = null;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('should call seachClicked when queryParams informed', () => {
    spyOn<any>(component, 'searchClicked');

    component['ngOnInit']();

    expect(component['searchClicked']).toHaveBeenCalled();
  });

  it('should emit form value when searchClicked fired', () => {
    spyOn(component.search, 'emit');

    component['searchForm'].setValue(formValueMock);
    component['searchClicked']();

    expect(component.search.emit).toHaveBeenCalledWith(rateProposalSearchMock);
  });

  it('should map form value to rateProposalSearch', () => {
    const resultValue = component['mapFormValueToSearchModel'](formValueMock);

    expect(resultValue).toEqual(rateProposalSearchMock);
  });

  describe('getRoomTypes', () => {

    it('should set roomTypeList when getRoomTypes fired', () => {
      component['getRoomTypes']();

      expect(component.roomTypeList).toBe(roomTypeList);
    });

    it('should add options "all" in getRoomTypes', () => {
      component['getRoomTypes']();

      expect(component.roomTypeOptions.length).toBe(roomTypeList.length);
      expect(component.roomTypeOptions[0].key).toBe('all');
    });
  });

  describe('searchItemsClient', () => {

    beforeEach(() => {
      clientList = [];
      const client = new Client();
      client.tradeName = 'ABS';
      client.document = '123';
      client.id = 'id123';
      clientList.push(client);

      spyOn(component['clientResource'], 'getClientsBySearchData').and.returnValue(of(clientList));
    });

    it('should set query in form', () => {
      component.searchItemsClient({query: 'algo'});

      expect(component['searchForm'].value.client).toBe('algo');
    });

    it('should add templateClientSeach on clientList', () => {
      component.searchItemsClient({query: 'algo'});

      expect(component.clientList[0].templateClientSearch).toBeTruthy();
    });

    it('should templateClientSeach contain trade name and document', () => {
      component.searchItemsClient({query: 'algo'});

      expect(component.clientList[0].templateClientSearch).toBe(`${clientList[0].tradeName} ${clientList[0].document}`);
    });
  });

  it('should set client id on form when updateItemClient fired', () => {
    component['updateItemClient'](clientList[0]);

    expect(component['searchForm'].value.customerClientId).toBe(clientList[0].id);
  });

  it('should map period when queryParams informed', () => {
    component['queryParams'] = { start: '2018-10-05', end: '2018-10-10'};

    const period = component['getQueryParamsPeriod']();

    expect(period).toEqual({beginDate: '2018-10-05T00:00:00', endDate: '2018-10-10T00:00:00'});
  });

  it('should return null period when queryParams not informed', () => {
    component['queryParams'] = null;

    const period = component['getQueryParamsPeriod']();

    expect(period).toEqual(null);
  });

});
