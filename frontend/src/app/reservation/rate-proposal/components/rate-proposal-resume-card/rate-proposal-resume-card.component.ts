import { Component, Input, OnInit } from '@angular/core';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { RateProposalShareInfoService } from 'app/shared/services/rate-proposal/rate-proposal-share-info.service';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';
import { PropertyStorageService } from '@bifrost/storage/services/property-storage.service';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';

@Component({
  selector: 'app-rate-proposal-resume-card',
  templateUrl: './rate-proposal-resume-card.component.html',
  styleUrls: ['./rate-proposal-resume-card.component.css']
})
export class RateProposalResumeCardComponent implements OnInit {

  // Buttons
  public cancelConfig: ButtonConfig;
  public confirmConfig: ButtonConfig;
  public propertyId: number;

  @Input() roomQtd = 1;
  @Input() dailyNumber: number;
  @Input() currencySymbol: string;
  @Input() dailyAverage: number;
  @Input() total: number;
  @Input() rateProposalSearch: RateProposalSearchModel;
  @Input() rateProposalItemSelected: RateProposalRateTypeItem;

  constructor(
    private sharedService: SharedService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private rateProposalShareInfoService: RateProposalShareInfoService,
    private propertyService: PropertyStorageService,
    private reservationService: ReservationService,
  ) { }

  ngOnInit() {
    this.propertyId = this.propertyService.getCurrentPropertyId();
    this.setButtonConfig();
  }

  public back() {
    this.location.back();
  }

  public reservationConfirm() {
    this.reservationService.goToReservationConfirm(
        this.rateProposalSearch,
        this.rateProposalItemSelected,
    );
  }

  /* CONFIG */
  private setButtonConfig(): void {
    this.cancelConfig = this.sharedService.getButtonConfig(
      'cancel-reservation',
      () => this.back(),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.confirmConfig = this.sharedService.getButtonConfig(
      'confirm-reservation',
      () => this.reservationConfirm(),
      'label.goToReservation',
      ButtonType.Primary,
    );
    this.confirmConfig.isDisabled = false;
  }
}
