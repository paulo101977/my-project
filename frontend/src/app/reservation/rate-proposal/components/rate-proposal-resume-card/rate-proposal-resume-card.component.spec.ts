// import { ComponentFixture, TestBed } from '@angular/core/testing';
//
// import { RateProposalResumeCardComponent } from './rate-proposal-resume-card.component';
// import { ButtonType } from 'app/shared/models/button-config';
// import { configureTestSuite } from 'ng-bullet';
// import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { ActivatedRouteStub, currencyFormatPipe, locationStub, padStub } from '../../../../../../testing';
// import { rateProposalShareInfoServiceStub } from 'app/shared/services/rate-proposal/testing';
// import { RouterTestingModule } from '@angular/router/testing';
// import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
//
// describe('RateProposalResumeCardComponent', () => {
//   let component: RateProposalResumeCardComponent;
//   let fixture: ComponentFixture<RateProposalResumeCardComponent>;
//
//   configureTestSuite(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         TranslateTestingModule,
//         RouterTestingModule,
//       ],
//       declarations: [
//         RateProposalResumeCardComponent,
//         padStub,
//         currencyFormatPipe,
//       ],
//       providers: [
//         sharedServiceStub,
//         rateProposalShareInfoServiceStub,
//         locationStub,
//         ActivatedRouteStub,
//       ],
//       schemas: [ NO_ERRORS_SCHEMA ]
//     });
//
//     fixture = TestBed.createComponent(RateProposalResumeCardComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   describe('#Buttons', () => {
//
//     it('should setButtonConfig', () => {
//       const cancelConfig = component['sharedService'].getButtonConfig(
//         'cancel-reservation',
//         component.back,
//         'action.cancel',
//         ButtonType.Secondary,
//       );
//
//       const confirmConfig = component['sharedService'].getButtonConfig(
//         'confirm-reservation',
//         component.reservationConfirm,
//         'label.goToReservation',
//         ButtonType.Primary,
//       );
//       confirmConfig.isDisabled = false;
//       expect(component.cancelConfig).toEqual(cancelConfig);
//       expect(component.confirmConfig).toEqual(confirmConfig);
//     });
//   });
// });
