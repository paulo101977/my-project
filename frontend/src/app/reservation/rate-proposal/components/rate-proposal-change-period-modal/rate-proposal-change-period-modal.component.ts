import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { IMyDate } from 'mydatepicker';

@Component({
  selector: 'app-rate-proposal-change-period-modal',
  templateUrl: './rate-proposal-change-period-modal.component.html',
  styleUrls: ['./rate-proposal-change-period-modal.component.css']
})
export class RateProposalChangePeriodModalComponent implements OnInit, OnChanges {

  @Input() modalId: string;

  @Input() dailyAmount = 0;
  @Input() mealPlanTypeId: number;
  @Input() currencySymbol: string;
  @Input() mealPlanTypeListOptions: Array<SelectObjectOption>;
  @Input() disableUntil: IMyDate;
  @Input() disableSince: IMyDate;

  @Output() cancelModal  = new EventEmitter<any>();

  @Output() confirmModal = new EventEmitter<any>();
  public periodForm: FormGroup;

  public optionsCurrencyMask: any;

  constructor( private fb: FormBuilder) { }

  ngOnInit() {
    this.setCurrencyMaskConfig();
    this.setForm();
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (this.periodForm) {
      this.periodForm.get('amountValue').setValue(this.dailyAmount);
      this.periodForm.get('mealPlanType').setValue(this.mealPlanTypeId);
    }
  }

  private setCurrencyMaskConfig() {
    this.optionsCurrencyMask = {prefix: '', thousands: '.', decimal: ',', align: 'left'};
  }

  private setForm() {
    this.periodForm = this.fb.group({
      period: [null, [Validators.required]],
      discount: 0,
      amountValue: this.dailyAmount ?  this.dailyAmount : 0,
      mealPlanType: this.mealPlanTypeId
    });
  }

  public cancel() {
    this.cancelModal.emit();
  }

  public confirm() {
    this.confirmModal.emit(this.periodForm.value);
  }
}
