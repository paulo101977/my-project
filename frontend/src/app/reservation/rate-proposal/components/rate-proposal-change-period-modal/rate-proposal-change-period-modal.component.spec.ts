// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
//
// import { RateProposalChangePeriodModalComponent } from './rate-proposal-change-period-modal.component';
// import { SharedModule } from 'app/shared/shared.module';
// import { TranslateModule } from '@ngx-translate/core';
// import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
// import { RouterTestingModule } from '@angular/router/testing';
// import { configureTestSuite } from 'ng-bullet';
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
// import { createAccessorComponent, currencyFormatPipe } from '../../../../../../testing';
//
// const thxDate = createAccessorComponent('thx-date-range-picker');
//
// describe('RateProposalChangePeriodModalComponent', () => {
//   let component: RateProposalChangePeriodModalComponent;
//   let fixture: ComponentFixture<RateProposalChangePeriodModalComponent>;
//
//   configureTestSuite(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         TranslateTestingModule,
//         ReactiveFormsModule,
//         RouterTestingModule
//       ],
//       providers: [
//         FormBuilder,
//       ],
//       declarations: [
//         RateProposalChangePeriodModalComponent,
//         thxDate,
//         currencyFormatPipe,
//       ],
//       schemas: [ NO_ERRORS_SCHEMA ]
//     });
//
//     fixture = TestBed.createComponent(RateProposalChangePeriodModalComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
