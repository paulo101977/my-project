import { createServiceStub } from '../../../../../../testing';
import { RateProposalResource } from 'app/reservation/rate-proposal/resources/rate-proposal.resource';
import { Observable, of } from 'rxjs';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';

export const rateProposalResourceStub = createServiceStub(RateProposalResource, {
    searchRateProposal(propertyId: number, params: RateProposalSearchModel): Observable<any> {
        return of( null );
    }
});
