import { Injectable } from '@angular/core';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { Observable } from 'rxjs';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: 'root'
})
export class RateProposalResource {

  constructor(private thexApi: ThexApiService) {}

  public searchRateProposal(propertyId: number, params: RateProposalSearchModel): Observable<any> {
    return this.thexApi.get(`RateProposal/${propertyId}`, { params: params});
  }
}
