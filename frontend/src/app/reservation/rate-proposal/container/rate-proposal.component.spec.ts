import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { configureAppName } from '@inovacaocmnet/thx-bifrost';
import { RateProposalComponent } from './rate-proposal.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'app/shared/shared.module';
import { RateProposalModule } from 'app/reservation/rate-proposal/rate-proposal.module';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { rateProposalResourceStub } from 'app/reservation/rate-proposal/resources/testing';
import { rateProposalServiceStub } from 'app/reservation/rate-proposal/services/testing';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { propertyStorageServiceStub } from '@bifrost/storage/services/testing';
import { mealPlanTypeResourceStub } from 'app/shared/resources/meal-plan-type/testing';
import { reservationBudgetServiceStub } from 'app/shared/services/reservation-budget/testing';
import { budgetServiceStub } from 'app/shared/services/budget/testing';
import { reservationServiceStub } from 'app/shared/services/reservation/testing';
import { rateProposalShareInfoServiceStub } from 'app/shared/services/rate-proposal/testing';

// TODO: create tests [DEBIT: 7517]

describe('RateProposalComponent', () => {
  let component: RateProposalComponent;
  let fixture: ComponentFixture<RateProposalComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        RateProposalComponent
      ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        sharedServiceStub,
        rateProposalResourceStub,
        rateProposalServiceStub,
        commomServiceStub,
        propertyStorageServiceStub,
        mealPlanTypeResourceStub,
        reservationBudgetServiceStub,
        budgetServiceStub,
        reservationServiceStub,
        rateProposalShareInfoServiceStub,
        configureAppName('pms'),
      ],
      schemas: [
        NO_ERRORS_SCHEMA,
      ]
    });

    fixture = TestBed.createComponent(RateProposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn<any>(component['_route'], 'navigate').and.callFake(() => {});
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set rateProposal selected when setRateProposalSelected', () => {
    const rate = new RateProposalRateTypeItem();

    component.setRateProposalSelected(rate);

    expect(component.rateProposalItemSelected).toEqual(rate);
  });
});
