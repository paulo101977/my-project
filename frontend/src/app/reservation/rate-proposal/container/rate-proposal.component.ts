import { AfterViewChecked, Component, ElementRef, HostListener, OnInit, DoCheck, Renderer2, ViewChild } from '@angular/core';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { RateProposalResource } from 'app/reservation/rate-proposal/resources/rate-proposal.resource';
import { ActivatedRoute, Router } from '@angular/router';
import { RateProposalService } from 'app/reservation/rate-proposal/services/rate-proposal.service';
import { RateProposalListBlock } from 'app/reservation/rate-proposal/models/rate-proposal-list-block';
import {
  ReservationItemCommercialBudgetConfig
} from 'app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';
import { ReservationBudgetService } from 'app/shared/services/reservation-budget/reservation-budget.service';
import { BudgetService } from 'app/shared/services/budget/budget.service';
import { ReservationItemBudget } from 'app/shared/models/revenue-management/reservation-budget/reservation-item-budget';
import {
  ReservationItemCommercialBudget
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';
import { RateProposalSortPriceEnum } from 'app/reservation/rate-proposal/models/rate-proposal-sort-price-enum';
import { MealPlanTypeResource } from 'app/shared/resources/meal-plan-type/meal-plan-type.resource';
import { MealPlanType } from 'app/shared/models/revenue-management/meal-plan-type';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { RateProposalShareInfoService } from 'app/shared/services/rate-proposal/rate-proposal-share-info.service';

@Component({
  selector: 'app-rate-proposal',
  templateUrl: './rate-proposal.component.html',
  styleUrls: ['./rate-proposal.component.css']
})
export class RateProposalComponent implements OnInit, DoCheck, AfterViewChecked {

  public orderType: number = RateProposalSortPriceEnum.LOWER;
  // Buttons
  public cancelConfig: ButtonConfig;
  public confirmConfig: ButtonConfig;

  public rateProposalList: Array<RateProposalListBlock>;
  public rateProposalItemSelected: RateProposalRateTypeItem;
  public rateProposalSearch: RateProposalSearchModel;
  public mealPlanTypeList: Array<MealPlanType>;
  public mealPlanTypeOptionList: Array<SelectObjectOption>;
  public propertyId: number;
  public isBottom = false;

  @ViewChild('resumeCard') resumeCard: ElementRef;

  @HostListener('window:scroll', ['$event']) onScroll(event) {
    const { selfElement, rateProposalList } = this;
    const doc = document.documentElement;
    const offset = doc.scrollTop + window.innerHeight;
    this.isBottom =
      rateProposalList
      && selfElement
      && offset > ( selfElement.nativeElement.clientHeight + 130 );
  }

  constructor(
    private sharedService: SharedService,
    private rateProposalResource: RateProposalResource,
    private rateProposalService: RateProposalService,
    private route: ActivatedRoute,
    private _route: Router,
    private reservationBudgetService: ReservationBudgetService,
    private budgetService: BudgetService,
    private reservationService: ReservationService,
    private rateProposalShareInfoervice: RateProposalShareInfoService,
    private mealPlanTypeResource: MealPlanTypeResource,
    private commonService: CommomService,
    private selfElement: ElementRef,
    private renderer: Renderer2,
    private propertyService: PropertyStorageService
  ) {}

  ngOnInit() {
    this.setButtonConfig();
    this.setProperty();
  }

  private setProperty() {
    this.propertyId = this.propertyService.getCurrentPropertyId();
    this.getMealPlanType();
  }

  ngDoCheck() {
    if (this.confirmConfig) {
      this.confirmConfig.isDisabled = !this.rateProposalItemSelected;
    }
  }

  ngAfterViewChecked(): void {
    const { resumeCard, isBottom, renderer } = this;

    if ( resumeCard ) {
      const { nativeElement } = resumeCard;

      renderer.removeClass(nativeElement, 'fixed-resume-card-at-bottom');

      if ( isBottom ) {
        renderer.addClass(nativeElement, 'fixed-resume-card-at-bottom');
      }
    }
  }


  public search(rateProposalSearch: RateProposalSearchModel) {
    this.propertyId = this.propertyService.getCurrentPropertyId();
    this.setRateProposalSelected(null);
    if (rateProposalSearch) {
      this.rateProposalSearch = { ...rateProposalSearch };
      const searchInfo = this.rateProposalService.searchRateProposalPreparingData(rateProposalSearch);

      this.rateProposalResource.searchRateProposal(this.propertyId, searchInfo)
        .subscribe(result => {
          if (result) {
            const list = this.rateProposalService.mapCommercialBudgetToRateProposal(result.commercialBudgetList,
              rateProposalSearch.roomTypeIdSort, this.orderType);
            for (const item of list) {
              item.rateList = item.rateList.map(rateItem => {
                rateItem.budgetConfig = this.createCommercialBudgetConfig(rateItem, rateProposalSearch);
                rateItem.mealPlanTypeListOptions = [...this.mealPlanTypeOptionList];
                return rateItem;
              });
            }
            this.rateProposalList = list;
          }
        });
    } else {
      this.rateProposalList = null;
    }
  }

  public createCommercialBudgetConfig(
    rateItem: RateProposalRateTypeItem,
    rateProposalSearch: RateProposalSearchModel
  ): ReservationItemCommercialBudgetConfig {
    const config = new ReservationItemCommercialBudgetConfig();
    const reservationItem = new ReservationItemBudget();

    reservationItem.commercialBudgetList = rateItem.commercialBudgetList;
    this.budgetService.getCommercialBudgetList(config, reservationItem);
    config.selectReservationItemCommercialBudgetOriginalList = rateItem.commercialBudgetList;
    const dateListFromPeriod = this.reservationService
      .getDateListFromPeriod({beginDate: rateProposalSearch.initialDate, endDate: rateProposalSearch.finalDate});
    config.dateListFromPeriod = dateListFromPeriod;
    config.reservationItemCommercialBudgetCurrent = new ReservationItemCommercialBudget();
    config.reservationItemCommercialBudgetCurrent.totalBudget = 0;
    config.isGratuityType = false;
    config.gratuityTypeId = null;
    config.averageDaily = 0;
    config.wasConfirmed = false;
    config.adultQuantity = rateProposalSearch.adultCount;
    config.childCount = rateProposalSearch.childrenAge ? rateProposalSearch.childrenAge.length : 0;
    config.reservationItemCommercialBudgetCurrent = rateItem.commercialBudgetList
      .filter(item => item.mealPlanTypeDefaultId == item.mealPlanTypeId)[0];
    this.reservationBudgetService.recalculateBudgetReservationItem(config);
    config.gratuityTypeSelectOptionsList = new Array<SelectObjectOption>();
    return config;
  }

  public setRateProposalSelected(rate: RateProposalRateTypeItem) {
    this.rateProposalItemSelected = rate;
  }

  /* ORDER */
  public orderBy() {
    this.orderType =
      this.orderType == RateProposalSortPriceEnum.LOWER ? RateProposalSortPriceEnum.HIGHER : RateProposalSortPriceEnum.LOWER;
    this.rateProposalList = this.rateProposalService.sortByPrice(this.rateProposalList, this.orderType);
  }

  public getOrderTypeName(orderType) {
    return orderType == RateProposalSortPriceEnum.HIGHER ? 'label.higherPrice' : 'label.lowerPrice';
  }

  /* CONFIG */
  private setButtonConfig(): void {
    this.cancelConfig = this.sharedService.getButtonConfig(
      'cancel-reservation',
      this.back,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.confirmConfig = this.sharedService.getButtonConfig(
      'confirm-reservation',
      this.reservationConfirm,
      'label.goToReservation',
      ButtonType.Primary,
    );
    this.confirmConfig.isDisabled = true;
  }

  public back = () => {
    this.rateProposalService.navigateBack();
  }

  public reservationConfirm = () => {
    const token = this.rateProposalShareInfoervice.storeRateProposal(this.rateProposalSearch, this.rateProposalItemSelected);
    this.rateProposalService.navigateConfirm(this.propertyId, token);
  }

  private getMealPlanType() {
    this.mealPlanTypeResource.getActiveMealPlanTypeList(this.propertyId)
      .subscribe(result => {
        this.rateProposalList = null;
        this.mealPlanTypeList = result.items;
        this.mealPlanTypeOptionList = this.commonService
          .toOption(result, 'mealPlanTypeId', 'propertyMealPlanTypeName');
      });
  }
}
