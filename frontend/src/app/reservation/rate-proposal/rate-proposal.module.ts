import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RateProposalModuleRoutingModule } from 'app/reservation/rate-proposal/rate-proposal-routing.module';
import { SharedModule } from 'app/shared/shared.module';
import { ThxChildSelectModule, ExpandableCardModule } from '@inovacao-cmnet/thx-ui';
import { CoreModule } from 'app/core/core.module';
import { ReservationBudgetModule } from 'app/revenue-management/reservation-budget/reservation-budget.module';
import {
  RateProposalChangePeriodModalComponent
} from './components/rate-proposal-change-period-modal/rate-proposal-change-period-modal.component';
import { RateProposalComponent } from 'app/reservation/rate-proposal/container/rate-proposal.component';
import {
  RateProposalSearchFormComponent
} from 'app/reservation/rate-proposal/components/rate-proposal-search-form/rate-proposal-search-form.component';
import {
  RateProposalCardListComponent
} from 'app/reservation/rate-proposal/components/rate-proposal-card-list/rate-proposal-card-list.component';
import {
  RateProposalResumeCardComponent
} from 'app/reservation/rate-proposal/components/rate-proposal-resume-card/rate-proposal-resume-card.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RateProposalModuleRoutingModule,
    ThxChildSelectModule,
    ExpandableCardModule,
    CoreModule,
    ReservationBudgetModule
  ],
  declarations: [
    RateProposalComponent,
    RateProposalSearchFormComponent,
    RateProposalCardListComponent,
    RateProposalResumeCardComponent,
    RateProposalChangePeriodModalComponent
  ],
  exports: [
    RateProposalComponent
  ]
})
export class RateProposalModule { }
