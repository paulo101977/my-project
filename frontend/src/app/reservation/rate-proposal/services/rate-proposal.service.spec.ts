import { inject, TestBed } from '@angular/core/testing';

import { RateProposalService } from './rate-proposal.service';
import { CoreModule } from 'app/core/core.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RateProposalSortPriceEnum } from 'app/reservation/rate-proposal/models/rate-proposal-sort-price-enum';
import { RateProposalListBlockMock } from 'app/shared/mock-data/rate-proposal-data';
import { RouterTestingModule } from '@angular/router/testing';

describe('RateProposalService', () => {

  let rateProposalListBlockMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, HttpClientTestingModule, RouterTestingModule],
      providers: [RateProposalService]
    });

    rateProposalListBlockMock = RateProposalListBlockMock;
  });

  it('should be created', inject([RateProposalService], (service: RateProposalService) => {
    expect(service).toBeTruthy();
  }));

  describe('#sort', () => {
    it('should sort list based on roomType then roomTypeORder - when id defined',
      inject([RateProposalService], (service: RateProposalService) => {
        const sortedList = service['sortByRoomType']([...rateProposalListBlockMock], 2);

        expect(sortedList[0]).toBe(rateProposalListBlockMock[1]);
        expect(sortedList[1]).toBe(rateProposalListBlockMock[0]);
        expect(sortedList[2]).toBe(rateProposalListBlockMock[2]);
      }));

    it('should sort list based in roomTypeOrder -  when id NOT defined',
      inject([RateProposalService], (service: RateProposalService) => {
        const sortedList = service['sortByRoomType']([...rateProposalListBlockMock], null);

        expect(sortedList[0]).toBe(rateProposalListBlockMock[0]);
        expect(sortedList[1]).toBe(rateProposalListBlockMock[2]);
        expect(sortedList[2]).toBe(rateProposalListBlockMock[1]);
      }));

    it('Should order list by price DESC', inject([RateProposalService], (service: RateProposalService) => {
      const list = service.sortByPrice(JSON.parse(JSON.stringify(rateProposalListBlockMock)),
        RateProposalSortPriceEnum.HIGHER);

      expect(list[0].rateList[0]).toEqual(rateProposalListBlockMock[0].rateList[1]);
      expect(list[0].rateList[1]).toEqual(rateProposalListBlockMock[0].rateList[0]);

      expect(list[1].rateList[0]).toEqual(rateProposalListBlockMock[1].rateList[0]);
      expect(list[1].rateList[1]).toEqual(rateProposalListBlockMock[1].rateList[1]);

      expect(list[2].rateList[0]).toEqual(rateProposalListBlockMock[2].rateList[0]);
      expect(list[2].rateList[1]).toEqual(rateProposalListBlockMock[2].rateList[1]);

    }));

  });
});
