import { Injectable } from '@angular/core';
import { RateProposalListBlock } from 'app/reservation/rate-proposal/models/rate-proposal-list-block';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';
import {
  ReservationItemCommercialBudget
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { RateProposalResource } from 'app/reservation/rate-proposal/resources/rate-proposal.resource';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { Observable } from 'rxjs';
import { DateService } from 'app/shared/services/shared/date.service';
import * as moment from 'moment';
import { RateProposalSortPriceEnum } from 'app/reservation/rate-proposal/models/rate-proposal-sort-price-enum';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RateProposalService {

  constructor(protected rateProposalResource: RateProposalResource,
              protected location: Location,
              protected _route: Router) {
  }


  public mapCommercialBudgetToRateProposal(commercialBudgetList: Array<ReservationItemCommercialBudget>,
                                           sortRoomType?: number,
                                           sortPrice?: RateProposalSortPriceEnum): any {
    if (commercialBudgetList) {
      const commercialObj = commercialBudgetList.reduce((tot, current, index, all) => {
        if (!tot[`${current.roomTypeId}`]) {
          const rateProposal = new RateProposalListBlock(current.roomTypeId, current.roomTypeAbbreviation, current.order);
          rateProposal.overbooking = current.overbooking;
          const defaultRatesByRoomTypeList = all.filter(item => item.roomTypeId === current.roomTypeId &&
            item.mealPlanTypeDefaultId === item.mealPlanTypeId);
          const itensList = Array<RateProposalRateTypeItem>();
          defaultRatesByRoomTypeList.forEach(rate => {
            let rateTypeItem = new RateProposalRateTypeItem();
            rateTypeItem = {...rate, ...rateTypeItem};
            rateTypeItem.commercialBudgetList = all.filter(element => element.roomTypeId === current.roomTypeId &&
              element.commercialBudgetName === rate.commercialBudgetName).map(e => {
              return <ReservationItemCommercialBudget> {...e};
            });
            itensList.push(rateTypeItem);
          });
          rateProposal.rateList = itensList;
          tot[`${current.roomTypeId}`] = rateProposal;
        }
        return tot;
      }, {});
      return this.sortByPrice(this.sortByRoomType(Object.keys(commercialObj).map(function (key) {
        return commercialObj[key];
      }), sortRoomType), sortPrice);
    }
    return null;
  }

  public sortByRoomType(list: Array<RateProposalListBlock> , sortRoomType) {
  return list.sort((element1, element2) => {
    return element1.roomTypeId == sortRoomType ? -1 :
      (element2.roomTypeId == sortRoomType ? 1 :
        (element1.roomTypeOrder > element2.roomTypeOrder ? 1 :
          (element1.roomTypeOrder < element2.roomTypeOrder ? -1 : 0)));
    });
  }

  public sortByPrice(list: Array<RateProposalListBlock>, sortType: RateProposalSortPriceEnum) {
    if (list && sortType) {
      list.forEach( item => {
        item.rateList.sort( (element1, element2) => {
          if (sortType == RateProposalSortPriceEnum.HIGHER) {
            return element1.totalBudget > element2.totalBudget ? -1 :
              (element1.totalBudget == element2.totalBudget ? 0 : 1);
          } else {
            return element1.totalBudget > element2.totalBudget ? 1 :
              (element1.totalBudget == element2.totalBudget ? 0 : -1);
          }
        });
      });
      return list;
    }
    return list;
  }

  public searchRateProposalPreparingData(params: RateProposalSearchModel): RateProposalSearchModel {
    params = {...params};
    const format = DateService.DATE_FORMAT_UNIVERSAL;
    if (moment(params.finalDate, format).diff(moment(params.initialDate, format), 'days', true) > 0) {
      params.initialDate = moment(params.initialDate, format).toDate().toStringUniversal();
      params.finalDate = moment(params.finalDate, format).add(-1, 'days').toDate().toStringUniversal();
    }

    return params;
  }

  public navigateBack() {
    this.location.back();
  }

  public navigateConfirm(propertyId: number, token) {
    this._route.navigate(['p', propertyId, 'reservation', 'new'], {queryParams: {rateProposal: token}});
  }

}
