import { createServiceStub } from '../../../../../../testing';
import { RateProposalService } from 'app/reservation/rate-proposal/services/rate-proposal.service';
import {
    ReservationItemCommercialBudget
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { Observable, of } from 'rxjs';
import { RateProposalListBlock } from 'app/reservation/rate-proposal/models/rate-proposal-list-block';
import { RateProposalSortPriceEnum } from 'app/reservation/rate-proposal/models/rate-proposal-sort-price-enum';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';

export const rateProposalServiceStub = createServiceStub(RateProposalService, {
    mapCommercialBudgetToRateProposal(
    commercialBudgetList: Array<ReservationItemCommercialBudget>, sortRoomType?: number, sortPrice?: RateProposalSortPriceEnum
    ): any {
        return null;
    },

    navigateBack() {
    },

    navigateConfirm(propertyId: number, token) {
    },

    searchRateProposalPreparingData (params: RateProposalSearchModel): RateProposalSearchModel {
      return null;
    },

    sortByPrice(list: Array<RateProposalListBlock>, sortType: RateProposalSortPriceEnum): Array<RateProposalListBlock> {
        return null;
    },

    sortByRoomType(list: Array<RateProposalListBlock>, sortRoomType): Array<RateProposalListBlock> {
        return null;
    },
});
