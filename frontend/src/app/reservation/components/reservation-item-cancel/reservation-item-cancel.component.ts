import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { ReservationItemResource } from '../../../shared/resources/reservation-item/reservation-item.resource';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ReservationItemView } from '../../../shared/models/reserves/reservation-item-view';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-reservation-item-cancel',
  templateUrl: './reservation-item-cancel.component.html',
})
export class ReservationItemCancelComponent implements OnInit {
  @Input() reservationItemView: ReservationItemView;

  @Output()
  cancelReservationItemEvent: EventEmitter<ReservationItemView> = new EventEmitter<ReservationItemView>();

  public modalTitle: string;
  public formGroupCancelReservationItem: FormGroup;
  public reasonOptionList: Array<SelectObjectOption>;
  private propertyId: number;

  constructor(
    private modalEmitToggleService: ModalEmitToggleService,
    public formBuilder: FormBuilder,
    private reservationItemResource: ReservationItemResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private commomService: CommomService,
    private route: ActivatedRoute,
    private propertyService: PropertyStorageService
  ) {}

  ngOnInit() {
    this.propertyId = this.propertyService.getCurrentPropertyId();
    this.setVars();
    this.setReservationForm();
    this.setReasonOptionList();
  }

  setVars() {
    this.modalTitle = 'reservationModule.reservationCancel.reservationCancelItem.title';
    this.reasonOptionList = new Array<SelectObjectOption>();
  }

  private setReasonOptionList() {
    this.reservationItemResource.getReservationItemReasons(this.propertyId).subscribe(reasonCanceledOptions => {
      this.reasonOptionList = this.commomService.convertObjectToSelectOptionObject(reasonCanceledOptions);
    });
  }

  public setReservationForm(): void {
    this.formGroupCancelReservationItem = this.formBuilder.group({
      reasonId: ['', [Validators.required]],
      cancellationDescription: [''],
    });
  }

  public closeModal(): void {
    this.modalEmitToggleService.emitToggleWithRef(this.reservationItemView.code);
    this.formGroupCancelReservationItem.reset();
  }

  public cancelReservationItem(): void {
    const reasonId = this.formGroupCancelReservationItem.get('reasonId').value;
    const cancellationDescription = this.formGroupCancelReservationItem.get('cancellationDescription').value;
    this.reservationItemResource.cancelReservationItem(this.reservationItemView.id, reasonId, cancellationDescription).subscribe(
      response => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('reservationModule.reservationCancel.reservationItem.successMessage'),
        );
        this.cancelReservationItemEvent.emit(this.reservationItemView);
        this.closeModal();
      });
  }
}
