// import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { HttpClientModule } from '@angular/common/http';
// // import { Observable } from 'rxjs';
// import { TranslateModule, TranslateService } from '@ngx-translate/core';
// import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
// import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
// import { ActivatedRoute } from '@angular/router';

// // Components
// import { ReservationItemCancelComponent } from './reservation-item-cancel.component';

// // Services
// import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
// import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
// import { SharedService } from 'app/shared/services/shared/shared.service';
// import { CommomService } from '../../../shared/services/shared/commom.service';
// import {SessionParameterService} from '../../../shared/services/parameter/session-parameter.service';

// // Resources
// import { ReservationItemResource } from '../../../shared/resources/reservation-item/reservation-item.resource';
// import {ParameterResource} from '../../../shared/resources/parameter/parameter-resource';

// // Models
// import { SuccessError } from 'app/shared/models/success-error-enum';
// import { ReservationItemViewData } from '../../../shared/mock-data/reservationItem-data';
// import { ReasonCategoryCancelData } from '../../../shared/mock-data/reason-category-cancel-data';
// import { SelectObjectOption } from '../../../shared/models/selectModel';

// describe('ReservationnItemCancelComponent', () => {
//   let component: ReservationItemCancelComponent;
//   let fixture: ComponentFixture<ReservationItemCancelComponent>;
//   const reasonList = new Array<SelectObjectOption>();

//   beforeAll(() => {

//     TestBed.resetTestEnvironment();

//     TestBed
//       .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
//       .configureTestingModule({
//         declarations: [
//           ReservationItemCancelComponent
//         ],
//         imports: [
//           TranslateModule.forRoot(),
//           ReactiveFormsModule,
//
//           HttpClientModule
//         ],
//         providers: [],
//         schemas: [
//           CUSTOM_ELEMENTS_SCHEMA
//         ]
//       });

//     fixture = TestBed.createComponent(ReservationItemCancelComponent);
//     component = fixture.componentInstance;

//     spyOn(component.cancelReservationItemEvent, 'emit').and.callThrough();
//     component.reservationItemView = ReservationItemViewData;

//     const reasonObj = new SelectObjectOption();
//     reasonObj.key = ReasonCategoryCancelData.id;
//     reasonObj.value = ReasonCategoryCancelData.reasonName;

//     reasonList.push(reasonObj);

//     spyOn(component['commomService'], 'convertObjectToSelectOptionObject').and.returnValue(reasonList);
//     spyOn(component['reservationItemResource'], 'getReservationItemReasons').and.returnValue(Observable.of(ReasonCategoryCancelData));

//     fixture.detectChanges();
//   });

//   describe('on init', () => {
//     it('should create', () => {
//       expect(component).toBeTruthy();
//     });

//     it('should set modalTitle, resonOptionList and setReservationForm', fakeAsync(() => {
//       tick();

//       expect(component.modalTitle).toEqual('reservationModule.reservationCancel.reservationCancelItem.title');
//       expect(Object.keys(component.formGroupCancelReservationItem.controls)).toEqual([
//         'reasonId',
//         'cancellationDescription'
//       ]);
//       expect(component.reasonOptionList).toEqual(reasonList);
//     }));
//   });

//   it('should closeModal', () => {
//     spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
//     spyOn(component.formGroupCancelReservationItem, 'reset');

//     component.closeModal();

//     expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.reservationItemView.code);
//     expect(component.formGroupCancelReservationItem.reset).toHaveBeenCalled();
//   });

//   it('should cancelReservationItem with success', () => {
//     spyOn(component['reservationItemResource'], 'cancelReservationItem').and.returnValue(Observable.of({}));
//     spyOn(component, 'closeModal');
//     spyOn(component['toasterEmitService'], 'emitChange');
//     const reasonId = 2;
//     const cancellationDescription = 'Motivo do Cancelamento';
//     component.reservationItemView.id = 1;

//     component.formGroupCancelReservationItem.get('reasonId').setValue(reasonId);
//     component.formGroupCancelReservationItem.get('cancellationDescription').setValue(cancellationDescription);

//     component.cancelReservationItem();

//     expect(component['reservationItemResource'].cancelReservationItem)
// .toHaveBeenCalledWith(component.reservationItemView.id, reasonId, cancellationDescription);
//     expect(component.cancelReservationItemEvent.emit).toHaveBeenCalledWith(component.reservationItemView);
//     expect(component.closeModal).toHaveBeenCalled();
//     expect(component['toasterEmitService'].emitChange)
// .toHaveBeenCalledWith(SuccessError.success, 'reservationModule.reservationCancel.reservationItem.successMessage');
//   });

//   it('should cancelReservationItem with error', () => {
//     spyOn(component['reservationItemResource'], 'cancelReservationItem').and.returnValue(Observable.throw({}));
//     spyOn(component, 'closeModal');
//     spyOn(component['toasterEmitService'], 'emitChange');
//     const reasonId = 2;
//     const cancellationDescription = 'Motivo do Cancelamento';
//     component.reservationItemView.id = 1;
//     component.formGroupCancelReservationItem.get('reasonId').setValue(reasonId);
//     component.formGroupCancelReservationItem.get('cancellationDescription').setValue(cancellationDescription);

//     component.cancelReservationItem();

//     expect(component['reservationItemResource'].cancelReservationItem)
// .toHaveBeenCalledWith(component.reservationItemView.id, reasonId, cancellationDescription);
//     expect(component.closeModal).not.toHaveBeenCalled();
//     expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(SuccessError.error, 'commomData.errorMessage');
//   });

// });
