
import {of as observableOf,  Observable } from 'rxjs';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { ReservationCancelComponent } from './reservation-cancel.component';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ReservationData } from '../../../shared/mock-data/reservation-data';
import { ReasonCategoryCancelData } from '../../../shared/mock-data/reason-category-cancel-data';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { RouterTestingModule } from '@angular/router/testing';
import {
  InterceptorHandlerService,
  ThexApiModule,
  configureApi,
  ApiEnvironment,
  configureAppName
} from '@inovacaocmnet/thx-bifrost';

describe('ReservationCancelComponent', () => {
  let component: ReservationCancelComponent;
  let fixture: ComponentFixture<ReservationCancelComponent>;
  const reasonList = new Array<SelectObjectOption>();

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      declarations: [ReservationCancelComponent],
      imports: [TranslateModule.forRoot(), ReactiveFormsModule, HttpClientModule, RouterTestingModule, ThexApiModule],
      providers: [ InterceptorHandlerService,
        configureApi({
          environment: ApiEnvironment.Development
        }),
        configureAppName('pms'),
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(ReservationCancelComponent);
    component = fixture.componentInstance;

    spyOn(component.cancelReservationEvent, 'emit').and.callThrough();
    component.reservation = ReservationData;

    const reasonObj = new SelectObjectOption();
    reasonObj.key = ReasonCategoryCancelData.id;
    reasonObj.value = ReasonCategoryCancelData.reasonName;

    reasonList.push(reasonObj);

    spyOn<any>(component['commomService'], 'convertObjectToSelectOptionObject').and.returnValue(reasonList);
    spyOn<any>(component['reservationItemResource'], 'getReservationItemReasons')
        .and.returnValue(observableOf(ReasonCategoryCancelData));

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should setVars', () => {
      expect(component.reasonOptionList).toEqual(reasonList);
      expect(component.modalTitle).toEqual('reservationModule.reservationCancel.title');
      expect(Object.keys(component.formGroupCancelReservation.controls)).toEqual(['reasonId', 'cancellationDescription']);
    });
  });

  it('should closeModal', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    spyOn(component.formGroupCancelReservation, 'reset');
    component.reservation.reservationCode = 'X5678987-0001';

    component.closeModal();

    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.reservation.reservationCode);
    expect(component.formGroupCancelReservation.reset).toHaveBeenCalled();
  });

  it(
    'should cancelReservation with success',
    fakeAsync(() => {
      spyOn<any>(component['reservationResource'], 'cancelReservation').and.returnValue(observableOf({}));
      spyOn(component, 'closeModal');
      spyOn(component['toasterEmitService'], 'emitChange');

      const reasonId = 2;
      const cancellationDescription = 'Motivo do Cancelamento';
      component.reservation.id = 1;

      component.formGroupCancelReservation.get('reasonId').setValue(reasonId);
      component.formGroupCancelReservation.get('cancellationDescription').setValue(cancellationDescription);

      component.cancelReservation();

      expect(component['reservationResource'].cancelReservation).toHaveBeenCalledWith(
        component.reservation.id,
        reasonId,
        cancellationDescription,
      );
      expect(component.cancelReservationEvent.emit).toHaveBeenCalled();
      expect(component.closeModal).toHaveBeenCalled();
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'reservationModule.reservationCancel.reservationItem.successMessage',
      );
    }),
  );
});
