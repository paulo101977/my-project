import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { ReservationResource } from '../../../shared/resources/reservation/reservation.resource';
import { ReservationItemResource } from '../../../shared/resources/reservation-item/reservation-item.resource';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { Reservation } from '../../../shared/models/reserves/reservation';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-reservation-cancel',
  templateUrl: './reservation-cancel.component.html',
})
export class ReservationCancelComponent implements OnInit {
  @Input() reservation: Reservation;

  @Output() cancelReservationEvent: EventEmitter<Reservation> = new EventEmitter<Reservation>();

  private propertyId: number;
  public modalTitle: string;
  public formGroupCancelReservation: FormGroup;
  public reasonOptionList: Array<SelectObjectOption>;

  constructor(
    private modalEmitToggleService: ModalEmitToggleService,
    public formBuilder: FormBuilder,
    private reservationResource: ReservationResource,
    private reservationItemResource: ReservationItemResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private commomService: CommomService,
    private route: ActivatedRoute,
    private propertyStoreService: PropertyStorageService
  ) {}

  ngOnInit() {
    this.propertyId = this.propertyStoreService.getCurrentPropertyId();
    this.setVars();
    this.setReservationForm();
    this.setReasonOptionList();
  }

  setVars() {
    this.modalTitle = 'reservationModule.reservationCancel.title';
    this.reasonOptionList = new Array<SelectObjectOption>();
  }

  private setReasonOptionList() {
    this.reservationItemResource.getReservationItemReasons(this.propertyId).subscribe(reasonCanceledOptions => {
      this.reasonOptionList = this.commomService.convertObjectToSelectOptionObject(reasonCanceledOptions);
    });
  }

  public setReservationForm(): void {
    this.formGroupCancelReservation = this.formBuilder.group({
      reasonId: ['', [Validators.required]],
      cancellationDescription: [''],
    });
  }

  public closeModal(): void {
    this.modalEmitToggleService.emitToggleWithRef(this.reservation.reservationCode);
    this.formGroupCancelReservation.reset();
  }

  public cancelReservation(): void {
    const reasonId = this.formGroupCancelReservation.get('reasonId').value;
    const cancellationDescription = this.formGroupCancelReservation.get('cancellationDescription').value;
    this.reservationResource.cancelReservation(this.reservation.id, reasonId, cancellationDescription).subscribe(response => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('reservationModule.reservationCancel.reservationItem.successMessage'),
      );
      this.cancelReservationEvent.emit();
      this.closeModal();
    });
  }
}
