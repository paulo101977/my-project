import { Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ReservationGroupOption } from '@app/shared/models/reserves/reservation-group-option';
import { ReservationRemark } from '@app/shared/models/reserves/reservation-remark';
import { Client } from '@app/shared/models/client';
import { ClientContact } from '@app/shared/models/clientContact';
import { SuccessError } from '@app/shared/models/success-error-enum';
import { Room } from '@app/shared/models/room';
import { RoomType } from 'app/room-type/shared/models/room-type';
import { BedTypeEnum } from '@app/shared/models/bed-type';
import { Reservation } from '@app/shared/models/reserves/reservation';
import { ReservationItemView } from '@app/shared/models/reserves/reservation-item-view';
import { PaymentType } from '@app/shared/models/payment-types';
import { CreditCard } from '@app/shared/models/credit-card';
import { ConfigHeaderPageNew } from '@app/shared/components/header-page-new/config-header-page-new';
import { ReservationConfirmationDto } from '@app/shared/models/dto/reserve-confirmation-dto';
import { PaymentTypeEnum } from '@app/shared/models/reserves/payment-type-enum';
import { ReservationConfirmationEnum } from '@app/shared/models/reserves/reservation-confirmation-enum';
import { ReservationDto } from '@app/shared/models/dto/reservation-dto';
import { ReservationItemDto } from '@app/shared/models/dto/reservation-item-dto';
import { PropertyService } from '@app/shared/services/property/property.service';
import { SharedService } from '@app/shared/services/shared/shared.service';
import { ModalRemarksFormEmitService } from '@app/shared/services/shared/modal-remarks-form-emit.service';
import { ModalGroupOptionsFormEmitService } from '@app/shared/services/shared/modal-group-options-form-emit.service';
import { ReservationService } from '@app/shared/services/reservation/reservation.service';
import { ReservationItemViewService } from '@app/shared/services/reservation-item-view/reservation-item-view.service';
import { DateService } from '@app/shared/services/shared/date.service';
import { ValidatorFormService } from '@app/shared/services/shared/validator-form.service';
import { ToasterEmitService } from '@app/shared/services/shared/toaster-emit.service';
import { RoomTypeService } from '@app/room-type/shared/services/room-type/room-type.service';
import { ModalEmitService } from '@app/shared/services/shared/modal-emit.service';
import { PaymentTypeService } from '@app/shared/services/shared/payment-type.service';
import { ModalEmitToggleService } from '@app/shared/services/shared/modal-emit-toggle.service';
import { StatusStyleService } from '@app/shared/services/reservation/status-style.service';
import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { ReservationResource } from '@app/shared/resources/reservation/reservation.resource';
import { ClientResource } from '@app/shared/resources/client/client.resource';
import * as _ from 'lodash';
import { RightButtonTop } from '@app/shared/models/right-button-top';
import { ParameterTypeEnum } from '@app/shared/models/parameter/pameterType.enum';
import { SessionParameterService } from '@app/shared/services/parameter/session-parameter.service';
import { ReservationWithBudget } from '@app/shared/models/revenue-management/reservation-budget/reservation-budget';
import { AutocompleteConfig } from '@app/shared/models/autocomplete/autocomplete-config';
import {
  ReservationItemCommercialBudgetConfig
} from 'app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';
import { MarketSegmentService } from '@app/shared/services/market-segment/market-segment.service';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { AssetsService } from '@app/core/services/assets.service';
import { ReservationServiceEmit } from '../../services/reservation-service-emit';
import { MarketSegmentResource } from 'app/shared/resources/market-segment/market-segment.resource';
import { OriginResource } from 'app/shared/resources/origin/origin.resource';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { IMyDate } from 'mydatepicker';
import { RateProposalShareInfoService } from 'app/shared/services/rate-proposal/rate-proposal-share-info.service';
import { RateProposalStorageItem } from 'app/reservation/rate-proposal/models/rate-proposal-storage-item';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { CountryCodeEnum } from 'app/shared/models/country-code-enum';
import { ReservationStatusEnum } from '@app/shared/models/reserves/reservation-status-enum';

@Component({
  selector: 'app-reservation-create',
  templateUrl: './reservation-create.component.html',
  styleUrls: ['./reservation-create.component.css'],
})
export class ReservationCreateComponent implements OnInit, OnDestroy {
  public readonly MODAL_RESUME_RESERVATION: 'reservation_resume_modal';
  public readonly ROUTE_VIEW_MODE = 'view';

  @ViewChild('childrenPopoverContent') childrenPopoverContent: ElementRef;
  @ViewChild('form') form: ElementRef;
  public filterParameters: '';
  public showReservationBudgetDetail: boolean;
  public isRoomTypeWithoutBaseRate: boolean;
  public reservationId: number;
  public client: Client;
  public clientContact: ClientContact;
  public topButtonsListConfig: Array<RightButtonTop>;
  public configHeaderPage: ConfigHeaderPageNew;
  public titlePage: string;
  public roomTypes: Array<RoomType>;
  public reservationItemId: number;
  public propertyId: number;
  private roomTypesOfProperty: Array<RoomType>;
  public autocompleteConfig: AutocompleteConfig;
  public autocompleteConfigContactName: AutocompleteConfig;
  public autocompleteConfigClientToBill: AutocompleteConfig;
  public newReservationForm: FormGroup;
  public reservation: Reservation;
  public reservationItemList: Array<ReservationItemDto>;
  public schedulePopoverIsVisible: boolean;
  public childrenPopoverIsVisible: boolean;
  public childrenQuantity: number;
  public reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig;
  public periodSelected: boolean;
  public remarksQuantity: number;
  public reservationItemViewWhichIsUpdating: ReservationItemView;
  public childAgeList: Array<any>;
  public paymentTypeOptionList: Array<PaymentType>;
  public openResume = true;

  // Budget dependencies
  public dateListFromPeriod: Array<Date>;
  public averagePrice: number;
  public currencyCountry: string;
  public totalsValuesAccomodations: any;
  public showBudgetButton: boolean;
  public budgetModalId: string;
  public budgetChildAgeList: Array<number>;
  private propertyCurrencyId: string;

  // AppGroupCotent dependencies
  public placeholderTextMoneyInput: string;
  public hasFlagNoShow: boolean;
  public hasClientToBill: boolean;
  public hasPaymentConfig: boolean;
  public hasPaymentCreditCardConfig: boolean;
  public hasFlagReservationConfirmed: boolean;
  public optionsOriginList: any[];
  public optionsMarketSegmentList: any[];
  public inputIdClientToBill: string;

  // Flag noShow/Deadline dependencies
  public launchDaily = true;
  public isActiveFlagNoShow: boolean;
  public isActiveFlagConfirmed: boolean;
  public isDeadlineDisabled: boolean;
  public showCancelReservationButton: boolean;
  public showReactivateReservationButton: boolean;
  public isWalkin: boolean;
  private quantityOfTimesEnteredInObservePaymentTypeInEdition: number;
  private codeOfReservationItemInvalid: string;
  public closeModalCallback: Function;
  public datePickerDisableUntil: IMyDate;
  public rateProposalToken: string;
  public rateProposalStoreItem: RateProposalStorageItem;

  // Subscriptions
  private unsubscribe: Subscription;
  private reservationBudgetSubscription: Subscription;
  private sub: Subscription;
  private reservationCode = null;
  private propertySubscription: Subscription;
  public propertyCountryCode: string;

  // Screen Conditions
  public isView: boolean;

  // currency symbol
  public currencySymbol: string;

  constructor(
    public translateService: TranslateService,
    public formBuilder: FormBuilder,
    private propertyService: PropertyService,
    public reservationService: ReservationService,
    public reservationItemViewService: ReservationItemViewService,
    private validatorFormService: ValidatorFormService,
    public sessionParameterService: SessionParameterService,
    public route: ActivatedRoute,
    private dateService: DateService,
    public modalRemarksFormEmitService: ModalRemarksFormEmitService,
    public modalGroupOptionsFormEmitService: ModalGroupOptionsFormEmitService,
    private toasterEmitService: ToasterEmitService,
    private roomTypeService: RoomTypeService,
    public modalEmitService: ModalEmitService,
    public router: Router,
    private reservationResource: ReservationResource,
    private roomTypeResource: RoomTypeResource,
    private sharedService: SharedService,
    private clientResource: ClientResource,
    private paymentTypeService: PaymentTypeService,
    private statusStyleService: StatusStyleService,
    private toggleModal: ModalEmitToggleService,
    private marketSegmentService: MarketSegmentService,
    private assetsService: AssetsService,
    public reservationServiceEmit: ReservationServiceEmit,
    private marketSegmentResource: MarketSegmentResource,
    private originResource: OriginResource,
    private commomService: CommomService,
    private rateProposalSharedService: RateProposalShareInfoService,
    private propertyStorageService: PropertyStorageService,
    private renderer: Renderer2
  ) {
    this.propertySubscription = this.propertyStorageService.currentPropertyChangeEmitted$.subscribe(() => {
      this.propertyId = this.propertyStorageService.getCurrentPropertyId();
      this.setPropertyCurrencyId();
      this.loadChildAgeList();
      this.reservation.reservationItemViewList = [];
      this.newReservationForm.get('contactGroup.client').setValue('');
      this.newReservationForm.get('clientToBillGroup.clientId').setValue('');
      this.newReservationForm.get('clientToBillGroup.clientName').setValue('');

    });
  }


  ngOnInit() {
    this.setVars();
    this.setListeners();
    this.optionsOriginList = [];
    this.optionsMarketSegmentList = [];
    this.closeModalCallback = () => this.goToReservationList();
  }

  private setVars() {
    this.propertyId = this.propertyStorageService.getCurrentPropertyId();
    this.isView = this.route.routeConfig.path.indexOf(this.ROUTE_VIEW_MODE) >= 0;
    this.propertyCountryCode = this.propertyService.getPropertyCountryCode();

    this.showReservationBudgetDetail = false;
    this.isRoomTypeWithoutBaseRate = false;
    this.setReservationForm();
    this.isWalkin = false;
    this.budgetModalId = 'reservation-budget-modal-id';
    this.setPropertyCurrencyId();
    this.setAutoCompleteConfig();
    this.setAutoCompleteConfigContactName();
    this.setAutocompleteConfigClientToBill();
    this.initReservation();
    this.isActiveFlagNoShow = false;
    this.isActiveFlagConfirmed = false;
    this.isDeadlineDisabled = false;
    this.hasFlagNoShow = false;
    this.hasClientToBill = false;
    this.hasPaymentConfig = false;
    this.hasPaymentCreditCardConfig = false;
    this.hasFlagReservationConfirmed = false;
    this.placeholderTextMoneyInput = this.translateService.instant('reservationModule.commomData.realTypeMoneyForPlaceholder');
    this.periodSelected = false;
    this.dateListFromPeriod = new Array<Date>();
    this.totalsValuesAccomodations = this.setTotalDefaultValuesAccomodationsPage();
    this.currencyCountry = window.navigator.language;
    this.schedulePopoverIsVisible = false;
    this.childrenPopoverIsVisible = false;
    this.averagePrice = 0;
    this.roomTypes = this.getRoomTypesOfProperty();
    this.setConfigHeaderPage();
    this.setButtonListHeaderConfig();

    this.remarksQuantity = this.reservationService.getRemarksQuantityOfReservation(
      this.reservation.externalComments,
      this.reservation.internalComments,
      this.reservation.partnerComments
    );
    this.loadChildAgeList();
    this.inputIdClientToBill = 'clientToBill';
    this.quantityOfTimesEnteredInObservePaymentTypeInEdition = 0;
    this.loadInfo();
  }

  private loadInfo() {
    this.sub = combineLatest(
      this.getOrigins(),
      this.getMarketSegment(),
      this.route.queryParams)
      .subscribe(([origins, marketSegments, queryParams]) => {
        this.reservationId = +queryParams.reservation;
        this.setOrigins(origins);
        this.setMarketSegment(marketSegments);

        if (queryParams.rateProposal) {
          this.rateProposalToken = queryParams.rateProposal;
          this.rateProposalStoreItem = this.rateProposalSharedService.getRateProposal(this.rateProposalToken);
          this.loadRateProposal();
        } else if (this.reservationId) {
          this.loadReservation();
          this.showBudgetButton = true;
        } else {
          this.getReservationCode();
        }

        if (+queryParams.iswalkin) {
          this.isWalkin = true;
        }

        this.paymentTypeOptionList = this.paymentTypeService.getPaymentTypeList(this.isWalkin);
      });
  }

  public showPrePaymentFlag() {
    return this.propertyCountryCode == CountryCodeEnum.PT;
  }

  private getReservationCode() {
    this.reservationResource.getReservationCode().subscribe(item => {
      this.reservationCode = item.reservationCode;
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    if (this.propertySubscription) {
      this.propertySubscription.unsubscribe();
    }
    if (this.reservationBudgetSubscription) {
      this.reservationBudgetSubscription.unsubscribe();
    }
  }

  private loadRateProposal() {
    if (this.newReservationForm) {
      this.newReservationForm.patchValue({
        contactGroup: {
          client: this.rateProposalStoreItem ? this.rateProposalStoreItem.rateProposalSearchModel.customerClientName : ''
        },
        clientToBillGroup: {
          clientId: this.rateProposalStoreItem ? this.rateProposalStoreItem.rateProposalSearchModel.customerClientName : '',
          clientName: this.rateProposalStoreItem ? this.rateProposalStoreItem.rateProposalSearchModel.customerClientId : ''
        }
      });
    }
  }

  private setPropertyCurrencyId() {
    this.propertyCurrencyId = this.sessionParameterService
      .getParameterValue(this.propertyId, ParameterTypeEnum.DefaultCurrency);
  }

  private setAutoCompleteConfig() {
    this.autocompleteConfig = new AutocompleteConfig();
    this.autocompleteConfig.dataModel = this.newReservationForm.get('contactGroup.client').value;
    this.autocompleteConfig.callbackToSearch = this.searchItemsClient;
    this.autocompleteConfig.extraClasses = 'thf-u-font-input-default';
    this.autocompleteConfig.callbackToGetSelectedValue = this.updateItemClient;
    this.autocompleteConfig.fieldObjectToDisplay = 'templateClientSearch';
    this.autocompleteConfig.placeholder = this.translateService
      .instant('label.placeholder.clientSearch');
  }

  private setAutoCompleteConfigContactName() {
    this.autocompleteConfigContactName = new AutocompleteConfig();
    this.autocompleteConfigContactName.dataModel = this.newReservationForm.get('contactGroup.name').value;
    this.autocompleteConfigContactName.callbackToSearch = this.searchItemsClientContact;
    this.autocompleteConfigContactName.extraClasses = 'thf-u-font-input-default';
    this.autocompleteConfigContactName.callbackToGetSelectedValue = this.updateItemContactName;
    this.autocompleteConfigContactName.fieldObjectToDisplay = 'name';
  }

  private setAutocompleteConfigClientToBill() {
    this.autocompleteConfigClientToBill = new AutocompleteConfig();
    this.autocompleteConfigClientToBill.dataModel = this.newReservationForm.get('clientToBillGroup.clientName').value;
    this.autocompleteConfigClientToBill.callbackToSearch = this.searchItemsClientToBill;
    this.autocompleteConfigClientToBill.extraClasses = 'thf-u-font-input-default';
    this.autocompleteConfigClientToBill.callbackToGetSelectedValue = this.updateItemClientToBill;
    this.autocompleteConfigClientToBill.fieldObjectToDisplay = 'templateClientSearch';
    this.autocompleteConfigClientToBill.placeholder = this.translateService
      .instant('label.placeholder.clientSearch');
  }

  public showCancelOrReactivateButton(): void {
    if (this.reservationService.canCancelReservation(this.reservation)) {
      this.showCancelReservationButton = true;
      this.showReactivateReservationButton = false;
    } else if (this.reservationService.canReactivateReservation(this.reservation)) {
      this.showCancelReservationButton = false;
      this.showReactivateReservationButton = true;
    } else {
      this.showCancelReservationButton = false;
      this.showReactivateReservationButton = false;
    }
    this.setButtonListHeaderConfig();
  }

  private loadChildAgeList(): void {
    this.childAgeList = this.propertyService.getChildAgeOfProperty(this.propertyId);
  }

  private loadReservation(): void {
    if (this.reservationId) {
      this.reservationResource.getReservationById(this.reservationId).subscribe(reservationData => {
        this.reservation = reservationData;
        this.setConfigHeaderPage();
        this.showCancelOrReactivateButton();

        this.loadReservationConfirmation(this.reservation.reservationConfirmation.paymentTypeId);
        this.remarksQuantity = this.reservationService.getRemarksQuantityOfReservation(
          this.reservation.externalComments,
          this.reservation.internalComments,
        );
        this.loadClientAndContacts();
        this.loadOriginAndSegment();
        this.reservation.reservationItemViewList = this.reservationService.getReservationItemViewListWithFormCardAndPricesCalculated(
          this.reservation.reservationItemViewList,
          this.roomTypes,
        );
        this.loadTotalsAccomodations();
        this.reservation.reservationItemViewList.forEach(reservationItem => {
          this.setStatusCanceledForReservationItems(reservationItem);
          this.loadRoomTypesOfAccomodation(reservationItem);
        });
        this.launchDaily = this.reservation.launchDaily;

        if (this.isView) {
          this.setFormDisabled();
        }
      });
    }
  }

  private setFormDisabled() {
    this.newReservationForm.disable({ onlySelf: true });
  }

  private setStatusCanceledForReservationItems(reservationItem) {
    if (this.statusStyleService.isCanceledStatus(reservationItem.status)) {
      reservationItem.isCanceledStatus = true;
      reservationItem.focus = false;
    }
  }

  private loadReservationConfirmation(paymentTypeId: number): void {
    this.reservation.reservationConfirmation.paymentTypeId = paymentTypeId;
    this.newReservationForm.get('selectValuePaymentType.paymentType').setValue(paymentTypeId);

    if (this.paymentTypeService.isDeposit(paymentTypeId)) {
      this.loadPaymentInfo();
    }

    if (this.paymentTypeService.isCreditCard(paymentTypeId)) {
      this.loadCreditCard();
    }

    if (this.paymentTypeService.isToBeBillet(paymentTypeId)) {
      this.loadClientToBillet();
    }

    if (this.showLayoutWithNoShowFlag(paymentTypeId)) {
      this.setNoShowReservationConfirmation(this.reservation.reservationConfirmation.ensuresNoShow);
    }
  }

  private loadCreditCard() {
    this.loadPaymentInfo();
    if (this.reservation.reservationConfirmation
      && this.reservation.reservationConfirmation.plastic) {

      if (this.reservation.reservationConfirmation.plastic.holder) {
        this.newReservationForm.get('creditCardGroup.holder')
          .setValue(this.reservation.reservationConfirmation.plastic.holder);
      }
      if (this.reservation.reservationConfirmation.plastic.plasticBrandId) {
        this.newReservationForm.get('creditCardGroup.selectValueBanner.banner')
          .setValue(this.reservation.reservationConfirmation.plastic.plasticBrandId);
      }
    }
  }

  private loadClientToBillet() {
    if (this.reservation.reservationConfirmation.billingClientId) {
      this.newReservationForm.get('clientToBillGroup.clientId').setValue(this.reservation.reservationConfirmation.billingClientId);
      this.newReservationForm.get('clientToBillGroup.clientName').setValue(this.reservation.reservationConfirmation.billingClientName);
    }
  }

  private loadClientToBilletWithClientOfReservation(clientId: string, clientName: string) {
    this.newReservationForm.get('clientToBillGroup.clientId').setValue(clientId);
    this.newReservationForm.get('clientToBillGroup.clientName').setValue(clientName);
  }

  private loadPaymentInfo() {
    if (this.reservation.reservationConfirmation.deadline) {
      const date = new Date(this.reservation.reservationConfirmation.deadline);
      this.isActiveFlagConfirmed = false;
      this.newReservationForm.get('deadlineGroup.deadline').setValue(date);
    } else {
      this.isActiveFlagConfirmed = true;
      this.setReservationConfirmationStatus(this.isActiveFlagConfirmed);
    }
    this.newReservationForm.get('moneyGroup.moneyValue').setValue(this.reservation.reservationConfirmation.value);
  }

  private loadClientAndContacts(): void {
    this.loadClient();
    this.loadContactName();
    this.loadContactEmail();
    this.loadContactPhone();
  }

  private loadClient(): void {
    if (this.reservation.client.id) {
      this.client = new Client();
      this.client = this.reservation.client;
      this.newReservationForm.get('contactGroup.client').setValue(this.reservation.client.tradeName);
      this.setClientModelAndClientOfContactGroup(this.reservation.client);
      if (this.canSetClientForBill() && this.reservation.reservationConfirmation.paymentTypeId == PaymentTypeEnum.ToBeBilled) {
        this.loadClientToBilletWithClientOfReservation(this.client.id, this.client.tradeName);
      }
      if (this.reservation.clientContact.id) {
        this.clientContact = new ClientContact();
        this.clientContact.id = this.reservation.clientContact.id;
      }
    }
  }

  private loadContactName(): void {
    this.newReservationForm.get('contactGroup.name').setValue(this.reservation.contactName);
  }

  private loadContactEmail(): void {
    this.newReservationForm.get('contactGroup.email').setValue(this.reservation.contactEmail);
  }

  private loadContactPhone(): void {
    this.newReservationForm.get('contactGroup.phone').setValue(this.reservation.contactPhone);
  }

  private loadTotalsAccomodations(): void {
    const totalsAccomodations = this.reservationService.getTotalsValuesReservationItemView(this.reservation.reservationItemViewList);
    this.setTotalValuesAccomodations(totalsAccomodations);
  }

  private loadOriginAndSegment(): void {
    const originExistsInList = this.optionsOriginList.some(ori => ori.key == this.reservation.originId);
    const marketSegmentExistsInList = this.optionsMarketSegmentList.some(mkt => mkt.key == this.reservation.segmentId);

    this.newReservationForm.get('selectOrigin.origin')
      .setValue(originExistsInList ? this.reservation.originId : '');
    this.newReservationForm.get('selectMarketSegment.marketSegment')
      .setValue(marketSegmentExistsInList ? this.reservation.segmentId : '');
  }

  private loadRoomTypesOfAccomodation(reservationItemView: ReservationItemView): void {
    const period = reservationItemView.formCard.get('periodCard').value;
    if (period) {
      const roomTypes = new Array<RoomType>();
      const initialDate = period.beginDate;
      const endDate = period.endDate;
      this.roomTypeResource
        .getRoomTypesWithOverBookingByPropertyId(this.propertyId, initialDate, endDate, true)
        .subscribe(roomTypesOfProperty => {
          roomTypesOfProperty.forEach(roomTypeObject => roomTypes.push(roomTypeObject));
          reservationItemView.roomTypes = roomTypes;
          this.roomTypesOfProperty = reservationItemView.roomTypes;

          this.setRoomTypeSelectedCard(reservationItemView);

          const requestCardValue = reservationItemView.formCard.get('roomTypeRequestedCard');
          requestCardValue.setValue(this.reservationService.getRoomTypeSelected(reservationItemView.roomTypes, requestCardValue.value));
          this.setNewRoomTypeListByGuestQuantityCard(
            reservationItemView,
            reservationItemView.formCard.get('childrenQuantityCard').value,
            reservationItemView.formCard.get('adultQuantityCard').value,
          );
          this.setExtraBedQuantityCardAndCribBedQuantityCardOfReservationItemView(reservationItemView);
          this.setRoomLayoutOfReservation(reservationItemView);
        });
    }
  }

  private setRoomTypeSelectedCard(reservationItemView: ReservationItemView) {
    const roomTypeSelected = this.reservationService
      .getRoomTypeSelected(reservationItemView.roomTypes, reservationItemView.formCard.get('roomTypeCard').value);

    if (roomTypeSelected.roomList) {
      if (reservationItemView.room) {
        roomTypeSelected.roomList.push(reservationItemView.room);
      }
      roomTypeSelected.roomList = _.orderBy(roomTypeSelected.roomList, ['roomNumber'], ['asc']);
    }

    reservationItemView.formCard.get('roomTypeCard').setValue(roomTypeSelected);
  }

  private setExtraBedQuantityCardAndCribBedQuantityCardOfReservationItemView(reservationItemView: ReservationItemView): void {
    reservationItemView.extraBedQuantityList = this.reservationService.setBedOptionalQuantity(
      reservationItemView.formCard.get('roomTypeCard').value,
      BedTypeEnum.Single,
    );
    reservationItemView.cribBedQuantityList = this.reservationService.setBedOptionalQuantity(
      reservationItemView.formCard.get('roomTypeCard').value,
      BedTypeEnum.Crib,
    );
    reservationItemView.formCard.get('extraBedQuantityCard').setValue(reservationItemView.extraBedCount);
    reservationItemView.formCard.get('cribBedQuantityCard').setValue(reservationItemView.extraCribCount);
  }

  private setRoomLayoutOfReservation(reservationItemView: ReservationItemView) {
    reservationItemView.formCard.get('roomLayoutCard').setValue(reservationItemView.roomLayout);
  }

  public disabledButtonConfirmReservation() {
    return (
      this.reservationService.getStatusIsPossibleToSaveReserve(
        this.newReservationForm.controls.contactGroup.valid,
        this.newReservationForm.controls.selectOrigin.valid,
        this.newReservationForm.controls.selectMarketSegment.valid,
        this.reservation.reservationItemViewList,
      ) ||
      this.reservationService.reservationConfirmationGroupIsInvalid(
        this.newReservationForm.controls.selectValuePaymentType.valid,
        this.newReservationForm.controls.moneyGroup.valid,
        this.newReservationForm.controls.deadlineGroup.valid,
        this.newReservationForm.controls.creditCardGroup.valid,
        this.newReservationForm.controls.clientToBillGroup.valid,
      ) ||
      this.reservationService.canReactivateReservation(this.reservation) ||
      this.isRoomTypeWithoutBaseRate ||
      (this.newReservationForm.controls.selectValuePaymentType.value == PaymentTypeEnum.ToBeBilled && !this.client)
    );
  }

  public setTotalsOfReservation(reservationItemViewList: Array<ReservationItemView>) {
    this.totalsValuesAccomodations = this.setTotalDefaultValuesAccomodationsPage();
    const totalsAccomodations = this.reservationService.getTotalsValuesReservationItemView(reservationItemViewList);
    this.setTotalValuesAccomodations(totalsAccomodations);
  }

  public setTotalDefaultValuesAccomodationsPage() {
    return {
      totalValueDaily: 0,
      totalRooms: 0,
      totalGuests: 0,
      totalAdults: 0,
      totalChildren: 0,
      currencySymbol: ' ',
    };
  }

  public setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
    let action = '';
    if (this.reservationId) {
      action = this.isView
        ? 'action.lbView'
        : 'action.lbEdit';
    } else {
      action = 'action.lbNew';
      if (this.isWalkin) {
        action = 'reservationModule.reserveCreate.titleWalkin';
      }
    }
    this.titlePage = this.translateService.instant(action, {
      labelName: this.translateService.instant('text.reservation')
    });
  }

  private setButtonListHeaderConfig() {
    if (!this.isView) {
      if (this.reservationId && (
        this.showCancelReservationButton ||
        this.showReactivateReservationButton)) {

        let title, iconDefault, iconAlt;
        if (this.showCancelReservationButton) {
          title = 'reservationModule.commomData.cancelReservation';
          iconDefault = this.assetsService.getImgUrlTo('close.svg');
          iconAlt = 'icon-button-cancel';
        } else {
          title = 'reservationModule.commomData.reactivateReservation';
          iconDefault = this.assetsService.getImgUrlTo('reativar_icon.svg');
          iconAlt = 'icon-button-reservaReativar';
        }

        this.topButtonsListConfig = [
          {
            title: 'label.reservationBudget',
            iconDefault: this.assetsService.getImgUrlTo('icon_dollar_sign.svg'),
            iconAlt: 'img.icon_budget',
            callback: () => this.navigateToReservationBudget(),
          },
          {
            title: title,
            iconDefault: iconDefault,
            iconAlt: iconAlt,
            callback: () => {
              if (this.showCancelReservationButton) {
                this.openCancelReservationModal();
              } else if (this.showReactivateReservationButton) {
                this.openReactivateReservationModal();
              }
            },
          },
        ];
      } else {
        this.topButtonsListConfig = [
          {
            title: 'label.reservationBudget',
            iconDefault: this.assetsService.getImgUrlTo('icon_dollar_sign.svg'),
            iconAlt: 'img.icon_budget',
            callback: () => this.navigateToReservationBudget(),
          },
        ];
      }
    }
  }

  private setListeners() {
    this.observePaymentType();
  }

  private observePaymentType() {
    this.newReservationForm.get('selectValuePaymentType.paymentType').valueChanges.subscribe(value => {
      if (!this.launchDaily) {
        this.launchDaily = true;
      }
      this.quantityOfTimesEnteredInObservePaymentTypeInEdition++;
      if (!this.reservation.id) {
        this.resetValuesReservationConfirmation();
      }
      this.setValidatorsInControlsOfPaymentType(value);
      this.hasClientToBill = this.showLayoutWithClientToBill(value);
      this.hasFlagNoShow = this.showLayoutWithNoShowFlag(value);
      this.hasPaymentConfig = this.isToShowMoneyInput(value);
      this.hasFlagReservationConfirmed = this.isToShowMoneyInput(value);
      this.hasPaymentCreditCardConfig = this.paymentTypeService.isCreditCard(value);
      if (this.isToShowMoneyInput) {
        this.setReservationConfirmationStatus(this.isActiveFlagConfirmed);
      }

      let tradeName = null;
      let clientId = null;
      if (this.hasClientToBill && this.client && value == PaymentTypeEnum.ToBeBilled) {
        tradeName = this.client.tradeName;
        clientId = this.client.id;
      }
      this.newReservationForm.get('clientToBillGroup.clientName').setValue(tradeName);
      this.newReservationForm.get('clientToBillGroup.clientId').setValue(clientId);
    });
  }

  private resetValuesReservationConfirmation() {
    this.resetPaymentTypeControls();
    if (this.keepIdOfReservationConfirmation()) {
      this.resetReservationConfirmationAndKeepId();
    } else if (!this.reservationId) {
      this.reservation.reservationConfirmation = new ReservationConfirmationDto();
    }
  }

  private resetPaymentTypeControls(): void {
    this.reservation.reservationConfirmation = new ReservationConfirmationDto();
    this.resetValuesAndValidatorsOfMoneyGroup();
    this.resetValuesAndValidatorsOfDeadlineGroup();
    this.resetValuesAndValidatorsOfCreditCardGroup();
    this.resetValuesAndValidatorsOfClientToBillGroup();
  }

  private resetValuesAndValidatorsOfMoneyGroup(): void {
    this.newReservationForm.get('moneyGroup').reset();
    this.newReservationForm.get('moneyGroup.moneyValue').clearValidators();
    this.newReservationForm.get('moneyGroup.moneyValue').updateValueAndValidity();
  }

  private resetValuesAndValidatorsOfDeadlineGroup(): void {
    this.newReservationForm.get('deadlineGroup').reset();
  }

  private resetValuesAndValidatorsOfCreditCardGroup(): void {
    this.newReservationForm.get('creditCardGroup').reset();
    this.newReservationForm.get('creditCardGroup.holder').clearValidators();
    this.newReservationForm.get('creditCardGroup.holder').updateValueAndValidity();
    this.newReservationForm.get('creditCardGroup.selectValueBanner.banner').clearValidators();
    this.newReservationForm.get('creditCardGroup.selectValueBanner.banner').updateValueAndValidity();
  }

  private resetValuesAndValidatorsOfClientToBillGroup(): void {
    this.newReservationForm.get('clientToBillGroup').reset();
    this.newReservationForm.get('clientToBillGroup.clientId').clearValidators();
    this.newReservationForm.get('clientToBillGroup.clientId').updateValueAndValidity();
  }

  private keepIdOfReservationConfirmation(): boolean {
    return this.reservationId > 0 && this.quantityOfTimesEnteredInObservePaymentTypeInEdition > 2;
  }

  private resetReservationConfirmationAndKeepId(): void {
    const reservationConfirmationId = this.reservation.reservationConfirmation.id;
    this.reservation.reservationConfirmation = new ReservationConfirmationDto();
    this.reservation.reservationConfirmation.id = reservationConfirmationId;
  }

  private getRoomTypesOfProperty() {
    return this.roomTypesOfProperty;
  }

  private setValidatorsInControlsOfPaymentType(paymentTypeEnum: PaymentTypeEnum): void {
    switch (paymentTypeEnum) {
      case PaymentTypeEnum.CreditCard:
        this.newReservationForm.get('creditCardGroup.holder').setValidators([Validators.required]);
        this.newReservationForm.get('creditCardGroup.selectValueBanner.banner').setValidators([Validators.required]);
        break;
      case PaymentTypeEnum.ToBeBilled:
        this.newReservationForm.get('clientToBillGroup.clientId').setValidators([Validators.required]);
        const client = this.newReservationForm.get('contactGroup.client').value;
        if (client.completeItem) {
          this.setClientForBillNameAndIdWhenClientIsSelected(client);
        } else {
          this.newReservationForm.get('clientToBillGroup.clientId').setValue(null);
        }
        break;
      default:
        this.newReservationForm.get('clientToBillGroup.clientId').setValidators(null);
        this.setClientForBillNameAndIdWhenClientIsSelected(null);
        break;
    }
  }

  private isToShowMoneyInput(value: number) {
    return this.paymentTypeService.isCreditCard(value) || this.paymentTypeService.isDeposit(value);
  }

  private setDeadlineDefaultValue() {
    const addTwoDays = this.dateService.getSystemDate(this.propertyId);
    this.datePickerDisableUntil = <IMyDate>{
      day: addTwoDays.getDate() - 1,
      month: addTwoDays.getMonth() + 1,
      year: addTwoDays.getFullYear()
    };
    addTwoDays.setDate(addTwoDays.getDate() + 2);
    this.newReservationForm.get('deadlineGroup.deadline').setValue(addTwoDays);
  }

  private showLayoutWithNoShowFlag(value: number) {
    return (
      this.paymentTypeService.isHotel(value) ||
      this.paymentTypeService.isToBeBillet(value)
    );
  }

  private showLayoutWithClientToBill(value: number) {
    return this.paymentTypeService.isClientToBill(value);
  }

  public setReservationGroupForm(reservationFormData: FormGroup) {
    this.newReservationForm.get('reservationGroup.period').setValue(reservationFormData.get('period').value);
    this.newReservationForm.get('reservationGroup.checkinHour').setValue(reservationFormData.get('checkinHour').value);
    this.newReservationForm.get('reservationGroup.checkoutHour').setValue(reservationFormData.get('checkoutHour').value);
    this.newReservationForm.get('reservationGroup.roomQuantity').setValue(reservationFormData.get('roomQuantity').value);
    this.newReservationForm.get('reservationGroup.adultQuantity').setValue(reservationFormData.get('adultQuantity').value);
    this.newReservationForm.get('reservationGroup.childrenQuantity').setValue(reservationFormData.get('childrenQuantity').value);
    this.newReservationForm.get('reservationGroup.roomType').setValue(reservationFormData.get('roomType').value);
    this.newReservationForm.get('reservationGroup.roomTypeRequested').setValue(reservationFormData.get('roomTypeRequested').value);
    this.newReservationForm.get('reservationGroup.childrenAgeList').setValue(reservationFormData.get('childrenAgeList').value);
  }


  public setReservationForm() {
    this.newReservationForm = this.formBuilder.group({
      contactGroup: this.formBuilder.group({
        client: [this.rateProposalStoreItem ? this.rateProposalStoreItem.rateProposalSearchModel.customerClientName : ''],
        name: ['', [Validators.required, this.reservationService.emptyString()]],
        email: ['', [Validators.required, Validators.email]],
        phone: [''],
      }),
      reservationGroup: this.formBuilder.group({
        period: ['', Validators.required],
        checkinHour: ['', [Validators.required, this.reservationService.checkinHourAndMinutesValid()]],
        checkoutHour: ['', [Validators.required, this.reservationService.checkinHourAndMinutesValid()]],
        roomQuantity: [
          0,
          [Validators.required, this.validatorFormService.capacityNumberIsMajorThanOne, this.validatorFormService.setNumberToZero],
        ],
        adultQuantity: [
          0,
          [Validators.required, this.validatorFormService.capacityNumberIsMajorThanOne, this.validatorFormService.setNumberToZero],
        ],
        childrenQuantity: [0, [this.validatorFormService.setNumberToZero]],
        roomType: [{ value: '' }],
        roomTypeRequested: null,
        childrenAgeList: this.formBuilder.array([]),
      }),
      selectValuePaymentType: this.formBuilder.group({
        paymentType: [0, Validators.required],
      }),
      selectOrigin: this.formBuilder.group({
        origin: ['', Validators.required],
      }),
      selectMarketSegment: this.formBuilder.group({
        marketSegment: ['', Validators.required],
      }),
      moneyGroup: this.formBuilder.group({
        moneyValue: [''],
      }),
      deadlineGroup: this.formBuilder.group({
        deadline: [''],
      }),
      creditCardGroup: this.formBuilder.group({
        holder: [''],
        plasticNumber: [''],
        expirationDate: [''],
        csc: [''],
        selectValueBanner: this.formBuilder.group({
          banner: [''],
        }),
      }),
      clientToBillGroup: this.formBuilder.group({
        clientId: [this.rateProposalStoreItem ? this.rateProposalStoreItem.rateProposalSearchModel.customerClientName : ''],
        clientName: [this.rateProposalStoreItem ? this.rateProposalStoreItem.rateProposalSearchModel.customerClientId : ''],
      }),
    });

    this.newReservationForm.get('reservationGroup.roomQuantity').setValue(1);
    this.newReservationForm.get('reservationGroup.adultQuantity').setValue(1);
  }

  public setNoShowReservationConfirmation(event) {
    this.reservation.reservationConfirmation.ensuresNoShow = event;
    this.isActiveFlagNoShow = event;
  }

  public setTotalValuesAccomodations(totals) {
    this.totalsValuesAccomodations = {
      totalRooms: totals.rooms,
      totalGuests: totals.guests,
      totalAdults: totals.adultQuantity,
      totalChildren: totals.childrenQuantity,
      totalValueDaily: totals.valuesDailies,
      currencySymbol: totals.currencySymbol || ' '
    };

    this.currencySymbol = totals.currencySymbol || ' ';
  }

  public sameCurrency() {
    if (this.reservationItemList && this.reservationItemList.length > 0) {
      const currency = this.reservationItemList[ 0 ].currencyId;
      const otherCurrencies = this.reservationItemList.filter(item => item.currencyId != currency);
      return !(otherCurrencies.length > 0);
    }
    return true;
  }

  public searchItemsClient = (searchData: any) => {
    const name: string = searchData.query;
    this.clientResource
      .getClientsBySearchData(this.propertyId, name)
      .subscribe(clients => {
        this.autocompleteConfig.resultList = clients.map((clientItem) => {
          if (clientItem) {
            clientItem
              .templateClientSearch = clientItem.tradeName + ' - ' + clientItem.document;
            return clientItem;
          }
        });
      });
  }

  public searchItemsClientToBill = (searchData: any) => {
    const name: string = searchData.query;
    this.clientResource
      .getClientsBySearchData(this.propertyId, name)
      .subscribe(clients => {
        this.autocompleteConfigClientToBill.resultList = clients.map((clientItem) => {
          if (clientItem) {
            clientItem
              .templateClientSearch = clientItem.tradeName + ' - ' + clientItem.document;
            return clientItem;
          }
        });
      });
  }

  public searchItemsClientContact = (searchData: any) => {
    if (this.client) {
      const name: string = searchData.query;
      this.clientResource
        .getContactsOfClientsBySearchData(this.propertyId, this.client.id, name)
        .subscribe(contacts => {
          this.autocompleteConfigContactName.resultList = contacts;
        });
    }
  }

  public updateItemClient = (item: Client) => {
    if (item) {
      this.setClientModelAndClientOfContactGroup(item);
      if (this.canSetClientForBill()) {
        this.setClientForBillNameAndIdWhenClientIsSelected(item);
      }
    } else {
      this.client = null;
      this.clientContact = null;
      this.newReservationForm.get('contactGroup.client').setValue('');
      this.newReservationForm.get('contactGroup').markAsDirty();
    }
  }

  private canSetClientForBill(): boolean {
    return (
      !this.newReservationForm.get('clientToBillGroup.clientId').value ||
      (this.newReservationForm.get('clientToBillGroup.clientId').value && !this.newReservationForm.get('clientToBillGroup').dirty)
    );
  }

  private setClientModelAndClientOfContactGroup(item: Client): void {
    this.client = item;
    this.newReservationForm.get('contactGroup.client').setValue(this.client.tradeName);
    this.newReservationForm.get('contactGroup').markAsDirty();
    const hasClient = true;
    this.paymentTypeOptionList = this.paymentTypeService.getPaymentTypeList(this.isWalkin, hasClient);
  }

  public updateItemContactName = (item: ClientContact) => {
    if (item) {
      this.clientContact = item;
      this.reservation.contactName = item.name;
      this.newReservationForm.get('contactGroup.name').setValue(item.name || '');
      this.newReservationForm.get('contactGroup.email').setValue(item.email || '');
      this.newReservationForm.get('contactGroup.phone').setValue(item.phoneNumber || '');
      this.newReservationForm.get('contactGroup').markAsDirty();
    } else {
      this.reservation.contactName = '';
      this.newReservationForm.get('contactGroup.name').setValue('');
      this.newReservationForm.get('contactGroup').markAsDirty();
    }
  }

  private initReservation() {
    if (this.sharedService.objectIsNullOrUndefined(this.reservation)) {
      this.reservation = new Reservation();
      this.reservation.propertyId = this.propertyId;
    }
  }

  private setNewRoomTypeListOfReservationItemView(reservationItemView: ReservationItemView, newRoomTypeList: Array<RoomType>) {
    const roomTypeSelected = reservationItemView.formCard.get('roomTypeCard').value;
    const cribQuantity = reservationItemView.formCard.get('cribBedQuantityCard').value;
    const extraQuantity = reservationItemView.formCard.get('extraBedQuantityCard').value;

    reservationItemView.roomTypes = new Array<RoomType>();
    reservationItemView.roomTypes = newRoomTypeList;
    reservationItemView.rooms = new Array<Room>();

    const roomType = reservationItemView.roomTypes.find(roomTypeItem => roomTypeItem.id === roomTypeSelected.id);
    reservationItemView.formCard.get('roomTypeCard').setValue(roomType);
    reservationItemView.formCard.get('roomTypeRequestedCard').setValue(roomType);
    reservationItemView.formCard.get('cribBedQuantityCard').setValue(cribQuantity);
    reservationItemView.formCard.get('extraBedQuantityCard').setValue(extraQuantity);
  }

  private setNewRoomTypeListByGuestQuantityCard(reservationItemView: ReservationItemView, childrenQuantity: number, adultQuantity: number) {
    const newRoomTypes = this.reservationService.getNewRoomTypesByGuestCapacity(
      reservationItemView.roomTypes,
      adultQuantity,
      childrenQuantity,
    );
    if (!this.roomTypeService.hasRoomTypeAtRoomTypeList(this.roomTypesOfProperty, reservationItemView.formCard.get('roomTypeCard').value)) {
      this.setNewRoomTypeListOfReservationItemView(reservationItemView, newRoomTypes);

      if (reservationItemView.roomTypes && reservationItemView.formCard.controls.roomTypeCard.value) {
        reservationItemView.formCard
          .get('roomTypeCard')
          .setValue(
            this.reservationService.getRoomTypeSelected(
              reservationItemView.roomTypes,
              reservationItemView.formCard.controls.roomTypeCard.value,
            ),
          );
      }
    }
  }

  public showRemarksModal() {
    this.modalRemarksFormEmitService.emitSimpleModal(
      this.reservation.internalComments,
      this.reservation.externalComments,
      this.reservation.partnerComments);
  }

  public showGroupOptionsModal() {
    this.modalGroupOptionsFormEmitService.emitSimpleModal(this.reservation.groupName, this.reservation.unifyAccounts);
  }

  public goBackPreviewPage() {
    this.reservationService.goBackPreviewPage();
  }

  private setOriginAndSegment(): void {
    this.reservation.originId = this.newReservationForm.get('selectOrigin.origin').value;
    this.reservation.segmentId = this.newReservationForm.get('selectMarketSegment.marketSegment').value;
  }

  public checkToSaveReservation(openResume?) {
    this.openResume = openResume;

    if (!this.reservationId || this.reservationService.hasChange(this.newReservationForm, this.reservation)) {
      if (this.reservationService.canSaveNewReservationForm(this.newReservationForm, this.reservation.reservationItemViewList)) {
        this.openConfirmReservationSaveModal();
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.error,
          this.translateService.instant('reservationModule.commomData.errorMessageToSaveReserve'),
        );
      }
    }
  }

  public saveReservationAndRedirect() {
    if (this.isWalkin && this.reservationWalkinIsInvalid()) {
      this.toasterEmitService.emitChange(SuccessError.error, this.translateService.instant('reservationModule.commomData.errorSaveWalkin'));
      return;
    }

    this.setNameAndEmailAndPhoneInReservation();
    this.setOriginAndSegment();
    this.setClientAndContact();
    this.setReservationConfirmation();

    if (this.reservationConfirmationIsInvalid()) {
      this.toasterEmitService.emitChange(
        SuccessError.error,
        `${this.translateService.instant(
          'reservationModule.commomData.errorReservationConfirmationPartOne',
        )} ${this.reservationItemViewService.reduceAndGetCodeOfReservationItem(
          this.codeOfReservationItemInvalid,
        )} ${this.translateService.instant('reservationModule.commomData.errorReservationConfirmationPartTwo')}`,
      );
      return;
    }

    this.reservation.walkin = this.isWalkin;
    this.reservation.launchDaily = this.showPrePaymentFlag() ? this.launchDaily : true;

    if (this.reservation.id == 0) {
      this.reservation.reservationItemViewList.forEach(budget => {
        if (budget.reservationItemCommercialBudgetConfig.isGratuityType) {
          budget.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent = null;
        }
        budget.reservationItemCommercialBudgetConfig.reservationBudgetTableList.forEach(budgetDaily => {
          if (!budgetDaily.currencyId) {
            budgetDaily.currencyId = this.propertyCurrencyId;
          }
        });
      });
      if (this.reservationCode) {
        this.reservation.reservationCode = this.reservationCode;
      }
      this.reservationResource.createReservation(this.reservation).subscribe(reservation => {
        this.reservation.reservationItemViewList.forEach((item, index) => {
          item.code = reservation.reservationItemList[ index ].reservationItemCode; // update code to real code.
        });
        this.resolveReservationSuccessPromise(reservation);
      });
    } else {
      this.reservationResource.editReservation(this.reservation).subscribe(reservation => {
        this.resolveReservationSuccessPromise(reservation);
      });
    }
  }

  private openConfirmReservationSaveModal() {
    this.modalEmitService.emitSimpleModal(
      this.translateService.instant('commomData.warning'),
      this.translateService.instant('reservationModule.reserveCreate.confirmReserveSaveAndRedirect'),
      () => this.saveReservationAndRedirect(),
      [],
      () => this.cancelReservationAndRedirect()
    );
  }

  public cancelReservationAndRedirect() {
    this.openResume = true;
    if (this.reservationBudgetSubscription) {
      this.reservationBudgetSubscription.unsubscribe();
    }
  }

  private reservationWalkinIsInvalid(): boolean {
    let hasErrors = false;
    this.reservation.reservationItemViewList.forEach(reservationItemView => {
      if (this.reservationItemViewService.periodOfReservationItemIsInvalidForWalkin(reservationItemView, this.propertyId)) {
        hasErrors = true;
      }
    });
    return hasErrors;
  }

  private reservationConfirmationIsInvalid(): boolean {
    let reservationItemViewWithLessCheckinDate = new ReservationItemView();
    if (
      this.reservation.reservationConfirmation.paymentTypeId == <number>ReservationConfirmationEnum.Deposit ||
      this.reservation.reservationConfirmation.paymentTypeId == <number>ReservationConfirmationEnum.CreditCard
    ) {
      this.reservation.reservationItemViewList.forEach((reservationItemView, index) => {
        if (index == 0) {
          reservationItemViewWithLessCheckinDate = reservationItemView;
        } else {
          const checkinDateOfCurrentElement = reservationItemView.formCard.get('periodCard').value.beginJsDate;
          const checkinDatefLastElement = this.reservation.reservationItemViewList[ index - 1 ]
            .formCard.get('periodCard').value.beginJsDate;
          if (checkinDateOfCurrentElement < checkinDatefLastElement) {
            reservationItemViewWithLessCheckinDate = reservationItemView;
          }
        }
      });
      if (
        this.reservationService.deadlineIsInvalid(
          this.reservation.reservationConfirmation.deadline,
          reservationItemViewWithLessCheckinDate.formCard.get('periodCard').value.beginJsDate,
        )
      ) {
        this.codeOfReservationItemInvalid = reservationItemViewWithLessCheckinDate.code;
        return true;
      }
    }
    return false;
  }

  public setReservationConfirmationStatus(reservationConfirmed: boolean) {
    if (!reservationConfirmed) {
      this.launchDaily = true;
    }
    this.isActiveFlagConfirmed = reservationConfirmed;
    this.reservation.reservationConfirmation.isConfirmed = this.isActiveFlagConfirmed;
    if (this.reservation.reservationConfirmation.isConfirmed) {
      this.setDeadlineDisabled(reservationConfirmed);
    } else {
      this.setDeadlineDefaultValue();
      this.isDeadlineDisabled = reservationConfirmed;
    }
  }

  private setDeadlineDisabled(reservationConfirmed: boolean) {
    this.reservation.reservationConfirmation.deadline = null;
    this.newReservationForm.get('deadlineGroup.deadline').setValue(null);
    this.isDeadlineDisabled = reservationConfirmed;
  }

  private setReservationConfirmation() {
    this.setNoShow();
    if (this.paymentTypeService.isCortesy(this.newReservationForm.get('selectValuePaymentType.paymentType').value)) {
      this.setCortesyToPayment();
    } else {
      this.setPayment();
    }
  }

  private setNoShow() {
    const paymentTypeId = this.newReservationForm.get('selectValuePaymentType.paymentType').value;
    if (this.paymentTypeIsInternalUseOrCortesyOrHotelOrToBillet(paymentTypeId)) {
      this.reservation.reservationConfirmation.ensuresNoShow = this.isActiveFlagNoShow;
      this.isActiveFlagConfirmed = false;
      this.reservation.reservationConfirmation.isConfirmed = false;
    }
  }

  private paymentTypeIsInternalUseOrCortesyOrHotelOrToBillet(paymentTypeId: number): boolean {
    return (
      this.paymentTypeService.isInternalUsage(paymentTypeId) ||
      this.paymentTypeService.isCortesy(paymentTypeId) ||
      this.paymentTypeService.isHotel(paymentTypeId) ||
      this.paymentTypeService.isToBeBillet(paymentTypeId)
    );
  }

  private setClientAndContact() {
    this.reservation.client = this.client;
    this.reservation.clientContact = this.clientContact;
  }

  private setCortesyToPayment() {
    this.setPayment();
    this.reservation.reservationConfirmation.value = null;
    this.reservation.reservationItemViewList.forEach(reservationItem => {
      reservationItem.dailyPrice = 0;
      reservationItem.totalDaily = 0;
      reservationItem.reservationItemCommercialBudgetConfig.reservationBudgetTableList.forEach(budget => {
        budget.manualRate = 0;
      });
    });
  }

  private setPayment() {
    this.reservation.reservationConfirmation.paymentTypeId = this.newReservationForm.get('selectValuePaymentType.paymentType').value;
    this.reservation.reservationConfirmation.billingClientId = this.newReservationForm.get('clientToBillGroup.clientId').value;
    this.reservation.reservationConfirmation.billingClientName = this.newReservationForm.get('clientToBillGroup.clientName').value;

    if (this.paymentTypeService.isDeposit(this.reservation.reservationConfirmation.paymentTypeId)) {
      this.setDeadlineAndValue();
    }
    if (this.paymentTypeService.isCreditCard(this.reservation.reservationConfirmation.paymentTypeId)) {
      this.setCreditCardData();
    }
    if (this.paymentTypeService.isToBeBillet(this.reservation.reservationConfirmation.paymentTypeId)) {
      this.setClientToBillet();
    } else {
      this.reservation.reservationConfirmation.billingClientId = null;
      this.reservation.reservationConfirmation.billingClientName = null;
    }
  }

  private setDeadlineAndValue() {
    if (this.newReservationForm.get('deadlineGroup.deadline').value) {
      const myDate = this.dateService.convertUniversalDateToIMyDate(this.newReservationForm.get('deadlineGroup.deadline').value);
      const year = myDate.date.year;
      const month = myDate.date.month;
      const day = myDate.date.day;
      this.reservation.reservationConfirmation.deadline = new Date(Date.UTC(year, month - 1, day));
    }
    this.reservation.reservationConfirmation.value = this.newReservationForm.get('moneyGroup.moneyValue').value;
  }

  private setCreditCardData() {
    this.setDeadlineAndValue();

    this.reservation.reservationConfirmation.plastic = new CreditCard();
    this.reservation.reservationConfirmation.plastic.plasticBrandId = this.newReservationForm.get(
      'creditCardGroup.selectValueBanner.banner',
    ).value;
    this.reservation.reservationConfirmation.plastic.holder = this.newReservationForm.get('creditCardGroup.holder').value;
  }

  private setClientToBillet(): void {
    this.reservation.reservationConfirmation.billingClientId = this.newReservationForm.get('clientToBillGroup.clientId').value;
  }

  private resolveReservationSuccessPromise(reservation: ReservationDto): void {
    this.toasterEmitService.emitChange(SuccessError.success, 'reservationModule.commomData.saveReserveSuccessMessage');
    this.reservationServiceEmit.emitSave(reservation);

    if (this.isWalkin) {
      this.reservationService.goToFnrh(reservation.id, this.propertyId);
    } else {
      this.reservation.reservationCode = reservation.reservationCode;
      this.reservation.id = reservation.id;
      this.reservation.reservationUid = reservation.reservationUid;
      this.reservationItemList = reservation.reservationItemList;
      if (this.openResume) {
        this.toggleModal.emitToggleWithRef(this.MODAL_RESUME_RESERVATION);
      }
    }
  }

  private setNameAndEmailAndPhoneInReservation(): void {
    this.reservation.contactName = this.reservationService.getContactName(this.newReservationForm.get('contactGroup'));
    this.reservation.contactEmail = this.reservationService.getContactEmail(this.newReservationForm.get('contactGroup'));
    this.reservation.contactPhone = this.reservationService.getContactPhone(this.newReservationForm.get('contactGroup'));
  }

  public setInternalCommentsAndExternalCommentsInReservation(reservationRemark: ReservationRemark) {
    this.reservation.externalComments = reservationRemark.externalComments;
    this.reservation.internalComments = reservationRemark.internalComments;
    this.remarksQuantity = this.reservationService.getRemarksQuantityOfReservation(
      this.reservation.externalComments,
      this.reservation.internalComments,
    );
  }

  public setGroupNameAndUnifyAccountsInReservation(reservationGroupOption: ReservationGroupOption) {
    this.reservation.groupName = reservationGroupOption.groupName;
    this.reservation.unifyAccounts = reservationGroupOption.unifyAccounts;
  }

  private getOrigins(): Observable<any> {
    return this.originResource.getOrigin(this.propertyId);
  }

  private setOrigins(origins): void {
    this.optionsOriginList = this.commomService.toOption(origins, 'id', 'description');
  }

  private getMarketSegment(): Observable<any> {
    return this.marketSegmentResource.getAllMarketSegmentList(this.propertyId);
  }

  private setMarketSegment(marketSegments): void {
    this.optionsMarketSegmentList = this.marketSegmentService
      .prepareSecondarySelectList(marketSegments.items);
  }

  public updateItemClientToBill = (item: Client) => {
    if (item) {
      this.setClientForBillNameAndIdWhenClientIsSelected(item);
    } else {
      this.newReservationForm.get('clientToBillGroup.clientName').setValue('');
      this.newReservationForm.get('clientToBillGroup.clientId').setValue('');
      this.newReservationForm.get('clientToBillGroup').markAsDirty();
    }
  }

  private setClientForBillNameAndIdWhenClientIsSelected(item: Client): void {
    if (item) {
      this.newReservationForm.get('clientToBillGroup.clientName').setValue(item.tradeName);
      this.newReservationForm.get('clientToBillGroup.clientId').setValue(item.id);
      this.newReservationForm.get('clientToBillGroup').markAsDirty();
      if (!this.client) {
        this.setClientModelAndClientOfContactGroup(item);
      }
    } else {
      this.newReservationForm.get('clientToBillGroup.clientName').setValue(null);
      this.newReservationForm.get('clientToBillGroup.clientId').setValue(null);
    }
  }

  public openCancelReservationModal(): void {
    this.toggleModal.emitToggleWithRef(this.reservation.reservationCode);
  }

  public cancelReservation(): void {
    this.reloadPage();
  }

  private reloadPage(): void {
    window.location.reload();
  }

  public openReactivateReservationModal(): void {
    this.modalEmitService.emitSimpleModal(
      this.translateService.instant('commomData.warning'),
      this.translateService.instant('reservationModule.commomData.confirmRactivateReservation'),
      this.reactivateReservation,
      [],
    );
  }

  private reactivateReservation = () => {
    this.reservationResource.reactivateReservation(this.reservationId).subscribe(
      response => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('reservationModule.commomData.reactivateDoneWithSuccess'),
        );
        const self = this;
        setTimeout(() => {
          self.reloadPage();
        }, 3000);
      },
      error => {
        this.toasterEmitService.emitChange(SuccessError.error, this.translateService.instant('commomData.errorMessage'));
      },
    );
  }

  public reactivateReservationItem(reservationItem: ReservationItemView): void {
    this.setFocusForReservationItem(reservationItem);
    this.showCancelOrReactivateButton();
  }

  private setFocusForReservationItem(reservationItem: ReservationItemView): void {
    this.reservation.reservationItemViewList.forEach(accommodation => (accommodation.focus = false));
    reservationItem.focus = true;
  }

  public setFocus(reservationItemViewWithLists: any) {
    reservationItemViewWithLists.oldList.forEach(accommodation => (accommodation.focus = false));
    reservationItemViewWithLists.newList[ reservationItemViewWithLists.newList.length - 1 ].focus = true;
  }

  public setStatusColorFooter() {
    this.reservation.reservationItemViewList = this.reservationService.getReservationItemViewListWithStatusColorFooter(
      this.reservation.reservationItemViewList,
    );
  }

  private setCurrencyIdToBudgetWithoutCurrency(commercialBudget) {
    if (commercialBudget.reservationBudgetList) {
      commercialBudget.reservationBudgetList.forEach(reservationBudget => {
        if (!reservationBudget.currencyId) {
          reservationBudget.currencyId = this.propertyCurrencyId;
        }
      });
    }
  }

  public budgetModalIdsetReservationBudget(commercialBudget: ReservationWithBudget): void {
    this.setCurrencyIdToBudgetWithoutCurrency(commercialBudget);
    this.isRoomTypeWithoutBaseRate = false;
    this.reservationItemCommercialBudget = commercialBudget.reservationItemCommercialBudget;
  }

  public updateReservationItemCard(commercialBudget: ReservationWithBudget) {
    const reservationItemCommercialBudget = commercialBudget.reservationItemCommercialBudget;
    this.isRoomTypeWithoutBaseRate = false;
    this.reservationItemCommercialBudget = commercialBudget.reservationItemCommercialBudget;
    if (reservationItemCommercialBudget && reservationItemCommercialBudget.reservationItemCode) {
      const reservationItemView = this.reservation.reservationItemViewList.find(r => {
        return r.code == reservationItemCommercialBudget.reservationItemCode;
      });
      reservationItemView.gratuityTypeId = commercialBudget.gratuityTypeId;
      reservationItemView.reservationItemCommercialBudgetConfig.reservationBudgetTableList = commercialBudget.reservationBudgetList;
      reservationItemView.reservationItemCommercialBudgetConfig = reservationItemCommercialBudget;

      const periodCardValue = reservationItemView.formCard.get('periodCard').value;
      if (periodCardValue != reservationItemView.initialPeriod) {
        reservationItemView.initialPeriod = periodCardValue;
        reservationItemView.period = periodCardValue;
      }

      const roomTypeRequestedCardValue = reservationItemView.formCard.get('roomTypeRequestedCard').value;
      if (roomTypeRequestedCardValue != reservationItemView.initialRequestedRoomType) {
        reservationItemView.initialRequestedRoomType = roomTypeRequestedCardValue;
        reservationItemView.requestedRoomType = roomTypeRequestedCardValue;
      }

      const roomTypeCardValue = reservationItemView.formCard.get('roomTypeCard').value;
      if (roomTypeCardValue != reservationItemView.initialReceivedRoomType) {
        reservationItemView.initialReceivedRoomType = roomTypeCardValue;
        reservationItemView.receivedRoomType = roomTypeCardValue;
      }

      this.reservation.reservationItemViewList = this.reservation.reservationItemViewList.map(reservationItemViewItem =>
        this.sharedService.assignObjectWithMethods({}, reservationItemViewItem),
      );
    }
  }

  public setReservationItemViewWhichIsUpdating(reservationItemView: ReservationItemView): void {
    this.reservationItemViewWhichIsUpdating = reservationItemView;
  }

  public goToReservationList() {
    this.reservationService.goToReservationList(this.propertyId);
  }

  public navigateToReservationBudget() {
    this.checkToSaveReservation();
    this.reservationBudgetSubscription = this.reservationServiceEmit.reservationSaved$
      .subscribe(
        reservation => {
          this.goToReservationBudget(reservation.id)
            .then(resolve => {
              return this.reservationBudgetSubscription.unsubscribe();
            });
        }
      );
  }

  public goToReservationBudget(reservationId: number) {
    return this.reservationService.goToReservationBudget(reservationId, this.propertyId);
  }

  public shouldLaunchDaily(status) {
    this.launchDaily = status;
  }

  public getReadOnlyForGroupOptions(): boolean {
    return this.isView || this.reservation.reservationItemViewList
            .some(x => x.status != null && x.status != ReservationStatusEnum.Confirmed &&
                    x.status != ReservationStatusEnum.ToConfirm);
  }
}
