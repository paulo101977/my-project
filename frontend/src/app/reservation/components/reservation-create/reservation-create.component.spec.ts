// TODO make 'should create' work
// import { Location } from '@angular/common';
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { FormBuilder, FormControl, ValidatorFn } from '@angular/forms';
// import { ActivatedRoute } from '@angular/router';
// import { RouterTestingModule } from '@angular/router/testing';
// import { TranslateService } from '@ngx-translate/core';
// import { AssetsService } from 'app/core/services/assets.service';
// import { ReservationCreateComponent } from 'app/reservation/components/reservation-create/reservation-create.component';
// import { ReservationServiceEmit } from 'app/reservation/services/reservation-service-emit';
// import { ReservationDto } from 'app/shared/models/dto/reservation-dto';
// import { Reservation } from 'app/shared/models/reserves/reservation';
// import { ClientResource } from 'app/shared/resources/client/client.resource';
// import { MarketSegmentResource } from 'app/shared/resources/market-segment/market-segment.resource';
// import { OriginResource } from 'app/shared/resources/origin/origin.resource';
// import { ReservationResource } from 'app/shared/resources/reservation/reservation.resource';
// import { RoomTypeResource } from 'app/shared/resources/room-type/room-type.resource';
// import { MarketSegmentService } from 'app/shared/services/market-segment/market-segment.service';
// import { ParameterService } from 'app/shared/services/parameter/parameter.service';
// import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
// import { PropertyService } from 'app/shared/services/property/property.service';
// import { RateProposalShareInfoService } from 'app/shared/services/rate-proposal/rate-proposal-share-info.service';
// import { ReservationItemViewService } from 'app/shared/services/reservation-item-view/reservation-item-view.service';
// import { ReservationService } from 'app/shared/services/reservation/reservation.service';
// import { StatusStyleService } from 'app/shared/services/reservation/status-style.service';
// import { RoomTypeService } from 'app/shared/services/room-type/room-type.service';
// import { CommomService } from 'app/shared/services/shared/commom.service';
// import { DateService } from 'app/shared/services/shared/date.service';
// import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
// import { ModalEmitService } from 'app/shared/services/shared/modal-emit.service';
// import { ModalGroupOptionsFormEmitService } from 'app/shared/services/shared/modal-group-options-form-emit.service';
// import { ModalRemarksFormEmitService } from 'app/shared/services/shared/modal-remarks-form-emit.service';
// import { PaymentTypeService } from 'app/shared/services/shared/payment-type.service';
// import { SharedService } from 'app/shared/services/shared/shared.service';
// import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
// import { ValidatorFormService } from 'app/shared/services/shared/validator-form.service';
// import { Observable, of } from 'rxjs';
// import { ActivatedRouteStub } from '../../../../../testing/activated-route-stub';
// import { createPipeStub } from '../../../../../testing/create-pipe-stub';
// import { createServiceStub } from '../../../../../testing/create-service-stub';
//
// fdescribe('ReservationCreateComponent', () => {
//   let fixture: ComponentFixture<ReservationCreateComponent>;
//   let component: ReservationCreateComponent;
//
//   const TranslateStubPipe = createPipeStub({name: 'translate'});
//   const ImgCdnStubPipe = createPipeStub({name: 'imgCdn'});
//
//   const TranslateServiceStub = createServiceStub<TranslateService>(TranslateService);
//   const PropertyServiceStub = createServiceStub<PropertyService>(PropertyService);
//   const ReservationItemViewServiceStub = createServiceStub<ReservationItemViewService>(ReservationItemViewService);
//   const SessionParameterServiceStub = createServiceStub<SessionParameterService>(SessionParameterService);
//   const DateServiceStub = createServiceStub<DateService>(DateService);
//   const ModalRemarksFormEmitServiceStub = createServiceStub<ModalRemarksFormEmitService>(ModalRemarksFormEmitService);
//   const ModalGroupOptionsFormEmitServiceStub = createServiceStub<ModalGroupOptionsFormEmitService>(ModalGroupOptionsFormEmitService);
//   const ToasterEmitServiceStub = createServiceStub<ToasterEmitService>(ToasterEmitService);
//   const RoomTypeServiceStub = createServiceStub<RoomTypeService>(RoomTypeService);
//   const ModalEmitServiceStub = createServiceStub<ModalEmitService>(ModalEmitService);
//   const RoomTypeResourceStub = createServiceStub<RoomTypeResource>(RoomTypeResource);
//   const SharedServiceStub = createServiceStub<SharedService>(SharedService);
//   const ClientResourceStub = createServiceStub<ClientResource>(ClientResource);
//   const PaymentTypeServiceStub = createServiceStub<PaymentTypeService>(PaymentTypeService);
//   const ModalEmitToggleServiceStub = createServiceStub<ModalEmitToggleService>(ModalEmitToggleService);
//   const StatusStyleServiceStub = createServiceStub<StatusStyleService>(StatusStyleService);
//   const ParameterServiceStub = createServiceStub<ParameterService>(ParameterService);
//   const MarketSegmentServiceStub = createServiceStub<MarketSegmentService>(MarketSegmentService);
//   const AssetsServiceStub = createServiceStub<AssetsService>(AssetsService);
//   const MarketSegmentResourceStub = createServiceStub<MarketSegmentResource>(MarketSegmentResource);
//   const OriginResourceStub = createServiceStub<OriginResource>(OriginResource);
//   const CommomServiceStub = createServiceStub<CommomService>(CommomService);
//   const RateProposalShareInfoServiceStub = createServiceStub<RateProposalShareInfoService>(RateProposalShareInfoService);
//   const LocationStub = createServiceStub<Location>(Location);
//
//   const ReservationServiceStub = createServiceStub<ReservationService>(ReservationService, {
//     checkinHourAndMinutesValid(): ValidatorFn {
//       return () => null;
//     }
//   });
//
//   const ValidatorFormServiceStub = createServiceStub<ValidatorFormService>(ValidatorFormService, {
//     capacityNumberIsMajorThanOne(capacityControl: any): null | { required: boolean } { return null; },
//     setNumberToZero(valueField: FormControl): void { return null; }
//   });
//
//   const ReservationResourceStub = createServiceStub<ReservationResource>(ReservationResource, {
//     getReservationById(reservationId: number): Observable<Reservation> {
//       const dto: ReservationDto = {
//         businessSourceId: 0,
//         companyClient: undefined,
//         companyClientContactPersonId: '',
//         companyClientId: '',
//         contactEmail: '',
//         contactId: 0,
//         contactName: '',
//         contactPhone: '',
//         externalComments: '',
//         groupName: '',
//         id: 0,
//         internalComments: '',
//         marketSegmentId: 0,
//         partnerComments: '',
//         propertyId: 0,
//         reservationCode: '',
//         reservationConfirmation: undefined,
//         reservationItemList: undefined,
//         reservationUid: '',
//         reservationVendorCode: '',
//         unifyAccounts: false,
//         walkin: false
//       };
//       const reservation = new Reservation();
//       reservation.normalizeResponse(dto);
//       return of(reservation);
//     }
//   });
//
//
//   beforeEach(async() => {
//     TestBed.configureTestingModule({
//       schemas: [ NO_ERRORS_SCHEMA ],
//       imports: [ RouterTestingModule ],
//       declarations: [
//         ReservationCreateComponent,
//         TranslateStubPipe,
//         ImgCdnStubPipe
//       ],
//       providers: [
//         FormBuilder,
//         { provide: ActivatedRoute, useClass: ActivatedRouteStub },
//         TranslateServiceStub,
//         PropertyServiceStub,
//         ReservationServiceStub,
//         ReservationItemViewServiceStub,
//         ValidatorFormServiceStub,
//         SessionParameterServiceStub,
//         DateServiceStub,
//         ModalRemarksFormEmitServiceStub,
//         ModalGroupOptionsFormEmitServiceStub,
//         ToasterEmitServiceStub,
//         RoomTypeServiceStub,
//         ModalEmitServiceStub,
//         ReservationResourceStub,
//         RoomTypeResourceStub,
//         SharedServiceStub,
//         ClientResourceStub,
//         PaymentTypeServiceStub,
//         ModalEmitToggleServiceStub,
//         StatusStyleServiceStub,
//         ParameterServiceStub,
//         MarketSegmentServiceStub,
//         AssetsServiceStub,
//         ReservationServiceEmit,
//         MarketSegmentResourceStub,
//         OriginResourceStub,
//         CommomServiceStub,
//         RateProposalShareInfoServiceStub,
//         LocationStub
//       ]
//     }).compileComponents();
//   });
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(ReservationCreateComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeDefined();
//   });
// });
