import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ReservationCardComponent} from './reservation-card.component';
import {TranslateModule} from '@ngx-translate/core';
import {SharedModule} from '@app/shared/shared.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {Data} from 'app/reservation/mock-data/index';
import {configureTestSuite} from 'ng-bullet';

describe('ReservationCardComponent', () => {
  let component: ReservationCardComponent;
  let fixture: ComponentFixture<ReservationCardComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ReservationCardComponent],
      imports: [
        TranslateModule.forRoot(),
        SharedModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ReservationCardComponent);
    component = fixture.componentInstance;
    component.comments = Data.COMMENTS_OBJECT;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
