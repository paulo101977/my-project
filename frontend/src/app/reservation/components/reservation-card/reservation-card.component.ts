import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ReservationSearchDto } from '@app/shared/models/dto/reservation-search-dto';
import { DateService } from 'app/shared/services/shared/date.service';
import {PaymentTypeService} from 'app/shared/services/shared/payment-type.service';
import { ModalRemarksFormEmitService } from '@app/shared/services/shared/modal-remarks-form-emit.service';
import {ReservationRemark} from 'app/shared/models/reserves/reservation-remark';
import {ReservationService} from 'app/shared/services/reservation/reservation.service';

@Component({
  selector: 'app-reservation-card',
  templateUrl: './reservation-card.component.html',
  styleUrls: ['./reservation-card.component.css']
})
export class ReservationCardComponent implements OnInit, OnChanges {

  @Input() reservationObj: ReservationSearchDto;
  public optionsCurrencyMask: any;
  public optionsCurrencyMaskLeft: any;
  public calcDaily = 0;
  public paymentName: string;
  public comments: any;
  public remarksQuantity: number;
  public isView: boolean;

  constructor(
    public payTypeSvc: PaymentTypeService,
    public modalRemarksFormEmitService: ModalRemarksFormEmitService,
    public reservationService: ReservationService,
    private dateService: DateService
  ) {

  }

  public ngOnInit(): void {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'right', precision: 2 };
    this.optionsCurrencyMaskLeft = { prefix: '', thousands: '.', decimal: ',', align: 'left', precision: 2 };
    this.setVars();
  }

  public ngOnChanges(change: SimpleChanges): void {
    if (change.reservationObj.currentValue) {
      this.comments = { ...change.reservationObj.currentValue};
      this.reservationObj = { ...change.reservationObj.currentValue};
      this.paymentName = this.getPaymentType(this.reservationObj.paymentTypeId);
      this.calculateDaily();
      this.calculateRemarks();
    }
  }

  private calculateRemarks() {
    this.remarksQuantity = this.reservationService.getRemarksQuantityOfReservation(
      this.comments.externalComments,
      this.comments.internalComments,
      this.comments.partnerComments
    );
}

  private setVars() {
    this.isView = true;
    this.calculateRemarks();

  }

  public calculateDaily() {
    if (this.reservationObj.arrivalDate && this.reservationObj.departureDate) {
      this.calcDaily = this.dateService
        .convertPeriodToDateList(this.reservationObj.arrivalDate, this.reservationObj.departureDate ).length;
    }
  }

  private getPaymentType(id: number) {
    return this.payTypeSvc.filterPaymentById(id);
  }

  public showRemarksModal() {
    this.modalRemarksFormEmitService.emitSimpleModal(
      this.comments.internalComments,
      this.comments.externalComments,
      this.comments.partnerComments);
  }

  public setInternalCommentsAndExternalCommentsInReservation(reservationRemark: ReservationRemark) {
    this.comments.externalComments = reservationRemark.externalComments;
    this.comments.internalComments = reservationRemark.internalComments;
    this.remarksQuantity = this.reservationService.getRemarksQuantityOfReservation(
      this.comments.externalComments,
      this.comments.internalComments,
    );
  }
}
