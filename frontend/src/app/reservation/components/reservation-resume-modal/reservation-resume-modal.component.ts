import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedService} from '../../../shared/services/shared/shared.service';
import {ModalEmitToggleService} from '../../../shared/services/shared/modal-emit-toggle.service';
import {ReservationService} from '../../../shared/services/reservation/reservation.service';
import {ReservationResource} from '../../../shared/resources/reservation/reservation.resource';
import {Reservation} from '../../../shared/models/reserves/reservation';
import {ButtonConfig, ButtonType} from '../../../shared/models/button-config';
import {ReservationItemDto} from '../../../shared/models/dto/reservation-item-dto';
import * as moment from 'moment';
import { DateService } from '../../../shared/services/shared/date.service';
import { Proforma } from '../../../shared/models/proforma';
import { ProformaResource } from '../../../shared/resources/proforma/proforma.resource';
import { PropertyService } from './../../../shared/services/property/property.service';
import { CountryCodeEnum } from '../../../shared/models/country-code-enum';
import {ModalEmitService} from 'app/shared/services/shared/modal-emit.service';
import {TranslateService} from '@ngx-translate/core';
import { FiscalDocumentsService } from '../../../shared/services/billing-account/fiscal-documents.service';
import { PdfPrintService } from '../../../shared/services/pdf-print/pdf-print.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { SuccessError } from '../../../shared/models/success-error-enum';
import {RoomlistResource} from 'app/room-list/shared/services/roomlist.resource';

@Component({
  selector: 'reservation-resume-modal',
  templateUrl: './reservation-resume-modal.component.html',
  styleUrls: ['./reservation-resume-modal.component.scss'],
})
export class ReservationResumeModalComponent implements OnInit, OnDestroy {

  public readonly MODAL_ROOM_LIST = 'room-list-share';

  @Input() modalId: string;
  @Input() close: Function;
  @Input() propertyId: number;
  @Input() currencySymbol: string;

  private _reservation: Reservation;

  @Input()
  set reservation(reservation: Reservation) {
    this._reservation = reservation;
  }
  get reservation(): Reservation {
    return this._reservation;
  }

  private _reservationItemList: Array<ReservationItemDto>;
  @Input()
  set reservationItemList(reservationItemList: Array<ReservationItemDto>) {
    this._reservationItemList = reservationItemList;
    if (reservationItemList) {
      if (reservationItemList.length > 0) {
        this.checkinDateToUse = moment(reservationItemList[0].estimatedArrivalDate, DateService.DATE_FORMAT_UNIVERSAL).toDate();
        this.checkoutDateToUse = moment(reservationItemList[0].estimatedDepartureDate, DateService.DATE_FORMAT_UNIVERSAL).toDate();
      }
      this.acomodationQuantity = reservationItemList.length;

      reservationItemList.forEach(item => {
        this.adultQuantity += item.adultCount;
        this.childQuantity += item.childCount;

        if (moment(item.estimatedArrivalDate).isBefore(this.checkinDateToUse)) {
          this.checkinDateToUse = moment(item.estimatedDepartureDate, DateService.DATE_FORMAT_UNIVERSAL).toDate();
        }

        if (moment(item.estimatedDepartureDate).isAfter(this.checkoutDateToUse)) {
          this.checkoutDateToUse = moment(item.estimatedDepartureDate, DateService.DATE_FORMAT_UNIVERSAL).toDate();
        }

        if (item.reservationBudgetList && item.reservationBudgetList.length > 0) {
          this.total += item.reservationBudgetList
            .reduce( (budgetTotal, budgetItem) =>  budgetTotal + budgetItem.manualRate , 0);
        }
      });
      this.averageDaily = this.setAverageDaily();
    }
  }
  get reservationItemList(): Array<ReservationItemDto> {
    return this._reservationItemList;
  }

  public checkinDateToUse: Date;
  public checkoutDateToUse: Date;
  public acomodationQuantity = 0;
  public adultQuantity = 0;
  public childQuantity = 0;
  public averageDaily = 0;
  public total = 0;
  public shareRoomListEmail: string;
  private propertyCountryCode: string;

  public buttonNewReservationConfig: ButtonConfig;
  public buttonReservationListConfig: ButtonConfig;
  public buttonShareRoomListConfig: ButtonConfig;
  public buttonShareRoomListCancelConfig: ButtonConfig;


  constructor(
    private sharedService: SharedService,
    public router: Router,
    public route: ActivatedRoute,
    private toggleModal: ModalEmitToggleService,
    private reservationService: ReservationService,
    private reservationResource: ReservationResource,
    private roomlistResource: RoomlistResource,
    private proFormaResource: ProformaResource,
    private propertyService: PropertyService,
    private modalEmitService: ModalEmitService,
    private translateService: TranslateService,
    private fiscalDocumentService: FiscalDocumentsService,
    private pdfPrintService: PdfPrintService,
    private toasterEmitService: ToasterEmitService
  ) {}

  ngOnInit() {
    this.propertyCountryCode = this.propertyService.getPropertyCountryCode();
    this.setButtonConfig();
  }

  ngOnDestroy () {
    this.toggleModal.closeAllModals();
  }

  private setAverageDaily() {
    return this.total / this.reservationItemList.reduce((lenght, item) => lenght + item.reservationBudgetList.length, 0 );
  }

  private setButtonConfig() {
    this.buttonNewReservationConfig = this.sharedService.getButtonConfig(
      'new-reservation-modal',
      this.goToNewReservation.bind(this),
      'label.insertNewReservation',
      ButtonType.Secondary,
    );
    this.buttonReservationListConfig = this.sharedService.getButtonConfig(
      'go-reservation-modal',
      this.goToConsultReservation.bind(this),
      'label.consultReservation',
      ButtonType.Primary,
    );
    this.buttonShareRoomListConfig = this.sharedService.getButtonConfig(
      'go-shareroomlist-modal',
      this.shareRoomlist.bind(this),
      'action.share',
      ButtonType.Primary,
    );
    this.buttonShareRoomListCancelConfig = this.sharedService.getButtonConfig(
      'go-shareroomlist-modal',
      this.cancelShareRoomlist.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
  }

  public goToSlipPage() {
    this.toggleModal.emitToggleWithRef(this.modalId);
    this.reservationService.goToSlip(this.reservation.id, this.propertyId);
  }

  public goToRoomList() {
    this.toggleModal.emitToggleWithRef(this.modalId);

    if (this.reservationItemList.length > 1) {
      this.reservationService.goToRoomList(this.reservation.id, this.reservationItemList.length, this.propertyId);
    } else {
      this.reservationResource
        .getReservationItemsByReservationIdAndPropertyId(this.reservation.id, this.propertyId)
        .subscribe(reservationItem => {
          this.reservationService.goToRoomList(reservationItem[0].id, this.reservationItemList.length, this.propertyId);
        });
    }
  }

  public goToReservationBudget() {
    this.toggleModal.emitToggleWithRef(this.modalId);
    this.reservationService.goToReservationBudget(this.reservation.id, this.propertyId);
  }

  private goToNewReservation() {
    this.toggleModal.emitToggleWithRef(this.modalId);
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    this.reservationService.goToNewReservation(`${this.propertyId}`);
  }

  private goToConsultReservation() {
    this.toggleModal.emitToggleWithRef(this.modalId);
    this.reservationService.goToReservationList(this.propertyId);
  }

  public sameCurrency() {
    if (this.reservationItemList && this.reservationItemList.length > 0) {
      const currency = this.reservationItemList[0].currencyId;
      const otherCurrencies = this.reservationItemList.filter( item => item.currencyId != currency);
      return !(otherCurrencies.length > 0);
    }
    return true;
  }

  public goToShareRoomList() {
    this.shareRoomListEmail = this.reservation.contactEmail;
    this.toggleModal.emitCloseWithRef(this.modalId);
    this.toggleModal.emitOpenWithRef(this.MODAL_ROOM_LIST);
  }

  public shareRoomlist() {
    this.roomlistResource.shareRoomList(this.reservation.reservationUid, this.shareRoomListEmail ).subscribe( () => {
      this.toasterEmitService.emitChange(SuccessError.success, 'label.roomListSharedSuccess');
      this.cancelShareRoomlist();
    });
  }

  public cancelShareRoomlist() {
    this.toggleModal.emitOpenWithRef(this.modalId);
    this.toggleModal.emitCloseWithRef(this.MODAL_ROOM_LIST);
  }

  public emailChange() {
    this.buttonShareRoomListConfig.isDisabled = !this.shareRoomListEmail;
    this.buttonShareRoomListConfig = {...this.buttonShareRoomListConfig};
  }

  public canShowShareRoomList() {
    return this.reservation.reservationItemViewList.length > 1;
  }

  public issueProforma() {
    this.toggleModal.closeAllModals();
    this.modalEmitService.emitSimpleModal(
      this.translateService.instant(`label.emitProForma`),
      this.translateService.instant('text.tourismTaxInProforma'),
      () => this.emitProforma(true), null, () => this.emitProforma()  );
  }

  public emitProforma(withTourism: boolean = false) {
    const proforma = new Proforma();
    proforma.reservationId = this.reservation.id;
    proforma.launchTourismTax = withTourism;
    this.proFormaResource.issueProforma(proforma)
      .subscribe(response => {
        this.printProforma(proforma);
        this.router.navigate(['../'], { relativeTo: this.route });
      });
  }

  public showIssueProformaButton() {
    return this.propertyCountryCode == CountryCodeEnum.PT;
  }

  private printProforma(proforma: Proforma) {
    this
      .fiscalDocumentService
      .getProforma(proforma)
      .subscribe(
      (byteCodes: any) => {
        if ( byteCodes ) {
          this.pdfPrintService.printPdfFromByteCode(byteCodes);
        }
      }, () => {
        this.showMessagePrintInvoiceError();
      });
  }

  private showMessagePrintInvoiceError() {
    this
      .toasterEmitService
      .emitChange(
        SuccessError.error,
        this.translateService.instant('alert.errorPrintInvoice')
      );
  }
}
