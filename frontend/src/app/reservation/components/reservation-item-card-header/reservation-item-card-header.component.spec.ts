// import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// // import { FormBuilder } from '@angular/forms';
// import { TranslateService } from '@ngx-translate/core';
// import { Observable } from 'rxjs';
// import { HttpClient, HttpHandler } from '@angular/common/http';
// import { RouterTestingModule } from '@angular/router/testing';
// import { TranslateModule } from '@ngx-translate/core';
// import { SharedModule } from '../../../shared/shared.module';
// import { MomentModule } from 'ngx-moment';

// // Components
// import { ReservationItemCardHeaderComponent } from './reservation-item-card-header.component';

// // Services
// import { ReservationItemViewService } from '../../../shared/services/reservation-item-view/reservation-item-view.service';
// import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
// import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
// import { ModalEmitService } from 'app/shared/services/shared/modal-emit.service';
// import { SharedService } from '../../../shared/services/shared/shared.service';

// // Resources
// import { PersonResource } from '../../../shared/resources/person/person.resource';
// import { ReservationItemResource } from '../../../shared/resources/reservation-item/reservation-item.resource';

// // Models
// import { GuestStatus } from '../../../shared/models/reserves/guest';
// import { Guest } from '../../../shared/models/reserves/guest';
// import { BedType } from '../../../shared/models/bed-type';
// import { RoomType } from '../../../shared/models/room-type/room-type';
// import { Room } from '../../../shared/models/room';
// import { ReservationItemView, ReservationItemViewStatusEnum } from '../../../shared/models/reserves/reservation-item-view';
// import { ReservationStatusEnum } from '../../../shared/models/reserves/reservation-status-enum';
// import { ReservationItemViewData } from '../../../shared/mock-data/reservationItem-data';
// import { SuccessError } from '../../../shared/models/success-error-enum';
// import { ReservationItemDtoData } from 'app/shared/mock-data/reservation-dto-data';

// describe('ReservationItemHeaderCardComponent', () => {
//   let component: ReservationItemCardHeaderComponent;
//   let fixture: ComponentFixture<ReservationItemCardHeaderComponent>;
//   const roomTypeMock = new RoomType();
//   const roomTypeMock2 = new RoomType();
//   const formBuilder = new FormBuilder();
//   let bedTypeCrib;
//   let bedTypeSingle;
//   const guest = new Guest();
//   const guestList = [];
//   const roomMock2 = new Room();
//   const roomMock = new Room();

//   function setGuest() {
//     guest.status = GuestStatus.NoGuest;
//     guest.formGuest = formBuilder.group({
//       guestName: ['']
//     });
//     guestList.push(guest);
//   }

//   function setRoom() {
//     roomMock.roomNumber = '101';
//     roomMock2.roomNumber = '209';
//   }

//   function setBedType() {
//     bedTypeCrib = new BedType();
//     bedTypeCrib.count = 0;
//     bedTypeCrib.type = 5;
//     bedTypeCrib.optionalLimit = 2;

//     bedTypeSingle = new BedType();
//     bedTypeSingle.count = 2;
//     bedTypeSingle.type = 1;
//     bedTypeSingle.optionalLimit = 1;
//   }

//   function setRoomType() {
//     roomTypeMock.id = 1;
//     roomTypeMock.name = 'Quarto Standard';
//     roomTypeMock.abbreviation = 'SPDC';
//     roomTypeMock.order = 1;
//     roomTypeMock.adultCapacity = 2;
//     roomTypeMock.childCapacity = 1;
//     roomTypeMock.roomTypeBedTypeList = new Array<BedType>();
//     roomTypeMock.roomTypeBedTypeList.push(bedTypeSingle);
//     roomTypeMock.roomTypeBedTypeList.push(bedTypeCrib);
//     roomTypeMock.isActive = true;
//     roomTypeMock.isInUse = false;
//     roomTypeMock.roomList = new Array<Room>();
//     roomTypeMock.roomList.push(roomMock);

//     roomTypeMock2.id = 2;
//     roomTypeMock2.name = 'Quarto Luxo';
//     roomTypeMock2.abbreviation = 'LXO';
//     roomTypeMock2.order = 1;
//     roomTypeMock2.adultCapacity = 3;
//     roomTypeMock2.childCapacity = 0;
//     roomTypeMock2.roomTypeBedTypeList = new Array<BedType>();
//     roomTypeMock2.roomTypeBedTypeList.push(bedTypeSingle);
//     roomTypeMock2.roomTypeBedTypeList.push(bedTypeCrib);
//     roomTypeMock2.isActive = true;
//     roomTypeMock2.isInUse = false;
//     roomTypeMock2.roomList = new Array<Room>();
//     roomTypeMock2.roomList.push(roomMock2);
//   }

//   function setreservationItem() {
//     setGuest();
//     setRoom();
//     setBedType();
//     setRoomType();

//     const accomodationCardForm = formBuilder.group({
//       periodCard: [''],
//       adultQuantityCard: [1],
//       childrenQuantityCard: [0],
//       childrenAgeCard: [''],
//       roomTypeCard: [''],
//       checkinHourCard: [''],
//       checkoutHourCard: [''],
//       roomNumberCard: [''],
//       roomLayoutCard: [''],
//       extraBedQuantityCard: [''],
//       cribBedQuantityCard: [''],
//       hasCribBedTypeCard: [false],
//       hasExtraBedTypeCard: [false]
//     });

//     component.reservationItemView = new ReservationItemView();
//     component.reservationItemView.code = '';
//     component.reservationItemView.room = new Room();
//     component.reservationItemView.period = '12/01/2017 - 14/01/2017';
//     component.reservationItemView.adultQuantity = 2;
//     component.reservationItemView.childrenQuantity = 1;
//     component.reservationItemView.status = ReservationStatusEnum.ToConfirm;
//     component.reservationItemView.guestQuantity = guestList;
//     component.reservationItemView.receivedRoomType = new RoomType();
//     component.reservationItemView.receivedRoomType.id = 1;
//     component.reservationItemView.receivedRoomType.name = 'Quarto Standard';
//     component.reservationItemView.receivedRoomType.abbreviation = 'SPDC';
//     component.reservationItemView.receivedRoomType.order = 1;
//     component.reservationItemView.receivedRoomType.adultCapacity = 2;
//     component.reservationItemView.receivedRoomType.childCapacity = 1;
//     component.reservationItemView.receivedRoomType.roomTypeBedTypeList = new Array<BedType>();
//     component.reservationItemView.receivedRoomType.roomTypeBedTypeList.push(bedTypeSingle);
//     component.reservationItemView.receivedRoomType.roomTypeBedTypeList.push(bedTypeCrib);
//     component.reservationItemView.receivedRoomType.isActive = true;
//     component.reservationItemView.receivedRoomType.isInUse = false;
//     component.reservationItemView.receivedRoomType.roomList = new Array<Room>();
//     component.reservationItemView.receivedRoomType.roomList.push(roomMock2);
//     component.reservationItemView.roomTypes = new Array<RoomType>();
//     component.reservationItemView.roomTypes.push(roomTypeMock);
//     component.reservationItemView.roomTypes.push(roomTypeMock2);
//     component.reservationItemView.dailyNumber = 3;
//     component.reservationItemView.dailyPrice = 1200.00;
//     component.reservationItemView.totalDaily = component.reservationItemView.dailyPrice * component.reservationItemView.dailyNumber;
//     component.reservationItemView.total = 3;
//     component.reservationItemView.room.roomNumber = 'ABCDEFG';
//     component.reservationItemView.focus = false;
//     component.reservationItemView.schedulePopoverIsVisible = false;
//     component.reservationItemView.formCard = accomodationCardForm;
//     component.reservationItemView.formCard.controls.roomTypeCard.setValue(component.reservationItemView.receivedRoomType);
//   }

//   beforeAll(() => {
//     TestBed.resetTestEnvironment();
//     TestBed
//       .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
//       .configureTestingModule({
//         imports: [
//           TranslateModule.forRoot(),
//           RouterTestingModule,
//           FormsModule,
//
//           ReactiveFormsModule,
//           MomentModule
//         ],
//         declarations: [
//           ReservationItemCardHeaderComponent
//         ],
//         providers: [],
//         schemas: [
//           CUSTOM_ELEMENTS_SCHEMA
//         ]
//       });
//     fixture = TestBed.createComponent(ReservationItemCardHeaderComponent);
//     component = fixture.componentInstance;

//     setreservationItem();
//     spyOn(component['deleteReservationItem'], 'emit').and.callThrough();

//     fixture.detectChanges();
//   });

//   describe('on init', () => {
//     it('should set vars', () => {
//       spyOn(component['reservationItemViewService'], 'canCancelStatusOfReservationItem').and.returnValue(false);
//       spyOn(component['reservationItemViewService'], 'canReactivateStatusOfReservationItem').and.returnValue(false);

//       expect(component.currencyCountry).toEqual(window.navigator.language);
//       expect(component.itemsOption).toEqual([
//         { title: component.translateService.instant('commomData.delete'), callbackFunction: component['deleteReservationItemByCode'] }
//       ]);
//     });

//     it('should set itemsOption when can reactivate reservationItem', () => {
//       spyOn(component['reservationItemViewService'], 'canCancelStatusOfReservationItem').and.returnValue(false);
//       spyOn(component['reservationItemViewService'], 'canReactivateStatusOfReservationItem').and.returnValue(true);

//       component['setVars']();

//       expect(component.itemsOption).toEqual([
//         { title: component.translateService.instant('commomData.reactivate'),
// callbackFunction: component['openModalReactivateReservationItem'] }
//       ]);
//     });

//     it('should set itemsOption when can cancel reservationItem', () => {
//       spyOn(component['reservationItemViewService'], 'canCancelStatusOfReservationItem').and.returnValue(true);
//       spyOn(component['reservationItemViewService'], 'canReactivateStatusOfReservationItem').and.returnValue(false);

//       component['setVars']();

//       expect(component.itemsOption).toEqual([
//         { title: component.translateService.instant('commomData.cancel'), callbackFunction: component['cancelReservationItemByCode'] }
//       ]);
//     });

//   });

//   describe('on emitEventForShowBudgetModal', () => {
//     it('should emit showBudgetModal', () => {
//       spyOn(component.showBudgetModal, 'emit');

//       component.emitEventForShowBudgetModal();

//       expect(component.showBudgetModal.emit).toHaveBeenCalledWith(component.reservationItemView);
//     });
//   });

//   describe('on actions', () => {
//     it('should cancelReservationItemByCode in runCallbackFunction', () => {
//       const result = { title: 'Excluir', callbackFunction: component['cancelReservationItemByCode'] };
//       spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
//       spyOn(component['reservationItemViewService'], 'canCancelStatusOfReservationItem').and.returnValue(true);

//       component.runCallbackFunction(result);

//       expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.reservationItemView.code);
//     });

//     it('should cancelReservationItemByCode in runCallbackFunction and calls deleteReservationItemView', () => {
//       const result = { title: 'Excluir', callbackFunction: component['cancelReservationItemByCode'] };
//       spyOn(component, 'deleteReservationItemView');
//       spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
//       spyOn(component['reservationItemViewService'], 'canCancelStatusOfReservationItem').and.returnValue(false);

//       component.runCallbackFunction(result);

//       expect(component['modalEmitToggleService'].emitToggleWithRef).not.toHaveBeenCalledWith(component.reservationItemView.code);
//       expect(component.deleteReservationItemView).toHaveBeenCalledWith(component.reservationItemView);
//     });

//     it('should deleteReservationItemByCode in runCallbackFunction', () => {
//       const result = { title: 'Excluir', callbackFunction: component['deleteReservationItemByCode'] };
//       spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
//       spyOn(component['reservationItemViewService'], 'canCancelStatusOfReservationItem').and.returnValue(false);

//       component.runCallbackFunction(result);

//       expect(component['modalEmitToggleService'].emitToggleWithRef).not.toHaveBeenCalledWith(component.reservationItemView.code);
//       expect(component['deleteReservationItem'].emit).toHaveBeenCalledWith(component.reservationItemView);
//     });

//     it('should deleteReservationItemView canReactivateStatusOfReservationItem', () => {
//       const reservationItemView = ReservationItemViewData;
//       spyOn(component['reservationItemViewService'], 'canReactivateStatusOfReservationItem').and.returnValue(true);

//       component.deleteReservationItemView(reservationItemView);

//       expect(component['deleteReservationItem'].emit).toHaveBeenCalledWith(reservationItemView);
//       expect(component.itemsOption).toEqual([
//         { title: component.translateService.instant('commomData.reactivate'),
// callbackFunction: component['openModalReactivateReservationItem'] }
//       ]);
//     });

//     it('should deleteReservationItemView canCancelStatusOfReservationItem', () => {
//       const reservationItemView = ReservationItemViewData;
//       spyOn(component, 'deleteReservationItemView');
//       spyOn(component['reservationItemViewService'], 'canCancelStatusOfReservationItem').and.returnValue(true);

//       component.deleteReservationItemView(reservationItemView);

//       expect(component.deleteReservationItemView).toHaveBeenCalledWith(reservationItemView);
//     });
//   });

//   describe('on reactivateReservationItem', () => {
//     it('should openModalReactivateReservationItem', () => {
//       spyOn(component['modalEmitService'], 'emitSimpleModal');

//       component['openModalReactivateReservationItem']();

//       expect(component['modalEmitService'].emitSimpleModal).toHaveBeenCalledWith('commomData.warning',
// 'reservationModule.commomData.confirmRactivateReservationItem', component['reactivateReservationItem'], []);
//     });

//     it('should reactivateReservationItem and setReservationItemStatusToConfirm with success', () => {
//       spyOn(component['reservationItemResource'], 'reactivateReservationItem').and.returnValue(Observable.of({}));
//       spyOn(component['toasterEmitService'], 'emitChange');
//       spyOn(component.reactivateReservationItemEvent, 'emit');
//       spyOn(component, 'setLabelActionAndItemsOptions');

//       const reasonId = 2;
//       const cancellationDescription = 'Motivo do Cancelamento';
//       component.reservationItemView.id = 1;

//       component['reactivateReservationItem']();

//       expect(component['reservationItemResource'].reactivateReservationItem).toHaveBeenCalledWith(component.reservationItemView.id);
//       expect(component.reactivateReservationItemEvent.emit).toHaveBeenCalledWith(component.reservationItemView);
//       expect(component['setLabelActionAndItemsOptions']).toHaveBeenCalled();
//       expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(SuccessError.success,
// 'reservationModule.commomData.reactivateOfReservationItemDoneWithSuccess');
//       expect(component.reservationItemView.status).toEqual(ReservationStatusEnum.ToConfirm);
//       expect(component.reservationItemView.isCanceledStatus).toBeFalsy();
//       expect(component.reservationItemView.focus).toBeTruthy();
//     });

//     it('should reactivateReservationItem with error', () => {
//       spyOn(component['reservationItemResource'], 'reactivateReservationItem').and.returnValue(Observable.throw({}));
//       spyOn(component['toasterEmitService'], 'emitChange');
//       spyOn(component.reactivateReservationItemEvent, 'emit');
//       spyOn(component, 'setLabelActionAndItemsOptions');
//       spyOn(component, 'setReservationItemStatusToConfirm');

//       const reasonId = 2;
//       const cancellationDescription = 'Motivo do Cancelamento';
//       component.reservationItemView.id = 1;

//       component['reactivateReservationItem']();

//       expect(component['reservationItemResource'].reactivateReservationItem).toHaveBeenCalledWith(component.reservationItemView.id);
//       expect(component.reactivateReservationItemEvent.emit).not.toHaveBeenCalledWith(component.reservationItemView);
//       expect(component['setLabelActionAndItemsOptions']).not.toHaveBeenCalled();
//       expect(component['setReservationItemStatusToConfirm']).not.toHaveBeenCalled();
//       expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(SuccessError.error, 'commomData.errorMessage');
//     });
//   });
// });
