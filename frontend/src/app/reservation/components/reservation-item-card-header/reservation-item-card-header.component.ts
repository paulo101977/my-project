import { Component, DoCheck, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ReservationItemViewService } from '../../../shared/services/reservation-item-view/reservation-item-view.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ReservationItemResource } from '../../../shared/resources/reservation-item/reservation-item.resource';
import { ReservationStatusEnum } from '../../../shared/models/reserves/reservation-status-enum';
import { ReservationItemView } from '../../../shared/models/reserves/reservation-item-view';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ReservationBudgetService } from '../../../shared/services/reservation-budget/reservation-budget.service';
import { SessionParameterService } from '../../../shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from '../../../shared/models/parameter/pameterType.enum';
import { ActivatedRoute } from '@angular/router';
import { DateService } from '../../../shared/services/shared/date.service';
import * as _ from 'lodash';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'reservation-item-card-header',
  templateUrl: 'reservation-item-card-header.component.html',
  styleUrls: ['reservation-item-card-header.component.css']
})

export class ReservationItemCardHeaderComponent implements OnInit, DoCheck {
  public itemsOption: Array<any>;
  private propertyId: number;
  public budgetedAmount: number;
  public currencySymbol: string;
  public commercialBudgetName: string;
  public reservationItemViewInfo: any;

  @Input() reservationItemView: ReservationItemView;
  @Input() index: number;
  @Input() isCanceledStatus: boolean;
  @Input() readonly: boolean;

  @Output() showBudgetModal = new EventEmitter<ReservationItemView>();
  @Output() deleteReservationItem = new EventEmitter<ReservationItemView>();
  @Output() reactivateReservationItemEvent = new EventEmitter<ReservationItemView>();
  @Output() sendBudgetForReservationItemCard = new EventEmitter();
  @Output() callBudgetModalOnCard = new EventEmitter();

  constructor(
    public translateService: TranslateService,
    private modalEmitToggleService: ModalEmitToggleService,
    private reservationItemViewService: ReservationItemViewService,
    public _route: ActivatedRoute,
    private modalEmitService: ModalEmitService,
    private reservationItemResource: ReservationItemResource,
    private reservationBudgetService: ReservationBudgetService,
    private toasterEmitService: ToasterEmitService,
    private sessionParameter: SessionParameterService,
    public dateService: DateService,
    private propertyService: PropertyStorageService
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  ngDoCheck() {
   this.setReservationItemView();
  }

  private setVars() {
    this.propertyId = this.propertyService.getCurrentPropertyId();
    this.setReservationItemView();
    this.setLabelActionAndItemsOptions();
  }

  private setReservationItemView() {
    const {
      roomTypeCard,
      periodCard,
      adultQuantityCard,
      childrenQuantityCard
    } = this.reservationItemView.formCard.getRawValue();

    const {
      guestName
    } = this.reservationItemView.guestQuantity[0].formGuest.getRawValue();

    this.reservationItemViewInfo = {
      roomTypeCard,
      periodCard,
      adultQuantityCard,
      childrenQuantityCard,
      guestName
    };

    this.budgetedAmount = 0;
    if (this.reservationItemView && (this.reservationItemView.gratuityTypeId || this.reservationItemView.budgetGratuityTypeId)) {
      const parameter = this.sessionParameter
        .extractSelectedOptionOfPossibleValues(
          this.sessionParameter.getParameter(this.propertyId, ParameterTypeEnum.DefaultCurrency));
      if (parameter) {
        this.currencySymbol = parameter.propertyParameterValue;
      }

      this.commercialBudgetName = this.translateService.instant('text.gratuitous');
      this.budgetedAmount = 0;
    } else {
      this.currencySymbol = this.reservationItemView.reservationItemCommercialBudgetConfig.reservationBudgetTableList[0].currencySymbol;
      this.commercialBudgetName =
        this.reservationItemView.reservationItemCommercialBudgetConfig.reservationBudgetTableList[0].commercialBudgetName
        || this.reservationItemView.reservationItemCommercialBudgetConfig.reservationBudgetTableList[0].id
          ? this.translateService.instant('label.comercialDeal')
          : this.translateService.instant('text.gratuitous');
      this.reservationItemView.budgetReservationBudgetList
        .forEach(reservationBudget => {
          this.budgetedAmount += reservationBudget.manualRate;
        });
    }
  }

  private setLabelActionAndItemsOptions(): void {
    if (!this.readonly) {
      let labelAction = this.translateService.instant('commomData.delete');
      let callbackFunction = this.deleteReservationItemByCode;

      if (this.reservationItemViewService.canCancelStatusOfReservationItem(this.reservationItemView)) {
        labelAction = this.translateService.instant('commomData.cancel');
        callbackFunction = this.cancelReservationItemByCode;
      } else if (this.reservationItemViewService.canReactivateStatusOfReservationItem(this.reservationItemView)) {
        labelAction = this.translateService.instant('commomData.reactivate');
        callbackFunction = this.openModalReactivateReservationItem;
      }

      this.itemsOption = [
        { title: labelAction, callbackFunction: callbackFunction },
      ];
    }
  }

  public deleteReservationItemView(reservationItemViewCanceled: ReservationItemView) {
    if (!this.readonly) {
      this.deleteReservationItem.emit(reservationItemViewCanceled);
      this.setLabelActionAndItemsOptions();
    }
  }

  private deleteReservationItemByCode = (): void => {
    this.deleteReservationItemView(this.reservationItemView);
  }

  private cancelReservationItemByCode = (): void => {
    if (this.reservationItemViewService.canCancelStatusOfReservationItem(this.reservationItemView)) {
      this.modalEmitToggleService.emitToggleWithRef(this.reservationItemView.code);
    } else {
      this.deleteReservationItemView(this.reservationItemView);
    }
  }

  private openModalReactivateReservationItem = (): void => {
    this.modalEmitService.emitSimpleModal(
      this.translateService.instant('commomData.warning'),
      this.translateService.instant('reservationModule.commomData.confirmRactivateReservationItem'),
      this.reactivateReservationItem,
      []
    );
  }

  private reactivateReservationItem = () => {
    if (!this.readonly) {
      this.reservationItemResource.reactivateReservationItem(this.reservationItemView.id)
        .subscribe(
          response => {
            this.toasterEmitService.emitChange(
              SuccessError.success,
              this.translateService
                .instant('reservationModule.commomData.reactivateOfReservationItemDoneWithSuccess')
            );
            this.setReservationItemStatusToConfirm();
            this.setLabelActionAndItemsOptions();
            this.reactivateReservationItemEvent.emit(this.reservationItemView);
          },
          error => {
            this.toasterEmitService
              .emitChange(SuccessError.error, this.translateService.instant('commomData.errorMessage'));
          }
        );
    }
  }

  private setReservationItemStatusToConfirm() {
    this.reservationItemView.status = ReservationStatusEnum.ToConfirm;
    this.reservationItemView.isCanceledStatus = false;
    this.reservationItemView.focus = true;
  }

  public emitEventForShowBudgetModal(reservationItemView: any) {
    if (!this.readonly) {
      const reservationItem = _.cloneDeep(reservationItemView);
      this.callBudgetModalOnCard.emit(reservationItem);
    }
  }

  public runCallbackFunction(item: any) {
    if (!this.readonly && item.callbackFunction) {
      item.callbackFunction();
    }
  }

  public getAmmount() {
    if ( this.reservationItemView && (this.reservationItemView.gratuityTypeId || this.reservationItemView.budgetGratuityTypeId ) ) {
      return 0;
    }
    return this.reservationItemView.budgetReservationBudgetList.reduce( (val, current) => val += current.manualRate , 0);
  }
}
