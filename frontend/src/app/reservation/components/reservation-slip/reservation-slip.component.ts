import { ValidatorFormService } from './../../../shared/services/shared/validator-form.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { RightButtonTop } from '../../../shared/models/right-button-top';
import { ButtonConfig, ButtonType, ButtonSize } from '../../../shared/models/button-config';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ActivatedRoute } from '@angular/router';
import { ReservationItemResource } from 'app/shared/resources/reservation-item/reservation-item.resource';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { AssetsService } from '@app/core/services/assets.service';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-reservation-slip',
  templateUrl: './reservation-slip.component.html',
  styleUrls: ['./reservation-slip.component.css'],
})
export class ReservationSlipComponent implements OnInit {
  @ViewChild('slipContainer') slipContainer: ElementRef;

  private propertyId: number;
  private reservationId: number;
  public formSlipModal: FormGroup;
  public configHeader: ConfigHeaderPageNew;
  public topButtons: Array<RightButtonTop>;
  private splitHtmlResponse: string;
  public readonly RESERVATION_SLIP_MODAL_ID = 'reservatio_slip_modal';

  // Buttons config
  public cancelButton: ButtonConfig;
  public openModalSendSlipButton: ButtonConfig;
  public printButton: ButtonConfig;
  public cancelSendSlipButton: ButtonConfig;
  public sendSlipButton: ButtonConfig;

  constructor(
    private sharedService: SharedService,
    private router: ActivatedRoute,
    private toasterEmitService: ToasterEmitService,
    private reservationItemResource: ReservationItemResource,
    private location: Location,
    private printService: PrintHelperService,
    private modalEmitToggleService: ModalEmitToggleService,
    private formBuilder: FormBuilder,
    private validatorFormService: ValidatorFormService,
    private assetsService: AssetsService,
    private propertyStorage: PropertyStorageService
  ) {}

  ngOnInit() {
    this.propertyId = this.propertyStorage.getCurrentPropertyId();
    this.reservationId = this.router.snapshot.params.reservationId;
    this.setFormSlipModal();
    this.setListeners();
    this.setButtons();
    this.setHeader();
    this.setTopButtons();

    this.renderHtmlSlip();
  }

  private setFormSlipModal() {
    this.formSlipModal = this.formBuilder.group({
      email: ['', [Validators.required, this.validatorFormService.noWhitespace]],
    });
  }

  private setListeners() {
    this.formSlipModal.valueChanges.subscribe(value => {
      this.sendSlipButton.isDisabled = this.formSlipModal.invalid;
    });
  }

  private setHeader() {
    this.configHeader = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  private setTopButtons() {
    this.topButtons = [];
    const buttonPrint = new RightButtonTop();
    buttonPrint.title = 'action.print';
    buttonPrint.iconDefault = this.assetsService.getImgUrlTo('print.svg');
    buttonPrint.callback = this.printSlip.bind(this);

    const buttonSend = new RightButtonTop();
    buttonSend.title = 'label.sendSlip';
    buttonSend.iconDefault = this.assetsService.getImgUrlTo('mail.svg');
    buttonSend.callback = this.openSendSlipModal.bind(this);

    this.topButtons.push(buttonPrint);
    this.topButtons.push(buttonSend);
  }

  private setButtons() {
    this.cancelButton = this.sharedService.getButtonConfig(
      'cancel-slip-page',
      this.cancel.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.printButton = this.sharedService.getButtonConfig(
      'print-slip-page',
      this.printSlip.bind(this),
      'action.print',
      ButtonType.Secondary,
    );
    this.openModalSendSlipButton = this.sharedService.getButtonConfig(
      'open-modal-send-slip',
      this.openSendSlipModal.bind(this),
      'action.send',
      ButtonType.Primary,
    );
    this.cancelSendSlipButton = this.sharedService.getButtonConfig(
      'cancel-slip-page',
      this.toogleSendSlipModal.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.sendSlipButton = this.sharedService.getButtonConfig(
      'send-slip',
      this.sendSlip.bind(this),
      'action.send',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
  }

  private cancel() {
    this.location.back();
  }

  private renderHtmlSlip() {
    this.reservationItemResource.getReservationSlipByPropertyIdAndReservationId(this.propertyId, this.reservationId).subscribe(text => {
      this.splitHtmlResponse = text;
      this.slipContainer.nativeElement.insertAdjacentHTML(
        'beforeend',
        '<div style="display: flex;justify-content: center;">' + this.splitHtmlResponse + '</div>',
      );

      // The print layout was 3 pages long with blank page on it
      // to solve this problem on the front-end, it was needed
      // to remove the table cellpadding
      const table = this.slipContainer.nativeElement.querySelector('table');
      table.removeAttribute('cellpadding');
    });
  }

  private openSendSlipModal() {
    this.toogleSendSlipModal();
  }

  private toogleSendSlipModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.RESERVATION_SLIP_MODAL_ID);
  }

  private sendSlip() {
    this.reservationItemResource
      .sendReservationSlip(this.propertyId, this.reservationId, [this.formSlipModal.get('email').value])
      .subscribe(response => {
        this.toasterEmitService.emitChange(SuccessError.success, 'text.emailSent');
        this.toogleSendSlipModal();
      });
  }

  private printSlip() {
    this.printService.printHtml(this.slipContainer.nativeElement.innerHTML);
  }

  public inputIsInvalidAndDirty(control: string): boolean {
    return this.validatorFormService.inputIsInvalidAndDirty(this.formSlipModal, control);
  }
}
