import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ReservationItemView } from 'app/shared/models/reserves/reservation-item-view';
import { SearchReservationDto } from 'app/shared/models/dto/search-reservation-dto';
import {
  ReservationStatusEnum,
  ReservationStatusPreCheckinEnum
} from 'app/shared/models/reserves/reservation-status-enum';
import { SearchReservationResultDto } from 'app/shared/models/dto/search-reservation-result-dto';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ButtonConfig, ButtonType, Type } from 'app/shared/models/button-config';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { DataTableGlobals } from 'app/shared/models/DataTableGlobals';
import { KeyValue } from 'app/shared/models/key-value';
import { KeysPipe } from 'app/shared/pipes/keys.pipe';
import { TranslateService } from '@ngx-translate/core';
import { DateService } from 'app/shared/services/shared/date.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { ReservationResource } from 'app/shared/resources/reservation/reservation.resource';
import { ReservationItemResource } from 'app/shared/resources/reservation-item/reservation-item.resource';
import { MealPlanTypeResource } from 'app/shared/resources/meal-plan-type/meal-plan-type.resource';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { MarketSegmentResource } from 'app/shared/resources/market-segment/market-segment.resource';
import { OriginResource } from 'app/shared/resources/origin/origin.resource';
import { RatePlanResource } from 'app/shared/resources/rate-plan/rate-plan.resource';
import { RoomTypeResource } from 'app/room-type/shared/services/room-type-resource/room-type.resource';
import { AutocompleteComponent } from 'app/shared/components/autocomplete/autocomplete.component';
import { AutocompleteConfig } from 'app/shared/models/autocomplete/autocomplete-config';
import { Client } from 'app/shared/models/client';
import { ClientResource } from 'app/shared/resources/client/client.resource';
import { ReservationSearchDto } from 'app/shared/models/dto/reservation-search-dto';
import { Subscription } from 'rxjs';
import { RoomType } from 'app/room-type/shared/models/room-type';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ModalRemarksFormEmitService } from 'app/shared/services/shared/modal-remarks-form-emit.service';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import { toSelectOption } from '@inovacao-cmnet/thx-ui';
import { CheckInRoomTypesDto } from 'app/shared/models/dto/checkin/checkin-room-types-dto';

export enum ButtomBarEnum {
  CHECKINTODAY,
  CHECKOUTTODAY,
  DEADLINE,
  GUESTINHOUSE,
  PRECHECKIN
}

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.scss']
})
export class ReservationListComponent implements OnInit, OnDestroy {
  @ViewChild('table') table: any;

  public readonly CANCEL_CHECKIN_MODAL_ID = 'cancel-checkin';
  public readonly REACTIVATE_NO_SHOW_MODAL_ID = 'reactivate-no-show-modal';
  public itemsTab: KeyValue[];
  public propertyId: number;
  public configHeaderPage: ConfigHeaderPageNew;
  private sub: Subscription;
    // Table dependencies
  public reservationItemsList: Array<ReservationSearchDto>;
  public reservationObj: SearchReservationResultDto;
  public selected: SearchReservationResultDto[] = [];
  public totalGuests: number;
  public totalReservations: number;

  // ButtonBar dependencies
  public checkinButtonIsActive: boolean;
  public checkoutButtonIsActive: boolean;
  public deadlineButtonIsActive: boolean;
  public guestInHouseButtonIsActive: boolean;
  public preCheckInIsActive: boolean;
  public buttomBar = ButtomBarEnum;
  // Advanced Search
  public hasAdvancedSearch: boolean;
  public optionsReservationStatus: Array<any>;
  public searchReservationDto: SearchReservationDto;
  public formSearchReservation: FormGroup;
  // Elastic Search
  public elasticSearchFilter: string;
  // Button dependencies
  public buttonResetFormSearchReservationConfig: ButtonConfig;
  public buttonListCustomSearchReservationsConfig: ButtonConfig;
  public buttonCancelConfirmConfig: ButtonConfig;
  public buttonConfirmConfirmConfig: ButtonConfig;

  public buttonCancelCheckinCancelConfig: ButtonConfig;
  public buttonCancelCheckinConfirmConfig: ButtonConfig;

  public buttonCancelReactivateConfig: ButtonConfig;
  public buttonConfirmReactivateConfig: ButtonConfig;

  public buttonCancelReactivateNoShowConfig: ButtonConfig;
  public buttonConfirmReactivateNoShowConfig: ButtonConfig;
  // Reservation actions
  public reservationItemViewCanceled: ReservationItemView;
  public searchReservationResultDtoCanceled: SearchReservationResultDto;
  public reservationToConfirm: SearchReservationResultDto;
  public reservationItemToReactivate: SearchReservationResultDto;
  public noShowToReactivate: SearchReservationResultDto;

  public mobile: boolean;

  public cancelCheckinInfo: any;
  public readonly columnMode = ColumnMode.flex;

  // Select dependencies
  public mealPlanTypeOptions: SelectObjectOption[];
  public marketSegmentOptions: SelectObjectOption[];
  public businessSourceOptions: SelectObjectOption[];
  public ratePlanOptions: SelectObjectOption[];
  public roomTypeOptions: SelectObjectOption[];
  public roomTypeList: RoomType[];
  public selectedItem = [];
  public associatedRoomList = [];
  private propertySubscription: Subscription;

  public idSelected: any;
  // Autcomplete dependencies
  @ViewChild('autoCompleteCompanyClient') autoCompleteCompanyClient: AutocompleteComponent;
  public autoCompleteConfig: AutocompleteConfig;
  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public translateService: TranslateService,
    private reservationResource: ReservationResource,
    private reservationItemResource: ReservationItemResource,
    private formBuilder: FormBuilder,
    private dateService: DateService,
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private toasterEmitService: ToasterEmitService,
    public dataTableGlobals: DataTableGlobals,
    private commonService: CommomService,
    private mealPlanTypeResource: MealPlanTypeResource,
    private marketSegmentResource: MarketSegmentResource,
    private originResource: OriginResource,
    private ratePlanResource: RatePlanResource,
    private roomTypeResource: RoomTypeResource,
    private clientResource: ClientResource,
    private propertyService: PropertyStorageService,
    private reservationService: ReservationService,
    private modalRemarksFormEmitService: ModalRemarksFormEmitService,
    private checkinResource: CheckinResource,
  ) {
    this.propertySubscription = this.propertyService.currentPropertyChangeEmitted$.subscribe( () => {
      this.propertyId = this.propertyService.getCurrentPropertyId();
      this.getMealPlanTypeList();
      this.getMarketSegmentList();
      this.getBusinessSourceList();
      this.getRatePlanList();
      this.getRoomTypeList();
      const params = this.route.snapshot.queryParams;
      if ( params && (params.filter || params.hasAdvancedSearch)) {
        this.loadReservationsRegistered();
      }
    });
  }

  public ngOnInit(): void {
    this.propertyId = this.propertyService.getCurrentPropertyId();
    this.setConfigHeaderPage();
    this.setButtonConfig();
    this.setFormSearchReservation();
    this.loadOptionsStatus();
    this.reservationItemViewCanceled = new ReservationItemView();
    this.getMealPlanTypeList();
    this.getMarketSegmentList();
    this.getBusinessSourceList();
    this.getRatePlanList();
    this.getRoomTypeList();
    this.setAutoCompleteConfig();


    this.sub = this.route.queryParams
      .subscribe(params => {
        if (params['filter'] && +params['filter'] !== ButtomBarEnum.PRECHECKIN) {
          this.setReservation();
          this.setButtomBarConfigReservation(+params['filter']);
          this.loadReservationsRegistered();
        }
        if (params['hasAdvancedSearch']) {
          this.getHasAdvancedSearch(params);
          this.loadReservationsRegistered();
        }
      });

    this.itemsTab = new Array<KeyValue>();
    this.itemsTab.push(
      this
        .sharedService
        .getKeyValue(
          this.translateService.instant('reservationModule.reserveList.filterSearch.defaultSearch')
        ),
    );

    this.checkScreenResolution();
  }

  public getHasAdvancedSearch(params, checkin?: SearchReservationDto) {
    this.hasAdvancedSearch = params['hasAdvancedSearch'];

    this.searchReservationDto = <SearchReservationDto>params;
    this.formSearchReservation.patchValue(params);
    this.setSearchReservationInFormValueDateRange('arrival', params);
    this.setSearchReservationInFormValueDateRange('departure', params);
    this.setSearchReservationInFormValueDateRange('passingBy', params);
    this.setSearchReservationInFormValueDateRange('creation', params);
    this.setSearchReservationInFormValueDateRange('cancellation', params);
    this.setSearchReservationInFormValueDateRange('deadline', params);

    if (this.preCheckInIsActive) {
      if (checkin) {
        this.searchReservationDto = {
          ...this.searchReservationDto,
          ...checkin
        };
      } else {
        this.searchReservationDto['isPrecheckin'] = true;
        this.searchReservationDto['isGuestVision'] = false;
      }
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.propertySubscription.unsubscribe();
  }

  @HostListener('window:resize')
  public checkScreenResolution() {
    this.mobile = window.screen.width <= 1024;
  }

  public setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      keepButtonActive: true,
      callBackFunction: this.showCustomSearch,
      hasBackPreviewPage: true,
    };
  }

  public setFormSearchReservation(): void {
    this.elasticSearchFilter = '';
    this.formSearchReservation = this.formBuilder.group({
      freeTextSearch: '',
      groupName: '',
      arrivalRange: null,
      departureRange: null,
      passingByRange: null,
      creationRange: null,
      cancellationRange: null,
      deadlineRange: null,
      reservationItemStatus: '',
      mealPlanType: '',
      marketSegment: '',
      businessSource: '',
      ratePlan: '',
      roomType: '',
      adults: '',
      children: '',
      companyClientId: '',
      companyClientName: '',
      isMigrated: false,
      isGuest: false,
      isMain: false,
      room: ''
    });

    this.formSearchReservation
      .get('isMain')
      .valueChanges
      .subscribe( () => {
        if (this.searchReservationDto && this.searchReservationDto['isMain']) {
          this.searchReservationDto['isMain'] = this.formSearchReservation.get('isMain').value;
          this.updateUrl(false);
          if (!!this.formSearchReservation.get('isMain').value) {
            this.loadReservationsRegistered();
          }
        }
      });

    this.formSearchReservation
      .get('isGuest')
      .valueChanges
      .subscribe( (item) => {
        if (this.searchReservationDto && this.searchReservationDto.isGuestVision) {
          this.searchReservationDto.isGuestVision = item;
        }
      });
  }

  public loadReservationsRegistered(): void {
    this.reservationObj = null;
    this
      .reservationResource
      .searchReservationsByCriteria(this.propertyId, this.searchReservationDto)
      .subscribe(({items, totalReservations, totalGuests}) => {
        this.reservationItemsList = items;
        this.totalReservations = totalReservations;
        this.totalGuests = totalGuests;
        this.getStatusColor();

        if (this.reservationItemsList && this.reservationItemsList.length > 0) {
          this.configReservationList(this.reservationItemsList);
          this.onDescriptionRow(this.reservationItemsList[0]);
        }
      });
  }

  private configReservationList(reservationItemsList) {
    this.associatedRoomList = [];

    reservationItemsList.forEach( row => {
      row.remarksQuantity = this.reservationService.getRemarksQuantityOfReservation(
          row.externalComments,
          row.internalComments,
          row.partnerComments
      );

      if ( row.roomId && row.roomNumber ) {
        this.associatedRoomList.push(row.roomId);
        row['beforeRoomId'] = row.roomId;
      }

      this.setRoomList(row);
    });
  }

  public updateUrl(isAdvancedSearch?: boolean) {
    let params;
    if (this.searchReservationDto) {
      params = isAdvancedSearch
        ? Object.assign({'hasAdvancedSearch': true}, this.searchReservationDto)
        : Object.assign({'filter': this.mapperToButtomBarEnum()});
    }

    this
      .router
      .routeReuseStrategy
      .shouldReuseRoute = (before: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean => {
        return before.routeConfig === curr.routeConfig;
    };

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: params,
      replaceUrl: true
    });
  }

  public setButtonBarConfig() {
    this.checkinButtonIsActive = false;
    this.checkoutButtonIsActive = false;
    this.deadlineButtonIsActive = false;
    this.guestInHouseButtonIsActive = false;
    this.preCheckInIsActive = false;
  }

  private setButtonConfig() {
    this.buttonResetFormSearchReservationConfig = this.sharedService.getButtonConfig(
      'reservationModule-button-clear',
      this.resetFormSearchReservation,
      'commomData.clean',
      ButtonType.Secondary,
      null,
      null,
      Type.Button
    );
    this.buttonListCustomSearchReservationsConfig = this.sharedService.getButtonConfig(
      'reservationModule-button-search',
      this.listCustomSearchReservations,
      'commomData.search',
      ButtonType.Primary,
    );
    this.buttonCancelConfirmConfig = this.sharedService.getButtonConfig(
      'reservationModule-confirm-cancel-modal',
      this.toggleConfirmModal,
      'commomData.not',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfirmConfig = this.sharedService.getButtonConfig(
      'reservationModule-confirm-confirm-modal',
      this.saveConfirmReservation,
      'commomData.yes',
      ButtonType.Primary,
    );

    this.buttonCancelCheckinCancelConfig = this.sharedService.getButtonConfig(
      'cancel-checkin',
      () => this.cancelCheckinCancel(),
      'commomData.not',
      ButtonType.Secondary
      );

    this.buttonCancelCheckinConfirmConfig = this.sharedService.getButtonConfig(
      'cancel-checkin-confirm',
      () => this.cancelCheckinConfirm(),
      'commomData.yes',
      ButtonType.Primary
    );
    this.buttonCancelReactivateConfig = this.sharedService.getButtonConfig(
      'reservationModule-reactivate-cancel-modal',
      this.toggleReactivateModal,
      'commomData.not',
      ButtonType.Secondary,
    );
    this.buttonConfirmReactivateConfig = this.sharedService.getButtonConfig(
      'reservationModule-reactivate-confirm-modal',
      this.saveReactivateReservationItem,
      'commomData.yes',
      ButtonType.Primary,
    );
    this.buttonCancelReactivateNoShowConfig = this.sharedService.getButtonConfig(
      'reactivate-no-show-cancel-modal',
      () => this.cancelReactivateNoShow(),
      'commomData.not',
      ButtonType.Secondary,
    );
    this.buttonConfirmReactivateNoShowConfig = this.sharedService.getButtonConfig(
      'reactivate-no-show-confirm-modal',
      () => this.confirmReactivateNoShow(),
      'commomData.yes',
      ButtonType.Primary,
    );
  }

  public listReservationsButtomBar(buttom: ButtomBarEnum, event?) {
    this.hasAdvancedSearch = false;
    this.setButtonBarConfig();
    this.resetFormSearchReservation();
    this.setReservation();
    this.setButtomBarConfigReservation(buttom);
    if (event) {
      if (!this.preCheckInIsActive) {
        this.updateUrl(false);
      } else {
        const params = Object.assign({'filter': this.mapperToButtomBarEnum(), 'hasAdvancedSearch': true});
        const chekin = this.searchReservationDto;

        this.getHasAdvancedSearch(params, chekin);
     }
    }
  }

  private setButtomBarConfigReservation(buttom: ButtomBarEnum) {
    this.preCheckInIsActive = false;
    switch (buttom) {
      case ButtomBarEnum.CHECKINTODAY:
        this.checkinButtonIsActive = true;
        this.searchReservationDto.checkInToday = true;
        break;
      case ButtomBarEnum.CHECKOUTTODAY:
        this.checkoutButtonIsActive = true;
        this.searchReservationDto.checkOutToday = true;
        break;
      case ButtomBarEnum.DEADLINE:
        this.deadlineButtonIsActive = true;
        this.searchReservationDto.deadline = true;
        break;
      case ButtomBarEnum.GUESTINHOUSE:
        this.guestInHouseButtonIsActive = true;
        this.searchReservationDto.reservationItemStatus = ReservationStatusEnum.Checkin;
        this.searchReservationDto.guestReservationItemStatusId = ReservationStatusEnum.Checkin;
        break;
      case ButtomBarEnum.PRECHECKIN:
        this.preCheckInIsActive = true;
        this.reservationObj = null;
        this.hasAdvancedSearch = true;
        this.reservationItemsList = null;
        this.optionsReservationStatus = new KeysPipe().transform(ReservationStatusPreCheckinEnum);
        this.translateReservationStatusEnumToView(this.optionsReservationStatus);
        this.searchReservationDto['isPrecheckin'] = true;
        this.searchReservationDto['isGuestVision'] = false;
        break;
      default:
        break;
    }
  }

  private mapperToButtomBarEnum() {
    if (this.checkinButtonIsActive) {
      return ButtomBarEnum.CHECKINTODAY;
    }
    if (this.checkoutButtonIsActive) {
      return ButtomBarEnum.CHECKOUTTODAY;
    }
    if (this.deadlineButtonIsActive) {
      return ButtomBarEnum.DEADLINE;
    }
    if (this.guestInHouseButtonIsActive) {
      return ButtomBarEnum.GUESTINHOUSE;
    }
    if (this.preCheckInIsActive) {
      return ButtomBarEnum.PRECHECKIN;
    }
  }

  public resetFormSearchReservation = () => {
    this.elasticSearchFilter = '';
    this.formSearchReservation.patchValue({
      freeTextSearch: '',
      groupName: '',
      arrivalRange: null,
      departureRange: null,
      passingByRange: null,
      creationRange: null,
      cancellationRange: null,
      deadlineRange: null,
      reservationItemStatus: '',
      mealPlanType: '',
      marketSegment: '',
      businessSource: '',
      ratePlan: '',
      roomType: '',
      adults: '',
      children: '',
      companyClientId: '',
      companyClientName: '',
      isMigrated: false,
      isGuest: false,
      isMain: false,
      room: ''
    });
  }

  public listCustomSearchReservations = () => {
    if (this.preCheckInIsActive) {
      this.loadReservationsRegistered();
    } else {
      this.normalizeFieldsFormToBackend();
      this.updateUrl(true);
    }
  }

  public setReservation() {
    this.searchReservationDto = new SearchReservationDto();
  }

  public normalizeFieldsFormToBackend() {
    this.setReservation();
    this.setFormValueInSearchReservation('freeTextSearch');
    this.setFormValueInSearchReservation('groupName');
    this.setFormValueInSearchReservation('reservationItemStatus');
    this.setFormValueDateRangeInSearchReservation('arrival');
    this.setFormValueDateRangeInSearchReservation('departure');
    this.setFormValueDateRangeInSearchReservation('passingBy');
    this.setFormValueDateRangeInSearchReservation('creation');
    this.setFormValueDateRangeInSearchReservation('cancellation');
    this.setFormValueDateRangeInSearchReservation('deadline');
    this.setFormValueInSearchReservation('mealPlanType');
    this.setFormValueInSearchReservation('marketSegment');
    this.setFormValueInSearchReservation('businessSource');
    this.setFormValueInSearchReservation('ratePlan');
    this.setFormValueInSearchReservation('roomType');
    this.setFormValueInSearchReservation('adults');
    this.setFormValueInSearchReservation('children');
    this.setFormValueInSearchReservation('companyClientId');
    this.setFormValueInSearchReservation('companyClientName');
    this.setFormValueInSearchReservation('isMigrated');
    this.setFormValueInSearchReservation('isMain');
    this.setFormValueInSearchReservation('room');
  }

  private setFormValueInSearchReservation(key: string) {
    if (this.formSearchReservation.get(key).value) {
      this.searchReservationDto[key] = this.formSearchReservation.get(key).value;
    }
  }

  private setFormValueDateRangeInSearchReservation(key: string) {
    if (this.formSearchReservation.get(key + 'Range').value) {
      this.searchReservationDto[key + 'Initial'] = this.formSearchReservation.get(key + 'Range').value['beginDate'];
      this.searchReservationDto[key + 'Final'] = this.formSearchReservation.get(key + 'Range').value['endDate'];
    }
  }

  private setSearchReservationInFormValueDateRange(key: string, searchReservation: any) {
    if (searchReservation.hasOwnProperty(key + 'Initial') && searchReservation.hasOwnProperty(key + 'Final')) {
      const dateObject = {
        beginDate: searchReservation[key + 'Initial'],
        endDate: searchReservation[key + 'Final'],
      };
      this.formSearchReservation.get(key + 'Range').setValue(dateObject);
    }
  }

  public showCustomSearch = () => {
    this.hasAdvancedSearch = !this.hasAdvancedSearch;

    if ( !this.preCheckInIsActive ) {
        this.setButtonBarConfig();
    }

    this.loadOptionsStatus();
  }

  public loadOptionsStatus() {
    this.optionsReservationStatus = new KeysPipe().transform(ReservationStatusEnum);
    this.translateReservationStatusEnumToView(this.optionsReservationStatus);
  }

  public translateReservationStatusEnumToView(reservationStatusEnum: Array<any>) {
    const values = this
      .sharedService
      .tranlasteEnumToView(ReservationStatusEnum, 'reservationModule.reserveList.reservationStatusEnum');
    reservationStatusEnum.forEach((option, index) => {
      option['value'] = values[index];
    });
  }

  public cancelReservationItem(searchReservationResultDto: SearchReservationResultDto): void {
    this.reservationItemViewCanceled = new ReservationItemView();
    this.reservationItemViewCanceled.id = searchReservationResultDto.reservationItemId;
    this.reservationItemViewCanceled.code = searchReservationResultDto.reservationItemCode;
    this.searchReservationResultDtoCanceled = searchReservationResultDto;
    this.modalEmitToggleService.emitToggleWithRef(this.reservationItemViewCanceled.code);
  }

  public updateReservationItemViewList(): void {
    this.reservationItemViewCanceled = null;
    this.loadReservationsRegistered();
  }

  public setFilter(filter: string) {
    this.formSearchReservation.patchValue({
      freeTextSearch: filter,
    });

    this.setButtonBarConfig();
    this.listCustomSearchReservations();
  }

  public listElasticSearchReservations(filter: string) {
    if (typeof filter === 'string') {
      filter.length == 0
        ? this.setFilter('')
        : this.setFilter(filter);
    }
  }

  public confirmReservationModal = (item: SearchReservationResultDto) => {
    this.reservationToConfirm = item;
    this.toggleConfirmModal();
  }

  public reactivateReservationItemModal = (item: SearchReservationResultDto) => {
    this.reservationItemToReactivate = item;
    this.toggleReactivateModal();
  }

  public toggleConfirmModal = () => {
    this.modalEmitToggleService.emitToggleWithRef('confirm-reservation');
  }

  public toggleReactivateModal = () => {
    this.modalEmitToggleService.emitToggleWithRef('reactivate-item-reservation');
  }

  public saveConfirmReservation = item => {
    this
      .reservationItemResource
      .confirm(this.reservationToConfirm.reservationItemId)
      .subscribe(() => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this
            .translateService
            .instant('reservationModule.reservationConfirm.successMessage'),
        );
        this.reservationToConfirm = null;
        this.loadReservationsRegistered();
        this.toggleConfirmModal();
    });
  }

  public saveReactivateReservationItem = () => {
    this
      .reservationItemResource
      .reactivateReservationItem(this.reservationItemToReactivate.reservationItemId)
      .subscribe(() => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this
            .translateService
            .instant('reservationModule.commomData.reactivateOfReservationItemDoneWithSuccess'),
        );
        this.reservationItemToReactivate = null;
        this.loadReservationsRegistered();
        this.toggleReactivateModal();
      });
  }

  /* Select get Funtions */
  private getMealPlanTypeList() {
    this
      .mealPlanTypeResource
      .getAllMealPlanTypeList(this.propertyId)
      .subscribe(response => {
        this.mealPlanTypeOptions = this.commonService
          .toOption(
            response.items,
            'mealPlanTypeId',
            'propertyMealPlanTypeName'
          );
      });
  }

  private getMarketSegmentList() {
    this
      .marketSegmentResource
      .getAllMarketSegmentList(this.propertyId)
      .subscribe(response => {
        this.marketSegmentOptions = this.commonService
          .toOption(
            response.items,
            'id',
            'name'
          );
      });
  }

  private getBusinessSourceList() {
    this
      .originResource
      .getOrigin(this.propertyId)
      .subscribe(response => {
        this.businessSourceOptions = this
          .commonService
          .toOption(
            response,
            'id',
            'description'
          );
      });
  }

  private getRatePlanList() {
    this
      .ratePlanResource
      .getAll(this.propertyId)
      .subscribe(response => {
        this.ratePlanOptions = this.commonService
          .toOption(
            response.items,
            'id',
            'agreementName'
          );
      });
  }

  private getRoomTypeList() {
    this
      .roomTypeResource
      .getRoomTypesByPropertyId(this.propertyId)
      .subscribe(response => {
        this.roomTypeList = response;
        this.roomTypeOptions = this.commonService
          .toOption(
            response,
            'id',
            'name'
          );
      });
  }

  /* Autocomplete */
  private setAutoCompleteConfig() {
    this.autoCompleteConfig = new AutocompleteConfig();
    this.autoCompleteConfig.dataModel = this.formSearchReservation.get('companyClientName').value;
    this.autoCompleteConfig.callbackToSearch = this.searchItemsClient;
    this.autoCompleteConfig.callbackToGetSelectedValue = this.updateItemClient;
    this.autoCompleteConfig.fieldObjectToDisplay = 'tradeName';
    // this.autoCompleteConfig.extraClasses = 'thf-u-font-input-default';
    this.autoCompleteConfig.placeholder = this.translateService.instant('label.placeholder.clientSearch');
  }

  public searchItemsClient = (searchData: any) => {

    this.formSearchReservation.get('companyClientId').setValue('');
    this.formSearchReservation.get('companyClientName').setValue(searchData.query);
    this.clientResource.getClientsBySearchData(this.propertyId, searchData.query)
      .subscribe(response => {
          this.autoCompleteConfig.resultList = [...response];
        }
      );
  }

  public updateItemClient = (item: Client) => {
    this.formSearchReservation.get('companyClientId').setValue(item.id);
    this.formSearchReservation.get('companyClientName').setValue(item.tradeName);
  }

  /* Table Functions */
  public onExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
    this.onDescriptionRow(row);
  }

  public onDescriptionClick(event) {
    if (event.type == 'click') {
      const params = this.route.snapshot.queryParams;
      if (params.filter !== ButtomBarEnum.PRECHECKIN) {
        this.reservationObj = event.row;
      }
      if (event.row.id != this.idSelected) {
        this.idSelected = event.row.id;
        this.selectedItem = [event.row];
      }
    }
  }

  public onDescriptionRow(item) {
    const params = this.route.snapshot.queryParams;
    if (params.filter !== ButtomBarEnum.PRECHECKIN.toString()) {
      this.reservationObj = item;
    }
    if (item.id != this.idSelected) {
      this.idSelected = item.id;
      this.selectedItem = [item];
    }
  }

  public showCancelButton(itemData: SearchReservationResultDto): boolean {
    return this.reservationService.showCancelButton(itemData);
  }

  public showCheckinButton(itemData: SearchReservationResultDto): boolean {
    return this.reservationService.showCheckinButton(itemData);
  }

  public showCancelCheckinButton(itemData: SearchReservationResultDto): boolean {
    return this.reservationService.showCancelCheckinButton(this.propertyId, itemData);
  }

  public showConfirmedButton(itemData: SearchReservationResultDto): boolean {
    return this.reservationService.showConfirmedButton(itemData);
  }

  public showToConfirmButton(itemData: SearchReservationResultDto): boolean {
    return this.reservationService.showToConfirmButton(itemData);
  }

  public showReactivateButton(itemData: SearchReservationResultDto): boolean {
    return this.reservationService.showReactivateButton(itemData);
  }

  public showEditReservationButton(itemData: SearchReservationResultDto): boolean {
    return this.reservationService.showEditReservationButton(itemData);
  }

  public showOrientateExpensesButton(itemData: SearchReservationResultDto): boolean {
    return this.reservationService.showOrientateExpensesButton(itemData);
  }

  public showManageKeyButton(itemData: SearchReservationResultDto): boolean {
    return (
      itemData.reservationItemStatusId == ReservationStatusEnum.ToConfirm ||
      itemData.reservationItemStatusId == ReservationStatusEnum.Confirmed ||
      itemData.reservationItemStatusId == ReservationStatusEnum.Checkin ||
      itemData.reservationItemStatusId == ReservationStatusEnum.Pending ||
      itemData.reservationItemStatusId == ReservationStatusEnum.WaitList
    );
  }

  public goToBudgetPage(id: number) {
    this.reservationService.goToBudgetPage(id, `${this.propertyId}`);
  }

  public goToGuestEntrance(item: any) {
    this.reservationService.goToGuestEntrance(item, `${this.propertyId}`);
  }

  public goToGuestPage(item: any) {
    this.reservationService.goToGuestPage(item, `${this.propertyId}`);
  }

  public showAddGuestButton(itemData: SearchReservationResultDto): boolean {
    return this.reservationService.showAddGuestButton(itemData);
  }

  public showAddGuestEntranceButton(itemData: SearchReservationResultDto): boolean {
    return this.reservationService.showAddGuestEntranceButton(itemData, this.roomTypeList);
  }

  public showViewReservationButton(): boolean {
    return this.reservationService.showViewReservationButton();
  }

  public showGuestFileButton(itemData: SearchReservationResultDto): boolean {
   return this.reservationService.showGuestFileButton(itemData);
  }

  public showBillingAccountButton(billbingAccountId) {
    return this.reservationService.showBillingAccountButton(billbingAccountId);
  }

  public goToBillingAccount(billingAccountId) {
    this.reservationService.goToBillingAccount(billingAccountId, `${this.propertyId}`);
  }

  public goToEditReservationPage(id: number) {
    this.reservationService.goToEditReservationPage(id, `${this.propertyId}`);
  }

  public goToViewReservationPage(id: number) {
    this.reservationService.goToViewReservationPage(id, `${this.propertyId}`);
  }

  public goToEditCheckinPage(id: number, reservationItemCode: string) {
    this.reservationService.goToEditCheckinPage(id, reservationItemCode, `${this.propertyId}`);
  }

  public confirmReservation(item: SearchReservationResultDto): void {
    this.confirmReservationModal(item);
  }

  public goToNewReservation() {
    this.reservationService.goToNewReservation(`${this.propertyId}`);
  }

  public goToSlipPage(item: SearchReservationResultDto) {
    this.reservationService.goToSlipPage(item, `${this.propertyId}`);
  }

  public reactivateReservationItem(item: SearchReservationResultDto) {
    this.reactivateReservationItemModal(item);
  }

  public cancelCheckin(info: SearchReservationResultDto) {
    this.cancelCheckinInfo = {
      guestName: info.guestName,
      reservationCode: info.reservationCode,
      reservationItemId: info.reservationItemId
    };
    this.modalEmitToggleService.emitToggleWithRef(this.CANCEL_CHECKIN_MODAL_ID);
  }

  public cancelCheckinCancel() {
    this.modalEmitToggleService.emitCloseWithRef(this.CANCEL_CHECKIN_MODAL_ID);
  }

  public cancelCheckinConfirm() {
    this.modalEmitToggleService.emitCloseWithRef(this.CANCEL_CHECKIN_MODAL_ID);
    this.reservationItemResource.cancelCheckin(this.cancelCheckinInfo.reservationItemId).subscribe( () => {
      this.toasterEmitService.emitChange(SuccessError.success, 'reservationModule.cancelCheckin.successMessage');
      this.loadReservationsRegistered();
    });
  }

  public showReactivateNoShowButton(itemData: SearchReservationResultDto) {
    return this.reservationService.showReactivateNoShowButton(itemData);
  }

  public reactivateNoShow(itemData: SearchReservationResultDto) {
    this.noShowToReactivate = itemData;
    this.modalEmitToggleService.emitToggleWithRef(this.REACTIVATE_NO_SHOW_MODAL_ID);
  }

  public cancelReactivateNoShow() {
    this.noShowToReactivate = null;
    this.modalEmitToggleService.emitCloseWithRef(this.REACTIVATE_NO_SHOW_MODAL_ID);
  }

  public confirmReactivateNoShow() {
    this.modalEmitToggleService.emitCloseWithRef(this.REACTIVATE_NO_SHOW_MODAL_ID);
    this.reservationItemResource.reactivateNoShow(this.noShowToReactivate.reservationItemId)
      .subscribe(() => {
        this.toasterEmitService
          .emitChange(SuccessError.success, 'reservationModule.reactivateNoShow.successMessage');
        this.noShowToReactivate = null;
        this.loadReservationsRegistered();
      });
  }

  public getDatabaseClass() {
    const { reservationObj, preCheckInIsActive } = this;
    return {
      'col-10': (reservationObj && !preCheckInIsActive),
      'col-12': (!reservationObj || preCheckInIsActive)
    };
  }

  public loadRoomList(row) {
    this
      .roomTypeResource
      .getRoomTypesWithOverBookingByPropertyId(
          this.propertyId,
          row.arrivalDate,
          row.departureDate,
          true
      )
      .subscribe((items) => {
        this.setRoomList(row, items);
        row.hasRoomList = true;
      });

  }


  public changeSelectedRoom(event, row) {
    event = event === '' ? null : event;

    const { associatedRoomList } = this;

    if ( row.hasRoomList ) {
      if ( !associatedRoomList.includes(event) && event ) {
        associatedRoomList.push(event);
      }

      if ( !event ) {
        this.associatedRoomList = associatedRoomList.filter( item => item !== row['beforeRoomId']);
      }

      row['beforeRoomId'] = event;

      this
        .checkinResource
        .associateRoomToReservationItem(
            this.propertyId,
            row.reservationItemId,
            row.receivedRoomTypeId,
            event,
        ).subscribe( () => {
          this.toasterEmitService.emitChange(
              SuccessError.success,
              this.translateService.instant('checkinModule.header.successMessage')
          );

          this.setRoomList(row);
        });
    }
  }

  // TODO: make tests
  private setRoomList(row: ReservationSearchDto, items?: Array<CheckInRoomTypesDto>) {
    const { roomNumber, receivedRoomTypeId, roomId } = row;
    const toDefineStr = this.translateService.instant('label.toDefine');

    // empty
    row['roomList'] = [{
        text: toDefineStr,
        value: '',
    }];

    if ( roomId && roomNumber ) {
      row['roomList'].push({
        text: roomNumber,
        value: roomId,
      });
    }

    if ( items && items.length ) {
      const roomList = this.getRoomList(receivedRoomTypeId, items);

      row['roomList'] = [
        row['roomList'][0],
        ...toSelectOption(roomList, 'roomNumber', 'id')
      ];
    }
  }


  private getRoomList(receivedRoomTypeId: number, items: Array<CheckInRoomTypesDto>) {
    if ( items && items.length ) {
      const roomTypeList = items.find( item => item.id === receivedRoomTypeId);

      if ( roomTypeList ) {
        return roomTypeList['roomList'].filter( item => !this.associatedRoomList.includes(item.id) );
      }

      return [];
    }

    return [];
  }

  public showRemarksModal(row) {
    this.modalRemarksFormEmitService.emitSimpleModal(
        row.internalComments,
        row.externalComments,
        row.partnerComments
    );
  }

  public getStatusColor(): any {
    if (this.reservationItemsList && this.reservationItemsList.length) {
      this.reservationItemsList.map(item => {
        const status = <ReservationStatusEnum>item.reservationItemStatusId;
        item.statusColor = this.reservationService.getStatusColorByReservationStatus(status);
      });
    }
  }
}
