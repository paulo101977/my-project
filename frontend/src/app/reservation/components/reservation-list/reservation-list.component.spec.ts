import { of } from 'rxjs';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MomentModule } from 'ngx-moment';
import { ButtomBarEnum, ReservationListComponent } from './reservation-list.component';
import { SearchReservationDto } from 'app/shared/models/dto/search-reservation-dto';
import { SearchReservationResultDto } from 'app/shared/models/dto/search-reservation-result-dto';
import { searchReservationDtoData } from 'app/shared/mock-data/serch-reservation-dto-data';
import { ReservationResource } from 'app/shared/resources/reservation/reservation.resource';
import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';
import { KeysPipe } from 'app/shared/pipes/keys.pipe';
import { ActivatedRoute } from '@angular/router';
import { CoreModule } from '@app/core/core.module';
import { configureTestSuite } from 'ng-bullet';
import { searchReservationResultDtoData } from 'app/shared/mock-data/serch-reservation-result-dto-data';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { Data } from 'app/reservation/mock-data';
import { ReservationSearchDto } from 'app/shared/models/dto/reservation-search-dto';
import { roomTypeResourceStub } from 'app/room-type/shared/services/room-type-resource/testing';
import { checkinResourceStub } from 'app/shared/resources/checkin/testing';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { CheckInRoomTypesDto } from 'app/shared/models/dto/checkin/checkin-room-types-dto';

fdescribe('ReserveListComponent', () => {
  let component: ReservationListComponent;
  let fixture: ComponentFixture<ReservationListComponent>;
  const formBuilder = new FormBuilder();
  let httpMock: HttpTestingController;
  let resource: ReservationResource;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        MomentModule,
        CoreModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { params: {property: '1'} },
            queryParams: of({filter: '1'}),
          },
        },
        roomTypeResourceStub,
        checkinResourceStub,
        toasterEmitServiceStub,
      ],
      declarations: [ReservationListComponent, CurrencyFormatPipe],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ReservationListComponent);
    component = fixture.componentInstance;

    resource = TestBed.get(ReservationResource);
    httpMock = TestBed.get(HttpTestingController);

    fixture.detectChanges();

    component['route'].snapshot.queryParams = { filter: null };
    spyOn<any>(component, 'configReservationList').and.callThrough();
    spyOn<any>(component, 'loadRoomList').and.callThrough();
    spyOn<any>(component, 'setRoomList').and.callThrough();
  });

  beforeAll( () => {
    spyOn<any>(component['propertyService'], 'getCurrentPropertyId').and.returnValue('1');
  });

  describe('on initials settings', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should init', () => {
      const enumExpected = [
        { key: '0', value: 'ToConfirm' },
        { key: '1', value: 'Confirmed' },
        { key: '2', value: 'Checkin' },
        { key: '3', value: 'Checkout' },
        { key: '4', value: 'Pending' },
        { key: '5', value: 'NoShow' },
        { key: '6', value: 'Canceled' },
        { key: '7', value: 'WaitList' },
        { key: '8', value: 'ReservationProposal' },
      ];

      spyOn(component, 'setConfigHeaderPage');
      spyOn(component, 'setFormSearchReservation');
      spyOn(new KeysPipe(), 'transform').and.returnValue(enumExpected);
      spyOn(component, 'translateReservationStatusEnumToView');

      component.ngOnInit();

      expect(component.setConfigHeaderPage).toHaveBeenCalled();
      expect(component.setFormSearchReservation).toHaveBeenCalled();
      expect(component.optionsReservationStatus).toEqual(enumExpected);
      expect(component.translateReservationStatusEnumToView).toHaveBeenCalledWith(enumExpected);
      expect(component.reservationItemViewCanceled).toBeTruthy();
    });

    it('should config HeaderPage', () => {
      expect(component.configHeaderPage.keepTitleButton).toBeTruthy();
      expect(component.configHeaderPage.keepButtonActive).toBeTruthy();
      expect(component.configHeaderPage.callBackFunction).toEqual(component.showCustomSearch);
      expect(component.configHeaderPage.hasBackPreviewPage).toBeTruthy();
    });

    it('should setForm', () => {
      component.setFormSearchReservation();

      expect(Object.keys(component.formSearchReservation.controls)).toEqual([
        'freeTextSearch',
        'groupName',
        'arrivalRange',
        'departureRange',
        'passingByRange',
        'creationRange',
        'cancellationRange',
        'deadlineRange',
        'reservationItemStatus',
        'mealPlanType',
        'marketSegment',
        'businessSource',
        'ratePlan',
        'roomType',
        'adults',
        'children',
        'companyClientId',
        'companyClientName',
        'isMigrated',
        'isGuest',
        'isMain',
        'room'
      ]);
    });

    it(
      'should set ButtonBar config',
      fakeAsync(() => {
        component.setButtonBarConfig();

        expect(component.checkinButtonIsActive).toBeFalsy();
        expect(component.checkoutButtonIsActive).toBeFalsy();
        expect(component.deadlineButtonIsActive).toBeFalsy();
      }),
    );
  });

  describe('on changes', () => {
    it('should get check in reservations from buttom bar', () => {
      const buttom: ButtomBarEnum = ButtomBarEnum.CHECKINTODAY;
      spyOn(component, 'resetFormSearchReservation');
      spyOn(component, 'setReservation');
      spyOn(component, 'updateUrl');

      component.searchReservationDto = new SearchReservationDto();

      const event = {
        type: 'click'
      };
      component.listReservationsButtomBar(buttom, event);

      expect(component.resetFormSearchReservation).toHaveBeenCalled();
      expect(component.setReservation).toHaveBeenCalled();
      expect(component.updateUrl).toHaveBeenCalledWith(false);
      expect(component.checkinButtonIsActive).toBeTruthy();
      expect(component.searchReservationDto.checkInToday).toBeTruthy();
      expect(component.checkoutButtonIsActive).toBeFalsy();
      expect(component.searchReservationDto.checkOutToday).toBeFalsy();
      expect(component.deadlineButtonIsActive).toBeFalsy();
      expect(component.searchReservationDto.deadline).toBeFalsy();
      expect(component.hasAdvancedSearch).toBeFalsy();
    });

    it('should get check out reservations from buttom bar', () => {
      const buttom: ButtomBarEnum = ButtomBarEnum.CHECKOUTTODAY;
      spyOn(component, 'resetFormSearchReservation');
      spyOn(component, 'setReservation');
      spyOn(component, 'updateUrl');

      component.searchReservationDto = new SearchReservationDto();

      const event = {
        type: 'click'
      };
      component.listReservationsButtomBar(buttom, event);

      expect(component.resetFormSearchReservation).toHaveBeenCalled();
      expect(component.setReservation).toHaveBeenCalled();
      expect(component.updateUrl).toHaveBeenCalledWith(false);
      expect(component.checkinButtonIsActive).toBeFalsy();
      expect(component.searchReservationDto.checkInToday).toBeFalsy();
      expect(component.checkoutButtonIsActive).toBeTruthy();
      expect(component.searchReservationDto.checkOutToday).toBeTruthy();
      expect(component.deadlineButtonIsActive).toBeFalsy();
      expect(component.searchReservationDto.deadline).toBeFalsy();
      expect(component.hasAdvancedSearch).toBeFalsy();
    });

    it('should get deadline expired reservations from buttom bar', () => {
      const buttom: ButtomBarEnum = ButtomBarEnum.DEADLINE;
      spyOn(component, 'resetFormSearchReservation');
      spyOn(component, 'setReservation');
      spyOn(component, 'updateUrl');

      component.searchReservationDto = new SearchReservationDto();

      const event = {
        type: 'click'
      };
      component.listReservationsButtomBar(buttom, event);

      expect(component.resetFormSearchReservation).toHaveBeenCalled();
      expect(component.setReservation).toHaveBeenCalled();
      expect(component.updateUrl).toHaveBeenCalledWith(false);
      expect(component.checkinButtonIsActive).toBeFalsy();
      expect(component.searchReservationDto.checkInToday).toBeFalsy();
      expect(component.checkoutButtonIsActive).toBeFalsy();
      expect(component.searchReservationDto.checkOutToday).toBeFalsy();
      expect(component.deadlineButtonIsActive).toBeTruthy();
      expect(component.searchReservationDto.deadline).toBeTruthy();
      expect(component.hasAdvancedSearch).toBeFalsy();
    });

    it('should not get reservations from buttom bar', () => {
      const buttom: ButtomBarEnum = ButtomBarEnum.CHECKOUTTODAY - ButtomBarEnum.DEADLINE;
      spyOn(component, 'resetFormSearchReservation');
      spyOn(component, 'setReservation');
      spyOn(component, 'updateUrl');

      component.searchReservationDto = new SearchReservationDto();

      const event = {
        type: 'click'
      };
      component.listReservationsButtomBar(buttom, event);
      component.listReservationsButtomBar(buttom);

      expect(component.resetFormSearchReservation).toHaveBeenCalled();
      expect(component.setReservation).toHaveBeenCalled();
      expect(component.updateUrl).toHaveBeenCalledWith(false);
      expect(component.checkinButtonIsActive).toBeFalsy();
      expect(component.searchReservationDto.checkInToday).toBeFalsy();
      expect(component.checkoutButtonIsActive).toBeFalsy();
      expect(component.searchReservationDto.checkOutToday).toBeFalsy();
      expect(component.deadlineButtonIsActive).toBeFalsy();
      expect(component.searchReservationDto.deadline).toBeFalsy();
      expect(component.hasAdvancedSearch).toBeFalsy();
    });

    it('should resetFormSearchReservation', () => {
      spyOn(component, 'setFormSearchReservation');

      const elasticSearchFilter = '';
      const formSearchReservationValue = {
        freeTextSearch: '',
        groupName: '',
        arrivalRange: null,
        departureRange: null,
        passingByRange: null,
        creationRange: null,
        cancellationRange: null,
        deadlineRange: null,
        reservationItemStatus: '',
        mealPlanType: '',
        marketSegment: '',
        businessSource: '',
        ratePlan: '',
        roomType: '',
        adults: '',
        children: '',
        companyClientId: '',
        companyClientName: '',
        isMigrated: false,
        isGuest: false,
        isMain: false,
        room: ''
      };

      component.resetFormSearchReservation();

      expect(component.formSearchReservation.value).toEqual(formSearchReservationValue);
      expect(component.elasticSearchFilter).toEqual(elasticSearchFilter);
    });

    it('should listCustomSearchReservations', () => {
      component.preCheckInIsActive = false;
      spyOn(component, 'normalizeFieldsFormToBackend');
      spyOn(component, 'updateUrl');

      component.listCustomSearchReservations();

      expect(component.normalizeFieldsFormToBackend).toHaveBeenCalled();
      expect(component.updateUrl).toHaveBeenCalledWith(true);
    });

    it('should setReservation', () => {
      component.setReservation();

      expect(component.searchReservationDto).not.toBeNull();
    });

    it('should normalizeFieldsFormToBackend', () => {
      component.formSearchReservation = formBuilder.group({
        freeTextSearch: '',
        groupName: '',
        arrivalRange: { beginDate: '2017-10-20', endDate: '2017-10-25' },
        departureRange: { beginDate: '2017-10-27', endDate: '2017-11-01' },
        passingByRange: '',
        creationRange: '',
        cancellationRange: '',
        deadlineRange: '',
        reservationItemStatus: 2,
        mealPlanType: '',
        marketSegment: '',
        businessSource: '',
        ratePlan: '',
        roomType: '',
        adults: '',
        children: '',
        companyClientId: '',
        companyClientName: '',
        isMigrated: false,
        isMain: false,
        room: ''
      });

      component.normalizeFieldsFormToBackend();

      expect(component.searchReservationDto).not.toBeNull();
      expect(component.searchReservationDto.reservationItemStatus).toEqual(2);
      expect(component.searchReservationDto.arrivalInitial).toEqual('2017-10-20');
      expect(component.searchReservationDto.arrivalFinal).toEqual('2017-10-25');
      expect(component.searchReservationDto.departureInitial).toEqual('2017-10-27');
      expect(component.searchReservationDto.departureFinal).toEqual('2017-11-01');
    });

    it('should showCustomSearch', () => {
      spyOn(component, 'setButtonBarConfig');

      component.showCustomSearch();

      expect(component.hasAdvancedSearch).toBeTruthy();
      expect(component.setButtonBarConfig).toHaveBeenCalled();
    });

    it('should translateReservationStatusEnumToView', () => {
      const enumTranslated = [
        { key: '0', value: 'reservationModule.reserveList.reservationStatusEnum.ToConfirm' },
        { key: '1', value: 'reservationModule.reserveList.reservationStatusEnum.Confirmed' },
        { key: '2', value: 'reservationModule.reserveList.reservationStatusEnum.Checkin' },
        { key: '3', value: 'reservationModule.reserveList.reservationStatusEnum.Checkout' },
        { key: '4', value: 'reservationModule.reserveList.reservationStatusEnum.Pending' },
        { key: '5', value: 'reservationModule.reserveList.reservationStatusEnum.NoShow' },
        { key: '6', value: 'reservationModule.reserveList.reservationStatusEnum.Canceled' },
        { key: '7', value: 'reservationModule.reserveList.reservationStatusEnum.WaitList' },
        { key: '8', value: 'reservationModule.reserveList.reservationStatusEnum.ReservationProposal' },
      ];

      component.translateReservationStatusEnumToView(component.optionsReservationStatus);

      expect(component.hasAdvancedSearch).toBeTruthy();
      expect(component.optionsReservationStatus).toEqual(enumTranslated);
    });
  });

  describe('on database connections', () => {
    it('should loadReservationsRegistered', fakeAsync(() => {
      spyOn<any>(component['reservationResource'], 'searchReservationsByCriteria').and.returnValue(of({items: [
          { id: 1},
        ]}));

      component.searchReservationDto = searchReservationDtoData;

      component.loadReservationsRegistered();
      tick();

      expect(
          component['reservationResource'].searchReservationsByCriteria
      ).toHaveBeenCalledWith('1', searchReservationDtoData);
      expect<any>(component.propertyId).toEqual('1');
      expect<any>(component['configReservationList']).toHaveBeenCalledWith(component.reservationItemsList);
    }));

    it('should test configReservationList', () => {
      const reservation = new ReservationSearchDto();
      reservation['externalComments'] = 'externalComments';
      reservation['internalComments'] = 'internalComments';
      reservation['partnerComments'] = 'partnerComments';
      const reservation2 = new ReservationSearchDto();
      reservation2.roomId = 2;
      reservation2.roomNumber = '101';

      const reservationList = [reservation, reservation2];

      component['configReservationList'](reservationList);

      expect(reservationList[0]['remarksQuantity']).toEqual(3);
      expect(component['setRoomList']).toHaveBeenCalled();
      expect(component['associatedRoomList']).toContain(2);
      expect(reservationList[1]['beforeRoomId']).toEqual(2);
    });
  });

  it('should cancelReservationItem', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    const searchReservationResultDto = new SearchReservationResultDto();
    searchReservationResultDto.reservationItemId = 1;
    searchReservationResultDto.reservationItemCode = '122112';

    component.cancelReservationItem(searchReservationResultDto);

    expect(component.reservationItemViewCanceled.id).toEqual(searchReservationResultDto.reservationItemId);
    expect(component.reservationItemViewCanceled.code).toEqual(searchReservationResultDto.reservationItemCode);
    expect(component.searchReservationResultDtoCanceled).toEqual(searchReservationResultDto);
    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.reservationItemViewCanceled.code);
  });

  it(
    'should goToEditReservationPage',
    fakeAsync(() => {
      spyOn(component.router, 'navigate');
      component.goToEditReservationPage(1);
      expect(component.router.navigate).toHaveBeenCalledWith(['p', '1', 'reservation', 'new'], { queryParams: { reservation: 1 } });
    }),
  );

  it('should showViewReservationButton', () => {
    spyOn(component['reservationService'], 'showViewReservationButton');
    component.showViewReservationButton();
    expect(component['reservationService'].showViewReservationButton)
      .toHaveBeenCalled();
  });

  it('should showReactivateNoShowButton', () => {
    spyOn(component['reservationService'], 'showReactivateNoShowButton');
    component.showReactivateNoShowButton(searchReservationResultDtoData);
    expect(component['reservationService'].showReactivateNoShowButton)
      .toHaveBeenCalled();
  });

  it('should reactivateNoShow', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    component.reactivateNoShow(searchReservationResultDtoData);
    expect(component.noShowToReactivate).toEqual(searchReservationResultDtoData);
    expect(component['modalEmitToggleService'].emitToggleWithRef)
      .toHaveBeenCalledWith(component.REACTIVATE_NO_SHOW_MODAL_ID);
  });

  it('should cancelReactivateNoShow', () => {
    spyOn(component['modalEmitToggleService'], 'emitCloseWithRef');
    component.cancelReactivateNoShow();
    expect(component.noShowToReactivate).toBeNull();
    expect(component['modalEmitToggleService'].emitCloseWithRef)
      .toHaveBeenCalledWith(component.REACTIVATE_NO_SHOW_MODAL_ID);
  });

  it('should confirmReactivateNoShow', () => {
    spyOn(component['modalEmitToggleService'], 'emitCloseWithRef');
    spyOn(component['reservationItemResource'], 'reactivateNoShow').and.returnValue(of({}));
    spyOn(component['toasterEmitService'], 'emitChange');
    spyOn(component, 'loadReservationsRegistered');

    component.noShowToReactivate = searchReservationResultDtoData;
    component.confirmReactivateNoShow();

    expect(component['modalEmitToggleService'].emitCloseWithRef)
      .toHaveBeenCalledWith(component.REACTIVATE_NO_SHOW_MODAL_ID);
    expect(component['reservationItemResource'].reactivateNoShow)
      .toHaveBeenCalledWith(searchReservationResultDtoData.reservationItemId);
    expect(component['toasterEmitService'].emitChange)
      .toHaveBeenCalledWith(SuccessError.success, 'reservationModule.reactivateNoShow.successMessage');
    expect(component.noShowToReactivate).toBeNull();
    expect(component.loadReservationsRegistered).toHaveBeenCalled();
  });

  it('should goToViewReservationPage', () => {
    spyOn(component['reservationService'], 'goToViewReservationPage');
    component.goToViewReservationPage(1);
    expect(component['reservationService'].goToViewReservationPage)
      .toHaveBeenCalledWith(1, component.propertyId);
  });

  it('should loadOptionsStatus', () => {
    spyOn<any>(component, 'translateReservationStatusEnumToView');
    component.loadOptionsStatus();

    expect(component.optionsReservationStatus).toEqual(Data.RESERVATION_STATUS);
    expect(component.translateReservationStatusEnumToView).toHaveBeenCalled();
  });

  describe( 'should test getDatabaseClass', () => {
    const getClass = ( param1, param2 ) => ({
      'col-10': param1,
      'col-12': param2
    });

    it( 'reservationObj is null and preCheckInIsActive is false', () => {
      component.preCheckInIsActive = false;
      component.reservationObj = null;
      expect(component.getDatabaseClass()).toEqual(getClass(null, true));
    });

    it( 'reservationObj is null and preCheckInIsActive is true', () => {
      component.preCheckInIsActive = true;
      component.reservationObj = null;
      expect(component.getDatabaseClass()).toEqual(getClass(null, true));
    });

    it( 'reservationObj and preCheckInIsActive is false', () => {
      component.preCheckInIsActive = false;
      component.reservationObj = new SearchReservationResultDto();
      expect(component.getDatabaseClass()).toEqual(getClass(true, false));
    });

    it( 'reservationObj and preCheckInIsActive is true', () => {
      component.preCheckInIsActive = true;
      component.reservationObj = new SearchReservationResultDto();
      expect(component.getDatabaseClass()).toEqual(getClass(false, true));
    });
  });


  it('should getHasAdvancedSearch', () => {
    const params = {
      arrivalFinal: '2019-08-20T00:00:00',
      arrivalInitial: '2019-08-18T00:00:00',
      hasAdvancedSearch: 'true'
    };

    component.getHasAdvancedSearch(params);

    expect(component.searchReservationDto.arrivalFinal).toEqual(params['arrivalFinal']);
    expect(component.formSearchReservation.get('arrivalRange').value).toEqual({
      beginDate: '2019-08-18T00:00:00',
      endDate: '2019-08-20T00:00:00'
    });
  });

  it('should getHasAdvancedSearch is pre checkin', () => {
    component.preCheckInIsActive = true;
    const params = {
      arrivalFinal: '2019-08-20T00:00:00',
      arrivalInitial: '2019-08-18T00:00:00',
      hasAdvancedSearch: 'true'
    };

    component.getHasAdvancedSearch(params);

    expect(component.searchReservationDto.isPrecheckin).toEqual(true);
    expect(component.searchReservationDto.isGuestVision).toEqual(false);
  });


  it('should test loadRoomList', fakeAsync(() => {
    const items = [1, 2];
    spyOn<any>(component['roomTypeResource'], 'getRoomTypesWithOverBookingByPropertyId').and.returnValue( of( { items }));
    const row = {};
    component.loadRoomList(row);
    tick();

    expect(component['setRoomList']).toHaveBeenCalled();
    expect(row['hasRoomList']).toEqual(true);
  }));

  describe('should test changeSelectedRoom', () => {
    const row = { reservationItemId: 1, receivedRoomTypeId: 2, hasRoomList: true };
    beforeAll( () => {
      spyOn<any>(component['checkinResource'], 'associateRoomToReservationItem').and.returnValue( of( null ));
      spyOn<any>(component['toasterEmitService'], 'emitChange').and.callFake( () => {});
    });

    it('common case', fakeAsync(() => {
      const event = 35;
      const propertyId = 2;

      component.propertyId = propertyId;
      component.associatedRoomList = [];
      component.changeSelectedRoom(event, row);
      tick();

      expect(component['checkinResource'].associateRoomToReservationItem).toHaveBeenCalledWith(
          propertyId,
          row.reservationItemId,
          row.receivedRoomTypeId,
          event,
      );
      expect(component['setRoomList']).toHaveBeenCalledWith(row);
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalled();
      expect(component.associatedRoomList).toContain(event);
      expect(row['beforeRoomId']).toEqual(event);
    }));

    it('with event empty', () => {
      const event = '';
      row['beforeRoomId'] = event;
      component.associatedRoomList = [event];
      component.changeSelectedRoom(event, row);

      expect(component.associatedRoomList.length).toEqual(0);
    });
  });


  it('should test getRoomList', () => {
    const item1 = new CheckInRoomTypesDto();
    const item2 = new CheckInRoomTypesDto();
    const receivedRoomTypeId = 2;
    const items = [ item1, item2 ];
    const roomList = [ 1, 2, 3 ];
    component.associatedRoomList = [2];

    item2.id = receivedRoomTypeId;
    item2['roomList'] = roomList;

    const result = component['getRoomList'](receivedRoomTypeId, items);


    expect(result).toEqual(roomList);
  });

  describe('should test setRoomList', () => {
    const row = { reservationItemId: 1, receivedRoomTypeId: 2 };
    const empty = { text: 'label', value: '' };
    beforeAll(() => {
      spyOn<any>(component['translateService'], 'instant').and.returnValue('label');
    });

    it('without items and without (roomNumber and roomId)', () => {
      component['setRoomList'](row);

      expect(row['roomList']).toEqual([empty]);
    });
  });
});
