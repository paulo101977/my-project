import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-flag-launch-daily',
  templateUrl: './flag-launch-daily.component.html',
  styleUrls: ['./flag-launch-daily.component.css']
})
export class FlagLaunchDailyComponent {
  public isActive = true;
  @Input('isActiveFlag')
  set isActiveFlag(active: boolean) {
    this.isActive = active;
  }
  get isActiveFlag(): boolean {
    return this.isActive;
  }
  @Input() disabled = false;

  @Output() flagChangeStatus = new EventEmitter();

  public itemWasUpdated(event) {
    this.flagChangeStatus.emit(this.isActive);
  }

}
