import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CreditCardDataComponent } from './credit-card-data.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserModule } from '@angular/platform-browser';
import { CreditCardBannerTextPipe } from '../../../../shared/pipes/credit-card-banner-text.pipe';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('CreditCardDataComponent', () => {
  let component: CreditCardDataComponent;
  let fixture: ComponentFixture<CreditCardDataComponent>;
  const formBuilder = new FormBuilder();

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [BrowserModule, TranslateModule.forRoot(), HttpClientModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [CreditCardDataComponent],
      providers: [CreditCardBannerTextPipe, InterceptorHandlerService],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(CreditCardDataComponent);
    component = fixture.componentInstance;

    component.parent = formBuilder.group({
      holder: [''],
      plasticNumber: [''],
      expirationDate: [''],
      csc: [''],
      selectValuebanner: formBuilder.group({
        banner: [''],
      }),
    });

    spyOn(component['commomService'], 'convertObjectToSelectOptionObjectByIdAndValueProperties').and.returnValue([]);

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should setVars', () => {
      expect(component.mask).toEqual([/[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/]);
    });
  });
});
