import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import { CreditCardBannerEnum } from '../../../../shared/models/credit-card-banner-enum';
import { CreditCardBannerTextPipe } from '../../../../shared/pipes/credit-card-banner-text.pipe';
import { SelectObjectOption } from '../../../../shared/models/selectModel';
import { PaymentTypeResource } from '../../../../shared/resources/payment-type/payment-type.resource';
import { CommomService } from '../../../../shared/services/shared/commom.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'credit-card-data',
  templateUrl: 'credit-card-data.component.html',
})
export class CreditCardDataComponent implements OnInit {
  public mask: any;
  public bannerOptionList: Array<SelectObjectOption>;
  public propertyId: number;

  @Input() parent: FormGroup;
  @Input() readonly: boolean;

  constructor(
    private creditCardBannerTextPipe: CreditCardBannerTextPipe,
    private commomService: CommomService,
    public route: ActivatedRoute,
    private paymentTypeResource: PaymentTypeResource,
  ) {}

  ngOnInit() {
    this.setVars();
    this.getPaymentTypeList();
  }

  setVars() {
    this.propertyId = this.route.snapshot.params.property;
    this.mask = [/[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/];
  }

  private getPaymentTypeList() {
    this.bannerOptionList = new Array<SelectObjectOption>();
    this.paymentTypeResource.getPlasticBrandPropertyList(this.propertyId).subscribe(response => {
      this.bannerOptionList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        response,
        'plasticBrandId',
        'plasticBrandName',
      );
    });
  }
}
