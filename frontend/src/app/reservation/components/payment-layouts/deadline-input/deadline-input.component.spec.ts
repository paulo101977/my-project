import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IMyDpOptionsData } from '../../../../shared/mock-data/IMyDpOptions-data';
// Componets
import { DeadlineInputComponent } from './deadline-input.component';

describe('DeadlineInputComponent', () => {
  let component: DeadlineInputComponent;
  let fixture: ComponentFixture<DeadlineInputComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [DeadlineInputComponent],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(DeadlineInputComponent);
    component = fixture.componentInstance;

    spyOn(component['dateService'], 'getConfigDatePicker').and.returnValue(IMyDpOptionsData);

    fixture.detectChanges();
  });
});
