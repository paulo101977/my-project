import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { IMyDate } from 'mydatepicker';

@Component({
  selector: 'deadline-input',
  templateUrl: 'deadline-input.component.html',
})
export class DeadlineInputComponent implements OnChanges {

  @Input() parent: FormGroup;

  @Input() extraClass: string;

  @Input() controlName: FormControl;
  @Input() disableUntil: IMyDate;
  @Input() readonly: boolean;

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['disableUntil']) {
      this.disableUntil = changes['disableUntil'].currentValue;
    }
  }
}
