import { Component, Output, EventEmitter, OnInit, Input } from '@angular/core';

@Component({
  selector: 'flag-reservation-confirmed',
  templateUrl: 'flag-reservation-confirmed.component.html',
})
export class FlagReservationConfirmedComponent implements OnInit {
  public isActive: boolean;

  @Input() isActiveFlagConfirmed: boolean;
  @Input() readonly: boolean;
  @Output() reservationConfirmedIsActive = new EventEmitter();

  ngOnInit() {
    this.isActive = this.isActiveFlagConfirmed;
  }

  public itemWasUpdated(event) {
    if (this.readonly) {
      return;
    }
    this.reservationConfirmedIsActive.emit(this.isActive);
  }
}
