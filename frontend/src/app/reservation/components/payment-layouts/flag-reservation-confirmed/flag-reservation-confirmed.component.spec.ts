import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

// Componets
import { FlagReservationConfirmedComponent } from './flag-reservation-confirmed.component';

describe('FlagReservationConfirmedComponent', () => {
  let component: FlagReservationConfirmedComponent;
  let fixture: ComponentFixture<FlagReservationConfirmedComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [FlagReservationConfirmedComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(FlagReservationConfirmedComponent);
    component = fixture.componentInstance;

    spyOn(component.reservationConfirmedIsActive, 'emit');

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should setVars', () => {
      component.isActiveFlagConfirmed = true;
      component.ngOnInit();
      expect(component.isActive).toEqual(component.isActiveFlagConfirmed);
    });
  });
});
