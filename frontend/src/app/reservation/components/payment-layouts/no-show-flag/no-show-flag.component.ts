import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'no-show-flag',
  templateUrl: 'no-show-flag.component.html',
})
export class NoShowFlagComponent implements OnInit {
  public isActive: boolean;

  @Input() isActiveFlagNoShow: boolean;
  @Input() readonly: boolean;

  @Output() noShowIsActive = new EventEmitter();

  ngOnInit() {
    this.isActive = this.isActiveFlagNoShow;
  }

  itemWasUpdated(event: boolean) {
    if (this.readonly) {
      return;
    }
    this.noShowIsActive.emit(this.isActive);
  }
}
