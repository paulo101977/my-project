import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

// Componets
import { NoShowFlagComponent } from './no-show-flag.component';

describe('NoShowFlagComponent', () => {
  let component: NoShowFlagComponent;
  let fixture: ComponentFixture<NoShowFlagComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [NoShowFlagComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(NoShowFlagComponent);
    component = fixture.componentInstance;

    spyOn(component.noShowIsActive, 'emit');
    component.isActiveFlagNoShow = false;

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should set isActive false', () => {
      expect(component.isActive).toBeFalsy();
    });

    it('should set isActive true', () => {
      component.isActiveFlagNoShow = true;

      component['ngOnInit']();

      expect(component.isActive).toBeTruthy();
    });
  });
});
