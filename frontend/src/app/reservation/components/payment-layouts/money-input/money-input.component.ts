import { FormGroup, FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'money-input',
  templateUrl: 'money-input.component.html',
})
export class MoneyInputComponent implements OnInit {
  public optionsCurrencyMask: any;

  @Input() title: string;
  @Input() placeholderText: string;
  @Input() parent: FormGroup;
  @Input() controlName: FormControl;
  @Input() readonly: boolean;

  ngOnInit() {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
  }
}
