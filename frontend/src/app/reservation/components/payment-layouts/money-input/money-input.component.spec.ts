import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CurrencyMaskModule } from 'ng2-currency-mask';

// Componets
import { MoneyInputComponent } from './money-input.component';
import { CoreModule } from '@app/core/core.module';

describe('MoneyInputComponent', () => {
  let component: MoneyInputComponent;
  let fixture: ComponentFixture<MoneyInputComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [CurrencyMaskModule, TranslateModule.forRoot(), CoreModule],
      declarations: [MoneyInputComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(MoneyInputComponent);
    component = fixture.componentInstance;

    component.title = 'My Title';
    component.placeholderText = 'R$ 00,00';
    component.parent = new FormBuilder().group({
      moneyValue: [''],
    });
    component.controlName = new FormControl();

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should setVars', () => {
      expect(component.optionsCurrencyMask).toEqual({ prefix: '', thousands: '.', decimal: ',', align: 'left' });
    });
  });
});
