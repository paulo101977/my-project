// import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { RouterTestingModule } from '@angular/router/testing';
// import { TranslateModule } from '@ngx-translate/core';
// import { ReservationItemCardFooterComponent } from './reservation-item-card-footer.component';
// import { Guest, GuestStatus } from '../../../shared/models/reserves/guest';
// import { BedType } from '../../../shared/models/bed-type';
// import { RoomType } from '../../../shared/models/room-type/room-type';
// import { Room } from '../../../shared/models/room';
// import { ReservationItemView } from '../../../shared/models/reserves/reservation-item-view';
// import { ReservationStatusEnum } from '../../../shared/models/reserves/reservation-status-enum';
// import { StatusReservationTextPipe } from '../../../shared/pipes/status-reservation-text.pipe';
//
// describe('ReservationItemFooterCardComponent', () => {
//   let component: ReservationItemCardFooterComponent;
//   let fixture: ComponentFixture<ReservationItemCardFooterComponent>;
//   const roomTypeMock = new RoomType();
//   const roomTypeMock2 = new RoomType();
//   const formBuilder = new FormBuilder();
//   let bedTypeCrib;
//   let bedTypeSingle;
//   const guest = new Guest();
//   const guestList = [];
//   const roomMock2 = new Room();
//   const roomMock = new Room();
//
//   function setGuest() {
//     guest.status = GuestStatus.NoGuest;
//     guest.formGuest = formBuilder.group({
//       guestName: [''],
//     });
//     guestList.push(guest);
//   }
//
//   function setRoom() {
//     roomMock.roomNumber = '101';
//     roomMock2.roomNumber = '209';
//   }
//
//   function setBedType() {
//     bedTypeCrib = new BedType();
//     bedTypeCrib.count = 0;
//     bedTypeCrib.type = 5;
//     bedTypeCrib.optionalLimit = 2;
//
//     bedTypeSingle = new BedType();
//     bedTypeSingle.count = 2;
//     bedTypeSingle.type = 1;
//     bedTypeSingle.optionalLimit = 1;
//   }
//
//   function setRoomType() {
//     roomTypeMock.id = 1;
//     roomTypeMock.name = 'Quarto Standard';
//     roomTypeMock.abbreviation = 'SPDC';
//     roomTypeMock.order = 1;
//     roomTypeMock.adultCapacity = 2;
//     roomTypeMock.childCapacity = 1;
//     roomTypeMock.roomTypeBedTypeList = new Array<BedType>();
//     roomTypeMock.roomTypeBedTypeList.push(bedTypeSingle);
//     roomTypeMock.roomTypeBedTypeList.push(bedTypeCrib);
//     roomTypeMock.isActive = true;
//     roomTypeMock.isInUse = false;
//     roomTypeMock.roomList = new Array<Room>();
//     roomTypeMock.roomList.push(roomMock);
//
//     roomTypeMock2.id = 2;
//     roomTypeMock2.name = 'Quarto Luxo';
//     roomTypeMock2.abbreviation = 'LXO';
//     roomTypeMock2.order = 1;
//     roomTypeMock2.adultCapacity = 3;
//     roomTypeMock2.childCapacity = 0;
//     roomTypeMock2.roomTypeBedTypeList = new Array<BedType>();
//     roomTypeMock2.roomTypeBedTypeList.push(bedTypeSingle);
//     roomTypeMock2.roomTypeBedTypeList.push(bedTypeCrib);
//     roomTypeMock2.isActive = true;
//     roomTypeMock2.isInUse = false;
//     roomTypeMock2.roomList = new Array<Room>();
//     roomTypeMock2.roomList.push(roomMock2);
//   }
//
//   function setreservationItem() {
//     setGuest();
//     setRoom();
//     setBedType();
//     setRoomType();
//
//     const accomodationCardForm = formBuilder.group({
//       periodCard: [''],
//       adultQuantityCard: [1],
//       childrenQuantityCard: [0],
//       childrenAgeCard: [''],
//       roomTypeCard: [''],
//       checkinHourCard: [''],
//       checkoutHourCard: [''],
//       roomNumberCard: [''],
//       roomLayoutCard: [''],
//       extraBedQuantityCard: [''],
//       cribBedQuantityCard: [''],
//       hasCribBedTypeCard: [false],
//       hasExtraBedTypeCard: [false],
//     });
//
//     component.parent = new FormGroup({});
//     component.reservationItemView = new ReservationItemView();
//     component.reservationItemView.code = '';
//     component.reservationItemView.room = new Room();
//     component.reservationItemView.period = '12/01/2017 - 14/01/2017';
//     component.reservationItemView.adultQuantity = 2;
//     component.reservationItemView.childrenQuantity = 1;
//     component.reservationItemView.status = ReservationStatusEnum.ToConfirm;
//     component.reservationItemView.guestQuantity = guestList;
//     component.reservationItemView.receivedRoomType = new RoomType();
//     component.reservationItemView.receivedRoomType.id = 1;
//     component.reservationItemView.receivedRoomType.name = 'Quarto Standard';
//     component.reservationItemView.receivedRoomType.abbreviation = 'SPDC';
//     component.reservationItemView.receivedRoomType.order = 1;
//     component.reservationItemView.receivedRoomType.adultCapacity = 2;
//     component.reservationItemView.receivedRoomType.childCapacity = 1;
//     component.reservationItemView.receivedRoomType.roomTypeBedTypeList = new Array<BedType>();
//     component.reservationItemView.receivedRoomType.roomTypeBedTypeList.push(bedTypeSingle);
//     component.reservationItemView.receivedRoomType.roomTypeBedTypeList.push(bedTypeCrib);
//     component.reservationItemView.receivedRoomType.isActive = true;
//     component.reservationItemView.receivedRoomType.isInUse = false;
//     component.reservationItemView.receivedRoomType.roomList = new Array<Room>();
//     component.reservationItemView.receivedRoomType.roomList.push(roomMock2);
//     component.reservationItemView.roomTypes = new Array<RoomType>();
//     component.reservationItemView.roomTypes.push(roomTypeMock);
//     component.reservationItemView.roomTypes.push(roomTypeMock2);
//     component.reservationItemView.dailyNumber = 3;
//     component.reservationItemView.dailyPrice = 1200.0;
//     component.reservationItemView.totalDaily = component.reservationItemView.dailyPrice * component.reservationItemView.dailyNumber;
//     component.reservationItemView.total = 3;
//     component.reservationItemView.room.roomNumber = 'ABCDEFG';
//     component.reservationItemView.focus = false;
//     component.reservationItemView.schedulePopoverIsVisible = false;
//     component.reservationItemView.formCard = accomodationCardForm;
//     component.reservationItemView.formCard.controls.roomTypeCard.setValue(component.reservationItemView.receivedRoomType);
//   }
//
//   beforeAll(() => {
//     TestBed.resetTestEnvironment();
//     TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
//       imports: [TranslateModule.forRoot(), RouterTestingModule, FormsModule, ReactiveFormsModule],
//       declarations: [ReservationItemCardFooterComponent, StatusReservationTextPipe],
//       providers: [],
//       schemas: [CUSTOM_ELEMENTS_SCHEMA],
//     });
//     fixture = TestBed.createComponent(ReservationItemCardFooterComponent);
//     component = fixture.componentInstance;
//
//     setreservationItem();
//
//     fixture.detectChanges();
//   });
//
//   describe('on init', () => {
//     it('should set vars', () => {
//       expect(component.currencyCountry).toEqual(window.navigator.language);
//     });
//   });
//
//   describe('on popovers', () => {
//     it('should set extrasBedPopoverIsVisible for true when reservationItemView is true in keepExtrasBedPopoverCardVisible', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function() {},
//       };
//       const reservationItemView = {
//         extrasBedPopoverIsVisible: true,
//       };
//
//       component.keepExtrasBedPopoverCardVisible(event, reservationItemView);
//
//       expect(reservationItemView.extrasBedPopoverIsVisible).toEqual(true);
//     });
//
//     it('should set extrasBedPopoverIsVisible for true when reservationItemView is false in keepExtrasBedPopoverCardVisible', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function() {},
//       };
//       const reservationItemView = {
//         extrasBedPopoverIsVisible: false,
//       };
//
//       component.keepExtrasBedPopoverCardVisible(event, reservationItemView);
//
//       expect(reservationItemView.extrasBedPopoverIsVisible).toEqual(true);
//     });
//
//     it('should set reservationItemView.extrasBedPopoverIsVisible for false in showOrHideExtrasBedPopoverCard', () => {
//       const event = {
//         type: 'click',
//         stopPropagation: function() {},
//       };
//       const reservationItemView = {
//         extrasBedPopoverIsVisible: true,
//       };
//
//       component.showOrHideExtrasBedPopoverCard(event, reservationItemView);
//
//       expect(reservationItemView.extrasBedPopoverIsVisible).toEqual(false);
//     });
//   });
//
//   it('should reduceAndGetCodeOfReservationItem', () => {
//     const code = '0001';
//     spyOn(component['reservationItemViewService'], 'reduceAndGetCodeOfReservationItem').and.returnValue(code);
//
//     const result = component.reduceAndGetCodeOfReservationItem(code);
//
//     expect(result).toEqual(code);
//   });
// });
