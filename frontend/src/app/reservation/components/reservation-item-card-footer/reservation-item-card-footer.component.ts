import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ReservationItemViewService } from '../../../shared/services/reservation-item-view/reservation-item-view.service';
import { ReservationItemView } from '../../../shared/models/reserves/reservation-item-view';
import { UserService } from '../../../shared/services/user/user.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';

@Component({
  selector: 'reservation-item-card-footer',
  templateUrl: 'reservation-item-card-footer.component.html',
  styleUrls: ['reservation-item-card-footer.component.css'],
})
export class ReservationItemCardFooterComponent implements OnInit {
  public currencyCountry: string;

  @Input() reservationItemView: ReservationItemView;

  @Input() parent: FormGroup;

  @Input() isRoomList: boolean;
  @Input() readonly: boolean;

  public propertyId: number;
  public currencySymbol: string;
  public budgetedAmount: number;
  public isGratuityType: boolean;
  public integrationForm: FormGroup;

  public buttonCancelModal: ButtonConfig;
  public buttonConfirmModal: ButtonConfig;

  constructor(
    private reservationItemViewService: ReservationItemViewService,
    public route: ActivatedRoute,
    public userService: UserService,
    public dateService: DateService,
    private modalToggleService: ModalEmitToggleService,
    private fb: FormBuilder,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    this.setVars();
    this.setIntegrationForm();
    this.setModalButtons();
  }

  private setModalButtons() {
    this.buttonCancelModal = this.sharedService.getButtonConfig(
      'buttonCancelIntegration', this.cancelIntegrationModal, 'action.cancel', ButtonType.Secondary);

    this.buttonConfirmModal = this.sharedService.getButtonConfig(
      'buttonCancelIntegration', this.confirmIntegrationModal, 'action.confirm', ButtonType.Primary);
  }

  private setIntegrationForm() {
    this.integrationForm = this.fb.group({
      externalReservationNumber: [this.reservationItemView.externalReservationNumber],
      partnerReservationNumber: [this.reservationItemView.partnerReservationNumber],
    });
  }

  setVars() {
    this.currencyCountry = this.userService.getUserpreferredCulture();
    this.propertyId = this.route.snapshot.params.property;
    this.reservationItemView.extraBedCount = 0;
    this.reservationItemView.extraCribCount = 0;
    if ( this.reservationItemView
      && this.reservationItemView.budgetReservationBudgetList
      && this.reservationItemView.budgetReservationBudgetList.length > 0 ) {
      this.currencySymbol = this.reservationItemView.budgetReservationBudgetList[0].currencySymbol;
    }
    if (this.reservationItemView && this.reservationItemView.budgetGratuityTypeId) {
      this.isGratuityType = this.reservationItemView.budgetGratuityTypeId > 0;
    }
    this.setBudgetAmount();
  }

  private setBudgetAmount() {
    if (this.isGratuityType) {
        this.budgetedAmount = 0;
    } else {
      this.reservationItemView.budgetReservationBudgetList.forEach(reservationBudget => {
        this.budgetedAmount += reservationBudget.manualRate;
      });
    }
  }

  public keepExtrasBedPopoverCardVisible(event, reservationItemData) {
    event.stopPropagation();
    reservationItemData.extrasBedPopoverIsVisible = true;
  }

  public showOrHideExtrasBedPopoverCard(event, reservationItemData) {
    event.stopPropagation();
    reservationItemData.extrasBedPopoverIsVisible = !reservationItemData.extrasBedPopoverIsVisible;
  }

  public reduceAndGetCodeOfReservationItem(code: string): string {
    return this.reservationItemViewService.reduceAndGetCodeOfReservationItem(code);
  }

  public goToRoomlist(reservationItemView) {
    if (!this.readonly) {
      this.reservationItemViewService.goToRoomlist(reservationItemView);
    }
  }

  public getOvernights() {
    return this.dateService
      .convertPeriodToDateList(this.reservationItemView.period.beginDate, this.reservationItemView.period.endDate, true).length;
  }

  public getTotal() {
    if ( this.reservationItemView && (this.reservationItemView.gratuityTypeId || this.reservationItemView.budgetGratuityTypeId)) {
      return 0;
    }
    return this.reservationItemView.budgetReservationBudgetList.reduce( (val, current) => val +=  current.manualRate, 0);
  }

  public editHigsCode() {
    // todo implement
    this.modalToggleService.emitOpenWithRef(this.reservationItemView.id);
    this.integrationForm.patchValue( this.reservationItemView );
  }

  public cancelIntegrationModal = () => {
    this.modalToggleService.emitCloseWithRef(this.reservationItemView.id);
  }

  public confirmIntegrationModal = () => {
    this.modalToggleService.emitCloseWithRef(this.reservationItemView.id);
    const { partnerReservationNumber, externalReservationNumber } = this.integrationForm.getRawValue();
    this.reservationItemView.partnerReservationNumber = partnerReservationNumber;
    this.reservationItemView.externalReservationNumber = externalReservationNumber;
  }
}
