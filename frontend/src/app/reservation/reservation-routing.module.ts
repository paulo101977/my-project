import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReservationCreateComponent } from './components/reservation-create/reservation-create.component';
import { ReservationListComponent } from './components/reservation-list/reservation-list.component';
import { ReservationSlipComponent } from './components/reservation-slip/reservation-slip.component';

export const reservationRoutes: Routes = [
  { path: '', component: ReservationListComponent },
  { path: 'new', component: ReservationCreateComponent },
  { path: 'view', component: ReservationCreateComponent },
  { path: 'slip/:reservationId', component: ReservationSlipComponent },
  { path: 'rate-proposal', loadChildren: './rate-proposal/rate-proposal.module#RateProposalModule' },
];

@NgModule({
  imports: [
    RouterModule.forChild(reservationRoutes),
  ],
  exports: [RouterModule],
})
export class ReservationRoutingModule {}
