import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ReservationRoutingModule } from './reservation-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { MomentModule } from 'ngx-moment';
import { MyDatePickerModule } from 'mydatepicker';
import { ReservationBudgetModule } from 'app/revenue-management/reservation-budget/reservation-budget.module';
import { ReservationItemCardComponent } from './components-containers/reservation-item-card/reservation-item-card.component';
import { ReservationItemCardBodyComponent } from './components-containers/reservation-item-card-body/reservation-item-card-body.component';
import { ReservationCreateComponent } from './components/reservation-create/reservation-create.component';
import { ReservationListComponent } from './components/reservation-list/reservation-list.component';
import { ReservationItemCardHeaderComponent } from './components/reservation-item-card-header/reservation-item-card-header.component';
import { ReservationItemCardFooterComponent } from './components/reservation-item-card-footer/reservation-item-card-footer.component';
import { NoShowFlagComponent } from './components/payment-layouts/no-show-flag/no-show-flag.component';
import { MoneyInputComponent } from './components/payment-layouts/money-input/money-input.component';
import { FlagReservationConfirmedComponent } from './components/payment-layouts/flag-reservation-confirmed/flag-reservation-confirmed.component';
import { DeadlineInputComponent } from './components/payment-layouts/deadline-input/deadline-input.component';
import { ReservationItemCancelComponent } from './components/reservation-item-cancel/reservation-item-cancel.component';
import { ReservationCancelComponent } from './components/reservation-cancel/reservation-cancel.component';
import { ReservationResumeModalComponent } from './components/reservation-resume-modal/reservation-resume-modal.component';
import { ReservationSlipComponent } from './components/reservation-slip/reservation-slip.component';
import { CreditCardDataComponent } from './components/payment-layouts/credit-card-data/credit-card-data.component';
import { PropertyService } from '../shared/services/property/property.service';
import { ReservationService } from '../shared/services/reservation/reservation.service';
import { GuestService } from '../shared/services/guest/guest.service';
import { ReservationItemResource } from '../shared/resources/reservation-item/reservation-item.resource';
import { CreditCardBannerTextPipe } from '../shared/pipes/credit-card-banner-text.pipe';
import { ReservationInsertFormComponent } from './components-containers/reservation-insert-form/reservation-insert-form.component';
import { RateProposalModule } from 'app/reservation/rate-proposal/rate-proposal.module';
import { ReservationCardComponent } from '@app/reservation/components/reservation-card/reservation-card.component';
import { FlagLaunchDailyComponent } from './components/payment-layouts/flag-launch-daily/flag-launch-daily.component';
import { ThxImgInfoModule, ThxSelectV2Module } from '@inovacao-cmnet/thx-ui';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    SharedModule,
    ReservationRoutingModule,
    MyDateRangePickerModule,
    MomentModule,
    MyDatePickerModule,
    ReservationBudgetModule,
    RateProposalModule,
    ThxImgInfoModule,
    ThxSelectV2Module,
  ],
  declarations: [
    ReservationCreateComponent,
    ReservationListComponent,
    ReservationCardComponent,
    ReservationItemCardComponent,
    ReservationItemCardHeaderComponent,
    ReservationItemCardFooterComponent,
    ReservationItemCardBodyComponent,
    NoShowFlagComponent,
    MoneyInputComponent,
    FlagReservationConfirmedComponent,
    DeadlineInputComponent,
    CreditCardDataComponent,
    ReservationItemCancelComponent,
    ReservationCancelComponent,
    ReservationResumeModalComponent,
    ReservationSlipComponent,
    ReservationInsertFormComponent,
    FlagLaunchDailyComponent
  ],
  exports: [
    ReservationListComponent,
    ReservationCreateComponent
  ],
  providers: [ ReservationService,
    GuestService,
    PropertyService,
    CreditCardBannerTextPipe,
    ReservationItemResource ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ReservationModule {}
