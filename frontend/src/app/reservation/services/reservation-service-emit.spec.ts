import { TestBed, inject } from '@angular/core/testing';

import { ReservationServiceEmit } from './reservation-service-emit';

describe('Reservation.ServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReservationServiceEmit]
    });
  });

  it('should be created', inject([ReservationServiceEmit], (service: ReservationServiceEmit) => {
    expect(service).toBeTruthy();
  }));
});
