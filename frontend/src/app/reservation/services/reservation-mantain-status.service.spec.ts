import { TestBed, inject } from '@angular/core/testing';

import { ReservationMantainStatusService } from './reservation-mantain-status.service';

describe('ReservationMantainStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReservationMantainStatusService]
    });
  });

  it('should be created', inject([ReservationMantainStatusService], (service: ReservationMantainStatusService) => {
    expect(service).toBeTruthy();
  }));
});
