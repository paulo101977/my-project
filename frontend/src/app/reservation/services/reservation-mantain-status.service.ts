import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReservationMantainStatusService {

  private listValues: any = {};

  constructor() {}

  public setValue(identifier: string, value: ReservationBudgetStatusChange) {
    this.listValues[identifier] = value;
  }

  public getValue(identifier): any {
    return this.listValues[identifier];
  }

  public resetToOldValues( identifier: string ) {
    const element = this.listValues[identifier];
    const keys = Object.keys( element );
    keys.forEach( key => {
      element[key].newValue = element[key].oldValue;
    });
  }

  public resetToNewValues( identifier: string ) {
    const element = this.listValues[identifier];
    const keys = Object.keys( element );
    keys.forEach( key => {
      element[key].oldValue = element[key].newValue;
    });
  }

  public newValueIn(identifier: string, itemKey: string, value: any) {
    const element = this.listValues[identifier];
    if (element) {
      const item = element[itemKey];
      if (item) {
        item.newValue = value;
      }
    }
  }

  public isEqual(identifier: string) {
    const element = this.listValues[identifier];
    const keys = Object.keys(element);
    let isEqual = true;
    keys.forEach( key => {
      if ( element[key].newValue != element[key].oldValue) {
        isEqual = false;
      }
    });
    return isEqual;
  }

}

export class OldAndNew {
  oldValue: any;
  newValue: any;

  constructor(oldValue: any, newValue?: any) {
    if (!newValue) {
      this.newValue = this.oldValue = oldValue;
    } else {
      this.oldValue = oldValue;
      this.newValue = newValue;
    }
  }
}

export class ReservationBudgetStatusChange {
  period: OldAndNew;
  adultQuantity: OldAndNew;
  childrenQuantity: OldAndNew;
  childAges: OldAndNew;
  uhType: OldAndNew;
  commercialBudget: OldAndNew;
  commercialBudgetKey: OldAndNew;
  reservationBudget: OldAndNew;
  reservationBudgetOriginal: OldAndNew;
  guestList: OldAndNew;
  gratuityType: OldAndNew;
}
