import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ReservationDto } from '../../shared/models/dto/reservation-dto';

@Injectable({
  providedIn: 'root'
})
export class ReservationServiceEmit {

  constructor() {
  }

  private emitSaveSource = new Subject<ReservationDto>();
  public reservationSaved$ = this.emitSaveSource.asObservable();

  public emitSave(reservation: ReservationDto) {
    this.emitSaveSource.next(reservation);
  }

}
