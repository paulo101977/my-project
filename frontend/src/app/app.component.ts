import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { InternationalizationService } from './shared/services/shared/internationalization.service';
import { TabAuthControlService, APP_NAME } from '@inovacaocmnet/thx-bifrost';

export enum CountryFormatNavigatorEnum {
  Brazil = 'pt-BR',
  UnitedStates = 'en-US',
  Argentina = 'es-AR',
}

export enum CountryFormatMomentEnum {
  Brazil = 'pt-br',
  UnitedStates = 'en-us',
  Argentina = 'es-ar',
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  public static tabId: string;

  constructor(
    @Inject(APP_NAME) private appName: string,
    public translate: InternationalizationService,
    public tabService: TabAuthControlService ) {
    let tabId = this.tabService.getCurrentTabId( this.appName );
    if (!tabId) {
      tabId = this.tabService.generateTabId( this.appName );
    }
    AppComponent.tabId = tabId;
    this.tabService.verifyTabs( this.appName );
  }

  ngOnInit(): void {}
  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHandler(event) {
    this.tabService.removeTab( this.appName , AppComponent.tabId );
  }
}
