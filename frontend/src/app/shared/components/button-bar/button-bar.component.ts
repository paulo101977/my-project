import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { ButtonBarConfig } from './button-bar-config';
import { ButtonBarButtonConfig } from './button-bar-button-config';
import { ButtonBarButtonStatus } from './button-bar-button-status.enum';

@Component({
  selector: 'app-button-bar',
  templateUrl: './button-bar.component.html',
  styleUrls: ['./button-bar.component.scss'],
})
export class ButtonBarComponent implements OnInit, OnChanges {
  @Input() buttonBarConfig: ButtonBarConfig;
  public buttons: Array<ButtonBarButtonConfig>;

  ngOnInit() {
    this.buttons = this.buttonBarConfig.buttons;
  }

  ngOnChanges(changes) {
    this.buttons = this.buttonBarConfig.buttons;
  }

  public clickBtn(button: ButtonBarButtonConfig) {
    if (button.status != ButtonBarButtonStatus.disabled) {
      this.resetStatusButtonList();
      if (button.callBackFunction) {
        button.callBackFunction();
      }
      button.status = ButtonBarButtonStatus.active;
    }
  }

  private resetStatusButtonList() {
    if (this.buttonBarConfig.buttons) {
      this.buttonBarConfig.buttons.forEach(b => {
        if (b.status != ButtonBarButtonStatus.disabled) {
          b.status = ButtonBarButtonStatus.default;
        }
      });
    }
  }
}
