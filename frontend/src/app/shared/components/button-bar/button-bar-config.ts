import { Function } from 'core-js/library/web/timers';
import { ButtonBarColor } from './button-bar-color.enum';
import { ButtonBarButtonConfig } from './button-bar-button-config';

export interface ButtonBarConfig {
  color: ButtonBarColor;
  buttons: Array<ButtonBarButtonConfig>;
}
