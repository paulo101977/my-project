// Models
import { ButtonBarButtonStatus } from 'app/shared/components/button-bar/button-bar-button-status.enum';

export interface ButtonBarButtonConfig {
  id?: number;
  title: string;
  status?: ButtonBarButtonStatus;
  cssClass?: string;
  callBackFunction: Function;
}
