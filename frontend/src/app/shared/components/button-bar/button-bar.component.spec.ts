import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ButtonBarComponent } from './button-bar.component';
import { ButtonBarConfig } from './button-bar-config';
import { ButtonBarButtonConfig } from './button-bar-button-config';
import { ButtonBarButtonStatus } from './button-bar-button-status.enum';

describe('ButtonBarComponent', () => {
  let component: ButtonBarComponent;
  let fixture: ComponentFixture<ButtonBarComponent>;

  const Button = {
    title: 'accountTypeModule.new.singleAccount.naturalPerson',
    status: ButtonBarButtonStatus.default,
    callBackFunction: () => {
      alert('Física');
    },
  } as ButtonBarButtonConfig;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ButtonBarComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ButtonBarComponent);
    component = fixture.componentInstance;

    component.buttonBarConfig = {
      buttons: [Button],
    } as ButtonBarConfig;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should do actions after clickBtn in a button with status default or active', () => {
    spyOn<any>(component, 'resetStatusButtonList');
    spyOn(Button, 'callBackFunction');

    component.clickBtn(Button);

    expect(component['resetStatusButtonList']).toHaveBeenCalled();
    expect(Button.callBackFunction).toHaveBeenCalled();
    expect(Button.status).toEqual(ButtonBarButtonStatus.active);
  });

  it('should do NOT actions after clickBtn in a button with disabled', () => {
    const button = {
      title: 'accountTypeModule.new.singleAccount.naturalPerson',
      status: ButtonBarButtonStatus.disabled,
      callBackFunction: () => {
        alert('Física');
      },
    } as ButtonBarButtonConfig;

    spyOn<any>(component, 'resetStatusButtonList');
    spyOn(button, 'callBackFunction');

    component.clickBtn(button);

    expect(component['resetStatusButtonList']).not.toHaveBeenCalled();
    expect(button.callBackFunction).not.toHaveBeenCalled();
    expect(button.status).toEqual(ButtonBarButtonStatus.disabled);
  });
});
