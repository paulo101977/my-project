import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ElementRef } from '@angular/core';
import { SidebarScrollFixedComponent } from './sidebar-scroll-fixed.component';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

export class MockElementRef extends ElementRef {
  constructor() {
    super(null);
  }
}

describe('SidebarScrollFixedComponent', () => {
  let component: SidebarScrollFixedComponent;
  let fixture: ComponentFixture<SidebarScrollFixedComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [],
      declarations: [SidebarScrollFixedComponent],
      providers: [],
      schemas: [],
    });

    fixture = TestBed.createComponent(SidebarScrollFixedComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should sidebar isFixed', () => {
    spyOn(component.sidebarContainer.nativeElement, 'getBoundingClientRect').and.callFake(() => {
      return { top: 20 };
    });

    component.sidebarFixed();

    expect(component.elementParentFixedTop).toEqual(20);
    expect(component.isFixed).toBeTruthy();
  });

  it('should sidebar is not Fixed', () => {
    spyOn(component.sidebarContainer.nativeElement, 'getBoundingClientRect').and.callFake(() => {
      return { top: 25 };
    });

    component.sidebarFixed();

    expect(component.elementParentFixedTop).toEqual(25);
    expect(component.isFixed).toBeFalsy();
  });
});
