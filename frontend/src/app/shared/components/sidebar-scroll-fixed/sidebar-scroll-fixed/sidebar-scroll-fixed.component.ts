import { AfterViewChecked, Component, ElementRef, Renderer2, ViewChild, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-sidebar-scroll-fixed',
  templateUrl: './sidebar-scroll-fixed.component.html',
  styleUrls: ['./sidebar-scroll-fixed.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class SidebarScrollFixedComponent implements AfterViewChecked {
  @ViewChild('sidebarContainer') sidebarContainer: ElementRef;
  @ViewChild('sidebarContent') sidebarContent: ElementRef;

  public elementParentFixedTop: number;
  public isFixed: boolean;

  constructor(private renderer: Renderer2) {}

  ngAfterViewChecked() {
    this.setVars();
  }

  setVars(): void {
    this.renderer.setStyle(
      this.sidebarContent.nativeElement,
      'width',
      this.sidebarContainer.nativeElement.getBoundingClientRect().width + 'px',
    );
    this.renderer.setStyle(
      this.sidebarContent.nativeElement,
      'height',
      this.sidebarContainer.nativeElement.offsetParent.getBoundingClientRect().height + 'px',
    );
  }

  public sidebarFixed(): void {
    this.elementParentFixedTop = this.sidebarContainer.nativeElement.getBoundingClientRect().top;

    if (this.elementParentFixedTop <= 20) {
      this.isFixed = true;
    } else if (this.elementParentFixedTop >= 21) {
      this.isFixed = false;
    }
  }
}
