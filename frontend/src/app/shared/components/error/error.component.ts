import { Component, DoCheck, forwardRef, Host, Input, OnInit, Optional, SkipSelf } from '@angular/core';
import { AbstractControl, AbstractControlDirective, ControlContainer, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'thx-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ErrorComponent),
      multi: true,
    },
  ],
})
export class ErrorComponent implements OnInit, DoCheck, ControlValueAccessor {
  @Input() formControlName: string;

  @Input() public control: AbstractControlDirective | AbstractControl;

  private errorMessages = {
    required: () => this.translateService.instant('validationError.required'),
    email: () => this.translateService.instant('validationError.email'),
    minlength: params => this.translateService.instant('validationError.minLength', { requiredLength: params.requiredLength }),
    maxlength: params => this.translateService.instant('validationError.maxLength', { requiredLength: params.requiredLength }),
    // TODO: analyze to remove the errors below and unify the keys
    requiredAndMajorThanZero: () => this.translateService.instant('commomData.errorMessages.requiredAndMajorThanZero'),
    notAcceptNegatives: () => this.translateService.instant('commomData.errorMessages.notAcceptNegatives'),
    majorThanZero: () => this.translateService.instant('commomData.errorMessages.majorThanZero'),
    invalidEmail: () => this.translateService.instant('commomData.errorMessages.invalidEmail'),
    matchPassword: () => this.translateService.instant('commomData.errorMessages.matchPassword'),
    onlyNumbers: () => this.translateService.instant('text.onlyNumbers'),
    invalidPortugueseNif: () => this.translateService.instant('validationError.invalidPortugueseNif'),
    invalidDocument: params => this.translateService.instant('validationError.invalidDocument', {document: params.document}),
    emptyString: () => this.translateService.instant('validationError.emptyString')
  };

  public error: { key: string; message: any };

  // ValueAccessor necessary things
  private propagateChange: Function;
  private onTouched: Function;
  private disabled = false;
  private selectedValue: any;

  private errorMessageHandler = (message) => {
    return this.translateService.instant(message);
  }

  constructor(
    @Optional()
    @Host()
    @SkipSelf()
    private controlContainer: ControlContainer,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    if (this.controlContainer && this.formControlName) {
      this.updateFormControl();
    }
  }

  ngDoCheck() {
    if (this.formControlName && this.control != this.controlContainer.control.get(this.formControlName)) {
      this.updateFormControl();
    }

    this.control && this.showErrorMessage();
  }

  updateFormControl() {
    this.control = this.controlContainer.control.get(this.formControlName);
  }

  writeValue(obj: any): void {
    this.selectedValue = obj;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  showErrorMessage() {
    if (this.control.touched && this.control.invalid) {
      const error = this.getFirstError();

      if (error && (!this.error || error != this.error.key)) {
        this.error = {
          key: error,
          message: this.getMessage(error, this.control.errors[error]),
        };
      }
    } else {
      this.error = null;
    }
  }

  private getFirstError(): string {
    return this.getErrors() && this.getErrors()[0];
  }

  private getErrors(): string[] {
    return Object.keys(this.control.errors);
  }

  private getMessage(type: string, params: any) {
    return !this.errorMessages[type]
      ? this.errorMessageHandler(params)
      : this.errorMessages[type] && this.errorMessages[type](params);
  }
}
