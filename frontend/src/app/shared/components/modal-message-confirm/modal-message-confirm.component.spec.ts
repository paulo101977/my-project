import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalMessageConfirmComponent } from './modal-message-confirm.component';
import { SharedService } from '../../services/shared/shared.service';
import { ButtonType } from '../../models/button-config';
import { ModalWrapperComponent } from '../modal-wrapper/modal-wrapper.component';
import { ButtonComponent } from '../button/button.component';
import { TranslateModule } from '@ngx-translate/core';

describe('ModalMessageConfirmComponent', () => {
  let component: ModalMessageConfirmComponent;
  let fixture: ComponentFixture<ModalMessageConfirmComponent>;
  let sharedService: SharedService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateModule.forRoot()],
      declarations: [ ModalMessageConfirmComponent, ModalWrapperComponent, ButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMessageConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    sharedService = TestBed.get(SharedService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set cancelButtonConfig', () => {
    const buttonConfig = sharedService
      .getButtonConfig('modal-message-cancel',
        component.cancel,
        'action.cancel',
        ButtonType.Secondary);

    expect(component['cancelButtonConfig']).toEqual(buttonConfig);
  });

  it('should set confirmButtonConfig', () => {
    const buttonConfig = sharedService
      .getButtonConfig('modal-message-confirm',
        component.confirm,
        'action.confirm',
        ButtonType.Primary);

    expect(component['confirmButtonConfig']).toEqual(buttonConfig);
  });

  it('should emit cancelClicked when cancel fired', () => {
    spyOn(component.cancelClicked, 'emit');

    component.cancel();

    expect(component.cancelClicked.emit).toHaveBeenCalled();
  });

  it('should emit confirmClicked when confirm fired', () => {
    spyOn(component.confirmClicked, 'emit');

    component.confirm();

    expect(component.confirmClicked.emit).toHaveBeenCalled();
  });
});
