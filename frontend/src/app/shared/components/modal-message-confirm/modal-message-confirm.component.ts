import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ButtonConfig, ButtonType } from '../../models/button-config';
import { SharedService } from '../../services/shared/shared.service';

@Component({
  selector: 'app-modal-message-confirm',
  templateUrl: './modal-message-confirm.component.html'
})
export class ModalMessageConfirmComponent implements OnInit {

  @Input() modalId: string;
  @Input() message: string;

  @Output() cancelClicked = new EventEmitter();
  @Output() confirmClicked = new EventEmitter();

  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;

  constructor( private sharedService: SharedService ) { }

  ngOnInit() {

    this.setButtonConfig();
  }

  // Actions
  public cancel = () => {
    this.cancelClicked.emit();
  }

  public confirm = () => {
    this.confirmClicked.emit();
  }

  // Config
  public setButtonConfig() {
    this.cancelButtonConfig = this.sharedService
      .getButtonConfig('modal-message-cancel',
      this.cancel,
        'action.cancel',
        ButtonType.Secondary);

    this.confirmButtonConfig = this.sharedService
      .getButtonConfig('modal-message-confirm',
        this.confirm,
        'action.confirm',
        ButtonType.Primary);
  }
}
