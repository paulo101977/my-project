import { of, of as observableOf } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA, SimpleChange, SimpleChanges } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentFixture, fakeAsync, TestBed, tick, async } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AddressComponent } from './address.component';
import { AddressType } from './../../models/address-type-enum';
import { Country } from './../../models/country';
import { CountryData } from './../../mock-data/country-data';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';
import {
  ADDRESS_FORM,
  ADDRESS_FORM_GROUP,
  COUNTRY,
  DATA,
  GOOGLE_API_DATA,
  LOCATION
} from 'app/shared/components/address/mock-data';
import { propertyServiceStub } from 'app/shared/services/property/testing';
import { configureTestSuite } from 'ng-bullet';
import { Location } from 'app/shared/models/location';

describe('AddressComponent', () => {
  let component: AddressComponent;
  let fixture: ComponentFixture<AddressComponent>;
  const countryList = new Array<Country>();

  for ( let i = 0; i < 21; i++) {
    countryList.push(CountryData);
  }

  const generateCountryList = (): Country[] => {
    const arr: Country[] = [];
    const country = {...DATA[COUNTRY]};

    for ( let i = 0; i < 19; i++ ) {
      if ( i === component['BRAZIL_POSITION'] ) {
        country.translationId = 1000000;
      }

      arr.push(country);
    }

    return arr;
  };

  const enumToArrayValues = (e: any): Array<any> => {
    const arr = [];
    for (const item in e) {
      if (!isNaN(Number(item))) {
        arr.push(item);
      }
    }

    return arr;
  };

  configureTestSuite( () => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(),
        ReactiveFormsModule,
        RouterTestingModule,
        FormsModule,
        CommonModule,
        HttpClientModule,
        CoreModule],
      declarations: [AddressComponent],
      providers: [
        propertyServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(AddressComponent);
    component = fixture.componentInstance;

    spyOn<any>(component.setAddressType, 'emit').and.callThrough();
    spyOn<any>(component['addressResource'], 'getCountry').and.returnValue(observableOf({ items: countryList, hasNext: true}));
    spyOn<any>(component['propertyService'], 'getPropertyCountryCode').and.returnValue('BR');
    component.countrySelectedOption = countryList[0];

    fixture.detectChanges();
  });

  it('should test ngOnInit', () => {
    const addressTypeOptionList = [
      {key: AddressType.Comercial, value: 'sharedModule.components.address.commercial'},
      {key: AddressType.Entrega, value: 'sharedModule.components.address.delivery'},
      {key: AddressType.Cobranca, value: 'sharedModule.components.address.billing'},
      {key: AddressType.Residencial, value: 'sharedModule.components.address.residential'},
      {key: AddressType.Correspondencia, value: 'sharedModule.components.address.correspondence'},
    ];
    const googlePlace = {
      componentRestrictions: {country: 'BR'},
    };
    const positionOfBrazil = 18;

    component.ngOnInit();

    expect(component.addressTypeOptionList).toEqual(addressTypeOptionList);
    expect(component.optionGooglePlaceApi).toEqual(googlePlace);
    expect(component.countriesOptionList).toEqual(countryList);
    expect(component.countrySelectedOption).toEqual(countryList[positionOfBrazil]);
  });

  describe('on set vars', () => {
    beforeEach(() => {
      spyOn<any>(component, 'setCountriesToSelect');
      spyOn<any>(component, 'setAddressTypeList');
      spyOn<any>(component, 'setAddressForm');
      spyOn<any>(component, 'clearRequiredFieldsValidator');
    });

    it('should dont call clearRequiredFieldsValidator', () => {
      component.setVars();
      expect(component['setCountriesToSelect']).toHaveBeenCalled();
      expect(component['setAddressTypeList']).toHaveBeenCalled();
      expect(component['setAddressForm']).toHaveBeenCalled();
      expect(component['clearRequiredFieldsValidator']).not.toHaveBeenCalled();
    });

    it('should call clearRequiredFieldsValidator', () => {
      component.notIsRequiredFields = true;
      component.setVars();
      expect(component['setCountriesToSelect']).toHaveBeenCalled();
      expect(component['setAddressTypeList']).toHaveBeenCalled();
      expect(component['setAddressForm']).toHaveBeenCalled();
      expect(component['clearRequiredFieldsValidator']).toHaveBeenCalled();
    });
  });

  it('should test clearRequiredFieldsValidator', () => {
    const streetNameControl = component['addressForm'].get('streetName');
    const locationCategoryIdControl = component['addressForm'].get('locationCategoryId');
    const event = {emitEvent: false};

    streetNameControl.clearValidators();
    streetNameControl.updateValueAndValidity({emitEvent: false});

    spyOn<any>(locationCategoryIdControl, 'clearValidators');
    spyOn<any>(locationCategoryIdControl, 'updateValueAndValidity');
    spyOn<any>(streetNameControl, 'clearValidators');
    spyOn<any>(streetNameControl, 'updateValueAndValidity');
    spyOn<any>(component['addressForm'], 'updateValueAndValidity');

    component['clearRequiredFieldsValidator']();

    expect(locationCategoryIdControl.clearValidators).toHaveBeenCalled();
    expect(locationCategoryIdControl.updateValueAndValidity).toHaveBeenCalledWith(event);
    expect(streetNameControl.clearValidators).toHaveBeenCalled();
    expect(streetNameControl.updateValueAndValidity).toHaveBeenCalledWith(event);
    expect(component['addressForm'].updateValueAndValidity).toHaveBeenCalled();
  });

  it('should chess method getAddress', () => {
    spyOn<any>(component, 'setAddressByGooglePlace').and.callFake(() => {
    });
    const eventGoogleApi = {
      address_components: null
    };

    component.getAddress(eventGoogleApi);

    expect(component['setAddressByGooglePlace'])
      .not
      .toHaveBeenCalled();

    eventGoogleApi.address_components = {};

    component.getAddress(eventGoogleApi);

    expect(component['setAddressByGooglePlace'])
      .toHaveBeenCalledWith(eventGoogleApi);
  });

  it('should test setAddressByGooglePlace', () => {
    const formData = DATA[ADDRESS_FORM];
    const googleApiData = DATA[GOOGLE_API_DATA];
    const country = DATA[COUNTRY];

    let addressFormValue;

    spyOn<any>(component['getAllInfo'], 'emit').and.callFake(() => {});

    component.emitCountrySubdivision = true;
    component.countrySelectedOption = country;

    component['setAddressByGooglePlace'](googleApiData);

    addressFormValue = component.addressForm.getRawValue();

    expect(component['getAllInfo'].emit).toHaveBeenCalledWith(googleApiData);
    expect(addressFormValue['latitude']).toEqual(googleApiData['geometry']['location']['lat']());
    expect(addressFormValue['longitude']).toEqual(googleApiData['geometry']['location']['lng']());
    expect(addressFormValue['completeAddress']).toEqual(googleApiData['formatted_address']);
    expect(addressFormValue).toEqual(formData);
    expect(addressFormValue['countrySubdivisionId']).toEqual(country.id);
  });

  it('should test chooseCountrySelect', () => {
    component.chooseCountrySelect(CountryData);

    expect(component.countrySelectedOption).toEqual(CountryData);
    expect(component.optionGooglePlaceApi.types).toBeUndefined();
    expect(component.optionGooglePlaceApi.componentRestrictions).toEqual({country: CountryData.countryTwoLetterIsoCode});
    expect(component.address).toEqual('');
  });

  it('should test setAddressForm call mapperAddressToSave on update', fakeAsync(() => {
    component['setAddressForm']();

    spyOn<any>(component['addressChanged'], 'emit').and.callFake(() => {
      component['mapperAddressToSave']();
    });
    spyOn<any>(component, 'mapperAddressToSave').and.callFake( () => new Location() );

    component.addressForm.patchValue({
      postalCode: 1234
    });

    tick();

    expect(component['addressChanged'].emit).toHaveBeenCalled();
    expect(component['mapperAddressToSave']).toHaveBeenCalled();
  }));

  it('should test preSelectCountry', () => {
    spyOn<any>(component, 'patchCountry').and.callFake( () => {} );

    component.countriesOptionList = generateCountryList();

    component.preSelectCountry();

    expect(component['patchCountry'])
      .toHaveBeenCalledWith(
        component
          .countriesOptionList[18]
      );
  });

  it('should test patchCountry with country.id', () => {
    spyOn<any>(component, 'clearAddress').and.callFake( () => {} );
    spyOn<any>(component, 'setCountryGoogleConfig').and.callFake( () => {} );
    spyOn<any>(component, 'updateValidators').and.callFake( () => {} );

    const country = {...DATA[COUNTRY]};

    country.id = 124567;

    component['patchCountry'](country);

    expect(component['clearAddress']).toHaveBeenCalled();
    expect(component['setCountryGoogleConfig']).toHaveBeenCalledWith(country.countryTwoLetterIsoCode);
    expect(component['updateValidators']).toHaveBeenCalledWith(country.countryTwoLetterIsoCode);
    expect(component.countrySelectedOption).toEqual(country);
  });

  it('should test patchCountry without country.id', () => {

    const countryTwoLetterIsoCode = 'EU';
    const country = {...DATA[COUNTRY]};

    country.id = null;

    component.countriesOptionList = generateCountryList();

    component.countriesOptionList[10].countryTwoLetterIsoCode = countryTwoLetterIsoCode;
    country.countryTwoLetterIsoCode = countryTwoLetterIsoCode;

    component['patchCountry'](country);

    expect(component.countrySelectedOption.countryTwoLetterIsoCode)
      .toEqual(country.countryTwoLetterIsoCode);
  });

  it('should test validateOnlyNumbers with valid number', () => {

    const control = { value: '12345678'};

    expect(component.validateOnlyNumbers(control)).toBeNull();
  });

  it('should test validateOnlyNumbers without valid number', () => {

    const control = { value: '12345678aaa123'};

    expect(component.validateOnlyNumbers(control)).not.toBeNull();
  });

  it('should test hasRequiredField with validator', () => {

    const formGroup = DATA[ADDRESS_FORM_GROUP];

    expect(component.hasRequiredField(formGroup.get('postalCode'))).toBeTruthy();
  });

  it('should test hasRequiredField without validator', () => {

    const formGroup = DATA[ADDRESS_FORM_GROUP];

    expect(component.hasRequiredField(formGroup.get('countrySubdivisionId'))).toBeFalsy();
  });

  it('should test removeValidators', () => {

    const form = component.addressForm = DATA[ADDRESS_FORM_GROUP];

    component['removeValidators']();

    expect(component.hasRequiredField(form.get('postalCode'))).toBeFalsy();
  });

  it('should test addValidators', () => {

    const form = component.addressForm = DATA[ADDRESS_FORM_GROUP];

    component['removeValidators']();
    component['addValidators']('BR');

    expect(component.hasRequiredField(form.get('division'))).toBeTruthy();
  });

  it('should test updateValidators with country code equal to BR_COUNTRY_CODE', () => {

    spyOn<any>(component, 'addValidators');
    component['updateValidators']('BR');

    expect(component['addValidators']).toHaveBeenCalled();
  });

  it('should test updateValidators with country code diff of BR_COUNTRY_CODE', () => {

    spyOn<any>(component, 'removeValidators');
    component['updateValidators']('');

    expect(component['removeValidators']).toHaveBeenCalled();
  });

  it('should test getAddressTypeNameById', () => {
    const base = 'sharedModule.components.address.';
    const arr = enumToArrayValues(AddressType);

    const typesResult =
      arr.map( item => component['getAddressTypeNameById'](item));

    expect(typesResult).toContain(`${base}commercial`);
    expect(typesResult).toContain(`${base}residential`);
    expect(typesResult).toContain(`${base}correspondence`);
    expect(typesResult).toContain(`${base}billing`);
    expect(typesResult).toContain(`${base}delivery`);
  });

  it('should test setAddressTypeList', () => {
    component.disabledAddressesType = [AddressType.Correspondencia, AddressType.Cobranca];

    component['setAddressTypeList']();

    expect(component.addressTypeOptionList.length).toEqual(3);
    expect(component.addressTypeOptionList[2].key).toEqual(AddressType.Residencial);
  });

  it('should test patchAddressForm without addressToEdit', () => {
   component.addressForm = DATA[ADDRESS_FORM_GROUP];

    spyOn<any>(component.addressForm, 'reset').and.callFake((r) => r);
    spyOn<any>(component, 'preSelectCountry').and.callFake(() => {} );
    component['patchAddressForm'](null);

    expect(component.addressForm.reset).toHaveBeenCalled();
    expect(component.preSelectCountry).toHaveBeenCalled();
  });

  it('should test patchAddressForm with addressToEdit and country', () => {
    const location = {...DATA[LOCATION]};

    component.addressForm = DATA[ADDRESS_FORM_GROUP];
    component.addressToEdit = location;

    const country = <Country> {
      name: component.addressToEdit.country,
      countryTwoLetterIsoCode: component.addressToEdit.countryCode
    };

    spyOn<any>(component, 'patchCountry').and.callFake((r) => r);
    spyOn<any>(component['addressForm'], 'patchValue').and.callFake((r) => r);
    spyOn<any>(component.addressForm, 'reset').and.callFake((r) => r);
    component['patchAddressForm'](location);

    expect(component['patchCountry']).toHaveBeenCalledWith(country);
    expect(component.addressForm.patchValue).toHaveBeenCalled();
  });

  it('should test patchAddressForm with addressToEdit and without country', () => {
    const location = {...DATA[LOCATION]};

    component.addressToEdit = location;
    component.addressToEdit.country = null;
    component.addressToEdit.countryCode = null;
    component.addressForm = DATA[ADDRESS_FORM_GROUP];


    spyOn<any>(component['addressForm'], 'patchValue').and.callFake((r) => r);
    spyOn<any>(component.addressForm, 'reset').and.callFake((r) => r);
    spyOn<any>(component, 'preSelectCountry').and.callFake(() => {} );
    component['patchAddressForm'](location);

    expect(component.addressForm.patchValue).toHaveBeenCalled();
    expect(component.preSelectCountry).toHaveBeenCalled();
  });

  it('should test ngOnChanges', () => {
    spyOn<any>(component, 'clearAddress').and.callFake( () => {} );
    spyOn<any>(component, 'setAddressTypeList').and.callFake( () => {} );
    spyOn<any>(component, 'patchAddressForm').and.callFake( f => f );

    const location = {...DATA[LOCATION]};
    const addressTypeArr = [AddressType.Correspondencia, AddressType.Cobranca, AddressType.Comercial];


    const changesObj: SimpleChanges = {
      addressToEdit: new SimpleChange({}, location, true),
      disabledAddressesType: new SimpleChange({}, addressTypeArr, true),
    };

    component.ngOnChanges( changesObj );

    expect(component['clearAddress']).toHaveBeenCalled();
    expect(component['setAddressTypeList']).toHaveBeenCalled();
    expect(component['patchAddressForm']).toHaveBeenCalledWith(location);
  });
});
