import { Country } from 'app/shared/models/country';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { FormBuilder, Validators } from '@angular/forms';
import { LocationCategoryEnum } from 'app/shared/models/location-category-enum';
import { Location } from 'app/shared/models/location';

export const ADDRESS_FORM = 'ADDRESS_FORM';
export const GOOGLE_API_DATA = 'GOOGLE_API_DATA';
export const COUNTRY = 'COUNTRY';
export const ADDRESS_FORM_GROUP = 'ADDRESS_FORM_GROUP';
export const LOCATION = 'LOCATION';

export class DATA {
  public static readonly ADDRESS_FORM = {
    additionalAddressDetails: null,
    browserLanguage: window.navigator.language,
    completeAddress: 'Blablabla Blablabla',
    country: 'Brasil',
    countryCode: 'BR',
    countrySubdivisionId: 5933,
    division: 'RJ',
    id: null,
    latitude: 12.221,
    locationCategoryId: '',
    longitude: 22.225,
    neighborhood: 'Recreio dos Bandeirantes',
    ownerId: '00000000-0000-0000-0000-000000000000',
    postalCode: '22795560',
    streetName: 'Rua Sérgio Branco Soares',
    streetNumber: '560',
    subdivision: 'Belo horizonte',
  };

  public static readonly GOOGLE_API_DATA = {
    address_components: [
      {long_name: 'Rua Sérgio Branco Soares', short_name: 'R. Sérgio Branco Soares', types: ['route']},
      {
        long_name: 'Recreio dos Bandeirantes',
        short_name: 'Recreio dos Bandeirantes',
        types: ['sublocality_level_1'],
      },
      {long_name: 'Rio de Janeiro', short_name: 'RJ', types: ['administrative_area_level_1']},
      {long_name: '22795-560', short_name: '22795-560', types: ['postal_code']},
      {long_name: '560', short_name: '560', types: ['street_number']},
      {long_name: 'Rio de Janeiro', short_name: 'RJ', types: ['administrative_area_level_2']},
      {long_name: 'São Paulo', short_name: 'SP', types: ['locality']},
      {long_name: 'Joinville', short_name: 'JV', types: ['sublocality']},
      {long_name: 'Belo horizonte', short_name: 'BH', types: ['administrative_area_level_3']},
      {long_name: 'Brasil', short_name: 'BR', types: ['country']},
    ],
    geometry: {
      location: {
        lat: () => 12.221,
        lng: () => 22.225,
      },

    },
    formatted_address: 'Blablabla Blablabla'
  };

  public static readonly COUNTRY = <Country>{
    translationId: 123,
    twoLetterIsoCode: 'BR',
    countryTwoLetterIsoCode: 'BR',
    name: 'Brasil',
    languageIsoCode: 'pt-br',
    id: 5933,
  };

  public static readonly ADDRESS_FORM_GROUP = new FormBuilder().group({
    id: null,
    ownerId: GuidDefaultData,
    postalCode: [null, [Validators.required]],
    streetName: [null, [Validators.required]],
    streetNumber: [null, [Validators.required]],
    additionalAddressDetails: null,
    division: [{value: null, disabled: true}, [Validators.required]],
    subdivision: [{value: null, disabled: true}, [Validators.required]],
    neighborhood: [null, [Validators.required]],
    browserLanguage: window.navigator.language,
    latitude: null,
    longitude: null,
    countryCode: null,
    country: null,
    completeAddress: null,
    countrySubdivisionId: null,
    locationCategoryId: ['', [Validators.required]]
  });

  public static readonly LOCATION = <Location>{
    recordIdentification: '1234',
    latitude: 15.1,
    longitude: 16.1,
    streetName: 'Rua do medonho',
    streetNumber: '125',
    additionalAddressDetails: 'Rua do medonho',
    postalCode: 20123450,
    countryCode: 'BR',
    ownerId: '123-456',
    browserLanguage: 'pt-br',
    neighborhood: 'Rio de Janeiro',
    cityId: 2304,
    city: 'RJ',
    locationCategoryId: 456,
    locationCategory: {
      name: LocationCategoryEnum.Cobranca,
      recordScope: 'scope',
      id: 456,
    },
    addressTranslation: 'balabalala',
    completeAddress: 'bala blalala',
    subdivision: 'Praca da bandeira',
    division: 'Tijuca',
    country: 'Brazil',
    countrySubdivisionId: '123789',
    id: 56,
  };
}
