import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { SelectObjectOption } from './../../models/selectModel';
import { AddressType } from './../../models/address-type-enum';
import { Country } from './../../models/country';
import { AddressResource } from './../../resources/address/address.resource';
import { TranslateService } from '@ngx-translate/core';
import { ThxPopoverDirective } from '@inovacao-cmnet/thx-ui';
import { Location } from 'app/shared/models/location';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { PropertyService } from 'app/shared/services/property/property.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
})
export class AddressComponent implements OnInit, OnChanges {
  @Input() addressToEdit: Location;
  @Input() isRequired = true;
  @Input() needShowUnicAddress = true;
  @Input() disabledAddressesType: Array<AddressType>;
  @Input() emitCountrySubdivision = false;
  @Input() notIsRequiredFields = false;
  // TODO kill
  @Output() setAddressType = new EventEmitter();
  @Output() getAllInfo = new EventEmitter();
  @Output() addressChanged = new EventEmitter();
  @ViewChild(ThxPopoverDirective) public infoUniquePopover: ThxPopoverDirective;

  public addressForm: FormGroup;
  public addressTypeOptionList: Array<SelectObjectOption>;
  public address: string;
  public optionGooglePlaceApi: any;
  public countriesOptionList: Country[];
  public countrySelectedOption: Country;

  private readonly COUNTRIES_CODES = ['BR', 'PT'];
  private readonly POSTAL_CODE_VALIDATORS = {
    BR: [Validators.minLength(8), Validators.maxLength(8)],
    PT: [Validators.minLength(7), Validators.maxLength(7)],
  };

  constructor(
    private addressResource: AddressResource,
    private translateService: TranslateService,
    private fb: FormBuilder,
    private propertyService: PropertyService,
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    this.clearAddress();
    if (simpleChanges.hasOwnProperty('disabledAddressesType')) {
      this.setAddressTypeList();
    }

    if (simpleChanges.addressToEdit && simpleChanges.addressToEdit.currentValue) {
      this.patchAddressForm(simpleChanges.addressToEdit.currentValue);
    }
  }

  setVars() {
    this.setCountriesToSelect();
    this.setAddressTypeList();
    this.setAddressForm();

    if ( this.notIsRequiredFields ) {
      this.clearRequiredFieldsValidator();
    }
  }

  public clearRequiredFieldsValidator() {
    const { addressForm } = this;
    if ( addressForm ) {
      const  streetNameControl = addressForm.get('streetName');
      const locationCategoryIdControl = addressForm.get('locationCategoryId');

      locationCategoryIdControl.clearValidators();
      locationCategoryIdControl.updateValueAndValidity({emitEvent: false});
      streetNameControl.clearValidators();
      streetNameControl.updateValueAndValidity({emitEvent: false});
      addressForm.updateValueAndValidity();
    }
  }

  public setAddressForm() {
    this.addressForm = this.fb.group({
      id: null,
      ownerId: GuidDefaultData,
      postalCode: [null, [Validators.required]],
      streetName: [null, [Validators.required]],
      streetNumber: [null, [Validators.required]],
      additionalAddressDetails: null,
      division: [{value: null, disabled: true}, [Validators.required]],
      subdivision: [{value: null, disabled: true}, [Validators.required]],
      neighborhood: [null, [Validators.required]],
      browserLanguage: window.navigator.language,
      latitude: null,
      longitude: null,
      countryCode: null,
      country: null,
      completeAddress: null,
      countrySubdivisionId: null,
      locationCategoryId: ['', [Validators.required]]
    });

    this.addressForm.valueChanges
      .subscribe(() => {
        this.addressChanged.emit({
          ...this.mapperAddressToSave(),
          valid: this.addressForm.valid
        });
      });
  }

  public patchAddressForm(addressToEdit: Location) {
    if (!this.addressForm) {
      this.setAddressForm();
    }
    if (addressToEdit) {
      this.addressForm.patchValue(addressToEdit);
      if (this.addressToEdit.country
        && this.addressToEdit.countryCode) {
        const country = <Country> {
          name: this.addressToEdit.country,
          countryTwoLetterIsoCode: this.addressToEdit.countryCode
        };
        this.patchCountry(country);
      } else {
        this.preSelectCountry();
      }
    } else {
      this.addressForm.reset();
      this.preSelectCountry();
    }
  }

  public setAddressTypeList() {
    this.addressTypeOptionList = [];
    for (const type in AddressType) {
      if (!isNaN(Number(type))) {
        const addressType = +type;
        if ( !this.disabledAddressesType || ( this.disabledAddressesType
              && this.disabledAddressesType.indexOf(addressType) < 0 )
          ) {
          this
            .addressTypeOptionList
            .push({
              key: addressType,
              value: this.getAddressTypeNameById(addressType)
            });
        }
      }
    }
  }

  private getAddressTypeNameById(addressType): string {
    switch (+addressType) {
      case AddressType.Comercial:
        return this.translateService.instant('sharedModule.components.address.commercial');
      case AddressType.Residencial:
        return this.translateService.instant('sharedModule.components.address.residential');
      case AddressType.Correspondencia:
        return this.translateService.instant('sharedModule.components.address.correspondence');
      case AddressType.Cobranca:
        return this.translateService.instant('sharedModule.components.address.billing');
      case AddressType.Entrega:
        return this.translateService.instant('sharedModule.components.address.delivery');
    }
  }

  public setCountriesToSelect() {
    this.addressResource.getCountry().subscribe(result => {
      this.countriesOptionList = result.items;
      this.preSelectCountry();
    });
  }

  public preSelectCountry() {
    if ( this.countriesOptionList ) {
      const countryCode = this.propertyService.getPropertyCountryCode();

      const selectedCountry = this
        .countriesOptionList
        .find( item => item.countryTwoLetterIsoCode === countryCode) || new Country();
      this.patchCountry(selectedCountry);
    }
  }

  public getAddress(eventGoogleApi: any) {
    if (eventGoogleApi && eventGoogleApi.address_components) {
      this.setAddressByGooglePlace(eventGoogleApi);
    }
  }

  public setAddressByGooglePlace(place: any) {
    this.getAllInfo.emit(place);
    let objPlaceToForm = {};
    if (this.emitCountrySubdivision && this.countrySelectedOption) {
      objPlaceToForm = {
        ...objPlaceToForm,
        countrySubdivisionId: this.countrySelectedOption.id
      };
    }
    objPlaceToForm = {
      ...objPlaceToForm,
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      completeAddress: place.formatted_address
    };
    place['address_components'].forEach(component => {
      switch (component.types[0]) {
        case 'route':
          objPlaceToForm = {...objPlaceToForm, streetName: component.long_name};
          break;
        case 'sublocality_level_1':
          objPlaceToForm = {...objPlaceToForm, neighborhood: component.long_name};
          break;
        case 'administrative_area_level_1':
          objPlaceToForm = {...objPlaceToForm, division: component.short_name};
          break;
        case 'administrative_area_level_2':
        case 'locality':
        case 'sublocality':
        case 'administrative_area_level_3':
          objPlaceToForm = {...objPlaceToForm, subdivision: component.long_name};
          break;
        case 'postal_code':
          let cepNumber = '';
          if (component.long_name) {
            cepNumber = component.long_name.replace('-', '');
          }
          objPlaceToForm = {...objPlaceToForm, postalCode: cepNumber};
          break;
        case 'country':
          objPlaceToForm = {...objPlaceToForm, countryCode: component.short_name, country: component.long_name};
          break;
        case 'street_number':
          objPlaceToForm = {...objPlaceToForm, streetNumber: component.short_name};
          break;
      }
    });
    this.addressForm.patchValue(objPlaceToForm);
  }

  public chooseCountrySelect(country) {
    this.addressForm.patchValue({
      postalCode: null,
      streetName: null,
      streetNumber: null,
      additionalAddressDetails: null,
      division: null,
      subdivision: null,
      neighborhood: null,
      latitude: null,
      longitude: null,
      completeAddress: null,
      countrySubdivisionId: null
    });
    this.patchCountry(country);
  }

  public setCountryGoogleConfig(countryTwoLetterIsoCode: string) {
    this.optionGooglePlaceApi = {
      componentRestrictions: {country: countryTwoLetterIsoCode},
    };
  }

  public patchCountry(country: Country) {
    if (!country.id) {
      this.countrySelectedOption = this.countriesOptionList
        .find(option => {
          return option.countryTwoLetterIsoCode.toLocaleLowerCase() ==
            country.countryTwoLetterIsoCode.toLocaleLowerCase();
        });
    } else {
      this.countrySelectedOption = country;
    }
    this.addressForm.patchValue({
      countryCode: this.countrySelectedOption.countryTwoLetterIsoCode,
      country: this.countrySelectedOption.name,
    });
    this.clearAddress();
    this.setCountryGoogleConfig(country.countryTwoLetterIsoCode);
    this.updateValidators(country.countryTwoLetterIsoCode);
  }

  public clearAddress() {
    this.address = '';
  }

  hidePopover() {
    this.infoUniquePopover.hidePopover();
  }

  public mapperAddressToSave() {
    return <Location> {
      ...this.addressForm.getRawValue(),
      locationCategory: {
        id: this.addressForm.get('locationCategoryId').value,
        name: this.getAddressTypeNameById(this.addressForm.get('locationCategoryId').value)
      }
    };
  }

  public updateValidators(countryCode: string) {
    !this.COUNTRIES_CODES.includes(countryCode)
      ? this.removeValidators()
      : this.addValidators(countryCode);
  }

  public validateOnlyNumbers = (control) => {
    if (control && control.value) {
      const isValid = control.value.match( '^[0-9]+$');
      return isValid ? null : {onlyNumbers: 'invalid'};
    }
    return null;
  }

  public addValidators(countryCode: string) {
    this.addressForm.get('postalCode')
      .setValidators([
        Validators.required,
        this.validateOnlyNumbers,
        ...(this.POSTAL_CODE_VALIDATORS[countryCode] || []),
      ]);
    this.addressForm.get('postalCode').updateValueAndValidity();
    this.addressForm.get('streetNumber').setValidators([Validators.required]);
    this.addressForm.get('streetNumber').updateValueAndValidity();
    this.addressForm.get('division').setValidators([Validators.required]);
    this.addressForm.get('division').updateValueAndValidity();
    this.addressForm.get('subdivision').setValidators([Validators.required]);
    this.addressForm.get('subdivision').updateValueAndValidity();
    this.addressForm.get('neighborhood').setValidators([Validators.required]);
    this.addressForm.get('neighborhood').updateValueAndValidity();
  }

  public removeValidators() {
    this.addressForm.get('postalCode').clearValidators();
    this.addressForm.get('postalCode').updateValueAndValidity();
    this.addressForm.get('streetNumber').clearValidators();
    this.addressForm.get('streetNumber').updateValueAndValidity();
    this.addressForm.get('division').clearValidators();
    this.addressForm.get('division').updateValueAndValidity();
    this.addressForm.get('subdivision').clearValidators();
    this.addressForm.get('subdivision').updateValueAndValidity();
    this.addressForm.get('neighborhood').clearValidators();
    this.addressForm.get('neighborhood').updateValueAndValidity();
  }

  public hasRequiredField(abstractControl: AbstractControl): boolean {
    if (abstractControl.validator) {
      const validator = abstractControl.validator({}as AbstractControl);
      if (validator && validator.required) {
        return true;
      }
    }
    return false;
  }

}
