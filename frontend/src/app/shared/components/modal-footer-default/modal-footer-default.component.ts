import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ButtonConfig, ButtonType } from '../../models/button-config';
import { SharedService } from '../../services/shared/shared.service';

@Component({
  selector: 'app-modal-footer-default',
  templateUrl: './modal-footer-default.component.html'
})
export class ModalFooterDefaultComponent implements OnInit, OnChanges {

  @Input() modalId: string;
  @Input() cancelButtonEnabled = true;
  @Input() confirmButtonEnabled = true;

  @Output() cancel = new EventEmitter();
  @Output() confirm = new EventEmitter();


  // Buttons Config
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;

  constructor( private sharedService: SharedService ) { }

  ngOnInit() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      `${this.modalId}-cancel`,
      this.cancelModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.cancelButtonConfig.isDisabled = !this.cancelButtonEnabled;

    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      `${this.modalId}-confirm`,
      this.confirmModal,
      'action.confirm',
      ButtonType.Primary
    );
    this.confirmButtonConfig.isDisabled = !this.confirmButtonEnabled;

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.cancelButtonConfig) {
      this.cancelButtonConfig.isDisabled = !this.cancelButtonEnabled;
    }
    if (this.confirmButtonConfig) {
      this.confirmButtonConfig.isDisabled = !this.confirmButtonEnabled;
    }
  }

  cancelModal = () => {
    this.cancel.emit();
  }

  confirmModal = () => {
    this.confirm.emit();
  }
}
