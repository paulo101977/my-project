import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFooterDefaultComponent } from './modal-footer-default.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('ModalFooterDefaultComponent', () => {
  let component: ModalFooterDefaultComponent;
  let fixture: ComponentFixture<ModalFooterDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, FormsModule, ReactiveFormsModule, TranslateModule.forRoot(), RouterTestingModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFooterDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
