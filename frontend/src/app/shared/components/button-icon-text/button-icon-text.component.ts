import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-button-icon-text',
  templateUrl: 'button-icon-text.component.html',
})
export class ButtonIconTextComponent {
  @Input() btnText: string;

  @Input() altImg: string;

  @Input() svgName: string;

  @Input() pathLink: string;
}
