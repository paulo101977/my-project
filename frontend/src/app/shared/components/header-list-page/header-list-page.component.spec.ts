import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { HttpClientModule } from '@angular/common/http';
import { HeaderListPageComponent } from './header-list-page.component';
import { HeaderPageComponent } from '../../components/header-page/header-page.component';
import { HeaderCreatePageComponent } from './../header-create-page/header-create-page.component';
import { HeaderEditPageComponent } from './../header-edit-page/header-edit-page.component';
import { FilterSearchComponent } from './../filter-search/filter-search.component';
import { ConfigHeaderPage, SizeOfSearchButtonEnum } from './../../models/config-header-page';
import { HeaderPageEnum } from './../../models/header-page-enum';
import { CoreModule } from '@app/core/core.module';

describe('HeaderListPageComponent', () => {
  let component: HeaderListPageComponent;
  let fixture: ComponentFixture<HeaderListPageComponent>;
  let configHeaderPage: ConfigHeaderPage;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [

        FormsModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        MomentModule,
        ReactiveFormsModule,
        HttpClientModule,
        CoreModule
      ],
      declarations: [
        HeaderPageComponent,
        HeaderCreatePageComponent,
        HeaderEditPageComponent,
        HeaderListPageComponent,
        FilterSearchComponent,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderListPageComponent);
    component = fixture.componentInstance;
    configHeaderPage = new ConfigHeaderPage(HeaderPageEnum.Edit);
    configHeaderPage.titlePage = 'Title Page';
    configHeaderPage.titleCreateButton = 'Title Create Button';
    configHeaderPage.sizeOfSearchButtonEnum = SizeOfSearchButtonEnum.Medium;
    configHeaderPage.goToCreatePage = function() {};
    spyOn(component, 'getConfig').and.returnValue(configHeaderPage);
    fixture.detectChanges();
  });

  it('should create HeaderListComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should get titlePage', () => {
    expect(component.titlePage).toEqual(configHeaderPage.titlePage);
  });

  it('should get titleCreateButton', () => {
    expect(component.titleCreateButton).toEqual(configHeaderPage.titleCreateButton);
  });

  it('should get sizeOfSearchButtonEnum', () => {
    expect(component.sizeOfSearchButtonEnum).toEqual(configHeaderPage.sizeOfSearchButtonEnum);
  });

  it('should run goToCreatePage', () => {
    spyOn(component, 'goToCreatePage');
    component.runCallbackFunction();
    expect(component.goToCreatePage).toHaveBeenCalled();
  });

  it('should back preview page', () => {
    spyOn(component.location, 'back');
    component.goBackPreviewPage();
    expect(component.location.back).toHaveBeenCalled();
  });
});
