import { Component, Input, OnChanges, Output, OnInit, EventEmitter, SimpleChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ConfigHeaderPage } from './../../models/config-header-page';
import { SizeOfSearchButtonEnum } from './../../models/config-header-page';
import { Action } from './../../models/action';
import { HeaderPageEnum } from './../../models/header-page-enum';

@Component({
  selector: 'app-header-list-page',
  templateUrl: './header-list-page.component.html',
})
export class HeaderListPageComponent implements OnInit, OnChanges {
  @Input() config: ConfigHeaderPage;
  @Input() searchableItems: any;
  @Output() searchUpdated = new EventEmitter();

  public titlePage: string;
  public titleCreateButton: string;
  public goToCreatePage: Function;
  public sizeOfSearchButtonEnum: SizeOfSearchButtonEnum;

  public configEvent: ConfigHeaderPage;

  public referenceToHeaderPageEnum: any = HeaderPageEnum;
  public referenceToSizeOfSearchButtonEnum: any = SizeOfSearchButtonEnum;

  constructor(public translateService: TranslateService, public _route: ActivatedRoute, public location: Location) {}

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setVars();
  }

  setVars() {
    this.configEvent = this.getConfig(this.config);
    if (this.configEvent != null) {
      this.titlePage = this.translateService.instant(this.configEvent.titlePage);
      this.titleCreateButton = this.translateService.instant(this.configEvent.titleCreateButton);
      this.goToCreatePage = this.configEvent.goToCreatePage;
      this.sizeOfSearchButtonEnum = this.configEvent.sizeOfSearchButtonEnum;
      this.updateItems = this.updateItems;
    }
  }

  public getConfig(config: ConfigHeaderPage) {
    return config;
  }

  public runCallbackFunction() {
    this.goToCreatePage();
  }

  public updateItems(items: Array<any>): void {
    this.searchUpdated.emit(items);
  }

  public goBackPreviewPage() {
    this.location.back();
  }
}
