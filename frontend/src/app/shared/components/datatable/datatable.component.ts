import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { DataTableGlobals } from '../../models/DataTableGlobals';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'thx-datatable',
  templateUrl: './datatable.component.html',
})
export class DatatableComponent implements OnInit {
  @ViewChild('defaultTemplate') defaultTemplate: TemplateRef<any>;
  @ViewChild('statusTemplate') statusTemplate: TemplateRef<any>;
  @ViewChild('optionsTemplate') optionsTemplate: TemplateRef<any>;
  @ViewChild('defaultLastCellTemplate') defaultLastCellTemplate: TemplateRef<any>;
  @ViewChild('editLastCellTemplate') editLastCellTemplate: TemplateRef<any>;
  @ViewChild('deletLastCellTemplate') deletLastCellTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
  @ViewChild('dateTemplate') dateTemplate: TemplateRef<any>;

  @Input() columnMode = 'flex';
  @Input() tableHasScroll = false;
  @Input()
  set itens(itens: Array<any>) {
    if (itens) {
      this._itens = [...itens];
      this.checkColumnsLastCell();
    }
  }

  get itens() {
    return this._itens;
  }

  @Input() statusIdentifier: string;
  @Input() statusTitle = 'label.active';
  @Input() statusPropName = 'isActive';
  @Input() itemsOption: Array<any>;
  @Input() rowHeight = 45;
  @Input() headerHeight = 45;
  @Input() footerHeight = 50;
  @Input() emptyMessage: string;
  @Input() emptyMessageContent: string;
  @Input() completeEmptyMessageContent: string;
  @Input() customTemplate: TemplateRef<any>;
  @Input() cellClass = 'no-submenu-options';
  @Input() hasLastCell = true;
  @Input() rowsLimit: number;
  @Input() tableExtraClass: string;
  @Input() editModeOption: boolean;
  @Input() deleteModeOption: boolean;
  @Input() selectionType: string;
  @Input() offset = 0;
  @Input() count: number;
  @Input() externalPaging = false;
  @Input() footerInfo: TemplateRef<any>;

  @Output() statusChanged: EventEmitter<any> = new EventEmitter();
  @Output() rowItemClicked: EventEmitter<any> = new EventEmitter();
  @Output() emptyLinkClicked: EventEmitter<any> = new EventEmitter();
  @Output() editClicked: EventEmitter<any> = new EventEmitter();
  @Output() deleteClicked: EventEmitter<any> = new EventEmitter();
  @Output() page: EventEmitter<any> = new EventEmitter();


  private _columns: Array<any>;
  private _itens: Array<any>;

  private _selected = [];

  @Input()
  set columns(columns: Array<any>) {
    this._columns = [];
    if (!columns) {
      return;
    }

    const firstCell = this.getFirstCell();
    const cells = this.getCells(columns);
    const lastCell = this.getLastCell();

    this._columns = [
      ...(firstCell ? [firstCell] : []),
      ...cells,
      ...(lastCell ? [lastCell] : [])
    ];
  }

  get columns() {
    return this._columns;
  }

  constructor(private dataTableGlobals: DataTableGlobals, public translateService: TranslateService) {
    if (!this.rowsLimit) {
      this.rowsLimit = dataTableGlobals.limitItensPage;
    }
  }

  ngOnInit() {}

  public itemWasUpdated($event): void {
    this.statusChanged.emit($event);
  }

  public runCallbackFunction(item: any, element: any, index: any) {
    if (item.callbackFunction) {
      item.callbackFunction(element, item.context, index);
    }
  }

  public goToCreatePage() {
    this.emptyLinkClicked.emit();
  }

  public clickRow(item) {
    if (event.type == 'click') {
      if (item.column.cellTemplate != this.statusTemplate && item.column.cellTemplate != this.optionsTemplate) {
        this.rowItemClicked.emit(item.row);
      }
    }
  }

  public edit(element) {
    this.editClicked.emit(element);
  }

  public delete(element) {
    this.deleteClicked.emit(element);
  }

  private getFirstCell() {
    if (this.statusIdentifier) {
      return {
        cellTemplate: this.statusTemplate,
        headerTemplate: this.headerTemplate,
        name: this.statusTitle,
        prop: this.statusPropName,
        width: 100,
        draggable: false,
        canAutoResize: false,
        headerClass: 'thf-u-text-center',
        cellClass: 'thf-u-text-center',
      };
    }

    if (this.customTemplate) {
      return {
        cellTemplate: this.customTemplate,
      };
    }
  }

  private getCells(columns: Array<any>) {
    return columns.map(col => {
      const aux = col;
      if (!aux.hasOwnProperty('headerTemplate')) {
        aux.headerTemplate = this.headerTemplate;
      }
      if (!aux.hasOwnProperty('cellTemplate')) {
        aux.cellTemplate = this.defaultTemplate;
      }
      if (!col.hasOwnProperty('flexGrow')) {
        aux.flexGrow = 1;
      }
      if (col.hasOwnProperty('type') && col.type == 'date') {
        aux.cellTemplate = this.dateTemplate;
      }
      return aux;
    });
  }

  private getLastCell() {
    if (!this.hasLastCell) {
      return;
    }

    const lastCellBase = {
      name: '',
      cellClass: 'thf-u-no-padding',
      width: 50,
      resizeable: false,
      draggable: false,
      canAutoResize: false,
    };
    let lastCell = {};

    if (this.checkTableOptions()) {
      lastCell = {
        cellTemplate: this.optionsTemplate,
      };
    } else if (this.editModeOption) {
      lastCell = {
        cellTemplate: this.editLastCellTemplate,
      };
    } else if (this.deleteModeOption) {
      lastCell = {
        cellTemplate: this.deletLastCellTemplate,
      };
    } else {
      lastCell = {
        cellTemplate: this.defaultLastCellTemplate,
        cellClass: '',
        width: 80
      };
    }

    return { ...lastCellBase, ...lastCell };
  }

  private checkTableOptions(): boolean {
    if (!this.itens) {
      return false;
    }

    const itemsHaveOptions = this.itens.some((item) => item.options);

    return itemsHaveOptions || !!this.itemsOption;
  }

  private checkColumnsLastCell(): void {
    if (!this.hasLastCell || !this.columns || !this.columns.length) {
      return;
    }

    const lastCellIndex = this.columns.length - 1;
    this.columns[lastCellIndex] = this.getLastCell();
  }
}
