import { Component, OnInit, Input } from '@angular/core';
import { SlimboxConfig } from '../../models/slimbox-config';

@Component({
  selector: 'app-slimbox',
  templateUrl: 'slimbox.component.html',
  styleUrls: ['slimbox.component.css'],
})
export class SlimboxComponent implements OnInit {
  public hasFooter: boolean;
  public hasHeader: boolean;
  public type: string;

  @Input() slimboxConfig: SlimboxConfig;
  @Input() slimboxStyleClass: string;

  ngOnInit() {
    if (this.slimboxConfig) {
      this.hasFooter = this.slimboxConfig.showFooter;
      this.hasHeader = this.slimboxConfig.showHeader;
      this.type = this.slimboxConfig.type ? this.slimboxConfig.type : 'default';
    }
  }
}
