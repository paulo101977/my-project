// Angular import
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BillingAccountForClosure } from '../../models/billing-account/billing-account-for-closure';
import { BillingAccountStatusEnum } from './../../models/billing-account/billing-account-status-enum';

@Component({
  selector: 'list-checkbox-with-icon',
  templateUrl: 'list-checkbox-with-icon.component.html',
  styleUrls: ['list-checkbox-with-icon.component.css'],
})
export class ListCheckboxWithIconComponent {
  @Input() billingAccountForClosureList: Array<BillingAccountForClosure>;
  @Input() showOwnerName: boolean;
  @Output() numberAccountsChecked = new EventEmitter();

  public changeCheckedStatus(billingAccountForClosure: BillingAccountForClosure) {
    this.getCheckedStatus(billingAccountForClosure.isChecked);

    this.numberAccountsChecked.emit(this.filterBillingAccountForClosureCheckedList(this.billingAccountForClosureList));
  }

  private filterBillingAccountForClosureCheckedList(billingAccountForClosureList: Array<BillingAccountForClosure>) {
    const billingAccountForClosureCheckedList = billingAccountForClosureList.filter(
      billingAccountForClosure => billingAccountForClosure.isChecked,
    );

    return billingAccountForClosureCheckedList;
  }

  public getCheckedStatus(status: boolean) {
    return status;
  }
}
