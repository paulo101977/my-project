import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ImgCdnTestingModule } from '@inovacao-cmnet/thx-ui';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { UserLanguageSelectionComponent } from './user-language-selection.component';

describe('UserLanguageSelectionComponent', () => {
  let component: UserLanguageSelectionComponent;
  let fixture: ComponentFixture<UserLanguageSelectionComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLanguageSelectionComponent ],
      imports: [
        CommonModule,
        ImgCdnTestingModule,
        TranslateTestingModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(UserLanguageSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLanguageSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
