export enum LanguageNamesEnum {
  'US' = 'English',
  'BR' = 'Português Brasil',
  'PT' = 'Português - Portugal',
  'AR' = 'Español'
}
