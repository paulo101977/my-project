import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserLanguageSelectionComponent } from './user-language-selection.component';
import { ThxPopoverBaseModule } from '../../../../../projects/ui/src/lib/components/molecules/thx-popover-base/thx-popover-base.module';
import { ImgCdnModule } from '@inovacao-cmnet/thx-ui';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [ UserLanguageSelectionComponent ],
  imports: [
    CommonModule,
    ThxPopoverBaseModule,
    ImgCdnModule,
    TranslateModule
  ],
  exports: [ UserLanguageSelectionComponent ]
})
export class UserLanguageModule { }
