import {Component, Input, Output, EventEmitter, HostListener, ElementRef} from '@angular/core';
import { LanguageNamesEnum } from './language-names-enum';

@Component({
  selector: 'app-user-language-selection',
  templateUrl: './user-language-selection.component.html',
  styleUrls: ['./user-language-selection.component.scss']
})
export class UserLanguageSelectionComponent {

  constructor(
    private elementRef: ElementRef
  ) { }

  public languageNamesEnum = LanguageNamesEnum;
  public popoverVisible = false;
  @Output() updateUserLanguage = new EventEmitter();
  @Input() languageList: any[];
  @Input() currentLanguage: LanguageNamesEnum;

  getSelectedLanguage(languageKey: string) {
    this.updateUserLanguage.emit(languageKey);
    this.popoverControl();
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.popoverVisible = false;
    }
  }

  public popoverControl() {
    this.popoverVisible = !this.popoverVisible;
  }

}
