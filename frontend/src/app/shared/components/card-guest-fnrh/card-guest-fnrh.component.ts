import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { GuestStatus } from 'app/shared/models/reserves/guest';

@Component({
  selector: 'app-card-guest-fnrh',
  templateUrl: './card-guest-fnrh.component.html',
})
export class CardGuestFnrhComponent implements OnInit, AfterViewInit {
  @ViewChild('checkbox') checkbox;

  @Input() index: number;
  @Input() guest: any;

  @Output()
  insertOrRemoveGuestReservationItemForCheckinEvent = new EventEmitter<CheckinGuestReservationItemDto>();

  public guestCard: GuestCard;

  constructor(private translateService: TranslateService) {}

  ngOnInit() {
    this.getItem(this.guest);
  }

  ngAfterViewInit(): void {
    if (this.checkbox) {
      if (!this.checkbox.nativeElement.checked) {
        this.insertOrRemoveGuestReservationItemForCheckinEvent.emit(this.guestCard.guest);
      }
    }
  }

  private getItem(item) {
    this.guestCard = item;
    this.verifyIfGuestExists(item);
  }

  private verifyIfGuestExists(item: GuestCard) {
    const guest = item.guest;
    guest.guestReservationItemName = guest.guestReservationItemName
      ? guest.guestReservationItemName
      : this.translateService.instant('checkinModule.guestFnrhcreateEdit.guestList.guestReservationItemName');
    guest.subdivision = guest.subdivision
      ? guest.subdivision
      : this.translateService.instant('checkinModule.guestFnrhcreateEdit.guestList.subdivision');
    guest.division = guest.division
      ? guest.division
      : this.translateService.instant('checkinModule.guestFnrhcreateEdit.guestList.division');
    guest.country = guest.country ? guest.country : this.translateService.instant('checkinModule.guestFnrhcreateEdit.guestList.country');
    guest.businessSourceName = guest.businessSourceName
      ? guest.businessSourceName
      : this.translateService.instant('checkinModule.guestFnrhcreateEdit.guestList.businessSourceName');
  }

  public insertOrRemoveGuestReservationItemForCheckin() {
    this.insertOrRemoveGuestReservationItemForCheckinEvent.emit(this.guestCard.guest);
  }

  public hasStatusCheckin(card) {
    if (card && card.guest && card.guest.guestStatusId == GuestStatus.Checkin) {
      return true;
    }
    return false;
  }
}
