import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'ngx-moment';
import { CardGuestFnrhComponent } from './card-guest-fnrh.component';
import { CoreModule } from '@app/core/core.module';
import { GuestStatus } from 'app/shared/models/reserves/guest';


const auxFN = (fixture, fn: Function) => {
  fixture.detectChanges();

  fixture.whenStable().then( () => fn() );
};

describe('CardEntranceGuestFnrhComponent', () => {
  let component: CardGuestFnrhComponent;
  let fixture: ComponentFixture<CardGuestFnrhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        MomentModule,
        CoreModule
      ],
      declarations: [CardGuestFnrhComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

  }));

  beforeEach( () => {
    fixture = TestBed.createComponent(CardGuestFnrhComponent);
    component = fixture.componentInstance;

    component.index = 0;
    component.guest = {
      guest: {
        guestReservationItemName: '',
        fnrhDocumentBirthDate: new Date('2017/12/4'),
        documentTypeName: 'CPF',
        documentInformation: '343.343.554-55',
        subdivision: '',
        division: 'RJ',
        country: 'Brasil',
        businessSourceName: 'booking.com',
        isValid: false,
        guestReservationItemId: 'card-id',
      },
      active: false,
      isValid: false,
      isChild: false,
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should getItem', () => {
    spyOn<any>(component, 'verifyIfGuestExists');

    component['getItem'](component.guest);

    expect(component.guestCard).toBeTruthy(component.guest);
    expect(component['verifyIfGuestExists']).toHaveBeenCalledWith(component.guest);
  });

  it('should verifyIfGuestExists', () => {
    component['verifyIfGuestExists'](component.guestCard);

    expect(component.guestCard.guest.guestReservationItemName).toEqual(
      'checkinModule.guestFnrhcreateEdit.guestList.guestReservationItemName',
    );
    expect(
      component.guestCard.guest.subdivision
    ).toEqual('checkinModule.guestFnrhcreateEdit.guestList.subdivision');
  });

  it('should insertOrRemoveGuestReservationItemForCheckin', () => {
    spyOn(component['insertOrRemoveGuestReservationItemForCheckinEvent'], 'emit');

    component.insertOrRemoveGuestReservationItemForCheckin();

    expect(component['insertOrRemoveGuestReservationItemForCheckinEvent'].emit)
      .toHaveBeenCalledWith(component.guestCard.guest);
  });

  describe('on render', () => {
    describe('on input checkbox render', () => {
      let input, inputId, label;

      beforeEach( () => {
        component.index = 0;
        inputId = 'guest-' + component.index;
      });

      it('should test if hidden input', (done) => {
        component.guest.guest.guestStatusId = GuestStatus.Checkin;

        auxFN(fixture, () => {
          input = fixture.nativeElement.querySelector(`#${inputId}`);
          label = fixture.nativeElement.querySelector(`label[for="${inputId}"]`);

          expect(input.attributes.hidden).toBeTruthy();
          expect(label.attributes.hidden).toBeTruthy();
          done();
        });
      });

      it('should test if not hidden input', (done) => {
        component.guest.guest.guestStatusId = GuestStatus.Canceled;

        auxFN(fixture, () => {
          input = fixture.nativeElement.querySelector(`#${inputId}`);
          label = fixture.nativeElement.querySelector(`label[for="${inputId}"]`);

          expect(input.attributes.hidden).toBeFalsy();
          expect(label.attributes.hidden).toBeFalsy();
          done();
        });
      });

      it('should test input click', (done) => {
        spyOn(component, 'insertOrRemoveGuestReservationItemForCheckin').and.callThrough();
        spyOn(component['insertOrRemoveGuestReservationItemForCheckinEvent'], 'emit');
        component.guest.guest.guestStatusId = GuestStatus.Canceled;

        input = fixture.nativeElement.querySelector(`#${inputId}`);

        input.click();


        expect(component.insertOrRemoveGuestReservationItemForCheckin).toHaveBeenCalled();
        expect(component['insertOrRemoveGuestReservationItemForCheckinEvent'].emit)
          .toHaveBeenCalledWith(component.guestCard.guest);
        done();
      });
    });

    describe('on image render', () => {
      const imgContainerId = '#guest-card-img-container';

      it('should test img is render', (done) => {
        const imgContainer = fixture.nativeElement.querySelector(imgContainerId);

        const img1 = imgContainer.children[0];
        const img2 = imgContainer.children[1];

        expect(img1.src).toContain('person-gray.svg');
        expect(img2.src).toContain('alert-master-details.svg');
        done();
      });

      it('should test img with card active', (done) => {
        component.guestCard.active = true;

        auxFN(fixture, () => {
          const imgContainer = fixture.nativeElement.querySelector(imgContainerId);
          const img1 = imgContainer.children[0];
          const img2 = imgContainer.children[1];

          expect(img1.src).toContain('person-white.svg');
          expect(img1.classList).toContain('thf-u-filter-svg--white');
          expect(img2.src).toContain('alert-master-details.svg');
          done();
        });
      });


      it('should test img with card active and isChild', (done) => {
        component.guestCard.active = true;
        component.guestCard.guest.isChild = true;

        auxFN(fixture, () => {
          const imgContainer = fixture.nativeElement.querySelector(imgContainerId);
          const img1 = imgContainer.children[0];
          const img2 = imgContainer.children[1];

          expect(img1.src).toContain('child_icon.svg');
          expect(img1.classList).toContain('thf-u-filter-svg--white');
          expect(img2.src).toContain('alert-master-details.svg');
          done();
        });
      });

      it('should test img with card active, isChild and isValid', (done) => {
        component.guestCard.active = true;
        component.guestCard.guest.isValid = true;
        component.guestCard.guest.isChild = true;

        auxFN(fixture, () => {
          const imgContainer = fixture.nativeElement.querySelector(imgContainerId);
          const img1 = imgContainer.children[0];
          const img2 = imgContainer.children[1];

          expect(img1.src).toContain('child_icon.svg');
          expect(img1.classList).toContain('thf-u-filter-svg--white');
          expect(img2.src).toContain('check-circle-outline-blue.svg');
          expect(img2.classList).toContain('thf-u-filter-svg--white');
          done();
        });
      });
    });
  });
});
