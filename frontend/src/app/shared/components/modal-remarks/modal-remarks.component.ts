import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ModalRemarksFormEmitService } from './../../services/shared/modal-remarks-form-emit.service';

import { ReservationRemark } from './../../models/reserves/reservation-remark';

@Component({
  selector: 'app-modal-remarks',
  templateUrl: './modal-remarks.component.html',
  styleUrls: ['./modal-remarks.component.css'],
})
export class ModalRemarksComponent {
  @Input() readonly: boolean;
  @Output() reservationRemarkUpdated = new EventEmitter();
  public reserveRemark: ReservationRemark;
  public reserveRemarkOld: ReservationRemark;
  public isVisible: boolean;

  constructor(public modalRemarksFormEmitService: ModalRemarksFormEmitService) {
    this.reserveRemark = new ReservationRemark();
    this.reserveRemarkOld = new ReservationRemark();
    modalRemarksFormEmitService.changeEmitted$.subscribe(response => {
      this.reserveRemark.internalComments = response[0];
      this.reserveRemark.externalComments = response[1];
      this.reserveRemark.partnerComments = response[2];

      this.reserveRemarkOld.internalComments = response[0];
      this.reserveRemarkOld.externalComments = response[1];
      this.reserveRemarkOld.partnerComments = response[2];
      this.showModal();
    });
  }

  public showModal() {
    this.isVisible = true;
  }

  public closeModal() {
    this.reserveRemark.internalComments = this.reserveRemarkOld.internalComments;
    this.reserveRemark.externalComments = this.reserveRemarkOld.externalComments;
    this.isVisible = false;
  }

  public saveRemarks() {
    this.reservationRemarkUpdated.emit(this.reserveRemark);
    this.isVisible = false;
  }
}
