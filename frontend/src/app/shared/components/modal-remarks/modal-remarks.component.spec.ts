import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';

import { ModalRemarksComponent } from './modal-remarks.component';

describe('ModalRemarksComponent', () => {
  let component: ModalRemarksComponent;
  let fixture: ComponentFixture<ModalRemarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ModalRemarksComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRemarksComponent);
    component = fixture.componentInstance;

    spyOn<any>(component.modalRemarksFormEmitService, 'changeEmitted$').and.callFake(() => {});

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should execute showModal', () => {
    spyOn(component, 'showModal');

    component.modalRemarksFormEmitService.emitSimpleModal('comentário interno',
      'comentário externo', 'comentário de parceiro');

    expect(component.showModal).toHaveBeenCalled();
  });

  it('should set isVisible for true in showModal', () => {
    component.showModal();

    expect(component.isVisible).toEqual(true);
  });

  it('should set reserveRemark.externalComments and reserveRemark.internalComments for reserveRemarkOld in closeModal', () => {
    component.closeModal();

    expect(component.reserveRemark.externalComments).toEqual(component.reserveRemarkOld.externalComments);
    expect(component.reserveRemark.internalComments).toEqual(component.reserveRemarkOld.internalComments);
  });

  it('should set isVisible for false in showModal', () => {
    component.closeModal();

    expect(component.isVisible).toEqual(false);
  });

  it('should set isVisible for false in saveRemarks', () => {
    component.saveRemarks();

    expect(component.isVisible).toEqual(false);
  });

  it('should emit onReservationRemarkUpdated in saveRemarks', () => {
    spyOn(component.reservationRemarkUpdated, 'emit');

    component.saveRemarks();

    expect(component.reservationRemarkUpdated.emit).toHaveBeenCalled();
  });
});
