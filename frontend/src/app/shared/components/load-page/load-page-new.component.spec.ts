import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule } from '@ngx-translate/core';
import { LoadPageNewComponent } from './load-page-new.component';
import { CoreModule } from '@app/core/core.module';

describe('LoadPageNewComponent', () => {
  let component: LoadPageNewComponent;
  let fixture: ComponentFixture<LoadPageNewComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), CoreModule],
      providers: [],
      declarations: [LoadPageNewComponent],
    });

    fixture = TestBed.createComponent(LoadPageNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(
    'should run LoadPageNewComponent and set show how true',
    fakeAsync(() => {
      component.ngOnInit();

      component['loadPageService'].show();

      expect(component.show).toBeTruthy();
    }),
  );

  it(
    'should run LoadPageNewComponent and set show how false',
    fakeAsync(() => {
      component.ngOnInit();

      component['loadPageService'].hide();

      tick(300);

      expect(component.show).toBeFalsy();
    }),
  );

  it('should destroy component on NgOnDestroy', () => {
    spyOn(component['subscription'], 'unsubscribe');

    component.ngOnDestroy();

    expect(component['subscription'].unsubscribe).toHaveBeenCalled();
  });
});
