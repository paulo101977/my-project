import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';

import { LoadPageComponent } from './load-page.component';
import { Subject } from 'rxjs';
import { ShowHide } from './../../models/show-hide-enum';
import { CoreModule } from '@app/core/core.module';

describe('LoadPageComponent', () => {
  let component: LoadPageComponent;
  let fixture: ComponentFixture<LoadPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), CoreModule],
      declarations: [LoadPageComponent],
      providers: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadPageComponent);
    component = fixture.componentInstance;

    const emitChangeSource = new Subject<any>();
    emitChangeSource.next([true]);

    spyOn<any>(component['loadPageEmitService'], 'changeEmitted$').and.callFake(() => {});

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(
    'should run LoadPageComponent and set show how true',
    fakeAsync(() => {
      component['loadPageEmitService'].emitChange(ShowHide.show);
      expect(component.show).toBeTruthy();
    }),
  );

  it(
    'should run LoadPageComponent and set show how false',
    fakeAsync(() => {
      component['loadPageEmitService'].emitChange(ShowHide.hide);
      expect(component.show).toBeFalsy();
    }),
  );
});
