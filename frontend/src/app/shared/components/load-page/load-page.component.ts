import { Component } from '@angular/core';

import { LoadPageEmitService } from './../../services/shared/load-emit.service';

@Component({
  selector: 'app-load',
  templateUrl: './load-page.component.html',
})
export class LoadPageComponent {
  public show: boolean;

  constructor(private loadPageEmitService: LoadPageEmitService) {
    loadPageEmitService.changeEmitted$.subscribe(response => {
      this.show = response[0];
    });
  }
}
