import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadPageService } from './../../services/shared/load-page.service';

@Component({
  selector: 'app-load-new',
  templateUrl: './load-page.component.html',
})
export class LoadPageNewComponent implements OnInit, OnDestroy {
  public show = false;
  private subscription: Subscription;

  constructor(private loadPageService: LoadPageService) {}

  ngOnInit() {
    this.subscription = this.loadPageService.changeEmitted$.subscribe(response => {
      this.show = response;
    });
  }

  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }
}
