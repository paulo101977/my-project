import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AutocompleteConfig } from '../../models/autocomplete/autocomplete-config';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css'],
})
export class AutocompleteComponent implements OnInit {
  @HostBinding('class') classes = 'thf-u-width-flex';

  @Input() controlName: string;

  @Input() id: string;

  public option: any;

  @Input()
  set modelToTemplate(value: any) {
    this.option = value;
  }

  private _list: Array<any>;
  @Input()
  set resultList(list: Array<any>) {
    this._list = list;
    this.stringList = [];
    if (list) {
      this.stringList = list.map(item => {
        return item[this.myField];
      });
    }
  }
  get resultList(): Array<any> {
    return this._list;
  }

  @Input()
  set parent(formGroup: FormGroup) {
    this._parent = formGroup;
    if (this._parent) {
      this.hasFormGroup = true;
    }
  }
  get parent(): FormGroup {
    return this._parent;
  }

  @Output() keyUp: EventEmitter<any> = new EventEmitter();
  public stringList: Array<string>;
  private myField: string;
  private _parent: FormGroup;
  public hasFormGroup = false;

  public text: string;
  public results: any[];
  public fieldToDisplayInOption: any;
  public emptyMessage: string;

  @Input() autocompleteConfig: AutocompleteConfig;
  @Input() readonly: boolean;

  constructor(private translateService: TranslateService) {}

  ngOnInit() {
    this.translateService.get('text.noItemsFound').subscribe(text => {
      this.emptyMessage = text;
    });
    if (this.autocompleteConfig) {
      this.text = this.autocompleteConfig.dataModel;
      this.fieldToDisplayInOption = this.autocompleteConfig.fieldObjectToDisplay;
      this.myField = this.autocompleteConfig.fieldObjectToDisplay;
    }
  }

  search(event) {
    if (this.autocompleteConfig) {
      this.autocompleteConfig.callbackToSearch(event);
    }
  }

  selectValue(event) {
    let element;
    if (this.resultList) {
      element = this.resultList.filter(item => item[this.myField] == event)[0];
    } else {
      element = event;
    }
    this.autocompleteConfig.callbackToGetSelectedValue(element);
  }

  selectValueModel(event) {
    let element;
    element = this.resultList ?
      this.resultList.find(item => item == event) :
      event;
    this.autocompleteConfig.callbackToGetSelectedValue(element);
  }

  onKeyUpNotify(event) {
    this.keyUp.emit(event);
  }
}

/*
Documentation of Autocomplete
https://github.com/primefaces/primeng/blob/master/src/app/components/autocomplete/autocomplete.ts
*/
