import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MenuOptionComponent } from '../menu-option/menu-option.component';

@Injectable({ providedIn: 'root' })
export class MenuService {
  private onCollapse = new Subject<any>();
  public collapseEmitted$ = this.onCollapse.asObservable();

  public optionMenuClicked: MenuOptionComponent = null;

  public collapse(value: boolean) {
    this.onCollapse.next(value);
  }

  public setOptionMenuClicked(menu: MenuOptionComponent) {
    this.optionMenuClicked = menu;
  }
}
