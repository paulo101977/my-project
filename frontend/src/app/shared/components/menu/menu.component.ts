import { Component, Input, ViewChildren, ElementRef, OnChanges, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MenuOption } from '../../models/menu-option';
import { MenuOptionComponent } from '../menu-option/menu-option.component';
import { MenuService } from './menu.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChildren('children') children: MenuOptionComponent[];

  @Input() options: MenuOption[];
  @Input() externalImg: string;
  public filteredOptions: MenuOption;
  public isSmall: boolean;
  public filter: string;

  private unsubscribe$ = new Subject();

  constructor(private _eref: ElementRef, private menuNewService: MenuService) {}

  ngOnInit(): void {
    this.menuNewService.collapseEmitted$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(value => {
        this.isSmall = value;
        if (this.isSmall) {
          this.recursivelyHiddenChildren(this.children);
          this.recursivelyInactiveChildren(this.children);
        }
      });
  }

  ngOnChanges() {
    this.isSmall = true;
    this.cloneOptions();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public getImg(): string {
    return this.externalImg || 'w-thex-pms.svg';
  }

  private cloneOptions() {
    this.filteredOptions = {
      name: 'root',
      children: _.cloneDeep(this.options),
    };
  }

  public onFilter() {
    this.cloneOptions();

    if (this.filter) {
      this.filteredOptions.children = this.filterMenu(this.filteredOptions.children);
    }
  }

  private filterMenu(menu: MenuOption[]) {
    let filteredMenu = [];

    for (const menuItem of menu) {
      if (menuItem.children) {
        filteredMenu = filteredMenu.concat(this.filterMenu(menuItem.children));
      } else if (menuItem.name.toLowerCase().indexOf(this.filter.toLowerCase()) !== -1) {
        filteredMenu.push(menuItem);
      }
    }

    return filteredMenu;
  }

  private recursivelyHiddenChildren(menu: MenuOptionComponent[]) {
    if (menu) {
      menu.forEach(m => {
        if (m.level > 0) {
          m.hidden = true;
        }
        if (m.submenu) {
          this.recursivelyHiddenChildren(m.submenu);
        }
      });
    }
  }

  private recursivelyInactiveChildren(menu: MenuOptionComponent[]) {
    if (menu) {
      menu.forEach(m => {
        m.isActive = false;
        if (m.submenu) {
          this.recursivelyInactiveChildren(m.submenu);
        }
      });
    }
  }

  public menuMouseEnter() {
    this.isSmall = false;
  }

  public menuMouseLeave() {
    this.isSmall = true;
    if (this.isSmall) {
      this.recursivelyHiddenChildren(this.children);
      this.recursivelyInactiveChildren(this.children);
    }
  }
}
