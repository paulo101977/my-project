import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-empty-content',
  templateUrl: 'app-empty-content.component.html',
})
export class AppEmptyContentComponent {
  @Input() title: string;

  @Input() textDescription: string;

  @Input() textBold: string;

  @Input() textComplement: string;

  @Input() extraClass: string;

  @Input() htmlDescription: string;
}
