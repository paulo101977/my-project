import { Component, OnInit, Input, AfterContentInit, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ModalEmitToggleService } from './../../services/shared/modal-emit-toggle.service';

@Component({
  selector: 'app-modal-wrapper',
  templateUrl: './modal-wrapper.component.html',
  styleUrls: ['./modal-wrapper.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalWrapperComponent implements OnInit, AfterContentInit {
  @Input() showHeader = true;
  @Input() title: string;
  @Input() modalWithParams: any;
  @Input() modalId: any;
  @Input() showModalIdInTitle: boolean;
  @Input() extraClass: string;
  @Input() extraHeaderClass: string;
  @Input() maxWidth: string;
  @Input() minWidth: string;
  @Input() hideClose: boolean;

  @Input() closeCallbackFunction: Function;
  @Output() closeModal: EventEmitter<any> = new EventEmitter<any>();
  public idsModals: any;

  constructor(private translateService: TranslateService, private modalEmitToggleService: ModalEmitToggleService) {}

  ngOnInit() {
    if (this.modalId) {
      this.modalEmitToggleService.pushModals(this.modalId);
    }

    this.modalEmitToggleService.changeEmitted$.subscribe(response => {
      this.idsModals = response;
    });

    if (!this.maxWidth) {
      this.maxWidth = '550px';
    }
    if (!this.minWidth) {
      this.minWidth = '450px';
    }
  }

  ngAfterContentInit(): void {
    this.idsModals = this.modalEmitToggleService.getModals();
  }

  public emitCloseModal(modalId: any): void {
    this.modalEmitToggleService.emitToggleWithRef(modalId);
    if (this.closeCallbackFunction) {
      this.closeCallbackFunction();
    }
  }
}
