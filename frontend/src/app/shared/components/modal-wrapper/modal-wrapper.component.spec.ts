import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ModalWrapperComponent } from './modal-wrapper.component';


describe('ModalWrapperComponent', () => {
  let component: ModalWrapperComponent;
  let fixture: ComponentFixture<ModalWrapperComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ModalWrapperComponent],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(ModalWrapperComponent);
    component = fixture.componentInstance;

    spyOn(component['modalEmitToggleService'].changeEmitted$, 'subscribe').and.callThrough();

    fixture.detectChanges();
  });

  describe('on Init', () => {
    it('should call the modalEmitToggleService SUBSCRIBE method when init component', () => {
      expect(component['modalEmitToggleService'].changeEmitted$.subscribe).toHaveBeenCalled();
    });

    it('should closeModal', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');

      const modalId = 'modalName';
      component.emitCloseModal(modalId);

      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(modalId);
    });
  });
});
