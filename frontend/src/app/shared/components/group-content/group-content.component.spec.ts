import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { GroupContentComponent } from './group-content.component';
import { SelectModel } from '../../models/selectModel';
import { GuestAccount } from './../../models/guest-account';
import { GuestAccountData } from './../../mock-data/guest-account-deta';
import { SearchableItemsData } from './../../mock-data/searchable-items-data';
import { TableRowTypeEnum } from './../../models/table-row-type-enum';
import { SearchableItems } from './../../models/filter-search/searchable-item';

describe('GroupContentComponent', () => {
  let component: GroupContentComponent;
  let fixture: ComponentFixture<GroupContentComponent>;
  const item = {
    id: '1',
    name: 'test',
  };

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [ReactiveFormsModule, TranslateModule.forRoot()],
      declarations: [GroupContentComponent],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });

    fixture = TestBed.createComponent(GroupContentComponent);
    component = fixture.componentInstance;

    component.itemListToFilter = new Array<any>();
    component.itemListToFilter.push(item);
    component.hasFilter = true;
    component.title = 'Título';
    component.placeholderSearchFilter = 'placeholder';
    component.tableRowType = TableRowTypeEnum.GuestAccount;

    spyOn(component.setListFiltered, 'emit').and.callThrough();

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should set searchableItems if has filter', () => {
      SearchableItemsData.items.push(item);
      expect(component.hasFilter).toBeTruthy();
      expect(component.searchableItems).toEqual(SearchableItemsData);
      expect(component.placeholderSearchFilter).toEqual('placeholder');
      expect(component.title).toEqual('Título');
    });
  });

  describe('on udpateList', () => {
    beforeEach(() => {
      component.itemListToFilter = new Array<any>();
    });

    it('should set itemListToFilter', () => {
      const list = new Array<any>();
      list.push(item);

      component.updateList(list);

      expect(component.itemListToFilter).toEqual(list);
    });
  });
});
