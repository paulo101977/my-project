import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { TableRowTypeEnum } from './../../models/table-row-type-enum';
import { SearchableItems } from './../../models/filter-search/searchable-item';
import { ButtonConfig } from 'app/shared/models/button-config';

@Component({
  selector: 'app-group-content',
  templateUrl: 'group-content.component.html',
})
export class GroupContentComponent implements OnInit, OnChanges {
  public searchableItems: SearchableItems;

  @Input() title: string;

  @Input() placeholderSearchFilter: string;

  @Input() tableRowType: TableRowTypeEnum;

  @Input() hasFilter: boolean;

  @Input() showFilterOnlyWhenListHasItens: boolean;

  @Input() hasButton: boolean;

  @Input() buttonConfigSetted: ButtonConfig;

  @Input() itemListToFilter: Array<any>;

  @Input() bodyClass: string;

  @Output() setListFiltered = new EventEmitter();

  ngOnInit() {
    if (this.hasFilter) {
      this.setSearchableItems();
    }
  }

  ngOnChanges(changes) {
    if (changes) {
      if (changes.hasOwnProperty('buttonConfigSetted') && changes.buttonConfigSetted.currentValue) {
        this.buttonConfigSetted = changes.buttonConfigSetted.currentValue;
      }
      if (this.hasFilter) {
        this.setSearchableItems();
      }
    }
  }

  private setSearchableItems(): void {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.itemListToFilter;
    this.searchableItems.placeholderSearchFilter = this.placeholderSearchFilter;
    this.searchableItems.tableRowTypeEnum = this.tableRowType;
  }

  public updateList(items: Array<any>) {
    if (items) {
      this.itemListToFilter = items;
      this.setListFiltered.emit(items);
    }
  }
}
