import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonConfig, ButtonType } from '../../models/button-config';
import { SharedService } from '../../services/shared/shared.service';

@Component({
  selector: 'app-new-account-modal',
  templateUrl: './new-account-modal.component.html',
  styleUrls: ['./new-account-modal.component.css']
})
export class NewAccountModalComponent implements OnInit {
  @Input() modalId: string;

  @Output() cancelModal = new EventEmitter();
  @Output() confirmModal = new EventEmitter<string>();

  public cancelModalButtonConfig: ButtonConfig;
  public confirmModalButtonConfig: ButtonConfig;

  public accountName: string;

  constructor( private sharedService: SharedService ) {}

  ngOnInit() {
    this.setButtonConfig();
  }

  public checkButtonDisabled() {
    this.confirmModalButtonConfig.isDisabled = !this.accountName;
  }

  private setButtonConfig() {
    this.cancelModalButtonConfig = this.sharedService.getButtonConfig(
      'cancel_new_accout',
      this.clickCancelModal,
      'action.cancel',
      ButtonType.Secondary
    );

    this.confirmModalButtonConfig = this.sharedService.getButtonConfig(
      'confirm_new_accout',
      this.clickConfimModal,
      'action.confirm',
      ButtonType.Primary,
      null,
      true
    );
  }

  clickCancelModal = () => { this.cancelModal.emit(); };
  clickConfimModal = () => { this.confirmModal.emit(this.accountName); };
}
