import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NewAccountModalComponent } from './new-account-modal.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

describe('NewAccountModalComponent', () => {
  let component: NewAccountModalComponent;
  let fixture: ComponentFixture<NewAccountModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateModule.forRoot() , FormsModule ],
      declarations: [ NewAccountModalComponent ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAccountModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit cancelModal when clickCancelModal fired', () => {
    spyOn(component.cancelModal , 'emit');

    component.clickCancelModal();

    expect(component.cancelModal.emit).toHaveBeenCalled();
  });

  it('should emit confirmModal with correct accountName when clickConfimModal fired', () => {
    spyOn(component.confirmModal , 'emit');
    component.accountName = 'nova conta';

    component.clickConfimModal();

    expect(component.confirmModal.emit).toHaveBeenCalledWith('nova conta');
  });

});
