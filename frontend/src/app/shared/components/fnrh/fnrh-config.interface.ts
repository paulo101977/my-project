// Models
import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import { NationalityDto } from './../../models/dto/checkin/nationality-dto';
import { DocumentType } from 'app/shared/models/document-type';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { GuestCard } from '../../models/checkin/guest-card';
import {FnrhSaveDto} from 'app/shared/models/dto/checkin/fnrh-save-dto';

export interface FnrhConfigInterface {
  guestReservationItemId: number;
  fnrh: FnrhDto;
  genderList: Array<SelectObjectOption>;
  occupationList: Array<SelectObjectOption>;
  guestTypeList: Array<SelectObjectOption>;
  nationalityList: Array<NationalityDto>;
  vehicleList: Array<SelectObjectOption>;
  reasonList: Array<SelectObjectOption>;
  documentTypesAutocomplete: Array<DocumentType>;
  guest: GuestCard;
  validateAvailableSlots?: {
    adultCapacity: number,
    childCapacity: number,
    adultCount: number,
    childCount: number
  };
  dataReservation: FnrhSaveDto;
  reservationItemCode?: string;
}
