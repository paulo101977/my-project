import { TranslateModule } from '@ngx-translate/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MyDatePickerModule } from 'mydatepicker';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FnrhComponent } from './fnrh.component';
import { SearchPersonsResultDto } from '../../models/person/search-persons-result-dto';
import { SuccessError } from '../../models/success-error-enum';
import { FnrhDto } from './../../models/dto/checkin/fnhr-dto';

import { SharedModule } from 'app/shared/shared.module';
import {
  DATA,
  GUEST_ADDRESS_LIST,
  GUEST_FNRH_AUTOCOMPLETE,
  GUEST_FORM_FNRH_CONFIG,
  RECEIVE_FNRH_LIST
} from 'app/checkin/mock-data';
import { configureTestSuite } from 'ng-bullet';
import { propertyServiceStub } from 'app/shared/services/property/testing';
import { DocumentTypeNaturalEnum } from 'app/shared/models/document-type';
import { CountryCodeEnum } from 'app/shared/models/country-code-enum';


describe('FnrhComponent', () => {
  let component: FnrhComponent;
  let fixture: ComponentFixture<FnrhComponent>;

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MyDatePickerModule,
        RouterTestingModule,
        RouterModule,
        HttpClientTestingModule,
        SharedModule
      ],
      providers: [
        propertyServiceStub,
      ],
      declarations: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(FnrhComponent);
    component = fixture.componentInstance;
    component.config = DATA[GUEST_FORM_FNRH_CONFIG];
    spyOn<any>(component['propertyService'], 'getPropertySession').and.returnValue(
      {
        locationList: [
          {
            country: 'BR'
          }
        ]
      }
    );

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on init', () => {
    it('should ngOnInit', () => {
      spyOn<any>(component, 'setButtonConfig');
      spyOn<any>(component, 'configDatePicker');
      spyOn<any>(component, 'setFnrhForm');

      component.ngOnInit();

      expect(component['setButtonConfig']).toHaveBeenCalled();
      expect(component['configDatePicker']).toHaveBeenCalled();
      expect(component['setFnrhForm']).toHaveBeenCalled();
    });

    it('should set vars on ngOnInit', () => {
      component.ngOnInit();
      const today = new Date();

      const disableSince = {
        year: today.getFullYear(),
        month: today.getMonth() + 1,
        day: today.getDate() + 1,
      };

      expect(component.myDatePickerOptions.disableSince).toEqual(disableSince);
      expect(Object.keys(component['fnrhForm'].controls)).toEqual([
        'basicData',
        'address',
        'person',
        'complementaryInformation',
        'additionalInformation',
      ]);
    });
  });

  describe('on changes', () => {
    it('should setValueInForm when fnrh dto is null', fakeAsync(() => {
      component.config = {
        guestReservationItemId: 1,
        fnrh: null,
        genderList: null,
        occupationList: null,
        guestTypeList: null,
        nationalityList: null,
        vehicleList: null,
        reasonList: null,
        documentTypesAutocomplete: null,
        guest: null,
        dataReservation: null
      };

      spyOn<any>(component, 'setValueInForm');
      spyOn<any>(component, 'setResponsableList');

      component.ngOnChanges(null);

      expect(component['setValueInForm']).not.toHaveBeenCalled();
      expect(component['setResponsableList']).toHaveBeenCalled();
    }));

    it('should setValueInForm when fnrh dto is not null', () => {
      component.config = DATA[GUEST_FORM_FNRH_CONFIG];

      spyOn<any>(component, 'setValueInForm');
      spyOn<any>(component, 'setResponsableList');

      component.ngOnChanges(null);

      expect(component['setValueInForm']).toHaveBeenCalled();
      expect(component['setResponsableList']).toHaveBeenCalled();
      expect(component.documentOptionSelectList).toEqual([
        { key: 1, value: 'CPF' },
        { key: 3, value: 'DNI' }
      ]);
      expect(component.nationalityOptionsList).toEqual([
        { key: 1, value: 'Brasileira' },
        { key: 5593, value: 'Portuguesa' }
      ]);
      expect(component['setValueInForm']).toHaveBeenCalled();
    });
  });

  describe('should setValueInForm', () => {
    it('value is not null, location is null and is adult', () => {
      const formData = {...DATA[RECEIVE_FNRH_LIST]};
      formData.isChild = false;

      spyOn<any>(component, 'setDocumentNotRequired');
      spyOn<any>(component, 'setAge');
      spyOn<any>(component, 'responsableControl');
      spyOn<any>(component, 'apllyMaskToDocument');


      component['setValueInForm'](formData);

      expect(component.fnrhForm.get('basicData.age').value).toEqual(43);
      expect(component.fnrhForm.get('basicData.document').value).toEqual('123.643.610-52');
      expect(component.fnrhForm.get('basicData.fullName').value).toEqual('fgg');
      expect(component.setDocumentNotRequired).not.toHaveBeenCalled();
      expect(component['setAge']).toHaveBeenCalled();
      expect(component.responsableControl).toHaveBeenCalled();
      expect(component.apllyMaskToDocument).toHaveBeenCalled();
    });

    it('value, location and country is not null', () => {
      const value = <FnrhDto>{
        age: 43,
        location: {
          countryCode: 'BR',
          country: 'Brasil'
        }
      };

      spyOn<any>(component, 'setPropertyCountryName');
      component['setValueInForm'](value);

      expect(component['setPropertyCountryName']).not.toHaveBeenCalled();
    });

    it('value and location is not null, country is null', () => {
      const value = <FnrhDto>{
        age: 43,
        location: {
          countryCode: 'BR',
          country: null
        }
      };

      spyOn<any>(component, 'setPropertyCountryName');
      component['setValueInForm'](value);

      expect(component['setPropertyCountryName']).toHaveBeenCalled();
    });

    it('value is not null and isChild', () => {
      const value = <FnrhDto>{
        age: 3,
        isChild: true
      };

      spyOn<any>(component, 'setDocumentNotRequired');
      component['setValueInForm'](value);

      expect(component['setDocumentNotRequired']).toHaveBeenCalled();
    });
  });

  describe('should setAge', () => {
    it('is child', () => {
      component.config.fnrh = <FnrhDto>{
        age: 2,
        isChild: true
      };
      spyOn<any>(component, 'setDocumentNotRequired');
      component['setAge']('2018-03-19T00:00:00');
      expect(component.setDocumentNotRequired).toHaveBeenCalled();
    });

    it('is not child', () => {
      component.config.fnrh = <FnrhDto>{
        age: 43,
        isChild: false
      };
      spyOn<any>(component, 'setDocumentRequired');
      component['setAge']('1976-03-19T00:00:00');
      expect(component.setDocumentRequired).toHaveBeenCalled();
    });
  });

  it('searchData >= 3', fakeAsync(() => {
    const search = {query: '191'};
    component.searchItemDocument(search);

    expect<any>(component.optionDocument).toEqual({
      countrySubdivisionId: 1,
      id: 1,
      isMandatory: true,
      name: 'CPF',
      personType: 'N'
    });

    const personsResult = <SearchPersonsResultDto[]>[
      { id: '1' },
      { id: '2' },
      { id: '3' }
    ];

    const promise = spyOn<any>(component['personResource'], 'getPersonsByCriteria').and.callFake(() => {
      return new Promise((resolve, reject) => resolve(personsResult));
    });

    promise(search, component.optionDocument).then(persons => {
      component.autocompleteDocumentConfig.resultList = persons;
      component.autoCompleteDocumentList = persons;
    });
    tick();

    expect(component.autocompleteDocumentConfig.resultList).toEqual(personsResult);
    expect(component.autoCompleteDocumentList).toEqual(personsResult);
  }));

  describe('should populateFieldsAutoComplete', () => {
    it('should isResponsable is true', () => {
      const elementSelected = DATA[GUEST_FNRH_AUTOCOMPLETE];

      component.populateFieldsAutoComplete(elementSelected);
      expect(component.fnrhForm.get('basicData.email').value).toEqual('william@email.com');
    });

    it('should isResponsable is false', () => {
      spyOn<any>(component, 'setAge');
      const elementSelected = DATA[GUEST_FNRH_AUTOCOMPLETE];

      component.populateFieldsAutoComplete(elementSelected, false);
      expect(component.guestIdToUse).toEqual(305);
      expect(component.fnrhForm.get('basicData.responsable').value).toEqual('1234');
      expect(component['setAge']).toHaveBeenCalled();
    });
  });

  describe('should onDateChanged', () => {
    it('is Adult', () => {
      const date = '1988-04-14T00:00:00';
      const age = 30;
      spyOn<any>(component, 'calculateAgeByDate').and.returnValue(age);
      spyOn<any>(component['checkinService'], 'checkIfIsChild').and.returnValue(false);
      spyOn<any>(component, 'setDocumentNotRequired');
      spyOn<any>(component, 'setDocumentRequired');
      spyOn<any>(component, 'responsableControl');

      component.onDateChanged(date);

      expect(component['checkinService'].checkIfIsChild).toHaveBeenCalledWith(component.propertyId, age);
      expect(component.fnrhForm.get('basicData.age').value).toEqual(age);
      expect(component.setDocumentNotRequired).not.toHaveBeenCalled();
      expect(component.setDocumentRequired).toHaveBeenCalled();
      expect(component.responsableControl).toHaveBeenCalled();
    });

    it('is Child', () => {
      const date = '2014-04-14T00:00:00';
      const age = 5;
      spyOn<any>(component, 'calculateAgeByDate').and.returnValue(age);
      spyOn<any>(component['checkinService'], 'checkIfIsChild').and.returnValue(true);
      spyOn<any>(component, 'setDocumentNotRequired');
      spyOn<any>(component, 'setDocumentRequired');
      spyOn<any>(component, 'responsableControl');

      component.onDateChanged(date);

      expect(component['checkinService'].checkIfIsChild).toHaveBeenCalledWith(component.propertyId, age);
      expect(component.fnrhForm.get('basicData.age').value).toEqual(age);
      expect(component.setDocumentNotRequired).toHaveBeenCalled();
      expect(component.setDocumentRequired).not.toHaveBeenCalled();
      expect(component.responsableControl).toHaveBeenCalled();
    });
  });

  describe('should saveFnrh', () => {
    beforeEach( () => {
      component.config.guest.guest.id = 463;
      component['setValueInForm'](DATA[RECEIVE_FNRH_LIST]);
      component.setValueAddressByEventEmitted(DATA[GUEST_ADDRESS_LIST]);
      component.fnrhForm.get('basicData.responsableName').setValue('William');
    });

    it('should saveFnrh form valid false', (() => {
      spyOn<any>(component['toasterEmitService'], 'emitChange').and.callThrough();

      component.fnrhForm.get('basicData').get('document').setErrors({'incorrect': true});
      component.saveFnrh();

      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.error,
        'commomData.requiredFields'
      );
    }));

    it('should emit form valid true', fakeAsync(() => {
      spyOn<any>(component['fnrhMapper'], 'toSave').and.returnValue( component.fnrhForm );
      spyOn<any>(component.fnrhSaved, 'emit');

      component.saveFnrh(false);

      expect(component['fnrhMapper'].toSave).toHaveBeenCalledWith(component.fnrhForm, component.documentList);
      expect(component.fnrhSaved.emit).toHaveBeenCalledWith(component.fnrhForm);
    }));

    it('should without printing', () => {
      spyOn<any>(component['fnrhMapper'], 'toSave').and.returnValue( component.fnrhForm );
      spyOn<any>(component.fnrhSaved, 'emit');

      component.saveFnrh(false);

      expect(component['fnrhMapper'].toSave).toHaveBeenCalledWith(component.fnrhForm, component.documentList);
      expect(component.fnrhSaved.emit).toHaveBeenCalledWith(component.fnrhForm);
    });

    it('should printing', () => {
      spyOn<any>(component['fnrhMapper'], 'toSave').and.returnValue( component.fnrhForm );
      spyOn<any>(component.fnrhSaved, 'emit');
      spyOn<any>(component, 'print');

      component.saveFnrh(true);

      expect(component['fnrhMapper'].toSave).toHaveBeenCalledWith(component.fnrhForm, component.documentList);
      expect(component.print).toHaveBeenCalled();
    });
  });

  describe('on setNationalityIsRequired', () => {
    let nationality;
    beforeEach(() => {
       nationality = component.fnrhForm.get('basicData.nationality');

       spyOn<any>(nationality, 'setValidators');
       spyOn<any>(nationality, 'updateValueAndValidity');
    });

    it('should test when country code not found', () => {
      spyOn<any>(component['propertyService'], 'getPropertyCountryCode').and.returnValue('FR');

      component['setNationalityIsRequired']();

      expect(nationality.setValidators).not.toHaveBeenCalled();
      expect(nationality.updateValueAndValidity).not.toHaveBeenCalled();
      expect(component.nationalityIsRequired).toEqual(false);
    });

    it('should test when country code was found', () => {
      spyOn<any>(component['propertyService'], 'getPropertyCountryCode').and.returnValue('BR');

      component['setNationalityIsRequired']();

      expect(nationality.setValidators).toHaveBeenCalled();
      expect(nationality.updateValueAndValidity).toHaveBeenCalled();
      expect(component.nationalityIsRequired).toEqual(true);
    });
  });

  it('should call documentIsValid - CPF', () => {
    spyOn(component['documentService'], 'cpfControlIsValid')
      .and.returnValue({'invalidDocument': {document: 'CPF'}});

    const documentTypeId = DocumentTypeNaturalEnum.CPF;
    const document = '123';

    component.fnrhForm.get('basicData.documentTypeId').setValue(documentTypeId);
    component.fnrhForm.get('basicData.document').setValue(document);

    expect(component['documentService'].cpfControlIsValid)
      .toHaveBeenCalledWith(documentTypeId, document);
  });

  it('should call documentIsValid - NIF', () => {
    spyOn(component['documentService'], 'nifControlIsValid')
      .and.returnValue({'invalidDocument': {document: 'NIF'}});

    const documentTypeId = DocumentTypeNaturalEnum.NIF;
    const document = '243661037';
    const countryCode = CountryCodeEnum.PT;

    component.fnrhForm.get('basicData.documentTypeId').setValue(documentTypeId);
    component.fnrhForm.get('address.countryCode').setValue(countryCode);
    component.fnrhForm.get('basicData.document').setValue(document);

    expect(component['documentService'].nifControlIsValid)
      .toHaveBeenCalledWith(documentTypeId, document, countryCode);
  });

  describe('#responsableControl', () => {
    it('should remove guest from the responsable list when need responsable', () => {
      const options = [{key: 1, value: 'success'}];
      spyOn<any>(component['commonService'], 'toOption').and.returnValue(options);
      component.responsableList = <any> [{id: 1}, {id: 2}];
      component.fnrhForm.patchValue({basicData: {
        incapable: true,
        id: 1
      }});

      component.responsableControl();

      expect(component.responsableList.length).toEqual(1);
      expect(component.selectOptionResponsables).toEqual(options);
    });

    it('should remove value from responsable from guest when does not need responsable', () => {
      component.fnrhForm.patchValue({basicData: {
        responsable: 'f17c85aa-e522-4ff6-b17e-9f72181231d6',
        incapable: false,
        id: 1
      }});

      component.responsableControl();

      expect(component.fnrhForm.get('basicData.responsable').value).toEqual(null);
    });
  });
});

