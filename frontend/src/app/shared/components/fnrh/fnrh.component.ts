import { AddressComponent } from '../address/address.component';
import {
  Component,
  ComponentRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IMyDpOptions, MyDatePicker } from 'mydatepicker';
import {
  PrintSingleGuestRegistrationComponent
} from 'app/checkin/components/print-single-guest-registration/print-single-guest-registration.component';
import { SelectObjectOption } from '../../models/selectModel';
import { DocumentType, DocumentTypeNaturalEnum } from 'app/shared/models/document-type';
import { ButtonConfig, ButtonSize, ButtonType } from '../../models/button-config';
import { FnrhConfigInterface } from 'app/shared/components/fnrh/fnrh-config.interface';
import { FnrhToPrintDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { AddressType } from '../../models/address-type-enum';
import { CheckinGuestReservationItemDto } from '../../models/dto/checkin/guest-reservation-item-dto';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { AutocompleteConfig } from '../../models/autocomplete/autocomplete-config';
import { PrintComunicator } from 'app/shared/interfaces/print-comunicator';
import { DateService } from 'app/shared/services/shared/date.service';
import { CommomService } from '../../services/shared/commom.service';
import { ModalEmitToggleService } from '../../services/shared/modal-emit-toggle.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { FnrhMapper } from 'app/shared/mappers/fnrh-mapper';
import { CheckinService } from 'app/shared/services/checkin/checkin.service';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { DocumentTypeMaskService } from '../../services/shared/document-type-mask.service';
import { PersonResource } from 'app/shared/resources/person/person.resource';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import * as moment from 'moment';
import { PropertyService } from '../../services/property/property.service';
import { Property } from '../../models/property';
import { Location } from '@angular/common';
import { FnrhService } from '../../services/fnrh/fnrh.service';
import { Subscription } from 'rxjs';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { Location as Address } from 'app/shared/models/location';
import { ClientDocumentTypeEnum } from 'app/shared/models/dto/client/client-document-types';
import { ValidatorFormService } from 'app/shared/services/shared/validator-form.service';
import { CountryCodeEnum } from 'app/shared/models/country-code-enum';
import { DocumentService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-fnrh',
  templateUrl: './fnrh.component.html',
  styleUrls: ['./fnrh.component.css'],
})
export class FnrhComponent implements OnInit, OnChanges, PrintComunicator, OnDestroy {
  private DOCUMENT_RG = 16;
  @ViewChild('birthDayField') birthDayElement: MyDatePicker;
  @ViewChild('addressComponent') addressComponent: AddressComponent;

  private _config: FnrhConfigInterface;
  private locationIsValid: boolean;

  @Input()
  set config(conf: FnrhConfigInterface) {
    this._config = conf;
    if (conf && conf.fnrh) {
      this.guestIdToUse = conf.fnrh.guestId;
      if (conf.fnrh.isChild) {
        this.childAgeRange = this.commonService.getAgeRangeFromAge(this.propertyId, conf.fnrh.childAge);
      }
    }
  }

  get config(): FnrhConfigInterface {
    return this._config;
  }

  @Input() public guestListQuantity: number;
  @Input() public editModeEnabled = false;
  @Input() responsableList: Array<CheckinGuestReservationItemDto>;
  @Input() buttonCustomSave: {textButton: string, buttonType: ButtonType};

  @Output() public appliedAddress = new EventEmitter();
  @Output() public formGroupUpdated = new EventEmitter();
  @Output() public fnrhSaved = new EventEmitter();

  public propertyId: number;
  public fnrhForm: FormGroup;
  public personTypeForm: FormGroup;
  public locationEdit: Address;

  // mask
  public phoneMask = ['+', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public cellPhoneMask = ['+', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public remark: boolean;
  public complementaryInfo: boolean;

  // Autocomplete Document dependencies
  public searchDataDocument: string;
  public searchDataEmail: string;
  public optionDocument: DocumentType;

  public autocompleteEmailConfig: AutocompleteConfig;
  public autocompleteDocumentConfig: AutocompleteConfig;

  public selectOptionResponsables: Array<SelectObjectOption> = [];
  public needResponsable: boolean;
  public nationalityOptionsList: Array<SelectObjectOption> = [];
  public countryName: string;
  public childAgeRange: number;

  // DatePickerRanger dependencies
  public myDatePickerOptions: IMyDpOptions;

  // Buttons dependencies
  public buttonSignConfig: ButtonConfig;
  public buttonPrintConfig: ButtonConfig;
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;
  public buttonApplyAddressConfig: ButtonConfig;
  public buttonModalCancel: ButtonConfig;
  public buttonModalSave: ButtonConfig;

  public documentOptionSelectList: Array<SelectObjectOption>;

  public autoCompleteEmailList: Array<any>;
  public autoCompleteDocumentList: Array<any>;

  public localeRequired = false;
  public documentRequired = true;

  public guestIdToUse: number;
  public property: Property;

  public emailRequired = true;
  public phoneRequired = true;
  public cellphoneRequired = true;

  private printSubscription: Subscription;
  public termsUse: boolean;
  public isReceiveOffers: Boolean;
  public isConfidentialityData: Boolean;
  public isSendDatabyElectronicMeans: Boolean;
  public nationalityIsRequired = false;
  public documentList = [];

  constructor(
    private formBuilder: FormBuilder,
    private personResource: PersonResource,
    private dateService: DateService,
    private modalEmitToggleService: ModalEmitToggleService,
    private checkinResource: CheckinResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private fnrhMapper: FnrhMapper,
    private checkinService: CheckinService,
    private commonService: CommomService,
    private activateRouter: ActivatedRoute,
    private router: Router,
    private printService: PrintHelperService,
    private documentMaskService: DocumentTypeMaskService,
    private propertyService: PropertyService,
    private location: Location,
    private fnrhService: FnrhService,
    private validatorFormService: ValidatorFormService,
    private documentService: DocumentService,
  ) {
  }

  ngOnInit() {
    this.configDatePicker();
    this.setButtonConfig();
    this.setFnrhForm();
    this.setNationalityIsRequired();

    this.propertyId = this.activateRouter.snapshot.params.property;
    this.setAutoCompleteConfig();
    this.printSubscription = this.fnrhService.fnrhPrintEmmitedd$
      .subscribe(() => {

        // print empty or filled record
        this.printService.printComponent(PrintSingleGuestRegistrationComponent, this);
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.config) {
      this.documentOptionSelectList = this.commonService
        .toOption(this.config.documentTypesAutocomplete, 'id', 'name');
      if (this.config.nationalityList) {
        this.nationalityOptionsList = this.commonService
          .toOption(this.config.nationalityList, 'countrySubdivisionId', 'nationality');
      }
    }
    if (this.config.fnrh) {
      this.setDocumentList(this.config.fnrh);
      this.setValueInForm(this.config.fnrh);
    }
    this.setResponsableList();
    if (this.buttonCustomSave) {
      this.setSaveButton(this.buttonCustomSave);
    }
  }

  private setDocumentList(fnrh) {
    if (fnrh.guestRegistrationDocumentList) {
      this.documentList = [...fnrh.guestRegistrationDocumentList];
    }
  }

  private setAutoCompleteConfig() {
    this.autocompleteEmailConfig = new AutocompleteConfig();
    this.autocompleteEmailConfig.dataModel = 'email';
    this.autocompleteEmailConfig.callbackToSearch = this.searchByEmail;
    this.autocompleteEmailConfig.callbackToGetSelectedValue = this.selectEmail;
    this.autocompleteEmailConfig.fieldObjectToDisplay = 'email';
    this.autocompleteEmailConfig.displayIcon = false;
    this.autocompleteEmailConfig.extraClasses = 'thf-input thf-u-margin-top--1 thf-u-padding--1';

    this.autocompleteDocumentConfig = new AutocompleteConfig();
    this.autocompleteDocumentConfig.dataModel = 'document';
    this.autocompleteDocumentConfig.callbackToSearch = this.searchItemDocument;
    this.autocompleteDocumentConfig.callbackToGetSelectedValue = this.selectDocument;
    this.autocompleteDocumentConfig.fieldObjectToDisplay = 'document';
    this.autocompleteDocumentConfig.displayIcon = false;
    this.autocompleteDocumentConfig.extraClasses = 'thf-input thf-u-margin-top--1 thf-u-padding--1';
  }

  public setFnrhForm() {
    this.fnrhForm = this.formBuilder.group({
      basicData: this.formBuilder.group({
        guestId: null,
        id: null,
        guestReservationItemId: null,
        personId: null,
        document: [null, [Validators.required, this.documentIsValid()]],
        documentTypeId: [1, [Validators.required]],
        email: [null, [Validators.required, Validators.email]],
        fullName: [null, [Validators.required]],
        birthDate: [null, [Validators.required, this.validateAvailableSlots()]],
        age: null,
        gender: null,
        guestType: [null, [Validators.required, this.validatorFormService.dontAcceptZeroValue]],
        nationality: [null],
        responsable: null,
        responsableName: null,
        incapable: false,
        phone: [null, [Validators.required]],
        cellPhone: [null, [Validators.required]],
      }),
      address: this.formBuilder.group({
        id: null,
        ownerId: GuidDefaultData,
        postalCode: null,
        streetName: null,
        streetNumber: null,
        additionalAddressDetails: null,
        division: null,
        subdivision: null,
        neighborhood: null,
        browserLanguage: window.navigator.language,
        latitude: null,
        longitude: null,
        countryCode: null,
        country: null,
        completeAddress: null,
        countrySubdivisionId: null,
        locationCategoryId: null
      }),
      person: this.formBuilder.group({
        id: null,
        personType: null,
        firstName: null,
        fullName: null,
        lastName: null,
        receiveOffers: null,
        sharePersonData: null,
        allowContact: null,
        countrySubdivisionId: null,
      }),
      complementaryInformation: this.formBuilder.group({
        occupation: null,
        vehicle: [null],
        lastAddress: [null],
        nextAddress: [null],
        reasonForTravel: [null],
        medicalAgreement: null,
        socialName: null,
      }),
      additionalInformation: this.formBuilder.group({
        remarks: null,
      })
    });

    this.personTypeForm = this.formBuilder.group({
      personType: ClientDocumentTypeEnum.Natural
    });

    this.addressComponent.clearAddress();
    this.observableFormGroup();
  }

  private observableFormGroup(): void {
    this.fnrhForm.valueChanges
      .subscribe((value) => {
        this.formGroupUpdated.emit(this.fnrhForm.value);
        this.controlContactRequired();

        this.buttonSaveConfig.isDisabled = this.fnrhForm.invalid || !this.locationIsValid;
      });
    this.fnrhForm.get('address').valueChanges
      .subscribe(() => {
        this.buttonApplyAddressConfig.isDisabled = !this.locationIsValid;
      });
  }

  private controlContactRequired() {
    const basicDataForm = this.fnrhForm.get('basicData');

    if (basicDataForm.get('email').value) {
      this.emailRequired = true;
      this.phoneRequired = false;
      this.cellphoneRequired = false;
    } else if (basicDataForm.get('phone').value) {
      this.emailRequired = false;
      this.phoneRequired = true;
      this.cellphoneRequired = false;
    } else if (basicDataForm.get('cellPhone').value) {
      this.emailRequired = false;
      this.phoneRequired = false;
      this.cellphoneRequired = true;
    }
    this.setFormRequiredValidators('basicData.email', this.emailRequired);
    this.setFormRequiredValidators('basicData.phone', this.phoneRequired);
    this.setFormRequiredValidators('basicData.cellPhone', this.cellphoneRequired);
  }

  private setFormRequiredValidators(controlName, enabled) {
    this.fnrhForm.get(controlName).setValidators(enabled ? [Validators.required] : null);
    this.fnrhForm.get(controlName).updateValueAndValidity({onlySelf: true});
  }

  private getPersonProperty(value: any, property: string) {
    const { person } = value;
    return (person ? person[property] : value[property]) || value[property];
  }

  public setValueInForm(value) {
    if (value) {
      this.fnrhForm.patchValue({
        basicData: {
          guestId: this.config.guest ? this.config.guest.guest.id : GuidDefaultData,
          id: this.config.fnrh.id,
          guestReservationItemId: this.config.guestReservationItemId,
          personId: value.personId,
          document: value.document
            ? value.document
            : (this.config.guest
              ? this.config.guest.guest.documentInformation
              : ''),
          documentTypeId: value.documentTypeId ? value.documentTypeId : 1,
          email: value.email,
          fullName: value.fullName,
          birthDate: value.birthDate,
          age: value.age,
          gender: value.gender,
          guestType: value.guestTypeId,
          nationality: value.nationality,
          responsable: value.responsibleGuestRegistrationId ? value.responsibleGuestRegistrationId : value.responsable,
          incapable: value.incapable,
          phone: value.phoneNumber,
          cellPhone: value.mobilePhoneNumber,
        },
        person: {
          receiveOffers: this.getPersonProperty(value, 'receiveOffers'),
          sharePersonData: this.getPersonProperty(value, 'sharePersonData'),
          allowContact: this.getPersonProperty(value, 'allowContact'),
        },
        complementaryInformation: {
          occupation: value.occupationId,
          vehicle: value.arrivingBy,
          lastAddress: value.arrivingFromText,
          nextAddress: value.nextDestinationText,
          reasonForTravel: value.purposeOfTrip,
          medicalAgreement: value.healthInsurance,
          socialName: value.socialName,
        },
        additionalInformation: {
          remarks: value.additionalInformation,
        }
      });

      if (value.location) {
        this.locationEdit = value.location;
        if (!value.location.country) {
          this.setPropertyCountryName();
        }
      }

      if (value.hasOwnProperty('isChild') && value.isChild) {
        this.fnrhForm.get('basicData.age').setValue(value.childAge);
        this.setDocumentNotRequired();
      }

      this.setAge(value.birthDate);
      this.responsableControl();
      this.apllyMaskToDocument(this.fnrhForm.get('basicData.document').value);
    }

    this.disableLocationTypeSelect();
    this.setAddressToApplyToAll();
  }

  private setAddressToApplyToAll(): void {
    if (this.fnrhService.fnrhAddressToApply) {
      this.locationEdit = this.fnrhService.fnrhAddressToApply;
    }
  }

  private setNationalityIsRequired() {
    const country = this.propertyService.getPropertyCountryCode();
    if ( this.fnrhForm &&
      ( country === CountryCodeEnum.BR || country === CountryCodeEnum.PT )
    ) {
      const nationality = this.fnrhForm.get('basicData.nationality');

      nationality.setValidators([
        Validators.required,
        this.validatorFormService.dontAcceptZeroValue
      ]);
      nationality.updateValueAndValidity();

      this.nationalityIsRequired = true;
    }
  }

  public setPropertyCountryName() {
    this.property = this.propertyService.getPropertySession(this.propertyId);
    if (this.property) {
      this.countryName = this.property.locationList[0].country;
    }
  }

  public setAge(birthDate): void {
    if (birthDate) {
      const newAge = this.dateService
        .calculateAgeByDate(this.dateService.convertStringToDate(birthDate, DateService.DATE_FORMAT_UNIVERSAL));
      this.fnrhForm.get('basicData.age').setValue(newAge);
      if (!this.commonService.isAdult(newAge) || (this.config.fnrh && this.config.fnrh.isChild)) {
        this.setDocumentNotRequired();
      } else {
        this.setDocumentRequired();
      }
    }
  }

  private disableLocationTypeSelect(): void {
    if (this.addressComponent) {
      const locationCategoryIdControl = this.addressComponent.addressForm.get('locationCategoryId');
      locationCategoryIdControl.setValue(AddressType.Residencial);
      locationCategoryIdControl.disable({onlySelf: true, emitEvent: true});
    }
  }

  public setValueAddressByEventEmitted(address: any) {
    this.locationIsValid = address.valid;
    this.fnrhForm.get('address').patchValue(address);
  }

  public searchByEmail = (searchData: any) => {
    this.searchDataEmail = searchData.query;
    if (searchData && searchData.query.length >= 3) {
      this.personResource.getPersonsByEmail(this.searchDataEmail).subscribe(persons => {
        this.autocompleteEmailConfig.resultList = [...persons];
        this.autoCompleteEmailList = [...persons];
      });
    }
  }

  public searchItemDocument = (searchData) => {
    this.searchDataDocument = searchData.query;
    if (searchData && searchData.query.length >= 3) {
      this.optionDocument = this.config.documentTypesAutocomplete.filter(
        item => item.id == this.fnrhForm.get('basicData.documentTypeId').value,
      )[0];
      if (searchData && this.optionDocument) {
        this.personResource.getPersonsByCriteria(searchData.query, this.optionDocument).subscribe(persons => {
          this.autocompleteDocumentConfig.resultList = [...persons];
          this.autoCompleteDocumentList = [...persons];
        });
      }
    }
  }

  public selectEmail = (elementSelected) => {
    this.populateFieldsAutoComplete(elementSelected);
  }

  public selectDocument = (elementSelected) => {
    this.populateFieldsAutoComplete(elementSelected);
  }

  public populateFieldsAutoComplete(elementSelected, isResponsable?: boolean) {
    if (!isResponsable) {
      this.guestIdToUse = elementSelected.guestId;
      this.fnrhForm.get('basicData').patchValue({
        personId: elementSelected.personId,
        document: elementSelected.document,
        documentTypeId: elementSelected.documentTypeId,
        gender: elementSelected.gender,
        guestType: elementSelected.guestTypeId,
        incapable: elementSelected.isLegallyIncompetent,
        responsable: elementSelected.responsibleGuestRegistrationId,
        fullName: elementSelected.fullName,
        birthDate: elementSelected.birthDate
      });
      this.setAge(elementSelected.birthDate);
    }
    this.fnrhForm.patchValue({
      basicData: {
        email: elementSelected.email,
        nationality: elementSelected.nationality,
        phone: elementSelected.phoneNumber,
        cellPhone: elementSelected.mobilePhoneNumber
      },
      complementaryInformation: {
        occupation: elementSelected.occupationId,
        vehicle: elementSelected.arrivingBy,
        lastAddress: elementSelected.arrivingFromText,
        nextAddress: elementSelected.nextDestinationText,
        reasonForTravel: elementSelected.purposeOfTrip,
        medicalAgreement: elementSelected.healthInsurance,
        socialName: elementSelected.socialName
      },
      additionalInformation: {
        remarks: elementSelected.additionalInformation
      }
    });
    this.locationEdit = elementSelected.location;

    this.fnrhForm.get('basicData').updateValueAndValidity();
  }

  public configDatePicker() {
    this.myDatePickerOptions = this.dateService.getClearConfigDatePicker();
    this.myDatePickerOptions.showInputField = true;
    this.myDatePickerOptions.editableDateField = true;
    this.myDatePickerOptions.indicateInvalidDate = true;
    const today = new Date();
    this.myDatePickerOptions.disableSince = {
      year: today.getFullYear(),
      month: today.getMonth() + 1,
      day: today.getDate() + 1,
    };
  }

  public onDateChanged(birthDate: string) {
    if (!birthDate) {
      return;
    }

    this.fnrhForm.get('basicData.birthDate').markAsTouched();

    const age = birthDate ? this.calculateAgeByDate(birthDate) : 0;
    const isChild = (this.config.fnrh && this.config.fnrh.isChild)
      || this.checkinService.checkIfIsChild(this.propertyId, age);

    !isChild
      ? this.setDocumentRequired()
      : this.setDocumentNotRequired();

    this.fnrhForm.get('basicData.age').patchValue(age);
    this.responsableControl();
  }

  public setDocumentNotRequired() {
    this.documentRequired = false;
    this.fnrhForm.get('basicData').get('document').clearValidators();
    this.fnrhForm.get('basicData').get('document').updateValueAndValidity();
    this.fnrhForm.get('basicData').get('documentTypeId').clearValidators();
    this.fnrhForm.get('basicData').get('documentTypeId').updateValueAndValidity();
  }

  public setDocumentRequired() {
    this.documentRequired = true;
    this.fnrhForm.get('basicData').get('document').setValidators([Validators.required, this.documentIsValid()]);
    this.fnrhForm.get('basicData').get('document').updateValueAndValidity();
    this.fnrhForm.get('basicData').get('documentTypeId').setValidators([Validators.required]);
    this.fnrhForm.get('basicData').get('documentTypeId').updateValueAndValidity();
  }

  public saveFnrh(needPrint?: boolean) {
    if (this.fnrhForm.valid) {
      const fnrhToSave = this.fnrhMapper.toSave(this.fnrhForm, this.documentList);
      fnrhToSave.guestId = this.guestIdToUse;
      if (!fnrhToSave.propertyId) {
        fnrhToSave.propertyId = this.propertyId;
      }
      if (needPrint) {
        this.print();
        return;
      }

      this.fnrhSaved.emit(fnrhToSave);
    } else {
      this.toasterEmitService
        .emitChange(SuccessError.error, this.translateService.instant('commomData.requiredFields'));
    }
  }

  public setButtonConfig() {
    this.buttonSignConfig = new ButtonConfig();

    this.buttonPrintConfig = new ButtonConfig();
    this.buttonPrintConfig.textButton = 'commomData.fnrh.buttons.print';
    this.buttonPrintConfig.buttonType = ButtonType.Secondary;
    this.buttonPrintConfig.callback = this.printFrnh.bind(this);
    this.buttonPrintConfig.id = 'fnrh-print';

    this.setSaveButton();

    this.buttonCancelConfig = new ButtonConfig();
    this.buttonCancelConfig.textButton = 'action.cancel';
    this.buttonCancelConfig.buttonType = ButtonType.Secondary;
    this.buttonCancelConfig.callback = this.cancel.bind(this);
    this.buttonCancelConfig.id = 'cancel-fnrh-edit';

    this.buttonApplyAddressConfig = new ButtonConfig();
    this.buttonApplyAddressConfig.textButton = 'commomData.fnrh.buttons.applyAddress';
    this.buttonApplyAddressConfig.buttonType = ButtonType.Secondary;
    this.buttonApplyAddressConfig.callback = () => this.toggleModal();
    this.buttonApplyAddressConfig.id = 'fnrh-apply-addres';
    this.buttonApplyAddressConfig.buttonSize = ButtonSize.Small;
    this.buttonApplyAddressConfig.isDisabled = true;

    this.buttonModalCancel = new ButtonConfig();
    this.buttonModalCancel.textButton = 'commomData.cancel';
    this.buttonModalCancel.buttonType = ButtonType.Secondary;
    this.buttonModalCancel.callback = () => this.toggleModal();
    this.buttonModalCancel.id = 'fnrh-modal-cancel';

    this.buttonModalSave = new ButtonConfig();
    this.buttonModalSave.textButton = 'commomData.confirm';
    this.buttonModalSave.buttonType = ButtonType.Primary;
    this.buttonModalSave.callback = () => this.applyAddress();
    this.buttonModalSave.id = 'fnrh-modal-save';
  }

  private setSaveButton(buttonCustomSave?: {textButton: string, buttonType: ButtonType}) {
    this.buttonSaveConfig = new ButtonConfig();
    this.buttonSaveConfig.textButton = 'commomData.fnrh.buttons.save';
    this.buttonSaveConfig.buttonType = ButtonType.Secondary;
    this.buttonSaveConfig.callback = () => this.saveFnrh();
    this.buttonSaveConfig.id = 'fnrh-save';
    this.buttonSaveConfig.isDisabled = true;
    if (this.editModeEnabled) {
      this.buttonSaveConfig.buttonType = ButtonType.Primary;
    }
    if (buttonCustomSave) {
      this.buttonSaveConfig.textButton = buttonCustomSave.textButton;
      this.buttonSaveConfig.buttonType = buttonCustomSave.buttonType;
    }
  }

  public toggleModal(): void {
    this.modalEmitToggleService.emitToggleWithRef('applyAddress');
  }

  public applyAddress(): any {
    this.appliedAddress.emit(this.fnrhForm.get('address').value);
    this.toggleModal();
    this.toasterEmitService
      .emitChange(SuccessError.success, this.translateService.instant('commomData.fnrh.actionsMessages.applyAddress'));
  }

  public setResponsableList() {
    if (this.responsableList) {
      this.selectOptionResponsables = this.commonService
        .toOption(this.responsableList, 'id', 'fullName');
    }
  }

  public updateIncapableValue() {
    this.responsableControl();
  }

  public chooseResponsable() {
    const responsableId = this.fnrhForm.get('basicData.responsable').value;
    if (responsableId) {
      this.getResponsable(this.findResponsableIdByGuestReservationItemId(responsableId));
    }
  }

  public findResponsableIdByGuestReservationItemId(responsableId) {
    const responsable = this.responsableList.filter(item => {
      if (item.id == responsableId) {
        return item;
      }
    })[0];
    this.fnrhForm.get('basicData').get('responsableName').setValue(responsable['fullName']);
    return responsable.guestReservationItemId;
  }

  public getResponsable(guestReservationItemId) {
    this.checkinResource.getFnrh(guestReservationItemId, this.propertyId, 'pt-br')
      .subscribe(fnrhDtoResponse => {
        this.populateFieldsAutoComplete(fnrhDtoResponse, true);
      });
  }


  public responsableControl() {
    const newAge = this.fnrhForm.get('basicData.age').value;
    this.needResponsable = this.fnrhForm.get('basicData.incapable').value === true
      || (!this.commonService.isAdult(newAge) && (this.fnrhForm.get('basicData.birthDate').value) || this.config.fnrh.isChild);

    const responsableControl = this.fnrhForm.get('basicData.responsable');
    if (this.needResponsable) {
      responsableControl.setValidators([Validators.required]);
      const guestRegistrationId = this.fnrhForm.get('basicData.id').value;
      this.responsableList = this.responsableList.filter(x => x.id != guestRegistrationId);
      this.selectOptionResponsables = this.commonService.toOption(this.responsableList, 'id', 'fullName');
    } else {
      responsableControl.clearValidators();
      responsableControl.patchValue(null);
    }
    responsableControl.updateValueAndValidity();
  }

  public printFrnh() {
    this.print();
    return;
  }

  public print() {
    this.fnrhService.printFnrh();
  }

  public getRequiredFields() {
    const {
      documentRequired,
      emailRequired,
      nationalityIsRequired,
      phoneRequired,
      cellphoneRequired,
      localeRequired,
    } = this;
    return {
      documentRequired,
      emailRequired,
      nationalityIsRequired,
      phoneRequired,
      cellphoneRequired,
      localeRequired,
    };
  }

  private getAddressRequiredFields() {
    const { addressComponent } = this;
    const obj = {};
    if ( addressComponent && addressComponent.addressForm && addressComponent.hasRequiredField ) {
      const { addressForm: { controls }, hasRequiredField } = addressComponent;

      Object.keys(controls).forEach( key => {
        obj[key] = hasRequiredField(controls[key]);
      });
    }

    return obj;
  }

  populateComponent(component: ComponentRef<any>) {
    const addressRequiredFields = this.getAddressRequiredFields();
    const requiredFields = this.getRequiredFields();
    const { reservationItemCode } = this.config;
    const fnrhToPrint: FnrhToPrintDto = {
      ...(
          this.fnrhMapper
            .toSave(
                this.fnrhForm,
                null,
                this.config.documentTypesAutocomplete
      )),
      requiredFields,
      addressRequiredFields,
      reservationItemCode,
    };

    fnrhToPrint.guestPolicyPrint = this.config.fnrh.guestPolicyPrint;
    const printSingleGuestInstance: PrintSingleGuestRegistrationComponent
      = <PrintSingleGuestRegistrationComponent>component.instance;

    const filteredGenderList = this.config.genderList
      .filter(item => item.key == fnrhToPrint.gender);

    const filteredGuestTypeList = this.config.guestTypeList
      .filter(guestType => guestType.key == fnrhToPrint.guestTypeId);

    const filteredNationalityOptionsList = this.nationalityOptionsList
      .filter(nationality => nationality.key == fnrhToPrint.nationality);

    const filteredOccupationList = this.config.occupationList
      .filter(item => item.key == fnrhToPrint.occupationId);

    const filteredReasonList = this.config.reasonList
      .filter(item => item.key == fnrhToPrint.purposeOfTrip);

    const filteredVehicleList = this.config.vehicleList
      .filter(item => item.key == fnrhToPrint.arrivingBy);

    fnrhToPrint.propertyId = this.propertyId;


    if ( filteredGenderList && filteredGenderList.length) {
      printSingleGuestInstance.genderName = filteredGenderList[0].value;
    }

    if ( filteredGuestTypeList && filteredGuestTypeList.length ) {
      printSingleGuestInstance.guestTypeName = filteredGuestTypeList[0].value;
    }

    if ( filteredNationalityOptionsList && filteredNationalityOptionsList.length ) {
      printSingleGuestInstance.nationalityName = filteredNationalityOptionsList[0].value;
    }

    if (
      fnrhToPrint.occupationId
      && filteredOccupationList
      && filteredOccupationList.length
    ) {
      printSingleGuestInstance.occupationName = filteredOccupationList[0].value;
    }

    if (fnrhToPrint.purposeOfTrip && filteredReasonList && filteredReasonList.length) {
      printSingleGuestInstance.reasonForTravel = filteredReasonList[0].value;
    }

    if (fnrhToPrint.arrivingBy && filteredVehicleList && filteredVehicleList.length) {
      printSingleGuestInstance.transportName = filteredVehicleList[0].value;
    }

    printSingleGuestInstance.addressTypeName = '';
    fnrhToPrint.roomNumber = this.config.dataReservation.roomNumber;
    fnrhToPrint.roomTypeAbbreviation = this.config.dataReservation.roomTypeAbbreviation;
    fnrhToPrint.adultCount = this.config.dataReservation.adultCount;
    fnrhToPrint.childCount = this.config.dataReservation.childCount;
    fnrhToPrint.arrivalDate = this.config.dataReservation.arrivalDate;
    fnrhToPrint.departureDate = this.config.dataReservation.departureDate;

    printSingleGuestInstance.frnh = fnrhToPrint;
  }

  private requireLocale() {
    const documentTypeId = this.fnrhForm.get('basicData').get('documentTypeId').value;
    return documentTypeId == DocumentTypeNaturalEnum.CPF
      || documentTypeId == this.DOCUMENT_RG;
  }

  public checkValidationsDocument() {
    if (this.fnrhForm && this.fnrhForm.get('basicData').get('documentTypeId').value) {
      this.localeRequired = this.requireLocale();
    }
    this.apllyMaskToDocument(this.fnrhForm.get('basicData.document').value);
  }

  public maskDocument(event) {
    const documentString = event.target.value;
    const backspaceCode = 8;
    if (event.keyCode != backspaceCode) {
      this.apllyMaskToDocument(documentString);
    }
  }

  public apllyMaskToDocument(documentString) {
    if (documentString) {
      this.fnrhForm.get('basicData.document')
        .setValue(
          this.documentMaskService
            .maskDocumentStringByDocumentTypeId(documentString, this.fnrhForm.get('basicData.documentTypeId').value),
        );
    }
  }

  public cancel() {
    this.location.back();
  }

  ngOnDestroy() {
    this.printSubscription.unsubscribe();
  }

  private documentIsValid() {
    return (control) => {
      if (this.fnrhForm && control.value) {
        const documentTypeId = this.fnrhForm.get('basicData').get('documentTypeId').value;
        const countryCode = this.fnrhForm.get('address').get('countryCode').value;

        switch (+documentTypeId) {
          case DocumentTypeNaturalEnum.CPF:
            return this.documentService
              .cpfControlIsValid(documentTypeId, control.value);
          case DocumentTypeNaturalEnum.NIF:
            if (countryCode == CountryCodeEnum.PT) {
              return this.documentService
                .nifControlIsValid(documentTypeId, control.value, countryCode);
            }
            return null;
          default:
            return null;
        }
      }
    };
  }

  public resetFnrh() {
    this.fnrhForm.reset();
    this.locationEdit = null;
  }

  public calculateAgeByDate(date) {
    const universalDate = moment(date, DateService.DATE_FORMAT_UNIVERSAL).toDate();
    return this.dateService.calculateAgeByDate(universalDate);
  }

  private validateAvailableSlots(): ValidatorFn {
    return (control: AbstractControl) => {
      if (this.config.validateAvailableSlots) {
        const slot = this.config.validateAvailableSlots;
        const age = control.value ? this.calculateAgeByDate(control.value) : 0;
        const hasAdultSlot = this.checkinService
          .hasAdultsAvailableSlots(slot.adultCapacity, slot.adultCount);
        const hasChildSlot = this.checkinService
          .hasChildrenAvailableSlots(slot.childCapacity, slot.childCount);
        const isChild = (this.config.fnrh && this.config.fnrh.isChild)
          || this.checkinService.checkIfIsChild(this.propertyId, age);

        if (!hasChildSlot && !hasAdultSlot) {
          return {notAvailableSlot: 'alert.noSlotsAvailable'};
        }

        if (!isChild) {
          if (!hasAdultSlot) {
            return {notAvailableSlot: 'alert.cantAddAdults'};
          }
        } else {
          if (hasChildSlot) {
            const range = this.commonService.getAgeRangeFromAge(this.propertyId, age);
            if (range != this.childAgeRange) {
              if (this.childAgeRange !== undefined && this.childAgeRange !== null) {
                this.toasterEmitService.emitChange(SuccessError.error, 'alert.childAgeChangeRange');
                return null;
              }
              this.childAgeRange = range;
            }
          } else if (hasAdultSlot) {
            return {notAvailableSlot: 'alert.adultAgeLowerThanAllowed'};
          }
        }
      }
      return null;
    };
  }

}
