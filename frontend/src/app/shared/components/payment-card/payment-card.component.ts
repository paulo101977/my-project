import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SelectObjectOption } from '../../models/selectModel';
import { BillingItemPlasticBrandCompanyClient } from '../../models/billingType/billing-item-plastic-brand-company-client';
import { CommomService } from '../../services/shared/commom.service';
import { SharedService } from './../../services/shared/shared.service';
import { BillingItemResource } from '../../resources/billingItem/billing-item.resource';

@Component({
  selector: 'payment-card',
  templateUrl: './payment-card.component.html',
  styleUrls: ['./payment-card.component.css'],
})
export class PaymentCardComponent implements OnInit, OnChanges {
  @Input() paymentForm: FormGroup;
  @Input() paymentTypeId: number;
  @Input() isDebit: boolean;

  private plasticBrandPropertyList: Array<BillingItemPlasticBrandCompanyClient>;
  public plasticBrandPropertyListSelect: Array<SelectObjectOption>;
  public optionsCurrencyMask: any;
  public installmentNumberList: Array<number>;

  constructor(
    private billingItemResource: BillingItemResource,
    private commomService: CommomService,
    private sharedService: SharedService,
  ) {}

  ngOnInit() {
    this.setVars();
    this.setListeners();
  }

  ngOnChanges() {
    this.getPlasticBrandPropertyList();
  }

  private setVars(): void {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
  }

  private setListeners(): void {
    this.paymentForm.get('card.plasticBrandPropertyId').valueChanges.subscribe(plasticBrandPropertyId => {
      if (this.plasticBrandPropertyList) {
        const plasticBrandProperty = this.sharedService.getElementInList(
          null,
          this.plasticBrandPropertyList,
          p => p.id == plasticBrandPropertyId,
        );
        this.installmentNumberList = plasticBrandProperty
          ? this.sharedService.createRange(plasticBrandProperty.maximumInstallmentsQuantity)
          : [1];
      }
    });
  }

  private getPlasticBrandPropertyList(): void {
    this.plasticBrandPropertyListSelect = new Array<SelectObjectOption>();
    this.billingItemResource.getAllBillingItemPlasticBrandCompanyClientListByPaymentTypeId(this.paymentTypeId).subscribe(response => {
      this.plasticBrandPropertyList = response.items;
      this.plasticBrandPropertyListSelect = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        response,
        'id',
        'plasticBrandFormatted',
      );
    });
  }

  private setInstallmentNumberList(billingItemPlasticBrandCompanyClient: BillingItemPlasticBrandCompanyClient): void {
    this.installmentNumberList = [1, 2, 3, 4, 5, 6];
  }
}
