import { Component, Input, OnInit } from '@angular/core';
import { DaysEnum } from 'app/shared/models/days.enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-week-day-show',
  templateUrl: './week-day-show.component.html',
  styleUrls: ['./week-day-show.component.scss']
})
export class WeekDayShowComponent implements OnInit {

  public dayList: Array<any>;
  public _weeklist: Array<number>;

  // TODO: change if necessary
  @Input() set weekList(_weeklist) {
    this._weeklist = _weeklist;
  }

  constructor(
    private translateService: TranslateService,
  ) { }

  public setDayList() {
    this.dayList = [{
      label: this.translateService.instant('label.initialsSunday'),
      day: DaysEnum.Sunday
    },
    {
      label: this.translateService.instant('label.initialsMonday'),
      day: DaysEnum.Monday
    },
    {
      label: this.translateService.instant('label.initialsTuesday'),
      day: DaysEnum.Tuesday
    },
    {
      label: this.translateService.instant('label.initialsWednesday'),
      day: DaysEnum.Wednesday
    },
    {
      label: this.translateService.instant('label.initialsThursday'),
      day: DaysEnum.Thursday
    },
    {
      label: this.translateService.instant('label.initialsFriday'),
      day: DaysEnum.Friday
    },
    {
      label: this.translateService.instant('label.initialsSaturday'),
      day: DaysEnum.Saturday
    }];
  }

  public checkIsEnabled( day ) {
    const { _weeklist } = this;
    return _weeklist && _weeklist.indexOf(day) >= 0;
  }

  ngOnInit() {
    this.setDayList();
  }
}
