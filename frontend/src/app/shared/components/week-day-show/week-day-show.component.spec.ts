import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { WeekDayShowComponent } from './week-day-show.component';
import { TranslateModule } from '@ngx-translate/core';

describe('WeekDayShowComponent', () => {
  let component: WeekDayShowComponent;
  let fixture: ComponentFixture<WeekDayShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeekDayShowComponent ],
      imports: [
        TranslateModule.forRoot(),
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekDayShowComponent);
    component = fixture.componentInstance;

    spyOn(component, 'setDayList').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', () => {
    component.ngOnInit();

    expect(component.setDayList).toHaveBeenCalled();
  });

  it('should test checkIsEnabled', () => {
    const list = [0, 1, 4];

    component.ngOnInit();

    component.weekList = list;

    expect(component.checkIsEnabled(list[1])).toEqual(true);
    expect(component.checkIsEnabled(6)).toEqual(false);
  });

  it('should test component render class', fakeAsync(() => {
    const list = [2, 4, 6];

    component.weekList = list;
    component.ngOnInit();


    fixture.detectChanges();

    tick();


    const wrapper = fixture.debugElement.nativeElement.querySelector('.week-show-comp-wrapper');
    const weekCompArr = wrapper.querySelectorAll('.day');
    const disabledArr = wrapper.querySelectorAll('.day.disabled');

    expect(weekCompArr.length).toEqual(7);
    expect(disabledArr.length).toEqual(4);
    expect(wrapper.textContent).toContain('label.initialsMonday');
  }));
});
