import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ColorPickerModule } from 'ngx-color-picker';
import { TranslateModule } from '@ngx-translate/core';
import { ColorPickerComponent } from './color-picker.component';

describe('ColorPickerComponent', () => {
  let component: ColorPickerComponent;
  let fixture: ComponentFixture<ColorPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ColorPickerComponent],
      imports: [ColorPickerModule, TranslateModule.forRoot()],
      providers: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select color a default Color', () => {
    const colorList = component._colorList;
    const qtdColors = colorList.length;
    const randomColor = colorList[Math.floor(Math.random() * colorList.length)];

    component.selectColor(randomColor);
    expect(component._colorList.length).toBe(qtdColors);
    expect(component.colorSelected).toBe(randomColor);
  });

  it('should select a diferent color', () => {
    const color = '#FFFFFF';
    const qtdColors = component._colorList.length;
    component.selectColor(color);

    expect(component._colorList.length).toBe(qtdColors + 1);
    expect(component.colorSelected).toBe(color);
  });

  it('should set two custom color and array will contains only last color selected', () => {
    const color1 = '#FFFFFF';
    const color2 = '#000000';
    const qtdColors = component.MAX_COLOR_BOXES;

    component.selectColor(color1);
    component.selectColor(color2);

    expect(component._colorList.length).toBe(qtdColors);
    expect(component.colorSelected).toBe(color2);
  });
});
