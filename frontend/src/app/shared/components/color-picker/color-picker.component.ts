import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'thx-color-picker',
  templateUrl: './color-picker.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ColorPickerComponent),
      multi: true,
    },
  ],
})
export class ColorPickerComponent implements OnInit, ControlValueAccessor {
  public COLOR_BLUE = '#009CC0';
  public COLOR_NAVY_BLUE = '#005C73';
  public COLOR_LIGHT_RED = '#FF6969';
  public COLOR_RED = '#D0021B';
  public COLOR_LIGHT_BLUE = '#00C5FF';
  public COLOR_DARK_BLUE = '#0098C5';
  public COLOR_LIGHT_PURPLE = '#A497E3';
  public COLOR_PURPLE = '#BD10E0';
  public COLOR_LIGHT_GREY = '#8F9EA1';
  public COLOR_GREY = '#50595B';
  public COLOR_LIGHT_YELLOW = '#F8E71C';
  public COLOR_YELLOW = '#FF9D32';
  public COLOR_LIGHT_OCRE = '#CE781A';
  public COLOR_OCRE = '#8B572A';
  public COLOR_LIGHT_WATHER_GREEN = '#50E3C2';
  public COLOR_WATHER_GREEN = '#00B38C';
  public COLOR_LIGHT_GREEN = '#B8E986';
  public COLOR_GREEN = '#417505';
  public COLOR_LIGHT_VIOLET = '#9013FE';
  public COLOR_VIOLET = '#750566';
  public COLOR_LIGHT_PINK = '#EA6FF0';
  public COLOR_PINK = '#DA1DC1';

  public MAX_COLOR_BOXES = 23;
  private _defaultColorList: Array<string>;
  public _colorList: Array<string>;
  public colorSelected: string;

  constructor() {}

  propagateChange = (_: any) => {};
  ngOnInit() {
    this.setColors();
  }

  private setColors() {
    this._defaultColorList = [
      this.COLOR_BLUE,
      this.COLOR_NAVY_BLUE,
      this.COLOR_LIGHT_RED,
      this.COLOR_RED,
      this.COLOR_LIGHT_BLUE,
      this.COLOR_DARK_BLUE,
      this.COLOR_LIGHT_PURPLE,
      this.COLOR_PURPLE,
      this.COLOR_LIGHT_GREY,
      this.COLOR_GREY,
      this.COLOR_LIGHT_YELLOW,
      this.COLOR_YELLOW,
      this.COLOR_LIGHT_OCRE,
      this.COLOR_OCRE,
      this.COLOR_LIGHT_WATHER_GREEN,
      this.COLOR_WATHER_GREEN,
      this.COLOR_LIGHT_GREEN,
      this.COLOR_GREEN,
      this.COLOR_LIGHT_VIOLET,
      this.COLOR_VIOLET,
      this.COLOR_LIGHT_PINK,
      this.COLOR_PINK
    ];

    this._colorList = [...this._defaultColorList];
  }

  selectColor(color: string) {
    this.colorSelected = color.toLocaleUpperCase();
    this.propagateChange(this.colorSelected);

    if (!this._colorList.some(colorAux => colorAux.toLocaleUpperCase() == this.colorSelected)) {
      if (this._colorList.length < this.MAX_COLOR_BOXES) {
        this._colorList.push(this.colorSelected);
      } else {
        this._colorList.splice(this._colorList.length - 1, 1, this.colorSelected);
      }
    }
  }

  /*ControlValueAcessor Contract */
  writeValue(value: any): void {
    this._colorList = [...this._defaultColorList];
    if (value !== null && value !== undefined) {
      this.selectColor(value);
    } else {
      this.colorSelected = null;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}
}
