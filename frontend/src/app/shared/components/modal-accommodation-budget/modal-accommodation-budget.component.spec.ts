import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { ModalAccommodationBudgetComponent } from './modal-accommodation-budget.component';
import { SuccessError } from './../../models/success-error-enum';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { CoreModule } from '@app/core/core.module';

describe('AccommodationBudget', () => {
  let component: ModalAccommodationBudgetComponent;
  let fixture: ComponentFixture<ModalAccommodationBudgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, TranslateModule.forRoot(), FormsModule, CurrencyMaskModule, CoreModule],
      declarations: [ModalAccommodationBudgetComponent],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAccommodationBudgetComponent);
    component = fixture.componentInstance;

    spyOn<any>(component.modalAccommodationBudgetFormEmitService, 'changeEmitted$').and.callFake(() => {});

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should execute showModal', () => {
    spyOn(component, 'showModal');

    const roomTypes = [];

    const roomTypeMock = new RoomType();
    roomTypeMock.id = 1;
    roomTypeMock.name = 'Quarto Standard';
    roomTypeMock.abbreviation = 'SPCD';
    roomTypeMock.order = 1;
    roomTypeMock.adultCapacity = 2;
    roomTypeMock.childCapacity = 1;
    roomTypeMock.roomTypeBedTypeList = [];
    roomTypeMock.isActive = true;
    roomTypeMock.isInUse = false;

    const roomTypeMock2 = new RoomType();
    roomTypeMock2.id = 2;
    roomTypeMock2.name = 'Quarto Luxo';
    roomTypeMock2.abbreviation = 'LXO';
    roomTypeMock2.order = 1;
    roomTypeMock2.adultCapacity = 3;
    roomTypeMock2.childCapacity = 0;
    roomTypeMock2.roomTypeBedTypeList = [];
    roomTypeMock2.isActive = true;
    roomTypeMock2.isInUse = false;

    roomTypes.push(roomTypeMock);
    roomTypes.push(roomTypeMock2);

    const dates = [];
    dates.push(new Date());
    dates.push(new Date());
    dates.push(new Date());

    const dailyPrices = new Map<Date, number>();

    component.modalAccommodationBudgetFormEmitService.emitSimpleModal(roomTypes[0], roomTypes, dates, dailyPrices);

    expect(component['budgetRoomType']).toBeTruthy();
    expect(component.roomTypeSelected).toEqual(roomTypes[0]);
    expect(component.roomTypeList).toEqual(roomTypes);
    expect(component.showModal).toHaveBeenCalled();
  });

  it('should set isVisible for true in showModal', () => {
    spyOn<any>(component, 'setPrices');
    spyOn<any>(component, 'setConfigCountryDateAndCurrency');

    component.showModal();

    expect(component.isVisible).toEqual(true);
  });

  it('should set isVisible for false in showModal', () => {
    component.closeModal();

    expect(component.isVisible).toEqual(false);
  });

  it('should set prices Default to 100 in showModal', () => {
    component.dates = [];
    component.dates.push.apply(component.dates, [new Date(), new Date(), new Date()]);

    component.showModal();

    expect(component.prices.every(price => price == 100)).toBeTruthy();
  });

  it('should set prices with dailyPrices existing in showModal', () => {
    component.dates = new Array<Date>();
    component.dates.push(new Date());
    component['dailyPrices'] = new Map<Date, number>();

    component['dailyPrices'].set(component.dates[0], 150);

    component.showModal();

    expect(component.prices.every(price => price == 150)).toBeTruthy();
  });

  it('should set config Country Date And Currency', () => {
    component.currencyCountry = window.navigator.language;
    if (component.currencyCountry == 'pt-BR') {
      component.optionsCountryDate = 'dd/MM/yyyy';
    } else if (component.currencyCountry == 'en-US') {
      component.optionsCountryDate = 'yyyy/MM/dd';
    }
    component.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };

    component['setConfigCountryDateAndCurrency']();

    if (component.currencyCountry == 'pt-BR') {
      expect(component.optionsCountryDate).toEqual('dd/MM/yyyy');
    } else if (component.currencyCountry == 'en-US') {
      expect(component.optionsCountryDate).toEqual('yyyy/MM/dd');
    }
    expect(component.optionsCurrencyMask).toEqual({ prefix: '', thousands: '.', decimal: ',', align: 'left' });
  });

  it('should set dailyPrices', () => {
    component.dates = [];
    component.dates.push.apply(
        component.dates,
        [new Date('2017/07/10'), new Date('2017/07/11'), new Date('2017/07/12')]
    );
    component.prices = [];
    component.prices.push.apply(component.prices, [100, 200, 300]);

    component['setDailyPrices']();

    expect(component['dailyPrices'].keys().next().value).toEqual(new Date('2017/07/10'));
    expect(component['dailyPrices'].values().next().value).toEqual(100);
    expect(component['dailyPrices'].size).toEqual(component.dates.length);
  });

  it('should closeModal, set dailyPrices and emit onDailyPriceUpdated in runCallbackFunction', () => {
    spyOn(component.dailyPriceUpdated, 'emit');
    spyOn(component, 'closeModal');

    component.dates = new Array<Date>();
    component.dates.push(new Date());
    component.prices = new Array<number>();
    component.prices.push(1);
    const dailyPrices = new Map<Date, number>();

    component.runCallbackFunction();

    expect(component.dailyPriceUpdated.emit).toHaveBeenCalledWith(component['budgetRoomType']);
    expect(component.closeModal).toHaveBeenCalled();
  });

  it('should emit toasterEmitService error in runCallbackFunction', () => {
    spyOn(component['toasterEmitService'], 'emitChange');
    spyOn(component.dailyPriceUpdated, 'emit');
    spyOn(component, 'closeModal');

    component.dates = new Array<Date>();
    component.dates.push(new Date());
    component.prices = new Array<number>();
    component.prices.push(undefined);
    const dailyPrices = new Map<Date, number>();

    component.runCallbackFunction();

    expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
      SuccessError.error,
      'reservationModule.commomData.setTheDailyReservationValues',
    );
    expect(component.dailyPriceUpdated.emit).not.toHaveBeenCalledWith(component['dailyPrices']);
    expect(component.closeModal).not.toHaveBeenCalled();
  });
});
