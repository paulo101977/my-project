import { roomRoutes } from '../../../room/room-routing.module';
import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ToasterEmitService } from './../../services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { ModalAccommodationBudgetFormEmitService } from './../../services/shared/modal-accommodation-budget-form-emit.service';
import { SuccessError } from './../../models/success-error-enum';
import { BudgetRoomType } from './../../models/budget-room-type';
import { RoomType } from '../../../room-type/shared/models/room-type';

@Component({
  selector: 'app-modal-accommodation-budget',
  templateUrl: './modal-accommodation-budget.component.html',
  styles: [
    `
      .overflow-plots {
        max-height: 185px;
        overflow-y: auto;
        overflow-x: hidden;
      }
    `,
  ],
})
export class ModalAccommodationBudgetComponent {
  @Output() dailyPriceUpdated = new EventEmitter();

  private budgetRoomType: BudgetRoomType;
  private dailyPrices: Map<Date, number>;
  public isVisible: boolean;
  public roomTypeList: Array<RoomType>;
  public dates: Array<Date>;
  public currencyCountry: string;
  public optionsCountryDate: string;
  public optionsCurrencyMask: any;
  public roomTypeSelected: RoomType;
  public prices: Array<any>;
  public callbackFunction: any;

  constructor(
    private translateService: TranslateService,
    private toasterEmitService: ToasterEmitService,
    public modalAccommodationBudgetFormEmitService: ModalAccommodationBudgetFormEmitService,
  ) {
    this.budgetRoomType = new BudgetRoomType();
    modalAccommodationBudgetFormEmitService.changeEmitted$.subscribe(response => {
      this.budgetRoomType = new BudgetRoomType();
      this.roomTypeSelected = response[0];
      this.roomTypeList = response[1];
      this.roomTypeSelected = this.getRoomTypeSelectedOfRoomTypeList();
      this.dates = response[2];
      this.dailyPrices = response[3];
      this.showModal();
    });
  }

  private getRoomTypeSelectedOfRoomTypeList(): RoomType {
    const roomType = this.roomTypeList.find(r => r.id == this.roomTypeSelected.id);
    if (!roomType) {
      return this.roomTypeSelected;
    }
    return roomType;
  }

  public showModal(): void {
    this.setPrices();
    this.setConfigCountryDateAndCurrency();
    this.isVisible = true;
  }

  public closeModal() {
    this.isVisible = false;
  }

  public runCallbackFunction(): void {
    this.setDailyPrices();
    if (this.dailyPricesIsInvalid()) {
      this.toasterEmitService.emitChange(
        SuccessError.error,
        this.translateService.instant('reservationModule.commomData.setTheDailyReservationValues'),
      );
      return;
    }
    this.budgetRoomType.dailyPriceList = this.dailyPrices;
    this.budgetRoomType.roomTypeRequested = this.roomTypeSelected;
    this.dailyPriceUpdated.emit(this.budgetRoomType);
    this.closeModal();
  }

  private setConfigCountryDateAndCurrency() {
    this.currencyCountry = window.navigator.language;
    if (this.currencyCountry == 'pt-BR') {
      this.optionsCountryDate = 'dd/MM/yyyy';
    } else if (this.currencyCountry == 'en-US') {
      this.optionsCountryDate = 'yyyy/MM/dd';
    }
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
  }

  private setPrices() {
    this.prices = new Array<number>();
    if (this.dailyPrices) {
      const self = this;
      this.dailyPrices.forEach(function(price, day) {
        self.prices.push(price);
      }, this.dailyPrices);
    } else {
      this.dates.forEach(() => {
        this.prices.push(100);
      });
    }
  }

  private setDailyPrices() {
    this.dailyPrices = new Map<Date, number>();
    this.dates.forEach((date, index) => {
      this.dailyPrices.set(date, this.prices[index]);
    });
  }

  private dailyPricesIsInvalid(): boolean {
    let searchPriceWithoutValue = false;
    this.dailyPrices.forEach(function(price, day) {
      if (price == undefined || price == null) {
        searchPriceWithoutValue = true;
      }
    }, this.dailyPrices);
    return searchPriceWithoutValue;
  }
}
