import { Component, OnInit, Input, OnChanges, DoCheck } from '@angular/core';
import { ButtonConfig, ButtonType, ButtonSize } from './../../models/button-config';
import { Type } from 'app/shared/models/button-config';

@Component({
  selector: 'app-button',
  templateUrl: 'button.component.html',
  styleUrls: ['button.component.css'],
})
export class ButtonComponent implements OnInit, OnChanges, DoCheck {
  @Input() buttonConfig: ButtonConfig;

  public isSecondaryButton: boolean;
  public isPrimaryButton: boolean;
  public isDangerButton: boolean;
  public isButtonWidget: boolean;
  public isButtonNone: boolean;
  public isButtonOutline: boolean;
  public isSmallSize: boolean;
  public isFullSize: boolean;
  public textButton: string;
  public textComplementButton: string;
  public id: string;
  public isDisabled: boolean;
  public extraClass: string;
  public width: string;
  public isActive: boolean;


  // fix the BUG 499
  public type: Type; // submit, button, etc.

  ngOnInit() {
    this.setVars();
  }

  ngDoCheck() {
    this.textComplementButton = this.buttonConfig.textComplementButton;
    if (this.buttonConfig) {
      this.isDisabled = this.buttonConfig.isDisabled;
      this.isActive = this.buttonConfig.isActive;
    }
  }

  ngOnChanges(changes) {
    if (changes) {
      if (changes.hasOwnProperty('buttonConfig') && changes.buttonConfig.currentValue) {
        this.buttonConfig = changes.buttonConfig.currentValue;
        this.setVars();
      }
      this.isDisabled = changes.buttonConfig.currentValue.isDisabled;
    }
  }

  setVars() {
    this.isPrimaryButton = ButtonType.Primary === this.buttonConfig.buttonType;
    this.isSecondaryButton = ButtonType.Secondary === this.buttonConfig.buttonType;
    this.isDangerButton = ButtonType.Danger === this.buttonConfig.buttonType;
    this.isButtonWidget = ButtonType.Widget === this.buttonConfig.buttonType;
    this.isButtonNone = ButtonType.None === this.buttonConfig.buttonType;
    this.isButtonOutline = ButtonType.Outline === this.buttonConfig.buttonType;
    this.isSmallSize = this.buttonConfig.buttonSize === ButtonSize.Small;
    this.isFullSize = this.buttonConfig.buttonSize === ButtonSize.Full;
    this.textButton = this.buttonConfig.textButton;
    this.textComplementButton = this.buttonConfig.textComplementButton;
    this.id = this.buttonConfig.id;
    this.isDisabled = this.buttonConfig.isDisabled;
    this.extraClass = this.buttonConfig.extraClass;
    this.width = this.buttonConfig.width;
    this.isActive = this.buttonConfig.isActive;
    this.type = this.buttonConfig.type;
  }

  public clickButton(event) {
    this.buttonConfig.callback(event);
  }
}
