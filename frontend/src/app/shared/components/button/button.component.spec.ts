import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ButtonConfig, ButtonSize, ButtonType, Type } from '../../models/button-config';
import { ButtonComponent } from './button.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;

  const Button = {
    callback: () => {},
    textButton: 'commomData.createAccount',
    textComplementButton: '',
    buttonType: ButtonType.Primary,
    id: 'id',
    buttonSize: ButtonSize.Small,
    isDisabled: false,
    extraClass: '',
    isActive: false,
    width: null,
    type: Type.Button
  } as ButtonConfig;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateTestingModule],
      declarations: [ButtonComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;

    component.buttonConfig = Button;

    fixture.detectChanges();
  });

  it('should set buttonConfigs', () => {
    expect(component.isPrimaryButton).toBeTruthy();
    expect(component.isSecondaryButton).toBeFalsy();
    expect(component.textButton).toEqual(Button.textButton);
    expect(component.id).toEqual(Button.id);
    expect(component.isSmallSize).toBeTruthy();
    expect(component.isDisabled).toBeFalsy();
    expect(component.type).toEqual(Type.Button);
  });

  it('should call callBackFunction when clickButton', () => {
    spyOn(component.buttonConfig, 'callback');

    component.clickButton({});

    expect(component.buttonConfig.callback).toHaveBeenCalled();
  });
});
