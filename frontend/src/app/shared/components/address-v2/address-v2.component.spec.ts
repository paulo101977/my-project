import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressV2Component } from './address-v2.component';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Country } from 'app/shared/models/country';
import { AddressService } from 'app/shared/services/shared/address.service';
import { AddressType } from 'app/shared/models/address-type-enum';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createAccessorComponent } from '../../../../../testing';
import {ThexApiTestingModule} from '@inovacaocmnet/thx-bifrost';

const thxInputText = createAccessorComponent('thx-input-text');
const thxSelectV2 = createAccessorComponent('thx-select-v2');

describe('AddressV2Component', () => {
  let component: AddressV2Component;
  let fixture: ComponentFixture<AddressV2Component>;
  const addressServiceStub: Partial<AddressService> = {
    getCountries(): Array<Country> {
      return [];
    } ,
    getAddressTypeList(): any[] {
      return [];
      },
    getAddressByGooglePlace(place: any, selectedCountrySubdivision): {} {
      return {};
    },
    getLocationCategoryEnumByLocationCategoryId(locationCategoryId: number): AddressType {
      return AddressType.Residencial;
      },
    getLocationCategoryIdByLocationCategoryEnum(addressTypeEnum: AddressType): number {
      return 1;
      },
  };

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        ThexApiTestingModule
      ],
      declarations: [
        AddressV2Component,
        thxInputText,
        thxSelectV2,
      ],
      providers: [
        {
          provide: AddressService,
          useValue: addressServiceStub
        }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(AddressV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
