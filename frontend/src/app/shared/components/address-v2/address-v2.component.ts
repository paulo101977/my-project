import { Component, Input, OnInit, Output, DoCheck } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AddressResource } from 'app/shared/resources/address/address.resource';
import { Country } from 'app/shared/models/country';
import { SelectOptionItem, toSelectOption } from '@inovacao-cmnet/thx-ui';
import { AddressService } from 'app/shared/services/shared/address.service';

@Component({
  selector: 'app-address-v2',
  templateUrl: './address-v2.component.html',
  styleUrls: ['./address-v2.component.scss']
})
export class AddressV2Component implements OnInit, DoCheck {

  @Input() showAddressType = true;

  public addressForm: FormGroup;

  // locale
  public locale: string;
  public optionGooglePlaceApi: any;
  private selectedCountry: Country;

  // lists
  private countryList: Country[];
  public countryOptionSelectList: SelectOptionItem[];
  public addressTypeOptionSelectList: SelectOptionItem[];

  constructor(private fb: FormBuilder,
              private addressService: AddressService) { }

  ngOnInit() {
    this.setForm();
    this.getAddressTypeList();
  }

  ngDoCheck() {
    this.getCountries();
  }


  private getCountries() {
    const countryList = this.addressService.getCountries();
    if (this.countryList != countryList) {
      this.countryList = countryList;
      this.countryOptionSelectList = toSelectOption(this.countryList, 'name', 'countryTwoLetterIsoCode');
    }
  }

  private setForm() {
    this.addressForm = this.fb.group({
      addressType: null,
      countryCode: null,
      streetNumber: null,
      streetComplement: null,
      postalCode: null,
      subdivision: [{value: null, disabled: true}],
      division: [{value: null, disabled: true}],
      streetName: null,
      neighborhood: null
    });

    this.addressForm.get('countryCode')
      .valueChanges
      .subscribe( value => {
        this.setSelectedCountry(value);
      this.setGooglePlaceRestrictions();
    });
  }

  searchLocale(eventGoogleApi: any) {
    if (eventGoogleApi && eventGoogleApi.address_components) {
      const address = this.addressService.getAddressByGooglePlace(eventGoogleApi, this.addressForm.get('countryCode'));
      this.setAddressInForm(address);
    }
  }

  public getAddressTypeList() {
    this.addressTypeOptionSelectList = this.addressService
      .getAddressTypeList();
  }

  private setSelectedCountry(countryId) {
    if (this.countryList) {
      this.selectedCountry = this.countryList.find( country => country.countryTwoLetterIsoCode == countryId);
    }
  }

  private setGooglePlaceRestrictions() {
    if (this.selectedCountry) {
      this.optionGooglePlaceApi = {
        componentRestrictions: {country: this.selectedCountry.countryTwoLetterIsoCode},
      };
    }
  }

  private setAddressInForm(address: any) {
    const { division, subdivision, streetNumber, postalCode, countryCode, neighborhood, streetName } = address;

    this.addressForm.patchValue({
      countryCode,
      division,
      subdivision,
      streetNumber,
      postalCode,
      neighborhood,
      streetName
    });
  }
}
