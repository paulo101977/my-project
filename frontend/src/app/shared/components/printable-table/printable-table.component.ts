import { Component, Input, OnChanges, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { PropertyService } from '../../services/property/property.service';
import { environment } from '@environment';

@Component({
  selector: 'app-printable-table',
  templateUrl: './printable-table.component.html',
  styleUrls: ['./printable-table.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PrintableTableComponent implements OnInit, OnChanges {
  public date;
  public appVersion: string;
  private propertyId: number;
  public propertyName: string;
  public hasHeaderColumn: boolean;
  public hasHeaderColumnExtra: boolean;
  public _periodInfo: any;
  public columns: any[];
  public columnsExtra: any[];
  public colSpan = 0;

  @Input() columnsName;
  @Input() extraItems;
  @Input() items;
  @Input() props;
  @Input() title;
  @Input() showPropertyName = true;
  @Input() showDateHour = true;
  @Input() singleTableForPage = true;
  @Input() showComments;
  @Input() set periodInfo(_periodInfo) {
    this._periodInfo = _periodInfo;
  }
  @Input() footerTemplate: TemplateRef<any>;

  @ViewChild('printableDefaultTemplate') printableDefaultTemplate: TemplateRef<any>;
  @ViewChild('dateTemplate') dateTemplate: TemplateRef<any>;
  @ViewChild('printableExtraTemplate') printableExtraTemplate: TemplateRef <any>;

  constructor(
    private route: ActivatedRoute,
    private propertyService: PropertyService
  ) {
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.date = `${ moment().format('L') } ${ moment().format('LTS') }`;
    const property = this.propertyService.getPropertySession(this.propertyId);
    if (property) {
      this.propertyName = property.name;
    }
  }

  ngOnChanges() {
    this.date = `${ moment().format('L') } ${ moment().format('LTS') }`;
    this.appVersion = environment.appVersion;
    if (this.columnsName) {
      this.hasHeaderColumn = this.columnsName.some(c => c.name);
      this.columns = this.columnsName.map(column => {
        this.colSpan = this.colSpan + 1;

        let printableTemplate = !column.hasOwnProperty('printableTemplate')
          ? this.printableDefaultTemplate
          : column.printableTemplate;
          if (column.hasOwnProperty('type') && column.type == 'date') {
            printableTemplate = this.dateTemplate;
          }
        return {
          ...column,
          align: column.printableAlignRight
            ? 'right'
            : 'left',
          width: column.hasOwnProperty('width')
            ? column.width
            : 'auto',
          printableTemplate: printableTemplate
        };
      });
    }
  }

}

