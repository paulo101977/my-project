import { Component, Input } from '@angular/core';
import { ConfirmModalDataConfig } from '../../models/confirm-modal-data-config';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: 'confirm-modal.component.html',
})
export class ConfirmModalComponent {
  @Input() confirmModalDataConfig: ConfirmModalDataConfig;
}
