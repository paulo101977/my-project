import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import * as _ from 'lodash';

@Component({
  selector: 'thx-custom-select',
  templateUrl: './custom-select.component.html',
  styleUrls: ['./custom-select.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomSelectComponent),
      multi: true,
    },
  ],
})
export class CustomSelectComponent implements OnInit, ControlValueAccessor, OnChanges {
  @ViewChild('input') inputElement: ElementRef;

  @Input() itemClass;
  @Input() itemSelectedClass;
  @Input() clearOnResetList = true;

  @Input() minHeight: string;
  @Input() arrowClass: string;
  @Input() maxHeight: string;
  @Input() notifyOnlyWhenClick = false;
  private _optionList: Array<any>;
  @Input()
  set optionList(list: Array<any>) {
    this._optionList = list;
    if (this.clearOnResetList) {
       this.selectedOption = null;
    }
    this.verifyOptionSelect();
  }
  get optionList(): Array<any> {
    return this._optionList;
  }
  @Input() keyOfTextOption = 'value';
  @Input() keyOfValueOption = 'key';

  private _option: any;
  @Input()
  set selectedOptionId(id: number) {
    this.selectedOption = this.optionList.filter(item => _.isEqual(item[this.keyOfValueOption], id))[0];

    this.verifyOptionSelect();
  }
  @Input()
  set selectedOption(option) {
    this.selectOption(option);
  }
  get selectedOption(): any {
    return this._option;
  }
  @Input() disabled: boolean;
  @Output() selecteOption: EventEmitter<any> = new EventEmitter();
  @Output() notFoundOption: EventEmitter<any> = new EventEmitter();
  public optionsOpened = false;

  constructor(public domSatizer: DomSanitizer) {}

  propagateChange = (i: any) => {};

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    this.maxHeight = this.maxHeight ? this.maxHeight : '175';
  }

  verifyOptionSelect() {
    if (this.optionList && this.selectedOption) {
      let optIsOnList = false;
      this.optionList.forEach(opt => {
        if (JSON.stringify(opt[this.keyOfValueOption]) == JSON.stringify(this.selectedOption[this.keyOfValueOption])) {
          optIsOnList = true;
        }
      });
      if (!optIsOnList) {
        this.notFoundOption.emit(this._option);
        this._option = null;
      }
    }
  }

  selectOption(option, event = null) {
    if (option) {
      this._option = option;
      this.optionsOpened = false;
      if (!this.notifyOnlyWhenClick || (this.notifyOnlyWhenClick && event)) {
        this.selecteOption.emit(option[this.keyOfValueOption]);
      }
    } else {
      this._option = null;
    }
  }

  toggleSelect() {
    if (!this.disabled) {
      this.optionsOpened = !this.optionsOpened;
    }
  }

  writeValue(obj: any): void {}

  registerOnChange(fn: any): void {}

  registerOnTouched(fn: any): void {}

  setDisabledState(isDisabled: boolean): void {}

  public getCurrentClassesItem(option): string {
    return this.selectedOption
      ? this.selectedOption[this.keyOfTextOption] == option[this.keyOfTextOption]
        ? `${this.itemSelectedClass} ${this.itemClass}`
        : this.itemClass
      : this.itemClass;
  }

  @HostListener('document:click')
  private onCloseOutside() {
    this.optionsOpened = false;
  }
}
