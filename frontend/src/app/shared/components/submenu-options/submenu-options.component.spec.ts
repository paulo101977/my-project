import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SubmenuOptionsComponent } from './submenu-options.component';
import { TranslateModule } from '@ngx-translate/core';
import { SubmenuOptionsModel } from './../../models/submenu-options';

describe('SubmenuOptionsComponent', () => {
  let component: SubmenuOptionsComponent;
  let fixture: ComponentFixture<SubmenuOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [SubmenuOptionsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmenuOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call itemWasUpdated', () => {
    const event = {
      type: 'click',
      stopPropagation: function() {},
    };

    const itemsOption = new SubmenuOptionsModel();
    itemsOption.title = 'mock string';
    itemsOption.callBackFunction = function() {};

    spyOn(component.itemWasAction, 'emit');
    spyOn(event, 'stopPropagation');

    component.onItemWasAction(event, itemsOption);

    expect(event.stopPropagation).toHaveBeenCalled();
    expect(component.itemWasAction.emit).toHaveBeenCalled();
  });
});
