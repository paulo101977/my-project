import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable ,  Observer } from 'rxjs';

@Component({
  selector: 'app-print-html-container',
  template: '<div class="print-only" [innerHTML]="domSatizer.bypassSecurityTrustHtml(html)" ></div>',
})
export class PrintHtmlContainerComponent implements OnInit {
  @Input() html: any;

  constructor(public domSatizer: DomSanitizer) {}

  ngOnInit() {}
}
