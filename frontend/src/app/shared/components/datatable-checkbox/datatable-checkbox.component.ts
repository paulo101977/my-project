import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'thx-datatable-checkbox',
  templateUrl: './datatable-checkbox.component.html',
  styleUrls: ['./datatable-checkbox.component.css'],
})
export class DatatableCheckboxComponent implements OnInit {
  @ViewChild('cellHeaderCheckboxAll') cellHeaderCheckboxAllTemplate: ElementRef;
  @ViewChild('cellCheckboxSingle') cellCheckboxSingleTemplate: ElementRef;
  @ViewChild('defaultTemplate') defaultTemplate: ElementRef;

  @Input() tableHasScroll: boolean;

  private _columns: Array<any>;
  @Input()
  set columns(columsArray: Array<any>) {
    this._columns = [];
    this._columns.push({
      cellTemplate: this.cellCheckboxSingleTemplate,
      headerTemplate: this.cellHeaderCheckboxAllTemplate,
      width: 100,
      resizeable: false,
      draggable: false,
      canAutoResize: false,
      prop: 'isActive',
    });
    this._columns = this._columns.concat(columsArray);
  }
  get columns(): Array<any> {
    return this._columns;
  }

  @Input() rowItens: Array<any>;
  private _selectedItens: Array<any>;
  @Input()
  set selectedItens(selectedItens: Array<any>) {
    if (selectedItens) {
      this._selectedItens = selectedItens;
      this.selectedItensChange.emit(selectedItens);
    } else {
      this._selectedItens = [];
    }
  }
  get selectedItens(): Array<any> {
    return this._selectedItens;
  }

  @Input() id = 'thx-datatable';
  @Input() rowsLimit: number;
  @Input() headerHeight = 45;
  @Input() footerHeight: number;
  @Input() tableExtraClass: string;
  @Input() rowHeight: string;
  @Input() emptyMessage: string;
  @Input() completeEmptyMessageContent: string;

  @Output() selectedItensChange = new EventEmitter();
  @Output() rowItemClicked = new EventEmitter();

  constructor() {}

  ngOnInit() {
    if (!this.selectedItens) {
      this.selectedItens = [];
    }
  }

  // Actions
  public onRowItemClicked(element) {
    this.rowItemClicked.emit(element);
  }

  public toggleAll(isActive) {
    if (isActive) {
      this.selectedItens = [...this.rowItens];
    } else {
      this.selectedItens = [];
    }
  }

  public toggleSingle(element) {
    const position = this.selectedItens.indexOf(element);
    if (position >= 0) {
      this.selectedItens.splice(position, 1);
      this.selectedItens = [...this.selectedItens];
    } else {
      this.selectedItens.push(element);
      this.selectedItens = [...this.selectedItens];
    }
  }
}
