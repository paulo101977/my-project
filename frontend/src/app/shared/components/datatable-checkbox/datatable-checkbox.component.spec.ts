import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DatatableCheckboxComponent } from './datatable-checkbox.component';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SubmenuOptionsComponent } from '../submenu-options/submenu-options.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('DatatableCheckboxComponent', () => {
  let component: DatatableCheckboxComponent;
  let fixture: ComponentFixture<DatatableCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DatatableCheckboxComponent, SubmenuOptionsComponent],
      imports: [NgxDatatableModule, FormsModule, TranslateModule.forRoot()],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatatableCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
