import {  NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { SubmenuPropertyComponent } from './submenu-property.component';
import { PropertyData } from '../../mock-data/property-data';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CoreModule } from '@app/core/core.module';
import { configureTestSuite } from 'ng-bullet';
import { tourismTaxServiceStub } from 'app/tax/testing/tax-tourism';

describe('SubmenuPropertyComponent', () => {
  let component: SubmenuPropertyComponent;
  let fixture: ComponentFixture<SubmenuPropertyComponent>;

  configureTestSuite(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      declarations: [SubmenuPropertyComponent],
      imports: [TranslateModule.forRoot(), RouterTestingModule, HttpClientTestingModule, CoreModule],
      providers: [
        tourismTaxServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(SubmenuPropertyComponent);
    component = fixture.componentInstance;
    component.property = PropertyData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn(component['propertyService'], 'getPropertyShared').and.returnValue(of(PropertyData));

    component.ngOnInit();

    expect(component['propertyService'].getPropertyShared).toHaveBeenCalled();
  });

  it('should goToRoomTypePage', () => {
    spyOn(component.router, 'navigate');
    component.navigate(['../room-type']);
    expect(component.router.navigate).toHaveBeenCalledWith(['../room-type'], { relativeTo: component._route });
  });

  it('should goToRoomPage', () => {
    spyOn(component.router, 'navigate');
    component.navigate(['../room']);
    expect(component.router.navigate).toHaveBeenCalledWith(['../room'], { relativeTo: component._route });
  });

  it('should goToPropertyEditPage', () => {
    spyOn(component.router, 'navigate');
    component.navigate(['../config']);
    expect(component.router.navigate).toHaveBeenCalledWith(['../config'], { relativeTo: component._route });
  });

  it('should goToOriginPage', () => {
    spyOn(component.router, 'navigate');
    component.navigate(['origin']);
    expect(component.router.navigate).toHaveBeenCalledWith(['origin'], { relativeTo: component._route });
  });

  it('should goToMarketSegmentPage', () => {
    spyOn(component.router, 'navigate');
    component.navigate(['market-segment']);
    expect(component.router.navigate).toHaveBeenCalledWith(['market-segment'], { relativeTo: component._route });
  });

  describe('on populateMenu', () => {
    it('should test when tourismTaxService.couldShow return true', () => {
      spyOn(component['tourismTaxService'], 'couldShow').and.returnValue( true );

      component['populateMenu']();

     expect(component.menuItensList.length).toEqual(12);
    });

    it('should test when tourismTaxService.couldShow return false', () => {
      spyOn(component['tourismTaxService'], 'couldShow').and.returnValue( false );

      component['populateMenu']();

      expect(component.menuItensList.length).toEqual(11);
    });
  });
});
