import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Property } from '../../models/property';
import { PropertyService } from '../../services/property/property.service';
import { AuthService } from 'app/shared/services/auth-service/auth.service';
import { TourismTaxService } from 'app/tax/services/tourism-tax.service';

@Component({
  selector: 'app-submenu-property',
  styleUrls: ['./submenu-property.component.css'],
  templateUrl: './submenu-property.component.html',
})
export class SubmenuPropertyComponent implements OnInit, OnDestroy {
  public property: Property;
  public subscription: Subscription;
  public menuItensList: Array<any>;
  public token: string;

  constructor(
    public _route: ActivatedRoute,
    public router: Router,
    private propertyService: PropertyService,
    private activateRoute: ActivatedRoute,
    private tourismTaxService: TourismTaxService,
  ) {}

  ngOnInit() {
    this.subscription = this.propertyService.getPropertyShared().subscribe(property => {
      this.property = property;
    });
    this.populateMenu();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private populateMenu() {
    this.menuItensList = [];
    this.menuItensList.push({
      title: 'hotelPanelModule.propertyEdit.createOrigin',
      routerLink: ['origin'],
      routeSufix: 'origin',
    });
    this.menuItensList.push({
      title: 'hotelPanelModule.propertyEdit.createMarketSegment',
      routerLink: ['market-segment'],
      routeSufix: 'market-segment',
    });
    this.menuItensList.push({
      title: 'hotelPanelModule.propertyEdit.roomSettings',
      routerLink: ['../room'],
      routeSufix: 'room',
    });
    this.menuItensList.push({
      title: 'hotelPanelModule.propertyEdit.contractData',
      routerLink: ['../config'],
      routeSufix: '/',
    });
    this.menuItensList.push({
      title: 'hotelPanelModule.propertyEdit.userManagement',
      routerLink: ['user'],
      routeSufix: 'user',
    });
    this.menuItensList.push({
      title: 'hotelPanelModule.propertyEdit.parameter',
      routerLink: ['parameter'],
      routeSufix: 'parameter',
    });
    this.menuItensList.push({
      title: 'hotelPanelModule.propertyEdit.policy',
      routerLink: ['policies'],
      routeSufix: 'policies',
    });
    this.menuItensList.push({
      title: 'title.governanceStatus',
      routerLink: ['governance-status'],
      routeSufix: 'governance-status',
    });
    this.menuItensList.push({
      title: 'hotelPanelModule.propertyEdit.roomTypeSettings',
      routerLink: ['../room-type'],
      routeSufix: '../room-type',
    });
    this.menuItensList.push({
      title: 'title.fiscalDocumentsParameter',
      routerLink: ['fiscal-documents/parameter'],
      routeSufix: 'fiscal-documents/parameter',
    });
    this.menuItensList.push({
      title: 'title.clientCategory',
      routerLink: ['category'],
      routeSufix: 'category',
    });

    // TODO: create logic to accept other countries DEBIT 7092
    if ( this.tourismTaxService.couldShow()) {
      this.menuItensList.push({
        title: 'title.tourismTax',
        routerLink: ['tax/tourism-tax'],
        routeSufix: 'tax/tourism-tax',
      });
    }
  }

  public navigate(route) {
    this.router.navigate(route, { relativeTo: this._route });
  }

  public isActive(url: string) {
    const propertyId = this.activateRoute.snapshot.params.property;
    url = 'p/' + propertyId + '/config/' + url;
    return this.router.isActive(url, true);
  }
}
