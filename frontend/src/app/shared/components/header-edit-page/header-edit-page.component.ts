import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { ConfigHeaderPage } from './../../models/config-header-page';

@Component({
  selector: 'app-header-edit-page',
  templateUrl: './header-edit-page.component.html',
})
export class HeaderEditPageComponent implements OnInit, OnChanges {
  @Input() config: ConfigHeaderPage;

  public configEvent: ConfigHeaderPage;
  public titlePage: string;
  public nameOfItemUpdated: string;
  public confirmDelete: Function;

  constructor(public translateService: TranslateService, public _location: Location) {}

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setVars();
  }

  private setVars() {
    this.configEvent = this.getConfig(this.config);
    if (this.configEvent != null) {
      this.titlePage = this.translateService.instant(this.configEvent.titlePage);
      this.nameOfItemUpdated = this.configEvent.nameOfItemUpdated;
      this.confirmDelete = this.configEvent.confirmDelete;
    }
  }

  public getConfig(config: ConfigHeaderPage) {
    return config;
  }

  public goBackPreviewPage() {
    this._location.back();
  }

  public runCallbackFunction() {
    this.confirmDelete();
  }
}
