import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

import { HeaderEditPageComponent } from './header-edit-page.component';
import { ConfigHeaderPage } from './../../models/config-header-page';
import { HeaderPageEnum } from './../../models/header-page-enum';

describe('HeaderEditPageComponent', () => {
  let component: HeaderEditPageComponent;
  let fixture: ComponentFixture<HeaderEditPageComponent>;
  let configHeaderPage: ConfigHeaderPage;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      declarations: [HeaderEditPageComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderEditPageComponent);
    component = fixture.componentInstance;
    configHeaderPage = new ConfigHeaderPage(HeaderPageEnum.Edit);
    configHeaderPage.titlePage = 'Title Page';
    configHeaderPage.nameOfItemUpdated = 'STD';
    configHeaderPage.confirmDelete = function() {};
    spyOn(component, 'getConfig').and.returnValue(configHeaderPage);
    fixture.detectChanges();
  });

  it('should create HeaderEditPage', () => {
    expect(component).toBeTruthy();
  });

  it('should get titlePage', () => {
    expect(component.titlePage).toEqual(configHeaderPage.titlePage);
  });

  it('should get nameOfItemUpdated', () => {
    expect(component.nameOfItemUpdated).toEqual(configHeaderPage.nameOfItemUpdated);
  });

  it('should run confirmDelete', () => {
    spyOn(component, 'confirmDelete');
    component.runCallbackFunction();
    expect(component.confirmDelete).toHaveBeenCalled();
  });

  it('should run _location.back', () => {
    spyOn(component._location, 'back');
    component.goBackPreviewPage();
    expect(component._location.back).toHaveBeenCalled();
  });
});
