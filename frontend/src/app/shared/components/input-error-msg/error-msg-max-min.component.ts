import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'error-msg-max-min',
  templateUrl: 'error-msg-max-min.component.html',
})
export class ErrorMsgMaxMinComponent implements OnInit, OnChanges {
  @Input() value: any;
  @Input() maxValue: any;
  @Input() minValue: any;

  constructor() {}

  ngOnChanges(changes) {
    this.checkIsInValid();
  }

  ngOnInit() {
    this.checkIsInValid();
  }

  public checkIsInValid() {
    if (this.value) {
      if (this.minValue != 0 && this.maxValue != 0) {
        return this.value < this.minValue || this.value > this.maxValue;
      }
    }
    return false;
  }
}
