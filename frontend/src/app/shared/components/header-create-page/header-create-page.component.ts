import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header-create-page',
  templateUrl: './header-create-page.component.html',
})
export class HeaderCreatePageComponent implements OnInit, OnChanges {
  @Input() titlePage: string;

  public titlePageComponent: string;

  constructor(public _location: Location) {}

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setVars();
  }

  private setVars() {
    this.titlePageComponent = this.getTitlePage(this.titlePage);
  }

  public goBackPreviewPage() {
    this._location.back();
  }

  public getTitlePage(titlePage: string): string {
    return titlePage;
  }
}
