import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Location } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { HeaderCreatePageComponent } from './header-create-page.component';

describe('HeaderCreatePageComponent', () => {
  let component: HeaderCreatePageComponent;
  let fixture: ComponentFixture<HeaderCreatePageComponent>;
  const titlePage = 'Title Page';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      declarations: [HeaderCreatePageComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderCreatePageComponent);
    component = fixture.componentInstance;
    spyOn(component, 'getTitlePage').and.returnValue(titlePage);
    fixture.detectChanges();
  });

  it('should create HeaderCreatePageComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should get title page', () => {
    expect(component.titlePageComponent).toEqual(titlePage);
  });

  it('should run _location.back', () => {
    spyOn(component._location, 'back');
    component.goBackPreviewPage();
    expect(component._location.back).toHaveBeenCalled();
  });
});
