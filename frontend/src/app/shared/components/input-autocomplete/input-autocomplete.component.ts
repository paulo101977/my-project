import {
  Component,
  Output,
  OnInit,
  OnChanges,
  Input,
  EventEmitter,
  SimpleChanges,
  ViewChild,
  ElementRef,
  AfterViewInit,
  HostListener,
  Renderer2,
} from '@angular/core';
import { AutocompleteTypeEnum } from './../../models/autocomplete/autocomplete.enum';
import { AutocompleteDisplayEnum } from './../../models/autocomplete/autocomplete.enum';
import { UpdateItemResponse } from './../../models/autocomplete/update-item-response';

@Component({
  selector: 'app-input-autocomplete',
  templateUrl: './input-autocomplete.component.html',
  styles: [':host { width: 100% }'],
})
export class AutoCompleteComponent implements OnInit, OnChanges, AfterViewInit {
  @Output() updateItem = new EventEmitter();
  @Output() searchItem = new EventEmitter();
  @Input() hasMarginTop = true;
  @Input() autocompleItems: Array<any>;
  @Input() autocompleteData: string;
  @Input() paramsFilter: Array<any>;
  @Input() paramsDisplay: string;
  @Input() inputId: any;
  @Input() autocompletePlaceholder: string;
  @Input() autocompleteDisplayEnum: AutocompleteDisplayEnum;
  @Input() autocompleteTypeEnum: AutocompleteTypeEnum;
  @Input() autocompleteWithSelect: boolean;
  @Input() autocompleteItemsSelect: Array<any>;
  @Output() updateAutocompleteItemsSelect = new EventEmitter();
  @Input() hasFocus: boolean;
  @Input() emptyMessage: string;

  @ViewChild('autofocus') input: ElementRef;

  public referenceToAutocompleteDisplayEnum: any = AutocompleteDisplayEnum;
  public referenceToAutocompleteTypeEnum: any = AutocompleteTypeEnum;
  public showItems: boolean;
  public autocompleteDataComponent: string;
  public selectedOption: string;

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
    if (this.autocompleteWithSelect) {
      this.selectedOption = this.autocompleteItemsSelect[0];
      this.chooseOptionSelect(this.selectedOption);
    }
    this.setVars();
  }

  ngAfterViewInit() {
    if (this.hasFocus) {
      this.input.nativeElement.focus();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setVars();
  }

  @HostListener('focusout')
  onFocusOut() {
    setTimeout(() => {
      this.showItems = false;
    }, 100);
  }

  private setVars(): void {
    this.autocompleteDataComponent = this.getAutocompleteData(this.autocompleteData);
  }

  private getAutocompleteData(autocompleteData: string): string {
    return autocompleteData;
  }

  showAutocompleItemsAndFireEvent(model: any) {
    this.showItems = false;
    if (model.length > 3) {
      this.showItems = true;
      this.searchItem.emit(model);
    }
    this.updateItem.emit(model);
  }

  chooseItem(item: any, choosenOption: any) {
    this.showItems = false;

    if (this.paramsDisplay) {
      this.autocompleteData = item[this.paramsDisplay];
    } else {
      this.autocompleteData = choosenOption;
    }

    const updateItemResponse = new UpdateItemResponse();
    updateItemResponse.completeItem = item;
    updateItemResponse.choosenOption = this.autocompleteData;
    this.updateItem.emit(updateItemResponse);
  }

  chooseOptionSelect(option: any) {
    this.selectedOption = option;
    this.updateAutocompleteItemsSelect.emit(option);
  }
}
