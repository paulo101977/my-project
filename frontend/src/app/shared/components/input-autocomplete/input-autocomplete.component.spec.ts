import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { AutoCompleteComponent } from './input-autocomplete.component';
import { UpdateItemResponse } from './../../models/autocomplete/update-item-response';
import { FilterListPipe } from './../../pipes/filter-select.pipe';
import { MarkTextInBlackPipe } from './../../pipes/mark-text-in-black.pipe';
import { CoreModule } from '@app/core/core.module';

describe('AutoCompleteComponent', () => {
  let component: AutoCompleteComponent;
  let fixture: ComponentFixture<AutoCompleteComponent>;
  let elemHelper;
  const autoCompleteDate = 'abc';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslateModule.forRoot(), FormsModule, CoreModule],
      declarations: [AutoCompleteComponent, FilterListPipe, MarkTextInBlackPipe],
      providers: [],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AutoCompleteComponent);
        component = fixture.componentInstance;
        elemHelper = fixture.debugElement;
        spyOn<any>(component, 'getAutocompleteData').and.returnValue(autoCompleteDate);
        fixture.detectChanges();
      });
  }));

  describe('Initialize AutoCompleteComponent', function() {
    it('should create AutoCompleteComponent', () => {
      expect(component).toBeTruthy();
    });

    it('should onUpdateItem not emit object when Items.length is minor than 3 and not showItems in showAutocompleItemsAndFireEvent', () => {
      spyOn(component.searchItem, 'emit');
      component.autocompleteData = 'a';

      component.showAutocompleItemsAndFireEvent(component.autocompleteData);
      fixture.detectChanges();

      expect(component.showItems).toBeFalsy();
      expect(component.searchItem.emit).not.toHaveBeenCalledWith(component.autocompleteData);
    });

    it('should onUpdateItem emit object when Items.length is greater than 3 and showItems in showAutocompleItemsAndFireEvent', () => {
      spyOn(component.searchItem, 'emit');
      component.autocompleteData = 'abcd';

      component.showAutocompleItemsAndFireEvent(component.autocompleteData);
      fixture.detectChanges();

      expect(component.showItems).toBeTruthy();
      expect(component.searchItem.emit).toHaveBeenCalledWith(component.autocompleteData);
      expect(elemHelper.query(By.css('.thf-autocomplete-area-items.thf-u-block'))).toBeTruthy();
    });

    it('should set data to input after choosen a item with paramsDisplay', () => {
      spyOn(component.updateItem, 'emit');
      const autocompleItemContactName = {
        cnpj: '89798798757',
        nomeFantasia: 'Asds Dasdsokok Fdfd',
        razaoSocial: 'ASLMN VNNVNVN AAP',
      };
      component.paramsDisplay = 'nomeFantasia';

      component.chooseItem(autocompleItemContactName, 'nomeFantasia');
      fixture.detectChanges();

      const updateItemResponse = new UpdateItemResponse();
      updateItemResponse.completeItem = autocompleItemContactName;
      updateItemResponse.choosenOption = component.autocompleteData;

      expect(component.autocompleteData).toEqual(autocompleItemContactName['nomeFantasia']);
      expect(component.showItems).toBeFalsy();
      expect(component.updateItem.emit).toHaveBeenCalledWith(updateItemResponse);
      expect(elemHelper.query(By.css('.thf-autocomplete-area-items.thf-u-block'))).toBeFalsy();
    });

    it('should set data to input after choosen a item withouth paramsDisplay', () => {
      spyOn(component.updateItem, 'emit');
      const autocompleItemContactName = {
        cnpj: '89798798757',
        nomeFantasia: 'Asds Dasdsokok Fdfd',
        razaoSocial: 'ASLMN VNNVNVN AAP',
      };
      component.paramsDisplay = null;

      component.chooseItem(autocompleItemContactName, 'Asds Dasdsokok Fdfd');
      fixture.detectChanges();

      const updateItemResponse = new UpdateItemResponse();
      updateItemResponse.completeItem = autocompleItemContactName;
      updateItemResponse.choosenOption = component.autocompleteData;

      expect(component.autocompleteData).toEqual(autocompleItemContactName['nomeFantasia']);
      expect(component.showItems).toBeFalsy();
      expect(component.updateItem.emit).toHaveBeenCalledWith(updateItemResponse);
      expect(elemHelper.query(By.css('.thf-autocomplete-area-items.thf-u-block'))).toBeFalsy();
    });

    it('should set data to input after choosen a item without paramsDisplay', () => {
      spyOn(component.updateAutocompleteItemsSelect, 'emit');
      const selectedOption = {
        option: 'option1',
      };

      component.chooseOptionSelect(selectedOption.option);

      expect<any>(component.selectedOption).toEqual(selectedOption.option);
      expect(component.updateAutocompleteItemsSelect.emit).toHaveBeenCalledWith(selectedOption.option);
    });
  });
});
