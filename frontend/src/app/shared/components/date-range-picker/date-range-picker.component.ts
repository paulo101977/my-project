import { Component, forwardRef, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IMyDrpOptions } from 'mydaterangepicker';
import { DateService } from '../../services/shared/date.service';
import { IMyDate } from 'mydatepicker';
import * as moment from 'moment';
import { PropertyDateService } from 'app/shared/services/shared/property-date.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'thx-date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateRangePickerComponent),
      multi: true,
    }
  ]
})
export class DateRangePickerComponent implements OnInit, ControlValueAccessor, OnChanges {

  @Input() id: string;
  @Input() placeholder: string;
  @Input() disableUntil: IMyDate;
  @Input() disableSince: IMyDate;
  @Input() options: IMyDrpOptions;
  @Input() minDateToday: boolean;
  @Input() maxDateToday: boolean;

  public propertyId: number;
  public value: any;

  propagateChange = (_: any) => {};

  constructor( private dateService: DateService,
               private propertyDateService: PropertyDateService,
               private route: ActivatedRoute) { }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    if (!this.options) {
      this.options = this.dateService.getClearConfigDateRangePicker();
      this.setUntilAndSince();
    }
    this.configDatePicker();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.options) {
      this.setUntilAndSince();
    }
  }

  private setUntilAndSince() {
    this.options = this.dateService.getClearConfigDateRangePicker(this.disableUntil, this.disableSince);
  }

  changeDate(date) {
    this.writeValue(date);
  }

  // Control Value Accessor methods
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    if (this.options) {
      this.options = {
        ...this.options,
        componentDisabled: isDisabled
      };
    }
  }

  writeValue(obj: any): void {
    let propagateValue = null;
    if ( obj ) {
      if (obj.hasOwnProperty('beginJsDate') && obj.hasOwnProperty('endJsDate') ) {
        propagateValue = {
          beginDate: this.dateService.convertDateToStringWithFormat(obj.beginJsDate, DateService.DATE_FORMAT_UNIVERSAL),
          endDate: this.dateService.convertDateToStringWithFormat(obj.endJsDate, DateService.DATE_FORMAT_UNIVERSAL)
        };
       } else {
        propagateValue = obj;
      }
    }
    if (propagateValue) {
      this.value = {
        beginDate: this.dateService.convertUniversalDateToIMyDate(propagateValue.beginDate).date,
        endDate: this.dateService.convertUniversalDateToIMyDate(propagateValue.endDate).date
      };
    } else {
      this.value = null;
    }
    this.propagateChange(propagateValue);
  }

  private configDatePicker() {
    const propertyDateToday = moment(this.dateService.getSystemDateWithoutFormat(this.propertyId));
    const propertyDateTodayIMyDate = <IMyDate>{
      day: +propertyDateToday.subtract(1, 'days').format('D'),
      month: +propertyDateToday.format('M'),
      year: +propertyDateToday.format('YYYY'),
    };

    if (this.minDateToday) {
      this.options.disableUntil = propertyDateTodayIMyDate;
    } else if ( this.maxDateToday ) { // disable the calendar after today
      this.options.disableSince = propertyDateTodayIMyDate;
    }
  }
}
