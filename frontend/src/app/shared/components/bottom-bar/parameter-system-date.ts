export class ParameterSystemDate {
  parameterTypeName: string;
  parameterDescription: string;
  featureGroupId: number;
  canInactive: boolean;
  propertyId: number;
  applicationParameterId: number;
  propertyParameterValue: string;
  propertyParameterMinValue: string;
  propertyParameterMaxValue: string;
  propertyParameterPossibleValues: string;
  isActive: boolean;
  id: string;
}
