import { DateService } from './../../services/shared/date.service';
import { SessionParameterService } from './../../services/parameter/session-parameter.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '@environment';

@Component({
  selector: 'app-bottom-bar',
  templateUrl: './bottom-bar.component.html',
  styleUrls: ['bottom-bar.component.css'],
})
export class BottomBarComponent implements OnInit {
  public propertyId: number;
  public systemDate: any;
  public appVersion: string;

  constructor(public activateRoute: ActivatedRoute,
              private dateService: DateService,
              public sessionParameterService: SessionParameterService) {
  }

  ngOnInit() {

    this.appVersion = environment.appVersion;
    this.propertyId = this.activateRoute.snapshot.params.property;
    this.systemDate = this.dateService.getSystemDateInViewFormat(this.propertyId);

    this
      .sessionParameterService
      .parameterModificationChange$
      .subscribe( item => {
        this.systemDate = this.dateService.getSystemDateInViewFormat(this.propertyId);
    });
  }
}
