import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-label',
  templateUrl: './input-label.component.html',
  styleUrls: ['./input-label.component.scss']
})
export class InputLabelComponent implements OnInit {
  public _disabled: boolean;

  @Input() parent: FormGroup;
  @Input() symbol: string;
  @Input() valueModel: string;
  @Input() set disabled( _disabled) {
    const { parent, valueModel } = this;
    this._disabled = _disabled;

    if ( parent && valueModel) {
      if ( parent.get(valueModel).disabled ) {
        parent.get(valueModel).enable();
      }

      if ( _disabled ) {
        parent.get(valueModel).disable();
      }
    }
  }

  public optionsCurrencyMask: any;

  constructor() { }

  ngOnInit() {
    this.optionsCurrencyMask = {
      prefix: '',
      thousands: '.',
      decimal: ',',
      align: 'center',
      allowNegative: false
    };
  }

}
