import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { InputLabelComponent } from './input-label.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CurrencyMaskModule } from 'ng2-currency-mask';

describe('InputLabelComponent', () => {
  let component: InputLabelComponent;
  let fixture: ComponentFixture<InputLabelComponent>;
  let input;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputLabelComponent ],
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        CurrencyMaskModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputLabelComponent);
    component = fixture.componentInstance;

    component.valueModel = 'mymodel';
    component.parent = formBuilder.group({
      mymodel: [ 0 ]
    });

    fixture.detectChanges();

    input = fixture.debugElement.nativeElement.querySelector('.input-label-wrapper input');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check value rendered', fakeAsync(() => {
    input = fixture.debugElement.nativeElement.querySelector('.input-label-wrapper input');

    component.parent.get('mymodel').setValue('12.95');

    tick();

    expect(input.value).toEqual('12.95'.replace('.', ','));
  }));

  it('should check disable component', fakeAsync(() => {

    component.disabled = true;

    tick();

    expect(input.disabled).toEqual(true);

    component.disabled = false;

    tick();

    expect(input.disabled).toEqual(false);
  }));
});
