import { Component, OnInit } from '@angular/core';
import { ModalEmitService } from './../../services/shared/modal-emit.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
})
export class ModalComponent implements OnInit {
  public title: string;
  public message: string;
  public callbackFunction: any;
  public closeCallbackFunction: any;
  public isVisible: boolean;
  private actions: any[];

  constructor(private modalEmitService: ModalEmitService) {}

  ngOnInit() {
    this.modalEmitService.changeEmitted$.subscribe(response => {
      const title = response[0];
      const message = response[1];
      const callbackFunction = response[2];
      const actions = response[3];
      const close = response[4];

      this.showModal(title, message, callbackFunction, actions, close);
    });
  }

  public showModal(title: string, message: string, callbackFunction: any, actions: Array<any> = [], closeCallBackFuncion: Function) {
    this.title = title;
    this.message = message;
    this.callbackFunction = callbackFunction;
    this.closeCallbackFunction = closeCallBackFuncion;
    this.isVisible = true;
  }

  public closeModal() {
    this.clearModal();
    if (this.closeCallbackFunction) {
      this.closeCallbackFunction();
    }
  }

  private clearModal() {
    this.title = '';
    this.message = '';
    this.isVisible = false;
  }

  public runCallbackFunction() {
    this.callbackFunction();
    this.clearModal();
  }
}
