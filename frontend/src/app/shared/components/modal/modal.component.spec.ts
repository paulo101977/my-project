
import {of as observableOf,  Observable } from 'rxjs';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslateModule } from '@ngx-translate/core';
import { ModalComponent } from './modal.component';
import { configureTestSuite } from 'ng-bullet';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;
  const title = 'My title';
  const message = 'My message';
  const callBackFunctionMock = () => 'data';
  const actions = Array<any>();
  const responseModalService = [title, message, callBackFunctionMock, actions];

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ModalComponent],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;

    component['modalEmitService'].changeEmitted$ = observableOf(responseModalService);

    fixture.detectChanges();
  });

  describe('on ModalComponent', () => {
    it('on init', () => {
      expect(component.title).toEqual(title);
      expect(component.message).toEqual(message);
      expect(component.callbackFunction).toEqual(callBackFunctionMock);
      expect(component.isVisible).toEqual(true);
    });

    it('should showModal', () => {
      component.showModal(title, message, callBackFunctionMock, actions, () => {} );

      expect(component.title).toEqual(title);
      expect(component.message).toEqual(message);
      expect(component.callbackFunction).toEqual(callBackFunctionMock);
      expect(component.isVisible).toEqual(true);
    });

    it('should closeModal', () => {
      spyOn<any>(component, 'clearModal');
      spyOn(component, 'closeCallbackFunction');

      component.closeModal();

      expect(component['clearModal']).toHaveBeenCalled();
      expect(component['closeCallbackFunction']).toHaveBeenCalled();
    });

    it('should clearModal', () => {
      component['clearModal']();

      expect(component.title).toEqual('');
      expect(component.message).toEqual('');
      expect(component.isVisible).toBeFalsy();
    });

    it('should runCallBack function', () => {
      spyOn(component, 'callbackFunction');
      spyOn<any>(component, 'clearModal');

      component.runCallbackFunction();

      expect(component.callbackFunction).toHaveBeenCalled();
      expect(component['clearModal']).toHaveBeenCalled();
    });
  });
});
