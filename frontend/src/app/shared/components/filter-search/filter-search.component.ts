import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { SearchableItems } from './../../models/filter-search/searchable-item';
import { TableRowTypeEnum } from './../../models/table-row-type-enum';
import { RoomTypeService } from '../../../room-type/shared/services/room-type/room-type.service';
import { RoomService } from './../../services/room/room.service';
import { ClientService } from './../../services/client/client.service';
import { GuestService } from './../../services/guest/guest.service';
import { BillingAccountService } from './../../services/billing-account/billing-account.service';
import { BillingEntryTypeService } from './../../services/billing-entry-type/billing-entry-type.service';
import { HousekeepingStatusPropertyService } from '../../services/governance/housekeeping-status-property.service';
import { RatePlanService } from '../../services/rate-plan/rate-plan.service';
import { ParameterDocumentService } from '../../services/document-parameter/parameter-document.service';

@Component({
  selector: 'app-filter-search',
  templateUrl: './filter-search.component.html',
})
export class FilterSearchComponent implements OnInit, OnChanges {
  @Output() searchUpdated = new EventEmitter();
  @Input() searchableItems: SearchableItems;
  @Input() hasBorderBottom = false;
  @Input() prepareToSearchFunction: Function;

  public searchableItemsComponent: SearchableItems;
  public placeholderSearchFilter: string;
  public placeholderActionWordFilter: string;
  public items: Array<any>;
  public searchData: string;

  constructor(
    public roomTypeService: RoomTypeService,
    public roomService: RoomService,
    private clientService: ClientService,
    private guestService: GuestService,
    private billingAccountService: BillingAccountService,
    private billingItemTypeService: BillingEntryTypeService,
    private ratePlanService: RatePlanService,
    private housekeepingStatusProperty: HousekeepingStatusPropertyService,
    private parameterDocumentService: ParameterDocumentService,
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setVars();
  }

  private setVars() {
    this.searchableItemsComponent = this.getSearchableItems(this.searchableItems);
    this.placeholderSearchFilter =
      this.getSearchableItems(this.searchableItems) != null ? this.searchableItemsComponent.placeholderSearchFilter : '';
    this.placeholderActionWordFilter =
      this.getSearchableItems(this.searchableItems) != null ? this.searchableItemsComponent.placeholderActionWordFilter : '';
  }

  public getSearchableItems(searchableItems: SearchableItems): SearchableItems {
    return searchableItems;
  }

  public updateItems(val: string): void {
    if (this.hasItem()) {
      this.searchData = this.prepareToSearchFunction ? this.prepareToSearchFunction(val) : val;
      this.items = this.searchableItemsComponent.items;
      if (this.searchData != '') {
        switch (this.searchableItemsComponent.tableRowTypeEnum) {
          case TableRowTypeEnum.RoomType:
            this.items = this.roomTypeService.getRoomTypesBySearchData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.Room:
            this.items = this.roomService.getRoomsBySearchData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.Client:
            this.items = this.clientService.getBySearchData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.GuestAccount:
            this.items = this.guestService.getBySearchGuestAccountData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.BillingAccount:
            this.items = this.billingAccountService.getBillingAccountBySearchData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.BillingTypeCategory:
            this.items = this.billingItemTypeService.getAllBillingItemCategoryBySearchData(
              this.searchData,
              this.searchableItemsComponent.items,
            );
            break;
          case TableRowTypeEnum.BillingTypeService:
            this.items = this.billingItemTypeService.getAllBillingItemServiceBySearchData(
              this.searchData,
              this.searchableItemsComponent.items,
            );
            break;
          case TableRowTypeEnum.BillingTypeTax:
            this.items = this.billingItemTypeService
              .getAllBillingItemTaxBySearchData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.CompanyClientAccount:
            this.items = this.billingAccountService.getAllClientAccountBySearchData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.GuestAccount:
            this.items = this.billingAccountService.getAllGuestAccountBySearchData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.HousekeepingStatusProperty:
            this.items = this.housekeepingStatusProperty.getAllFilterBySearchData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.RatePlan:
            this.items = this.ratePlanService.getAllFilterBySearchData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.RatePlanComission:
            this.items = this.ratePlanService.getAllComissionsFilterByData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.RatePlanUhType:
            this.items = this.ratePlanService.getAllUhTypesFilterByData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.RatePlanCustomers:
            this.items = this.ratePlanService.getAllCustomersFilterByData(this.searchData, this.searchableItemsComponent.items);
            break;
          case TableRowTypeEnum.ParameterDocumentItens:
            this.items = this.parameterDocumentService
              .getAllItensFilterByTypeOrItem(this.searchData, this.searchableItemsComponent.items);
            break;
        }
      }
      this.searchUpdated.emit(this.items);
    }
  }

  public hasItem() {
    if (
      this.searchableItemsComponent != undefined &&
      this.searchableItemsComponent.items != undefined &&
      this.itemsSizeIsGreaterThanZero()
    ) {
      return true;
    }
    return false;
  }

  private itemsSizeIsGreaterThanZero() {
    if (this.searchableItemsComponent.items.length > 0) {
      return true;
    }
    return false;
  }

}
