import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core/';
import { HttpClientModule } from '@angular/common/http';
import { FilterSearchComponent } from './filter-search.component';
import { SearchableItems } from './../../models/filter-search/searchable-item';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { Room } from './../../models/room';
import { TableRowTypeEnum } from './../../models/table-row-type-enum';
import { GetAllCompanyClientResultDto } from './../../models/dto/client/get-all-company-client-result-dto';
import { GuestAccountData } from './../../mock-data/guest-account-deta';
import { GetAllBillingAccountData } from './../../mock-data/get-all-billing-account-data';
import { BillingItemCategoryData } from './../../mock-data/billing-type-group-data';
import { GetAllBillingItemServiceData } from './../../mock-data/get-all-billing-item-service-data';
import { GetAllBillingItemTaxData } from './../../mock-data/get-all-billing-item-tax-data';
import { CoreModule } from '@app/core/core.module';

describe('FilterSearchComponent', () => {
  let component: FilterSearchComponent;
  let fixture: ComponentFixture<FilterSearchComponent>;
  const searchableItems = new SearchableItems();
  searchableItems.items = new Array<any>();

  function getRoomType() {
    const roomType = new RoomType();
    roomType.abbreviation = 'STD';
    roomType.name = 'Standard';
    roomType.order = 1;
    return roomType;
  }

  function getRoom() {
    const room = new Room();
    room.roomType = new RoomType();
    room.roomType.abbreviation = 'STD';
    room.roomType.name = 'Standard';
    room.roomNumber = '165';
    return room;
  }

  function getCompanyClientResultDto() {
    const companyClientResultDto = new GetAllCompanyClientResultDto();
    companyClientResultDto.name = 'Cliente Teste';
    companyClientResultDto.document = '123456789';
    return companyClientResultDto;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslateModule.forRoot(), HttpClientModule, FormsModule, ReactiveFormsModule, CoreModule],
      declarations: [FilterSearchComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(FilterSearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
  }));

  describe('Initialize FilterSearchComponent', function() {
    it('should create FilterSearchComponent', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Get room types', () => {
    beforeEach(() => {
      searchableItems.items = [];
      searchableItems.items.push(getRoomType());
      searchableItems.tableRowTypeEnum = TableRowTypeEnum.RoomType;

      component.searchableItemsComponent = searchableItems;
    });

    afterEach(() => {
      searchableItems.items = [];
    });

    it('should get room types by abbreviation', () => {
      component.updateItems(getRoomType().abbreviation);
      expect(component.items[0].abbreviation).toEqual(component.searchableItemsComponent.items[0].abbreviation);
    });

    it('should get room types by name', () => {
      component.updateItems(getRoomType().name);
      expect(component.items[0].name).toEqual(component.searchableItemsComponent.items[0].name);
    });

    it('should get room types by order', () => {
      component.updateItems(getRoomType().order.toString());
      expect(component.items[0].order).toEqual(component.searchableItemsComponent.items[0].order);
    });

    it('should not get room types when the search data does not match in abbreviation, name, order', () => {
      component.updateItems('Does not exist');
      expect(component.items.length).toEqual(0);
    });
  });

  describe('Get rooms', () => {
    beforeEach(() => {
      searchableItems.items = [];
      searchableItems.items.push(getRoom());
      searchableItems.tableRowTypeEnum = TableRowTypeEnum.Room;

      component.searchableItemsComponent = searchableItems;
    });

    it('should get rooms by roomType abbreviation', () => {
      component.updateItems(getRoom().roomType.abbreviation);
      expect(component.items[0].roomType.abbreviation).toEqual(component.searchableItemsComponent.items[0].roomType.abbreviation);
    });

    it('should get room by roomType name', () => {
      component.updateItems(getRoom().roomType.name);
      expect(component.items[0].roomType.name).toEqual(component.searchableItemsComponent.items[0].roomType.name);
    });

    it('should get room by roomNumber', () => {
      component.updateItems(getRoom().roomNumber.toString());
      expect(component.items[0].roomNumber).toEqual(component.searchableItemsComponent.items[0].roomNumber);
    });

    it('should not get room when the search data does not match in roomType abbreviation, roomType name, roomNumber', () => {
      component.updateItems('Does not exist');
      expect(component.items.length).toEqual(0);
    });
  });

  describe('Get clients', () => {
    beforeEach(() => {
      searchableItems.items = [];
      searchableItems.items.push(getCompanyClientResultDto());
      searchableItems.tableRowTypeEnum = TableRowTypeEnum.Client;

      component.searchableItemsComponent = searchableItems;
    });

    it('should get clients by name', () => {
      component.updateItems(getCompanyClientResultDto().name);
      expect(component.items[0].name).toEqual(component.searchableItemsComponent.items[0].name);
    });

    it('should get clients by document', () => {
      component.updateItems(getCompanyClientResultDto().document);
      expect(component.items[0].document).toEqual(component.searchableItemsComponent.items[0].document);
    });

    it('should not get room when the search data does not match in client name, document', () => {
      component.updateItems('Does not exist');
      expect(component.items.length).toEqual(0);
    });
  });

  describe('Get GuestAccount', () => {
    beforeEach(() => {
      searchableItems.items = [];
      searchableItems.items.push(GuestAccountData);
      searchableItems.tableRowTypeEnum = TableRowTypeEnum.GuestAccount;

      component.searchableItemsComponent = searchableItems;
    });

    it('should get Guests by guestName', () => {
      component.updateItems(GuestAccountData.guestName);
      expect(component.items[0].name).toEqual(component.searchableItemsComponent.items[0].name);
    });

    it('should get Guests by reservationCode', () => {
      component.updateItems(GuestAccountData.reservationCode);
      expect(component.items[0].document).toEqual(component.searchableItemsComponent.items[0].document);
    });

    it('should not get room when the search data does not match in guest name, reservationCode', () => {
      component.updateItems('Does not exist');
      expect(component.items.length).toEqual(0);
    });
  });

  describe('Get BillingAccount', () => {
    beforeEach(() => {
      searchableItems.items = [];
      searchableItems.items.push(GetAllBillingAccountData);
      searchableItems.tableRowTypeEnum = TableRowTypeEnum.BillingAccount;

      component.searchableItemsComponent = searchableItems;
    });

    it('should get BillingAccounts by guestName', () => {
      component.updateItems(GetAllBillingAccountData.billingAccountName);
      expect(component.items[0].name).toEqual(component.searchableItemsComponent.items[0].name);
    });

    it('should get BillingAccounts by reservationCode', () => {
      component.updateItems(GetAllBillingAccountData.reservationCode);
      expect(component.items[0].document).toEqual(component.searchableItemsComponent.items[0].document);
    });

    it('should get BillingAccounts by roomNumber', () => {
      component.updateItems(GetAllBillingAccountData.roomNumber);
      expect(component.items[0].roomNumber).toEqual(component.searchableItemsComponent.items[0].roomNumber);
    });

    it('should not get room when the search data does not match in guest name, reservationCode', () => {
      component.updateItems('Does not exist');
      expect(component.items.length).toEqual(0);
    });
  });

  describe('Get BillingTypeCategory', () => {
    beforeEach(() => {
      searchableItems.items = [];
      searchableItems.items.push(BillingItemCategoryData);
      searchableItems.tableRowTypeEnum = TableRowTypeEnum.BillingTypeCategory;

      component.searchableItemsComponent = searchableItems;
    });

    it('should get BillingTypeCategory by categoryName', () => {
      component.updateItems(BillingItemCategoryData.categoryName);
      expect(component.items[0].categoryName).toEqual(component.searchableItemsComponent.items[0].categoryName);
    });

    it('should get BillingTypeCategory by categoryFixedName', () => {
      component.updateItems(BillingItemCategoryData.categoryFixedName);
      expect(component.items[0].categoryFixedName).toEqual(component.searchableItemsComponent.items[0].categoryFixedName);
    });
  });

  describe('Get BillingTypeService', () => {
    beforeEach(() => {
      searchableItems.items = [];
      searchableItems.items.push(GetAllBillingItemServiceData);
      searchableItems.tableRowTypeEnum = TableRowTypeEnum.BillingTypeService;

      component.searchableItemsComponent = searchableItems;
    });

    it('should get GetAllBillingItemServiceData by billingItemName', () => {
      component.updateItems(GetAllBillingItemServiceData.billingItemName);
      expect(component.items[0].billingItemName).toEqual(component.searchableItemsComponent.items[0].billingItemName);
    });

    it('should get GetAllBillingItemServiceData by billingItemCategoryName', () => {
      component.updateItems(GetAllBillingItemServiceData.billingItemCategoryName);
      expect(component.items[0].billingItemCategoryName).toEqual(component.searchableItemsComponent.items[0].billingItemCategoryName);
    });
  });

  describe('Get BillingTypeTax', () => {
    beforeEach(() => {
      searchableItems.items = [];
      searchableItems.items.push(GetAllBillingItemTaxData);
      searchableItems.tableRowTypeEnum = TableRowTypeEnum.BillingTypeTax;

      component.searchableItemsComponent = searchableItems;
    });

    it('should get GetAllBillingItemTaxData by billingItemName', () => {
      component.updateItems(GetAllBillingItemTaxData.billingItemName);
      expect(component.items[0].billingItemName).toEqual(component.searchableItemsComponent.items[0].billingItemName);
    });

    it('should get GetAllBillingItemTaxData by billingItemCategoryName', () => {
      component.updateItems(GetAllBillingItemTaxData.billingItemCategoryName);
      expect(component.items[0].billingItemCategoryName).toEqual(component.searchableItemsComponent.items[0].billingItemCategoryName);
    });
  });
});
