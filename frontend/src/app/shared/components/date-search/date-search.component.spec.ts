import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DateSearchComponent } from './date-search.component';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { imgCdnStub } from '../../../../../testing';

describe('DateSearchComponent', () => {
  let component: DateSearchComponent;
  let fixture: ComponentFixture<DateSearchComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        DateSearchComponent,
        imgCdnStub,
      ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule
      ],
      providers: [
        sharedServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(DateSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call search method end emit result when searchClick fired', () => {
     spyOn<any>(component, 'search');
     spyOn(component.searchResultItems, 'emit');

     component['searchClick']();

     expect(component['search']).toHaveBeenCalled();
     expect(component.searchResultItems.emit).toHaveBeenCalledWith(component.searchItemsList);
  });

  it('should return originalList when searchData is null', () => {
    component.searchItemsList = [{id: 1}, {id: 2}];
    component.originalItemsList = [{id: 1}, {id: 2}, {id: 3}];
    component.searchDate = undefined;

    component['searchClick']();

    expect(component.searchItemsList).toEqual(component.originalItemsList);
  });

  it('should return error when filterFunction not informed', () => {
    spyOn(console, 'error');
    component.searchItemsList = [{id: 1}, {id: 2}];
    component.searchDate = '1';

    component['searchClick']();

    expect(console.error).toHaveBeenCalled();
  });

  it('should return filtered array when filterFunction informed', () => {
    const listStub = [{id: 1}, {id: 2}];
    const filterFunctionStub = (search, list) => {
      return [list[0]];
    };
    component.filterFunction = filterFunctionStub;
    component.searchItems = listStub;
    component.searchDate = '1';

    component['searchClick']();

    expect(component.searchItemsList.length).toEqual(1);
    expect(component.searchItemsList).toContain(listStub[0]);
  });

});
