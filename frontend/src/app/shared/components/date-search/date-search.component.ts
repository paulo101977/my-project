import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ButtonConfig, ButtonType } from '../../models/button-config';
import { SharedService } from '../../services/shared/shared.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-date-search',
  templateUrl: './date-search.component.html'
})
export class DateSearchComponent implements OnInit, OnChanges {

  @Input() searchItems: Array<any>;
  @Input() filterFunction: Function;

  @Output() searchResultItems = new EventEmitter<Array<any>>();

  public searchButtonConfig: ButtonConfig;

  public searchDate: string;
  public searchItemsList: Array<any>;
  public originalItemsList: Array<any>;

  constructor(
      private sharedService: SharedService
  ) {}

  ngOnInit() {
    this.setButtonConfig();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('searchItems') &&
      !this.originalItemsList &&
      changes.searchItems.currentValue) {
      this.originalItemsList = _.cloneDeep(changes.searchItems.currentValue);
    }
  }

  private setButtonConfig() {
    this.searchButtonConfig = this.sharedService.getButtonConfig('button-search', this.searchClick, '', ButtonType.Secondary);
  }

  private searchClick = () => {
    this.search();
    this.searchResultItems.emit(this.searchItemsList);
  }

  private search() {
    if (this.searchDate) {
      if (this.filterFunction) {
        this.searchItemsList = this.filterFunction(this.searchDate, this.searchItems);
      } else {
        this.searchItemsList = this.searchItems;
        console.error('date-search has no filterFunction');
      }
    } else {
      this.searchItemsList = this.originalItemsList;
    }
  }

  public changeDate(date) {
    this.searchDate = date;
  }


}
