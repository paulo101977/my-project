import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {APP_NAME, AuthService, LocalUserManagerService, PropertyStorageService} from '@inovacaocmnet/thx-bifrost';
import {TranslateService} from '@ngx-translate/core';
import {SessionParameterService} from 'app/shared/services/parameter/session-parameter.service';
import {DateService} from 'app/shared/services/shared/date.service';
import * as moment from 'moment';
import {startWith} from 'rxjs/operators';
import {SuccessError} from '../../models/success-error-enum';
import {User} from '../../models/user';
import {PropertyResource} from '../../resources/property/property.resource';
import {UserResource} from '../../resources/user/user.resource';
import {InternationalizationService} from '../../services/shared/internationalization.service';
import {ToasterEmitService} from '../../services/shared/toaster-emit.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
})
export class TopBarComponent implements OnInit, OnChanges {
  public propertyId: number;
  public propertyName: string;
  public user: any;
  public languageList: any[];
  public languageFlag: string;

  public properties = [];
  public systemDate: string;

  constructor(
    @Inject(APP_NAME) public appName,
    public router: Router,
    public authService: AuthService,
    public activateRoute: ActivatedRoute,
    private propertyResource: PropertyResource,
    private userResource: UserResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private internationalizationService: InternationalizationService,
    private userManager: LocalUserManagerService,
    private propertyManager: PropertyStorageService,
    private dateService: DateService,
    private sessionParameterService: SessionParameterService
  ) {
  }

  ngOnChanges() {
    this.propertyId = this.activateRoute.snapshot.params.property;
    this.setUser();
    this.setPropertyName();
  }

  ngOnInit() {
    this.propertyId = this.activateRoute.snapshot.params.property;
    this.setUser();
    this.setPropertyName();
    this.setLanguageFlag();
    this.getPropertyLanguages();
    this.getSystemDate();
  }

  setPropertyName() {
    const property = this.propertyManager.getProperty(this.appName, this.propertyId);
    if (property) {
      this.propertyName = property.name;
    }
  }

  logout() {
    this.authService.logout(this.appName);
    this.router.navigateByUrl('/auth/login');
  }

  public updateUserLanguage(newLanguage: string) {
    if (this.user) {
      this.changeLanguageconfig(this.user, newLanguage);
    }
  }

  public updateUserLocalStorage(user: User, newLanguage: string) {
    // TODO: There are two User classes with different signatures. Fix it.
    const userInstance = <any>user;
    userInstance.preferredLanguage = newLanguage;
    userInstance.preferredCulture = newLanguage;
    this.userManager.setUser(this.appName, userInstance);
  }

  private getPropertyLanguages() {
    this.propertyResource.getPropertyLanguages().subscribe(data => {
      if (data.items) {
        this.languageList = data.items.map(item => {
          item.value = this.internationalizationService.getCountry(item.value);
          return item;
        });
      }
    });
  }

  private setUser() {
    this.user = this.userManager.getUser(this.appName);
  }

  private setLanguageFlag() {
    if (this.user.preferredLanguage) {
      this.languageFlag = this.internationalizationService.getCountry(this.user.preferredLanguage);
    }
  }

  private async changeLanguageconfig(user: User, newLanguage: string) {
    this.userResource.updateLanguage(user.id, newLanguage)
      .subscribe(() => {
          this.updateUserLocalStorage(user, newLanguage);
          this.internationalizationService.setLanguageNavigator(newLanguage);
          this.languageFlag = this.internationalizationService.getCountry(newLanguage);
          this.toasterEmitService.emitChange(
            SuccessError.success,
            this.translateService.instant('variable.lbEditSuccessM', {
              labelName: this.translateService.instant('label.language')
            })
          );
        },
        () => {
          this.toasterEmitService.emitChange(
            SuccessError.success,
            this.translateService.instant('variable.lbEditFailM', {
              labelName: this.translateService.instant('label.language')
            })
          );
        });
  }

  private getSystemDate() {
    this
      .sessionParameterService
      .parameterModificationChange$
      .pipe(startWith(null))
      .subscribe(item => {
        const dateSystem = this.dateService
          .getSystemDateWithoutFormat(this.propertyId);
        this.systemDate = null;
        if (dateSystem) {
          this.systemDate = moment(dateSystem).format('dddd, DD MMMM');
        }
      });
  }
}
