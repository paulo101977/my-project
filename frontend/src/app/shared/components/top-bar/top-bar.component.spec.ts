import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { User } from '@bifrost/storage/models/user';
import { ImgCdnTestingModule } from '@inovacao-cmnet/thx-ui';
import {
  AuthService,
  configureAppName,
  LocalUserManagerService,
  PropertyStorageService
} from '@inovacaocmnet/thx-bifrost';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { UserResource } from 'app/shared/resources/user/user.resource';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { InternationalizationService } from 'app/shared/services/shared/internationalization.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { configureTestSuite } from 'ng-bullet';
import { Observable, of, of as observableOf } from 'rxjs';
import { createServiceStub } from '../../../../../testing';
import { getUserData } from '../../mock-data/user.data';
import { SuccessError } from '../../models/success-error-enum';

import { TopBarComponent } from './top-bar.component';

describe('TopBarComponent', () => {
  let component: TopBarComponent;
  let fixture: ComponentFixture<TopBarComponent>;
  const resultLanguages = {
    next: false,
    total: 2,
    items: [
      {key: 'en-us', value: 'en-us'},
      {key: 'pt-br', value: 'pt-br'}
    ]
  };

  const authProvider = createServiceStub(AuthService, {
    logout(appName: string): void {}
  });

  const propertyResourceProvider = createServiceStub(PropertyResource, {
    getPropertyLanguages(): Observable<any> { return of(null); }
  });

  const userResourceProvider = createServiceStub(UserResource, {
    updateLanguage(userId: string, culture: string): Observable<any> { return of(null); }
  });

  const toasterEmitProvider = createServiceStub(ToasterEmitService, {
    emitChange(isSuccess: SuccessError, message: string) {}
  });

  const internationalizationProvider = createServiceStub(InternationalizationService, {
    getCountry(language: string): string { return 'pt'; },
    setLanguageNavigator(newLanguage: string) {}
  });

  const localUserManagerProvider = createServiceStub(LocalUserManagerService, {
    getUser(appName: string): User {
      return <User>{
        preferredLanguage: 'pt-br',
        preferredCulture: 'BR',
        id: '1'
      };
    },
    setUser(appName: string, user: User): void {}
  });

  const propertyStorageProvider = createServiceStub(PropertyStorageService, {
    getProperty(appName: string, propertyId: number): any { return null; }
  });

  const dateProvider = createServiceStub(DateService, {
    getSystemDateWithoutFormat(propertyId: number): string { return null; }
  });

  const sessionParameterProvider = createServiceStub(SessionParameterService, {
    parameterModificationChange$: of(null)
  });

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        declarations: [TopBarComponent],
        imports: [
          TranslateTestingModule,
          RouterTestingModule,
          ImgCdnTestingModule,
        ],
        providers: [
          configureAppName('APP'),
          authProvider,
          propertyResourceProvider,
          userResourceProvider,
          toasterEmitProvider,
          internationalizationProvider,
          localUserManagerProvider,
          propertyStorageProvider,
          dateProvider,
          sessionParameterProvider,
        ],
        schemas: [NO_ERRORS_SCHEMA]
      });

    const store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      }
    };

    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);

    localStorage.setItem('user', JSON.stringify(getUserData()));

    fixture = TestBed.createComponent(TopBarComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create TopBarComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should get Languages from Property', () => {
    spyOn(component['propertyResource'], 'getPropertyLanguages')
      .and.returnValue(observableOf(resultLanguages));

    component['getPropertyLanguages']();

    expect(component.languageList).toEqual(resultLanguages.items);
  });

  it('should setLanguageFlag', () => {
    component.user = {
      preferredLanguage: 'EN-us'
    };
    spyOn(component['internationalizationService'], 'getCountry')
      .and.returnValue('us');

    component['setLanguageFlag']();

    expect(component.languageFlag).toEqual('us');
  });

  it('should updateUserLanguage', () => {
    const newLanguage = 'pt-br';
    spyOn<any>(component, 'changeLanguageconfig');

    component.updateUserLanguage(newLanguage);

    expect(component['changeLanguageconfig']).toHaveBeenCalledWith(component.user, newLanguage);
  });

  it('should updateUserLocalStorage', () => {
    const newLanguage = 'pt-br';
    const userData = getUserData();
    spyOn(component['userManager'], 'setUser').and.callFake(() => {});

    component.updateUserLocalStorage(userData, newLanguage);

    expect(component['userManager'].setUser).toHaveBeenCalledWith(component['appName'], userData);
  });

  it('should changeLanguageconfig', () => {
    const newLanguage = 'pt-br';
    const userData = getUserData();
    spyOn(component, 'updateUserLocalStorage');
    spyOn(component['internationalizationService'], 'setLanguageNavigator');
    spyOn(component['userResource'], 'updateLanguage').and.returnValue(observableOf(true));
    spyOn(component['toasterEmitService'], 'emitChange');
    spyOn(component['internationalizationService'], 'getCountry')
      .and.returnValue('br');

    component['changeLanguageconfig'](userData, newLanguage);

    expect(component.updateUserLocalStorage).toHaveBeenCalledWith(userData, newLanguage);
    expect(component['internationalizationService'].setLanguageNavigator).toHaveBeenCalledWith(newLanguage);
    expect(component.languageFlag).toEqual('br');
    expect(component['userResource'].updateLanguage).toHaveBeenCalledWith(userData.id, newLanguage);
    expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
      SuccessError.success,
      component['translateService'].instant('variable.lbEditSuccessM', {
        labelName: component['translateService'].instant('label.language')
      })
    );
  });
});
