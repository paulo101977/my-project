import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

import { ConfigHeaderPage } from './../../models/config-header-page';
import { HeaderPageEnum } from './../../models/header-page-enum';

@Component({
  selector: 'app-header-page',
  templateUrl: './header-page.component.html',
})
export class HeaderPageComponent implements OnInit, OnChanges {
  @Input() config: ConfigHeaderPage;
  @Input() searchableItems: any;
  @Output() searchUpdated = new EventEmitter();

  public configComponent: ConfigHeaderPage;
  public searchableItemsComponent: Array<any>;
  public referenceToHeaderPageEnum: any = HeaderPageEnum;

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setVars();
  }

  setVars() {
    this.configComponent = this.getConfig(this.config);
    this.searchableItemsComponent = this.getAllItems(this.searchableItems);
  }

  public getConfig(config: ConfigHeaderPage) {
    return config;
  }

  public getAllItems(allItems: Array<any>) {
    return allItems;
  }

  public updateItems(items: Array<any>): void {
    this.searchUpdated.emit(items);
  }
}
