import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { HeaderPageComponent } from './header-page.component';
import { HeaderListPageComponent } from './../header-list-page/header-list-page.component';
import { HeaderCreatePageComponent } from './../header-create-page/header-create-page.component';
import { HeaderEditPageComponent } from './../header-edit-page/header-edit-page.component';
import { FilterSearchComponent } from '../filter-search/filter-search.component';

import { ConfigHeaderPage } from './../../models/config-header-page';
import { HeaderPageEnum } from './../../models/header-page-enum';
import { CoreModule } from '@app/core/core.module';

describe('HeaderPageComponent', () => {
  let component: HeaderPageComponent;
  let fixture: ComponentFixture<HeaderPageComponent>;
  let configHeaderPage: ConfigHeaderPage;
  let allItems: Array<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule, TranslateModule.forRoot(), CoreModule],
      declarations: [
        HeaderPageComponent,
        FilterSearchComponent,
        HeaderListPageComponent,
        HeaderCreatePageComponent,
        HeaderEditPageComponent,
      ],
      providers: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderPageComponent);
    component = fixture.componentInstance;
    configHeaderPage = new ConfigHeaderPage(HeaderPageEnum.Create);
    allItems = new Array<any>();

    spyOn(component, 'getConfig').and.returnValue(configHeaderPage);
    spyOn(component, 'getAllItems').and.returnValue(allItems);
    fixture.detectChanges();
  });

  it('should create HeaderPage', () => {
    expect(component).toBeTruthy();
  });

  it('should get allItems', () => {
    expect(component.searchableItemsComponent).toEqual(allItems);
  });

  it('should get configComponent', () => {
    expect(component.configComponent).toEqual(configHeaderPage);
  });
});
