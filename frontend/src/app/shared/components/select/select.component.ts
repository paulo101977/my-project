import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { SelectObjectOption } from '../../models/selectModel';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css'],
})
export class SelectComponent implements OnChanges, AfterViewInit {
  private _selectedOptionId: number;
  @Input() hasMagin = true;
  @Input() hasIcon = true;
  @Input() inTable: boolean;
  @Input() hasBlankOption = true;
  @Input() blankOptionText = 'label.select';
  @Input() width: 75;
  @Input() disabled: boolean;
  @Input() readonly: boolean;
  @Input() choseOnlyOnClick = false;
  @Input() placeholder: string;
  @Input() dontChooseWhenset = true;
  @Input() optionLimited = false;

  _optionList: any;

  get optionList() {
    return this._optionList;
  }

  @Input()
  set optionList(val) {
    if (this.hasFormGroup) {
      const isObject = this.listIsObject(val);
      const isNumber = this.listIsNumber(val);

      if (isObject) {
        this._optionList = val;
      } else if (isNumber) {
        this._optionList = val.map(option => new SelectObjectOption(option, option.toString()));
      } else {
        this._optionList = val.map(option => new SelectObjectOption(option, option));
      }
    } else {
      this._optionList = val;
    }
  }

  @Input() optionChoosedByUser: string;

  @Input()
  set selectedOptionId(number) {
    this._selectedOptionId = number;
    if (this.dontChooseWhenset) {
      this.chooseOption(this._selectedOptionId);
    }
    this.selectedOptionIdChange.emit(number);
  }

  get selectedOptionId(): number {
    return this._selectedOptionId;
  }

  @Input() selectId: string;

  @Input() hasFormGroup: boolean;

  @Input() parent: FormGroup;

  @Input() isUppercase: boolean;

  @Input() propertyNameToDisplay: string;

  @Output() selectedOption = new EventEmitter<string>();
  @Output() selectedOptionIdChange = new EventEmitter();

  @ViewChild('select') select: ElementRef<any>;
  public selectWidth: number;

  public isObject: boolean;
  public isString: boolean;
  public isNumber: boolean;
  public _selectedOption: string;
  private readonly FONT_PIXEL = 16;

  constructor() {}

  ngOnChanges() {
    this.isObject = this.listIsObject(this.optionList);
    this.isString = this.listIsString(this.optionList);
    this.isNumber = this.listIsNumber(this.optionList);
    this.setVars();
    if (!this.choseOnlyOnClick) {
      if (this._selectedOption) {
        this.selectedOption.emit(this._selectedOption);
      }
    }
  }

  ngAfterViewInit() {
    if (this.optionLimited && this.select) {
      const width = (this.select.nativeElement.getBoundingClientRect().width);
      this.selectWidth = width > 100 ? width / 100 * this.FONT_PIXEL : width;
    }
  }

  setVars() {
    if (this.optionList && this.optionList.length > 0) {
      this._selectedOption = this.optionList[0];
    }
  }

  public onChange(option) {
    if (this.optionList) {
      this.optionList.filter((optionItem, index) => {
        if (option === optionItem) {
          this._selectedOption = this.optionList[index];
        }
      });
    }
  }

  public chooseOption(optionId: any) {
    this.selectedOption.emit(optionId);
  }

  public listIsObject(list: any) {
    if (typeof list === 'string' || (_.isArray(list) && _.isString(list[0])) || this.listIsNumber(list)) {
      return false;
    }
    return true;
  }

  public listIsString(list: any) {
    if (_.isString(list) || (_.isArray(list) && _.isString(list[0]))) {
      return true;
    }

    return false;
  }

  public listIsNumber(list: any) {
    if (_.isNumber(list) || (_.isArray(list) && _.isNumber(list[0]))) {
      return true;
    }

    return false;
  }
}
