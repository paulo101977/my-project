import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { SelectComponent } from './select.component';

xdescribe('SelectComponent', () => {
  let component: SelectComponent;
  let fixture: ComponentFixture<SelectComponent>;
  const formBuilder = new FormBuilder();

  beforeAll(async() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, TranslateModule.forRoot()],
      declarations: [SelectComponent],
      providers: [],
    }).compileComponents();
  });

  beforeAll( () => {
    fixture = TestBed.createComponent(SelectComponent);
    component = fixture.componentInstance;

    spyOn(component.selectedOption, 'emit').and.callThrough();
    component.hasFormGroup = false;
    component.isUppercase = true;
    component.selectId = 'federativeUnit';
    component.parent = undefined;

    fixture.detectChanges();
  });

  describe('on init', () => {
    beforeEach(() => {
      component.optionList = ['RJ', 'SC', 'SP'];
    });

    it('should know if list is object or string', () => {
      const list = ['A', 'B', 'C'];
      const string = 'ABC';

      expect(component.listIsObject(list)).toBeFalsy();
      expect(component.listIsObject(string)).toBeFalsy();
    });

    xit('should set selectedOptions and dispatch event when hasFormGroup is false', () => {
      component['ngOnChanges']();

      expect(component.selectedOption).toEqual(component.optionList[0]);
      expect(component.selectedOption.emit).toHaveBeenCalledWith(component.optionList[0]);
      expect(component.isObject).toBeFalsy();
    });

    afterEach(() => {
      component.selectedOption = undefined;
    });
  });

  describe('on init with list objects', () => {
    beforeEach(() => {
      component.optionList = [{ key: 1, value: '' }];
    });
  });

  describe('on hasFormGroup', () => {
    beforeEach(() => {
      component.optionList = ['RJ', 'SC', 'SP'];
      component.hasFormGroup = true;
      component.isUppercase = true;
      component.selectId = 'federativeUnit';
      component.parent = formBuilder.group({
        federativeUnit: [''],
      });
    });

    xit('should set selectedOptions and dispatch event when hasFormGroup is true', () => {
      component['ngOnChanges']();
      expect(component.selectedOption).toEqual(component.optionList[0]);
      expect(component.selectedOption.emit).toHaveBeenCalledWith(component.optionList[0]);
    });
  });

  describe('on Change', () => {
    xit('should set selectedOption', () => {
      const federativeUnit = 'RJ';

      component.onChange(federativeUnit);

      expect<any>(component.selectedOption).toEqual(federativeUnit);
    });
  });
});
