import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { WeekDayCheckConfig } from '../../models/revenue-management/week-day-check-config';
import { DaysEnum } from '../../models/days.enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'week-day-check-list',
  templateUrl: 'week-day-check-list.component.html',
  styleUrls: ['week-day-check-list.component.scss']
})
export class WeekDayCheckListComponent implements OnInit {
  @Output() weekDaysSelected = new EventEmitter();

  public inputDataList: Array<WeekDayCheckConfig>;
  public inputDataSelectedList: Array<WeekDayCheckConfig>;
  public daysSelectedList: Array<number>;

  constructor(private translateService: TranslateService) {}

  ngOnInit() {
    this.inputDataList = new Array<WeekDayCheckConfig>();
    this.inputDataSelectedList = new Array<WeekDayCheckConfig>();
    this.daysSelectedList = Array<number>();

    this.inputDataList.push(this.setWeekDayCheckConfig(this.translateService.instant('label.initialsSunday'), DaysEnum.Sunday));
    this.inputDataList.push(this.setWeekDayCheckConfig(this.translateService.instant('label.initialsMonday'), DaysEnum.Monday));
    this.inputDataList.push(this.setWeekDayCheckConfig(this.translateService.instant('label.initialsTuesday'), DaysEnum.Tuesday));
    this.inputDataList.push(this.setWeekDayCheckConfig(this.translateService.instant('label.initialsWednesday'), DaysEnum.Wednesday));
    this.inputDataList.push(this.setWeekDayCheckConfig(this.translateService.instant('label.initialsThursday'), DaysEnum.Thursday));
    this.inputDataList.push(this.setWeekDayCheckConfig(this.translateService.instant('label.initialsFriday'), DaysEnum.Friday));
    this.inputDataList.push(this.setWeekDayCheckConfig(this.translateService.instant('label.initialsSaturday'), DaysEnum.Saturday));

    this.onInputValueSelected();
  }

  public onInputValueSelected() {
    this.inputDataSelectedList = this.inputDataList.filter(this.getDaysSelected);
    this.daysSelectedList = this.inputDataSelectedList.map(inputDataSelected => inputDataSelected.value);

    this.weekDaysSelected.emit(this.daysSelectedList);
  }

  private getDaysSelected = day => {
    return day.isChecked;
  }

  private setWeekDayCheckConfig(initialsDayName: string, dayValue: number) {
    const weekDayConfig = new WeekDayCheckConfig();
    weekDayConfig.inputId = dayValue;
    weekDayConfig.inputTextLabel = initialsDayName;
    weekDayConfig.value = dayValue;
    weekDayConfig.isChecked = true;

    return weekDayConfig;
  }
}
