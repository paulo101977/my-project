import { Component, Input, OnInit } from '@angular/core';
import { BillingAccountWithAccount } from '../../models/billing-account/billing-account-with-account';
import { DateService } from '../../services/shared/date.service';
import { PropertyService } from '../../services/property/property.service';
import { Property } from '../../models/property';

@Component({
  selector: 'app-statement-thermal-printer',
  templateUrl: './statement-thermal-printer.component.html',
})
export class StatementThermalPrinterComponent implements OnInit {

  private _propertyId: number;
  @Input('propertyId')
  set propertyId ( property: number) {
    this._propertyId = property;
    this.getPropertyInfo(property);
  }
  get propertyId(): number {
    return this._propertyId;
  }
  @Input() statementList = new Array<BillingAccountWithAccount>();

  public property: Property;
  constructor( private dateService: DateService, private propertyService: PropertyService) { }

  ngOnInit() {
  }

  public formatDate(date: string) {
    return this.dateService.convertUniversalDateToViewFormat(date);
  }

  public getPropertyInfo(propertyId: number) {
    this.property = this.propertyService.getPropertySession(propertyId);
  }
}
