import { SelectObjectOption } from './../../models/selectModel';
import { BillingItemService } from './../../models/billingType/billing-item-service';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, HostListener, forwardRef } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'thx-custom-dropdown',
  templateUrl: './custom-dropdown.component.html',
  styleUrls: ['custom-dropdown.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomDropdownComponent),
      multi: true,
    },
  ],
})
export class CustomDropdownComponent implements OnChanges, ControlValueAccessor {
  @Input() optionList: Array<SelectObjectOption>;
  @Input() placeholderText: string;

  @Output() clear = new EventEmitter();
  @Output() choose = new EventEmitter();

  public itemChosen: SelectObjectOption;
  public showItemsSelect = false;
  public filter: string;
  public parametersFilter: Array<string>;

  constructor() {}

  propagateChange = (key: any) => {};

  ngOnChanges() {
    this.setVars();
    this.showItemsSelect = false;
  }

  private setVars(): void {
    this.filter = '';
    this.parametersFilter = ['value'];
  }

  public toogleItemsAreaSelect() {
    this.showItemsSelect = !this.showItemsSelect;
  }

  public removeItemFromSelect() {
    this.clear.emit();
  }

  public chooseItem(item: SelectObjectOption) {
    this.itemChosen = item;
    this.propagateChange(item.key);
    this.choose.emit(item.key);
    this.toogleItemsAreaSelect();
  }

  public optionListIsEmpty(): boolean {
    return _.isEmpty(this.optionList);
  }

  @HostListener('document:click')
  private onCloseOutside() {
    this.showItemsSelect = false;
  }

  /*ControlValueAcessor Contract */
  writeValue(value: any): void {
    this.itemChosen = this.optionList.find(item => item.key == value);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}
}
