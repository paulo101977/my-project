import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { IMyDate, IMyDpOptions, IMyInputFieldChanged, MyDatePicker } from 'mydatepicker';
import * as moment from 'moment';
import { DateService } from '../../services/shared/date.service';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { PropertyDateService } from 'app/shared/services/shared/property-date.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'thx-datepicker',
  templateUrl: './datepicker.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true,
    },
  ],
})
export class DatepickerComponent implements OnInit, OnChanges, ControlValueAccessor {

  public propertyId: number;
  @ViewChild('dateElement') dateElement: MyDatePicker;

  @Input() id: string;
  @Input() formControl: string;
  private date: Date;
  @Input()
  set inputValue(myDate: any) {
     this.writeValue(myDate);
  }
  get inputValue(): any {
    return this.date;
  }
  @Input() minDateToday: boolean;
  @Input() disableUntil: IMyDate;
  @Input() disableSince: IMyDate;
  @Input() maxDateToday: boolean;
  @Input() disabled: boolean;
  @Input() readonly: boolean;
  @Input() showClearDateBtn = false;
  @Output() dateChanged = new EventEmitter();
  @Output() dateInputBlur = new EventEmitter();

  public dateValue: any;
  public myDatePickerOptions: IMyDpOptions;

  constructor(private dateService: DateService,
              private propertyDateService: PropertyDateService,
              private route: ActivatedRoute) {}

  propagateChange = (_: string) => {};


  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.configDatePicker();
  }

  ngOnChanges( simpleChange: SimpleChanges ) {
    if (simpleChange['disableUntil'] && this.myDatePickerOptions) {
      this.myDatePickerOptions.disableUntil = simpleChange['disableUntil'].currentValue;
    }

    if (simpleChange['disableSince'] && this.myDatePickerOptions) {
      this.myDatePickerOptions.disableSince = simpleChange['disableSince'].currentValue;
    }
    this.configDatePicker();
  }

  private configDatePicker() {
    const { minDateToday, maxDateToday, disableUntil, disableSince } = this;

    this.myDatePickerOptions = this.dateService.getClearConfigDatePicker();
    this.myDatePickerOptions.showInputField = true;
    this.myDatePickerOptions.editableDateField = true;
    this.myDatePickerOptions.indicateInvalidDate = true;
    this.myDatePickerOptions.showClearDateBtn = this.showClearDateBtn;

    const propertyDateToday = moment(this.dateService.getSystemDateWithoutFormat(this.propertyId));
    const propertyDateTodayIMyDate = <IMyDate>{
      day: +propertyDateToday.subtract(1, 'days').format('D'),
      month: +propertyDateToday.format('M'),
      year: +propertyDateToday.format('YYYY'),
    };

    if (minDateToday) {
      this.myDatePickerOptions.disableUntil = propertyDateTodayIMyDate;
    } else if (maxDateToday) { // disable the calendar after today
      this.myDatePickerOptions.disableSince = propertyDateTodayIMyDate;
    }
    if (disableUntil) {
      this.myDatePickerOptions.disableUntil = disableUntil;
    }
    if (disableSince) { // disable the calendar after the date provide
      this.myDatePickerOptions.disableSince = disableSince;
    }
  }

  emitDateChanged(date) {
    this.writeValue(date);
  }

  inputBlur(event) {
    event.value = this.date ? (<Date>this.date).toStringUniversal() : event.value;
    this.dateInputBlur.emit(event);
  }

  onInputFieldChanged(event: IMyInputFieldChanged) {
    if (this.dateElement && this.dateElement.inputBoxEl) {
      const elementHtml = this.dateElement.inputBoxEl.nativeElement;
      const result = event.value.replace(/[a-zA-Z.,!@#$%¨&*();:]/gi, '');
      elementHtml.value = result;
      event.value = elementHtml.value;
      if (!event.valid) {
        if (elementHtml.value.length == 3 && elementHtml.value.indexOf('/') < 0) {
          elementHtml.value = event.value.slice(0, 2) + '/' + event.value.slice(2, event.value.length);
          event.value = elementHtml.value;
        }
        if (elementHtml.value.length == 6 && (elementHtml.value.match(new RegExp('/', 'g')) || []).length < 2) {
          elementHtml.value = event.value.slice(0, 5) + '/' + event.value.slice(5, event.value.length);
          event.value = elementHtml.value;
        }
        if (elementHtml.value.length > 10) {
          elementHtml.value = event.value.slice(0, 10);
          event.value = elementHtml.value;
        }
        event.valid = moment(event.value, event.dateFormat.toUpperCase(), true).isValid();
      }
      this.dateElement.invalidDate = !event.valid;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  writeValue(myDate: any): void {
    if ( myDate !== null && myDate !== undefined ) {
      if (typeof myDate == 'string') {
        myDate = this.dateService.convertStringToDate(myDate, DateService.DATE_FORMAT_UNIVERSAL);
      }
      if (!myDate.hasOwnProperty('date')) {
        this.dateValue = {
          date: {
            day: myDate.getDate(),
            month: myDate.getMonth() + 1,
            year: myDate.getFullYear(),
          },
        };
        this.date = myDate;
      }
      let dateToPropagate;
      if (myDate.hasOwnProperty('jsdate')) {
        if (myDate.jsdate) {
          dateToPropagate = (<Date>myDate.jsdate).toStringUniversal();
        }
        this.date = myDate.jsdate;
      } else {
        dateToPropagate = (<Date>myDate).toStringUniversal();
        this.date = myDate;
      }
      this.propagateChange(dateToPropagate);
      this.dateChanged.emit(dateToPropagate);
    } else {
      this.dateValue = null;
      this.date = null;
      this.dateChanged.emit(null);
    }
  }

  registerOnTouched(fn: any): void {}
}
