import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DatepickerComponent } from './datepicker.component';
import { MyDatePickerModule } from 'mydatepicker';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpHandler } from '@angular/common/http';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';
import { MomentModule } from 'ngx-moment';

describe('DatepickerComponent', () => {
  let component: DatepickerComponent;
  let fixture: ComponentFixture<DatepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MyDatePickerModule,
        RouterTestingModule,
        TranslateTestingModule,
        MomentModule
      ],
      declarations: [DatepickerComponent],
      providers: [HttpHandler, InterceptorHandlerService],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
