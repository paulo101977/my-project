import { Component, OnInit, Input } from '@angular/core';
import { RoomTypeInfo } from '../../../models/room-type/room-type-info';
import { PadPipe } from '../../../pipe/pad.pipe';

@Component({
  selector: 'room-type-info',
  templateUrl: './room-type-info.component.html',
  styleUrls: ['./room-type-info.component.css']
})
export class RoomTypeInfoComponent implements OnInit {

  @Input() roomInfo: RoomTypeInfo;

  constructor() {
  }

  ngOnInit() {
  }

}
