import { Component, EventEmitter, Input, OnInit, Output, ViewChildren } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuOption } from '../../models/menu-option';
import { MenuService } from '../menu/menu.service';

@Component({
  selector: 'app-menu-option',
  templateUrl: './menu-option.component.html',
})
export class MenuOptionComponent implements OnInit {
  @ViewChildren('submenu') submenu: MenuOptionComponent[];

  @Input() menuOptions: MenuOption;
  @Input() level: number;
  @Output() toggle = new EventEmitter();

  public hidden = false;
  public isActive: boolean;

  constructor(public router: Router,
              public location: Location,
              public route: ActivatedRoute,
              private menuNewService: MenuService) {
  }

  ngOnInit() {
    this.level = this.level || 0;
    if (this.level != 0) {
      this.hidden = true;
    }
    if (this.menuOptions) {
      if (this.menuOptions.children) {
        this.isActive = this.menuOptions.children.some(mo => {
          return mo.children ?
            mo.children.some(child => this.menuIsActive(child)) :
            this.menuIsActive(mo);
        });
      } else {
        this.isActive = this.menuIsActive(this.menuOptions);
      }
    }
  }

  public onClick(event: MouseEvent, menu: MenuOptionComponent) {
    this.menuNewService.setOptionMenuClicked(menu);
    this.menuNewService.collapse(false);

    if (event instanceof MouseEvent) {
      event.preventDefault();
      event.stopPropagation();

      menu.hidden = !menu.hidden;

      if (menu.hidden === false) {
        this.isActive = true;
      } else {
        if (this.menuOptions && this.menuOptions.children) {
          this.isActive = this.menuOptions.children.some(mo => {
            return mo.children ?
              mo.children.some(child => this.menuIsActive(child)) :
              this.menuIsActive(mo);
          });
        } else {
          this.isActive = this.menuIsActive(this.menuOptions);
        }
      }
      if (this.menuOptions.routerLink) {
        this.menuNewService.collapse(true);
        this.navigate(menu.menuOptions.routerLink, menu.menuOptions.queryParams);
      }
    }
  }

  private navigate(routerLink: any | undefined, queryParams: any | undefined) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.router.navigated = false;
    const propertyId = this.route.snapshot.params.property;
    this.router.navigateByUrl(routerLink == 'home' || !propertyId ? `/${routerLink}` : `p/${propertyId}/${routerLink}`, {
      relativeTo: this.route,
    });
  }

  private menuIsActive(menu: MenuOption): boolean {
    return menu.routerLink && this.location.path().endsWith(menu.routerLink);
  }
}
