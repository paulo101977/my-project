export class RadioGroupOption {
  id: string;
  label: string;
  value: any;
}
