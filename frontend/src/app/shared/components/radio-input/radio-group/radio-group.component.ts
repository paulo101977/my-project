import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { RadioGroupOption } from '../models/radio-group-option';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CustomSelectComponent } from '../../custom-select/custom-select.component';

@Component({
  selector: 'app-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioGroupComponent),
      multi: true,
    },
  ],
})
export class RadioGroupComponent implements OnInit, ControlValueAccessor {

  @Input() options: Array<RadioGroupOption>;
  @Input() name: string;

  public disabled = false;
  optionSelected: any;

  constructor() { }

  // Lifecycle
  ngOnInit() {
  }

  public changeOption(option) {
    this.optionSelected = option;
    this.propagateChange(this.optionSelected);
  }

  // Control Value Accessor
  propagateChange = (_: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.changeOption(obj);
  }

}
