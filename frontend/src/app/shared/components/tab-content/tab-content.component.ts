import {
  AfterViewInit, ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren
} from '@angular/core';
import { ConfigLinkToTab } from '../../models/config-link-to-tab';
import { KeyValue } from '../../models/key-value';

@Component({
  selector: 'app-tab-content',
  templateUrl: './tab-content.component.html',
  styleUrls: ['./tab-content.component.css']
})
export class TabContentComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() inputName: string;
  @Input() items: KeyValue[] = [];
  @Input() configLink: ConfigLinkToTab;
  @Output() itemOfSectionContentWasChanged = new EventEmitter<number>();
  @Input() selectedId: any;

  public callBackFunction: Function;
  public currentId: number;
  public showSliderButtons: boolean;
  private readonly SCROLL_LEFT = 80;

  @ViewChildren('itemsListRef', {read: ElementRef}) elemRefs: QueryList<ElementRef>;
  public itemsList: ElementRef;

  constructor(private cdRef: ChangeDetectorRef) {}

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['inputName'] && !this.selectedId) {
      this.setIndexDefaultToTab();
    }

    if (changes['selectedId']) {
      if (this.currentId != changes['selectedId'].currentValue ) {
        this.currentId = changes['selectedId'].currentValue;
        this.itemOfSectionContentWasChanged.emit(this.currentId);
      }
    }
  }

  ngAfterViewInit() {
    // this.showSliderButtons = true;
    // this.cdRef.detectChanges();
    this.elemRefs.changes.subscribe((item) => {
      this.itemsList = item.first;
      const boxWidth = item.first.nativeElement.parentElement.getBoundingClientRect().width;
      const toScrollWidth = item.first.nativeElement.getBoundingClientRect().width;
      if (toScrollWidth > boxWidth) {
        this.showSliderButtons = true;
        this.cdRef.detectChanges();
      }
    });
  }

  private setIndexDefaultToTab(): void {
    if (this.items && this.items.length > 0) {
      this.currentId = this.items[0].key;
      this.itemOfSectionContentWasChanged.emit(this.currentId);
    }
  }

  private setVars(): void {
    this.hasConfigLink();
  }

  public runCallbackFunction() {
    this.callBackFunction();
  }

  private hasConfigLink() {
    if (this.configLink && this.configLink.linkAction) {
      this.callBackFunction = this.configLink.linkAction;
    }
  }

  public fireEventOnItemOfSectionContentWasChanged(key: any): void {
    this.itemOfSectionContentWasChanged.emit(key);
  }

  public prevScroll() {
    this.itemsList.nativeElement.parentElement.scrollBy(-this.SCROLL_LEFT, 0);
  }

  public nextScroll() {
    this.itemsList.nativeElement.parentElement.scrollBy(this.SCROLL_LEFT, 0);
  }
}
