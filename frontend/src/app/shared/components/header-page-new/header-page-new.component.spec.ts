import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderPageNewComponent } from './header-page-new.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ConfigHeaderPageNew } from './config-header-page-new';

describe('HeaderPageNewComponent', () => {
  let component: HeaderPageNewComponent;
  let fixture: ComponentFixture<HeaderPageNewComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), RouterTestingModule],
      providers: [],
      declarations: [HeaderPageNewComponent],
    });

    fixture = TestBed.createComponent(HeaderPageNewComponent);
    component = fixture.componentInstance;

    component.config = <ConfigHeaderPageNew>{
      title: 'test',
    };

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
      expect(component.showButton).toBeFalsy();
    });

    it('should setVars without configHeaderPageNew', () => {
      spyOn<any>(component, 'hasBackPreviewPage');
      spyOn<any>(component, 'hasCallBackFunction');
      spyOn<any>(component, 'keepTitleButtonInView');
      component.config = null;

      component['setVars']();

      expect(component['hasBackPreviewPage']).not.toHaveBeenCalled();
      expect(component['hasCallBackFunction']).not.toHaveBeenCalled();
      expect(component['keepTitleButtonInView']).not.toHaveBeenCalled();
    });

    it('should setVars with configHeaderPageNew', () => {
      spyOn<any>(component, 'hasBackPreviewPage');
      spyOn<any>(component, 'hasCallBackFunction');
      spyOn<any>(component, 'keepTitleButtonInView');
      component.config = <ConfigHeaderPageNew>{
        keepTitleButton: true,
        keepButtonActive: false,
        callBackFunction: () => {},
      };

      component['setVars']();

      expect(component['hasBackPreviewPage']).toHaveBeenCalled();
      expect(component['hasCallBackFunction']).toHaveBeenCalled();
      expect(component['keepTitleButtonInView']).toHaveBeenCalled();
    });

    it('should verify if hasBackPreviewPage', () => {
      component.config.hasBackPreviewPage = true;

      component['hasBackPreviewPage']();

      expect(component.enableBackButton).toBeTruthy();
    });

    it('should goBackPreviewPage', () => {
      spyOn(component.location, 'back');

      component['goBackPreviewPage']();

      expect(component.location.back).toHaveBeenCalled();
    });

    it('should verify if hasCallBackFunction', () => {
      const functionToPass = Function;
      component.config.callBackFunction = functionToPass;

      component['hasCallBackFunction']();

      expect(component.callBackFunction).toEqual(functionToPass);
    });

    it('should keepTitleButtonInView', () => {
      component.config.keepTitleButton = true;

      component['keepTitleButtonInView']();

      expect(component.keepTitleButton).toBeTruthy();
    });

    it('should keepButtonActiveWhenClickedInView', () => {
      component.config.keepButtonActive = true;

      component['keepButtonActiveWhenClickedInView']();

      expect(component.keepButtonActive).toBeTruthy();
    });

    it('should runCallbackFunction', () => {
      component.callBackFunction = Function;
      spyOn(component, 'callBackFunction');
      spyOn<any>(component, 'keepButtonActiveWhenClickedInView');

      component['runCallbackFunction']();

      expect(component.callBackFunction).toHaveBeenCalled();
      expect(component['keepButtonActiveWhenClickedInView']).toHaveBeenCalled();
    });
  });
});
