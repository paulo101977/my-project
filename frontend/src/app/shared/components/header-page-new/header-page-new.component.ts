import { Component, OnInit, Input, ViewChild, ElementRef, AfterContentInit, OnChanges } from '@angular/core';
import { Location } from '@angular/common';
import { ConfigHeaderPageNew } from './config-header-page-new';
import { RightButtonTop } from '../../models/right-button-top';

@Component({
  selector: 'app-header-page-new',
  templateUrl: './header-page-new.component.html',
  styleUrls: ['./header-page-new.component.css'],
})
export class HeaderPageNewComponent implements OnInit, OnChanges, AfterContentInit {
  @Input() config: ConfigHeaderPageNew;
  @Input() hasFilter: boolean;
  @Input() hasHeaderMiddleText: boolean;
  @Input() rightButtonList: Array<RightButtonTop>;
  @Input() filterButtonIsLarge: boolean;
  @Input() enableBackButton: boolean;
  @ViewChild('button') button: ElementRef;
  public keepTitleButton: boolean;
  public keepButtonActive: boolean;
  public callBackFunction: Function;
  public showButton: boolean;
  public showButtonRightList: boolean;
  public backPreviewPageCallBackFunction: Function;

  constructor(public location: Location) {}

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges(changes) {
    if (changes.rightButtonList) {
      this.showButtonRightList = this.rightButtonList && this.rightButtonList.length > 0;
    }
  }

  private setVars(): void {
    if (this.config) {
      this.hasBackPreviewPage();
      this.setBackPreviewCallBackFunction();
      this.hasCallBackFunction();
      this.keepTitleButtonInView();
    }
  }

  ngAfterContentInit(): void {
    this.showButton = this.button && this.button.nativeElement.children.length > 0;
    this.showButtonRightList = this.rightButtonList && this.rightButtonList.length > 0;
  }

  private hasBackPreviewPage() {
    if (this.config.hasBackPreviewPage) {
      this.enableBackButton = true;
    }
  }

  private setBackPreviewCallBackFunction() {
    if (this.config.backPreviewPageCallBackFunction) {
      this.backPreviewPageCallBackFunction = this.config.backPreviewPageCallBackFunction;
    }
  }

  public goBackPreviewPage() {
    if (this.backPreviewPageCallBackFunction) {
      this.backPreviewPageCallBackFunction();
    } else {
      this.location.back();
    }
  }

  private hasCallBackFunction() {
    if (this.config.callBackFunction) {
      this.callBackFunction = this.config.callBackFunction;
    }
  }

  private keepTitleButtonInView() {
    if (this.config.keepTitleButton) {
      this.keepTitleButton = true;
    }
  }

  private keepButtonActiveWhenClickedInView() {
    if (this.config.keepButtonActive) {
      this.keepButtonActive = true;
    }
  }

  public runCallbackFunction() {
    this.callBackFunction();
    this.keepButtonActiveWhenClickedInView();
  }

  runCallbackButtonFunction(btnIten: RightButtonTop) {
    btnIten.callback();
  }
}
