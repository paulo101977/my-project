import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'thx-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css'],
})
export class SearchInputComponent implements OnInit {
  @Input() extraClass: string;
  @Input() placeholder: string;

  @Output() searchByTerm: EventEmitter<string> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  public changeValue(value) {
    this.searchByTerm.emit(value);
  }
}
