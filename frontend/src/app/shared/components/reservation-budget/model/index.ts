export enum ModalReservationState {
  canceled = 1,
  closed =  2,
  confirmed = 3
}
