import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import {
  ReservationItemCommercialBudget
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';
import { GratuityType } from 'app/shared/models/revenue-management/reservation-budget/gratuity-type';
import { GratuityTypeIdEnum } from 'app/shared/models/gratuity-type-id-enum';
import { TranslateService } from '@ngx-translate/core';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ReservationBudgetService } from 'app/shared/services/reservation-budget/reservation-budget.service';
import { ReservationBudget } from 'app/shared/models/revenue-management/reservation-budget/reservation-budget';
import { ReservationItemViewStatusEnum } from 'app/shared/models/reserves/reservation-item-view';
import * as _ from 'lodash';
import { ReservationModalService } from 'app/shared/components/reservation-budget/service/reservation-modal.service';
import { ModalReservationState } from 'app/shared/components/reservation-budget/model';
import { Proforma } from '../../../shared/models/proforma';
import { ProformaResource } from '../../../shared/resources/proforma/proforma.resource';
import { PropertyService } from '../../../shared/services/property/property.service';
import { CountryCodeEnum } from '../../../shared/models/country-code-enum';
import {ModalEmitService} from 'app/shared/services/shared/modal-emit.service';
import { FiscalDocumentsService } from '../../../shared/services/billing-account/fiscal-documents.service';
import { PdfPrintService } from '../../../shared/services/pdf-print/pdf-print.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { SuccessError } from '../../../shared/models/success-error-enum';

@Component({
  selector: 'app-reservation-budget',
  templateUrl: './reservation-budget.component.html',
  styleUrls: ['./reservation-budget.component.css'],
})
export class ReservationBudgetComponent implements OnInit, OnChanges, OnDestroy {

  @Input() propertyId: number;
  @Input() showHeader = true;
  @Input() showFooter = true;

  @Input() originalReservationBudgetListSaved: Array<ReservationBudget>;
  @Input() period: any;
  @Input() gratuityTypeSelected: number;
  @Input() reservationStatus: ReservationItemViewStatusEnum;
  @Input() isMigrated: boolean;
  @Input() reservationId: number;
  @Input() reservationItemId: number;
  private periodOLD: any;

  private _commercialBudgetList: Array<ReservationItemCommercialBudget>;
  @Input('commercialBudgetList')
  set commercialBudgetList( list: Array<ReservationItemCommercialBudget>) {
    this._commercialBudgetList = list ? JSON.parse(JSON.stringify(list)) : [];
    this.prepareSelectOptions();
  }

  get commercialBudgetList(): Array<ReservationItemCommercialBudget> {
    return this._commercialBudgetList;
  }

  // Commercial Budget Select
  public commercialBudgetSelectList: Array<any> = [];
  public commercialBudgetSelectedItemComplete: ReservationItemCommercialBudget;
  @Input() commercialBudgetSelectedItem: any;

  @Input() selectedReservationBudgetList: Array<ReservationBudget>;

  @Output() cancelClicked = new EventEmitter();
  @Output() confirmedClicked = new EventEmitter();

  private selectedReservationBudgetListIntern: Array<ReservationBudget>;

  // Gratuity
  public isForFree = false;
  public gratuityTypeList: Array<any>;
  public gratuityTypeSelectOptionsList: Array<SelectObjectOption>;
  public gratuityTypeId = 0;
  //
  public averageDaily = 0;
  public overnights = 0;
  public total = 0;
  private propertyCountryCode: string;

  // Buttons
  public issueProformaIndividualButtonConfig: ButtonConfig;
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;

  constructor(
    private currencyFormatPipe: CurrencyFormatPipe,
    private translateService: TranslateService,
    private commonService: CommomService,
    private sharedService: SharedService,
    private reservationBudgetService: ReservationBudgetService,
    private reservationModalService: ReservationModalService,
    private proFormaResource: ProformaResource,
    private propertyService: PropertyService,
    private modalEmitService: ModalEmitService,
    private fiscalDocumentService: FiscalDocumentsService,
    private pdfPrintService: PdfPrintService,
    private toasterEmitService: ToasterEmitService
    ) { }

  ngOnInit() {
    this.setButtonConfig();
    this.setGratuityTypeList();
    this.propertyCountryCode = this.propertyService.getPropertyCountryCode();
    this.reservationModalService.createListener();
  }

  ngOnChanges( changes: SimpleChanges ) {
    if (changes.hasOwnProperty('commercialBudgetSelectedItem')) {
      this.commercialBudgetSelectedItemComplete = this.getCommercialBudgetSelectedItemComplete(this.commercialBudgetSelectedItem);
    }
    if (changes.hasOwnProperty('period')) {
      this.periodOLD = changes.period.previousValue;
    }
    if (changes.hasOwnProperty('gratuityTypeSelected')) {
      this.gratuityTypeId = changes.gratuityTypeSelected.currentValue;
      this.isForFree = this.gratuityTypeId > 0;
    }
  }

  ngOnDestroy(): void {
    this.reservationModalService.complete();
  }

  public setSelectedCommercialBudget( item ) {
    if (this.commercialBudgetSelectedItem != item) {
      this.commercialBudgetSelectedItem = item;
      this.commercialBudgetSelectedItemComplete = this.getCommercialBudgetSelectedItemComplete(item);
      this.controlButtons();
    }
  }

  public toggleGratuityType( item ) {
    this.isForFree = item;
    if (!this.isForFree) {
      this.gratuityTypeId = 0;
    }
    this.controlButtons();
  }

  public updateGratuityType( item ) {
    if ( this.gratuityTypeId != item ) {
      this.gratuityTypeId = item;
      this.controlButtons();
    }
  }

  public cancelChangesReservationBudget = () => {
    this.reservationModalService.setModalState(ModalReservationState.canceled);
    this.cancelClicked.emit();
  }

  public saveReservationBudget = () => {
    this.confirmedClicked.emit( this.getReturnObj() );
  }

  private getReturnObj() {
    const { commercialBudgetList,
      commercialBudgetSelectedItemComplete,
      commercialBudgetSelectedItem,
      selectedReservationBudgetListIntern,
      gratuityTypeId,
      isForFree,
      period,
      periodOLD,
    } = this;
    return {
      commercialBudgetList: commercialBudgetList,
      commercialBudgetItem: commercialBudgetSelectedItemComplete,
      commercialBudgetItemKey: commercialBudgetSelectedItem,
      reservationBudgetList: selectedReservationBudgetListIntern,
      gratuityTypeId: gratuityTypeId,
      isGratuity: isForFree,
      period: {
        current: period,
        previous: periodOLD
      }
    };
  }
  public agreementRateHasChanged() {
    if (this.originalReservationBudgetListSaved && this.selectedReservationBudgetListIntern   ) {
      const sumTotal = this.selectedReservationBudgetListIntern
        .reduce( (val, currentItem) => val += currentItem.manualRate, 0);

      const sumTotalOriginal = this.originalReservationBudgetListSaved
        .reduce( (val, currentItem) => val += currentItem.manualRate, 0);

      return sumTotalOriginal != sumTotal;
    }
    return false;
  }

  public updateTotals( totals ) {
    if ( totals ) {
      this.overnights = totals.overnightQtd;
      this.averageDaily = totals.averageDaily;
      this.total = totals.total;
    }
  }

  public unselectItem( item ) {
    this.commercialBudgetSelectedItem = null;
    this.commercialBudgetSelectedItemComplete = null;
  }

  // CONFIG
  private commercialBudgetListHasSelectedItem() {
    let hasItem = false;
    if (this.commercialBudgetSelectList) {
      this.commercialBudgetSelectList.forEach( item => {
        hasItem = item.key == this.commercialBudgetSelectedItem;
      });
    }
    return hasItem;
  }

  private prepareSelectOptions() {
    this.commercialBudgetSelectList = this.commercialBudgetList
      .filter( item => item.isMealPlanTypeDefault) // filter only the mealPlanDefault
      .map(reservationItemCommercialBudget => {
        const totalBudget = this.currencyFormatPipe.transform(
          reservationItemCommercialBudget.totalBudget,
          reservationItemCommercialBudget.currencySymbol);
        const option = new SelectObjectOption();
        option.key = this.reservationBudgetService.getReservationItemCommercialBudgetIdentifier(reservationItemCommercialBudget);
        option.value = `<div class="justify-content-row" style="padding-left: 5px"><div class="thf-u-font--13">${
          reservationItemCommercialBudget.commercialBudgetName
          }</div><div class="thf-u-font--13">${
          reservationItemCommercialBudget.mealPlanTypeName
          }</div><div class="thf-u-font--13" style="margin-top: 5px">
              ${totalBudget}
            </div></div>`;
        return option;
      }) || [];
  }

  private getCommercialBudgetSelectedItemComplete( key ) {
    return this.commercialBudgetList
      .find( item => _.isEqual(this.reservationBudgetService.getReservationItemCommercialBudgetIdentifier(item), key));
  }

  private setGratuityTypeList(): void {
    this.gratuityTypeList = [];
    this.gratuityTypeSelectOptionsList = new Array<SelectObjectOption>();
    this.translateService.get('text.internalUsage').subscribe(text => {
      const gratuityType1 = new GratuityType();
      gratuityType1.gratuityTypeId = GratuityTypeIdEnum.InternalUsage;
      gratuityType1.gratuityTypeName = text;

      const gratuityType2 = new GratuityType();
      gratuityType2.gratuityTypeId = GratuityTypeIdEnum.Cortesy;
      gratuityType2.gratuityTypeName = this.translateService.instant('text.courtesy');

      this.gratuityTypeList.push(gratuityType1);
      this.gratuityTypeList.push(gratuityType2);
    });

    this.gratuityTypeSelectOptionsList = this.commonService.toOption(
        this.gratuityTypeList,
        'gratuityTypeId',
        'gratuityTypeName',
      );
  }

  private setButtonConfig(): void {
    this.issueProformaIndividualButtonConfig = this.sharedService.getButtonConfig(
      'issue-proforma',
      this.issueProformaIndividual,
      'label.issueProforma',
      ButtonType.Secondary,
    );
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel',
      this.cancelChangesReservationBudget,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'confirm',
      this.saveReservationBudget,
      'action.confirm',
      ButtonType.Primary,
    );
    this.controlButtons();
  }

  private controlButtons() {
    if ( (this.commercialBudgetSelectedItem && !this.isForFree)  || ( this.isForFree && this.gratuityTypeId > 0) ) {
      this.confirmButtonConfig.isDisabled = false;
    } else {
      this.confirmButtonConfig.isDisabled = true;
    }
  }

  public updateList( item ) {
    this.selectedReservationBudgetListIntern = item;
  }

  private issueProformaIndividual = () => {
    this.modalEmitService.emitSimpleModal(
      this.translateService.instant(`label.emitProForma`),
      this.translateService.instant('text.tourismTaxInProforma'),
      () => this.emitProforma(true), null, () => this.emitProforma()  );
  }

  public emitProforma(withTourism = false) {
    const proforma = new Proforma();
    proforma.reservationItemId = this.reservationItemId;
    proforma.launchTourismTax = withTourism;
    this.proFormaResource.issueProforma(proforma)
      .subscribe(response => {
        this.printProforma(proforma);
      });
  }

  private printProforma(proforma: Proforma) {
    this
      .fiscalDocumentService
      .getProforma(proforma)
      .subscribe(
      (byteCodes: any) => {
        if ( byteCodes ) {
          this.pdfPrintService.printPdfFromByteCode(byteCodes);
        }
      }, () => {
        this.showMessagePrintInvoiceError();
      });
  }

  private showMessagePrintInvoiceError() {
    this
      .toasterEmitService
      .emitChange(
        SuccessError.error,
        this.translateService.instant('alert.errorPrintInvoice')
      );
  }

  public showIssueProformaButton(): boolean {
    return this.reservationId && this.propertyCountryCode == CountryCodeEnum.PT;
  }
}
