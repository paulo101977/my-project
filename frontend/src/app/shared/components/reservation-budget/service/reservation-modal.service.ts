import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ModalReservationState } from 'app/shared/components/reservation-budget/model';

@Injectable({
  providedIn: 'root',
})
export class ReservationModalService {
  private _modalStateChanged;

  public createListener() {
    if ( !this._modalStateChanged ) {
      this._modalStateChanged = new Subject<ModalReservationState>();
    }
  }

  public setModalState( value: ModalReservationState ) {
    if ( this._modalStateChanged ) {
      this._modalStateChanged.next(value);
    }
  }

  public getModalState(): Observable<ModalReservationState> {
    return this._modalStateChanged.asObservable();
  }

  public complete() {
    if ( this._modalStateChanged ) {
      this._modalStateChanged.complete();
      this._modalStateChanged = null;
    }
  }
}
