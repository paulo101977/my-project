import { createServiceStub } from '../../../../../../../testing';
import { ReservationModalService } from 'app/shared/components/reservation-budget/service/reservation-modal.service';
import { ModalReservationState } from 'app/shared/components/reservation-budget/model';
import { of } from 'rxjs/internal/observable/of';

export const reservationModalServiceStub = createServiceStub(ReservationModalService, {
  complete() {},
  getModalState: () => of( null ),
  createListener() {},
  setModalState(value: ModalReservationState) {}
});
