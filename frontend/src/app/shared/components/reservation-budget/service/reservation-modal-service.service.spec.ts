import { TestBed, inject } from '@angular/core/testing';

import { ReservationModalService } from './reservation-modal.service';

describe('ReservationModalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReservationModalService]
    });
  });

  it('should be created', inject([ReservationModalService], (service: ReservationModalService) => {
    expect(service).toBeTruthy();
  }));
});
