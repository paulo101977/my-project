import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationBudgetComponent } from './reservation-budget.component';

import {  NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { currencyFormatPipe } from '../../../../../testing';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { reservationModalServiceStub } from 'app/shared/components/reservation-budget/service/testing';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { ThexApiTestingModule } from '@inovacaocmnet/thx-bifrost';
import { PropertyService } from '../../../shared/services/property/property.service';
import { createServiceStub } from 'testing/create-service-stub';

// TODO: create tests [DEBIT: 7517]

describe('ReservationBudgetComponent', () => {
  let component: ReservationBudgetComponent;
  let fixture: ComponentFixture<ReservationBudgetComponent>;

  const propertyServiceStub = createServiceStub(PropertyService, {
    getPropertyCountryCode(): string { return 'BR'; }
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ReservationBudgetComponent,
        currencyFormatPipe,
      ],
      imports: [
        TranslateTestingModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ThexApiTestingModule
      ],
      providers: [
        commomServiceStub,
        sharedServiceStub,
        reservationModalServiceStub,
        propertyServiceStub
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(ReservationBudgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
