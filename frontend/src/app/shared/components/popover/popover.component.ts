import { Component, AfterContentInit, ElementRef, Input, ViewChild, ViewContainerRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
})
export class PopoverComponent implements OnInit, AfterContentInit {
  @ViewChild('header') header: ElementRef;
  @ViewChild('footer') footer: ElementRef;
  public showHeader: boolean;
  public showFooter: boolean;

  constructor() {}

  ngOnInit(): void {
    this.showHeader = false;
    this.showFooter = false;
  }

  ngAfterContentInit(): void {
    this.showHeader = this.header.nativeElement.children.length > 0;
    this.showFooter = this.footer.nativeElement.children.length > 0;
  }
}
