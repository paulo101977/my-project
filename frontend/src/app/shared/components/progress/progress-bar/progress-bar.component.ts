import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'thx-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css'],
})
export class ProgressBarComponent implements OnInit {
  @Input() class: string;
  @Input() style: string;
  @Input() value: number;
  @Input() icon: string;
  @Input() unit: string;
  private _bgColor: SafeStyle;
  private _textColor: SafeStyle;

  @Input()
  set bgColor(color) {
    this._bgColor = this.sanitizer.bypassSecurityTrustStyle('var(--color-' + color + ')');
  }

  get bgColor() {
    return this._bgColor;
  }

  @Input()
  set textColor(color) {
    this._textColor = this.sanitizer.bypassSecurityTrustStyle('var(--color-' + color + ')');
  }

  get textColor() {
    return this._textColor;
  }

  constructor(protected sanitizer: DomSanitizer) {}

  ngOnInit() {}
}
