import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { ModalGroupOptionsFormEmitService } from './../../services/shared/modal-group-options-form-emit.service';

import { ReservationGroupOption } from './../../models/reserves/reservation-group-option';

@Component({
  selector: 'app-modal-group-options',
  templateUrl: './modal-group-options.component.html',
})
export class ModalGroupOptionsComponent {
  @Input() readonly: boolean;
  @Output() reservationGroupOptionUpdated = new EventEmitter();
  public reserveGroupOption: ReservationGroupOption;
  public reserveGroupOptionOld: ReservationGroupOption;
  public isVisible: boolean;

  constructor(public modalGroupOptionsFormEmitService: ModalGroupOptionsFormEmitService) {
    this.reserveGroupOption = new ReservationGroupOption();
    this.reserveGroupOptionOld = new ReservationGroupOption();
    modalGroupOptionsFormEmitService.changeEmitted$.subscribe(response => {
      this.reserveGroupOption.groupName = response[0];
      this.reserveGroupOption.unifyAccounts = response[1];
      this.reserveGroupOptionOld.groupName = response[0];
      this.reserveGroupOptionOld.unifyAccounts = response[1];
      this.showModal();
    });
  }

  public showModal() {
    this.isVisible = true;
  }

  public closeModal() {
    this.reserveGroupOption.groupName = this.reserveGroupOptionOld.groupName;
    this.reserveGroupOption.unifyAccounts = this.reserveGroupOptionOld.unifyAccounts;
    this.isVisible = false;
  }

  public saveReservationGroupOption() {
    this.reservationGroupOptionUpdated.emit(this.reserveGroupOption);
    this.isVisible = false;
  }
}
