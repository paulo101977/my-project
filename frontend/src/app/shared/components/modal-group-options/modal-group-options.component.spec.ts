import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { ModalGroupOptionsComponent } from './modal-group-options.component';

describe('GroupOptions', () => {
  let component: ModalGroupOptionsComponent;
  let fixture: ComponentFixture<ModalGroupOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), FormsModule],
      declarations: [ModalGroupOptionsComponent],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalGroupOptionsComponent);
    component = fixture.componentInstance;

    spyOn<any>(component.modalGroupOptionsFormEmitService, 'changeEmitted$').and.callFake(() => {});

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should execute showModal', () => {
    spyOn(component, 'showModal');

    component.modalGroupOptionsFormEmitService.emitSimpleModal('grupo Master', true);

    expect(component.showModal).toHaveBeenCalled();
  });

  it('should set isVisible for true in showModal', () => {
    component.showModal();

    expect(component.isVisible).toBeTruthy();
  });

  it('should set isVisible for false in showModal', () => {
    component.closeModal();

    expect(component.isVisible).toBeFalsy();
  });

  it('should set reserveGroupOption.groupName and reserveGroupOption.unifyAccounts for reserveGroupOptionOld in closeModal', () => {
    component.closeModal();

    expect(component.reserveGroupOption.groupName).toEqual(component.reserveGroupOptionOld.groupName);
    expect(component.reserveGroupOption.unifyAccounts).toEqual(component.reserveGroupOptionOld.unifyAccounts);
  });

  it('should set isVisible for false in showModal', () => {
    component.closeModal();

    expect(component.isVisible).toBeFalsy();
  });

  it('should set isVisible for false in saveReservationGroupOption', () => {
    component.saveReservationGroupOption();

    expect(component.isVisible).toBeFalsy();
  });

  it('should emit onReservationGroupOptionUpdated in saveReservationGroupOption', () => {
    spyOn(component.reservationGroupOptionUpdated, 'emit');

    component.saveReservationGroupOption();

    expect(component.reservationGroupOptionUpdated.emit).toHaveBeenCalled();
  });
});
