import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'thx-group-buttons',
  templateUrl: './group-buttons.component.html',
})
export class GroupButtonsComponent implements OnInit {
  @Input() buttonItens: Array<any>;
  @Input() mantainChecked: false;
  @Input() btnClass: string;
  @Input() hasBtnClean: boolean;
  @Input('selectedOption')
  set selectedOption (item: any) {
    this._selectedBtn = item;
    this.selectedOptionChange.emit(item);
  }
  get selectedOption (): any {
    return this._selectedBtn;
  }

  @Output() clickButton: EventEmitter<any> = new EventEmitter();
  @Output() changeSelected: EventEmitter<any> = new EventEmitter();
  @Output() clean: EventEmitter<any> = new EventEmitter();
  @Output() selectedOptionChange = new EventEmitter();
  public _selectedBtn: any;

  constructor() {}

  ngOnInit() {}

  public get selectedBtn() {
    return this._selectedBtn;
  }

  buttonClicked(element) {
    this.selectedOption = element;
    this.clickButton.emit(element);
    this.changeSelected.emit(element);
  }

  clear() {
    this.selectedOption = null;
    this.clean.emit();
  }
}
