import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupButtonsComponent } from './group-buttons.component';
import { TranslateModule } from '@ngx-translate/core';

describe('GroupButtonsComponent', () => {
  let component: GroupButtonsComponent;
  let fixture: ComponentFixture<GroupButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [GroupButtonsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
