import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { ToasterComponent } from './toaster.component';
import { CoreModule } from '@app/core/core.module';

describe('ToasterComponent', () => {
  let component: ToasterComponent;
  let fixture: ComponentFixture<ToasterComponent>;
  const responseToasterService = [true, 'Mensagem do toaster'];

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), RouterTestingModule, CoreModule],
      declarations: [ToasterComponent],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(ToasterComponent);
    component = fixture.componentInstance;
    component['toasterEmitService'].changeEmitted$ = of(responseToasterService);

    fixture.detectChanges();
  });

  describe('on init hasRedirect false and success true', () => {
    it(
      'should set initial config',
      fakeAsync(() => {
        expect(component.isSuccess).toBeTruthy();
        expect(component.isVisible).toBeTruthy();
        expect<any>(component.message).toEqual(<string>responseToasterService[1]);

        tick(5000);

        fixture.whenStable().then(() => {
          expect(component.isVisible).toBeFalsy();
          expect(component.message).toEqual('');
        });
      }),
    );
  });
});
