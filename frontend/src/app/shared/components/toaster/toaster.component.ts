import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterEmitService } from './../../services/shared/toaster-emit.service';
import { SubscriptionLike as ISubscription } from 'rxjs';

@Component({
  selector: 'app-toaster',
  templateUrl: './toaster.component.html',
})
export class ToasterComponent implements OnInit, OnDestroy {
  public isSuccess: boolean;
  public message: string;
  public isVisible: boolean;
  public hasRedirect: boolean;
  private redirect: any[];
  private subscription: ISubscription;

  constructor(private toasterEmitService: ToasterEmitService, private route: Router) {}

  ngOnInit() {
    this.subscription = this.toasterEmitService.changeEmitted$.subscribe(response => {
      const isSuccess = response[0];
      const message = response[1];
      const hasRedirect = response[2];
      if (isSuccess) {
        this.isSuccess = true;
      } else {
        this.isSuccess = false;
      }
      if (hasRedirect) {
        this.hasRedirect = true;
        this.redirect = response[2];
      }
      this.showToaster(message);
    });
  }

  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }

  private showToaster(message: string) {
    this.isVisible = true;
    this.message = message;
    setTimeout(() => {
      this.isVisible = false;
      this.message = '';
      if (this.hasRedirect) {
        this.route.navigate(this.redirect);
      }
    }, 5000);
  }
}
