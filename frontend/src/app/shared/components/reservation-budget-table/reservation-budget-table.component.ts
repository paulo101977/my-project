import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { MealPlanTypeResource } from 'app/shared/resources/meal-plan-type/meal-plan-type.resource';
import { MealPlanType } from 'app/shared/models/revenue-management/meal-plan-type';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { CommomService } from 'app/shared/services/shared/commom.service';
import {
  ReservationItemCommercialBudget
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { ReservationBudget } from 'app/shared/models/revenue-management/reservation-budget/reservation-budget';
import { ReservationBudgetService } from 'app/shared/services/reservation-budget/reservation-budget.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import {
  ReservationItemCommercialBudgetDay
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-day';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';
import { ReservationModalService } from 'app/shared/components/reservation-budget/service/reservation-modal.service';
import { ModalReservationState } from 'app/shared/components/reservation-budget/model';


@Component({
  selector: 'app-reservation-budget-table',
  templateUrl: './reservation-budget-table.component.html',
  styleUrls: ['./reservation-budget-table.component.css']
})
export class ReservationBudgetTableComponent implements OnInit, OnChanges, OnDestroy {

  public readonly MODAL_UPDATE_DAY_VALUE = 'manual_rate_change_modal';
  public readonly MEAL_PLAN_TYPE_ID_DEFAULT = 2;

  // Obrigatórios
  @Input() propertyId: number;
  @Input() isForFree: boolean;
  @Input() period: any;
  @Input() reservationStatus: ReservationStatusEnum;
  @Input() isMigrated: boolean;
  @Input() commercialBudgetList: Array<ReservationItemCommercialBudget>;
  @Input() commercialBudgetSelectedItem: ReservationItemCommercialBudget;
  @Input() originalReservationBudgetListSaved: Array<ReservationBudget>;

  // Table
  private _selectedReservationBudgetList: Array<ReservationBudget>;
  private _selectedReservationBudgetListORIGINAL: Array<ReservationBudget>;

  @Input('selectedReservationBudgetList')
  set selectedReservationBudgetList(list: Array<ReservationBudget>) {
    if (this.originalReservationBudgetListSaved) {
      if (list) {
        list.forEach( (item, index) => {
          const oldItem = this.originalReservationBudgetListSaved[index];
          if (oldItem) {
            item.baseRateChanged = oldItem.manualRate != item.manualRate;
          }
        });
      }
    }
    this._selectedReservationBudgetListORIGINAL = this.cloneReservationBudgetList( list );
    this._selectedReservationBudgetList = this.cloneReservationBudgetList( list );
  }

  get selectedReservationBudgetList(): Array<ReservationBudget> {
    return this._selectedReservationBudgetList;
  }

  @Output() totalsHasChanged = new EventEmitter();
  @Output() updateList = new EventEmitter();

  // MealPlan
  public mealPlanTypeList: Array<MealPlanType>;
  public mealPlanTypeSelectList: Array<SelectObjectOption>;

  private hasSetSelectedReservationBudgetList = false;

  public optionsCurrencyMask: any;
  public cantEditMigrated = true;

  constructor(private mealPlanTypeResource: MealPlanTypeResource,
              private commomService: CommomService,
              private reservationBudgetService: ReservationBudgetService,
              private dateService: DateService,
              private sessionParameter: SessionParameterService,
              private sharedService: SharedService,
              private modalToggleService: ModalEmitToggleService,
              private currencyService: CurrencyService,
              private reservationModalService: ReservationModalService ) {
  }

  ngOnInit() {
    this.setInputMask();
    this.setMealPlanTypeList();
    this.getCanEditMigrated();
    this.listenModalClose();
  }

  private getCanEditMigrated() {
    this.cantEditMigrated = this.sessionParameter
      .getParameterValue(this.propertyId, ParameterTypeEnum.EditMigrated) == 'true';
  }

  ngOnDestroy(): void {
    this.reservationModalService.complete();
  }

  public listenModalClose() {
    this
      .reservationModalService
      .getModalState()
      .subscribe( (state: ModalReservationState) => {

        if ( state === ModalReservationState.canceled ) {
          this._selectedReservationBudgetList =
            this.cloneReservationBudgetList( this._selectedReservationBudgetListORIGINAL );
          this.setSelectedReservationBudgetList();
          this.updateList.emit(this.selectedReservationBudgetList);
          this.totalsHasChanged.emit(this.calculateTotals());
        }
      });
  }

  public cloneReservationBudgetList( list: Array<ReservationBudget>): Array<ReservationBudget> {
    return list ? JSON.parse(JSON.stringify(list)) : list;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('selectedReservationBudgetList')) {
      const { selectedReservationBudgetList } = changes;
      this.hasSetSelectedReservationBudgetList = selectedReservationBudgetList.currentValue;
    }
    this.setSelectedReservationBudgetList();
    this.updateList.emit(this.selectedReservationBudgetList);
    this.totalsHasChanged.emit(this.calculateTotals());
  }

  public setInputMask() {
    // the input number mask
    this.optionsCurrencyMask = {
      prefix: '',
      thousands: '.',
      decimal: ',',
      align: 'center',
      allowZero: true,
      allowNegative: false
    };
  }

  // REQUESTS
  private setMealPlanTypeList(): void {
    this.mealPlanTypeResource.getAllMealPlanTypeList(this.propertyId).subscribe(
      response => {
        this.mealPlanTypeList = [...response.items];
        this.mealPlanTypeSelectList = this.commomService
          .convertObjectToSelectOptionObjectByIdAndValueProperties(
            this.mealPlanTypeList,
            'mealPlanTypeId',
            'propertyMealPlanTypeName'
          );
      }
    );
  }

  // Config
  public setSelectedReservationBudgetList() {
    const mealPlanTypes: Array<any> = null;
    if (this.selectedReservationBudgetList) {

      /* TODO a parte de mealPlantypes foi desabilitada do orçamento em um primeiro momento, para evitar um bug
      Essa feature simplesmente mantinha as pensões selecionadas mesmo quando eu mudava as tarifas e/ou gratuidade
      Será pensada uma nova solução para manter a feature ativa sem bugs. */

      /*mealPlanTypes = this.selectedReservationBudgetList.map( item => {
        return {
          day: item.budgetDay,
          mealPlanTypeId: item.mealPlanTypeId
        };
      });*/
    }

    if (!this.hasSetSelectedReservationBudgetList) {
      if (this.commercialBudgetSelectedItem && !this.isForFree) {
        this.selectedReservationBudgetList = this.prepareBudget(this.reservationBudgetService
          .transformCommercialBudgetDayListInReservationBudget(
            this.commercialBudgetSelectedItem.commercialBudgetDayList,
            this.isForFree,
            this.commercialBudgetSelectedItem.commercialBudgetName, mealPlanTypes ));

        /*if (mealPlanTypes) {
          // seleciono as mealPlans antigas
          this.selectedReservationBudgetList.forEach( (item, index) => {
            let mealPlanTypeId = null;
            if (mealPlanTypes) {
              const filtered = mealPlanTypes.filter( mp => moment(item.budgetDay).isSame( moment(mp.day) ) );
              if ( filtered && filtered.length > 0 ) {
                mealPlanTypeId = filtered[0].mealPlanTypeId;
              }
              if (mealPlanTypeId) {
                this.updateMealPlanTypeIdSelected( mealPlanTypeId, index);
              }
            }
          });
        }*/
      }
    }
    if ( this.isForFree || !this.commercialBudgetSelectedItem  && !this.selectedReservationBudgetList ) {
        this.selectedReservationBudgetList = [];
        const selectedReservationBudgetListTemp = [];
        if (this.period) {
          const dateList = this.dateService
            .convertPeriodToDateList(this.period.beginDate, this.period.endDate);
          dateList.forEach(date => {
            /*let mealPlanTypeId = null;
            if (mealPlanTypes) {
              const filtered = mealPlanTypes.filter( item => moment(date).isSame( moment(item.day) ) );
              if ( filtered && filtered.length > 0 ) {
                mealPlanTypeId = filtered[0].mealPlanTypeId;
              }
            }*/
            selectedReservationBudgetListTemp.push(this.getCommercialBudgetDayDefault(date, null));
          });
        }
        this.selectedReservationBudgetList = this.prepareBudget(selectedReservationBudgetListTemp);
    }

    this.selectedReservationBudgetList = [...this.selectedReservationBudgetList];
    this.hasSetSelectedReservationBudgetList = false;
  }

  private getCommercialBudgetDayDefault(date: any, mealPlanTypeId) {
    const reservationBudget = new ReservationBudget();
    reservationBudget.budgetDay = date;
    reservationBudget.baseRate = 0;
    reservationBudget.baseRateChanged = false;
    reservationBudget.manualRate = 0;
    reservationBudget.agreementRate = 0;
    const parameterCurrency = this.sessionParameter
      .getParameter(this.propertyId, ParameterTypeEnum.DefaultCurrency);
    if (parameterCurrency) {
      reservationBudget.currencySymbol = this.currencyService.getDefaultCurrencySymbol(this.propertyId);
      reservationBudget.currencyId = parameterCurrency.propertyParameterValue;
    }
    reservationBudget.discount = 0;
    reservationBudget.mealPlanTypeId = mealPlanTypeId ? mealPlanTypeId : this.MEAL_PLAN_TYPE_ID_DEFAULT;
    return reservationBudget;
  }

  // Logic
  public manualRateChange(rowIndex) {
    const element = this.selectedReservationBudgetList[rowIndex];
    if (element.manualRate < element.agreementRate) {
      element.discount = this.sharedService.getDiscountPercentage(element.manualRate, element.agreementRate);
    } else {
      element.discount = 0;
    }
    this.updateList.emit(this.selectedReservationBudgetList);
    this.totalsHasChanged.emit(this.calculateTotals());
  }

  public discountChange(rowIndex) {
    const element = this.selectedReservationBudgetList[rowIndex];
    element.manualRate = this.sharedService
      .getValueWithDiscount(element.agreementRate, element.discount);
    this.updateList.emit(this.selectedReservationBudgetList);
    this.totalsHasChanged.emit(this.calculateTotals());
  }

  public updateMealPlanTypeIdSelected(mealPlanTypeId, rowIndex) {
    const originalList = this.commercialBudgetList;
    const current = this.commercialBudgetSelectedItem;
    if (current && !this.isForFree) {
      const element = originalList.filter(item => item.mealPlanTypeId == mealPlanTypeId &&
        item.ratePlanId == current.ratePlanId && item.currencyId == current.currencyId)[0];
      if (element) {
        const day = this.selectedReservationBudgetList[rowIndex].budgetDay;
        const dayInfo: ReservationItemCommercialBudgetDay = element.commercialBudgetDayList.filter(d => d.day == day)[0];
        if (dayInfo) {
          this._selectedReservationBudgetList[rowIndex].manualRate = dayInfo.total;
          this._selectedReservationBudgetList[rowIndex].agreementRate = dayInfo.total;
          this._selectedReservationBudgetList[rowIndex].discount = 0;
        }
      }
    }
    this.updateList.emit(this.selectedReservationBudgetList);
    this.totalsHasChanged.emit(this.calculateTotals());
  }

  private calculateTotals() {
    const total = this.getTotalBudget();
    const rateVariation = this.commercialBudgetSelectedItem ?
      this.commercialBudgetSelectedItem.rateVariation : 0;
    const averageDaily = this.setAverageDaily();
    const overnightQtd = this.setOvernights();
    return {averageDaily, overnightQtd, rateVariation, total};
  }

  public setOvernights() {
    if (this.period) {
      return this.dateService
        .calculateOvernightStayAsString(this.period.beginDate, this.period.endDate);
    }
  }

  public setAverageDaily() {
    const total = this.getTotalBudget();
    return total > 0 ? total / this.selectedReservationBudgetList.length : 0;
  }

  public getTotalBudget() {
    if (this.selectedReservationBudgetList && !this.isForFree) {
      return this.selectedReservationBudgetList.reduce((val, currentItem) => val += currentItem.manualRate, 0);
    }
    return 0;
  }

  public mantainSameValue() {
    this.modalToggleService.emitCloseWithRef(this.MODAL_UPDATE_DAY_VALUE);
  }

  public updateValue() {
    this.modalToggleService.emitCloseWithRef(this.MODAL_UPDATE_DAY_VALUE);
    // Todo implement
  }

  public canEditBudget( item ) {
    const diff = this.dateService.compareWithSystemDate(this.propertyId, item.budgetDay);
    return diff >= 0 && this.canEdit();
  }

  public canEdit() {
    // verify if migrated reservation can be editted
    if (this.isMigrated) {
      return this.cantEditMigrated;
    }
    return true;
  }

  public prepareBudget( newReservationBudgetList: ReservationBudget[]) {
    if (this.reservationStatus) {
      const pastDaysBudget = this.originalReservationBudgetListSaved.filter( item => !this.canEditBudget(item));
      const futureDaysBudget = newReservationBudgetList.filter( item => this.canEditBudget(item));
      return [...pastDaysBudget, ...futureDaysBudget];
    }
    return newReservationBudgetList;
  }

}
