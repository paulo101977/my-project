import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationBudgetTableComponent } from './reservation-budget-table.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { amDateFormatPipeStub, currencyFormatPipe } from '../../../../../testing';
import { reservationModalServiceStub } from 'app/shared/components/reservation-budget/service/testing';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { currencyServiceStub } from 'app/shared/services/shared/testing/currency-service';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { sessionParameterServiceStub } from 'app/shared/services/parameter/testing';
import { ThexApiModule, configureApi, ApiEnvironment } from '@inovacaocmnet/thx-bifrost';

// TODO: create tests [DEBIT: 7517]

describe('ReservationBudgetTableComponent', () => {
  let component: ReservationBudgetTableComponent;
  let fixture: ComponentFixture<ReservationBudgetTableComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ThexApiModule
      ],
      providers: [
        reservationModalServiceStub,
        modalToggleServiceStub,
        currencyServiceStub,
        dateServiceStub,
        sharedServiceStub,
        sessionParameterServiceStub,
        configureApi({
          environment: ApiEnvironment.Development
        })
      ],
      declarations: [
        ReservationBudgetTableComponent,
        amDateFormatPipeStub,
        currencyFormatPipe,
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(ReservationBudgetTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
