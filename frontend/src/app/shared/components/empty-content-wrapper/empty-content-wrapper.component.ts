import { Component } from '@angular/core';

@Component({
  selector: 'app-empty-content-wrapper',
  templateUrl: './empty-content-wrapper.component.html',
})
export class EmptyContentWrapperComponent {}
