import { FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectObjectOption } from './../../models/selectModel';
import { PaymentTypeWithBillingItem } from './../../models/billingType/payment-type-with-billing-item';
import { PaymentTypeEnum } from './../../models/reserves/payment-type-enum';
import { ButtonConfig, ButtonType } from './../../models/button-config';
import { SuccessError } from './../../models/success-error-enum';
import { CommomService } from './../../services/shared/commom.service';
import { SharedService } from './../../services/shared/shared.service';
import { ToasterEmitService } from './../../services/shared/toaster-emit.service';
import { BillingItemResource } from '../../resources/billingItem/billing-item.resource';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';
import { BillingAccountTypeEnum } from 'app/shared/models/billing-account/billing-account-type-enum';

@Component({
  selector: 'payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css'],
})
export class PaymentComponent implements OnInit, OnChanges {
  @Input() accountTypeId: BillingAccountTypeEnum;
  @Input() paymentForm: FormGroup;
  @Input() billingAccountItemName: string;
  @Input() paymentTotal: number;
  @Input() holderName: string;
  @Output() paymentTypeWithBillingItemListWasUpdated = new EventEmitter<Array<PaymentTypeWithBillingItem>>();
  @Output() closeThisModal = new EventEmitter();
  @Output() paymentWasMade = new EventEmitter();

  public debitBalance: number;
  public valueInDebit: number;
  private propertyId: number;
  public isCard: boolean;
  public isBankCheck: boolean;
  public paymentTypeList: Array<SelectObjectOption>;
  public optionsCurrencyMask: any;
  public paymentTypeIdChosen: number;
  public isDebit: boolean;
  public currencySymbol: any;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonPayConfig: ButtonConfig;

  constructor(
    private billingItemResource: BillingItemResource,
    private commomService: CommomService,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private toasterEmitService: ToasterEmitService,
    private sessionParameter: SessionParameterService,
    private currencyService: CurrencyService
  ) {}

  ngOnInit() {
    this.setVars();
    this.setListeners();

    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.currencySymbol = this.currencyService.getDefaultCurrencySymbol(this.propertyId);
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.paymentForm.get('paymentTypeId').setValue(null);
    this.setPriceAndDebitBalance();
    if (this.propertyId) {
      this.getPaymentTypeList();
    }
  }

  private setVars(): void {
    this.isCard = false;
    this.isBankCheck = false;
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
    this.setButtonConfig();
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-payment-modal',
      this.closeModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonPayConfig = this.sharedService.getButtonConfig(
      'pay-payment-modal',
      this.fireEventForMakePayment,
      'action.pay',
      ButtonType.Primary,
    );
  }

  private closeModal = () => {
    this.closeThisModal.emit();
  }

  private fireEventForMakePayment = () => {
    this.paymentWasMade.emit();
  }

  private getPaymentTypeList(): void {
    this.paymentTypeList = new Array<SelectObjectOption>();
    this.billingItemResource.getAllPaymentTypeWithBillingItemListByPropertyId(this.propertyId).subscribe(response => {
      this.paymentTypeList = this.commomService
        .toOption(response, 'id', 'paymentTypeName');

      if (this.accountTypeId != BillingAccountTypeEnum.Company) {
        this.paymentTypeList = [...this.paymentTypeList.filter(type => {
          if (type.key != PaymentTypeEnum.ToBeBilled) {
            return type;
          }
        })];
      }

      this.paymentTypeWithBillingItemListWasUpdated.emit(response.items);
    });
  }

  private isCardOrBankCheck(paymentTypeWithBillingItem: PaymentTypeWithBillingItem): boolean {
    return paymentTypeWithBillingItem.id == PaymentTypeEnum.CreditCard || paymentTypeWithBillingItem.id == PaymentTypeEnum.Debit;
  }

  private setListeners(): void {
    this.paymentForm.valueChanges.subscribe(value => {
      this.buttonPayConfig = this.sharedService.getButtonConfig(
        'pay-payment-modal',
        this.fireEventForMakePayment,
        'action.pay',
        ButtonType.Primary,
        null,
        this.paymentForm.invalid,
      );
    });

    this.paymentForm.get('paymentTypeId').valueChanges.subscribe(paymentTypeId => {
      paymentTypeId = parseFloat(paymentTypeId);
      this.clearDataOfPaymentForm();
      switch (paymentTypeId) {
        case PaymentTypeEnum.Check:
          this.isBankCheck = true;
          this.isCard = false;
          this.clearValidatorsInPaymentForm();
          break;
        case PaymentTypeEnum.Debit:
          this.isDebit = true;
          this.isBankCheck = false;
          this.isCard = true;
          this.paymentTypeIdChosen = paymentTypeId;
          this.paymentForm.get('card.installmentsNumber').setValue(1);
          this.setValidatorsInPaymentForm();
          break;
        case PaymentTypeEnum.CreditCard:
          this.isDebit = false;
          this.isBankCheck = false;
          this.isCard = true;
          this.paymentTypeIdChosen = paymentTypeId;
          this.setValidatorsInPaymentForm();
          break;
        default:
          this.isBankCheck = false;
          this.isCard = false;
          this.clearValidatorsInPaymentForm();
          break;
      }
    });

    this.paymentForm.get('price').valueChanges.subscribe(price => {
      if (isNaN(price) || price == null) {
        this.valueInDebit = this.debitBalance;
      } else if (price > this.debitBalance) {
        this.toasterEmitService.emitChange(SuccessError.error, 'text.valueIsGreaterThanDebitBalance');
        this.paymentForm.get('price').setValue(this.debitBalance);
      } else {
        this.valueInDebit = this.debitBalance - price;
      }
    });
  }

  private setValidatorsInPaymentForm(): void {
    this.paymentForm.get('card.plasticBrandPropertyId').setValidators([Validators.required]);
    this.paymentForm.get('card.installmentsNumber').setValidators([Validators.required]);
  }

  private clearValidatorsInPaymentForm(): void {
    this.paymentForm.get('card.plasticBrandPropertyId').clearValidators();
    this.paymentForm.get('card.installmentsNumber').clearValidators();
  }

  private clearDataOfPaymentForm(): void {
    this.paymentForm.get('card.plasticBrandPropertyId').setValue(null);
    this.paymentForm.get('card.installmentsNumber').setValue(null);
    this.paymentForm.get('card.nsuCode').setValue(null);
    this.paymentForm.get('bankCheck.number').setValue(null);
  }

  private setPriceAndDebitBalance(): void {
    if (this.paymentTotal) {
      this.debitBalance = Math.abs(this.paymentTotal);
      this.paymentForm.get('price').setValue(Math.abs(this.paymentTotal));
    }
  }
}
