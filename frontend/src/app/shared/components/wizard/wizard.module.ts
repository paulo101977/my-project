import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepsComponent } from './steps/steps.component';
import { StepComponent } from './step/step.component';

@NgModule({
  imports: [CommonModule],
  declarations: [StepsComponent, StepComponent],
  exports: [StepsComponent, StepComponent],
})
export class WizardModule {}
