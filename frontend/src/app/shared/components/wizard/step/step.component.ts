import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'thx-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.css'],
})
export class StepComponent {
  @Input() active = false;
  _color: SafeStyle;

  constructor(protected sanitizer: DomSanitizer) {}

  @Input()
  set color(color) {
    this._color = this.sanitizer.bypassSecurityTrustStyle('var(--color-' + color + ')');
  }

  get color() {
    return this._color;
  }
}
