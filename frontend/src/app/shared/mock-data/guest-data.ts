import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Guest } from './../models/reserves/guest';

const formBuilder = new FormBuilder();
const formGuest = formBuilder.group({
  guestName: ['Ricardo'],
});

export const GuestAdultData = new Guest();
GuestAdultData.formGuest = formGuest;
GuestAdultData.isMain = true;

export const GuestChildData = new Guest();
GuestChildData.formGuest = formGuest;
GuestChildData.isChild = true;
GuestChildData.childAge = 5;
