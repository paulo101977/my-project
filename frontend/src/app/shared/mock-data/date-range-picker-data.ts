const today = new Date();
const tomorrow = new Date();
tomorrow.setDate(tomorrow.getDate() + 1);

export const PeriodData = {
  beginJsDate: today,
  endJsDate: tomorrow,
  beginDate: {
    year: today.getFullYear(),
    month: today.getMonth() + 1,
    day: today.getDate(),
  },
  endDate: {
    year: tomorrow.getFullYear(),
    month: tomorrow.getMonth() + 1,
    day: tomorrow.getDate(),
  },
};
