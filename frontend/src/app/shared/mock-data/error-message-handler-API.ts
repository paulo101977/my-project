// Models
import { ErrorMessageHandlerAPI } from './../models/error-message/error-message-handler-api';

const errorData = {
  status: 400,
  error: {
    code: 'code',
    message: ['deu erro', 'erro'],
    detailedMessage: 'erro detalhes',
    helpUrl: 'www.help.com',
    details: [
      {
        guid: 'stringGUID',
        code: 'stringCode',
        message: 'stringMessage',
        detailedMessage: 'stringDetails',
      },
    ],
  },
};

export const ErrorMessageHandlerAPIData = new ErrorMessageHandlerAPI(errorData);
