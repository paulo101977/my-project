import { ReasonDto } from './../models/dto/checkin/reason-dto';

export const ReasonDtoData = new ReasonDto();
ReasonDtoData.reasonName = 'Compras';
ReasonDtoData.reasonCategoryId = 7;
ReasonDtoData.chainId = 1;
ReasonDtoData.isActive = true;
ReasonDtoData.id = 3;

export const ReasonDtoData2 = new ReasonDto();
ReasonDtoData2.reasonName = 'Congresso/Feira';
ReasonDtoData2.reasonCategoryId = 7;
ReasonDtoData2.chainId = 1;
ReasonDtoData2.isActive = true;
ReasonDtoData2.id = 4;

export const ReasonDtoDataList = new Array<ReasonDto>();
ReasonDtoDataList.push(ReasonDtoData);
ReasonDtoDataList.push(ReasonDtoData2);
