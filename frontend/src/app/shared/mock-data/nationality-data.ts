import { NationalityDto } from 'app/shared/models/dto/checkin/nationality-dto';

export const NationalityDtoData = new NationalityDto();
NationalityDtoData.languageIsoCode = 'pt-BR';
NationalityDtoData.name = 'Brasil';
NationalityDtoData.countrySubdivisionId = 1;
NationalityDtoData.nationality = 'Brasileira';
NationalityDtoData.id = 1;

export const NationalityDtoData2 = new NationalityDto();
NationalityDtoData2.languageIsoCode = 'pt-BR';
NationalityDtoData2.name = 'Brasil';
NationalityDtoData2.countrySubdivisionId = 5593;
NationalityDtoData2.nationality = 'Portuguesa';
NationalityDtoData2.id = 1;

export const NationalityDtoDataList = new Array<NationalityDto>();
NationalityDtoDataList.push(NationalityDtoData);
NationalityDtoDataList.push(NationalityDtoData2);
