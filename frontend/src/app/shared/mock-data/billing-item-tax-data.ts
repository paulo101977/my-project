// Models
import { BillingItemTaxServiceAssociateData } from './billing-item-tax-service-associate-data';
import { BillingItemTaxServiceAssociate } from './../models/billingType/billing-item-tax-service-associate';
import { BillingItemTax } from './../models/billingType/billing-item-tax';

export const BillingItemTaxData = new BillingItemTax();
BillingItemTaxData.billingItemCategoryId = 1;
BillingItemTaxData.isActive = true;
BillingItemTaxData.name = 'Day use';
BillingItemTaxData.serviceAndTaxList = new Array<BillingItemTaxServiceAssociate>();
BillingItemTaxData.serviceAndTaxList.push(BillingItemTaxServiceAssociateData);
