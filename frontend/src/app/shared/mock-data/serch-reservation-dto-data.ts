import { SearchReservationDto } from 'app/shared/models/dto/search-reservation-dto';

export const searchReservationDtoData = new SearchReservationDto();

searchReservationDtoData.checkInToday = true;
searchReservationDtoData.checkOutToday = false;
searchReservationDtoData.deadline = false;
searchReservationDtoData.passingByInitial = '';
searchReservationDtoData.passingByFinal = '';
searchReservationDtoData.groupName = '';
searchReservationDtoData.cancellationInitial = '';
searchReservationDtoData.cancellationFinal = '';
searchReservationDtoData.creationInitial = '';
searchReservationDtoData.creationFinal = '';
searchReservationDtoData.deadlineInitial = '';
searchReservationDtoData.deadlineFinal = '';
searchReservationDtoData.departureInitial = '';
searchReservationDtoData.departureFinal = '';
searchReservationDtoData.arrivalInitial = '';
searchReservationDtoData.arrivalFinal = '';
searchReservationDtoData.reservationItemStatus = null;
searchReservationDtoData.freeTextSearch = '';
searchReservationDtoData.order = '';
searchReservationDtoData.page = 1;
searchReservationDtoData.pageSize = 10;
