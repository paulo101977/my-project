// Models
import { SearchableItems } from './../models/filter-search/searchable-item';
import { TableRowTypeEnum } from './../models/table-row-type-enum';

export const SearchableItemsData = new SearchableItems();
SearchableItemsData.items = [];
SearchableItemsData.placeholderSearchFilter = 'placeholder';
SearchableItemsData.tableRowTypeEnum = TableRowTypeEnum.GuestAccount;

export const SearchableItemsBillingAccountData = new SearchableItems();
SearchableItemsBillingAccountData.items = [];
SearchableItemsBillingAccountData.placeholderSearchFilter = 'billingAccountModule.commomData.placeholderFilter';
SearchableItemsBillingAccountData.tableRowTypeEnum = TableRowTypeEnum.BillingAccount;
