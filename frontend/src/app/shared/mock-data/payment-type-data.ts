// Models
import { PaymentType } from './../models/payment-types';

export const PaymentTypeInternalUsage = new PaymentType();
PaymentTypeInternalUsage.key = 1;
PaymentTypeInternalUsage.value = 'reservationModule.paymentTypes.internalUsage';

export const PaymentTypeDeposit = new PaymentType();
PaymentTypeDeposit.key = 2;
PaymentTypeDeposit.value = 'reservationModule.paymentTypes.deposit';

export const PaymentTypeCourtesy = new PaymentType();
PaymentTypeCourtesy.key = 3;
PaymentTypeCourtesy.value = 'reservationModule.paymentTypes.courtesy';

export const PaymentTypeHotel = new PaymentType();
PaymentTypeHotel.key = 4;
PaymentTypeHotel.value = 'reservationModule.paymentTypes.hotel';

export const PaymentTypeCreditCard = new PaymentType();
PaymentTypeCreditCard.key = 5;
PaymentTypeCreditCard.value = 'reservationModule.paymentTypes.creditCard';

export const PaymentTypeToBeBillet = new PaymentType();
PaymentTypeToBeBillet.key = 6;
PaymentTypeToBeBillet.value = 'reservationModule.paymentTypes.toBeBillet';

export const PaymentTypeListData = Array<PaymentType>();
PaymentTypeListData.push(PaymentTypeToBeBillet);
PaymentTypeListData.push(PaymentTypeInternalUsage);
PaymentTypeListData.push(PaymentTypeDeposit);
PaymentTypeListData.push(PaymentTypeCourtesy);
PaymentTypeListData.push(PaymentTypeHotel);
PaymentTypeListData.push(PaymentTypeCreditCard);
