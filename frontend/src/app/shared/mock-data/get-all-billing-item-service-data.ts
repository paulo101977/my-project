// Models
import { GetAllBillingItemService } from './../models/billingType/get-all-billing-item-service';

export const GetAllBillingItemServiceData = new GetAllBillingItemService();
GetAllBillingItemServiceData.id = 1;
GetAllBillingItemServiceData.billingItemName = 'Imposto sobre Serviço';
GetAllBillingItemServiceData.billingItemCategoryName = 'Grupo A';
GetAllBillingItemServiceData.isActive = false;

export const GetAllBillingItemServiceInactiveData = new GetAllBillingItemService();
GetAllBillingItemServiceInactiveData.id = 2;
GetAllBillingItemServiceInactiveData.billingItemName = 'CONFINS Serviços';
(GetAllBillingItemServiceInactiveData.billingItemCategoryName = 'Grupo B'), (GetAllBillingItemServiceInactiveData.isActive = false);

export const GetAllBillingItemServiceData1 = new GetAllBillingItemService();
GetAllBillingItemServiceData1.id = 3;
GetAllBillingItemServiceData1.billingItemName = 'Imposto sobre circulação de mercadorias e serviços';
GetAllBillingItemServiceData1.billingItemCategoryName = 'Grupo C';
GetAllBillingItemServiceData1.isActive = false;

export const GetAllBillingItemServiceData2 = new GetAllBillingItemService();
GetAllBillingItemServiceData2.id = 4;
GetAllBillingItemServiceData2.billingItemName = 'Day extra';
GetAllBillingItemServiceData2.billingItemCategoryName = 'Grupo D';
GetAllBillingItemServiceData2.isActive = false;

export const GetAllBillingItemServiceData3 = new GetAllBillingItemService();
GetAllBillingItemServiceData3.id = 5;
GetAllBillingItemServiceData3.billingItemName = 'Multa de atraso';
GetAllBillingItemServiceData3.billingItemCategoryName = 'Grupo E';
GetAllBillingItemServiceData3.isActive = false;
