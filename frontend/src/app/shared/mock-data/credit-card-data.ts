// Models
import { CreditCard } from './../models/credit-card';

export const CreditCardData = new CreditCard();

CreditCardData.holder = 'Saulo Mendes';
CreditCardData.plasticBrandId = 1;
