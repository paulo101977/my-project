// Models
import { ReservationConfirmationDto } from './../models/dto/reserve-confirmation-dto';
import { ClientContact } from './../models/clientContact';
import { Client } from './../models/client';
import { ReservationItemGuestView } from './../models/reserves/reservation-item-guest-view';
import { GuestReservationItemData } from './guest-reservation-item-data';
import { ReservationConfirmationToBilletData } from './reservation-confirmation-dto-data';
import { Reservation } from './../models/reserves/reservation';
import { GuestReservationItem } from 'app/shared/models/reserves/guest-reservation-item';
import { ReservationItem } from 'app/shared/models/reserves/reservation-item';
import { ReservationItemViewToConfirmData, ReservationItemViewCancelData, ReservationItemViewCheckinData } from './reservationItem-data';

export const ReservationData = new Reservation();

export const ReservationItemGuestViewData = new ReservationItemGuestView();
ReservationItemGuestViewData.id = 10;
ReservationItemGuestViewData.guestReservationItemList.push(GuestReservationItemData);
ReservationItemGuestViewData.estimatedArrivalDate = '';
ReservationItemGuestViewData.estimatedDepartureDate = '';

ReservationData.id = 1;
ReservationData.externalComments = 'Comentário externo';
ReservationData.internalComments = 'Comentário interno';
ReservationData.contactName = 'Abc';
ReservationData.contactEmail = 'abc@email.com';
ReservationData.contactPhone = '12345678';
ReservationData.groupName = '12345678';
ReservationData.unifyAccounts = true;
ReservationData.client = new Client();
ReservationData.client.id = '32323223aa';
ReservationData.client.tradeName = 'Totvs';
ReservationData.clientContact = new ClientContact();
ReservationData.clientContact.id = '44444bbb';
ReservationData.reservationItemGuestViewList.push(ReservationItemGuestViewData);

export const ReservationDataWithConfirmationToBillet = new Reservation();

ReservationDataWithConfirmationToBillet.id = 1;
ReservationDataWithConfirmationToBillet.externalComments = 'Comentário externo';
ReservationDataWithConfirmationToBillet.internalComments = 'Comentário interno';
ReservationDataWithConfirmationToBillet.contactName = 'Abc';
ReservationDataWithConfirmationToBillet.contactEmail = 'abc@email.com';
ReservationDataWithConfirmationToBillet.contactPhone = '12345678';
ReservationDataWithConfirmationToBillet.groupName = '12345678';
ReservationDataWithConfirmationToBillet.unifyAccounts = true;
ReservationDataWithConfirmationToBillet.client = new Client();
ReservationDataWithConfirmationToBillet.client.id = '32323223aa';
ReservationDataWithConfirmationToBillet.client.tradeName = 'Totvs';
ReservationDataWithConfirmationToBillet.clientContact = new ClientContact();
ReservationDataWithConfirmationToBillet.clientContact.id = '44444bbb';
ReservationDataWithConfirmationToBillet.reservationItemGuestViewList.push(ReservationItemGuestViewData);
ReservationDataWithConfirmationToBillet.reservationConfirmation = ReservationConfirmationToBilletData;

export const ReservationToConfirmData = new Reservation();

ReservationToConfirmData.id = 10;
ReservationToConfirmData.externalComments = 'Comentário externo';
ReservationToConfirmData.internalComments = 'Comentário interno';
ReservationToConfirmData.contactName = 'Abc';
ReservationToConfirmData.contactEmail = 'abc@email.com';
ReservationToConfirmData.contactPhone = '12345678';
ReservationToConfirmData.groupName = '12345678';
ReservationToConfirmData.unifyAccounts = true;
ReservationToConfirmData.client = new Client();
ReservationToConfirmData.client.id = '32323223aa';
ReservationToConfirmData.client.tradeName = 'Totvs';
ReservationToConfirmData.clientContact = new ClientContact();
ReservationToConfirmData.clientContact.id = '44444bbb';
ReservationToConfirmData.reservationItemGuestViewList.push(ReservationItemGuestViewData);
ReservationToConfirmData.reservationItemViewList.push(ReservationItemViewToConfirmData);

export const ReservationCancelData = new Reservation();

ReservationCancelData.id = 10;
ReservationCancelData.externalComments = 'Comentário externo';
ReservationCancelData.internalComments = 'Comentário interno';
ReservationCancelData.contactName = 'Abc';
ReservationCancelData.contactEmail = 'abc@email.com';
ReservationCancelData.contactPhone = '12345678';
ReservationCancelData.groupName = '12345678';
ReservationCancelData.unifyAccounts = true;
ReservationCancelData.client = new Client();
ReservationCancelData.client.id = '32323223aa';
ReservationCancelData.client.tradeName = 'Totvs';
ReservationCancelData.clientContact = new ClientContact();
ReservationCancelData.clientContact.id = '44444bbb';
ReservationCancelData.reservationItemGuestViewList.push(ReservationItemGuestViewData);
ReservationCancelData.reservationItemViewList.push(ReservationItemViewCancelData);

export const ReservationCheckinData = new Reservation();

ReservationCheckinData.id = 10;
ReservationCheckinData.externalComments = 'Comentário externo';
ReservationCheckinData.internalComments = 'Comentário interno';
ReservationCheckinData.contactName = 'Abc';
ReservationCheckinData.contactEmail = 'abc@email.com';
ReservationCheckinData.contactPhone = '12345678';
ReservationCheckinData.groupName = '12345678';
ReservationCheckinData.unifyAccounts = true;
ReservationCheckinData.client = new Client();
ReservationCheckinData.client.id = '32323223aa';
ReservationCheckinData.client.tradeName = 'Totvs';
ReservationCheckinData.clientContact = new ClientContact();
ReservationCheckinData.clientContact.id = '44444bbb';
ReservationCheckinData.reservationItemGuestViewList.push(ReservationItemGuestViewData);
ReservationCheckinData.reservationItemViewList.push(ReservationItemViewCheckinData);
