import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import { FnrhSaveDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';

export const FnrhDtoData = new FnrhDto();
FnrhDtoData.id = '1343-4234-423432-42343-423';
FnrhDtoData.document = '332.323.323-32';
FnrhDtoData.documentTypeId = 1;
FnrhDtoData.guestReservationItemId = 1;
FnrhDtoData.firstName = 'Caio';
FnrhDtoData.lastName = 'Régis';
FnrhDtoData.fullName = 'Caio de Andrade Régis';
FnrhDtoData.email = 'regisacaio@gmail.com';
FnrhDtoData.birthDate = '1990/06/27';
FnrhDtoData.age = 27;
FnrhDtoData.gender = 'male';
FnrhDtoData.occupationId = 1;
FnrhDtoData.guestTypeId = 1;
FnrhDtoData.phoneNumber = '3323-3332';
FnrhDtoData.mobilePhoneNumber = '3232-3232';
FnrhDtoData.nationality = 1;
FnrhDtoData.purposeOfTrip = 'Lazer';
FnrhDtoData.arrivingBy = 1;
FnrhDtoData.arrivingFrom = 2;
FnrhDtoData.nextDestination = 1;
FnrhDtoData.additionalInformation = 'Info additional';
FnrhDtoData.location = null;

export const FnrhSaveDtoData = new FnrhSaveDto();
FnrhSaveDtoData.additionalInformation = null;
FnrhSaveDtoData.arrivingBy = 1;
FnrhSaveDtoData.arrivingFromText = 'Rio de Janeiro';
FnrhSaveDtoData.birthDate = new Date('1990/06/27');
FnrhSaveDtoData.document.documentInformation = '38576961920';
FnrhSaveDtoData.document.documentTypeId = 1;
FnrhSaveDtoData.document.id = 0;
FnrhSaveDtoData.email = '';
FnrhSaveDtoData.firstName = 'LUIZ ANTONIO CICHELETO';
FnrhSaveDtoData.fullName = 'LUIZ ANTONIO CICHELETO';
FnrhSaveDtoData.gender = 'M';
FnrhSaveDtoData.guestReservationItemId = 10;
FnrhSaveDtoData.guestTypeId = 1;
FnrhSaveDtoData.healthInsurance = null;
FnrhSaveDtoData.id = null;
FnrhSaveDtoData.lastName = 'LUIZ ANTONIO CICHELETO';
FnrhSaveDtoData.location.additionalAddressDetails = '';
FnrhSaveDtoData.location.division = 'PR';
FnrhSaveDtoData.location.id = 0;
FnrhSaveDtoData.location.locationCategoryId = 4;
FnrhSaveDtoData.location.neighborhood = '';
FnrhSaveDtoData.location.postalCode = 0;
FnrhSaveDtoData.location.streetName = ',';
FnrhSaveDtoData.location.streetNumber = ',';
FnrhSaveDtoData.location.subdivision = 'Foz do Iguaçu';
FnrhSaveDtoData.mobilePhoneNumber = null;
FnrhSaveDtoData.nationality = 1;
FnrhSaveDtoData.nextDestinationText = 'Natal';
FnrhSaveDtoData.occupationId = null;
FnrhSaveDtoData.personId = 'undefined';
FnrhSaveDtoData.phoneNumber = null;
FnrhSaveDtoData.purposeOfTrip = 1;
FnrhSaveDtoData.socialName = null;
