// Models
import { BillingAccountHeader } from './../models/billing-account/billing-account-header';

export const BillingAccountHeaderData = new BillingAccountHeader();

BillingAccountHeaderData.reservationItemCode = '1211212';
BillingAccountHeaderData.externalComments =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod ante eu ipsum consequat, sit amet ullamcorper lacus euismod.' +
  ' Quisque sed lectus a enim egestas luctus.';
BillingAccountHeaderData.internalComments =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod ante eu ipsum consequat, sit amet ullamcorper lacus euismod.' +
  ' Quisque sed lectus a enim egestas luctus.';
BillingAccountHeaderData.quantityComments = 2;
BillingAccountHeaderData.priceDayDaily = 100;
BillingAccountHeaderData.priceTotal = 200;
BillingAccountHeaderData.statusName = 'Check-In';
