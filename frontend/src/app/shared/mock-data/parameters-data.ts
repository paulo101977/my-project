import { Parameter, ParameterType, ParameterOption } from '../models/parameter';

export const parameterGroupList: Array<Parameter> = [];

const param = new Parameter();
param.parameterDescription = 'Horário de Check-In';
param.propertyParameterValue = '12:00';
param.parameterTypeName = ParameterType.Time;
parameterGroupList.push(param);

const param1 = new Parameter();
param1.parameterDescription = 'Horário de Check-Out';
param1.propertyParameterValue = '14:00';
param1.parameterTypeName = ParameterType.Time;
parameterGroupList.push(param1);

const param2 = new Parameter();
param2.parameterDescription = 'Idade máxima da Criança';
param2.propertyParameterValue = '12';
param2.parameterTypeName = ParameterType.Number;
parameterGroupList.push(param2);

const param3 = new Parameter();
param3.parameterDescription = 'Lançamento de Diárias';
param3.parameterTypeName = ParameterType.Select;
param3.propertyParameterValue = '2';
const paramOptions = new Array<ParameterOption>();
paramOptions.push(new ParameterOption('Check-in', '1'), new ParameterOption('Auditoria', '2'));
param3.propertyParameterPossibleValues = JSON.stringify(paramOptions);

parameterGroupList.push(param3);
