// Models
import { GetAllBillingAccount } from './../models/billing-account/get-all-billing-account';

export const GetAllBillingAccountData = new GetAllBillingAccount();
GetAllBillingAccountData.billingAccountId = '1';
GetAllBillingAccountData.reservationCode = 'UIT1234PT9';
GetAllBillingAccountData.accountTypeName = 'Hóspede';
GetAllBillingAccountData.billingAccountName = 'João Fagundes';
GetAllBillingAccountData.roomTypeName = 'STDC';
GetAllBillingAccountData.roomNumber = '204c';
GetAllBillingAccountData.arrivalDate = new Date();
GetAllBillingAccountData.departureDate = new Date();
GetAllBillingAccountData.balance = -105;

export const GetAllBillingAccountData1 = new GetAllBillingAccount();
GetAllBillingAccountData1.billingAccountId = '2';
GetAllBillingAccountData1.reservationCode = 'PYY1764P08';
GetAllBillingAccountData1.accountTypeName = 'Empresa';
GetAllBillingAccountData1.billingAccountName = 'Túlio Gutzon';
GetAllBillingAccountData1.roomTypeName = 'SUPC';
GetAllBillingAccountData1.roomNumber = '212c';
GetAllBillingAccountData1.arrivalDate = new Date();
GetAllBillingAccountData1.departureDate = new Date();
GetAllBillingAccountData1.balance = 1124;
