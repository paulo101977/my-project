// Models
import { BillingAccountWithAccount } from './../models/billing-account/billing-account-with-account';
import { BillingAccountTypeEnum } from './../models/billing-account/billing-account-type-enum';
import { BillingAccountItemEntryData, BillingAccountItemEntryData2 } from './billing-account-item-entry-data';
import { BillingAccountWithEntry } from './../models/billing-account/billing-account-with-entry';

export const BillingAccountWithEntryData = new BillingAccountWithEntry();
BillingAccountWithEntryData.billingAccountId = '1';
BillingAccountWithEntryData.billingAccountItemName = 'João Mendes';
BillingAccountWithEntryData.isBlocked = false;
BillingAccountWithEntryData.billingAccountItems = [];
BillingAccountWithEntryData.billingAccountItems.push(BillingAccountItemEntryData);
BillingAccountWithEntryData.billingAccountItems.push(BillingAccountItemEntryData2);

export const BillingAccountWithAccountData = new BillingAccountWithAccount();

BillingAccountWithAccountData.mainBillingAccountId = '1';
BillingAccountWithAccountData.holderName = 'João Apolinário';
BillingAccountWithAccountData.isHolder = true;
BillingAccountWithAccountData.arrivalDate = new Date();
BillingAccountWithAccountData.departureDate = new Date();
BillingAccountWithAccountData.accountTypeId = 1;
BillingAccountWithAccountData.accountTypeName = 'Hóspede';
BillingAccountWithAccountData.statusId = 1;
BillingAccountWithAccountData.statusName = 'Aberta';
BillingAccountWithAccountData.accountTypeId = BillingAccountTypeEnum.Guest;
BillingAccountWithAccountData.total = 2000;
BillingAccountWithAccountData.billingAccounts.push(BillingAccountWithEntryData);
BillingAccountWithAccountData.billingAccounts.push(BillingAccountWithEntryData);

export const BillingAccountWithAccountData2 = new BillingAccountWithAccount();

BillingAccountWithAccountData2.mainBillingAccountId = '2';
BillingAccountWithAccountData2.holderName = 'Ricardo Mendes';
BillingAccountWithAccountData2.isHolder = false;
BillingAccountWithAccountData2.arrivalDate = new Date();
BillingAccountWithAccountData2.departureDate = new Date();
BillingAccountWithAccountData2.accountTypeId = 2;
BillingAccountWithAccountData2.accountTypeName = 'Hóspede';
BillingAccountWithAccountData2.statusId = 2;
BillingAccountWithAccountData2.statusName = 'Fechada';
BillingAccountWithAccountData2.accountTypeId = BillingAccountTypeEnum.Guest;
BillingAccountWithAccountData2.total = -100;
BillingAccountWithAccountData2.billingAccounts.push(BillingAccountWithEntryData);
