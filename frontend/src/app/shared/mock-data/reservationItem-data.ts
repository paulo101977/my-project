import { FormBuilder, FormGroup } from '@angular/forms';
import { GuestChildData, GuestAdultData } from './guest-data';
import { BedType } from './../models/bed-type';
import { BedTypeSingleData, BedTypeCribData } from './bed-type-data';
import { ReservationItemView } from './../models/reserves/reservation-item-view';
import { GuestState } from './../models/reserves/guest';
import { Guest } from './../models/reserves/guest';
import { Room } from './../models/room';
import { RoomType } from '../../room-type/shared/models/room-type';
import { ReservationStatusEnum } from './../models/reserves/reservation-status-enum';

export const ReservationItemViewData = new ReservationItemView();

const guest = new Guest();

let reservationItemViewCardForm: FormGroup;
let reservationItemViewCardFormWithAllFieldsSetted: FormGroup;
let reservationItemViewCardFormWithPeriodAndWithoutRoomType: FormGroup;
const guestList = [];
const formBuilder = new FormBuilder();
const roomTypeMock = new RoomType();
const roomTypeMock2 = new RoomType();
const roomMock = new Room();
roomMock.roomNumber = '101';
const roomMock2 = new Room();
roomMock2.roomNumber = '209';

roomTypeMock.id = 1;
roomTypeMock.name = 'Quarto Standard';
roomTypeMock.abbreviation = 'SPDC';
roomTypeMock.order = 1;
roomTypeMock.adultCapacity = 2;
roomTypeMock.childCapacity = 1;
roomTypeMock.roomTypeBedTypeList = new Array<BedType>();
roomTypeMock.roomTypeBedTypeList.push(BedTypeSingleData);
roomTypeMock.roomTypeBedTypeList.push(BedTypeCribData);
roomTypeMock.isActive = true;
roomTypeMock.isInUse = false;
roomTypeMock.roomList = new Array<Room>();
roomTypeMock.roomList.push(roomMock);

roomTypeMock2.id = 2;
roomTypeMock2.name = 'Quarto Luxo';
roomTypeMock2.abbreviation = 'LXO';
roomTypeMock2.order = 1;
roomTypeMock2.adultCapacity = 3;
roomTypeMock2.childCapacity = 0;
roomTypeMock2.roomTypeBedTypeList = new Array<BedType>();
roomTypeMock2.roomTypeBedTypeList.push(BedTypeSingleData);
roomTypeMock2.roomTypeBedTypeList.push(BedTypeCribData);
roomTypeMock2.isActive = true;
roomTypeMock2.isInUse = false;
roomTypeMock2.roomList = new Array<Room>();
roomTypeMock2.roomList.push(roomMock2);

guest.state = GuestState.NoGuest;
guest.formGuest = formBuilder.group({
  guestName: [''],
});
guest.isChild = false;
guest.isMain = true;

guestList.push(guest);

reservationItemViewCardForm = formBuilder.group({
  periodCard: [null],
  adultQuantityCard: [1],
  childrenQuantityCard: [0],
  roomTypeCard: [null],
  checkinHourCard: [''],
  checkoutHourCard: [''],
  roomNumberCard: [''],
  roomLayoutCard: [''],
  extraBedQuantityCard: [''],
  cribBedQuantityCard: [''],
  hasCribBedTypeCard: [false],
  hasExtraBedTypeCard: [false],
});

reservationItemViewCardFormWithAllFieldsSetted = formBuilder.group({
  periodCard: [
    {
      beginJsDate: { year: 2018, month: 10, day: 9 },
      endJsDate: { year: 2018, month: 10, day: 19 },
    },
  ],
  adultQuantityCard: [1],
  childrenQuantityCard: [0],
  roomTypeCard: [roomTypeMock],
  roomTypeRequestedCard: [roomTypeMock],
  checkinHourCard: ['14:00'],
  checkoutHourCard: ['12:00'],
  roomNumberCard: ['1000'],
  roomLayoutCard: [''],
  extraBedQuantityCard: [''],
  cribBedQuantityCard: [''],
  hasCribBedTypeCard: [false],
  hasExtraBedTypeCard: [false],
});

reservationItemViewCardFormWithPeriodAndWithoutRoomType = formBuilder.group({
  periodCard: [
    {
      beginJsDate: { year: 2018, month: 10, day: 9 },
      endJsDate: { year: 2018, month: 10, day: 19 },
    },
  ],
  adultQuantityCard: [1],
  childrenQuantityCard: [0],
  roomTypeCard: [null],
  checkinHourCard: ['14:00'],
  checkoutHourCard: ['12:00'],
  roomNumberCard: ['1000'],
  roomLayoutCard: [''],
  extraBedQuantityCard: [''],
  cribBedQuantityCard: [''],
  hasCribBedTypeCard: [false],
  hasExtraBedTypeCard: [false],
});

ReservationItemViewData.code = 'reservation1';
ReservationItemViewData.room = new Room();
ReservationItemViewData.period = '12/01/2017 - 14/01/2017';
ReservationItemViewData.adultQuantity = 1;
ReservationItemViewData.childrenQuantity = 0;
ReservationItemViewData.status = ReservationStatusEnum.ToConfirm;
ReservationItemViewData.guestQuantity = guestList;
ReservationItemViewData.guestQuantity.push(GuestAdultData);
ReservationItemViewData.guestQuantity.push(GuestChildData);
ReservationItemViewData.receivedRoomType = new RoomType();
ReservationItemViewData.receivedRoomType.id = 1;
ReservationItemViewData.receivedRoomType.name = 'Quarto Standard';
ReservationItemViewData.receivedRoomType.abbreviation = 'SPDC';
ReservationItemViewData.receivedRoomType.order = 1;
ReservationItemViewData.receivedRoomType.adultCapacity = 2;
ReservationItemViewData.receivedRoomType.childCapacity = 1;
ReservationItemViewData.receivedRoomType.roomTypeBedTypeList = new Array<BedType>();
ReservationItemViewData.receivedRoomType.roomTypeBedTypeList.push(BedTypeCribData);
ReservationItemViewData.receivedRoomType.roomTypeBedTypeList.push(BedTypeSingleData);
ReservationItemViewData.receivedRoomType.isActive = true;
ReservationItemViewData.receivedRoomType.isInUse = false;
ReservationItemViewData.receivedRoomType.roomList = new Array<Room>();
ReservationItemViewData.receivedRoomType.roomList.push(roomMock);
ReservationItemViewData.roomTypes = new Array<RoomType>();
ReservationItemViewData.roomTypes.push(roomTypeMock);
ReservationItemViewData.roomTypes.push(roomTypeMock2);
ReservationItemViewData.dailyNumber = 3;
ReservationItemViewData.dailyPrice = 1200.0;
ReservationItemViewData.totalDaily = ReservationItemViewData.dailyPrice * ReservationItemViewData.dailyNumber;
ReservationItemViewData.total = 3;
ReservationItemViewData.room.roomNumber = 'ABCDEFG';
ReservationItemViewData.focus = false;
ReservationItemViewData.schedulePopoverIsVisible = false;
ReservationItemViewData.formCard = reservationItemViewCardFormWithAllFieldsSetted;
ReservationItemViewData.isCanceledStatus = false;

export const ReservationItemViewData2 = new ReservationItemView();

ReservationItemViewData2.code = 'reservation2';
ReservationItemViewData2.room = new Room();
ReservationItemViewData2.period = null;
ReservationItemViewData2.adultQuantity = 1;
ReservationItemViewData2.childrenQuantity = 0;
ReservationItemViewData2.status = ReservationStatusEnum.ToConfirm;
ReservationItemViewData2.guestQuantity = guestList;
ReservationItemViewData2.receivedRoomType = new RoomType();
ReservationItemViewData2.receivedRoomType.id = 1;
ReservationItemViewData2.receivedRoomType.name = 'Quarto Luxo';
ReservationItemViewData2.receivedRoomType.abbreviation = 'LXO';
ReservationItemViewData2.receivedRoomType.order = 1;
ReservationItemViewData2.receivedRoomType.adultCapacity = 3;
ReservationItemViewData2.receivedRoomType.childCapacity = 2;
ReservationItemViewData2.receivedRoomType.roomTypeBedTypeList = new Array<BedType>();
ReservationItemViewData2.receivedRoomType.roomTypeBedTypeList.push(BedTypeCribData);
ReservationItemViewData2.receivedRoomType.roomTypeBedTypeList.push(BedTypeSingleData);
ReservationItemViewData2.receivedRoomType.isActive = true;
ReservationItemViewData2.receivedRoomType.isInUse = false;
ReservationItemViewData2.receivedRoomType.roomList = new Array<Room>();
ReservationItemViewData2.receivedRoomType.roomList.push(roomMock);
ReservationItemViewData2.roomTypes = new Array<RoomType>();
ReservationItemViewData2.roomTypes.push(roomTypeMock);
ReservationItemViewData2.roomTypes.push(roomTypeMock2);
ReservationItemViewData2.dailyNumber = 3;
ReservationItemViewData2.dailyPrice = 100.0;
ReservationItemViewData2.totalDaily = ReservationItemViewData2.dailyPrice * ReservationItemViewData2.dailyNumber;
ReservationItemViewData2.total = 3;
ReservationItemViewData2.room.roomNumber = 'ABC';
ReservationItemViewData2.focus = false;
ReservationItemViewData2.schedulePopoverIsVisible = false;
ReservationItemViewData2.formCard = reservationItemViewCardForm;

export const ReservationItemViewData3 = new ReservationItemView();

ReservationItemViewData3.code = 'reservation3';
ReservationItemViewData3.room = new Room();
ReservationItemViewData3.period = '14/01/2017 - 16/01/2017';
ReservationItemViewData3.adultQuantity = 1;
ReservationItemViewData3.childrenQuantity = 0;
ReservationItemViewData3.status = ReservationStatusEnum.ToConfirm;
ReservationItemViewData3.guestQuantity = guestList;
ReservationItemViewData3.receivedRoomType = new RoomType();
ReservationItemViewData3.receivedRoomType.id = 1;
ReservationItemViewData3.receivedRoomType.name = 'Quarto Luxo';
ReservationItemViewData3.receivedRoomType.abbreviation = 'LXO';
ReservationItemViewData3.receivedRoomType.order = 1;
ReservationItemViewData3.receivedRoomType.adultCapacity = 3;
ReservationItemViewData3.receivedRoomType.childCapacity = 2;
ReservationItemViewData3.receivedRoomType.roomTypeBedTypeList = new Array<BedType>();
ReservationItemViewData3.receivedRoomType.roomTypeBedTypeList.push(BedTypeCribData);
ReservationItemViewData3.receivedRoomType.roomTypeBedTypeList.push(BedTypeSingleData);
ReservationItemViewData3.receivedRoomType.isActive = true;
ReservationItemViewData3.receivedRoomType.isInUse = false;
ReservationItemViewData3.receivedRoomType.roomList = new Array<Room>();
ReservationItemViewData3.receivedRoomType.roomList.push(roomMock);
ReservationItemViewData3.roomTypes = new Array<RoomType>();
ReservationItemViewData3.roomTypes.push(roomTypeMock);
ReservationItemViewData3.roomTypes.push(roomTypeMock2);
ReservationItemViewData3.dailyNumber = 3;
ReservationItemViewData3.dailyPrice = 100.0;
ReservationItemViewData3.totalDaily = ReservationItemViewData3.dailyPrice * ReservationItemViewData3.dailyNumber;
ReservationItemViewData3.total = 3;
ReservationItemViewData3.room.roomNumber = 'ABC';
ReservationItemViewData3.focus = false;
ReservationItemViewData3.schedulePopoverIsVisible = false;
ReservationItemViewData3.formCard = reservationItemViewCardFormWithPeriodAndWithoutRoomType;

export const ReservationItemViewData4 = new ReservationItemView();

ReservationItemViewData4.code = 'reservation3';
ReservationItemViewData4.room = new Room();
ReservationItemViewData4.period = '14/01/2017 - 16/01/2017';
ReservationItemViewData4.adultQuantity = 1;
ReservationItemViewData4.childrenQuantity = 0;
ReservationItemViewData4.status = ReservationStatusEnum.ToConfirm;
ReservationItemViewData4.guestQuantity = guestList;
ReservationItemViewData4.receivedRoomType = new RoomType();
ReservationItemViewData4.receivedRoomType.id = 1;
ReservationItemViewData4.receivedRoomType.name = 'Quarto Luxo';
ReservationItemViewData4.receivedRoomType.abbreviation = 'LXO';
ReservationItemViewData4.receivedRoomType.order = 1;
ReservationItemViewData4.receivedRoomType.adultCapacity = 3;
ReservationItemViewData4.receivedRoomType.childCapacity = 2;
ReservationItemViewData4.receivedRoomType.roomTypeBedTypeList = new Array<BedType>();
ReservationItemViewData4.receivedRoomType.roomTypeBedTypeList.push(BedTypeCribData);
ReservationItemViewData4.receivedRoomType.roomTypeBedTypeList.push(BedTypeSingleData);
ReservationItemViewData4.receivedRoomType.isActive = true;
ReservationItemViewData4.receivedRoomType.isInUse = false;
ReservationItemViewData4.receivedRoomType.roomList = new Array<Room>();
ReservationItemViewData4.receivedRoomType.roomList.push(roomMock);
ReservationItemViewData4.roomTypes = new Array<RoomType>();
ReservationItemViewData4.roomTypes.push(roomTypeMock);
ReservationItemViewData4.roomTypes.push(roomTypeMock2);
ReservationItemViewData4.dailyNumber = 3;
ReservationItemViewData4.dailyPrice = 100.0;
ReservationItemViewData4.totalDaily = ReservationItemViewData4.dailyPrice * ReservationItemViewData4.dailyNumber;
ReservationItemViewData4.total = 3;
ReservationItemViewData4.room.roomNumber = 'ABC';
ReservationItemViewData4.focus = false;
ReservationItemViewData4.schedulePopoverIsVisible = false;
ReservationItemViewData4.formCard = reservationItemViewCardFormWithPeriodAndWithoutRoomType;

export const ReservationItemViewToConfirmData = new ReservationItemView();

ReservationItemViewToConfirmData.id = 10;
ReservationItemViewToConfirmData.status = ReservationStatusEnum.ToConfirm;
ReservationItemViewToConfirmData.code = '001';

export const ReservationItemViewToConfirmIdZeroData = new ReservationItemView();

ReservationItemViewToConfirmIdZeroData.id = 0;
ReservationItemViewToConfirmIdZeroData.status = ReservationStatusEnum.ToConfirm;
ReservationItemViewToConfirmIdZeroData.code = '001';

export const ReservationItemViewCancelData = new ReservationItemView();

ReservationItemViewCancelData.id = 11;
ReservationItemViewCancelData.status = ReservationStatusEnum.Canceled;

export const ReservationItemViewCheckinData = new ReservationItemView();

ReservationItemViewCheckinData.id = 12;
ReservationItemViewCheckinData.status = ReservationStatusEnum.Checkin;

export const ReservationItemViewCanceledData = new ReservationItemView();
ReservationItemViewCanceledData.id = 123;
ReservationItemViewCanceledData.status = ReservationStatusEnum.Canceled;
