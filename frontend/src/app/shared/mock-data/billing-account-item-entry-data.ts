// Models
import { BillingAccountItemEntry } from './../models/billing-account/billing-account-item-entry';

export const BillingAccountItemEntryData = new BillingAccountItemEntry();

BillingAccountItemEntryData.billingAccountItemId = '1';
BillingAccountItemEntryData.billingAccountItemDate = new Date(2018, 1, 22);
BillingAccountItemEntryData.billingAccountItemDate.setHours(18, 40);
BillingAccountItemEntryData.total = 1000;
BillingAccountItemEntryData.billingItemName = 'Pensão';
BillingAccountItemEntryData.billingItemCategoryName = 'Estacionamento';
BillingAccountItemEntryData.billingAccountItemTypeName = 'Débito';

export const BillingAccountItemEntryData2 = new BillingAccountItemEntry();

BillingAccountItemEntryData2.billingAccountItemId = '2';
BillingAccountItemEntryData2.billingAccountItemDate = new Date(2018, 1, 21);
BillingAccountItemEntryData2.billingAccountItemDate.setHours(8, 5);
BillingAccountItemEntryData2.total = -200;
BillingAccountItemEntryData2.billingItemName = 'Pensão';
BillingAccountItemEntryData2.billingItemCategoryName = 'Restaurante';
BillingAccountItemEntryData2.billingAccountItemTypeName = 'Débito';
