import { GuestReservationItem } from './../models/reserves/guest-reservation-item';

export const GuestReservationItemData = new GuestReservationItem();

GuestReservationItemData.guestReservationItemId = 1;
GuestReservationItemData.reservationItemId = 10;
GuestReservationItemData.guestId = 5;
GuestReservationItemData.guestName = 'Caio Régis';
GuestReservationItemData.guestEmail = 'caio.regis@bematech.com.br';
GuestReservationItemData.guestDocument = '212.333.433-22';
GuestReservationItemData.guestDocumentTypeId = 1;
GuestReservationItemData.checkinDate = new Date('2017/10/10');
GuestReservationItemData.checkoutDate = new Date('2017/10/15');
GuestReservationItemData.estimatedArrivalDate = new Date('2017/10/10');
GuestReservationItemData.estimatedDepartureDate = new Date('2017/10/15');
GuestReservationItemData.preferredName = 'Caio';
GuestReservationItemData.guestCitizenship = 'Rio de Janeiro';
GuestReservationItemData.guestLanguage = 'Português';
GuestReservationItemData.pctDailyRate = 0;
GuestReservationItemData.isIncognito = false;
GuestReservationItemData.isChild = false;
GuestReservationItemData.childAge = 27;
GuestReservationItemData.isMain = false;
GuestReservationItemData.guestTypeId = 1;
GuestReservationItemData.guestStatusId = 1;
