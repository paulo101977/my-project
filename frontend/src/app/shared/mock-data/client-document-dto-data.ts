// Models
import { ClientDocumentDto } from '../../shared/models/dto/client/client-document-dto';

export const ClientDocumentDtoData = new ClientDocumentDto();

ClientDocumentDtoData.ownerId = 'meuIdGuidXXX';
ClientDocumentDtoData.documentTypeId = 11;
ClientDocumentDtoData.documentInformation = 'meunumeroNit';
ClientDocumentDtoData.id = 1;
