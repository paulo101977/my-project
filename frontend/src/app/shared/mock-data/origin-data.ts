// Models
import { Origin } from 'app/shared/models/origin';

export const OriginData = new Origin();

OriginData.id = 1;
OriginData.description = 'Booking';
OriginData.isActive = true;

export const OriginIsNotActiveData = new Origin();

OriginData.id = 1;
OriginData.description = 'Decolar.com';
OriginData.isActive = false;
