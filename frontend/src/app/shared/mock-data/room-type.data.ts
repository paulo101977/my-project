// Models
import { RoomTypeView } from './../models/room-type-view';
import { RoomType } from '../../room-type/shared/models/room-type';

export const RoomTypeStandardData = new RoomType();

RoomTypeStandardData.abbreviation = 'STD';
RoomTypeStandardData.name = 'Standard';
RoomTypeStandardData.order = 1;
RoomTypeStandardData.adultCapacity = 1;
RoomTypeStandardData.childCapacity = 1;

export const RoomTypeLuxoData = new RoomType();

RoomTypeLuxoData.id = 1;
RoomTypeLuxoData.abbreviation = 'LXO';
RoomTypeLuxoData.name = 'Luxo';
RoomTypeLuxoData.order = 1;

export const RoomTypeViewStandardData = new RoomTypeView();

RoomTypeViewStandardData.abbreviation = 'STD';
RoomTypeViewStandardData.name = 'Standard';
RoomTypeViewStandardData.order = 1;
RoomTypeViewStandardData.adultCapacity = 1;
RoomTypeViewStandardData.childCapacity = 1;
RoomTypeViewStandardData.isInUse = true;
RoomTypeViewStandardData.isActive = true;

export const RoomTypeViewStandardIsInUseAndNotActiveData = new RoomTypeView();

RoomTypeViewStandardIsInUseAndNotActiveData.abbreviation = 'STD';
RoomTypeViewStandardIsInUseAndNotActiveData.name = 'Standard';
RoomTypeViewStandardIsInUseAndNotActiveData.order = 1;
RoomTypeViewStandardIsInUseAndNotActiveData.adultCapacity = 1;
RoomTypeViewStandardIsInUseAndNotActiveData.childCapacity = 1;
RoomTypeViewStandardIsInUseAndNotActiveData.isInUse = true;
RoomTypeViewStandardIsInUseAndNotActiveData.isActive = false;

export const RoomTypeViewStandardNotInUseDataAndIsActive = new RoomTypeView();

RoomTypeViewStandardNotInUseDataAndIsActive.id = 1;
RoomTypeViewStandardNotInUseDataAndIsActive.abbreviation = 'LXO';
RoomTypeViewStandardNotInUseDataAndIsActive.name = 'Luxo';
RoomTypeViewStandardNotInUseDataAndIsActive.order = 1;
RoomTypeViewStandardNotInUseDataAndIsActive.adultCapacity = 1;
RoomTypeViewStandardNotInUseDataAndIsActive.childCapacity = 1;
RoomTypeViewStandardNotInUseDataAndIsActive.isInUse = false;
RoomTypeViewStandardNotInUseDataAndIsActive.isActive = true;

export const RoomTypeViewStandardNotInUseDataAndIsNotActive = new RoomTypeView();

RoomTypeViewStandardNotInUseDataAndIsNotActive.id = 1;
RoomTypeViewStandardNotInUseDataAndIsNotActive.abbreviation = 'LXO';
RoomTypeViewStandardNotInUseDataAndIsNotActive.name = 'Luxo';
RoomTypeViewStandardNotInUseDataAndIsNotActive.order = 1;
RoomTypeViewStandardNotInUseDataAndIsNotActive.adultCapacity = 1;
RoomTypeViewStandardNotInUseDataAndIsNotActive.childCapacity = 1;
RoomTypeViewStandardNotInUseDataAndIsNotActive.isInUse = false;
RoomTypeViewStandardNotInUseDataAndIsNotActive.isActive = false;
