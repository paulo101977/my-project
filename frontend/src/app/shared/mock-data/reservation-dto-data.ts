// Models
import { GuestReservationItemData } from './guest-reservation-item-data';
import { ReservationItemDto } from './../models/dto/reservation-item-dto';
import { GuestReservationItemDto } from './../models/dto/guest-reservation-item-dto';
import { ReservationDto } from './../models/dto/reservation-dto';

export const ReservationDtoData = new ReservationDto();

export const GuestReservationItemDtoData = new GuestReservationItemDto();

GuestReservationItemDtoData.id = 1;
GuestReservationItemDtoData.reservationItemId = 10;
GuestReservationItemDtoData.guestId = 5;
GuestReservationItemDtoData.guestName = 'Caio Régis';
GuestReservationItemDtoData.guestEmail = 'caio.regis@bematech.com.br';
GuestReservationItemDtoData.guestDocument = '212.333.433-22';
GuestReservationItemDtoData.guestDocumentTypeId = 1;
GuestReservationItemDtoData.checkInDate = new Date('2017/10/10');
GuestReservationItemDtoData.checkOutDate = new Date('2017/10/15');
GuestReservationItemDtoData.estimatedArrivalDate = new Date('2017/10/10');
GuestReservationItemDtoData.estimatedDepartureDate = new Date('2017/10/15');
GuestReservationItemDtoData.preferredName = 'Caio';
GuestReservationItemDtoData.guestCitizenship = 'Rio de Janeiro';
GuestReservationItemDtoData.guestLanguage = 'Português';
GuestReservationItemDtoData.pctDailyRate = 0;
GuestReservationItemDtoData.isIncognito = false;
GuestReservationItemDtoData.isChild = false;
GuestReservationItemDtoData.childAge = 27;
GuestReservationItemDtoData.isMain = false;
GuestReservationItemDtoData.guestTypeId = 1;
GuestReservationItemDtoData.guestStatusId = 1;

export const ReservationItemDtoData = new ReservationItemDto();
ReservationItemDtoData.id = 10;
ReservationItemDtoData.guestReservationItemList.push(GuestReservationItemDtoData);

ReservationDtoData.id = 1;
ReservationDtoData.reservationUid = null;
ReservationDtoData.reservationCode = null;
ReservationDtoData.reservationVendorCode = null;
ReservationDtoData.propertyId = null;
ReservationDtoData.contactId = null;
ReservationDtoData.contactName = null;
ReservationDtoData.contactEmail = null;
ReservationDtoData.contactPhone = null;
ReservationDtoData.internalComments = null;
ReservationDtoData.externalComments = null;
ReservationDtoData.groupName = null;
ReservationDtoData.unifyAccounts = null;
ReservationDtoData.reservationItemList.push(ReservationItemDtoData);
