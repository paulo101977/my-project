// Models
import { IMyDpOptions } from 'mydatepicker';

const today = new Date();
export const IMyDpOptionsData: IMyDpOptions = {
  dateFormat: 'dd/mm/yyyy',
  showClearDateBtn: false,
  height: '44px',
  markCurrentDay: true,
  editableDateField: false,
  indicateInvalidDate: true,
  openSelectorOnInputClick: true,
  disableUntil: {
    year: today.getFullYear(),
    month: today.getMonth() + 1,
    day: today.getDate() - 1,
  },
};

export const IMyDpOptionsClearData: IMyDpOptions = {
  dateFormat: 'dd/mm/yyyy',
  showClearDateBtn: false,
  height: '44px',
  markCurrentDay: true,
  editableDateField: false,
  indicateInvalidDate: true,
  openSelectorOnInputClick: true,
};
