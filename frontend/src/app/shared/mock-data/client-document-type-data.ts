import { ClientDocumentTypesDto } from './../models/dto/client/client-document-types';
import { ClientDocumentTypeEnum } from '../models/dto/client/client-document-types';

const doc1 = new ClientDocumentTypesDto();
doc1.personType = ClientDocumentTypeEnum.Natural;
doc1.countryCode = 'BO';
doc1.name = 'NIT';
doc1.stringFormatMask = null;
doc1.regexValidationExpression = null;
doc1.isMandatory = true;
doc1.id = 11;

const doc2 = new ClientDocumentTypesDto();
doc2.personType = ClientDocumentTypeEnum.Natural;
doc2.countryCode = 'BO';
doc2.name = 'cpf';
doc2.stringFormatMask = null;
doc2.regexValidationExpression = null;
doc2.isMandatory = true;
doc2.id = 12;

const doc3 = new ClientDocumentTypesDto();
doc3.personType = ClientDocumentTypeEnum.Natural;
doc3.countryCode = 'BO';
doc3.name = 'RUT';
doc3.stringFormatMask = null;
doc3.regexValidationExpression = null;
doc3.isMandatory = true;
doc3.id = 13;

const doc4 = new ClientDocumentTypesDto();
doc4.personType = ClientDocumentTypeEnum.Natural;
doc4.countryCode = 'BO';
doc4.name = 'CI';
doc4.stringFormatMask = null;
doc4.regexValidationExpression = null;
doc4.isMandatory = true;
doc4.id = 14;

const doc5 = new ClientDocumentTypesDto();
doc5.personType = ClientDocumentTypeEnum.Legal;
doc5.countryCode = 'BO';
doc5.name = 'CI';
doc5.stringFormatMask = null;
doc5.regexValidationExpression = null;
doc5.isMandatory = true;
doc5.id = 15;

export const documentTypeListData = new Array<ClientDocumentTypesDto>();
documentTypeListData.push(doc1);
documentTypeListData.push(doc2);
documentTypeListData.push(doc3);
documentTypeListData.push(doc4);

export const documentTypeListDataWithLegal = new Array<ClientDocumentTypesDto>();
documentTypeListDataWithLegal.push(doc1);
documentTypeListDataWithLegal.push(doc2);
documentTypeListDataWithLegal.push(doc3);
documentTypeListDataWithLegal.push(doc4);
documentTypeListDataWithLegal.push(doc5);
