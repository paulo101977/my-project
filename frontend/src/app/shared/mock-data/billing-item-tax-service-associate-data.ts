// Models
import { BillingItemTax } from './../models/billingType/billing-item-tax';
import { BillingItemTaxServiceAssociate } from './../models/billingType/billing-item-tax-service-associate';

export const BillingItemTaxServiceAssociateData = new BillingItemTaxServiceAssociate();
BillingItemTaxServiceAssociateData.id = '1';
BillingItemTaxServiceAssociateData.billingItemServiceId = 1;
BillingItemTaxServiceAssociateData.beginDate = new Date();
BillingItemTaxServiceAssociateData.endDate = new Date(new Date().getDate() + 1);
BillingItemTaxServiceAssociateData.isActive = true;
BillingItemTaxServiceAssociateData.taxPercentage = 6;
BillingItemTaxServiceAssociateData.name = 'Imposto sobre Serviço';
BillingItemTaxServiceAssociateData.categoryName = 'Grupo A';

export const BillingItemTaxServiceAssociateData1 = new BillingItemTaxServiceAssociate();
BillingItemTaxServiceAssociateData1.id = '2';
BillingItemTaxServiceAssociateData1.billingItemServiceId = 2;
BillingItemTaxServiceAssociateData1.beginDate = new Date();
BillingItemTaxServiceAssociateData1.endDate = new Date(new Date().getDate() + 4);
BillingItemTaxServiceAssociateData1.isActive = true;
BillingItemTaxServiceAssociateData1.taxPercentage = 9;
BillingItemTaxServiceAssociateData1.name = 'CONFINS Serviços';
BillingItemTaxServiceAssociateData1.categoryName = 'Grupo B';

export const BillingItemTaxServiceAssociateData2 = new BillingItemTaxServiceAssociate();
BillingItemTaxServiceAssociateData2.id = '3';
BillingItemTaxServiceAssociateData2.billingItemServiceId = 3;
BillingItemTaxServiceAssociateData2.beginDate = new Date();
BillingItemTaxServiceAssociateData2.endDate = new Date(new Date().getDate() + 2);
BillingItemTaxServiceAssociateData2.isActive = true;
BillingItemTaxServiceAssociateData2.taxPercentage = 2;
BillingItemTaxServiceAssociateData2.name = 'Imposto sobre circulação de mercadorias e serviços';
BillingItemTaxServiceAssociateData2.categoryName = 'Grupo C';

export const BillingItemTaxServiceAssociateData3 = new BillingItemTaxServiceAssociate();
BillingItemTaxServiceAssociateData3.id = '3';
BillingItemTaxServiceAssociateData3.billingItemServiceId = 4;
BillingItemTaxServiceAssociateData3.beginDate = new Date();
BillingItemTaxServiceAssociateData3.endDate = new Date(new Date().getDate() + 2);
BillingItemTaxServiceAssociateData3.isActive = true;
BillingItemTaxServiceAssociateData3.taxPercentage = 2;
BillingItemTaxServiceAssociateData3.name = 'ICMS';
BillingItemTaxServiceAssociateData3.categoryName = 'Grupo C';
