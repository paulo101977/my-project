import { Governance } from '../models/governance/governance';

const g1 = new Governance();
g1.id = '1';
g1.statusName = 'Limpo';
g1.housekeepingStatusName = 'Limpo';
g1.color = '#1DC29D';
g1.propertyId = 1;
g1.visibleBtnIsActive = true;

const g2 = new Governance();
g2.id = '2';
g2.statusName = 'Sujo';
g2.housekeepingStatusName = 'Sujo';
g2.color = '#FF6969';
g2.propertyId = 1;
g2.visibleBtnIsActive = true;

const g3 = new Governance();
g3.id = '3';
g3.statusName = 'Manutenção';
g3.housekeepingStatusName = 'Manutenção';
g3.color = '#1DC29D';
g3.propertyId = 1;
g3.visibleBtnIsActive = true;

const g4 = new Governance();
g4.id = '4';
g4.statusName = 'Stand By';
g4.housekeepingStatusName = 'Criado pelo usuário';
g4.color = '#F5A623';
g4.propertyId = 1;

export const governanceList = [g1, g2, g3, g4];
