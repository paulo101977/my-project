// Models
import { AvailabilityReservationDto } from '../models/dto/availability-grid/availability-reservation-dto';
import { AvailabilityDto } from '../models/dto/availability-grid/availability-dto';
import { AvailabilityFooterDto } from '../models/dto/availability-grid/availability-footer-dto';
import { AvailabilityResourcesDto } from '../models/dto/availability-grid/availability-resources-dto';
import { AvailabilityRoomTypeDto } from '../models/dto/availability-grid/availability-room-type-dto';
import { AvailabilityRoomsDto } from '../models/dto/availability-grid/availability-rooms-dto';
import { AvailabilityRoomDto } from '../models/dto/availability-grid/availability-room-dto';

export const AvailabilityReservationDtoData = new AvailabilityReservationDto();
AvailabilityReservationDtoData.id = 1;
AvailabilityReservationDtoData.roomId = 103;
AvailabilityReservationDtoData.roomTypeId = 1;
AvailabilityReservationDtoData.reservationId = 1;
AvailabilityReservationDtoData.start = null;
AvailabilityReservationDtoData.end = null;
AvailabilityReservationDtoData.guestName = '';
AvailabilityReservationDtoData.adultCount = 1;
AvailabilityReservationDtoData.childCount = 0;
AvailabilityReservationDtoData.status = 3;
AvailabilityReservationDtoData.isConjugated = false;

export const AvailabilityDtoData = new AvailabilityDto();
AvailabilityDtoData.roomTypeId = 2;
AvailabilityDtoData.date = '20/12/2018';
AvailabilityDtoData.availableRooms = 3;
AvailabilityDtoData.rooms = 4;

export const AvailabilityFooterDtoData = new AvailabilityFooterDto();
AvailabilityFooterDtoData.id = 1;
AvailabilityFooterDtoData.name = 'Total';
AvailabilityFooterDtoData.total = 10;
AvailabilityFooterDtoData.values = [{ date: '21/12/2018', number: 4 }];

export const AvailabilityRoomTypeDtoData = new AvailabilityRoomTypeDto();
AvailabilityRoomTypeDtoData.id = 1;
AvailabilityRoomTypeDtoData.name = 'Standard';
AvailabilityRoomTypeDtoData.total = 6;

export const AvailabilityResourcesDtoData = new AvailabilityResourcesDto();
AvailabilityResourcesDtoData.id = '1';
AvailabilityResourcesDtoData.footerList = [AvailabilityFooterDtoData];
AvailabilityResourcesDtoData.availabilityList = [AvailabilityDtoData];
AvailabilityResourcesDtoData.roomTypeList = [AvailabilityRoomTypeDtoData];

export const AvailabilityRoomDtoData = new AvailabilityRoomDto();
AvailabilityRoomDtoData.id = 1;
AvailabilityRoomDtoData.roomId = 5;
AvailabilityRoomDtoData.name = '105';
AvailabilityRoomDtoData.beds = null;

export const AvailabilityRoomsDtoData = new AvailabilityRoomsDto();
AvailabilityRoomsDtoData.id = '1';
AvailabilityRoomsDtoData.reservationList = [AvailabilityReservationDtoData];
AvailabilityRoomsDtoData.roomList = [AvailabilityRoomDtoData];
