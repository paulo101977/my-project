// Models
import { BillingItemTax } from './../models/billingType/billing-item-tax';
import { BillingItemServiceTaxAssociate } from './../models/billingType/billing-item-service-tax-associate';

export const BillingItemServiceTaxAssociateData = new BillingItemServiceTaxAssociate();
BillingItemServiceTaxAssociateData.id = '295bc26b-a5e4-40ab-b7ee-d5ab2874c4e6';
BillingItemServiceTaxAssociateData.billingItemTaxId = 1;
BillingItemServiceTaxAssociateData.beginDate = new Date();
BillingItemServiceTaxAssociateData.endDate = new Date();
BillingItemServiceTaxAssociateData.endDate.setDate(BillingItemServiceTaxAssociateData.endDate.getDate() + 2);
BillingItemServiceTaxAssociateData.isActive = true;
BillingItemServiceTaxAssociateData.taxPercentage = 6;
BillingItemServiceTaxAssociateData.name = 'Imposto sobre Serviço';
BillingItemServiceTaxAssociateData.categoryName = 'Grupo A';

export const BillingItemServiceTaxAssociateData1 = new BillingItemServiceTaxAssociate();
BillingItemServiceTaxAssociateData1.id = '295bc26b-a5e4-40ab-b7ee-d5ab2874c4e5';
BillingItemServiceTaxAssociateData1.billingItemTaxId = 2;
BillingItemServiceTaxAssociateData1.beginDate = new Date();
BillingItemServiceTaxAssociateData1.endDate = new Date();
BillingItemServiceTaxAssociateData1.endDate.setDate(BillingItemServiceTaxAssociateData1.endDate.getDate() + 10);
BillingItemServiceTaxAssociateData1.isActive = true;
BillingItemServiceTaxAssociateData1.taxPercentage = 9;
BillingItemServiceTaxAssociateData1.name = 'CONFINS Serviços';
BillingItemServiceTaxAssociateData1.categoryName = 'Grupo B';

export const BillingItemServiceTaxAssociateData2 = new BillingItemServiceTaxAssociate();
BillingItemServiceTaxAssociateData2.id = '295bc26b-a5e4-40ab-b7ee-d5ab2874c4e4';
BillingItemServiceTaxAssociateData2.billingItemTaxId = 3;
BillingItemServiceTaxAssociateData2.beginDate = new Date();
BillingItemServiceTaxAssociateData2.endDate = new Date();
BillingItemServiceTaxAssociateData2.endDate.setDate(BillingItemServiceTaxAssociateData2.endDate.getDate() + 15);
BillingItemServiceTaxAssociateData2.isActive = true;
BillingItemServiceTaxAssociateData2.taxPercentage = 2;
BillingItemServiceTaxAssociateData2.name = 'Imposto sobre circulação de mercadorias e serviços';
BillingItemServiceTaxAssociateData2.categoryName = 'Grupo C';
