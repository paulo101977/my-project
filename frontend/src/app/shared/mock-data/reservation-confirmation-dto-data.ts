// Models
import { ReservationConfirmationDto } from './../models/dto/reserve-confirmation-dto';

export const ReservationConfirmationToBilletData = new ReservationConfirmationDto();
ReservationConfirmationToBilletData.paymentTypeId = 6;
ReservationConfirmationToBilletData.billingClientId = '06ccfd8e-ce55-4f56-b433-ddb4e25d9533';
ReservationConfirmationToBilletData.billingClientName = 'Totvs';
ReservationConfirmationToBilletData.ensuresNoShow = true;
