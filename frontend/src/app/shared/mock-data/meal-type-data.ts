import { RateMealPlanType } from 'app/shared/models/revenue-management/rate-meal-plan-type';

export const mealTypeData1: RateMealPlanType = {
  id: '84506554-23f0-4659-b425-017add377e5d',
  mealPlanTypeId: 1,
  mealPlanTypeName: 'Nenhuma',
  propertyId: 5,
  propertyMealPlanTypeCode: 'SAP',
  propertyMealPlanTypeName: 'Nenhuma',
  isActive: true,
  adultMealPlanAmount: 0,
  child1MealPlanAmount: 0,
  child2MealPlanAmount: 0,
  child3MealPlanAmount: 0,
};

export const mealTypeData2: RateMealPlanType = {
  id: '63556cec-e6f4-4d13-b6d6-3a3190945465',
  mealPlanTypeId: 4,
  mealPlanTypeName: 'Meia Pensão Almoço',
  propertyId: 5,
  propertyMealPlanTypeCode: 'MAPA',
  propertyMealPlanTypeName: 'Meia Pensão Almoço',
  isActive: true
};

export const mealTypeListData: RateMealPlanType[] = [mealTypeData1, mealTypeData2];
