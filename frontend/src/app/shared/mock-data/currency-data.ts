import { CurrencyService } from 'app/shared/services/shared/currency-service.service';
import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';


export const currencyServiceStub: Partial<CurrencyService> = {
  getDefaultCurrencySymbol(propertyId: number): string { return 'R$'; }
};

export const currencyPipeStub: Partial<CurrencyFormatPipe> = {
  transform(value: number, currencySign?: string, decimalLength: number = 2, chunkDelimiter: string = '.',
            decimalDelimiter: string = ',', chunkLength: number = 3, showCurrencySign: boolean = true): string {
    return `${value}`;
  }
};
