// Models
import { Chain } from './../models/chain';
import { Brand } from './../models/brand';
import { Contact } from './../models/contact';
import { Location } from './../models/location';
import { DocumentType } from './../models/document-type';
import { DocumentForLegalAndNaturalPerson } from './../models/document';
import { Person } from './../models/person';
import { Company } from './../models/company';
import { Property } from './../models/property';

export const PropertyData = new Property();

const locationMock = new Location();
locationMock.countryCode = 'BR';
locationMock.latitude = -23.020174;
locationMock.longitude = -43.476222;
locationMock.postalCode = 22795;
locationMock.streetName = 'Rua Sérgio Branco Soares';
locationMock.streetNumber = '32';

const contactMock = new Contact();
contactMock.contactType = 2;
contactMock.contactInformation = 'meusite.com.br';

const documentMock = new DocumentForLegalAndNaturalPerson();
documentMock.documentInformation = '32322323233223';
documentMock.documentType = new DocumentType();
documentMock.documentType.personType = 'Legal';
documentMock.documentType.countryCode = 'BR';
documentMock.documentType.name = 'CNPJ';
documentMock.documentType.stringFormatMask = null;
documentMock.documentType.regexValidationExpression = null;
documentMock.documentType.isMandatory = true;
documentMock.documentType.id = 2;

PropertyData.id = 1;
PropertyData.propertyUId = 'da862a40-23a5-4d4a-9230-85e22add2eaa';
PropertyData.name = 'blue stars';
PropertyData.totalOfRooms = 2;
PropertyData.propertyType = 1;
PropertyData.company = new Company();
PropertyData.company.shortName = 'stars';
PropertyData.company.tradeName = 'blue starts company';
PropertyData.company.person = new Person();
PropertyData.company.person.firstName = 'firstName';
PropertyData.company.person.secondName = 'secondName';
PropertyData.company.person.contactList = new Array<Contact>();
PropertyData.company.person.documentList = new Array<DocumentForLegalAndNaturalPerson>();
PropertyData.company.person.documentList.push(documentMock);
PropertyData.brand = new Brand();
PropertyData.brand.name = 'blue';
PropertyData.brand.chain = new Chain();
PropertyData.brand.chain.name = 'name';
PropertyData.brand.chain.id = 100;
PropertyData.brand.id = 1007;
PropertyData.contactList = new Array<Contact>();
PropertyData.contactList.push(contactMock);
PropertyData.locationList = new Array<Location>();
PropertyData.locationList.push(locationMock);
