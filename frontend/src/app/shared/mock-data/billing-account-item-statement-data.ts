// Models
import { BillingAccountItemStatement } from './../models/billing-account/billing-account-item-statement';

export const BillingAccountItemStatementData = new BillingAccountItemStatement();
BillingAccountItemStatementData.billingAccountItemDate = new Date();
BillingAccountItemStatementData.billingAccountItemName = 'Wi-fi';
BillingAccountItemStatementData.amount = 1000;
