// Models
import { Country } from './../models/country';

export const CountryData = new Country();

CountryData.translationId = 1;
CountryData.twoLetterIsoCode = 'BR';
CountryData.countryTwoLetterIsoCode = 'BR';
CountryData.name = 'Brasil';
CountryData.languageIsoCode = null;
CountryData.id = 1;
