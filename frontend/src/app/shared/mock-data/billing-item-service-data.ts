// Models
import { BillingItemService } from './../models/billingType/billing-item-service';
import {
  BillingItemServiceTaxAssociateData,
  BillingItemServiceTaxAssociateData1,
  BillingItemServiceTaxAssociateData2,
} from './billing-item-service-tax-associate-data';

export const BillingItemServiceData = new BillingItemService();
BillingItemServiceData.id = 1;
BillingItemServiceData.isActive = true;
BillingItemServiceData.name = 'Day Use';
BillingItemServiceData.billingItemCategoryId = 21;
BillingItemServiceData.serviceAndTaxList = [BillingItemServiceTaxAssociateData, BillingItemServiceTaxAssociateData1];

export const BillingItemServiceData1 = new BillingItemService();
BillingItemServiceData1.id = 2;
BillingItemServiceData1.isActive = false;
BillingItemServiceData1.name = 'Desconto de frigobar';
BillingItemServiceData1.billingItemCategoryId = 24;
BillingItemServiceData.serviceAndTaxList = [BillingItemServiceTaxAssociateData2];

export const BillingItemServiceData2 = new BillingItemService();
BillingItemServiceData2.id = 3;
BillingItemServiceData2.isActive = true;
BillingItemServiceData2.name = 'Equipamentos';
BillingItemServiceData2.billingItemCategoryId = 75;
BillingItemServiceData.serviceAndTaxList = [BillingItemServiceTaxAssociateData1, BillingItemServiceTaxAssociateData2];
