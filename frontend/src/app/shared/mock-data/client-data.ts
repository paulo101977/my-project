// Models
import { ClientDto } from './../models/dto/client/client-dto';
import { ClientContact } from './../models/clientContact';
import { Client } from './../models/client';
import { GetAllCompanyClientResultDto } from './../models/dto/client/get-all-company-client-result-dto';
import { ClientDocumentTypeEnum } from '../models/dto/client/client-document-types';

export const ClientData = new Client();

ClientData.document = '112212121';
ClientData.id = '21ec6fa3-afd9-4889-9490-a4d50863eeee';
ClientData.shortName = 'Nome Fantasia';
ClientData.tradeName = 'Razão Social';

export const ClientContactData = new ClientContact();

ClientContactData.id = 'd84924e8-bdef-470f-a778-e8b37873bfdf';
ClientContactData.name = 'Ricardo Mendes';
ClientContactData.email = 'comercial@totvs.com.br';
ClientContactData.phoneNumber = '12345678';

export const ClientInfoData = new GetAllCompanyClientResultDto();

ClientInfoData.id = 'd84924e8-bdef-470f-a778-e8b37873bfdf';
ClientInfoData.name = 'Cliente Teste';
ClientInfoData.phone = '21 9999-9999';
ClientInfoData.isActive = true;

export const ClientDtoData = new ClientDto();

ClientDtoData.id = '23';
ClientDtoData.propertyId = 1;
ClientDtoData.personType = ClientDocumentTypeEnum.Legal;
ClientDtoData.shortName = 'Straus';
ClientDtoData.tradeName = 'Lucagin';
ClientDtoData.isActive = true;
ClientDtoData.email = 'lugin@gmail.com';
ClientDtoData.homePage = 'lu.com.lu';
ClientDtoData.phoneNumber = '23455432';
ClientDtoData.cellPhoneNumber = '98765678';
ClientDtoData.locationList = [];
ClientDtoData.documentList = [];
ClientDtoData.companyClientContactPersonList = [];
