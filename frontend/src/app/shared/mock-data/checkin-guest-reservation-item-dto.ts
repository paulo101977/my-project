// Models
import { CheckinGuestReservationItemDto } from './../models/dto/checkin/guest-reservation-item-dto';

export const CheckinGuestReservationItemDtoData = new CheckinGuestReservationItemDto();
CheckinGuestReservationItemDtoData.guestReservationItemId = 1;
export const CheckinGuestReservationItemDtoData2 = new CheckinGuestReservationItemDto();
CheckinGuestReservationItemDtoData2.guestReservationItemId = 2;

export const CheckinGuestReservationItemDtoDataList = new Array<CheckinGuestReservationItemDto>();
CheckinGuestReservationItemDtoDataList.push(CheckinGuestReservationItemDtoData);
CheckinGuestReservationItemDtoDataList.push(CheckinGuestReservationItemDtoData2);
