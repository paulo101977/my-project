// Models
import { BillingItemCategory } from './../models/billingType/billing-item-category';

export const BillingItemCategoryData = new BillingItemCategory();
BillingItemCategoryData.id = 1;
BillingItemCategoryData.propertyId = 1;
BillingItemCategoryData.standardCategoryId = 1;
BillingItemCategoryData.isActive = true;
BillingItemCategoryData.categoryName = 'Eventos';
BillingItemCategoryData.categoryFixedName = 'EV';

export const BillingItemCategoryData1 = new BillingItemCategory();
BillingItemCategoryData1.id = 2;
BillingItemCategoryData1.propertyId = 1;
BillingItemCategoryData1.standardCategoryId = 1;
BillingItemCategoryData1.isActive = true;
BillingItemCategoryData1.categoryName = 'Hospedagem';
BillingItemCategoryData1.categoryFixedName = 'OR';

export const BillingItemCategoryData2 = new BillingItemCategory();
BillingItemCategoryData2.id = 3;
BillingItemCategoryData2.propertyId = 1;
BillingItemCategoryData2.standardCategoryId = 1;
BillingItemCategoryData2.isActive = true;
BillingItemCategoryData2.categoryName = 'Internet';
BillingItemCategoryData2.categoryFixedName = 'TL';

export const BillingItemCategoryInactiveData = new BillingItemCategory();
BillingItemCategoryData2.id = 3;
BillingItemCategoryData2.propertyId = 1;
BillingItemCategoryData2.standardCategoryId = 1;
BillingItemCategoryData2.isActive = false;
BillingItemCategoryData2.categoryName = 'Internet';
BillingItemCategoryData2.categoryFixedName = 'TL';
