import { Parameter } from '../models/parameter';

export const propertyParameterData1: Parameter = {
  applicationParameterId: 7,
  featureGroupId: 3,
  id: '0a18b05c-de28-4d43-b866-f69ca42c2573',
  isActive: true,
  parameterDescription: 'Criança 1',
  parameterTypeName: 'Number',
  propertyId: 5,
  propertyParameterMaxValue: '10',
  propertyParameterMinValue: '0',
  propertyParameterValue: '6',
  propertyParameterPossibleValues: '',
  canInactive: true
};

export const propertyParameterData2: Parameter = {
  applicationParameterId: 8,
  featureGroupId: 3,
  id: '18a8cf05-ec81-465f-9fdb-fe4752e1b00c',
  isActive: true,
  parameterDescription: 'Criança 2',
  parameterTypeName: 'Number',
  propertyId: 5,
  propertyParameterMaxValue: '11',
  propertyParameterMinValue: '7',
  propertyParameterValue: '9',
  propertyParameterPossibleValues: '',
  canInactive: true
};

export const propertyParameterData3: Parameter = {
  applicationParameterId: 9,
  featureGroupId: 3,
  id: 'c7645b0c-88ed-4d54-868d-4eef7307e58a',
  isActive: true,
  parameterDescription: 'Criança 3',
  parameterTypeName: 'Number',
  propertyId: 5,
  propertyParameterMaxValue: '17',
  propertyParameterMinValue: '10',
  propertyParameterValue: '12',
  propertyParameterPossibleValues: '',
  canInactive: true
};

export const propertyParameterDataList: Parameter[] = [
  propertyParameterData1,
  propertyParameterData2,
  propertyParameterData3
];
