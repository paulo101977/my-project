import { User } from '../models/user';

export function getUserData(): User {
  return {
    id: '00000000-0000-0000-0000-000000000000',
    userUid: '',
    personId: '',
    tenantId: '',
    userLogin: '',
    preferredLanguage: 'en-us',
    preferredCulture: '',
    isActive: true,
    isAdmin: true,
    name: 'teste change language',
    email: '',
    lastLink: '',
    roleIdList: [],

    photoUrl: '',
    photoBase64: ''
  };
}
