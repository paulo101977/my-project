// Models
import { BillingAccountSidebar } from './../models/billing-account/billing-account-sidebar';
import { BillingAccountTypeEnum } from './../models/billing-account/billing-account-type-enum';

export const BillingAccountSidebarData = new BillingAccountSidebar();

BillingAccountSidebarData.billingAccountId = '1';
BillingAccountSidebarData.holderName = 'João Apolinário';
BillingAccountSidebarData.isHolder = true;
BillingAccountSidebarData.arrivalDate = new Date(2018, 2, 20);
BillingAccountSidebarData.departureDate = new Date(2018, 2, 22);
BillingAccountSidebarData.price = -200.2;
BillingAccountSidebarData.accountTypeId = BillingAccountTypeEnum.Guest;
BillingAccountSidebarData.isClosed = true;

export const BillingAccountSidebarData2 = new BillingAccountSidebar();

BillingAccountSidebarData2.billingAccountId = '2';
BillingAccountSidebarData2.holderName = 'Ricardo Mendes';
BillingAccountSidebarData2.isHolder = false;
BillingAccountSidebarData2.arrivalDate = new Date(2018, 3, 15);
BillingAccountSidebarData2.departureDate = new Date(2018, 3, 17);
BillingAccountSidebarData2.price = 300.1;
BillingAccountSidebarData2.accountTypeId = BillingAccountTypeEnum.Guest;
BillingAccountSidebarData2.isClosed = false;
