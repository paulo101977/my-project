// Models
import { GetAllBillingItemTax } from './../models/billingType/get-all-billing-item-tax';

export const GetAllBillingItemTaxData = new GetAllBillingItemTax();
GetAllBillingItemTaxData.id = 1;
GetAllBillingItemTaxData.billingItemName = 'Imposto sobre Serviço';
(GetAllBillingItemTaxData.billingItemCategoryName = 'Grupo A'), (GetAllBillingItemTaxData.isActive = true);

export const GetAllBillingItemTaxData1 = new GetAllBillingItemTax();
GetAllBillingItemTaxData1.id = 2;
GetAllBillingItemTaxData1.billingItemName = 'CONFINS Serviços';
(GetAllBillingItemTaxData1.billingItemCategoryName = 'Grupo B'), (GetAllBillingItemTaxData1.isActive = true);

export const GetAllBillingItemTaxData2 = new GetAllBillingItemTax();
GetAllBillingItemTaxData2.id = 3;
GetAllBillingItemTaxData2.billingItemName = 'Imposto sobre circulação de mercadorias e serviços';
(GetAllBillingItemTaxData2.billingItemCategoryName = 'Grupo C'), (GetAllBillingItemTaxData2.isActive = true);

export const GetAllBillingItemTaxInactiveData = new GetAllBillingItemTax();
GetAllBillingItemTaxData.id = 1;
GetAllBillingItemTaxData.billingItemName = 'Nome do Item';
GetAllBillingItemTaxData.billingItemCategoryName = 'Nome da Categoria';
GetAllBillingItemTaxData.isActive = false;
