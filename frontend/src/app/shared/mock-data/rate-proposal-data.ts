export let RateProposalListBlockMock = [
  {
    roomTypeId: 1,
    roomTypeName: 'STD',
    roomTypeOrder: 1,
    rateList: [
      {
        currencyId: 'abc',
        roomTypeId: '1',
        mealPlanTypeId: 2,
        ratePlanId: '1',
        companyClientId: null,
        commercialBudgetName: 'Tarifa base',
        currencySymbol: 'R$',
        roomTypeAbbreviation: 'STD',
        mealPlanTypeName: '1',
        rateVariation: 0,
        totalBudget: 1500,
        order: 1,
        mealPlanTypeDefaultId: 2,
        mealPlanTypeDefaultName: 'Café',
        isMealPlanTypeDefault: true,
        commercialBudgetDayList: []
      },
      {
        currencyId: 'abc',
        roomTypeId: '1',
        mealPlanTypeId: 3,
        ratePlanId: '1',
        companyClientId: null,
        commercialBudgetName: 'Acordo 1',
        currencySymbol: 'R$',
        roomTypeAbbreviation: 'STD',
        mealPlanTypeName: 'Janta',
        rateVariation: 0,
        totalBudget: 3000,
        order: 1,
        mealPlanTypeDefaultId: 2,
        mealPlanTypeDefaultName: 'Café',
        isMealPlanTypeDefault: false,
        commercialBudgetDayList: []
      }
    ]
  },
  {
    roomTypeId: 2,
    roomTypeName: 'LXO',
    roomTypeOrder: 20,
    rateList: [
      {
        currencyId: 'abc',
        roomTypeId: '2',
        mealPlanTypeId: 2,
        ratePlanId: '1',
        companyClientId: null,
        commercialBudgetName: 'Tarifa base',
        currencySymbol: 'R$',
        roomTypeAbbreviation: 'LXO',
        mealPlanTypeName: '1',
        rateVariation: 0,
        totalBudget: 4000,
        order: 20,
        mealPlanTypeDefaultId: 2,
        mealPlanTypeDefaultName: 'Café',
        isMealPlanTypeDefault: true,
        commercialBudgetDayList: []
      },
      {
        currencyId: 'abc',
        roomTypeId: '2',
        mealPlanTypeId: 3,
        ratePlanId: '1',
        companyClientId: null,
        commercialBudgetName: 'Acordo 12',
        currencySymbol: 'R$',
        roomTypeAbbreviation: 'LXO',
        mealPlanTypeName: 'Janta',
        rateVariation: 0,
        totalBudget: 1000,
        order: 20,
        mealPlanTypeDefaultId: 2,
        mealPlanTypeDefaultName: 'Café',
        isMealPlanTypeDefault: false,
        commercialBudgetDayList: []
      }
    ]
  },
  {
    roomTypeId: 3,
    roomTypeName: 'PSD',
    roomTypeOrder: 3,
    rateList: [
      {
        currencyId: 'abc',
        roomTypeId: '3',
        mealPlanTypeId: 2,
        ratePlanId: '1',
        companyClientId: null,
        commercialBudgetName: 'Tarifa base',
        currencySymbol: 'R$',
        roomTypeAbbreviation: 'PSD',
        mealPlanTypeName: '1',
        rateVariation: 0,
        totalBudget: 15000,
        order: 3,
        mealPlanTypeDefaultId: 2,
        mealPlanTypeDefaultName: 'Café',
        isMealPlanTypeDefault: true,
        commercialBudgetDayList: []
      },
      {
        currencyId: 'abc',
        roomTypeId: '3',
        mealPlanTypeId: 3,
        ratePlanId: '1',
        companyClientId: null,
        commercialBudgetName: 'Acordo 1',
        currencySymbol: 'R$',
        roomTypeAbbreviation: 'PSD',
        mealPlanTypeName: 'Janta',
        rateVariation: 0,
        totalBudget: 10000,
        order: 3,
        mealPlanTypeDefaultId: 2,
        mealPlanTypeDefaultName: 'Café',
        isMealPlanTypeDefault: false,
        commercialBudgetDayList: []
      }
    ]
  }
];
