import { ReservationsInAdvance } from 'app/interactive-report/models/reports';

export const reservationsInAdvance1: ReservationsInAdvance = {
  reservationCode: 'SSDSAXZXZ-001',
  billingAccountName: 'Conta de Bebidas',
  arrivalDate: '2019-03-01T00:00:00',
  departureDate: '2019-04-01T00:00:00',
  abbreviation: 'LXO',
  paymentTypeName: 'Diária',
  amountFormatted: 'R$ 1000',
  guestName: 'Caio Régis',
  statusName: 'Checkin',
  uh: '100',
  billingAccountItemDate: '2019-03-01T00:00:00'
};
