// Models
import { ReservationBudget } from './../models/revenue-management/reservation-budget/reservation-budget';

export const ReservationBudgetData1 = new ReservationBudget();

ReservationBudgetData1.budgetDay = new Date();
ReservationBudgetData1.manualRate = 200;

export const ReservationBudgetData2 = new ReservationBudget();

ReservationBudgetData2.budgetDay = new Date();
ReservationBudgetData2.manualRate = 300;

export const ReservationBudgetData3 = new ReservationBudget();

ReservationBudgetData3.budgetDay = new Date();
ReservationBudgetData3.manualRate = 100;

export const ReservationBudgetData4 = new ReservationBudget();

ReservationBudgetData4.budgetDay = new Date();
ReservationBudgetData4.manualRate = 250;
