import { BedType } from './../models/bed-type';

export const BedTypeSingleData = new BedType();

BedTypeSingleData.count = 2;
BedTypeSingleData.bedType = 1;
BedTypeSingleData.optionalLimit = 1;

export const BedTypeCribData = new BedType();

BedTypeCribData.count = 0;
BedTypeCribData.bedType = 5;
BedTypeCribData.optionalLimit = 2;
