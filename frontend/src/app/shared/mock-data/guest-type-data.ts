import { PropertyGuestTypeDto } from 'app/shared/models/dto/property-guest-type-dto';

export const PropertyGuestTypeDtoData = new PropertyGuestTypeDto();
PropertyGuestTypeDtoData.id = 1;
PropertyGuestTypeDtoData.propertyId = 1;
PropertyGuestTypeDtoData.guestTypeName = 'NORMAL';
PropertyGuestTypeDtoData.isVip = false;

export const PropertyGuestTypeDtoData2 = new PropertyGuestTypeDto();
PropertyGuestTypeDtoData2.id = 2;
PropertyGuestTypeDtoData2.propertyId = 1;
PropertyGuestTypeDtoData2.guestTypeName = 'VIP';
PropertyGuestTypeDtoData2.isVip = true;

export const PropertyGuestTypeDtoDataList = new Array<PropertyGuestTypeDto>();
PropertyGuestTypeDtoDataList.push(PropertyGuestTypeDtoData);
PropertyGuestTypeDtoDataList.push(PropertyGuestTypeDtoData2);
