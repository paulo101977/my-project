// Models
import { CompanyClientAccount } from './../models/dto/company-client-account-dto';
import { ReservationStatusEnum } from './../models/reserves/reservation-status-enum';

export const CompanyClientAccountData = new CompanyClientAccount();
CompanyClientAccountData.arrivalDate = new Date();
CompanyClientAccountData.departureDate = new Date();
CompanyClientAccountData.companyClientName = 'Caio Régis';
CompanyClientAccountData.reservationCode = 'YYYO77718';
CompanyClientAccountData.reservationItemStatus = ReservationStatusEnum.ToConfirm;

export const CompanyClientAccountData1 = new CompanyClientAccount();
CompanyClientAccountData1.arrivalDate = new Date();
CompanyClientAccountData1.departureDate = new Date();
CompanyClientAccountData1.companyClientName = 'Caio de Andrade';
CompanyClientAccountData1.reservationCode = 'CXMSWIE43895';
CompanyClientAccountData1.reservationItemStatus = ReservationStatusEnum.ToConfirm;
