// Models
import { ReasonCategoryCancel } from './../models/reasonCategoryCancel';

export const ReasonCategoryCancelData = new ReasonCategoryCancel();

ReasonCategoryCancelData.reasonName = 'Desistência';
ReasonCategoryCancelData.reasonCategoryId = 1;
ReasonCategoryCancelData.chainId = 1;
ReasonCategoryCancelData.isActive = true;
ReasonCategoryCancelData.id = 1;
