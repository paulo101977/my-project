import { TransportationTypeDto } from './../models/dto/checkin/transportation-type-dto';

export const TransportationTypeDtoData = new TransportationTypeDto();
TransportationTypeDtoData.transportationTypeName = 'Avião';
TransportationTypeDtoData.id = 1;

export const TransportationTypeDtoData2 = new TransportationTypeDto();
TransportationTypeDtoData2.transportationTypeName = 'Automóvel';
TransportationTypeDtoData2.id = 2;

export const TransportationTypeDtoData3 = new TransportationTypeDto();
TransportationTypeDtoData3.transportationTypeName = 'Moto';
TransportationTypeDtoData3.id = 3;

export const TransportationTypeDtoDataList = new Array<TransportationTypeDto>();
TransportationTypeDtoDataList.push(TransportationTypeDtoData);
TransportationTypeDtoDataList.push(TransportationTypeDtoData2);
TransportationTypeDtoDataList.push(TransportationTypeDtoData3);
