import { CheckinGuestReservationItemDto } from '../models/dto/checkin/guest-reservation-item-dto';

export const CheckinGuestReservationItemDtoData = new CheckinGuestReservationItemDto();
CheckinGuestReservationItemDtoData.guestReservationItemId = 1;
CheckinGuestReservationItemDtoData.guestReservationItemName = 'Caio Régis';
CheckinGuestReservationItemDtoData.guestRegistrationDocumentBirthDate = new Date('1990/06/27');
