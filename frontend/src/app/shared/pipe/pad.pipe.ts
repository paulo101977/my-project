import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pad',
})
export class PadPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    let valueAux = value + '';
    while (valueAux.length < args) {
      valueAux = '0' + valueAux;
    }
    return valueAux;
  }
}
