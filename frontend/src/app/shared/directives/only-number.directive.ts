import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[only-number]',
})
export class OnlyNumberDirective {
  @HostListener('keypress', ['$event'])
  public keyEvent(event: KeyboardEvent): void {
    const x = event.charCode || event.keyCode;
    if (x >= 48 && x <= 57) {
      return;
    }
    event.preventDefault();
  }
}
