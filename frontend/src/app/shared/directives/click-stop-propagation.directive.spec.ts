import { ClickStopPropagationDirective } from './click-stop-propagation.directive';

describe('ClickStopPropagationDirective', () => {
  const directive = new ClickStopPropagationDirective();

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should call stopPropagation', () => {
    const event = new Event('click');
    spyOn(event, 'stopPropagation');

    directive.onClick(event);

    expect(event.stopPropagation).toHaveBeenCalled();
  });
});
