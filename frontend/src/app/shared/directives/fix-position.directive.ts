import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appFixPosition]',
})
export class FixPositionDirective {
  private OFFSET_TOP_START = 20;
  @Input() alignElementRelativeTo: ElementRef;
  @Input() widht100 = true;
  @Input() zIndexClass = 'thf-u-z-index--10';

  /**
   * Espaço que ficará entre o elemento referencia(caso seja passado) e a própio elemento.
   * Caso nenhum elemento seja passado o espaço não é utilizado
   * @type   {number}
   */
  @Input() spaceBetweenElements = 20;
  private lastScrollPosition = 0;

  constructor(private selfElement: ElementRef, private renderer: Renderer2) {}

  @HostListener('window:scroll', ['$event'])
  public onScroll(event) {
    if (!this.lastScrollPosition) {
      this.lastScrollPosition = this.calculatePositionRelativeDocument(this.selfElement.nativeElement);
    }

    if (
      window.scrollY >= this.calculatePositionRelativeDocument(this.selfElement.nativeElement) &&
      window.scrollY >= this.lastScrollPosition
    ) {
      const computedStyleAlignElement = getComputedStyle(this.selfElement.nativeElement.parentNode);
      const width =
        this.selfElement.nativeElement.parentNode.clientWidth -
        (parseFloat(computedStyleAlignElement.paddingLeft) + parseFloat(computedStyleAlignElement.paddingRight));
      const height = this.selfElement.nativeElement.offsetHeight;

      this.addClasses(this.selfElement, width, height, 0, true);
    } else {
      this.removeClasses(this.selfElement);
    }

    if (this.alignElementRelativeTo) {
      // alinha referente ao elemento informado
      const config = this.getValidWidth(this.alignElementRelativeTo.nativeElement.parentNode);
      const width = config.width - (parseFloat(config.conputedStyle.paddingLeft) + parseFloat(config.conputedStyle.paddingRight));
      const height = this.alignElementRelativeTo.nativeElement.offsetHeight;
      const position = this.selfElement.nativeElement.offsetTop + this.selfElement.nativeElement.offsetHeight + this.spaceBetweenElements;
      if (this.calculatePositionRelativeDocument(this.selfElement.nativeElement) <= 0) {
        this.addClasses(this.alignElementRelativeTo, width, height, position);
      } else {
        this.removeClasses(this.alignElementRelativeTo);
      }
    }
  }

  private removeClasses(element) {
    this.renderer.removeClass(element.nativeElement, 'thf-u-position--fixed');

    if (element != this.alignElementRelativeTo) {
      this.renderer.removeClass(element.nativeElement, this.zIndexClass);
    } else {
      this.renderer.removeClass(element.nativeElement, 'thf-u-z-index--10');
    }

    this.renderer.removeClass(element.nativeElement, 'width-100-discount-menu');
  }

  private addClasses(element, width, height, position: number, width100?: boolean) {
    this.renderer.addClass(element.nativeElement, 'thf-u-position--fixed');
    if (element != this.alignElementRelativeTo) {
      this.renderer.addClass(element.nativeElement, this.zIndexClass);
    } else {
      this.renderer.addClass(element.nativeElement, 'thf-u-z-index--10');
    }
    if (width100) {
      this.renderer.addClass(element.nativeElement, 'width-100-discount-menu');
    } else {
      element.nativeElement.style.width = width + 'px';
      element.nativeElement.style.height = height + 'px';
    }
    element.nativeElement.style.top = position + 'px';
  }

  private calculatePositionRelativeDocument(element) {
    let offsetTop = 0;
    do {
      if (!isNaN(element.offsetTop)) {
        offsetTop += element.offsetTop;
      }
    } while ((element = element.offsetParent));
    return offsetTop;
  }

  private getValidWidth(element) {
    const config = { width: 0, conputedStyle: null };
    do {
      if (element.clientWidth > 0) {
        config.width = element.clientWidth;
        config.conputedStyle = getComputedStyle(element);
        return config;
      }
    } while ((element = element.offsetParent));

    return config;
  }
}
