// Models
import { BillingAccountWithEntry } from '../models/billing-account/billing-account-with-entry';

export interface SharedStore {
  billingAccountWithEntryList: Array<BillingAccountWithEntry>;
}
