import { FormGroup } from '@angular/forms';

export class FnrhWithId {
  id: number;
  fnrh: FormGroup;
}
