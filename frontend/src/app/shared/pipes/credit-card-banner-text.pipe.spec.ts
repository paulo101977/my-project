import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { inject, TestBed } from '@angular/core/testing';
import { CreditCardBannerTextPipe } from './credit-card-banner-text.pipe';

describe('CreditCardBannerTextPipe', () => {
  let creditCardBannerTextPipe: CreditCardBannerTextPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [],
    });
  });

  beforeEach(inject([TranslateService], (translateService: TranslateService) => {
    const translateServiceMock = translateService;
    creditCardBannerTextPipe = new CreditCardBannerTextPipe(translateServiceMock);
  }));

  it('should return "MasterCard"', () => {
    const masterCard = 1;
    expect(creditCardBannerTextPipe.transform(masterCard)).toEqual('commomData.creditCard.banners.masterCard');
  });

  it('should return "Visa"', () => {
    const visa = 2;
    expect(creditCardBannerTextPipe.transform(visa)).toEqual('commomData.creditCard.banners.visa');
  });

  it('should return "Elo"', () => {
    const elo = 3;
    expect(creditCardBannerTextPipe.transform(elo)).toEqual('commomData.creditCard.banners.elo');
  });
});
