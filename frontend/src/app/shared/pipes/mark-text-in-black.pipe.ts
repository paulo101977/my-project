import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'markTextInBlack',
  pure: false,
})
export class MarkTextInBlackPipe implements PipeTransform {
  public transform(item: any, filter: any): string {
    if (!item || !filter) {
      return item;
    }
    return this.applyFilter(item, filter);
  }
  public applyFilter(obj: any, filter: any): string {
    if (typeof obj === 'string') {
      if (obj.toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
        return obj.replace(filter, '<strong>' + filter + '</strong>');
      }
    }
    return obj;
  }
}
