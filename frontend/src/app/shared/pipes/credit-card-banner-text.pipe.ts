import { Pipe, PipeTransform } from '@angular/core';
import { CreditCardBannerEnum } from './../models/credit-card-banner-enum';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'creditCardBannerText',
})
export class CreditCardBannerTextPipe implements PipeTransform {
  constructor(private translateService: TranslateService) {}

  transform(creditCardBannerNumber: CreditCardBannerEnum) {
    if (creditCardBannerNumber === CreditCardBannerEnum.MaterCard) {
      return this.translateService.instant('commomData.creditCard.banners.masterCard');
    }
    if (creditCardBannerNumber === CreditCardBannerEnum.Visa) {
      return this.translateService.instant('commomData.creditCard.banners.visa');
    }
    if (creditCardBannerNumber === CreditCardBannerEnum.Elo) {
      return this.translateService.instant('commomData.creditCard.banners.elo');
    }
  }
}
