// Models
import { ReservationStatusEnum } from './../models/reserves/reservation-status-enum';
import { KeysPipe } from './keys.pipe';

describe('KeysPipe', () => {
  const pipe = new KeysPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return a array with keys e values of Enum', () => {
    const enumExpected = [
      { key: '0', value: 'ToConfirm' },
      { key: '1', value: 'Confirmed' },
      { key: '2', value: 'Checkin' },
      { key: '3', value: 'Checkout' },
      { key: '4', value: 'Pending' },
      { key: '5', value: 'NoShow' },
      { key: '6', value: 'Canceled' },
      { key: '7', value: 'WaitList' },
      { key: '8', value: 'ReservationProposal' },
    ];

    expect(pipe.transform(ReservationStatusEnum)).toEqual(enumExpected);
  });
});
