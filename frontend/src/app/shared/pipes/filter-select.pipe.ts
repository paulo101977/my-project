import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'filterlist',
  pure: false,
})
export class FilterListPipe implements PipeTransform {
  public transform(items: any[], filter: any, propertiesToVerify: Array<any>, parentObjectName: string = null): any[] {
    if (!items || !filter) {
      return items;
    }
    return items.filter((item: any) => this.applyFilter(item, filter, propertiesToVerify, parentObjectName));
  }

  public applyFilter(obj: any, filter: string, propertiesToVerify: Array<any>, parentObjectName: string): boolean {
    if (_.isString(obj)) {
      if (!_.includes(_.toLower(obj), _.toLower(filter))) {
        return false;
      }
    } else {
      // if it's a formGroup get your 'value' property like array
      obj = _.has(obj, 'controls') ? obj.value : obj;
      if (!_.isEmpty(propertiesToVerify)) {
        return this.verifyInPropertyNames(obj, filter, propertiesToVerify, parentObjectName);
      }
    }
    return true;
  }

  private verifyInPropertyNames(obj: any, filter: string, propertiesToVerify: Array<any>, parentObjectName: string): boolean {
    let countNotFound = 0;
    propertiesToVerify.forEach(propertyToVerify => {
      if (parentObjectName && !_.includes(_.toLower(_.toString(obj[parentObjectName][propertyToVerify])), _.toLower(filter))) {
        countNotFound++;
      } else if (!parentObjectName && !_.includes(_.toLower(_.toString(obj[propertyToVerify])), _.toLower(filter))) {
        countNotFound++;
      }
    });

    if (_.eq(countNotFound, propertiesToVerify.length)) {
      return false;
    }
    return true;
  }
}
