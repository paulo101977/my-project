import { TestBed } from '@angular/core/testing';
import { DateService } from 'app/shared/services/shared/date.service';
import { configureTestSuite } from 'ng-bullet';
import { createServiceStub } from '../../../../testing';
import { YearsPipe } from './years.pipe';

describe('YearsPipe', () => {
  const dateProvider = createServiceStub(DateService, {
    calculateAgeByDate(date: Date): number { return 1; }
  });
  let dateService: DateService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [dateProvider]
    });

    dateService = TestBed.get(DateService);
  });

  it('create an instance', () => {
    const pipe = new YearsPipe(dateService);
    expect(pipe).toBeTruthy();
  });
});
