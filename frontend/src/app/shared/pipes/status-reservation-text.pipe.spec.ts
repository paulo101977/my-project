import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { inject, TestBed } from '@angular/core/testing';
import { StatusReservationTextPipe } from './status-reservation-text.pipe';

describe('StatusReservationTextPipe', () => {
  let statusReservationTextPipe: StatusReservationTextPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [],
    });
  });

  beforeEach(inject([TranslateService], (translateService: TranslateService) => {
    const translateServiceMock = translateService;
    statusReservationTextPipe = new StatusReservationTextPipe(translateServiceMock);
  }));

  it('should return "a confirmar"', () => {
    const toConfirm = 0;
    expect(statusReservationTextPipe.transform(toConfirm)).toEqual('commomData.status.toConfirm');
  });

  it('should return "confirmado"', () => {
    const confirmed = 1;
    expect(statusReservationTextPipe.transform(confirmed)).toEqual('commomData.status.confirmed');
  });

  it('should return "checkin"', () => {
    const checkin = 2;
    expect(statusReservationTextPipe.transform(checkin)).toEqual('commomData.status.checkin');
  });

  it('should return "checkout"', () => {
    const checkout = 3;
    expect(statusReservationTextPipe.transform(checkout)).toEqual('commomData.status.checkout');
  });

  it('should return "pending"', () => {
    const pending = 4;
    expect(statusReservationTextPipe.transform(pending)).toEqual('commomData.status.pending');
  });

  it('should return "noShow"', () => {
    const noShow = 5;
    expect(statusReservationTextPipe.transform(noShow)).toEqual('commomData.status.noShow');
  });

  it('should return "canceled"', () => {
    const canceled = 6;
    expect(statusReservationTextPipe.transform(canceled)).toEqual('commomData.status.canceled');
  });

  it('should return "waitList"', () => {
    const waitList = 7;
    expect(statusReservationTextPipe.transform(waitList)).toEqual('commomData.status.waitList');
  });

  it('should return "reservationProposal"', () => {
    const reservationProposal = 8;
    expect(statusReservationTextPipe.transform(reservationProposal)).toEqual('commomData.status.reservationProposal');
  });
});
