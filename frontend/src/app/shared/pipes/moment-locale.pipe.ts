import { PipeTransform } from '@angular/core';
import * as moment from 'moment';

export class MomentLocalePipe implements PipeTransform {
  _locale;

  constructor(locale) {
    this._locale = locale;
  }

  transform(value: Date | moment.Moment | string | number): string {
    return !value ? '' : moment(value).format(this._locale);
  }
}
