import { Pipe, PipeTransform } from '@angular/core';
import { ReservationStatusEnum } from './../models/reserves/reservation-status-enum';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'statusReservationText',
})
export class StatusReservationTextPipe implements PipeTransform {
  constructor(private translateService: TranslateService) {}

  transform(reservationStatusNumber: ReservationStatusEnum) {
    if (reservationStatusNumber === ReservationStatusEnum.ToConfirm) {
      return this.translateService.instant('commomData.status.toConfirm');
    }
    if (reservationStatusNumber === ReservationStatusEnum.Confirmed) {
      return this.translateService.instant('commomData.status.confirmed');
    }
    if (reservationStatusNumber === ReservationStatusEnum.Checkin) {
      return this.translateService.instant('commomData.status.checkin');
    }
    if (reservationStatusNumber === ReservationStatusEnum.Checkout) {
      return this.translateService.instant('commomData.status.checkout');
    }
    if (reservationStatusNumber === ReservationStatusEnum.Pending) {
      return this.translateService.instant('commomData.status.pending');
    }
    if (reservationStatusNumber === ReservationStatusEnum.NoShow) {
      return this.translateService.instant('commomData.status.noShow');
    }
    if (reservationStatusNumber === ReservationStatusEnum.Canceled) {
      return this.translateService.instant('commomData.status.canceled');
    }
    if (reservationStatusNumber === ReservationStatusEnum.WaitList) {
      return this.translateService.instant('commomData.status.waitList');
    }
    if (reservationStatusNumber === ReservationStatusEnum.ReservationProposal) {
      return this.translateService.instant('commomData.status.reservationProposal');
    }
  }
}
