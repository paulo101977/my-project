import { Pipe, PipeTransform } from '@angular/core';
import { DocumentTypeLegalEnum, DocumentTypeNaturalEnum } from '@inovacaocmnet/thx-bifrost';

@Pipe({
  name: 'documentType'
})
export class DocumentTypePipe implements PipeTransform {

  transform(documentTypeId: any, args?: any): any {
    const naturalDocumentType = DocumentTypeNaturalEnum[documentTypeId];
    const legalDocumentType = DocumentTypeLegalEnum[documentTypeId];
    return naturalDocumentType || legalDocumentType;
  }

}
