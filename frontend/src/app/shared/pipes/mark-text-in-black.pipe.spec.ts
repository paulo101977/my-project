// Pipes
import { MarkTextInBlackPipe } from './mark-text-in-black.pipe';

describe('MarkTextInBlackPipe', () => {
  let markTextInBlackPipe: MarkTextInBlackPipe;
  beforeEach(() => {
    markTextInBlackPipe = new MarkTextInBlackPipe();
  });
  it('should return string between <strong></strong> (in black) when substring exists', () => {
    const filter = 'ing';
    const item = 'testing pipe';
    expect(markTextInBlackPipe.transform(item, filter)).toEqual('test<strong>ing</strong> pipe');
  });
  it('should not return string between <strong></strong> (in black) when substring not exists', () => {
    const filter = 'gold';
    const item = 'testing pipe';
    expect(markTextInBlackPipe.transform(item, filter)).toEqual('testing pipe');
  });
});
