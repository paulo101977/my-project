import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'momentCalendar',
})
export class MomentCalendarPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value) {
      return moment(value).calendar();
    }
    return '';
  }
}
