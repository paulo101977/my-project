import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { CurrencyService } from '../services/shared/currency-service.service';

@Pipe({
  name: 'thxCurrency',
})

export class ThxCurrencyPipe extends CurrencyPipe implements PipeTransform {
  constructor(private currencyService: CurrencyService) {
    super('pt-BR');
  }

  transform(value: any, args?: any): any {
    console.warn('deprecate: Use currencyPipe instead');
    const symbolCode = this.currencyService.getCurrencyCode(args ? args : 'pt-br');
    return super.transform(value, symbolCode, true, '1.2-2');
  }
}
