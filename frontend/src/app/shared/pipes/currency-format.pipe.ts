import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Injectable({ providedIn: 'root' })
@Pipe({
  name: 'currencyFormat',
})
// TODO: move to bifrost DEBIT 7317
export class CurrencyFormatPipe implements PipeTransform {
  transform(
    value: number,
    currencySign?: string,
    decimalLength: number = 2,
    chunkDelimiter: string = '.',
    decimalDelimiter: string = ',',
    chunkLength: number = 3,
    showCurrencySign: boolean = true,
  ): string {
    const symbol = currencySign ? currencySign : 'R$ ';
    const result = '\\d(?=(\\d{' + chunkLength + '})+' + (decimalLength > 0 ? '\\D' : '$') + ')';
    if (value == null || isNaN(value)) {
      value = 0;
    }
    const num = value.toFixed(Math.max(0, Math.floor(decimalLength)));

    if (value < 0 && showCurrencySign) {
      return (
        '- ' +
        symbol + ' ' +
        (decimalDelimiter ? num.replace('.', decimalDelimiter).replace('-', '') : num).replace(
          new RegExp(result, 'g'),
          '$&' + chunkDelimiter,
        )
      );
    } else if (value < 0 && !showCurrencySign) {
      return (
        '- ' +
        (decimalDelimiter ? num.replace('.', decimalDelimiter).replace('-', '') : num).replace(
          new RegExp(result, 'g'),
          '$&' + chunkDelimiter,
        )
      );
    } else if (value >= 0 && showCurrencySign) {
      return (
        symbol + ' ' +
        (decimalDelimiter ? num.replace('.', decimalDelimiter).replace('-', '') : num).replace(
          new RegExp(result, 'g'),
          '$&' + chunkDelimiter,
        )
      );
    } else if (value >= 0 && !showCurrencySign) {
      return (decimalDelimiter ? num.replace('.', decimalDelimiter).replace('-', '') : num).replace(
        new RegExp(result, 'g'),
        '$&' + chunkDelimiter,
      );
    }
  }
}
