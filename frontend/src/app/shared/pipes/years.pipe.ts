import { Pipe, PipeTransform } from '@angular/core';
import { DateService } from 'app/shared/services/shared/date.service';

@Pipe({
  name: 'years'
})
export class YearsPipe implements PipeTransform {

  constructor(
    private dateService: DateService
  ) {}

  transform(value: Date | string): any {
    return this.dateService.calculateAgeByDate(value);
  }

}
