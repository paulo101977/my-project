import { tick, async, fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup } from '@angular/forms';
import { FilterListPipe } from './filter-select.pipe';
import { RoomType } from '../../room-type/shared/models/room-type';

describe('FilterListPipe', () => {
  let filterListPipe: FilterListPipe;
  let fieldsToVerify = [];
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(() => {
    filterListPipe = new FilterListPipe();
  });

  it('should return array of string when find "testing pipe"', () => {
    const filter = 'testing pipe';
    const items: string[] = ['testing pipe'];

    expect(filterListPipe.transform(items, filter, fieldsToVerify)).toEqual(items);
  });

  it('should return empty list when find "testing pipe"', () => {
    const filter = 'testing pipe';
    const items: string[] = ['test pipe'];

    expect(filterListPipe.transform(items, filter, fieldsToVerify)).toEqual([]);
  });

  it('should find "abc123" in name property of object from string list', () => {
    const obj: RoomType = new RoomType();
    obj.name = 'abc123';
    const filter = 'abc123';
    fieldsToVerify = ['name'];

    expect(filterListPipe.applyFilter(obj, filter, fieldsToVerify, null)).toBeTruthy();
  });

  it('should find "abc123" in object without parameters from string list', () => {
    const obj: RoomType = new RoomType();
    obj.name = 'abc123';
    const filter = 'abc123';

    expect(filterListPipe.applyFilter(obj, filter, fieldsToVerify, null)).toBeTruthy();
  });

  it('should check if it`s controls', () => {
    const rod = formBuilder.group({
      name: ['Rodrigo'],
      number: 123,
    });
    const filter = 'Rodrigo';
    fieldsToVerify = ['name'];
    expect(filterListPipe.applyFilter(rod, filter, fieldsToVerify, null)).toBeTruthy();
  });

  it('should find "abc123" in array property of object from object list', () => {
    const obj = {
      name: ['abc123'],
    };
    const filter = 'abc123';

    expect(filterListPipe.applyFilter(obj, filter, fieldsToVerify, null)).toBeTruthy();
  });
});
