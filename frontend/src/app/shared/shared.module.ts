import { DocumentTypePipe } from 'app/shared/pipes/document-type.pipe';
import { PersonHeaderMapper } from './mappers/person-header-mapper';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http/';
import { TextMaskModule } from 'angular2-text-mask';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { MomentModule } from 'ngx-moment';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ColorPickerModule } from 'ngx-color-picker';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AngularGooglePlaceModule } from 'ngx-google-places';
import { ProgressModule } from './components/progress/progress.module';
import { FilterSearchComponent } from './components/filter-search/filter-search.component';
import { HeaderPageComponent } from './components/header-page/header-page.component';
import { SubmenuPropertyComponent } from './components/submenu-property/submenu-property.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { BottomBarComponent } from './components/bottom-bar/bottom-bar.component';
import { ToasterComponent } from './components/toaster/toaster.component';
import { ModalWrapperComponent } from './components/modal-wrapper/modal-wrapper.component';
import { ModalComponent } from './components/modal/modal.component';
import { ModalRemarksComponent } from './components/modal-remarks/modal-remarks.component';
import { ModalGroupOptionsComponent } from './components/modal-group-options/modal-group-options.component';
import { ModalAccommodationBudgetComponent } from './components/modal-accommodation-budget/modal-accommodation-budget.component';
import { HeaderListPageComponent } from './components/header-list-page/header-list-page.component';
import { HeaderCreatePageComponent } from './components/header-create-page/header-create-page.component';
import { HeaderEditPageComponent } from './components/header-edit-page/header-edit-page.component';
import { LoadPageComponent } from './components/load-page/load-page.component';
import { AutoCompleteComponent } from './components/input-autocomplete/input-autocomplete.component';
import { PopoverComponent } from './components/popover/popover.component';
import { AppEmptyContentComponent } from './components/app-empty-content/app-empty-content.component';
import { AddressComponent } from './components/address/address.component';
import { SelectComponent } from './components/select/select.component';
import { SubmenuOptionsComponent } from './components/submenu-options/submenu-options.component';
import { TabContentComponent } from './components/tab-content/tab-content.component';
import { HeaderPageNewComponent } from './components/header-page-new/header-page-new.component';
import { LoadPageNewComponent } from './components/load-page/load-page-new.component';
import { GroupContentComponent } from './components/group-content/group-content.component';
import { EmptyContentWrapperComponent } from './components/empty-content-wrapper/empty-content-wrapper.component';
import { GuestFnrhCreateEditComponent } from '../checkin/component-container/guest-fnrh-create-edit/guest-fnrh-create-edit.component';
import { GuestFnrhHeaderComponent } from '../checkin/components/guest-fnrh-header/guest-fnrh-header.component';
import { GuestFnrhSidebarComponent } from '../checkin/components/sidebar/guest-fnrh-sidebar/guest-fnrh-sidebar.component';
import { SidebarScrollFixedComponent } from './components/sidebar-scroll-fixed/sidebar-scroll-fixed/sidebar-scroll-fixed.component';
import { CardGuestFnrhComponent } from './components/card-guest-fnrh/card-guest-fnrh.component';
import { ListCheckboxWithIconComponent } from './components/list/list-checkbox-with-icon.component';
import { ButtonBarComponent } from './components/button-bar/button-bar.component';
import { ButtonIconTextComponent } from './components/button-icon-text/button-icon-text.component';
import { GuestFnrhPopoverRoomtypeComponent } from '../checkin/components/guest-fnrh-popover-roomtype/guest-fnrh-popover-roomtype.component';
import { GuestFnrhPopoverUhNumComponent } from '../checkin/components/guest-fnrh-popover-uh-num/guest-fnrh-popover-uh-num.component';
import { ButtonComponent } from './components/button/button.component';
import { FnrhComponent } from './components/fnrh/fnrh.component';
import { MenuComponent } from './components/menu/menu.component';
import { MenuOptionComponent } from './components/menu-option/menu-option.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { SlimboxComponent } from './components/slimbox/slimbox.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { SlimboxOptionComponent } from './components/slimbox/slimbox-option.component';
import { GroupButtonsComponent } from './components/group-buttons/group-buttons.component';
import { CustomSelectComponent } from './components/custom-select/custom-select.component';
import {
  PrintSingleGuestRegistrationComponent
} from '../checkin/components/print-single-guest-registration/print-single-guest-registration.component';
import {
  PrintAllGuestRegistrationComponent
} from '../checkin/components/print-all-guest-registration/print-all-guest-registration.component';
import { PaymentComponent } from './components/payment/payment.component';
import { PaymentCardComponent } from './components/payment-card/payment-card.component';
import { DatatableComponent } from './components/datatable/datatable.component';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { WeekDayCheckListComponent } from './components/week-day-check-list/week-day-check-list.component';
import { ConfirmModalComponent } from './components/confirm-modal/confirm-modal.component';
import { PrintHtmlContainerComponent } from './components/print-html-container/print-html-container.component';
import { ErrorMsgMaxMinComponent } from './components/input-error-msg/error-msg-max-min.component';
import { HeaderTitleComponent } from './components/header-title/header-title.component';
import { TextButtonComponent } from './components/text-button/text-button.component';
import { IconButtonComponent } from './components/icon-button/icon-button.component';
import { ContentTitleComponent } from './components/content-title/content-title.component';
import { ContentDescriptionComponent } from './components/content-description/content-description.component';
import { FilterListPipe } from './pipes/filter-select.pipe';
import { MarkTextInBlackPipe } from './pipes/mark-text-in-black.pipe';
import { StatusReservationTextPipe } from './pipes/status-reservation-text.pipe';
import { CreditCardBannerTextPipe } from './pipes/credit-card-banner-text.pipe';
import { KeysPipe } from './pipes/keys.pipe';
import { CurrencyFormatPipe } from './pipes/currency-format.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { MomentCalendarPipe } from './pipes/moment-calendar.pipe';
import { PadPipe } from './pipe/pad.pipe';
import { ClickStopPropagationDirective } from './directives/click-stop-propagation.directive';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { FixPositionDirective } from './directives/fix-position.directive';
import { SearchInputComponent } from './components/search-input/search-input.component';
import { DatatableCheckboxComponent } from './components/datatable-checkbox/datatable-checkbox.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { ErrorComponent } from './components/error/error.component';
import { CustomDropdownComponent } from './components/custom-dropdown/custom-dropdown.component';
import { ThxCurrencyPipe } from './pipes/thx-currency.pipe';
import { PropertyResolver } from './resolvers/property.resolver';
import { ScrollPositionResolver } from './resolvers/scroll-position.resolver';
import { DateRangePickerComponent } from './components/date-range-picker/date-range-picker.component';
import { NewAccountModalComponent } from './components/new-account-modal/new-account-modal.component';
import { ModalMessageConfirmComponent } from './components/modal-message-confirm/modal-message-confirm.component';
import { DateSearchComponent } from './components/date-search/date-search.component';
import { PrintRpsComponent } from '../revenue-management/rate-plan/print-rps/print-rps.component';
import { RadioInputModule } from './components/radio-input/radio-input.module';
import { ModalFooterDefaultComponent } from './components/modal-footer-default/modal-footer-default.component';
import { PrintableTableComponent } from './components/printable-table/printable-table.component';
import { CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';
import { CustomCurrencyMaskConfig } from './models/currency-config';
import { StatementThermalPrinterComponent } from './components/statement-thermal-printer/statement-thermal-printer.component';
import { CoreModule } from '@app/core/core.module';
import { ThxImgModule, ThxPopoverModule, ThxSliderButtonsModule } from '@inovacao-cmnet/thx-ui';
import { ImageCropperComponent, ImageCropperModule } from 'ngx-img-cropper';
import { ReservationBudgetComponent } from './components/reservation-budget/reservation-budget.component';
import { ReservationBudgetTableComponent } from './components/reservation-budget-table/reservation-budget-table.component';
import { WeekDayShowComponent } from './components/week-day-show/week-day-show.component';
import { InputLabelComponent } from './components/input-label/input-label.component';
import {
  GuestEntranceCreateEditComponent
} from 'app/checkin/component-container/guest-entrance-create-edit/guest-entrance-create-edit.component';
import {
  CardEntranceGuestFnrhComponent
} from 'app/checkin/components/guest-entrance/card-entrance-guest-fnrh/card-entrance-guest-fnrh.component';
import {
  GuestEntranceFnrhSidebarComponent
} from 'app/checkin/components/guest-entrance/guest-entrance-fnrh-sidebar/guest-entrance-fnrh-sidebar.component';
import {
  GuestFnrhEntranceHeaderComponent
} from 'app/checkin/components/guest-entrance/guest-fnrh-entrance-header/guest-fnrh-entrance-header.component';
import { ModalGuestEntranceComponent } from 'app/checkin/components/guest-entrance/modal-guest-entrance/modal-guest-entrance.component';
import { StatusDotComponent } from './components/status-dot/status-dot.component';
import { ClientDocumentComponent } from 'app/client/components/client-document/client-document.component';
import { GuestPolicyPrintComponent } from 'app/checkin/components/guest-policy-print/guest-policy-print.component';
import { PermissionsModule } from '@thex/security';
import { RoomTypeInfoComponent } from 'app/shared/components/room-type/room-type-info/room-type-info.component';
import { ThxInputV2Module, ThxSelectV2Module } from '@inovacao-cmnet/thx-ui';
import { AddressV2Component } from './components/address-v2/address-v2.component';
import { UserLanguageModule } from './components/user-language-selection/user-language.module';
import { YearsPipe } from './pipes/years.pipe';



@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TextMaskModule,
    CurrencyMaskModule,
    MyDateRangePickerModule,
    AngularGooglePlaceModule,
    MomentModule,
    HttpClientModule,
    MyDatePickerModule,
    NgxDatatableModule,
    ColorPickerModule,
    AutoCompleteModule,
    ProgressModule,
    RadioInputModule,
    CoreModule,
    ThxPopoverModule,
    ThxSliderButtonsModule,
    ImageCropperModule,
    ThxImgModule,
    ThxInputV2Module,
    ThxSelectV2Module,
    ThxInputV2Module,
    PermissionsModule,
    UserLanguageModule,
  ],
  declarations: [
    TopBarComponent,
    SubmenuPropertyComponent,
    BottomBarComponent,
    FilterSearchComponent,
    HeaderPageComponent,
    ToasterComponent,
    ModalComponent,
    ModalRemarksComponent,
    ModalGroupOptionsComponent,
    ModalAccommodationBudgetComponent,
    HeaderListPageComponent,
    HeaderCreatePageComponent,
    HeaderEditPageComponent,
    LoadPageComponent,
    LoadPageNewComponent,
    FilterListPipe,
    AutoCompleteComponent,
    PopoverComponent,
    AppEmptyContentComponent,
    AddressComponent,
    SelectComponent,
    ModalWrapperComponent,
    MarkTextInBlackPipe,
    SubmenuOptionsComponent,
    ClickStopPropagationDirective,
    TabContentComponent,
    StatusReservationTextPipe,
    KeysPipe,
    HeaderPageNewComponent,
    GroupContentComponent,
    CreditCardBannerTextPipe,
    EmptyContentWrapperComponent,
    GuestFnrhCreateEditComponent,
    GuestFnrhHeaderComponent,
    GuestFnrhSidebarComponent,
    GuestEntranceCreateEditComponent,
    SidebarScrollFixedComponent,
    CardGuestFnrhComponent,
    ButtonBarComponent,
    ButtonIconTextComponent,
    GuestFnrhPopoverRoomtypeComponent,
    GuestFnrhPopoverUhNumComponent,
    ButtonComponent,
    FnrhComponent,
    OnlyNumberDirective,
    MenuComponent,
    MenuOptionComponent,
    SearchbarComponent,
    CurrencyFormatPipe,
    DatatableComponent,
    TruncatePipe,
    SlimboxComponent,
    AutocompleteComponent,
    SlimboxOptionComponent,
    ColorPickerComponent,
    GroupButtonsComponent,
    MomentCalendarPipe,
    ListCheckboxWithIconComponent,
    PaymentComponent,
    PaymentCardComponent,
    WeekDayCheckListComponent,
    MomentCalendarPipe,
    PadPipe,
    FixPositionDirective,
    CustomSelectComponent,
    ErrorMsgMaxMinComponent,
    ConfirmModalComponent,
    HeaderTitleComponent,
    TextButtonComponent,
    IconButtonComponent,
    ContentTitleComponent,
    ContentDescriptionComponent,
    SearchInputComponent,
    DatatableCheckboxComponent,
    DatepickerComponent,
    ErrorComponent,
    CustomDropdownComponent,
    ThxCurrencyPipe,
    PrintSingleGuestRegistrationComponent,
    PrintAllGuestRegistrationComponent,
    PrintHtmlContainerComponent,
    PrintRpsComponent,
    DateRangePickerComponent,
    NewAccountModalComponent,
    ModalMessageConfirmComponent,
    ModalFooterDefaultComponent,
    DateSearchComponent,
    PrintableTableComponent,
    StatementThermalPrinterComponent,
    InputLabelComponent,
    CardEntranceGuestFnrhComponent,
    GuestEntranceFnrhSidebarComponent,
    GuestFnrhEntranceHeaderComponent,
    WeekDayShowComponent,
    InputLabelComponent,
    ModalGuestEntranceComponent,
    StatementThermalPrinterComponent,
    ReservationBudgetComponent,
    ReservationBudgetTableComponent,
    StatusDotComponent,
    ClientDocumentComponent,
    GuestPolicyPrintComponent,
    RoomTypeInfoComponent,
    AddressV2Component,
    YearsPipe,
    DocumentTypePipe,
],
  providers: [
    FilterListPipe,
    MomentCalendarPipe,
    CurrencyFormatPipe,
    PropertyResolver,
    ScrollPositionResolver,
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },
    PersonHeaderMapper
  ],
  exports: [
    TopBarComponent,
    SubmenuPropertyComponent,
    BottomBarComponent,
    FilterSearchComponent,
    HeaderPageComponent,
    TranslateModule,
    ToasterComponent,
    ModalComponent,
    ModalRemarksComponent,
    ModalGroupOptionsComponent,
    ModalAccommodationBudgetComponent,
    LoadPageComponent,
    LoadPageNewComponent,
    FilterListPipe,
    AutoCompleteComponent,
    TextMaskModule,
    CurrencyMaskModule,
    PopoverComponent,
    AppEmptyContentComponent,
    AddressComponent,
    SelectComponent,
    ModalWrapperComponent,
    MarkTextInBlackPipe,
    SubmenuOptionsComponent,
    ClickStopPropagationDirective,
    TabContentComponent,
    StatusReservationTextPipe,
    KeysPipe,
    HeaderPageNewComponent,
    GroupContentComponent,
    CreditCardBannerTextPipe,
    MomentModule,
    EmptyContentWrapperComponent,
    GuestFnrhCreateEditComponent,
    GuestFnrhHeaderComponent,
    GuestFnrhSidebarComponent,
    GuestEntranceCreateEditComponent,
    SidebarScrollFixedComponent,
    CardGuestFnrhComponent,
    ButtonBarComponent,
    ButtonIconTextComponent,
    GuestFnrhPopoverRoomtypeComponent,
    GuestFnrhPopoverUhNumComponent,
    ButtonComponent,
    FnrhComponent,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    MenuComponent,
    OnlyNumberDirective,
    MenuOptionComponent,
    SearchbarComponent,
    CurrencyFormatPipe,
    DatatableComponent,
    TruncatePipe,
    SlimboxComponent,
    AutocompleteComponent,
    SlimboxOptionComponent,
    ColorPickerComponent,
    GroupButtonsComponent,
    MomentCalendarPipe,
    PadPipe,
    FixPositionDirective,
    CustomSelectComponent,
    ListCheckboxWithIconComponent,
    PaymentComponent,
    WeekDayCheckListComponent,
    WeekDayShowComponent,
    ConfirmModalComponent,
    HeaderTitleComponent,
    TextButtonComponent,
    IconButtonComponent,
    ContentTitleComponent,
    ContentDescriptionComponent,
    ErrorMsgMaxMinComponent,
    SearchInputComponent,
    DatatableCheckboxComponent,
    ProgressModule,
    MyDatePickerModule,
    MyDateRangePickerModule,
    DatepickerComponent,
    ErrorComponent,
    CustomDropdownComponent,
    ThxCurrencyPipe,
    PrintSingleGuestRegistrationComponent,
    PrintAllGuestRegistrationComponent,
    PrintHtmlContainerComponent,
    DateRangePickerComponent,
    NewAccountModalComponent,
    ModalMessageConfirmComponent,
    DateSearchComponent,
    RadioInputModule,
    ModalFooterDefaultComponent,
    DateSearchComponent,
    PrintableTableComponent,
    StatementThermalPrinterComponent,
    CoreModule,
    ThxPopoverModule,
    ImageCropperComponent,
    InputLabelComponent,
    CardEntranceGuestFnrhComponent,
    GuestEntranceFnrhSidebarComponent,
    GuestFnrhEntranceHeaderComponent,
    ModalGuestEntranceComponent,
    ImageCropperComponent,
    ReservationBudgetComponent,
    ReservationBudgetTableComponent,
    StatusDotComponent,
    ClientDocumentComponent,
    GuestPolicyPrintComponent,
    RoomTypeInfoComponent,
    AddressV2Component,
    YearsPipe,
    DocumentTypePipe,
  ],
  // TODO: Remove CUSTOM_ELEMENTS_SCHEMA DEBT 7141
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [
    PrintSingleGuestRegistrationComponent,
    PrintAllGuestRegistrationComponent,
    PrintHtmlContainerComponent,
    PrintRpsComponent,
    StatementThermalPrinterComponent
  ],
})
export class SharedModule {}
