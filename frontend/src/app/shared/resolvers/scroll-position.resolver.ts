import { ActivatedRouteSnapshot, NavigationEnd, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { filter } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ScrollPositionResolver implements Resolve<any> {

  private needScrollUp = true;
  private history = [];

  constructor(private router: Router,
              private location: PlatformLocation) {
    this.location.onPopState( () => {
      this.needScrollUp = false;
    });
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({urlAfterRedirects}: NavigationEnd) => {
        if (this.history.length == 2) {
          this.history.shift();
        }
        this.history = [...this.history, urlAfterRedirects];
        if (this.getPath(urlAfterRedirects)
          != this.getPath(this.getPreviousUrl())) {
          window.scrollTo(0, 0);
        }
      });
    return null;
  }

  public getPreviousUrl(): string {
    return this.history[this.history.length - 2] || '/index';
  }

  private getPath(url: string) {
    return url.split('?')[0] || url;
  }
}
