import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { SessionParameterService } from '../services/parameter/session-parameter.service';

@Injectable({ providedIn: 'root' })
export class ParameterResolver implements Resolve<any> {
  constructor(private parameterSession: SessionParameterService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const propertyId = route.params.property;
    this.parameterSession.populateSessionParameters(propertyId);
    return null;
  }
}
