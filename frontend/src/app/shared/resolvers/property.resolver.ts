import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { PropertyResource } from '../resources/property/property.resource';
import { PropertyService } from '../services/property/property.service';

@Injectable({ providedIn: 'root' })
export class PropertyResolver implements Resolve<any> {
  constructor(private propertyResource: PropertyResource, private propertyService: PropertyService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const propertyId = route.params.property;
    const propertyStored = this.propertyService.getPropertySession(propertyId);

    if (!propertyStored || propertyStored.id != propertyId) {
      this.propertyResource.getPropertyById(propertyId).subscribe(result => {
        this.propertyService.setPropertySession(result);
      });
    }

    return null;
  }
}
