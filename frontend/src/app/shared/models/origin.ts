export class Origin {
  id: number;
  code: string;
  description: string;
  isActive: boolean;
  propertyId: number;
}
