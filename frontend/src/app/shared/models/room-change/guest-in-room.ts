export class GuestInRoom {
  reservationItemId: number;
  guestName: string;
  checkInDate: any;
  id: number;
  estimatedDepartureDate: any;
  currentBudget: number;
  currencyId: string;
  currencySymbol: string;
}
