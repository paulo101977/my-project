export class RightButtonTop {
  title: string;
  iconDefault: string;
  iconAlt: string;
  callback: any;
}
