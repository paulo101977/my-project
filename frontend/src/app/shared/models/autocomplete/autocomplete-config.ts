export class AutocompleteConfig {
  displayIcon = true;
  dataModel: any;
  resultList: any[];
  callbackToSearch: Function;
  callbackToGetSelectedValue: Function;
  fieldObjectToDisplay: any;
  extraClasses: string;
  placeholder: any;
  forceSelection = false;
}
