export enum AutocompleteDisplayEnum {
  Simple,
  WithIcon,
}

export enum AutocompleteTypeEnum {
  Client,
  ClientContact,
  GuestDocument,
  Email,
  BillingItemService,
}
