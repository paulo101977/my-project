import { LocationDto } from './dto/location-dto';
import { Contact } from './contact';
import { DocumentForLegalAndNaturalPerson } from './document';

export class Person {
  id: string;
  firstName: string;
  secondName: string;
  contactList: Contact[];
  documentList: DocumentForLegalAndNaturalPerson[];
}
