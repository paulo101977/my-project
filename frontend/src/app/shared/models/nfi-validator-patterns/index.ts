export class NfiValidatorPatterns {
  // there are others NIF, how, for example, spanish NIF
  public static readonly PORTUGUESE_NFI_REGEX = /([123568]\d{1}|45|7[0124579]|9[0189])(\d{7})/;
}
