export class ErrorMessageHandlerAPI {
  code: string;
  message: Array<string>;
  detailedMessage: string;
  helpUrl: string;
  details: Array<DetailErrorMessageAPI> = [];

  constructor(error) {
    if (error.status === 400 || error.status === 500) {
      if (error.hasOwnProperty('_body')) {
        Object.assign(this, JSON.parse(error._body));
      } else {
        Object.assign(this, error.error);
      }
      this.getErrorMessage();
    }
  }

  public getErrorMessage() {
    if (!this.details.length) {
      return 'commomData.errorMessage';
    }

    const result = Array.from(new Set(this.details.map(x => x.message)))
      .sort((a, b) => a.localeCompare(b))
      .join('\n');

    return result;
  }
}

class DetailErrorMessageAPI {
  guid: string;
  code: string;
  message: string;
  detailedMessage: string;
}
