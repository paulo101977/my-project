import { TestBed, inject } from '@angular/core/testing';

import { ErrorMessageHandlerAPI } from './error-message-handler-api';

describe('ErrorMessageHandlerAPI', () => {
  it('should Create with _body', () => {
    const error = {
      ok: false,
      status: 400,
      _body:
        '{"code":"Gas.Ncc.NfcArgPber.Zip.Pbagebyyref.GasNccPbagebyyreReebef.3","message":"[App controller on put error]"' +
        ',"detailedMessage":"AppControllerOnPutError","helpUrl":"","details":[{"guid":"4db8f3af-ffca-4d9b-94d5-402fa74ea93e",' +
        '"code":"Ubfcvgnyvgl.Cebcregl.Qbznva.Ragvgvrf.EbbzGlcr+RagvglReebe.7","message":"Já existe um tipo de quarto com a abreviação"' +
        ',"detailedMessage":"RoomTypeShouldNotDuplicateAbreviation"}]}',
    };

    const handler = new ErrorMessageHandlerAPI(error);

    expect(handler.getErrorMessage()).toEqual('Já existe um tipo de quarto com a abreviação');
  });

  it('should Create without _body', () => {
    const error = {
      status: 400,
      error: {
        code: '121221',
        message: '[App controller on put error]',
        detailedMessage: 'AppControllerOnPutError',
        helpUrl: '',
        details: [
          {
            guid: '4db8f3af-ffca-4d9b-94d5-402fa74ea93e',
            code: 'Ubfcvgnyvgl.Cebcregl.Qbznva.Ragvgvrf.EbbzGlcr+RagvglReebe.7',
            message: 'Já existe um tipo de quarto com a abreviação',
            detailedMessage: 'RoomTypeShouldNotDuplicateAbreviation',
          },
        ],
      },
    };

    const handler = new ErrorMessageHandlerAPI(error);

    expect(handler.getErrorMessage()).toEqual('Já existe um tipo de quarto com a abreviação');
  });

  it('on Create with different error. No error messages. Ex 404', () => {
    const error = {
      ok: false,
      status: 404,
      _body: '',
    };
    const handler = new ErrorMessageHandlerAPI(error);
    spyOn(handler, 'getErrorMessage');

    expect(handler.getErrorMessage).not.toHaveBeenCalled();
  });
});
