export class BedType {
  bedType: BedTypeEnum;
  count: number;
  optionalLimit: number;
}

export enum BedTypeEnum {
  Single = 1,
  Double = 2,
  Reversible = 3,
  Extra = 4,
  Crib = 5,
}
