export class Action {
  link: string;
  actionEnum: ActionEnum;
}

export enum ActionEnum {
  add = <any>'addButton',
  edit = <any>'editButton',
  delete = <any>'deleteButton',
  back = <any>'backButton',
}
