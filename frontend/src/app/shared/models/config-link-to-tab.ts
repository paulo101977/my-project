export class ConfigLinkToTab {
  linkText: string;
  linkAction: Function;
  hasLink: boolean;
  hasHeightLimit: boolean;
}
