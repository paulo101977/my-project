export class ReasonCategoryCancel {
  reasonName: string;
  reasonCategoryId: number;
  chainId: number;
  isActive: boolean;
  id: number;
}
