export class GuestAccount {
  billingAccountId: string;
  reservationId: number;
  reservationItemId: number;
  guestReservationItemId: number;
  guestId: number;
  personId: string;
  reservationCode: string;
  guestName: string;
  arrivalDate: Date;
  departureDate: Date;
  reservationItemStatus: number;
  reservationItemStatusName: string;
  isActive: boolean;
}

export class GuestAccountDatatable extends GuestAccount {
  strArrivalDate: string;
  strDepartureDate: string;
  strReservationItemStatus: string;
}
