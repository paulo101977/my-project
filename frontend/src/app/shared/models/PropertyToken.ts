export class PropertyToken {
  authenticated: boolean;
  created: Date;
  expiration: Date;
  newPasswordToken: string;
  id: string;
}
