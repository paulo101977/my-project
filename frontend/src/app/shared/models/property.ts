import { Company } from './company';
import { Brand } from './brand';
import { Location } from './location';
import { Contact } from './contact';
import { DocumentForLegalAndNaturalPerson } from './document';
import { PropertyStatusEnum } from './property-status-enum';

export class Property {
  id: number;
  name: string;
  propertyUId: string;
  propertyType: number;
  brand: Brand;
  brandId: number;
  chainId: number;
  company: Company;
  locationList: Location[];
  contactList: Contact[];
  documentList: DocumentForLegalAndNaturalPerson[];
  totalOfRooms: number;
  cnpj: string;
  inscEst: string;
  inscMun: string;
  contactPhone: string;
  contactWebSite: string;
  shortName: string;
  tradeName: string;
  propertyStatusId: PropertyStatusEnum;
  preferredLanguage: string;
  preferredCulture: string;
  photo?: string;
}
