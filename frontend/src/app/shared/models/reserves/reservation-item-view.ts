import { FormArray, FormGroup } from '@angular/forms';
import { ReservationItem } from './reservation-item';
import { RoomLayout } from './../room-layout';
import { ReservationItemDto } from './../dto/reservation-item-dto';
import { IModelSerialize } from './../../interfaces/model-serialize.interface';
import { Room } from './../room';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { Guest } from './guest';
import { ReservationStatusEnum } from './reservation-status-enum';
import * as moment from 'moment';
import {
  ReservationItemCommercialBudgetConfig
} from '../../../revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';
import { ReservationBudget } from '../revenue-management/reservation-budget/reservation-budget';
import { ReservationItemCommercialBudget } from '../revenue-management/reservation-budget/reservation-item-commercial-budget';
import { ReservationItemCommercialBudgetDay } from '../revenue-management/reservation-budget/reservation-item-commercial-budget-day';
import { SelectObjectOption } from '../selectModel';
import { DateService } from '../../services/shared/date.service';

export class ReservationItemView extends ReservationItem implements IModelSerialize<ReservationItemDto> {
  checkinHour: string;
  checkoutHour: string;
  code: string;
  period: any;
  initialPeriod: any;
  roomQuantity: number;
  adultQuantity: number;
  childrenQuantity: number;
  rooms: Array<Room>;
  roomTypes: Array<RoomType>;
  roomLayout: RoomLayout;
  roomLayoutList: Array<RoomLayout>;
  dailyNumber: number;
  status: ReservationStatusEnum;
  mainGuestName: string;
  total: number;
  guestQuantity: Array<Guest>;
  formCard: FormGroup;
  focus: boolean;
  schedulePopoverIsVisible: boolean;
  childrenPopoverIsVisible: boolean;
  bedsIncludedPopoverIsVisible: boolean;
  extrasBedPopoverIsVisible: boolean;
  childrenListCard: Array<number>;
  reservationItemCommercialBudgetConfig: ReservationItemCommercialBudgetConfig; // após novo orçamento, ficará deprecated
  reservationItemCommercialBudgetConfigChanged: ReservationItemCommercialBudgetConfig; // após novo orçamento, ficará deprecated
  totalDaily: number;
  dailyPrice: number;
  extraBedQuantityList: Array<any>;
  cribBedQuantityList: Array<any>;
  isCanceledStatus: boolean;
  initialReceivedRoomType: RoomType;
  initialRequestedRoomType: RoomType;
  statusColorFooter: object;
  currencyId: string;
  isMigrated: boolean;
  externalReservationNumber: string;
  partnerReservationNumber: string;
  // novo orçamento
  budgetCommercialBudgetList: Array<any>;
  budgetCommercialBudgetItemKey?: any;
  budgetGratuityTypeId?: any;
  budgetReservationBudgetList?: Array<ReservationBudget>;

  constructor() {
    super();
    this.id = 0;
    this.reservationItemUid = '00000000-0000-0000-0000-000000000000';
    this.reservationId = 0;
    this.extraBedCount = 0;
    this.extraCribCount = 0;
    this.guestQuantity = new Array<Guest>();
    this.childrenListCard = new Array<number>();
    this.roomTypes = new Array<RoomType>();
    this.rooms = new Array<Room>();
    this.roomLayoutList = new Array<RoomLayout>();
  }

  public normalizeRequest(): ReservationItemDto {
    this.setCurrencySymbol(this.reservationItemCommercialBudgetConfig.reservationBudgetTableList);
    const reservationItemDto = new ReservationItemDto();
    reservationItemDto.id = this.id;
    reservationItemDto.reservationItemUid = this.reservationItemUid;
    reservationItemDto.reservationId = this.reservationId;
    reservationItemDto.reservationItemCode = this.code;

    let estimatedArrivalDate, estimatedDepartureDate;
    estimatedArrivalDate = this.estimatedArrivalDate && reservationItemDto.reservationItemStatusId < ReservationStatusEnum.Checkin ?
      this.estimatedArrivalDate : this.getEstimatedArrivalDateInPeriod();
    estimatedDepartureDate = this.estimatedDepartureDate && reservationItemDto.reservationItemStatusId < ReservationStatusEnum.Checkin ?
      this.estimatedDepartureDate : this.getEstimatedDepartureDateInPeriod();

    reservationItemDto.estimatedArrivalDate = estimatedArrivalDate;
    reservationItemDto.checkInDate = this.getCheckin();
    reservationItemDto.estimatedDepartureDate = estimatedDepartureDate;
    reservationItemDto.checkOutDate = this.getCheckout();
    reservationItemDto.receivedRoomTypeId = this.getReceivedRoomTypeId();
    reservationItemDto.requestedRoomTypeId = this.getRequestedRoomTypeId();
    reservationItemDto.roomId = this.getRoomId();
    reservationItemDto.adultCount = this.formCard.get('adultQuantityCard').value;
    reservationItemDto.childCount = this.formCard.get('childrenQuantityCard').value;
    reservationItemDto.roomLayoutId = this.formCard.get('roomLayoutCard').value.id;
    reservationItemDto.extraBedCount = this.getExtraBedCount() || 0;
    reservationItemDto.extraCribCount = this.getCribBedCount() || 0;
    reservationItemDto.reservationBudgetList = this.reservationItemCommercialBudgetConfig.reservationBudgetTableList;
    reservationItemDto.reservationItemStatusId = this.status;
    reservationItemDto.gratuityTypeId = this.gratuityTypeId;
    reservationItemDto.currencyId = this.currencyId;
    reservationItemDto.mealPlanTypeId = this.mealPlanTypeId;
    reservationItemDto.ratePlanId = this.ratePlanId;
    reservationItemDto.isMigrated = this.isMigrated;
    reservationItemDto.externalReservationNumber = this.externalReservationNumber;
    reservationItemDto.partnerReservationNumber = this.partnerReservationNumber;


    this.guestQuantity.forEach((guest, index) => {
      if (reservationItemDto.guestReservationItemList
        && reservationItemDto.guestReservationItemList[index]) {
        guest.id = reservationItemDto.guestReservationItemList[index].guestId;
      }
      reservationItemDto.guestReservationItemList.push(guest.normalizeRequest());
    });

    const childrenAgeListFormArray = <FormArray>this.formCard.get('childrenAgeListCard');
    const childGuestList = reservationItemDto.guestReservationItemList.filter(g => g.isChild);

    for (let i = 0; i < childGuestList.length; i++) {
      childGuestList[i].childAge = childrenAgeListFormArray.controls[i].get('childAge').value;
    }
    return reservationItemDto;
  }

  public normalizeResponse(reservationItemDto: ReservationItemDto): void {
    this.id = reservationItemDto.id;
    this.reservationItemUid = reservationItemDto.reservationItemUid;
    this.reservationId = reservationItemDto.reservationId;
    this.code = reservationItemDto.reservationItemCode;
    this.estimatedArrivalDate = reservationItemDto.estimatedArrivalDate;
    this.estimatedDepartureDate = reservationItemDto.estimatedDepartureDate;
    this.setPeriod(reservationItemDto);
    this.setCheckinDate(reservationItemDto.checkInDate ? reservationItemDto.checkInDate : reservationItemDto.estimatedArrivalDate);
    this.setCheckoutDate(reservationItemDto.checkOutDate ? reservationItemDto.checkOutDate : reservationItemDto.estimatedDepartureDate);
    this.receivedRoomType = reservationItemDto.receivedRoomType;
    this.requestedRoomType = reservationItemDto.requestedRoomType;
    this.room = reservationItemDto.room;
    this.roomLayout = reservationItemDto.roomLayout;
    this.initialReceivedRoomType = reservationItemDto.receivedRoomType;
    this.initialRequestedRoomType = reservationItemDto.requestedRoomType;

    this.status = reservationItemDto.reservationItemStatusId;
    this.adultQuantity = reservationItemDto.adultCount;
    this.childrenQuantity = reservationItemDto.childCount;
    this.extraBedCount = reservationItemDto.extraBedCount;
    this.extraCribCount = reservationItemDto.extraCribCount;
    this.gratuityTypeId = reservationItemDto.gratuityTypeId;
    this.currencyId = reservationItemDto.currencyId;
    this.mealPlanTypeId = reservationItemDto.mealPlanTypeId;
    this.ratePlanId = reservationItemDto.ratePlanId;
    this.reservationItemCommercialBudgetConfig = new ReservationItemCommercialBudgetConfig();
    this.reservationItemCommercialBudgetConfig.adultQuantity = this.adultQuantity;
    this.reservationItemCommercialBudgetConfig.childCount = this.childrenQuantity;
    this.reservationItemCommercialBudgetConfig.reservationItemId = this.reservationId;
    this.reservationItemCommercialBudgetConfig.roomTypeId = this.requestedRoomTypeId;
    this.reservationItemCommercialBudgetConfig.wasConfirmed = true;
    this.reservationItemCommercialBudgetConfig.reservationItemCode = this.code;
    this.reservationItemCommercialBudgetConfig.gratuityTypeId = this.gratuityTypeId;
    this.reservationItemCommercialBudgetConfig.isGratuityType = this.gratuityTypeId ? true : false;
    this.reservationItemCommercialBudgetConfig.dateListFromPeriod = this.getDateListFromPeriod(this.period);
    this.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent = new ReservationItemCommercialBudget();
    this.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent.ratePlanId = this.ratePlanId;
    this.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent.currencyId = this.currencyId;
    this.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent.mealPlanTypeId = this.mealPlanTypeId;
    this.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent.commercialBudgetDayList = [];
    this.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent.totalBudget = 0;
    this.reservationItemCommercialBudgetConfig.selectReservationItemCommercialBudgetList = new Array<SelectObjectOption>();

    this.isMigrated = reservationItemDto.isMigrated;
    this.externalReservationNumber = reservationItemDto.externalReservationNumber;
    this.partnerReservationNumber = reservationItemDto.partnerReservationNumber;

    if (reservationItemDto.reservationBudgetList) {
      this.reservationItemCommercialBudgetConfig.reservationBudgetTableList = new Array<ReservationBudget>();
      reservationItemDto.reservationBudgetList.forEach(reservationBudget => {
        this.reservationItemCommercialBudgetConfig.reservationBudgetTableList.push(reservationBudget);
        this.reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent.commercialBudgetDayList.push(
          this.getCommercialBudgetDayByReservationBudget(reservationBudget)
        );
      });
    }

    // Novo Orçamento
    this.budgetReservationBudgetList = JSON.parse(JSON.stringify(this.reservationItemCommercialBudgetConfig.reservationBudgetTableList));
    this.budgetCommercialBudgetItemKey = this.getReservationItemCommercialBudgetIdentifier(reservationItemDto);

    if (reservationItemDto.guestReservationItemList && reservationItemDto.guestReservationItemList.length > 0) {
      reservationItemDto.guestReservationItemList.forEach(guestReservationItemDto => {
        const guest = new Guest();
        guest.normalizeResponse(guestReservationItemDto);
        this.guestQuantity.push(guest);
      });

      this.mainGuestName = reservationItemDto.guestReservationItemList[0].guestName;
    }
  }

  public getReservationItemCommercialBudgetIdentifier(reservationItemCommercialBudget) {
    if (reservationItemCommercialBudget) {
      const {currencyId, mealPlanTypeId, requestedRoomTypeId, ratePlanId} = reservationItemCommercialBudget;
      return {currencyId, mealPlanTypeId, roomTypeId: requestedRoomTypeId, ratePlanId};
    }
    return null;
  }

  private setCurrencySymbol(budgetList: Array<ReservationBudget>) {
    if (budgetList.length > 0) {
      budgetList.forEach(budget => {
        const currencyIsString = typeof budget.currencySymbol === 'string';
        budget.currencySymbol = currencyIsString ? budget.currencySymbol : this.getCurrencySymbol(budget.currencySymbol);
      });
    }
  }

  public getCurrencySymbol(currencyName: any) {
    switch (currencyName && currencyName.title) {
      case 'Real':
        return 'R$';
      case 'Dólar':
        return '$';
      case 'Euro':
        return '€';
      case 'Peso Argentino':
        return '$';
      default:
        return 'R$';
    }
  }

  private getCommercialBudgetDayByReservationBudget(reservationBudget: ReservationBudget) {
    const commercialBudgetDay = new ReservationItemCommercialBudgetDay();
    commercialBudgetDay.day = reservationBudget.budgetDay;
    commercialBudgetDay.baseRateAmount = reservationBudget.baseRate;
    commercialBudgetDay.baseRateWithRatePlanAmount = reservationBudget.agreementRate;
    commercialBudgetDay.mealPlanTypeId = reservationBudget.mealPlanTypeId;
    commercialBudgetDay.currencySymbol = reservationBudget.currencySymbol;
    commercialBudgetDay.currencyId = reservationBudget.currencyId;
    commercialBudgetDay.childRateAmount = reservationBudget.childRate;

    return commercialBudgetDay;
  }

  private getReceivedRoomTypeId(): number {
    const receivedRoomType = this.formCard.get('roomTypeCard').value;
    return receivedRoomType.id;
  }

  private getDateListFromPeriod(period: any) {
    const dateListFromPeriod = this.convertPeriodToDateList(period.beginJsDate, period.endJsDate);
    return dateListFromPeriod;
  }

  private convertPeriodToDateList(dateBegin: any, dateEnd: any) {
    const dateList = new Array<Date>();

    if (dateBegin && dateEnd) {
      const quantityDays = this.diffDays(dateBegin, dateEnd);

      dateList.push(dateBegin);

      for (let day = 1; day < quantityDays; day++) {
        const nextDate = new Date(dateBegin);
        dateList.push(this.addDays(nextDate, day));
      }
    }

    return dateList;
  }

  public diffDays(date1: Date, date2: Date): number {
    let diffDays = 0;

    if (date2 && date1) {
      const timeDiff = Math.abs(date2.getTime() - date1.getTime());
      diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    }
    return diffDays;
  }

  public addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  private getRequestedRoomTypeId(): number {
    let requestedRoomType = this.formCard.get('roomTypeRequestedCard').value;
    if (!requestedRoomType) {
      requestedRoomType = this.formCard.get('roomTypeCard').value;
    }
    return requestedRoomType.id;
  }

  private getRoomId(): number {
    return this.formCard.get('roomNumberCard').value == null || undefined ? null : this.formCard.get('roomNumberCard').value.id;
  }

  private getEstimatedArrivalDateInPeriod(): string {
    const dateService = new DateService();
    const date = dateService
      .convertStringToDate(this.formCard.get('periodCard').value.beginDate, DateService.DATE_FORMAT_UNIVERSAL);
    date.setHours(this.getHourInCheckin(), this.getMinutesInCheckin());
    return dateService.convertDateToStringWithFormat(date, DateService.DATE_FORMAT_UNIVERSAL);
  }

  private getEstimatedDepartureDateInPeriod(): string {
    const dateService = new DateService();
    const date = dateService
      .convertStringToDate(this.formCard.get('periodCard').value.endDate, DateService.DATE_FORMAT_UNIVERSAL);
    date.setHours(this.getHourInCheckout(), this.getMinutesInCheckout());
    return dateService.convertDateToStringWithFormat(date, DateService.DATE_FORMAT_UNIVERSAL);
  }

  private setPeriod(reservationItemDto: ReservationItemDto): void {
    const period: any = {
      beginDate: reservationItemDto.checkInDate ? reservationItemDto.checkInDate : reservationItemDto.estimatedArrivalDate,
      endDate: reservationItemDto.checkOutDate ? reservationItemDto.checkOutDate : reservationItemDto.estimatedDepartureDate
    };
    this.period = period;
    this.initialPeriod = period;
  }

  private getCheckin(): string {
    const checkin: Date = moment(this.getEstimatedArrivalDateInPeriod(), DateService.DATE_FORMAT_UNIVERSAL).toDate();
    checkin.setHours(this.getHourInCheckin(), this.getMinutesInCheckin());
    return new DateService().convertDateToStringWithFormat(checkin, DateService.DATE_FORMAT_UNIVERSAL);
  }

  private setCheckinDate(checkinDate: string): void {
    this.checkinHour = checkinDate
      ? moment(checkinDate, DateService.DATE_FORMAT_UNIVERSAL).format('HH:mm')
      : null;
  }

  private setCheckoutDate(checkoutDate: string): void {
    this.checkoutHour = checkoutDate ? moment(checkoutDate, DateService.DATE_FORMAT_UNIVERSAL).format('HH:mm') : null;
  }

  private getCheckout(): string {
    const checkout: Date = moment(this.getEstimatedDepartureDateInPeriod(), DateService.DATE_FORMAT_UNIVERSAL).toDate();
    checkout.setHours(this.getHourInCheckout(), this.getMinutesInCheckout());
    return new DateService().convertDateToStringWithFormat(checkout, DateService.DATE_FORMAT_UNIVERSAL);
  }

  private getHourInCheckin(): number {
    const hourAndMinutes = this.formCard.get('checkinHourCard').value;
    return hourAndMinutes.substring(0, 2);
  }

  private getMinutesInCheckin(): number {
    const hourAndMinutes = this.formCard.get('checkinHourCard').value;
    return hourAndMinutes.substring(3, 5);
  }

  private getHourInCheckout(): number {
    const hourAndMinutes = this.formCard.get('checkoutHourCard').value;
    return hourAndMinutes.substring(0, 2);
  }

  private getMinutesInCheckout(): number {
    const hourAndMinutes = this.formCard.get('checkoutHourCard').value;
    return hourAndMinutes.substring(3, 5);
  }

  private getExtraBedCount(): number {
    return this.formCard.get('extraBedQuantityCard').value == null || undefined || '' ? 0 : this.formCard.get('extraBedQuantityCard').value;
  }

  private getCribBedCount(): number {
    return this.formCard.get('cribBedQuantityCard').value == null || undefined || '' ? 0 : this.formCard.get('cribBedQuantityCard').value;
  }
}

export enum ReservationItemViewStatusEnum {
  Draft = <any>'-',
  Checkin = <any>'checkin',
  Checkout = <any>'checkout',
}
