export enum PaymentTypeEnum {
  InternalUsage = 1,
  Deposit = 2,
  Courtesy = 3,
  Hotel = 4,
  CreditCard = 5,
  ToBeBilled = 6,
  Check = 7,
  Money = 8,
  Debit = 9,
}
