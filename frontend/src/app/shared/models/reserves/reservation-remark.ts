export class ReservationRemark {
  internalComments: string;
  externalComments: string;
  partnerComments: string;

  constructor() {
    this.internalComments = '';
    this.externalComments = '';
    this.partnerComments = '';
  }
}
