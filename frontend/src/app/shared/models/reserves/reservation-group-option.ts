export class ReservationGroupOption {
  groupName: string;
  unifyAccounts: boolean;

  constructor() {
    this.groupName = '';
    this.unifyAccounts = false;
  }
}
