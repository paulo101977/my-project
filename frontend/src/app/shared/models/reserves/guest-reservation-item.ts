import { IModelSerialize } from './../../interfaces/model-serialize.interface';
import { GuestReservationItemDto } from './../dto/guest-reservation-item-dto';

export class GuestReservationItem implements IModelSerialize<GuestReservationItemDto> {
  guestReservationItemId: number;
  reservationItemId: number;
  guestId: number;
  personId: string;
  guestName: string;
  guestEmail: string;
  guestDocument: string;
  guestDocumentTypeId: number;
  checkinDate: Date;
  checkoutDate: Date;
  estimatedArrivalDate: Date;
  estimatedDepartureDate: Date;
  preferredName: string;
  guestCitizenship: string;
  guestLanguage: string;
  pctDailyRate: number;
  isIncognito: boolean;
  isChild: boolean;
  childAge: number;
  isMain: boolean;
  guestTypeId: number;
  guestStatusId: number;
  completeAddress: string;

  public normalizeRequest(): GuestReservationItemDto {
    const guestReservationItemDto = new GuestReservationItemDto();

    guestReservationItemDto.id = this.guestReservationItemId;
    guestReservationItemDto.reservationItemId = this.reservationItemId;
    guestReservationItemDto.guestId = this.guestId;
    guestReservationItemDto.guestName = this.guestName;
    guestReservationItemDto.personId = this.personId;
    guestReservationItemDto.guestEmail = this.guestEmail;
    guestReservationItemDto.guestDocumentTypeId = this.guestDocumentTypeId;
    guestReservationItemDto.guestDocument = this.guestDocument;
    guestReservationItemDto.estimatedArrivalDate = this.estimatedArrivalDate;
    guestReservationItemDto.checkInDate = this.checkinDate;
    guestReservationItemDto.estimatedDepartureDate = this.estimatedDepartureDate;
    guestReservationItemDto.checkOutDate = this.checkoutDate;
    guestReservationItemDto.guestStatusId = this.guestStatusId;
    guestReservationItemDto.guestTypeId = this.guestTypeId;
    guestReservationItemDto.preferredName = this.preferredName;
    guestReservationItemDto.guestCitizenship = this.guestCitizenship;
    guestReservationItemDto.guestLanguage = this.guestLanguage;
    guestReservationItemDto.pctDailyRate = this.pctDailyRate;
    guestReservationItemDto.isIncognito = this.isIncognito;
    guestReservationItemDto.isChild = this.isChild;
    guestReservationItemDto.childAge = this.childAge;
    guestReservationItemDto.isMain = this.isMain;

    return guestReservationItemDto;
  }

  public normalizeResponse(guestReservationItemDto: GuestReservationItemDto): void {
    this.guestReservationItemId = guestReservationItemDto.id;
    this.reservationItemId = guestReservationItemDto.reservationItemId;
    this.guestId = guestReservationItemDto.guestId;
    this.personId = guestReservationItemDto.personId;
    this.guestName = guestReservationItemDto.guestName;
    this.guestEmail = guestReservationItemDto.guestEmail;
    this.guestDocument = guestReservationItemDto.guestDocument;
    this.checkinDate = guestReservationItemDto.checkInDate;
    this.checkoutDate = guestReservationItemDto.checkOutDate;
    this.isChild = guestReservationItemDto.isChild;
    this.isMain = guestReservationItemDto.isMain;
    this.estimatedArrivalDate = guestReservationItemDto.estimatedArrivalDate;
    this.estimatedDepartureDate = guestReservationItemDto.estimatedDepartureDate;
    this.preferredName = guestReservationItemDto.preferredName;
    this.guestCitizenship = guestReservationItemDto.guestCitizenship;
    this.guestLanguage = guestReservationItemDto.guestLanguage;
    this.pctDailyRate = guestReservationItemDto.pctDailyRate;
    this.isIncognito = guestReservationItemDto.isIncognito;
    this.childAge = guestReservationItemDto.childAge;
    this.guestTypeId = guestReservationItemDto.guestTypeId;
    this.guestStatusId = guestReservationItemDto.guestStatusId;

    this.completeAddress = this.getCompleteAddress(guestReservationItemDto);
  }

  private getCompleteAddress(guestReservationItemDto: GuestReservationItemDto): string {
    if (guestReservationItemDto.guest && guestReservationItemDto.guest.person) {
      if (guestReservationItemDto.guest.person.locationList && guestReservationItemDto.guest.person.locationList.length > 0) {
        return guestReservationItemDto.guest.person.locationList[0].completeAddress;
      }
    }
    return '';
  }
}
