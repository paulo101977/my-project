import { FormGroup, FormBuilder } from '@angular/forms';
import { GuestReservationItemDto } from './../dto/guest-reservation-item-dto';
import { IModelSerialize } from './../../interfaces/model-serialize.interface';
import { Contact } from './../contact';
import { Room } from './../room';
import { GuestDto } from './../dto/guest-dto';

export enum GuestState {
  NoGuest = <any>'noGuest',
  CreatingGuest = <any>'creatingGuest',
  GuestCreated = <any>'guestCreated',
}

export enum GuestStatus {
  ToConfirm = 0,
  Confirmed = 1,
  Checkin = 2,
  Checkout = 3,
  Pending = 4,
  NoShow = 5,
  Canceled = 6,
  WaitList = 7,
  ReservationProposal = 8,
}


export class Guest implements IModelSerialize<GuestReservationItemDto> {
  id: number;
  personId: string;
  guestId: number;
  fullName: string;
  childAge: number;
  email: string;
  CPF: string;
  type: string;
  address: string;
  incognito: boolean;
  nationality: string;
  language: string;
  state: GuestState;
  status: GuestStatus;
  formGuest: FormGroup;
  uHPreferably: Room;
  lastUH: Room;
  lastDailyValue: number;
  periodofStay: Date;
  dailyPercent: number;
  remarks: string;
  document: Array<Document>;
  documentInformation: string;
  contact: Array<Contact>;
  isChild: boolean;
  isMain: boolean;
  isDisabledGuest: boolean;
  showAutocompleItemsGuests: boolean;
  quantityClicksOutInputAndAutocomplete: number;

  constructor() {
    this.id = 0;
    this.personId = null;
    this.childAge = null;
    this.fullName = '';
    this.showAutocompleItemsGuests = false;
    this.quantityClicksOutInputAndAutocomplete = 0;
    this.isChild = false;
    this.isMain = false;
  }

  public normalizeRequest(): GuestReservationItemDto {
    const guestReservationItemDto = new GuestReservationItemDto();
    guestReservationItemDto.id = this.id;
    guestReservationItemDto.guestId = this.guestId;
    guestReservationItemDto.personId = this.personId;
    guestReservationItemDto.isChild = this.isChild;
    guestReservationItemDto.isMain = this.isMain;
    guestReservationItemDto.guestName = this.guestName();
    guestReservationItemDto.guest = new GuestDto();
    guestReservationItemDto.guest.guestId = this.guestId;
    guestReservationItemDto.guest.personId = this.personId;
    return guestReservationItemDto;
  }

  public normalizeResponse(guestReservationItemDto: GuestReservationItemDto): void {
    this.formGuest = new FormBuilder().group({
      guestName: [guestReservationItemDto.guestName],
    });
    this.id = guestReservationItemDto.id;
    this.personId = guestReservationItemDto.personId;
    this.isChild = guestReservationItemDto.isChild;
    this.isMain = guestReservationItemDto.isMain;
    this.childAge = guestReservationItemDto.childAge;
    this.guestId = guestReservationItemDto.guestId;
    if (this.guestName() == '' || null || undefined) {
      this.state = GuestState.NoGuest;
    } else {
      this.state = GuestState.GuestCreated;
    }
    this.status = guestReservationItemDto.guestStatusId;

  }

  private guestName(): string {
    return this.formGuest.get('guestName').value;
  }
}
