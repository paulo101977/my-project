import { ClientDto } from './../../dto/client/client-dto';
import { RoomTypeLuxoData } from './../../../mock-data/room-type.data';
import { RoomLayout } from './../../room-layout';
import { Room } from './../../room';
import { ReservationItemDto } from './../../dto/reservation-item-dto';
import { ReservationDto } from './../../dto/reservation-dto';
import { Reservation } from './../reservation';
import { DateService } from '../../../services/shared/date.service';
import * as moment from 'moment';
import { async, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpHandler } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';
describe('Reservation', () => {
  const reservation = new Reservation();

  // necessary http handler to work
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [],
      providers: [ HttpHandler, InterceptorHandlerService ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  it('should normalizeRequest', () => {
    reservation.id = 1;
    reservation.reservationUid = '';
    reservation.reservationCode = '';
    reservation.reservationVendorCode = '';
    reservation.propertyId = 1;
    reservation.contactName = 'Abc';
    reservation.contactEmail = 'abc@email.com';
    reservation.contactPhone = '12233333';
    reservation.internalComments = 'Comentário Interno';
    reservation.externalComments = 'Comentário Externo';
    reservation.groupName = 'Grupo A';
    reservation.unifyAccounts = true;
    reservation.client.id = '12222323233aaa';
    reservation.clientContact.id = '00222323233aaa';

    const reservationDto = reservation.normalizeRequest();
    expect(reservationDto.id).toEqual(reservation.id);
    expect(reservationDto.reservationUid).toEqual(reservation.reservationUid);
    expect(reservationDto.reservationCode).toEqual(reservation.reservationCode);
    expect(reservationDto.propertyId).toEqual(reservation.propertyId);
    expect(reservationDto.contactName).toEqual(reservation.contactName);
    expect(reservationDto.contactEmail).toEqual(reservation.contactEmail);
    expect(reservationDto.contactPhone).toEqual(reservation.contactPhone);
    expect(reservationDto.internalComments).toEqual(reservation.internalComments);
    expect(reservationDto.externalComments).toEqual(reservation.externalComments);
    expect(reservationDto.groupName).toEqual(reservation.groupName);
    expect(reservationDto.unifyAccounts).toEqual(reservation.unifyAccounts);
    expect(reservationDto.companyClientId).toEqual(reservation.client.id);
    expect(reservationDto.companyClientContactPersonId).toEqual(reservation.clientContact.id);
    expect(reservationDto.groupName).toEqual(reservation.groupName);
    expect(reservationDto.reservationItemList.length).toEqual(reservation.reservationItemViewList.length);
  });

  it('should normalizeResponse', () => {
    const reservationDto = new ReservationDto();
    reservationDto.id = 1;
    reservationDto.reservationUid = '112-22aa';
    reservationDto.reservationCode = '0001';
    reservationDto.reservationVendorCode = '2225bbb';
    reservationDto.propertyId = 1;
    reservationDto.contactName = 'Abc';
    reservationDto.contactEmail = 'abc@gmail.com';
    reservationDto.contactPhone = '12345678';
    reservationDto.internalComments = 'Comentário Interno';
    reservationDto.externalComments = 'Comentário Externo';
    reservationDto.groupName = 'Grupo 01';
    reservationDto.unifyAccounts = true;
    reservationDto.companyClientId = 'aarr555';
    reservationDto.companyClientContactPersonId = 'bbcc4';
    reservationDto.companyClient = new ClientDto();
    reservationDto.companyClient.tradeName = 'Trade Name 22';

    const reservationItemDto = new ReservationItemDto();
    reservationItemDto.id = 2;
    reservationItemDto.reservationItemUid = '1111aaa';
    reservationItemDto.reservationId = 1;
    reservationItemDto.reservationItemCode = '00010';
    reservationItemDto.estimatedArrivalDate = moment().format(DateService.DATE_FORMAT_UNIVERSAL);
    reservationItemDto.estimatedDepartureDate = moment().format(DateService.DATE_FORMAT_UNIVERSAL);
    reservationItemDto.receivedRoomType = RoomTypeLuxoData;
    reservationItemDto.requestedRoomType = RoomTypeLuxoData;
    reservationItemDto.receivedRoomTypeId = RoomTypeLuxoData.id;
    reservationItemDto.requestedRoomTypeId = RoomTypeLuxoData.id;
    reservationItemDto.roomId = 10;
    reservationItemDto.room = new Room();
    reservationItemDto.room.id = 10;
    reservationItemDto.roomLayout = new RoomLayout();
    reservationItemDto.roomLayout.id = 'aaabbcc';
    reservationItemDto.adultCount = 2;
    reservationItemDto.childCount = 1;
    reservationItemDto.roomLayoutId = '23344aaabbbccc';
    reservationItemDto.extraBedCount = 1;
    reservationItemDto.extraCribCount = 2;

    reservationDto.reservationItemList.push(reservationItemDto);

    const dateService: DateService = TestBed.get(DateService);

    const period = {
      beginDate: reservationItemDto.estimatedArrivalDate,
      endDate: reservationItemDto.estimatedDepartureDate
    };
    const dateArrival = dateService.convertStringToDate(reservationItemDto.estimatedArrivalDate, DateService.DATE_FORMAT_UNIVERSAL);
    const checkinHour = dateArrival.getHours() < 10
        ? '0' + dateArrival.getHours().toString()
        : dateArrival.getHours().toString();
    const checkinMinutes = dateArrival.getMinutes() < 10
        ? '0' + dateArrival.getMinutes().toString()
        : dateArrival.getMinutes().toString();
    const checkinHourAndMinute = checkinHour + ':' + checkinMinutes;

    const dateDeparture = dateService.convertStringToDate(reservationItemDto.estimatedDepartureDate, DateService.DATE_FORMAT_UNIVERSAL);
    const checkoutHour = dateDeparture.getHours() < 10
        ? '0' + dateDeparture.getHours().toString()
        : dateDeparture.getHours().toString();
    const checkoutMinutes = dateDeparture.getMinutes() < 10
        ? '0' + dateDeparture.getMinutes().toString()
        : dateDeparture.getMinutes().toString();
    const checkoutHourAndMinute = checkoutHour + ':' + checkoutMinutes;

    reservation.normalizeResponse(reservationDto);

    expect(reservationDto.id).toEqual(reservation.id);
    expect(reservationDto.reservationUid).toEqual(reservation.reservationUid);
    expect(reservationDto.reservationCode).toEqual(reservation.reservationCode);
    expect(reservationDto.reservationVendorCode).toEqual(reservation.reservationVendorCode);
    expect(reservationDto.propertyId).toEqual(reservation.propertyId);
    expect(reservationDto.contactName).toEqual(reservation.contactName);
    expect(reservationDto.contactEmail).toEqual(reservation.contactEmail);
    expect(reservationDto.contactPhone).toEqual(reservation.contactPhone);
    expect(reservationDto.internalComments).toEqual(reservation.internalComments);
    expect(reservationDto.externalComments).toEqual(reservation.externalComments);
    expect(reservationDto.groupName).toEqual(reservation.groupName);
    expect(reservationDto.unifyAccounts).toEqual(reservation.unifyAccounts);
    expect(reservationDto.companyClientId).toEqual(reservation.client.id);
    expect(reservationDto.companyClientContactPersonId).toEqual(reservation.clientContact.id);
    expect(reservationDto.companyClient.tradeName).toEqual(reservation.client.tradeName);

    expect(reservationDto.reservationItemList.length).toEqual(1);
    expect(reservationDto.reservationItemList[0].id).toEqual(reservation.reservationItemViewList[0].id);
    expect(reservationDto.reservationItemList[0].reservationItemUid).toEqual(reservation.reservationItemViewList[0].reservationItemUid);
    expect(reservationDto.reservationItemList[0].reservationId).toEqual(reservation.reservationItemViewList[0].reservationId);
    expect(reservationDto.reservationItemList[0].reservationItemCode).toEqual(reservation.reservationItemViewList[0].code);
    expect(period).toEqual(reservation.reservationItemViewList[0].period);
    expect(checkinHourAndMinute).toEqual(reservation.reservationItemViewList[0].checkinHour);
    expect(checkoutHourAndMinute).toEqual(reservation.reservationItemViewList[0].checkoutHour);
    expect(reservationDto.reservationItemList[0].receivedRoomTypeId).toEqual(reservation.reservationItemViewList[0].receivedRoomType.id);
    expect(reservationDto.reservationItemList[0].requestedRoomTypeId).toEqual(reservation.reservationItemViewList[0].requestedRoomType.id);
    expect(reservationDto.reservationItemList[0].room.id).toEqual(reservation.reservationItemViewList[0].room.id);
    expect(reservationDto.reservationItemList[0].roomLayout.id).toEqual(reservation.reservationItemViewList[0].roomLayout.id);
    expect(reservationDto.reservationItemList[0].adultCount).toEqual(reservation.reservationItemViewList[0].adultQuantity);
    expect(reservationDto.reservationItemList[0].childCount).toEqual(reservation.reservationItemViewList[0].childrenQuantity);
    expect(reservationDto.reservationItemList[0].extraBedCount).toEqual(reservation.reservationItemViewList[0].extraBedCount);
    expect(reservationDto.reservationItemList[0].extraCribCount).toEqual(reservation.reservationItemViewList[0].extraCribCount);
  });
});
