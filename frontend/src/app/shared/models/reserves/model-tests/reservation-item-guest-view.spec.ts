import { TestBed, inject } from '@angular/core/testing';
import { ReservationItemGuestView } from './../reservation-item-guest-view';
import { RoomListResultDto } from './../../dto/room-list-result-dto';

describe('ReservationItemGuestView', () => {
  const reservationItemGuestView = new ReservationItemGuestView();

  it('should normalizeResponse', () => {
    const roomListResultDto = new Array<RoomListResultDto>();
    roomListResultDto.push(new RoomListResultDto());
    roomListResultDto.push(new RoomListResultDto());
    roomListResultDto.push(new RoomListResultDto());

    const normalized = reservationItemGuestView.normalizeResponseRoomList(roomListResultDto);
    expect(normalized.length).toEqual(3);
    expect(normalized[0] instanceof ReservationItemGuestView).toBeTruthy();
  });
});
