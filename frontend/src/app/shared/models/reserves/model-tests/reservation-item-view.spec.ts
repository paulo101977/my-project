// import { TestBed, inject } from '@angular/core/testing';
// import { FormBuilder, AbstractControl, Validators } from '@angular/forms';

// // Routes
// import { propertyRoutes } from '../../../../property/property-routing.module';

// // Models
// import { Guest } from './../guest';
// import { RoomTypeLuxoData } from './../../../mock-data/room-type.data';
// import { RoomLayout } from './../../room-layout';
// import { ReservationBudget } from './../reservation-budget';
// import { RoomType } from './../../room-type/room-type';
// import { Room } from './../../room';
// import { ReservationDto } from './../../dto/reservation-dto';
// import { Reservation } from './../reservation';
// import { ReservationStatusEnum } from './../reservation-status-enum';
// import { ReservationItemView, ReservationItemViewStatusEnum } from './../reservation-item-view';
// import { ReservationItemDto } from './../../dto/reservation-item-dto';

// describe('ReservationItemView', () => {

//     const reservationItemView = new ReservationItemView();

//     it('should normalizeRequest', () => {

//         const room = new Room();
//         room.id =  1;
//         const receivedRoomType = new RoomType();
//         receivedRoomType.id =  1;
//         const requestedRoomType = new RoomType();
//         requestedRoomType.id =  2;
//         const roomLayout = new RoomLayout();
//         roomLayout.id = 'aabcc22';
//         const extraBedQuantity = 1;
//         const cribBedQuantity = 2;
//         const guest = new Guest();
//         guest.formGuest = new FormBuilder().group({
//             guestName: ['abcd']
//           });
//         guest.isChild = true;
//         guest.isMain = false;
//         guest.id = 1;
//         const childAge = 8;

//         reservationItemView.id = 1;
//         reservationItemView.reservationItemUid = '';
//         reservationItemView.reservationId = 1;
//         reservationItemView.checkinHour = '14:00';
//         reservationItemView.checkoutHour = '12:00';
//         reservationItemView.code = '001';
//         reservationItemView.period = {
//             beginJsDate: new Date(),
//             endJsDate: new Date()
//         };
//         reservationItemView.roomQuantity = 1;
//         reservationItemView.adultQuantity = 2;
//         reservationItemView.childrenQuantity = 3;
//         reservationItemView.room = room;
//         reservationItemView.receivedRoomType = receivedRoomType;
//         reservationItemView.requestedRoomType = requestedRoomType;
//         reservationItemView.status = ReservationStatusEnum.ToConfirm;
//         reservationItemView.guestQuantity.push(guest);
//         reservationItemView.formCard = new FormBuilder().group({
//             periodCard: [ reservationItemView.period ],
//             adultQuantityCard: [ reservationItemView.adultQuantity ],
//             childrenQuantityCard: [ reservationItemView.childrenQuantity ],
//             roomTypeCard: [ reservationItemView.receivedRoomType ],
//             roomTypeRequestedCard: [ reservationItemView.requestedRoomType ],
//             checkinHourCard: [ reservationItemView.checkinHour ],
//             checkoutHourCard: [ reservationItemView.checkoutHour ],
//             roomNumberCard: [room],
//             roomLayoutCard: [roomLayout],
//             extraBedQuantityCard: [extraBedQuantity],
//             cribBedQuantityCard: [cribBedQuantity],
//             childrenAgeListCard: new FormBuilder().array([new FormBuilder().group({
//                 childAgeCard: [childAge]
//             })])
//         });

//         const reservatioItemDto = reservationItemView.normalizeRequest();

//         expect(reservationItemView.id).toEqual(reservatioItemDto.id);
//         expect(reservationItemView.reservationItemUid).toEqual(reservatioItemDto.reservationItemUid);
//         expect(reservationItemView.status).toEqual(ReservationStatusEnum.ToConfirm);
//         expect(reservationItemView.reservationId).toEqual(reservatioItemDto.reservationId);
//         expect(reservationItemView.code).toEqual(reservatioItemDto.reservationItemCode);
//         expect(reservationItemView.formCard.get('periodCard').value.beginJsDate).toEqual(reservatioItemDto.estimatedArrivalDate);
//         expect(reservationItemView['getCheckin']()).toEqual(reservatioItemDto.checkInDate);
//         expect(reservationItemView.formCard.get('periodCard').value.endJsDate).toEqual(reservatioItemDto.estimatedDepartureDate);
//         expect(reservationItemView['getCheckout']()).toEqual(reservatioItemDto.checkOutDate);
//         expect(reservationItemView.formCard.get('roomTypeCard').value.id).toEqual(reservatioItemDto.receivedRoomTypeId);
//         expect(reservationItemView.formCard.get('roomTypeRequestedCard').value.id).toEqual(reservatioItemDto.requestedRoomTypeId);
//         expect(reservationItemView.formCard.get('roomNumberCard').value.id).toEqual(reservatioItemDto.roomId);
//         expect(ReservationStatusEnum.ToConfirm).toEqual(reservatioItemDto.reservationItemStatus);
//         expect(reservationItemView.formCard.get('adultQuantityCard').value).toEqual(reservatioItemDto.adultCount);
//         expect(reservationItemView.formCard.get('childrenQuantityCard').value).toEqual(reservatioItemDto.childCount);
//         expect(reservationItemView.formCard.get('roomLayoutCard').value.id).toEqual(reservatioItemDto.roomLayoutId);
//         expect(reservationItemView.formCard.get('extraBedQuantityCard').value).toEqual(reservatioItemDto.extraBedCount);
//         expect(reservationItemView.formCard.get('cribBedQuantityCard').value).toEqual(reservatioItemDto.extraCribCount);
//         expect(reservatioItemDto.guestReservationItemList[0].childAge).toEqual(childAge);

//         reservationItemView.formCard.get('roomNumberCard').setValue(null);

//         expect(reservationItemView.formCard.get('roomNumberCard').value).toEqual(null);
//     });

//     it('should normalizeResponse', () => {
//         const reservationItemDto = new ReservationItemDto();
//         reservationItemDto.id = 1;
//         reservationItemDto.reservationItemUid = '1111aaa';
//         reservationItemDto.reservationId = 1;
//         reservationItemDto.reservationItemCode = '00010';
//         reservationItemDto.estimatedArrivalDate = new Date();
//         reservationItemDto.estimatedDepartureDate = new Date();
//         reservationItemDto.receivedRoomType = RoomTypeLuxoData;
//         reservationItemDto.requestedRoomType = RoomTypeLuxoData;
//         reservationItemDto.receivedRoomTypeId = RoomTypeLuxoData.id;
//         reservationItemDto.requestedRoomTypeId = RoomTypeLuxoData.id;
//         reservationItemDto.roomId = 10;
//         reservationItemDto.room = new Room();
//         reservationItemDto.room.id = 10;
//         reservationItemDto.roomLayout = new RoomLayout();
//         reservationItemDto.roomLayout.id = 'aaabbcc';
//         reservationItemDto.adultCount = 2;
//         reservationItemDto.childCount = 1;
//         reservationItemDto.roomLayoutId = '23344aaabbbccc';
//         reservationItemDto.extraBedCount = 1;
//         reservationItemDto.extraCribCount = 2;
//         reservationItemDto.reservationItemStatus = ReservationStatusEnum.Checkin;

//         const period = {
//             beginJsDate: reservationItemDto.estimatedArrivalDate,
//             endJsDate: reservationItemDto.estimatedDepartureDate,
//             beginDate: {
//                 year: reservationItemDto.estimatedArrivalDate.getFullYear(),
//                 month: reservationItemDto.estimatedArrivalDate.getMonth() + 1,
//                 day: reservationItemDto.estimatedArrivalDate.getDate()
//             },
//             endDate: {
//                 year: reservationItemDto.estimatedDepartureDate.getFullYear(),
//                 month: reservationItemDto.estimatedDepartureDate.getMonth() + 1,
//                 day: reservationItemDto.estimatedDepartureDate.getDate()
//             }
//         };
//         // const checkinHour = reservationItemDto.estimatedArrivalDate.getHours() < 10
// ? '0' + reservationItemDto.estimatedArrivalDate.getHours().toString()
// : reservationItemDto.estimatedArrivalDate.getHours().toString();
//         // const checkinMinutes = reservationItemDto.estimatedArrivalDate.getMinutes() < 10
// ? '0' + reservationItemDto.estimatedArrivalDate.getMinutes().toString()
// : reservationItemDto.estimatedArrivalDate.getMinutes().toString();
//         // const checkinHourAndMinute = checkinHour + ':' + checkinMinutes;
//         const checkinHourAndMinute = '14:00';

//         // const checkoutHour = reservationItemDto.estimatedDepartureDate.getHours() < 10
// ? '0' + reservationItemDto.estimatedDepartureDate.getHours().toString()
// : reservationItemDto.estimatedDepartureDate.getHours().toString();
//         // const checkoutMinutes = reservationItemDto.estimatedDepartureDate.getMinutes() < 10
// ? '0' + reservationItemDto.estimatedDepartureDate.getMinutes().toString()
// : reservationItemDto.estimatedDepartureDate.getMinutes().toString();
//         // const checkoutHourAndMinute = checkoutHour + ':' + checkoutMinutes;
//         const checkoutHourAndMinute = '12:00';

//         const reservation-budget = new ReservationBudget();
//         reservation-budget.id = 1;
//         reservation-budget.reservationBudgetUid = 'aadccd222';
//         reservation-budget.reservationItemId = reservationItemDto.id;
//         reservation-budget.budgetDay = new Date();
//         reservation-budget.manualRate = 200;

//         reservationItemDto.reservationBudgetList.push(reservation-budget);

//         reservationItemView.normalizeResponse(reservationItemDto);

//         expect(reservationItemDto.id).toEqual(reservationItemView.id);
//         expect(reservationItemDto.reservationItemUid).toEqual(reservationItemView.reservationItemUid);
//         expect(reservationItemDto.reservationId).toEqual(reservationItemView.reservationId);
//         expect(reservationItemDto.reservationItemCode).toEqual(reservationItemView.code);
//         expect(period).toEqual(reservationItemView.period);
//         expect(checkinHourAndMinute).toEqual(reservationItemView.checkinHour);
//         expect(checkoutHourAndMinute).toEqual(reservationItemView.checkoutHour);
//         expect(reservationItemDto.receivedRoomTypeId).toEqual(reservationItemView.receivedRoomType.id);
//         expect(reservationItemDto.requestedRoomTypeId).toEqual(reservationItemView.requestedRoomType.id);
//         expect(reservationItemDto.roomId).toEqual(reservationItemView.room.id);
//         expect(reservationItemDto.room.id).toEqual(reservationItemView.room.id);
//         expect(reservationItemDto.roomLayout.id).toEqual(reservationItemView.roomLayout.id);
//         expect(ReservationStatusEnum.Checkin).toEqual(reservationItemView.status);
//         expect(reservationItemDto.adultCount).toEqual(reservationItemView.adultQuantity);
//         expect(reservationItemDto.childCount).toEqual(reservationItemView.childrenQuantity);
//         expect(reservationItemDto.extraBedCount).toEqual(reservationItemView.extraBedCount);
//         expect(reservationItemDto.extraCribCount).toEqual(reservationItemView.extraCribCount);
//         expect(reservationItemDto.reservationBudgetList.length).toEqual(reservationItemView.reservationBudgetList.length);
//         expect(reservationItemDto.reservationBudgetList[0].id).toEqual(reservationItemView.reservationBudgetList[0].id);
//         expect(reservationItemDto.reservationBudgetList[0].reservationBudgetUid)
// .toEqual(reservationItemView.reservationBudgetList[0].reservationBudgetUid);
//         expect(reservationItemDto.reservationBudgetList[0].reservationItemId)
// .toEqual(reservationItemView.reservationBudgetList[0].reservationItemId);
//         expect(reservationItemDto.reservationBudgetList[0].budgetDay).toEqual(reservationItemView.reservationBudgetList[0].budgetDay);
//         expect(reservationItemDto.reservationBudgetList[0].manualRate).toEqual(reservationItemView.reservationBudgetList[0].manualRate);
//     });

// });
