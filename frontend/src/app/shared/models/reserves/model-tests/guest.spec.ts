import { TestBed, inject } from '@angular/core/testing';
import { FormBuilder, AbstractControl, Validators } from '@angular/forms';

import { GuestReservationItemDto } from './../../dto/guest-reservation-item-dto';
import { Guest } from './../guest';
import { RoomLayout } from './../../room-layout';

describe('Guest', () => {
  const guest = new Guest();
  guest.formGuest = new FormBuilder().group({
    guestName: ['abcd'],
  });

  it('should normalizeRequest', () => {
    guest.isChild = false;
    guest.isMain = true;
    guest.id = 1;

    const guestReservationItemDto = guest.normalizeRequest();

    expect(guestReservationItemDto.id).toEqual(guest.id);
    expect(guestReservationItemDto.isChild).toEqual(guest.isChild);
    expect(guestReservationItemDto.isMain).toEqual(guest.isMain);
    expect(guestReservationItemDto.guestName).toEqual(guest.formGuest.get('guestName').value);
  });

  it('should normalizeResponse', () => {
    guest.isChild = false;
    guest.isMain = true;

    const guestReservationItemDto = new GuestReservationItemDto();
    guestReservationItemDto.id = 1;
    guestReservationItemDto.guestName = 'Abcd';
    guestReservationItemDto.isMain = true;
    guestReservationItemDto.isChild = false;

    guest.normalizeResponse(guestReservationItemDto);

    expect(guestReservationItemDto.id).toEqual(guest.id);
    expect(guestReservationItemDto.isChild).toEqual(guest.isChild);
    expect(guestReservationItemDto.isMain).toEqual(guest.isMain);
    expect(guestReservationItemDto.guestName).toEqual(guest.formGuest.get('guestName').value);
  });
});
