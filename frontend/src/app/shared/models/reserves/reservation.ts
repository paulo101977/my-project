// Models
import { ClientContact } from './../clientContact';
import { Client } from './../client';
import { IModelSerialize } from './../../interfaces/model-serialize.interface';
import { ReservationItemGuestView } from './reservation-item-guest-view';
import { ReservationItemView } from './reservation-item-view';
import { ReservationDto } from './../dto/reservation-dto';
import { ReservationStatusEnum } from './../reserves/reservation-status-enum';
import { ReservationConfirmationDto } from './../dto/reserve-confirmation-dto';

export class Reservation implements IModelSerialize<ReservationDto> {
  id: number;
  reservationUid: string;
  reservationCode: string;
  reservationVendorCode: string;
  propertyId: number;
  contactName: string;
  contactEmail: string;
  contactPhone: string;
  total: number;
  launchDaily: boolean;
  internalComments: string;
  externalComments: string;
  partnerComments: string;
  groupName: string;
  unifyAccounts: boolean;
  originId: number;
  segmentId: number;
  reservationItemViewList: Array<ReservationItemView>;
  reservationItemGuestViewList: Array<ReservationItemGuestView>;
  client: Client;
  clientContact: ClientContact;
  reservationConfirmation: ReservationConfirmationDto;
  commercialBudgetName: string;
  walkin: boolean;

  // Construtor necessário para inicializar os campos, pois como alguns campos
  // não são obrigatórios, estavam indo undefined para o back.
  constructor() {
    this.id = 0;
    this.reservationUid = '00000000-0000-0000-0000-000000000000';
    this.reservationCode = '001';
    this.reservationVendorCode = '001';
    this.propertyId = 0;
    this.contactName = '';
    this.contactEmail = '';
    this.total = 0;
    this.internalComments = '';
    this.externalComments = '';
    this.partnerComments = '';
    this.groupName = '';
    this.unifyAccounts = false;
    this.reservationItemViewList = new Array<ReservationItemView>();
    this.reservationItemGuestViewList = new Array<ReservationItemGuestView>();
    this.client = new Client();
    this.clientContact = new ClientContact();
    this.reservationConfirmation = new ReservationConfirmationDto();
  }

  public normalizeRequest(): ReservationDto {
    const reservation = new ReservationDto();
    reservation.id = this.id;
    reservation.reservationUid = this.reservationUid;
    reservation.reservationCode = this.reservationCode;
    reservation.reservationVendorCode = this.reservationVendorCode;
    reservation.propertyId = this.propertyId;
    reservation.contactName = this.contactName;
    reservation.contactEmail = this.contactEmail;
    reservation.contactPhone = this.contactPhone;
    reservation.internalComments = this.internalComments;
    reservation.externalComments = this.externalComments;
    reservation.partnerComments = this.partnerComments;
    reservation.groupName = this.groupName;
    reservation.unifyAccounts = this.unifyAccounts;
    reservation.businessSourceId = this.originId;
    reservation.marketSegmentId = this.segmentId;
    reservation.companyClientId = this.client != null ? this.client.id : null;
    reservation.companyClientContactPersonId = this.clientContact != null ? this.clientContact.id : null;
    this.reservationConfirmation.launchDaily = this.launchDaily;
    reservation.reservationConfirmation = this.reservationConfirmation;

    reservation.walkin = this.walkin;
    reservation.launchDaily = this.launchDaily;

    this.reservationItemViewList.forEach(reservationItemView => {
      reservation.reservationItemList.push(reservationItemView.normalizeRequest());
    });

    return reservation;
  }

  public normalizeResponse(reservationDto: ReservationDto): void {
    this.id = reservationDto.id;
    this.reservationUid = reservationDto.reservationUid;
    this.reservationCode = reservationDto.reservationCode;
    this.reservationVendorCode = reservationDto.reservationVendorCode;
    this.propertyId = reservationDto.propertyId;
    this.contactName = reservationDto.contactName;
    this.contactEmail = reservationDto.contactEmail;
    this.contactPhone = reservationDto.contactPhone;
    this.internalComments = reservationDto.internalComments;
    this.externalComments = reservationDto.externalComments;
    this.partnerComments = reservationDto.partnerComments;
    this.groupName = reservationDto.groupName;
    this.unifyAccounts = reservationDto.unifyAccounts;
    this.originId = reservationDto.businessSourceId;
    this.segmentId = reservationDto.marketSegmentId;
    this.client.id = reservationDto.companyClientId;
    this.clientContact.id = reservationDto.companyClientContactPersonId;
    this.client.tradeName = reservationDto.companyClient == null ? null : reservationDto.companyClient.tradeName;
    this.reservationConfirmation = reservationDto.reservationConfirmation;
    this.launchDaily = reservationDto.reservationConfirmation ? reservationDto.reservationConfirmation.launchDaily : true;

    if (reservationDto.reservationItemList) {
      reservationDto.reservationItemList.forEach(reservationItemDto => {
        const reservationItemView = new ReservationItemView();
        reservationItemView.normalizeResponse(reservationItemDto);
        this.reservationItemViewList.push(reservationItemView);
      });
    }
  }

  public normalizeResponseToGuestView(reservationDto: ReservationDto): void {
    this.id = reservationDto.id;
    this.reservationUid = reservationDto.reservationUid;
    this.reservationCode = reservationDto.reservationCode;
    this.reservationVendorCode = reservationDto.reservationVendorCode;
    this.propertyId = reservationDto.propertyId;
    this.contactName = reservationDto.contactName;
    this.contactEmail = reservationDto.contactEmail;
    this.contactPhone = reservationDto.contactPhone;
    this.internalComments = reservationDto.internalComments;
    this.externalComments = reservationDto.externalComments;
    this.partnerComments = reservationDto.partnerComments;
    this.groupName = reservationDto.groupName;
    this.unifyAccounts = reservationDto.unifyAccounts;

    if (reservationDto.reservationItemList) {
      reservationDto.reservationItemList.forEach(reservationItemDto => {
        const reservationItemView = new ReservationItemGuestView();
        reservationItemView.normalizeResponse(reservationItemDto);
        this.reservationItemGuestViewList.push(reservationItemView);
      });
    }
  }
}
