// Models
import { IModelSerialize } from './../../interfaces/model-serialize.interface';
import { ReservationItem } from './reservation-item';
import { ReservationBudget } from './../revenue-management/reservation-budget/reservation-budget';
import { ReservationStatusEnum } from './../reserves/reservation-status-enum';
import { GuestReservationItem } from './../reserves/guest-reservation-item';
import { ReservationItemDto } from './../dto/reservation-item-dto';
import { RoomListResultDto } from './../dto/room-list-result-dto';

export class ReservationItemGuestView extends ReservationItem implements IModelSerialize<ReservationItemDto> {
  checkinDate: string | Date;
  checkoutDate: string | Date;
  reservationItemCode: string;
  requestedRoomTypeId: number;
  receivedRoomTypeId: number;
  roomId: number;
  adultCount: number;
  childCount: number;
  reservationItemStatus: ReservationStatusEnum;
  reservationItemStatusFormatted: string;
  roomLayoutId: string;
  reservationBudgetList: Array<ReservationBudget>;
  guestReservationItemList: Array<GuestReservationItem>;
  roomTypeId: number;
  roomNumber: number;
  roomTypeName: string;
  roomTypeAbbreviation: string;
  searchableNames: Array<string>;
  isValid: boolean;
  reservationContactName: string;
  isActive: boolean;

  constructor() {
    super();
    this.reservationBudgetList = new Array<ReservationBudget>();
    this.guestReservationItemList = new Array<GuestReservationItem>();
  }

  public normalizeRequest(): ReservationItemDto {
    const reservationItemDto = new ReservationItemDto();
    return reservationItemDto;
  }

  public normalizeResponse(reservationItemDto: ReservationItemDto): void {
    this.id = reservationItemDto.id;
    this.reservationItemUid = reservationItemDto.reservationItemUid;
    this.reservationId = reservationItemDto.reservationId;
    this.reservationItemCode = reservationItemDto.reservationItemCode;
    this.checkinDate = reservationItemDto.checkInDate;
    this.checkoutDate = reservationItemDto.checkOutDate;
    this.estimatedArrivalDate = reservationItemDto.estimatedArrivalDate;
    this.estimatedDepartureDate = reservationItemDto.estimatedDepartureDate;
    this.receivedRoomType = reservationItemDto.receivedRoomType;
    this.requestedRoomType = reservationItemDto.requestedRoomType;
    this.requestedRoomTypeId = reservationItemDto.requestedRoomTypeId;
    this.receivedRoomTypeId = reservationItemDto.receivedRoomTypeId;
    this.room = reservationItemDto.room;
    this.roomId = reservationItemDto.roomId;
    this.reservationItemStatus = reservationItemDto.reservationItemStatusId;
    this.reservationItemStatusFormatted = reservationItemDto.reservationItemStatusFormatted;
    this.adultCount = reservationItemDto.adultCount;
    this.childCount = reservationItemDto.childCount;
    this.extraBedCount = reservationItemDto.extraBedCount;
    this.extraCribCount = reservationItemDto.extraCribCount;
    this.roomLayoutId = reservationItemDto.roomLayoutId;

    if (reservationItemDto.reservationBudgetList) {
      reservationItemDto.reservationBudgetList.forEach(reservationBudget => {
        this.reservationBudgetList.push(reservationBudget);
      });
    }

    if (reservationItemDto.guestReservationItemList) {
      reservationItemDto.guestReservationItemList.forEach(guestReservationItemDto => {
        const guestReservationItem = new GuestReservationItem();
        guestReservationItem.normalizeResponse(guestReservationItemDto);
        this.guestReservationItemList.push(guestReservationItem);
      });
    }
  }

  public normalizeResponseRoomList(reservationItemDtoList: Array<RoomListResultDto>): Array<ReservationItemGuestView> {
    const reservationItemGuestViewList = new Array<ReservationItemGuestView>();

    if (reservationItemDtoList) {
      reservationItemDtoList.forEach(reservationItemDto => {
        const item = new ReservationItemGuestView();
        item.id = reservationItemDto.id;
        item.reservationItemUid = '';
        item.reservationId = reservationItemDto.reservationId;
        item.reservationItemCode = reservationItemDto.reservationItemCode;
        item.estimatedArrivalDate = reservationItemDto.estimatedArrivalDate;
        item.estimatedDepartureDate = reservationItemDto.estimatedDepartureDate;
        item.receivedRoomType = null;
        item.requestedRoomType = null;
        item.requestedRoomTypeId = null;
        item.receivedRoomTypeId = null;
        item.room = null;
        item.roomId = null;
        item.reservationItemStatus = reservationItemDto.reservationItemStatus;
        item.adultCount = reservationItemDto.adultCount;
        item.childCount = reservationItemDto.childCount;
        item.extraBedCount = null;
        item.extraCribCount = null;
        item.roomLayoutId = null;

        item.roomTypeId = reservationItemDto.roomTypeId;
        item.roomNumber = reservationItemDto.roomNumber;
        item.roomTypeName = reservationItemDto.roomTypeName;
        item.roomTypeAbbreviation = reservationItemDto.roomTypeAbbreviation;
        item.checkinDate = reservationItemDto.checkIn;
        item.checkoutDate = reservationItemDto.checkOut;
        item.searchableNames = reservationItemDto.searchableNames;
        item.isValid = reservationItemDto.isValid;
        item.reservationContactName = reservationItemDto.reservationContactName;

        reservationItemGuestViewList.push(item);
      });
    }
    return reservationItemGuestViewList;
  }
}
