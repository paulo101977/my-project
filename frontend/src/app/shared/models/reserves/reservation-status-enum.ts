export enum ReservationStatusEnum {
  ToConfirm = 0,
  Confirmed = 1,
  Checkin = 2,
  Checkout = 3,
  Pending = 4,
  NoShow = 5,
  Canceled = 6,
  WaitList = 7,
  ReservationProposal = 8,
}

export enum ReservationStatusPreCheckinEnum {
  ToConfirm = 0,
  Confirmed = 1,
}
