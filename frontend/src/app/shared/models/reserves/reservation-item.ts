// Models
import { Room } from './../room';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { ReservationStatusEnum } from './reservation-status-enum';

export abstract class ReservationItem {
  id: number;
  reservationItemUid: string;
  reservationId: number;
  room: Room;
  requestedRoomType: RoomType;
  requestedRoomTypeId: number;
  receivedRoomType: RoomType;
  extraBedCount: number;
  extraCribCount: number;
  gratuityTypeId: number;
  currencyId: string;
  mealPlanTypeId: number;
  ratePlanId: string;
  reservationItemStatusId: ReservationStatusEnum;
  estimatedArrivalDate: string | Date;
  estimatedDepartureDate: string | Date;
}
