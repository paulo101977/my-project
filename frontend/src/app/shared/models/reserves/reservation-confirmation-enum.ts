export enum ReservationConfirmationEnum {
  InternalUsage = <any>1,
  Deposit = <any>2,
  Courtesy = <any>3,
  Hotel = <any>4,
  CreditCard = <any>5,
  ToBeBilled = <any>6,
}
