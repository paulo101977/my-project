export class ReservationHour {
  public checkinHour: string;
  public checkoutHour: string;
  public errorMessage: string;

  constructor() {
    this.checkinHour = '14:00';
    this.checkoutHour = '12:00';
  }
}
