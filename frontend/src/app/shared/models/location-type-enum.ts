export enum LocationType {
  cep = <any>'cep',
  streetName = <any>'streetName',
  countryCode = <any>'countryCode',
  streetNumber = <any>'streetNumber',
}
