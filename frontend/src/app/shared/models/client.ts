import { DocumentType } from './document-type';
import { ClientContact } from './clientContact';

export class Client {
  id: string;
  contacts: Array<ClientContact>;
  cnpj: string;
  nomeFantasia: string;
  razaoSocial: string;
  document: string;
  documentType: DocumentType;
  documentTypeId: number;
  shortName: string;
  tradeName: string;
  templateClientSearch: string;
}
