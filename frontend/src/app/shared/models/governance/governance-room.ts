export class GovernanceRoom {
  roomId: number;
  roomAbbreviation: string;
  roomType?: string;
  roomNumber: string;
  floor: number;
  roomStatus: string;
  checkin: string;
}
