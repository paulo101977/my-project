export class Governance {
  id: string;
  isActive: boolean;
  statusName: string;
  housekeepingStatusName: string;
  housekeepingStatusId: number;
  color: string;
  propertyId: number;
  visibleBtnIsActive = false;
}
