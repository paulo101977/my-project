import { GovernanceRoom } from 'app/shared/models/governance/governance-room';

export class GovernanceRoomAssoc {
  userId: string;
  name: string;
  photoUrl?: string;
  listRoom?: Array<GovernanceRoom>;
  countRoom?: number;
}
