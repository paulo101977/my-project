// Models
import { LocationCategoryEnum } from './location-category-enum';

export class Location {
  recordIdentification: string;
  latitude: number;
  longitude: number;
  streetName: string;
  streetNumber: string;
  additionalAddressDetails: string;
  postalCode: number;
  countryCode: string;
  ownerId: string;
  browserLanguage: string;
  neighborhood: string;
  cityId: number;
  city: string;
  locationCategoryId: number;
  locationCategory: LocationCategory;
  addressTranslation: string;
  completeAddress: string;
  subdivision: string;
  division: string;
  country: string;
  countrySubdivisionId: string;
  id: number;
}

export class LocationCategory {
  name: LocationCategoryEnum;
  recordScope: string;
  id: number;
}
