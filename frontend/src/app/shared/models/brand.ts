import { Chain } from './chain';

export class Brand {
  id: number;
  name: string;
  chain: Chain;
}
