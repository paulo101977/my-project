import { Person } from './person';

export class Company {
  id: number;
  shortName: string;
  tradeName: string;
  person: Person;
}
