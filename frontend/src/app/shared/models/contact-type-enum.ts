export enum ContactType {
  email = 1,
  tel = 2,
  homePage = 3,
}
