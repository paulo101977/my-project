export class CurrencyExchange {
  currencyId: string;
  currencyName?: string;
  currentDate: string;
  exchangeRate: number;
  propertyExchangeRate: number;
  symbol?: string;
  id: string;
  propertyId: number;
}

export class InsertQuotationForm {
  startDate: string;
  endDate: string;
  currencyId: string;
  propertyExchangeRate: number;
  propertyId: number;
}
