import { RateProposalStorageItem } from 'app/reservation/rate-proposal/models/rate-proposal-storage-item';

export interface IRateProposalStorage {
  [p: string]: RateProposalStorageItem;
}
