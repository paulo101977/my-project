export class MarketSegment {
  name: string;
  isActive: boolean;
  code: string;
  segmentAbbreviation: string;
  parentMarketSegmentId: number;
  id: number;
  childMarketSegmentList: Array<MarketSegment>;
  propertyId: number;
}
