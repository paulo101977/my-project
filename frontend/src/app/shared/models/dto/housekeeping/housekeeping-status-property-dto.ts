export class HousekeepingStatusProperty {
  housekeepingStatusId: number;
  housekeepingStatusPropertyId: number;
  color: string;
  housekeepingStatusName: string;
  roomNumber: string;
  statusRoom: string;
  roomTypeName: string;
  checkInDate: Date;
  checkOutDate: Date;
  quantityDouble: number;
  quantitySingle: number;
  extraCribCount: number;
  extraBadCount: number;
  adultCount: number;
  childCount: number;
  reservationItemStatusId: number;
  housekeppingPropertyStatusNameList: Array<any>;
  id: number;
  Blocked: boolean;
}
