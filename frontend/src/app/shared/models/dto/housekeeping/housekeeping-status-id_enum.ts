export enum HouseKeepingStatusIdEnum {
  clean = 1,
  dirty = 2,
  mantainance = 3,
  inspection = 4,
  arrangement = 5,
  created = 6,
}
