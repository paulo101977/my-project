export class RoomBlock {
  propertyId: number;
  roomId: number;
  blockingStartDate: string;
  blockingEndDate: string;
  reasonId: number;
  comments: string;
}
