import { GuestDto } from './guest-dto';

export class GuestReservationItemDto {
  id: number;
  personId: string;
  guest: GuestDto;
  reservationItemId: number;
  guestId: number;
  guestName: string;
  guestEmail: string;
  guestDocumentTypeId: number;
  guestDocument: string;
  estimatedArrivalDate: Date;
  checkInDate: Date;
  estimatedDepartureDate: Date;
  checkOutDate: Date;
  guestStatusId: number;
  guestTypeId: number;
  preferredName: string;
  guestCitizenship: string;
  guestLanguage: string;
  pctDailyRate: number;
  isIncognito: boolean;
  isChild: boolean;
  childAge: number;
  isMain: boolean;

  constructor() {
    this.id = 0;
    this.childAge = null;
    this.guest = new GuestDto();
  }
}
