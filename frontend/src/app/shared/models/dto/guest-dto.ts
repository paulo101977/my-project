import { PersonDto } from './person-dto';

export class GuestDto {
  guestId: number;
  personId: string;
  person: PersonDto;
  id: number;

  constructor() {
    this.guestId = null;
    this.personId = null;
  }
}
