export class PropertyGuestTypeDto {
  public propertyId: number;
  public guestTypeName: string;
  public code: string;
  public isVip: boolean;
  public isIncognito: boolean;
  public isActive: boolean;
  public id: number;
}

export class PropertyGuestTypeExemptOfTourismTaxDto extends PropertyGuestTypeDto {
  public isExemptOfTourismTax: boolean;
}
