export class Reason {
  reasonName: string;
  reasonCategoryId: number;
  reasonCategoryName: string;
  isActive: boolean;
  propertyId: number;
  id: number;
}

export class ReasonCategory {
  reasonCategoryName: string;
  id: number;
}
