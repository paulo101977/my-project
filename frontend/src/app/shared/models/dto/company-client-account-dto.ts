export class CompanyClientAccount {
  billingAccountId: string;
  reservationId: number;
  reservationItemId: number;
  personId: string;
  reservationCode: string;
  reservationItemCode: string;
  arrivalDate: Date;
  departureDate: Date;
  reservationItemStatus: number;
  reservationItemStatusName: string;
  compannyClientId: string;
  companyClientName: string;
}
