export class BillingItemDiscountDto {
  billingAccountId: string;
  billingAccountItemId: string;
  billingItemId: number;
  isPercentual: boolean;
  percentual: number;
  reasonId: number;
  amountDiscount: number;
  isCredit: boolean;
  propertyId: number;
}
