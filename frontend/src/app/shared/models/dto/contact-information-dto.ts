import { PersonDto } from './person-dto';

export class ContactInformationDto {
  ownerId: string;
  contactInformationTypeId: number;
  information: string;
  contactInformationType: ContactInformationTypeDto;
  owner: PersonDto;
  id: number;
}

export class ContactInformationTypeDto {
  contactInformationTypeId: number;
  name: string;
  stringFormatMask: string;
  regexValidationExpression: string;
  id: number;
}
