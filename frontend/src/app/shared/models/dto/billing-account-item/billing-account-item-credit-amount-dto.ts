export class BillingAccountItemCreditAmountDto {
  id: string;
  differenceAmountAndCreditNote: number;
  differenceAmountAndCreditNoteFormatted: number;
  creditAmount: number;
  billingItemName: string;
  billingInvoiceId: string;
  billingAccountItemDate: Date;
  billingItemId: number;
  reasonId: number;
}
