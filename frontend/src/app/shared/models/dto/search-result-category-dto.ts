import { SearchPersonAndGuestDto } from './search-person-and-guest-dto';

export class SearchResultCategoryDto {
  public searchCategoryName: string;
  public searchPersonAndGuest: Array<SearchPersonAndGuestDto>;
}
