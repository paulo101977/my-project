export class SearchPersonAndGuestDto {
  public personId: string;
  public guestId: number;
  public name: string;
  public document: string;
  public documentTypeId: number;
  public documentType: string;
}
