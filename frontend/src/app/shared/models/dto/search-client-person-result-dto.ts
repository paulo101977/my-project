// Models
import { CompanyClientAccountDto } from './billing-account/company-client-account-dto';

export class SearchClientAndPersonsResultDto {
  companyClientId: string;
  reservationId: string;
  reservationItemId: number;
  guestReservationItemId: number;
  billingAccountTypeId: number;
  startDate: Date;
  endDate: Date;
  statusId: number;
  isMainAccount: boolean;
  marketSegmentId: number;
  businessSourceId: number;
  accountBalance: number;
  companyClient: CompanyClientAccountDto;
  id: string;
}
