import { ReservationStatusEnum } from './../reserves/reservation-status-enum';

export class RoomListResultDto {
  id: number;
  reservationId: number;
  roomTypeId: number;
  statusId: number;
  roomNumber: number;
  roomTypeName: string;
  roomTypeAbbreviation: string;
  checkIn: string;
  checkOut: string;
  adultCount: number;
  childCount: number;
  searchableNames: Array<string>;
  isValid: boolean;
  reservationContactName: string;
  reservationItemCode: string;
  reservationItemStatus: ReservationStatusEnum;
  estimatedArrivalDate: Date;
  estimatedDepartureDate: Date;
}
