export class Location {
  browserLanguage: string;
  latitude: number;
  longitude: number;
  streetName: string;
  streetNumber: string;
  additionalAddressDetails: string;
  postalCode: number;
  countryCode: string;
  locationCategoryId: number;
  neighborhood: string;
  completeAddress: string;
  subdivision: string;
  division: string;
  country: string;
  id: number;
  city: string;

  constructor() {
    this.id = 0;
  }
}

export class Document {
  documentTypeId: number;
  documentInformation: string;
  documentTypeName: string;
  id: number;
  ownerId: string;

  constructor() {
    this.id = 0;
  }
}

class Person {
  personType: string;
  firstName: string;
  lastName: string;
  fullName: string;
  receiveOffers: boolean;
  sharePersonData: boolean;
  allowContact: boolean;
  countrySubdivisionId: number;

  constructor() {}
}



export class FnrhSaveDto {
  guestId: number;
  personId: string;
  firstName: string;
  lastName: string;
  email: string;
  fullName: string;
  phoneNumber: string;
  mobilePhoneNumber: string;
  birthDate: Date;
  age: number;
  gender: string;
  arrivingBy: number;
  additionalInformation: string;
  nextDestinationText: string;
  arrivingFromText: string;
  occupationId: number;
  guestTypeId: number;
  nationality: number;
  purposeOfTrip: number;
  socialName: string;
  healthInsurance: string;
  document: Document;
  location: Location;
  person: Person;
  guestReservationItemId: number;
  id: string;
  propertyId: number;
  tenantId: string;
  responsibleGuestRegistrationId: string;
  responsableName: string;
  isLegallyIncompetent: boolean;
  isChild: boolean;
  guestRegistrationDocumentList: any[];
  roomTypeAbbreviation?: string;
  roomNumber?: number;
  adultCount?: number;
  childCount?: number;
  arrivalDate?: string;
  departureDate?: string;
  guestPolicyPrint?: any;


  constructor() {
    this.document = new Document();
    this.location = new Location();
    this.person = new Person();
  }
}

export class FnrhToPrintDto extends FnrhSaveDto {
  reservationItemCode: string;

  requiredFields: {
    documentRequired: boolean;
    emailRequired: boolean;
    nationalityIsRequired: boolean;
    phoneRequired: boolean;
    cellphoneRequired: boolean;
    localeRequired: boolean;
  };

  addressRequiredFields: {
    additionalAddressDetails?: boolean;
    browserLanguage?: boolean;
    completeAddress?: boolean;
    country?: boolean;
    countryCode?: boolean;
    countrySubdivisionId?: boolean;
    division?: boolean;
    id?: boolean;
    latitude?: boolean;
    locationCategoryId?: boolean;
    longitude?: boolean;
    neighborhood?: boolean;
    ownerId?: boolean;
    postalCode?: boolean;
    streetName?: boolean;
    streetNumber?: boolean;
    subdivision?: boolean;
  };
}
