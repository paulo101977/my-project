import { LocationDto } from './../location-dto';

export class FnrhDto {
  id: string;
  personId: string;
  document: string;
  documentTypeId: number;
  guestReservationItemId: number;
  firstName: string;
  socialName: string;
  lastName: string;
  fullName: string;
  email: string;
  birthDate: any;
  age: number;
  guestId: number;
  gender: string;
  occupationId: number;
  guestTypeId: number;
  phoneNumber: string;
  mobilePhoneNumber: string;
  nationality: number;
  purposeOfTrip: string;
  arrivingBy: number;
  arrivingFrom: number;
  arrivingFromText: string;
  nextDestination: number;
  nextDestinationText: string;
  additionalInformation: string;
  healthInsurance: string;
  location: LocationDto;
  // todo verificar no backend os campos abaixo.
  incapable: boolean;
  responsable: number;
  responsibleGuestRegistrationId: string;
  isChild: boolean;
  isMain: boolean;
  childAge: number;
  guestRegistrationDocumentList: any[];
  receiveOffers?: boolean;
  sharePersonData?: boolean;
  allowContact?: boolean;
  guestPolicyPrint?: any;

  constructor() {
    this.personId = '00000000-0000-0000-0000-000000000000';
  }
}
