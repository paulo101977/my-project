export class CheckinGuestReservationItemDto {
  businessSourceName: string;
  reservationItemId: number;
  reservationItemCode: string;
  reservationItemStatusId: number;
  guestReservationItemId: number;
  guestReservationItemName: string;
  guestStatusId: number;
  socialName: string;
  personId?: string;
  documentInformation: string;
  documentTypeName: string;
  guestRegistrationId?: string;
  guestRegistrationDocumentBirthDate?: Date;
  division: string;
  subdivision: string;
  country: string;
  isValid?: boolean;
  id: number;
  isChild: boolean;
  childAge: number;
  isMain: boolean;
  documentTypeId: number;
  guestId: number;
  guestName: string;
  guestEmail: string;
  guestDocumentTypeId: number;
  guestDocument: string;
  estimatedArrivalDate: Date;
  checkInDate: Date;
  estimatedDepartureDate: Date;
  checkOutDate: Date;
  guestTypeId: number;
  preferredName: string;
  guestCitizenship: string;
  guestLanguage: string;
  propertyId: number;
  currentBudget: number;
}
