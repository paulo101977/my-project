export class GuestFnrhDto {
  reservationContactName: string;
  businessSourceName: string;
  reservationItemId: number;
  reservationItemCode: string;
  reservationItemStatusId: number;
  guestReservationItemId: number;
  guestReservationItemName: string;
  personId: string;
  documentInformation: string;
  documentTypeName: string;
  fnrhId: string;
  fnrhDocumentBirthDate: Date;
  division: string;
  subdivision: string;
  country: string;
  isValid: boolean;
  id: number;
}
