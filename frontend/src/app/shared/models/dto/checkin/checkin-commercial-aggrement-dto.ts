export class CheckinCommercialAggrementDto {
  currencyId: string;
  roomTypeId: number;
  mealPlanTypeId: number;
  companyClientId: number;
  roomTypeAbbreviation: string;
  mealPlanTypeName: string;
  commercialBudgetName: string;
  currencySymbol: string;
  rateVariation: number;
  totalBudget: number;
}
