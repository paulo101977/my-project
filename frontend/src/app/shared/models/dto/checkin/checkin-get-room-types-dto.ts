import { CheckInRoomTypesDto } from './checkin-room-types-dto';

export class CheckInGetRoomTypesDto {
  hasNext: boolean;
  items: Array<CheckInRoomTypesDto>;
  total: number;
}
