export class NationalityDto {
  languageIsoCode: string;
  name: string;
  countrySubdivisionId: number;
  nationality: string;
  id: number;
}
