import { CheckinCommercialAggrementDto } from './checkin-commercial-aggrement-dto';
import { CommercialBudgetList, ReservationBudget } from 'app/shared/models/revenue-management/reservation-budget/reservation-budget';

export class CheckinReservationItemBudgetDto {
  reservationItemId: number;
  reservationItemCode: string;
  mealPlanTypeId: number;
  ratePlanId: number;
  currencyId: number;
  gratuityTypeId: number;
  commercialBudgetList: Array<CommercialBudgetList>;
  reservationBudgetDtoList: Array<ReservationBudget>;
  isSeparated: boolean;
  reservationBudgetCurrencySymbol?: string;
}
