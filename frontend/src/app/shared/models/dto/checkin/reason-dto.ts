export class ReasonDto {
  reasonName: string;
  reasonCategoryId: number;
  chainId: number;
  isActive: boolean;
  id: number;
}
