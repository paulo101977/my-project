import { CheckInNumUhsDto } from './checkin-num-uhs-dto';

export class CheckInGetNumUhsDto {
  hasNext: boolean;
  items: Array<CheckInNumUhsDto>;
  total: number;
}
