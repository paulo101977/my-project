import { BedType } from '../../bed-type';

export class CheckInRoomTypesDto {
  name: string;
  abbreviation: string;
  id: number;
  overbooking: boolean;
  adultCapacity: number;
  childCapacity: number;
  roomTypeBedTypeList: BedType[];
}
