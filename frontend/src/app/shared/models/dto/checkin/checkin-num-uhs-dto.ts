export class CheckInNumUhsDto {
  roomNumber: any;
  remarks: string;
  isActive: boolean;
  parentRoomId: string;
  propertyId: number;
  roomTypeId: number;
  building: string;
  wing: string;
  floor: string;
  childRoomIdList: Array<any>;
  roomType: string;
  childRoomTotal: string;
  id: number;
  housekeepingStatusName: string;
  color: string;
}
