import { Location } from 'app/shared/models/location';
import { ContactInformationDto } from './contact-information-dto';
import { PersonInformationDto } from './person-information-dto';
import { LocationDto } from './location-dto';
import { GuestReservationItemDto } from './guest-reservation-item-dto';

export class PersonDto {
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  personType: number;
  guest: GuestReservationItemDto;
  company: null;
  documentList: Array<any>;
  locationList: Array<LocationDto> | Array<Location>;
  companyContactPersonList: Array<any>;
  contactInformationList: Array<ContactInformationDto>;
  employeeList: Array<any>;
  personInformationList: Array<PersonInformationDto>;
  propertyContactPersonList: Array<any>;
  userList: Array<any>;
  id: string;
  fullName: string;
}
