export class AvailabilityRoomTypeDto {
  id: number;
  name: string;
  total: number;
  expanded?: boolean;
}
