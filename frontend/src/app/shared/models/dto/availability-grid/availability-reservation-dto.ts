export class AvailabilityReservationDto {
  id: number;
  roomId: number;
  reservationId: number;
  roomTypeId: number;
  start: string;
  startFormatted: string;
  end: string;
  endFormatted: string;
  guestName: string;
  adultCount: number;
  childCount: number;
  status: number;
  isConjugated: boolean;
  statusName: string;
  reservationCode: string;
  roomNumber: string;
  roomTypeName?: string;
}

export class AvailabilityReservationChangeDto {
  id: number;
  roomId: number;
  roomTypeId: number;
  initialDate: string;
  finalDate: string;
}
