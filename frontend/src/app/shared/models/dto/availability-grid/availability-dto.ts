export class AvailabilityDto {
  roomTypeId: number;
  date: string;
  dateFormatted?: string;
  availableRooms: number;
  rooms: number;
}
