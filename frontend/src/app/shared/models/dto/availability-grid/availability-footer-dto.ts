export class AvailabilityFooterDto {
  id: number;
  name: string;
  total: number;
  values: FooterItemDto[];
}

export class FooterItemDto {
  date: string;
  dateFormatted?: string;
  number: number;
}
