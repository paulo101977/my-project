// Models
import { AvailabilityRoomTypeDto } from './availability-room-type-dto';
import { AvailabilityDto } from './availability-dto';
import { AvailabilityFooterDto } from './availability-footer-dto';
import { RoomBlock } from '../housekeeping/room-block';

export class AvailabilityResourcesDto {
  id: string;
  roomTypeList: AvailabilityRoomTypeDto[];
  availabilityList: AvailabilityDto[];
  footerList: AvailabilityFooterDto[];
  roomsBlocked: RoomBlock[];
}
