export class AvailabilityRoomDto {
  id: number;
  roomId: number;
  name: string;
  beds: any;
}
