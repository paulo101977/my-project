// Models
import { AvailabilityRoomDto } from './availability-room-dto';
import { AvailabilityReservationDto } from './availability-reservation-dto';

export class AvailabilityRoomsDto {
  id: string;
  roomList: AvailabilityRoomDto[];
  reservationList: AvailabilityReservationDto[];
}
