export class Policy {
  description: string;
  policyTypeId: PolicyTypeEnum;
  policyTypeName: string;
  isActive: boolean;
  propertyId: number;
  id: number;
}

export enum PolicyTypeEnum {
  'checkin' = 1,
  'checkout' = 2,
  'modification' = 3,
  'prepaymentAndWarranty' = 4,
  'children' = 5,
  'others' = 6,
  'cancellation' = 7
}
