// Models
import { RoomLayout } from './../room-layout';
import { Room } from './../room';
import { GuestReservationItemDto } from './guest-reservation-item-dto';
import { ReservationBudget } from './../revenue-management/reservation-budget/reservation-budget';
import { ReservationStatusEnum } from './../reserves/reservation-status-enum';
import { RoomType } from '../../../room-type/shared/models/room-type';

export class ReservationItemDto {
  id: number;
  reservationItemUid: string;
  reservationId: number;
  estimatedArrivalDate: string;
  checkInDate: string;
  estimatedDepartureDate: string;
  checkOutDate: string;
  reservationItemCode: string;
  requestedRoomTypeId: number;
  receivedRoomTypeId: number;
  requestedRoomType: RoomType;
  receivedRoomType: RoomType;
  roomId: number;
  adultCount: number;
  childCount: number;
  reservationItemStatusId: ReservationStatusEnum;
  reservationItemStatusFormatted: string;
  roomLayoutId: string;
  extraBedCount: number;
  extraCribCount: number;
  reservationBudgetList: Array<ReservationBudget>;
  guestReservationItemList: Array<GuestReservationItemDto>;
  room: Room;
  roomLayout: RoomLayout;
  gratuityTypeId: number;
  currencyId: string;
  currencySymbol: string;
  mealPlanTypeId: number;
  commercialBudgetName: string;
  ratePlanId: string;
  isMigrated: boolean;
  externalReservationNumber: string;
  partnerReservationNumber: string;

  constructor() {
    this.reservationBudgetList = new Array<ReservationBudget>();
    this.guestReservationItemList = new Array<GuestReservationItemDto>();
  }
}
