import { ReservationStatusEnum } from './../reserves/reservation-status-enum';

export class SearchReservationResultDto {
  reservationCode: string;
  reservationItemCode: string;
  reservationItemId: number;
  reservationItemStatusId: ReservationStatusEnum;
  arrivalDate: Date;
  departureDate: Date;
  cancellationDate: Date;
  roomNumber: number;
  requestedRoomTypeId: number;
  requestedRoomTypeName: string;
  requestedRoomTypeAbbreviation: string;
  receivedRoomTypeId: number;
  receivedRoomTypeName: string;
  receivedRoomTypeAbbreviation: string;
  guestReservationItemStatusId: number;
  guestReservationItemStatusName: string;
  guestId: number;
  guestReservationItemId: number;
  guestIsIncognito: false;
  guestName: string;
  guestLanguage: string;
  client: string;
  groupName: string;
  creationTime: Date;
  reservationBy: string;
  deadline: string;
  guestDocument: string;
  total: number;
  reservationItemStatusName: string;
  id: number;

  isShow: boolean;
}
