import { ClientContactDto } from './client-contact-dto';
import { ClientDocumentDto } from './client-document-dto';
import { ClientLocationDto } from './client-location-dto';
import { ClientDocumentTypeEnum } from './client-document-types';
import { CustomerStation } from '../../integration/customerStation';
import { ChannelClientAssociation } from '../../integration/channel';

export class ClientDto {
  companyId: number;
  id: string;
  personId: string;
  propertyId: number;
  personType: ClientDocumentTypeEnum;
  shortName: string;
  tradeName: string;
  email: string;
  homePage: string;
  externalCode: string;
  phoneNumber: string;
  cellPhoneNumber: string;
  isActive: boolean;
  propertyCompanyClientCategoryId: string;
  locationList: ClientLocationDto[];
  documentList: ClientDocumentDto[];
  companyClientContactPersonList: ClientContactDto[];
  customerStation: CustomerStation[];
  companyClientChannelList: ChannelClientAssociation[];

  constructor() {
    this.locationList = [];
    this.documentList = [];
    this.companyClientContactPersonList = [];
  }
}
