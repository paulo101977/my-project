export class ClientDocumentTypesDto {
  personType: ClientDocumentTypeEnum;
  countryCode: string;
  countrySubdivisionId: number;
  name: string;
  stringFormatMask: string;
  regexValidationExpression: string;
  isMandatory: boolean;
  id: number;
}

export enum ClientDocumentTypeEnum {
  Legal = 'L',
  Natural = 'N',
}
