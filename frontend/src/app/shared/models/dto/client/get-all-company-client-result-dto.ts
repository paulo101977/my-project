export class GetAllCompanyClientResultDto {
  id: string;
  isActive: boolean;
  type: string;
  name: string;
  document: string;
  email: string;
  phone: string;
  homePage: string;
  clientCategoryId: string;
  clientCategoryName: string;
}
