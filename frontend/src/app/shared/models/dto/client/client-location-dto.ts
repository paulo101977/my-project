export class ClientLocationDto {
  ownerId: string;
  browserLanguage: string;
  locationCategoryId: number;
  latitude: number;
  longitude: number;
  streetName: string;
  streetNumber: string;
  additionalAddressDetails: string;
  postalCode: number;
  countryCode: string;
  neighborhood: string;
  cityId: number;
  completeAddress: string;
  subdivision: string;
  division: string;
  country: string;
  id: number;
  countrySubdivisionId: number;

  constructor() {
    this.ownerId = '00000000-0000-0000-0000-000000000000';
  }
}
