export class ClientContactDto {
  personId: string;
  name: string;
  occupationName: string;
  phoneNumber: string;
  email: string;

  constructor() {
    this.personId = '00000000-0000-0000-0000-000000000000';
  }
}
