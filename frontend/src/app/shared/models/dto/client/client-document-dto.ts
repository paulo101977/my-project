// Models
import { ClientDocumentTypesDto } from './client-document-types';

export class ClientDocumentDto {
  ownerId: string;
  documentType: ClientDocumentTypesDto;
  documentTypeId: number;
  documentTypeName: string;
  documentInformation: string;
  id: number;

  constructor() {
    this.id = 0;
    this.ownerId = '00000000-0000-0000-0000-000000000000';
  }
}
