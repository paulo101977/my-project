export class InvoiceSearchDto {
  public initialDate: Date;
  public finalDate: Date;
  public initialNumber: number;
  public finalNumber: number;
  public isXml: boolean;
}
