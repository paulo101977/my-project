import { InvoiceDto } from './invoice-dto';
import { BillingInvoicePropertyDto } from '../rps/billing-invoice-property-dto';
import { BillingInvoicePropertyPrintDto } from '../rps/billing-invoice-property-print-dto';

export class InvoicePrintDto extends InvoiceDto {
  public billingAccount: any;
  public billingInvoiceCancelReason: any;
  public billingInvoiceProperty: BillingInvoicePropertyPrintDto;
  public property: any;
  public externalRps: number;
}
