export class InvoiceDto {
  public id: string;
  public billingInvoiceNumber: number;
  public billingInvoiceSeries: number;
  public emissionDate: Date;
  public cancelDate: Date;
  public emissionUserId: string;
  public emissionUserName: string;
  public cancelUserId: string;
  public externalNumber: number;
  public externalSeries: number;
  public externalEmissionDate: Date;
  public billingAccountId: string;
  public aditionalDetails: string;
  public billingInvoicePropertyId: string;
  public billingInvoiceCancelReasonId: string;
  public integratorLink: string;
  public integratorXml: string;
  public integratorId: string;
  public billingInvoiceStatusId: number;
  public billingInvoiceStatusName: number;
  public externalCodeNumber: string;
  public errorDate: Date;
  public propertyId: number;
  public billingInvoiceTypeId: number;
  public billingInvoiceTypeName: string;
  public externalRps: number;
}
