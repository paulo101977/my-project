export enum BillingInvoiceTypeEnum {
    NFSE = 1,
    NFCE = 2,
    NFE = 3,
    Factura = 4,
    CreditNoteTotal = 5,
    DebitNote = 6,
    CreditNotePartial = 7,
    SAT = 8
}
