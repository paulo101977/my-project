import { PersonDto } from './person-dto';
import { Location } from './../location';

export class PersonHeaderDto extends PersonDto {
    email: string;
    phoneNumber: string;
    billingAccountId: string;
}
