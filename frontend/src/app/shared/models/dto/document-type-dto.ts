export class DocumentTypeDto {
  personType: string;
  countryCode: string;
  name: string;
  stringFormatMask: string;
  regexValidationExpression: string;
  isMandatory: boolean;
  id: number;
}
