import { PersonDto } from './person-dto';

export class PersonInformationDto {
  ownerId: string;
  personInformationTypeId: number;
  information: string;
  owner: PersonDto;
  personInformationType: PersonInformationTypeDto;
  id: number;
}

export class PersonInformationTypeDto {
  personInformationTypeId: number;
  name: string;
  id: number;
}
