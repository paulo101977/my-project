// Models
import { ReservationItemDto } from './reservation-item-dto';
import { ReservationStatusEnum } from './../reserves/reservation-status-enum';
import { ClientDto } from './client/client-dto';
import { ReservationConfirmationDto } from './reserve-confirmation-dto';

export class ReservationDto {
  id: number;
  reservationUid: string;
  reservationCode: string;
  reservationVendorCode: string;
  propertyId: number;
  contactId: number;
  contactName: string;
  contactEmail: string;
  contactPhone: string;
  internalComments: string;
  externalComments: string;
  partnerComments: string;
  groupName: string;
  unifyAccounts: boolean;
  reservationItemList: Array<ReservationItemDto>;
  companyClientId: string;
  companyClientContactPersonId: string;
  companyClient: ClientDto;
  reservationConfirmation: ReservationConfirmationDto;
  businessSourceId: number;
  marketSegmentId: number;
  walkin: boolean;
  launchDaily: boolean;

  constructor() {
    this.reservationItemList = new Array<ReservationItemDto>();
  }
}
