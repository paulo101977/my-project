export class LocationDto {
  ownerId: string;
  latitude: number;
  longitude: number;
  streetName: string;
  streetNumber: string;
  additionalAddressDetails: string;
  postalCode: string;
  countryCode: string;
  locationCategory: LocationCategoryDto;
  neighborhood: string;
  cityId: number;
  city: CityDto;
  addressTranslation: AddressTranslationDto;
  completeAddress: string;
  browserLanguage: string;
  locationCategoryId: number;
  subdivision: string;
  division: string;
  country: string;
  id: number;
}

export class LocationCategoryDto {
  name: string;
  recordScope: string;
  id: number;
}

export class AddressTranslationDto {
  cityId: number;
  cityName: string;
  stateId: number;
  stateName: string;
  countryId: number;
  countryName: string;
  languageIsoCode: string;
  twoLetterIsoCode: string;
  countryTwoLetterIsoCode: string;
}

export class CityDto {
  twoLetterIsoCode: string;
  threeLetterIsoCode: string;
  countryTwoLetterIsoCode: string;
  countryThreeLetterIsoCode: string;
  externalCode: string;
  parentSubdivisionId: number;
  subdivisionTypeId: number;
  officialNatural: string;
  officialLegal: string;
  id: number;
}
