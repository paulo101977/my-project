import { ReservationStatusEnum } from './../reserves/reservation-status-enum';

export class ReservationSearchDto {
    adultsCount: number;
    billingAccountId: any;
    businessSourceId: number;
    businessSourceName: any;    // TODO: Origem da reserva
    childCount: number;
    client: string;
    companyClientId: any;
    companyClientName: any;
    creationTime: Date;
    arrivalDate: Date;
    currencySymbol: any;
    departureDate: Date;
    cancellationDate: Date;
    estimatedArrivalDateGuest: Date;
    estimatedDepartureDateGuest: Date;
    groupName: string;
    guestDocument: string;
    guestId: number;
    guestIsIncognito = false;
    guestLanguage: string;
    guestName: string;
    guestEmail: any;
    guestPhoneNumber: string;
    guestReservationItemId: number;
    guestReservationItemStatusId: number;
    guestReservationItemStatusName: string;
    guestTypeId: number;
    guestTypeName: any;
    id: number;
    marketSegmentId: number;
    marketSegmentName: any;     // TODO: Segmento da reserva
    mealPlanTypeId: number;
    ratePlanId: any;
    manualRate: any;
    ratePlanName: any;
    totalARR: any;            // TODO: Valor Tarifa
    receivedRoomTypeAbbreviation: string;
    receivedRoomTypeId: number;
    receivedRoomTypeName: string;
    requestedRoomTypeAbbreviation: string;
    requestedRoomTypeId: number;
    requestedRoomTypeName: string;
    reservationBy: string;
    reservationCode: string;
    reservationItemCode: string;
    reservationItemId: number;
    reservationItemStatusId: ReservationStatusEnum;
    reservationItemStatusName: string;
    roomNumber: any;
    tenantId: string;
    total: number;
    deadline: any;
    checkInDate: Date;
    checkOutDate: Date;
    paymentTypeId: number;
    isShow: boolean;
    statusColor?: object;
    roomId?: number;
}
