// Models
import { CreditCard } from './../credit-card';

export class ReservationConfirmationDto {
  id: string;
  paymentTypeId: number;
  billingClientId: string;
  billingClientName: string;
  ensuresNoShow: boolean;
  isConfirmed: boolean;
  value: number;
  deadline: any;
  plastic: CreditCard;
  launchDaily: boolean;
}
