export class RatePlanUhTypeDto {
  id: number;
  name: string;
  abbreviation: string;
  roomTypeId: number;
}
