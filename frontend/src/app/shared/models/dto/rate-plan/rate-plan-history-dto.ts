import { RateHistoryHeaderDto } from '../../../../revenue-management/rate-history/models/rate-history-header-dto';
import { PropertyBaseRateHistory } from './property-base-rate-history';

export class RatePlanHistoryDto extends RateHistoryHeaderDto {
  id: string;
  propertyId: number;
  initialDate: string;
  finalDate: string;
  currencyId: string;
  mealPlanTypeDefaultId: string;
  ratePlanId: string;
  sunday: boolean;
  monday: boolean;
  tuesday: boolean;
  wednesday: boolean;
  thursday: boolean;
  friday: boolean;
  saturday: boolean;
  tenantId: string;
  currencyName: string;
  creatorUserName: string;
  creationTime: string;
  appliedTo: Array<string>;
  propertyBaseRateHistoryList: Array<PropertyBaseRateHistory>;
  rateRows: Array<PropertyBaseRateHistory>;
  mealPlanRows: Array<PropertyBaseRateHistory>;
}
