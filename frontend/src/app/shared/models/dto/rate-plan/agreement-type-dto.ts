export class AgreementTypeDto {
  id: number;
  agreementTypeName: string;
  ratePlanList: Array<any>;
}

export enum AgreementTypeEnum {
  B2C = 1,
  B2B = 2,
}
