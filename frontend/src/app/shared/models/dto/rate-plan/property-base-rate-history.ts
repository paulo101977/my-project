export class PropertyBaseRateHistory {
  id: string;
  currencyName: string;
  mealPlanTypeName: string;
  roomTypeName: string;
  propertyId: number;
  mealPlanTypeDefault: number;
  mealPlanTypeId: number;
  roomTypeId: number;
  pax1Amount: number;
  pax2Amount: number;
  adultMealPlanAmount: number;
  currencyId: string;
  currencySymbol: string;
  ratePlanId: string;
  tenantId: string;
  propertyBaseRateHeaderHistoryId: string;
  level: number;
  creatorUserName: string;
}
