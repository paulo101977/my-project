export class RatePlanUhTypeAssociationDto {
  id: string;
  roomTypeId: number;
  percentualAmmount: number;
  isDecreased: boolean;
  publishOnline: boolean;
  ratePlanId: string;
}
