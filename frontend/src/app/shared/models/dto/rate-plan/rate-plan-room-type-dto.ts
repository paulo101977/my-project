export class RatePlanRoomTypeDto {
  'id': string;
  'name': string;
  'abbreviation': string;
}
