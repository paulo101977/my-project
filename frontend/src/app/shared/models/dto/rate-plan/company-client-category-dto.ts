export class CompanyClientCategoryDto {
  'id': string;
  'propertyCompanyClientCategoryName': string;
  'propertyId': number;
}
