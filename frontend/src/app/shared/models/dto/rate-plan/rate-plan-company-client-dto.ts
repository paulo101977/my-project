export class RatePlanCompanyClientDto {
  id: string;
  companyClientId: string;
  ratePlanId: string;
}
