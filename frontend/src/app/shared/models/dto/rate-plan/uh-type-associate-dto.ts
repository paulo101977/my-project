import { RatePlanUhTypeDto } from './rate-plan-uh-type-dto';

export class UhTypeAssociateDto extends RatePlanUhTypeDto {
  roomTypeId: number;
  roomTypeName: string;
  percentualAmmount: number;
  isDecreased: boolean;
  publishOnline: boolean;
}
