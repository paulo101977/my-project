export class CompanyClientDto {
  'id': string;
  'name': string;
  'type': string;
  'document': string;
  'email': string;
  'phone': string;
  'homePage': string;
  'isActive': boolean;
  'clientCategoryId': string;
  'clientCategoryName': string;
}
