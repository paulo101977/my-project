export class RatePlanComissionDto {
  id: string;
  ratePlanId: number;
  billingItemId: number;
  billingItemName: string;
  commissionPercentualAmount: number;
}
