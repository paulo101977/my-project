// Models
import { RateHistoryHeaderDto } from '../../../../revenue-management/rate-history/models/rate-history-header-dto';

export class RatePlanDto {
  id: string;
  agreementTypeId: number;
  agreementName: string;
  startDate: string;
  endDate: string;
  propertyId: number;
  mealPlanTypeId: number;
  rateTypeId: number;
  marketSegmentId: number;
  isActive: boolean;
  publishOnline: boolean;
  currencyId: string;
  rateNet: boolean;
  ratePlanCommissionList: Array<any>;
  ratePlanCompanyClientList: Array<any>;
  ratePlanRoomTypeList: Array<any>;
  propertyBaseRateHeaderHistoryList: Array<RateHistoryHeaderDto>;
  distributionCode: string;
  description: string;
  roundingTypeId: number;
}
