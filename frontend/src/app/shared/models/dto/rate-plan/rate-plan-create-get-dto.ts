// Models
import { Currency } from '../../revenue-management/currency-base-rate';
import { AgreementTypeDto } from './agreement-type-dto';
import { MealPlanType } from '../../revenue-management/meal-plan-type';
import { ItemsResponse } from '../../backend-api/item-response';
import { MarketSegment } from '../market-segment';

export class RatePlanCreateGetDto {
  currencyList: Array<Currency>;
  agreementTypeList: Array<AgreementTypeDto>;
  propertyMealPlanTypeList: Array<MealPlanType>;
  marketSegmentList: Array<MarketSegment>;
  propertyRateTypeList: Array<any>; // todo criar modelo de rateType
}

export class RatePlanCreateGetResponseDTO {
  currencyList: ItemsResponse<Currency>;
  agreementTypeList: ItemsResponse<AgreementTypeDto>;
  propertyMealPlanTypeList: ItemsResponse<MealPlanType>;
  marketSegmentList: ItemsResponse<MarketSegment>;
  propertyRateTypeList: ItemsResponse<any>; // todo criar modelo de rateType
}
