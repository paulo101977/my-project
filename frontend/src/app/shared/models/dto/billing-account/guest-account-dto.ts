export class GuestAccountDto {
  companyClientId: string;
  reservationId: number;
  reservationItemId: number;
  guestReservationItemId: number;
  billingAccountTypeId: number;
  startDate: string;
  endDate: string;
  statusId: number;
  isMainAccount: boolean;
  id: string;
  propertyId: number;
}
