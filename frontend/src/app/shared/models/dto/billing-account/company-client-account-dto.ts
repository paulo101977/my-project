// Models
import { Location } from './../../location';
import { DocumentForLegalAndNaturalPerson } from 'app/shared/models/document';

export class CompanyClientAccountDto {
  companyClientId: string;
  reservationId: number;
  reservationItemId: number;
  reservationItemCode: string;
  guestReservationItemId: number;
  billingAccountTypeId: number;
  startDate: string;
  endDate: string;
  statusId: number;
  isMainAccount: boolean;
  id: string;
  companyId: number;
  personId: string;
  propertyId: number;
  personType: string;
  shortName: string;
  tradeName: string;
  email: string;
  homePage: string;
  phoneNumber: string;
  cellPhoneNumber: string;
  isActive: boolean;
  documentList: Array<DocumentForLegalAndNaturalPerson>;
  locationList: Array<Location>;
  companyClientContactPersonList: string;
  isIssuingBank: boolean;
}
