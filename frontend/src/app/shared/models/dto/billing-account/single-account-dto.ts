// Models
import { CompanyClientAccountDto } from './company-client-account-dto';

export class SingleAccountDto {
  propertyId: number;
  companyClientId: string;
  reservationId: number;
  reservationItemId: number;
  guestReservationItemId: number;
  billingAccountTypeId: number;
  startDate: string;
  endDate: string;
  statusId: number;
  isMainAccount: boolean;
  id: string;
  marketSegmentId: number;
  businessSourceId: number;
  accountBalance: number;
  companyClient: CompanyClientAccountDto;
  isIssuingBank: boolean;
}
