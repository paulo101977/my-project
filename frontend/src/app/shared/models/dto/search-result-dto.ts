import { SearchResultCategoryDto } from './search-result-category-dto';

export class SearchResultDto {
  public searchResultCategory: Array<SearchResultCategoryDto>;
}
