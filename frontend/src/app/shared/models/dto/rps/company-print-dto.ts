import { ContactInformationDto } from '../contact-information-dto';
import { CompanyClientContactDto } from './company-client-contact-dto';
import { ClientDocumentDto } from '../client/client-document-dto';
import { LocationDto } from '../location-dto';

export class CompanyPrintDto {
  companyId: number;
  personId: string;
  propertyId: number;
  personType: string;
  shortName: string;
  tradeName: string;
  email: string;
  homePage: string;
  phoneNumber: string;
  cellPhoneNumber: string;
  isActive: boolean;
  isAcquirer: boolean;
  dateOfBirth: Date;
  countrySubdivisionId: number;
  propertyCompanyClientCategoryId: string;
  documentList: Array<ClientDocumentDto>;
  locationList: Array<LocationDto>;
  companyClientContactPersonList: Array<CompanyClientContactDto>;
  id: string;
}
