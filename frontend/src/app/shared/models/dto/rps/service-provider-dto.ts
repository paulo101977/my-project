import { LocationDto } from '../location-dto';

export class ServiceProviderDto {
  cnpj: string;
  inscEst: string;
  inscMun: string;
  companyId: number;
  propertyTypeId: number;
  brandId: number;
  name: string;
  contactFirstName: string;
  contactLastName: string;
  contactEmail: string;
  contactPhone: string;
  contactWebSite: string;
  shortName: string;
  tradeName: string;
  propertyUId: string;
  propertyContactPersonList: Array<any>;
  propertyGuestPrefsList: Array<any>;
  propertyGuestTypeList: Array<any>;
  reservationList: Array<any>;
  locationList: Array<LocationDto>;
  roomTypeList: Array<any>;
  roomList: Array<any>;
  propertyType: string;
  company: string;
  brand: string;
  totalOfRooms: number;
  id: string;
}
