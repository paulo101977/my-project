import { BillingAccountWithEntry } from '../../billing-account/billing-account-with-entry';

export class AccountEntryDto {
  id: string;
  mainBillingAccountId: string;
  isHolder: boolean;
  holderName: string;
  arrivalDate: Date;
  departureDate: Date;
  accountTypeName: string;
  accountTypeId: number;
  statusName: string;
  statusId: number;
  total: number;
  groupKey: string;
  periodOfStay: number;
  averageSpendPerDay: number;
  reservationItemCode: string;
  reservationCode: string;
  roomNumber: string;
  roomTypeName: string;
  billingClientId: number;
  billingClientName: number;
  reservationItemArrivalDate: Date;
  reservationItemDepartureDate: Date;
  billingAccounts: Array<BillingAccountWithEntry>;
}
