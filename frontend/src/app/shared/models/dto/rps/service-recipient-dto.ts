import { GuestPrintDto } from './guest-print-dto';
import { CompanyPrintDto } from './company-print-dto';

export class ServiceRecipientDto {
  company: CompanyPrintDto;
  guest: GuestPrintDto;
}
