import { BillingInvoicePropertySupportedTypeDto } from './billing-invoice-property-supported-type-dto';

export class ParameterDocumentDto {
  id: string;
  billingInvoicePropertySeries: string;
  billingInvoiceModelId: number;
  lastNumber: string;
  isIntegrated: boolean;
  propertyId: number;
  isActive: boolean;
  emailAlert: string;
  description: string;
  childBillingInvoicePropertySeries: string;
  childLastNumber: string;
  allowsCancel: boolean;
  daysForCancel: number;
  billingInvoicePropertySupportedTypeList: Array<BillingInvoicePropertySupportedTypeDto>;
}
