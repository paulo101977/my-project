import { InvoiceDto } from '../invoice/invoice-dto';
import { AccountEntryDto } from './account-entry-dto';
import { ServiceProviderDto } from './service-provider-dto';
import { ServiceRecipientDto } from './service-recipient-dto';
import { InvoicePrintDto } from '../invoice/invoice-print-dto';

export class RpsPrintDto {
  id: string;
  invoice: InvoicePrintDto;
  accountEntries: AccountEntryDto;
  serviceProvider: ServiceProviderDto;
  serviceRecipient: ServiceRecipientDto;
}
