export class SubdivisionServiceDto {
  id: string;
  code: string;
  description: string;
}
