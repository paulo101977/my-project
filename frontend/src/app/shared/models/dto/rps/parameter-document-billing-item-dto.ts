export class ParameterDocumentBillingItemDto {
  billingItemId: number;
  billingItemTypeId: number;
  code: string;
  billingItemName: string;
  billingInvoiceModelName: string;
  billingInvoicePropertySeries: string;
  coutrySubdivisionId: number;
}
