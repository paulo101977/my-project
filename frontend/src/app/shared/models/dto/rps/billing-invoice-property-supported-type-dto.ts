export class BillingInvoicePropertySupportedTypeDto {
  id: string;
  propertyId: number;
  billingItemId: number;
  billingItemTypeId: number;
  billingInvoicePropertyId: string;
  countrySubdvisionServiceId: string;
  billingItemName: string;
  billingItemTypeName: string;
  codeService: string;
}
