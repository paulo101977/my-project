export class BillingInvoicePropertyDto {
  id: string;
  billingInvoiceModeName: string;
  billingInvoicePropertySeries: string;
}
