export class CompanyClientContactDto {
  companyClientId: string;
  personId: string;
  name: string;
  occupationId: number;
  occupationName: string;
  phoneNumber: string;
  email: string;
  id: string;
}
