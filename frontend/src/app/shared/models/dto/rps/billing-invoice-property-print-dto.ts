import { BillingInvoicePropertyDto } from './billing-invoice-property-dto';

export class BillingInvoicePropertyPrintDto extends BillingInvoicePropertyDto {
  billingInvoiceModeName: string;
  billingInvoiceModelId: number;
  lastNumber: number;
  isIntegrated: boolean;
  propertyId: number;
  isActive: boolean;
  emailAlert: string;
  description: string;
  billingInvoiceModel: string;
  billingInvoicePropertySupportedTypeList: Array<any>;
  id: string;
}
