export class BillingInvoiceModelDto {
  id: number;
  description: string;
  taxModel: string;
}
