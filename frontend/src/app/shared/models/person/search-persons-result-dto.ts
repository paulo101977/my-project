import { Location } from 'app/shared/models/location';
// Models
import { LocationDto } from './../dto/location-dto';
import { ContactType } from './../contact-type-enum';
import { PersonInformationType } from './person-information-type-enum';

export class SearchPersonsResultDto {
  id: string;
  guestId: number;
  name: string;
  document: string;
  documentType: string;
  contactInformation: Array<ContactInformation>;
  personInformation: Array<PersonInformation>;
  locationList: Array<LocationDto> | Array<Location>;
  companyClientId: string;

  documentTypeId: number;
  firstName: string;
  socialName: string;
  lastName: string;
  fullName: string;
  email: string;
  birthDate: Date;
  personId: string;
  age: number;
  gender: string;
  occupationId: number;
  guestTypeId: number;
  phoneNumber: string;
  mobilePhoneNumber: string;
  nationality: number;
  purposeOfTrip: number;
  arrivingBy: number;
  arrivingFrom: number;
  arrivingFromText: string;
  nextDestination: number;
  nextDestinationText: string;
  additionalInformation: string;
  healthInsurance: string;
  responsibleGuestRegistrationId: string;
  isLegallyIncompetent: true;
  guestReservationItemId: number;
  propertyId: number;
  tenantId: string;
  checkinDate: Date;
  isChild: boolean;
  childAge: number;
  isMain: true;
}

export class ContactInformation {
  type: ContactType;
  typeValue: string;
  value: string;
}

export class PersonInformation {
  type: PersonInformationType;
  typeValue: string;
  value: string;
}
