export enum PersonInformationType {
  nationality = 1,
  socialName = 2,
  language = 3,
}
