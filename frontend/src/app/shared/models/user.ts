export class User {
  id: string;
  userUid: string;
  personId: string;
  tenantId: string;
  userLogin: string;
  preferredLanguage: string;
  preferredCulture: string;
  isActive: boolean;
  isAdmin: boolean;
  name: string;
  email: string;
  lastLink: string;
  roleIdList: any[];

  photoUrl?: string;
  photoBase64?: string;
}
