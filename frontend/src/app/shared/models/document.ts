import { DocumentType } from './document-type';

export class DocumentForLegalAndNaturalPerson {
  documentInformation: string;
  documentType: DocumentType;
  ownerId: string;
  documentTypeId: number;
  id: number;
}
