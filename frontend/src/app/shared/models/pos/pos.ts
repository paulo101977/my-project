import { Fee } from '../billingType/fee';

export class POS {
  name: string;
  billingItemName: string;
  billingItemCategoryName: string;
  billingItemCategoryId: number;
  isActive: boolean;
  propertyId: number;
  id: number;
  integrationCode: string;
  serviceAndTaxList: Fee[];

  constructor() {
    this.serviceAndTaxList = [];
  }
}
