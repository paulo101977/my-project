export class FavoriteModel {
  details: Array<any>;
  id?: string;
  type?: string;
  userId: string;
}
