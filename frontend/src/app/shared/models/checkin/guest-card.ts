import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';

export class GuestCard {
  guest: CheckinGuestReservationItemDto;
  active: boolean;
  isValid: boolean;
  number: number; // Guest number gotten in the guestList
  isSelected: boolean;
  isReadonly?: boolean;
}
