export class Country {
  translationId: number;
  twoLetterIsoCode: string;
  countryTwoLetterIsoCode: string;
  name: string;
  languageIsoCode: string;
  id: number;
}
