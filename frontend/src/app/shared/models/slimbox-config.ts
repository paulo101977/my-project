export class SlimboxConfig {
  showFooter: boolean;
  showHeader: boolean;
  type: string;
}
