export enum ParameterTypeEnum {
  ChildMaxAge = 0,
  Checkin = 1,
  Checkout = 2,
  DailyLauch = 3,
  DefaultCurrency = 4,
  ServiceDaily = 5,
  ServiceUpseling = 6,
  ChildAgeRange1 = 7,
  ChildAgeRange2 = 8,
  ChildAgeRange3 = 9,
  SistemDate = 10,
  AuditStep = 11,
  TimeZone = 12,
  EditMigrated = 15
}
