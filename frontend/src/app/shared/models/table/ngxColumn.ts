export class NgxColumn {
  name: string;
  prop: string;

  constructor(name: string, prop?: string) {
    this.name = name;
    if (prop) {
      this.prop = prop;
    } else {
      this.prop = name;
    }
  }
}
