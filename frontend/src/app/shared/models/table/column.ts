export class Column {
  name: string;
  width: string;
  align: string;

  constructor(name: string, width?: number | string, align?: string) {
    this.name = name;
    if (width == '' || width == null) {
      this.width = 'col';
    } else {
      this.width = 'col-' + width;
    }
    if (align) {
      this.align = align;
    } else {
      this.align = 'left';
    }
  }
}
