export enum AddressType {
  Comercial = 1,
  Entrega = 2,
  Cobranca = 3,
  Residencial = 4,
  Correspondencia = 5,
}
