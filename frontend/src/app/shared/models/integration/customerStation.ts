import { ClientDocumentTypeEnum } from '../dto/client/client-document-types';

export class CustomerStation {
    id: string;
    client: string;
    stationName: string;
    category: string;
    contact: string;
    email: string;
    phone: string;
    isActive: boolean;
}

export class CustomerStationClient {
  clientStationMatrix: string;
  stationName: string;
  socialName: string;
  subsidiaryCategory: string;
  homePage: string;
  isActive: boolean;
  personType: ClientDocumentTypeEnum;
  locationList: Array<any>;
  companyClientContactPersonList: Array<any>;
  documentList: Array<any>;
  id: string;
}
