export class ChannelCode {
  key: number;
  value: string;
  id: number;
}

export class ChannelOrigin {
  key: number;
  value: string;
  id: number;
}

export class ChannelCompanyAssociation {
  channelId: string;
  description: string;
  companyId: string;
  companyDescription: string;
  isActive: boolean;
  id: string;
  channelCodeName: string;
  businessSourceName: string;
}

export class ChannelClientAssociation {
  id: string;
  channelId: string;
  companyClientId: string;
  isActive: boolean;
  companyId: string;
  channelCodeName: string;
  channelCodeId: number;
  channelDescription: string;
  businessSourceId: number;
  businessSourceDescription: string;
}


export class Channel {
  channelCodeId: number;
  description: string;
  businessSourceId: number;
  distributionAmount: number;
  isActive: boolean;
  id: string;
  channelCodeName: string;
  businessSourceName: string;
  code: string;
  costOfDistribution: number;
  origin: string;
  marketSegmentId: number;
}

