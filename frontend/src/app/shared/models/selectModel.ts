export class SelectModel<T> {
  formGroupName: string;
  fieldName: string;
  value: T;
}

export class SelectObjectOption {
  key: any;
  value: any;

  constructor(key?: any, value?: any) {
    this.key = key;
    this.value = value;
  }
}
