export class OptionMenu {
  name: string;
  path: string;
  paramName: string;
  paramValue: any;
}
