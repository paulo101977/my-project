export class ClientContact {
  id: string;
  name: string;
  email: string;
  phoneNumber: string;
}
