// Model
import { ButtonConfig } from './button-config';

export class ConfirmModalDataConfig {
  actionCancelButton: ButtonConfig;
  actionConfirmButton: ButtonConfig;
  textConfirmModal: string;
  alertTextToConfirmModal?: string;
  alertSecondParagraph?: string;
  dates?: string;
}
