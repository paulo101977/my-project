export class BillingItemCategory {
  id: number;
  propertyId: number;
  standardCategoryId: number;
  isActive: boolean;
  categoryName: string;
  categoryFixedName: string;
}
