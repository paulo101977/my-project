export class GetAllBillingItemTax {
  id: number;
  isActive: boolean;
  billingItemName: string;
  billingItemCategoryName: string;
  billingItemTaxId: string;
  billingItemServiceId: string;
}
