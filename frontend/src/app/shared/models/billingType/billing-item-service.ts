// Models
import { BillingItemServiceTaxAssociate } from './billing-item-service-tax-associate';

export class BillingItemService {
  id: number;
  propertyId: number;
  name: string;
  isActive: boolean;
  billingItemCategoryId: number;
  billingItemCategoryName: string;
  serviceAndTaxList: Array<BillingItemServiceTaxAssociate>;
  integrationCode: string;
}
