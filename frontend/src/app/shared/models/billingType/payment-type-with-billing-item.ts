export class PaymentTypeWithBillingItem {
  public id: number;
  public billingItemId: number;
  public paymentTypeName: string;
}
