export class BillingItemTaxServiceAssociate {
  id: string;
  billingItemServiceId: number;
  name: string;
  isActive: boolean;
  taxPercentage: number;
  beginDate: Date;
  endDate: Date;
  categoryName: string;
}
