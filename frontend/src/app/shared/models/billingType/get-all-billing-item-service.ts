export class GetAllBillingItemService {
  id: number;
  isActive: boolean;
  billingItemName: string;
  billingItemCategoryName: string;
}
