export class Fee {
  identify: string;
  id: string;
  billingItemTaxId: number;
  billingItemServiceId: number;
  name: string;
  isActive: boolean;
  taxPercentage: number;
  beginDate: Date;
  endDate: Date;
  categoryName: string;
}
