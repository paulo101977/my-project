export class BillingItemPlasticBrandCompanyClient {
  public acquirerId: number;
  public plasticBrandPropertyId: string;
  public plasticBrandId: number;
  public plasticBrandName: string;
  public acquirerName: string;
  public plasticBrandFormatted: string;
  public id: number;
  public maximumInstallmentsQuantity: number;
}
