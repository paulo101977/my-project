export class PaymentTypeWithouBePlasticCardOrDebit {
  id: number;
  propertyId: number;
  paymentTypeId: number;
  description: string;
  isActive: boolean;
  integrationCode: string;
}
