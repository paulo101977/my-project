// Models
import { BillingItemTaxServiceAssociate } from './billing-item-tax-service-associate';

export class BillingItemTax {
  id: number;
  propertyId: number;
  billingItemCategoryId: number;
  name: string;
  isActive: boolean;
  serviceAndTaxList: Array<BillingItemTaxServiceAssociate>;
  integrationCode: string;
}
