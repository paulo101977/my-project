export class BillingAccountOwner {
  public ownerId: string;
  public ownerName: string;
  public isCompany: boolean;
}
