// Models
import { BillingAccountItemEntry } from './billing-account-item-entry';

export class BillingAccountStatement {
  public billingItemList: Array<BillingAccountItemEntry>;
  public billingItemTotal: number;

  constructor() {
    this.billingItemList = new Array<BillingAccountItemEntry>();
  }
}
