export class BillingAccountItemReverse {
  public billingAccountId: string;
  public originalBillingAccountItemId: string;
  public reasonId: number;
  public observation: string;
  public propertyId: number;
}
