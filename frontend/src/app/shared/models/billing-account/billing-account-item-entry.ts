// Models
import { BillingAccountItemTypeEnum } from './billing-account-item-type-enum';

export class BillingAccountItemEntry {
    public billingAccountItemId: string;
    public billingAccountItemDate: Date;
    public dateParameter: Date;
    public discount: number;
    public total: number;
    public billingItemName: string;
    public billingItemId: number;
    public billingItemCategoryName: string;
    public billingAccountItemTypeId: BillingAccountItemTypeEnum;
    public billingAccountItemTypeName: string;
    public isChecked: boolean;
    public reasonName: string;
    public billingInvoiceId: string;
    public integrationCode: string;
    public partialBillingAccountItemId: string;
    public totalWithoutRate: number;
    public percentageRate: number;
    public plasticBrandFormatted: string;
    public isProduct?: boolean;
    public currencySymbol: string;

    public billingAccountChildrenItems: Array<BillingAccountItemEntry>;

    constructor() {
        this.billingAccountChildrenItems = new Array<BillingAccountItemEntry>();
    }
}
