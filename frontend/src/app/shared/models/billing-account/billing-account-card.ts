// Models
import { BillingAccountSidebar } from './billing-account-sidebar';
import { BillingAccountWithEntry } from 'app/shared/models/billing-account/billing-account-with-entry';

export class BillingAccountCard {
  public isActive: boolean;
  public billingAccountSidebar: BillingAccountSidebar;
  public billingAccounts: Array<BillingAccountWithEntry> = [];
}
