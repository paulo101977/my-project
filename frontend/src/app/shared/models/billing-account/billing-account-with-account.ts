// Models
import { BillingAccountTypeEnum } from './billing-account-type-enum';
import { BillingAccountWithEntry } from './billing-account-with-entry';

export class BillingAccountWithAccount {
  public mainBillingAccountId: string;
  public holderName: string;
  public isHolder: boolean;
  public arrivalDate: Date;
  public departureDate: Date;
  public accountTypeId: BillingAccountTypeEnum;
  public accountTypeName: string;
  public statusId: number;
  public statusName: string;
  public averageSpendPerDay: number;
  public total: number;
  public groupKey: string;
  public periodOfStay: number;
  public reservationItemCode: string;
  public reservationCode: string;
  public roomNumber: string;
  public roomTypeName: string;
  public id: string;
  public currencySymbol: string;

  public billingAccounts: Array<BillingAccountWithEntry>;

  constructor() {
    this.billingAccounts = new Array<BillingAccountWithEntry>();
  }
}
