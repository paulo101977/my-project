export class GetAllBillingAccount {
  public billingAccountId: string;
  public reservationCode: string;
  public accountTypeName: string;
  public billingAccountName: string;
  public roomTypeName: string;
  public roomNumber: string;
  public arrivalDate: Date;
  public departureDate: Date;
  public balance: number;
  public balanceFormatted: string;
  public groupKey: string;
  public holderName: string;
}
