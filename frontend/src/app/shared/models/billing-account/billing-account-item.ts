export class BillingAccountItem {
  public propertyId: number;
  public billingAccountId: string;
  public billingItemId: number;
  public amount: number;
  public id: string;
  public nsu?: string;
  public checkNumber?: string;
  public installmentsQuantity?: number;
  public serviceDescription?: string;
  public externalId?: number | string;
  public billingAccountItemDate: Date;
  public billingItemName: string;
  public creditAmount: number;
  public billingInvoiceId: string;
}
