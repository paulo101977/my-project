export enum BillingAccountStatusEnum {
  Open = 1,
  Closed = 2,
  Pending = 3
}
