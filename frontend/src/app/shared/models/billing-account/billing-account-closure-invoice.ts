import {BillingInvoiceTypeEnum} from 'app/shared/models/dto/invoice/billing-invoice-type-enum';

export class BillingAccountClosureInvoice {
  invoiceId: string;
  type: BillingInvoiceTypeEnum;
}
