// Models
import { BillingAccountForClosure } from './billing-account-for-closure';
import { BillingAccountOwner } from './billing-account-owner';

export class GetAllBillingAccountForClosure {
  billingAccountForClosureList: Array<BillingAccountForClosure>;
  id: string;
  ownerList: Array<BillingAccountOwner>;
  total: number;
}
