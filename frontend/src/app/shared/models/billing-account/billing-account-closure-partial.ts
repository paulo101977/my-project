// Models
import { BillingAccountItem } from './billing-account-item';

export class BillingAccountClosurePartial {
  billingAccountItemList: Array<string>;
  billingAccountList: Array<string>;
  billingAccountName: string;
  propertyId: number;
  credit: BillingAccountItem;
  id: string;
}
