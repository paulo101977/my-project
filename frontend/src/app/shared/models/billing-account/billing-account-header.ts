export class BillingAccountHeader {
  public reservationCode: string;
  public reservationItemCode: string;
  public internalComments: string;
  public externalComments: string;
  public quantityComments: number;
  public priceDayDaily: number;
  public priceTotal: number;
  public statusName: string;
  public reservationItemStatusId: number;
  public roomNumber: string;
  public reservationItemId: number;
  public groupKey: string;
  public billingAccountTypeId: number;
  public allClosed: boolean;
  public currencyId: string;
  public currencySymbol: string;
}
