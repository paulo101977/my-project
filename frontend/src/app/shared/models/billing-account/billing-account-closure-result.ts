import {BillingAccountClosureInvoice} from 'app/shared/models/billing-account/billing-account-closure-invoice';

export class BillingAccountClosureResult {
  public accountBalance: number;
  public id: string;
  public invoiceList: Array<BillingAccountClosureInvoice>;
}
