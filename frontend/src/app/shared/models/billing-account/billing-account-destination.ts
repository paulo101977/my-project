// Models
import { BillingAccountItemEntry } from './billing-account-item-entry';

export class BillingAccountDestination {
  billingAccountId: string;
  billingAccountItemName: string;
  total: number;
  billingAccountItems: Array<BillingAccountItemEntry>;
  accountTypeId: number;
  isHolder: boolean;
  holderName: string;
  departureDate: Date;
  arrivalDate: Date;
  accountTypeName: string;
  statusId: number;
  statusName: string;

  constructor() {
    this.billingAccountItems = new Array<BillingAccountItemEntry>();
  }
}
