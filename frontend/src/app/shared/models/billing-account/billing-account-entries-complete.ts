// Models
import { BillingAccountWithAccount } from './billing-account-with-account';

export class BillingAccountEntriesComplete {
  total: number;
  periodOfStay: number;
  averageSpendPerDay: number;
  billingAccountWithAccounts: Array<BillingAccountWithAccount>;
}
