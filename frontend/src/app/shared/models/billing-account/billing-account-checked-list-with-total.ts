// Models
import { BillingAccountForClosure } from './billing-account-for-closure';

export class BillingAccountCheckedListWithTotal {
  billingAccountCheckedList: Array<string>;
  total: number;
}
