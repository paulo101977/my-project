// Models
import { BillingAccountItemTypeEnum } from './billing-account-item-type-enum';

export class BillingAccountItemStatement {
  public billingAccountItemDate: Date;
  public billingAccountItemName: string;
  public amount: number;
  public billingAccountItemTypeId: BillingAccountItemTypeEnum;
}
