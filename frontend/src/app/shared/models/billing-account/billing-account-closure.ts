// Models
import { BillingAccountItem } from './billing-account-item';

export class BillingAccountClosure {
  public billingAccountList: Array<string>;
  public propertyId: number;
  public credit: BillingAccountItem;
  public id: string;
  public ownerDestination: string;
  public isCompany: boolean;
  public reservationId: number;
  public reservatiomItemId: number;
  public billingAccountName: string;
  public twoLetterIsoCodeCountry: string;
}
