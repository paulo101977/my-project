// Models
import { BillingAccountItemTransferChild } from './billing-account-item-transfer-child';

export class BillingAccountItemTransfer {
  public id: string;
  public billingAccountItemIdDestination: string;
  public billingAccountItemTransferList: Array<BillingAccountItemTransferChild>;

  constructor() {
    this.billingAccountItemTransferList = new Array<BillingAccountItemTransferChild>();
  }
}
