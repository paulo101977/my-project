// Models
import { BillingAccountItemEntry } from './billing-account-item-entry';
import { InvoiceDto } from '../dto/invoice/invoice-dto';

export class BillingAccountWithEntry {
  billingAccountId: string;
  billingAccountItemName: string;
  isBlocked: boolean;
  billingAccountItems: Array<BillingAccountItemEntry>;
  startDate: Date;
  statusId: number;
  statusName: string;
  invoices: Array<InvoiceDto>;
  total: number;
  totalWithoutRate: number;
  percentageRate: number;


  constructor() {
    this.billingAccountItems = new Array<BillingAccountItemEntry>();
  }
}
