export class GetAllBillingAccountOpen {
  public reservationCode: string;
  public accountTypeId: number;
  public accountTypeName: string;
  public billingAccountName: string;
  public roomTypeName: string;
  public roomNumber: string;
  public balance: number;
  public balanceFormatted: string;
  public billingAccountId: string;
  public statusId: number;
  public id: string;
}
