export class BillingAccountForClosure {
  id: string;
  reservationId: number;
  reservatiomItemId: number;
  guestReservatiomItemId: number;
  roomId: number;
  roomNumber: string;
  billingAccountTypeId: number;
  billingAccountItemName: string;
  isHolder: boolean;
  statusId: number;
  total: number;
  isCompany: boolean;
  ownerName: string;
  ownerId: string;
  isChecked: boolean;
}
