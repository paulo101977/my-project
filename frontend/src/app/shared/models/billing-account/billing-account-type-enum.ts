export enum BillingAccountTypeEnum {
  Sparse = 1,
  Guest = 2,
  Company = 3,
  GroupAccount = 4,
}
