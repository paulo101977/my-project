// Models
import { BillingAccountTypeEnum } from './billing-account-type-enum';

export class BillingAccountSidebar {
  public billingAccountId: string;
  public holderName: string;
  public isHolder: boolean;
  public arrivalDate: Date;
  public departureDate: Date;
  public price: number;
  public accountTypeId: BillingAccountTypeEnum;
  public isClosed: boolean;
  public guestReservationItemStatusId: number;
  public guestReservationItemStatusName: string;
  public billingInvoicePendingStatusId: number;
  public billingInvoiceErrorStatusId: number;
  public billingInvoiceIssuedStatusId: number;
  public currencyId: string;
  public currencySymbol: string;
}
