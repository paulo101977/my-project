export enum PropertyStatusEnum {
  register = 13,
  contract = 14,
  training = 15,
  production = 19,
  blocked = 20,
  unblocked = 21
}
