import { DocumentForLegalAndNaturalPerson } from './document';
import { GuestState } from 'app/shared/models/reserves/guest';

export class GuestCardRoomlist {
  guestReservationItemId: number;
  reservationItemId: number;
  guestId: number;
  personId: number;
  guestName: string;
  preferredName: string;
  childAge: string;
  guestEmail: string;
  type: number;
  isIncognito: boolean;
  isChild: boolean;
  isMain: boolean;
  guestCitizenship: string;
  language: string;
  uHPreferably: string;
  lastUH: string;
  lastDailyValue: number;
  pctDailyRate: number;
  remarks: string;
  guestDocument: DocumentForLegalAndNaturalPerson;
  guestDocumentTypeId: string;
  guestStatusId: number;
  completeAddress: string;
  checkinDate: Date;
  checkoutDate: Date;
  estimatedArrivalDate: Date;
  estimatedDepartureDate: Date;
  periodOfStay: any;
  showDetails: boolean;
  state: GuestState;
  lastState: GuestState;
}
