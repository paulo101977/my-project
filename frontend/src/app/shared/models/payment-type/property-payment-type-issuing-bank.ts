// Models
import { PropertyPaymentTypeIssuingBankDetail } from './property-payment-type-issuing-bank-detail';

export class PropertyPaymentTypeIssuingBank {
  public id: string;
  public propertyId: number;
  public paymentTypeId: number;
  public issuingBankId: string;
  public description: string;
  public isActive: boolean;
  public propertyPaymentTypeIssuingBankDetailList: Array<PropertyPaymentTypeIssuingBankDetail>;

  constructor() {
    this.propertyPaymentTypeIssuingBankDetailList = new Array<PropertyPaymentTypeIssuingBankDetail>();
  }
}
