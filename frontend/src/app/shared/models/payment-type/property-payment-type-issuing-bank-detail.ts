// Models
import { PropertyPaymentTypeIssuingBankDetailCondition } from './property-payment-type-issuing-bank-detail-condition';

export class PropertyPaymentTypeIssuingBankDetail {
  public id: string;
  public billingItemPaymentTypeId: number;
  public plasticBrandPropertyId: string;
  public plasticBrandId: number;
  public acquirerId: string;
  public maximumInstallmentsQuantity: number;
  public isActive: boolean;
  public billingItemPaymentTypeDetailConditionList: Array<PropertyPaymentTypeIssuingBankDetailCondition>;
  // Only View
  public acquirerName: string;
  public plasticBrandPropertyName: string;

  constructor() {
    this.billingItemPaymentTypeDetailConditionList = new Array<PropertyPaymentTypeIssuingBankDetailCondition>();
  }
}

export class PropertyPaymentTypeIssuingBankListDetail {
  public acquirerId: string;
  public paymentTypeName: string;
  public paymentTypeId: number;
  public isActive: boolean;
  public acquirerName: string;
  public billingItemPaymentTypeDetailConditionList: Array<PropertyPaymentTypeIssuingBankDetailCondition>;
}
