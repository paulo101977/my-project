// Models
import { CompanyClientAccount } from './../dto/company-client-account-dto';
import { PropertyPaymentTypeIssuingBankDetail } from './property-payment-type-issuing-bank-detail';

export class PaymentType {
  id: number;
  paymentTypeName: string;
}

export class GetAllPaymentTypeDto {
  id: number;
  propertyId: number;
  paymentTypeId: number;
  acquirerId: string;
  description: string;
  isActive: boolean;
  acquirer: CompanyClientAccount;
  paymentTypeName: string;
  acquirerName: string;
  billingItemPaymentTypeDetailList: Array<PropertyPaymentTypeIssuingBankDetail>;
  integrationCode: string;
}
