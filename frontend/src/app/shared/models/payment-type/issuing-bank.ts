export class IssuingBank {
  companyId: number;
  personId: string;
  shortName: string;
  tradeName: string;
  isActive: boolean;
  isIssuingBank: true;
}
