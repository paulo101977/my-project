export class PropertyPaymentTypeIssuingBankDetailCondition {
  public id: string;
  public billingItemPaymentTypeDetailId: string;
  public installmentNumber: number;
  public commissionPct: number;
  public paymentDeadline: number;
}
