export class BudgetCompareConfig {
  actualBudget: number;
  actualCurrencyId: string;
  actualCurrencySymbol: string;

  newBudget: number;
  newCurrencyId: string;
  newCurrencySymbol: string;
}
