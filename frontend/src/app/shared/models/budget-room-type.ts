// Models
import { RoomType } from '../../room-type/shared/models/room-type';

export class BudgetRoomType {
  public dailyPriceList: Map<Date, number>;
  public roomTypeRequested: RoomType;

  constructor() {
    this.dailyPriceList = new Map<Date, number>();
  }
}
