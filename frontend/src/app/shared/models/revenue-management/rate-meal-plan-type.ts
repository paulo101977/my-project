import { MealPlanType } from 'app/shared/models/revenue-management/meal-plan-type';
import {
  BaseRateConfigurationMealPlanType
} from 'app/shared/models/revenue-management/meal-type-fix-rate';

export type RateMealPlanType = MealPlanType & Partial<BaseRateConfigurationMealPlanType>;
