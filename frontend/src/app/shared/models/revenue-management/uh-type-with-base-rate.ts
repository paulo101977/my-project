export class RoomTypeWithBaseRate {
  roomType: string;
  paxOne: string;
  paxTwo: string;
  paxThree: string;
  paxFour: string;
  paxFive: string;
  paxAdditional: string;
  childZeroToFiveYears: string;
  childSixToNineYears: string;
  childTenTotwelveYears: string;
}
