export class PropertyParameter {
  parameterTypeName: string;
  parameterDescription: string;
  featureGroupId: number;
  propertyId: number;
  applicationParameterId: number;
  propertyParameterValue: number;
  propertyParameterMinValue: number;
  propertyParameterMaxValue: number;
  propertyParameterPossibleValues: number;
  isActive: boolean;
  id: string;
}
