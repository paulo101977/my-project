export class CurrencyBaseRateConfiguration {
  hasNext: boolean;
  items: Array<Currency>;
  total: number;
}

export class Currency {
  twoLetterIsoCode: string;
  currencyName: string;
  alphabeticCode: string;
  numnericCode: string;
  symbol: string;
  countrySubdivision: string;
  id: string;
  billingAccountItemList: Array<any>;
  currencyExchangeList: Array<any>;
  propertyBaseRateList: Array<any>;
  propertyCurrencyExchangeList: Array<any>;
  ratePlanList: Array<any>;
  reservationBudgetList: Array<any>;
  exchangeRate: number;
  minorUnit: number;
  countrySubdivisionId: number;
}
