// Models
import { MealPlanType } from 'app/shared/models/revenue-management/meal-plan-type';

export class MealPlanTypeConfiguration {
  hasNext: boolean;
  items: Array<MealPlanType>;
  total: number;
}
