// Models
import { PropertyParameter } from './property-parameter';

export class PropertyParameterConfiguration {
  propertyParameter: Array<PropertyParameter>;
  id: number;
}
