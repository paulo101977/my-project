export class RateCalendarFilter {
  initialDate: string;
  finalDate: string;
  adultCount: number;
  childrenAges?: Array<number>;
  roomTypeId: number;
  currencyId: string;
  mealPlanTypeId: number;
  ratePlanId: string;

  // values used internally passing data through url
  companyClientName: string;
  companyClientId: string;
  date: string;
}
