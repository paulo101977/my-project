// Models
import { RoomTypeParametersConfiguration } from './room-type-parameters-configuration';

export class RoomTypeRateConfirguration {
  hasNext: boolean;
  items: Array<RoomTypeParametersConfiguration>;
  total: number;
}
