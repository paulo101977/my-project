// Models
import { Parameter } from '../parameter';
import { CurrencyBaseRateConfiguration } from './currency-base-rate';
import { MealPlanTypeConfiguration } from './meal-plan-type-configuration';
import { RoomTypeRateConfirguration } from './room-type-rate-confirguration';

export class ConfigurationDataParameters {
  currencyList: CurrencyBaseRateConfiguration;
  mealPlanTypeList: MealPlanTypeConfiguration;
  roomTypeList: RoomTypeRateConfirguration;
  propertyParameterList: Array<Parameter>;
}
