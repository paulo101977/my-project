export class RateCalendarDayDto {
  id: number;
  baseRateAmount: number;
  baseRateWithRatePlanAmount: number;
  date: string;
  dateFormatted: string;
  occupation: number;
  occupiedRooms: number;
  roomTotal: number;
  currencySymbol: string;
  levelCode: string;
  isLevel: boolean;
}
