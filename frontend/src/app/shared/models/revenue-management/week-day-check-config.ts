export class WeekDayCheckConfig {
  inputTextLabel: string;
  inputId: number;
  value: number;
  isChecked: boolean;
}
