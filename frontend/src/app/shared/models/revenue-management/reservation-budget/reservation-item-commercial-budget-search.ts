export class ReservationItemCommercialBudgetSearch {
  propertyId: number;
  reservationItemId: number;
  roomTypeId: number;
  initialDate: Date;
  finalDate: Date;
  adultCount: number;
  childrenAge: Array<number>;
  companyClientId: string;
  mealPlanTypeId: number;
  systemDate: string;
  ratePlanId: string;
}
