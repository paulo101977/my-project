// Models
import { ReservationItemCommercialBudgetDay } from './reservation-item-commercial-budget-day';

export class ReservationItemCommercialBudget {
  currencyId: string;
  roomTypeId: number;
  mealPlanTypeId: number;
  ratePlanId: string;
  companyClientId: string;

  commercialBudgetName: string;
  currencySymbol: string;
  roomTypeAbbreviation: string;
  mealPlanTypeName: string;

  rateVariation: number;
  totalBudget: number;
  order: number;

  mealPlanTypeDefaultId: number;
  mealPlanTypeDefaultName: string;
  isMealPlanTypeDefault: boolean;
  overbooking: boolean;

  commercialBudgetDayList: Array<ReservationItemCommercialBudgetDay>;
}
