import {
  ReservationItemCommercialBudgetConfig
} from 'app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';

export class ReservationBudget {
    id: number;
    reservationBudgetUid: string;
    reservationItemId: number;
    budgetDay: Date;
    baseRate: number;
    childRate: number;
    mealPlanTypeId: number;
    rateVariation: number;
    agreementRate: number;
    previousManualRate: number;
    previousAgreementRate: number;
    manualRate: number;
    separatedRate: number;
    discount: number;
    currencyId: string;
    currencySymbol: any;
    commercialBudgetName: string;
    baseRateChanged: boolean;
    mealPlanTypeBaseRate: number;
    childMealPlanTypeRate: number;
    childAgreementRate: number;
    mealPlanTypeAgreementRate: number;
    childMealPlanTypeAgreementRate: number;
    propertyBaseRateHeaderHistoryId: string;
    originalManualRate: number;
    overbooking: boolean;

  constructor() {
    this.id = 0;
    this.reservationBudgetUid = '00000000-0000-0000-0000-000000000000';
    this.reservationItemId = 0;
  }
}

export class ReservationBudgetDtoList {
  reservationBudgetUid: string;
  reservationItemId: number;
  budgetDay: Date;
  baseRate: number;
  childRate: number;
  mealPlanTypeId: number;
  rateVariation: number;
  agreementRate: number;
  manualRate: number;
  discount: number;
  currencyId: string;
  currencySymbol: string;
  id: number;
}

export class CommercialBudgetList {
  public currencyId: string;
  public roomTypeId: number;
  public mealPlanTypeId: number;
  public ratePlanId: number;
  public companyClientId: number;
  public roomTypeAbbreviation: string;
  public mealPlanTypeDefaultName: string;
  public mealPlanTypeDefaultId: number;
  public mealPlanTypeName: string;
  public commercialBudgetName: string;
  public currencySymbol: string;
  public rateVariation: number;
  public totalBudget: number;
  public commercialBudgetDayList: Array<CommercialBudgetDayList>;
  public id: number;
}

export class CommercialBudgetDayList {
  day: Date;
  baseRateAmount: number;
  childRateAmount: number;
  baseRateWithRatePlanAmount: number;
  currencySymbol: string;
  currencyId: string;
  mealPlanTypeId: number;
  ratePlanId: number;
  id: number;
}

export class ReservationWithBudget {
  reservationBudgetList: Array<ReservationBudget>;
  reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig;
  gratuityTypeId: number;
  commercialBudgetItemKey?: any;
  commercilBudgetItemList?: Array<any>;
}
