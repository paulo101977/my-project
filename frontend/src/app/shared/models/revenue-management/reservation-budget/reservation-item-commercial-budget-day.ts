export class ReservationItemCommercialBudgetDay {
  day: Date;
  baseRateAmount: number;
  baseRateHeaderHistoryId: string;
  mealPlanAmount: number;
  childMealPlanAmount: number;
  childRateAmout: number;
  childRateAmount: number;
  baseRateWithRatePlanAmount: number;
  baseRateAmountWithRatePlan: number;
  childRateAmountWithRatePlan: number;
  mealPlanAmountWithRatePlan: number;
  childMealPlanAmountWithRatePlan: number;
  currencyId: string;
  mealPlanTypeId: number;
  ratePlanId: string;
  currencySymbol: string;
  baseRateChanged: false;
  total: number;
  overbooking: boolean;
}
