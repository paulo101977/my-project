// Models
import { ReservationItemCommercialBudget } from './reservation-item-commercial-budget';
import { ReservationBudget } from './../../revenue-management/reservation-budget/reservation-budget';

export class ReservationItemBudget {
  id: number;
  currencyId: string;
  gratuityTypeId: number;
  mealPlanTypeId: number;
  ratePlanId: string;
  reservationItemId: number;
  reservationItemCode: string;
  commercialBudgetList: Array<ReservationItemCommercialBudget>;
  reservationBudgetDtoList: Array<ReservationBudget>;
  reservationItemStatus: number;
}
