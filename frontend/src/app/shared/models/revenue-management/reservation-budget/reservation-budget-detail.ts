export class ReservationBudgetDetail {
  date: Date;
  offerAmount: number;
  chargedAmount: number;
  discount: number;
  pensionType: number;
}
