// Models
import { ReservationItemsBudget } from './reservation-items-budget';

export class ReservationBudgetCard {
  public isActive: boolean;
  public reservationItem: ReservationItemsBudget;
}
