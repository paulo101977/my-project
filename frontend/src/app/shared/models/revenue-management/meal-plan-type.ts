export class MealPlanType {
  public id: string;
  public mealPlanTypeName: string;
  public propertyMealPlanTypeCode: string;
  public propertyMealPlanTypeName: string;
  public propertyId: number;
  public mealPlanTypeId: number;
  public isActive: boolean;
}
