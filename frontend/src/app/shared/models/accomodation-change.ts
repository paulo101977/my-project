export class AccomodationChange {
  reservationItemId: number;
  reasonId: number;
  roomId: number;
  roomTypeId: number;
  propertyId: number;
}
