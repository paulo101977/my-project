export class MenuOption {
  name: string;
  routerLink?: string;
  queryParams?: any;
  img?: string;
  children?: MenuOption[];
  disabled?: boolean;
}
