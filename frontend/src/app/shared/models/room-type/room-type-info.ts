import { BedType } from '../../../shared/models/bed-type';

export class RoomTypeInfo {
  name: string;
  abbreviation: string;
  maximumRate: number;
  minimumRate: number;
  adultCapacity: number;
  childCapacity: number;
  bedSingle: BedType;
  bedDouble: BedType;
  bedExtra: BedType;
  bedReversible: BedType;
  bedCrib: BedType;
  currencySymbol: string;

  constructor() {
  }
}
