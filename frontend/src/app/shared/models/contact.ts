import { ContactType } from './contact-type-enum';

export class Contact {
  recordIdentification: string;
  contactType: ContactType;
  contactInformation: string;
}
