export class Proforma {
    public reservationId: number;
    public reservationItemId: number;
    public launchTourismTax: boolean;
}
