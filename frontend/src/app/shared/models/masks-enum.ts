export enum Masks {
  HourAndMinute = <any>[/\d/, /\d/, ':', /\d/, /\d/],
  Cnpj = <any>[/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/],
  Cpf = <any>[/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/],
}
