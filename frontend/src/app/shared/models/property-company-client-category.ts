export class PropertyCompanyClientCategory {
  id: number;
  propertyCompanyClientCategoryName: string;
  isActive: boolean;
  propertyId: number;
}
