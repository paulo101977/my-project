import { Action } from './action';
import { HeaderPageEnum } from './header-page-enum';

export class ConfigHeaderPage {
  titlePage: string;
  titleCreateButton: string;
  headerPageEnum: HeaderPageEnum;
  goToCreatePage: Function;
  confirmDelete: Function;
  sizeOfSearchButtonEnum: SizeOfSearchButtonEnum;
  nameOfItemUpdated: string;

  constructor(headerPageEnum: HeaderPageEnum) {
    this.headerPageEnum = headerPageEnum;
  }
}

export enum SizeOfSearchButtonEnum {
  Small = 1,
  Medium = 2,
  Big = 3,
}
