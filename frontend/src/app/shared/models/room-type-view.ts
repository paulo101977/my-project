import { BedType } from './bed-type';

export class RoomTypeView {
  id: number;
  name: string;
  abbreviation: string;
  order: number;
  adultCapacity: number;
  childCapacity: number;
  roomTypeBedTypeList: BedType[];
  bedSingleCount: number;
  bedDoubleCount: number;
  bedReversibleCount: number;
  bedExtraCount: number;
  bedCribCount: number;
  isActive: boolean;
  isInUse: boolean;
}
