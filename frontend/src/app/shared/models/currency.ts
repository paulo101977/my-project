export class Currency {
  countrySubdivisionId: number;
  twoLetterIsoCode: string;
  currencyName: string;
  alphabeticCode: string;
  numnericCode: number;
  minorUnit: number;
  symbol: string;
  exchangeRate: number;
  countrySubdivision: string;
  billingAccountItemList: any[];
  currencyExchangeList: any[];
  propertyBaseRateList: any[];
  propertyCurrencyExchangeList: any[];
  ratePlanList: any[];
  reservationBudgetList: any[];
  id: string;
}
