import { Property } from './property';
import { RoomType } from '../../room-type/shared/models/room-type';

export class Room {
  public id: number;
  public roomNumber: string;
  public floor: string;
  public wing: string;
  public building: string;
  public remarks: string;
  public isActive: boolean;
  public isInUse: boolean;
  public parentRoom: Room;
  public roomType: RoomType;
  public roomTypeId: number;
  public propertyId: number;
  public childRoomIdList: Array<number>;
}
