export class MoveRoomReason {
  reasonName: string;
  reasonCategoryId: number;
  isActive: boolean;
  chainId: number;
  reasonCategoryName: string;
  id: number;
}
