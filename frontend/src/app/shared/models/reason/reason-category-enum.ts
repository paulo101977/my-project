export enum ReasonCategoryEnum {
  Cancellation = 1,
  ReservationScheduling = 2,
  EventDeadline = 3,
  RoomTransference = 4,
  RoomBlocking = 5,
  Discount = 6,
  PurposeOfTrip = 7,
  ArrivingBy = 8,
  Reversal = 9,
  ReopenBillingAccount = 10,
  InvoiceCancellation = 11,
  HousekeepingReview = 12,
  HousekeepingStop = 13,
  CreditNoteIssue = 14
}
