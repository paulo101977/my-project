export class UserInvitation {
  id: string;
  chainId: number;
  propertyId: number;
  brandId: number;
  email: string;
  name: string;
  invitationLink: string;
  invitationDate: string;
  invitationAcceptanceDate: string;
  isActive: true;
  preferredCulture: string;
  preferredLanguage: string;
  chainName: string;
  propertyName: string;
  brandName: string;
}
