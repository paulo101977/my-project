export interface ItemsResponse<t> {
  hasNext: boolean;
  items: Array<t>;
}

export interface ReservationItemsResponse<t> extends ItemsResponse<t> {
  totalGuests: number;
  totalReservations: number;
}

export interface ReportCheckinCheckoutObj<t> {
  listReservationCheckReportDto: ReservationItemsResponse<t>;
  totalReservationCheckReport: any;
}
