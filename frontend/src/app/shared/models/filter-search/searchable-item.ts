import { TableRowTypeEnum } from '../table-row-type-enum';

export class SearchableItems {
  items: Array<any>;
  tableRowTypeEnum: TableRowTypeEnum;
  placeholderSearchFilter: string;
  placeholderActionWordFilter: string;
}
