export enum AvailabilityGridPeriodEnum {
  weekly = 7,
  fortnightly = 15,
  monthly = 30,
}
