// Enums
import { AvailabilityGridPeriodEnum } from './availability-grid-period.enum';

export interface AvailabilityGridFilter {
  initialDate: string;
  period: AvailabilityGridPeriodEnum;
}
