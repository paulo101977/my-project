import { TestBed } from '@angular/core/testing';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumberRangeValidator } from './number-range-validator';

describe('NumberRangeValidator', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      providers: [],
    });
  });

  it('should control return valid in range 0 to 100', () => {
    const control = new FormControl();
    control.setValidators(NumberRangeValidator.range(0, 100));

    control.setValue(40);

    expect(control.valid).toBeTruthy();
  });

  it('should control not return valid in range 0 to 100', () => {
    const control = new FormControl();
    control.setValidators(NumberRangeValidator.range(0, 100));

    control.setValue(140);

    expect(control.valid).toBeFalsy();
  });
});
