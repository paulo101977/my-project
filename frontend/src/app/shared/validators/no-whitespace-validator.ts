import { AbstractControl } from '@angular/forms';

export class NoWhitespaceValidator {
  static match(c: AbstractControl): { [key: string]: any } {
    const isWhitespace = (c.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }
}
