import { AbstractControl, ValidatorFn } from '@angular/forms';

export class PasswordValidator {
  /**
   * Validator that identifies if both passwords are the same
   * the controls must have 'password' and 'confirmPassword' name
   * @param {AbstractControl} ac
   * @returns {ValidatorFn}
   */
  static match(ac: AbstractControl): ValidatorFn {
    const password = ac.get('password').value; // to get value in input tag
    const confirmPassword = ac.get('confirmPassword').value; // to get value in input tag

    if (password != confirmPassword) {
      ac.get('confirmPassword').setErrors({ matchPassword: true });
    } else {
      return null;
    }
  }
}
