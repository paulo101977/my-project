import { AbstractControl, ValidatorFn } from '@angular/forms';


export function equalToFieldValidator(fieldPath: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const selfValue = control.value;
    const fieldValue = control.parent && control.parent.get(fieldPath).value;

    return (!selfValue || !fieldValue) || selfValue === fieldValue
      ? null
      : { equalToField: 'error.fieldIsDifferent' };
  };
}
