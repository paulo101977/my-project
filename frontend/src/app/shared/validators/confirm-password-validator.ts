import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

function clearErrorState(control, errorState) {
  const oldErrors = { ...control.errors };

  if (control.hasError(errorState)) {
    delete oldErrors[errorState];
  }

  return Object.keys(oldErrors).length ? oldErrors : null;
}

export function confirmPasswordValidator(
  originalField,
  confirmField,
  errorMessage = 'error.confirmPassword'
): ValidatorFn {
  return (control: AbstractControl): ValidationErrors => {
    const confirmPasswordControl = control.get(confirmField);
    const password = control.get(originalField).value;
    const confirmPassword = confirmPasswordControl.value;
    const confirmPasswordError = (!password || !confirmPassword || password == confirmPassword)
      ? null
      : {confirmPassword: errorMessage};
    const oldErrors = clearErrorState(confirmPasswordControl, 'confirmPassword');

    const errors = oldErrors || confirmPasswordError
      ? { ...oldErrors, ...confirmPasswordError }
      : null;

    confirmPasswordControl.setErrors(errors);

    return null;
  };
}
