import { Injectable } from '@angular/core';
import { DocumentTypeLegalEnum, DocumentTypeNaturalEnum } from 'app/shared/models/document-type';
import { CountryCodeEnum } from 'app/shared/models/country-code-enum';
import { AbstractControl, FormGroup, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class NifValidatorService {

  private static checkDigit(nif: string): number {
    let c = 0;
    for (let i = 0; i < nif.length - 1; ++i) {
      c += Number(nif[i]) * (10 - i - 1);
    }
    c = 11 - (c % 11);
    return c >= 10 ? 0 : c;
  }

  public static checkNif(nif: string, regex: RegExp): boolean {
    if (!regex.test(nif) || isNaN(+nif)) {
      return false;
    }

    return NifValidatorService.checkDigit(nif) === Number(nif.charAt(nif.length - 1));
  }

  public setNifValidatorRequired(
    form: FormGroup,
    documentControl: AbstractControl,
    countryCode: string,
    documentTypeId: any,
    error: any
  ) {

    documentControl.clearValidators();

    if (( documentTypeId == DocumentTypeNaturalEnum.NIF  || documentTypeId == DocumentTypeLegalEnum.NIF )
      && countryCode == CountryCodeEnum.PT ) {
      documentControl.setValidators(
        [
          Validators.required,
          error
        ]);
    } else {
      documentControl.setValidators([Validators.required]);
    }

    if ( documentControl.value && !documentControl.touched ) {
      documentControl.markAsTouched();
    }

    documentControl.updateValueAndValidity({emitEvent: false});
    form.updateValueAndValidity({emitEvent: false});
  }
}
