import { TestBed } from '@angular/core/testing';

import { NifValidatorService } from 'app/shared/services/nif-validator/nif-validator.service';
import { DocumentTypeLegalEnum, DocumentTypeNaturalEnum } from 'app/shared/models/document-type';
import { FormBuilder, Validators } from '@angular/forms';
import { NfiValidatorPatterns } from 'app/shared/models/nfi-validator-patterns';
import { DATA } from 'app/shared/services/nif-validator/mock-data';

describe('NifValidatorService', () => {
  let service: NifValidatorService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NifValidatorService]
    });

    service = new NifValidatorService();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('on validator Portuguese', () => {

    // true cases
    it('checkNif singular 1', () => {
      expect(
        NifValidatorService.checkNif('122371135',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(true);
    });

    it('checkNif singular 2', () => {
      expect(
        NifValidatorService.checkNif('243661037',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(true);
    });

    it('checkNif singular 3', () => {
      expect(
        NifValidatorService.checkNif('381852113',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(true);
    });

    it('checkNif singular 5', () => {
      expect(
        NifValidatorService.checkNif('593596951',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(true);
    });

    it('checkNif singular 6', () => {
      expect(
        NifValidatorService.checkNif('607000120',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(true);
    });

    it('checkNif singular individual entrepreneur', () => {
      expect(
        NifValidatorService.checkNif('844871796',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(true);
    });

    // errors cases
    it('checkNif Irregular collective person or provisional number', () => {
      expect(
        NifValidatorService.checkNif('951778838',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });

    it('checkNif singular 1 error', () => {
      expect(
        NifValidatorService.checkNif('122371139',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });

    it('checkNif singular 2 error', () => {
      expect(
        NifValidatorService.checkNif('243661737',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });

    it('checkNif singular 3 error', () => {
      expect(
        NifValidatorService.checkNif('391852113',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });

    it('checkNif singular 5 error', () => {
      expect(
        NifValidatorService.checkNif('593196951',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });

    it('checkNif singular 6 error', () => {
      expect(
        NifValidatorService.checkNif('607080120',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });

    it('checkNif singular individual entrepreneur error', () => {
      expect(
        NifValidatorService.checkNif('840871796',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });

    it('checkNif singular generic error', () => {
      expect(
        NifValidatorService.checkNif('123',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });

    it('checkNif singular generic error empty', () => {
      expect(
        NifValidatorService.checkNif('',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });

    it('checkNif singular generic error string', () => {
      expect(
        NifValidatorService.checkNif('abcd',  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });

    it('checkNif singular generic error undefined', () => {
      expect(
        NifValidatorService.checkNif(undefined,  NfiValidatorPatterns.PORTUGUESE_NFI_REGEX)
      ).toEqual(false);
    });
  });

  describe('on setNifValidatorRequired', () => {
    const error = {error: 'myError'};
    let documentControl, form, countryCode, documentTypeId;
    beforeEach(() => {
      form = new FormBuilder().group(DATA.formData);
      documentControl = form.get('document');
      countryCode = 'PT';
      documentTypeId = DocumentTypeNaturalEnum.NIF;

      spyOn(documentControl, 'clearValidators');
      spyOn(documentControl, 'setValidators');
      spyOn(documentControl, 'markAsTouched');
      spyOn(documentControl, 'updateValueAndValidity');
      spyOn(form, 'updateValueAndValidity');
    });

    it('should test when return not PT', () => {
      countryCode = 'FR';

      service.setNifValidatorRequired(
        form,
        documentControl,
        countryCode,
        documentTypeId,
        error
      );

      expect(documentControl.setValidators).toHaveBeenCalledWith([Validators.required]);
      expect(documentControl.clearValidators).toHaveBeenCalled();
      expect(documentControl.updateValueAndValidity).toHaveBeenCalled();
      expect(form.updateValueAndValidity).toHaveBeenCalled();
    });

    it('should test when return PT', () => {
      documentTypeId = DocumentTypeLegalEnum.NIF;

      service.setNifValidatorRequired(
        form,
        documentControl,
        countryCode,
        documentTypeId,
        error
      );

      expect(documentControl.setValidators).toHaveBeenCalledWith([
          Validators.required,
          error,
        ]
      );
      expect(documentControl.clearValidators).toHaveBeenCalled();
      expect(documentControl.updateValueAndValidity).toHaveBeenCalled();
      expect(form.updateValueAndValidity).toHaveBeenCalled();
    });
  });
});
