import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup } from '@angular/forms';
import { BillingInvoicePropertySupportedTypeDto } from '../../models/dto/rps/billing-invoice-property-supported-type-dto';
import { ParameterDocumentDto } from '../../models/dto/rps/parameter-document-dto';

@Injectable({ providedIn: 'root' })
export class ParameterDocumentService {
  public readonly TYPE_SERVICE = 1;
  public readonly TYPE_PDV = 4;
  constructor(private tranlateSerive: TranslateService) {}

  public mapper(
    propertyId: number,
    formDocument: FormGroup,
    billingInvoicePropertySupportedTypeList: Array<BillingInvoicePropertySupportedTypeDto>,
    editParameterDocument?: ParameterDocumentDto,
  ) {
    const parameterDocument = new ParameterDocumentDto();
    if (editParameterDocument) {
      parameterDocument.id = editParameterDocument.id;
    }
    parameterDocument.billingInvoiceModelId = formDocument.get('model').value;
    parameterDocument.billingInvoicePropertySeries = formDocument.get('serie').value;
    parameterDocument.lastNumber = formDocument.get('lastNum').value;
    parameterDocument.emailAlert = formDocument.get('email').value;
    parameterDocument.isIntegrated = formDocument.get('integration').value ? true : false;
    parameterDocument.description = formDocument.get('obs').value;
    parameterDocument.allowsCancel = formDocument.get('allowsCancel').value ? true : false;
    parameterDocument.daysForCancel = formDocument.get('daysForCancel').value;
    parameterDocument.childBillingInvoicePropertySeries = formDocument.get('childSerie').value;
    parameterDocument.childLastNumber = formDocument.get('childLastNum').value;
    parameterDocument.propertyId = propertyId;
    parameterDocument.isActive = true;
    parameterDocument.billingInvoicePropertySupportedTypeList = billingInvoicePropertySupportedTypeList.map(item => {
      const element = new BillingInvoicePropertySupportedTypeDto();
      element.billingItemId = item.billingItemId;
      if (item.billingItemTypeId != this.TYPE_PDV) {
        element.countrySubdvisionServiceId = item.countrySubdvisionServiceId;
      }
      if (item.id) {
        element.id = item.id;
        element.billingInvoicePropertyId = item.billingInvoicePropertyId;
        element.billingItemId = item.billingItemId;
      } else {
        element.propertyId = propertyId;
        element.billingItemTypeId = item.billingItemTypeId;
      }
      return element;
    });

    return parameterDocument;
  }

  public getAllItensFilterByTypeOrItem(seachData: string, allItems: Array<any>): Array<any> {
    let billintItemId;
    if ((this.tranlateSerive.instant('label.service') as String).toLowerCase().indexOf(seachData.toLowerCase()) >= 0) {
      billintItemId = this.TYPE_SERVICE;
    } else if ((this.tranlateSerive.instant('label.pos') as String).toLowerCase().indexOf(seachData.toLowerCase()) >= 0) {
      billintItemId = this.TYPE_PDV;
    }
    return allItems.filter(
      item => item.billingItemName.toLowerCase().indexOf(seachData.toLowerCase()) >= 0 || item.billingItemTypeId == billintItemId,
    );
  }
}
