import { inject, TestBed } from '@angular/core/testing';

import { ParameterDocumentService } from './parameter-document.service';
import { TranslateModule } from '@ngx-translate/core';

describe('ParameterDocumentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [],
    });
  });

  it('should be created', inject([ParameterDocumentService], (service: ParameterDocumentService) => {
    expect(service).toBeTruthy();
  }));
});
