// Angular imports
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
// Vendors
import * as _ from 'lodash';
// Models
import { ReservationBudget } from './../../models/revenue-management/reservation-budget/reservation-budget';
import {
  ReservationItemCommercialBudgetSearch
} from '../../models/revenue-management/reservation-budget/reservation-item-commercial-budget-search';
import { ReservationItemCommercialBudget } from './../../models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import {
  ReservationItemCommercialBudgetDay
} from './../../models/revenue-management/reservation-budget/reservation-item-commercial-budget-day';
import {
  ReservationItemCommercialBudgetConfig
} from '../../../revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';
import { SharedService } from '../shared/shared.service';
import * as moment from 'moment';

@Injectable({providedIn: 'root'})
export class ReservationBudgetService {
  constructor(
    private translateService: TranslateService,
    private sharedService: SharedService
  ) {
  }

  public calculateAverageDailyValue(reservationBudgetList: Array<ReservationBudget>): number {
    let sumPrice = 0;

    if (reservationBudgetList.length == 0) {
      return sumPrice;
    }

    reservationBudgetList.forEach(reservationBudget => {
      sumPrice += reservationBudget.manualRate;
    });

    return sumPrice / reservationBudgetList.length;
  }

  public getReservationItemCommercialBudgetSearch(
    propertyId: number,
    roomTypeId: number,
    dateListFromPeriod: Array<any>,
    companyClientId: string,
    adultCount: number,
    childCount: Array<number>,
    reservationItemId?: number,
  ): ReservationItemCommercialBudgetSearch {
    let initialDate;
    let finalDate;
    if (dateListFromPeriod && dateListFromPeriod.length > 0) {
      initialDate = dateListFromPeriod[0];
      finalDate = dateListFromPeriod[dateListFromPeriod.length - 1];
    }
    const reservationItemCommercialBudgetSearch = new ReservationItemCommercialBudgetSearch();
    reservationItemCommercialBudgetSearch.propertyId = propertyId;
    if (reservationItemId) {
      reservationItemCommercialBudgetSearch.reservationItemId = reservationItemId;
    }
    reservationItemCommercialBudgetSearch.roomTypeId = roomTypeId;
    reservationItemCommercialBudgetSearch.initialDate = initialDate;
    reservationItemCommercialBudgetSearch.finalDate = finalDate;
    reservationItemCommercialBudgetSearch.companyClientId = companyClientId;
    reservationItemCommercialBudgetSearch.adultCount = adultCount;
    reservationItemCommercialBudgetSearch.childrenAge = childCount;
    return reservationItemCommercialBudgetSearch;
  }

  public getReservationItemCommercialBudgetCurrent(
    commercialBudgetList: Array<ReservationItemCommercialBudget>,
    currencyId: string,
    mealPlanTypeId: number,
    ratePlanId: string = null,
  ): ReservationItemCommercialBudget {
    if (commercialBudgetList) {
      return commercialBudgetList.find(c => c.currencyId == currencyId && c.mealPlanTypeId == mealPlanTypeId && c.ratePlanId == ratePlanId);
    }
  }

  public getReservationBudgetByCommercialBudgetDay(
    reservationBudget: ReservationBudget,
    commercialBudgetDay: ReservationItemCommercialBudgetDay,
    commercialBudgetName: string,
    isGratuityType: boolean = false,
  ): ReservationBudget {
    reservationBudget.budgetDay = commercialBudgetDay.day;
    reservationBudget.baseRate = commercialBudgetDay.baseRateAmount;
    reservationBudget.childRate = commercialBudgetDay.childRateAmount;
    reservationBudget.agreementRate = commercialBudgetDay.total;
    reservationBudget.childAgreementRate = commercialBudgetDay.childRateAmountWithRatePlan;

    reservationBudget.manualRate = isGratuityType ? 0 : reservationBudget.agreementRate;
    if (!reservationBudget.mealPlanTypeId) {
      reservationBudget.mealPlanTypeId = commercialBudgetDay.mealPlanTypeId;
    }
    reservationBudget.currencySymbol = commercialBudgetDay.currencySymbol;
    reservationBudget.currencyId = commercialBudgetDay.currencyId;
    reservationBudget.discount = isGratuityType ? 100 : 0;

    reservationBudget.commercialBudgetName = commercialBudgetName ? commercialBudgetName : this.translateService.instant('text.gratuitous');
    reservationBudget.mealPlanTypeBaseRate = commercialBudgetDay.mealPlanAmount;
    reservationBudget.childMealPlanTypeRate = commercialBudgetDay.childMealPlanAmount;

    reservationBudget.propertyBaseRateHeaderHistoryId = commercialBudgetDay.baseRateHeaderHistoryId;
    reservationBudget.mealPlanTypeAgreementRate = commercialBudgetDay.mealPlanAmountWithRatePlan;
    reservationBudget.childMealPlanTypeAgreementRate = commercialBudgetDay.childMealPlanAmountWithRatePlan;
    reservationBudget.overbooking = commercialBudgetDay.overbooking;


    return reservationBudget;
  }

  public reservationItemCommercialBudgetWasChanged(
    newReservationItemCommercialBudget: ReservationItemCommercialBudget,
    reservationItemCommercialBudgetCurrent: ReservationItemCommercialBudget,
  ): boolean {
    return (
      (newReservationItemCommercialBudget && !reservationItemCommercialBudgetCurrent) ||
      !_.isEqual(newReservationItemCommercialBudget, reservationItemCommercialBudgetCurrent)
    );
  }

  public getDailiesTotals(reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig) {
    let totalBudget = 0;
    const budgetCurrent = reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent;
    if (
      reservationItemCommercialBudget.reservationBudgetTableList &&
      reservationItemCommercialBudget.reservationBudgetTableList.length > 0
      && budgetCurrent
    ) {
      budgetCurrent.currencySymbol =
        reservationItemCommercialBudget.reservationBudgetTableList[0].currencySymbol;
      for (const item of reservationItemCommercialBudget.reservationBudgetTableList) {
        totalBudget += item.manualRate;
      }

    }

    return totalBudget;
  }

  public getAverageDailies(reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig) {
    if (reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent &&
      reservationItemCommercialBudget.reservationBudgetTableList) {
      return (
        reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.totalBudget /
        reservationItemCommercialBudget.reservationBudgetTableList.length
      );
    }
    return 0;
  }

  public getDailiesTotalsOfAgreement(reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig) {
    let totalBudget = 0;
    if (
      reservationItemCommercialBudget &&
      reservationItemCommercialBudget.reservationBudgetTableList &&
      reservationItemCommercialBudget.reservationBudgetTableList.length > 0
      && reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent
    ) {
      reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.currencySymbol =
        reservationItemCommercialBudget.reservationBudgetTableList[0].currencySymbol;
      reservationItemCommercialBudget.reservationBudgetTableList.forEach(reservationBudget => {
        totalBudget += reservationBudget.agreementRate;
      });
    }

    return totalBudget;
  }

  public getRateVariationDefault(budgetConfig: ReservationItemCommercialBudgetConfig) {
    return budgetConfig.reservationItemCommercialBudgetCurrent.rateVariation;
  }

  public showCurrencySymbol(reservationItemCommercialBudgetConfig: ReservationItemCommercialBudgetConfig, isGratuityType: boolean) {
    if (reservationItemCommercialBudgetConfig.reservationItemCommercialBudgetCurrent) {
      return !isGratuityType;
    }
    return false;
  }

  public recalculateBudgetReservationItem(config: ReservationItemCommercialBudgetConfig) {
    config.reservationBudgetTableList =
      this.recalculateBudgetNewGuest(config.reservationItemCommercialBudgetCurrent.commercialBudgetDayList,
      config.reservationItemCommercialBudgetCurrent.commercialBudgetName);


    config.reservationItemCommercialBudgetCurrent.totalBudget =
      this.getDailiesTotals(
        config,
      );
    config.averageDaily = this.getAverageDailies(
      config,
    );
    config.reservationItemCommercialBudgetCurrent.rateVariation =
      this.getRateVariationDefault(config);
  }


  public recalculateTotalBudget(list: Array<ReservationBudget>): number {
    if (list) {
      return list.reduce( (prev, next) => {
        return prev + next.manualRate;
      }, 0);
    }

    return 0;
  }

  public recalculateBudgetNewGuest(
    commercialBudgetDayList: Array<ReservationItemCommercialBudgetDay>,
    budgetName: string
  ): Array<ReservationBudget>  {
    const reservationBudgetList = [];
    if (commercialBudgetDayList) {
      return commercialBudgetDayList.map(day => {
        const reservationBudget = new ReservationBudget();
        return this.getReservationBudgetByCommercialBudgetDay(
          reservationBudget,
          day,
          budgetName
        );
      });
    }

    return reservationBudgetList;
  }

  public setTotals(reservationBudget: ReservationItemCommercialBudgetConfig): void {
    if (reservationBudget.reservationItemCommercialBudgetCurrent
      && reservationBudget.reservationItemCommercialBudgetCurrent.totalBudget) {
      reservationBudget.reservationItemCommercialBudgetCurrent.totalBudget =
        this.getDailiesTotals(
          reservationBudget,
        );
      this.setAverageDaily(reservationBudget);
      reservationBudget.reservationItemCommercialBudgetCurrent.rateVariation = this.getRateVariation(reservationBudget);
    }
  }

  public setAverageDaily(reservationBudget: ReservationItemCommercialBudgetConfig) {
    if (reservationBudget.isGratuityType) {
      reservationBudget.averageDaily = 0;
    } else {
      reservationBudget.averageDaily = this.getAverageDailies(
        reservationBudget,
      );
    }
  }

  public getRateVariation(reservationBudget: ReservationItemCommercialBudgetConfig) {
    return reservationBudget.reservationItemCommercialBudgetCurrent.rateVariation;
    // todo validar o lógica que será aplicada para o controle de variação tarifária
  }

  public setDiscount(reservationBudgetTableList: Array<ReservationBudget>, rowIndex: number) {
    const element = reservationBudgetTableList[rowIndex];
    element.manualRate = this.sharedService.getValueWithDiscount(
      element.agreementRate,
      element.discount
    );
  }

  public  getReservationItemCommercialBudgetIdentifier(reservationItemCommercialBudget) {
    if (reservationItemCommercialBudget) {
      const {currencyId, mealPlanTypeId, roomTypeId, ratePlanId} = reservationItemCommercialBudget;
      return {currencyId, mealPlanTypeId, roomTypeId, ratePlanId};
    }
    return null;
  }

  public transformCommercialBudgetDayListInReservationBudget(commercialBudgetDayList: Array<any>,
                                                             isForFree, commercialBudgetName, mealPlanTypes?: Array<any>) {
    const selectedReservationBudgetList = [];
    if (commercialBudgetDayList) {
      commercialBudgetDayList.forEach(budgetDay => {
        let mealPlanTypeId = null;
        if (mealPlanTypes) {
          const filtered = mealPlanTypes.filter( mp => moment(budgetDay.day).isSame( moment(mp.day) ) );
          if ( filtered && filtered.length > 0 ) {
            mealPlanTypeId = filtered[0].mealPlanTypeId;
          }
        }
        const reservationBudget = new ReservationBudget();
        reservationBudget.mealPlanTypeId = mealPlanTypeId;
        const item = this.getReservationBudgetByCommercialBudgetDay(
          reservationBudget,
          budgetDay,
          commercialBudgetName,
          isForFree
        );
        selectedReservationBudgetList.push(item);
      });
    }
    return selectedReservationBudgetList;
  }

  // método sempre pega à partir do tipo de pensão informado,
  // o tipo de pensão default referente à ele, pois só os defaults aparecem no select
  public prepareAndReturnBudgetKey( key: any, list: Array<ReservationItemCommercialBudget>) {
    if (list && list.length > 0) {
      const filteredElements = list.filter( item => {
        if (item.currencyId == key.currencyId &&
          item.mealPlanTypeId == key.mealPlanTypeId &&
          item.ratePlanId == key.ratePlanId &&
          item.roomTypeId == key.roomTypeId) {
          return item;
        }
      });
      if (filteredElements && filteredElements.length > 0) {
        const {currencyId, ratePlanId, mealPlanTypeDefaultId, roomTypeId} = filteredElements[0];
        return {
          currencyId,
          ratePlanId,
          mealPlanTypeId: mealPlanTypeDefaultId,
          roomTypeId
        };
      }
    }
    return key;
  }
}
