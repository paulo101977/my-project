import { createServiceStub } from '../../../../../../testing';
import { ReservationBudgetService } from 'app/shared/services/reservation-budget/reservation-budget.service';
import {
    ReservationItemCommercialBudgetConfig
} from 'app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';

export const reservationBudgetServiceStub = createServiceStub(ReservationBudgetService, {
    recalculateBudgetReservationItem(config: ReservationItemCommercialBudgetConfig) {
    }
});

