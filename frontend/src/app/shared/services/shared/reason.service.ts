import { Injectable } from '@angular/core';
import { Reason } from '../../models/dto/reason';

@Injectable({ providedIn: 'root' })
export class ReasonService {
  constructor() {}

  public filterBySearchData(searchData: string, allItems: Array<Reason>): Array<Reason> {
    return allItems.filter(item => {
      return (
        item.reasonName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.reasonCategoryName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1
      );
    });
  }
}
