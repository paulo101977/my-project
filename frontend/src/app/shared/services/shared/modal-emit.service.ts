import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ModalEmitService {
  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();
  // Service message commands
  emitSimpleModal(title: string, message: string, callbackFunction: any, actions: Array<any> = [], closeCallback?: Function) {
    this.emitChangeSource.next([title, message, callbackFunction, actions, closeCallback]);
  }
}
