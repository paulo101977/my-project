import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ModalAccommodationBudgetFormEmitService {
  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();
  // Service message commands
  emitSimpleModal(roomTypeSelected: any, roomTypeList: Array<any>, dateList: Array<any>, dailyPrices: Map<Date, number>) {
    this.emitChangeSource.next([roomTypeSelected, roomTypeList, dateList, dailyPrices]);
  }
}
