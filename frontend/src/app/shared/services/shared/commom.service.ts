import { Injectable } from '@angular/core';
import { SessionParameterService } from '../parameter/session-parameter.service';
import { SelectObjectOption } from './../../models/selectModel';
import { ParameterTypeEnum } from '../../models/parameter/pameterType.enum';

@Injectable({ providedIn: 'root' })
export class CommomService {
  public readonly ADULT_AGE = 18;

  constructor(private sessionParameter: SessionParameterService) {}

  public convertObjectToSelectOptionObject(objectToConvert: any) {
    let arrayOptionsToSelect;

    arrayOptionsToSelect = objectToConvert.items.map(reasonOption => {
      const objSelect = new SelectObjectOption();
      objSelect.key = reasonOption.id;
      objSelect.value = reasonOption.reasonName;
      return objSelect;
    });

    return arrayOptionsToSelect;
  }

  public convertObjectToSelectOptionObjectByIdAndValueProperties(objectToConvert: any, keyName: string, valueName: string) {
    console.warn('This method was deprecated! Try .toOption instead!');
    return this.toOption(objectToConvert, keyName, valueName);
  }

  public toOption(objectToConvert: any, keyName: string, valueName: string) {
    let arrayOptionsToSelect;

    if (objectToConvert) {
      if (objectToConvert.hasOwnProperty('items')) {
        objectToConvert = objectToConvert.items;
      }
      arrayOptionsToSelect = objectToConvert.map(reasonOption => {
        return { key: reasonOption[keyName], value: reasonOption[valueName] };
      });
    }
    return arrayOptionsToSelect;
  }

  public isAdultBasedOnChildParameters(propertyId, newAge): Boolean {
    const maxKidAge = this.sessionParameter.getParameterValue(propertyId, ParameterTypeEnum.ChildMaxAge);
    return newAge > +maxKidAge;
  }

  public isAdult(newAge): boolean {
    return newAge >= this.ADULT_AGE;
  }

  public getAgeRangeFromAge(propertyId: number, age: number): number {
    const range1 = this.sessionParameter.getParameter(propertyId, ParameterTypeEnum.ChildAgeRange1);
    if (age <= parseInt(range1.propertyParameterValue, 0)) {
      if (range1.isActive) {
        return 1;
      }
    }
    const range2 = this.sessionParameter.getParameter(propertyId, ParameterTypeEnum.ChildAgeRange2);
    if (age > parseInt(range1.propertyParameterValue, 0) && age <= parseInt(range2.propertyParameterValue, 0)) {
      if (range2.isActive) {
        return 2;
      }
    }
    const range3 = this.sessionParameter.getParameter(propertyId, ParameterTypeEnum.ChildAgeRange3);
    if (age > parseInt(range2.propertyParameterValue, 0) && age <= parseInt(range3.propertyParameterValue, 0)) {
      if (range3.isActive) {
        return 3;
      }
    }
  }

}
