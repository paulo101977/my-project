import { inject, TestBed } from '@angular/core/testing';
import { CurrencyService } from './currency-service.service';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { currencyFormatPipe } from '../../../../../testing';
import { configureTestSuite } from 'ng-bullet';
import { sessionParameterServiceStub } from 'app/shared/services/parameter/testing';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';

describe('CurrencyService', () => {
  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        currencyFormatPipe,
        sessionParameterServiceStub,
      ],
    });
  });

  it('should be created', inject([CurrencyService], (service: CurrencyService) => {
    expect(service).toBeTruthy();
  }));


  describe('on formatToPercent', () => {
    it('should test with NaN value', inject([CurrencyService], (service: CurrencyService) => {
      const value = 'bbbb';
      expect(service.formatToPercent(value)).toEqual(value);
    }));

    it('should test when value is number', inject([CurrencyService], (service: CurrencyService) => {
      const value = '20';
      expect(service.formatToPercent(value)).toEqual('R$  20,0 %');
    }));
  });

  describe('on verifyPecentageField', () => {
    let event;
    beforeEach(() => {
      event = {
        keyCode: 8,
      };
    });

    it('should test with not value', inject([CurrencyService], (service: CurrencyService) => {
      expect(service.verifyPecentageField(null, null, null, null)).toEqual(0);
    }));

    it('should test with nan fieldValue && keyCode !== backspaceCode',
      inject([CurrencyService], (service: CurrencyService) => {

      event.keyCode = 1;
      expect(
        service.verifyPecentageField(event, 'blablabla', null, null)
      ).toEqual(null);
    }));

    it('should test with not min value',
      inject([CurrencyService], (service: CurrencyService) => {
        const value = -10;

        expect(
          service.verifyPecentageField(event, value, null, null)
        ).toEqual(0);
    }));

    it('should test with not max value',
      inject([CurrencyService], (service: CurrencyService) => {
        const value = 1000;

        expect(
          service.verifyPecentageField(event, value, null, null)
        ).toEqual(99);
    }));

    it('should test with min value',
      inject([CurrencyService], (service: CurrencyService) => {
        const value = -10;

        expect(
          service.verifyPecentageField(event, value, 1, null)
        ).toEqual(1);
    }));

    it('should test with max value',
      inject([CurrencyService], (service: CurrencyService) => {
        const value = 102;

        expect(
          service.verifyPecentageField(event, value, 1, 101)
        ).toEqual(101);
    }));
  });

  it('should test formatCurrencyToString',
    inject([CurrencyService], (service: CurrencyService) => {
      const value = 102;
      const sign = '+R';

      expect(
        service.formatCurrencyToString(value, sign)
      ).toEqual(`${sign} ${value},00`);
  }));

  it('should test formatCurrencyToString',
    inject([CurrencyService], (service: CurrencyService) => {
      const parameterFake = { title: 'PT-blablabla'};
      const propertyId = 11;
      spyOn<any>(service['sessionParameterService'], 'getParameter').and.returnValue(parameterFake);
      spyOn(service['sessionParameterService'], 'extractSelectedOptionOfPossibleValues')
        .and.returnValue(parameterFake);

      const str = service.getDefaultCurrencySymbol(propertyId);

      expect(
        service['sessionParameterService'].getParameter
      ).toHaveBeenCalledWith(propertyId, ParameterTypeEnum.DefaultCurrency);
      expect(
        service['sessionParameterService'].extractSelectedOptionOfPossibleValues
      ).toHaveBeenCalledWith(parameterFake);
      expect(str).toEqual('PT');
  }));
});
