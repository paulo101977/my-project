import { TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { ShowHide } from './../../models/show-hide-enum';
import { ShowHideContentEmitService } from './show-hide-content-emit-service';

describe('ShowHideContentEmitService', () => {
  const service = new ShowHideContentEmitService();

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      providers: [],
    });
  });

  it('should emit next', () => {
    spyOn(service['emitChangeSource'], 'next');

    service.emitChange(ShowHide.hide);

    expect(service['emitChangeSource'].next).toHaveBeenCalledWith(ShowHide.hide);
  });
});
