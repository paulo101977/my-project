import { inject, TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { CountService } from './count.service';

describe('CountService', () => {
  let service: CountService;

  const field: FormControl = new FormControl();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [],
    });
  });

  beforeEach(inject([CountService], (countService: CountService) => {
    service = countService;
  }));

  it('service should exists', () => {
    expect(service).toBeTruthy();
  });

  it('field form should to init with 13 chars and finish with 0 chars', () => {
    let count: number;
    field.setValue('Testing chars');

    service.setFieldToCount(field);
    service.getLenght().subscribe(response => {
      count = response.count;
    });

    field.setValue('Testing');
    expect(count).toEqual(7);
    field.setValue('Testin');
    expect(count).toEqual(6);
    field.setValue('Testi');
    expect(count).toEqual(5);
    field.setValue('Test');
    expect(count).toEqual(4);
    field.setValue('Tes');
    expect(count).toEqual(3);
    field.setValue('Te');
    expect(count).toEqual(2);
    field.setValue('T');
    expect(count).toEqual(1);
    field.setValue('');
    expect(count).toEqual(0);
  });
});
