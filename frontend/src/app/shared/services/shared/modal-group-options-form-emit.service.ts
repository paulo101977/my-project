import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ModalGroupOptionsFormEmitService {
  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();
  // Service message commands
  emitSimpleModal(groupName: string, unifyAccounts: boolean) {
    this.emitChangeSource.next([groupName, unifyAccounts]);
  }
}
