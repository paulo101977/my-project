import { inject, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ModalCancelActionService } from './modal-cancel-action.service';

describe('ModalCancelActionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, TranslateModule.forRoot()],
      providers: [],
    });
  });

  it('should create', inject([ModalCancelActionService], (service: ModalCancelActionService) => {
    expect(service).toBeTruthy();
  }));

  it('should call modalEmitService', inject([ModalCancelActionService], (service: ModalCancelActionService) => {
    const modalService = service['modalEmitService'];
    spyOn(modalService, 'emitSimpleModal');

    service.cancelAction();

    expect(modalService.emitSimpleModal).toHaveBeenCalledWith(
      'commomData.warning',
      'commomData.actionCancel',
      service['backPreviewPage'],
      [],
    );
  }));
});
