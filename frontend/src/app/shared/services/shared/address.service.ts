import { Injectable } from '@angular/core';
import { AddressType } from './../../models/address-type-enum';
import { TranslateService } from '@ngx-translate/core';
import {AddressResource} from 'app/shared/resources/address/address.resource';
import {Country} from 'app/shared/models/country';

@Injectable({ providedIn: 'root' })
export class AddressService {
  private countryList: Array<Country>;

  constructor( private translateService: TranslateService,
               private addressResource: AddressResource) {
    this.loadCounties();
  }

  public getCountries(): Array<Country> {
    return this.countryList;
  }

  public getLocationCategoryEnumByLocationCategoryId(locationCategoryId: number): AddressType {
    if (locationCategoryId == 2) {
      return AddressType.Residencial;
    } else if (locationCategoryId == 3) {
      return AddressType.Correspondencia;
    } else if (locationCategoryId == 4) {
      return AddressType.Cobranca;
    } else if (locationCategoryId == 5) {
      return AddressType.Entrega;
    } else {
      return AddressType.Comercial;
    }
  }

  public getLocationCategoryIdByLocationCategoryEnum(addressTypeEnum: AddressType): number {
    if (addressTypeEnum == AddressType.Residencial) {
      return 2;
    } else if (addressTypeEnum == AddressType.Correspondencia) {
      return 3;
    } else if (addressTypeEnum == AddressType.Cobranca) {
      return 4;
    } else if (addressTypeEnum == AddressType.Entrega) {
      return 5;
    } else {
      return 1;
    }
  }

  public getAddressTypeList(): any[] { // todo test this method
    const list = [];
    for (const type in AddressType) {
      if (!isNaN(Number(type))) {
        list.push({
          value: type,
          text: this.getAddressTypeNameById(+type)
        });
      }
    }
    return list;
  }

  private getAddressTypeNameById(addressType): string { // todo test this method
    switch (+addressType) {
      case AddressType.Comercial:
        return this.translateService.instant('sharedModule.components.address.commercial');
      case AddressType.Residencial:
        return this.translateService.instant('sharedModule.components.address.residential');
      case AddressType.Correspondencia:
        return this.translateService.instant('sharedModule.components.address.correspondence');
      case AddressType.Cobranca:
        return this.translateService.instant('sharedModule.components.address.billing');
      case AddressType.Entrega:
        return this.translateService.instant('sharedModule.components.address.delivery');
    }
  }

  public getAddressByGooglePlace(place: any, selectedCountrySubdivision) {
    let objPlaceToForm = {};
    if (selectedCountrySubdivision) {
      objPlaceToForm = {
        ...objPlaceToForm,
        countrySubdivisionId: selectedCountrySubdivision
      };
    }
    objPlaceToForm = {
      ...objPlaceToForm,
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      completeAddress: place.formatted_address
    };
    place['address_components'].forEach(component => {
      switch (component.types[0]) {
        case 'route':
          objPlaceToForm = {...objPlaceToForm, streetName: component.long_name};
          break;
        case 'sublocality_level_1':
          objPlaceToForm = {...objPlaceToForm, neighborhood: component.long_name};
          break;
        case 'administrative_area_level_1':
          objPlaceToForm = {...objPlaceToForm, division: component.short_name};
          break;
        case 'administrative_area_level_2':
        case 'locality':
        case 'sublocality':
        case 'administrative_area_level_3':
          objPlaceToForm = {...objPlaceToForm, subdivision: component.long_name};
          break;
        case 'postal_code':
          let cepNumber = '';
          if (component.long_name) {
            cepNumber = component.long_name.replace('-', '');
          }
          objPlaceToForm = {...objPlaceToForm, postalCode: cepNumber};
          break;
        case 'country':
          objPlaceToForm = {...objPlaceToForm, countryCode: component.short_name, country: component.long_name};
          break;
        case 'street_number':
          objPlaceToForm = {...objPlaceToForm, streetNumber: component.short_name};
          break;
      }
    });
    return objPlaceToForm;
  }

  private loadCounties() {
    this.addressResource.getCountry().subscribe(result => {
      this.countryList = result.items;
    });
  }
}
