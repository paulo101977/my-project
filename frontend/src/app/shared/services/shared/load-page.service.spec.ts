import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { LoadPageService } from './load-page.service';

describe('LoadPageService', () => {
  const service = new LoadPageService();

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      providers: [],
    });
  });

  it('should emit next with true', () => {
    spyOn(service['emitChangeSource'], 'next');

    service.show();

    expect(service['emitChangeSource'].next).toHaveBeenCalledWith(true);
  });

  it(
    'should emit next with false',
    fakeAsync(() => {
      spyOn(service['emitChangeSource'], 'next');

      service.hide();

      tick(300);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith(false);
    }),
  );
});
