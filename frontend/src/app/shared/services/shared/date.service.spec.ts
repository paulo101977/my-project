import {DateService} from './date.service';
import * as moment from 'moment';

describe('DateService', () => {

  let service: DateService;

  beforeEach(() => {
    service = new DateService();
  });

  it('should count daily', () => {
    const startDate = new Date(2019, 5, 14);
    const endDate = new Date(2019, 5, 18);

    expect(service.diffDaily(startDate, endDate)).toEqual(4);
  });

  it('should validate date using moment - DATE_FORMAT_UNIVERSAL', () => {
    expect(service.isValid('2018/07/26')).toBeFalsy();
    expect(service.isValid('26-07-2018')).toBeFalsy();
    expect(service.isValid('2018-07-26AA9:14:57')).toBeFalsy();
    expect(service.isValid('2018-07-26T19:14:57')).toBeTruthy();
  });

  it('should validate date using moment - MOMENT_DATE_FORMAT_US', () => {
    expect(service.isValid('2018/07/26')).toBeFalsy();
    expect(service.isValid('26-07-2018')).toBeFalsy();
    expect(service.isValid('2018-07-26')).toBeTruthy();
  });

  it('should validate date using moment - LocaleData', () => {
    moment.locale('pt-br');
    expect(service.isValid('2018/07/26')).toBeFalsy();
    expect(service.isValid('26/07/2018')).toBeTruthy();
  });
});
