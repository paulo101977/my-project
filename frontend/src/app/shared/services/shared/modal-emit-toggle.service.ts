import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ModalEmitToggleService {
  public modals: object;
  private emitChangeSource = new Subject<any>();
  changeEmitted$ = this.emitChangeSource.asObservable();

  constructor() {
    this.modals = {};
  }

  public pushModals(newModal: string): void {
    this.modals[newModal] = false;
  }

  public getModals(): object {
    return this.modals;
  }

  public emitToggleWithRef(modalId: any): void {
    this.modals[modalId] = !this.modals[modalId];
    this.emitChangeSource.next(this.modals);
  }

  public emitCloseWithRef(modalId: any): void {
    this.modals[modalId] = false;
    this.emitChangeSource.next(this.modals);
  }

  public emitOpenWithRef(modalId: any): void {
    this.modals[modalId] = true;
    this.emitChangeSource.next(this.modals);
  }

  public emitOpenWithRefIfisNotShowing(modalId: any): void {
    if ( !this.modals.hasOwnProperty(modalId) || !this.modals[modalId] ) {
      this.modals[modalId] = true;
      this.emitChangeSource.next(this.modals);
    }
  }

  public emitToggleWithRefIfNoModalShowing(modalId: any): void {
    let someModal = false;
    for (const property in this.modals) {
      if (this.modals.hasOwnProperty(property) && this.modals[property] == true) {
        someModal = true;
      }
    }
    if (!someModal) {
      this.emitToggleWithRef(modalId);
    }
  }

  public closeAllModals(): void {
    this.modals = {};
    this.emitChangeSource.next(this.modals);
  }
}
