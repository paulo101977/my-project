import { AbstractControl } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

export abstract class BaseErrorValidation {
  protected readonly validationMessages = {
    required: 'required',
    patterns: 'pattern',
  };

  abstract getMessageError(controlForm: AbstractControl): string;
}
