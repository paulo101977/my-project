import { Injectable } from '@angular/core';
import { PaymentType } from '../../models/payment-types';
import { GetAllPaymentTypeDto } from '../../models/payment-type/payment-type';
import { TranslateService } from '@ngx-translate/core';

@Injectable({ providedIn: 'root' })
export class PaymentTypeService {
  private internalUsage: PaymentType;
  private deposit: PaymentType;
  private courtesy: PaymentType;
  private hotel: PaymentType;
  private creditCard: PaymentType;
  private toBeBillet: PaymentType;

  constructor(private translateService: TranslateService) {
    this.setPayments();
  }

  private setPayments() {
    this.internalUsage = new PaymentType();
    this.internalUsage.key = 1;
    this.internalUsage.value = this.translateService.instant('reservationModule.paymentTypes.internalUsage');
    this.deposit = new PaymentType();
    this.deposit.key = 2;
    this.deposit.value = this.translateService.instant('reservationModule.paymentTypes.deposit');
    this.courtesy = new PaymentType();
    this.courtesy.key = 3;
    this.courtesy.value = this.translateService.instant('reservationModule.paymentTypes.courtesy');
    this.hotel = new PaymentType();
    this.hotel.key = 4;
    this.hotel.value = this.translateService.instant('reservationModule.paymentTypes.hotel');
    this.creditCard = new PaymentType();
    this.creditCard.key = 5;
    this.creditCard.value = this.translateService.instant('reservationModule.paymentTypes.creditCard');
    this.toBeBillet = new PaymentType();
    this.toBeBillet.key = 6;
    this.toBeBillet.value = this.translateService.instant('reservationModule.paymentTypes.toBeBillet');
  }

  public getPaymentTypeList(isWalkin: boolean, hasClient?: boolean) {
    let paymentTypeList = [this.internalUsage, this.deposit, this.courtesy, this.hotel, this.creditCard];
    if (isWalkin) {
      paymentTypeList = [this.internalUsage, this.courtesy, this.hotel];
    }
    if (hasClient) {
      paymentTypeList.push(this.toBeBillet);
    }
    return paymentTypeList;
  }

  public isCortesy(paymentTypeId: number) {
    return paymentTypeId == this.courtesy.key;
  }

  public isClientToBill(paymentTypeId: number) {
    return paymentTypeId == this.toBeBillet.key;
  }

  public isHotel(paymentTypeId: number) {
    return paymentTypeId == this.hotel.key;
  }

  public isInternalUsage(paymentTypeId: number) {
    return paymentTypeId == this.internalUsage.key;
  }

  public isToBeBillet(paymentTypeId: number) {
    return paymentTypeId == this.toBeBillet.key;
  }

  public isCreditCard(paymentTypeId: number) {
    return paymentTypeId == this.creditCard.key;
  }

  public isDeposit(paymentTypeId: number) {
    return paymentTypeId == this.deposit.key;
  }

  public filterBySearchData(searchData: string, allItems: Array<GetAllPaymentTypeDto>): Array<GetAllPaymentTypeDto> {
    return allItems.filter(item => {
      const description = item.acquirerName || item.description || '';
      return (
        item.paymentTypeName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        description.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1
      );
    });
  }

  public filterPaymentById(id: number) {
    return (this.getPaymentTypeList(false).find(item => item.key == id) || { value: ''}).value;
  }
}
