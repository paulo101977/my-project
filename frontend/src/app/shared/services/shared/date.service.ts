import { IMyDpOptions } from 'mydatepicker';
import { Injectable } from '@angular/core';
import { IMyDate, IMyDrpOptions } from 'mydaterangepicker';
import * as moment from 'moment';
import { User } from '../../models/user';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';

@Injectable({ providedIn: 'root' })
export class DateService {
  public static DATE_FORMAT_UNIVERSAL = 'YYYY-MM-DDTHH:mm:ss';

  public MOMENT_DATE_FORMAT_US = 'YYYY-MM-DD';
  public USER = 'user';

  constructor(
    private sessionParameterService?: SessionParameterService
  ) {}

  // TODO: make unit test to this method
  public getSystemDate(propertyId: number): Date {
    if (propertyId) {
      const propertyParameterValue = this.getSystemDateWithoutFormat(propertyId);

      return moment(propertyParameterValue).toDate();
    }
    return moment().toDate();
  }

  // TODO: make unit test to this method
  public getSystemDateInViewFormat(propertyId: number): string {
    if (propertyId) {
      const propertyParameterValue = this.getSystemDateWithoutFormat(propertyId);

      return this.convertUniversalDateToViewFormat(propertyParameterValue);
    }

    return '';
  }

  // TODO: make unit test to this method
  public getSystemDateWithoutFormat(propertyId: number) {
    if (propertyId) {
      const systemDateParam =  this
        .sessionParameterService
        .getParameter(propertyId, ParameterTypeEnum.SistemDate);

      if (systemDateParam && systemDateParam.propertyParameterValue) {
        return systemDateParam.propertyParameterValue;
      }
    }

    return '';
  }

  // todo teste unitário
  public convertUniversalDateToIMyDate(universalDate: string) {
    const momentDate = moment(universalDate, DateService.DATE_FORMAT_UNIVERSAL).toDate();
    let dateValue;
    if (momentDate) {
      dateValue = {
        date: {
          day: momentDate.getDate(),
          month: momentDate.getMonth() + 1,
          year: momentDate.getFullYear(),
        },
      };
      return dateValue;
    }
    return null;
  }

  public convertUniversalDateToViewFormat(strUniversalDate: string): string {
    if (strUniversalDate) {
      return moment(strUniversalDate, DateService.DATE_FORMAT_UNIVERSAL).format('L');
    }
    return '';
  }

  public convertStringToDate(dateString: string, format?: string): Date {
    if (dateString && !format) {
      return new Date(dateString);
    } else if (dateString && format) {
      return moment(dateString, format).toDate();
    }
    return new Date();
  }

  public convertDateToString(date: Date): string {
    return this.convertDateToStringWithFormat(date,  DateService.DATE_FORMAT_UNIVERSAL);
  }

  public convertDateToStringWithFormat(date: Date, format: string) {
    if (typeof date === 'object') {
      return moment(date).format(format);
    }
    return null;
  }

  public convertDateToStringViewFormat(date: Date): string {
    return date ? moment(date).format('L') : '-';
  }

  public convertPeriodToDateList(dateBegin: any, dateEnd: any, removeLast = true, isDaily = false): Array<Date> {
    const dateList = new Array<Date>();

    if (dateBegin && dateEnd) {
      const quantityDays = isDaily ? this.diffDays(dateBegin, dateEnd) : this.diffDailyUtc(dateBegin, dateEnd);

      dateList.push(new Date(dateBegin));

      for (let day = 1; day <= quantityDays; day++) {
        const nextDate = new Date(dateBegin);
        if ((removeLast && day < quantityDays) || !removeLast ) {
          dateList.push( this.addDays(nextDate, day) );
        }
      }
    }

    return dateList;
  }

  public addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  public date1IsMajorThanDate2(date1: Date, date2: Date): boolean {
    if (date2 && date1) {
      return moment(date1.getTime()) > moment(date2.getTime());
    }

    return false;
  }

  public diffDays(date1: Date, date2: Date): number {
    const beginDate = moment(date1);
    const endDate = moment(date2);

    return endDate.diff(beginDate, 'days');
  }


  public diffDaily(date1: Date, date2: Date): number {
    const beginDate = moment(date1).startOf('day');
    const endDate = moment(date2).startOf('day');


    return endDate.diff(beginDate, 'days');
  }

  public diffDailyUtc(date1: Date, date2: Date): number {

    const beginDate = moment.utc(date1).startOf('day');
    const endDate = moment.utc(date2).startOf('day');


    return endDate.diff(beginDate, 'days');
  }

  public hourAndMinutesAreInValid(hourAndMinutes: string): boolean {
    if (hourAndMinutes) {
      if (hourAndMinutes.match('^([01][0-9]|2[0-3]):[0-5][0-9]$')) {
        return false;
      }
    }
    return true;
  }

  public getDateRangePicker(dateBegin: string | Date, dateEnd: string | Date): any {
    let begin: Date;
    let end: Date;

    if (typeof dateBegin === 'object' && typeof dateEnd === 'object') {
      begin = dateBegin;
      end = dateEnd;
    }
    if (typeof dateBegin === 'string' && typeof dateEnd === 'string') {
      begin = this.convertStringToDate(dateBegin, DateService.DATE_FORMAT_UNIVERSAL);
      end = this.convertStringToDate(dateEnd, DateService.DATE_FORMAT_UNIVERSAL);
    }

    return {
      beginDate: (<Date>begin).toStringUniversal(),
      endDate:  (<Date>end).toStringUniversal()
    };
  }

  public getDatePicker(dateParam: string | Date): any {
    let date: Date;

    if (typeof dateParam === 'object') {
      date = dateParam;
    }
    if (typeof dateParam === 'string') {
      date = this.convertStringToDate(dateParam);
    }
    return {
      date: { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() },
      jsdate: date,
    };
  }

  public getConfigDateRangePicker(): IMyDrpOptions {
    const today = new Date();

    return {
      dateFormat: 'dd/mm/yyyy',
      firstDayOfWeek: 'mo',
      sunHighlight: false,
      markCurrentDay: true,
      height: '44px',
      width: '100%',
      selectorWidth: 'auto',
      selectorHeight: 'auto',
      inline: false,
      showClearBtn: false,
      showApplyBtn: false,
      showSelectDateText: false,
      alignSelectorRight: false,
      indicateInvalidDateRange: true,
      showClearDateRangeBtn: false,
      editableDateRangeField: false,
      openSelectorOnInputClick: true,
      showSelectorArrow: false,
      disableUntil: {
        year: today.getFullYear(),
        month: today.getMonth() + 1,
        day: today.getDate() - 1,
      },
    };
  }

  public getConfigDatePicker(): IMyDpOptions {
    const today = new Date();

    return {
      dateFormat: 'dd/mm/yyyy',
      showClearDateBtn: false,
      height: '44px',
      markCurrentDay: true,
      editableDateField: false,
      indicateInvalidDate: true,
      openSelectorOnInputClick: true,
      disableUntil: {
        year: today.getFullYear(),
        month: today.getMonth() + 1,
        day: today.getDate() - 1,
      },
    };
  }

  public getClearConfigDatePicker(): IMyDpOptions {
    return {
      dateFormat: 'dd/mm/yyyy',
      showClearDateBtn: false,
      height: '44px',
      markCurrentDay: true,
      editableDateField: false,
      indicateInvalidDate: true,
      openSelectorOnInputClick: true,
    };
  }

  public getClearConfigDateRangePicker( disableUntil?: IMyDate, disableSince?: IMyDate): IMyDrpOptions {
    const obj = {
      dateFormat: 'dd/mm/yyyy',
      firstDayOfWeek: 'mo',
      sunHighlight: false,
      markCurrentDay: true,
      height: '44px',
      width: '100%',
      inline: false,
      showClearBtn: false,
      showApplyBtn: false,
      showSelectDateText: false,
      alignSelectorRight: false,
      indicateInvalidDateRange: true,
      showClearDateRangeBtn: false,
      editableDateRangeField: false,
      openSelectorOnInputClick: true,
      showSelectorArrow: false,
    };

    if (disableUntil) {
      obj['disableUntil'] = disableUntil;
    }
    if (disableSince) {
      obj['disableSince'] = disableSince;
    }
    return obj;
  }

  public getAmericanFormatDate(dateParameter: any) {
    const hour = 3;
    const min = 0;
    const seg = 0;
    const dateFormat = new Date(Date.UTC(dateParameter.year, dateParameter.month - 1, dateParameter.day, hour, min, seg));
    const user = <User>JSON.parse(localStorage.getItem(this.USER));
    if (user && user.preferredCulture) {
      return dateFormat.toLocaleDateString(user.preferredCulture);
    }
  }

  public calculateAgeByDate(date: Date | string): number {
    if (date) {
      const birthday = moment(date);
      const age = moment().diff(birthday, 'years');
      return age;
    }
    return 0;
  }

  public setDatePickerPeriod(dateData: Date) {
    const date = new Date(dateData);
    const myDatePickerOptions = {
      date: {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate(),
      },
    };

    return myDatePickerOptions;
  }

  public getHoursAndMinutes(dateData: Date) {
    const oneCharacter = 1;
    const zeroBeforeHourOrMimnutes = '0';
    const hours =
      dateData.getHours().toString().length == oneCharacter
        ? zeroBeforeHourOrMimnutes + dateData.getHours().toString()
        : dateData.getHours().toString();
    const minutes =
      dateData.getMinutes().toString().length == oneCharacter
        ? zeroBeforeHourOrMimnutes + dateData.getMinutes().toString()
        : dateData.getMinutes().toString();
    return hours + ':' + minutes;
  }

  public removeTime(jsDate: Date) {
    return moment
      .utc(jsDate)
      .startOf('day')
      .format('YYYY-MM-DD');
  }

  public calculateOvernightStayAsString(startDateStr: string, endDateStr: string) {
    const startDate = this.convertStringToDate(startDateStr, DateService.DATE_FORMAT_UNIVERSAL);
    const endDate = this.convertStringToDate(endDateStr, DateService.DATE_FORMAT_UNIVERSAL);
    return this.calculateOvernightStay(startDate, endDate);
  }

  public calculateOvernightStay(startDate, endDate) {
    return moment(this.removeHourFromUniversalDate(endDate), 'L')
      .diff(moment(this.removeHourFromUniversalDate(startDate), 'L'), 'days');
  }

  public removeHourFromUniversalDate(universalDate: string) {
    return moment(universalDate, DateService.DATE_FORMAT_UNIVERSAL).format('L');
  }

  public removeTimeFromUniversalDate(universalDate: string) {
    return moment(universalDate, DateService.DATE_FORMAT_UNIVERSAL).format('YYYY-MM-DD');
  }

  public convertTimeToSeconds(days, hours, minutes) {
    let total = 0;
    total += days > 0 && days * 24 * 60 * 60;
    total += hours > 0 && hours * 60 * 60;
    total += minutes > 0 && minutes * 60;
    return total;
  }

  public getTodayIMydate(): IMyDate {
    const today = new Date();
    return {
      year: today.getFullYear(),
      month: today.getMonth() + 1,
      day: today.getDate() - 1,
    };
  }

  public isBetweenInPeriod(dateSearch, beginDate, endDate) {
    return moment(dateSearch).isSameOrAfter(beginDate) &&
      moment(dateSearch).isSameOrBefore(endDate);
  }

  // View formatters
  public formatVigenceString(dateInit: string, dateEnd: string, separator = '-' ): string {
    const inicialDate = this.convertUniversalDateToViewFormat(dateInit);
    const finalDate = this.convertUniversalDateToViewFormat(dateEnd);
    return `${inicialDate} ${separator} ${finalDate}`;
  }

  public compareWithSystemDate(propertyId: number, date: string ): number {
    if (date) {
      const systemDate = this.getSystemDate(propertyId);
      return moment(date, DateService.DATE_FORMAT_UNIVERSAL).startOf('day').diff(systemDate, 'days');
    }
  }

  public clearTime(date: string) {
    return date ? date.replace('T00:00:00', '') : date;
  }

  public isValid(dateInput) {
    if (typeof dateInput === 'string') {
      const dateUniversalFormatIsValid = this.isValidFormatByFormat(dateInput, DateService.DATE_FORMAT_UNIVERSAL);
      if (dateUniversalFormatIsValid) {
        return true;
      } else {
        const result = dateInput.replace(/[a-zA-Z.,!@#$%¨&*();:]/gi, '');
        const dateUSFormatIsValid = this.isValidFormatByFormat(result, this.MOMENT_DATE_FORMAT_US);
        const dateLocaleFormatIsValid = this.isValidFormatByFormat(result, moment.localeData().longDateFormat('L'));
        return dateUSFormatIsValid || dateLocaleFormatIsValid;
      }
    } else if (dateInput instanceof Date) {
      return moment(dateInput, DateService.DATE_FORMAT_UNIVERSAL, true).isValid();
    }
    return false;
  }

  public isValidFormatByFormat(dateInput, format) {
    return moment(dateInput, format, true).isValid();
  }

  public getTotalDaysFromMonth(date: Date): number {
      return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
  }

}
