import { inject, TestBed } from '@angular/core/testing';

import { PrintHelperService } from './print-helper.service';

describe('PrintHelperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  it('should be created', inject([PrintHelperService], (service: PrintHelperService) => {
    expect(service).toBeTruthy();
  }));
});
