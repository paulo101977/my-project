import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Observable ,  Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CountService {
  public count = new Subject<any>();

  setFieldToCount(control: AbstractControl) {
    control.valueChanges.subscribe(value => {
      this.count.next({ count: value.length });
    });
  }

  getLenght(): Observable<any> {
    return this.count.asObservable();
  }
}
