import { inject, TestBed } from '@angular/core/testing';
import { ClientDocumentDto } from '../../models/dto/client/client-document-dto';
import { ClientDataDocumentService } from './client-data-document.service';

describe('ClientDataDocumentService', () => {
  let service: ClientDataDocumentService;
  const documents = [
    <ClientDocumentDto>{
      ownerId: 'eeae6861-e4f9-498e-8066-1e77f5db2305',
      documentTypeId: 1,
      documentTypeName: 'CPF',
      documentInformation: '134.001.222-00',
      documentType: null,
      id: 1,
    },
    <ClientDocumentDto>{
      ownerId: 'eeae6861-e4f9-498e-8066-1e77f5db2305',
      documentTypeId: 13,
      documentTypeName: 'Inscrição Estadual',
      documentInformation: '135.001.222-00',
      documentType: null,
      id: 2,
    },
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  beforeEach(inject([ClientDataDocumentService], (clientDataDocumentService: ClientDataDocumentService) => {
    service = clientDataDocumentService;
  }));

  it('should call getAllDocuments return array', () => {
    const list = service.getAllDocuments();
    expect(list).toEqual([]);
  });

  it('should resetDocumentList', () => {
    service.resetDocumentList();

    expect(service.documentList).toEqual([]);
  });
});
