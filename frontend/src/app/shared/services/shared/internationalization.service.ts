import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../user/user.service';
import * as moment from 'moment';

@Injectable({providedIn: 'root'})
export class InternationalizationService {

  constructor(public translate: TranslateService,
              private userService: UserService) {
    const user = this.userService.getUserLocalStorage();
    if (user) {
      this.setLanguageNavigator(user.preferredLanguage);
      moment.locale(user.preferredLanguage);
    } else {
      this.checkLanguageInAssets();
    }
    this.userService.userLocalStorageChanged$.subscribe( userLocal => {
      if (userLocal) {
        this.setLanguageNavigator(userLocal.preferredLanguage);
        moment.locale(userLocal.preferredLanguage);
      }
    });
  }

  private checkLanguageInAssets() {
    const language = navigator.language;
    this.translate.use(language).subscribe(() => {
      this.setLanguageNavigator(language);
      moment.locale(language);
    }, () => {
      this.setLanguageNavigator('en-US');
      moment.locale('en-US');
    });
  }

  public setLanguageNavigator(newLanguage: string) {
    this.translate.setDefaultLang(newLanguage);
    this.translate.use(newLanguage);
  }

  public getCountry(language: string): string {
    // example pt-br, en-us
    if (language.split('-') && language.split('-')[1]) {
      return language.split('-')[1];
    }
    return '';
  }
}
