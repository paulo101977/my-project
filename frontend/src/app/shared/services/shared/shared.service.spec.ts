import { ButtonSize, ButtonType } from '../../models/button-config';
import { inject, TestBed } from '@angular/core/testing';
import { Reservation } from './../../models/reserves/reservation';
import { ReservationStatusEnum } from './../../models/reserves/reservation-status-enum';
import { SharedService } from './shared.service';

describe('SharedService', () => {
  let service: SharedService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  beforeEach(inject([SharedService], (sharedService: SharedService) => {
    service = sharedService;
  }));

  it('should object is not null or undefined', () => {
    const reservation = new Reservation();
    expect(service.objectIsNullOrUndefined(reservation)).toBeFalsy();
  });

  it('should object is null or undefined', () => {
    expect(service.objectIsNullOrUndefined(null)).toBeTruthy();
  });

  it('should createRange', () => {
    const rangeCreated = service.createRange(2);
    expect(rangeCreated.length).toEqual(2);
    expect(rangeCreated[0]).toEqual(1);
    expect(rangeCreated[1]).toEqual(2);
  });

  it('should return true in elementExistsInList', () => {
    const array = new Array<number>();
    array.push(1);
    const element = 1;

    const rangeCreated = service.elementExistsInList(element, array);
    expect(rangeCreated).toBeTruthy();
  });

  it('should return false in elementExistsInList', () => {
    const array = new Array<number>();
    array.push(1);
    const element = 2;

    const rangeCreated = service.elementExistsInList(element, array);
    expect(rangeCreated).toBeFalsy();
  });

  it('should return values of enum', () => {
    const enumReservation = ReservationStatusEnum;

    const arrayValuesToCompare = [
      'ToConfirm',
      'Confirmed',
      'Checkin',
      'Checkout',
      'Pending',
      'NoShow',
      'Canceled',
      'WaitList',
      'ReservationProposal',
    ];

    expect(service.getValuesEnum(enumReservation)).toEqual(arrayValuesToCompare);
  });

  it('should return values translated of enum', () => {
    const enumReservation = ReservationStatusEnum;

    const arrayValuesToCompare = [
      'reservationModule.reservationList.reservationStatusEnum.ToConfirm',
      'reservationModule.reservationList.reservationStatusEnum.Confirmed',
      'reservationModule.reservationList.reservationStatusEnum.Checkin',
      'reservationModule.reservationList.reservationStatusEnum.Checkout',
      'reservationModule.reservationList.reservationStatusEnum.Pending',
      'reservationModule.reservationList.reservationStatusEnum.NoShow',
      'reservationModule.reservationList.reservationStatusEnum.Canceled',
      'reservationModule.reservationList.reservationStatusEnum.WaitList',
      'reservationModule.reservationList.reservationStatusEnum.ReservationProposal',
    ];

    expect(service.tranlasteEnumToView(enumReservation, 'reservationModule.reservationList.reservationStatusEnum')).toEqual(
      arrayValuesToCompare,
    );
  });

  it('should getButtonConfig', () => {
    const goBackPreviousPageFunction = () => {};
    expect(
      service.getButtonConfig('tax-create-edit-cancel', goBackPreviousPageFunction, 'commomData.cancel', ButtonType.Secondary).id,
    ).toEqual('tax-create-edit-cancel');
    expect(
      service.getButtonConfig('tax-create-edit-cancel', goBackPreviousPageFunction, 'commomData.cancel', ButtonType.Secondary).callback,
    ).toEqual(goBackPreviousPageFunction);
    expect(
      service.getButtonConfig('tax-create-edit-cancel', goBackPreviousPageFunction, 'commomData.cancel', ButtonType.Secondary).textButton,
    ).toEqual('commomData.cancel');
    expect(
      service.getButtonConfig('tax-create-edit-cancel', goBackPreviousPageFunction, 'commomData.cancel', ButtonType.Secondary).buttonType,
    ).toEqual(ButtonType.Secondary);
    expect(
      service.getButtonConfig(
        'tax-create-edit-cancel',
        goBackPreviousPageFunction,
        'commomData.cancel',
        ButtonType.Secondary,
        ButtonSize.Small,
      ).buttonSize,
    ).toEqual(ButtonSize.Small);
    expect(
      service.getButtonConfig(
        'tax-create-edit-cancel',
        goBackPreviousPageFunction,
        'commomData.cancel',
        ButtonType.Secondary,
        ButtonSize.Small,
        true,
      ).isDisabled,
    ).toBeTruthy();
  });

  it('should removeElementFromList', () => {
    const element1 = { id: 1, name: 'elemento 1' };
    const element2 = { id: 2, name: 'elemento 2' };
    const element3 = { id: 3, name: 'elemento 3' };

    const list = [element1, element2, element3];

    service.removeElementFromList(element2, list);

    expect(list.length).toEqual(2);
    expect(list).not.toContain(element2);
  });
});
