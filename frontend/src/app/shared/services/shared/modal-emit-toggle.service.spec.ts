import { inject, TestBed } from '@angular/core/testing';
import { ModalEmitToggleService } from './modal-emit-toggle.service';

describe('ModalEmitToggleService', () => {
  let service: ModalEmitToggleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  beforeEach(inject([ModalEmitToggleService], (modalEmitToggleService: ModalEmitToggleService) => {
    service = modalEmitToggleService;
  }));

  it('should call pushModals with argument, expected to increase one more key in the object this.modals and value toEqual FALSE', () => {
    service.pushModals('test1');
    service.pushModals('test2');
    service.pushModals('test3');

    expect(service.modals['test1']).toBe(false);
    expect(service.modals['test2']).toBe(false);
    expect(service.modals['test3']).toBe(false);
  });

  it('should call getModals, expected to return this.modals object', () => {
    service.getModals();

    expect(service.getModals()).toEqual({});
  });

  it('should call pushModals and getModals, expected to return this.modals object with key pushed', () => {
    service.pushModals('test');
    service.getModals();

    expect(service.getModals()).toEqual({ test: false });
  });

  it('should call emitToggleWithRef and change REF for TRUE or FALSE', () => {
    service.pushModals('test');

    service.emitToggleWithRef('test');

    expect(service.getModals()['test']).toEqual(true);

    service.emitToggleWithRef('test');

    expect(service.getModals()['test']).toEqual(false);
  });
});
