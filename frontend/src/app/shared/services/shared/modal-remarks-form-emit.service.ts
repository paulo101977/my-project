import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ModalRemarksFormEmitService {
  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();
  // Service message commands
  emitSimpleModal(internalComents: string, externalComents: string, partnerComents: string) {
    this.emitChangeSource.next([internalComents, externalComents, partnerComents]);
  }
}
