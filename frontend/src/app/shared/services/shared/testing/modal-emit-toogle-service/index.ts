import { createServiceStub } from '../../../../../../../testing';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { of } from 'rxjs';

export const  modalToggleServiceStub = createServiceStub(ModalEmitToggleService, {
  changeEmitted$: of( null ),
  emitCloseWithRef(modalId: any): void {},
  closeAllModals(): void {},
  emitToggleWithRef(modalId: any): void {},
  emitOpenWithRef(modalId: any): void {},
});
