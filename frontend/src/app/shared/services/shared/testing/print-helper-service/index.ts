import { createServiceStub } from '../../../../../../../testing';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { Type } from '@angular/core';
import { PrintComunicator } from 'app/shared/interfaces/print-comunicator';

export const printHelperServiceStub = createServiceStub(PrintHelperService, {
    printHtml(html: any) {
    },

    printComponent(type: Type<any>, comunicator?: PrintComunicator, plainTextMode?: boolean) {
        return true;
    }
});

