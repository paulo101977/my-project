import { InternationalizationService } from 'app/shared/services/shared/internationalization.service';
import { createServiceStub } from '../../../../../../../testing';

export const internationalizationServiceStub = createServiceStub(InternationalizationService, {
  getCountry(language: string): string {
    return null;
  }
});
