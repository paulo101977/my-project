import { createServiceStub } from '../../../../../../../testing';
import { ModalEmitService } from 'app/shared/services/shared/modal-emit.service';

export const modalEmitServiceStub = createServiceStub(ModalEmitService, {
    emitSimpleModal(title: string, message: string, callbackFunction: any, actions: Array<any> = [], closeCallback?: Function) {
    }
});
