import { createServiceStub } from '../../../../../../../testing';
import { ValidatorFormService } from 'app/shared/services/shared/validator-form.service';
import { FormControl, FormGroup } from '@angular/forms';

export const validatorFormServiceStub = createServiceStub(ValidatorFormService, {
  invalidPortugueseNif: (valueField: any) => null,
  dontAcceptZeroValue: (valueField: any) => null,
  capacityNumberIsMajorThanOne: (capacityControl: any) => null,
  inputIsInvalidAndDirty: (formGroup: FormGroup, controlName: string) => true,
  noWhitespace: (valueField: any) => null,
  notAcceptNegatives: (valueField: any) => null,
  setNumberToOne: (valueField: FormControl) => null,
  setNumberToZero: (valueField: FormControl) => null,
});
