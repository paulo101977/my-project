import { createServiceStub } from '../../../../../../../testing';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ButtonConfig, ButtonSize, ButtonType, Type as TypeButton } from 'app/shared/models/button-config';

export const sharedServiceStub = createServiceStub(SharedService, {
  getButtonConfig: (
    id: string,
    callback: Function,
    textButton: string,
    buttonType: ButtonType,
    buttonSize?: ButtonSize,
    isDisabled?: boolean,
    type?: TypeButton
  ) => new ButtonConfig(),
  getHeaders(): any {}
});
