import { createServiceStub } from '../../../../../../../testing';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';

export const currencyServiceStub = createServiceStub(CurrencyService, {
  getDefaultCurrencySymbol: (propertyId: number) => ''
});
