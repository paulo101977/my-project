import { createServiceStub } from '../../../../../../../testing';
import { PropertyDateService } from 'app/shared/services/shared/property-date.service';

export const propertyDateServiceStub = createServiceStub(PropertyDateService, {
  getTodayDatePropertyTimezone: (propertyId: number) => '',
});
