import { createServiceStub } from '../../../../../../../testing';
import { CommomService } from 'app/shared/services/shared/commom.service';

export const commomServiceStub = createServiceStub(CommomService, {
  toOption: (objectToConvert: any, keyName: string, valueName: string) => [],
  convertObjectToSelectOptionObjectByIdAndValueProperties(objectToConvert: any, keyName: string, valueName: string): any {
  }
});
