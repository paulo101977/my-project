import { createServiceStub } from '../../../../../../../testing';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';

export const toasterEmitServiceStub = createServiceStub(ToasterEmitService, {
  emitChange(isSuccess: SuccessError, message: string) {},
  emitChangeTwoMessages(isSuccess: SuccessError, message: string, variable: string) {}
});
