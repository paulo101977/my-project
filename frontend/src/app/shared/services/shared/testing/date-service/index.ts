import { createServiceStub } from '../../../../../../../testing';
import { DateService } from 'app/shared/services/shared/date.service';
import { IMyDate } from 'mydaterangepicker';

export const dateServiceStub = createServiceStub(DateService, {
    getSystemDate: (propertyId: number) => null,

    getDateRangePicker: (dateBegin: string | Date, dateEnd: string | Date) => null,

    convertUniversalDateToIMyDate(universalDate: string): any | null {
    },

    addDays: (date: Date, days: number) => null,

    convertStringToDate: (dateString: string, format?: string) => null,

    getClearConfigDatePicker: () => null,

    convertUniversalDateToViewFormat(strUniversalDate: string): string {
        return '';
    },

    convertDateToStringWithFormat(date: Date, format: string): string | null {
        return '';
    },

    formatVigenceString(dateInit: string, dateEnd: string, separator: string = '-'): string {
        return '';
    },

    getTodayIMydate(): IMyDate {
      const today = new Date();
      return {
        year: today.getFullYear(),
        month: today.getMonth() + 1,
        day: today.getDate() - 1,
      };
    }
});
