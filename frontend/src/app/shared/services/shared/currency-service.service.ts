import { Injectable } from '@angular/core';
import { CurrencyFormatPipe } from '../../pipes/currency-format.pipe';
import { CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';

@Injectable({ providedIn: 'root' })
export class CurrencyService {
  constructor(
    private currencyPipe: CurrencyFormatPipe,
    private sessionParameterService: SessionParameterService
  ) {}

  public formatToPercent(value) {
    if (value && !isNaN(parseFloat(value))) {
      return this.currencyPipe.transform(parseFloat(value), '', 1) + ' %';
    }
    return value;
  }

  public verifyPecentageField(event, value, minValue?, maxValue?) {
    if (!value) {
      return 0;
    }
    if (value) {
      if (!minValue) {
        minValue = 0;
      }
      if (!maxValue) {
        maxValue = 99;
      }

      const backspaceCode = 8;
      const fieldValue = parseFloat(value);
      if (isNaN(fieldValue) && event.keyCode != backspaceCode) {
        value = null;
      } else {
        if (fieldValue < minValue) {
          value = minValue;
        }
        if (fieldValue > maxValue) {
          value = maxValue;
        }
      }
    }
    return value;
  }

  formatCurrencyToString(value, sign?) {
    return this.currencyPipe.transform(parseFloat(value), sign);
  }

  public getMaskRateConfig(): CurrencyMaskConfig {
    return {
      align: 'right',
      allowNegative: false,
      allowZero: true,
      decimal: '.',
      precision: 2,
      prefix: '',
      suffix: ' %',
      thousands: '',
    };
  }

  public getDefaultCurrencySymbol(propertyId: number): string {
    const parameter = this
      .sessionParameterService
      .getParameter(propertyId, ParameterTypeEnum.DefaultCurrency);

    if (parameter) {
      const selectedOption = this
        .sessionParameterService
        .extractSelectedOptionOfPossibleValues(parameter);

      if ( selectedOption ) {
        const title: string = selectedOption.title;
        const str = title.split('-')[0];
        if (str && str.length > 0) {
          return str;
        }
      }
    }
    return '';
  }

  public getCurrencyCode(currencyName: string) {
    switch (currencyName) {
      case 'pt-BR':
        return 'BRL';
      case 'en-UR':
        return 'USD';
      default:
        return 'BRL';
    }
  }

  public getCurrencySymbol(currencyName: any) {
    switch (currencyName && currencyName.title) {
      case 'Real':
        return 'R$';
      case 'Dólar':
        return '$';
      case 'Euro':
        return '€';
      case 'Peso Argentino':
        return '$';
      default:
        return 'R$';
    }
  }
}
