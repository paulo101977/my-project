import { inject, TestBed } from '@angular/core/testing';

import { ModalAccommodationBudgetFormEmitService } from './modal-accommodation-budget-form-emit.service';
import { RoomType } from '../../../room-type/shared/models/room-type';

describe('ModalAccommodationBudgetFormEmitService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  it('should create', inject([ModalAccommodationBudgetFormEmitService], (service: ModalAccommodationBudgetFormEmitService) => {
    expect(service).toBeTruthy();
  }));

  it('should execute next of the emitChangeSource in emitSimpleModal', inject(
    [ModalAccommodationBudgetFormEmitService],
    (service: ModalAccommodationBudgetFormEmitService) => {
      spyOn(service['emitChangeSource'], 'next');

      const roomTypes = [];

      const roomTypeMock = new RoomType();
      roomTypeMock.id = 1;
      roomTypeMock.name = 'Quarto Standard';
      roomTypeMock.abbreviation = 'SPCD';
      roomTypeMock.order = 1;
      roomTypeMock.adultCapacity = 2;
      roomTypeMock.childCapacity = 1;
      roomTypeMock.roomTypeBedTypeList = [];
      roomTypeMock.isActive = true;
      roomTypeMock.isInUse = false;

      const roomTypeMock2 = new RoomType();
      roomTypeMock2.id = 2;
      roomTypeMock2.name = 'Quarto Luxo';
      roomTypeMock2.abbreviation = 'LXO';
      roomTypeMock2.order = 1;
      roomTypeMock2.adultCapacity = 3;
      roomTypeMock2.childCapacity = 0;
      roomTypeMock2.roomTypeBedTypeList = [];
      roomTypeMock2.isActive = true;
      roomTypeMock2.isInUse = false;

      roomTypes.push(roomTypeMock);
      roomTypes.push(roomTypeMock2);

      const dates = [];
      dates.push(new Date());
      dates.push(new Date());
      dates.push(new Date());

      const dailyPrices = new Map<Date, number>();

      service.emitSimpleModal(roomTypes[0], roomTypes, dates, dailyPrices);

      expect(service['emitChangeSource'].next).toHaveBeenCalled();
    },
  ));
});
