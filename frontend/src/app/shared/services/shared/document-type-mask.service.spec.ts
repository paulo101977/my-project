import { inject, TestBed } from '@angular/core/testing';
import { DocumentTypeMaskService } from './document-type-mask.service';

describe('DocumentTypeMaskService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  it('should be created', inject([DocumentTypeMaskService], (service: DocumentTypeMaskService) => {
    expect(service).toBeTruthy();
  }));
});
