import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn } from '@angular/forms';
import { NifValidatorService } from 'app/shared/services/nif-validator/nif-validator.service';
import { NfiValidatorPatterns } from 'app/shared/models/nfi-validator-patterns';

let stopObservable = false;

@Injectable({ providedIn: 'root' })
export class ValidatorFormService {
  public setNumberToZero(valueField: FormControl): any {
    if (valueField.value === null || isNaN(valueField.value)) {
      stopObservable = false;
      return valueField.setValue(0);
    }

    if (valueField.value >= 1 && !stopObservable) {
      stopObservable = true;
      return valueField.setValue(valueField.value);
    }
  }

  public noWhitespace(valueField: any) {
    const isWhitespace = (valueField.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  public capacityNumberIsMajorThanOne(capacityControl: any) {
    if (capacityControl.value === null || capacityControl.value === undefined) {
      return null;
    } else {
      return capacityControl.value.toString() >= 1 ? null : { required: true };
    }
  }
  public notAcceptNegatives(valueField: any) {
    if (valueField.value === null) {
      return null;
    } else {
      return valueField.value >= 0 ? null : { required: true };
    }
  }

  public setNumberToOne(valueField: FormControl) {
    if (valueField.value === null) {
      stopObservable = false;
      return valueField.setValue(1);
    }

    if (valueField.value > 1 && !stopObservable) {
      stopObservable = true;
      return valueField.setValue(valueField.value);
    }
  }

  public inputIsInvalidAndDirty(formGroup: FormGroup, controlName: string): boolean {
    return formGroup.get(controlName).invalid && formGroup.get(controlName).dirty;
  }

  public endDateIsMajorThanBeginDate = (form: AbstractControl) => {
    return form.get('beginDate').value > form.get('endDate').value
      ? {endDateInvalid: true}
      : null;
  }

  public dontAcceptZeroValue(valueField: any) {
    if (valueField.value !== null) {
      return valueField.value  > 0  ? null : { zeroValue: true };
    }
    return null;
  }

  public invalidPortugueseNif(valueField: any) {
    const { value } = valueField;
    if ( value !== null ) {
      return NifValidatorService
        .checkNif(value, NfiValidatorPatterns.PORTUGUESE_NFI_REGEX) ? null : { invalidPortugueseNif: true };
    }

    return null;
  }

  public regexValidator(regex: RegExp): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const valid = regex.test(control.value);
      return valid ? {'error': {value: control.value}} : null;
    };
  }
}
