import { TestBed } from '@angular/core/testing';

import { InternationalizationService } from './internationalization.service';
import { getUserData } from '../../mock-data/user.data';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';

describe('InternationalizationService', () => {
  let service: InternationalizationService;

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: [TranslateModule.forRoot()]
      });

    const store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      }
    };

    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);

    localStorage.setItem('user', JSON.stringify(getUserData()));

    service = TestBed.get(InternationalizationService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should setLanguageNavigator', () => {
    const userData = getUserData();
    spyOn(service.translate, 'setDefaultLang');
    spyOn(service.translate, 'use');

    service.setLanguageNavigator(userData.preferredLanguage);

    expect(service.translate.setDefaultLang).toHaveBeenCalledWith(userData.preferredLanguage);
    expect(service.translate.use).toHaveBeenCalledWith(userData.preferredLanguage);
  });

  it('should getCountry', () => {
    expect(service.getCountry('en-us')).toEqual('us');
  });

});
