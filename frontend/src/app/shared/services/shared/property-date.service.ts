import { Injectable } from '@angular/core';
import * as momentTz from 'moment-timezone';
import { ParameterTypeEnum } from '../../models/parameter/pameterType.enum';
import { SessionParameterService } from '../parameter/session-parameter.service';
import { DateService } from './date.service';

@Injectable({ providedIn: 'root' })
export class PropertyDateService {

  constructor(private sessionParameterService: SessionParameterService) {
  }

  public getTodayDatePropertyTimezone(propertyId: number) {
    const timezone = this.sessionParameterService.getParameterValue(propertyId, ParameterTypeEnum.TimeZone);
    return momentTz().tz(timezone).format(DateService.DATE_FORMAT_UNIVERSAL);
  }

}
