import { Injectable } from '@angular/core';
import { conformToMask } from 'angular2-text-mask';
import { DocumentTypeLegalEnum, DocumentTypeNaturalEnum } from '../../models/document-type';

@Injectable({ providedIn: 'root' })
export class DocumentTypeMaskService {
  constructor() {}

  public getDocumentMaskByDocumentTypeId(documentId: number): Array<any> {
    switch (+documentId) {
      case DocumentTypeNaturalEnum.CPF:
        return [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
      case DocumentTypeNaturalEnum.DNI:
        return null;
      case DocumentTypeNaturalEnum.CI:
        return null;
      case DocumentTypeNaturalEnum.RUT:
        return null;
      case DocumentTypeNaturalEnum.RUC:
        return null;
      case DocumentTypeNaturalEnum.NIT:
        return null;
      case DocumentTypeNaturalEnum.PASSPORT:
        return null;
      case DocumentTypeNaturalEnum.RG:
        return [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/];
      case DocumentTypeLegalEnum.CNPJ:
        return [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];
    }
  }

  public maskDocumentStringByDocumentTypeId(documentString: string, documentTypeId: number): string {
    const mask = this.getDocumentMaskByDocumentTypeId(+documentTypeId);
    if (mask) {
      const conformedDocumentMask = conformToMask(documentString, mask, { guide: false });
      return conformedDocumentMask.conformedValue;
    }
    return documentString;
  }
}
