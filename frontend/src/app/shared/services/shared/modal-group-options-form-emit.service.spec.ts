import { inject, TestBed } from '@angular/core/testing';
import { ModalGroupOptionsFormEmitService } from './modal-group-options-form-emit.service';

describe('ModalGroupOptionsFormEmitService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  it('should create', inject([ModalGroupOptionsFormEmitService], (service: ModalGroupOptionsFormEmitService) => {
    expect(service).toBeTruthy();
  }));

  it('should execute next of the emitChangeSource in emitSimpleModal', inject(
    [ModalGroupOptionsFormEmitService],
    (service: ModalGroupOptionsFormEmitService) => {
      spyOn(service['emitChangeSource'], 'next');
      const groupName = 'Grupo master';
      const unifyAccounts = true;

      service.emitSimpleModal(groupName, unifyAccounts);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith([groupName, unifyAccounts]);
    },
  ));
});
