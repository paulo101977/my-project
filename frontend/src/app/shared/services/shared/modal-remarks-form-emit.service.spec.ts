import { inject, TestBed } from '@angular/core/testing';

import { ModalRemarksFormEmitService } from './modal-remarks-form-emit.service';

describe('ModalRemarksFormEmitService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  it('should create', inject([ModalRemarksFormEmitService], (service: ModalRemarksFormEmitService) => {
    expect(service).toBeTruthy();
  }));

  it('should execute next of the emitChangeSource in emitSimpleModal', inject(
    [ModalRemarksFormEmitService],
    (service: ModalRemarksFormEmitService) => {
      spyOn(service['emitChangeSource'], 'next');
      const internalComents = 'Comentário Externo';
      const externalComents = 'Comentário Internto';
      const partnerComents = 'Comentário Parceiro';

      service.emitSimpleModal(internalComents, externalComents, partnerComents);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith([internalComents, externalComents, partnerComents]);
    },
  ));
});
