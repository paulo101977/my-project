import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ClientDocumentDto } from './../../models/dto/client/client-document-dto';

@Injectable({ providedIn: 'root' })
export class ClientDataDocumentService {
  public documentList: any;
  public allDocumentList = [];
  private emitChangeSource = new Subject<any>();
  changeEmitted$ = this.emitChangeSource.asObservable();

  constructor() {
    this.documentList = [];
  }

  public resetDocumentList(): void {
    this.documentList = [];
  }

  public getAllDocuments(): ClientDocumentDto[] {
    return this.allDocumentList;
  }
}
