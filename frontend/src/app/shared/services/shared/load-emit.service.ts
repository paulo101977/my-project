import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ShowHide } from './../../models/show-hide-enum';

@Injectable({ providedIn: 'root' })
export class LoadPageEmitService {
  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();
  // Service message commands
  emitChange(show: ShowHide) {
    this.emitChangeSource.next([show]);
  }
}
