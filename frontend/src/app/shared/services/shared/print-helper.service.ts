import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  EmbeddedViewRef,
  Injectable,
  Injector,
  Type
} from '@angular/core';
import { PrintComunicator } from '../../interfaces/print-comunicator';
import { PrintHtmlContainerComponent } from '../../components/print-html-container/print-html-container.component';

@Injectable({ providedIn: 'root' })
export class PrintHelperService {

  public plainTextMode: boolean;
  public domElem: HTMLElement;
  public componentRef: ComponentRef<any>;
  private counter: number;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector
  ) {
  }

  public printComponent(type: Type<any>, comunicator?: PrintComunicator, plainTextMode?: boolean) {
    const resolver = this.componentFactoryResolver.resolveComponentFactory(type);
    this.componentRef = resolver.create(this.injector);
    this.plainTextMode = plainTextMode;
    if (comunicator) {
      comunicator.populateComponent(this.componentRef);
    }
    this.componentRef.changeDetectorRef.detectChanges();
    this.appRef.attachView(this.componentRef.hostView);
    this.domElem = (this.componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    document.body.appendChild(this.domElem);

    const imgList = this.domElem.getElementsByTagName('img');
    const hasImage = imgList.length, len = imgList.length;
    this.counter = 0;

    if (hasImage) {
      Array.from(imgList).forEach(img => {
        img.addEventListener('load', () => this.incrementCounter(len), false);
        img.addEventListener('error', () => this.incrementCounter(len), false);
      });
    } else {
      this.printWindow();
    }
  }

  private incrementCounter(len: number) {
    this.counter++;
    if (this.counter === len) {
      this.printWindow();
    }
  }

  private printWindow() {
    if (this.plainTextMode) {
      const printWindow = window.open();
      printWindow.document.open('text/plain');
      printWindow.document.write(this.domElem.innerHTML);
      printWindow.document.close();
      printWindow.focus();
      printWindow.print();
      printWindow.close();
    } else {
      window.print();
    }
    this.componentRef.destroy();
  }

  public printHtml(html: any) {
    const comunicator = <PrintComunicator>{
      populateComponent(component) {
        const c = <PrintHtmlContainerComponent>component.instance;
        c.html = html;
      },
    };
    this.printComponent(PrintHtmlContainerComponent, comunicator);
  }
}
