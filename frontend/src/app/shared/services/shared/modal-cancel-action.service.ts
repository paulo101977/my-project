import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ModalEmitService } from '../../services/shared/modal-emit.service';

@Injectable({ providedIn: 'root' })
export class ModalCancelActionService {
  constructor(private _location: Location, private modalEmitService: ModalEmitService, private translateService: TranslateService) {}

  public cancelAction() {
    this.modalEmitService.emitSimpleModal(
      this.translateService.instant('commomData.warning'),
      this.translateService.instant('commomData.actionCancel'),
      this.backPreviewPage,
      [],
    );
  }

  private backPreviewPage = () => {
    this._location.back();
  }
}
