import { inject, TestBed } from '@angular/core/testing';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ValidatorFormService } from './validator-form.service';

describe('ValidatorFormService', () => {
  const data = new FormControl();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      providers: [],
    });
  });

  describe('on setNumberToZero', () => {
    it('should setNumberToZero if number is null', inject([ValidatorFormService], (service: ValidatorFormService) => {
      spyOn<any>(data, 'setValue').and.returnValue(0);
      expect(service.setNumberToZero(data)).toEqual(0);
    }));

    it('should not setNumberToZero if major or equal 1', inject([ValidatorFormService], (service: ValidatorFormService) => {
      spyOn<any>(data, 'setValue').and.returnValue(12);
      expect(service.setNumberToZero(data)).toEqual(12);
    }));
  });

  describe('on capacityNumberIsMajorThanOne', () => {
    it('should capacityNumberIsMajorThanOne return null when value is null', inject(
      [ValidatorFormService],
      (service: ValidatorFormService) => {
        const dataCapacity = {
          value: null,
        };
        expect(service.capacityNumberIsMajorThanOne(data)).toEqual(null);
      },
    ));

    it('should capacityNumberIsMajorThanOne return {required: true} when value is minor than 1', inject(
      [ValidatorFormService],
      (service: ValidatorFormService) => {
        const dataCapacity = {
          value: 0,
        };
        expect(service.capacityNumberIsMajorThanOne(dataCapacity)).toEqual({ required: true });
      },
    ));
  });
  describe('Whitespace Validator', () => {
    it('empty string is invalid', inject([ValidatorFormService], (service: ValidatorFormService) => {
      const control = { value: '' };
      const result = service.noWhitespace(control);
      expect(result !== null).toBe(true);
      expect(result['whitespace']).toBe(true);
    }));

    it('should notAcceptNegatives return {required: true} when value is minor than 0', inject(
      [ValidatorFormService],
      (service: ValidatorFormService) => {
        const dataCapacity = {
          value: -1,
        };
        expect(service.notAcceptNegatives(dataCapacity)).toEqual({ required: true });
      },
    ));

    it('should notAcceptNegatives return NULL when value is if major or equal 0', inject(
      [ValidatorFormService],
      (service: ValidatorFormService) => {
        const dataCapacity = {
          value: 1,
        };
        expect(service.notAcceptNegatives(dataCapacity)).toEqual(null);
      },
    ));
  });
  describe('rgexValidator', () => {

    const numberRegex = /\d/g;

    it('should be valid', inject([ValidatorFormService], (service: ValidatorFormService) => {
      const control = new FormControl('ABC', [service.regexValidator(numberRegex)]);
      expect(control.valid).toBeTruthy();
    }));

    it('should be invalid', inject([ValidatorFormService], (service: ValidatorFormService) => {
      const control = new FormControl(123, [service.regexValidator(numberRegex)]);
      expect(control.valid).toBeFalsy();
    }));
  });
});

