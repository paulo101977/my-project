import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { PaymentTypeService } from './payment-type.service';

describe('PaymentTypeService', () => {
  let service: PaymentTypeService;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [],
    });

    service = TestBed.get(PaymentTypeService);
  });

  it('should set payments and get paymentList when isWakin is false', () => {
    expect(service.getPaymentTypeList(false)).toEqual([
      service['internalUsage'],
      service['deposit'],
      service['courtesy'],
      service['hotel'],
      service['creditCard'],
    ]);

    expect(service['courtesy'].key).toEqual(3);
    expect(service['courtesy'].value).toEqual('reservationModule.paymentTypes.courtesy');
    expect(service['internalUsage'].key).toEqual(1);
    expect(service['internalUsage'].value).toEqual('reservationModule.paymentTypes.internalUsage');
    expect(service['creditCard'].key).toEqual(5);
    expect(service['creditCard'].value).toEqual('reservationModule.paymentTypes.creditCard');
    expect(service['deposit'].key).toEqual(2);
    expect(service['deposit'].value).toEqual('reservationModule.paymentTypes.deposit');
    expect(service['hotel'].key).toEqual(4);
    expect(service['hotel'].value).toEqual('reservationModule.paymentTypes.hotel');
  });

  it('should set payments and get paymentList when isWakin is true', () => {
    expect(service.getPaymentTypeList(true)).toEqual([
      service['internalUsage'],
      service['courtesy'],
      service['hotel'],
    ]);

    expect(service['courtesy'].key).toEqual(3);
    expect(service['courtesy'].value).toEqual('reservationModule.paymentTypes.courtesy');
    expect(service['internalUsage'].key).toEqual(1);
    expect(service['internalUsage'].value).toEqual('reservationModule.paymentTypes.internalUsage');
    expect(service['hotel'].key).toEqual(4);
    expect(service['hotel'].value).toEqual('reservationModule.paymentTypes.hotel');
  });

  it('should set payments and get paymentList when hasClient is true', () => {
    expect(service.getPaymentTypeList(false, true)).toEqual([
      service['internalUsage'],
      service['deposit'],
      service['courtesy'],
      service['hotel'],
      service['creditCard'],
      service['toBeBillet'],
    ]);

    expect(service['courtesy'].key).toEqual(3);
    expect(service['courtesy'].value).toEqual('reservationModule.paymentTypes.courtesy');
    expect(service['internalUsage'].key).toEqual(1);
    expect(service['internalUsage'].value).toEqual('reservationModule.paymentTypes.internalUsage');
    expect(service['creditCard'].key).toEqual(5);
    expect(service['creditCard'].value).toEqual('reservationModule.paymentTypes.creditCard');
    expect(service['deposit'].key).toEqual(2);
    expect(service['deposit'].value).toEqual('reservationModule.paymentTypes.deposit');
    expect(service['hotel'].key).toEqual(4);
    expect(service['hotel'].value).toEqual('reservationModule.paymentTypes.hotel');
    expect(service['toBeBillet'].key).toEqual(6);
    expect(service['toBeBillet'].value).toEqual('reservationModule.paymentTypes.toBeBillet');
  });

  it('should return true when payment is courtesy', () => {
    expect(service.isCortesy(service['courtesy'].key)).toBeTruthy();
  });

  it('should return false when payment is not courtesy', () => {
    expect(service.isCortesy(service['hotel'].key)).toBeFalsy();
  });

  it('should return true when payment is toBeBillet', () => {
    expect(service.isToBeBillet(service['toBeBillet'].key)).toBeTruthy();
  });

  it('should return true when payment is hotel', () => {
    expect(service.isHotel(service['hotel'].key)).toBeTruthy();
  });

  it('should return true when payment is deposit', () => {
    expect(service.isDeposit(service['deposit'].key)).toBeTruthy();
  });

  it('should return true when payment is creditCard', () => {
    expect(service.isCreditCard(service['creditCard'].key)).toBeTruthy();
  });

  it('should return true when payment is internalUsage', () => {
    expect(service.isInternalUsage(service['internalUsage'].key)).toBeTruthy();
  });
});
