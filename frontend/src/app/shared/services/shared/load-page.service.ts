
import {timer as observableTimer,  Observable ,  Subject ,  Subscription } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class LoadPageService {
  private emitChangeSource = new Subject<any>();
  changeEmitted$ = this.emitChangeSource.asObservable();

  private instances = 0;
  private subscription: Subscription;

  public show() {
    this.emitChangeSource.next(true);
    this.instances++;

    if (this.subscription && !this.subscription.closed) {
      this.subscription.unsubscribe();
    }
  }

  public hide() {
    this.instances--;

    if (this.instances == 0) {
      this.subscription = observableTimer(300).subscribe(() => this.emitChangeSource.next(false));
    }
  }
}
