import { TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared.module';
import { SelectObjectOption } from './../../models/selectModel';
import { ReasonCategoryCancelData } from './../../mock-data/reason-category-cancel-data';
import { ReasonCategoryCancel } from './../../models/reasonCategoryCancel';
import { CommomService } from './commom.service';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('CommomService', () => {
  let service: CommomService;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [SharedModule, RouterTestingModule, TranslateModule.forRoot()],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    service = TestBed.get(CommomService);
  });

  it('should convertObjectToSelectOptionObject', () => {
    const reasonList = new Array<SelectObjectOption>();
    const reasonCategoryCancelDataList = {
      items: new Array<ReasonCategoryCancel>(),
    };
    reasonCategoryCancelDataList.items.push(ReasonCategoryCancelData);

    const reasonObj = new SelectObjectOption();
    reasonObj.key = ReasonCategoryCancelData.id;
    reasonObj.value = ReasonCategoryCancelData.reasonName;
    reasonList.push(reasonObj);

    expect(service.convertObjectToSelectOptionObject(reasonCategoryCancelDataList)).toEqual(reasonList);
  });

  it('should convertObjectToSelectOptionObjectByIdAndValueProperties', () => {
    const reasonList = [];
    const reasonCategoryCancelDataList = {
      items: new Array<ReasonCategoryCancel>(),
    };
    reasonCategoryCancelDataList.items.push(ReasonCategoryCancelData);

    const reasonObj = {
      key: ReasonCategoryCancelData.id,
      value: ReasonCategoryCancelData.reasonName,
    };
    reasonList.push(reasonObj);

    expect(service.convertObjectToSelectOptionObjectByIdAndValueProperties(reasonCategoryCancelDataList, 'id', 'reasonName')).toEqual(
      reasonList,
    );
  });

  it('should toOption', () => {
    const reasonList = [];
    const reasonCategoryCancelDataList = {
      items: new Array<ReasonCategoryCancel>(),
    };
    reasonCategoryCancelDataList.items.push(ReasonCategoryCancelData);

    const reasonObj = {
      key: ReasonCategoryCancelData.id,
      value: ReasonCategoryCancelData.reasonName,
    };
    reasonList.push(reasonObj);

    expect(service.toOption(reasonCategoryCancelDataList, 'id', 'reasonName')).toEqual(reasonList);
  });
});
