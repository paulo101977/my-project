import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SuccessError } from './../../models/success-error-enum';

@Injectable({ providedIn: 'root' })
export class ToasterEmitService {
  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();

  // Service message commands
  emitChange(isSuccess: SuccessError, message: string) {
    this.emitChangeSource.next([isSuccess, message]);
  }

  emitChangeTwoMessages(isSuccess: SuccessError, message: string, variable: string) {
    const msgToShow = message + ' ' + variable;
    this.emitChangeSource.next([isSuccess, msgToShow]);
  }
}
