import { TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { BaseRateService } from './base-rate.service';
import { baseRateMealTypeListData } from '../../mock-data/base-rate-config-meal-type-data';

describe('BaseRateService', () => {
  let service: BaseRateService;

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: []
      });

    service = TestBed.get(BaseRateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return some valid with checkMealTypeValues', () => {
    expect(service.checkMealTypeValues(baseRateMealTypeListData)).toBeTruthy();
  });

  it('should return any valid with checkMealTypeValues', () => {
    baseRateMealTypeListData[0].adultMealPlanAmount = null;
    baseRateMealTypeListData[1].adultMealPlanAmount = null;
    expect(service.checkMealTypeValues(baseRateMealTypeListData)).toBeFalsy();
  });
});
