import { createServiceStub } from '../../../../../../testing';
import { RateProposalShareInfoService } from 'app/shared/services/rate-proposal/rate-proposal-share-info.service';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';

export const rateProposalShareInfoServiceStub = createServiceStub(RateProposalShareInfoService, {
  storeRateProposal: (rateProposalSearch: RateProposalSearchModel, rateProposalItem: RateProposalRateTypeItem) => '',
});
