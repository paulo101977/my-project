import { inject, TestBed } from '@angular/core/testing';

import { RateProposalShareInfoService } from './rate-proposal-share-info.service';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';
import {
  ReservationItemCommercialBudgetConfig
} from 'app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';

describe('RateProposalShareInfoService', () => {
  let rateProposalSearch: RateProposalSearchModel;
  let rateProposalRateItem: RateProposalRateTypeItem;

  let objFake = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RateProposalShareInfoService]
    });

    rateProposalSearch = new RateProposalSearchModel();
    rateProposalSearch.roomQtd = 1;
    rateProposalSearch.initialDate = '2018-01-01T00:00:00';
    rateProposalSearch.finalDate = '2018-01-14T00:00:00';
    rateProposalSearch.roomTypeIdSort = 1;
    rateProposalSearch.childrenAge = [1];
    rateProposalSearch.adultCount = 2;

    rateProposalRateItem = new RateProposalRateTypeItem();
    rateProposalRateItem.budgetConfig = new ReservationItemCommercialBudgetConfig();
    rateProposalRateItem.commercialBudgetList = [];

    spyOn(localStorage, 'setItem').and.callFake( (key, value: Object) => {
      if (!objFake) {
        objFake = {};
      }
      objFake[key] = {...value};
    });

    spyOn(localStorage, 'removeItem').and.callFake( (key) => {
      if (objFake && objFake[key]) {
        delete objFake[key];
      }
    });

    spyOn(localStorage, 'getItem').and.callFake( (key) => {

      if (objFake && objFake[key]) {
        return objFake[key];
      }
      return null;
    });

    spyOn(localStorage, 'clear').and.callFake( () => {
      objFake = null;
    });
  });

  it('should be created', inject([RateProposalShareInfoService], (service: RateProposalShareInfoService) => {
    expect(service).toBeTruthy();
  }));

  it('should add item to Storage', inject([RateProposalShareInfoService], (service: RateProposalShareInfoService) => {

    service.storeRateProposal(rateProposalSearch, rateProposalRateItem);

    expect(service.rateProposalStorage).toBeDefined();
    expect( Object.keys(service.rateProposalStorage).length).toBe(1);
  }));

  it('should return item stored', inject([RateProposalShareInfoService], (service: RateProposalShareInfoService) => {
    const token = service.storeRateProposal(rateProposalSearch, rateProposalRateItem);

    const value = service.getRateProposal(token);

    expect(value).toBeDefined();
  }));

  it('should remove item from store', inject([RateProposalShareInfoService], (service: RateProposalShareInfoService) => {
    const token = service.storeRateProposal(rateProposalSearch, rateProposalRateItem);

    service.removeRateProposal(token);

    expect(Object.keys(service.rateProposalStorage).length).toBe(0);
  }));

  it('should remove all items from store', inject([RateProposalShareInfoService], (service: RateProposalShareInfoService) => {
    const token1 = service.storeRateProposal(rateProposalSearch, rateProposalRateItem);
    const token2 = service.storeRateProposal(rateProposalSearch, rateProposalRateItem);

    service.cleanAll();

    expect(Object.keys(service.rateProposalStorage).length).toBe(0);
  }));

  describe('#persist on localStorage', () => {

    let token;

    beforeEach( inject([RateProposalShareInfoService], (service: RateProposalShareInfoService)  => {
      token = service.storeRateProposal(rateProposalSearch, rateProposalRateItem);

      service['persistRateProposal']();
    }));

    afterEach(() => {
      localStorage.clear();
    });

    it('should store rateProposal on localStorage',
      inject([RateProposalShareInfoService], (service: RateProposalShareInfoService) => {

      expect(localStorage.setItem).toHaveBeenCalledWith(service['LOCAL_STORAGE_KEY'], JSON.stringify(service.rateProposalStorage));
      expect(Object.keys(objFake).length).toBe(1);
    }));

  });



});
