import { HostListener, Injectable, OnDestroy } from '@angular/core';
import { RateProposalStorageItem } from 'app/reservation/rate-proposal/models/rate-proposal-storage-item';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';
import { IRateProposalStorage } from 'app/shared/models/rate-proposal/i-rate-proposal-storage';


@Injectable({
  providedIn: 'root'
})
export class RateProposalShareInfoService {

  private readonly LOCAL_STORAGE_KEY = 'rate-proposal';

  constructor() {
    window.onbeforeunload = () => {
      this.persistRateProposal();
    };
    this.getPersistedRateProposal();
  }

  private _rateProposalStorage: IRateProposalStorage;
  set rateProposalStorage(rateProposal: IRateProposalStorage) {
    this._rateProposalStorage = rateProposal;
  }
  get rateProposalStorage(): IRateProposalStorage {
    return this._rateProposalStorage;
  }

  public storeRateProposal(rateProposalSearch: RateProposalSearchModel, rateProposalItem: RateProposalRateTypeItem) {
    if (!this.rateProposalStorage) {
      this.rateProposalStorage = {};
    }
    const store = new RateProposalStorageItem();
    store.rateProposalSearchModel = rateProposalSearch;
    store.rateProposalTypeItemItem = rateProposalItem;
    const token = this.generateToken(rateProposalItem);
    this.rateProposalStorage[token] = store;
    return token;
  }

  public getRateProposal(token: string) {
    return this.rateProposalStorage[token];
  }


  public removeRateProposal(token: string) {
    delete this.rateProposalStorage[token];
  }

  public cleanAll() {
    this.rateProposalStorage = {};
  }

  private generateToken(rateProposalItem: RateProposalRateTypeItem) {
    // TODO improve token generation based on rateProposalItem
    const random = Math.random().toString(36).substr(2);
    return `${random}-${new Date().toStringUniversal()}`;
  }

  private persistRateProposal () {
    if (this.rateProposalStorage) {
      localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(this.rateProposalStorage));
    }
  }

  private getPersistedRateProposal() {
    const storedInfo = localStorage.getItem(this.LOCAL_STORAGE_KEY);
    this.rateProposalStorage = storedInfo ? <IRateProposalStorage>JSON.parse(storedInfo) : {};
    localStorage.removeItem(this.LOCAL_STORAGE_KEY);
  }

}
