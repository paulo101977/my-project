import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PdfPrintService {

  constructor() { }

  public printPdfFromByteCode(byteCode) {
    const printIframe = document.createElement('iframe');
    const blobUrl = URL.createObjectURL(byteCode);

    printIframe.style.display = 'none';
    printIframe.src = blobUrl;
    document.body.appendChild(printIframe);
    printIframe.contentWindow.print();
    printIframe.contentWindow.onafterprint = () => {
      document.body.removeChild(printIframe);
    };
  }
}
