import { createServiceStub } from '../../../../../../testing';
import { PdfPrintService } from 'app/shared/services/pdf-print/pdf-print.service';

export const pdfPrintServiceStub = createServiceStub(PdfPrintService, {
    printPdfFromByteCode(byteCode) {
    }
});
