import { TestBed, inject } from '@angular/core/testing';

import { PdfPrintService } from './pdf-print.service';

describe('PdfPrintService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PdfPrintService]
    });
  });

  it('should be created', inject([PdfPrintService], (service: PdfPrintService) => {
    expect(service).toBeTruthy();
  }));
});
