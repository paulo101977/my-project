import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Location } from 'app/shared/models/location';

@Injectable({ providedIn: 'root' })
export class FnrhService {

  private emitPrintFnrh = new Subject<any>();
  fnrhPrintEmmitedd$ = this.emitPrintFnrh.asObservable();

  // LocationDto in FnrhDto
  public fnrhAddressToApply: Location;

  public printFnrh() {
    this.emitPrintFnrh.next();
  }
}
