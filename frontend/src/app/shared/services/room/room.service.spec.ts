import { TranslateModule } from '@ngx-translate/core';
import { TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { Room } from './../../models/room';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { RoomService } from './room.service';

describe('RoomService', () => {
  let service: RoomService;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [],
    });

    service = TestBed.get(RoomService);
  });

  it('should create RoomService', () => {
    expect(service).toBeTruthy();
  });

  it('should chosenConjugatedRooms to be valid', () => {
    const chosenConjugatedRooms = new Array<Room>();
    chosenConjugatedRooms.push(new Room());
    chosenConjugatedRooms.push(new Room());

    expect(service.conjugatedRoomListIsInvalid(chosenConjugatedRooms)).toEqual(false);
  });

  it('should chosenConjugatedRooms to be invalid', () => {
    const chosenConjugatedRooms = new Array<Room>();
    chosenConjugatedRooms.push(new Room());

    expect(service.conjugatedRoomListIsInvalid(chosenConjugatedRooms)).toEqual(true);
  });

  describe('Get rooms by search data', () => {
    const roomType = new RoomType();
    roomType.abbreviation = 'STD';
    roomType.name = 'Standard';
    roomType.order = 1;

    const room = new Room();
    room.roomType = roomType;
    room.roomNumber = '100A';

    const rooms = new Array<Room>();
    rooms.push(room);

    it('should get room by roomType abbreviation', () => {
      const searchData = roomType.abbreviation;
      const roomsSearched = service.getRoomsBySearchData(searchData, rooms);
      expect(roomsSearched[0].roomType.abbreviation).toEqual(roomType.abbreviation);
    });

    it('should get room by roomType name', () => {
      const searchData = roomType.name;
      const roomsSearched = service.getRoomsBySearchData(searchData, rooms);
      expect(roomsSearched[0].roomType.name).toEqual(roomType.name);
    });

    it('should get room types by roomNumber', () => {
      const searchData = room.roomNumber;
      const roomsSearched = service.getRoomsBySearchData(searchData, rooms);
      expect(roomsSearched[0].roomNumber).toEqual(room.roomNumber);
    });

    it('should not get room types when the search data does not match in roomType abbreviation, roomType name, roomNumber, remarks', () => {
      const searchData = 'Does not exist';
      const roomsSearched = service.getRoomsBySearchData(searchData, rooms);
      expect(roomsSearched.length).toEqual(0);
    });
  });
});
