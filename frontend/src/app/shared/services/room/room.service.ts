import { Injectable } from '@angular/core';
import { Room } from '../../models/room';

@Injectable({ providedIn: 'root' })
export class RoomService {
  public getRoomsBySearchData(searchData: string, allItems: Array<Room>): Array<Room> {
    return allItems.filter(
      item =>
        item.roomType.name.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.roomType.abbreviation.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.roomNumber
          .toString()
          .toLocaleLowerCase()
          .search(searchData.toLocaleLowerCase()) > -1,
    );
  }

  public conjugatedRoomListIsInvalid(chosenConjugatedRooms: Array<Room>): boolean {
    return !(chosenConjugatedRooms != null && this.chosenConjugatedRoomsHasMoreOfTheOneElement(chosenConjugatedRooms));
  }

  public chosenConjugatedRoomsHasMoreOfTheOneElement(chosenConjugatedRooms: Array<Room>): boolean {
    return chosenConjugatedRooms.length > 1;
  }
}
