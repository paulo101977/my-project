import { createServiceStub } from '../../../../../../testing';
import { AuthService } from 'app/shared/services/auth-service/auth.service';
import { PropertyAuthInfo } from 'app/auth/models/property-auth-info';

export const authServiceStub = createServiceStub(AuthService, {
  getPropertyToken: () => new PropertyAuthInfo(),
  getPropertyCountryCode() { return 'BR'; },
});
