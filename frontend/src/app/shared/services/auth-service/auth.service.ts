import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Property } from '../../models/property';
import { PropertyToken } from '../../models/PropertyToken';
import { PropertyResource } from '../../resources/property/property.resource';
import { UserService } from '../user/user.service';
import { LoginForm } from '../../../auth/models/login-form';
import { PropertyAuthInfo } from '../../../auth/models/property-auth-info';
import { AuthResource } from '../../resources/auth/auth.resource';
import { PropertyStorageService } from '@bifrost/storage/services/property-storage.service';
import { TokenManagerService } from '@inovacaocmnet/thx-bifrost';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly KEY_PROPERTY_TOKEN = 'propertyToken';
  private readonly KEY_OLD_TOKEN = 'oldToken';
  private readonly KEY_TOKEN = 'token';
  public newPassToken: string;

  redirectUrl: string;

  constructor(
    private authResource: AuthResource,
    private propertyResource: PropertyResource,
    private userService: UserService,
    private tokenService: TokenManagerService
  ) {}

  public getToken(): string {
    return localStorage.getItem(this.KEY_TOKEN);
  }

  public setToken(token: string): void {
    const oldToken = localStorage.getItem(this.KEY_TOKEN);
    if ( oldToken) {
      localStorage.setItem(this.KEY_OLD_TOKEN, oldToken);
    }
    localStorage.setItem(this.KEY_TOKEN, token);
  }

  public rollbackToken() {
    const oldToken = localStorage.getItem(this.KEY_OLD_TOKEN);
    if (oldToken) {
      localStorage.setItem(this.KEY_TOKEN, oldToken);
      localStorage.removeItem(this.KEY_OLD_TOKEN);
    }
  }

  public getPropertyTokenFromLocalStorage(propertyId: number) {
    return JSON.parse(localStorage.getItem(`${this.KEY_PROPERTY_TOKEN}_${propertyId}`));
  }

  public addPropertyTokenOnLocalStorage(propertyId: number, propertyInfo: PropertyAuthInfo) {
    localStorage.setItem(`${this.KEY_PROPERTY_TOKEN}_${propertyId}`, JSON.stringify(propertyInfo));
  }

  private loginPromise(loginForm: LoginForm): Promise<any> {
    return new Promise<any>( (resolve, reject) => {
      this
        .authResource
        .login(loginForm)
        .subscribe( data => resolve(data), err => reject(err));
    });
  }

  public  async authenticate(loginForm: LoginForm): Promise<any> {
    try {
      const data = await this.loginPromise(loginForm);
      const { newPasswordToken } = data;

      this.setToken( newPasswordToken );

      await this.storeUserInfo( newPasswordToken );

      return new Promise((resolve, reject) => {
        if (newPasswordToken) {
          resolve(newPasswordToken);
        }

        reject(null);
      });
    } catch (e) {
      console.error(e);
      return new Promise((resolve, reject) => {
          reject(e);
      });
    }
  }

  private storeUserInfo(token: string): Promise<any> {
    return new Promise( resolve => {

      return this
        .authResource
        .getUserInfo()
        .subscribe(data => resolve(this.setUser(data)));
    });
  }

  public async authenticateOnProperty(propertyId: number) {
    try {
      const token = await this.authResource.getTokenByPropertyId(propertyId).toPromise();
      if (!token) {
        return false;
      }
      this.setPropertyToken(null, token);
      this.setToken(token.newPasswordToken);

      await this.updatePropertyInfoOnStorage(propertyId, token);
      return true;
    } catch (e) {
      console.error(e);
      return false;
    }
  }


  private async updatePropertyInfoOnStorage(propertyId: number, token) {
    const property = await this.propertyResource.getPropertyById(propertyId).toPromise();
    this.setPropertyToken(property, token);
  }

  public getPropertyToken(): PropertyAuthInfo {
    console.warn('DEPRECATED: use tokenManager from bifrost instead');
    return JSON.parse(sessionStorage.getItem(this.KEY_PROPERTY_TOKEN)) || {};
  }

  public setPropertyToken(property: Property, propertyTokenConfig: PropertyToken) {
    const propertyToken = {
      propertyId: property && property.id,
      token: propertyTokenConfig.newPasswordToken,
      propertyName: property && property.name,
    };

    this.addPropertyTokenInSession(propertyToken);
    if (property && property.id) {
      this.addPropertyTokenOnLocalStorage(property.id, propertyToken);
    }
  }

  public addPropertyTokenInSession(propertyToken: PropertyAuthInfo) {
    console.warn('DEPRECATED: use tokenManager from bifrost instead');
    sessionStorage.setItem(this.KEY_PROPERTY_TOKEN, JSON.stringify(propertyToken));
  }

  public setUser(user: any): void {
    try {
      this.userService.storeUserInLocalStorage(user);
    } catch (err) {
      this.logout();
      console.error(err);
    }
  }

  public isAuthenticated(): boolean {
    const token = this.getToken();
    const jwtHelper = new JwtHelperService();
    return token && !jwtHelper.isTokenExpired(token);
  }

  public isAuthenticatedOnProperty(propertyId: number): boolean {
    return !!this.getPropertyTokenFromLocalStorage(propertyId);
  }

  public logout(): void {
    this.cleanSession();
  }

  public cleanSession() {
    localStorage.clear();
    sessionStorage.clear();
  }

  public getRootUrl(): string {
    const propertyId = localStorage.getItem('property');

    return propertyId ? '/home' : '/auth/login';
  }

  public getPropertyCountryCode(): string {
    const token = this.tokenService.extractTokenPayload(this.tokenService.getCurrentPropertyToken());
    return token ? token.PropertyCountryCode : '';
  }
}
