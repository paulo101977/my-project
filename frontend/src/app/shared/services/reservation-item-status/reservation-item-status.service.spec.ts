import { TestBed } from '@angular/core/testing';
import { ReservationItemStatusService } from 'app/shared/services/reservation-item-status/reservation-item-status.service';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { ReservationStatusEnum } from './../../models/reserves/reservation-status-enum';

describe('ReservationItemViewService', () => {
  let service;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      providers: [],
    });

    service = new ReservationItemStatusService();
  });

  it('should canDoCheckin', () => {
    const status = ReservationStatusEnum.Confirmed;
    expect(service.canDoCheckin(status)).toBeTruthy();
  });

  it('should not canDoCheckin', () => {
    const status = ReservationStatusEnum.Checkin;
    expect(service.canDoCheckin(status)).toBeFalsy();
  });
});
