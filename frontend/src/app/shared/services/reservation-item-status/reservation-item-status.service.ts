import { Injectable } from '@angular/core';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';

@Injectable({ providedIn: 'root' })
export class ReservationItemStatusService {
  public canDoCheckin(reservationStatusEnum: ReservationStatusEnum): boolean {
    return reservationStatusEnum == ReservationStatusEnum.ToConfirm || reservationStatusEnum == ReservationStatusEnum.Confirmed;
  }
}
