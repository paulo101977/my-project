import { TestBed } from '@angular/core/testing';
import { StatusStyleService } from './status-style.service';
import { ReservationStatusEnum } from './../../models/reserves/reservation-status-enum';

describe('StatusStyleService', () => {
  let service: StatusStyleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [],
    });

    service = new StatusStyleService();
  });

  describe('on verify status', () => {
    it('should verify if return true when status is valid', () => {
      expect(service.isCanceledStatus(ReservationStatusEnum.Canceled)).toBeTruthy();
      expect(service.isToConfirmStatus(ReservationStatusEnum.ToConfirm)).toBeTruthy();
      expect(service.isConfirmedStatus(ReservationStatusEnum.Confirmed)).toBeTruthy();
      expect(service.isNoShowStatus(ReservationStatusEnum.NoShow)).toBeTruthy();
      expect(service.isPendingStatus(ReservationStatusEnum.Pending)).toBeTruthy();
      expect(service.isWaitListStatus(ReservationStatusEnum.WaitList)).toBeTruthy();
      expect(service.isCheckinStatus(ReservationStatusEnum.Checkin)).toBeTruthy();
      expect(service.isCheckoutStatus(ReservationStatusEnum.Checkout)).toBeTruthy();
      expect(service.isReservationProposalStatus(ReservationStatusEnum.ReservationProposal)).toBeTruthy();
    });
  });
});
