// import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
// import { FormGroup, FormBuilder, Validators, ValidatorFn, FormControl, FormArray, ReactiveFormsModule } from '@angular/forms';
// import { TranslateModule, TranslateService } from '@ngx-translate/core';
// import { Http } from '@angular/http';
//
// import { ReservationCreateComponent } from '../../../reservation/components/reservation-create/reservation-create.component';
//
// import { ReservationBudget } from './../../models/reserves/reservation-budget';
// import { RoomLayout } from './../../models/room-layout';
// import { ReservationItemView, ReservationItemViewStatusEnum } from './../../models/reserves/reservation-item-view';
// import { Reservation } from './../../models/reserves/reservation';
// import { ReservationStatusEnum } from './../../models/reserves/reservation-status-enum';
// import { Room } from './../../models/room';
// import { RoomType } from './../../models/room-type/room-type';
// import { Guest } from '../../models/reserves/guest';
// import { GuestStatus } from '../../models/reserves/guest';
// import { BedTypeEnum } from '../../models/bed-type';
// import { BedType } from '../../models/bed-type';
// import { RoomTypeLuxoData, RoomTypeStandardData } from './../../mock-data/room-type.data';
// import { ReservationItemViewData,
// ReservationItemViewData2,
// ReservationItemViewCancelData } from './../../mock-data/reservationItem-data';
// import { ReservationBudgetData1, ReservationBudgetData2, ReservationBudgetData4 } from '../../mock-data/reservation-budget-data';
//
// import { ReservationItemViewService } from './../reservation-item-view/reservation-item-view.service';
// import { SharedService } from './../shared/shared.service';
// import { ValidatorFormService } from './../shared/validator-form.service';
// import { DateService } from './../shared/date.service';
// import { ReservationService } from './reservation.service';
// import { RoomTypeService } from '../room-type/room-type.service';
// import { StatusStyleService } from './status-style.service';
//
// import { RoomLayoutResource } from './../../resources/room-layout/room-layout.resource';
//
// xdescribe('ReservationService', () => {
//   let service: ReservationService;
//   const hourMask = '[0-9]{2}:[0-9]{2}';
//   let newReservationForm: FormGroup;
//   let reservationItemCardForm: FormGroup;
//   let formBuilder: FormBuilder;
//   let translateService: TranslateService;
//   let validatorFormService: ValidatorFormService;
//   const roomTypes = [];
//   const reservationItemView = new ReservationItemView();
//   let reservationItemViewList = new Array<ReservationItemView>();
//   let roomQuantity, period, adultQuantity, childrenQuantity;
//   const guestArray = [];
//
//   const roomTypeMock = new RoomType();
//   roomTypeMock.id = 1;
//   roomTypeMock.name = 'Quarto Standard';
//   roomTypeMock.abbreviation = 'SPDC';
//   roomTypeMock.order = 1;
//   roomTypeMock.adultCapacity = 2;
//   roomTypeMock.childCapacity = 1;
//   roomTypeMock.roomTypeBedTypeList = new Array<BedType>();
//   roomTypeMock.isActive = true;
//   roomTypeMock.isInUse = false;
//
//   const roomTypeMock2 = new RoomType();
//   roomTypeMock2.id = 2;
//   roomTypeMock2.name = 'Quarto Luxo';
//   roomTypeMock2.abbreviation = 'LXO';
//   roomTypeMock2.order = 1;
//   roomTypeMock2.adultCapacity = 3;
//   roomTypeMock2.childCapacity = 0;
//   roomTypeMock2.roomTypeBedTypeList = new Array<BedType>();
//   roomTypeMock2.isActive = true;
//   roomTypeMock2.isInUse = false;
//
//   roomTypes.push(roomTypeMock);
//   roomTypes.push(roomTypeMock2);
//
//   const roomLayoutList = new Array<RoomLayout>();
//   const roomLayout = new RoomLayout();
//   roomLayout.id = '1122112aaa';
//   const roomLayout2 = new RoomLayout();
//   roomLayout2.id = '3333454bbb';
//
//   roomLayoutList.push(roomLayout);
//   roomLayoutList.push(roomLayout2);
//
//   let promiseGetRoomLayoutList;
//
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       imports: [TranslateModule.forRoot(), ReactiveFormsModule],
//       providers: [],
//     });
//   });
//
//   beforeEach(inject(
//     [ReservationService, FormBuilder, TranslateService, ValidatorFormService],
//     (
//       reservationService: ReservationService,
//       formBuilderMock: FormBuilder,
//       translateServiceMock: TranslateService,
//       validatorFormServiceMock: ValidatorFormService,
//     ) => {
//       service = reservationService;
//       formBuilder = formBuilderMock;
//       translateService = translateServiceMock;
//       validatorFormService = validatorFormServiceMock;
//
//       promiseGetRoomLayoutList = spyOn(service['roomLayoutResource'], 'getRoomLayoutListByRoomTypeId').and.callFake(() => {
//         return new Promise((resolve, reject) => resolve(roomLayoutList));
//       });
//
//       newReservationForm = formBuilder.group({
//         contactGroup: formBuilder.group({
//           client: ['', [Validators.required]],
//           name: ['', [Validators.required]],
//           email: ['', [Validators.required]],
//           phone: ['', [Validators.required]],
//         }),
//         reservationGroup: formBuilder.group({
//           period: [''],
//           checkinHour: ['', [Validators.required, Validators.pattern(hourMask)]],
//           checkoutHour: ['', [Validators.required, Validators.pattern(hourMask)]],
//           roomQuantity: [0, [Validators.required, validatorFormService.capacityNumberIsMajorThanOne,
// validatorFormService.setNumberToZero]],
//           adultQuantity: [
//             0,
//             [Validators.required, validatorFormService.capacityNumberIsMajorThanOne, validatorFormService.setNumberToZero],
//           ],
//           childrenQuantity: [0, [Validators.required, validatorFormService.setNumberToZero]],
//           roomType: ['', Validators.required],
//           roomTypeRequested: ['', Validators.required],
//           childrenAgeList: formBuilder.array([
//             formBuilder.group({
//               childAge: [0],
//             }),
//           ]),
//         }),
//       });
//
//       reservationItemCardForm = formBuilder.group({
//         periodCard: [''],
//         adultQuantityCard: [1],
//         childrenQuantityCard: [0],
//         roomTypeCard: [''],
//         checkinHourCard: [''],
//         checkoutHourCard: [''],
//         roomNumberCard: [''],
//         roomLayoutCard: [''],
//       });
//     },
//   ));
//
//   it('should return zero when reservation does not have remarks in getRemarksQuantityOfReservation', () => {
//     const reservation = new Reservation();
//
//     const remarksQuantity = service.getRemarksQuantityOfReservation(reservation.externalComments, reservation.internalComments);
//
//     expect(remarksQuantity).toEqual(0);
//   });
//
//   it('should return one when reservation has only internal remarks in getRemarksQuantityOfReservation', () => {
//     const reservation = new Reservation();
//     reservation.internalComments = 'A';
//
//     const remarksQuantity = service.getRemarksQuantityOfReservation(reservation.externalComments, reservation.internalComments);
//
//     expect(remarksQuantity).toEqual(1);
//   });
//
//   it('should return one when reserve has only external remarks in getRemarksQuantityOfReservation', () => {
//     const reservation = new Reservation();
//     reservation.externalComments = 'A';
//
//     const remarksQuantity = service.getRemarksQuantityOfReservation(reservation.externalComments, reservation.internalComments);
//
//     expect(remarksQuantity).toEqual(1);
//   });
//
//   it('should return two when reserve has internal and external remarks in getRemarksQuantityOfReservation', () => {
//     const reservation = new Reservation();
//     reservation.internalComments = 'A';
//     reservation.externalComments = 'B';
//
//     const remarksQuantity = service.getRemarksQuantityOfReservation(reservation.externalComments, reservation.internalComments);
//
//     expect(remarksQuantity).toEqual(2);
//   });
//
//   it('should getReservationItemViewListWithFormCard', () => {
//     const reservationItemViewList = new Array<ReservationItemView>();
//     const room = new Room();
//     room.roomNumber = '001';
//     const roomType = new RoomType();
//     roomType.id = 1;
//
//     const estimatedArrivalDate = new Date(2017, 10, 20);
//     const estimatedDepartureDate = new Date(2017, 10, 25);
//
//     const reservationItemView = new ReservationItemView();
//     reservationItemView.checkinHour = '15:00';
//     reservationItemView.checkoutHour = '13:00';
//     reservationItemView.period = {
//       beginJsDate: estimatedArrivalDate,
//       endJsDate: estimatedDepartureDate,
//       beginDate: {
//         year: estimatedArrivalDate.getFullYear(),
//         month: estimatedArrivalDate.getMonth(),
//         day: estimatedArrivalDate.getDate(),
//       },
//       endDate: {
//         year: estimatedDepartureDate.getFullYear(),
//         month: estimatedDepartureDate.getMonth(),
//         day: estimatedDepartureDate.getDate(),
//       },
//     };
//     reservationItemView.room = room;
//     reservationItemView.receivedRoomType = RoomTypeLuxoData;
//     reservationItemView.requestedRoomType = RoomTypeStandardData;
//     reservationItemView.adultQuantity = 1;
//     reservationItemView.childrenQuantity = 1;
//     reservationItemViewList.push(reservationItemView);
//     const reservationBudget = new ReservationBudget();
//     reservationBudget.budgetDay = new Date();
//     reservationBudget.manualRate = 100;
//     reservationItemView.reservationBudgetList.push(reservationBudget);
//     reservationItemView.reservationBudgetList.push(reservationBudget);
//     reservationItemView.reservationBudgetList.push(reservationBudget);
//
//     const accommodationsReserveWithFormCard = service.getReservationItemViewListWithFormCardAndPricesCalculated(
//       reservationItemViewList,
//       roomTypes,
//     );
//
//     expect(accommodationsReserveWithFormCard.length).toEqual(1);
//     expect(accommodationsReserveWithFormCard[0].formCard.get('periodCard').value).toEqual(reservationItemView.period);
//     expect(accommodationsReserveWithFormCard[0].formCard.get('checkinHourCard').value).toEqual(reservationItemView.checkinHour);
//     expect(accommodationsReserveWithFormCard[0].formCard.get('checkoutHourCard').value).toEqual(reservationItemView.checkoutHour);
//     expect(accommodationsReserveWithFormCard[0].formCard.get('roomNumberCard').value).toEqual(reservationItemView.room);
//     expect(accommodationsReserveWithFormCard[0].formCard.get('adultQuantityCard').value).toEqual(reservationItemView.adultQuantity);
//     expect(accommodationsReserveWithFormCard[0].formCard.get('childrenQuantityCard').value)
// .toEqual(reservationItemView.childrenQuantity);
//     expect(accommodationsReserveWithFormCard[0].formCard.get('roomTypeCard').value).toEqual(reservationItemView.receivedRoomType);
//     expect(accommodationsReserveWithFormCard[0].formCard.get('roomTypeRequestedCard').value)
// .toEqual(reservationItemView.requestedRoomType);
//     expect(accommodationsReserveWithFormCard[0].dailyNumber).toEqual(5);
//     expect(accommodationsReserveWithFormCard[0].dailyPrice).toEqual(100);
//     expect(accommodationsReserveWithFormCard[0].totalDaily).toEqual(300);
//     expect(accommodationsReserveWithFormCard[0].focus).toBeTruthy();
//   });
//
//   it('should checkStatusReservationItemCanceledToReorderReservationItemList', () => {
//     const reservationItemViewList = new Array<ReservationItemView>();
//     reservationItemViewList.push(ReservationItemViewData);
//     reservationItemViewList.push(ReservationItemViewCancelData);
//     reservationItemViewList.push(ReservationItemViewData2);
//
//     const reservationItemViewListReOrder = new Array<ReservationItemView>();
//     reservationItemViewListReOrder.push(ReservationItemViewData);
//     reservationItemViewListReOrder.push(ReservationItemViewData2);
//     reservationItemViewListReOrder.push(ReservationItemViewCancelData);
//
//     const serviceResponse = service.checkStatusReservationItemCanceledToReorderReservationItemList(reservationItemViewList);
//
//     expect(serviceResponse).toEqual(reservationItemViewListReOrder);
//   });
//
//   it('should getDailyPrice', () => {
//     const reservationItemView = new ReservationItemView();
//
//     reservationItemView.reservationBudgetList.push(ReservationBudgetData4);
//     reservationItemView.reservationBudgetList.push(ReservationBudgetData4);
//
//     const dailyPrice = service.getDailyPrice(reservationItemView);
//
//     expect(dailyPrice).toEqual(250);
//   });
//
//   it('should getTotalDaily', () => {
//     const reservationItemView = new ReservationItemView();
//
//     reservationItemView.reservationBudgetList.push(ReservationBudgetData4);
//     reservationItemView.reservationBudgetList.push(ReservationBudgetData4);
//
//     const totalDaily = service.getTotalDaily(reservationItemView);
//
//     expect(totalDaily).toEqual(500);
//   });
//
//   it('should getReservationBudgetList', () => {
//     const dailyPrices = new Map<Date, number>();
//     const date1 = new Date();
//     const date2 = new Date();
//     const value1 = 100;
//     const value2 = 100;
//     dailyPrices.set(date1, value1);
//     dailyPrices.set(date2, value2);
//
//     const reservationBudgetList = service.getReservationBudgetList(dailyPrices);
//
//     expect(reservationBudgetList.length).toEqual(2);
//     expect(reservationBudgetList[0].budgetDay).toEqual(date1);
//     expect(reservationBudgetList[0].manualRate).toEqual(value1);
//     expect(reservationBudgetList[1].budgetDay).toEqual(date2);
//     expect(reservationBudgetList[1].manualRate).toEqual(value2);
//   });
//
//   xit('should getDateListFromPeriod', () => {
//     const beginDate = new Date();
//     const endDate = new Date();
//     endDate.setDate(beginDate.getDate() + 2);
//
//     const period: Object = {
//       beginJsDate: beginDate,
//       endJsDate: endDate,
//     };
//
//     const dateListFromPeriod = service.getDateListFromPeriod(period);
//
//     expect(dateListFromPeriod.length).toEqual(2);
//   });
//
//   it(
//     'should getRoomLayoutList',
//     fakeAsync(() => {
//       const reservationItemView = new ReservationItemView();
//       reservationItemView.formCard = reservationItemCardForm;
//
//       const roomType = new RoomType();
//       roomType.id = 1;
//       let roomLayoutListInThen = new Array<RoomLayout>();
//
//       reservationItemView.formCard.get('roomTypeCard').setValue(roomType);
//
//       promiseGetRoomLayoutList().then(roomLayoutList => {
//         roomLayoutListInThen = roomLayoutList;
//       });
//       service.getRoomLayoutList(reservationItemView);
//       tick();
//
//       expect(roomLayoutListInThen.length).toEqual(2);
//     }),
//   );
//
//   it('should keep roomLayoutCard in getRoomLayoutCardOfTheReservationItemView', () => {
//     const reservationItemView = new ReservationItemView();
//     reservationItemView.roomLayoutList = roomLayoutList;
//     reservationItemView.formCard = reservationItemCardForm;
//
//     const roomType = new RoomType();
//     roomType.id = 1;
//
//     reservationItemView.formCard.get('roomTypeCard').setValue(roomType);
//     reservationItemView.formCard.get('roomLayoutCard').setValue(roomLayout2);
//
//     const roomLayout = service.getRoomLayoutCardOfTheReservationItemView(reservationItemView);
//
//     expect(roomLayout.id).toEqual(roomLayout2.id);
//   });
//
//   it('should get first roomLayoutCard  in getRoomLayoutCardOfTheReservationItemView', () => {
//     const reservationItemView = new ReservationItemView();
//     reservationItemView.roomLayoutList = roomLayoutList;
//     reservationItemView.formCard = reservationItemCardForm;
//
//     const roomType = new RoomType();
//     roomType.id = 1;
//
//     reservationItemView.formCard.get('roomTypeCard').setValue(roomType);
//
//     const roomLayoutIntern = service.getRoomLayoutCardOfTheReservationItemView(reservationItemView);
//
//     expect(roomLayoutIntern.id).toEqual(roomLayout.id);
//   });
//
//   it('should get first roomLayoutCard when change roomLayoutList in getRoomLayoutCardOfTheReservationItemView', () => {
//     const reservationItemView = new ReservationItemView();
//     reservationItemView.roomLayoutList = roomLayoutList;
//     const oldRoomLayout = new RoomLayout();
//     oldRoomLayout.id = 'aacc445';
//     reservationItemView.formCard = reservationItemCardForm;
//
//     const roomType = new RoomType();
//     roomType.id = 1;
//
//     reservationItemView.formCard.get('roomTypeCard').setValue(roomType);
//     reservationItemView.formCard.get('roomLayoutCard').setValue(oldRoomLayout);
//
//     const roomLayoutIntern = service.getRoomLayoutCardOfTheReservationItemView(reservationItemView);
//
//     expect(roomLayoutIntern.id).toEqual(roomLayout.id);
//   });
//
//   it('should generateCodesForReservationItemViewList', () => {
//     const reservationItemViewList = new Array<ReservationItemView>();
//     const code = 'aaabbb-0011';
//
//     const reservationItemViewWithId = new ReservationItemView();
//     reservationItemViewWithId.id = 1;
//     reservationItemViewWithId.code = code;
//
//     reservationItemViewList.push(reservationItemViewWithId);
//     for (let i = 1; i <= 1000; i++) {
//       reservationItemViewList.push(new ReservationItemView());
//     }
//
//     service.generateCodesForReservationItemViewList(reservationItemViewList);
//     reservationItemViewList.push(new ReservationItemView());
//     reservationItemViewList.push(new ReservationItemView());
//     service.generateCodesForReservationItemViewList(reservationItemViewList);
//
//     expect(code).toEqual(reservationItemViewList[0].code);
//     expect('0002').toEqual(reservationItemViewList[1].code);
//     expect('0003').toEqual(reservationItemViewList[2].code);
//     expect('0011').toEqual(reservationItemViewList[10].code);
//     expect('0012').toEqual(reservationItemViewList[11].code);
//     expect('0101').toEqual(reservationItemViewList[100].code);
//     expect('0102').toEqual(reservationItemViewList[101].code);
//     expect('1001').toEqual(reservationItemViewList[1000].code);
//     expect('1002').toEqual(reservationItemViewList[1001].code);
//     expect('1003').toEqual(reservationItemViewList[1002].code);
//   });
//
//   describe('on getContact ', () => {
//     it('should getContactName', () => {
//       const name = 'Abc';
//       newReservationForm.get('contactGroup.name').setValue(name);
//       const nameRetorned = service.getContactName(newReservationForm.get('contactGroup'));
//
//       expect(name).toEqual(nameRetorned);
//     });
//
//     it('should getContactEmail', () => {
//       const email = 'abc@email.com';
//       newReservationForm.get('contactGroup.email').setValue(email);
//
//       const emailRetorned = service.getContactEmail(newReservationForm.get('contactGroup'));
//
//       expect(email).toEqual(emailRetorned);
//     });
//
//     it('should getContactPhone', () => {
//       const phone = '021911222';
//       newReservationForm.get('contactGroup.phone').setValue(phone);
//
//       const phoneRetorned = service.getContactPhone(newReservationForm.get('contactGroup'));
//
//       expect(phone).toEqual(phoneRetorned);
//     });
//   });
//
//   it('should getStatusToConfirm', () => {
//     const status = service.getReservationStatusToConfirm();
//
//     expect(ReservationStatusEnum.ToConfirm).toEqual(status);
//   });
//
//   describe('on getMessageError ', () => {
//     it('should required error message for checkinHour', () => {
//       newReservationForm.get('reservationGroup.checkinHour').markAsTouched();
//       newReservationForm.get('reservationGroup.checkinHour').markAsDirty();
//
//       const errorMessage = service.getMessageError(newReservationForm.get('reservationGroup.checkinHour'));
//
//       expect(errorMessage).toEqual(translateService.instant('commomData.requiredFields'));
//     });
//
//     it('should required error message for checkoutHour', () => {
//       newReservationForm.get('reservationGroup.checkoutHour').markAsTouched();
//       newReservationForm.get('reservationGroup.checkoutHour').markAsDirty();
//
//       const errorMessage = service.getMessageError(newReservationForm.get('reservationGroup.checkoutHour'));
//
//       expect(errorMessage).toEqual(translateService.instant('commomData.requiredFields'));
//     });
//
//     it('should hourAndMinutesAreInValid error message for checkinHour', () => {
//       newReservationForm.get('reservationGroup.checkinHour').markAsTouched();
//       newReservationForm.get('reservationGroup.checkinHour').markAsDirty();
//       newReservationForm.get('reservationGroup.checkinHour').setValidators(service.checkinHourAndMinutesValid());
//       newReservationForm.get('reservationGroup.checkinHour').setValue('30:00');
//
//       const errorMessage = service.getMessageError(newReservationForm.get('reservationGroup.checkinHour'));
//
//       expect(errorMessage).toEqual(translateService.instant('reservationModule.commomData.hourAndMinutesAreInValid'));
//     });
//
//     it('should hourAndMinutesAreInValid error message for checkoutHour', () => {
//       newReservationForm.get('reservationGroup.checkoutHour').markAsTouched();
//       newReservationForm.get('reservationGroup.checkoutHour').markAsDirty();
//       newReservationForm.get('reservationGroup.checkoutHour').setValidators(service.checkinHourAndMinutesValid());
//       newReservationForm.get('reservationGroup.checkoutHour').setValue('10:70');
//
//       const errorMessage = service.getMessageError(newReservationForm.get('reservationGroup.checkoutHour'));
//
//       expect(errorMessage).toEqual(translateService.instant('reservationModule.commomData.hourAndMinutesAreInValid'));
//     });
//
//     it('should error message empty for checkinHour', () => {
//       newReservationForm.get('reservationGroup.checkinHour').markAsTouched();
//       newReservationForm.get('reservationGroup.checkinHour').markAsDirty();
//       newReservationForm.get('reservationGroup.checkinHour').setValue('12:00');
//
//       const errorMessage = service.getMessageError(newReservationForm.get('reservationGroup.checkinHour'));
//
//       expect(errorMessage).toEqual('');
//     });
//
//     it('should error message empty for checkoutHour', () => {
//       newReservationForm.get('reservationGroup.checkoutHour').markAsTouched();
//       newReservationForm.get('reservationGroup.checkoutHour').markAsDirty();
//       newReservationForm.get('reservationGroup.checkoutHour').setValue('12:00');
//
//       const errorMessage = service.getMessageError(newReservationForm.get('reservationGroup.checkoutHour'));
//
//       expect(errorMessage).toEqual('');
//     });
//   });
//
//   describe('on getReservationItemViewList', () => {
//     beforeEach(() => {
//       const guest = new Guest();
//       guest.status = GuestStatus.NoGuest;
//       guest.formGuest = formBuilder.group({
//         guestName: [''],
//       });
//
//       guestArray.push(guest);
//       roomQuantity = 2;
//       period = '17/07/17 - 21/07/17';
//       adultQuantity = 3;
//       childrenQuantity = 1;
//
//       const manualRate = 130;
//       const date = new Date(2017, 7, 17);
//
//       const reservationBudget = new ReservationBudget();
//       reservationBudget.budgetDay = date;
//       reservationBudget.manualRate = manualRate;
//
//       reservationItemView.code = '';
//       reservationItemView.period = '';
//       reservationItemView.roomQuantity = 2;
//       reservationItemView.adultQuantity = 3;
//       reservationItemView.childrenQuantity = 1;
//       reservationItemView.room = new Room();
//       reservationItemView.room.roomNumber = 'ABCDEFG';
//       reservationItemView.receivedRoomType = new RoomType();
//       reservationItemView.receivedRoomType.id = 1;
//       reservationItemView.receivedRoomType.name = 'Quarto Standard';
//       reservationItemView.receivedRoomType.abbreviation = 'SPDC';
//       reservationItemView.receivedRoomType.order = 1;
//       reservationItemView.receivedRoomType.adultCapacity = 2;
//       reservationItemView.receivedRoomType.childCapacity = 1;
//       reservationItemView.receivedRoomType.roomTypeBedTypeList = [];
//       reservationItemView.receivedRoomType.isActive = true;
//       reservationItemView.receivedRoomType.isInUse = false;
//       reservationItemView.dailyNumber = 3;
//       reservationItemView.dailyPrice = 1200;
//       reservationItemView.totalDaily = reservationItemView.dailyPrice * reservationItemView.dailyNumber;
//       reservationItemView.status = ReservationStatusEnum.ToConfirm;
//       reservationItemView.guestQuantity = guestArray;
//       reservationItemView.childrenPopoverIsVisible = false;
//       reservationItemView.formCard = reservationItemCardForm;
//       reservationItemView.reservationBudgetList.push(reservationBudget);
//
//       reservationItemViewList.push(reservationItemView);
//       reservationItemViewList.push(reservationItemView);
//
//       newReservationForm.get('reservationGroup.roomQuantity').setValue(roomQuantity);
//       newReservationForm.get('reservationGroup.period').setValue(period);
//       newReservationForm.get('reservationGroup.adultQuantity').setValue(adultQuantity);
//       newReservationForm.get('reservationGroup.childrenQuantity').setValue(childrenQuantity);
//       newReservationForm.get('reservationGroup.roomType').setValue(roomTypes[0]);
//     });
//
//     it('should getReservationItemViewList with other array when room quantity is greater than one', () => {
//       const averageDailyPrice = 100;
//       const dailyPrices = new Map<Date, number>();
//       spyOn(service, 'setGuestQuantity').and.returnValue(guestArray);
//
//       spyOn(service, 'getDailyFromPeriod').and.returnValue(reservationItemView.dailyNumber);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices).length,
//       ).toEqual(reservationItemViewList.length);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .adultQuantity,
//       ).toEqual(adultQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .childrenQuantity,
//       ).toEqual(childrenQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .receivedRoomType,
//       ).toEqual(roomTypes[0]);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .guestQuantity,
//       ).toEqual(reservationItemView.guestQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0].room
//           .roomNumber,
//       ).toEqual(reservationItemView.room.roomNumber);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .totalDaily,
//       ).toEqual(reservationItemView.dailyNumber * averageDailyPrice);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[1]
//           .adultQuantity,
//       ).toEqual(adultQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[1]
//           .childrenQuantity,
//       ).toEqual(childrenQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[1]
//           .receivedRoomType,
//       ).toEqual(roomTypes[0]);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[1]
//           .guestQuantity,
//       ).toEqual(reservationItemView.guestQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[1].room
//           .roomNumber,
//       ).toEqual(reservationItemView.room.roomNumber);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[1]
//           .totalDaily,
//       ).toEqual(reservationItemView.dailyNumber * averageDailyPrice);
//     });
//
//     it('should setControl childrenAgeListCard in initFormGroupReservationItemView when isCreatingNewReservationItemFormGroup is true',
// () => {
//       const averageDailyPrice = 100;
//       const dailyPrices = new Map<Date, number>();
//       spyOn(service, 'setGuestQuantity').and.returnValue(guestArray);
//       spyOn(service, 'getDailyFromPeriod').and.returnValue(reservationItemView.dailyNumber);
//
//       const reservationItemViewList = service.getReservationItemViewList(
//         newReservationForm.get('reservationGroup'),
//         roomTypes,
//         averageDailyPrice,
//         dailyPrices,
//       );
//       const childrenAgeListCard = <FormArray>reservationItemViewList[0].formCard.get('childrenAgeListCard');
//
//       expect(childrenAgeListCard.length).toEqual(1);
//       expect(childrenAgeListCard.controls[0].get('childAgeCard').value).toEqual(0);
//     });
//
//     it('should setControl childrenAgeListCard in initFormGroupReservationItemView when isCreatingNewReservationItemFormGroup is false',
// () => {
//       const reservationItemViewList = new Array<ReservationItemView>();
//       reservationItemViewList.push(ReservationItemViewData);
//
//       const reservationItemViewListWithFormCard = service.getReservationItemViewListWithFormCardAndPricesCalculated(
//         reservationItemViewList,
//         roomTypes,
//       );
//
//       const childrenAgeListCard = <FormArray>reservationItemViewList[0].formCard.get('childrenAgeListCard');
//
//       expect(childrenAgeListCard.length).toEqual(1);
//       expect(childrenAgeListCard.controls[0].get('childAgeCard').value).toEqual(5);
//     });
//
//     it('should set reservationBudgetList in reservationItemView', () => {
//       const averageDailyPrice = 130;
//       const manualRate = 130;
//       const date = new Date(2017, 7, 17);
//       const dailyPrices = new Map<Date, number>();
//       dailyPrices.set(date, manualRate);
//       spyOn(service, 'setGuestQuantity').and.returnValue(guestArray);
//       spyOn(service, 'getDailyFromPeriod').and.returnValue(reservationItemView.dailyNumber);
//
//       const reservationItemViewList = new Array<ReservationItemView>();
//       const reservationItem = new ReservationItemView();
//       const reservationBudget = new ReservationBudget();
//       reservationBudget.budgetDay = date;
//       reservationBudget.manualRate = manualRate;
//       reservationItem.reservationBudgetList.push(reservationBudget);
//       reservationItemViewList.push(reservationItem);
//
//       const accomodations = service.getReservationItemViewList(
//         newReservationForm.get('reservationGroup'),
//         roomTypes,
//         averageDailyPrice,
//         dailyPrices,
//       );
//
//       expect(accomodations[0].reservationBudgetList.length).toEqual(reservationItemViewList[0].reservationBudgetList.length);
//       expect(accomodations[0].reservationBudgetList[0].budgetDay).toEqual(reservationItemViewList[0].reservationBudgetList[0].budgetDay);
//     expect(accomodations[0].reservationBudgetList[0].manualRate).toEqual(reservationItemViewList[0].reservationBudgetList[0].manualRate);
//       expect(accomodations[0].reservationBudgetList[0].id).toEqual(reservationItemViewList[0].reservationBudgetList[0].id);
//       expect(accomodations[0].reservationBudgetList[0].reservationBudgetUid).toEqual(
//         reservationItemViewList[0].reservationBudgetList[0].reservationBudgetUid,
//       );
//       expect(accomodations[0].reservationBudgetList[0].reservationItemId).toEqual(
//         reservationItemViewList[0].reservationBudgetList[0].reservationItemId,
//       );
//     });
//
//     it('should getReservationItemViewList to set property FormCard when room quantity is greater than one', () => {
//       const averageDailyPrice = 100;
//       const dailyPrices = new Map<Date, number>();
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .formCard.controls.adultQuantityCard.value,
//       ).toEqual(adultQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .formCard.controls.childrenQuantityCard.value,
//       ).toEqual(childrenQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .formCard.controls.roomTypeCard.value,
//       ).toEqual(reservationItemView.receivedRoomType);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[1]
//           .formCard.controls.adultQuantityCard.value,
//       ).toEqual(adultQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[1]
//           .formCard.controls.childrenQuantityCard.value,
//       ).toEqual(childrenQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[1]
//           .formCard.controls.roomTypeCard.value,
//       ).toEqual(reservationItemView.receivedRoomType);
//     });
//
//     it('should getReservationItemViewList with one AccommodationReserve when room quantity is equals one', () => {
//       const averageDailyPrice = 100;
//       const dailyPrices = new Map<Date, number>();
//
//       roomQuantity = 1;
//       period = '17/07/17 - 21/07/17';
//       adultQuantity = 1;
//       childrenQuantity = 0;
//       reservationItemViewList = new Array<ReservationItemView>();
//       reservationItemViewList.push(reservationItemView);
//
//       spyOn(service, 'setGuestQuantity').and.returnValue(guestArray);
//
//       newReservationForm.get('reservationGroup.roomQuantity').setValue(roomQuantity);
//       newReservationForm.get('reservationGroup.period').setValue(period);
//       newReservationForm.get('reservationGroup.adultQuantity').setValue(adultQuantity);
//       newReservationForm.get('reservationGroup.childrenQuantity').setValue(childrenQuantity);
//
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices).length,
//       ).toEqual(reservationItemViewList.length);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .adultQuantity,
//       ).toEqual(adultQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .childrenQuantity,
//       ).toEqual(childrenQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .receivedRoomType,
//       ).toEqual(roomTypes[0]);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0]
//           .guestQuantity,
//       ).toEqual(reservationItemView.guestQuantity);
//       expect(
//         service.getReservationItemViewList(newReservationForm.get('reservationGroup'), roomTypes, averageDailyPrice, dailyPrices)[0].room
//           .roomNumber,
//       ).toEqual(reservationItemView.room.roomNumber);
//     });
//   });
//
//   describe('on ReservationItemView', () => {
//     it('should setBedQuantity only bedType Crib', () => {
//       const bedType = new BedType();
//       bedType.bedType = BedTypeEnum.Crib;
//       bedType.count = 0;
//       bedType.optionalLimit = 2;
//
//       const roomTypeMock = new RoomType();
//       roomTypeMock.id = 1;
//       roomTypeMock.name = 'Quarto Standard';
//       roomTypeMock.abbreviation = 'SPDC';
//       roomTypeMock.order = 1;
//       roomTypeMock.adultCapacity = 2;
//       roomTypeMock.childCapacity = 1;
//       roomTypeMock.roomTypeBedTypeList = new Array<BedType>();
//       roomTypeMock.roomTypeBedTypeList.push(bedType);
//       roomTypeMock.isActive = true;
//       roomTypeMock.isInUse = false;
//
//       const bedQuantity = [1, 2];
//
//       expect(service.setBedOptionalQuantity(roomTypeMock, BedTypeEnum.Crib)).toEqual(bedQuantity);
//     });
//
//     it('should setBedQuantity only bedType Extra', () => {
//       const bedType = new BedType();
//       bedType.bedType = BedTypeEnum.Single;
//       bedType.count = 0;
//       bedType.optionalLimit = 3;
//
//       const roomTypeMock = new RoomType();
//       roomTypeMock.id = 1;
//       roomTypeMock.name = 'Quarto Standard';
//       roomTypeMock.abbreviation = 'SPDC';
//       roomTypeMock.order = 1;
//       roomTypeMock.adultCapacity = 2;
//       roomTypeMock.childCapacity = 1;
//       roomTypeMock.roomTypeBedTypeList = new Array<BedType>();
//       roomTypeMock.roomTypeBedTypeList.push(bedType);
//       roomTypeMock.isActive = true;
//       roomTypeMock.isInUse = false;
//
//       const bedQuantity = [1, 2, 3];
//
//       expect(service.setBedOptionalQuantity(roomTypeMock, BedTypeEnum.Single)).toEqual(bedQuantity);
//     });
//
//     it('should setBedQuantity with no bedTypes', () => {
//       const bedType = new BedType();
//       bedType.bedType = BedTypeEnum.Single;
//       bedType.count = 4;
//       bedType.optionalLimit = 0;
//
//       const roomTypeMock = new RoomType();
//       roomTypeMock.id = 1;
//       roomTypeMock.name = 'Quarto Standard';
//       roomTypeMock.abbreviation = 'SPDC';
//       roomTypeMock.order = 1;
//       roomTypeMock.adultCapacity = 2;
//       roomTypeMock.childCapacity = 1;
//       roomTypeMock.roomTypeBedTypeList = new Array<BedType>();
//       roomTypeMock.roomTypeBedTypeList.push(bedType);
//       roomTypeMock.isActive = true;
//       roomTypeMock.isInUse = false;
//
//       const bedQuantity = [];
//
//       expect(service.setBedOptionalQuantity(roomTypeMock, BedTypeEnum.Single)).toEqual(bedQuantity);
//     });
//
//     it('should calculate Daily Number returning 10 from Period', () => {
//       let dailyCount;
//
//       const dateBegin = new Date('07/10/2017');
//       const dateEnd = new Date('07/20/2017');
//
//       dailyCount = service['getDailyFromPeriod'](dateBegin, dateEnd);
//
//       expect(dailyCount).toEqual(10);
//     });
//
//     it('should calculate Daily Number returning 5 from Period', () => {
//       let dailyCount;
//
//       const dateBegin = new Date('2017/07/10');
//       const dateEnd = new Date('2017/07/15');
//
//       dailyCount = service['getDailyFromPeriod'](dateBegin, dateEnd);
//       expect(dailyCount).toEqual(5);
//     });
//
//     it('should calculate Daily Number returning 1 from Period', () => {
//       let dailyCount;
//
//       const dateBegin = new Date('2017/07/15');
//       const dateEnd = new Date('2017/07/15');
//
//       dailyCount = service['getDailyFromPeriod'](dateBegin, dateEnd);
//       expect(dailyCount).toEqual(1);
//     });
//
//     it('should calculate Daily Number returning 0 from Period', () => {
//       let dailyCount;
//
//       const dateBegin = new Date('2017/07/15');
//       const dateEnd = undefined;
//
//       dailyCount = service['getDailyFromPeriod'](dateBegin, dateEnd);
//       expect(dailyCount).toEqual(0);
//     });
//
//     it('should calculate average daily price returning 100', () => {
//       const dailyPriceData = new Map<any, any>();
//       dailyPriceData.set(new Date('2017/07/10'), 150);
//       dailyPriceData.set(new Date('2017/07/11'), 100);
//       dailyPriceData.set(new Date('2017/07/12'), 50);
//       let averageDailyPrice = 0;
//
//       averageDailyPrice = service.calculateAverageDailyValue(dailyPriceData);
//
//       expect(averageDailyPrice).toEqual(100);
//     });
//
//     it('should calculate average daily price returning 0', () => {
//       const dailyPriceData = new Map<any, any>();
//       let averageDailyPrice = 0;
//
//       averageDailyPrice = service.calculateAverageDailyValue(dailyPriceData);
//
//       expect(averageDailyPrice).toEqual(0);
//     });
//
//     it('should show all roomtypes when guestCapacity is minor than roomTypesCapacity', () => {
//       const childrenQuantity = 0;
//       const adultQuantity = 2;
//       const roomTypes = new Array<RoomType>();
//
//       const roomType = new RoomType();
//       roomType.adultCapacity = 3;
//       roomType.childCapacity = 0;
//       const roomType2 = new RoomType();
//       roomType2.adultCapacity = 2;
//       roomType2.childCapacity = 1;
//
//       roomTypes.push(roomType);
//       roomTypes.push(roomType2);
//
//       expect(service.getNewRoomTypesByGuestCapacity(roomTypes, adultQuantity, childrenQuantity)).toEqual(roomTypes);
//     });
//
//     it('should show only roomtypes when guestCapacity is major some than roomTypesCapacity', () => {
//       const childrenQuantity = 0;
//       const adultQuantity = 3;
//       const roomTypes = new Array<RoomType>();
//
//       const roomType = new RoomType();
//       roomType.adultCapacity = 3;
//       roomType.childCapacity = 0;
//       const roomType2 = new RoomType();
//       roomType2.adultCapacity = 2;
//       roomType2.childCapacity = 1;
//
//       roomTypes.push(roomType);
//       roomTypes.push(roomType2);
//
//       expect(service.getNewRoomTypesByGuestCapacity(roomTypes, adultQuantity, childrenQuantity)).toEqual([roomTypes[0]]);
//     });
//
//     it('should show only roomtypes when guestCapacity is major some than roomTypesCapacity', () => {
//       const childrenQuantity = 2;
//       const adultQuantity = 4;
//       const roomTypes = new Array<RoomType>();
//
//       const roomType = new RoomType();
//       roomType.adultCapacity = 3;
//       roomType.childCapacity = 0;
//       const roomType2 = new RoomType();
//       roomType2.adultCapacity = 2;
//       roomType2.childCapacity = 1;
//
//       roomTypes.push(roomType);
//       roomTypes.push(roomType2);
//
//       expect(service.getNewRoomTypesByGuestCapacity(roomTypes, adultQuantity, childrenQuantity)).toEqual([]);
//     });
//   });
//
//   describe('on getTotalsValuesReservationItemView', () => {
//     it('should getTotalsValuesReservationItemView when more than one reserve', () => {
//       const reservationItemViewList = new Array<ReservationItemView>();
//
//       const reservationItemView = new ReservationItemView();
//       reservationItemView.adultQuantity = 4;
//       reservationItemView.childrenQuantity = 2;
//       reservationItemView.totalDaily = 1200;
//
//       const reservationItemView2 = new ReservationItemView();
//       reservationItemView2.adultQuantity = 1;
//       reservationItemView2.childrenQuantity = 2;
//       reservationItemView2.totalDaily = 800;
//
//       reservationItemViewList.push(reservationItemView);
//       reservationItemViewList.push(reservationItemView2);
//
//       const totals = {
//         adultQuantity: reservationItemViewList[0].adultQuantity + reservationItemViewList[1].adultQuantity,
//         childrenQuantity: reservationItemViewList[0].childrenQuantity + reservationItemViewList[1].childrenQuantity,
//         rooms: reservationItemViewList.length,
//         guests:
//           reservationItemViewList[0].adultQuantity +
//           reservationItemViewList[0].childrenQuantity +
//           reservationItemViewList[1].adultQuantity +
//           reservationItemViewList[1].childrenQuantity,
//         valuesDailies: reservationItemViewList[0].totalDaily + reservationItemViewList[1].totalDaily,
//       };
//
//       expect(service.getTotalsValuesReservationItemView(reservationItemViewList)).toEqual(totals);
//     });
//   });
//
//   describe('on getStatusIsPossibleToSaveReserve', () => {
//     it('should return true when form is invalid', () => {
//       const reservationItemViewList = new Array<ReservationItemView>();
//       reservationItemViewList.push(ReservationItemViewData2);
//       const formIsValid = false;
//       const selectOrigin = false;
//       const selectMarketSegment = false;
//       expect(service.getStatusIsPossibleToSaveReserve(formIsValid, selectOrigin, selectMarketSegment, reservationItemViewList)).toEqual(
//         true,
//       );
//     });
//
//     it('should return true when form is valid and arrayReservationItemView is empty', () => {
//       const formIsValid = true;
//       const selectOrigin = true;
//       const selectMarketSegment = true;
//       const reservationItemViewListEmpty = new Array<ReservationItemView>();
//       expect(
//         service.getStatusIsPossibleToSaveReserve(formIsValid, selectOrigin, selectMarketSegment, reservationItemViewListEmpty),
//       ).toEqual(true);
//     });
//
//     it('should return false when form is valid and arrayAccommodation has all forms at cards valid', () => {
//       const formIsValid = true;
//       const selectOrigin = true;
//       const selectMarketSegment = true;
//       const reservationItemViewList = new Array<ReservationItemView>();
//       reservationItemViewList.push(ReservationItemViewData2);
//       expect(service.getStatusIsPossibleToSaveReserve(formIsValid, selectOrigin, selectMarketSegment, reservationItemViewList)).toEqual(
//         false,
//       );
//     });
//
//     it('should return false when form is valid and reservationItemViewCardFormInvalid has forms at card valid', () => {
//       const formIsValid = true;
//       const selectOrigin = true;
//       const selectMarketSegment = true;
//       const reservationItemViewFormInvalid = new ReservationItemView();
//       const reservationItemViewListWithOneFormCardInvalid = new Array<ReservationItemView>();
//
//       const reservationItemViewCardFormInvalid = formBuilder.group({
//         periodCard: [''],
//         adultQuantityCard: [1, [validatorFormService.capacityNumberIsMajorThanOne]],
//         childrenQuantityCard: [0],
//         roomTypeCard: [''],
//         checkinHourCard: [''],
//         checkoutHourCard: [''],
//         roomNumberCard: [''],
//       });
//
//       reservationItemViewFormInvalid.formCard = reservationItemViewCardFormInvalid;
//       reservationItemViewListWithOneFormCardInvalid.push(reservationItemViewFormInvalid);
//
//       expect(
//         service.getStatusIsPossibleToSaveReserve(
//           formIsValid,
//           selectOrigin,
//           selectMarketSegment,
//           reservationItemViewListWithOneFormCardInvalid,
//         ),
//       ).toEqual(false);
//     });
//
//     it('should return true when form is valid and reservationItemViewCardFormInvalid has one form at card invalid', () => {
//       const formIsValid = true;
//       const selectOrigin = true;
//       const selectMarketSegment = true;
//       const reservationItemViewFormInvalid = new ReservationItemView();
//       const reservationItemViewListReserveWithOneFormCardInvalid = new Array<ReservationItemView>();
//
//       const accomodationCardFormInvalid = formBuilder.group({
//         periodCard: [''],
//         adultQuantityCard: [0, [validatorFormService.capacityNumberIsMajorThanOne]],
//         childrenQuantityCard: [0],
//         roomTypeCard: [''],
//         checkinHourCard: [''],
//         checkoutHourCard: [''],
//         roomNumberCard: [''],
//       });
//
//       reservationItemViewFormInvalid.formCard = accomodationCardFormInvalid;
//       reservationItemViewListReserveWithOneFormCardInvalid.push(reservationItemViewFormInvalid);
//
//       expect(
//         service.getStatusIsPossibleToSaveReserve(
//           formIsValid,
//           selectOrigin,
//           selectMarketSegment,
//           reservationItemViewListReserveWithOneFormCardInvalid,
//         ),
//       ).toEqual(true);
//     });
//   });
//
//   describe('on getModalContent', () => {
//     it('should set modal content to GuestDetailsPage', () => {
//       reservationItemViewList = new Array<ReservationItemView>();
//       reservationItemViewList.push(reservationItemView);
//
//       expect(service.getModalContent(reservationItemViewList)).toEqual(
//         service.translateService.instant('reservationModule.reserveCreate.confirmReserveSaveAndGoToGuestDetails'),
//       );
//     });
//
//     it('should set modal content to RoomListPage', () => {
//       reservationItemViewList = new Array<ReservationItemView>();
//       reservationItemViewList.push(reservationItemView);
//       reservationItemViewList.push(reservationItemView);
//
//       expect(service.getModalContent(reservationItemViewList)).toEqual(
//         service.translateService.instant('reservationModule.reserveCreate.confirmReserveSaveAndGoToRoomList'),
//       );
//     });
//   });
//
//   describe('on roomTypes', () => {
//     it('should set new array of roomTypes', () => {
//       expect(service.getRoomTypeSelected(roomTypes, roomTypeMock2)).toEqual(roomTypeMock2);
//     });
//   });
//
//   it('should getDailyFromPeriod', () => {
//     const dateBegin = new Date(2017, 0, 1);
//     const dateEnd = new Date(2017, 0, 3);
//
//     const dailyFromPeriod = service.getDailyFromPeriod(dateBegin, dateEnd);
//
//     expect(dailyFromPeriod).toEqual(2);
//   });
//
//   it('should return newFormArray in getChildAgeListFormGroupWithValueDefault', () => {
//     const size = 2;
//
//     const newFormArray = service.getChildAgeListFormGroupWithValueDefault(size);
//
//     expect(newFormArray.length).toEqual(size);
//     expect(newFormArray[0].get('childAge').value).toEqual(0);
//     expect(newFormArray[1].get('childAge').value).toEqual(0);
//   });
//
//   it('should canCancelReservation', () => {
//     const reservation = new Reservation();
//     const reservatioItemView = new ReservationItemView();
//     reservatioItemView.id = 1;
//     reservatioItemView.status = ReservationStatusEnum.ToConfirm;
//     reservatioItemView.status = ReservationStatusEnum.Confirmed;
//     reservation.reservationItemViewList.push(reservatioItemView);
//
//     expect(service.canCancelReservation(reservation)).toBeTruthy();
//   });
//
//   it('should not canCancelReservation', () => {
//     const reservation = new Reservation();
//     const reservatioItemView = new ReservationItemView();
//     reservatioItemView.id = 1;
//     reservatioItemView.status = ReservationStatusEnum.Checkin;
//     reservatioItemView.status = ReservationStatusEnum.Canceled;
//     reservation.reservationItemViewList.push(reservatioItemView);
//
//     expect(service.canCancelReservation(reservation)).toBeFalsy();
//   });
//
//   it('should canReactivateReservation', () => {
//     const reservation = new Reservation();
//     const reservatioItemView = new ReservationItemView();
//     reservatioItemView.id = 1;
//     reservatioItemView.status = ReservationStatusEnum.Canceled;
//     reservation.reservationItemViewList.push(reservatioItemView);
//
//     expect(service.canReactivateReservation(reservation)).toBeTruthy();
//   });
//
//   it('should not canReactivateReservation', () => {
//     const reservation = new Reservation();
//     const reservatioItemView = new ReservationItemView();
//     reservatioItemView.id = 1;
//     reservatioItemView.status = ReservationStatusEnum.ToConfirm;
//     reservation.reservationItemViewList.push(reservatioItemView);
//
//     expect(service.canReactivateReservation(reservation)).toBeFalsy();
//   });
//
//   it('should return reservationConfirmationGroupIsInvalid how true', () => {
//     expect(service.reservationConfirmationGroupIsInvalid(true, true, true, true, false)).toBeTruthy();
//   });
//
//   it('should return reservationConfirmationGroupIsInvalid how false', () => {
//     expect(service.reservationConfirmationGroupIsInvalid(true, true, true, true, true)).toBeFalsy();
//   });
//
//   it('should getReservationItemViewListWithStatusColorFooter', () => {
//     spyOn(service['statusStyleService'], 'isCanceledStatus').and.returnValue(true);
//     spyOn(service['statusStyleService'], 'isConfirmedStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isToConfirmStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isNoShowStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isCheckinStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isPendingStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isCheckoutStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isWaitListStatus').and.returnValue(false);
//     let reservationItemViewList = new Array<ReservationItemView>();
//     reservationItemViewList.push(new ReservationItemView());
//
//     reservationItemViewList = service.getReservationItemViewListWithStatusColorFooter(reservationItemViewList);
//
//     expect(reservationItemViewList[0].statusColorFooter).toEqual({
//       'thf-u-background-color--cancel': true,
//       'thf-u-background-color--confirmed': false,
//       'thf-u-background-color--to-confirm': false,
//       'thf-u-background-color--no-show': false,
//       'thf-u-background-color--checkin': false,
//       'thf-u-background-color--pending': false,
//       'thf-u-background-color--checkout': false,
//       'thf-u-background-color--wait-list': false,
//     });
//   });
//
//   it('should getStatusColorByReservationStatus', () => {
//     spyOn(service['statusStyleService'], 'isCanceledStatus').and.returnValue(true);
//     spyOn(service['statusStyleService'], 'isConfirmedStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isToConfirmStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isNoShowStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isCheckinStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isPendingStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isCheckoutStatus').and.returnValue(false);
//     spyOn(service['statusStyleService'], 'isWaitListStatus').and.returnValue(false);
//
//     const statusColorObj = service.getStatusColorByReservationStatus(ReservationStatusEnum.ToConfirm);
//
//     expect(statusColorObj).toEqual({
//       'thf-u-background-color--cancel': true,
//       'thf-u-background-color--confirmed': false,
//       'thf-u-background-color--to-confirm': false,
//       'thf-u-background-color--no-show': false,
//       'thf-u-background-color--checkin': false,
//       'thf-u-background-color--pending': false,
//       'thf-u-background-color--checkout': false,
//       'thf-u-background-color--wait-list': false,
//     });
//   });
//
//   it('should deadlineIsInvalid when deadline is greater than checkin', () => {
//     const deadline = new Date(2018, 1, 20);
//     const checkin = new Date(2018, 1, 19);
//
//     expect(service.deadlineIsInvalid(deadline, checkin)).toBeTruthy();
//   });
//
//   it('should deadlineIsInvalid when deadline is less than checkin', () => {
//     const deadline = new Date(2018, 1, 19);
//     const checkin = new Date(2018, 1, 20);
//
//     expect(service.deadlineIsInvalid(deadline, checkin)).toBeFalsy();
//   });
//
//   it('should deadlineIsInvalid when deadline is equal checkin', () => {
//     const deadline = new Date(2018, 1, 20);
//     const checkin = new Date(2018, 1, 20);
//
//     expect(service.deadlineIsInvalid(deadline, checkin)).toBeFalsy();
//   });
// });
