import { createServiceStub } from '../../../../../../testing';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';

export const reservationServiceStub = createServiceStub(ReservationService, {
    getDateListFromPeriod(period: any, removeLast: boolean = true): Array<any> {
        return null;
    }
});
