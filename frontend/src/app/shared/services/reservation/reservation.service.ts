import { EMPTY, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BaseErrorValidation } from './../../services/shared/base-error-validation';
import { ValidatorFormService } from '../../services/shared/validator-form.service';
import { DateService } from '../../services/shared/date.service';
import { RoomLayoutResource } from './../../resources/room-layout/room-layout.resource';
import { SharedService } from '../../services/shared/shared.service';
import { RoomTypeService } from '../../../room-type/shared/services/room-type/room-type.service';
import { ReservationItemViewService } from './../reservation-item-view/reservation-item-view.service';
import { StatusStyleService } from './status-style.service';
import { RoomLayout } from './../../models/room-layout';
import { Reservation } from './../../models/reserves/reservation';
import { ReservationStatusEnum } from './../../models/reserves/reservation-status-enum';
import { ReservationItemView } from './../../models/reserves/reservation-item-view';
import { Room } from './../../models/room';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { BedTypeEnum } from './../../models/bed-type';
import { Guest, GuestState } from '../../models/reserves/guest';
import {
  ReservationItemCommercialBudgetConfig
} from 'app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';
import * as _ from 'lodash';
import * as moment from 'moment';
import { SearchReservationResultDto } from 'app/shared/models/dto/search-reservation-result-dto';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { RateProposalShareInfoService } from 'app/shared/services/rate-proposal/rate-proposal-share-info.service';
import { PropertyStorageService } from '@bifrost/storage/services/property-storage.service';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';



@Injectable({ providedIn: 'root' })
export class ReservationService extends BaseErrorValidation {
  constructor(
    public location: Location,
    public translateService: TranslateService,
    public formBuilder: FormBuilder,
    private validatorFormService: ValidatorFormService,
    private dateService: DateService,
    private roomLayoutResource: RoomLayoutResource,
    private roomTypeService: RoomTypeService,
    private sharedService: SharedService,
    private reservationItemViewService: ReservationItemViewService,
    private statusStyleService: StatusStyleService,
    public router: Router,
    public route: ActivatedRoute,
    public rateProposalSharedInfo: RateProposalShareInfoService,
    private propertyService: PropertyStorageService,
  ) {
    super();
  }

  public checkStatusReservationItemCanceledToReorderReservationItemList(reservationItemList: Array<ReservationItemView>) {
    return reservationItemList.sort(reservationItem => {
      if (reservationItem.status === ReservationStatusEnum.Canceled) {
        return 1;
      }
    });
  }

  public getRemarksQuantityOfReservation(...comments: string[]): number {
    const commentsWithContent = comments.filter(comment => !!comment);
    return commentsWithContent.length;
  }

  public getReservationItemViewListWithFormCardAndPricesCalculated(
    reservationItemViewList: Array<ReservationItemView>,
    roomTypes: Array<RoomType>,
  ): Array<ReservationItemView> {
    reservationItemViewList.map(reservationItemView => {
      reservationItemView.roomTypes = roomTypes;
      reservationItemView.formCard = this.initFormGroupReservationItemView(reservationItemView);
      reservationItemView.dailyNumber = this.getDailyFromPeriod(
        reservationItemView.period.beginJsDate,
        reservationItemView.period.endJsDate,
      );
      reservationItemView.dailyPrice = this.getDailyPrice(reservationItemView);
      reservationItemView.totalDaily = this.getTotalDaily(reservationItemView);
      return reservationItemView;
    });

    this.setFocusInFirstItem(reservationItemViewList);
    return reservationItemViewList;
  }

  private setFocusInFirstItem(reservationItemViewList: Array<ReservationItemView>) {
    reservationItemViewList[0].focus = true;
  }

  public getDailyPrice(reservationItemView: ReservationItemView): number {
    let totalValue = 0;
    let days = 0;
    reservationItemView.reservationItemCommercialBudgetConfig.reservationBudgetTableList.forEach(reservationBudget => {
      days += 1;
      totalValue += reservationBudget.manualRate;
    });
    return this.checkTotalValueAndDaysIsGreaterThanZero(totalValue, days) ? totalValue / days : 0;
  }

  private checkTotalValueAndDaysIsGreaterThanZero(totalValue: number, days: number): boolean {
    return totalValue > 0 && days > 0;
  }

  public getTotalDaily(reservationItemView: ReservationItemView): number {
    let totalValue = 0;
    let days = 0;
    reservationItemView.reservationItemCommercialBudgetConfig.reservationBudgetTableList.forEach(reservationBudget => {
      days += 1;
      totalValue += reservationBudget.manualRate;
    });
    return totalValue;
  }

  public getDateListFromPeriod(period: any, removeLast = true): Array<any> {
    const beginDate = this.dateService.convertStringToDate(period.beginDate, DateService.DATE_FORMAT_UNIVERSAL);
    const endDate = this.dateService.convertStringToDate(period.endDate, DateService.DATE_FORMAT_UNIVERSAL);

    const dateListFromPeriod = this.dateService.convertPeriodToDateList(beginDate, endDate, removeLast);
    return dateListFromPeriod;
  }

  public generateCodesForReservationItemViewList(reservationItemViewList: Array<ReservationItemView>): void {
    for (let i = 0; i < reservationItemViewList.length; i++) {
      if (!reservationItemViewList[i].id && !reservationItemViewList[i].code) {
        if (i < 9) {
          reservationItemViewList[i].code = '000' + (i + 1);
        } else if (i < 99) {
          reservationItemViewList[i].code = '00' + (i + 1);
        } else if (i < 999) {
          reservationItemViewList[i].code = '0' + (i + 1);
        } else {
          reservationItemViewList[i].code = (i + 1).toString();
        }

        this.setReservationItemViewCodeInBudgetConfig(reservationItemViewList[i]);
      }
    }
  }

  private setReservationItemViewCodeInBudgetConfig(reservationItemView: ReservationItemView) {
    reservationItemView.reservationItemCommercialBudgetConfig.reservationItemCode = reservationItemView.code;
  }

  public getContactName(formGroup: AbstractControl): string {
    return formGroup.get('name').value;
  }

  public getContactEmail(formGroup: AbstractControl): string {
    return formGroup.get('email').value;
  }

  public getContactPhone(formGroup: AbstractControl): string {
    return formGroup.get('phone').value;
  }

  public checkinHourAndMinutesValid(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (this.dateService.hourAndMinutesAreInValid(control.value)) {
        return { hourAndMinutesAreInValid: { valid: false } };
      }
      return null;
    };
  }

  public emptyString(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const errorMessage = { emptyString: {valid: false} };
      return `${control.value}`.trim() == '' ? errorMessage : null;
    };
  }


  public getDailyFromPeriod(dateBegin: Date, dateEnd: Date): number {
    let diffDays = 0;

    if (dateBegin && dateEnd) {
      diffDays = this.dateService.diffDays(dateBegin, dateEnd);
      if (diffDays == 0) {
        diffDays = 1;
      }
      return diffDays;
    }

    return diffDays;
  }

  public getReservationItemViewList(formGroup: AbstractControl, roomTypes: Array<RoomType>, averageDailyPrice: number,
                                    reservationItemBudgetConfig: ReservationItemCommercialBudgetConfig): Array<ReservationItemView> {
    const roomQuantity = formGroup.get('roomQuantity').value;
    if (roomQuantity > 1) {
      const reservationItemViewAccordingWithRoomQuantityList = new Array<ReservationItemView>();
      for (let i = 0; i < roomQuantity; i++) {
        reservationItemViewAccordingWithRoomQuantityList.push(
          this.getAccommodationReserve(formGroup, roomTypes, averageDailyPrice, reservationItemBudgetConfig)
        );
      }
      return reservationItemViewAccordingWithRoomQuantityList;
    } else {
      const accomodationReserveList = new Array<ReservationItemView>();
      accomodationReserveList.push(this.getAccommodationReserve(formGroup, roomTypes, averageDailyPrice, reservationItemBudgetConfig));
      return accomodationReserveList;
    }
  }

  private getAccommodationReserve(formGroup: AbstractControl, roomTypes: Array<RoomType>, averageDailyPrice: number,
                                  reservationItemBudgetConfig: ReservationItemCommercialBudgetConfig): ReservationItemView {
    const reservationItemView = new ReservationItemView();
    reservationItemView.focus = false;
    reservationItemView.room = new Room();
    reservationItemView.room.id = null;
    reservationItemView.rooms = new Array<Room>();
    reservationItemView.rooms = formGroup.get('roomType').value.roomList ? formGroup.get('roomType').value.roomList : [];
    reservationItemView.receivedRoomType = new RoomType();
    reservationItemView.receivedRoomType = formGroup.get('roomType').value;
    reservationItemView.initialReceivedRoomType = new RoomType();
    reservationItemView.initialReceivedRoomType = formGroup.get('roomType').value;
    reservationItemView.requestedRoomType = new RoomType();
    reservationItemView.requestedRoomType = formGroup.get('roomType').value;
    reservationItemView.initialRequestedRoomType = new RoomType();
    reservationItemView.initialRequestedRoomType = formGroup.get('roomType').value;
    reservationItemView.roomTypes = new Array<RoomType>();
    reservationItemView.roomTypes = roomTypes;
    reservationItemView.period = formGroup.get('period').value;
    reservationItemView.initialPeriod = formGroup.get('period').value;
    reservationItemView.adultQuantity = formGroup.get('adultQuantity').value;
    reservationItemView.childrenQuantity = formGroup.get('childrenQuantity').value;
    reservationItemView.status = ReservationStatusEnum.ToConfirm;
    reservationItemView.guestQuantity = this.setGuestQuantity(reservationItemView.adultQuantity, reservationItemView.childrenQuantity);
    reservationItemView.checkinHour = formGroup.get('checkinHour').value;
    reservationItemView.checkoutHour = formGroup.get('checkoutHour').value;
    reservationItemView.schedulePopoverIsVisible = false;
    reservationItemView.childrenPopoverIsVisible = false;
    reservationItemView.dailyNumber = this.getDailyFromPeriod(
      formGroup.get('period').value.beginJsDate,
      formGroup.get('period').value.endJsDate,
    );
    reservationItemView.dailyPrice = averageDailyPrice;
    reservationItemView.totalDaily = reservationItemView.dailyPrice * reservationItemView.dailyNumber;
    reservationItemView.formCard = this.initFormGroupReservationItemView(reservationItemView, true, formGroup);
    reservationItemView.extraBedQuantityList = this.setBedOptionalQuantity(
      reservationItemView.formCard.value.roomTypeCard, BedTypeEnum.Single
    );
    reservationItemView.cribBedQuantityList = this.setBedOptionalQuantity(
      reservationItemView.formCard.value.roomTypeCard, BedTypeEnum.Crib
    );
    reservationItemView.extraCribCount = 0;
    reservationItemView.extraBedCount = 0;
    reservationItemView.reservationItemCommercialBudgetConfig = new ReservationItemCommercialBudgetConfig();
    reservationItemView.reservationItemCommercialBudgetConfig = _.cloneDeep(reservationItemBudgetConfig);

    this.getRoomLayoutList(reservationItemView).subscribe(roomLayoutList => {
      reservationItemView.roomLayoutList = roomLayoutList;
    });


    return reservationItemView;
  }

  public getRoomLayoutList(reservationItemView: ReservationItemView): Observable<Array<RoomLayout>> {
    if (reservationItemView.formCard.get('roomTypeCard').value) {
      return this.roomLayoutResource.getRoomLayoutListByRoomTypeId(reservationItemView.formCard.get('roomTypeCard').value.id);
    } else {
      return EMPTY;
    }
  }

  public getRoomLayoutCardOfTheReservationItemView(reservationItemView: ReservationItemView): RoomLayout {
    let roomLayout: RoomLayout;
    if (reservationItemView.formCard.get('roomLayoutCard').value.id) {
      roomLayout = this.findRoomLayoutOfTheReservationItemViewInRoomLayoutListOfTheRoomTypeSelected(
        reservationItemView,
        reservationItemView.roomLayoutList,
      );
      if (this.sharedService.objectIsNullOrUndefined(roomLayout)) {
        roomLayout = reservationItemView.roomLayoutList[0];
      }
    } else {
      roomLayout = reservationItemView.roomLayoutList[0];
    }
    return roomLayout;
  }

  private findRoomLayoutOfTheReservationItemViewInRoomLayoutListOfTheRoomTypeSelected(
    reservatioItemView: ReservationItemView,
    roomLayoutList: Array<RoomLayout>,
  ) {
    return roomLayoutList.find(r => r.id == reservatioItemView.formCard.get('roomLayoutCard').value.id);
  }

  public setBedOptionalQuantity(roomType: RoomType, bedType: number) {
    let quantityoptionalBedType;
    const quantityoptionalBedTypeList = new Array<any>();

    quantityoptionalBedType = this.roomTypeService.getQuantityOptionalBedType(roomType, bedType);

    if (quantityoptionalBedType) {
      return this.sharedService.createRange(quantityoptionalBedType.optionalLimit);
    }

    return this.sharedService.createRange(0);
  }

  public initFormGroupReservationItemView(
    reservationItemView: ReservationItemView,
    isCreatingNewReservationItemFormGroup = false,
    newReservationItemFormGroup: AbstractControl = null,
  ): FormGroup {
    const formCard = this.formBuilder.group({
      periodCard: [reservationItemView.period, [Validators.required]],
      adultQuantityCard: [reservationItemView.adultQuantity,
        [
          Validators.required,
          this.validatorFormService.capacityNumberIsMajorThanOne,
          this.validatorFormService.setNumberToZero
        ],
      ],
      childrenQuantityCard: [reservationItemView.childrenQuantity, [this.validatorFormService.setNumberToZero]],
      roomTypeCard: [reservationItemView.receivedRoomType, [Validators.required]],
      roomTypeRequestedCard: [reservationItemView.requestedRoomType, [Validators.required]],
      checkinHourCard: [reservationItemView.checkinHour, [Validators.required, this.checkinHourAndMinutesValid()]],
      checkoutHourCard: [reservationItemView.checkoutHour, [Validators.required, this.checkinHourAndMinutesValid()]],
      roomNumberCard: [reservationItemView.room ? reservationItemView.room : ''],
      roomLayoutCard: [reservationItemView.roomLayout ? reservationItemView.roomLayout : ''],
      extraBedQuantityCard: [''],
      cribBedQuantityCard: [''],
      hasCribBedTypeCard: [false],
      hasExtraBedTypeCard: [false],
      childrenAgeListCard: this.formBuilder.array([]),
      emitBudgetEvent: true,
      endDate: [reservationItemView.period.endDate, [Validators.required]]
    });

    if (isCreatingNewReservationItemFormGroup) {
      (<FormGroup>formCard).setControl(
        'childrenAgeListCard',
        this.formBuilder.array(this.initChildAgeFormArray(newReservationItemFormGroup)),
      );
    } else {
      (<FormGroup>formCard).setControl('childrenAgeListCard', this.formBuilder.array(this.loadChildAgeFormArray(reservationItemView)));
    }

    return formCard;
  }

  public initChildAgeFormArray(newReservationItemFormGroup: AbstractControl): Array<FormGroup> {
    const newFormArray = new Array<FormGroup>();
    const controls = (<FormArray>newReservationItemFormGroup.get('childrenAgeList')).controls;

    for (let i = 0; i < controls.length; i++) {
      newFormArray.push(
        this.formBuilder.group({
          childAge: [controls[i].get('childAge').value],
        }),
      );
    }
    return newFormArray;
  }

  public loadChildAgeFormArray(reservationItemView: ReservationItemView): Array<FormGroup> {
    const formArray = new Array<FormGroup>();
    const childGuestList = reservationItemView.guestQuantity.filter(guest => guest.isChild);

    childGuestList.forEach(guest => {
      formArray.push(
        this.formBuilder.group({
          childAge: guest.childAge,
        }),
      );
    });

    return formArray;
  }

  public getRoomTypeSelected(roomTypes: Array<RoomType>, roomTypeSelected: RoomType) {
    return roomTypes.filter(roomTypeOption => {
      if (roomTypeOption.id === roomTypeSelected.id) {
        return roomTypeSelected;
      }
    })[0];
  }

  public getMessageError(controlForm: AbstractControl): string {
    let errorMessage = '';

    if (controlForm.dirty && controlForm.errors) {
      Object.keys(controlForm.errors)
        .map(key => {
          if (controlForm.errors.required) {
            errorMessage = this.translateService.instant('commomData.requiredFields');
          } else if (controlForm.errors.hourAndMinutesAreInValid) {
            errorMessage = this.translateService.instant('reservationModule.commomData.hourAndMinutesAreInValid');
          }
        })
        .join(' ');
    }
    return errorMessage;
  }

  private setGuestQuantity(adultQtd: number, childrenQtd: number) {
    const guestArray = [];

    for (let i = 0; i < adultQtd; i++) {
      const guest = new Guest();
      guest.state = GuestState.NoGuest;
      guest.isChild = false;
      guest.formGuest = this.getFormGroupGuestName();
      guest.isMain = i === 0;
      guestArray.push(guest);
    }

    for (let i = 0; i < childrenQtd; i++) {
      const guest = new Guest();
      guest.state = GuestState.NoGuest;
      guest.isChild = true;
      guest.formGuest = this.getFormGroupGuestName();
      guestArray.push(guest);
    }

    return guestArray;
  }

  private getFormGroupGuestName() {
    return this.formBuilder.group({
      guestName: [''],
    });
  }

  public getTotalsValuesReservationItemView(reservationItemViewList: Array<ReservationItemView>) {
    const totals = {
      adultQuantity: 0,
      childrenQuantity: 0,
      rooms: 0,
      guests: 0,
      valuesDailies: 0,
      currencySymbol: null
    };

    totals.rooms = reservationItemViewList.length;
    for (let count = 0; count < totals.rooms; count++) {
      let totalBudget = 0;

      reservationItemViewList[count].budgetReservationBudgetList.forEach( item => {
        totalBudget += item.manualRate;
        totals.currencySymbol = item.currencySymbol;
      });

      totals.valuesDailies = totals.valuesDailies +
        totalBudget;
      totals.adultQuantity = totals.adultQuantity + reservationItemViewList[count].formCard.get('adultQuantityCard').value || 0;
      totals.childrenQuantity = totals.childrenQuantity + reservationItemViewList[count].formCard.get('childrenQuantityCard').value || 0;
    }

    totals.guests = totals.childrenQuantity + totals.adultQuantity;

    return totals;
  }

  public getStatusIsPossibleToSaveReserve(
    formValid: boolean,
    selectOriginFormIsValid: boolean,
    selectMarketSegmentFormIsValid: boolean,
    reservationItemViewList: Array<ReservationItemView>,
  ) {
    const disabled = !formValid || !(reservationItemViewList.length > 0) || !selectOriginFormIsValid || !selectMarketSegmentFormIsValid;
    let formCardInvalid = false;
    if (reservationItemViewList.length > 0) {
      for (let i = 0; i < reservationItemViewList.length; i++) {
        if (!reservationItemViewList[i].formCard.valid) {
          formCardInvalid = !reservationItemViewList[i].formCard.valid;
          break;
        }
      }
      return disabled || formCardInvalid;
    }

    return disabled;
  }

  public reservationConfirmationGroupIsInvalid(
    selectValuePaymentTypeIsValid: boolean,
    moneyGroupIsValid: boolean,
    deadlineGroupIsValid: boolean,
    creditCardGroupIsValid: boolean,
    clientToBillGroupIsValid: boolean,
  ): boolean {
    return (
      !selectValuePaymentTypeIsValid || !moneyGroupIsValid || !deadlineGroupIsValid || !creditCardGroupIsValid || !clientToBillGroupIsValid
    );
  }

  public getNewRoomTypesByGuestCapacity(roomTypes: Array<RoomType>, adultQuantity: number, childrenQuantity: number) {
    const newRoomTypes = new Array<RoomType>();
    if (roomTypes) {
      roomTypes.forEach(roomType => {
        if (roomType.adultCapacity >= adultQuantity && roomType.childCapacity >= childrenQuantity) {
          newRoomTypes.push(roomType);
        }
      });
    }
    return newRoomTypes;
  }

  public getChildAgeListFormGroupWithValueDefault(size: number): Array<AbstractControl> {
    const list = Array(size);
    return this.getChildAgeListFormGroupWithValue(list);
  }

  public getChildAgeListFormGroupWithValue(childAges: Array<any>): Array<AbstractControl> {
    const newFormArray = new Array<FormGroup>();
    for (let i = 0; i < childAges.length; i++) {
      newFormArray.push(
        this.formBuilder.group({
          childAge: childAges[i],
        }),
      );
    }
    return newFormArray;
  }

  public getChildAgeCardListFormGroupWithValueDefault(size: number): Array<AbstractControl> {
    const newFormArray = new Array<FormGroup>();

    for (let i = 0; i < size; i++) {
      newFormArray.push(
        this.formBuilder.group({
          childAge: 0,
        }),
      );
    }
    return newFormArray;
  }

  public canCancelReservation(reservation: Reservation): boolean {
    for (const reservationItemView of reservation.reservationItemViewList) {
      if (reservationItemView.id > 0 && !this.reservationItemViewService.canCancelStatusOfReservationItem(reservationItemView)) {
        return false;
      }
    }
    return true;
  }

  public canReactivateReservation(reservation: Reservation): boolean {
    for (const reservationItemView of reservation.reservationItemViewList) {
      if (!this.reservationItemViewService.canReactivateStatusOfReservationItem(reservationItemView)) {
        return false;
      }
    }
    return true;
  }

  public getReservationItemViewListWithStatusColorFooter(reservationItemViewList: Array<ReservationItemView>): Array<ReservationItemView> {
    reservationItemViewList.forEach(reservationItemView => {
      reservationItemView.statusColorFooter = {
        'thf-u-background-color--cancel': this.statusStyleService.isCanceledStatus(reservationItemView.status),
        'thf-u-background-color--confirmed': this.statusStyleService.isConfirmedStatus(reservationItemView.status),
        'thf-u-background-color--to-confirm': this.statusStyleService.isToConfirmStatus(reservationItemView.status),
        'thf-u-background-color--no-show': this.statusStyleService.isNoShowStatus(reservationItemView.status),
        'thf-u-background-color--checkin': this.statusStyleService.isCheckinStatus(reservationItemView.status),
        'thf-u-background-color--pending': this.statusStyleService.isPendingStatus(reservationItemView.status),
        'thf-u-background-color--checkout': this.statusStyleService.isCheckoutStatus(reservationItemView.status),
        'thf-u-background-color--wait-list': this.statusStyleService.isWaitListStatus(reservationItemView.status),
      };
    });
    return reservationItemViewList;
  }

  public getStatusColorByReservationStatus(status: ReservationStatusEnum | number): object {
    return {
      'thf-u-background-color--cancel': this.statusStyleService.isCanceledStatus(status),
      'thf-u-background-color--confirmed': this.statusStyleService.isConfirmedStatus(status),
      'thf-u-background-color--to-confirm': this.statusStyleService.isToConfirmStatus(status),
      'thf-u-background-color--no-show': this.statusStyleService.isNoShowStatus(status),
      'thf-u-background-color--checkin': this.statusStyleService.isCheckinStatus(status),
      'thf-u-background-color--pending': this.statusStyleService.isPendingStatus(status),
      'thf-u-background-color--checkout': this.statusStyleService.isCheckoutStatus(status),
      'thf-u-background-color--wait-list': this.statusStyleService.isWaitListStatus(status),
    };
  }

  public deadlineIsInvalid(deadline: Date, checkin: Date): boolean {
    return deadline > checkin;
  }

  public getChildrenAgeListFromReservationItemView(reservationItemView: ReservationItemView): Array<number> {
    const childrenAgeList = [];
    const childrenAgeControlList = reservationItemView.formCard.get('childrenAgeListCard') as FormArray;
    childrenAgeControlList.controls.forEach(control => {
      childrenAgeList.push(control.get('childAge').value);
    });
    return childrenAgeList;
  }

  public getChildrenAgeListOfFormArray(formArray: FormArray, formControlName: string): Array<number> {
    const childrenAgeList = [];
    formArray.controls.forEach(control => {
      childrenAgeList.push(control.get(formControlName).value);
    });
    return childrenAgeList;
  }

  public canSaveNewReservationForm(newReservationForm: FormGroup, reservationItemViewList: ReservationItemView[]): boolean {
    return (
      !this.getStatusIsPossibleToSaveReserve(
        newReservationForm['controls'].contactGroup.valid,
        newReservationForm['controls'].selectOrigin.valid,
        newReservationForm['controls'].selectMarketSegment.valid,
        reservationItemViewList,
      ) &&
      !this.reservationConfirmationGroupIsInvalid(
        newReservationForm.controls.selectValuePaymentType.valid,
        newReservationForm.controls.moneyGroup.valid,
        newReservationForm.controls.deadlineGroup.valid,
        newReservationForm.controls.creditCardGroup.valid,
        newReservationForm.controls.clientToBillGroup.valid,
      )
    );
  }

  public hasChange(newReservationForm: FormGroup, reservation: Reservation) {
    return (
      this.newReservationFormWasChanged(newReservationForm) ||
      this.reservationItemViewService.hasChangeInReservationViewList(reservation.reservationItemViewList)
    );
  }

  public newReservationFormWasChanged(newReservationForm: FormGroup): boolean {
    return (
      newReservationForm.get('contactGroup').dirty ||
      newReservationForm.get('selectMarketSegment').dirty ||
      newReservationForm.get('selectValuePaymentType').dirty ||
      newReservationForm.get('moneyGroup').dirty ||
      newReservationForm.get('deadlineGroup').dirty ||
      newReservationForm.get('creditCardGroup').dirty ||
      newReservationForm.get('clientToBillGroup').dirty
    );
  }

  public getCalculateDaily(arrivalDate: Date, departureDate: Date) {
    if (arrivalDate && departureDate) {
     const calc = new Date(departureDate).getTime() - new Date(arrivalDate).getTime();
     return Math.round(Math.abs(calc / (1000 * 60 * 60 * 24))) - 1;
    }
  }

  public showCancelButton(itemData: SearchReservationResultDto): boolean {
    return (
      itemData.reservationItemStatusId == ReservationStatusEnum.ToConfirm ||
      itemData.reservationItemStatusId == ReservationStatusEnum.Confirmed
    );
  }

  public showCheckinButton(itemData: SearchReservationResultDto): boolean {
  return (
    itemData.reservationItemStatusId == ReservationStatusEnum.Confirmed ||
    itemData.reservationItemStatusId == ReservationStatusEnum.ToConfirm ||
    itemData.guestReservationItemStatusId == ReservationStatusEnum.Confirmed ||
    itemData.guestReservationItemStatusId == ReservationStatusEnum.ToConfirm
  );
  }

  public showCancelCheckinButton(propertyId: number, itemData: SearchReservationResultDto): boolean {
    const checkinDate = this.dateService.convertStringToDate( `${itemData.arrivalDate}`, DateService.DATE_FORMAT_UNIVERSAL);
    const systemDate = this.dateService.getSystemDate(propertyId);
    return itemData.guestReservationItemStatusId == ReservationStatusEnum.Checkin && moment(checkinDate).isSame(systemDate, 'days') ;
  }

  public goToReservationConfirm(
      rateProposalSearch: RateProposalSearchModel,
      rateProposalItemSelected: RateProposalRateTypeItem
  ) {
    const propertyId = this.propertyService.getCurrentPropertyId();
    const token = this.rateProposalSharedInfo.storeRateProposal(
        rateProposalSearch,
        rateProposalItemSelected,
    );
    this.router.navigate([
      'p', propertyId, 'reservation', 'new'
    ], { queryParams: { rateProposal: token} });
  }

  public goToEditCheckinPage(id: number, reservationItemCode: string, propertyId: string) {
    this.router.navigate(['p', propertyId, 'guest', 'fnrh', 'edit', id, reservationItemCode]);
  }

  public showConfirmedButton(itemData: SearchReservationResultDto): boolean {
    return itemData.reservationItemStatusId == ReservationStatusEnum.ToConfirm;
  }

  public showToConfirmButton(itemData: SearchReservationResultDto): boolean {
    return itemData.reservationItemStatusId == ReservationStatusEnum.WaitList;
  }

  public showReactivateButton(itemData: SearchReservationResultDto): boolean {
    return itemData.reservationItemStatusId == ReservationStatusEnum.Canceled;
  }

  public showEditReservationButton(itemData: SearchReservationResultDto): boolean {
    return (
      itemData.reservationItemStatusId == ReservationStatusEnum.ToConfirm ||
      itemData.reservationItemStatusId == ReservationStatusEnum.Confirmed ||
      itemData.reservationItemStatusId == ReservationStatusEnum.Checkin ||
      itemData.reservationItemStatusId == ReservationStatusEnum.WaitList ||
      itemData.reservationItemStatusId == ReservationStatusEnum.Pending
    );
  }

  public goToViewReservationPage(id: number, propertyId: string) {
    this.router.navigate(['p', propertyId, 'reservation', 'view'], {queryParams: {reservation: id}});
  }

  public showAddGuestEntranceButton(itemData: SearchReservationResultDto, roomTypeList: Array<any>): boolean {
    let found;

    if ( roomTypeList && roomTypeList.length) {
      found = roomTypeList.find( roomType => roomType.id === itemData.receivedRoomTypeId);
    }

    return !!found
      && itemData.reservationItemStatusId === ReservationStatusEnum.Checkin
      && (
        (
          itemData['adultsCount'] <= found.adultCapacity
          && itemData['childCount'] < found.childCapacity
        ) ||
        (
          itemData['adultsCount'] < found.adultCapacity
          && itemData['childCount'] <= found.childCapacity
        )
      );
  }



  public showBillingAccountButton(billbingAccountId) {
    return billbingAccountId != GuidDefaultData;
  }

  public showGuestFileButton(itemData: SearchReservationResultDto): boolean {
    return !(itemData.reservationItemStatusId == ReservationStatusEnum.Checkout
      || itemData.reservationItemStatusId == ReservationStatusEnum.Canceled);
  }


  public showAddGuestButton(itemData: SearchReservationResultDto): boolean {
    return itemData.reservationItemStatusId == ReservationStatusEnum.Checkin;
  }

  public showOrientateExpensesButton(itemData: SearchReservationResultDto): boolean {
    return (
      itemData.reservationItemStatusId == ReservationStatusEnum.ToConfirm ||
      itemData.reservationItemStatusId == ReservationStatusEnum.Confirmed ||
      itemData.reservationItemStatusId == ReservationStatusEnum.WaitList ||
      itemData.reservationItemStatusId == ReservationStatusEnum.Checkin
    );
  }

  // NAVIGATE
  public goBackPreviewPage() {
    this.location.back();
  }

  public goToEditReservationPage(id: number, propertyId: string) {
    this.router.navigate(['p', propertyId, 'reservation', 'new'], {queryParams: {reservation: id}});
  }

  public goToGuestEntrance(item: any, propertyId: string) {
    this.router.navigate(
      [
        'p',
        propertyId,
        'guest',
        'fnrh',
        'edit',
        'entrance',
        item.reservationItemId,
        item.reservationCode
      ]);
  }

  public goToBillingAccount(billingAccountId, propertyId: string) {
    this.router.navigate(['p', propertyId, 'billing-account', 'edit'],
      {queryParams: {billingAccountId: billingAccountId}});
  }

  public goToBudgetPage(id: number, propertyId: string) {
    this.router.navigate(['p', propertyId, 'rm', 'reservation-budget', id]);
  }

  public goToGuestPage(item: any, propertyId: string) {
    this.router.navigate(['p', propertyId, 'guest', 'fnrh', 'edit', item.reservationItemId,
      item.reservationCode, 'guest', item.guestReservationItemId]);
  }

  public showViewReservationButton(): boolean {
    return true;
  }

  public goToReservationList(propertyId?: number) {
    return this.router.navigate(['p', `${propertyId}`, 'reservation' ]);
  }

  public goToReservationBudget(reservationId: number, propertyId: number|string ) {
    return this.router
      .navigate(['p', propertyId, 'rm', 'reservation-budget', reservationId]);
  }

  public goToFnrh(reservationId?: number|string, propertyId?: number|string) {
    return this.router.navigate(['p', propertyId,  'guest', 'fnrh', 'new', reservationId]);
  }

  public goToSlip(reservationId?: number|string, propertyId?: number|string) {
    return this.router.navigate(['p', propertyId, 'reservation', 'slip', reservationId]);
  }

  public goToRoomList(id: string|number, reservationItemCount: number, propertyId?: number ) {
    if (reservationItemCount > 1) {
      return this.router.navigate(['p', propertyId, 'guest', 'edit', 'room-list', id]);
    }
    return this.router.navigate(['p', propertyId, 'guest', 'edit', id]);
  }


  public goToSlipPage(item: SearchReservationResultDto, propertyId: string) {
    return this.router.navigate(['p', propertyId, 'reservation', 'slip', item.id]);
  }

  public goToNewReservation(propertyId: string) {
    return this.router.navigate(['p', propertyId, 'reservation', 'new']);
  }

  public showReactivateNoShowButton(itemData: SearchReservationResultDto): boolean {
    return itemData.reservationItemStatusId == ReservationStatusEnum.NoShow;
  }
}
