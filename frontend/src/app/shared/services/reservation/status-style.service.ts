import { Injectable } from '@angular/core';
import { ReservationStatusEnum } from './../../models/reserves/reservation-status-enum';

@Injectable({ providedIn: 'root' })
export class StatusStyleService {
  public isCanceledStatus(status: ReservationStatusEnum) {
    return status === ReservationStatusEnum.Canceled;
  }

  public isToConfirmStatus(status: ReservationStatusEnum) {
    return status === ReservationStatusEnum.ToConfirm;
  }

  public isConfirmedStatus(status: ReservationStatusEnum) {
    return status === ReservationStatusEnum.Confirmed;
  }

  public isCheckinStatus(status: ReservationStatusEnum) {
    return status === ReservationStatusEnum.Checkin;
  }

  public isCheckoutStatus(status: ReservationStatusEnum) {
    return status === ReservationStatusEnum.Checkout;
  }

  public isPendingStatus(status: ReservationStatusEnum) {
    return status === ReservationStatusEnum.Pending;
  }

  public isNoShowStatus(status: ReservationStatusEnum) {
    return status === ReservationStatusEnum.NoShow;
  }

  public isWaitListStatus(status: ReservationStatusEnum) {
    return status === ReservationStatusEnum.WaitList;
  }

  public isReservationProposalStatus(status: ReservationStatusEnum) {
    return status === ReservationStatusEnum.ReservationProposal;
  }

  public getStatusColor(id: number) {
    switch (id) {
      case ReservationStatusEnum.ToConfirm:
        return '#37D4DD';
      case ReservationStatusEnum.Canceled:
        return '#D84141';
      case ReservationStatusEnum.WaitList:
        return '#ACA8BD';
      case ReservationStatusEnum.Pending:
        return '#FF720E';
      case ReservationStatusEnum.Checkin:
        return '#00B087';
      case ReservationStatusEnum.Checkout:
        return '#534C78';
      case ReservationStatusEnum.Confirmed:
        return '#00729F';
      case ReservationStatusEnum.NoShow:
        return '#FFAD2A';
      case ReservationStatusEnum.ReservationProposal:
        return '#F35678'; // TODO: descobrir a real cor do status ReservationProposal
    }
  }
}
