import { createServiceStub } from '../../../../../../testing';
import { BillingAccountItemService } from 'app/shared/services/billing-account-item/billing-account-item.service';
import { BillingAccountWithEntry } from 'app/shared/models/billing-account/billing-account-with-entry';
import { BillingAccountDestination } from 'app/shared/models/billing-account/billing-account-destination';
import { BillingAccountItemEntry } from 'app/shared/models/billing-account/billing-account-item-entry';
import { FormGroup } from '@angular/forms';
import { BillingAccountWithAccount } from 'app/shared/models/billing-account/billing-account-with-account';

export const billingAccountItemServiceStub = createServiceStub(BillingAccountItemService, {
    addBillingAccountItemListInStore
    (billingAccountWithEntryList: Array<BillingAccountWithEntry | BillingAccountDestination>): void {
    },

    canReverseBillingAccountItem(billingAccountItem: Array<BillingAccountItemEntry>): boolean {
        return true;
    },

    cleanPersistedData() {
    },

    deleteBillingAccountItemListInStore(): void {
    },

    discountTypeIsPercent(formGroup: FormGroup): boolean {
        return true;
    },

    discountTypeIsValue(formGroup: FormGroup): boolean {
        return true;
    },

    getBillingAccountWithEntryCheckedList(billingAccountWithAccount: BillingAccountWithAccount): Array<BillingAccountWithEntry> {
        return [];
    },

    persistData() {
    },

    recoverPersistedData() {
    }
});

