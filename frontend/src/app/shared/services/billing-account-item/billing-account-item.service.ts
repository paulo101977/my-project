import { BillingAccountItemEntry } from './../../models/billing-account/billing-account-item-entry';
import { BillingAccountItemTypeEnum } from './../../models/billing-account/billing-account-item-type-enum';
import { SharedStore } from './../../store/shared-store';
import { BillingAccountWithAccount } from './../../models/billing-account/billing-account-with-account';
import { BillingAccountWithEntry } from '../../models/billing-account/billing-account-with-entry';
import { SharedService } from '../../services/shared/shared.service';
import { BillingAccountDestination } from '../../models/billing-account/billing-account-destination';
import { Injectable, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';

@Injectable({ providedIn: 'root' })
export class BillingAccountItemService {

  private readonly TRANSFER_STORAGE_KEY = 'transfer-account';
  // Redux based variables
  public billingAccountItemEntryListObservable: Observable<Array<any>>;
  public billingAccountWithEntryList: Array<BillingAccountWithEntry>;

  constructor(private store: Store<SharedStore>, private sharedService: SharedService) {
    this.billingAccountItemEntryListObservable = store.pipe(select(storeItem => storeItem.billingAccountWithEntryList));
    this.billingAccountItemEntryListObservable.subscribe(list => {
      this.billingAccountWithEntryList = list;
    });
  }

  public canReverseBillingAccountItem(billingAccountItem: Array<BillingAccountItemEntry>) {
    return (
      billingAccountItem &&
      billingAccountItem.length > 0 &&
      billingAccountItem.findIndex(b => b.billingAccountItemTypeId == BillingAccountItemTypeEnum.Reverse) < 0
    );
  }

  public addBillingAccountItemListInStore(billingAccountWithEntryList: Array<BillingAccountWithEntry | BillingAccountDestination>): void {
    this.deleteBillingAccountItemListInStore();

    billingAccountWithEntryList.forEach(billingAccountWithEntry => {
      let billingAccountItemEntryCheckedList = new Array<BillingAccountItemEntry>();
      billingAccountItemEntryCheckedList = billingAccountWithEntry.billingAccountItems.filter(
        billingItemEntryChecked => billingItemEntryChecked.isChecked,
      );

      billingAccountWithEntry.billingAccountItems = [...billingAccountItemEntryCheckedList];
      const billingAccountWithEntryType = {
        type: 'ADD',
        billingAccountWithEntry: billingAccountWithEntry,
      };

      this.store.dispatch(billingAccountWithEntryType);
    });
  }

  public deleteBillingAccountItemListInStore(): void {
    const billingAccountItemWithType = {
      type: 'DELETE',
      billingAccountWithEntry: null,
    };
    this.store.dispatch(billingAccountItemWithType);
  }

  public getBillingAccountWithEntryCheckedList(billingAccountWithAccount: BillingAccountWithAccount): Array<BillingAccountWithEntry> {
    const billingAccountWithEntryCheckedList = new Array<BillingAccountWithEntry>();
    billingAccountWithAccount.billingAccounts.forEach(billingAccountWithEntry => {
      billingAccountWithEntry.billingAccountItems.forEach(billingAccountItemEntry => {
        if (
          !this.sharedService.elementExistsInList(billingAccountWithEntry, billingAccountWithEntryCheckedList) &&
          billingAccountItemEntry.isChecked
        ) {
          billingAccountWithEntryCheckedList.push(billingAccountWithEntry);
        }
      });
    });

    return billingAccountWithEntryCheckedList;
  }

  public discountTypeIsValue(formGroup: FormGroup): boolean {
    return formGroup.get('discountType').value == 'value';
  }

  public discountTypeIsPercent(formGroup: FormGroup): boolean {
    return formGroup.get('discountType').value == 'percent';
  }

  public persistData() {
    if (this.billingAccountWithEntryList && this.billingAccountWithEntryList.length > 0) {
      localStorage.setItem(this.TRANSFER_STORAGE_KEY, JSON.stringify(this.billingAccountWithEntryList));
    }
  }

  public recoverPersistedData() {
    const value = localStorage.getItem(this.TRANSFER_STORAGE_KEY);
    if (value && value.length > 0) {
      this.addBillingAccountItemListInStore(JSON.parse(value));
    }
  }

  public cleanPersistedData() {
    localStorage.removeItem(this.TRANSFER_STORAGE_KEY);
  }
}
