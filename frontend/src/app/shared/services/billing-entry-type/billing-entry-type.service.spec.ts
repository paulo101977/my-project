import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { BillingItemCategory } from '../../models/billingType/billing-item-category';
import { BillingItemCategoryData } from '../../mock-data/billing-type-group-data';
import { GetAllBillingItemTax } from '../../models/billingType/get-all-billing-item-tax';
import { GetAllBillingItemTaxData } from '../../mock-data/get-all-billing-item-tax-data';
import { GetAllBillingItemService } from '../../models/billingType/get-all-billing-item-service';
import { GetAllBillingItemServiceData } from '../../mock-data/get-all-billing-item-service-data';
import { BillingEntryTypeService } from './billing-entry-type.service';
import { SharedModule } from 'app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('BillingEntryTypeService', () => {
  let service: BillingEntryTypeService;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        SharedModule,
        RouterTestingModule
      ],
      providers: [],
    });

    service = TestBed.get(BillingEntryTypeService);
  });

  describe('on getAllBillingItemServiceBySearchData', () => {
    it('should get GetAllBillingItemServiceData by billingItemName', () => {
      let allItems = new Array<GetAllBillingItemService>();
      allItems.push(GetAllBillingItemServiceData);

      allItems = service.getAllBillingItemServiceBySearchData(GetAllBillingItemServiceData.billingItemName, allItems);

      expect(allItems[0].billingItemCategoryName).toEqual(GetAllBillingItemServiceData.billingItemCategoryName);
    });

    it('should get GetAllBillingItemServiceData by billingItemCategoryName', () => {
      let allItems = new Array<GetAllBillingItemService>();
      allItems.push(GetAllBillingItemServiceData);

      allItems = service.getAllBillingItemServiceBySearchData(GetAllBillingItemServiceData.billingItemCategoryName, allItems);

      expect(allItems[0].billingItemCategoryName).toEqual(GetAllBillingItemServiceData.billingItemCategoryName);
    });
  });

  describe('on getAllBillingItemCategoryBySearchData', () => {
    it('should get BillingItemCategoryData by categoryName', () => {
      let allItems = new Array<BillingItemCategory>();
      allItems.push(BillingItemCategoryData);

      allItems = service.getAllBillingItemCategoryBySearchData(BillingItemCategoryData.categoryName, allItems);

      expect(allItems[0].categoryName).toEqual(BillingItemCategoryData.categoryName);
    });

    it('should get BillingItemCategoryData by categoryFixedName', () => {
      let allItems = new Array<BillingItemCategory>();
      allItems.push(BillingItemCategoryData);

      allItems = service.getAllBillingItemCategoryBySearchData(BillingItemCategoryData.categoryFixedName, allItems);

      expect(allItems[0].categoryFixedName).toEqual(BillingItemCategoryData.categoryFixedName);
    });
  });

  describe('on getAllBillingItemTaxBySearchData', () => {
    it('should get GetAllBillingItemTaxData by billingItemName', () => {
      let allItems = new Array<GetAllBillingItemTax>();
      allItems.push(GetAllBillingItemTaxData);

      allItems = service.getAllBillingItemTaxBySearchData(GetAllBillingItemTaxData.billingItemName, allItems);

      expect(allItems[0].billingItemName).toEqual(GetAllBillingItemTaxData.billingItemName);
    });

    it('should get GetAllBillingItemTaxData by billingItemCategoryName', () => {
      let allItems = new Array<GetAllBillingItemTax>();
      allItems.push(GetAllBillingItemTaxData);

      allItems = service.getAllBillingItemTaxBySearchData(GetAllBillingItemTaxData.billingItemCategoryName, allItems);

      expect(allItems[0].billingItemCategoryName).toEqual(GetAllBillingItemTaxData.billingItemCategoryName);
    });
  });
});
