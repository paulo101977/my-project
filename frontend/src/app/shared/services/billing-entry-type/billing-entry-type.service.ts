import { DateService } from './../shared/date.service';
import { BillingItemCategory } from '../../models/billingType/billing-item-category';
import { GetAllBillingItemService } from '../../models/billingType/get-all-billing-item-service';
import { GetAllBillingItemTax } from '../../models/billingType/get-all-billing-item-tax';
import { BillingItemTaxServiceAssociate } from '../../models/billingType/billing-item-tax-service-associate';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class BillingEntryTypeService {
  constructor(private dateService: DateService) {}

  public getAllBillingItemServiceBySearchData(
    searchData: string,
    allItems: Array<GetAllBillingItemService>,
  ): Array<GetAllBillingItemService> {
    return allItems.filter(
      item =>
        item.billingItemName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        (item.billingItemCategoryName && item.billingItemCategoryName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1),
    );
  }

  public getAllBillingItemCategoryBySearchData(searchData: string, allItems: Array<BillingItemCategory>): Array<BillingItemCategory> {
    return allItems.filter(
      item =>
        item.categoryName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        (item.categoryFixedName && item.categoryFixedName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1),
    );
  }

  public getAllBillingItemTaxBySearchData(searchData: string, allItems: Array<GetAllBillingItemTax>): Array<GetAllBillingItemTax> {
    return allItems.filter(
      item =>
        item.billingItemName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        (item.billingItemCategoryName && item.billingItemCategoryName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1),
    );
  }

  public getTotalTaxPercentage(billingItemTaxServiceAssociateList: Array<BillingItemTaxServiceAssociate>): number {
    let totalTaxPercentage = 0;
    billingItemTaxServiceAssociateList.forEach(billingItemTaxServiceAssociate => {
      totalTaxPercentage += billingItemTaxServiceAssociate.taxPercentage;
    });
    return totalTaxPercentage;
  }

  public beginDateOfBillingItemIsEqualBeginDateOfFormControl(beginDate: Date, formGroup: FormGroup): boolean {
    return beginDate == formGroup.get('beginDate').value;
  }

  public endDateOfBillingItemIsEqualEndDateOfFormControl(endDate: Date, formGroup: FormGroup): boolean {
    return endDate && endDate == formGroup.get('endDate').value;
  }
}
