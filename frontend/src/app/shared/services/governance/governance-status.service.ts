import { Injectable } from '@angular/core';
import { Governance } from '../../models/governance/governance';
import { HouseKeepingStatusIdEnum } from '../../models/dto/housekeeping/housekeeping-status-id_enum';
import { AssetsService } from '@app/core/services/assets.service';

@Injectable({ providedIn: 'root' })
export class GovernanceStatusService {
  public HOUSEKEEPING_STATUS_ID_CREATED_BY_PROPERTY = 6;
  constructor( private assetsService: AssetsService) {}

  public getAllGovernanceStatusBySearchData(searchData: string, allItems: Array<Governance>): Array<Governance> {
    return allItems.filter(item => item.statusName && item.statusName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1);
  }

  public filterDefault(list: Array<Governance>) {
    if (list) {
      return list.filter(item => item.housekeepingStatusId != this.HOUSEKEEPING_STATUS_ID_CREATED_BY_PROPERTY);
    }
    return [];
  }

  public sortByDefault(list) {
    return list.sort((status1, status2) => {
      if (status1.housekeepingStatusId > status2.housekeepingStatusId) {
        return 1;
      } else if (status1.housekeepingStatusId < status2.housekeepingStatusId) {
        return -1;
      } else {
        return 0;
      }
    });
  }
  public getHousekeepingStatusIconById(housekeepingStatusId) {
    switch (housekeepingStatusId) {
      case HouseKeepingStatusIdEnum.clean:
        return this.assetsService.getImgUrlTo('icon_clean.svg');
      case HouseKeepingStatusIdEnum.dirty:
        return this.assetsService.getImgUrlTo('icon_dirty.svg');
      case HouseKeepingStatusIdEnum.mantainance:
        return this.assetsService.getImgUrlTo('icon_mantainance.svg');
      case HouseKeepingStatusIdEnum.inspection:
        return this.assetsService.getImgUrlTo('icon_inspection.svg');
      case HouseKeepingStatusIdEnum.arrangement:
        return this.assetsService.getImgUrlTo('icon_arrangement.svg');
      default:
        return this.assetsService.getImgUrlTo('icon_created.svg');
    }
  }
}
