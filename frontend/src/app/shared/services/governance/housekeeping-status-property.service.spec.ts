import { inject, TestBed } from '@angular/core/testing';

import { HousekeepingStatusPropertyService } from './housekeeping-status-property.service';

describe('HousekeepingStatusPropertyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  it('should be created', inject([HousekeepingStatusPropertyService], (service: HousekeepingStatusPropertyService) => {
    expect(service).toBeTruthy();
  }));
});
