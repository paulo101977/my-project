import { inject, TestBed } from '@angular/core/testing';

import { GovernanceStatusService } from './governance-status.service';
import { Governance } from '../../models/governance/governance';

describe('GovernanceStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  it('should be created', inject([GovernanceStatusService], (service: GovernanceStatusService) => {
    expect(service).toBeTruthy();
  }));

  it('should filter a list', inject([GovernanceStatusService], (service: GovernanceStatusService) => {
    const searchParams = 'Limpo';
    const listItens = [];
    const govStat1 = new Governance();
    govStat1.statusName = 'Limpo';
    listItens.push(govStat1);
    const govStat2 = new Governance();
    govStat2.statusName = 'Sujo';
    listItens.push(govStat2);
    expect(service.getAllGovernanceStatusBySearchData(searchParams, listItens).length).toEqual(1);
    expect(service.getAllGovernanceStatusBySearchData(searchParams, listItens)).toContain(govStat1);
  }));
});
