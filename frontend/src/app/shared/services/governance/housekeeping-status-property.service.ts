import { Injectable } from '@angular/core';
import { HousekeepingStatusProperty } from '../../models/dto/housekeeping/housekeeping-status-property-dto';

@Injectable({ providedIn: 'root' })
export class HousekeepingStatusPropertyService {
  constructor() {}

  public getAllFilterBySearchData(searchData: string, allItems: Array<HousekeepingStatusProperty>): Array<HousekeepingStatusProperty> {
    return allItems.filter(item => {
      if (
        item.housekeepingStatusName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.roomTypeName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.statusRoom.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.roomNumber.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1
      ) {
        return item;
      }
    });
  }
}
