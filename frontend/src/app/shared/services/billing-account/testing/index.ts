import { createServiceStub } from '../../../../../../testing';
import { BillingAccountService } from 'app/shared/services/billing-account/billing-account.service';
import { BillingAccountCard } from 'app/shared/models/billing-account/billing-account-card';
import { CompanyClientAccount } from 'app/shared/models/dto/company-client-account-dto';
import { GuestAccount } from 'app/shared/models/guest-account';
import { BillingAccountWithAccount } from 'app/shared/models/billing-account/billing-account-with-account';
import { BillingAccountForClosure } from 'app/shared/models/billing-account/billing-account-for-closure';
import { GetAllBillingAccountForClosure } from 'app/shared/models/billing-account/get-all-billing-account-for-closure';
import { GetAllBillingAccount } from 'app/shared/models/billing-account/get-all-billing-account';
import { BillingAccountSidebar } from 'app/shared/models/billing-account/billing-account-sidebar';
import { BillingAccountItemEntry } from 'app/shared/models/billing-account/billing-account-item-entry';
import { FiscalDocumentsService } from 'app/shared/services/billing-account/fiscal-documents.service';
import { Observable, of } from 'rxjs';

export const billingAccountServiceStub = createServiceStub(BillingAccountService, {
    billingAccountCardIsGroupAccount(billingAccountCardList: Array<BillingAccountCard>, billingAccountId: string): boolean {
        return true;
    },

    getAllClientAccountBySearchData(searchData: string, allItems: Array<CompanyClientAccount>): Array<CompanyClientAccount> {
        return [];
    },

    getAllGuestAccountBySearchData(searchData: string, allItems: Array<GuestAccount>): Array<GuestAccount> {
        return [];
    },

    getBillingAccountOpenForClosureList(
        billingAccountWithAccount: BillingAccountWithAccount, billingAccountList: Array<BillingAccountForClosure>
    ): Array<BillingAccountForClosure> {
        return [];
    },

    hasAccountsToClose(billingAccoutForClousureList: GetAllBillingAccountForClosure): boolean {
        return true;
    },

    getBillingAccountBySearchData(searchData: string, allItems: Array<GetAllBillingAccount>): Array<GetAllBillingAccount> {
        return [];
    },

    isCloseAndOk(sideBar: BillingAccountSidebar): boolean {
        return true;
    },

    showActionsButtonForCreditWithChildrensItems(billingAccountEntry: BillingAccountItemEntry): boolean {
        return true;
    },

    showActionsButtonForCreditWithoutChildrensItems(billingAccountEntry: BillingAccountItemEntry): boolean {
        return true;
    },

    showActionsButtonForDebitWithChildrensItems(billingAccountEntry: BillingAccountItemEntry): boolean {
        return true;
    },

    showActionsButtonForDebitWithoutChildrensItems(billingAccountEntry: BillingAccountItemEntry): boolean {
        return true;
    },

    showActionsButtonForSplitPayment(billingAccountEntry: BillingAccountItemEntry): string {
        return '';
    },

    showActionsButtonForTransferredPayment(billingAccountEntry: BillingAccountItemEntry): string {
        return '';
    },

    showIconRpsWarnings(sideBar: BillingAccountSidebar): boolean {
        return true;
    }
});

export const fiscalDocumentsServiceStub = createServiceStub(FiscalDocumentsService, {
    getIconOfFiscalDocument(status: number): string {
        return '';
    },

    getNfse(id: string): Observable<Blob> {
        return of(null);
    },

    getStatusColorOfFiscalDocument(status: number): string {
        return '';
    },

    getStatusStringOfFiscalDocument(status: number): string {
        return '';
    },

    isError(status: number): boolean {
        return true;
    },

    isPending(status: number): boolean {
        return true;
    }
});
