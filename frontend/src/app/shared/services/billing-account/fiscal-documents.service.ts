import { Injectable } from '@angular/core';
import { NfStatusEnum } from '../../models/dto/nf/nf-status-enum';
import { HttpService } from '../../../core/services/http/http.service';
import { ResponseContentType } from '@angular/http';
import { map } from 'rxjs/operators';
import { ContentType } from '@angular/http/src/enums';
import { Proforma } from '@app/shared/models/proforma';

@Injectable({ providedIn: 'root' })
export class FiscalDocumentsService {
  constructor(private httpClient: HttpService) {
  }

  public getStatusStringOfFiscalDocument(status: number): string {
    switch (status) {
      case NfStatusEnum.Error:
        return 'label.nfeError';
      case NfStatusEnum.Pending:
        return 'label.nf2Emitting';
      case NfStatusEnum.Issued:
        return 'label.nfeSuccess';
      case NfStatusEnum.canceling:
        return 'label.canceling';
      case NfStatusEnum.canceled:
        return 'label.canceled';
    }
  }

  public getStatusColorOfFiscalDocument(status: number): string {
    switch (status) {
      case NfStatusEnum.Error:
        return 'thf-document-status-error';
      case NfStatusEnum.Pending:
        return 'thf-document-status-emitting';
      case NfStatusEnum.Issued:
        return 'thf-document-status-success';
      case NfStatusEnum.canceling:
      case NfStatusEnum.canceled:
        return 'thf-document-status-error';
    }
  }

  public getIconOfFiscalDocument(status: number): string {
    switch (status) {
      case NfStatusEnum.Error:
        return 'icon-cancel.svg';
      case NfStatusEnum.Pending:
        return 'icon-spinner.svg';
      case NfStatusEnum.Issued:
        return 'icon-complete.svg';
      case NfStatusEnum.canceling:
      case NfStatusEnum.canceled:
        return 'icon-cancel.svg';
    }
  }

  public isPending(status: number) {
    return status == NfStatusEnum.Pending;
  }

  public isError(status: number) {
    return status == NfStatusEnum.Error;
  }

  public getNfse(id: string) {
    return this.httpClient
      .get(`billinginvoiceproperty/pdf/${id}`, { responseType: 'blob' });
  }

  public getProforma(proforma: Proforma) {
    return this.httpClient
      .get(`billinginvoiceproperty/proforma/pdf/${proforma.reservationId}/${proforma.reservationItemId}`, { responseType: 'blob' });
  }
}
