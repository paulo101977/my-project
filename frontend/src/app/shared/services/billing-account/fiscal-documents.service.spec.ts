import { inject, TestBed } from '@angular/core/testing';

import { FiscalDocumentsService } from './fiscal-documents.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('FiscalDocumentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [ InterceptorHandlerService ],
    });
  });

  it('should be created', inject([FiscalDocumentsService], (service: FiscalDocumentsService) => {
    expect(service).toBeTruthy();
  }));
});
