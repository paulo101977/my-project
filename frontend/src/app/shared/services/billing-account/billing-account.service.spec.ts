import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { GetAllBillingAccountData } from './../../mock-data/get-all-billing-account-data';
import { GetAllBillingAccount } from './../../models/billing-account/get-all-billing-account';
import { BillingAccountService } from './billing-account.service';

describe('BillingAccountService', () => {
  let service: BillingAccountService;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [],
    });

    service = TestBed.get(BillingAccountService);
  });

  it('should getBillingAccountBySearchData', () => {
    const allItems = new Array<GetAllBillingAccount>();
    allItems.push(GetAllBillingAccountData);

    let searchDataGuestName = 'João';
    expect(service.getBillingAccountBySearchData(searchDataGuestName, allItems)).toEqual([GetAllBillingAccountData]);

    searchDataGuestName = 'Juberto';
    expect(service.getBillingAccountBySearchData(searchDataGuestName, allItems)).toEqual([]);

    let searchDataReservationCode = 'UIT';
    expect(service.getBillingAccountBySearchData(searchDataReservationCode, allItems)).toEqual([GetAllBillingAccountData]);

    searchDataReservationCode = 'kkk';
    expect(service.getBillingAccountBySearchData(searchDataReservationCode, allItems)).toEqual([]);

    let searchDataRoomNumber = '20';
    expect(service.getBillingAccountBySearchData(searchDataRoomNumber, allItems)).toEqual([GetAllBillingAccountData]);

    searchDataRoomNumber = '206k';
    expect(service.getBillingAccountBySearchData(searchDataRoomNumber, allItems)).toEqual([]);
  });
});
