import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { GetAllBillingAccount } from './../../models/billing-account/get-all-billing-account';
import { CompanyClientAccount } from './../../models/dto/company-client-account-dto';
import { GuestAccount } from './../../models/guest-account';
import { BillingAccountItemEntry } from '../../models/billing-account/billing-account-item-entry';
import { BillingAccountItemTypeEnum } from '../../../shared/models/billing-account/billing-account-item-type-enum';
import { BillingAccountForClosure } from '../../../shared/models/billing-account/billing-account-for-closure';
import { BillingAccountTypeEnum } from '../../../shared/models/billing-account/billing-account-type-enum';
import { BillingAccountWithAccount } from '../../../shared/models/billing-account/billing-account-with-account';
import { BillingAccountStatusEnum } from '../../../shared/models/billing-account/billing-account-status-enum';
import { BillingAccountCard } from './../../models/billing-account/billing-account-card';
import { GetAllBillingAccountForClosure } from '../../models/billing-account/get-all-billing-account-for-closure';
import { BillingAccountSidebar } from '../../models/billing-account/billing-account-sidebar';

@Injectable({ providedIn: 'root' })
export class BillingAccountService {
  public getBillingAccountBySearchData(searchData: string, allItems: Array<GetAllBillingAccount>): Array<GetAllBillingAccount> {
    return allItems.filter(
      item =>
        (item.reservationCode && item.reservationCode.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1) ||
        (item.accountTypeName && item.accountTypeName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1) ||
        (item.roomNumber &&
          item.roomNumber
            .toString()
            .toLocaleLowerCase()
            .search(searchData.toLocaleLowerCase()) > -1) ||
        (item.holderName &&
          item.holderName
            .toString()
            .toLocaleLowerCase()
            .search(searchData.toLocaleLowerCase()) > -1) ||
        (item.billingAccountName &&
          item.billingAccountName
            .toString()
            .toLocaleLowerCase()
            .search(searchData.toLocaleLowerCase()) > -1),
    );
  }

  public getAllClientAccountBySearchData(searchData: string, allItems: Array<CompanyClientAccount>): Array<CompanyClientAccount> {
    return allItems.filter(
      item =>
        (item.reservationCode && item.reservationCode.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1) ||
        (item.companyClientName && item.companyClientName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1),
    );
  }

  public getAllGuestAccountBySearchData(searchData: string, allItems: Array<GuestAccount>): Array<GuestAccount> {
    return allItems.filter(
      item =>
        (item.guestName && item.guestName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1) ||
        (item.reservationCode && item.reservationCode.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1),
    );
  }

  public showActionsButtonForDebitWithChildrensItems(billingAccountEntry: BillingAccountItemEntry) {
    return (
      billingAccountEntry.billingAccountItemTypeId == BillingAccountItemTypeEnum.Debit &&
      this.hasBillingAccountChildrenItems(billingAccountEntry)
    );
  }

  public showActionsButtonForDebitWithoutChildrensItems(billingAccountEntry: BillingAccountItemEntry) {
    return (
      billingAccountEntry.billingAccountItemTypeId == BillingAccountItemTypeEnum.Debit &&
      !this.hasBillingAccountChildrenItems(billingAccountEntry)
    );
  }

  public showActionsButtonForCreditWithChildrensItems(billingAccountEntry: BillingAccountItemEntry) {
    return (
      billingAccountEntry.billingAccountItemTypeId == BillingAccountItemTypeEnum.Credit &&
      this.hasBillingAccountChildrenItems(billingAccountEntry)
    );
  }

  public showActionsButtonForCreditWithoutChildrensItems(billingAccountEntry: BillingAccountItemEntry) {
    return (
      billingAccountEntry.billingAccountItemTypeId == BillingAccountItemTypeEnum.Credit &&
      !this.hasBillingAccountChildrenItems(billingAccountEntry)
    );
  }

  public showActionsButtonForSplitPayment(billingAccountEntry: BillingAccountItemEntry) {
    return '';
  }

  public showActionsButtonForTransferredPayment(billingAccountEntry: BillingAccountItemEntry) {
    return '';
  }

  private hasBillingAccountChildrenItems(billingAccountEntry: BillingAccountItemEntry) {
    if (billingAccountEntry.billingAccountChildrenItems != null) {
      return billingAccountEntry.billingAccountChildrenItems.length >= 1;
    } else {
      return false;
    }
  }

  public getBillingAccountOpenForClosureList(
    billingAccountWithAccount: BillingAccountWithAccount,
    billingAccountList: Array<BillingAccountForClosure>,
  ): Array<BillingAccountForClosure> {
    let billingAccountOpenForClosureList = [];
    if (billingAccountWithAccount.accountTypeId == BillingAccountTypeEnum.GroupAccount) {
      billingAccountOpenForClosureList = billingAccountList.filter(b => b.statusId == BillingAccountStatusEnum.Open);
    } else {
      billingAccountOpenForClosureList = billingAccountList.filter(
        b => b.statusId == BillingAccountStatusEnum.Open && b.billingAccountTypeId != BillingAccountTypeEnum.GroupAccount,
      );
    }
    billingAccountOpenForClosureList = _.orderBy(billingAccountOpenForClosureList, ['ownerId', 'ownerName'], ['asc', 'asc']);
    return billingAccountOpenForClosureList;
  }

  public billingAccountCardIsGroupAccount(billingAccountCardList: Array<BillingAccountCard>, billingAccountId: string): boolean {
    const billingAccountCard = billingAccountCardList.find(b => b.billingAccountSidebar.billingAccountId == billingAccountId);
    if (billingAccountCard && billingAccountCard.billingAccountSidebar) {
      return billingAccountCard.billingAccountSidebar.accountTypeId == BillingAccountTypeEnum.GroupAccount;
    } else {
      return false;
    }
  }

  public hasAccountsToClose(billingAccoutForClousureList: GetAllBillingAccountForClosure) {
    return (
      billingAccoutForClousureList.hasOwnProperty('billingAccountForClosureList') &&
      billingAccoutForClousureList.billingAccountForClosureList &&
      billingAccoutForClousureList.billingAccountForClosureList.length > 1
    );
  }

  public showIconRpsWarnings(sideBar: BillingAccountSidebar) {
    return (
      sideBar.isClosed &&
      (sideBar.billingInvoicePendingStatusId > 0 || sideBar.billingInvoiceErrorStatusId > 0 || sideBar.billingInvoiceIssuedStatusId > 0)
    );
  }

  public isCloseAndOk(sideBar: BillingAccountSidebar) {
    return (
      sideBar.isClosed &&
      (sideBar.billingInvoicePendingStatusId == 0 && sideBar.billingInvoiceErrorStatusId == 0 && sideBar.billingInvoiceIssuedStatusId == 0)
    );
  }
}
