import { createServiceStub } from '../../../../../../testing';
import { CheckinService } from 'app/shared/services/checkin/checkin.service';
import { CheckInNumUhsDto } from 'app/shared/models/dto/checkin/checkin-num-uhs-dto';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { FnrhSaveDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';

export const checkinServiceStub = createServiceStub(CheckinService, {
   nextCardGuest(lastGuestId: number) {
   },

    uhChosen(uh: CheckInNumUhsDto) {
    },

    checkIfIsChild(propertyId, age: number): boolean {
       return true;
    },

    fnrhListValid(guestCardList: Array<GuestCard>): void {
    },
    getAdultsAvailableSlots(adultCapacity: number, adultCount: number): number {
        return 0;
    },
    getChildrenAvailableSlots(childCapacity: number, childCount: number): number {
        return 0;
    },
    guestReservationList(array: FnrhSaveDto): void {
    },
    hasAdultsAvailableSlots(adultCapacity: number, adultCount: number): boolean {
        return false;
    },
    hasChildrenAvailableSlots(childCapacity: number, childCount: number): boolean {
        return false;
    }
});
