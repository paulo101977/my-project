import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { CheckInNumUhsDto } from './../../models/dto/checkin/checkin-num-uhs-dto';
import { CheckinService } from './checkin.service';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('CheckinService', () => {
  let service: CheckinService;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientModule, RouterTestingModule],
      providers: [ InterceptorHandlerService ],
    });

    service = TestBed.get(CheckinService);
  });

  it('should nextCardGuest', () => {
    spyOn(service['emitChangeSource'], 'next');
    service.nextCardGuest(1);
    expect(service['emitChangeSource'].next).toHaveBeenCalledWith(1);
  });

  it('should uhChosen when roomNumber is number', () => {
    spyOn(service['emitChangeUhChosen'], 'next');
    const uh = new CheckInNumUhsDto();
    uh.roomNumber = '10';

    service.uhChosen(uh);

    expect(service['emitChangeUhChosen'].next).toHaveBeenCalledWith(true);
  });

  it('should uhChosen when roomNumber is not number', () => {
    spyOn(service['emitChangeUhChosen'], 'next');
    const uh = new CheckInNumUhsDto();
    uh.roomNumber = '10a';

    service.uhChosen(uh);

    expect(service['emitChangeUhChosen'].next).toHaveBeenCalledWith(true);
  });

  it('should not uhChosen', () => {
    spyOn(service['emitChangeUhChosen'], 'next');
    const uh = new CheckInNumUhsDto();
    service.uhChosen(uh);

    expect(service['emitChangeUhChosen'].next).toHaveBeenCalledWith(false);
  });

  xit('should fnrhListValid', () => {
    spyOn(service['emitChangeFnrhList'], 'next');
    const guestCard = new GuestCard();
    guestCard.isValid = true;
    const arrayGuestCard = new Array<GuestCard>(guestCard);

    service.fnrhListValid(arrayGuestCard);

    expect(service['emitChangeFnrhList'].next).toHaveBeenCalledWith(true);
  });

  it('should not fnrhListValid', () => {
    spyOn(service['emitChangeFnrhList'], 'next');
    const guestCard = new GuestCard();
    guestCard.isValid = false;
    const arrayGuestCard = new Array<GuestCard>(guestCard);

    service.fnrhListValid(arrayGuestCard);

    expect(service['emitChangeFnrhList'].next).toHaveBeenCalledWith(false);
  });
});
