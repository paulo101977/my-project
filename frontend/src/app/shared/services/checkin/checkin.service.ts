import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { GuestCard } from 'app/shared/models/checkin/guest-card';
import { CheckInNumUhsDto } from './../../models/dto/checkin/checkin-num-uhs-dto';
import { CommomService } from '../shared/commom.service';
import {FnrhSaveDto} from 'app/shared/models/dto/checkin/fnrh-save-dto';

@Injectable({ providedIn: 'root' })
export class CheckinService {
  private STATUS_CHECKIN = 2;

  private emitChangeSource = new Subject<any>();
  changeEmitted$ = this.emitChangeSource.asObservable();

  private emitChangeUhChosen = new Subject<any>();
  uhChosenChangeEmitted$ = this.emitChangeUhChosen.asObservable();

  private emitChangeFnrhList = new Subject<any>();
  fnrhListChangeEmitted$ = this.emitChangeFnrhList.asObservable();

  private emitGuestReservationList = new Subject<any>();
  guestReservationListEmitted$ = this.emitGuestReservationList.asObservable();

  constructor( private commonService: CommomService) {}

  public nextCardGuest(lastGuestId: number) {
    this.emitChangeSource.next(lastGuestId);
  }

  public uhChosen(uh: CheckInNumUhsDto) {
    if (uh && uh.roomNumber) {
      this.emitChangeUhChosen.next(true);
    } else {
      this.emitChangeUhChosen.next(false);
    }
  }

  public fnrhListValid(guestCardList: Array<GuestCard>) {
    let hasValid = false;
    if (guestCardList) {
      hasValid = guestCardList.some(card => card.isValid && card.guest.guestStatusId != this.STATUS_CHECKIN);
    }
    this.emitChangeFnrhList.next(hasValid);
  }

  public guestReservationList(array: FnrhSaveDto) {
    this.emitGuestReservationList.next(array);
  }

  public getAdultsAvailableSlots(adultCapacity: number, adultCount: number): number {
    if (adultCount <= adultCapacity) {
      return adultCapacity - adultCount;
    }
    return 0;
  }

  public getChildrenAvailableSlots(childCapacity: number, childCount: number): number {
    if (childCount <= childCapacity) {
      return childCapacity - childCount;
    }
    return 0;
  }

  public hasAdultsAvailableSlots(adultCapacity: number, adultCount: number): boolean {
    return this.getAdultsAvailableSlots(adultCapacity, adultCount) > 0;
  }

  public hasChildrenAvailableSlots(childCapacity: number, childCount: number): boolean {
    return this.getChildrenAvailableSlots(childCapacity, childCount) > 0;
  }

  public checkIfIsChild(propertyId, age: number): boolean {
    const isTeenager = this.commonService.isAdultBasedOnChildParameters(propertyId, age);
    return !this.commonService.isAdult(age) && !isTeenager;
  }
}
