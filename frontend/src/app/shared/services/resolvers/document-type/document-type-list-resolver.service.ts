import { Injectable } from '@angular/core';
import { ActivatedRoute, Resolve, Router } from '@angular/router';
import { Observable, pipe } from 'rxjs';


import { DocumentType } from 'app/shared/models/document-type';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { DocumentTypeResource } from 'app/shared/resources/document-type/document-type.resource';
import { map, take } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class DocumentTypeListResolver implements Resolve<ItemsResponse<DocumentType>> {
  constructor(private documentTypeResource: DocumentTypeResource, private router: Router, private route: ActivatedRoute) {}

  resolve(): Observable<ItemsResponse<DocumentType>> {
    return this.documentTypeResource
      .getDocumentTypesByPersonTypeAndCountryCode('n', 'br')
      .pipe(
        take(1),
        map(response => {
          if (response) {
            return response;
          } else {
            this.route.params.subscribe(params => this.router.navigate(['p', params.property, 'account-type']));
            return null;
          }
        })
      );
  }
}
