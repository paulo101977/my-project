import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TestBed } from '@angular/core/testing';
import { ClientInfoData } from '../../mock-data/client-data';
import { GetAllCompanyClientResultDto } from '../../models/dto/client/get-all-company-client-result-dto';
import { ClientDocumentTypeEnum } from '../../models/dto/client/client-document-types';
import { ClientDto } from '../../models/dto/client/client-dto';
import { ClientService } from './client.service';

describe('ClientService', () => {
  let service: ClientService;
  const formBuilder = new FormBuilder();

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      providers: [],
    });

    service = TestBed.get(ClientService);
  });

  describe('on init ClientService', () => {
    it('should create service', () => {
      expect(service).toBeTruthy();
    });

    it('should parsingDataEndtoSave', () => {
      const dataSend = <ClientDto>{
        personId: '',
        propertyId: 0,
        companyId: 1,
        personType: ClientDocumentTypeEnum.Legal,
        shortName: '',
        tradeName: '',
        isActive: false,
        email: '',
        homePage: '',
        phoneNumber: '',
        cellPhoneNumber: '',
        propertyCompanyClientCategoryId: '',
        locationList: [],
        documentList: [],
        companyClientContactPersonList: [],
      };

      const formGroup = formBuilder.group({
        personType: ClientDocumentTypeEnum.Legal,
        shortName: '',
        tradeName: '',
        isActive: false,
        email: '',
        homePage: '',
        phoneNumber: '',
        cellPhoneNumber: '',
        group: '',
        propertyCompanyClientCategoryId: '',
        propertyId: '',
        companyId: '',
        personId: '',
      });

      const mapEnd = service.parsingDataEndtoSave(dataSend, formGroup.value);

      expect(mapEnd.personType).toEqual(ClientDocumentTypeEnum.Legal);
      expect(mapEnd.shortName).toEqual('');
      expect(mapEnd.tradeName).toEqual('');
      expect(mapEnd.isActive).toBeFalsy();
      expect(mapEnd.email).toEqual('');
      expect(mapEnd.homePage).toEqual('');
      expect(mapEnd.phoneNumber).toEqual('');
      expect(mapEnd.cellPhoneNumber).toEqual('');
      expect(mapEnd.propertyCompanyClientCategoryId).toEqual('');
    });

    it('should getBySearchData to filter list', () => {
      const clients = new Array<GetAllCompanyClientResultDto>();
      clients.push(ClientInfoData);

      const response = service.getBySearchData('Cliente Teste', clients);

      expect(response[0]).toEqual(ClientInfoData);
      expect(response.length).toEqual(1);
    });
  });
});
