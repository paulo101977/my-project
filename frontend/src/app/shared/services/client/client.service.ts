import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { ClientDto } from './../../models/dto/client/client-dto';
import { GetAllCompanyClientResultDto } from 'app/shared/models/dto/client/get-all-company-client-result-dto';
import { ClientDocumentTypeEnum } from '../../models/dto/client/client-document-types';

@Injectable({ providedIn: 'root' })
export class ClientService {
  private isNaturalPerson(personType) {
    return personType === undefined ? true : personType === ClientDocumentTypeEnum.Natural;
  }

  public parsingDataEndtoSave(dataSend: ClientDto, formDataGroup): ClientDto {
    if (!dataSend) {
      dataSend = new ClientDto();
    }

    const clientBasicData = [
      'companyId',
      'personId',
      'propertyId',
      'personType',
      'shortName',
      'tradeName',
      'email',
      'homePage',
      'externalCode',
      'phoneNumber',
      'cellPhoneNumber',
      'isActive',
      'propertyCompanyClientCategoryId',
    ];

    _.map(clientBasicData, item => {
      dataSend[item] = formDataGroup[item];
    });

    if (this.isNaturalPerson(formDataGroup['personType'])) {
      dataSend['shortName'] = dataSend['tradeName'];
    }

    _.map(dataSend['locationList'], locationItem => {
      locationItem['browserLanguage'] = window.navigator.language;
    });

    return dataSend;
  }

  public getBySearchData(searchData: string, allItems: Array<GetAllCompanyClientResultDto>): Array<GetAllCompanyClientResultDto> {
    return allItems.filter(
      item =>
        item.name.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.document.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1,
    );
  }
}
