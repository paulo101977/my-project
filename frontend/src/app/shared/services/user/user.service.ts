import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { Subject } from 'rxjs/index';

@Injectable({providedIn: 'root'})
export class UserService {
  private readonly USER: string = 'pms:user';

  private userLocalStorageChange = new Subject<User>();
  userLocalStorageChanged$ = this.userLocalStorageChange.asObservable();

  public getUserLocalStorage(): User {
    return JSON.parse(localStorage.getItem(this.USER));
  }

  public updateUserLocalStorage(user: User): void {
    localStorage.setItem(this.USER, JSON.stringify(user));
  }

  public isAdmin() {
    // TODO remove this when roles are full implemented
    return true;
  }

  public getUserpreferredLanguage(): string {
    const user = this.getUserLocalStorage();
    if (user) {
      return user.preferredLanguage;
    }
    return '';
  }

  public getUserpreferredCulture(): string {
    const user = this.getUserLocalStorage();
    if (user) {
      return user.preferredCulture;
    }
    return '';
  }

  public storeUserInLocalStorage(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
    this.userLocalStorageChange.next(user);
  }
}
