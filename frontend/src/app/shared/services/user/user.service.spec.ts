import { TestBed } from '@angular/core/testing';

import { getUserData } from '../../mock-data/user.data';
import { UserService } from './user.service';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { User } from '../../models/user';

describe('UserService', () => {
  let service: UserService;

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: []
      });

    service = TestBed.get(UserService);

    const store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      }
    };

    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should getUserLocalStorage', () => {
    localStorage.setItem(service['USER'], JSON.stringify(getUserData()));
    expect(service.getUserLocalStorage()).toEqual(getUserData());
  });

  it('should updateUserLocalStorage', () => {
    const newUser: User = getUserData();
    newUser.name = 'new user';

    service.updateUserLocalStorage(newUser);

    expect(JSON.parse(localStorage.getItem(service['USER']))).toEqual(newUser);
  });

});
