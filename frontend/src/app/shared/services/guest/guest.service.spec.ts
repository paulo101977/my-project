import { TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { HttpClientModule } from '@angular/common/http';
import { Guest, GuestState } from './../../models/reserves/guest';
import { ReservationItemView } from './../../models/reserves/reservation-item-view';
import { ReservationItemGuestView } from './../../models/reserves/reservation-item-guest-view';
import { GuestReservationItem } from './../../models/reserves/guest-reservation-item';
import { Room } from './../../models/room';
import { DocumentForLegalAndNaturalPerson } from './../../models/document';
import { DocumentType } from './../../models/document-type';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { SearchPersonsResultDto } from './../../models/person/search-persons-result-dto';
import { GuestAccountData } from './../../mock-data/guest-account-deta';
import { GuestAccount } from './../../models/guest-account';
import { GuestService } from './guest.service';
import { GuestCardRoomlist } from '../../models/guest-card-roomlist';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('GuestService', () => {
  let service: GuestService;
  let formBuilder: FormBuilder;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [
        HttpClientModule,
        ReactiveFormsModule,
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      providers: [ InterceptorHandlerService ],
    });

    formBuilder = TestBed.get(FormBuilder);
    service = TestBed.get(GuestService);
  });

  describe('on guest', () => {
    it('should set guest input to link default', () => {
      const guest = new Guest();
      guest.state = GuestState.NoGuest;
      guest.formGuest = formBuilder.group({
        guestName: [''],
      });

      expect(service.getGuestState(guest)).toEqual(GuestState.NoGuest);
    });

    it('should set guest input to link with guest name', () => {
      const guest = new Guest();
      guest.state = GuestState.NoGuest;
      guest.formGuest = formBuilder.group({
        guestName: ['Hudson Gomes Nunes'],
      });

      expect(service.getGuestState(guest)).toEqual(GuestState.GuestCreated);
    });

    it('should set guest link default to input', () => {
      const guest = new Guest();
      guest.state = GuestState.NoGuest;
      guest.formGuest = formBuilder.group({
        guestName: [''],
      });

      expect(service.getGuestStateCreatingGuest()).toEqual(GuestState.CreatingGuest);
    });

    it('should guestNameIsEmpty', () => {
      const guest = new Guest();
      guest.formGuest = formBuilder.group({
        guestName: [''],
      });
      const data = new ReservationItemView();
      data.guestQuantity = new Array<Guest>();
      data.guestQuantity.push(guest);

      expect(service.guestNameIsEmpty(data)).toBeTruthy();
    });

    it('should guestNameIsEmpty', () => {
      const guest = new Guest();
      guest.formGuest = formBuilder.group({
        guestName: ['Hudson Nunes'],
      });
      const data = new ReservationItemView();
      data.guestQuantity = new Array<Guest>();
      data.guestQuantity.push(guest);

      expect(service.guestNameIsEmpty(data)).toBeFalsy();
    });

    it('should is child', () => {
      const guest: GuestCardRoomlist = new GuestCardRoomlist();
      guest.isChild = true;
      expect(service.isChild(guest)).toBeTruthy();
    });

    it('should getAdultGuestDefaultValues', () => {
      const guest = new Guest();
      guest.state = GuestState.NoGuest;
      guest.formGuest = formBuilder.group({
        guestName: [''],
      });
      guest.isChild = false;

      expect(service.getAdultGuestDefaultValues().state).toEqual(guest.state);
      expect(service.getAdultGuestDefaultValues().isChild).toEqual(guest.isChild);
      expect(service.getAdultGuestDefaultValues().formGuest.get('guestName').value).toEqual(guest.formGuest.get('guestName').value);
    });

    it('should getChildGuestDefaultValues', () => {
      const guest = new Guest();
      guest.state = GuestState.NoGuest;
      guest.formGuest = formBuilder.group({
        guestName: [''],
      });
      guest.isChild = true;

      expect(service.getChildGuestDefaultValues().state).toEqual(guest.state);
      expect(service.getChildGuestDefaultValues().isChild).toEqual(guest.isChild);
      expect(service.getChildGuestDefaultValues().formGuest.get('guestName').value).toEqual(guest.formGuest.get('guestName').value);
    });
  });

  describe('on guest form', () => {
    it('should change status guestCard to GuestCreating after editGuest', () => {
      const guest: GuestCardRoomlist = new GuestCardRoomlist();
      guest.state = GuestState.GuestCreated;
      guest.lastState = GuestState.GuestCreated;

      service.setEditGuestForm(guest);

      expect(guest.state).toEqual(GuestState.CreatingGuest);
      expect(guest.lastState).toEqual(GuestState.GuestCreated);
    });

    it('should change status guestCard to GuestCreated after saveGuest', () => {
      const periodOfStay = {
        beginJsDate: new Date('2017/08/01'),
        endJsDate: new Date('2017/08/10'),
      };

      const guestForm = formBuilder.group({
        status: GuestState.CreatingGuest,
        lastStatus: GuestState.GuestCreated,
        periodOfStay: '',
        guestReservationItemId: '',
        reservationItemId: '',
        guestId: '',
        personId: '',
        guestName: '',
        guestEmail: '',
        guestDocument: '',
        guestDocumentTypeId: '',
        checkinDate: '',
        checkoutDate: '',
        estimatedArrivalDate: '',
        estimatedDepartureDate: '',
        preferredName: '',
        guestCitizenship: '',
        language: '',
        pctDailyRate: '',
        isIncognito: '',
        isChild: '',
        childAge: '',
        isMain: '',
        type: '',
        guestStatusId: '',
      });
      const guest: GuestCardRoomlist = new GuestCardRoomlist();

      service.setSaveGuestForm(guest, guestForm);

      expect(guest.state).toEqual(GuestState.GuestCreated);
      expect(guest.lastState).toEqual(GuestState.CreatingGuest);
    });

    it('should change status guestCard to LastStatus after cancelEdit', () => {
      const guest: GuestCardRoomlist = new GuestCardRoomlist();
      guest.state = GuestState.CreatingGuest;
      guest.lastState = GuestState.NoGuest;

      service.cancelEditForm(guest);

      expect(guest.state).toEqual(GuestState.NoGuest);
      expect(guest.lastState).toEqual(GuestState.CreatingGuest);
    });

    it('should change showDetails', () => {
      const guest: GuestCardRoomlist = new GuestCardRoomlist();
      guest.showDetails = false;

      service.toogleShowDetailsForm(guest);

      expect(guest.showDetails).toBeTruthy();
    });

    it('should clean guestForm', () => {
      const documentType = new DocumentType();
      documentType.name = 'CPF';
      const document = new DocumentForLegalAndNaturalPerson();
      document.documentType = documentType;

      const guest: GuestCardRoomlist = new GuestCardRoomlist();
      guest.state = GuestState.GuestCreated;
      guest.lastState = GuestState.CreatingGuest;
      guest.showDetails = false;
      guest.guestName = 'Tem algo aqui';
      guest.preferredName = 'Tem algo aqui';
      guest.childAge = 'Tem algo aqui';
      guest.guestEmail = 'Tem algo aqui';
      guest.isIncognito = true;
      guest.guestCitizenship = 'Tem algo aqui';
      guest.language = 'Tem algo aqui';
      guest.uHPreferably = 'Tem algo aqui';
      guest.lastUH = 'Tem algo aqui';
      guest.lastDailyValue = 100;
      guest.pctDailyRate = 100;
      guest.remarks = 'Tem algo aqui';
      guest.guestDocument = document;
      guest.guestDocumentTypeId = 'Tem algo aqui';
      guest.completeAddress = 'Tem algo aqui';

      service.setCleanGuestForm(guest);

      expect(guest.state).toEqual(GuestState.NoGuest);
      expect(guest.lastState).toEqual(GuestState.GuestCreated);
      expect(guest.showDetails).toEqual(true);
      expect(guest.guestName).toEqual('');
      expect(guest.preferredName).toEqual('');
      expect(guest.childAge).toEqual('');
      expect(guest.guestEmail).toEqual('');
      expect(guest.isIncognito).toEqual(false);
      expect(guest.guestCitizenship).toEqual('');
      expect(guest.language).toEqual('');
      expect(guest.uHPreferably).toEqual('');
      expect(guest.lastUH).toEqual('');
      expect(guest.lastDailyValue).toEqual(0);
      expect(guest.pctDailyRate).toEqual(0);
      expect(guest.remarks).toEqual('');
      expect(guest.guestDocument).toEqual(null);
      expect(guest.guestDocumentTypeId).toEqual('');
      expect(guest.completeAddress).toEqual('');
    });

    it('should add reservationItem to ReservationItemFormArray', () => {
      const reservationItemGuestView = new ReservationItemGuestView();
      reservationItemGuestView.guestReservationItemList = [];
      reservationItemGuestView.estimatedArrivalDate = new Date('2017/08/01').toString();
      reservationItemGuestView.estimatedDepartureDate = new Date('2017/08/10').toString();
      const reservationItemGuestView2 = new ReservationItemGuestView();
      reservationItemGuestView2.guestReservationItemList = [];
      reservationItemGuestView2.estimatedArrivalDate = new Date('2017/08/01').toString();
      reservationItemGuestView2.estimatedDepartureDate = new Date('2017/08/10').toString();

      const form = formBuilder.group({
        reservationItems: formBuilder.array([]),
      });

      service.addReservationItemToForm(form, reservationItemGuestView);
      service.addReservationItemToForm(form, reservationItemGuestView2);

      expect(form.get('reservationItems').value.length).toEqual(2);
    });

    it('should add guest to GuestFormArray', () => {
      spyOn(service, 'getDateRangePickerToGuest');

      const guest = new GuestReservationItem();
      guest.checkinDate = new Date('2017/08/01');
      guest.checkoutDate = new Date('2017/08/10');
      const guest2 = new GuestReservationItem();
      guest2.checkinDate = new Date('2017/08/01');
      guest2.checkoutDate = new Date('2017/08/10');

      const form = formBuilder.group({
        guests: formBuilder.array([]),
      });

      service.addGuestsToreservationItemForm(form, guest);
      service.addGuestsToreservationItemForm(form, guest2);

      expect(form.get('guests').value.length).toEqual(2);
      expect(service.getDateRangePickerToGuest).toHaveBeenCalled();
    });

    it('should get Date Range Limit from', () => {
      const reservationItemGuestView = new ReservationItemGuestView();
      reservationItemGuestView.estimatedArrivalDate = new Date('2017/08/01').toString();
      reservationItemGuestView.estimatedDepartureDate = new Date('2017/08/10').toString();

      const config = service.getDateRangeLimitReservationItemGuestView(reservationItemGuestView);

      expect(config.disableUntil.year).toEqual(2017);
      expect(config.disableUntil.month).toEqual(7);
      expect(config.disableUntil.day).toEqual(31);
      expect(config.disableSince.year).toEqual(2017);
      expect(config.disableSince.month).toEqual(8);
      expect(config.disableSince.day).toEqual(11);
    });

    it('should return Room from Guest', () => {
      const reservationItemGuestView = new ReservationItemGuestView();
      reservationItemGuestView.roomNumber = 1000;

      expect(service.getRoomNumberFromReservationItemGuestView(reservationItemGuestView)).toEqual('1000');
    });

    it('should return empty Room from Guest', () => {
      const room = new Room();

      const reservationItemGuestView = new ReservationItemGuestView();
      reservationItemGuestView.room = room;

      expect(service.getRoomNumberFromReservationItemGuestView(reservationItemGuestView)).toEqual('');
    });

    it('should return RoomType from Guest', () => {
      const reservationItemGuestView = new ReservationItemGuestView();
      reservationItemGuestView.roomTypeAbbreviation = 'XPTO';

      expect(service.getRoomTypeFromReservationItemGuestView(reservationItemGuestView)).toEqual('XPTO');
    });

    it('should return empty RoomType from Guest', () => {
      const roomType = new RoomType();

      const reservationItemGuestView = new ReservationItemGuestView();
      reservationItemGuestView.receivedRoomType = roomType;

      expect(service.getRoomTypeFromReservationItemGuestView(reservationItemGuestView)).toEqual('');
    });

    it('should fill Id and Name from Guest', () => {
      const person = new SearchPersonsResultDto();
      person.guestId = 2;
      person.fullName = 'Caio Régis';
      const guest = formBuilder.group({
        guestEmail: '',
        guestId: '',
        guestName: '',
        guestCitizenship: '',
        preferredName: '',
        language: '',
      });

      service['updateGuestFormPersonalData'](person, guest);

      expect(guest.get('guestId').value).toEqual(2);
      expect(guest.get('guestName').value).toEqual('Caio Régis');
    });

    it('should fill PersonalInformation from Guest', () => {
      const person = new SearchPersonsResultDto();
      const guest = formBuilder.group({
        guestCitizenship: '',
        preferredName: '',
        language: '',
      });
      const personInformation = [
        {
          type: 1,
          typeValue: 'Nationality',
          value: 'Brasileira',
        },
        {
          type: 2,
          typeValue: 'SocialName',
          value: 'Caio Régis',
        },
        {
          type: 3,
          typeValue: 'Language',
          value: 'Português',
        },
      ];
      person.personInformation = personInformation;

      service['updatePersonInformationGuestForm'](person, guest);

      expect(guest.get('guestCitizenship').value).toEqual('Brasileira');
      expect(guest.get('preferredName').value).toEqual('Caio Régis');
      expect(guest.get('language').value).toEqual('Português');
    });
  });

  describe('on Autocomplete Guests Document', () => {
    it('should get config autocomplete', () => {
      const config = service.getConfigAutocompleItemsGuestsDocument();

      expect(config['inputId']).toEqual('document');
      expect(config['paramDisplay']).toEqual('document');
      expect(config['autocompleSelectItems'].length).toEqual(3);
    });

    it('should update guests info with documents of autocomplete', () => {
      spyOn<any>(service, 'updateGuestFormPersonalData').and.callThrough();
      spyOn<any>(service, 'updatePersonInformationGuestForm');
      const person = new SearchPersonsResultDto();
      person.document = '123456789';
      person.guestId = 88889999;
      person.fullName = 'Caio Régis';
      person.email = 'test@caio.com.br';
      person.guestTypeId = 7;

      const guest = formBuilder.group({
        guestDocument: '',
        guestDocumentTypeId: '',
        guestId: '',
        guestName: '',
        guestCitizenship: '',
        preferredName: '',
        language: '',
        guestEmail: '',
        type: ''
      });

      service.updateGuestForm(person, guest);

      const { guestId, guestName, guestDocument, guestEmail, type } = guest.value;

      expect(guestName).toEqual(person.fullName);
      expect(guestDocument).toEqual(person.document);
      expect(guestId).toEqual(person.guestId);
      expect(guestEmail).toEqual(person.email);
      expect(type).toEqual(person.guestTypeId);
      expect(service['updateGuestFormPersonalData']).toHaveBeenCalledWith(person, guest);
      expect(service['updatePersonInformationGuestForm']).toHaveBeenCalledWith(person, guest);
    });

  });

  describe('on search GuestAccount', () => {
    it('should getBySearchGuestAccountData', () => {
      const allItems = new Array<GuestAccount>();
      allItems.push(GuestAccountData);
      let searchData = 'Yoseph';

      expect(service.getBySearchGuestAccountData(searchData, allItems)).toEqual([GuestAccountData]);

      searchData = 'Juberto';
      expect(service.getBySearchGuestAccountData(searchData, allItems)).toEqual([]);

      searchData = 'YYY';
      expect(service.getBySearchGuestAccountData(searchData, allItems)).toEqual([GuestAccountData]);

      searchData = 'kkk';
      expect(service.getBySearchGuestAccountData(searchData, allItems)).toEqual([]);
    });
  });
});
