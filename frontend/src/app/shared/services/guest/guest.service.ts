import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReservationItemGuestView } from './../../models/reserves/reservation-item-guest-view';
import { Guest, GuestState } from './../../models/reserves/guest';
import { ReservationItemView } from './../../models/reserves/reservation-item-view';
import { GuestReservationItem } from './../../models/reserves/guest-reservation-item';
import { DocumentType } from './../../models/document-type';
import { PersonInformationType } from './../../models/person/person-information-type-enum';
import { SearchPersonsResultDto } from './../../models/person/search-persons-result-dto';
import { GuestAccount } from './../../models/guest-account';
import { GuestCardRoomlist } from '../../models/guest-card-roomlist';
import { DateService } from './../shared/date.service';
import { NumberRangeValidator } from './../../validators/number-range-validator';

@Injectable({ providedIn: 'root' })
export class GuestService {
  constructor(private formBuilder: FormBuilder, private dateService: DateService) {}

  public getGuestState(guest: Guest) {
    const hasGuestName = guest.formGuest.value.guestName === '' || guest.formGuest.value.guestName === null;
    return hasGuestName ? GuestState.NoGuest : GuestState.GuestCreated;
  }

  public guestNameIsEmpty(data: ReservationItemView) {
    return data.guestQuantity[0].formGuest.value.guestName === '' || data.guestQuantity[0].formGuest.value.guestName === null;
  }

  public getGuestStateCreatingGuest() {
    return GuestState.CreatingGuest;
  }

  public isChild(guest: GuestCardRoomlist): boolean {
    return guest.isChild;
  }

  public getAdultGuestDefaultValues() {
    const guest = new Guest();
    guest.state = GuestState.NoGuest;
    guest.formGuest = this.formBuilder.group({
      guestName: [''],
    });
    guest.isChild = false;

    return guest;
  }

  public getChildGuestDefaultValues() {
    const guest = new Guest();
    guest.state = GuestState.NoGuest;
    guest.formGuest = this.formBuilder.group({
      guestName: [''],
    });
    guest.isChild = true;

    return guest;
  }

  public addReservationItemToForm(form: FormGroup, reservationItemGuestView: ReservationItemGuestView) {
    const reservationItemArray = <FormArray>form.controls['reservationItems'];
    if (reservationItemGuestView) {
      const reservationItemForm = this.initReservationItem(reservationItemGuestView);

      reservationItemArray.push(reservationItemForm);

      if (reservationItemGuestView.guestReservationItemList) {
        reservationItemGuestView.guestReservationItemList.forEach(guest => {
          this.addGuestsToreservationItemForm(reservationItemForm, guest);
        });
      }
    }
  }

  public initReservationItem(reservationItemGuestView: ReservationItemGuestView) {
    const configDateRangePicker = this.getDateRangeLimitReservationItemGuestView(reservationItemGuestView);
    return this.formBuilder.group({
      id: reservationItemGuestView.id,
      reservationId: reservationItemGuestView.reservationId,
      reservationItemUid: reservationItemGuestView.reservationItemUid,
      estimatedArrivalDate: reservationItemGuestView.estimatedArrivalDate,
      estimatedDepartureDate: reservationItemGuestView.estimatedDepartureDate,
      requestedRoomTypeId: reservationItemGuestView.requestedRoomTypeId,
      receivedRoomTypeId: reservationItemGuestView.receivedRoomTypeId,
      requestedRoomType: reservationItemGuestView.requestedRoomType,
      receivedRoomType: reservationItemGuestView.receivedRoomType,
      roomId: reservationItemGuestView.roomId,
      checkinDate: reservationItemGuestView.checkinDate,
      checkoutDate: reservationItemGuestView.checkoutDate,
      configDateRangePicker: configDateRangePicker,
      code: reservationItemGuestView.reservationItemCode,
      status: reservationItemGuestView.reservationItemStatus,
      statusFormatted: reservationItemGuestView.reservationItemStatusFormatted,
      adultCount: reservationItemGuestView.adultCount,
      childCount: reservationItemGuestView.childCount,
      active: false,
      roomLayoutId: reservationItemGuestView.roomLayoutId,
      extraBedCount: reservationItemGuestView.extraBedCount,
      extraCribCount: reservationItemGuestView.extraCribCount,
      reservationBudgetList: reservationItemGuestView.reservationBudgetList,
      guests: this.formBuilder.array([]),
      roomNumber: reservationItemGuestView.roomNumber,
      roomTypeName: reservationItemGuestView.roomTypeName,
      roomTypeAbbreviation: reservationItemGuestView.roomTypeAbbreviation,
      isValid: reservationItemGuestView.isValid,
      searchableNames: [reservationItemGuestView.searchableNames],
    });
  }

  public addGuestsToreservationItemForm(reservationItemForm: FormGroup, guest: GuestReservationItem) {
    const guestArray = <FormArray>reservationItemForm.controls['guests'];
    const guestForm = this.initGuest(guest);

    if (guestForm) {
      guestArray.push(guestForm);
    }
  }

  private initGuest(guest: GuestReservationItem) {
    const periodOfStay = this.getDateRangePickerToGuest(guest);
    const cardState = guest.guestName ? GuestState.GuestCreated : GuestState.NoGuest;

    return this.formBuilder.group({
      guestReservationItemId: guest.guestReservationItemId,
      reservationItemId: guest.reservationItemId,
      guestId: guest.guestId,
      personId: guest.personId,
      guestName: [guest.guestName, [Validators.required]],
      preferredName: guest.preferredName,
      childAge: [guest.childAge, Validators.required],
      guestEmail: guest.guestEmail,
      type: [guest.guestTypeId, [Validators.required]],
      isIncognito: guest.isIncognito,
      isChild: guest.isChild,
      isMain: guest.isMain,
      guestCitizenship: guest.guestCitizenship,
      language: guest.guestLanguage,
      uHPreferably: '',
      lastUH: '',
      lastDailyValue: 0,
      periodOfStay: periodOfStay,
      checkinDate: [guest.checkinDate],
      checkoutDate: [guest.checkoutDate],
      pctDailyRate: [guest.pctDailyRate, NumberRangeValidator.range(0, 100)],
      remarks: '',
      state: cardState,
      lastState: cardState,
      guestStatusId: guest.guestStatusId,
      showDetails: false,
      guestDocument: [guest.guestDocument],
      guestDocumentTypeId: guest.guestDocumentTypeId,
      estimatedArrivalDate: [guest.estimatedArrivalDate, [Validators.required]],
      estimatedDepartureDate: [guest.estimatedDepartureDate, [Validators.required]],
      completeAddress: guest.completeAddress,
    });
  }

  public getDateRangePickerToGuest(guestReservationItem: GuestReservationItem): any {
    return this.dateService.getDateRangePicker(guestReservationItem.estimatedArrivalDate, guestReservationItem.estimatedDepartureDate);
  }

  public getDateRangeLimitReservationItemGuestView(reservationItemGuestView: ReservationItemGuestView) {
    const myDateRangePickerOptions = this.dateService.getConfigDateRangePicker();
    let estimatedArrivalDate: Date;
    let estimatedDepartureDate: Date;

    if (
      typeof reservationItemGuestView.estimatedArrivalDate === 'object' &&
      typeof reservationItemGuestView.estimatedDepartureDate === 'object'
    ) {
      estimatedArrivalDate = reservationItemGuestView.estimatedArrivalDate;
      estimatedDepartureDate = reservationItemGuestView.estimatedDepartureDate;
    }
    if (
      typeof reservationItemGuestView.estimatedArrivalDate === 'string' &&
      typeof reservationItemGuestView.estimatedDepartureDate === 'string'
    ) {
      estimatedArrivalDate = this.dateService.convertStringToDate(reservationItemGuestView.estimatedArrivalDate);
      estimatedDepartureDate = this.dateService.convertStringToDate(reservationItemGuestView.estimatedDepartureDate);
    }

    estimatedArrivalDate.setDate(estimatedArrivalDate.getDate() - 1);
    estimatedDepartureDate.setDate(estimatedDepartureDate.getDate() + 1);

    myDateRangePickerOptions.disableUntil = {
      year: estimatedArrivalDate.getFullYear(),
      month: estimatedArrivalDate.getMonth() + 1,
      day: estimatedArrivalDate.getDate(),
    };
    myDateRangePickerOptions.disableSince = {
      year: estimatedDepartureDate.getFullYear(),
      month: estimatedDepartureDate.getMonth() + 1,
      day: estimatedDepartureDate.getDate(),
    };
    return myDateRangePickerOptions;
  }

  // Change status card in Roomlist
  public setEditGuestForm(guest: GuestCardRoomlist) {
    const state = guest.state;

    guest.state = GuestState.CreatingGuest;
    guest.lastState = state;
  }

  public setCleanGuestForm(guest: GuestCardRoomlist) {
    guest.state = GuestState.NoGuest;
    guest.lastState = GuestState.GuestCreated;
    guest.showDetails = true;
    guest.guestName = '';
    guest.preferredName = '';
    guest.childAge = '';
    guest.guestEmail = '';
    guest.isIncognito = false;
    guest.guestCitizenship = '';
    guest.language = '';
    guest.uHPreferably = '';
    guest.lastUH = '';
    guest.lastDailyValue = 0;
    guest.pctDailyRate = 0;
    guest.remarks = '';
    guest.guestDocument = null;
    guest.guestDocumentTypeId = '';
    guest.completeAddress = '';
  }

  public setSaveGuestForm(guest: GuestCardRoomlist, guestForm: FormGroup) {
    guest.state = GuestState.GuestCreated;
    guest.lastState = GuestState.CreatingGuest;

    guest.guestReservationItemId = guestForm.get('guestReservationItemId').value;
    guest.reservationItemId = guestForm.get('reservationItemId').value;
    guest.guestId = guestForm.get('guestId').value;
    guest.personId = guestForm.get('personId').value;
    guest.guestName = guestForm.get('guestName').value;
    guest.guestEmail = guestForm.get('guestEmail').value;
    guest.guestDocument = guestForm.get('guestDocument').value;
    guest.guestDocumentTypeId = guestForm.get('guestDocumentTypeId').value;
    guest.checkinDate = guestForm.get('checkinDate').value;
    guest.checkoutDate = guestForm.get('checkoutDate').value;
    guest.estimatedArrivalDate = guestForm.get('estimatedArrivalDate').value;
    guest.estimatedDepartureDate = guestForm.get('estimatedDepartureDate').value;
    guest.preferredName = guestForm.get('preferredName').value;
    guest.guestCitizenship = guestForm.get('guestCitizenship').value;
    guest.language = guestForm.get('language').value;
    guest.pctDailyRate = guestForm.get('pctDailyRate').value;
    guest.isIncognito = guestForm.get('isIncognito').value;
    guest.isChild = guestForm.get('isChild').value;
    guest.childAge = guestForm.get('childAge').value;
    guest.isMain = guestForm.get('isMain').value;
    guest.type = guestForm.get('type').value;
    guest.guestStatusId = guestForm.get('guestStatusId').value;
    guest.estimatedArrivalDate = guestForm.get('periodOfStay').value.beginDate;
    guest.estimatedDepartureDate = guestForm.get('periodOfStay').value.endDate;
  }

  public cancelEditForm(guest: GuestCardRoomlist) {
    const lastState = guest.lastState;
    const state = guest.state;

    guest.lastState = state;
    guest.state = lastState;
  }

  public toogleShowDetailsForm(guest: GuestCardRoomlist) {
    guest.showDetails = !guest.showDetails;
  }
  // End Change status card in Roomlist

  public getRoomNumberFromReservationItemGuestView(reservationItemGuestView: ReservationItemGuestView): string {
    if (reservationItemGuestView.roomNumber) {
      return reservationItemGuestView.roomNumber.toString();
    }
    return '';
  }

  public getRoomTypeFromReservationItemGuestView(reservationItemGuestView: ReservationItemGuestView): string {
    if (reservationItemGuestView.roomTypeAbbreviation) {
      return reservationItemGuestView.roomTypeAbbreviation;
    }
    return '';
  }

  private getConfigAutoCompleteDocuments(): Array<DocumentType> {
    let autocompleSelectItems = new Array<DocumentType>();

    const cpfDocument = new DocumentType();
    cpfDocument.id = 1;
    cpfDocument.name = 'CPF';

    const dniDocument = new DocumentType();
    dniDocument.id = 3;
    dniDocument.name = 'DNI';

    const ciDocument = new DocumentType();
    ciDocument.id = 5;
    ciDocument.name = 'CI';

    autocompleSelectItems = new Array<DocumentType>();
    autocompleSelectItems.push(cpfDocument);
    autocompleSelectItems.push(dniDocument);
    autocompleSelectItems.push(ciDocument);

    return autocompleSelectItems;
  }

  public getConfigAutocompleItemsGuestsDocument(): Object {
    return {
      inputId: 'document',
      paramDisplay: 'document',
      autocompleSelectItems: this.getConfigAutoCompleteDocuments(),
    };
  }

  public updateGuestForm(itemData: SearchPersonsResultDto, guest: FormGroup) {
    this.updateGuestFormPersonalData(itemData, guest);
    this.updatePersonInformationGuestForm(itemData, guest);
  }

  private updateGuestFormPersonalData(itemData: SearchPersonsResultDto, guest: FormGroup) {
    if (itemData) {
      const { guestId, fullName, email, document, guestTypeId } = itemData;

      guest.patchValue({
        guestId,
        guestName: fullName,
        guestEmail: email,
        guestDocument: document,
        type: guestTypeId
      });
    }
  }

  private updatePersonInformationGuestForm(itemData: SearchPersonsResultDto, guest: FormGroup) {
    if (itemData) {
      const person = itemData;

      if (person.personInformation) {
        person.personInformation.forEach(info => {
          switch (info.type) {
            case PersonInformationType.nationality:
              guest.get('guestCitizenship').setValue(info.value);
              break;
            case PersonInformationType.socialName:
              guest.get('preferredName').setValue(info.value);
              break;
            case PersonInformationType.language:
              guest.get('language').setValue(info.value);
              break;
            default:
              break;
          }
        });
      }
    }
  }

  public getBySearchGuestAccountData(searchData: string, allItems: Array<GuestAccount>): Array<GuestAccount> {
    return allItems.filter(
      item =>
        item.guestName && item.guestName.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1 ||
        item.reservationCode && item.reservationCode.toLocaleLowerCase().search(searchData.toLocaleLowerCase()) > -1,
    );
  }
}
