import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class RatePlanService {
  constructor() {}

  public getAllFilterBySearchData(searchData: string, allItems: Array<any>): Array<any> {
    return allItems.filter(item => item.agreementName.toLowerCase().indexOf(searchData) >= 0);
  }

  public getAllComissionsFilterByData(seachData: string, allItems: Array<any>): Array<any> {
    return allItems.filter(item => item.billingItemName.toLowerCase().indexOf(seachData) >= 0);
  }

  public getAllUhTypesFilterByData(seachData: string, allItems: Array<any>): Array<any> {
    return allItems.filter(
      item =>
        item.abbreviation.toLowerCase().indexOf(seachData.toLowerCase()) >= 0 ||
        item.name.toLowerCase().indexOf(seachData.toLowerCase()) >= 0,
    );
  }

  public getAllCustomersFilterByData(seachData: string, allItems: Array<any>): Array<any> {
    return allItems.filter(
      item =>
        item.name.toLowerCase().indexOf(seachData.toLowerCase()) >= 0 || item.document.toLowerCase().indexOf(seachData.toLowerCase()) >= 0,
    );
  }
}
