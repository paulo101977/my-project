import { Injectable } from '@angular/core';
import { ReservationItemCommercialBudgetConfig
} from '@app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';
import { SessionParameterService } from '@app/shared/services/parameter/session-parameter.service';
import {
  ReservationBudget,
  ReservationWithBudget,
} from '@app/shared/models/revenue-management/reservation-budget/reservation-budget';
import { ReservationItemCommercialBudget
} from '@app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget';
import { ParameterTypeEnum } from '@app/shared/models/parameter/pameterType.enum';
import { CurrencyFormatPipe } from '@app/shared/pipes/currency-format.pipe';
import { ReservationItemBudget } from '@app/shared/models/revenue-management/reservation-budget/reservation-item-budget';
import { SelectObjectOption } from '@app/shared/models/selectModel';

@Injectable({ providedIn: 'root' })
export class BudgetService {
  public readonly MEAL_PLAN_TYPE_ID_DEFAULT = 2;
  constructor(
    private sessionParameter: SessionParameterService,
    private currencyFormatPipe: CurrencyFormatPipe
  ) {}

  public isUpSelling(actualBudget: number, newBudget: number): boolean {
    return actualBudget < newBudget;
  }

  public isDownSelling(actualBudget: number, newBudget: number): boolean {
    return actualBudget > newBudget;
  }

  public isSameSelling(actualBudget: number, newBudget: number): boolean {
    return actualBudget == newBudget;
  }

  public getReservationBudgetListDefault(reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig, propertyId: number) {
    reservationItemCommercialBudget.reservationBudgetTableList = [];
    if (reservationItemCommercialBudget.dateListFromPeriod) {
      reservationItemCommercialBudget.dateListFromPeriod.forEach(date => {
        this.setBudgetDayListDefault(reservationItemCommercialBudget, date, propertyId);
      });
    } else if (reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent
    && reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.commercialBudgetDayList) {
      reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.commercialBudgetDayList.forEach(budgetDay => {
        this.setBudgetDayListDefault(reservationItemCommercialBudget, budgetDay.day, propertyId);
      });
    }

    return reservationItemCommercialBudget;
  }

  private setBudgetDayListDefault(reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig, date: Date, propertyId: number) {
    const reservationBudget = new ReservationBudget();
    reservationBudget.budgetDay = date;
    reservationBudget.baseRate = 0;
    reservationBudget.baseRateChanged = false;
    reservationBudget.manualRate = 0;
    reservationBudget.agreementRate = 0;
    reservationBudget.mealPlanTypeId = 0;
    reservationBudget.currencySymbol = this.sessionParameter.extractSelectedOptionOfPossibleValues(
      this.sessionParameter.getParameter(propertyId, ParameterTypeEnum.DefaultCurrency),
    );
    reservationBudget.discount = 0;
    reservationBudget.mealPlanTypeId = this.MEAL_PLAN_TYPE_ID_DEFAULT;
    this.pushInBudgetTable(reservationItemCommercialBudget, reservationBudget);
    this.resetBudgetCurrentWithMealPlanTypeDefault(reservationItemCommercialBudget);
  }

  private pushInBudgetTable(reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig, reservationBudget: ReservationBudget) {
    reservationItemCommercialBudget.reservationBudgetTableList.push(reservationBudget);
  }

  private resetBudgetCurrentWithMealPlanTypeDefault(reservationItemCommercialBudget: ReservationItemCommercialBudgetConfig) {
    reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent = new ReservationItemCommercialBudget();
    reservationItemCommercialBudget.reservationItemCommercialBudgetCurrent.mealPlanTypeId = this.MEAL_PLAN_TYPE_ID_DEFAULT;
  }

  public getCommercialBudgetList(reservationItemCommercialBudgetConfig: ReservationItemCommercialBudgetConfig,
                                  reservationItemBudget: ReservationItemBudget) {
    reservationItemCommercialBudgetConfig
      .selectReservationItemCommercialBudgetOriginalList = reservationItemBudget.commercialBudgetList;

    reservationItemCommercialBudgetConfig.selectReservationItemCommercialBudgetList =
      reservationItemBudget.commercialBudgetList
        .filter( item => item.isMealPlanTypeDefault) // filter only the mealPlanDefault
        .map(reservationItemCommercialBudget => {
          const totalBudget = this.currencyFormatPipe.transform(
            reservationItemCommercialBudget.totalBudget,
            reservationItemCommercialBudget.currencySymbol);
          const option = new SelectObjectOption();
          option.key = reservationItemCommercialBudget;
          option.value = `<div class="justify-content-row" style="padding-left: 5px"><div class="thf-u-font--13">${
            reservationItemCommercialBudget.commercialBudgetName
            }</div><div class="thf-u-font--13">${
            reservationItemCommercialBudget.mealPlanTypeName
            }</div><div class="thf-u-font--13" style="margin-top: 5px">
              ${totalBudget}
            </div></div>`;
          return option;
      });

    return reservationItemCommercialBudgetConfig;
  }
}
