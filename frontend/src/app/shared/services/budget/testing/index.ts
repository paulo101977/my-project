import { createServiceStub } from '../../../../../../testing';
import { BudgetService } from 'app/shared/services/budget/budget.service';
import { ReservationItemCommercialBudgetConfig
} from 'app/revenue-management/reservation-budget/components-containers/reservation-budget-detail/reservation-item-budget-config';
import { ReservationItemBudget } from 'app/shared/models/revenue-management/reservation-budget/reservation-item-budget';

export const budgetServiceStub = createServiceStub(BudgetService, {
    getCommercialBudgetList(
        reservationItemCommercialBudgetConfig: ReservationItemCommercialBudgetConfig,
        reservationItemBudget: ReservationItemBudget): ReservationItemCommercialBudgetConfig {
        return null;
    },
});
