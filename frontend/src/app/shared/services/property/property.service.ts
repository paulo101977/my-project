import { PropertyToken } from './../../models/PropertyToken';
import { AuthService } from './../auth-service/auth.service';
import { Injectable } from '@angular/core';
import {Subject, Observable, of} from 'rxjs';
import { Property } from './../../models/property';
import { SessionParameterService } from '../parameter/session-parameter.service';
import { ParameterTypeEnum } from '../../models/parameter/pameterType.enum';
import * as _ from 'lodash';
import {mergeMap, tap} from 'rxjs/operators';
import { AuthService as BifrostAuthService, PropertyStorageService, TokenManagerService } from '@inovacaocmnet/thx-bifrost';
import {PropertyResource} from 'app/shared/resources/property/property.resource';

@Injectable({ providedIn: 'root' })
export class PropertyService {
  private subject = new Subject<any>();

  constructor(
    private sessionParameter: SessionParameterService,
    private authService: AuthService,
    private bifrostAuthService: BifrostAuthService,
    private propertyStorageService: PropertyStorageService,
    private tokenService: TokenManagerService,
    public propertyResource: PropertyResource,
  ) {}

  public getPropertyUIdfrom(propertyId: number) {
    const property = this.getPropertySession(propertyId);
    return property.propertyUId;
  }

  public setPropertyToShare(property: Property) {
    this.subject.next(property);
  }

  public getPropertyShared(): Observable<Property> {
    return this.subject.asObservable();
  }

  public getChildAgeOfProperty(propertyId: number) {
    const age = this.getHigherChildAge(propertyId);
    const listAges = [];
    if (age > 0) {
      for (let i = 0; i <= age; i++) {
        listAges.push({ value: i, text: _.padStart(i + '', 2, '0') });
      }
    }
    return listAges;
  }

  public getHigherChildAge(propertyId: number): number {
    let child = this.sessionParameter.getParameter(propertyId, ParameterTypeEnum.ChildAgeRange3);
    let parameterValue = '0';
    if (child && !child.isActive) {
      child = this.sessionParameter.getParameter(propertyId, ParameterTypeEnum.ChildAgeRange2);
      if (child && !child.isActive) {
        child = this.sessionParameter.getParameter(propertyId, ParameterTypeEnum.ChildAgeRange1);
      }
    }
    if (child) {
      parameterValue = child.propertyParameterValue;
    }
    return parseInt(parameterValue, 0);
  }

  public setPropertySession(property: Property) {
    this.propertyStorageService.setCurrentProperty(property);
  }

  public getPropertySession(propertyId: number): Property {
    return this.propertyStorageService.getCurrentProperty();
  }

  public getPropertyCountryCode() {
    const token = this.tokenService.extractTokenPayload(
      this.tokenService.getCurrentPropertyToken());
    return token ? token.PropertyCountryCode : '';
  }

  public setProperty(appName: string, property) {
    this.propertyStorageService.addProperty(appName, property);
    this.propertyStorageService.setCurrentProperty(property);
  }

  private getPropertySessionName(properyId) {
    console.warn('DEPRECATED: use tokenManager from bifrost instead');
    return `property-${properyId}`;
  }

  private setToken(appName: string, propertyId: number, property: any) {
    this.tokenService.setPropertyToken(appName, this.tokenService.getCurrentPropertyToken(), propertyId, property );
  }

  private getProperty(propertyId: number) {
    return this.propertyResource.getPropertyById(propertyId);
  }

  public authenticateOnProperty(appName: string, property: any) {
    if (property) {
      const propertyId = property.id;
      return this.bifrostAuthService.authenticateOnProperty(appName, propertyId)
        .pipe(
          mergeMap( () => this.getProperty(propertyId)),
          tap( result => this.setToken(appName, propertyId, result)),
          tap( result => this.setProperty(appName, result) )
        );
    }
    return of();
  }

  public async asyncAuthenticateOnProperty(appName: string, property: any) {
    return await this.authenticateOnProperty(appName, property).toPromise();
  }

}
