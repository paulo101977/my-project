import { createServiceStub } from '../../../../../../testing';
import { PropertyService } from 'app/shared/services/property/property.service';
import { Property } from 'app/shared/models/property';

export const propertyServiceStub = createServiceStub(PropertyService, {
  getPropertyCountryCode: () => '',
  getPropertySession: (propertyId: number) => new Property(),
  getHigherChildAge(propertyId: number): number {
    return 0;
  }
});
