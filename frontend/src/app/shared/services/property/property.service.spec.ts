import { AuthService } from './../auth-service/auth.service';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TestBed } from '@angular/core/testing';
import { Property } from 'app/shared/models/property';
import { PropertyService } from './property.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';
import { createServiceStub } from 'testing/create-service-stub';
import { AdminApiModule,
  ThexApiModule,
  CentralApiModule,
  configureApi,
  ApiEnvironment,
  configureAppName,
} from '@inovacaocmnet/thx-bifrost';


describe('PropertyService', () => {
  let service: PropertyService;

  const authServiceStub = createServiceStub(AuthService, {
    getPropertyCountryCode(): any { return 'BR'; }
  });

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [
        CentralApiModule,
        AdminApiModule,
        ThexApiModule,
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
      ],
      providers: [
        InterceptorHandlerService,
        authServiceStub,
        configureApi({
          environment: ApiEnvironment.Development
        }),
        configureAppName('crs'),
      ]
    });

    service = TestBed.get(PropertyService);
  });

  it('should create PropertyService', () => {
    expect(service).toBeTruthy();
  });

  it('should setPropertyToShare', () => {
    spyOn(service['subject'], 'next');

    const property = new Property();
    service.setPropertyToShare(property);

    expect(service['subject'].next).toHaveBeenCalledWith(property);
  });

  it('should getPropertyShared', () => {
    spyOn(service['subject'], 'asObservable');

    service.getPropertyShared();

    expect(service['subject'].asObservable).toHaveBeenCalled();
  });

  it('should create PropertyService', () => {
    const childAgeList = service.getChildAgeOfProperty(1);
    expect(childAgeList.length).toEqual(service.getHigherChildAge(1));
  });
});
