import { Injectable } from '@angular/core';
import { ParameterResource } from '../../resources/parameter/parameter-resource';
import { ParameterTypeEnum } from '../../models/parameter/pameterType.enum';
import { Parameter, ParameterGroup } from '../../models/parameter';
import * as _ from 'lodash';
import { Subject, BehaviorSubject } from 'rxjs/index';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { TranslateService } from '@ngx-translate/core';

@Injectable({ providedIn: 'root' })
export class SessionParameterService {
  private PARAMETER_KEY = 'parameter';
  private OBJECT_PARAMETER_KEY = 'propertyParameters';

  private parameterModification = new Subject<Parameter>();
  parameterModificationChange$ = this.parameterModification.asObservable();

  constructor(private parameterResource: ParameterResource,
              private toasterService: ToasterEmitService,
              private translateService: TranslateService) {}

  public populateSessionParameters(propertyId: number) {
    const parameterKeyProperty = this.getParameterKey(propertyId);
    if (!sessionStorage.getItem(parameterKeyProperty)) {
      this.parameterResource.getParametersList(propertyId).subscribe(result => {
        sessionStorage.setItem(parameterKeyProperty, JSON.stringify(result));
        this.parameterModification.next();
      });
    }
  }

  public updateParameterOnList(propertyId: number, parameter: Parameter) {
    const parameterKeyProperty = this.getParameterKey(propertyId);
    const parameters = sessionStorage.getItem(parameterKeyProperty);
    if (parameters) {
      const parametersObj = JSON.parse(parameters);
      (<Array<ParameterGroup>>parametersObj).forEach((item, i) => {
        if (item.featureGroupId == parameter.featureGroupId) {
          item.parameters.forEach((item2, k) => {
            if (item2.id == parameter.id) {
              parametersObj[i].parameters[k] = parameter;
              sessionStorage.removeItem(parameterKeyProperty);
              sessionStorage.setItem(parameterKeyProperty, JSON.stringify(parametersObj));
              this.parameterModification.next(parameter);
            }
          });
        }
      });
    }
  }

  public getParameter(propertyId: number, parameterType: ParameterTypeEnum): Parameter {

    if (!propertyId) {
      return null;
    }

    if (parameterType == ParameterTypeEnum.ChildMaxAge) {
      return this.getChildMaxAge(propertyId);
    }

    const sessionParam = sessionStorage.getItem(this.getParameterKey(propertyId));
    let parameter = null;
    if (sessionParam) {
      const list = JSON.parse(sessionParam) as Array<ParameterGroup>;
      list.some(parameterGroup => {
        const param = _.find(parameterGroup.parameters, { applicationParameterId: parameterType });
        if (param) {
          parameter = param;
          return true;
        } else {
          return false;
        }
      });
    }
    if (!parameter) {
      this.notifyParametersNotConfigured(parameterType);
    }
    return parameter;
  }

  public getChildren(propertyId): Array<any> {
    const sessionParam = sessionStorage.getItem(this.getParameterKey(propertyId));

    if ( sessionParam ) {
      const list = JSON.parse(sessionParam) as Array<ParameterGroup>;

      return list && list.length > 1 ? list[2].parameters : [];
    }

    return [];
  }

  public getParameterValue(propertyId: number, parameterType: ParameterTypeEnum): string {
    const parameter = this.getParameter(propertyId, parameterType);
    if (parameter) {
      return parameter.propertyParameterValue;
    }
    return null;
  }

  public cleanPrameters(propertyId: number) {
    sessionStorage.removeItem(this.getParameterKey(propertyId));
  }

  private getParameterKey(propertyId) {
    return this.PARAMETER_KEY + '-' + propertyId;
  }

  private getChildMaxAge(propertyId: number) {
    const childRAnge3 = this.getParameter(propertyId, ParameterTypeEnum.ChildAgeRange3);
    if (childRAnge3.isActive) {
      return childRAnge3;
    }
    const childRAnge2 = this.getParameter(propertyId, ParameterTypeEnum.ChildAgeRange2);
    if (childRAnge2.isActive) {
      return childRAnge2;
    }

    const childRAnge1 = this.getParameter(propertyId, ParameterTypeEnum.ChildAgeRange1);
    return childRAnge1;
  }

  public extractSelectedOptionOfPossibleValues(parameter: Parameter) {
    if (parameter && parameter.propertyParameterPossibleValues && parameter.propertyParameterValue) {
      const option = JSON.parse(parameter.propertyParameterPossibleValues)
        .find(possibleValue => parameter.propertyParameterValue.toLowerCase() == possibleValue.value.toLowerCase());
      if (option) {
        return option;
      }
    }
    return null;
  }

  /* PARAMETER Validations */
  private notifyParametersNotConfigured(parameterType: ParameterTypeEnum) {
    let labelError = '';
    // TODO implement other messages if need
    switch (parameterType) {
      case ParameterTypeEnum.DefaultCurrency:
        labelError = this.translateService.instant('text.currencyError');
        this.toasterService.emitChange(SuccessError.error, labelError);
        console.error(labelError);
        break;
    }
  }

}
