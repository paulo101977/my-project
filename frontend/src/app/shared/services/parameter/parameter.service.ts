import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Parameter, ParameterGroup, ParameterReturn } from '../../models/parameter';
import { ParameterResource } from '../../resources/parameter/parameter-resource';
import { ParameterTypeEnum } from '../../models/parameter/pameterType.enum';

@Injectable({ providedIn: 'root' })
export class ParameterService {
  constructor(private _parameterResource: ParameterResource) {}

  getParameters(propertyId: number): Observable<Array<ParameterGroup>> {
    return this._parameterResource.getParametersList(propertyId);
  }

  saveParameter(parameter: Parameter, propertyId: number) {
    return this._parameterResource.saveParameterChange(parameter, propertyId);
  }

  saveAllParameters(parameterGroupList: Array<ParameterGroup>) {
    const parameterReturn = new ParameterReturn();
    parameterReturn.propertyParameters = parameterGroupList;
    return this._parameterResource.saveAllParametersChange(parameterReturn);
  }

  saveChildrenAgeRange(parameterGroup: ParameterGroup, propertyId: number) {
    if (parameterGroup && parameterGroup.parameters) {
      const params = parameterGroup.parameters;
      return this._parameterResource.saveChildRange(params, propertyId);
    }
  }

  toggleParameter(parameter: Parameter) {
    return this._parameterResource.toggleParameter(parameter);
  }

  fieldMustBeDisabled(parameter: Parameter) {
    if (
      parameter &&
      (parameter.applicationParameterId == ParameterTypeEnum.SistemDate || parameter.applicationParameterId == ParameterTypeEnum.AuditStep)
    ) {
      if (parameter.propertyParameterValue && parseInt(parameter.propertyParameterValue, 0) > 0) {
        return true;
      }
    }
    return false;
  }
}
