import { createServiceStub } from '../../../../../../testing';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { Parameter } from 'app/shared/models/parameter';

export const sessionParameterServiceStub = createServiceStub(SessionParameterService, {
  getParameter: (propertyId: number, parameterType: ParameterTypeEnum) => new Parameter(),
  getParameterValue: (propertyId: number, parameterType: ParameterTypeEnum) => 'paramValue',
  extractSelectedOptionOfPossibleValues: (parameter: Parameter) => null,
  cleanPrameters(propertyId: number) {
  },
  getChildren: (propertyId) => null,
  populateSessionParameters(propertyId: number) {
  },
  updateParameterOnList(propertyId: number, parameter: Parameter) {
  }
});

