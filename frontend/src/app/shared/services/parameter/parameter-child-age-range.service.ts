// Angualar imports
import { Injectable } from '@angular/core';
import { Parameter, ParameterGroup, ParameterGroupType } from '../../models/parameter';
import { ParameterTypeEnum } from '../../models/parameter/pameterType.enum';
import * as _ from 'lodash';

@Injectable({ providedIn: 'root' })
export class ParameterChildAgeRangeService {
  constructor() {}

  public isGroupChilden(groupId) {
    return groupId == ParameterGroupType.GroupChildren;
  }

  public getParameterByType(parameterGroupList: Array<ParameterGroup>, parameterId: ParameterTypeEnum): Parameter {
    let parameter = null;
    parameterGroupList.forEach( parameterGroup => {
      const param = _.find(parameterGroup.parameters, {applicationParameterId: parameterId} );
      if (param) {
        parameter = param;
        return parameter;
      }
    });
    return parameter;
  }

  public someInGroupCanInactive(parameterGroupList: Array<ParameterGroup>, groupId) {
    const element = parameterGroupList.filter( item => item.featureGroupId == groupId)[0];
    return _.find(element.parameters, { canInactive: true });
  }

  public validateAgeRange(value, parameter: Parameter) {
    const intValue = parseInt(value, 0);
    const intMax = parseInt(parameter.propertyParameterMaxValue, 0);
    const intMin = parseInt(parameter.propertyParameterMinValue, 0);

    if (intValue <= intMax && intValue > intMin) {
      return true;
    }
    return false;
  }
}
