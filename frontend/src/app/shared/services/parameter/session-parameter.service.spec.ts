import { inject, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared.module';
import { SessionParameterService } from './session-parameter.service';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('SessionParameterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BrowserDynamicTestingModule, RouterTestingModule, SharedModule, TranslateModule.forRoot()],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  it('should be created', inject([SessionParameterService], (service: SessionParameterService) => {
    expect(service).toBeTruthy();
  }));
});
