import { DateService } from 'app/shared/services/shared/date.service';
import { FormBuilder } from '@angular/forms';
import { TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { ReservationItemViewService } from './reservation-item-view.service';
import { RoomTypeStandardData } from './../../mock-data/room-type.data';
import { ReservationItemView } from './../../models/reserves/reservation-item-view';
import { ReservationStatusEnum } from './../../models/reserves/reservation-status-enum';
import { SharedModule } from '../../shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('ReservationItemViewService', () => {
  let service: ReservationItemViewService;
  let dateService: DateService;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        SharedModule,
        RouterTestingModule
      ],
      providers: [ DateService ]
    });

    service = TestBed.get(ReservationItemViewService);
    dateService = TestBed.get(DateService);
  });

  describe('on isAccommodationSaved', () => {
    it('should set false when accommodation was not saved', () => {
      const reservationItemView = new ReservationItemView();
      reservationItemView.status = ReservationStatusEnum.ToConfirm;

      expect(service.isAccommodationSaved(reservationItemView)).toBeFalsy();
    });

    it('should set true when accommodation is save', () => {
      const reservationItemView = new ReservationItemView();
      reservationItemView.status = ReservationStatusEnum.Checkin;

      expect(service.isAccommodationSaved(reservationItemView)).toBeTruthy();
    });
  });

  describe('on hasMoreThanOneAccommodationToSave', () => {
    it('should set false when accomodations to save equals one', () => {
      const reservationItemViewList = new Array<ReservationItemView>();
      const reservationItemView = new ReservationItemView();
      reservationItemView.status = ReservationStatusEnum.ToConfirm;
      reservationItemViewList.push(reservationItemView);

      expect(service.hasMoreThanOneAccommodationToSave(reservationItemViewList)).toBeFalsy();
    });

    it('should set true when accomodations to save is more than one', () => {
      const accommodations = new Array<ReservationItemView>();
      const accommodation = new ReservationItemView();
      accommodation.status = ReservationStatusEnum.ToConfirm;
      accommodations.push(accommodation);
      accommodations.push(accommodation);

      expect(service.hasMoreThanOneAccommodationToSave(accommodations)).toBeTruthy();
    });
  });

  describe('on isRoomTypeCapacityMajorThanGuestCapacity', () => {
    it('should set true when isRoomTypeCapacityMajorThanGuestCapacity', () => {
      const roomTypeCapacity = 3;
      const guestCapacity = 2;

      expect(service.isRoomTypeCapacityMajorThanGuestCapacity(guestCapacity, roomTypeCapacity)).toBeTruthy();
    });

    it('should set true when isRoomTypeCapacityMajorThanGuestCapacity', () => {
      const roomTypeCapacity = 1;
      const guestCapacity = 2;

      expect(service.isRoomTypeCapacityMajorThanGuestCapacity(guestCapacity, roomTypeCapacity)).toBeFalsy();
    });
  });

  describe('on getRoomTypeRequestedOfNewReservationItem', () => {
    it('should getRoomTypeRequestedOfNewReservationItem with reservationGroup.roomTypeRequested', () => {
      const formGroup = new FormBuilder().group({
        reservationGroup: new FormBuilder().group({
          roomTypeRequested: [RoomTypeStandardData],
        }),
      });

      const roomType = service.getRoomTypeRequestedOfNewReservationItem(formGroup);

      expect(roomType).toEqual(RoomTypeStandardData);
    });

    it('should getRoomTypeRequestedOfNewReservationItem without reservationGroup.roomTypeRequested', () => {
      const formGroup = new FormBuilder().group({
        reservationGroup: new FormBuilder().group({
          roomTypeRequested: null,
          roomType: [RoomTypeStandardData],
        }),
      });

      const roomType = service.getRoomTypeRequestedOfNewReservationItem(formGroup);

      expect(roomType).toEqual(RoomTypeStandardData);
    });
  });

  describe('on getRoomTypeRequestedOfReservationItemExistent', () => {
    it('should getRoomTypeRequestedOfReservationItemExistent with roomTypeRequestedCard', () => {
      const formGroup = new FormBuilder().group({
        roomTypeRequestedCard: [RoomTypeStandardData],
      });

      const roomType = service.getRoomTypeRequestedOfReservationItemExistent(formGroup);

      expect(roomType).toEqual(RoomTypeStandardData);
    });

    it('should getRoomTypeRequestedOfReservationItemExistent without roomTypeRequestedCard', () => {
      const formGroup = new FormBuilder().group({
        roomTypeRequestedCard: null,
        roomTypeCard: RoomTypeStandardData,
      });

      const roomType = service.getRoomTypeRequestedOfReservationItemExistent(formGroup);

      expect(roomType).toEqual(RoomTypeStandardData);
    });
  });

  describe('on change status of reservationItem', () => {
    it('should return true when is edit page', () => {
      const path = '/hotel/reservation/new;reservation=2';
      expect(service.isEditPageReservationItem(path)).toBeTruthy();
    });

    it('should return false when is not edit page', () => {
      const path = '/hotel/reservation/new';
      expect(service.isEditPageReservationItem(path)).toBeFalsy();
    });

    it('should return true when hasStatusChanged', () => {
      const status = ReservationStatusEnum.ToConfirm;
      expect(service.hasStatusChanged(status)).toBeTruthy();
      const statusConfirmed = ReservationStatusEnum.Confirmed;
      expect(service.hasStatusChanged(statusConfirmed)).toBeTruthy();
    });

    it('should return false when hasStatusUnChanged', () => {
      const status = ReservationStatusEnum.Checkin;
      expect(service.hasStatusChanged(status)).toBeFalsy();
    });

    it('should canCancelStatusOfReservationItem when status is ToConfirm', () => {
      const reservationItemView = new ReservationItemView();
      reservationItemView.id = 1;
      reservationItemView.status = ReservationStatusEnum.ToConfirm;

      expect(service.canCancelStatusOfReservationItem(reservationItemView)).toBeTruthy();
    });

    it('should canCancelStatusOfReservationItem when status is Confirmed', () => {
      const reservationItemView = new ReservationItemView();
      reservationItemView.id = 1;
      reservationItemView.status = ReservationStatusEnum.Confirmed;

      expect(service.canCancelStatusOfReservationItem(reservationItemView)).toBeTruthy();
    });

    it('should not canCancelStatusOfReservationItem', () => {
      const reservationItemView = new ReservationItemView();
      reservationItemView.id = 1;
      reservationItemView.status = ReservationStatusEnum.Canceled;

      expect(service.canCancelStatusOfReservationItem(reservationItemView)).toBeFalsy();
    });

    it('should canReactivateStatusOfReservationItem', () => {
      const reservationItemView = new ReservationItemView();
      reservationItemView.id = 1;
      reservationItemView.status = ReservationStatusEnum.Canceled;

      expect(service.canReactivateStatusOfReservationItem(reservationItemView)).toBeTruthy();
    });

    it('should not canReactivateStatusOfReservationItem', () => {
      const reservationItemView = new ReservationItemView();
      reservationItemView.id = 1;
      reservationItemView.status = ReservationStatusEnum.Confirmed;

      expect(service.canReactivateStatusOfReservationItem(reservationItemView)).toBeFalsy();
    });
  });

  it('should periodOfReservationItemIsInvalidForWalkin', () => {
    const reservationItemView = new ReservationItemView();
    const today = new Date(2018, 1, 4);
    const systemDate = new Date(2018, 1, 3);
    const propertyId = 1;

    spyOn(dateService, 'getSystemDate').and.returnValue(systemDate);

    reservationItemView.formCard = new FormBuilder().group({
      periodCard: {
        beginDate: today.toStringUniversal(),
      },
    });

    expect(service.periodOfReservationItemIsInvalidForWalkin(reservationItemView, propertyId)).toBeTruthy();
  });

  it('should not periodOfReservationItemIsInvalidForWalkin', () => {
    const reservationItemView = new ReservationItemView();
    const today = new Date(2018, 1, 4);
    const systemDate = new Date(2018, 1, 4);
    const propertyId = 1;

    spyOn(dateService, 'getSystemDate').and.returnValue(systemDate);

    reservationItemView.formCard = new FormBuilder().group({
      periodCard: {
        beginDate: today.toStringUniversal(),
      },
    });

    expect(service.periodOfReservationItemIsInvalidForWalkin(reservationItemView, propertyId)).toBeFalsy();
  });

  it('should reduceAndGetCodeOfReservationItem with code empty', () => {
    const code = '';

    const result = service.reduceAndGetCodeOfReservationItem(code);

    expect(result).toEqual(code);
  });

  it('should reduceAndGetCodeOfReservationItem without dash', () => {
    const code = '0001';

    const result = service.reduceAndGetCodeOfReservationItem(code);

    expect(result).toEqual(code);
  });

  it('should reduceAndGetCodeOfReservationItem with dash', () => {
    const code = 'BR-20171206-4PGR6UMBDU7-0001';

    const result = service.reduceAndGetCodeOfReservationItem(code);

    expect(result).toEqual('0001');
  });
});
