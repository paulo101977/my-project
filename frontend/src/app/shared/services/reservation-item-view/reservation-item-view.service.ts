import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { RoomType } from '../../../room-type/shared/models/room-type';
import { ReservationItemView } from './../../models/reserves/reservation-item-view';
import { ReservationStatusEnum } from './../../models/reserves/reservation-status-enum';
import { DateService } from '../shared/date.service';
import * as moment from 'moment';

@Injectable({ providedIn: 'root' })
export class ReservationItemViewService {
  private roomlistRequestSource = new Subject<ReservationItemView>();
  roomlistRequested$ = this.roomlistRequestSource.asObservable();

  constructor(private dateService: DateService) {}

  public isAccommodationSaved(accommodation: ReservationItemView): boolean {
    return accommodation.status !== ReservationStatusEnum.ToConfirm;
  }

  public hasMoreThanOneAccommodationToSave(accommodations: Array<ReservationItemView>): boolean {
    return accommodations.length > 1;
  }

  public isRoomTypeCapacityMajorThanGuestCapacity(guestCapacity: number, roomTypeCapacity: number): boolean {
    return guestCapacity < roomTypeCapacity;
  }

  public getRoomTypeRequestedOfNewReservationItem(formGroup: FormGroup): RoomType {
    let roomTypeRequested = formGroup.get('reservationGroup.roomTypeRequested').value;
    if (!roomTypeRequested) {
      roomTypeRequested = formGroup.get('reservationGroup.roomType').value;
    }
    return roomTypeRequested;
  }

  public getRoomTypeRequestedOfReservationItemExistent(formGroup: FormGroup): RoomType {
    let roomTypeRequested = formGroup.get('roomTypeRequestedCard').value;
    if (!roomTypeRequested) {
      roomTypeRequested = formGroup.get('roomTypeCard').value;
    }
    return roomTypeRequested;
  }

  public isEditPageReservationItem(path) {
    return path.indexOf('reservation=') > -1;
  }

  public hasStatusChanged(status) {
    return status === ReservationStatusEnum.ToConfirm || status === ReservationStatusEnum.Confirmed;
  }

  public canCancelStatusOfReservationItem(reservationItemView: ReservationItemView): boolean {
    return (
      reservationItemView.id > 0 &&
      (reservationItemView.status == ReservationStatusEnum.ToConfirm || reservationItemView.status == ReservationStatusEnum.Confirmed)
    );
  }

  public canReactivateStatusOfReservationItem(reservationItemView: ReservationItemView): boolean {
    return reservationItemView.id > 0 && reservationItemView.status == ReservationStatusEnum.Canceled;
  }

  public periodOfReservationItemIsInvalidForWalkin(reservationItemView: ReservationItemView, propertyId: number): boolean {

    if (
      ( reservationItemView === null )
      || ( reservationItemView !== null && reservationItemView.formCard === null)
      || (
        reservationItemView !== null
          && reservationItemView.formCard !== null
          && reservationItemView.formCard.get('periodCard').value === null
        )
      ) {
      return true;
    }

    const beginJsDate: Date =
      moment(reservationItemView.formCard.get('periodCard').value.beginDate, DateService.DATE_FORMAT_UNIVERSAL).toDate();

    const systemDate = this.dateService.getSystemDate(propertyId);

    return (
      beginJsDate.getDate() != systemDate.getDate() ||
      beginJsDate.getMonth() != systemDate.getMonth() ||
      beginJsDate.getFullYear() != systemDate.getFullYear()
    );
  }

  public reduceAndGetCodeOfReservationItem(code: string): string {
    if (!code) {
      return '';
    }

    const array = code.split('-');
    if (array.length == 4) {
      return array[3];
    }
    return code;
  }

  public goToRoomlist(reservationItemView) {
    this.roomlistRequestSource.next(reservationItemView);
  }

  public hasChangeInReservationViewList(reservationItemViewList: ReservationItemView[]) {
    let hasChange = false;
    if (reservationItemViewList) {
      reservationItemViewList.forEach(reservationItemView => {
        const reservationItemViewSaved = this.isAccommodationSaved(reservationItemView);
        const hasChangeReservationItemView = reservationItemView.formCard.dirty;
        hasChange = (!reservationItemViewSaved && reservationItemView.reservationId === 0) || hasChangeReservationItemView;
      });
    }
    return hasChange;
  }
}
