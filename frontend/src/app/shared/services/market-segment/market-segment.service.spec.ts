import { TestBed, inject } from '@angular/core/testing';

import { MarketSegmentService } from './market-segment.service';

describe('MarketSegmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MarketSegmentService]
    });
  });

  it('should be created', inject([MarketSegmentService], (service: MarketSegmentService) => {
    expect(service).toBeTruthy();
  }));
});
