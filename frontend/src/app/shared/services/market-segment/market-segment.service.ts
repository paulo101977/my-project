import { Injectable } from '@angular/core';
import { MarketSegment } from '../../models/dto/market-segment';

@Injectable({ providedIn: 'root' })
export class MarketSegmentService {

  constructor() { }

  public prepareSecondarySelectList( marketSegmentList: Array<MarketSegment>) {
    const list = [];
    if (marketSegmentList) {
      for (const marketSegment of marketSegmentList) {
        if (marketSegment.childMarketSegmentList && marketSegment.childMarketSegmentList.length > 0) {
          for (const childMarket of marketSegment.childMarketSegmentList) {
            list.push({
              'key': childMarket.id,
              'value': `${marketSegment.segmentAbbreviation} - ${childMarket.name}`
            });
          }
        }
      }
    }
    return list;
  }

}
