import { Injectable } from '@angular/core';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { TranslateService } from '@ngx-translate/core';
import { GuestStatus } from 'app/shared/models/reserves/guest';

@Injectable({ providedIn: 'root' })
export class GuestReservationItemService {
  constructor(private translateService: TranslateService) {}

  public getInvalidGuestReservationItemList(list: CheckinGuestReservationItemDto[],
  ): Array<CheckinGuestReservationItemDto> {
    return list.filter(guest => !guest.isValid
      || guest.guestStatusId == GuestStatus.Checkin);
  }

  public getFormattedMessageOfGuestInvalidList(checkinGuestReservationItemList: Array<CheckinGuestReservationItemDto>): string {
    let formattedMessage = '';
    checkinGuestReservationItemList.forEach((guest, index) => {
      formattedMessage += guest.guestReservationItemName;
      if (index == checkinGuestReservationItemList.length - 2) {
        formattedMessage += ` ${this.translateService.instant('commomData.and')} `;
      } else if (index < checkinGuestReservationItemList.length - 2) {
        formattedMessage += ', ';
      }
    });
    return formattedMessage;
  }
}
