import { TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { GuestReservationItemService } from './guest-reservation-item.service';
import { TranslateModule } from '@ngx-translate/core';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';

describe('GuestReservationItemService', () => {
  let service;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [],
    });

    service = TestBed.get(GuestReservationItemService);
  });

  it('should getInvalidGuestReservationItemList', () => {
    const checkinGuestReservationItemList = new Array<CheckinGuestReservationItemDto>();
    const checkinGuestReservationItemDtoValid = new CheckinGuestReservationItemDto();
    checkinGuestReservationItemDtoValid.isValid = true;
    const checkinGuestReservationItemDtoInvalid = new CheckinGuestReservationItemDto();
    checkinGuestReservationItemDtoInvalid.isValid = false;

    checkinGuestReservationItemList.push(checkinGuestReservationItemDtoValid);
    checkinGuestReservationItemList.push(checkinGuestReservationItemDtoInvalid);

    const invalidGuestReservationItemList = service.getInvalidGuestReservationItemList(checkinGuestReservationItemList);

    expect(invalidGuestReservationItemList[0]).toEqual(checkinGuestReservationItemDtoInvalid);
    expect(invalidGuestReservationItemList.length).toEqual(1);
  });

  it('should getFormattedMessageOfGuestInvalidList', () => {
    const checkinGuestReservationItemList = new Array<CheckinGuestReservationItemDto>();
    const checkinGuestReservationItemDto1 = new CheckinGuestReservationItemDto();
    checkinGuestReservationItemDto1.guestReservationItemName = 'Ricardo';
    const checkinGuestReservationItemDto2 = new CheckinGuestReservationItemDto();
    checkinGuestReservationItemDto2.guestReservationItemName = 'Yoseph';
    const checkinGuestReservationItemDto3 = new CheckinGuestReservationItemDto();
    checkinGuestReservationItemDto3.guestReservationItemName = 'Caio';

    checkinGuestReservationItemList.push(checkinGuestReservationItemDto1);
    checkinGuestReservationItemList.push(checkinGuestReservationItemDto2);
    checkinGuestReservationItemList.push(checkinGuestReservationItemDto3);

    const message = service.getFormattedMessageOfGuestInvalidList(checkinGuestReservationItemList);

    expect(message).toEqual('Ricardo, Yoseph commomData.and Caio');
  });
});
