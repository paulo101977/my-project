import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ColorsService {
  public ARRAY_COLOR_CONTRAST_FONT_BLACK = ['#F8E71C', '#B8E986'];
  constructor() {}

  public getFontBasedOnColor(color) {
    color = color.indexOf('#') < 0 ? '#' + color : color;
    return this.ARRAY_COLOR_CONTRAST_FONT_BLACK.indexOf(color.toUpperCase()) >= 0 ? '#000' : '#fff';
  }
  public getBlackAndWhiteColorBasedOn(bgColor) {
    if (!bgColor) {
      return '';
    }
    return parseInt(bgColor.replace('#', ''), 16) > 0xffffff / 2 ? '#000' : '#fff';
  }
}
