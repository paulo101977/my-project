import { inject, TestBed } from '@angular/core/testing';

import { ColorsService } from './colors.service';

describe('ColorsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [],
    });
  });

  it('should be created', inject([ColorsService], (service: ColorsService) => {
    expect(service).toBeTruthy();
  }));
});
