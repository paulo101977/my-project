import { TestBed } from '@angular/core/testing';
import { HigsApiService } from '@inovacaocmnet/thx-bifrost';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../testing/create-service-stub';

import { ReservationDownloadResourceService } from './reservation-download-resource.service';

const HigsApiProvider = createServiceStub(HigsApiService, {
  get<T>(path: any, options?: any): Observable<T> {
    return of(null);
  }
});

describe('ReservationDownloadResourceService', () => {
  let service: ReservationDownloadResourceService;
  let higsApiService: HigsApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ReservationDownloadResourceService,
        HigsApiProvider
      ]
    });

    service = TestBed.get(ReservationDownloadResourceService);
    higsApiService = TestBed.get(HigsApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#search', () => {
    it('should call HigsApiProvider#get', () => {
      spyOn(higsApiService, 'get');
      service.search({});

      expect(higsApiService.get).toHaveBeenCalled();
    });

    it('should clean null parameters', () => {
      spyOn(higsApiService, 'get');
      service.search({ foo: null, bar: 'baz'});

      expect(higsApiService.get).toHaveBeenCalledWith('reservationDownload', { params: { bar: 'baz' } });
    });
  });

  describe('#getHistory', () => {
    it('should call HigsApiProvider#get', () => {
      spyOn(higsApiService, 'get');
      service.getHistory('123');

      expect(higsApiService.get).toHaveBeenCalledWith(`reservationDownload/123/history`);
    });
  });
});
