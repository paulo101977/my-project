import { Injectable } from '@angular/core';
import { HigsApiService, ItemsResponse } from '@inovacaocmnet/thx-bifrost';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReservationDownloadResourceService {

  constructor(
    private shared: SharedService,
    private api: HigsApiService
  ) { }

  public search(params): Observable<ItemsResponse<any>> {
    return this.api.get('reservationDownload', {
      params: this.shared.removeNullParams(params)
    });
  }

  public getHistory(reservationDownloadId: string) {
    return this.api.get(`reservationDownload/${reservationDownloadId}/history`);
  }
}
