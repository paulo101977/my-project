import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http/';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { Policy } from './../../models/dto/policy';
import { SharedService } from './../../services/shared/shared.service';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { GuestPolicy } from 'app/policy/models/guest-policy';

@Injectable({ providedIn: 'root' })
export class PolicyResource {
  private headers = this.sharedService.getHeaders();
  private url = 'PropertyPolicy';

  constructor(private pmsApi: ThexApiService,
              private sharedService: SharedService) {}

  public getAllPolicyList(propertyId: number): Observable<ItemsResponse<Policy>> {
    return this.pmsApi.get<ItemsResponse<Policy>>(`${this.url}/${propertyId}/getallpropertypolicy`);
  }

  public deletePolicy(id: number): Observable<HttpResponse<Object>> {
    return this.pmsApi.delete(`${this.url}/${id}/delete`, { observe: 'response' });
  }

  public createPolicy(policy: Policy): Observable<Policy> {
    return this.pmsApi.post<Policy>(this.url, policy);
  }

  public updatePolicy(policy: Policy): Observable<HttpResponse<Object>> {
    return this.pmsApi.put(this.url, policy, { observe: 'response' });
  }

  public updatePolicyStatus(policy: Policy): Observable<HttpResponse<Object>> {
    return this.pmsApi.patch(`${this.url}/${policy.id}/toggleactivation`, policy, { observe: 'response' });
  }

  public getAllGuestPolicies(): Observable<any> {
    return this.pmsApi.get('PropertyGuestPolicy');
  }

  public createGuestPolicy(guestPolicy: GuestPolicy): Observable<any> {
    return this.pmsApi.post('PropertyGuestPolicy', guestPolicy);
  }

  public editGuestPolicy(guestPolicy: GuestPolicy): Observable<any> {
    return this.pmsApi.put('PropertyGuestPolicy', guestPolicy);
  }

  public deleteGuestPolicy(id: string): Observable<any> {
    return this.pmsApi.delete(`PropertyGuestPolicy/${id}`);
  }
}
