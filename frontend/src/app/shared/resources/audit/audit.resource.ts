import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from '../../services/shared/shared.service';
import { TimeForAudit } from '../../../audit/models/time-for-audit';
import { AuditStepDto } from '../../../audit/models/audit-step-dto';
import { AuditStepProcessDto } from '../../../audit/models/audit-step-process-dto';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class AuditResource {
  private url = 'PropertyAuditProcess/';

  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getTimeForAudit(propertyId: number) {
    return this.httpClient.get<TimeForAudit>(this.url + `${propertyId}/GetTimeForAudit`);
  }

  public getAuditStep(propertyId: number) {
    return this.httpClient.get<AuditStepDto>(this.url + `${propertyId}/GetAuditStep`);
  }

  public startAuditProcess(propertyId: number) {
    return this.httpClient.patch<AuditStepProcessDto>(this.url + `${propertyId}`, null);
  }

  public getSystemDate(propertyId: number) {
    return this.httpClient.get<TimeForAudit>(this.url + `${propertyId}/GetSystemDate`);
  }
}
