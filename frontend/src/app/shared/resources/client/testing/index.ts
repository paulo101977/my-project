import { createServiceStub } from '../../../../../../testing';
import { ClientResource } from 'app/shared/resources/client/client.resource';
import { Observable, of } from 'rxjs';
import { CompanyClientAccount } from 'app/shared/models/dto/company-client-account-dto';
import { Client } from 'app/shared/models/client';
import { GetAllCompanyClientResultDto } from 'app/shared/models/dto/client/get-all-company-client-result-dto';
import { CompanyClientDto } from 'app/shared/models/dto/rate-plan/company-client-dto';
import { ClientContact } from 'app/shared/models/clientContact';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { ClientDto } from 'app/shared/models/dto/client/client-dto';

export const clientResourceStub = createServiceStub(ClientResource, {
    updateClientStatus(clientItem: any): Observable<GetAllCompanyClientResultDto> {
        return of( null );
    },

    getReservationCompanyClientAccount(propertyId: number): Observable<ItemsResponse<CompanyClientAccount>> {
        return of( null );
    },

    getClientsBySearchData(propertyId: number, data: string): Observable<Client[]> {
        return of( null );
    },

    getClientById(id: string): Observable<ClientDto> {
        return of( null );
    },

    getAllWithTributeWithheld(): Observable<Array<CompanyClientDto>> {
        return of( null );
    },

    getAllCompanyClientResultDto(propertyId: number): Observable<ItemsResponse<GetAllCompanyClientResultDto>> {
        return of( null );
    },

    editClient(clientItem: any): Observable<any> {
        return of( null );
    },

    deleteAssociationWithTributeWithheld(companyClientId: string): Observable<any> {
        return of( null );
    },

    createClient(clientItem: any): Observable<any> {
        return of( null );
    },

    associateClientListWithTributsWithheld(dtoList: Array<CompanyClientDto>): Observable<any> {
        return of( null );
    },

    getContactsOfClientsBySearchData(propertyId: number, companyClientId: string, searchData: string):
        Observable<Array<ClientContact>> {
            return of( null );
    }
});

