import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GetAllCompanyClientResultDto } from '../../models/dto/client/get-all-company-client-result-dto';
import { ClientContact } from '../../models/clientContact';
import { Client } from '../../models/client';
import { ClientDto } from '../../models/dto/client/client-dto';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { CompanyClientAccount } from 'app/shared/models/dto/company-client-account-dto';
import { CompanyClientDto } from 'app/shared/models/dto/rate-plan/company-client-dto';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { LoaderInterceptor } from 'app/core/interceptors/loader.interceptor';

@Injectable({ providedIn: 'root' })
export class ClientResource {
  constructor(private thexApi: ThexApiService,
              private loaderInterceptor: LoaderInterceptor ) {}

  public getAllCompanyClientResultDto(propertyId: number): Observable<ItemsResponse<GetAllCompanyClientResultDto>> {
    return this.thexApi.get<ItemsResponse<GetAllCompanyClientResultDto>>(
      `CompanyClient?PropertyId=${propertyId}`,
    );
  }

  public getClientsBySearchData(propertyId: number, data: string): Observable<Client[]> {
    this.loaderInterceptor.disableOnce();
    return this.thexApi
      .get<ItemsResponse<Client>>(`CompanyClient/search?PropertyId=${propertyId}&SearchData=${data}`).pipe(
      map(response => response.items));
  }

  public getContactsOfClientsBySearchData(
    propertyId: number,
    companyClientId: string,
    searchData: string,
  ): Observable<Array<ClientContact>> {
    this.loaderInterceptor.disableOnce();
    return this.thexApi
      .get<ItemsResponse<ClientContact>>(
        `CompanyClient/contactpersons?PropertyId=${propertyId}&CompanyClientId=${companyClientId}&SearchData=${searchData}`,
      ).pipe(
        map(response => {
          return response.items as Array<ClientContact>;
        })
      );
  }

  public getClientById(id: string): Observable<ClientDto> {
    return this.thexApi.get<ClientDto>(`CompanyClient/${id}`).pipe(map(data => {
      data.documentList.forEach(doc => {
        doc['documentTypeName'] = doc.documentType.name;
      });
      return data;
    }));
  }

  public editClient(clientItem: any): Observable<any> {
    return this.thexApi.put(`CompanyClient/${clientItem.id}`, clientItem);
  }

  public createClient(clientItem: any): Observable<any> {
    return this.thexApi.post(`CompanyClient/`, clientItem);
  }

  public updateClientStatus(clientItem: any): Observable<GetAllCompanyClientResultDto> {
    return this.thexApi.patch<GetAllCompanyClientResultDto>(
      `CompanyClient/${clientItem.id}/toggleactivation`,
      clientItem,
    );
  }

  public getReservationCompanyClientAccount(propertyId: number): Observable<ItemsResponse<CompanyClientAccount>> {
    return this.thexApi.get<ItemsResponse<CompanyClientAccount>>(
      `ReservationCompanyClient/${propertyId}`,
    );
  }

  public associateClientListWithTributsWithheld(dtoList: Array<CompanyClientDto>) {
    return this.thexApi.patch<any>(
      `CompanyClient/associatewithtributswithheld`, dtoList
    );
  }

  public getAllWithTributeWithheld(): Observable<Array<CompanyClientDto>> {
    return this.thexApi.get<Array<CompanyClientDto>>(
    `CompanyClient/getallwithtributwithheld`
    );
  }

  public deleteAssociationWithTributeWithheld(companyClientId: string): Observable<any> {
    return this.thexApi.delete(
      `CompanyClient/deleteassociationwithtributwithheld/${companyClientId}`
    );
  }
}
