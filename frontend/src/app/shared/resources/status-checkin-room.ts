export class StatusCheckInRooms {
  parentRoomId: number;
  propertyId: number;
  roomTypeId: number;
  id: number;
  building: string;
  wing: string;
  floor: string;
  roomNumber: string;
  remarks: string;
  isActive: boolean;
  housekeepingStatusPropertyId: string;
  color: string;
}
