import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http/';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { Brand } from './../../models/brand';
import { SharedService } from './../../services/shared/shared.service';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class BrandResource {
  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getAllBrand(): Observable<ItemsResponse<Brand>> {
    return this.httpClient.get<ItemsResponse<Brand>>('Brand/');
  }
}
