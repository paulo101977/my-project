import { createServiceStub } from '../../../../../../testing';
import { PaymentTypeResource } from 'app/shared/resources/payment-type/payment-type.resource';
import { Observable, of } from 'rxjs';
import { GetAllPaymentTypeDto } from 'app/shared/models/payment-type/payment-type';
import { PaymentTypeWithouBePlasticCardOrDebit } from 'app/shared/models/billingType/payment-type-withou-be-plastic-card-Debit';
import { PropertyPaymentTypeIssuingBank } from 'app/shared/models/payment-type/property-payment-type-issuing-bank';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { Acquirer } from 'app/shared/models/payment-type/acquirer';
import { HttpResponse } from '@angular/common/http';
import { PaymentType } from '../../../models/payment-types';
import { PlasticBrandProperty } from 'app/shared/models/payment-type/plastic-brand-property';

export const paymentTypeResourceStub = createServiceStub(PaymentTypeResource, {
    createPaymentTypePlasticCardOrDebit(propertyPaymentTypeIssuingBank: GetAllPaymentTypeDto): Observable<GetAllPaymentTypeDto> {
        return of( null );
    },

    createPaymentTypeWithoutBePlasticCardOrDebit(
        propertyPaymentTypeIssuingBank: PaymentTypeWithouBePlasticCardOrDebit): Observable<PaymentTypeWithouBePlasticCardOrDebit> {
        return of( null );
    },

    createPropertyPaymentTypeIssuingBank(propertyPaymentTypeIssuingBank: PropertyPaymentTypeIssuingBank):
        Observable<PropertyPaymentTypeIssuingBank> {
        return of( null );
    },

    delete(propertyId: number, paymentTypeId: number, acquirerId: string): Observable<HttpResponse<any>> {
        return of( null );
    },

    getAcquirerList(propertyId: number): Observable<ItemsResponse<Acquirer>> {
        return of( null );
    },

    getAllPaymentsType(propertyId: number): Observable<ItemsResponse<GetAllPaymentTypeDto>> {
        return of( null );
    },

    getPaymentsTypeUsed(propertyId): Observable<ItemsResponse<any>> {
        return of( null );
    },

    getPaymentTypeList(propertyId: number): Observable<ItemsResponse<PaymentType>> {
        return of( null );
    },

    getPaymentTypePlasticCardOrDebit(
        propertyId: number, paymentTypeId: number, acquirerId: string): Observable<GetAllPaymentTypeDto> {
        return of( null );
    },

    getPaymentTypeWithoutBePlasticCardOrDebit(propertyId: number, paymentTypeId: number): Observable<GetAllPaymentTypeDto> {
        return of( null );
    },

    getPlasticBrandPropertyList(propertyId: number): Observable<ItemsResponse<PlasticBrandProperty>> {
        return of( null );
    },

    getPropertyPaymentTypeIssuingBankByPaymentTypeId(
        propertyId: number, paymentTypeId: number): Observable<PropertyPaymentTypeIssuingBank> {
        return of( null );
    },

    toggleStatus(propertyId: number, paymentTypeId: number, acquirerId: string, toggleValue: boolean): Observable<HttpResponse<any>> {
        return of( null );
    },

    updatePaymentTypePlasticCardOrDebit(propertyPaymentTypeIssuingBank: GetAllPaymentTypeDto): Observable<GetAllPaymentTypeDto> {
        return of( null );
    },

    updatePaymentTypeWithoutBePlasticCardOrDebit(propertyPaymentTypeIssuingBank: PaymentTypeWithouBePlasticCardOrDebit):
        Observable<PaymentTypeWithouBePlasticCardOrDebit> {
        return of( null );
    }
});
