import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http/src/response';
import { Acquirer } from './../../models/payment-type/acquirer';
import { PaymentType } from './../../models/payment-types';
import { PlasticBrandProperty } from 'app/shared/models/payment-type/plastic-brand-property';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { PropertyPaymentTypeIssuingBank } from './../../models/payment-type/property-payment-type-issuing-bank';
import { GetAllPaymentTypeDto } from './../../models/payment-type/payment-type';
import { PaymentTypeWithouBePlasticCardOrDebit } from './../../models/billingType/payment-type-withou-be-plastic-card-Debit';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { HttpService } from 'app/core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class PaymentTypeResource {
  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getPaymentTypeList(propertyId: number): Observable<ItemsResponse<PaymentType>> {
    return this.httpClient.get<ItemsResponse<PaymentType>>(
      `PaymentType/forcreation?PropertyId=${propertyId}`,
    );
  }

  public getPlasticBrandPropertyList(propertyId: number): Observable<ItemsResponse<PlasticBrandProperty>> {
    return this.httpClient.get<ItemsResponse<PlasticBrandProperty>>(
      `PlasticBrandProperty?PropertyId=${propertyId}`,
    );
  }

  public getAcquirerList(propertyId: number): Observable<ItemsResponse<Acquirer>> {
    return this.httpClient.get<ItemsResponse<Acquirer>>(
      `CompanyClient/isacquirer?PropertyId=${propertyId}`,
    );
  }

  public createPropertyPaymentTypeIssuingBank(
    propertyPaymentTypeIssuingBank: PropertyPaymentTypeIssuingBank,
  ): Observable<PropertyPaymentTypeIssuingBank> {
    return this.httpClient.post<PropertyPaymentTypeIssuingBank>(
      `PropertyPaymentTypeIssuingBank`,
      propertyPaymentTypeIssuingBank,
    );
  }

  public updatePaymentTypeWithoutBePlasticCardOrDebit(
    propertyPaymentTypeIssuingBank: PaymentTypeWithouBePlasticCardOrDebit,
  ): Observable<PaymentTypeWithouBePlasticCardOrDebit> {
    return this.httpClient.put<PaymentTypeWithouBePlasticCardOrDebit>(
      `billingitem/paymenttype`,
      propertyPaymentTypeIssuingBank,
    );
  }

  public updatePaymentTypePlasticCardOrDebit(propertyPaymentTypeIssuingBank: GetAllPaymentTypeDto): Observable<GetAllPaymentTypeDto> {
    return this.httpClient.put<GetAllPaymentTypeDto>(
      `billingitem/paymenttype`,
      propertyPaymentTypeIssuingBank,
    );
  }

  public getPropertyPaymentTypeIssuingBankByPaymentTypeId(
    propertyId: number,
    paymentTypeId: number,
  ): Observable<PropertyPaymentTypeIssuingBank> {
    return this.httpClient.get<PropertyPaymentTypeIssuingBank>(
      `PropertyPaymentTypeIssuingBank/${propertyId}/${paymentTypeId}/bypaymenttype`,
    );
  }

  public getPropertyPaymentTypeIssuingBankByPaymentTypeIdAndBankId(
    propertyId: number,
    paymentTypeId: number,
    bankId: string,
  ): Observable<PropertyPaymentTypeIssuingBank> {
    return this.httpClient.get<PropertyPaymentTypeIssuingBank>(
      `PropertyPaymentTypeIssuingBank/${propertyId}/${paymentTypeId}/${bankId}`,
    );
  }

  public getAllPaymentsType(propertyId: number): Observable<ItemsResponse<GetAllPaymentTypeDto>> {
    return this.httpClient.get<ItemsResponse<GetAllPaymentTypeDto>>(
      `billingitem/${propertyId}/allpaymenttypes`,
    );
  }

  public getPaymentsTypeUsed(propertyId): Observable<ItemsResponse<any>> {
    return this.httpClient.get<ItemsResponse<GetAllPaymentTypeDto>>(
      `billingitem/paymenttypes`,
      {params: { propertyId }}
    );
  }

  public toggleStatus(propertyId: number, paymentTypeId: number, acquirerId: string, toggleValue: boolean): Observable<HttpResponse<any>> {
    return this.httpClient.patch(
      `billingitem/${propertyId}/${paymentTypeId}/${acquirerId}/${toggleValue}/paymenttype`,
      {},
      { observe: 'response' },
    );
  }

  public delete(propertyId: number, paymentTypeId: number, acquirerId: string): Observable<HttpResponse<any>> {
    return this.httpClient.delete(
      `billingitem/${propertyId}/${paymentTypeId}/${acquirerId}/paymenttype`,
      { observe: 'response' },
    );
  }

  public getPaymentTypePlasticCardOrDebit(propertyId: number, paymentTypeId: number, acquirerId: string): Observable<GetAllPaymentTypeDto> {
    return this.httpClient.get<GetAllPaymentTypeDto>(
      `billingitem/${propertyId}/${paymentTypeId}/${acquirerId}/getpaymenttype`,
    );
  }

  public getPaymentTypeWithoutBePlasticCardOrDebit(propertyId: number, paymentTypeId: number): Observable<GetAllPaymentTypeDto> {
    return this.httpClient.get<GetAllPaymentTypeDto>(
      `billingitem/${propertyId}/${paymentTypeId}/getpaymenttype`,
    );
  }

  public createPaymentTypePlasticCardOrDebit(propertyPaymentTypeIssuingBank: GetAllPaymentTypeDto): Observable<GetAllPaymentTypeDto> {
    return this.httpClient.post<GetAllPaymentTypeDto>(
      `billingitem/paymenttype`,
      propertyPaymentTypeIssuingBank,
    );
  }

  public createPaymentTypeWithoutBePlasticCardOrDebit(
    propertyPaymentTypeIssuingBank: PaymentTypeWithouBePlasticCardOrDebit,
  ): Observable<PaymentTypeWithouBePlasticCardOrDebit> {
    return this.httpClient.post<PaymentTypeWithouBePlasticCardOrDebit>(
      `billingitem/paymenttype`,
      propertyPaymentTypeIssuingBank,
    );
  }
}
