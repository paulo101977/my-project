import { ChannelResource } from 'app/shared/resources/channel/channel.resource';
import { of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing';

export const channelResourceStub = createServiceStub(ChannelResource, {
  add: () => of(<any>{}),
  edit: () => of(<any>{}),
  getOptionList: () => of(<any>{}),
  getOriginList: () => of(<any>{}),
});

