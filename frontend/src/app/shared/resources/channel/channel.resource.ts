import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { Channel, ChannelCode, ChannelOrigin } from '../../models/integration/channel';
import { HttpService } from '../../../core/services/http/http.service';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { HttpParams } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class ChannelResource {

  url = 'Channel';
  urlCode = 'ChannelCode';
  urlOrigin = 'BusinessSource'; // ?PropertyId=11&BringsInactive=false

  constructor(private httpClient: HttpService) {
  }

  public getAllChannelList({all, searchData}: {all?: boolean, searchData?: string}): Observable<ItemsResponse<Channel>> {
    let params = new HttpParams();

    if (all) {
      params = params.append('all', String(all));
    }

    if (searchData) {
      params = params.append('searchData', searchData);
    }

    return this
      .httpClient
      .get<ItemsResponse<Channel>>(this.url, {params: params});
  }

  public add(item: Channel): Observable<Channel> {
    return this
      .httpClient
      .post<Channel>(this.url, item);
  }

  public edit(item: Channel): Observable<Channel> {
    const {id} = item;
    return this
      .httpClient
      .put<Channel>(`${this.url}/${id}`, item);
  }

  // TODO: change model any
  public getOptionList(): Observable<any> {
    return this
      .httpClient
      .get<ChannelCode[]>(this.urlCode);
  }

  // TODO: change model any
  public getOriginList(propertyId: number): Observable<any> {
    return this
      .httpClient
      .get<ChannelOrigin[]>(`${this.urlOrigin}?PropertyId=${propertyId}&BringsInactive=false`);
  }

  public checkAvailableChannelAssociation(channelId: string, companyId: string): Observable<boolean> {
    const params = new HttpParams()
      .set('channelid', channelId)
      .set('companyid', companyId);

    return this
      .httpClient
      .get<boolean>('CompanyClient/checkavailablechannelassociation', {params: params});
  }

}
