import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { SharedService } from './../../services/shared/shared.service';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class ReleaseUhResource {
  private headers = this.sharedService.getHeaders();

  constructor(private http: HttpService, private sharedService: SharedService) {}

  releaseUh(reservationItemId: number): Observable<any> {
    return this.http.patch('Reservationitems/' + reservationItemId + '/realease', this.headers);
  }
}
