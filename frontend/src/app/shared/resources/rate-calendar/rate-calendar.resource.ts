import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http/';
import { ConfigurationDataParameters } from '../../models/revenue-management/configuration-data-parameters';
import { SharedService } from '../../services/shared/shared.service';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { RateCalendarDayDto } from '../../models/revenue-management/rate-calendar-day-dto';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class RateCalendarResource {
  private url = 'rateCalendar/';

  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getConfigurationDataParameters(propertyId: number): Observable<ConfigurationDataParameters> {
    return this.httpClient.get<ConfigurationDataParameters>(this.url + `${propertyId}/calendarParameters`);
  }

  public getCalendar(propertyId: number, filter: any): Observable<ItemsResponse<RateCalendarDayDto>> {
    let childrenAges = null;
    if (filter && filter.childrenAges && filter.childrenAges.length > 0) {
      childrenAges = [...filter.childrenAges];
    }
    delete filter.childrenAges;
    let params = new HttpParams({ fromObject: <any>filter });
    if (childrenAges) {
      (<Array<number>>childrenAges).forEach( value => {
        params = params.append('childrenAge', `${value}`);
      });
    }
    return this.httpClient.get<ItemsResponse<RateCalendarDayDto>>(this.url + propertyId, { params: params });
  }
}
