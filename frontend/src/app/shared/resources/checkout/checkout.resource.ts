import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from '../../services/shared/shared.service';
import { Observable ,  forkJoin } from 'rxjs';
import { RpsPrintDto } from '../../models/dto/rps/rps-print-dto';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class CheckoutResource {
  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public checkEarlyCheckout(propertyId: number, reservationItemId: number): Observable<any> {
    return this.httpClient.get(
      `Reservationitems/property/${propertyId}/reservationitem/${reservationItemId}/earlyCheckout`,
    );
  }

  public chekcoutAcomodation(propertyId: number, reservationItemId: number): Observable<any> {
    return this.httpClient.patch(
      `Reservationitems/property/${propertyId}/reservationitem/${reservationItemId}/checkout`,
      null,
    );
  }

  public checkoutGuest(propertyId: number, guestReservationItemId: number): Observable<any> {
    return this.httpClient.patch(
      `GuestReservationItem/property/${propertyId}/guestreservationitem/${guestReservationItemId}/checkout`,
      null,
    );
  }

  public completeCheckout(propertyId: number, reservationItemId: number, guestReservationItemidList: Array<number>) {
    const requestArray = [];
    requestArray.push(this.chekcoutAcomodation(propertyId, reservationItemId));
    if (guestReservationItemidList) {
      for (const guestReservationItemid of guestReservationItemidList) {
        requestArray.push(this.checkoutGuest(propertyId, guestReservationItemid));
      }
    }
    return forkJoin(requestArray);
  }

  public getInvoiceInfo(propertyId, invoiceId, language): Observable<RpsPrintDto> {
    return this.httpClient.get<RpsPrintDto>(
      `BillingInvoiceIntegration/property/${propertyId}/invoice/${invoiceId}/languague/${language}/rps`,
    );
  }
}
