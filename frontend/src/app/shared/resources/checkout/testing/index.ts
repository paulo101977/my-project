import { createServiceStub } from '../../../../../../testing';
import { CheckoutResource } from 'app/shared/resources/checkout/checkout.resource';
import { Observable, of } from 'rxjs';
import { RpsPrintDto } from 'app/shared/models/dto/rps/rps-print-dto';

export const checkoutResourceStub = createServiceStub(CheckoutResource, {
    checkEarlyCheckout(propertyId: number, reservationItemId: number): Observable<any> {
        return of(null);
    },

    checkoutGuest(propertyId: number, guestReservationItemId: number): Observable<any> {
        return of(null);
    },

    chekcoutAcomodation(propertyId: number, reservationItemId: number): Observable<any> {
        return of(null);
    },

    completeCheckout(
        propertyId: number, reservationItemId: number, guestReservationItemidList: Array<number>
    ): Observable<any[]> {
        return of(null);
    },

    getInvoiceInfo(propertyId, invoiceId, language): Observable<RpsPrintDto> {
        return of(null);
    }
});

