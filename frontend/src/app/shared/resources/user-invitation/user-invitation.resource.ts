import { Injectable } from '@angular/core';

import { UserInvitation } from '../../models/user-invitation';
import { SharedService } from '../../services/shared/shared.service';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class UserInvitationResource {
  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public invite(userInvitation: UserInvitation) {
    return this.httpClient.post(`UserInvitation`, userInvitation);
  }
}
