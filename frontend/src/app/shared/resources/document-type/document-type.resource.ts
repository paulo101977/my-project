import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SharedService } from './../../services/shared/shared.service';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { ClientDocumentTypesDto } from '../../models/dto/client/client-document-types';
import { HttpService } from '../../../core/services/http/http.service';
import { SupportApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class DocumentTypeResource {
  private headers = this.sharedService.getHeaders();

  constructor(private httpClient: HttpService,
              private sharedService: SharedService,
              private supportApi: SupportApiService) {}

  public getDocumentTypesByPersonTypeAndCountryCode(
    personType: string,
    countryCode: string,
  ): Observable<ItemsResponse<ClientDocumentTypesDto>> {
    return this.supportApi.get<ItemsResponse<ClientDocumentTypesDto>>(
      `Documenttypes/personType/${personType}/countryCode/${countryCode}`,
    );
  }

  public getAllDocumentTypes(): Observable<any> {
    return this.supportApi.get<ItemsResponse<ClientDocumentTypesDto>>(
      `Documenttypes/allbynaturalpersontype`,
    );
  }

  public getAll(): Observable<any> {
    return this.supportApi.get<ItemsResponse<ClientDocumentTypesDto>>(
      `Documenttypes`,
    );
  }

  public getAllByPropertyCountryCode(): Observable<any> {
    return this.supportApi.get<ItemsResponse<ClientDocumentTypesDto>>(
      `DocumentTypes/getallbypropertycountrycode`,
    );
  }
}
