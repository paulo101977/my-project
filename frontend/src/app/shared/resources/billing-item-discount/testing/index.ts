import { createServiceStub } from '../../../../../../testing';
import { BillingItemDiscountResource } from 'app/shared/resources/billing-item-discount/billing-item-discount.resource';
import { BillingItemDiscountDto } from 'app/shared/models/dto/billing-item-discount-dto';
import { Observable, of } from 'rxjs';

export const billingItemDiscountResourceStug = createServiceStub(BillingItemDiscountResource, {
    applyDiscountTo(itens: Array<BillingItemDiscountDto>): Observable<any> {
        return of(null);
    }
});
