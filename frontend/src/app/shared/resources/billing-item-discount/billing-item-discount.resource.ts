import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from '../../services/shared/shared.service';
import { BillingItemDiscountDto } from '../../models/dto/billing-item-discount-dto';
import { Observable } from 'rxjs';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class BillingItemDiscountResource {
  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  applyDiscountTo(itens: Array<BillingItemDiscountDto>): Observable<any> {
    return this.httpClient.post('BillingAccountItem/discount', itens);
  }
}
