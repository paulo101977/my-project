import { createServiceStub } from '../../../../../../testing';
import { BillingItemResource } from 'app/shared/resources/billingItem/billing-item.resource';
import { Observable, of } from 'rxjs';
import { ItemsResponse } from '../../../../../../projects/rms/src/lib/premise/models/item-response';
import { BillingItemPlasticBrandCompanyClient } from 'app/shared/models/billingType/billing-item-plastic-brand-company-client';
import { PaymentTypeWithBillingItem } from 'app/shared/models/billingType/payment-type-with-billing-item';
import { BillingItemPaymentTypeDetailList } from 'app/billing-account/models/payment-billing-item';
import { PlasticCard } from 'app/billing-account/models/plastic-card';

export const billingItemResourceStub = createServiceStub(BillingItemResource, {
    getAllBillingItemPlasticBrandCompanyClientListByPaymentTypeId(paymentTypeId: number):
        Observable<ItemsResponse<BillingItemPlasticBrandCompanyClient>> {
        return of( null );
    },

    getAllPaymentTypeWithBillingItemListByPropertyId(propertyId: number):
        Observable<ItemsResponse<PaymentTypeWithBillingItem>> {
        return of( null );
    },

    getBillingItemPaymentType(propertyId: number): Observable<ItemsResponse<BillingItemPaymentTypeDetailList>> {
        return of( null );
    },

    getBillingItemPlasticCard(paymentTypeId: number): Observable<ItemsResponse<PlasticCard>> {
        return of( null );
    }
});
