import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http/';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { BillingItemTaxServiceAssociate } from './../../models/billingType/billing-item-tax-service-associate';
import { GetAllBillingItemTax } from 'app/shared/models/billingType/get-all-billing-item-tax';
import { GetAllBillingItemService } from 'app/shared/models/billingType/get-all-billing-item-service';
import { SharedService } from './../../services/shared/shared.service';
import { BillingItemTax } from 'app/shared/models/billingType/billing-item-tax';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class BillingItemTaxResource {
  private headers = this.sharedService.getHeaders();
  private url = 'billingitem';
  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getBillingItemTaxServicesAssociatedList(billingId: number): Observable<ItemsResponse<BillingItemTaxServiceAssociate>> {
    return this.httpClient.get<ItemsResponse<BillingItemTaxServiceAssociate>>(
      `billingitem/${billingId}/billingitem`,
    );
  }

  public saveBillingItemTaxAndServicesAssociated(billingItemTax: BillingItemTax): Observable<BillingItemTax> {
    return this.httpClient.post<BillingItemTax>(`billingitem/createtaxwithservice`, billingItemTax);
  }

  public getBillingItemTaxServicesAssociated(billingId: number, propertyId: number): Observable<BillingItemTax> {
    return this.httpClient.get<BillingItemTax>(
      `billingitem/${billingId}/property/${propertyId}/billingitem`,
    );
  }

  public updateBillingItemTaxWithService(billingItemTax: any): Observable<any> {
    return this.httpClient.put<BillingItemTax>(
      `billingitem/updatebillingitemtaxwithservice`,
      billingItemTax,
    );
  }

  public getAllBillingItemTaxListByPropertyId(propertyId: number): Observable<ItemsResponse<GetAllBillingItemTax>> {
    return this.httpClient.get<ItemsResponse<GetAllBillingItemTax>>(`${this.url}/${propertyId}/getallbillingitemtax`);
  }

  public deleteBillingItemTax(id: number, propertyId: number): Observable<HttpResponse<Object>> {
    return this.httpClient.delete(`${this.url}/${id}/property/${propertyId}`, { observe: 'response' });
  }

  public updateBillingItemTaxStatus(id: number): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${id}/toggleactivation`, {}, { observe: 'response' });
  }

  public getServiceAssociateList(propertyId: number): Observable<ItemsResponse<GetAllBillingItemService>> {
    return this.httpClient.get<ItemsResponse<GetAllBillingItemService>>(
      `billingitem/${propertyId}/getAllbillingitemforassociateservice`,
    );
  }
}
