import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { PaymentTypeWithBillingItem } from './../../models/billingType/payment-type-with-billing-item';
import { BillingItemPlasticBrandCompanyClient } from './../../models/billingType/billing-item-plastic-brand-company-client';
import { SharedService } from './../../services/shared/shared.service';
import { HttpService } from '../../../core/services/http/http.service';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { PlasticCard } from 'app/billing-account/models/plastic-card';
import { BillingItemPaymentTypeDetailList } from 'app/billing-account/models/payment-billing-item';

@Injectable({ providedIn: 'root' })
export class BillingItemResource {
  private url = 'billingitem';

  constructor(private thexApiService: ThexApiService, private sharedService: SharedService) {}

  public getAllPaymentTypeWithBillingItemListByPropertyId(propertyId: number): Observable<ItemsResponse<PaymentTypeWithBillingItem>> {
    return this
        .thexApiService
        .get<ItemsResponse<PaymentTypeWithBillingItem>>(`${this.url}/paymenttypes?PropertyId=${propertyId}`);
  }

  public getAllBillingItemPlasticBrandCompanyClientListByPaymentTypeId(
    paymentTypeId: number,
  ): Observable<ItemsResponse<BillingItemPlasticBrandCompanyClient>> {
    return this
        .thexApiService
        .get<ItemsResponse<BillingItemPlasticBrandCompanyClient>>(`${this.url}/${paymentTypeId}/plasticbrand`);
  }


  public getBillingItemPlasticCard(paymentTypeId: number): Observable<ItemsResponse<PlasticCard>> {
    return this
      .thexApiService
      .get<ItemsResponse<PlasticCard>>(`${this.url}/${paymentTypeId}/plasticbrand`);
  }

  public getBillingItemPaymentType(propertyId: number): Observable<ItemsResponse<BillingItemPaymentTypeDetailList>> {
    return this
      .thexApiService
      .get<ItemsResponse<BillingItemPaymentTypeDetailList>>(`${this.url}/${propertyId}/allpaymenttypes`);
  }
}
