import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http/';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { BillingItemService } from '../../models/billingType/billing-item-service';
import { GetAllBillingItemService } from '../../models/billingType/get-all-billing-item-service';
import { GetAllBillingItemTax } from 'app/shared/models/billingType/get-all-billing-item-tax';
import { BillingItemCategory } from '../../models/billingType/billing-item-category';
import { SharedService } from '../../services/shared/shared.service';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class BillingItemServiceResource {
  private url = 'billingitem';

  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public createBillingItemServicesWithTaxAssociated(billingItemService: BillingItemService): Observable<BillingItemService> {
    return this.httpClient.post<BillingItemService>(`${this.url}/createservicewithtax`, billingItemService);
  }

  public updateBillingItemServicesWithTaxAssociated(billingItemService: BillingItemService): Observable<HttpResponse<Object>> {
    return this.httpClient.put(`${this.url}/updatebillingitemservicewithtax`, billingItemService, { observe: 'response' });
  }

  public getServiceById(id: number, propertyId: number): Observable<BillingItemService> {
    return this.httpClient.get<BillingItemService>(`${this.url}/${id}/property/${propertyId}/billingitem`);
  }

  public getAllBillingItemServiceListByPropertyId(propertyId: number): Observable<ItemsResponse<GetAllBillingItemService>> {
    return this.httpClient.get<ItemsResponse<GetAllBillingItemService>>(`${this.url}/${propertyId}/getallbillingitemservice`);
  }

  public deleteBillingItemService(id: number, propertyId: number): Observable<HttpResponse<Object>> {
    return this.httpClient.delete(`${this.url}/${id}/property/${propertyId}`, { observe: 'response' });
  }

  public updateBillingItemServiceStatus(id: number): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${id}/toggleactivation`, {}, { observe: 'response' });
  }

  public getAllBillingItemTaxToAssociateList(propertyId: number) {
    return this.httpClient.get<ItemsResponse<GetAllBillingItemTax>>(`${this.url}/${propertyId}/getAllbillingitemforassociatetax`);
  }

  public getAllBillingItemCategoryActiveListByPropertyId(propertyId: number): Observable<ItemsResponse<BillingItemCategory>> {
    return this.httpClient.get<ItemsResponse<BillingItemCategory>>(
      `billingitemcategory/${propertyId}/getallcategoriesforitem?PageSize=1000`,
    );
  }

  public searchBillingItemServiceListByCriteria(propertyId: number): Observable<ItemsResponse<BillingItemService>> {
    return this.httpClient.hideLoader().get<ItemsResponse<BillingItemService>>(`${this.url}/${propertyId}/getallserviceforrealese`);
  }
}
