import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { Proforma } from '../../../shared/models/proforma';

@Injectable({ providedIn: 'root' })
export class ProformaResource {
    constructor(
        private thexApi: ThexApiService
      ) {}

    public issueProforma(proforma: Proforma): Observable<object> {
        return this.thexApi.post(`proforma`, proforma);
    }
}
