
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { SharedService } from './../../services/shared/shared.service';

import { RoomLayout } from './../../models/room-layout';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { Observable } from 'rxjs';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class RoomLayoutResource {
  private urlRoomLayoutGet = 'roomLayouts/byroomtype/id';

  constructor(private sharedService: SharedService, private httpClient: HttpService) {}

  public getRoomLayoutListByRoomTypeId(roomTypeId: number): Observable<RoomLayout[]> {
    return this.httpClient
      .get<ItemsResponse<RoomLayout>>(this.urlRoomLayoutGet.replace('id', roomTypeId.toString())).pipe(
      map(response => response.items));
  }
}
