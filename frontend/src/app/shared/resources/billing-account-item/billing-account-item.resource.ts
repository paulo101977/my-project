import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BillingAccountItem } from './../../models/billing-account/billing-account-item';
import { BillingAccountItemReverse } from './../../models/billing-account/billing-account-item-reverse';
import { BillingAccountItemTransfer } from './../../models/billing-account/billing-account-item-transfer';
import { SharedService } from './../../services/shared/shared.service';
import { HttpService } from '../../../core/services/http/http.service';
import {BillingAccountItemCreditAmountDto} from 'app/shared/models/dto/billing-account-item/billing-account-item-credit-amount-dto';

@Injectable({ providedIn: 'root' })
export class BillingAccountItemResource {
  private headers = this.sharedService.getHeaders();
  private url = 'BillingAccountItem';
  readonly guidDefault = '00000000-0000-0000-0000-000000000000';

  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public createBillingAccountItemDebit(billingAccountItem: BillingAccountItem): Observable<any> {
    return this.httpClient.post<BillingAccountItem>(this.url + `/debit`, billingAccountItem);
  }

  public createBillingAccountItemCredit(billingAccountItem: BillingAccountItem): Observable<any> {
    return this.httpClient.post<BillingAccountItem>(this.url + `/credit`, billingAccountItem);
  }

  public reverseBillingAccountItem(billingAccountItemReverseList: Array<BillingAccountItemReverse>): Observable<any> {
    return this.httpClient.post<BillingAccountItem>(this.url + `/reversal`, {
      billingAccountItems: billingAccountItemReverseList,
      id: this.guidDefault,
    });
  }

  public transferBillingAccountItem(billingAccountItemTransfer: BillingAccountItemTransfer): Observable<any> {
    return this.httpClient.post<BillingAccountItemTransfer>(this.url + `/transfer`, billingAccountItemTransfer);
  }

  public undoTransferBillingAccountItem(billingAccountItemUndoTransfer: BillingAccountItemTransfer): Observable<any> {
    return this.httpClient.post<BillingAccountItemTransfer>(this.url + `/undotransfer`, billingAccountItemUndoTransfer);
  }

  public getAllBillingAccountItemCreditAmountByInvoiceId(invoiceId: string): Observable<Array<BillingAccountItemCreditAmountDto>> {
    return this.httpClient.get<Array<BillingAccountItemCreditAmountDto>>(
      `${this.url}/getallbillingaccountitemcreditamountbyinvoiceid/${invoiceId}`);
  }
}
