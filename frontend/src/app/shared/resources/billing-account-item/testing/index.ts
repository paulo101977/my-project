import { createServiceStub } from '../../../../../../testing';
import { BillingAccountItemResource } from 'app/shared/resources/billing-account-item/billing-account-item.resource';
import { BillingAccountItem } from 'app/shared/models/billing-account/billing-account-item';
import { of , Observable } from 'rxjs';
import { BillingAccountItemCreditAmountDto } from 'app/shared/models/dto/billing-account-item/billing-account-item-credit-amount-dto';
import { BillingAccountItemReverse } from 'app/shared/models/billing-account/billing-account-item-reverse';
import { BillingAccountItemTransfer } from 'app/shared/models/billing-account/billing-account-item-transfer';

export const billingAccountItemResourceStub = createServiceStub(BillingAccountItemResource, {
    createBillingAccountItemCredit(billingAccountItem: BillingAccountItem): Observable<any> {
        return of(null);
    },

    createBillingAccountItemDebit(billingAccountItem: BillingAccountItem): Observable<any> {
        return of(null);
    },

    getAllBillingAccountItemCreditAmountByInvoiceId(invoiceId: string): Observable<Array<BillingAccountItemCreditAmountDto>> {
        return of(null);
    },

    reverseBillingAccountItem(billingAccountItemReverseList: Array<BillingAccountItemReverse>): Observable<any> {
        return of(null);
    },

    transferBillingAccountItem(billingAccountItemTransfer: BillingAccountItemTransfer): Observable<any> {
        return of(null);
    },

    undoTransferBillingAccountItem(billingAccountItemUndoTransfer: BillingAccountItemTransfer): Observable<any> {
        return of(null);
    },
});
