import { createServiceStub } from '../../../../../../testing';
import { CurrencyExchangeResource } from 'app/shared/resources/currency-exchange/currency-exchange.resource';
import { of } from 'rxjs/internal/observable/of';
import { CurrencyExchange, InsertQuotationForm } from 'app/shared/models/currency-exchange';


export const currencyExchangeResourceStub = createServiceStub(CurrencyExchangeResource, {
  getCurrencies: () => of( {items: null, hasNext: false } ),
  getQuotationByPeriod:
    (propertyId: number, stDate: string, endDate: string, currencyId: string) => of({items: null, hasNext: false } ),
  updateQuotation: (item: CurrencyExchange) => of( null ),
  createQuotation: (quotationData: InsertQuotationForm) => of( null ),
  getQuotationList: () => of( null ),
});

