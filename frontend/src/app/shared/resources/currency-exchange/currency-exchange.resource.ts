import { Injectable } from '@angular/core';
import { HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CurrencyExchange, InsertQuotationForm } from '../../models/currency-exchange';
import { Currency } from '../../models/currency';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class CurrencyExchangeResource {
  private urlCurrency = `currency`;
  private urlCurrencyExchange = `PropertyCurrencyExchange`;

  constructor(private httpClient: HttpService) {}

  public getCurrencies(isQuotation?: boolean): Observable<ItemsResponse<Currency>> {
    let params = new HttpParams();
    if ( isQuotation ) {
      params = params.set('IsQuotation', 'true');
    }

    return this.httpClient.get<ItemsResponse<Currency>>(this.urlCurrency, {params});
  }

  public getQuotationCurrent(propertyId: number): Observable<CurrencyExchange> {
    return this.httpClient.get<CurrencyExchange>(`${this.urlCurrencyExchange}/${propertyId}/quotationcurrent`);
  }

  public getQuotationByPeriod(
    propertyId: number,
    stDate: string,
    endDate: string,
    currencyId: string,
  ): Observable<ItemsResponse<CurrencyExchange>> {
    const params = new HttpParams()
      .set('StartDate', stDate)
      .set('EndDate', endDate)
      .set('CurrencyId', currencyId.toString());

    return this
      .httpClient
      .get<ItemsResponse<CurrencyExchange>>(
    `${this.urlCurrencyExchange}/${propertyId}/quotationbyperiod`,
        { params, }
        );
  }

  public createQuotation(quotationData: InsertQuotationForm): Observable<CurrencyExchange> {
    return this
      .httpClient
      .post<CurrencyExchange>(`${this.urlCurrencyExchange}/insertquotation`, quotationData);
  }

  public updateQuotations(quotations: CurrencyExchange[]): Observable<HttpResponse<Object>> {
    return this
      .httpClient
      .put<HttpResponse<Object>>(`${this.urlCurrencyExchange}/updatequotationfuture`, quotations);
  }

  // TODO: create model
  public getQuotationByCurrencyId( currencyId: string ): Observable<any> {
    return this
      .httpClient
      .get(`${this.urlCurrencyExchange}/quotationByCurrencyId?CurrencyId=${currencyId}`);
  }

  public getQuotationList(): Observable<any> {
    return this
      .httpClient
      .get(`${this.urlCurrencyExchange}/currentquotationlist`);
  }

  public updateQuotation(item: CurrencyExchange): Observable<any> {
    return this
      .httpClient
      .patch(`${this.urlCurrencyExchange}`, item);
  }
}
