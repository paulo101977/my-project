import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MealPlanType } from '../../models/revenue-management/meal-plan-type';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { HttpResponse } from '@angular/common/http';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class MealPlanTypeResource {
  private url = 'PropertyMealPlanType';

  constructor(private httpClient: ThexApiService) {}

  public getAllMealPlanTypeList(propertyId: number): Observable<ItemsResponse<MealPlanType>> {
    return this.httpClient
      .get<ItemsResponse<MealPlanType>>(`${this.url}/${propertyId}/getallpropertymealplantype?all=true`);
  }

  public getActiveMealPlanTypeList(propertyId: number): Observable<ItemsResponse<MealPlanType>> {
    return this.httpClient
      .get<ItemsResponse<MealPlanType>>(`${this.url}/${propertyId}/getallpropertymealplantype?all=false`);
  }

  public updateMealPlanType(mealPlanTypeList: Array<MealPlanType>): Observable<any> {
    return this.httpClient.put(`${this.url}`, mealPlanTypeList);
  }

  public toggleActivation(id: string): Observable<HttpResponse<Object>> {
    return this.httpClient
      .patch(`${this.url}/${id}/toggleactivation`, {}, { observe: 'response' });
  }
}
