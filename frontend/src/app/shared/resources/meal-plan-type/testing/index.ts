import { createServiceStub } from '../../../../../../testing';
import { MealPlanTypeResource } from 'app/shared/resources/meal-plan-type/meal-plan-type.resource';
import { Observable, of } from 'rxjs';
import { MealPlanType } from 'app/shared/models/revenue-management/meal-plan-type';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { HttpResponse } from '@angular/common/http';

export const mealPlanTypeResourceStub = createServiceStub(MealPlanTypeResource, {
    getActiveMealPlanTypeList(propertyId: number): Observable<ItemsResponse<MealPlanType>> {
        return of( null );
    },

    getAllMealPlanTypeList(propertyId: number): Observable<ItemsResponse<MealPlanType>> {
        return of( null );
    },

    toggleActivation(id: string): Observable<HttpResponse<Object>> {
        return of( null );
    },

    updateMealPlanType(mealPlanTypeList: Array<MealPlanType>): Observable<any> {
        return of( null );
    }
});
