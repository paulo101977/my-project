import { createServiceStub } from '../../../../../../testing';
import { GovernanceAssocResource } from 'app/shared/resources/governance/governance.assoc.resource';
import { Observable, of } from 'rxjs';
import { GovernanceStatusResource } from 'app/shared/resources/governance/governance-status.resource';
import { Governance } from 'app/shared/models/governance/governance';
import {User} from 'app/shared/models/user';

export const governanceAssocResourceStub = createServiceStub(GovernanceAssocResource, {
  createHousekeepingGenerator (list): Observable<any> {
    return null;
  },
  housekeepingGenerator (list): Observable<any> {
    return null;
  },
  saveUserImage(item: User): Observable<User> {
    return null;
  }
});

export const governanceStatusResourceStub = createServiceStub(GovernanceStatusResource, {
  create(governance: Governance, propertyId): Observable<any> {
    return of(null);
  },

  delete(governanceId): Observable<any> {
    return of(null);
  },

  edit(governance: Governance): Observable<any> {
    return of(null);
  },

  getAll(propertyId): Observable<Governance[]> {
    return of(null);
  },

  toggleStatus(housekeepingId): Observable<any> {
    return of(null);
  }
});

