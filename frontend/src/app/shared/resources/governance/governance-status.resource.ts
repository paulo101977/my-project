
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


import { SharedService } from '../../services/shared/shared.service';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { Governance } from '../../models/governance/governance';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class GovernanceStatusResource {
  // solicitado pelo backend
  private DEFAULT_HOUSEKEEPINGSTATUSID = 6;

  private headers = this.sharedService.getHeaders();

  private endpoint = 'housekeepingstatusproperty';

  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  getAll(propertyId): Observable<Governance[]> {
    return this.httpClient
      .get<ItemsResponse<Governance>>(`${this.endpoint}/${propertyId}/getallhousekeepingstatusproperty`)
      .pipe(map(data => data.items));
  }

  toggleStatus(housekeepingId): Observable<any> {
    return this.httpClient.patch<any>(
      `${this.endpoint}/${housekeepingId}/toggleactivation`,
      this.headers,
    );
  }

  create(governance: Governance, propertyId): Observable<any> {
    governance.housekeepingStatusId = this.DEFAULT_HOUSEKEEPINGSTATUSID;
    governance.propertyId = propertyId;
    governance.visibleBtnIsActive = true;
    return this.httpClient.post<any>(this.endpoint, governance);
  }

  edit(governance: Governance) {
    return this.httpClient.put<any>(this.endpoint, governance);
  }

  delete(governanceId): Observable<any> {
    return this.httpClient.delete(`${this.endpoint}/${governanceId}/delete`);
  }
}
