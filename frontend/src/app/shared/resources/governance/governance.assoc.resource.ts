import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HousekeepingApiService } from '@inovacaocmnet/thx-bifrost';
import { HttpParams } from '@angular/common/http';
import { User } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class GovernanceAssocResource {

  constructor(
    private api: HousekeepingApiService
  ) {}

  public getGridUserByRoomId( id: string ) {
    const params = new HttpParams()
      .set('housekeepingRoomId', id);

    return this
      .api
      .get('housekeepingRoom/getGridUser', { params });
  }

  // TODO: create model
  public saveRoomsByUser(item): Observable<any> {
    return this
      .api
      .post<any>('housekeepingRoom', item);
  }

  public saveUserImage(item: User): Observable<User> {
    return this
      .api
      .put<User>('User/sendPhoto?id=' + item.id, item);
  }

  // TODO: create model
  public getRoomsByUser(id: string): Observable<any> {
    const params = new HttpParams()
      .set('userId', id);

    return this
      .api
      .get<any>(
        `user/getRoomByUserId`, { params })
      .pipe(map(data => data));
  }

  // TODO: create model
  public getRooms(): Observable<any> {
      return this
        .api
        .get<any>(
          `housekeepingRoom/getAllGrid`)
        .pipe(map(data => data));
  }

// TODO: create model
  public getEmployeeList(): Observable<any> {
      return this
        .api
        .get<any>('user/getUserRoomHistory?isFilterSystemDate=true');
  }

  public housekeepingGenerator(list): Observable<any> {
    return this
      .api
      .post<any>('housekeepingGenerator', list);
  }

  public createHousekeepingGenerator(list): Observable<any> {
    const maidList = { roomMaidList: list };
    return this
      .api
      .post<any>('housekeepingGenerator/CreateHouseKeeping', maidList);
  }

}
