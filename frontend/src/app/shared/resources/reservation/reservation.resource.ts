import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpParams, HttpResponse } from '@angular/common/http/';
import { Observable } from 'rxjs';

import { ReservationDto } from '../../models/dto/reservation-dto';
import { SharedService } from '../../services/shared/shared.service';
import { Reservation } from '../../models/reserves/reservation';
import { SearchReservationDto } from '../../models/dto/search-reservation-dto';
import { RoomListResultDto } from '../../models/dto/room-list-result-dto';
import { ReservationItemGuestView } from '../../models/reserves/reservation-item-guest-view';
import { GuestReservationItem } from '../../models/reserves/guest-reservation-item';
import { ItemsResponse, ReservationItemsResponse } from '../../models/backend-api/item-response';
import { ReservationRemark } from '../../models/reserves/reservation-remark';
import { FnrhSaveDto } from '../../models/dto/checkin/fnrh-save-dto';
import { ReservationSearchDto } from '@app/shared/models/dto/reservation-search-dto';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class ReservationResource {
  private url = 'reservations';
  private urlGet =
    'reservations/id?Expand=reservationItemList.reservationBudgetList,reservationItemList.currency,reservationItemList.ratePlan,' +
    'reservationItemList.gratuityType,reservationItemList.room,reservationItemList.roomLayout,' +
    'reservationItemList.guestReservationItemList,reservationItemList.requestedRoomType,reservationItemList.receivedRoomType,' +
    'ReservationConfirmationList.Plastic';
  private urlGetWithOnlyReservationItemList = 'reservations/id?Expand=reservationItemList.RatePlan';
  private urlGetReservationItems = 'Reservations/id/byreservationitem';
  private urlGetRoomList = 'Reservationitems/propertyId/reservationId/accommodations';
  private urlPost = 'Reservations';

  private urlUpdateAndAssociateGuestToReservationItem =
    'GuestReservationItem/guestReservationItemId/associate';
  private urlDisassociateGuestFromReservationItem =
    'GuestReservationItem/guestReservationItemId/disassociate';

  constructor(private httpClient: ThexApiService, private sharedService: SharedService) {}

  public createReservation(reservation: Reservation): Observable<ReservationDto> {
    return this.httpClient.post<ReservationDto>(`${this.urlPost}`, reservation.normalizeRequest());
  }

  public editReservation(reservation: Reservation): Observable<ReservationDto> {
    return this.httpClient.put<ReservationDto>(`${this.urlPost}/${reservation.id}`, reservation.normalizeRequest());
  }

  public getReservationItemWithGuestsByReservationId(reservationItemId: number): Observable<Reservation> {
    return this.httpClient.get<ReservationDto>(this.urlGetReservationItems.replace('id', reservationItemId.toString())).pipe(
      map(response => {
        const reservationDto = response;
        const reservation = new Reservation();
        reservation.normalizeResponseToGuestView(reservationDto);
        return reservation;
      }));
  }

  public getReservationItemsByReservationIdAndPropertyId(
    reservationId: number,
    propertyId: number,
  ): Observable<ReservationItemGuestView[]> {
    return this.httpClient
      .get<ItemsResponse<RoomListResultDto>>(
        this.urlGetRoomList.replace('propertyId', propertyId.toString())
          .replace('reservationId', reservationId.toString()),
      ).pipe(
      map(response => {
        const reservationItemGuestView = new ReservationItemGuestView();
        return reservationItemGuestView.normalizeResponseRoomList(response.items);
      }));
  }

  public getReservationById(reservationId: number): Observable<Reservation> {
    return this.httpClient.get<ReservationDto>(this.urlGet.replace('id', reservationId.toString())).pipe(map(response => {
      const reservation = new Reservation();
      reservation.normalizeResponse(response);
      return reservation;
    }));
  }

  public getGuestRegistrationByReservationitem(reservationItemId, propertyId, language): Observable<ItemsResponse<FnrhSaveDto>> {
    return this.httpClient.get<ItemsResponse<FnrhSaveDto>>(
          `guestRegistration/ByReservationItem/${reservationItemId}/property/${propertyId}/language/${language}`,
    );
  }

  public getReservationWithOnlyReservationItemListById(reservationId: number): Observable<Reservation> {
    return this.httpClient
      .get<ReservationDto>(this.urlGetWithOnlyReservationItemList.replace('id', reservationId.toString())).pipe(
      map(response => {
        const reservation = new Reservation();
        reservation.normalizeResponse(response);
        return reservation;
      }));
  }

  public searchReservationsByCriteria(
    propertyId: number,
    criteria: SearchReservationDto,
  ): Observable<ReservationItemsResponse<ReservationSearchDto>> {
    let httpParams = new HttpParams();
    if (criteria) {
      Object.keys(criteria).forEach(function(key) {
        httpParams = httpParams.set(key, criteria[key]);
      });
    }
    return this.httpClient.get<ReservationItemsResponse<ReservationSearchDto>>(`reservations/${propertyId}/search`, { params: httpParams });
  }

  public updateAndAssociateGuestToReservationItem(guestReservationItemId: number, guestReservationItem: GuestReservationItem) {
    return this.httpClient.patch(
      this.urlUpdateAndAssociateGuestToReservationItem.replace('guestReservationItemId', guestReservationItemId.toString()),
      guestReservationItem.normalizeRequest(),
    );
  }

  public disassociateGuestFromReservationItem(guestReservationItemId: number) {
    return this.httpClient.patch(
      this.urlDisassociateGuestFromReservationItem.replace('guestReservationItemId', guestReservationItemId.toString()),
      {},
    );
  }

  public cancelReservation(reservationId: number, reasonId: number, cancellationDescription: string): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(
      `${this.url}/${reservationId}/cancel`,
      {
        reasonId: reasonId,
        cancellationDescription: cancellationDescription,
      },
      { observe: 'response' },
    );
  }

  public reactivateReservation(reservationId: number): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${reservationId}/reactivate`, {}, { observe: 'response' });
  }

  public updateRemarks(reservationId: number, reservationRemark: ReservationRemark): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${reservationId}/savenote`, reservationRemark, { observe: 'response' });
  }

  public confirmReservation(reservationId: number): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${reservationId}/confirm`, {}, { observe: 'response' });
  }

  public getReservationCode(): Observable<any> {
    return this.httpClient
      .get<Observable<any>>('Reservations/newReservationCode');
  }
}
