import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TokenInterceptor } from 'app/auth/interceptors/auth.interceptor';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { User } from 'app/shared/models/user';
import { map } from 'rxjs/operators';
import { PropertyToken } from '../../models/PropertyToken';

import { LoginForm } from '../../../auth/models/login-form';
import { NewPasswordForm } from '../../../auth/models/new-password-form';

@Injectable({
  providedIn: 'root'
})
export class AuthResource {
  constructor(
    private adminApi: AdminApiService,
    private tokenInterceptor: TokenInterceptor
  ) { }

  private static getHeaders(token): HttpHeaders {
    return new HttpHeaders({
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    });
  }

  public login(loginForm: LoginForm) {
    console.warn('DEPRECATED: use authResource from bifrost instead');
    loginForm.grantType = 'password';

    this.tokenInterceptor.disableOnce();
    return this.adminApi
      .post(`User/login`, loginForm);
  }

  public signup(passwordForm: NewPasswordForm, token: string) {
    console.warn('DEPRECATED: use authResource from bifrost instead');
    this.tokenInterceptor.disableOnce();
    return this.adminApi.post(`User/newuserpassword`, passwordForm, {
      headers: AuthResource.getHeaders(token)
    });
  }

  public resetPassword(passwordForm: NewPasswordForm, token: string) {
    console.warn('DEPRECATED: use authResource from bifrost instead');
    this.tokenInterceptor.disableOnce();
    return this.adminApi.post(`User/newpassword`, passwordForm, {
      headers: AuthResource.getHeaders(token)
    });
  }

  public recoverPassword(email: string) {
    console.warn('DEPRECATED: use authResource from bifrost instead');
    return this.adminApi.post(`User/forgotpassword/${email}`, null);
  }

  public getUserInfo() {
    const upperCaseLanguage = (languageIso) => {
      const [language, country] = languageIso.split('-');
      return `${language}-${country.toUpperCase()}`;
    };

    return this
      .adminApi
      .get(`User/currentUser`)
      .pipe(
        map((user: User): User => {
          return {
            ...user,
            preferredCulture: upperCaseLanguage(user.preferredCulture),
            preferredLanguage: upperCaseLanguage(user.preferredLanguage)
          };
        })
      );
  }

  public getTokenByPropertyId(propertyId: number) {
    return this.adminApi.get<PropertyToken>(`User/property/${propertyId}/token`);
  }

}
