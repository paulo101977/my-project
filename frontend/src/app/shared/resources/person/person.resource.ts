import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { SearchPersonsResultDto } from './../../models/person/search-persons-result-dto';
import { DocumentType } from './../../models/document-type';
import { SearchClientAndPersonsResultDto } from './../../models/dto/search-client-person-result-dto';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { SharedService } from './../../services/shared/shared.service';
import { FnrhDto } from '../../models/dto/checkin/fnhr-dto';
import { SearchResultDto } from '../../models/dto/search-result-dto';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { LoaderInterceptor } from 'app/core/interceptors/loader.interceptor';

@Injectable({ providedIn: 'root' })
export class PersonResource {
  private urlPersonsSearchByNameAndDocument =
    'guestRegistration/search?searchData=valSearchData&documentTypeId=valDocumentTypeId&page=1&pageSize=5';
  private urlPersonsSearchByName = 'guestRegistration/search?searchData=valSearchData&page=1&pageSize=5';
  private urlPersonsSearchByEmail = 'guestRegistration/search?searchData=valSearchData&Page=1&PageSize=5&ByEmail=true';

  constructor(private thexApi: ThexApiService,
              private sharedService: SharedService,
              private loaderInterceptor: LoaderInterceptor) {}

  public getPersonsByCriteria(valSearchData: string, valDocumentTypeId: DocumentType): Observable<SearchPersonsResultDto[]> {
    this.loaderInterceptor.disableOnce();
    return this.thexApi
      .get<ItemsResponse<SearchPersonsResultDto>>(
        this.urlPersonsSearchByNameAndDocument
          .replace('valSearchData', valSearchData.toString())
          .replace('valDocumentTypeId', valDocumentTypeId.id.toString()),
      ).pipe(
      map(response => response.items));
  }

  public getClientAndPersonsByCriteria(
    valSearchData: string,
    valPropertyId: number,
    valPersonType: string,
    valDocumentTypeId: DocumentType,
  ): Observable<ItemsResponse<SearchClientAndPersonsResultDto>> {
    this.loaderInterceptor.disableOnce();
    return this.thexApi
      .get<ItemsResponse<SearchClientAndPersonsResultDto>>(
        `companyClient/searchwithlocation?PropertyId=${valPropertyId}&SearchData=${valSearchData}&Language=pt-br&PersonType=${
          valPersonType
        }&DocumentTypeId=${valDocumentTypeId.id}&PageSize=5`,
      );
  }

  public getPersonsByNameAndDocument(data: string): Observable<SearchPersonsResultDto[]> {
    const cpf = 1;
    this.loaderInterceptor.disableOnce();
    return this.thexApi
      .get<ItemsResponse<SearchPersonsResultDto>>(
        this.urlPersonsSearchByNameAndDocument.replace('valSearchData', data).replace('valDocumentTypeId', cpf.toString()),
      ).pipe(
      map(response => response.items));
  }

  public getPersonsByName(data: string): Observable<FnrhDto[]> {
    this.loaderInterceptor.disableOnce();
    return this.thexApi
      .get<ItemsResponse<FnrhDto>>(this.urlPersonsSearchByName.replace('valSearchData', data)).pipe(
      map(response => response.items));
  }

  public getPersonsByEmail(data: string): Observable<SearchPersonsResultDto[]> {
    this.loaderInterceptor.disableOnce();
    return this.thexApi
      .get<ItemsResponse<SearchPersonsResultDto>>(this.urlPersonsSearchByEmail.replace('valSearchData', data)).pipe(
      map(response => response.items));
  }

  public getCategorizedPersonByName(name: any): Observable<SearchResultDto> {
    this.loaderInterceptor.disableOnce();
    return this.thexApi.get<SearchResultDto>('Guest/search', { params: { SearchData: name } });
  }

  public getPersonListByDocument(documentInformation: string, documentTypeId: number): Observable<SearchPersonsResultDto[]> {
    this.loaderInterceptor.disableOnce();
    return this.thexApi
      .get<ItemsResponse<SearchPersonsResultDto>>(
        `persons/search?searchData=${documentInformation}&documentTypeId=${documentTypeId}`
      ).pipe(
      map(response => response.items));
  }
}
