import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http/';

import { PropertyGuestTypeDto } from './../../models/dto/property-guest-type-dto';
import { GuestAccount } from './../../models/guest-account';
import { GuestInRoom } from '../../models/room-change/guest-in-room';
import { SharedService } from './../../services/shared/shared.service';
import { Observable } from 'rxjs';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { HttpService } from '../../../core/services/http/http.service';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class GuestResource {
  private headers = this.sharedService.getHeaders();

  constructor(private thexApi: ThexApiService, private sharedService: SharedService) {}

  public getGuestTypesByPropertyId(propertyId: number): Observable<ItemsResponse<PropertyGuestTypeDto>> {
    return this.thexApi.get<ItemsResponse<PropertyGuestTypeDto>>(
      `PropertyGuestType/${propertyId}/byproperty`,
    );
  }

  public getGuestAccountByPropertyId(propertyId: number): Observable<ItemsResponse<GuestAccount>> {
    return this.thexApi.get<ItemsResponse<GuestAccount>>(`ReservationGuest/${propertyId}`);
  }

  public getGuestList(roomId: number, actualDate: string): Observable<ItemsResponse<GuestInRoom>> {
    const params = new HttpParams().set('datecurrent', actualDate);
    return this.thexApi.get<ItemsResponse<GuestInRoom>>(`GuestReservationItem/roomid/${roomId}`, {
      params: params,
    });
  }
}
