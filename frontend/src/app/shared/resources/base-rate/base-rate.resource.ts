import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { ConfigurationDataParameters } from '../../models/revenue-management/configuration-data-parameters';
import { PropertyRate } from '../../models/revenue-management/property-base-rate';
import {BaseRateLevel} from 'app/revenue-management/base-rate/models/base-rate-level';

@Injectable({providedIn: 'root'})
export class BaseRateResource {
  private url = 'propertyBaseRate/';
  private urlPremise = 'propertyBaseRate/createbypremise';
  private urlLevel = 'propertyBaseRate/createbylevel';

  constructor(private api: ThexApiService) {}

  public getConfigurationDataParameters(propertyId: number): Observable<ConfigurationDataParameters> {
    return this.api.get<ConfigurationDataParameters>(this.url + `${propertyId}/configurationDataParameters`);
  }

  public sendConfigurationDataParameters(baseRateConfiguration: PropertyRate, bIsPremise: boolean): Observable<any> {
    if (bIsPremise) {
      return this.sendConfigurationDataPremise(baseRateConfiguration);
    }

    return this.api.post<PropertyRate>(this.url, baseRateConfiguration);
  }

  public sendConfigurationDataPremise(baseRateConfiguration: PropertyRate): Observable<any> {
    return this.api.post<PropertyRate>(this.urlPremise, baseRateConfiguration);
  }

  public sendConfigurationDataLevel(baseRateConfiguration: BaseRateLevel): Observable<any> {
    return this.api.post<PropertyRate>(this.urlLevel, baseRateConfiguration);
  }
}
