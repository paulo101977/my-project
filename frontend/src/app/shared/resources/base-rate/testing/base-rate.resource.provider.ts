
import { BaseRateResource } from 'app/shared/resources/base-rate/base-rate.resource';
import { of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing';
import { BaseRateLevel } from 'app/revenue-management/base-rate/models/base-rate-level';

export const baseRateResourceProvider = createServiceStub(BaseRateResource, {
    getConfigurationDataParameters: (propertyId: number) => of({
      currencyList: {hasNext: false, items: [], total: 0},
      mealPlanTypeList: {hasNext: false, items: [], total: 0},
      propertyParameterList: [],
      roomTypeList: {hasNext: false, items: [], total: 0}
    }),

    sendConfigurationDataLevel: (baseRateConfiguration: BaseRateLevel) => of( null ) ,
});
