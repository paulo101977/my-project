import { Inject, Injectable } from '@angular/core';
import {
  APP_NAME,
  LocalUserManagerService,
  User,
  UserInvitation,
  UserResourceService
} from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UserResource {
  constructor(
    @Inject(APP_NAME) private appName: string,
    private resource: UserResourceService,
    private localUserManager: LocalUserManagerService
  ) {}

  public getAll(): Observable<User[]> {
    return this.resource.getAll()
      .pipe(map(data => data.items));
  }

  public invite(userInvitation: UserInvitation) {
    return this.resource.invite(userInvitation);
  }

  toggleActivation(uid: string, propertyId: number) {
    return this.resource.toggleActivation(uid, propertyId);
  }

  public updateLanguage(userId: string, culture: string) {
    return this.resource.updateLanguage(userId, culture);
  }

  public updateAdmin(userId: string, isAdmin: boolean) {
    return this.resource.updateAdmin(userId, isAdmin);
  }

  public getLink(userId: string) {
    return this.resource.getLink(userId);
  }

  public getCurrentUser(): Observable<User> {
    return this.resource.getCurrentUser();
  }

  public updateUserInfo(user: User) {
    return this.resource.updateUserInfo(user.id, user.name, user.dateOfBirth, user.phoneNumber, user.nickName);
  }

  public saveUserImage(item: User): Observable<User> {
    return this
      .resource
      .saveUserImage(item)
      .pipe(
        map((user) => {
          return {
            ...user,
            // timestamp is needed to avoid cache problems
            // TODO remove when the cache problem is solved
            photoUrl: user.photoUrl + '?_=' + new Date().getTime(),
          };
        }),
        this.updateLocalStorage()
      );
  }

  public updatePassword(userId: string, previousPassword: string, newPassword: string) {
    return this.resource.updatePassword(userId, previousPassword, newPassword);
  }

  public delete(userId: string) {
    return this.resource.delete(userId);
  }

  private updateLocalStorage() {
    return map((user) => {
      const newUser = {
        ...this.localUserManager.getUser(this.appName),
        ...user
      };

      this.localUserManager.setUser(this.appName, newUser);
      return newUser;
    });
  }
}
