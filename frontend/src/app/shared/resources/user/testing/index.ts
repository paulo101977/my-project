import { createServiceStub } from '../../../../../../testing';
import { UserResource } from '../user.resource';
import { Observable, of } from 'rxjs';
import { User } from '@inovacaocmnet/thx-bifrost';
import {UserInvitation} from 'app/shared/models/user-invitation';

export const userResourceStub = createServiceStub(UserResource, {
  getCurrentUser (): Observable<User> {
    return of(null);
  },
  updatePassword (userId: string, previousPassword: string, newPassword: string): Observable<any> {
    return of(null);
  },
  getAll(): Observable<User[]> {
    return of(null);
  }, getLink(userId: string): Observable<{}> {
    return of(null);
  }, invite(userInvitation: UserInvitation): Observable<{}> {
    return of(null);
  }, saveUserImage(item: User): Observable<User> {
    return of(null);
  }, toggleActivation(uid: string, propertyId: number): Observable<{}> {
    return of(null);
  }, updateAdmin(userId: string, isAdmin: boolean): Observable<{}> {
    return of(null);
  }, updateLanguage(userId: string, culture: string): Observable<{}> {
    return of(null);
  }, updateUserInfo(user: User): Observable<{}> {
    return of(null);
  }, delete(userId: string): Observable<any> {
    return of(null);
  }

});
