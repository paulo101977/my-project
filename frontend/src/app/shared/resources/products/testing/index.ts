import { createServiceStub } from '../../../../../../testing';
import { ProductsResource } from 'app/shared/resources/products/products.resource';
import { Observable, of } from 'rxjs';
import { PosProduct, Product, ProductGroups, ProductUnity } from 'app/products/models/products/products';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { ProductsBillingItem } from 'app/billing-account/models/products-billing-item';

export const productsResourceStub = createServiceStub(ProductsResource, {
    addGroup(item: ProductGroups): Observable<ProductGroups> {
        return of( null );
    },

    addProduct(item: Product): Observable<Array<Product>> {
        return of( null );
    },

    billingAccountDebit(items): Observable<any> {
        return of( null );
    },

    billingPosSearch(billingItemId: number, search: string): Observable<ItemsResponse<any>> {
        return of( null );
    },

    deleteProduct(id: number): Observable<Array<Product>> {
        return of( null );
    },

    getAllGroups(): Observable<ItemsResponse<ProductGroups>> {
        return of( null );
    },

    getAllNcm(): Observable<ItemsResponse<any>> {
        return of( null );
    },

    getAllGroupsCategory(billingItemId: number): Observable<ItemsResponse<ProductGroups>> {
        return of( null );
    },

    getAllUnitys(): Observable<ItemsResponse<ProductUnity>> {
        return of( null );
    },

    getProductBillingItem(): Observable<ItemsResponse<ProductsBillingItem>> {
        return of( null );
    },

    getProductById(id: string): Observable<Product> {
        return of( null );
    },

    getGroupFixList(): Observable<ItemsResponse<any>> {
        return of( null );
    },

    getAllProducts(): Observable<ItemsResponse<Product>> {
        return of( null );
    },

    getAllPos(): Observable<ItemsResponse<PosProduct>> {
        return of( null );
    },

    deleteProductGroup(item: ProductGroups): Observable<ProductGroups> {
        return of( null );
    },

    editGroup(item: ProductGroups): Observable<ProductGroups> {
        return of( null );
    },

    editToogleGroup(id: string): Observable<ProductGroups> {
        return of( null );
    },

    getProductsByGroupId(idGroup: string, billintItemId: number): Observable<ItemsResponse<Product>> {
        return of( null );
    },

    searchNcm(param: string): Observable<ItemsResponse<any>> {
        return of( null );
    },

    setProduct(item: Product): Observable<Array<Product>> {
        return of( null );
    },

    setToggleProduct(id: string): Observable<Array<Product>> {
        return of( null );
    }
});
