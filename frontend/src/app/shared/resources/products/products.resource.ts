import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { Product , ProductGroups, ProductUnity } from 'app/products/models/products/products';
import { PosProduct } from 'app/products/models/products/products';
import { ProductsBillingItem } from 'app/billing-account/models/products-billing-item';
import { PdvApiService } from '@inovacaocmnet/thx-bifrost';
import { HttpParams } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class ProductsResource {

  constructor(
    private pdvService: PdvApiService
  ) {}


  public getAllProducts(): Observable<ItemsResponse<Product>> {
    return this
      .pdvService
      .get<ItemsResponse<Product>>(`product`);
  }

  public getProductById(id: string): Observable<Product> {
    return this
      .pdvService
      .get<Product>(`product/${id}`);
  }

  public getProductsByGroupId(idGroup: string, billintItemId: number ): Observable<ItemsResponse<Product>> {
    return this
      .pdvService
      .get<ItemsResponse<Product>>(`product/category/${idGroup}/${billintItemId}`);
  }

  public addProduct(item: Product): Observable<Array<Product>> {
    return this
      .pdvService
      .post<Array<Product>>(`product`, item);
  }

  public setProduct(item: Product): Observable<Array<Product>> {
    const { id } = item;

    return this
      .pdvService
      .put<Array<Product>>(`product/${id}`, item);
  }

  public setToggleProduct( id: string): Observable<Array<Product>> {
    return this
      .pdvService
      .patch<Array<Product>>(`product/${id}/toggleactivation`);
  }


  public deleteProduct(id: number): Observable<Array<Product>> {
    return this
      .pdvService
      .delete<Array<Product>>(`product/${id}`);
  }


  public getAllGroups(): Observable<ItemsResponse<ProductGroups>> {
    return this
      .pdvService
      .get<ItemsResponse<ProductGroups>>(`productCategory`);
  }

  public addGroup(item: ProductGroups): Observable<ProductGroups> {
    return this
      .pdvService
      .post<ProductGroups>(`productCategory`, item);
  }

  public editGroup(item: ProductGroups): Observable<ProductGroups> {
    return this
      .pdvService
      .put<ProductGroups>(`productCategory/${item.id}`, item);
  }

  public editToogleGroup(id: string): Observable<ProductGroups> {
    return this
      .pdvService
      .patch<ProductGroups>(`productCategory/${id}/toggleactivation`);
  }

  public deleteProductGroup(item: ProductGroups): Observable<ProductGroups> {
    return this
      .pdvService
      .delete<ProductGroups>(`productCategory/${item.id}`);
  }

  public getAllUnitys(): Observable<ItemsResponse<ProductUnity>> {
    return this
      .pdvService
      .get<ItemsResponse<ProductUnity>>(`unitMeasurement`);
  }


  public getAllNcm(): Observable<ItemsResponse<any>> {
    return this
      .pdvService
      .get<ItemsResponse<any>>(`ncm`);
  }

  public searchNcm(param: string): Observable<ItemsResponse<any>> {
    const params = new HttpParams()
      .set('Param', param);

    return this
      .pdvService
      .get<ItemsResponse<any>>(`ncm/search`, { params });
  }


  public getGroupFixList(): Observable<ItemsResponse<any>> {
    return this
      .pdvService
      .get<ItemsResponse<any>>( `productCategory/groupfixed`);
  }

  public getAllPos(): Observable<ItemsResponse<PosProduct>> {
    return this
      .pdvService
      .get<ItemsResponse<PosProduct>>(`billingItem`);
  }

  public getProductBillingItem(): Observable<ItemsResponse<ProductsBillingItem>> {
    return this
      .pdvService
      .get<ItemsResponse<ProductsBillingItem>>(`productBillingItem`);
  }


  public billingAccountDebit(items) {
    return this
      .pdvService
      .post(`BillingAccountItem/debit`, items);
  }

  public billingPosSearch(billingItemId: number, search: string): Observable<ItemsResponse<any>> {
    const params = new HttpParams()
      .set('Param', search);

    return this
    .pdvService
    .get<ItemsResponse<ProductGroups[]>>(`product/${billingItemId}/search`, { params });
  }

  public getAllGroupsCategory(billingItemId: number): Observable<ItemsResponse<ProductGroups>> {
    const params = new HttpParams()
      .set('BillingItemId', billingItemId.toString());

    return this
      .pdvService
      .get<ItemsResponse<ProductGroups>>(`productCategory/getAllByBillingItemId`, { params });
  }

}
