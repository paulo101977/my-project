import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SharedService } from './../../services/shared/shared.service';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class BudgetResource {
  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getBudgetByRoomTypeAndRoom(
    propertyId: number,
    roomTypeId: any,
    reservationItemId: string,
    checkinDate: any,
    checkoutDate: any,
  ): Observable<any> {
    let Params = new HttpParams();
    roomTypeId = roomTypeId.toString();

    Params = Params.append('InitialDate', checkinDate);
    Params = Params.append('FinalDate', checkoutDate);
    Params = Params.append('ReservationItemId', reservationItemId);
    Params = Params.append('RoomTypeId', roomTypeId);

    return this.httpClient.get<any>(`ReservationBudget/${propertyId}`, { params: Params });
  }
}
