import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http/';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { SharedService } from '../../services/shared/shared.service';
import { POS } from '../../models/pos/pos';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({
  providedIn: 'root',
})
export class POSResource {
  private url = 'billingitemPDV';

  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public createPOS(pos: POS): Observable<POS> {
    return this.httpClient.post<POS>(`${this.url}/createservicewithtax`, pos);
  }

  public updatePOS(pos: POS): Observable<POS> {
    return this.httpClient.put<POS>(`${this.url}/updatebillingitemservicewithtax`, pos);
  }

  public getPOSById(id: number, propertyId: number): Observable<POS> {
    return this.httpClient.get<POS>(`${this.url}/${id}/property/${propertyId}/billingitem`);
  }

  public getAllPOSListByPropertyId(propertyId: number): Observable<ItemsResponse<POS>> {
    return this.httpClient.get<ItemsResponse<POS>>(`${this.url}/${propertyId}/getallbillingitemservice`);
  }

  public deletePOS(id: number, propertyId: number): Observable<HttpResponse<Object>> {
    return this.httpClient.delete(`${this.url}/${id}/property/${propertyId}`, { observe: 'response' });
  }

  public updatePOSStatus(id: number): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${id}/toggleactivation`, {}, { observe: 'response' });
  }
}
