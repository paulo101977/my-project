import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { Injectable } from '@angular/core';

import { Property } from './../../models/property';
import { HttpService } from 'app/core/services/http/http.service';
import { of as observableOf, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PropertyResource {
  constructor(
    private httpClient: HttpService,
    private thexApi: ThexApiService
  ) {
  }

  public getProperty(propertyId: number): Observable<Property> {
    return this.httpClient.get<Property>('properties/' + propertyId + '?Expand=company,brand.chain');
  }

  public getAll(): Observable<ItemsResponse<Property>> {
    return this.thexApi.get<ItemsResponse<Property>>('properties/getall');
  }

  public getPropertyById(propertyId: number): Observable<Property> {
    return this.thexApi.get<Property>(`properties/${propertyId}/getbypropertyid`);
  }

  public updateProperty(property: Property): Observable<Property> {
    return this.httpClient.put<Property>('', property);
  }

  public getPropertyLanguages() {
    return observableOf({
      next: false,
      total: 2,
      items: [
        { key: 'en-US', value: 'en-US' },
        { key: 'pt-BR', value: 'pt-BR' },
        { key: 'pt-PT', value: 'pt-PT' },
        { key: 'es-AR', value: 'es-AR' }
      ]
    });
  }

  public changePhoto(photo: string): Observable<Property> {
    const params = {Photo: photo};
    return this.thexApi.patch<Property>('properties/changephoto', params);
  }
}
