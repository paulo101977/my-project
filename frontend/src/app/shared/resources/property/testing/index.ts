import { createServiceStub } from 'testing';
import { PropertyResource } from '../property.resource';
import {Observable, of} from 'rxjs';
import {ItemsResponse} from 'app/shared/models/backend-api/item-response';
import {Property} from 'app/shared/models/property';

export const propertyResourceStub = createServiceStub(PropertyResource, {
  getAll(): Observable<ItemsResponse<Property>> {
    return of( null );
  },
  getProperty(propertyId: number): Observable<Property> {
    return of( null );
  },
  getPropertyById(propertyId: number): Observable<Property> {
    return of( null );
  },
  getPropertyLanguages(): Observable<any> {
    return of( null );
  },
  updateProperty(property: Property): Observable<Property> {
    return of( null );
  },
  changePhoto(photo: string): Observable<Property> {
    return of( null );
  },
});
