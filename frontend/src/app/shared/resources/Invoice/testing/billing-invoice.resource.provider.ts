import {createServiceStub} from '../../../../../../testing';
import {BillingInvoiceResource} from '../billing-invoice.resource';
import {InvoiceDto} from 'app/shared/models/dto/invoice/invoice-dto';
import { of} from 'rxjs';
import { BillingAccountItemCreditAmountDto } from 'app/shared/models/dto/billing-account-item/billing-account-item-credit-amount-dto';

export const BillingInvoiceProvider = createServiceStub(BillingInvoiceResource, {
  resendInvoice: (propertyId: number, invoice: InvoiceDto) => of(null),
  createCreditNote: (billingAccountItemList: Array<BillingAccountItemCreditAmountDto>) => of(null),
  cancelInvoice: (invoice: InvoiceDto, reasonId: number) => of(null),
});
