import { Injectable } from '@angular/core';
import { InvoiceDto } from '../../models/dto/invoice/invoice-dto';
import { forkJoin, Observable } from 'rxjs';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { BillingAccountItemCreditAmountDto } from '@app/shared/models/dto/billing-account-item/billing-account-item-credit-amount-dto';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class BillingInvoiceResource {
  constructor(private thexApi: ThexApiService) {}

  public resendInvoice(propertyId: number, invoice: InvoiceDto): Observable<any> {
    return this.thexApi.patch(
      `BillingInvoiceIntegration/property/${propertyId}/invoice/${invoice.id}/resend`,
      null,
    );
  }

  public resendManyInvoices(propertyId: number, invoiceList: Array<InvoiceDto>): Observable<any> {
    const invoiceRequestList = [];
    for (const invoice of invoiceList) {
      invoiceRequestList.push(this.resendInvoice(propertyId, invoice));
    }
    return forkJoin(invoiceRequestList);
  }

  public cancelInvoice(invoice: InvoiceDto, reasonId: number): Observable<any> {
    return this.thexApi.patch(`billinginvoiceproperty/${invoice.propertyId}/CancelBillingInvoice/${invoice.id}/${reasonId}`,
      null);
  }

  public createCreditNote(billingAccountItemList: Array<BillingAccountItemCreditAmountDto>): Observable<any> {
    return this.thexApi.post<Array<BillingAccountItemCreditAmountDto>>(`billinginvoice/createcreditnote`, billingAccountItemList);
  }

  public getAllByFilters(propertyId: number, invoiceSearchDto: any): Observable<BlobPart> {
    const headers = new HttpHeaders();
    headers.set('Accept', 'application/octet-stream');

    const httpParams = new HttpParams()
      .set('initialNumber', invoiceSearchDto.initialNumber)
      .set('finalNumber', invoiceSearchDto.finalNumber)
      .set('initialDate', invoiceSearchDto.initialDate)
      .set('finalDate', invoiceSearchDto.finalDate)
      .set('Xml', invoiceSearchDto.isXml);

    return this.thexApi.get(`BillingInvoiceIntegration/property/${propertyId}/invoice/download`, {
        params: httpParams, headers: headers, responseType: 'blob'
      }
    );
  }

  public getInvoicesFromBillingAccountId(id: string): Observable<Array<InvoiceDto>> {
    return this.thexApi.get<Array<InvoiceDto>>(`BillingAccount/${id}/invoices`);
  }
}
