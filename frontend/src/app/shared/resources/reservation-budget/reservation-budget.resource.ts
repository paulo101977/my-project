import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http/';
import * as moment from 'moment';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { ReservationBudgetHeader } from './../../models/revenue-management/reservation-budget/reservation-budget-header';
import { SharedService } from './../../services/shared/shared.service';
import { ReservationItemBudget } from '../../models/revenue-management/reservation-budget/reservation-item-budget';
import { ReservationItemsBudget } from '../../models/revenue-management/reservation-budget/reservation-items-budget';
import {
  ReservationItemCommercialBudgetSearch
} from '../../models/revenue-management/reservation-budget/reservation-item-commercial-budget-search';
import { DateService } from '../../services/shared/date.service';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class ReservationBudgetResource {

  constructor(private thexApi: ThexApiService, private sharedService: SharedService) {}

  public getAllReservationItems(propertyId: number, reservationId: number): Observable<ItemsResponse<ReservationItemsBudget>> {
    return this.thexApi.get<ItemsResponse<ReservationItemsBudget>>(
      `reservationitems/${propertyId}/${reservationId}/accommodations`,
    );
  }

  public getReservationHeader(reservationId: number): Observable<ReservationBudgetHeader> {
    return this.thexApi.get<ReservationBudgetHeader>(`reservations/${reservationId}/reservationHeader`);
  }

  public getReservationItemBudget(
    reservationItemCommercialBudgetSearch: ReservationItemCommercialBudgetSearch,
  ): Observable<ReservationItemBudget> {
    let params = new HttpParams();
    Object.getOwnPropertyNames(reservationItemCommercialBudgetSearch).forEach((key, index) => {
      let value = reservationItemCommercialBudgetSearch[key];
      if (value instanceof Date) {
        value = moment(value).format(DateService.DATE_FORMAT_UNIVERSAL);
      }
      if (key != 'childrenAge') {
        params = params.set(key, value);
      }
    });

    if ( reservationItemCommercialBudgetSearch['childrenAge'] ) {
      for (const childrenAge of reservationItemCommercialBudgetSearch['childrenAge']) {
        params = params.append('childrenAge', childrenAge + '' );
      }
    }

    return this.thexApi.get<ReservationItemBudget>(`reservationBudget/${reservationItemCommercialBudgetSearch.propertyId}`, {
      params,
    });
  }

  public recalculateReservation(propertyId: number, reservationId: number): Observable<HttpResponse<Object>> {
    return this.thexApi.put(`reservationBudget/${propertyId}/${reservationId}/accomodations`, {}, { observe: 'response' });
  }

  public updateBudgetReservationItem(
    propertyId: number,
    reservationItemId: number,
    reservationItemWithBudget,
  ): Observable<HttpResponse<Object>> {
    return this.thexApi.patch(`reservationitems/${propertyId}/${reservationItemId}/budget`, reservationItemWithBudget, {
      observe: 'response',
    });
  }
}
