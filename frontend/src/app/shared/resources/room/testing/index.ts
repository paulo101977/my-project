import { of } from 'rxjs';
import { RoomResource } from '../room.resource';
import { createServiceStub } from 'testing';
import { Room } from '@app/shared/models/room';

const rooms = [
  { ...new Room, id: 1 }
];

export const roomResourceStub = createServiceStub(RoomResource, {
  createRoom: (room: Room) => of( null ),
  getRoomsWithoutChildByPropertyId: (propertyId: number) => of( rooms ),
  deleteRoom: (id: number) => of( null )
});
