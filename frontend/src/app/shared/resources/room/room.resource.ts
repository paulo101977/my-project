
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Room } from '../../models/room';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { StatusCheckInRooms } from '../status-checkin-room';
import { SharedService } from './../../services/shared/shared.service';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class RoomResource {
  private urlRoom = 'rooms/';
  private putUrlRoom = 'rooms/id';
  private activeUrlRoom = 'rooms/roomId/toggleactivation';
  private getUrlRoom = 'rooms/roomId?Expand=RoomType.RoomTypeBedTypeList';
  private urlParentRoom = 'properties/propertyId/parentrooms/';
  private urlPropertyRoom = 'properties/propertyId/rooms?_expandables=ChildRoomIdList';
  private urlPropertyRoomWithoutParentAndChildren = 'properties/propertyId/roomswithoutparentandchildren';
  private urlParentRoomChildren = 'rooms/roomId/childrooms';

  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getRoomsWithoutParentAndChildrenByPropertyId(propertyId: number): Observable<Room[]> {
    return this.httpClient
      .get<ItemsResponse<Room>>(this.urlPropertyRoomWithoutParentAndChildren.replace('propertyId', propertyId.toString())).pipe(
      map(response => response.items));
  }

  public getRoomsWithoutChildByPropertyId(propertyId: number): Observable<Room[]> {
    return this.httpClient
      .get<ItemsResponse<Room>>(this.urlPropertyRoom.replace('propertyId', propertyId.toString())).pipe(
      map(response => response.items));
  }

  public getChildrenRoomByRoomId(roomId: number): Observable<Room[]> {
    return this.httpClient
      .get<ItemsResponse<Room>>(this.urlParentRoomChildren.replace('roomId', roomId.toString())).pipe(
      map(response => response.items));
  }

  public getRoomsByPropertyId(propertyId: number): Observable<Room[]> {
    return this.httpClient
      .get<any>(this.urlPropertyRoom.replace('propertyId', propertyId.toString())).pipe(
      map(response => response.result.items));
  }

  public getParentRoomsByPropertyId(propertyId: number): Observable<Room[]> {
    return this.httpClient
      .get<ItemsResponse<Room>>(this.urlParentRoom.replace('propertyId', propertyId.toString())).pipe(
      map(response => response.items));
  }

  public updateRoomStatusById(roomId: number): Observable<any> {
    return this.httpClient.patch(this.activeUrlRoom.replace('roomId', roomId.toString()), null);
  }

  public getRoomById(roomId: number): Observable<Room> {
    return this.httpClient.get<Room>(this.getUrlRoom.replace('roomId', roomId.toString()));
  }

  public createRoom(room: Room): Observable<Room> {
    return this.httpClient.post<any>(this.urlRoom, room).pipe(map(res => res.result as Room));
  }

  public deleteRoom(id: number): Observable<any> {
    return this.httpClient.delete(this.urlRoom + id);
  }

  public editRoom(room: Room): Observable<Room> {
    return this.httpClient.put<Room>(this.putUrlRoom.replace('id', room.id.toString()), room);
  }

  public getSearchRooms(propertyId: number, searchRoom: string): Observable<ItemsResponse<StatusCheckInRooms>> {
    const params = new HttpParams().set('searchData', searchRoom);
    return this.httpClient
      .hideLoader()
      .get<ItemsResponse<StatusCheckInRooms>>(
      `Rooms/${propertyId}/statuscheckinrooms`,
      { params: params },
    );
  }
}
