import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http/';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { Chain } from './../../models/chain';
import { SharedService } from './../../services/shared/shared.service';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class ChainResource {
  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getAllChain(): Observable<ItemsResponse<Chain>> {
    return this.httpClient.get<ItemsResponse<Chain>>('Chain/');
  }
}
