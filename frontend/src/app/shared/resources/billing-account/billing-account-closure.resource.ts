import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http/';

import { BillingAccountClosure } from './../../models/billing-account/billing-account-closure';
import { BillingAccountClosureResult } from './../../models/billing-account/billing-account-closure-result';
import { BillingAccountClosurePartial } from '../../models/billing-account/billing-account-closure-partial';
import { SharedService } from './../../services/shared/shared.service';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class BillingAccountClosureResource {
  private url = 'billingAccountClosure';

  constructor(private thexApi: ThexApiService, private sharedService: SharedService) {}

  public payBillingAccount(billingAccountClosure: BillingAccountClosure): Observable<BillingAccountClosureResult> {
    return this.thexApi.post<BillingAccountClosureResult>(this.url, billingAccountClosure);
  }

  public payPartialBillingAccount(billingAccountClosurePartial: BillingAccountClosurePartial): Observable<BillingAccountClosurePartial> {
    return this.thexApi.post<BillingAccountClosurePartial>(`${this.url}/partial`, billingAccountClosurePartial);
  }
}
