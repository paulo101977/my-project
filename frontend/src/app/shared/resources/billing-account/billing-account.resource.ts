import { PersonHeaderDto } from './../../models/dto/person-header-dto';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { ItemsResponse } from './../../models/backend-api/item-response';
import { SingleAccountDto } from 'app/shared/models/dto/billing-account/single-account-dto';
import { GetAllBillingAccountOpen } from '../../models/billing-account/get-all-billing-account-open';
import { GetAllBillingAccountForClosure } from '../../models/billing-account/get-all-billing-account-for-closure';
import { BillingAccountHeader } from './../../models/billing-account/billing-account-header';
import { BillingAccountSidebar } from 'app/shared/models/billing-account/billing-account-sidebar';
import { GetAllBillingAccount } from './../../models/billing-account/get-all-billing-account';
import { CompanyClientAccountDto } from './../../models/dto/billing-account/company-client-account-dto';
import { GuestAccountDto } from './../../models/dto/billing-account/guest-account-dto';
import { BillingAccountWithAccount } from './../../models/billing-account/billing-account-with-account';
import { BillingAccountEntriesComplete } from '../../models/billing-account/billing-account-entries-complete';
import { SharedService } from './../../services/shared/shared.service';
import { BillingAccountReopenReason } from '../../models/billing-account/billing-account-reopen-reason';
import { HttpService } from '../../../core/services/http/http.service';
import { HttpParams } from '@angular/common/http';
import { BillingAccountStatusEnum } from '../../models/billing-account/billing-account-status-enum';
import { ErrorHandlerInterceptor } from 'app/core/interceptors/error-handler.interceptor';

@Injectable({ providedIn: 'root' })
export class BillingAccountResource {

  public static readonly FILTER_OPENED_ACCOUNTS = 'openedAccounts';
  public static readonly FILTER_CLOSED_ACCOUNTS = 'closedAccounts';
  public static readonly FILTER_PENDING_ACCOUNTS = 'pendingAccounts';
  public static readonly FILTER_ALL_ACCOUNTS = 'allAccounts';

  private headers = this.sharedService.getHeaders();
  private url = 'BillingAccount';

  constructor(private httpClient: HttpService,
              private sharedService: SharedService,
              private errorInterceptor: ErrorHandlerInterceptor) {}

  public getAllBillingAccountByPropertyId(propertyId: number, filterBy?: number): Observable<ItemsResponse<GetAllBillingAccount>> {
    const queryParams = this.createQueryParamsToFilter(filterBy);
    return this.httpClient.get<ItemsResponse<GetAllBillingAccount>>(`${this.url}/${propertyId}/search`, {params: queryParams});
  }

  private createQueryParamsToFilter(filterBy: number) {
    let queryParams: HttpParams = new HttpParams();
    if (filterBy && filterBy != 0) {
      if (filterBy == BillingAccountStatusEnum.Open) {
        queryParams = queryParams.append(BillingAccountResource.FILTER_OPENED_ACCOUNTS, 'true');
      } else if (filterBy == BillingAccountStatusEnum.Closed) {
        queryParams = queryParams.append(BillingAccountResource.FILTER_CLOSED_ACCOUNTS, 'true');
      } else if (filterBy == BillingAccountStatusEnum.Pending) {
        queryParams = queryParams.append(BillingAccountResource.FILTER_PENDING_ACCOUNTS, 'true');
      }
    }
    return queryParams;
  }

  public getHeaderByBillingAccountId(billingAccountId: string): Observable<BillingAccountHeader> {
    return this.httpClient.get<BillingAccountHeader>(`${this.url}/${billingAccountId}/header`);
  }

  public getAllSidebarByBillingAccountId(billingAccountId: string): Observable<ItemsResponse<BillingAccountSidebar>> {
    return this.httpClient.get<ItemsResponse<BillingAccountSidebar>>(`${this.url}/${billingAccountId}/sidebar`);
  }

  public getBillingAccountWithAccountById(billingAccountId: string): Observable<BillingAccountWithAccount> {
    return this.httpClient.get<BillingAccountWithAccount>(`${this.url}/${billingAccountId}/accountentries`);
  }

  public saveBillingAccount(billingAccount: GuestAccountDto | CompanyClientAccountDto | SingleAccountDto): Observable<any> {
    this.errorInterceptor.disableOnce();
    return this.httpClient
      .disableErrorHandler()
      .post<GuestAccountDto>(this.url, billingAccount);
  }

  public saveBillingAccountSparce(billingAccount: GuestAccountDto | CompanyClientAccountDto | SingleAccountDto): Observable<any> {
    return this.httpClient.post<GuestAccountDto>(this.url + `/sparce`, billingAccount);
  }

  public getStatementByBillingAccountId(billingAccountId: string): Observable<BillingAccountWithAccount> {
    return this.httpClient.get<BillingAccountWithAccount>(`${this.url}/${billingAccountId}/statement`);
  }

  public createBillingAccountWithEntry(billingAccountId: string, billingAccountName: string): Observable<any> {
    return this.httpClient.post(this.url + `/add`, {
      billingAccountId: billingAccountId,
      billingAccountName: billingAccountName,
      startDate: new Date().toStringUniversal()
    });
  }

  public getSearchAccountOpen(
    propertyId: number,
    bilingAccountId: string,
    textSearched: string,
  ): Observable<ItemsResponse<GetAllBillingAccountOpen>> {
    return this.httpClient.get<ItemsResponse<GetAllBillingAccountOpen>>(
      `${this.url}/${propertyId}/transfersearch?FreeTextSearch=${textSearched}&AccountStatus=1&BaseAccountIgnored=${bilingAccountId}`,
    );
  }

  public getOpenAccounts(propertyId: number, billingAccountId: string): Observable<ItemsResponse<GetAllBillingAccountOpen>> {
    return this.httpClient.get<ItemsResponse<GetAllBillingAccountOpen>>(`${this.url}/${propertyId}/${billingAccountId}/uhorsparceaccounts`);
  }

  public getForClosure(billingAccountId: string, allSimiliarAccounts: boolean): Observable<GetAllBillingAccountForClosure> {
    return this.httpClient.get<GetAllBillingAccountForClosure>(`${this.url}/${billingAccountId}/${allSimiliarAccounts}/forclosure`);
  }

  public showStatementOfBillingAccountList(billingAccountGuidList: Array<string>): Observable<ItemsResponse<BillingAccountWithAccount>> {
    return this.httpClient.post<ItemsResponse<BillingAccountWithAccount>>(`${this.url}/accountentrieslist`, billingAccountGuidList);
  }

  public getStatementUhTotal(billingAccountId: string): Observable<BillingAccountEntriesComplete> {
    return this.httpClient.get<BillingAccountEntriesComplete>(`${this.url}/${billingAccountId}/accountentriescomplete`);
  }

  public getReasonReopenList(propertyId: number, reasonCategoryId: number): Observable<ItemsResponse<BillingAccountReopenReason>> {
    return this.httpClient.get<ItemsResponse<BillingAccountReopenReason>>(
      `reason/${propertyId}/${reasonCategoryId}/getallreason`,
    );
  }

  public reopenBillingAccountClosed(
    propertyId: number,
    billingAccountId: string,
    reasonId: number,
  ): Observable<BillingAccountReopenReason> {
    return this.httpClient.post<BillingAccountReopenReason>(`${this.url}/${propertyId}/${billingAccountId}/${reasonId}/reopen`, null);
  }

  public deleteBillingAccount(billingAccountId: string): Observable<any> {
    return this.httpClient.delete<any>(`${this.url}/${billingAccountId}`);
  }

  public associateHeaderPerson(personHeaderDto: PersonHeaderDto): Observable<PersonHeaderDto> {
    return this.httpClient.patch<PersonHeaderDto>(this.url + `/associatepersonheader`, personHeaderDto);
  }

  public getPersonHeaderByBillingAccountId(billingAccountId: string): Observable<PersonHeaderDto> {
    return this.httpClient.get<PersonHeaderDto>(`${this.url}/getpersonheaderbybillingaccountid/${billingAccountId}`);
  }
}
