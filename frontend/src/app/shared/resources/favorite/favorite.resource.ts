import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { FavoriteModel } from 'app/shared/models/favorites';
import { PreferenceApiService } from '@inovacaocmnet/thx-bifrost';
import { ErrorHandlerInterceptor } from 'app/core/interceptors/error-handler.interceptor';
import { LoaderInterceptor } from 'app/core/interceptors/loader.interceptor';

@Injectable({ providedIn: 'root' })
export class FavoriteResource {

  constructor(private preferenceApiService: PreferenceApiService,
              private errorInterceptor: ErrorHandlerInterceptor,
              private loaderInterceptor: LoaderInterceptor) {}

  public getBillingItemsFav( userId: string ): Observable<ItemsResponse<FavoriteModel>> {
    this.errorInterceptor.disableOnce();
    this.loaderInterceptor.disableOnce();
    return this
      .preferenceApiService
      .get<ItemsResponse<FavoriteModel>>(`userpreference/billingitemfav/${userId}`);
  }

  public setBillingItemFav(item: FavoriteModel, isFavorite: boolean): Observable<FavoriteModel> {

    let urlSegmentFav = 'userpreference/billingitemfav';

    if (!isFavorite) {
      urlSegmentFav = `${urlSegmentFav}/undo`;
    }

    return this
      .preferenceApiService
      .post<FavoriteModel>(`${urlSegmentFav}`, item);

  }

  // TODO: create model
  public getCardMoreSelectedData(userId: string): Observable<any> {
    this.errorInterceptor.disableOnce();
    this.loaderInterceptor.disableOnce();
    return this
      .preferenceApiService
      .get<any>(`userpreference/mostselected/${ userId }`);
  }

}
