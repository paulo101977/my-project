import { createServiceStub } from '../../../../../../testing';
import { FavoriteResource } from 'app/shared/resources/favorite/favorite.resource';
import { FavoriteModel } from 'app/shared/models/favorites';
import { Observable, of } from 'rxjs';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';

export const favoriteResourceStub = createServiceStub(FavoriteResource, {
    getBillingItemsFav(userId: string): Observable<ItemsResponse<FavoriteModel>> {
        return of( null );
    },

    getCardMoreSelectedData(userId: string): Observable<any> {
        return of( null );
    },

    setBillingItemFav(item: FavoriteModel, isFavorite: boolean): Observable<FavoriteModel> {
        return of( null );
    },
});

