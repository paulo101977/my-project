import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from './../../models/country';

import { SharedService } from './../../services/shared/shared.service';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class AddressResource {
  private urlCountry = `Country`;

  constructor(private thexApi: ThexApiService, private sharedService: SharedService) {}

  public getCountry(): Observable<ItemsResponse<Country>> {
    return this.thexApi.get<ItemsResponse<Country>>(`${this.urlCountry}?LanguageIsoCode=pt-br`);
  }
}
