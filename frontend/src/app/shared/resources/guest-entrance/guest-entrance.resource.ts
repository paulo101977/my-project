import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http/';
import { Observable } from 'rxjs';

import {ThexApiService} from '@inovacaocmnet/thx-bifrost';
import {FnrhSaveDto} from 'app/shared/models/dto/checkin/fnrh-save-dto';
import {
  ReservationItemCommercialBudgetSearch
} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-commercial-budget-search';
import {ReservationItemBudget} from 'app/shared/models/revenue-management/reservation-budget/reservation-item-budget';
import {DateService} from 'app/shared/services/shared/date.service';
import * as moment from 'moment';
import {CheckinReservationItemBudgetDto} from 'app/shared/models/dto/checkin/checkin-reservation-item-budget-dto';

@Injectable({ providedIn: 'root' })
export class GuestEntranceResource {
  private offerUrl = 'GuestEntrance/';
  private createUrl = 'GuestEntrance/createGuest/';
  private updateUrl = 'GuestEntrance/updateGuest/';

  constructor(private thexApiSvc: ThexApiService) {}

  public getGuestEntrance(
    reservationItemCommercialBudgetSearch: ReservationItemCommercialBudgetSearch
  ): Observable<CheckinReservationItemBudgetDto> {
    let params = new HttpParams();
    Object.getOwnPropertyNames(reservationItemCommercialBudgetSearch).forEach((key, index) => {
      let value = reservationItemCommercialBudgetSearch[key];
      if (value instanceof Date) {
        value = moment(value).format(DateService.DATE_FORMAT_UNIVERSAL);
      }
      if (key != 'childrenAge') {
        params = params.set(key, value);
      }
    });

    if ( reservationItemCommercialBudgetSearch['childrenAge'] ) {
      for (const childrenAge of reservationItemCommercialBudgetSearch['childrenAge']) {
        params = params.append('childrenAge', childrenAge + '' );
      }
    }
    return this.thexApiSvc.get<CheckinReservationItemBudgetDto>
    (`${this.offerUrl}${reservationItemCommercialBudgetSearch.propertyId}`, {
      params,
    });
  }

  public createFnrh(fnrh: FnrhSaveDto): Observable<FnrhSaveDto> {
    return this.thexApiSvc.post<FnrhSaveDto>(`${this.createUrl}`, fnrh);
  }

  public confirmEntrance(guestReservation: Array<any>): Observable<any> {
    return this.thexApiSvc.post<any>(
      `${this.offerUrl}confirm`,
      guestReservation,
    );
  }

  public updateFnrhEntrance(fnrh: FnrhSaveDto): Observable<FnrhSaveDto> {
    return this.thexApiSvc.put<FnrhSaveDto>(`${this.updateUrl}${fnrh.id}`, fnrh);
  }
}
