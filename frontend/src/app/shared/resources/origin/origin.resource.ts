import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Origin } from './../../models/origin';
import { SharedService } from './../../services/shared/shared.service';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class OriginResource {
  private urlOrigin = `BusinessSource`;

  constructor(private httpClient: ThexApiService, private sharedService: SharedService) {}

  public getActiveOrigins(propertyId: number): Observable<ItemsResponse<Origin>> {
    return this.httpClient
      .get<ItemsResponse<Origin>>(`${this.urlOrigin}/?PropertyId=${propertyId}&Order=id,description&PageSize=1000`);
  }

  public getOrigin(propertyId: number): Observable<Origin[]> {
    return this.httpClient
      .get<ItemsResponse<Origin>>(`${this.urlOrigin}/?PropertyId=${propertyId}&BringsInactive=true&Order=id,description&PageSize=1000`)
      .pipe(map(response => response.items));
  }

  public createOrigin(originData: Origin): Observable<Origin> {
    return this.httpClient.post<Origin>(this.urlOrigin, originData);
  }

  public updateOriginStatus(originItem: any): Observable<Origin> {
    return this.httpClient.patch<Origin>(`${this.urlOrigin}/${originItem.id}/toggleactivation`, {});
  }

  public updateOrigin(originItem: any): Observable<Origin> {
    return this.httpClient.put<Origin>(`${this.urlOrigin}/${originItem.id}`, originItem, {});
  }

  public removeOrigin(originItem: any): Observable<Origin> {
    return this.httpClient.delete<Origin>(`${this.urlOrigin}/${originItem.id}`);
  }
}
