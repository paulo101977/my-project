import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from './../../services/shared/shared.service';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { BillingInvoicePropertyDto } from '../../models/dto/rps/billing-invoice-property-dto';
import { Observable } from '../../../../../node_modules/rxjs';
import { ParameterDocumentDto } from '../../models/dto/rps/parameter-document-dto';
import { forkJoin } from 'rxjs';
import { BillingInvoiceModelDto } from '../../models/dto/rps/billing-invoice-model-dto';
import { SubdivisionServiceDto } from '../../models/dto/rps/subdivision-service-dto';
import { DownloadSetupDto } from '../../models/dto/download-setup-dto';

import { ParameterDocumentBillingItemDto } from '../../models/dto/rps/parameter-document-billing-item-dto';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class ParameterDocumentsResource {
  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getAll(propertyId: number): Observable<ItemsResponse<Array<BillingInvoicePropertyDto>>> {
    return this.httpClient.get<ItemsResponse<Array<BillingInvoicePropertyDto>>>(
      `billinginvoiceproperty/${propertyId}/getall`,
    );
  }

  public getAllWithoutReference(): Observable<ItemsResponse<Array<BillingInvoicePropertyDto>>> {
    return this.httpClient.get<ItemsResponse<Array<BillingInvoicePropertyDto>>>(
      `billinginvoiceproperty/getallwithoutreference`,
    );
  }

  public getById(billingItemId: number): Observable<ParameterDocumentDto> {
    return this.httpClient.get<ParameterDocumentDto>(`billinginvoiceproperty/${billingItemId}`);
  }

  public create(parameterDocument: ParameterDocumentDto) {
    return this.httpClient.post(`billinginvoiceproperty/parametersdocument`, parameterDocument);
  }

  public update(parameterDocument: ParameterDocumentDto) {
    return this.httpClient.put(`billinginvoiceproperty`, parameterDocument);
  }

  public save(parameterDocument: ParameterDocumentDto) {
    if (parameterDocument.id) {
      return this.update(parameterDocument);
    } else {
      return this.create(parameterDocument);
    }
  }

  // Suport requests
  public getAllModels(): Observable<ItemsResponse<Array<BillingInvoiceModelDto>>> {
    return this.httpClient.get<ItemsResponse<Array<BillingInvoiceModelDto>>>(`billinginvoicemodel`);
  }

  public getAllVisibleModels(): Observable<ItemsResponse<Array<BillingInvoiceModelDto>>> {
    return this.httpClient.get<ItemsResponse<Array<BillingInvoiceModelDto>>>(`billinginvoicemodel/getallvisible`);
  }

  public getAllCountrySubdivisionServices(propertyId: number): Observable<ItemsResponse<Array<SubdivisionServiceDto>>> {
    return this.httpClient.get<ItemsResponse<Array<SubdivisionServiceDto>>>(
      `countrysubdvisionservice/${propertyId}/getAll`,
    );
  }

  public getBillingItens(propertyId: number): Observable<ItemsResponse<Array<ParameterDocumentBillingItemDto>>> {
    return this.httpClient.get<ItemsResponse<Array<ParameterDocumentBillingItemDto>>>(
      `billingitem/${propertyId}/billingitemforparametersdocuments`,
    );
  }

  public getDownloadSetup() {
    return this.httpClient.get<DownloadSetupDto>(
      `billingInvoiceIntegration/downloadSetup`,
    );
  }

  // Join request
  public getNewInfo(propertyId: number): Observable<any> {
    const modelsRequest = this.getAllVisibleModels();
    const countrySubdivisionRequest = this.getAllCountrySubdivisionServices(propertyId);
    const billingItensRequest = this.getBillingItens(propertyId);
    return forkJoin([modelsRequest, countrySubdivisionRequest, billingItensRequest]);
  }

  public getEditInfo(propertyId: number, billingItemId: number): Observable<any> {
    const modelsRequest = this.getAllVisibleModels();
    const countrySubdivisionRequest = this.getAllCountrySubdivisionServices(propertyId);
    const getByIdRequest = this.getById(billingItemId);
    const billingItensRequest = this.getBillingItens(propertyId);
    return forkJoin([modelsRequest, getByIdRequest, countrySubdivisionRequest, billingItensRequest]);
  }
}
