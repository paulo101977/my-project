import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SharedService } from '../../services/shared/shared.service';
import { RoomBlock } from '../../models/dto/housekeeping/room-block';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class RoomBlockResource {
  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public blockRoom(roomToBlock: RoomBlock): Observable<any> {
    return this.httpClient.post(`housekeepingstatusproperty/blockingroom`, roomToBlock);
  }

  public blockRoomList(roomListToBlock: Array<RoomBlock>): Observable<any> {
    if (roomListToBlock && roomListToBlock.length == 1) {
      return this.blockRoom(roomListToBlock[0]);
    }
    return this.httpClient.post(`housekeepingstatusproperty/roomblockinglist`, roomListToBlock);
  }

  public unblockRoomList(propertyId, roomListToBlock: Array<RoomBlock>): Observable<any> {
    const roomIdObject = {
      roomIdsList: roomListToBlock.map(item => item.roomId),
    };
    return this.httpClient.request('delete', `housekeepingstatusproperty/${propertyId}/unlockroom`, {
      body: roomIdObject,
    });
  }

  public unblockRoom(id: RoomBlock): Observable<any> {
    return this.httpClient.delete(`housekeepingstatusproperty/${id}/unlockroomforavailability`);
  }
}
