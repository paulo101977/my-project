import { Injectable } from '@angular/core';
import { HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PropertyCompanyClientCategory } from '../../models/property-company-client-category';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class CategoryResource {
  private urlCategory = `PropertyCompanyClientCategory`;

  constructor(private httpClient: HttpService) {}

  public getCategory(propertyId: number, all?: boolean): Observable<ItemsResponse<PropertyCompanyClientCategory>> {

    let params = new HttpParams();

    if (all) {
      params = params.append('all', String(all));
    }

    return this.httpClient.get<ItemsResponse<PropertyCompanyClientCategory>>(
      `${this.urlCategory}/${propertyId}/getallcompanyclientcategory`, {params: params}
    );
  }

  public createCategory(categoryData: PropertyCompanyClientCategory): Observable<PropertyCompanyClientCategory> {
    return this.httpClient.post<PropertyCompanyClientCategory>(this.urlCategory, categoryData);
  }

  public updateCategory(categoryItem: any): Observable<PropertyCompanyClientCategory> {
    return this.httpClient.put<PropertyCompanyClientCategory>(this.urlCategory, categoryItem);
  }

  public updateCategoryStatus(categoryItem: any): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.urlCategory}/${categoryItem.id}/toggleactivation`, {}, { observe: 'response' });
  }

  public removeCategory(categoryItem: any): Observable<HttpResponse<Object>> {
    return this.httpClient.delete(`${this.urlCategory}/${categoryItem.id}/delete`, { observe: 'response' });
  }
}
