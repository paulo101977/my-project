import { createServiceStub } from '../../../../../../testing';
import { CategoryResource } from 'app/shared/resources/category/category.resource';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { PropertyCompanyClientCategory } from 'app/shared/models/property-company-client-category';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';

export const categoryResourceStub = createServiceStub(CategoryResource, {
    createCategory(categoryData: PropertyCompanyClientCategory): Observable<PropertyCompanyClientCategory> {
        return of( null );
    }, getCategory(propertyId: number, all?: boolean): Observable<ItemsResponse<PropertyCompanyClientCategory>> {
        return of( null );
    }, removeCategory(categoryItem: any): Observable<HttpResponse<Object>> {
        return of( null );
    }, updateCategory(categoryItem: any): Observable<PropertyCompanyClientCategory> {
        return of( null );
    }, updateCategoryStatus(categoryItem: any): Observable<HttpResponse<Object>> {
        return of( null );
    }
});
