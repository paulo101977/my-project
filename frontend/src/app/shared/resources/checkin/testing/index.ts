import { createServiceStub } from '../../../../../../testing';
import { CheckinResource } from 'app/shared/resources/checkin/checkin.resource';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { FnrhSaveDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { CheckInGetNumUhsDto } from 'app/shared/models/dto/checkin/checkin-get-numuhs-dto';
import { CheckInHeaderDto } from 'app/shared/models/dto/checkin/checkin-header-dto';
import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import { CheckInGetRoomTypesDto } from 'app/shared/models/dto/checkin/checkin-get-room-types-dto';
import { PropertyGuestTypeDto } from 'app/shared/models/dto/property-guest-type-dto';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { NationalityDto } from 'app/shared/models/dto/checkin/nationality-dto';
import { OccupationDto } from 'app/shared/models/dto/checkin/occupation-dto';
import { ReasonDto } from 'app/shared/models/dto/checkin/reason-dto';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { TransportationTypeDto } from 'app/shared/models/dto/checkin/transportation-type-dto';

export const checkinResourceStub = createServiceStub(CheckinResource, {
    associateRoomToReservationItem(propertyId: number, reservationItemId:
        number, roomTypeId: number, roomId: number): Observable<HttpResponse<Object>> {
        return of (null );
    },
    confirmCheckin(guestReservationItemIdList: Array<any>, reservationId: number, propertyId: number): Observable<any> {
        return of (null );
    },
    createFnrh(fnrh: FnrhSaveDto): Observable<FnrhSaveDto> {
        return of (null );
    },
    getAvailableRoomsByPeriod(roomType: number, arrivalData: any, departureDate: any, propertyId: number): Observable<CheckInGetNumUhsDto> {
        return of (null );
    },
    getCompleteAuxInfo(propertyId, reservationItemId, reservationId, language): Observable<any> {
        return of (null );
    },
    getCompleteReservationInfo(propertyId, guestReservationItemId, language, checkinHeaderDto: CheckInHeaderDto): Observable<any> {
        return of (null );
    },
    getFnrh(guestReservationItemId: number, propertyId: number, languageIsoCode: string): Observable<FnrhDto> {
        return of (null );
    },
    getGuestTypesByPropertyId(propertyId: number): Observable<ItemsResponse<PropertyGuestTypeDto>> {
        return of (null );
    },
    getHeader(reservationItemId: number): Observable<CheckInHeaderDto> {
        return of (null );
    },
    getNationalities(languageIsoCode: string): Observable<ItemsResponse<NationalityDto>> {
        return of (null );
    },
    getOccupations(): Observable<ItemsResponse<OccupationDto>> {
        return of (null );
    },
    getReasonTravel(propertyId: number, reasonCategoryId: number): Observable<ItemsResponse<ReasonDto>> {
        return of (null );
    },
    getReservationGuestList(propertyId: number, reservationItemId: number): Observable<ItemsResponse<CheckinGuestReservationItemDto>> {
        return of (null );
    },
    getResponsables(propertyId: number, reservationId): Observable<ItemsResponse<CheckinGuestReservationItemDto>> {
        return undefined;
    },
    getRoomTypes(propertyId: number, data: CheckInHeaderDto): Observable<CheckInGetRoomTypesDto> {
        return of (null );
    },
    getRoomTypesActive(propertyId: number, actualDate: any): Observable<CheckInGetRoomTypesDto> {
        return of (null );
    },
    getTransportationType(): Observable<ItemsResponse<TransportationTypeDto>> {
        return of (null );
    },
    getUhNums(data: CheckInHeaderDto, propertyId: number): Observable<CheckInGetNumUhsDto> {
        return of (null );
    },
    getUhNumsByTypeAndPeriod(roomType: number, arrivalData: any, departureDate: any, propertyId: number, reservationItemId?: number):
        Observable<CheckInGetNumUhsDto> {
        return of (null );
    },
    updateFnrh(fnrh: FnrhSaveDto): Observable<FnrhSaveDto> {
        return of (null );
    }

});
