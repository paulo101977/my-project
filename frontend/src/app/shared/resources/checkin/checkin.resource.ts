import { Injectable } from '@angular/core';
import { HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, forkJoin, of } from 'rxjs';

import { CheckInGetRoomTypesDto } from '../../models/dto/checkin/checkin-get-room-types-dto';
import { CheckInHeaderDto } from '../../models/dto/checkin/checkin-header-dto';
import { CheckInGetNumUhsDto } from '../../models/dto/checkin/checkin-get-numuhs-dto';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { PropertyGuestTypeDto } from 'app/shared/models/dto/property-guest-type-dto';
import { TransportationTypeDto } from 'app/shared/models/dto/checkin/transportation-type-dto';
import { ReasonDto } from '../../models/dto/checkin/reason-dto';
import { NationalityDto } from '../../models/dto/checkin/nationality-dto';
import { CheckinGuestReservationItemDto } from 'app/shared/models/dto/checkin/guest-reservation-item-dto';
import { FnrhDto } from 'app/shared/models/dto/checkin/fnhr-dto';
import { FnrhSaveDto } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { OccupationDto } from '../../models/dto/checkin/occupation-dto';
import { SharedService } from '../../services/shared/shared.service';
import { DocumentTypeResource } from '../document-type/document-type.resource';
import { HttpService } from '../../../core/services/http/http.service';
import { ReasonCategoryEnum } from 'app/shared/models/reason/reason-category-enum';
import { switchMap } from 'rxjs/operators';
import { SupportApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class CheckinResource {
  constructor(
    private httpClient: HttpService,
    private sharedService: SharedService,
    private documentService: DocumentTypeResource,
    private supportApi: SupportApiService
  ) {
  }

  public getHeader(reservationItemId: number): Observable<CheckInHeaderDto> {
    return this.httpClient.get<CheckInHeaderDto>(`Reservations/${reservationItemId}/header`);
  }

  public getRoomTypes(propertyId: number, data: CheckInHeaderDto): Observable<CheckInGetRoomTypesDto> {
    return this.httpClient.get<CheckInGetRoomTypesDto>(
      `Roomtypes/${propertyId}/withoverbooking?InitialDate=${data.arrivalDate}&FinalDate=${
        data.departureDate
      }&IsActive=true&ExpandRooms=false&ChildCapacity=${data.childCount}&AdultCapacity=${data.adultCount}`,
    );
  }

  public getRoomTypesActive(propertyId: number, actualDate: any): Observable<CheckInGetRoomTypesDto> {
    let Params = new HttpParams();

    Params = Params.append('InitialDate', actualDate);
    Params = Params.append('FinalDate', actualDate);
    Params = Params.append('IsActive', 'true');
    Params = Params.append('ExpandRooms', 'false');
    return this.httpClient.get<CheckInGetRoomTypesDto>(`Roomtypes/${propertyId}/withoverbooking`, {
      params: Params,
    });
  }

  public getUhNums(data: CheckInHeaderDto, propertyId: number): Observable<CheckInGetNumUhsDto> {
    return this.httpClient.get<CheckInGetNumUhsDto>(
      `Rooms/${propertyId}/${data.receivedRoomTypeId}/byperiodofroomtype?InitialDate=${
        data.arrivalDate
      }&FinalDate=${data.departureDate}`,
    );
  }

  public getUhNumsByTypeAndPeriod(
    roomType: number,
    arrivalData: any,
    departureDate: any,
    propertyId: number,
    reservationItemId?: number
  ): Observable<CheckInGetNumUhsDto> {
    const queryParams = this.prepareRoomByPeriodParams(arrivalData, departureDate, reservationItemId);
    return this.httpClient.get<CheckInGetNumUhsDto>(
      `Rooms/${propertyId}/${roomType}/byperiodofroomtype`, {params: queryParams}
    );
  }

  public getAvailableRoomsByPeriod( roomType: number,
                                    arrivalData: any,
                                    departureDate: any,
                                    propertyId: number): Observable<CheckInGetNumUhsDto>  {
    const queryParams = this.prepareRoomByPeriodParams(arrivalData, departureDate, null);

    return this.httpClient.get<CheckInGetNumUhsDto>(
      `Rooms/${propertyId}/${roomType}/byperiodofroomtype/changeofroom`, {params: queryParams}
    );

  }

  public getReservationGuestList(propertyId: number, reservationItemId: number): Observable<ItemsResponse<CheckinGuestReservationItemDto>> {
    return this.httpClient.get<ItemsResponse<CheckinGuestReservationItemDto>>(
      `Reservationitems/${propertyId}/${reservationItemId}/${window.navigator.language}/guestguestRegistration`
    );
  }

  public getGuestTypesByPropertyId(propertyId: number): Observable<ItemsResponse<PropertyGuestTypeDto>> {
    return this.httpClient.get<ItemsResponse<PropertyGuestTypeDto>>(
      `PropertyGuestType/${propertyId}/byproperty`,
    );
  }

  public getNationalities(languageIsoCode: string): Observable<ItemsResponse<NationalityDto>> {
    return this.httpClient.get<ItemsResponse<NationalityDto>>(`nationality?Language=${languageIsoCode}`);
  }

  public getReasonTravel(propertyId: number, reasonCategoryId: number): Observable<ItemsResponse<ReasonDto>> {
    return this.httpClient.get<ItemsResponse<ReasonDto>>(
      `reason?PropertyId=${propertyId}&ReasonCategoryId=${reasonCategoryId}`,
    );
  }

  public getTransportationType(): Observable<ItemsResponse<TransportationTypeDto>> {
    return this.supportApi.get<ItemsResponse<TransportationTypeDto>>(`TransportationType`);
  }

  public associateRoomToReservationItem(
    propertyId: number,
    reservationItemId: number,
    roomTypeId: number,
    roomId: number,
  ): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(
      `Reservationitems/${propertyId}/${reservationItemId}/associateroom/${roomTypeId}/${roomId}/`,
      {},
      { observe: 'response' },
    );
  }

  public getFnrh(guestReservationItemId: number, propertyId: number, languageIsoCode: string): Observable<FnrhDto> {
    return this.httpClient.get<FnrhDto>(
      `guestRegistration/ByGuestReservationItem/${guestReservationItemId}/property/${propertyId}/language/${languageIsoCode}`,
    );
  }

  public createFnrh(fnrh: FnrhSaveDto): Observable<FnrhSaveDto> {
    return this.httpClient.post<FnrhSaveDto>(`guestRegistration`, fnrh);
  }

  public updateFnrh(fnrh: FnrhSaveDto): Observable<FnrhSaveDto> {
    return this.httpClient.put<FnrhSaveDto>(`guestRegistration/${fnrh.id}`, fnrh);
  }

  public confirmCheckin(guestReservationItemIdList: Array<any>, reservationId: number, propertyId: number): Observable<any> {
    return this.httpClient.patch<any>(
      `GuestReservationItem/${reservationId}/${propertyId}/confirmcheckin`,
      guestReservationItemIdList,
    );
  }

  public getOccupations(): Observable<ItemsResponse<OccupationDto>> {
    return this.httpClient.get<ItemsResponse<OccupationDto>>(`GeneralOccupation`);
  }

  public getResponsables(propertyId: number, reservationId): Observable<ItemsResponse<CheckinGuestReservationItemDto>> {
    return this.httpClient.get<ItemsResponse<CheckinGuestReservationItemDto>>(
      `guestRegistration/${propertyId}/${reservationId}/responsibles`,
    );
  }

  public getCompleteAuxInfo(propertyId, reservationItemId, reservationId, language): Observable<any> {
    const reasonToTravelCategoryId = ReasonCategoryEnum.PurposeOfTrip;
    const documentRequest = this.documentService.getAllDocumentTypes();
    const guestTypeRequest = this.getGuestTypesByPropertyId(propertyId);
    const transportRequest = this.getTransportationType();
    const nacionalityRequest = this.getNationalities(language);
    const reasonsRequest = this.getReasonTravel(propertyId, reasonToTravelCategoryId);
    const occupationRequest = this.getOccupations();
    const guestRegistrationRequest = this.getReservationGuestList(propertyId, reservationItemId);

    return this.getHeader(reservationItemId).pipe(
      switchMap((responseHeader) => forkJoin([
        documentRequest,
        guestTypeRequest,
        transportRequest,
        nacionalityRequest,
        reasonsRequest,
        occupationRequest,
        of(responseHeader),
        guestRegistrationRequest,
        this.getResponsables(propertyId, responseHeader.id),
      ]))
    );
  }

  public getCompleteReservationInfo(propertyId, guestReservationItemId, language, checkinHeaderDto: CheckInHeaderDto): Observable<any> {
    const fnrhRequest = this.getFnrh(guestReservationItemId, propertyId, language);
    const roomTypesRequest = this.getRoomTypes(propertyId, checkinHeaderDto);
    const roomsRequest = this.getUhNums(checkinHeaderDto, propertyId);
    return forkJoin([fnrhRequest, roomTypesRequest, roomsRequest]);
  }


  private prepareRoomByPeriodParams(arrivalData: any, departureDate: any, reservationItemId: number) {
    let queryParams = new HttpParams();
    queryParams = queryParams.append('InitialDate', arrivalData);
    queryParams = queryParams.append('FinalDate', departureDate);
    if (reservationItemId) {
      queryParams = queryParams.append('reservationItemId', reservationItemId + '');
    }
    return queryParams;
  }
}
