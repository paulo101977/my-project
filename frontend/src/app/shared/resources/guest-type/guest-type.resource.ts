import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http/';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { PropertyGuestTypeDto } from '../../models/dto/property-guest-type-dto';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class GuestTypeResource {
  private url = 'PropertyGuestType';

  constructor(private httpClient: HttpService) {
  }

  public getAllGuestTypeListByPropertyId(propertyId: number): Observable<ItemsResponse<PropertyGuestTypeDto>> {
    return this.httpClient.get<ItemsResponse<PropertyGuestTypeDto>>(`${this.url}/${propertyId}/byproperty`);
  }

  public createGuestType(guestType: PropertyGuestTypeDto): Observable<PropertyGuestTypeDto> {
    return this.httpClient.post<PropertyGuestTypeDto>(this.url, guestType);
  }

  public updateGuestType(guestType: PropertyGuestTypeDto): Observable<PropertyGuestTypeDto> {
    return this.httpClient.put<PropertyGuestTypeDto>(`${this.url}/${guestType.id}`, guestType);
  }

  public getGuestTypeById(id: number): Observable<PropertyGuestTypeDto> {
    return this.httpClient.get<PropertyGuestTypeDto>(`${this.url}/${id}`);
  }

  public deleteGuestType(id: number): Observable<HttpResponse<Object>> {
    return this.httpClient.delete(`${this.url}/${id}`, { observe: 'response' });
  }

  public updateGuestTypeStatus(id: number): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${id}/toggleactivation`, {}, { observe: 'response' });
  }

}
