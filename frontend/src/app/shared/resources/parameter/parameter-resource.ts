import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {forkJoin, Observable} from 'rxjs';
import {Parameter, ParameterGroup, ParameterReturn} from '../../models/parameter';
import {ParameterSystemDate} from '../../components/bottom-bar/parameter-system-date';
import {HttpService} from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class ParameterResource {
  constructor(private httpClient: HttpService) {}

  public getParametersList(propertyId: number): Observable<Array<ParameterGroup>> {
    return this.httpClient
      .get<ParameterReturn>(`PropertyParameter/${propertyId}/propertyparameters`).pipe(
      map(res => res.propertyParameters));
  }

  public saveParameterChange(parameter: Parameter, propertyId: number): Observable<any> {
    if (!this.parameterHasPropertyId(parameter)) {
      parameter.propertyId = propertyId;
    }
    return this.httpClient.put('PropertyParameter/updatesingle', parameter);
  }

  public saveAllParametersChange(parameterReturn: ParameterReturn): Observable<any> {
    return this.httpClient.put('PropertyParameter', parameterReturn);
  }

  public toggleParameter(parameter: Parameter): Observable<any> {
    return this.httpClient.patch(`PropertyParameter/${parameter.id}/toggleactivation`, parameter);
  }

  public saveChildRange(params: Array<Parameter>, propertyId: number): Observable<any> {
    const range1 = this.saveParameterChange(params[0], propertyId);
    const range2 = this.saveParameterChange(params[1], propertyId);
    const range3 = this.saveParameterChange(params[2], propertyId);

    return forkJoin([range1, range2, range3]);
  }

  private parameterHasPropertyId(parameter: Parameter) {
    return parameter.propertyId || parameter.propertyId <= 0;
  }

  public getParameterSystemDate(propertyId: number) {
    return this.httpClient.get<ParameterSystemDate>(`PropertyParameter/${propertyId}/systemdate`);
  }
}
