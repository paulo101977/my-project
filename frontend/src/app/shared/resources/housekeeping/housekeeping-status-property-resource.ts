import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { HousekeepingStatusProperty } from '../../models/dto/housekeeping/housekeeping-status-property-dto';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({providedIn: 'root'})
export class HousekeepingStatusPropertyResource {
  private endpoint = 'housekeepingstatusproperty/';

  constructor(private httpClient: HttpService) {
  }

  public getAll(propertyId): Observable<HousekeepingStatusProperty[]> {
    return this.httpClient
      .get<ItemsResponse<HousekeepingStatusProperty>>(
        `${this.endpoint}${propertyId}/getAllhousekeepingalterstatusroom`)
      .pipe(map(data => data.items));
  }

  public getAllFilterByHousekeepingStatus(propertyId, housekeepingStatusId): Observable<HousekeepingStatusPropertyResource[]> {
    return this.httpClient
      .get<ItemsResponse<HousekeepingStatusPropertyResource>>(
        `${this.endpoint}${propertyId}/getAllhousekeepingalterstatusroom?HousekeepingPropertyStatusId=${housekeepingStatusId}`)
      .pipe(map(data => data.items));
  }

  public getAllFilterBySearchData(propertyId, term): Observable<HousekeepingStatusPropertyResource[]> {
    return this.httpClient
      .get<ItemsResponse<HousekeepingStatusPropertyResource>>(
        `${this.endpoint}${propertyId}/getAllhousekeepingalterstatusroom?SearchData=${term}`)
      .pipe(map(data => data.items));
  }

  public changeMultipleStatus(housekeepingStatusProperty, roomIdList: Array<number>): Observable<any> {
    return this.httpClient
      .put(`${this.endpoint}housestatuskeeping/${housekeepingStatusProperty}/altermanyroom`, {
          roomIdsList: roomIdList
        }
      );
  }
}
