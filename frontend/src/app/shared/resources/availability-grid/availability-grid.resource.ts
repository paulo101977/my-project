
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import * as moment from 'moment';
import { AvailabilityResourcesDto } from '../../models/dto/availability-grid/availability-resources-dto';
import { AvailabilityRoomsDto } from '../../models/dto/availability-grid/availability-rooms-dto';
import { AvailabilityReservationChangeDto } from '../../models/dto/availability-grid/availability-reservation-dto';
import { SharedService } from '../../services/shared/shared.service';
import { DateService } from '../../services/shared/date.service';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class AvailabilityGridResource {
  constructor(private httpClient: HttpService, private sharedService: SharedService, private dateService: DateService) {}

  public getRoomTypes(propertyId: number, initialDate: string, period: number): Observable<AvailabilityResourcesDto> {

    return this.httpClient
      .get<AvailabilityResourcesDto>(
        `Availability/${propertyId}/roomtypes?InitialDate=${initialDate}&Period=${period}`,
      ).pipe(
      map(data => {
        data.availabilityList.map(availabilityItem => {
          availabilityItem.date = moment(availabilityItem.dateFormatted, DateService.DATE_FORMAT_UNIVERSAL)
            .format(this.dateService.MOMENT_DATE_FORMAT_US);
        });

        data.footerList.map(footerItem => {
          footerItem.values.map(value => {
            value.date = value.dateFormatted || value.date;
            value.date = moment(value.date, DateService.DATE_FORMAT_UNIVERSAL)
              .format(this.dateService.MOMENT_DATE_FORMAT_US);
            return value;
          });

          return footerItem;
        });

        return data;
      }));
  }

  public getRooms(propertyId: number, roomTypeId: number, initialDate: string, period: number): Observable<AvailabilityRoomsDto> {
    return this.httpClient
      .get<AvailabilityRoomsDto>(
        `Availability/${propertyId}/${roomTypeId}/rooms?InitialDate=${initialDate}&Period=${period}`,
      ).pipe(
      map(data => {
        data.reservationList.map(reservation => {
          reservation.start = reservation.startFormatted;
          reservation.end = reservation.endFormatted;
        });
        return data;
      }));
  }

  public changeReservationPeriod(propertyId: number, reservation: AvailabilityReservationChangeDto) {
    return this.httpClient.patch(
      `Reservationitems/${propertyId}/${reservation.id}/changePeriod`,
      reservation,
      { observe: 'response' },
    );
  }
}
