import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http/';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { Reason, ReasonCategory } from './../../models/dto/reason';
import { SharedService } from './../../services/shared/shared.service';
import { HttpService } from 'app/core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class ReasonResource {
  private url = 'reason';

  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getAllReasonList(propertyId: number): Observable<ItemsResponse<Reason>> {
    return this.httpClient.get<ItemsResponse<Reason>>(`${this.url}/${propertyId}/getallreason`);
  }

  public getAllReasonCategoryList(): Observable<ItemsResponse<ReasonCategory>> {
    return this.httpClient.get<ItemsResponse<ReasonCategory>>(`${this.url}/getallreasoncategory`);
  }

  public deleteReason(id: number): Observable<HttpResponse<Object>> {
    return this.httpClient.delete(`${this.url}/${id}/delete`, { observe: 'response' });
  }

  public createReason(reason: Reason): Observable<Reason> {
    return this.httpClient.post<Reason>(`${this.url}`, reason);
  }

  public updateReason(reason: Reason): Observable<HttpResponse<Object>> {
    return this.httpClient.put(`${this.url}`, reason, { observe: 'response' });
  }

  public updateReasonStatus(reason: Reason): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${reason.id}/toggleactivation`, reason, { observe: 'response' });
  }

  public getAllReasonListByPropertyIdAndCategoryId(propertyId: number, reasonCategoryId: number): Observable<ItemsResponse<Reason>> {
    return this.httpClient.get<ItemsResponse<Reason>>(`${this.url}?PropertyId=${propertyId}&ReasonCategoryId=${reasonCategoryId}`);
  }

  public getMoveRoomList(propertyId: number, reasonCategoryId: number): Observable<ItemsResponse<Reason>> {
    return this.httpClient.get<ItemsResponse<Reason>>(`${this.url}/${propertyId}/${reasonCategoryId}/getallreason`);
  }
}
