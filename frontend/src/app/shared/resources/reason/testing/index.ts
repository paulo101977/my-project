import { createServiceStub } from '../../../../../../testing';
import { ReasonResource } from 'app/shared/resources/reason/reason.resource';
import { Reason, ReasonCategory } from 'app/shared/models/dto/reason';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';

export const reasonResourceStub = createServiceStub(ReasonResource, {
    createReason(reason: Reason): Observable<Reason> {
        return of(null);
    },

    deleteReason(id: number): Observable<HttpResponse<Object>> {
        return of(null);
    },

    getAllReasonCategoryList(): Observable<ItemsResponse<ReasonCategory>> {
        return of(null);
    },

    getAllReasonList(propertyId: number): Observable<ItemsResponse<Reason>> {
        return of(null);
    },

    getAllReasonListByPropertyIdAndCategoryId(propertyId: number, reasonCategoryId: number): Observable<ItemsResponse<Reason>> {
        return of(null);
    },

    getMoveRoomList(propertyId: number, reasonCategoryId: number): Observable<ItemsResponse<Reason>> {
        return of(null);
    },

    updateReason(reason: Reason): Observable<HttpResponse<Object>> {
        return of(null);
    },

    updateReasonStatus(reason: Reason): Observable<HttpResponse<Object>> {
        return of(null);
    }
});
