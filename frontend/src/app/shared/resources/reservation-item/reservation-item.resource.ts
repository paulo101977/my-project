import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http/';
import { ReasonCategoryCancel } from './../../models/reasonCategoryCancel';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { AccomodationChange } from '../../models/accomodation-change';

import { SharedService } from 'app/shared/services/shared/shared.service';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class ReservationItemResource {
  private url = 'ReservationItems';

  constructor(private httpClient: ThexApiService) {}

  public cancelReservationItem(
    reservationItemId: number,
    reasonId: number,
    cancellationDescription: string,
  ): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(
      `${this.url}/${reservationItemId}/cancel`,
      {
        reasonId: reasonId,
        cancellationDescription: cancellationDescription,
      },
      { observe: 'response' },
    );
  }

  public getReservationItemReasons(propertyId: number): Observable<ItemsResponse<ReasonCategoryCancel>> {
    return this.httpClient.get<ItemsResponse<ReasonCategoryCancel>>(
      `reason?PropertyId=${propertyId}&ReasonCategoryId=1`,
    );
  }

  public reactivateReservationItem(reservationItemId: number): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${reservationItemId}/reactivate`, {}, { observe: 'response' });
  }

  public setAccomodationChange(accomodationChange: AccomodationChange): Observable<AccomodationChange> {
    return this.httpClient.post<AccomodationChange>(`${this.url}/accommodationchange`, accomodationChange);
  }

  public getReservationSlipByPropertyIdAndReservationId(propertyId: number, reservationId: number): Observable<any> {
    return this.httpClient.get(`${this.url}/property/${propertyId}/reservationId/${reservationId}/confirmationreservation`, {
      responseType: 'text',
    });
  }

  public sendReservationSlip(propertyId: number, reservationId: number, emailList: Array<string>): Observable<any> {
    return this.httpClient.post(`${this.url}/property/${propertyId}/reservation/${reservationId}/sendemail`, emailList);
  }

  public getReservationItemById(reservationItemId: number): Observable<any> {
    return this.httpClient.get(`${this.url}/${reservationItemId}`);
  }

  public confirm(reservationItemId: number): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${reservationItemId}/confirm`, {}, { observe: 'response' });
  }

  public cancelCheckin(reservationItemId: number): Observable<any> {
    return this.httpClient.patch(`${this.url}/${reservationItemId}/cancelcheckin`);
  }

  public reactivateNoShow(reservationItemId: number): Observable<any> {
    return this.httpClient.patch(`${this.url}/${reservationItemId}/reactivateNoShow`);
  }

}
