import { createServiceStub } from '../../../../../../testing';
import { RatePlanResource } from 'app/shared/resources/rate-plan/rate-plan.resource';
import { Observable, of } from 'rxjs';
import { RatePlanDto } from 'app/shared/models/dto/rate-plan/rate-plan-dto';

export const ratePlanResourceStub = createServiceStub(RatePlanResource, {
    getAll(propertyId, params?: any): Observable<any> {
        return of( null );
    },

    getAllBillingItens(propertyId): Observable<any> {
        return of( null );
    },

    getAllCompanyclient(propertyId): Observable<any> {
        return of( null );
    },

    getAllCompanyClientCategories(propertyId): Observable<any> {
        return of( null );
    },

    getAllCreateInfo(propertyId): Observable<any> {
        return of( null );
    },

    getAllUhTypes(propertyId): Observable<any> {
        return of( null );
    },

    getCompleteRatePlanEditInfo(propertyId: number, ratePlanId: string): Observable<any> {
        return of( null );
    },

    getCompleteRatePlanHeaderInfo(propertyId): Observable<any> {
        return of( null );
    },

    getRatePlanById(ratePlanId): Observable<any> {
        return of( null );
    },

    save(ratePlan: RatePlanDto): Observable<any> {
        return of( null );
    },

    toggleStatus(ratePlanId): Observable<any> {
        return of( null );
    }
});
