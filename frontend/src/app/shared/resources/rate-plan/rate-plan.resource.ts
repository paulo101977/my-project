import { map } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SharedService } from './../../services/shared/shared.service';
import {
  RatePlanCreateGetDto,
  RatePlanCreateGetResponseDTO
} from '../../models/dto/rate-plan/rate-plan-create-get-dto';
import { ItemsResponse } from '../../models/backend-api/item-response';
import { RatePlanDto } from '../../models/dto/rate-plan/rate-plan-dto';
import { RatePlanRoomTypeDto } from '../../models/dto/rate-plan/rate-plan-room-type-dto';
import { CompanyClientDto } from '../../models/dto/rate-plan/company-client-dto';
import { RatePlanBillingItemDto } from '../../models/dto/rate-plan/rate-plan-billing-item-dto';
import { CompanyClientCategoryDto } from '../../models/dto/rate-plan/company-client-category-dto';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class RatePlanResource {
  constructor(private httpClient: ThexApiService, private sharedService: SharedService) {}

  public getAll(propertyId, params?: any): Observable<any> {
    params = new HttpParams({ fromObject: <any>params });
    return this.httpClient.get<ItemsResponse<RatePlanDto>>('ratePlan/' + propertyId + '/rateplans', {
      params: params,
    });
  }

  public getRatePlanById(ratePlanId): Observable<any> {
    return this.httpClient.get<ItemsResponse<RatePlanDto>>('ratePlan/' + ratePlanId);
  }

  public toggleStatus(ratePlanId) {
    return this.httpClient.patch('ratePlan/' + ratePlanId + '/toggleactivation', null);
  }

  public getCompleteRatePlanHeaderInfo(propertyId): Observable<any> {
    return this.httpClient
      .get<RatePlanCreateGetResponseDTO>(`ratePlan/${propertyId}/headerrateplan`).pipe(
      map(result => {
        const ratePlanCreateGet = new RatePlanCreateGetDto();
        ratePlanCreateGet.currencyList = result.currencyList.items;
        ratePlanCreateGet.agreementTypeList = result.agreementTypeList.items;
        ratePlanCreateGet.propertyMealPlanTypeList = result.propertyMealPlanTypeList.items;
        ratePlanCreateGet.marketSegmentList = result.marketSegmentList.items;
        ratePlanCreateGet.propertyRateTypeList = result.propertyRateTypeList.items;

        return ratePlanCreateGet;
      }));
  }

  public getCompleteRatePlanEditInfo(propertyId: number, ratePlanId: string): Observable<any> {
    const createRequestList = this.getCreateRequest(propertyId);
    const editRequest = this.getRatePlanById(ratePlanId);
    createRequestList.push(editRequest);
    const editRequestList = createRequestList;
    return forkJoin(editRequestList);
  }

  public getAllUhTypes(propertyId): Observable<any> {
    return this.httpClient
      .get<ItemsResponse<RatePlanRoomTypeDto>>('Properties/' + propertyId + '/roomtypes').pipe(
      map(item => item.items));
  }

  public getAllCompanyclient(propertyId): Observable<any> {
    return this.httpClient
      .get<ItemsResponse<CompanyClientDto>>('CompanyClient?propertyId=' + propertyId).pipe(
      map(item => item.items));
  }

  public getAllBillingItens(propertyId): Observable<any> {
    return this.httpClient
      .get<ItemsResponse<RatePlanBillingItemDto>>('ratePlan/' + propertyId + '/billingitens').pipe(
      map(item => item.items));
  }

  public getAllCompanyClientCategories(propertyId): Observable<any> {
    return this.httpClient
      .get<ItemsResponse<CompanyClientCategoryDto>>(
        'PropertyCompanyClientCategory/' + propertyId + '/getallcompanyclientcategory',
      ).pipe(
      map(item => item.items));
  }

  private getCreateRequest(propertyId) {
    const headerRequest = this.getCompleteRatePlanHeaderInfo(propertyId);
    const uhTypesRequest = this.getAllUhTypes(propertyId);
    const companyClientRequest = this.getAllCompanyclient(propertyId);
    const billingItensRequest = this.getAllBillingItens(propertyId);
    const companyClientCategoryRequest = this.getAllCompanyClientCategories(propertyId);
    return [headerRequest, uhTypesRequest, companyClientRequest, billingItensRequest, companyClientCategoryRequest];
  }

  public getAllCreateInfo(propertyId): Observable<any> {
    return forkJoin(this.getCreateRequest(propertyId));
  }

  public save(ratePlan: RatePlanDto): Observable<any> {
    if (ratePlan.id) {
      return this.httpClient.put('ratePlan', ratePlan);
    } else {
      return this.httpClient.post('ratePlan', ratePlan);
    }
  }
}
