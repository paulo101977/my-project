import { MarketSegment } from './../../models/dto/market-segment';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpParams, HttpResponse } from '@angular/common/http/';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class MarketSegmentResource {
  private url = 'MarketSegment';

  constructor(private httpClient: ThexApiService) {}

  public getAllMarketSegmentList(propertyId: number, all?: boolean): Observable<ItemsResponse<MarketSegment>> {
    let params = new HttpParams();
    all
      ? params = new HttpParams().set('all', 'true')
      : params = new HttpParams().set('all', 'false');

    return this.httpClient.get<ItemsResponse<MarketSegment>>(`${this.url}?PropertyId=${propertyId}`, {params: params});
  }

  public deleteMarketSegment(id: number): Observable<HttpResponse<Object>> {
    return this.httpClient.delete(`${this.url}/${id}`, { observe: 'response' });
  }

  public createMarketSegment(marketSegment: MarketSegment): Observable<MarketSegment> {
    return this.httpClient.post<MarketSegment>(`${this.url}`, marketSegment);
  }

  public updateMarketSegment(marketSegment: MarketSegment): Observable<HttpResponse<Object>> {
    return this.httpClient.put(`${this.url}/${marketSegment.id}`, marketSegment, { observe: 'response' });
  }

  public updateMarketSegmentStatus(marketSegment: MarketSegment): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${marketSegment.id}/toggleactivation`, marketSegment, { observe: 'response' });
  }
}
