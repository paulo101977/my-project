import { createServiceStub } from '../../../../../../testing';
import { MarketSegmentResource } from 'app/shared/resources/market-segment/market-segment.resource';
import { Observable, of } from 'rxjs';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { MarketSegment } from 'app/shared/models/dto/market-segment';
import { DATA } from 'app/integration/components/modal-add-edit/mock-data';

export const marketResourceStub = createServiceStub(MarketSegmentResource, {
  getAllMarketSegmentList(propertyId: number, all?: boolean): Observable<ItemsResponse<MarketSegment>> {
    return of(DATA.MARKET_SEGMENT_LIST);
  }
});
