import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { CustomerStation  } from '../../models/integration/customerStation';
import { HttpService } from 'app/core/services/http/http.service';


@Injectable({ providedIn: 'root' })
export class CustomerResource {

    constructor(
      private httpClient: HttpService
    ) {}

    public getAllList(): Observable<CustomerStation[]> {
        return this
          .httpClient
          .get<CustomerStation[]>('http://localhost:3000/customerConsult');
    }

    public loadCustomerDataById(id: number) {
      return this
        .httpClient
        .get(`http://localhost:3000/customer/${id}`);
    }

    public add(item: CustomerStation): Observable<CustomerStation> {
      return this
        .httpClient
        .post
            <CustomerStation>('http://localhost:3000/customerConsult', item);
    }

    private async request(method, url, item?) {
      const _self = this;
      return new Promise( (resolve, reject) => {
        _self
          .httpClient[method](url, item)
          .subscribe( res => {
            resolve(res);
          }, err => {
              reject(err);
          });
      });
    }

    public async editClientStation(item: any) {
      const _self = this;
      const { id } = item;
      const url = `http://localhost:3000/customer/${id}`;

      return this.request('put', url, item);
    }

    public async addNewClientStation(item: any) {
      const _self = this;
      const url = 'http://localhost:3000/customer';

      return this.request('post', url, item);

    }


    public loadSubsidiaryCategory() {
      return this.httpClient.get('http://localhost:3000/subsidiary');
    }


    public edit(item: CustomerStation): any {
      const { id } = item;
      return this.httpClient.put(`http://localhost:3000/customerConsult/${id}`, item);
    }

    // TODO: implement this method
    public deleteById(id: any): any {
      return this.httpClient.delete(`http://localhost:3000/customerConsult/${id}`);
    }

    // TODO: implement other methods of resource


}
