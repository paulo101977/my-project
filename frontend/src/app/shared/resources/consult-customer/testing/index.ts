import { createServiceStub } from '../../../../../../testing';
import { CustomerResource } from 'app/shared/resources/consult-customer/customer.resource';
import { Observable, of } from 'rxjs';
import { CustomerStation } from 'app/shared/models/integration/customerStation';

export const customerResourceStub = createServiceStub(CustomerResource, {
    add(item: CustomerStation): Observable<CustomerStation> {
        return of( null );
    },

    addNewClientStation(item: any): Promise<any> {
        return null;
    },

    deleteById(id: any): any {
    },

    edit(item: CustomerStation): any {
    },

    editClientStation(item: any): Promise<any> {
        return null;
    },

    getAllList(): Observable<CustomerStation[]> {
        return of(null);
    },

    loadCustomerDataById(id: number): Observable<Object> {
        return of(null);
    },

    loadSubsidiaryCategory(): Observable<Object> {
        return of(null);
    }
});
