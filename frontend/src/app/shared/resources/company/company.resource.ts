import { Injectable } from '@angular/core';


import { Company } from './../../models/company';
import { SharedService } from './../../services/shared/shared.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from '../../../core/services/http/http.service';

@Injectable({ providedIn: 'root' })
export class CompanyResource {
  private urlCompany = 'company/';

  constructor(private httpClient: HttpService, private sharedService: SharedService) {}

  public getCompanyById(companyId: number): Observable<Company> {
    return this.httpClient.get<Company>(this.urlCompany + companyId);
  }
}
