import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http/';
import { ItemsResponse } from './../../models/backend-api/item-response';
import { BillingItemCategory } from './../../models/billingType/billing-item-category';
import { HttpService } from '../../../core/services/http/http.service';
import { HttpParams } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class BillingItemCategoryResource {
  private url = 'billingitemcategory';

  constructor(private httpClient: HttpService) {}

  public getAllBillingItemCategoryListByPropertyId(propertyId: number): Observable<ItemsResponse<BillingItemCategory>> {
    return this.httpClient.get<ItemsResponse<BillingItemCategory>>(`${this.url}/${propertyId}/billingItemCategory?PageSize=1000`);
  }

  public getAllBillingItemCategoryActiveListByPropertyId(propertyId: number): Observable<ItemsResponse<BillingItemCategory>> {
    return this.httpClient.get<ItemsResponse<BillingItemCategory>>(
      `billingitemcategory/${propertyId}/getallcategoriesforitem?PageSize=1000`,
    );
  }

  public getBillingItemCategoryFixedList(): Observable<ItemsResponse<BillingItemCategory>> {
    return this.httpClient.get<ItemsResponse<BillingItemCategory>>(`${this.url}/groupfixed?PageSize=1000`);
  }

  public getBillingItemCategoryById(id: number): Observable<BillingItemCategory> {
    return this.httpClient.get<BillingItemCategory>(`${this.url}/${id}`);
  }

  public createBillingItemCategory(billingItemCategory: BillingItemCategory): Observable<HttpResponse<Object>> {
    return this.httpClient.post(`${this.url}`, billingItemCategory, { observe: 'response' });
  }

  public updateBillingItemCategory(id: number, billingItemCategory: BillingItemCategory): Observable<HttpResponse<Object>> {
    return this.httpClient.put(`${this.url}/${id}`, billingItemCategory, { observe: 'response' });
  }

  public deleteBillingItemCategory(id: number): Observable<HttpResponse<Object>> {
    return this.httpClient.delete(`${this.url}/${id}/delete`, { observe: 'response' });
  }

  public updateBillingItemCategoryStatus(id: number): Observable<HttpResponse<Object>> {
    return this.httpClient.patch(`${this.url}/${id}/toggleactivation`, {}, { observe: 'response' });
  }

  // TODO: create model
  public searchBillingItemServiceByCodeOrDescription(propertyId: number, value: string): Observable<ItemsResponse<any>> {
    let  params = new HttpParams();

    if ( value ) {
      params = new HttpParams()
        .set('param', value);
    }


    return this
      .httpClient
      .get<ItemsResponse<any>>(`billingitem/${propertyId}/getallbillingitemservice/search`, { params });
  }

  public getCategoryListWithService(propertyId: number): Observable<ItemsResponse<any>> {
    return this.httpClient.get<ItemsResponse<any>>(`billingitem/${propertyId}/getallbillingitemservice/true`);
  }

  // TODO: create model
  public getBillingItemServiceByCategoryId(propertyId: number, id: string): Observable<ItemsResponse<any>> {

    let  params = new HttpParams();

    if ( id ) {
      params = new HttpParams()
        .set('billingItemGroupId', id)
        .set('launcher', 'true');
    }

    return this
      .httpClient
      .get<ItemsResponse<any>>(`billingitem/${propertyId}/getallserviceforrealese`, { params });
  }
}
