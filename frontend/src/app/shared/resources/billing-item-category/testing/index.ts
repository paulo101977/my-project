import { createServiceStub } from '../../../../../../testing';
import { BillingItemCategoryResource } from 'app/shared/resources/billing-item-category/billing-item-category.resource';
import { Observable, of } from 'rxjs';
import { BillingItemCategory } from 'app/shared/models/billingType/billing-item-category';
import { HttpResponse } from '@angular/common/http';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';

export const billingItemCategoryResourceStub = createServiceStub(BillingItemCategoryResource, {
    createBillingItemCategory(billingItemCategory: BillingItemCategory): Observable<HttpResponse<Object>> {
        return of( null );
    },

    deleteBillingItemCategory(id: number): Observable<HttpResponse<Object>> {
        return of( null );
    },

    getAllBillingItemCategoryActiveListByPropertyId(propertyId: number): Observable<ItemsResponse<BillingItemCategory>> {
        return of( null );
    },

    getAllBillingItemCategoryListByPropertyId(propertyId: number): Observable<ItemsResponse<BillingItemCategory>> {
        return of( null );
    },

    getBillingItemCategoryById(id: number): Observable<BillingItemCategory> {
        return of( null );
    },
    getBillingItemCategoryFixedList(): Observable<ItemsResponse<BillingItemCategory>> {
        return of( null );
    },
    getBillingItemServiceByCategoryId(propertyId: number, id: string): Observable<ItemsResponse<any>> {
        return of( null );
    },
    getCategoryListWithService(propertyId: number): Observable<ItemsResponse<any>> {
        return of( null );
    },
    searchBillingItemServiceByCodeOrDescription(propertyId: number, value: string): Observable<ItemsResponse<any>> {
        return of( null );
    },
    updateBillingItemCategory(id: number, billingItemCategory: BillingItemCategory): Observable<HttpResponse<Object>> {
        return of( null );
    },
    updateBillingItemCategoryStatus(id: number): Observable<HttpResponse<Object>> {
        return of( null );
    }
});
