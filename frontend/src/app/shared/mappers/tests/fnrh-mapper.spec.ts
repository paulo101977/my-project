import { TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { FnrhMapper } from 'app/shared/mappers/fnrh-mapper';
import { DATA, DOCUMENT_TYPE_LIST, FORM_GROUP, LOCATION } from 'app/shared/mappers/mock-data/fnrh-mapper-mock';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';

describe('FnrhMapper', () => {
  let service: FnrhMapper;
  const formGroup = DATA[FORM_GROUP];

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: []
      });

    service = TestBed.get(FnrhMapper);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test getDocumentData method', () => {
    const basicDataValue = formGroup.get('basicData').value;
    const result = service['getDocumentData'](formGroup, DOCUMENT_TYPE_LIST);

    expect(result.ownerId).toEqual(basicDataValue['ownerId']);
    expect(result.documentInformation).toEqual(basicDataValue['document']);
    expect(result.documentTypeId).toEqual(basicDataValue['documentTypeId']);
    expect(result.documentTypeName).toEqual('CPF');
  });

  it('should test getLocationData method normal case', () => {
    const location = DATA[LOCATION];
    const getResult = () =>
      service['getLocationData'](location, formGroup);

    const result = getResult();

    expect(result).toBeTruthy();
  });

  it('should test getLocationData method null case', () => {
    const location = DATA[LOCATION];
    const getResult = () =>
      service['getLocationData'](location, formGroup);

    formGroup.get('basicData').patchValue({documentTypeId: 0});
    formGroup.get('address').patchValue({streetName: '', subdivision: ''});

    const result = getResult();

    expect(result).toBeNull();
  });

  it('should test getLocationData method unset postal code', () => {
    const location = DATA[LOCATION];
    const getResult = () =>
      service['getLocationData'](location, formGroup);

    formGroup.get('basicData').patchValue({documentTypeId: 16});
    formGroup.get('address').patchValue({streetName: 'blabla', subdivision: 'blabla', postalCode: null});
    location.postalCode = 1;

    const result = getResult();

    expect<any>(result.postalCode).toEqual('');
  });

  it('should test getComplementaryInformationData method', () => {
    let result = service['getComplementaryInformationData'](formGroup);

    expect(result.occupationId).not.toBeNull();
    expect(result.arrivingBy).not.toBeNull();
    expect(result.purposeOfTrip).not.toBeNull();

    formGroup.get('complementaryInformation').patchValue({
      occupation: 0,
      vehicle: 0,
      reasonForTravel: 0,
    });
    result = service['getComplementaryInformationData'](formGroup);

    expect(result.occupationId).toBeNull();
    expect(result.arrivingBy).toBeNull();
    expect(result.purposeOfTrip).toBeNull();
  });

  it('should test getBasicData method before update', () => {
    const result = service['getBasicData'](formGroup);

    expect(result.birthDate).not.toBeNull();
    expect(result.age).not.toBeNull();
    expect(result.id).not.toEqual(GuidDefaultData);
    expect(result.personId).not.toEqual(GuidDefaultData);
    expect(result.guestTypeId).not.toBeNull();
    expect(result.nationality).toEqual(1);
    expect(result.isLegallyIncompetent).toEqual(false);
    expect(result.responsibleGuestRegistrationId).not.toBeNull();
  });

  it('should test getBasicData method after update', () => {
    formGroup.get('basicData').patchValue({
      birthDate: null,
      id: null,
      personId: null,
      guestType: null,
      nationality: 0,
      incapable: true,
      responsable: false,
    });

    const result = service['getBasicData'](formGroup);

    expect(result.birthDate).toBeUndefined();
    expect(result.age).toBeUndefined();
    expect(result.id).toEqual(GuidDefaultData);
    expect(result.personId).toEqual(GuidDefaultData);
    expect(result.guestTypeId).toEqual(0);
    expect(result.nationality).toBeNull();
    expect(result.isLegallyIncompetent).not.toEqual(false);
    expect(result.responsibleGuestRegistrationId).toBeNull();
  });

  it('should test toSave method', () => {
    spyOn<any>(service, 'getBasicData').and.callThrough();
    spyOn<any>(service, 'getComplementaryInformationData').and.callThrough();
    spyOn<any>(service, 'getLocationData').and.callThrough();
    spyOn<any>(service, 'getDocumentData').and.callThrough();

    const result = service['toSave'](formGroup);

    expect(service['getBasicData']).toHaveBeenCalled();
    expect(service['getComplementaryInformationData']).toHaveBeenCalled();
    expect(service['getLocationData']).toHaveBeenCalled();
    expect(service['getDocumentData']).toHaveBeenCalled();
  });

  it('should test toSave method', () => {
    spyOn(service, 'toSave').and.callThrough();

    const result = service['toSave'](formGroup);

    expect( Object.keys(result.person).length ).toEqual(8);
    expect(service['toSave']).toHaveBeenCalled();
  });
});
