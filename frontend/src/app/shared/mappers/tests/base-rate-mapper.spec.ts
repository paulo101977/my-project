import { TestBed } from '@angular/core/testing';
import { RoomTypeParametersConfiguration } from 'app/shared/models/revenue-management/room-type-parameters-configuration';
import { BaseRateMapper } from '../base-rate-mapper';

describe('BaseRateMapper', () => {
  let service: BaseRateMapper;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [BaseRateMapper]
    });

    service = TestBed.get(BaseRateMapper);
  });

  it('should create service', () => {
    expect(service).toBeDefined();
  });

  it('should return an empty array when base rate list is empty', () => {
    const arr = service.getBaseRateTableListModel([]);

    expect(arr).toEqual([]);
  });

  it('should return an array of BaseRateTable', () => {
    const roomTypeList: RoomTypeParametersConfiguration[] = [
      {
        abbreviation: '',
        adultCapacity: 0,
        child1Amount: '222',
        child2Amount: '221',
        child3Amount: '223',
        childCapacity: 0,
        childLimitInputs: undefined,
        distributionCode: '',
        freeChildQuantity1: 0,
        freeChildQuantity2: 0,
        freeChildQuantity3: 0,
        id: 0,
        isActive: false,
        isChecked: false,
        maximumRate: 0,
        minimumRate: 0,
        name: '',
        order: 0,
        paxAdditional: '333',
        paxFive: '123',
        paxFour: '132',
        paxLimitInputs: undefined,
        paxOne: '122',
        paxThree: '133',
        paxTwo: '112',
        propertyId: 0,
        roomTypeBedTypeList: undefined
      }
    ];
    const arr = service.getBaseRateTableListModel(roomTypeList);
    const baseRateTable = arr[0];
    const roomType = roomTypeList[0];

    expect(baseRateTable.name).toEqual(roomType.name);
    expect(baseRateTable.roomTypeId).toEqual(roomType.id);
    expect(baseRateTable.pax1Amount).toEqual(+roomType.paxOne);
    expect(baseRateTable.pax2Amount).toEqual(+roomType.paxTwo);
    expect(baseRateTable.pax3Amount).toEqual(+roomType.paxThree);
    expect(baseRateTable.pax4Amount).toEqual(+roomType.paxFour);
    expect(baseRateTable.pax5Amount).toEqual(+roomType.paxFive);
    expect(baseRateTable.paxAdditionalAmount).toEqual(+roomType.paxAdditional);
    expect(baseRateTable.child1Amount).toEqual(+roomType.child1Amount);
    expect(baseRateTable.child2Amount).toEqual(+roomType.child2Amount);
    expect(baseRateTable.child3Amount).toEqual(+roomType.child3Amount);
  });
});
