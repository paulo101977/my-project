import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { PropertyPaymentTypeIssuingBankDetail } from './../models/payment-type/property-payment-type-issuing-bank-detail';
import {
  PropertyPaymentTypeIssuingBankDetailCondition
} from './../models/payment-type/property-payment-type-issuing-bank-detail-condition';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { GetAllPaymentTypeDto } from './../models/payment-type/payment-type';
import { PlasticBrandProperty } from './../models/payment-type/plastic-brand-property';

@Injectable({ providedIn: 'root' })
export class PaymentTypeMapper {
  readonly guidDefault = '00000000-0000-0000-0000-000000000000';

  public toPropertyPaymentTypeIssuingBankDetail(
    formModalAssociateFlag: FormGroup,
    propertyPaymentTypeIssuingBank: GetAllPaymentTypeDto,
    acquirerList: Array<SelectObjectOption>,
    brandList: Array<PlasticBrandProperty>,
  ): PropertyPaymentTypeIssuingBankDetail {
    const propertyPaymentTypeIssuingBankDetail = new PropertyPaymentTypeIssuingBankDetail();
    propertyPaymentTypeIssuingBankDetail.billingItemPaymentTypeId = propertyPaymentTypeIssuingBank.paymentTypeId
      ? propertyPaymentTypeIssuingBank.paymentTypeId
      : 0;
    propertyPaymentTypeIssuingBankDetail.id = formModalAssociateFlag.get('id').value ? formModalAssociateFlag.get('id').value : 0;
    const plasticBrandPropertyId = brandList.find(
      b => b.plasticBrandId == formModalAssociateFlag.get('plasticBrandProperty.plasticBrandPropertyId').value,
    ).id;
    propertyPaymentTypeIssuingBankDetail.plasticBrandPropertyId = brandList.find(
      b => b.plasticBrandId == formModalAssociateFlag.get('plasticBrandProperty.plasticBrandPropertyId').value,
    ).id;
    propertyPaymentTypeIssuingBankDetail.plasticBrandId = brandList.find(
      b => b.plasticBrandId == formModalAssociateFlag.get('plasticBrandProperty.plasticBrandPropertyId').value,
    ).plasticBrandId;
    propertyPaymentTypeIssuingBankDetail.plasticBrandPropertyName = brandList.find(
      b => b.plasticBrandId == formModalAssociateFlag.get('plasticBrandProperty.plasticBrandPropertyId').value,
    ).plasticBrandName;
    propertyPaymentTypeIssuingBankDetail.maximumInstallmentsQuantity = 1;
    propertyPaymentTypeIssuingBankDetail.isActive = formModalAssociateFlag.get('isActive').value;

    const propertyPaymentTypeIssuingBankDetailCondition = new PropertyPaymentTypeIssuingBankDetailCondition();
    propertyPaymentTypeIssuingBankDetailCondition.id = formModalAssociateFlag.get('propertyPaymentTypeIssuingBankDetailConditionId').value;
    propertyPaymentTypeIssuingBankDetailCondition.commissionPct = formModalAssociateFlag.get('commissionPct').value;
    propertyPaymentTypeIssuingBankDetailCondition.paymentDeadline = formModalAssociateFlag.get('paymentDeadline').value;
    propertyPaymentTypeIssuingBankDetailCondition.installmentNumber = 1;
    propertyPaymentTypeIssuingBankDetailCondition.billingItemPaymentTypeDetailId = propertyPaymentTypeIssuingBankDetail.id
      ? propertyPaymentTypeIssuingBankDetail.id
      : '0';

    propertyPaymentTypeIssuingBankDetail.billingItemPaymentTypeDetailConditionList.push(propertyPaymentTypeIssuingBankDetailCondition);
    return propertyPaymentTypeIssuingBankDetail;
  }

  public toCreditCardPropertyPaymentTypeIssuingBankDetail(
    formModalAssociateFlag: FormGroup,
    propertyPaymentTypeIssuingBank: GetAllPaymentTypeDto,
    acquirerList: Array<SelectObjectOption>,
    brandList: Array<PlasticBrandProperty>,
  ): PropertyPaymentTypeIssuingBankDetail {
    const propertyPaymentTypeIssuingBankDetail = new PropertyPaymentTypeIssuingBankDetail();
    propertyPaymentTypeIssuingBankDetail.billingItemPaymentTypeId = propertyPaymentTypeIssuingBank.paymentTypeId
      ? propertyPaymentTypeIssuingBank.paymentTypeId
      : 0;
    propertyPaymentTypeIssuingBankDetail.id = formModalAssociateFlag.get('id').value ? formModalAssociateFlag.get('id').value : 0;
    propertyPaymentTypeIssuingBankDetail.plasticBrandPropertyId = brandList.find(
      b => b.plasticBrandId == formModalAssociateFlag.get('plasticBrandProperty.plasticBrandPropertyId').value,
    ).id;
    propertyPaymentTypeIssuingBankDetail.plasticBrandId = brandList.find(
      b => b.plasticBrandId == formModalAssociateFlag.get('plasticBrandProperty.plasticBrandPropertyId').value,
    ).plasticBrandId;
    propertyPaymentTypeIssuingBankDetail.plasticBrandPropertyName = brandList.find(
      b => b.plasticBrandId == formModalAssociateFlag.get('plasticBrandProperty.plasticBrandPropertyId').value,
    ).plasticBrandName;
    propertyPaymentTypeIssuingBankDetail.maximumInstallmentsQuantity = formModalAssociateFlag.get('maxPlots').value;
    propertyPaymentTypeIssuingBankDetail.isActive = formModalAssociateFlag.get('isActive').value;

    for (let i = 0; i < propertyPaymentTypeIssuingBankDetail.maximumInstallmentsQuantity; i++) {
      if (
        formModalAssociateFlag.get('conditionList').value[i].installmentNumber &&
        formModalAssociateFlag.get('conditionList').value[i].commissionPct &&
        formModalAssociateFlag.get('conditionList').value[i].paymentDeadline
      ) {
        propertyPaymentTypeIssuingBankDetail.billingItemPaymentTypeDetailConditionList.push(
          this.setObjectToPropertyPaymentTypeIssuingBankDetailConditionList(formModalAssociateFlag.get('conditionList').value[i]),
        );
      }
    }

    return propertyPaymentTypeIssuingBankDetail;
  }

  private setObjectToPropertyPaymentTypeIssuingBankDetailConditionList(conditionData: PropertyPaymentTypeIssuingBankDetailCondition) {
    const propertyPaymentTypeIssuingBankDetailCondition = new PropertyPaymentTypeIssuingBankDetailCondition();
    propertyPaymentTypeIssuingBankDetailCondition.id = conditionData.id ? conditionData.id : this.guidDefault;
    propertyPaymentTypeIssuingBankDetailCondition.commissionPct = conditionData.commissionPct;
    propertyPaymentTypeIssuingBankDetailCondition.paymentDeadline = conditionData.paymentDeadline;
    propertyPaymentTypeIssuingBankDetailCondition.installmentNumber = conditionData.installmentNumber;
    propertyPaymentTypeIssuingBankDetailCondition.billingItemPaymentTypeDetailId = conditionData.billingItemPaymentTypeDetailId ?
      conditionData.billingItemPaymentTypeDetailId : '0';

    return propertyPaymentTypeIssuingBankDetailCondition;
  }
}
