import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Room } from './../models/room';

@Injectable({ providedIn: 'root' })
export class RoomMapper {
  public getNewRoomByFormGroupToSendServer(propertyId: number, formGroup: AbstractControl, chosenConjugatedRooms: Array<Room>): Room {
    const room = new Room();
    room.roomNumber = formGroup.get('roomNumber').value;
    room.floor = formGroup.get('floor').value;
    room.wing = formGroup.get('wing').value;
    room.building = formGroup.get('building').value;
    room.remarks = formGroup.get('remarks').value;
    room.isActive = true;
    room.childRoomIdList = this.getConjugatedRoomList(chosenConjugatedRooms);
    room.propertyId = propertyId;

    return room;
  }

  public getRoomUpdatedByFormGroupToSendServer(room: Room, formGroup: AbstractControl, chosenConjugatedRooms: Array<Room>): Room {
    room.roomNumber = formGroup.get('roomNumber').value;
    room.floor = formGroup.get('floor').value;
    room.wing = formGroup.get('wing').value;
    room.building = formGroup.get('building').value;
    room.remarks = formGroup.get('remarks').value;
    room.childRoomIdList = this.getConjugatedRoomList(chosenConjugatedRooms);

    return room;
  }

  private getConjugatedRoomList(chosenConjugatedRooms: Array<Room>): Array<number> {
    const childRoomIdList = new Array<number>();

    if (chosenConjugatedRooms != null) {
      chosenConjugatedRooms.forEach(room => {
        childRoomIdList.push(room.id);
      });
    }

    return childRoomIdList;
  }
}
