import { Injectable } from '@angular/core';
import { BillingAccountClosure } from '../models/billing-account/billing-account-closure';
import { BillingAccountItem } from './../models/billing-account/billing-account-item';
import { BillingAccountForClosure } from './../models/billing-account/billing-account-for-closure';
import { BillingAccountOwner } from './../models/billing-account/billing-account-owner';

@Injectable({ providedIn: 'root' })
export class BillingAccountClosureMapper {
  readonly guidDefault = '00000000-0000-0000-0000-000000000000';

  public toBillingAccountClosure(
    billingAccountList: Array<string>,
    propertyId: number,
    billingAccountItem: BillingAccountItem,
    billingAccountForClosureDestination: BillingAccountForClosure = null,
    billingAccountOwner: BillingAccountOwner = null,
  ): BillingAccountClosure {
    const billingAccountClosure = new BillingAccountClosure();
    billingAccountClosure.billingAccountList = billingAccountList;
    billingAccountClosure.propertyId = propertyId;
    billingAccountClosure.credit = billingAccountItem;
    billingAccountClosure.id = this.guidDefault;
    if (billingAccountOwner && billingAccountForClosureDestination) {
      billingAccountClosure.ownerDestination = billingAccountOwner.ownerId ? billingAccountOwner.ownerId : null;
      billingAccountClosure.isCompany = billingAccountOwner ? billingAccountOwner.isCompany : null;
      billingAccountClosure.reservationId = billingAccountForClosureDestination ? billingAccountForClosureDestination.reservationId : 0;
      billingAccountClosure.reservatiomItemId = billingAccountForClosureDestination
        ? billingAccountForClosureDestination.reservatiomItemId
        : 0;
    } else if (!billingAccountOwner && billingAccountForClosureDestination) {
      billingAccountClosure.ownerDestination = billingAccountForClosureDestination.ownerId
        ? billingAccountForClosureDestination.ownerId
        : null;
      billingAccountClosure.isCompany = billingAccountForClosureDestination ? billingAccountForClosureDestination.isCompany : null;
      billingAccountClosure.reservationId = billingAccountForClosureDestination ? billingAccountForClosureDestination.reservationId : 0;
      billingAccountClosure.reservatiomItemId = billingAccountForClosureDestination
        ? billingAccountForClosureDestination.reservatiomItemId
        : 0;
    }
    return billingAccountClosure;
  }

  public toBillingAccountClosureWhenTotalIsZero(billingAccountId: string, propertyId: number): BillingAccountClosure {
    const billingAccountClosure = new BillingAccountClosure();
    billingAccountClosure.billingAccountList = [billingAccountId];
    billingAccountClosure.propertyId = propertyId;
    billingAccountClosure.id = billingAccountId;
    return billingAccountClosure;
  }
}
