import { LocationDto } from './../models/dto/location-dto';
import { DocumentForLegalAndNaturalPerson } from 'app/shared/models/document';
import { AddressService } from './../services/shared/address.service';
import { PersonHeaderDto } from './../models/dto/person-header-dto';
import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class PersonHeaderMapper {

  readonly guidDefault = '00000000-0000-0000-0000-000000000000';

  constructor(private addressService: AddressService) {}

  public dtoToFormGroup(formGroup: AbstractControl, personHeaderDto: PersonHeaderDto) {
    formGroup.patchValue({
      personId: personHeaderDto.id,
      document: personHeaderDto.documentList[0].documentInformation,
      documentTypeId: personHeaderDto.documentList[0].documentTypeId,
      email: personHeaderDto.email,
      fullName: personHeaderDto.fullName,
      phone: personHeaderDto.phoneNumber
    });
  }

  public formGroupToDto(formGroup: FormGroup, billingAccountId: string): PersonHeaderDto {
    const personHeaderDto = new PersonHeaderDto();

    personHeaderDto.id = formGroup.get('personId').value ? formGroup.get('personId').value : this.guidDefault;
    personHeaderDto.firstName = formGroup.get('fullName').value;
    personHeaderDto.fullName = formGroup.get('fullName').value;
    personHeaderDto.email = formGroup.get('email').value;
    personHeaderDto.phoneNumber = formGroup.get('phone').value;
    personHeaderDto.billingAccountId = billingAccountId;

    const location: LocationDto = formGroup.get('address').value;
    location.ownerId = formGroup.get('personId').value ? formGroup.get('personId').value : this.guidDefault;
    personHeaderDto.locationList = [location];

    const document = new DocumentForLegalAndNaturalPerson();
    document.ownerId = formGroup.get('personId').value ? formGroup.get('personId').value : this.guidDefault;
    document.documentInformation = formGroup.get('document').value;
    document.documentTypeId = formGroup.get('documentTypeId').value;
    document.id = 0;
    document.documentType = null;
    personHeaderDto.documentList = [];
    personHeaderDto.documentList.push(document);

    return personHeaderDto;
  }

  private companyClientIdExist(dataToMapper: FormGroup): boolean {
    return dataToMapper.get('companyClientId').value && !(dataToMapper.get('companyClientId').value == this.guidDefault);
  }
}
