import { FormBuilder } from '@angular/forms';
import { Document, Location } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { DocumentType } from 'app/shared/models/document-type';

export const FORM_GROUP = 'FORM_GROUP';
export const DOCUMENT = 'DOCUMENT';
export const LOCATION = 'LOCATION';

export const DOCUMENT_TYPE_LIST: DocumentType[] = [
  {
    id: 1,
    countryCode: 'pt-bt',
    isMandatory: true,
    name: 'CPF',
    personType: 'L',
    regexValidationExpression: '',
    stringFormatMask: '',
    countrySubdivisionId: 1,
  }
];

export class DATA {

  public static readonly FORM_GROUP = new FormBuilder()
    .group({
      basicData: new FormBuilder().group({
        age: 40,
        birthDate: '1978-04-21T00:00:00',
        cellPhone: undefined,
        document: '320.221.010-65',
        documentTypeId: 1,
        email: 'paulo10.1977@yahoo.com.br',
        fullName: 'Paulo Sérgio',
        gender: 'M',
        guestId: 3523,
        guestReservationItemId: 4110,
        guestType: 0,
        id: '00000000-0000-0000-0000-000000000123',
        ownerId: '00000000-0000-0000-0000-000000000123',
        incapable: false,
        nationality: 1,
        personId: 1,
        phone: '21 996397564',
        responsable: '1',
        responsableName: 'Paulo 123',
      }),
      address: new FormBuilder().group({
        additionalAddressDetails: 'Praca da Bandeira, Rio de Janeiro - RJ, 20270-132, Brasil',
        browserLanguage: 'pt-br',
        completeAddress: 'Praca da Bandeira, Rio de Janeiro - RJ, 20270-132, Brasil',
        country: 'Brasil',
        countryCode: 'BR',
        countrySubdivisionId: null,
        division: 'RJ',
        id: 5519,
        latitude: -22.91,
        locationCategoryId: 4,
        longitude: -43.21,
        neighborhood: 'Praca da Bandeira',
        ownerId: '4dd0c48a-60b9-43df-0583-08d6b85f8d7f',
        postalCode: '20270-132',
        streetName: 'Rio de Janeiro',
        streetNumber: '125',
        subdivision: 'Rio de Janeiro',
      }),
      complementaryInformation: new FormBuilder().group({
        lastAddress: 'Rio',
        medicalAgreement: 'Bradesco',
        nextAddress: 'São',
        occupation: '1',
        reasonForTravel: '7',
        socialName: 'Paulo Sérgio',
        vehicle: '1',
      }),
      additionalInformation: new FormBuilder().group({
        remarks: 'Blablablabla'
      }),
      person: new FormBuilder().group({
        receiveOffers: true,
        sharePersonData: true,
        allowContact: true,
        lastName: 'Sacramento',
        firstName: 'Paulo',
        countrySubdivisionId: 123,
        personType: 1,
        id: 123456,
      }),
    });

  public static readonly DOCUMENT = <Document>{
    documentTypeId: 1,
    documentInformation: null,
    documentTypeName: 'Blablabla2',
    id: 1234567,
    ownerId: '1234',
  };

  public static readonly LOCATION = <Location>{
    browserLanguage: 'pt-br',
    latitude: 12,
    longitude: 13,
    streetName: '',
    streetNumber: '125',
    additionalAddressDetails: 'Praca da Bandeira, Rio de Janeiro - RJ, 20270-132, Brasil',
    postalCode: 20270132,
    countryCode: '5689',
    locationCategoryId: 123,
    neighborhood: 'Rio de Janeiro',
    completeAddress: 'Praca da Bandeira, Rio de Janeiro - RJ, 20270-132, Brasil',
    subdivision: 'RJ',
    division: 'Rio de Janeiro',
    country: 'RJ',
    id: 123,
    city: 'RIO',
  };
}
