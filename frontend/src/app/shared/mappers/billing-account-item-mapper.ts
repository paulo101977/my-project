import { FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { BillingAccountItem } from './../models/billing-account/billing-account-item';
import { PaymentTypeWithBillingItem } from './../models/billingType/payment-type-with-billing-item';
import { BillingAccountItemReverse } from './../models/billing-account/billing-account-item-reverse';
import { BillingAccountWithEntry } from '../models/billing-account/billing-account-with-entry';
import { SharedService } from './../services/shared/shared.service';

@Injectable({ providedIn: 'root' })
export class BillingAccountItemMapper {
  public readonly guidDefault = '00000000-0000-0000-0000-000000000000';

  constructor(private sharedService: SharedService) {}

  public toBillintAccountItemForPostDebit(formGroup: FormGroup, propertyId: number, billingAccountId: string): BillingAccountItem {
    const billingAccountItem = new BillingAccountItem();
    billingAccountItem.id = this.guidDefault;
    billingAccountItem.billingAccountId = billingAccountId;
    billingAccountItem.propertyId = propertyId;
    billingAccountItem.amount = formGroup.get('postDebit.priceWithoutTax').value;
    billingAccountItem.billingItemId = formGroup.get('postDebit.billingItemServiceId').value;

    return billingAccountItem;
  }

  public toBillintAccountItemForPostCredit(
    formGroup: FormGroup,
    propertyId: number,
    billingAccountId: string,
    paymentTypeWithBillingItemList: Array<PaymentTypeWithBillingItem>,
  ): BillingAccountItem {
    const billingItemId = this.getBillingItemId(formGroup.get('postCredit.paymentTypeId').value, paymentTypeWithBillingItemList);
    const billingAccountItem = new BillingAccountItem();
    billingAccountItem.id = this.guidDefault;
    billingAccountItem.billingAccountId = billingAccountId;
    billingAccountItem.propertyId = propertyId;
    if (formGroup.get('postCredit.card.plasticBrandPropertyId').value) {
      billingAccountItem.billingItemId = formGroup.get('postCredit.card.plasticBrandPropertyId').value;
    } else {
      billingAccountItem.billingItemId = billingItemId;
    }
    billingAccountItem.nsu = formGroup.get('postCredit.card.nsuCode').value;
    billingAccountItem.amount = formGroup.get('postCredit.price').value;
    billingAccountItem.checkNumber = formGroup.get('postCredit.bankCheck.number').value;

    return billingAccountItem;
  }

  public toBillintAccountItemForPayment(
    formGroup: FormGroup,
    propertyId: number,
    billingAccountId: string,
    paymentTypeWithBillingItemList: Array<PaymentTypeWithBillingItem>,
  ): BillingAccountItem {
    const billingItemId = this.getBillingItemId(formGroup.get('paymentTypeId').value, paymentTypeWithBillingItemList);
    const billingAccountItem = new BillingAccountItem();
    billingAccountItem.id = this.guidDefault;
    billingAccountItem.billingAccountId = this.guidDefault;
    billingAccountItem.propertyId = propertyId;
    if (formGroup.get('card.plasticBrandPropertyId').value) {
      billingAccountItem.billingItemId = formGroup.get('card.plasticBrandPropertyId').value;
      billingAccountItem.installmentsQuantity = formGroup.get('card.installmentsNumber').value;
    } else {
      billingAccountItem.billingItemId = billingItemId;
    }
    billingAccountItem.nsu = formGroup.get('card.nsuCode').value;
    billingAccountItem.amount = formGroup.get('price').value;
    billingAccountItem.checkNumber = formGroup.get('bankCheck.number').value;

    return billingAccountItem;
  }

  private getBillingItemId(paymentTypeId: number, paymentTypeWithBillingItemList: Array<PaymentTypeWithBillingItem>): number {
    const callbackFuncion = p => p.id == paymentTypeId;
    const paymentTypeWithBillingItem = this.sharedService.getElementInList(null, paymentTypeWithBillingItemList, callbackFuncion);
    return paymentTypeWithBillingItem.billingItemId;
  }

  public toBillintAccountItemReverse(
    formGroup: FormGroup,
    propertyId: number,
    billingAccountId: string,
    billingAccountWithEntryList: Array<BillingAccountWithEntry>,
  ): Array<BillingAccountItemReverse> {
    const billingAccountItemReverseList = new Array<BillingAccountItemReverse>();
    billingAccountWithEntryList.forEach(billingAccountItemEntry => {
      billingAccountItemEntry.billingAccountItems.forEach(billingAccountItem => {
        const billingAccountItemReverse = new BillingAccountItemReverse();
        billingAccountItemReverse.billingAccountId = billingAccountId;
        billingAccountItemReverse.propertyId = propertyId;
        billingAccountItemReverse.originalBillingAccountItemId = billingAccountItem.billingAccountItemId;
        billingAccountItemReverse.reasonId = formGroup.get('reason.reasonId').value;
        billingAccountItemReverse.observation = formGroup.get('reason.observation').value;
        billingAccountItemReverseList.push(billingAccountItemReverse);
      });
    });

    return billingAccountItemReverseList;
  }
}
