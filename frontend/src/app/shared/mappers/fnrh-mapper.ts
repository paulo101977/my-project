import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Document, FnrhSaveDto, Location } from 'app/shared/models/dto/checkin/fnrh-save-dto';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { DocumentType } from 'app/shared/models/document-type';

@Injectable({ providedIn: 'root' })
export class FnrhMapper {
  private readonly DOCUMENT_TYPE_CPF = 1;
  private readonly DOCUMENT_TYPE_RG = 16;

  public toSave(formGroup: FormGroup, documentList?: any[], documentTypeList?: DocumentType[]): FnrhSaveDto {
    const {location, ...dto} = new FnrhSaveDto();

    return {
      ...dto,
      ...this.getBasicData(formGroup),
      ...this.getComplementaryInformationData(formGroup),
      location: this.getLocationData(location, formGroup),
      document: this.getDocumentData(formGroup, documentTypeList),
      ...this.getPerson(formGroup),
      guestRegistrationDocumentList: this.transformDocumentListData(documentList),
    };
  }

  private transformDocumentListData(documentList: any[]) {
    if (documentList) {
      return documentList
        .map(doc => {
          return {
            documentInformation: doc.documentInformation,
            documentTypeId: +doc.documentTypeId,
            id: doc.id || GuidDefaultData
          };
        });
    }
    return [];
  }

  private getBasicData(formGroup: FormGroup) {
    const basicData = (<FormGroup> formGroup.get('basicData')).getRawValue();

    return {
      ...(basicData.birthDate ? {birthDate: basicData.birthDate} : {}),
      ...(basicData.birthDate ? {age: basicData.age} : {}),
      id: basicData.id == null ? GuidDefaultData : basicData.id,
      personId: basicData.personId ? basicData.personId : GuidDefaultData,
      guestTypeId: basicData.guestType == null ? 0 : basicData.guestType,
      nationality: basicData.nationality == 0 ? null : basicData.nationality,
      isLegallyIncompetent: basicData.incapable ? basicData.incapable : false,
      responsibleGuestRegistrationId: basicData.responsable ? basicData.responsable : null,
      guestReservationItemId: basicData.guestReservationItemId,
      firstName: basicData.fullName,
      lastName: basicData.fullName,
      email: basicData.email,
      fullName: basicData.fullName,
      phoneNumber: basicData.phone,
      mobilePhoneNumber: basicData.cellPhone,
      gender: basicData.gender,
      responsableName: basicData.responsableName,
      // This is needed at the guest FNRH entrance
      isChild: basicData.age < 18
    };
  }

  private getComplementaryInformationData(formGroup: FormGroup) {
    const complementaryInformationForm = <FormGroup> formGroup.get('complementaryInformation');
    const additionalInformationForm = <FormGroup> formGroup.get('additionalInformation');
    const {
      occupation, vehicle, nextAddress, lastAddress, reasonForTravel, medicalAgreement, socialName
    } = complementaryInformationForm.value;
    const { remarks } = additionalInformationForm.value;

    return {
      occupationId: occupation == 0 ? null : occupation,
      arrivingBy: vehicle == 0 ? null : vehicle,
      nextDestinationText: nextAddress,
      arrivingFromText: lastAddress,
      purposeOfTrip: reasonForTravel == 0 ? null : reasonForTravel,
      healthInsurance: medicalAgreement,
      socialName: socialName,
      additionalInformation: remarks
    };
  }

  private getLocationData(location: Location, formGroup: FormGroup): Location {
    const basicDataForm = <FormGroup> formGroup.get('basicData');
    const addressForm = <FormGroup> formGroup.get('address');
    const { documentTypeId } = basicDataForm.getRawValue();
    const address = addressForm.value;

    if (
      !address.streetName &&
      !address.subdivision &&
      ![this.DOCUMENT_TYPE_CPF, this.DOCUMENT_TYPE_RG].includes(documentTypeId)
    ) {
      return null;
    }

    return  {
      ...location,
      id: +address.id,
      postalCode: address.postalCode ? address.postalCode.toString().replace('-', '') : '',
      streetName: address.streetName,
      streetNumber: address.streetNumber,
      additionalAddressDetails: address.additionalAddressDetails,
      division: address.division,
      subdivision: address.subdivision,
      neighborhood: address.neighborhood,
      locationCategoryId: address.locationCategoryId,
      browserLanguage: window.navigator.language,
      latitude: address.latitude,
      longitude: address.longitude,
      countryCode: address.countryCode,
      country: address.country,
      completeAddress: address.completeAddress,
    };
  }

  private getDocumentData(formGroup: FormGroup, documentTypeList: DocumentType[]): Document {
    const basicData = (<FormGroup>formGroup.get('basicData')).getRawValue();
    const { ...document } = new Document();
    let documentType = null;

    if (documentTypeList) {
      documentType = documentTypeList.find(item => item.id == (+basicData.documentTypeId));
    }

    return {
      ...document,
      documentInformation: basicData.document,
      documentTypeId: +basicData.documentTypeId,
      documentTypeName: documentType ? documentType.name : '',
      ownerId: basicData.ownerId,
    };
  }

  public getPerson(formGroup: FormGroup) {
    const fullName = formGroup.get('basicData.fullName').value;
    const { id, ...person } = formGroup.get('person').value;

    return {
      personId: id,
      person: {...person, fullName},
    };
  }
}
