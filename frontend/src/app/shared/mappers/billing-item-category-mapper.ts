import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class BillingItemCategoryMapper {
  public getBillingItemCategoryByFormGroupToSendServer(formGroup: AbstractControl): any {
    return {
      standardCategoryId: formGroup.get('standardCategoryId').value,
      categoryName: formGroup.get('categoryName').value,
      isActive: formGroup.get('isActive').value,
    };
  }

  public getFormGroupByBillingItemCategoryToSendServer(formGroup: FormGroup, billingItemCategory: any): FormGroup {
    formGroup.get('standardCategoryId').setValue(billingItemCategory.standardCategoryId);
    formGroup.get('categoryName').setValue(billingItemCategory.categoryName);
    formGroup.get('isActive').setValue(billingItemCategory.isActive);

    return formGroup;
  }
}
