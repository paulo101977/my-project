import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GuestAccount } from './../models/guest-account';
import { GuestAccountDto } from './../models/dto/billing-account/guest-account-dto';
import { CompanyClientAccountDto } from './../models/dto/billing-account/company-client-account-dto';
import { CompanyClientAccount } from './../models/dto/company-client-account-dto';
import { SingleAccountDto } from './../models/dto/billing-account/single-account-dto';
import { DocumentForLegalAndNaturalPerson } from 'app/shared/models/document';
import { Location } from './../models/location';
import { BillingAccountTypeEnum } from './../models/billing-account/billing-account-type-enum';
import { BillingAccountWithAccount } from 'app/shared/models/billing-account/billing-account-with-account';
import { BillingAccountStatement } from './../models/billing-account/billing-account-statement';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';

@Injectable({ providedIn: 'root' })
export class BillingAccountMapper {

  constructor() {}

  public toSaveGuestAccountDto(dataToMapper: GuestAccount, propertyId: number): GuestAccountDto {
    const guestAccountDto = new GuestAccountDto();
    guestAccountDto.companyClientId = null;
    guestAccountDto.reservationId = dataToMapper.reservationId;
    guestAccountDto.reservationItemId = dataToMapper.reservationItemId;
    guestAccountDto.guestReservationItemId = dataToMapper.guestReservationItemId;
    guestAccountDto.billingAccountTypeId = 2;
    guestAccountDto.startDate = new Date().toStringUniversal();
    guestAccountDto.statusId = 1;
    guestAccountDto.id = GuidDefaultData;
    guestAccountDto.propertyId = propertyId;

    return guestAccountDto;
  }

  public toSaveClientAccountDto(dataToMapper: CompanyClientAccount, propertyId: number): CompanyClientAccountDto {
    const companyClinetAccountDto = new CompanyClientAccountDto();
    companyClinetAccountDto.companyClientId = dataToMapper.compannyClientId;
    companyClinetAccountDto.reservationId = dataToMapper.reservationId;
    companyClinetAccountDto.reservationItemCode = dataToMapper.reservationItemCode;
    companyClinetAccountDto.reservationItemId = dataToMapper.reservationItemId;
    companyClinetAccountDto.guestReservationItemId = null;
    companyClinetAccountDto.billingAccountTypeId = 3;
    companyClinetAccountDto.startDate = new Date().toStringUniversal();
    companyClinetAccountDto.statusId = 1;
    companyClinetAccountDto.id = '00000000-0000-0000-0000-000000000000';
    companyClinetAccountDto.propertyId = propertyId;
    return companyClinetAccountDto;
  }

  public toSaveSingleAccountDto(dataToMapper: FormGroup, propertyId: number): SingleAccountDto {
    const singleAccountDto = new SingleAccountDto();

    singleAccountDto.id = GuidDefaultData;
    singleAccountDto.companyClientId = this.companyClientIdExist(dataToMapper) ? dataToMapper.get('companyClientId').value : null;
    singleAccountDto.propertyId = propertyId;
    singleAccountDto.reservationId = null;
    singleAccountDto.reservationItemId = null;
    singleAccountDto.billingAccountTypeId = BillingAccountTypeEnum.Sparse;
    singleAccountDto.startDate = new Date().toStringUniversal();
    singleAccountDto.endDate = null;
    singleAccountDto.statusId = 1;
    singleAccountDto.isMainAccount = true;
    singleAccountDto.marketSegmentId = dataToMapper.get('staticalData.marketSegment').value;
    singleAccountDto.businessSourceId = dataToMapper.get('staticalData.origin').value;
    singleAccountDto.accountBalance = 0;
    singleAccountDto.propertyId = propertyId;

    singleAccountDto.companyClient = new CompanyClientAccountDto();
    singleAccountDto.companyClient.id = singleAccountDto.companyClientId;
    singleAccountDto.companyClient.companyId = 0;
    singleAccountDto.companyClient.personId = dataToMapper.get('personId').value ? dataToMapper.get('personId').value : GuidDefaultData;
    singleAccountDto.companyClient.propertyId = propertyId;
    singleAccountDto.companyClient.personType = dataToMapper.get('personType').value;
    singleAccountDto.companyClient.shortName = dataToMapper.get('clientName').value;
    singleAccountDto.companyClient.tradeName = dataToMapper.get('clientName').value;
    singleAccountDto.companyClient.email = dataToMapper.get('email').value;
    singleAccountDto.companyClient.homePage = null;
    singleAccountDto.companyClient.phoneNumber = dataToMapper.get('phone').value;
    singleAccountDto.companyClient.cellPhoneNumber = null;
    singleAccountDto.companyClient.isActive = true;
    singleAccountDto.companyClient.isIssuingBank = false;

    const location = dataToMapper.get('address').value;
    location.ownerId = dataToMapper.get('personId').value ? dataToMapper.get('personId').value : GuidDefaultData;
    location.postalCode = location.postalCode
      ? location.postalCode.toString().replace('-', '')
      : '';

      singleAccountDto.companyClient.locationList = new Array<Location>();
    singleAccountDto.companyClient.locationList.push(location);

    const document = new DocumentForLegalAndNaturalPerson();
    document.ownerId = dataToMapper.get('personId').value ? dataToMapper.get('personId').value : GuidDefaultData;
    document.documentInformation = dataToMapper.get('document').value;
    document.documentTypeId = dataToMapper.get('documentTypeId').value;
    document.id = 0;
    document.documentType = null;
    singleAccountDto.companyClient.documentList = new Array<DocumentForLegalAndNaturalPerson>();
    singleAccountDto.companyClient.documentList.push(document);

    return singleAccountDto;
  }

  private companyClientIdExist(dataToMapper: FormGroup): boolean {
    return dataToMapper.get('companyClientId').value && !(dataToMapper.get('companyClientId').value == GuidDefaultData);
  }

}
