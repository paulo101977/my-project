import { Injectable } from '@angular/core';
import { BillingAccountItemTransfer } from './../models/billing-account/billing-account-item-transfer';
import { BillingAccountItemTransferChild } from './../models/billing-account/billing-account-item-transfer-child';
import { BillingAccountWithEntry } from '../models/billing-account/billing-account-with-entry';

@Injectable({ providedIn: 'root' })
export class BillingAccountItemTransferMapper {
  readonly guidDefault = '00000000-0000-0000-0000-000000000000';

  public toBillingAccountItemTransfer(
    billingAccountItemEntryList: Array<BillingAccountWithEntry>,
    billingAccountIdDestination: string,
  ): BillingAccountItemTransfer {
    const billingAccountItemTransfer = new BillingAccountItemTransfer();
    billingAccountItemTransfer.id = this.guidDefault;
    billingAccountItemTransfer.billingAccountItemIdDestination = billingAccountIdDestination;
    billingAccountItemEntryList.forEach(billingAccountItemEntry => {
      billingAccountItemEntry.billingAccountItems.forEach(billingAccountItem => {
        const billingAccountItemTransferChild = new BillingAccountItemTransferChild();
        billingAccountItemTransferChild.billingAccountItemId = billingAccountItem.billingAccountItemId;
        billingAccountItemTransfer.billingAccountItemTransferList.push(billingAccountItemTransferChild);
      });
    });
    return billingAccountItemTransfer;
  }

  public toBillingAccountItemUndoTransfer(billingAccountItemEntryList: Array<BillingAccountWithEntry>): BillingAccountItemTransfer {
    const billingAccountItemTransfer = new BillingAccountItemTransfer();
    billingAccountItemTransfer.id = this.guidDefault;
    billingAccountItemEntryList.forEach(billingAccountItemEntry => {
      billingAccountItemEntry.billingAccountItems.forEach(billingAccountItem => {
        const billingAccountItemTransferChild = new BillingAccountItemTransferChild();
        billingAccountItemTransferChild.billingAccountItemId = billingAccountItem.billingAccountItemId;
        billingAccountItemTransfer.billingAccountItemTransferList.push(billingAccountItemTransferChild);
      });
    });
    return billingAccountItemTransfer;
  }
}
