import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BillingItemTaxServiceAssociate } from 'app/shared/models/billingType/billing-item-tax-service-associate';
import { BillingItemTax } from 'app/shared/models/billingType/billing-item-tax';
import { BillingItemService } from './../models/billingType/billing-item-service';
import { BillingItemServiceTaxAssociate } from './../models/billingType/billing-item-service-tax-associate';
import { GetAllBillingItemTax } from './../models/billingType/get-all-billing-item-tax';

@Injectable({ providedIn: 'root' })
export class BillingItemTypeMapper {
  public setTaxToService(formGroup: FormGroup, billingItemTaxModel: GetAllBillingItemTax): BillingItemServiceTaxAssociate {
    const billingItemTax = new BillingItemServiceTaxAssociate();
    billingItemTax.billingItemTaxId = billingItemTaxModel.id;
    billingItemTax.id = formGroup.get('id').value;
    billingItemTax.name = billingItemTaxModel.billingItemName;
    billingItemTax.categoryName = billingItemTaxModel.billingItemCategoryName;
    billingItemTax.isActive = formGroup.get('isActive').value;
    billingItemTax.taxPercentage = formGroup.get('taxPercentage').value;
    billingItemTax.beginDate = formGroup.get('beginDate').value;
    billingItemTax.endDate = formGroup.get('endDate').value;
    return billingItemTax;
  }

  public toBillingItemTax(formGroupData: FormGroup, billingItemTaxList: Array<BillingItemTaxServiceAssociate>): BillingItemTax {
    const billingItemTax = new BillingItemTax();
    billingItemTax.billingItemCategoryId = formGroupData.get('group').value;
    billingItemTax.isActive = formGroupData.get('isActive').value ? true : false;
    billingItemTax.name = formGroupData.get('description').value;
    billingItemTax.serviceAndTaxList = billingItemTaxList;
    billingItemTax.id = formGroupData.get('id').value ? formGroupData.get('id').value : 0;
    billingItemTax.integrationCode = formGroupData.get('integrationCode').value;

    return billingItemTax;
  }

  public toSaveServiceWithTaxAssociated(
    formGroup: FormGroup,
    billingItemTaxModel: Array<BillingItemServiceTaxAssociate>,
  ): BillingItemService {
    if (billingItemTaxModel && billingItemTaxModel.length > 1) {
      billingItemTaxModel.forEach(tax => {
        tax.billingItemServiceId = formGroup.get('id').value;
      });
    }

    const billingItemService = new BillingItemService();
    billingItemService.id = formGroup.get('id').value;
    billingItemService.name = formGroup.get('name').value;
    billingItemService.isActive = formGroup.get('isActive').value ? true : false;
    billingItemService.billingItemCategoryId = formGroup.get('group').value;
    billingItemService.serviceAndTaxList = billingItemTaxModel;
    billingItemService.integrationCode = formGroup.get('integrationCode').value;

    return billingItemService;
  }

  public toSaveTaxWithServiceAssociated(formGroupData: FormGroup, serviceAssociateName: any): BillingItemTaxServiceAssociate {
    const billingItemService = new BillingItemTaxServiceAssociate();
    billingItemService.id = formGroupData.get('id').value;
    billingItemService.billingItemServiceId = formGroupData.get('selectValueService.serviceAssociate').value;
    if (serviceAssociateName) {
      billingItemService.name = serviceAssociateName.billingItemName;
      billingItemService.categoryName = serviceAssociateName.billingItemCategoryName;
    }
    billingItemService.isActive = formGroupData.get('isActive').value ? true : false;
    billingItemService.taxPercentage = formGroupData.get('taxPercentage').value;
    billingItemService.beginDate = formGroupData.get('beginDate').value;
    billingItemService.endDate = formGroupData.get('endDate').value
      ? formGroupData.get('endDate').value
      : null;
    return billingItemService;
  }
}
