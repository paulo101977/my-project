import { TestBed, inject } from '@angular/core/testing';
import { FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { fnrhList } from './fnrh.reducer';

describe('FnrhReducer', () => {
  it('should get state default', () => {
    const state = [];
    const actionAndObject = {
      type: 'Error_FNRH',
      fnrhWithId: {
        id: 1,
        fnrh: {},
      },
    };

    const response = fnrhList(state, actionAndObject);

    expect(response).toEqual([]);
  });

  it('should ADD_FNRH', () => {
    const state = [];
    const actionAndObject = {
      type: 'ADD_FNRH',
      fnrhWithId: {
        id: 1,
        fnrh: {},
      },
    };

    const response = fnrhList(state, actionAndObject);

    expect(response[0].id).toEqual(actionAndObject.fnrhWithId.id);
    expect(response[0].fnrh).toEqual(actionAndObject.fnrhWithId.fnrh);
  });

  it('should UPDATE_FNRH', () => {
    const state = [];
    const newActionAndObject = {
      type: 'ADD_FNRH',
      fnrhWithId: {
        id: 1,
        fnrh: {},
      },
    };
    const updateActionAndObject = {
      type: 'UPDATE_FNRH',
      fnrhWithId: {
        id: 1,
        fnrh: {},
      },
    };

    const newItem = fnrhList(state, newActionAndObject);
    const response = fnrhList(newItem, updateActionAndObject);

    expect(response.length).toEqual(1);
    expect(response[0].id).toEqual(updateActionAndObject.fnrhWithId.id);
    expect(response[0].fnrh).toEqual(updateActionAndObject.fnrhWithId.fnrh);
  });

  it('should DELETE_FNRH', () => {
    const state = [];
    const newActionAndObject = {
      type: 'ADD_FNRH',
      fnrhWithId: {
        id: 1,
        fnrh: {},
      },
    };
    const deleteActionAndObject = {
      type: 'DELETE_FNRH',
      fnrhWithId: {
        id: 1,
        fnrh: {},
      },
    };

    const newItem = fnrhList(state, newActionAndObject);
    const response = fnrhList(newItem, deleteActionAndObject);

    expect(response.length).toEqual(0);
  });
});
