export function fnrhList(state: any = [], { type, fnrhWithId }) {
  switch (type) {
    case 'ADD_FNRH':
      return [...state, fnrhWithId];
    case 'UPDATE_FNRH':
      return state.map(item => {
        return item.id == fnrhWithId.id ? Object.assign({}, item, fnrhWithId) : item;
      });
    case 'DELETE_FNRH':
      return state.filter(item => {
        return item.id !== item.id;
      });
    default:
      return state;
  }
}
