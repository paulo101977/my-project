export function billingAccountWithEntryList(state: any = [], { type, billingAccountWithEntry }) {
  switch (type) {
    case 'ADD':
      return [...state, billingAccountWithEntry];
    case 'DELETE':
      return [];
    default:
      return state;
  }
}
