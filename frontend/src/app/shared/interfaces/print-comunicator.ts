import { ComponentRef } from '@angular/core';

export interface PrintComunicator {
  populateComponent(component: ComponentRef<any>);
}
