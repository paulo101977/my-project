export interface IModelSerialize<T> {
  normalizeRequest(): T;
  normalizeResponse(object: T): void;
}
