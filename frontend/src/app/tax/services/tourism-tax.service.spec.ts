import { inject, TestBed } from '@angular/core/testing';
import { TourismTaxService } from './tourism-tax.service';
import { PropertyService } from 'app/shared/services/property/property.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { AdminApiModule,
  ThexApiModule,
  ApiEnvironment,
  configureApi,
  configureAppName  } from '@inovacaocmnet/thx-bifrost';

describe('TourismTaxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, RouterTestingModule, TranslateTestingModule, AdminApiModule, ThexApiModule ],
      providers: [
        PropertyService,
        TourismTaxService,
        configureApi({
          environment: ApiEnvironment.Development
        }),
        configureAppName('pms')
      ]
    });
  });

  it('should be created', inject([TourismTaxService], (service: TourismTaxService) => {
    expect(service).toBeTruthy();
  }));
});
