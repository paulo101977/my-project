import { Injectable } from '@angular/core';
import { CountryCodeEnum } from '@inovacaocmnet/thx-bifrost';
import { PropertyService } from 'app/shared/services/property/property.service';

@Injectable({
  providedIn: 'root'
})
export class TourismTaxService {

  constructor(
    private propertyService: PropertyService
  ) { }

  public couldShow() {
    const propertyCountryList = [CountryCodeEnum.PT];
    const countryCode = this.propertyService.getPropertyCountryCode();
    return !!propertyCountryList && propertyCountryList.includes(countryCode);
  }
}
