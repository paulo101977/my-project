import { Component, OnInit } from '@angular/core';
import { CheckinAuditEnum } from 'app/tax/models/checkin-audit';
import { BillingItemCategoryResource } from 'app/shared/resources/billing-item-category/billing-item-category.resource';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';
import { TranslateService } from '@ngx-translate/core';
import { TourismTaxResource } from 'app/tax/resources/tourism-tax.resource';
import { PropertyGuestTypeExemptOfTourismTaxDto } from 'app/shared/models/dto/property-guest-type-dto';
import { GuestTypeResource } from 'app/shared/resources/guest-type/guest-type.resource';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';

@Component({
  selector: 'app-tourism-tax',
  templateUrl: './tourism-tax.component.html',
  styleUrls: ['./tourism-tax.component.scss']
})
export class TourismTaxComponent implements OnInit {
  public radioCheckinCheckoutOptions: Array<any>;
  public buttonConfig: ButtonConfig;
  public propertyGuestTypeList: Array<PropertyGuestTypeExemptOfTourismTaxDto>;

  public isActive = true;
  public isFirstTime = true;
  public form: FormGroup;
  public propertyId: number;
  public billingItemCategoryList: Array<any>;
  public labelValueOfTax: string;
  public amountCurrencyMask: any;
  public numberOfNightsMask: any;
  public tourismTaxId: string;
  public tourismTaxTranslation: string;

  constructor(
    private billingItemCategoryResource: BillingItemCategoryResource,
    private sharedService: SharedService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private commonService: CommomService,
    private currencyService: CurrencyService,
    private translateService: TranslateService,
    private tourismTaxResource: TourismTaxResource,
    private guestTypeResource: GuestTypeResource,
    private toasterService: ToasterEmitService,
  ) { }

  ngOnInit() {
    this.setRadioOptions();
    this.setButtonConfig();
    this.setForm();
    this.setMasks();
    this.tourismTaxTranslation = this.translateService.instant('text.tourismTax');

    this.propertyId = this.route.snapshot.params.property;

    this.setLabelTaxOfTurism();

    this.getBillingItemCategoryList();
    this.retrieveTourismTax();

  }

  public retrieveTourismTax() {
    this
      .tourismTaxResource
      .getSavedTourismTax()
      .subscribe(  response => {
        const { propertyGuestTypeList, isActive, ...item} = response;

        this.isFirstTime = false;
        this.propertyGuestTypeList = propertyGuestTypeList;
        this.isActive = isActive;
        this.tourismTaxId = item.id;

        if ( !isActive ) {
          this.form.disable({emitEvent: false });
        }

        this.patchForm(item);
    }, () => {
        this.isFirstTime = true;
        this.retrievePropertyGuestTypeList();
    });
  }

  public patchForm(items) {
    this.form.patchValue(items, {emitEvent: false});
  }

  public retrievePropertyGuestTypeList() {
    this
      .guestTypeResource
      .getAllGuestTypeListByPropertyId(this.propertyId)
      .subscribe( ({items}) => {
        this.propertyGuestTypeList = <Array<PropertyGuestTypeExemptOfTourismTaxDto>>items
          .map( item => ({
            ...item,
            isExemptOfTourismTax: false
          }));
      });
  }

  public setMasks() {
    this.amountCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'center' };
    this.numberOfNightsMask = { prefix: '', thousands: '', decimal: '', align: 'center', precision: 0 };
  }

  public setLabelTaxOfTurism() {
    const symbol = this.currencyService.getDefaultCurrencySymbol(this.propertyId );
    this.labelValueOfTax = this
      .translateService
      .instant('label.valueOfTax', { symbol });
  }

  public setForm() {
    this.form = this.fb.group({
      billingItemId: [null, [Validators.required]],
      amount: [ null , [Validators.required, Validators.min(0.01)] ],
      numberOfNights: [ 0 ],
      isTotalOfNights : [ false ],
      // TODO: comment until next version
      // launchType: [ CheckinAuditEnum.Checkin ]
    });

    this.form.valueChanges.subscribe( () => {
      if ( this.buttonConfig ) {
        this.buttonConfig.isDisabled = this.isInvalidForm() || !this.isActive;
      }
    });

    this
      .form
      .get('isTotalOfNights')
      .valueChanges
      .subscribe( isTotalOfNights => {
        if ( isTotalOfNights) {
          this.disableAndSetNightValue();
        } else {
          this.form.get('numberOfNights').enable({emitEvent: false });
        }
    });
  }

  public disableAndSetNightValue() {
    this.form.get('numberOfNights').disable({emitEvent: false });
    this.form.get('numberOfNights').setValue(0);
  }

  public isValidNight() {
    const { numberOfNights, isTotalOfNights } = this.form.getRawValue();
    const isValidNumberOfNights = !isNaN(numberOfNights) && numberOfNights > 0;

    return isValidNumberOfNights || isTotalOfNights;
  }

  public isInvalidForm(): boolean {
    const { amount } = this.form.getRawValue();
    return !this.form.valid || isNaN(amount) || !this.isValidNight();
  }

  private translate(key: string) {
    return this.translateService.instant(key, {
      labelName: this.tourismTaxTranslation
    });
  }

  public onChangeIsActive(isActive: boolean) {
    this
      .tourismTaxResource
      .toggleTourismTax(this.tourismTaxId)
      .subscribe( () => {
        let message = this.translate('variable.lbActivatedWithSuccessF');

        this.setActiveChange(isActive);

        if ( !isActive ) {
          message = this.translate('variable.lbInactivatedWithSuccessF');
        }

        this.emitSuccessMessage(message);
    });
  }

  public setActiveChange(isActive: boolean) {
    const { buttonConfig } = this;

    this.form.enable({emitEvent: false});

    if ( !isActive ) {
      this.form.disable({emitEvent: false});
    } else {
      const {  isTotalOfNights } = this.form.getRawValue();

      if ( isTotalOfNights ) {
        this.disableAndSetNightValue();
      }
    }

    if ( buttonConfig ) {
      buttonConfig.isDisabled = this.isInvalidForm();

      if ( !isActive && this.isInvalidForm() ) {
        buttonConfig.isDisabled = true;
      }
    }
  }

  public getBillingItemCategoryList() {
    this
      .billingItemCategoryResource
      .getCategoryListWithService(this.propertyId)
      .subscribe(({items}) => {
        this.billingItemCategoryList =
          this.commonService.toOption(items, 'id', 'billingItemName');
      });
  }

  public setButtonConfig() {
    this.buttonConfig = this.sharedService.getButtonConfig(
      'confirm-button',
      this.confirm,
      'action.confirm',
      ButtonType.Primary,
      null,
      true
    );
  }

  public emitSuccessMessage(message: string) {
    this
      .toasterService
      .emitChange(SuccessError.success, message);
  }

  public confirm = () => {
    const { propertyGuestTypeList, tourismTaxId } = this;
    let message = this.translate('variable.lbSaveSuccessF');

    const itemToSend = this.form.getRawValue();
    itemToSend.propertyGuestTypeList = propertyGuestTypeList;

    if ( this.isFirstTime ) {
      this
        .tourismTaxResource
        .saveTourismTax(itemToSend)
        .subscribe( ({id}) => {
          this.tourismTaxId = id;
          this.isFirstTime = false;
          this.emitSuccessMessage(message);
      });

    } else {
      message = this.translate('variable.lbEditSuccessF');
      itemToSend.id = tourismTaxId;
      this
        .tourismTaxResource
        .updateTourismTax(itemToSend)
        .subscribe( () => {
          this.emitSuccessMessage(message);
      });
    }
  }

  public setRadioOptions() {
    this.radioCheckinCheckoutOptions = [
      {
        id: 'checkin',
        label: 'label.checkinCaps',
        value: CheckinAuditEnum.Checkin
      },
      {
        id: 'audit',
        label: 'label.audit',
        value: CheckinAuditEnum.Audit
      },
    ];
  }
}
