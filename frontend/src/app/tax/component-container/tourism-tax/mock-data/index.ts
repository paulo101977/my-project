import { TourismTax } from 'app/tax/models/tourism-tax';
import { CheckinAuditEnum } from 'app/tax/models/checkin-audit';
import { PropertyGuestTypeDto, PropertyGuestTypeExemptOfTourismTaxDto } from 'app/shared/models/dto/property-guest-type-dto';


export class DATA {
  public static readonly TOURISM_TAX = <TourismTax>{
    isActive: true,
    amount: 100,
    id: '123',
    billingItemId: 6,
    isIntegratedFiscalDocument: false,
    isTotalOfNights: true,
    // TODO: comment until next version
    // launchType: CheckinAuditEnum.Audit,
    numberOfNights: 2,
    propertyGuestTypeList: <Array<PropertyGuestTypeExemptOfTourismTaxDto>>[
      {
        isExemptOfTourismTax: true,
        code: '123',
        guestTypeName: '234',
        id: 1,
        isActive: true,
        isIncognito: true,
        isVip: true,
        propertyId: 1
      },
      {
        isExemptOfTourismTax: false,
        code: '124',
        guestTypeName: '2345',
        id: 2,
        isActive: false,
        isIncognito: false,
        isVip: false,
        propertyId: 1
      }
    ]
  };

  public static readonly TOURISM_TAX_FROM_GUEST =
    <Array<PropertyGuestTypeDto>>DATA.TOURISM_TAX.propertyGuestTypeList.map(
      item => {
        const { isExemptOfTourismTax, ...el } = item;
        return el;
      });

  public static readonly FORM = {
    billingItemId: 1,
    amount: 100,
    numberOfNights: 1,
    isTotalOfNights : true,
    // TODO: comment until next version
    // launchType: CheckinAuditEnum.Checkin
  };
}
