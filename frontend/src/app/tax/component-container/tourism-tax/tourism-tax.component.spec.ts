import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { TourismTaxComponent } from './tourism-tax.component';

import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  ActivatedRouteStub,
  commomServiceStub, createAccessorComponent,
  createServiceStub,
  currencyServiceStub,
  sharedServiceStub,
} from '../../../../../testing';
import { BillingItemCategoryResource } from 'app/shared/resources/billing-item-category/billing-item-category.resource';
import { ActivatedRoute } from '@angular/router';
import { of, throwError } from 'rxjs';
import { TourismTaxResource } from 'app/tax/resources/tourism-tax.resource';
import { GuestTypeResource } from 'app/shared/resources/guest-type/guest-type.resource';
import { DATA } from 'app/tax/component-container/tourism-tax/mock-data';
import { tourismTaxResourceStub } from 'app/tax/testing/tax-tourism';

const SAVED_TOURISM_RESPONSE = DATA.TOURISM_TAX;

const RadioGroupComponent = createAccessorComponent('app-radio-group');


const billingItemCategoryResourceStub = createServiceStub(BillingItemCategoryResource, {
  getCategoryListWithService: (propertyId: number) => of( null )
});


const guestTypeResourceStub = createServiceStub(GuestTypeResource, {
  getAllGuestTypeListByPropertyId: (propertyId: number) => of(
    {hasNext: true, items: DATA.TOURISM_TAX_FROM_GUEST }
  ),
});

describe('TourismTaxComponent', () => {
  const propertyId = 1;
  let component: TourismTaxComponent;
  let fixture: ComponentFixture<TourismTaxComponent>;


  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        TourismTaxComponent,
        RadioGroupComponent,
      ],
      imports: [
        TranslateTestingModule,
        HttpClientModule,
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
      ],
      providers: [
        billingItemCategoryResourceStub,
        commomServiceStub,
        sharedServiceStub,
        currencyServiceStub,
        tourismTaxResourceStub,
        guestTypeResourceStub,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(TourismTaxComponent);
    component = fixture.componentInstance;
    component['route'].params = of({ property: propertyId });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', fakeAsync(() => {
    spyOn(component, 'setLabelTaxOfTurism');
    spyOn(component, 'getBillingItemCategoryList');
    spyOn(component, 'retrieveTourismTax');
    spyOn(component, 'setRadioOptions');
    spyOn(component, 'setButtonConfig');
    spyOn(component, 'setForm');
    spyOn(component, 'setMasks');

    component['route'].snapshot.params.property = propertyId;

    component.ngOnInit();

    tick();

    expect(component.setRadioOptions).toHaveBeenCalled();
    expect(component.setButtonConfig).toHaveBeenCalled();
    expect(component.setForm).toHaveBeenCalled();
    expect(component.setMasks).toHaveBeenCalled();
    expect(component.propertyId).toEqual(propertyId);
    expect(component.setLabelTaxOfTurism).toHaveBeenCalled();
    expect(component.getBillingItemCategoryList).toHaveBeenCalled();
    expect(component.retrieveTourismTax).toHaveBeenCalled();
  }));

  describe('on retrieveTourismTax', () => {
    const { propertyGuestTypeList, isActive, ...items} = SAVED_TOURISM_RESPONSE;

    beforeEach(() => {
      component.isActive = false;
      component.propertyGuestTypeList = null;
      spyOn(component, 'patchForm');
    });

    it('should retrieve response not null', fakeAsync(() => {
      spyOn(
        component['tourismTaxResource'], 'getSavedTourismTax'
      ).and.returnValue(of(SAVED_TOURISM_RESPONSE));

      component.retrieveTourismTax();

      tick();

      expect(component.isFirstTime).toEqual(false);
      expect(component.propertyGuestTypeList).toEqual(propertyGuestTypeList);
      expect(component.isActive).toEqual(isActive);
      expect(component.patchForm).toHaveBeenCalledWith(items);
    }));

    it('should retrieve response empty', fakeAsync(() => {
      spyOn(component['tourismTaxResource'], 'getSavedTourismTax')
        .and.returnValue(throwError(new Error('')));
      spyOn(component, 'retrievePropertyGuestTypeList');

      component.retrieveTourismTax();

      tick();

      expect(component.isFirstTime).toEqual(true);
      expect(component.propertyGuestTypeList).toBeFalsy();
      expect(component.isActive).not.toEqual(isActive);
      expect(component.patchForm).not.toHaveBeenCalled();
      expect(component.retrievePropertyGuestTypeList).toHaveBeenCalled();
    }));

    it('should test retrievePropertyGuestTypeList', fakeAsync(() => {
      const result = DATA.TOURISM_TAX_FROM_GUEST.map(item => ({
        ...item, isExemptOfTourismTax: false
      }));
      component.retrievePropertyGuestTypeList();

      tick();

      expect(component.propertyGuestTypeList).toEqual(result);
    }));
  });

  describe( 'on setForm', () => {
    it('disable button isInvalidForm return true and isActive', fakeAsync(() => {
      spyOn(component, 'isInvalidForm').and.returnValue( true );

      component.isActive = true;
      component.setForm();

      component['form'].patchValue({});

      tick();

      expect(component.buttonConfig.isDisabled).toEqual(true);
    }));

    it('disable button isInvalidForm return true and not isActive', fakeAsync(() => {
      spyOn(component, 'isInvalidForm').and.returnValue( true );

      component.isActive = false;
      component.setForm();

      component['form'].patchValue({});

      tick();

      expect(component.buttonConfig.isDisabled).toEqual(true);
    }));

    it('disable button isInvalidForm return false and isActive', fakeAsync(() => {
      spyOn(component, 'isInvalidForm').and.returnValue( false );

      component.isActive = true;
      component.setForm();

      component['form'].patchValue({});

      tick();

      expect(component.buttonConfig.isDisabled).toEqual(false);
    }));

    it('disable button isInvalidForm return false and not isActive', fakeAsync(() => {
      spyOn(component, 'isInvalidForm').and.returnValue( false );

      component.isActive = false;
      component.setForm();

      component['form'].patchValue({});

      tick();

      expect(component.buttonConfig.isDisabled).toEqual(true);
    }));

    it('test isTotalOfNights set true', fakeAsync(() => {
      spyOn(component, 'disableAndSetNightValue');
      component.setForm();

      component['form'].patchValue({isTotalOfNights: true});

      tick();

      expect(component.disableAndSetNightValue).toHaveBeenCalled();
    }));

    it('test isTotalOfNights not set true', fakeAsync(() => {
      spyOn(component, 'disableAndSetNightValue');

      component.form.get('numberOfNights').disable({emitEvent: false });
      component.setForm();

      component['form'].patchValue({isTotalOfNights: false});

      tick();

      expect(component.disableAndSetNightValue).not.toHaveBeenCalled();
      expect(component.form.get('numberOfNights').enabled).toEqual(true);
    }));
  });

  it('test disableAndSetNightValue', fakeAsync(() => {
    component.disableAndSetNightValue();

    tick();

    expect(component.form.get('numberOfNights').disabled).toEqual(true);
    expect(component.form.get('numberOfNights').value).toEqual(0);
  }));

  describe('on validation', () => {
    it('test isValidNight when NaN numberOfNights and isTotalOfNights', () => {
      component['form'].patchValue({
        numberOfNights: null,
        isTotalOfNights: true
      });

      expect(component.isValidNight()).toEqual(true);
    });

    it('test isValidNight when 0 numberOfNights and isTotalOfNights', () => {
      component['form'].patchValue({
        numberOfNights: 0,
        isTotalOfNights: true
      });

      expect(component.isValidNight()).toEqual(true);
    });

    it('test isValidNight when 0 numberOfNights and not isTotalOfNights', () => {
      component['form'].patchValue({
        numberOfNights: 0,
        isTotalOfNights: false
      });

      expect(component.isValidNight()).toEqual(false);
    });

    it('test isValidNight when NaN numberOfNights and not isTotalOfNights', () => {
      component['form'].patchValue({
        numberOfNights: null,
        isTotalOfNights: false
      });

      expect(component.isValidNight()).toEqual(false);
    });

    it('test isValidNight when numberOfNights and not isTotalOfNights', () => {
      component['form'].patchValue({
        numberOfNights: 1,
        isTotalOfNights: false
      });

      expect(component.isValidNight()).toEqual(true);
    });

    it('test isValidNight when numberOfNights and isTotalOfNights', () => {
      component['form'].patchValue({
        numberOfNights: 1,
        isTotalOfNights: true
      });

      expect(component.isValidNight()).toEqual(true);
    });

    it('test isInvalidForm when invalid form', () => {
      component['form'].patchValue({});

      expect(component.isInvalidForm()).toEqual(true);
    });

    it('test isInvalidForm when valid form and amount is NaN', () => {
      component['form'].patchValue( DATA.FORM );
      component['form'].get('amount').setValue(null);

      expect(component.isInvalidForm()).toEqual(true);
    });

    it('test isInvalidForm when valid form and amount not is NaN', () => {
      component['form'].patchValue( DATA.FORM );

      expect(component.isInvalidForm()).toEqual(false);
    });

    it('test isInvalidForm when valid form and amount not is NaN and isValidNight return true', () => {
      spyOn(component, 'isValidNight').and.returnValue(true);
      component['form'].patchValue( DATA.FORM );

      expect(component.isInvalidForm()).toEqual(false);
    });

    it('test isInvalidForm when valid form and amount not is NaN and isValidNight return false', () => {
      spyOn(component, 'isValidNight').and.returnValue(false);
      component['form'].patchValue( DATA.FORM );

      expect(component.isInvalidForm()).toEqual(true);
    });

    it('test setActiveChange when isActive is true', () => {
      component.setActiveChange(true);
      expect(component['form'].enabled).toEqual(true);
    });

    it('test setActiveChange when isActive is false', () => {
      component.setActiveChange(false);
      expect(component['form'].disabled).toEqual(true);
    });

    it('test setActiveChange when isActive is true and isTotalOfNights', () => {
      spyOn(component, 'disableAndSetNightValue');
      component['form'].patchValue({isTotalOfNights: true });
      component.setActiveChange(true);

      expect(component.disableAndSetNightValue).toHaveBeenCalled();
    });

    it('test setActiveChange when isActive is true and not isTotalOfNights', () => {
      spyOn(component, 'disableAndSetNightValue');
      component['form'].patchValue({isTotalOfNights: false });
      component.setActiveChange(true);

      expect(component.disableAndSetNightValue).not.toHaveBeenCalled();
    });

    it('test setActiveChange when isInvalidForm() is true and not isActive', () => {
      spyOn(component, 'isInvalidForm').and.returnValue(true);

      component.setActiveChange(false);

      expect(component.buttonConfig.isDisabled).toEqual(true);
    });

    it('test setActiveChange when isInvalidForm() is true and isActive', () => {
      spyOn(component, 'isInvalidForm').and.returnValue(true);

      component.setActiveChange(true);

      expect(component.buttonConfig.isDisabled).toEqual(true);
    });

    it('test setActiveChange when isInvalidForm() is false and not isActive', () => {
      spyOn(component, 'isInvalidForm').and.returnValue(false);

      component.setActiveChange(false);

      expect(component.buttonConfig.isDisabled).toEqual(false);
    });

    it('test setActiveChange when isInvalidForm() is false and isActive', () => {
      spyOn(component, 'isInvalidForm').and.returnValue(false);

      component.setActiveChange(true);

      expect(component.buttonConfig.isDisabled).toEqual(false);
    });
  });

  describe('on onChangeIsActive', () => {
    beforeEach(() => {
      spyOn(component, 'emitSuccessMessage');
      spyOn(component, 'setActiveChange');
      spyOn<any>(component, 'translate').and.callFake(f => f );
    });
    it( 'should test when isActive', fakeAsync(() => {
      component.onChangeIsActive(true);

      tick();

      expect(component.setActiveChange).toHaveBeenCalledWith(true);
      expect(component.emitSuccessMessage).toHaveBeenCalledWith('variable.lbActivatedWithSuccessF');
    }));

    it( 'should test when not isActive', fakeAsync(() => {
      component.onChangeIsActive(false);

      tick();

      expect(component.setActiveChange).toHaveBeenCalledWith(false);
      expect(component.emitSuccessMessage).toHaveBeenCalledWith('variable.lbInactivatedWithSuccessF');
    }));
  });

  describe('on confirm', () => {
    let propertyGuestTypeList;
    let itemToSend;
    beforeEach(() => {
      propertyGuestTypeList = DATA.TOURISM_TAX_FROM_GUEST.map(item => ({
        ...item, isExemptOfTourismTax: true
      }));
      spyOn(component, 'emitSuccessMessage');
      spyOn<any>(component, 'translate').and.callFake(f => f );
      component['form'].patchValue(DATA.FORM, {emitEvent: false});

      itemToSend = {
        ...DATA.FORM,
        propertyGuestTypeList
      };
      delete itemToSend.id;
    });
    it( 'should test on save', fakeAsync(() => {
      const id = '124';
      spyOn<any>(component['tourismTaxResource'], 'saveTourismTax').and.returnValue(of({
        id
      }));

      component.isFirstTime = true;
      component.propertyGuestTypeList = propertyGuestTypeList;

      component.confirm();

      tick();

      expect(component.emitSuccessMessage).toHaveBeenCalledWith('variable.lbSaveSuccessF');
      expect(component['tourismTaxResource'].saveTourismTax).toHaveBeenCalledWith(itemToSend);
      expect(component.tourismTaxId).toEqual(id);
    }));

    it( 'should test on update', fakeAsync(() => {
      const id = '123';
      spyOn(component['tourismTaxResource'], 'updateTourismTax').and.callThrough();

      component.isFirstTime = false;
      component.propertyGuestTypeList = propertyGuestTypeList;
      itemToSend.id = id;
      component.tourismTaxId = id;

      component.confirm();

      tick();

      expect(component.emitSuccessMessage).toHaveBeenCalledWith('variable.lbEditSuccessF');
      expect(component['tourismTaxResource'].updateTourismTax).toHaveBeenCalledWith(itemToSend);
    }));
  });
});
