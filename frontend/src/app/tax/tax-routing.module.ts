import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TourismTaxComponent } from 'app/tax/component-container/tourism-tax/tourism-tax.component';

const routes: Routes = [
  { path: 'tourism-tax', component: TourismTaxComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxRoutingModule { }
