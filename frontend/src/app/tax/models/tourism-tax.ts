import { PropertyGuestTypeExemptOfTourismTaxDto } from 'app/shared/models/dto/property-guest-type-dto';
import { CheckinAuditEnum } from 'app/tax/models/checkin-audit';

export class TourismTax {
  amount: number;
  billingItemId: number;
  id: string;
  isActive: true;
  isIntegratedFiscalDocument: boolean;
  isTotalOfNights: boolean;
  launchType: CheckinAuditEnum;
  numberOfNights: number;
  propertyGuestTypeList: Array<PropertyGuestTypeExemptOfTourismTaxDto>;
}

export class GuestReservationItemIdToSend {
  guestReservationItemId: number;
}
