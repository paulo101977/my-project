import { Injectable } from '@angular/core';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';
import { GuestReservationItemIdToSend, TourismTax } from 'app/tax/models/tourism-tax';
import { ErrorHandlerInterceptor } from 'app/core/interceptors/error-handler.interceptor';

@Injectable({ providedIn: 'root' })
export class TourismTaxResource {

  public readonly TOURISM_TAX_PATH = 'tourismtax';

  constructor(
    private thexApi: ThexApiService,
    private errorInterceptor: ErrorHandlerInterceptor
  ) {}

  public getSavedTourismTax(): Observable<TourismTax> {
    this.errorInterceptor.disableOnce();

    return this
      .thexApi
      .get(`${this.TOURISM_TAX_PATH}/getbypropertyid`);
  }

  public saveTourismTax(item: TourismTax): Observable<TourismTax> {
    return this
      .thexApi
      .post(`${this.TOURISM_TAX_PATH}`, item);
  }

  public updateTourismTax(item: TourismTax): Observable<any> {
    return this
      .thexApi
      .put(`${this.TOURISM_TAX_PATH}`, item);
  }

  public toggleTourismTax(id: string): Observable<any> {
    return this
      .thexApi
      .patch(`${this.TOURISM_TAX_PATH}/toggleactivation/${id}`);
  }

  public launchTourismTax(guestForCheckinIdList: Array<GuestReservationItemIdToSend>): Observable<any> {
    return this
      .thexApi
      .patch(`${this.TOURISM_TAX_PATH}/launch`, guestForCheckinIdList);
  }
}
