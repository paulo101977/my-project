import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaxRoutingModule } from './tax-routing.module';
import { TourismTaxComponent } from './component-container/tourism-tax/tourism-tax.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    TaxRoutingModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [TourismTaxComponent]
})
export class TaxModule { }
