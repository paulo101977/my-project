import { createServiceStub } from '../../../../../testing';
import { TourismTaxResource } from 'app/tax/resources/tourism-tax.resource';
import { of } from 'rxjs';
import { TourismTaxService } from 'app/tax/services/tourism-tax.service';
import { GuestReservationItemIdToSend } from 'app/tax/models/tourism-tax';

export const tourismTaxResourceStub = createServiceStub(TourismTaxResource, {
  getSavedTourismTax: () => of( null ),
  toggleTourismTax: (id: string) => of( null ),
  saveTourismTax: (item) => of( null ),
  updateTourismTax: (item) => of( null ),
  launchTourismTax: (guestForCheckinIdList: Array<GuestReservationItemIdToSend>) => of( null ),
});

export const tourismTaxServiceStub = createServiceStub(TourismTaxService, {
  couldShow: () => false,
});
