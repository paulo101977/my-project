export class MenuItems {
  title: string;
  imgSrc: string;
  items: Items[];
}

export class Items {
  title: string;
  routerLink: string;
}
