import {Component} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'property-config-container',
  templateUrl: './property-config-container.component.html',
})

export class PropertyConfigContainerComponent {

  constructor(private translateService: TranslateService) { }

  public title = this.translateService.instant('hotelPanelModule.propertyEdit.title');

}
