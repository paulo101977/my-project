import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { APP_NAME } from '@inovacaocmnet/thx-bifrost';
import { PropertyConfigContainerComponent } from './property-config-container.component';
import { SharedModule } from 'app/shared/shared.module';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';

describe('PropertyConfigContainerComponent', () => {
  let component: PropertyConfigContainerComponent;
  let fixture: ComponentFixture<PropertyConfigContainerComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        PropertyConfigContainerComponent,
      ],
      imports: [TranslateTestingModule, RouterTestingModule, HttpClientTestingModule, SharedModule],
      providers: [
        { provide: APP_NAME, useValue: {} },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(PropertyConfigContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
