import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { Property } from 'app/shared/models/property';
import { modalEmitServiceStub } from 'app/shared/services/shared/testing/modal-emit-service';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {AdminApiService, APP_NAME} from '@inovacaocmnet/thx-bifrost';
import {
  PropertySidebarContainerComponent,
} from 'app/property/components-containers/property-sidebar-container/property-sidebar-container.component';
import { propertyStorageServiceStub } from '@bifrost/storage/services/testing';
import { propertyResourceStub } from 'app/shared/resources/property/testing';
import { of } from 'rxjs';
import { toasterEmitServiceStub } from '../../../../../testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

describe('PropertySidebarContainerComponent', () => {

  let component: PropertySidebarContainerComponent;
  let fixture: ComponentFixture<PropertySidebarContainerComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        PropertySidebarContainerComponent,
      ],
      imports: [
        TranslateTestingModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      providers: [
        { provide: APP_NAME, useValue: {} },
        { provide: AdminApiService, useValue: {} },
        propertyStorageServiceStub,
        propertyResourceStub,
        toasterEmitServiceStub,
        modalEmitServiceStub
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(PropertySidebarContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should populate attributes and call getPropertyData and populate attributes in ngOnInit', () => {
    spyOn<any>(component, 'getPropertyData').and.callThrough();
    spyOn(component, 'buildList');

    spyOn(component['propertyResource'], 'getPropertyById').and.returnValue(of(<Property>{
      name: 'hotel',
      tradeName: 'group',
      photo: 'image',
    }));

    component.ngOnInit();

    expect(component['getPropertyData']).toHaveBeenCalled();
    expect(component['buildList']).toHaveBeenCalled();
    expect(component.hotelName).toBe('hotel');
    expect(component.tradeName).toBe('group');
    expect(component.photoUrl).not.toBeUndefined();
  });

  it('should buildList', () => {
    spyOn(component['propertyService'], 'getPropertyCountryCode').and.returnValue('BR');
   });

  it('should closeModal', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    component.closeModal();
    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalled();
  });

  it('should toggleModal', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    component.toggleModal();
    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalled();
  });

  it('should changePhoto', () => {
    spyOn(component['propertyResource'], 'changePhoto').and.returnValue(of(<Property>{photo: 'photoUrl'}));
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    component.changePhoto('image/png;base64,AAANSUhE');

    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalled();
  });
});

