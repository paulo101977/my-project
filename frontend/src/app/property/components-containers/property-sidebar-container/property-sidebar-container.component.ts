import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {CountryCodeEnum, PropertyStorageService} from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { MENU_LIST_DATA } from 'app/property/mock-data/menu-list-mock-data';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { PropertyService } from 'app/shared/services/property/property.service';

@Component({
  selector: 'app-property-sidebar-container',
  templateUrl: './property-sidebar-container.component.html',
  styleUrls: ['./property-sidebar-container.component.scss']
})
export class PropertySidebarContainerComponent implements OnInit {

  constructor(
    private propertyStorageService: PropertyStorageService,
    private propertyResource: PropertyResource,
    private modalEmitToggleService: ModalEmitToggleService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private propertyService: PropertyService
  ) { }

  public menuList: any;
  private propertyId: number;
  public hotelName: string;
  public tradeName: string;
  public photoUrl: string;

  public readonly MODAL_ID = 'property-photo-modal';
  public selectedImage: string;

  @ViewChild('photoInput') photoInput: ElementRef;

  ngOnInit() {
    this.getPropertyData();
    this.buildList();
  }

  public buildList() {
    const propertyCountryList = [CountryCodeEnum.PT];
    const countryCode = this.propertyService.getPropertyCountryCode();
    this.menuList = MENU_LIST_DATA;

    if (propertyCountryList && propertyCountryList.includes(countryCode)) {
      this.menuList[0].items.push({
        title: 'title.tourismTax',
        routerLink: '../config/tax/tourism-tax'
      });
    }
  }
  private getPropertyData() {
    this.propertyId = this.propertyStorageService.getCurrentPropertyId();
    this.propertyResource.getPropertyById(this.propertyId).subscribe(data => {
      this.hotelName = data.name;
      this.tradeName = data.tradeName;
      this.photoUrl = data.photo ? `${data.photo}?_=${new Date().getTime()}` : data.photo;
    });
  }

  public changePhoto(imgBase64) {
    const photoBase64 = imgBase64.substr(imgBase64.indexOf('base64,') + 7);
    this.propertyResource.changePhoto(photoBase64)
      .subscribe(({photo}) => {
        this.photoUrl = `${photo}?_=${new Date().getTime()}`;
        this.toggleModal();

        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('variable.lbEditSuccessF', {
            labelName: this.translateService.instant('label.photo'),
          })
        );
      });
  }

  public closeModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_ID);
  }

  public toggleModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_ID);
  }

  public photoInputClick() {
    this.photoInput.nativeElement.click();
  }

  public openPhotoModal($event) {
    const file: File = $event.target.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (loadEvent: any) => {
      this.selectedImage = loadEvent.target.result;
      this.photoInput.nativeElement.value = '';
      this.toggleModal();
    };

    myReader.readAsDataURL(file);
  }
}
