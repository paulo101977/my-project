import { MenuItems, Items } from 'app/property/models/menuItems';

// TO DO: Switch from images to icons OR upgrade images's quality

// TO DO: Investigate IF the sidebar is gonna show in all the menu's pages
export const MENU_LIST_DATA_1 = new MenuItems();
MENU_LIST_DATA_1.title = 'title.hotel';
MENU_LIST_DATA_1.imgSrc = 'company.svg';
MENU_LIST_DATA_1.items = <Items[]>[
  {
    title: 'hotelPanelModule.propertyEdit.hotelData.title',
    routerLink: '../config'
  },
  {
    title: 'label.parameters',
    routerLink: '../config/parameter'
  },
  {
    title: 'title.policys',
    routerLink: '../config/policies'
  },
  {
    title: 'menu.userManagement',
    routerLink: '../config/user'
  },
  {
    title: 'menu.guestType',
    routerLink: '../config/guest-type'
  },
  {
    title: 'menu.reasonRegister',
    routerLink: '../reason'
  },
  {
    title: 'title.currencyExchange',
    routerLink: '../currency-exchange-manage'
  }
];

export const MENU_LIST_DATA_2 = new MenuItems();
MENU_LIST_DATA_2.title = 'menu.reservation';
MENU_LIST_DATA_2.imgSrc = 'ht-bookings-gray.svg';
MENU_LIST_DATA_2.items = <Items[]>[
  {
    title: 'hotelPanelModule.propertyEdit.createOrigin',
    routerLink: '../config/origin'
  },
  {
    title: 'hotelPanelModule.propertyEdit.createMarketSegment',
    routerLink: '../config/market-segment'
  }
];

export const MENU_LIST_DATA_3 = new MenuItems();
MENU_LIST_DATA_3.title = 'menu.room';
MENU_LIST_DATA_3.imgSrc = 'camaCasal.min.svg';
MENU_LIST_DATA_3.items = <Items[]>[
  {
    title: 'title.configUh',
    routerLink: '../room'
  },
  {
    title: 'menu.roomType',
    routerLink: '../room-type'
  },
  {
    title: 'title.governanceStatus',
    routerLink: '../governance/room'
  }
];

export const MENU_LIST_DATA_4 = new MenuItems();
MENU_LIST_DATA_4.title = 'menu.client';
MENU_LIST_DATA_4.imgSrc = 'adult-icon.svg';
MENU_LIST_DATA_4.items = <Items[]>[
  {
    title: 'menu.clientCategory',
    routerLink: '../config/category'
  },
  {
    title: 'menu.client',
    routerLink: '../client'
  }
];

export const MENU_LIST_DATA_5 = new MenuItems();
MENU_LIST_DATA_5.title = 'menu.billingAccount';
MENU_LIST_DATA_5.imgSrc = 'tarifa-grey.svg';
MENU_LIST_DATA_5.items = <Items[]>[
  {
    title: 'title.parameterFiscal',
    routerLink: '../config/fiscal-documents/parameter'
  }
];

export const MENU_LIST_DATA: MenuItems[] = [
  MENU_LIST_DATA_1,
  MENU_LIST_DATA_2,
  MENU_LIST_DATA_3,
  MENU_LIST_DATA_4,
  MENU_LIST_DATA_5
];
