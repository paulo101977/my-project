import { Property } from 'app/shared/models/property';
import { Brand } from 'app/shared/models/brand';
import { Chain } from 'app/shared/models/chain';

export const PROPERTY_1_DATA = new Property();
PROPERTY_1_DATA.id = 1;
PROPERTY_1_DATA.name = 'Hotel Totvs Resorts Rio de Janeiro';
PROPERTY_1_DATA.brand = new Brand();
PROPERTY_1_DATA.brand.name = 'Bandeira Totvs Hoteis Resorts';
PROPERTY_1_DATA.brand.chain = new Chain();
PROPERTY_1_DATA.brand.chain.name = 'Rede Totvs Hoteis';
PROPERTY_1_DATA.propertyStatusId = 19; // production

export const PROPERTY_2_DATA = new Property();
PROPERTY_2_DATA.id = 2;
PROPERTY_2_DATA.name = 'Teste Portugal';
PROPERTY_2_DATA.brand = new Brand();
PROPERTY_2_DATA.brand.name = 'Bandeira Totvs Hoteis Resorts';
PROPERTY_2_DATA.brand.chain = new Chain();
PROPERTY_2_DATA.brand.chain.name = 'Rede Atlas Hoteis';
PROPERTY_2_DATA.propertyStatusId = 15; // training

export const PROPERTY_3_DATA = new Property();
PROPERTY_3_DATA.id = 17;
PROPERTY_3_DATA.name = 'Portugal Hotel';
PROPERTY_3_DATA.brand = new Brand();
PROPERTY_3_DATA.brand.name = 'Portugal Bandeira';
PROPERTY_3_DATA.brand.chain = new Chain();
PROPERTY_3_DATA.brand.chain.name = 'Portugal Rede';

export const PROPERTY_4_DATA = new Property();
PROPERTY_4_DATA.id = 17;
PROPERTY_4_DATA.name = 'Homolog Hotel';
PROPERTY_4_DATA.brand = new Brand();
PROPERTY_4_DATA.brand.name = 'Homolog Bandeira';
PROPERTY_4_DATA.brand.chain = new Chain();
PROPERTY_4_DATA.brand.chain.name = 'Homolog Rede';

export const PROPERTIES_DATA: Property[] = [
  PROPERTY_1_DATA,
  PROPERTY_2_DATA,
  PROPERTY_3_DATA,
  PROPERTY_4_DATA
];
