import { TestBed } from '@angular/core/testing';

import { PropertyService } from './property.service';
import { configureTestSuite } from 'ng-bullet';
import { PROPERTY_1_DATA, PROPERTY_2_DATA } from 'app/property/mock-data/property-mock-data';

describe('PropertyService', () => {
  let service: PropertyService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [PropertyService]
    });

    service = TestBed.get(PropertyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should getInitials', () => {
    const initials = service.getInitials(PROPERTY_1_DATA);
    expect(initials).toEqual('HT');
  });

  it('should getStatus', () => {
    const statusProperty_1 = service.getStatus(PROPERTY_1_DATA);
    expect(statusProperty_1.statusColor).toEqual('#00B992');
    expect(statusProperty_1.statusName).toEqual('active');

    const statusProperty_2 = service.getStatus(PROPERTY_2_DATA);
    expect(statusProperty_2.statusColor).toEqual('#4F4870');
    expect(statusProperty_2.statusName).toEqual('training');
  });

});
