import { Injectable } from '@angular/core';
import { Property } from 'app/shared/models/property';
import { PropertyStatusEnum } from 'app/shared/models/property-status-enum';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  constructor() {
  }

  public getInitials(property: Property) {
    if (property) {
      const nameArray = property.name.split(' ');
      return nameArray.reduce((prev, current) => `${prev}${current[0]}`, '').slice(0, 2);
    }
    return '';
  }

  public getStatus(property: Property): { statusColor, statusName } {
    if (property && property.propertyStatusId) {
      let statusColor: string;
      let statusName: string;
      switch (property.propertyStatusId) {
        case +PropertyStatusEnum.register:
          statusColor = ''; // TODO Change color when UX is done
          statusName = 'register';
          break;
        case +PropertyStatusEnum.contract:
          statusColor = ''; // TODO Change color when UX is done
          statusName = 'contract';
          break;
        case +PropertyStatusEnum.training:
          statusColor = '#4F4870';
          statusName = 'training';
          break;
        case +PropertyStatusEnum.production:
          statusColor = '#00B992';
          statusName = 'active';
          break;
        case +PropertyStatusEnum.blocked:
          statusColor = ''; // TODO Change color when UX is done
          statusName = 'blocked';
          break;
      }
      return {
        statusColor,
        statusName
      };
    }
    return null;
  }

}
