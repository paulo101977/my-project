import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UserModule } from 'app/user/user.module';
import {
  ThxBaseTopBarModule,
  ThxPopoverBaseModule,
  AcronymModule,
  ThxImgModule,
  ThxPageTitleModule,
  ThxToasterV2Module,
  ThxSidebardCardContainerModule
} from '@inovacao-cmnet/thx-ui';
import { PropertyRoutingModule } from './property-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MyDatePickerModule } from 'mydatepicker';
import { HttpClientModule } from '@angular/common/http';
import { GovernanceModule } from '../governance/governance.module';
import { PropertyCardComponent } from './components/property-card/property-card.component';
import { PropertyEditComponent } from './components/property-edit/property-edit.component';
import { PropertyContentContainerComponent } from './components/property-content-container/property-content-container.component';
import { PropertyConfigContainerComponent } from './components-containers/property-config-container/property-config-container.component';
import { ParameterComponent } from './components/parameter/parameter.component';
import { ParameterInputTypeComponent } from './components/parameter-input-type/parameter-input-type.component';
import { ParameterService } from '../shared/services/parameter/parameter.service';
import { PropertyResource } from '../shared/resources/property/property.resource';
import { ParameterResource } from 'app/shared/resources/parameter/parameter-resource';
import { ConfigModule } from '../config/config.module';
import { PropertiesPanelComponent } from './components/properties-panel/properties-panel.component';
import { PropertyTopBarComponent } from './components/property-top-bar/property-top-bar.component';
import { PropertySidebarContainerComponent } from './components-containers/property-sidebar-container/property-sidebar-container.component';
import { PropertyCardSidebarComponent } from './components/property-card-sidebar/property-card-sidebar.component';
import { PropertyListSidebarComponent } from './components/property-list-sidebar/property-list-sidebar.component';
import { UserLanguageModule } from 'app/shared/components/user-language-selection/user-language.module';
import { PhotoBaseComponent } from './components/photo-base/photo-base.component';
import { PropertyPhotoUploadComponent } from './components/property-photo-upload/property-photo-upload.component';

@NgModule({
  declarations: [
    PropertyCardComponent,
    PropertyEditComponent,
    PropertyContentContainerComponent,
    PropertyConfigContainerComponent,
    ParameterComponent,
    ParameterInputTypeComponent,
    PropertiesPanelComponent,
    PropertyTopBarComponent,
    PropertySidebarContainerComponent,
    PropertyCardSidebarComponent,
    PropertyListSidebarComponent,
    PhotoBaseComponent,
    PropertyPhotoUploadComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    SharedModule,
    PropertyRoutingModule,
    MyDatePickerModule,
    ConfigModule,
    GovernanceModule,
    ThxImgModule,
    ThxPageTitleModule,
    UserModule,
    ThxBaseTopBarModule,
    ThxPopoverBaseModule,
    UserLanguageModule,
    AcronymModule,
    ThxToasterV2Module,
    ThxSidebardCardContainerModule
  ],
  providers: [PropertyResource, ParameterService, ParameterResource],
  exports: [
    PropertiesPanelComponent,
    PropertyCardComponent,
  ],
})
export class PropertyModule {}
