import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CropperSettings, ImageCropperComponent } from 'ngx-img-cropper';

@Component({
  selector: 'app-property-photo-upload',
  templateUrl: './property-photo-upload.component.html',
  styleUrls: ['./property-photo-upload.component.scss']
})
export class PropertyPhotoUploadComponent implements OnInit {
  @ViewChild('cropper') cropper: ImageCropperComponent;

  item: string;
  cropperSettings: CropperSettings;
  data: any;

  @Input()
  get selectedImage() {
    return this.item;
  }

  set selectedImage(val) {
    if (val) {
      this.item = val;
      this.loadImage();
    }
  }

  @Output() dataChange = new EventEmitter<string>();
  @Output() exitChange = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.configCropper();
  }

  public configCropper() {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.width = 176;
    this.cropperSettings.height = 99;
    this.cropperSettings.croppedWidth = 176;
    this.cropperSettings.croppedHeight = 99;
    this.cropperSettings.minWidth = 176;
    this.cropperSettings.minHeight = 99;
    this.cropperSettings.canvasWidth = 400;
    this.cropperSettings.canvasHeight = 225;
    this.cropperSettings.rounded = false;
    this.cropperSettings.keepAspect = true;

    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 1;

    this.data = {};
  }

  public loadImage() {
    const image: any = new Image();

    if ( this.selectedImage ) {
      image.src = this.selectedImage;
      image.onload = () => {
        this.cropper.setImage(image);
      };
    }
  }

  public cancelRequest() {
    this.exitChange.emit();
  }

  public changeData(): void {
    this.dataChange.emit(this.data.image);
  }

}
