import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { PropertyContentContainerComponent } from './property-content-container.component';
import { ToasterComponent } from '../../../shared/components/toaster/toaster.component';
import { ModalComponent } from '../../../shared/components/modal/modal.component';
import { ModalRemarksComponent } from '../../../shared/components/modal-remarks/modal-remarks.component';
import { ModalGroupOptionsComponent } from '../../../shared/components/modal-group-options/modal-group-options.component';
import {
  ModalAccommodationBudgetComponent
} from '../../../shared/components/modal-accommodation-budget/modal-accommodation-budget.component';
import { LoadPageComponent } from '../../../shared/components/load-page/load-page.component';
import { LoadPageNewComponent } from '../../../shared/components/load-page/load-page-new.component';
import { TopBarComponent } from '../../../shared/components/top-bar/top-bar.component';
import { BottomBarComponent } from '../../../shared/components/bottom-bar/bottom-bar.component';
import { MenuComponent } from '../../../shared/components/menu/menu.component';
import { MenuOptionComponent } from '../../../shared/components/menu-option/menu-option.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

xdescribe('PropertyContentContainerComponent', () => {
  let component: PropertyContentContainerComponent;
  let fixture: ComponentFixture<PropertyContentContainerComponent>;
  let menuComponent: MenuComponent;
  let topBarComponent: TopBarComponent;
  let fixtureMenu: ComponentFixture<MenuComponent>;
  let fixtureTopBar: ComponentFixture<TopBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, FormsModule, RouterTestingModule, TranslateModule.forRoot(), CurrencyMaskModule],
      declarations: [
        PropertyContentContainerComponent,
        ModalComponent,
        ModalRemarksComponent,
        ModalGroupOptionsComponent,
        ModalAccommodationBudgetComponent,
        ToasterComponent,
        LoadPageComponent,
        LoadPageNewComponent,
        TopBarComponent,
        BottomBarComponent,
        MenuComponent,
        MenuOptionComponent,
      ],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyContentContainerComponent);
    fixtureMenu = TestBed.createComponent(MenuComponent);
    fixtureTopBar = TestBed.createComponent(TopBarComponent);
    component = fixture.componentInstance;
    menuComponent = fixtureMenu.componentInstance;
    topBarComponent = fixtureTopBar.componentInstance;

    fixture.detectChanges();
  });

  it('should create PropertyContentContainerComponent', () => {
    expect(component).toBeTruthy();
    expect(menuComponent).toBeTruthy();
    expect(topBarComponent).toBeTruthy();
  });
});
