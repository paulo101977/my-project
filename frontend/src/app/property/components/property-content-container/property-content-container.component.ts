import { Location } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { UserPermissionMenuItem, UserPermissionService } from '@thex/security';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';
import { AppComponent } from 'app/app.component';
import { APP_NAME, ProductEnum, TabAuthControlService } from '@inovacaocmnet/thx-bifrost';
import { UserService } from 'app/shared/services/user/user.service';
import { PmsMenuPermissions } from 'app/permission/menu/pms-menu-permissions';


@Component({
  selector: 'property-content-container',
  templateUrl: './property-content-container.component.html',
  styleUrls: ['./property-content-container.component.scss'],
})
export class PropertyContentContainerComponent implements OnInit {
  public menuOptions$: Observable<UserPermissionMenuItem[]>;
  public userName = '';
  public hideMainMenu: boolean;

  constructor(
    @Inject(APP_NAME) private appName: string,
    private location: Location,
    public userService: UserService,
    private tabService: TabAuthControlService,
    private userPermissionService: UserPermissionService,
  ) {
  }

  private setMenuOptions() {
    this.menuOptions$ = this.userPermissionService
      .getUserPermissions(this.appName, ProductEnum.PMS, PmsMenuPermissions)
      .pipe(
        pluck('menuList'),
      );
  }

  ngOnInit() {
    this.setShowMainMenu();
    this.tabService.addTab(this.appName, AppComponent.tabId);
  }

  private setShowMainMenu() {
    this.hideMainMenu = this.location.path().indexOf('home') > -1;
    this.setMenuOptions();
  }
}
