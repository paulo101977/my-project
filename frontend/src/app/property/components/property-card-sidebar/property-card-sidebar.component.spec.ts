import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_NAME } from '@inovacaocmnet/thx-bifrost';
import { configureTestSuite } from 'ng-bullet';
import { PropertyCardSidebarComponent } from './property-card-sidebar.component';


describe('PropertyCardSidebarComponent', () => {
  let component: PropertyCardSidebarComponent;
  let fixture: ComponentFixture<PropertyCardSidebarComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        PropertyCardSidebarComponent
      ],
      imports: [  ],
      providers: [
        { provide: APP_NAME, useValue: {} }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(PropertyCardSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should onClick', () => {
    spyOn<any>(component.changePhoto, 'emit');

    component.onClick('event');
    expect(component.changePhoto.emit).toHaveBeenCalledWith('event');
  });
});
