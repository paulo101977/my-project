import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-property-card-sidebar',
  templateUrl: './property-card-sidebar.component.html',
  styleUrls: ['./property-card-sidebar.component.scss']
})
export class PropertyCardSidebarComponent {
  @Input() hotelName: string;
  @Input() hotelGroup: string;
  @Input() photoUrl: string;
  @Output() changePhoto = new EventEmitter();

  public onClick(e) {
    this.changePhoto.emit(e);
  }
}
