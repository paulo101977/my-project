import { Component, OnInit, Input, Output, ViewChild, EventEmitter, AfterViewInit } from '@angular/core';
import { Parameter, ParameterType, ParameterOption, ParameterGroup, ParameterGroupType } from '../../../shared/models/parameter';
import * as moment from 'moment';
import { ParameterTypeEnum } from '../../../shared/models/parameter/pameterType.enum';
import { DateService } from '../../../shared/services/shared/date.service';

@Component({
  selector: 'app-parameter-input-type',
  templateUrl: './parameter-input-type.component.html',
  styleUrls: ['./parameter-input-type.component.css'],
})
export class ParameterInputTypeComponent implements OnInit, AfterViewInit {
  @Input() parameter: Parameter;
  @Input() possibleOptions: Array<any>; // In case of inputType Select

  @Input() customValidation: Function;
  @Input() disabled: Boolean;

  @ViewChild('inputDefault') defaultTemplate;
  @ViewChild('inputTime') timeTemplate;
  @ViewChild('inputDate') dateTemplate;
  @ViewChild('inputNumber') numberTemplate;
  @ViewChild('inputSelect') selectTemplate;
  @ViewChild('inputCheck') checkTemplate;

  @Output() inputValueChange: EventEmitter<any> = new EventEmitter();
  @Output() changeParam: EventEmitter<Parameter> = new EventEmitter();
  @Output() changeError: EventEmitter<any> = new EventEmitter();

  public selectedTemplate = ViewChild;
  public oldValue = null;

  public minDateIsToday = false;

  constructor(public dateService: DateService) {}

  ngOnInit() {
    if (!this.parameter) {
      console.error('a parameter must be set to ParameterInputTypeComponent');
    }
    this.defineInputType();
  }

  ngAfterViewInit(): void {
    this.oldValue = this.parameter.propertyParameterValue;
  }

  defineInputType() {
    switch (this.parameter.parameterTypeName) {
      case ParameterType.Select:
        if (!this.parameter.propertyParameterPossibleValues) {
          // caso não tenha nenhuma opção definida,
          // uma será criada com o valor definido.
          if (this.parameter.propertyParameterValue) {
            const opt = new ParameterOption();
            opt.title = this.parameter.propertyParameterValue;
            opt.value = this.parameter.propertyParameterValue;
            this.possibleOptions = [opt];
          }
        } else {
          const possibleValues = JSON.parse(this.parameter.propertyParameterPossibleValues);
          if (possibleValues instanceof Array) {
            this.possibleOptions = possibleValues;
          } else {
            this.possibleOptions = [];
            const start = this.parameter[possibleValues['start'].charAt(0).toLowerCase() + possibleValues['start'].substr(1)];
            const end = this.parameter[possibleValues['end'].charAt(0).toLowerCase() + possibleValues['end'].substr(1)];
            for (let i = start; i <= end; i++) {
              const opt = new ParameterOption();
              opt.title = i;
              opt.value = i;
              this.possibleOptions.push(opt);
            }
          }
        }
        this.selectedTemplate = this.selectTemplate;
        break;
      case ParameterType.Time:
        this.selectedTemplate = this.timeTemplate;
        break;
      case ParameterType.Date:
        this.selectedTemplate = this.dateTemplate;
        if (this.parameter.applicationParameterId == ParameterTypeEnum.SistemDate) {
          this.minDateIsToday = true;
        }
        break;
      case ParameterType.Number:
        this.selectedTemplate = this.numberTemplate;
        if (!this.parameter.propertyParameterValue) {
          this.parameter.propertyParameterValue = '0';
        }
        break;
      case ParameterType.CheckBox:
        this.selectedTemplate = this.checkTemplate;
        break;
      default:
        this.selectedTemplate = this.defaultTemplate;
        break;
    }
  }

  notifyChange(value) {
    if (
      (this.parameter.propertyParameterValue != value && this.validateInput(value)) ||
      (this.parameter.featureGroupId == ParameterGroupType.GroupChildren && this.validateInput(value)) ||
      (this.parameter.parameterTypeName == ParameterType.Number &&
        !this.parameter.propertyParameterMaxValue &&
        !this.parameter.propertyParameterMinValue)
    ) {
      this.parameter.propertyParameterValue = value;
      this.changeParam.emit(this.parameter);
    }
  }

  validateInput(value): boolean {
    if (this.parameter.propertyParameterMinValue && this.parameter.propertyParameterMaxValue) {
      if (this.customValidation) {
        if (!this.customValidation(value, this.parameter)) {
          this.changeError.emit(this.createErrorMessage());
          this.parameter.propertyParameterValue = this.oldValue;
        }
      }

      if (this.parameter.parameterTypeName == ParameterType.Number) {
        const intValue = parseInt(value, 0);
        const intMax = parseInt(this.parameter.propertyParameterMaxValue, 0);
        const intMin = parseInt(this.parameter.propertyParameterMinValue, 0);

        if (intValue <= intMax && intValue >= intMin) {
          return true;
        } else {
          this.changeError.emit(this.createErrorMessage());
        }
      }

      if (this.parameter.parameterTypeName == ParameterType.Time) {
        const format = 'HH:mm';
        const time = moment(value, format, true);
        const minMoment = moment(this.parameter.propertyParameterMinValue, format);
        const maxMoment = moment(this.parameter.propertyParameterMaxValue, format);

        if (time.isSameOrAfter(minMoment) && time.isSameOrBefore(maxMoment)) {
          return true;
        } else {
          this.changeError.emit(this.createErrorMessage());
        }
      }

      if (this.parameter.parameterTypeName == ParameterType.Select) {
        if (value <= this.parameter.propertyParameterMaxValue && value >= this.parameter.propertyParameterMinValue) {
          return true;
        }
      }
    } else if (this.parameter.applicationParameterId == ParameterTypeEnum.SistemDate) {
      return value && moment(this.dateService.convertStringToDate(value, 'YYYY-MM-DD')).isSameOrAfter(moment(), 'days');
    } else {
      return true;
    }

    return false;
  }

  createErrorMessage() {
    return {
      description: this.parameter.parameterDescription,
      min: this.parameter.propertyParameterMinValue,
      max: this.parameter.propertyParameterMaxValue,
    };
  }

  public maskValidation = rawValue => {
    const numbers = rawValue.split('');
    const firstDigit = numbers[0];
    const validateArray = [];

    if (firstDigit == 0) {
      validateArray.push(/[0]/, /[0-9]/);
    } else if (firstDigit == 1) {
      validateArray.push(/[1]/, /[0-9]/);
    } else if (firstDigit == 2) {
      validateArray.push(/[2]/, /[0-3]/);
    } else {
      return [/[0-2]/];
    }

    validateArray.push(':', /[0-5]/, /[0-9]/);
    return validateArray;
  }

  public changeInput(value) {
    if (this.validateInput(value)) {
      this.inputValueChange.emit(value);
      this.oldValue = value;
    }
  }
}
