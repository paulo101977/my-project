import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterInputTypeComponent } from './parameter-input-type.component';

describe('ParameterInputTypeComponent', () => {
  let component: ParameterInputTypeComponent;
  let fixture: ComponentFixture<ParameterInputTypeComponent>;

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterInputTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
