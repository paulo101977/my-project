import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-photo-base',
  templateUrl: './photo-base.component.html',
  styleUrls: ['./photo-base.component.scss']
})
export class PhotoBaseComponent {

  @Input() photoUrl: string;
  @Input() name: string;
  @Output() changePhoto = new EventEmitter();

  public onClick(e) {
    this.changePhoto.emit(e);
  }

}
