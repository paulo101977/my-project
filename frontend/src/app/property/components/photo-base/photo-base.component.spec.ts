import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { configureTestSuite } from 'ng-bullet';
import { AcronymTestingModule } from '@inovacao-cmnet/thx-ui';

import { PhotoBaseComponent } from './photo-base.component';

describe('PhotoBaseComponent', () => {
  let component: PhotoBaseComponent;
  let fixture: ComponentFixture<PhotoBaseComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [PhotoBaseComponent],
      imports: [
        CommonModule,
        TranslateModule.forRoot(),
        AcronymTestingModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(PhotoBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should onClick', () => {
    spyOn<any>(component.changePhoto, 'emit');

    component.onClick('event');
    expect(component.changePhoto.emit).toHaveBeenCalledWith('event');
  });
});
