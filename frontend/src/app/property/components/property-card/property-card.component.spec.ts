import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { PropertyCardComponent } from './property-card.component';
import { configureTestSuite } from 'ng-bullet';
import { CommonModule } from '@angular/common';
import { PROPERTY_1_DATA } from 'app/property/mock-data/property-mock-data';
import { ImgCdnTestingModule } from '@inovacao-cmnet/thx-ui';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { Property } from 'app/shared/models/property';
import { assetsCdnServiceStub } from '../../../../../projects/ui/src/lib/services/testing/assets-cdn.stub';

describe('PropertyCardComponent', () => {
  let component: PropertyCardComponent;
  let fixture: ComponentFixture<PropertyCardComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [PropertyCardComponent],
      imports: [
        CommonModule,
        TranslateTestingModule,
        RouterTestingModule,
        ImgCdnTestingModule
      ],
      providers: [ assetsCdnServiceStub ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(PropertyCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should chooseProperty', () => {
    spyOn(component.chosen, 'emit');

    component.chooseProperty(PROPERTY_1_DATA);

    expect(component.chosen.emit).toHaveBeenCalledWith(PROPERTY_1_DATA);
  });

  it('should getPropertyInitials', () => {
    spyOn(component['propertyService'], 'getInitials');

    component.getPropertyInitials();

    expect(component['propertyService'].getInitials).toHaveBeenCalledWith(component.property);
  });

  it('should getPropertyStatus', () => {
    spyOn(component['propertyService'], 'getStatus');

    component.getPropertyStatus();

    expect(component['propertyService'].getStatus).toHaveBeenCalledWith(component.property);
  });

  it('should ngOnChanges has photo', () => {
    component.urlPhoto = 'hotel.jpg';
    component.property = new Property();
    component.property.photo = 'pms_login_bg_v2.jpg';

    component.ngOnChanges({});
    expect(component.urlPhoto).toEqual('pms_login_bg_v2.jpg');
  });

  it('should ngOnChanges do not have photo', () => {
    spyOn<any>(component['assetsService'], 'getImgUrlTo');
    component.property = new Property();

    component.ngOnChanges({});

    expect(component['assetsService'].getImgUrlTo).toHaveBeenCalledWith('hotel.jpg');
  });

});
