import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Property } from 'app/shared/models/property';
import { PropertyService } from 'app/property/services/property.service';
import { TranslateService } from '@ngx-translate/core';
import { AssetsCdnService } from '../../../../../projects/ui/src/lib/services/assets-service/assets-cdn.service';

@Component({
  selector: 'app-property-card',
  styleUrls: ['./property-card.component.css'],
  templateUrl: './property-card.component.html',
})
export class PropertyCardComponent implements OnInit, OnChanges {

  @Input() property: Property;
  @Output() chosen = new EventEmitter<Property>();

  public statusColor = '#00B992';
  public statusName = 'active';
  public urlPhoto: string;

  constructor(
    private propertyService: PropertyService,
    private translate: TranslateService,
    private assetsService: AssetsCdnService
  ) {
  }

  ngOnInit() {
    this.getPropertyStatus();
  }

  ngOnChanges (changes: SimpleChanges): void {
    if (this.property && this.property.photo) {
      this.urlPhoto = this.property.photo;
    } else {
      this.urlPhoto = this.assetsService.getImgUrlTo('hotel.jpg');
    }
  }

  public chooseProperty(property: Property) {
    this.chosen.emit(property);
  }

  public getPropertyInitials() {
    return this.propertyService.getInitials(this.property);
  }

  public getPropertyStatus() {
    const status = this.propertyService.getStatus(this.property);

    if (status) {
      this.statusColor = status.statusColor;
      this.statusName = this.translate.instant('label.' + status.statusName);
    }
  }

}
