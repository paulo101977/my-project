import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Parameter, ParameterGroup, ParameterGroupType } from '../../../shared/models/parameter';
import { ParameterTypeEnum } from '../../../shared/models/parameter/pameterType.enum';
import { ParameterService } from '../../../shared/services/parameter/parameter.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { SessionParameterService } from '../../../shared/services/parameter/session-parameter.service';
import { ParameterChildAgeRangeService } from '../../../shared/services/parameter/parameter-child-age-range.service';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { ShowHide } from '../../../shared/models/show-hide-enum';

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.css'],
  providers: [ParameterService],
})
export class ParameterComponent implements OnInit {
  public parametersGroupList: Array<ParameterGroup>;
  public propertyId: number;
  public ageRangeValidator: Function;

  constructor(private _parameterService: ParameterService,
              private _loadPageEmitService: LoadPageEmitService,
              private _toasterEmitService: ToasterEmitService,
              private _translateService: TranslateService,
              private _router: ActivatedRoute,
              private _sessionParameter: SessionParameterService,
              private _parameterChildAgeRange: ParameterChildAgeRangeService) {
  }

  ngOnInit() {
    this.propertyId = this._router.snapshot.params.property;
    this.getParameterList();
    this.ageRangeValidator = this.validateAgeRange.bind(this);
  }

  getParameterList() {
    this.parametersGroupList = [];
    this._loadPageEmitService.emitChange(ShowHide.show);
    this._parameterService.getParameters(this.propertyId).subscribe(response => {
      this._loadPageEmitService.emitChange(ShowHide.hide);
      if (response) {
        this.parametersGroupList = response;
      }
    });
  }

  saveParameters(parameter: Parameter) {
    if (
      parameter.applicationParameterId == ParameterTypeEnum.ChildAgeRange1 ||
      parameter.applicationParameterId == ParameterTypeEnum.ChildAgeRange2 ||
      parameter.applicationParameterId == ParameterTypeEnum.ChildAgeRange3
    ) {
      this.childAgeChange(parameter.propertyParameterValue, parameter);
      this._loadPageEmitService.emitChange(ShowHide.show);
      const parameterAgeGroup = this.parametersGroupList.filter(item => item.featureGroupId == ParameterGroupType.GroupChildren)[0];
      this._parameterService.saveChildrenAgeRange(parameterAgeGroup, this.propertyId).subscribe(
        response => {
          this._loadPageEmitService.emitChange(ShowHide.hide);
          this._toasterEmitService.emitChange(
            SuccessError.success,
            this._translateService.instant('hotelPanelModule.propertyEdit.propertyParameter.salvoSucesso'),
          );
          parameterAgeGroup.parameters.forEach(param => {
            this.updateSessionParameter(param);
          });
        },
        error => {
          this._loadPageEmitService.emitChange(ShowHide.hide);
        },
      );
    } else {
      if (parameter && parameter.id) {
        this._loadPageEmitService.emitChange(ShowHide.show);
        this._parameterService.saveParameter(parameter, this.propertyId).subscribe(
          response => {
            this._loadPageEmitService.emitChange(ShowHide.hide);
            this.updateSessionParameter(parameter);
            this._toasterEmitService.emitChange(
              SuccessError.success,
              this._translateService.instant('hotelPanelModule.propertyEdit.propertyParameter.salvoSucesso'),
            );
          },
          error => {
            console.error(error);
            this._loadPageEmitService.emitChange(ShowHide.hide);
          },
        );
      }
    }
  }

  toastError(params) {
    this._toasterEmitService.emitChange(
      SuccessError.error,
      this._translateService.instant('hotelPanelModule.propertyEdit.propertyParameter.parameterInvalid', {
        description: params.description,
        min: params.min,
        max: params.max,
      }),
    );
  }

  public someInGroupCanInactive(groupId) {
    return this._parameterChildAgeRange.someInGroupCanInactive(this.parametersGroupList, groupId);
  }

  public changeStatus(param: Parameter, withoutChildAgeCheck?: boolean, saveAfter?: Parameter) {
    this._loadPageEmitService.emitChange(ShowHide.show);
    if (!withoutChildAgeCheck) {
      if (this.childAgeRangeStatusControl(param) === false) {
        this._loadPageEmitService.emitChange(ShowHide.hide);
        return;
      }
    }
    if (!param.isActive && param.applicationParameterId != ParameterTypeEnum.ChildAgeRange1) {
      param.propertyParameterMinValue = '0';
      param.propertyParameterValue = '0';
      param.propertyParameterMaxValue = '0';
    }
    this._parameterService.saveParameter(param, this.propertyId).subscribe(
      result => {
        if (saveAfter) {
          this.changeStatus(saveAfter, false);
        } else {
          this._loadPageEmitService.emitChange(ShowHide.hide);
        }
        let stringStatus;
        if (param.isActive) {
          stringStatus = 'variable.toggleParameterActivateSuccess';
        } else {
          stringStatus = 'variable.toggleParameterInactivateSuccess';
        }
        this._toasterEmitService.emitChange(SuccessError.success, this._translateService.instant(stringStatus));
      },
      error => {
        this._loadPageEmitService.emitChange(ShowHide.hide);
        let stringStatus;
        if (param.isActive) {
          stringStatus = 'variable.toggleParameterActivateError';
        } else {
          stringStatus = 'variable.toggleParameterInactivateError';
        }
        this._toasterEmitService.emitChange(SuccessError.error, this._translateService.instant(stringStatus));
      },
    );
  }

  private childAgeRangeStatusControl(param: Parameter) {
    if (param.applicationParameterId == ParameterTypeEnum.ChildAgeRange2 && !param.isActive) {
      const child3 = this._parameterChildAgeRange.getParameterByType(
        this.parametersGroupList,
        ParameterTypeEnum.ChildAgeRange3,
      ) as Parameter;
      const child1 = this._parameterChildAgeRange.getParameterByType(
        this.parametersGroupList,
        ParameterTypeEnum.ChildAgeRange1,
      ) as Parameter;
      if (child3 && child3.isActive) {
        child3.isActive = false;
        child1.propertyParameterMaxValue = child3.propertyParameterMaxValue;
        this.changeStatus(child3, true, child1);
      } else if (!child3.isActive) {
        child1.propertyParameterMaxValue = param.propertyParameterMaxValue;
      }
    }
    if (param.applicationParameterId == ParameterTypeEnum.ChildAgeRange2 && param.isActive) {
      const child1 = this._parameterChildAgeRange.getParameterByType(
        this.parametersGroupList,
        ParameterTypeEnum.ChildAgeRange1,
      ) as Parameter;

      this.recalculateRange1WhenRange2IsActive(child1, param);

      this.changeStatus(child1, true);
    }

    if (param.applicationParameterId == ParameterTypeEnum.ChildAgeRange3 && !param.isActive) {
      const child2 = this._parameterChildAgeRange.getParameterByType(
        this.parametersGroupList,
        ParameterTypeEnum.ChildAgeRange2,
      ) as Parameter;
      if (child2 && child2.isActive) {
        child2.propertyParameterMaxValue = param.propertyParameterMaxValue;
        this.changeStatus(child2, true);
      }
    }

    if (param.applicationParameterId == ParameterTypeEnum.ChildAgeRange3 && param.isActive) {
      const child2 = this._parameterChildAgeRange.getParameterByType(
        this.parametersGroupList,
        ParameterTypeEnum.ChildAgeRange2,
      ) as Parameter;
      if (child2 && !child2.isActive) {
        const child1 = this._parameterChildAgeRange.getParameterByType(
          this.parametersGroupList,
          ParameterTypeEnum.ChildAgeRange1,
        ) as Parameter;
        child2.isActive = true;
        child1.propertyParameterValue = (parseFloat(child1.propertyParameterValue) - 4).toString();
        this.validadeMinValues(child1);
        if (parseFloat(child1.propertyParameterValue) <= 0) {
          child1.propertyParameterValue = (parseFloat(child1.propertyParameterValue) + 4).toString();
          child2.isActive = false;
          setTimeout(() => {
            param.isActive = !param.isActive;
          }, 50);
          this._toasterEmitService.emitChange(SuccessError.error, this._translateService.instant('variable.rangeChild1TooLow'));
          return false;
        }
        this.recalculateRange1WhenRange3IsActive(child2, child1);
        this.recalculateRange2WhenRange3IsActive(child2, param);
      } else if (child2 && child2.isActive) {
        const child1 = this._parameterChildAgeRange.getParameterByType(
          this.parametersGroupList,
          ParameterTypeEnum.ChildAgeRange1,
        ) as Parameter;
        child2.propertyParameterValue = (parseFloat(child2.propertyParameterValue) - 2).toString();
        this.validadeMinValues(child2);
        if (parseFloat(child2.propertyParameterValue) <= parseFloat(child1.propertyParameterValue)) {
          child2.propertyParameterValue = (parseFloat(child2.propertyParameterValue) + 2).toString();
          setTimeout(() => {
            param.isActive = !param.isActive;
          }, 50);
          this._toasterEmitService.emitChange(SuccessError.error, this._translateService.instant('variable.rangeChild2TooLow'));
          return false;
        }
        param.propertyParameterMaxValue = child2.propertyParameterMaxValue;
        child2.propertyParameterMaxValue = child2.propertyParameterValue;
        param.propertyParameterMinValue = (parseFloat(child2.propertyParameterValue) + 1).toString();
        param.propertyParameterValue = (parseFloat(child2.propertyParameterValue) + 2).toString();
        this.changeStatus(child2, true);
      }
    }
  }

  private recalculateRange1WhenRange2IsActive(child1: Parameter, param: Parameter) {
    child1.propertyParameterValue = (parseFloat(child1.propertyParameterValue) - 2).toString();
    this.validadeMinValues(child1);
    param.propertyParameterMaxValue = child1.propertyParameterMaxValue;
    child1.propertyParameterMaxValue = child1.propertyParameterValue;
    param.propertyParameterMinValue = (parseFloat(child1.propertyParameterValue) + 1).toString();
    param.propertyParameterValue = (parseFloat(child1.propertyParameterValue) + 2).toString();
  }

  private recalculateRange2WhenRange3IsActive(child2: Parameter, param: Parameter) {
    child2.propertyParameterValue = (parseFloat(child2.propertyParameterValue) - 2).toString();
    this.validadeMinValues(child2);
    param.propertyParameterMaxValue = child2.propertyParameterMaxValue;
    child2.propertyParameterMaxValue = child2.propertyParameterValue;
    param.propertyParameterMinValue = (parseFloat(child2.propertyParameterValue) + 1).toString();
    param.propertyParameterValue = (parseFloat(child2.propertyParameterValue) + 2).toString();
    this.changeStatus(child2, true);
  }

  private recalculateRange1WhenRange3IsActive(child2: Parameter, child1: Parameter) {
    child2.propertyParameterMaxValue = child1.propertyParameterMaxValue;
    child1.propertyParameterMaxValue = child1.propertyParameterValue;
    child2.propertyParameterMinValue = (parseFloat(child1.propertyParameterValue) + 1).toString();
    child2.propertyParameterValue = (parseFloat(child1.propertyParameterValue) + 4).toString();
    this.changeStatus(child1, true);
  }

  public updateSessionParameter(parameter: Parameter) {
    this._sessionParameter.updateParameterOnList(this.propertyId, parameter);
  }

  public isGroupChilden(groupId) {
    return this._parameterChildAgeRange.isGroupChilden(groupId);
  }

  public childAgeChange(value, parameter) {
    if (value < 0) {
      value = 0;
    }
    if (parameter.applicationParameterId == ParameterTypeEnum.ChildAgeRange1) {
      const parameter2 = this._parameterChildAgeRange.getParameterByType(this.parametersGroupList, ParameterTypeEnum.ChildAgeRange2);
      if (parameter2.isActive) {
        parameter2.propertyParameterMinValue = (parseInt(value, 0) + 1).toString();
        this.validadeMinValues(parameter2);
      }
    }
    if (parameter.applicationParameterId == ParameterTypeEnum.ChildAgeRange2) {
      const parameter1 = this._parameterChildAgeRange.getParameterByType(this.parametersGroupList, ParameterTypeEnum.ChildAgeRange1);
      const parameter3 = this._parameterChildAgeRange.getParameterByType(this.parametersGroupList, ParameterTypeEnum.ChildAgeRange3);

      parameter1.propertyParameterMaxValue = (parseInt(value, 0) - 1).toString();
      this.validadeMinValues(parameter1);
      if (parameter3.isActive) {
        parameter3.propertyParameterMinValue = (parseInt(value, 0) + 1).toString();
        this.validadeMinValues(parameter3);
      }
    }
    if (parameter.applicationParameterId == ParameterTypeEnum.ChildAgeRange3) {
      const parameter2 = this._parameterChildAgeRange.getParameterByType(this.parametersGroupList, ParameterTypeEnum.ChildAgeRange2);
      parameter2.propertyParameterMaxValue = (parseInt(value, 0) - 1).toString();
      this.validadeMinValues(parameter2);
    }
  }

  private validadeMinValues(parameter: Parameter) {
    if (parseInt(parameter.propertyParameterMinValue, 0) < 0) {
      parameter.propertyParameterMinValue = '0';
    }
    if (parseInt(parameter.propertyParameterValue, 0) < 0) {
      parameter.propertyParameterValue = '0';
    }
    if (parseInt(parameter.propertyParameterMaxValue, 0) < 0) {
      parameter.propertyParameterMaxValue = '0';
    }
  }

  public validateAgeRange(value, parameter: Parameter) {
    return this._parameterChildAgeRange.validateAgeRange(value, parameter);
  }

  public fieldMustBeDisabled(param) {
    return this._parameterService.fieldMustBeDisabled(param);
  }
}
