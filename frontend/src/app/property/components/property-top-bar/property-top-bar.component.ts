import { Component, ElementRef, HostListener, Inject, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { APP_NAME, LocalUserManagerService, PropertyStorageService, User } from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { UserResource } from 'app/shared/resources/user/user.resource';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { InternationalizationService } from 'app/shared/services/shared/internationalization.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import * as moment from 'moment';
import { startWith } from 'rxjs/operators';
import { Property } from 'app/shared/models/property';
import { PropertyService } from 'app/shared/services/property/property.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-property-top-bar',
  templateUrl: './property-top-bar.component.html',
  styleUrls: ['./property-top-bar.component.scss']
})
export class PropertyTopBarComponent implements OnInit, OnChanges {
  public propertyId: number;
  public propertyName: string;
  public user: User;
  public languageList: any[];
  public languageFlag: string;
  public propertyList: Array<Property>;
  public currentProperty: Property;
  public systemDate: string;
  public openProfile = false;
  public popoverVisible = false;
  public popoverCompanyVisible = false;
  public panelVisible = false;

  @HostListener('document:click', ['$event'])
  clickout(event) {
      switch (event.target.closest('div').className) {
        case 'user-language__selected':
            this.openProfile = false;
            this.popoverCompanyVisible = false;
            this.popoverVisible = true;
          break;
        case 'item user-company__selected active':
            this.openProfile = false;
            this.popoverVisible = false;
            this.popoverCompanyVisible = true;
          break;
        case 'clickable profile active':
              this.popoverVisible = false;
              this.popoverCompanyVisible = false;
              this.openProfile = true;
            break;
        default:
            this.popoverCompanyVisible = false;
            this.openProfile = false;
            this.popoverVisible = false;
          break;
      }
  }


  constructor(
    @Inject(APP_NAME) private appName,
    public activateRoute: ActivatedRoute,
    public router: Router,
    private propertyResource: PropertyResource,
    private userResource: UserResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private internationalizationService: InternationalizationService,
    private userManager: LocalUserManagerService,
    private propertyManager: PropertyStorageService,
    private dateService: DateService,
    private sessionParameterService: SessionParameterService,
    private propertyStorageService: PropertyStorageService,
    private propertyService: PropertyService,
    private elementRef: ElementRef,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnChanges() {
    this.propertyId = this.activateRoute.snapshot.params.property;
    this.setPropertyName();
    this.setProperty();
  }

  ngOnInit() {
    this.userManager.localUser$
      .subscribe(() => {
        const user = this.userManager.getUser(this.appName);
        this.user = {
          ...user,
          nickName: user.nickName && user.nickName.trim()
        };
      });

    this.propertyId = this.activateRoute.snapshot.params.property;
    this.setPropertyName();
    this.setLanguageFlag();
    this.getPropertyLanguages();
    this.getSystemDate();
    this.setProperty();
  }

  private getPropertyLanguages() {
    this.propertyResource.getPropertyLanguages().subscribe(data => {
      if (data.items) {
        this.languageList = data.items.map(item => {
          item.value = this.internationalizationService.getCountry(item.value);
          return item;
        });
      }
    });
  }

  setPropertyName() {
    const property = this.propertyManager.getProperty(this.appName, this.propertyId);
    if (property) {
      this.propertyName = property.name;
    }
  }

  public setProperty() {
   this.propertyList = this.propertyStorageService.getPropertyList(this.appName);
    this.currentProperty = this.propertyStorageService.getCurrentProperty();
    this.panelVisible = !!(this.propertyList && this.propertyList.length && this.currentProperty);
  }

  public setPropertyToken(property: Property) {
    this.propertyService.authenticateOnProperty(this.appName, property)
      .subscribe( () => {
        this.goToPage(property.id);
      });
  }

  private goToPage(id: number) {
    const urlAvaibilityGrid = '/p/' + id + '/dashboard';
    this.propertyId = id;
    this.setPropertyName();
    this.document.location.href = urlAvaibilityGrid ;
  }

  private setLanguageFlag() {
    if (this.user && this.user.preferredLanguage) {
      this.languageFlag = this.internationalizationService.getCountry(this.user.preferredLanguage);
    }
  }

  public updateUserLanguage(newLanguage: string) {
    if (this.user) {
      this.changeLanguageconfig(this.user, newLanguage);
      this.updateSystemDateLanguage(newLanguage);
    }
  }

  public updateSystemDateLanguage(userLanguage: string) {
    moment.locale(userLanguage);
    this.getSystemDate();
  }

  public updateUserLocalStorage(user: User, newLanguage: string) {
    user.preferredLanguage = newLanguage;
    user.preferredCulture = newLanguage;
    this.userManager.setUser(this.appName, user);
  }

  private async changeLanguageconfig(user: User, newLanguage: string) {
    this.userResource.updateLanguage(user.id, newLanguage)
      .subscribe(() => {
          this.updateUserLocalStorage(user, newLanguage);
          this.internationalizationService.setLanguageNavigator(newLanguage);
          this.languageFlag = this.internationalizationService.getCountry(newLanguage);
          this.toasterEmitService.emitChange(
            SuccessError.success,
            this.translateService.instant('variable.lbEditSuccessM', {
              labelName: this.translateService.instant('label.language')
            })
          );
        },
        () => {
          this.toasterEmitService.emitChange(
            SuccessError.success,
            this.translateService.instant('variable.lbEditFailM', {
              labelName: this.translateService.instant('label.language')
            })
          );
        });
  }

  private getSystemDate() {
    this
      .sessionParameterService
      .parameterModificationChange$
      .pipe(startWith(null))
      .subscribe( () => {
        const date = this.dateService.getSystemDateWithoutFormat(this.propertyId);

        if (date !== 'Invalid date' && !!date) {
          this.systemDate = moment(date).format('dddd, DD MMMM');
        }
      });
  }

  public goToPanel() {
    this.router.navigate(['home']);
  }
}
