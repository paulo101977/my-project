import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ImgCdnTestingModule, AcronymTestingModule, InfoTooltipModule } from '@inovacao-cmnet/thx-ui';
import {
  configureAppName,
  LocalUserManagerService,
  PropertyStorageService,
  User,
  AdminApiTestingService,
  AdminApiService,
  APP_NAME
} from '@inovacaocmnet/thx-bifrost';
import { getUserData } from 'app/shared/mock-data/user.data';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { UserResource } from 'app/shared/resources/user/user.resource';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { InternationalizationService } from 'app/shared/services/shared/internationalization.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { configureTestSuite } from 'ng-bullet';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../testing';

import { PropertyTopBarComponent } from './property-top-bar.component';
import * as moment from 'moment';


describe('PropertyTopBarComponent', () => {
  let component: PropertyTopBarComponent;
  let fixture: ComponentFixture<PropertyTopBarComponent>;
  const resultLanguages = {
    next: false,
    total: 2,
    items: [
      {key: 'en-us', value: 'en-us'},
      {key: 'pt-br', value: 'pt-br'}
    ]
  };

  const propertyResourceProvider = createServiceStub(PropertyResource, {
    getPropertyLanguages(): Observable<any> { return of(null); }
  });

  const userResourceProvider = createServiceStub(UserResource, {
    updateLanguage(userId: string, culture: string): Observable<any> { return of(null); }
  });

  const toasterEmitProvider = createServiceStub(ToasterEmitService, {
    emitChange(isSuccess: SuccessError, message: string) {}
  });

  const internationalizationProvider = createServiceStub(InternationalizationService, {
    getCountry(language: string): string { return 'pt'; },
    setLanguageNavigator(newLanguage: string) {}
  });

  const localUserManagerProvider = createServiceStub(LocalUserManagerService, {
    getUser(appName: string): User {
      return <User>{
        preferredLanguage: 'pt-br',
        preferredCulture: 'BR',
        id: '1',
        photoUrl: 'my-photo'
      };
    },
    setUser(appName: string, user: User): void {},
    localUser$: of()
  });

  const propertyStorageProvider = createServiceStub(PropertyStorageService, {
    getProperty(appName: string, propertyId: number): any { return null; },
    getCurrentProperty (): any { return null; },
    getPropertyList (appName: string): any {
      return '';
    }
  });

  const dateProvider = createServiceStub(DateService, {
    getSystemDateWithoutFormat(propertyId: number): string { return null; }
  });

  const sessionParameterProvider = createServiceStub(SessionParameterService, {
    parameterModificationChange$: of(null)
  });

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        declarations: [PropertyTopBarComponent],
        imports: [
          TranslateTestingModule,
          RouterTestingModule,
          ImgCdnTestingModule,
          AcronymTestingModule,
          InfoTooltipModule
        ],
        providers: [
          configureAppName('APP'),
          propertyResourceProvider,
          userResourceProvider,
          toasterEmitProvider,
          internationalizationProvider,
          localUserManagerProvider,
          propertyStorageProvider,
          dateProvider,
          sessionParameterProvider,
          AdminApiTestingService,
          {provide: AdminApiService, useExisting: AdminApiTestingService},
          { provide: APP_NAME, useValue: {} }
        ],
        schemas: [NO_ERRORS_SCHEMA]
      });

    const store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      }
    };

    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);

    localStorage.setItem('user', JSON.stringify(getUserData()));

    fixture = TestBed.createComponent(PropertyTopBarComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create PropertyTopBarComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should get Languages from Property', fakeAsync(() => {
    spyOn(component['propertyResource'], 'getPropertyLanguages')
      .and.returnValue(of(resultLanguages));

    component['getPropertyLanguages']();
    tick();

    expect(component.languageList).toEqual(resultLanguages.items);
  }));

  it('should setLanguageFlag', () => {
    component.user = <User>{
      preferredLanguage: 'EN-us'
    };
    spyOn(component['internationalizationService'], 'getCountry')
      .and.returnValue('us');

    component['setLanguageFlag']();

    expect(component.languageFlag).toEqual('us');
  });

  it('should updateUserLanguage', () => {
    const newLanguage = 'pt-br';
    spyOn<any>(component, 'changeLanguageconfig').and.callFake( () => {} );
    spyOn<any>(component, 'updateSystemDateLanguage');

    component.updateUserLanguage(newLanguage);

    expect(component['changeLanguageconfig']).toHaveBeenCalledWith(component.user, newLanguage);
    expect(component['updateSystemDateLanguage']).toHaveBeenCalledWith(newLanguage);
  });

  it('should updateSystemDateLanguage', () => {
    const newLanguage = 'pt-br';

    spyOn<any>(moment, 'locale');
    spyOn<any>(component, 'getSystemDate');

    component.updateSystemDateLanguage(newLanguage);

    expect(component['getSystemDate']).toHaveBeenCalled();
    expect(moment['locale']).toHaveBeenCalledWith(newLanguage);
  });

  it('should updateUserLocalStorage', () => {
    const newLanguage = 'pt-br';
    const userData = getUserData();
    spyOn(component['userManager'], 'setUser').and.callFake(() => {});

    component.updateUserLocalStorage(userData, newLanguage);

    expect(component['userManager'].setUser).toHaveBeenCalledWith(component['appName'], userData);
  });

  it('should changeLanguageconfig', fakeAsync(() => {
    const newLanguage = 'pt-br';
    const userData = getUserData();
    spyOn(component, 'updateUserLocalStorage');
    spyOn(component['internationalizationService'], 'setLanguageNavigator');
    spyOn(component['userResource'], 'updateLanguage').and.returnValue(of(true));
    spyOn(component['toasterEmitService'], 'emitChange');
    spyOn(component['internationalizationService'], 'getCountry')
      .and.returnValue('br');

    component['changeLanguageconfig'](userData, newLanguage);
    tick();

    expect(component.updateUserLocalStorage).toHaveBeenCalledWith(userData, newLanguage);
    expect(component['internationalizationService'].setLanguageNavigator).toHaveBeenCalledWith(newLanguage);
    expect(component.languageFlag).toEqual('br');
    expect(component['userResource'].updateLanguage).toHaveBeenCalledWith(userData.id, newLanguage);
    expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
      SuccessError.success,
      component['translateService'].instant('variable.lbEditSuccessM', {
        labelName: component['translateService'].instant('label.language')
      })
    );
  }));
});
