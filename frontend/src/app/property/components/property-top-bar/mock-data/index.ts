import { Property } from 'app/shared/models/property';

export class Index {
  public static readonly propertyList = <Property[]>[
    {
      id: 1,
      name: 'Hotel Totvs Resorts Rio de Janeiro',
      propertyUId: '',
      propertyType: 1,
      brand: null,
      brandId: 1,
      chainId: 1,
      company: null,
      locationList: [],
      contactList: [],
      documentList: [],
      totalOfRooms: 0,
      cnpj: '',
      inscEst: '',
      inscMun: '',
      contactPhone: '',
      contactWebSite: '',
      shortName: '',
      tradeName: '',
      propertyStatusId: 1,
      preferredLanguage: '',
      preferredCulture: ''
    },
    {
      name: 'Hotel portugal',
      propertyUId: '',
      propertyType: 2,
      brand: null,
      brandId: 2,
      chainId: 2,
      company: null,
      locationList: [],
      contactList: [],
      documentList: [],
      totalOfRooms: 0,
      cnpj: '',
      inscEst: '',
      inscMun: '',
      contactPhone: '',
      contactWebSite: '',
      shortName: '',
      tradeName: '',
      propertyStatusId: 1,
      preferredLanguage: '',
      preferredCulture: '',
    }
  ];
}
