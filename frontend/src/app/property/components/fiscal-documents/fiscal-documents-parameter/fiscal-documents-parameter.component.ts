import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../../shared/models/button-config';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { ParameterDocumentsResource } from '../../../../shared/resources/parameter-documents/parameter-documents-resource';

@Component({
  selector: 'app-fiscal-documents-parameter',
  templateUrl: './fiscal-documents-parameter.component.html',
  styleUrls: ['./fiscal-documents-parameter.component.css'],
})
export class FiscalDocumentsParameterComponent implements OnInit {
  // General
  public propertyId;
  number;

  // Table
  public pageItemsList: Array<any>;
  public columns: Array<any>;
  public itemsOptions: Array<any>;
  public showTable = false;

  // Config
  public newDocumentButtonConfig: ButtonConfig;

  constructor(
    private translateService: TranslateService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private sharedService: SharedService,
    private parameterDocumentsResource: ParameterDocumentsResource,
  ) {}

  // Lifecycle
  ngOnInit() {
    this.propertyId = this.activatedRouter.snapshot.params.property;

    this.setTableConfig();
    this.setButtonConfig();
    this.loadDocuments();
  }

  // Actions
  public newDocument() {
    this.router.navigate(['new'], { relativeTo: this.activatedRouter });
  }

  public editDocument(item) {
    this.router.navigate(['edit', item.id], { relativeTo: this.activatedRouter });
  }

  // Config & setup
  public loadDocuments() {
    this.parameterDocumentsResource.getAllWithoutReference().subscribe(result => {
      this.pageItemsList = [...result.items];
    });
  }

  public setTableConfig() {
    this.itemsOptions = [{title: 'action.edit', callbackFunction: this.editDocument.bind(this)}];
    this.columns = [
      {name: 'label.model', prop: 'billingInvoiceModeName'},
      {name: 'label.series', prop: 'billingInvoicePropertySeries'}
    ];
    this.showTable = true;
  }

  public setButtonConfig() {
    this.newDocumentButtonConfig = this.sharedService.getButtonConfig(
      'newDocument',
      this.newDocument.bind(this),
      'label.newDocument',
      ButtonType.Secondary,
      ButtonSize.Small,
    );
  }
}
