import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { FiscalDocumentsParameterComponent } from './fiscal-documents-parameter.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

xdescribe('FiscalDocumentsParameterComponent', () => {
  let component: FiscalDocumentsParameterComponent;
  let fixture: ComponentFixture<FiscalDocumentsParameterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), SharedModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [FiscalDocumentsParameterComponent],
      providers: [ InterceptorHandlerService ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiscalDocumentsParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
