import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import {
  FiscalDocumentsParameterCreateEditComponent
} from './fiscal-documents-parameter-create-edit/fiscal-documents-parameter-create-edit.component';
import { FiscalDocumentsParameterComponent } from './fiscal-documents-parameter/fiscal-documents-parameter.component';
import {
  FiscalDocumentsAssociateItemsBoxComponent
} from './fiscal-documents-associate-items-box/fiscal-documents-associate-items-box.component';
import {
  FiscalDocumentsAssociateItemsModalComponent
} from './fiscal-documents-associate-items-modal/fiscal-documents-associate-items-modal.component';
import { EditorModule } from 'primeng/editor';
import { FiscalDocumentsRoutingModule } from './fiscal-documents-routing.module';

@NgModule({
  imports: [CommonModule, SharedModule, HttpClientModule, ReactiveFormsModule, EditorModule, FiscalDocumentsRoutingModule],
  declarations: [
    FiscalDocumentsParameterComponent,
    FiscalDocumentsParameterCreateEditComponent,
    FiscalDocumentsAssociateItemsBoxComponent,
    FiscalDocumentsAssociateItemsModalComponent,
  ],
})
export class FiscalDocumentsModule {}
