import { PropertyService } from '@app/shared/services/property/property.service';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SearchableItems } from 'app/shared/models/filter-search/searchable-item';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { TableRowTypeEnum } from 'app/shared/models/table-row-type-enum';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { ParameterDocumentBillingItemDto } from 'app/shared/models/dto/rps/parameter-document-billing-item-dto';
import { BillingInvoicePropertySupportedTypeDto } from 'app/shared/models/dto/rps/billing-invoice-property-supported-type-dto';
import * as _ from 'lodash';
import { CountryCodeEnum } from '@app/shared/models/country-code-enum';

@Component({
  selector: 'fiscal-documents-associate-items-box',
  templateUrl: './fiscal-documents-associate-items-box.component.html',
  styleUrls: ['./fiscal-documents-associate-items-box.component.css'],
})
export class FiscalDocumentsAssociateItemsBoxComponent implements OnInit {
  public readonly TYPE_SERVICE = 1;
  public readonly TYPE_PDV = 4;
  public readonly MODAL_ASSOCIATE_ITENS = 'modal_associate_itens';

  public countryCodeEnumReference = CountryCodeEnum;

  public propertyCountryCode: string;

  @ViewChild('variationCell') variationCellTemplate: TemplateRef<any>;
  @ViewChild('serviceType') serviceTypeCellTemplate: TemplateRef<any>;

  @Input()
  set associatedItemsList(list: Array<BillingInvoicePropertySupportedTypeDto>) {
    if (list) {
      this.setPageItens(list);
      if (this.searchBarConfig && !this.searchBarConfig.items) {
        this.searchBarConfig.items = this.associatedItemsList;
      }
    }
  }

  get associatedItemsList(): Array<BillingInvoicePropertySupportedTypeDto> {
    return this.pageItemsList;
  }
  @Output() associatedItemsListChange = new EventEmitter();

  @Input() countrySubdivisionList: Array<SelectObjectOption>;
  @Input() billingItenList: Array<ParameterDocumentBillingItemDto>;

  // Table
  public columns: Array<any>;
  public pageItemsList: Array<BillingInvoicePropertySupportedTypeDto>;

  // General
  public associateButtonConfig: ButtonConfig;
  public showTable = false;
  public searchBarConfig: SearchableItems;

  constructor(
    private translateService: TranslateService,
    private sharedService: SharedService,
    private modalToggleService: ModalEmitToggleService,
    private propertyService: PropertyService
  ) {}

  // Lifecycle
  ngOnInit() {
    this.setPropertyCountryCode();
    this.setTableColumns();
    this.setSearchConfig();
    this.setButtonConfig();
  }

  // Actions
  private setPageItens(list: Array<BillingInvoicePropertySupportedTypeDto>) {
    this.pageItemsList = [...list];
    this.associatedItemsListChange.emit(this.pageItemsList);
  }

  public prepareToAssociate() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_ASSOCIATE_ITENS);
  }

  public associateItens(itens: Array<BillingInvoicePropertySupportedTypeDto>) {
    this.closeModal();
    if (!this.associatedItemsList) {
      this.associatedItemsList = [];
    }
    itens.forEach(item => {
      const element = _.find(this.pageItemsList, { billingItemName: item.billingItemName });
      if (element) {
        this.associatedItemsList.splice(this.pageItemsList.indexOf(element), 1, item);
      } else {
        this.associatedItemsList.push(item);
      }
    });
    this.associatedItemsList = [...this.associatedItemsList];
    this.searchBarConfig.items = this.associatedItemsList;
  }

  public closeModal() {
    this.modalToggleService.emitToggleWithRef(this.MODAL_ASSOCIATE_ITENS);
  }

  public changeCodService(value, element) {
    element.countrySubdvisionServiceId = value;
  }

  public updateList(filteredList) {
    this.setPageItens(filteredList);
  }

  // Config
  public setButtonConfig() {
    this.associateButtonConfig = this.sharedService.getButtonConfig(
      'associateItem',
      this.prepareToAssociate.bind(this),
      'label.associateItem',
      ButtonType.Secondary,
    );
    this.associateButtonConfig.extraClass = 'thf-u-width-flex';
  }

  public setTableColumns() {
    this.columns = [];

    this.translateService.get('label.type').subscribe(value => {
      this.columns.push({ name: value, prop: 'billingItemTypeId', cellTemplate: this.serviceTypeCellTemplate });
    });
    this.translateService.get('label.item').subscribe(value => {
      this.columns.push({ name: value, prop: 'billingItemName' });
    });

    if (this.propertyCountryCode == CountryCodeEnum.BR) {
      this.translateService.get('label.codService').subscribe(value => {
        this.columns.push({
          name: value,
          prop: 'coutrySubdivisionSelected',
          cellTemplate: this.variationCellTemplate,
          cellClass: 'thf-u-padding-vertical--05',
        });
      });
    }
    this.showTable = true;
  }

  public setSearchConfig() {
    this.searchBarConfig = new SearchableItems();
    this.searchBarConfig.items = this.pageItemsList;
    this.searchBarConfig.tableRowTypeEnum = TableRowTypeEnum.ParameterDocumentItens;
    this.searchBarConfig.placeholderSearchFilter = 'placeholder.searchByTypeOrItem';
  }

  private setPropertyCountryCode() {
    this.propertyCountryCode = this.propertyService.getPropertyCountryCode();
  }
}
