import { PropertyService } from './../../../../shared/services/property/property.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { FiscalDocumentsAssociateItemsBoxComponent } from './fiscal-documents-associate-items-box.component';
import { RouterTestingModule} from '@angular/router/testing';
import { createServiceStub } from 'testing/create-service-stub';

describe('FiscalDocumentsAssociateItemsBoxComponent', () => {
  let component: FiscalDocumentsAssociateItemsBoxComponent;
  let fixture: ComponentFixture<FiscalDocumentsAssociateItemsBoxComponent>;

  const propertyServiceStub = createServiceStub(PropertyService, {
    getPropertyCountryCode(): string { return 'BR'; }
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), SharedModule, RouterTestingModule],
      declarations: [FiscalDocumentsAssociateItemsBoxComponent],
      providers: [
        propertyServiceStub
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiscalDocumentsAssociateItemsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
