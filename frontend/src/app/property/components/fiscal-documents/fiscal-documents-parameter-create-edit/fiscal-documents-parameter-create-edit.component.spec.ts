import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { FiscalDocumentsParameterCreateEditComponent } from './fiscal-documents-parameter-create-edit.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonModule } from '@angular/common';
import { EditorModule } from 'primeng/editor';

xdescribe('FiscalDocumentsParameterCreateEditComponent', () => {
  let component: FiscalDocumentsParameterCreateEditComponent;
  let fixture: ComponentFixture<FiscalDocumentsParameterCreateEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), SharedModule, ReactiveFormsModule, FormsModule, RouterTestingModule, CommonModule, EditorModule],
      declarations: [FiscalDocumentsParameterCreateEditComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiscalDocumentsParameterCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
