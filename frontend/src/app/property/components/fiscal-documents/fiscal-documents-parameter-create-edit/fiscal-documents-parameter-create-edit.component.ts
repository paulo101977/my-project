import { PropertyService } from '@app/shared/services/property/property.service';
import { Component, OnInit } from '@angular/core';
import { ConfigHeaderPageNew } from '../../../../shared/components/header-page-new/config-header-page-new';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectObjectOption } from '../../../../shared/models/selectModel';
import { ActivatedRoute, Router } from '@angular/router';
import { ParameterDocumentsResource } from '../../../../shared/resources/parameter-documents/parameter-documents-resource';
import { ParameterDocumentDto } from '../../../../shared/models/dto/rps/parameter-document-dto';
import { CommomService } from '../../../../shared/services/shared/commom.service';
import { LoadPageEmitService } from '../../../../shared/services/shared/load-emit.service';
import { ShowHide } from '../../../../shared/models/show-hide-enum';
import { ParameterDocumentBillingItemDto } from '../../../../shared/models/dto/rps/parameter-document-billing-item-dto';
import { ButtonConfig, ButtonType, ButtonSize } from '../../../../shared/models/button-config';
import { SharedService } from '../../../../shared/services/shared/shared.service';
import { BillingInvoicePropertySupportedTypeDto } from '../../../../shared/models/dto/rps/billing-invoice-property-supported-type-dto';
import { ParameterDocumentService } from '../../../../shared/services/document-parameter/parameter-document.service';
import { ToasterEmitService } from '../../../../shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { SuccessError } from '../../../../shared/models/success-error-enum';
import { ModalEmitToggleService } from '../../../../shared/services/shared/modal-emit-toggle.service';
import { ErrorMessageHandlerAPI } from '../../../../shared/models/error-message/error-message-handler-api';
import { BillingAccountItemTypeEnum } from '@app/shared/models/billing-account/billing-account-item-type-enum';
import { BillingInvoiceTypeEnum } from '@app/shared/models/dto/invoice/billing-invoice-type-enum';
import { ValidatorFormService } from '@app/shared/services/shared/validator-form.service';

@Component({
  selector: 'app-fiscal-documents-parameter-create-edit',
  templateUrl: './fiscal-documents-parameter-create-edit.component.html',
  styleUrls: ['./fiscal-documents-parameter-create-edit.component.css'],
})
export class FiscalDocumentsParameterCreateEditComponent implements OnInit {
  public readonly MODAL_ATTENTION = 'modal-confirmation-add-params';
  // Header
  private configHeaderPage: ConfigHeaderPageNew;

  // Form
  public parameterForm: FormGroup;

  // Info
  public propertyId: number;
  public billingInvoiceId: number;
  public countryServiceList: Array<SelectObjectOption>;
  public modelList: Array<SelectObjectOption>;
  public parameterDocument: ParameterDocumentDto;
  public billingItenList: Array<ParameterDocumentBillingItemDto>;
  public billingInvoicePropertySupportedTypeList: Array<BillingInvoicePropertySupportedTypeDto> = [];

  // Utils
  public isEdit: boolean;
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;
  public downloadButtonConfig: ButtonConfig;
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;
  public model: string;
  public showFieldDaysForCancel: boolean;
  public showDownloadSetup: boolean;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private activeRoute: ActivatedRoute,
              private commomService: CommomService,
              private parameterDocumentsResource: ParameterDocumentsResource,
              private loader: LoadPageEmitService,
              private sharedService: SharedService,
              private pameterDocumentService: ParameterDocumentService,
              private toastService: ToasterEmitService,
              private translateService: TranslateService,
              private modalService: ModalEmitToggleService,
              private propertyService: PropertyService) {
  }

  // Lifecycle
  ngOnInit() {
    this.configHeader();
    this.setFormConfig();
    this.setButtonConfig();
    this.billingInvoiceId = this.activeRoute.snapshot.params.billingInvoiceId;
    this.propertyId = this.activeRoute.snapshot.params.property;
    if (this.billingInvoiceId) {
      this.isEdit = true;
      this.loadBillingInvoice();
    } else {
      this.loadInfo();
    }
    const countryCode = this.propertyService.getPropertyCountryCode();
  }

  // Actions
  public cancel() {
    this.isEdit
      ? this.router.navigate(['../..'], {relativeTo: this.activeRoute})
      : this.router.navigate(['../'], {relativeTo: this.activeRoute});
  }

  public confirm() {
    this.modalService.emitToggleWithRef(this.MODAL_ATTENTION);
  }

  public cancelModal() {
    this.modalService.emitToggleWithRef(this.MODAL_ATTENTION);
  }

  public confirmModal() {
    const parameterDocument = this.pameterDocumentService.mapper(
      this.propertyId,
      this.parameterForm,
      this.billingInvoicePropertySupportedTypeList,
      this.parameterDocument,
    );
    this.parameterDocumentsResource.save(parameterDocument).subscribe(
      result => {
        this.cancel();
        if (this.isEdit) {
          this.toastService.emitChange(SuccessError.success, this.translateService.instant('alert.fiscalDocumentsSuccessEdit'));
        } else {
          this.toastService.emitChange(SuccessError.success, this.translateService.instant('alert.fiscalDocumentsSuccessInsert'));
        }
      },
      error => {
        if (this.isEdit) {
          this.toastService.emitChange(SuccessError.error, this.translateService.instant('alert.fiscalDocumentsErrorEdit'));
        } else {
          this.toastService.emitChange(SuccessError.error, new ErrorMessageHandlerAPI(error).getErrorMessage());
        }
      },
    );
  }

  // Config
  public loadInfo() {
    this.loader.emitChange(ShowHide.show);
    this.parameterDocumentsResource.getNewInfo(this.propertyId).subscribe(
      result => {
        this.modelList = this.commomService.toOption(result[0], 'id', 'description');
        this.countryServiceList = this.commomService.toOption(result[1], 'id', 'code');
        this.billingItenList = result[2].items;
        this.loader.emitChange(ShowHide.hide);
        this.setModelField();
      },
      error => {
        this.loader.emitChange(ShowHide.hide);
      },
    );
  }

  private setModelField() {
    if (this.modelList && this.modelList.length > 0) {
      this.parameterForm.get('model').setValue(this.modelList[0].key);
    }
  }

  public loadBillingInvoice() {
    this.loader.emitChange(ShowHide.show);
    this.parameterDocumentsResource.getEditInfo(this.propertyId, this.billingInvoiceId).subscribe(
      result => {
        this.modelList = this
          .commomService
          .toOption(result[0], 'id', 'description');
        this.parameterDocument = result[1];
        this.billingInvoicePropertySupportedTypeList = this.parameterDocument.billingInvoicePropertySupportedTypeList;
        this.countryServiceList = this
          .commomService
          .toOption(result[2], 'id', 'code');
        this.billingItenList = result[3].items;
        this.populateFields(this.parameterDocument);
        this.loader.emitChange(ShowHide.hide);
      },
      error => {
        this.loader.emitChange(ShowHide.hide);
      },
    );
  }

  public configHeader() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  public setFormConfig() {
    this.parameterForm = this.formBuilder.group({
      model: [null, Validators.required],
      serie: [null, Validators.required],
      lastNum: [null, Validators.required],
      email: [null, Validators.required],
      integration: null,
      obs: [null],
      allowsCancel: null,
      daysForCancel: [null],
      childSerie: [null],
      childLastNum: null
    });

    this.setListeners();
  }

  private setListeners() {
    this.parameterForm.valueChanges
      .subscribe(() => {
        this.confirmButtonConfig.isDisabled = this.parameterForm.invalid;
    });

    this.parameterForm.get('model').valueChanges.subscribe(model => {
      if (model && this.modelList && this.modelList.length > 0) {
        this.model = this.modelList.find(m => m.key == model).value;
        this.showDownloadSetup = model == BillingInvoiceTypeEnum.SAT;

        if (this.parameterForm.get('model').value == BillingInvoiceTypeEnum.Factura) {
          this.setValidatorsInChildSerieAndLastNum();
        } else if (this.parameterForm.get('model').value == BillingInvoiceTypeEnum.NFSE) {
          this.parameterForm.get('obs').setValue(this.translateService.instant('text.rpsObs', {
            labelName: this.model
          }));
          this.clearValidatorsInChildSerieAndLastNum();
        } else {
          this.parameterForm.get('obs').setValue('');
          this.clearValidatorsInChildSerieAndLastNum();
        }
      }
    });

    this.parameterForm.get('allowsCancel').valueChanges.subscribe(allowsCancel => {
      if (allowsCancel) {
        this.setValidatorsInDaysForCancel();
        this.showFieldDaysForCancel = true;
      } else {
        this.clearValidatorsInDaysForCancel();
        this.showFieldDaysForCancel = false;
      }
    });
  }

  private setValidatorsInChildSerieAndLastNum() {
    this.parameterForm.get('childSerie').setValidators([Validators.required]);
    this.parameterForm.get('childSerie').updateValueAndValidity();

    this.parameterForm.get('childLastNum').setValidators([Validators.required]);
    this.parameterForm.get('childLastNum').updateValueAndValidity();
  }

  private clearValidatorsInChildSerieAndLastNum() {
    this.parameterForm.get('childSerie').reset();
    this.parameterForm.get('childSerie').clearValidators();
    this.parameterForm.get('childSerie').updateValueAndValidity();

    this.parameterForm.get('childLastNum').reset();
    this.parameterForm.get('childLastNum').clearValidators();
    this.parameterForm.get('childLastNum').updateValueAndValidity();
  }

  private setValidatorsInDaysForCancel() {
    this.parameterForm.get('daysForCancel').setValidators([Validators.required, Validators.pattern('[0-9]+')]);
    this.parameterForm.get('daysForCancel').updateValueAndValidity();
  }

  private clearValidatorsInDaysForCancel() {
    this.parameterForm.get('daysForCancel').reset();
    this.parameterForm.get('daysForCancel').clearValidators();
    this.parameterForm.get('daysForCancel').updateValueAndValidity();
  }

  public populateFields(parameterDocument: ParameterDocumentDto) {
    if (this.parameterForm) {
      this.parameterForm.patchValue({
        model: parameterDocument.billingInvoiceModelId,
        serie: parameterDocument.billingInvoicePropertySeries,
        lastNum: parameterDocument.lastNumber,
        email: parameterDocument.emailAlert,
        integration: parameterDocument.isIntegrated,
        obs: parameterDocument.description,
        childSerie: parameterDocument.childBillingInvoicePropertySeries,
        childLastNum: parameterDocument.childLastNumber,
        allowsCancel: parameterDocument.allowsCancel,
        daysForCancel: parameterDocument.daysForCancel
      });
    }
  }

  public setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancel-document-param',
      this.cancel.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'save-document-param',
      this.confirm.bind(this),
      'action.confirm',
      ButtonType.Primary,
    );
    this.downloadButtonConfig = this.sharedService.getButtonConfig(
      'download-document',
      this.downloadSetup.bind(this),
      'action.downloadSetup',
      ButtonType.Primary,
      ButtonSize.Normal
    );
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancelSave',
      this.cancelModal.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirmSave',
      this.confirmModal.bind(this),
      'action.confirm',
      ButtonType.Primary,
    );
  }

  public downloadSetup() {
    this.parameterDocumentsResource.getDownloadSetup().subscribe(res => location.href = res.linkDownload);
  }

  public showCreditNote(): boolean {
    return this.parameterForm.get('model').value == BillingInvoiceTypeEnum.Factura;
  }
}
