import { CountryCodeEnum } from 'app/shared/models/country-code-enum';
import { PropertyService } from '@app/shared/services/property/property.service';
import { Component, DoCheck, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { DataTableGlobals } from 'app/shared/models/DataTableGlobals';
import { ParameterDocumentBillingItemDto } from 'app/shared/models/dto/rps/parameter-document-billing-item-dto';
import { BillingInvoicePropertySupportedTypeDto } from 'app/shared/models/dto/rps/billing-invoice-property-supported-type-dto';
import * as _ from 'lodash';

@Component({
  selector: 'fiscal-documents-associate-items-modal',
  templateUrl: './fiscal-documents-associate-items-modal.component.html',
  styleUrls: ['./fiscal-documents-associate-items-modal.component.css'],
})
export class FiscalDocumentsAssociateItemsModalComponent implements OnInit, DoCheck {
  public readonly TYPE_SERVICE = 1;
  public readonly TYPE_PDV = 4;

  @ViewChild('defaultCell') defaultCellTemplate: TemplateRef<any>;

  @Input() modalId: string;
  private _serviceList: Array<ParameterDocumentBillingItemDto>;
  @Input()
  set serviceList(list: Array<ParameterDocumentBillingItemDto>) {
    if (list) {
      this._serviceList = _.orderBy(list, ['id'], ['asc']);
    }
    if (!this.originalserviceList && this._serviceList) {
      this.originalserviceList = _.clone(this._serviceList);
    }
    this.typeChoose ? this.filterListByTypeChoose(this.typeChoose) : null;
  }
  get serviceList() {
    return this._serviceList;
  }
  @Input() countrySubdivisionList: Array<SelectObjectOption>;

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() confirm: EventEmitter<Array<BillingInvoicePropertySupportedTypeDto>> = new EventEmitter();

  // Config
  public typeServiceButtonConfig: ButtonConfig;
  public typePDVButtonConfig: ButtonConfig;
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;
  public typeChoose: number;
  public coutrySubdivisionSelected: string;

  public countryCodeEnumReference = CountryCodeEnum;
  public propertyCountryCode: string;

  // Table
  public columns: Array<any> = [];
  public itemsSelectedList: Array<ParameterDocumentBillingItemDto> = [];
  public serviceSelectedList: Array<ParameterDocumentBillingItemDto> = [];
  public pdvSelectedList: Array<ParameterDocumentBillingItemDto> = [];
  private originalserviceList: Array<ParameterDocumentBillingItemDto>;
  public showTable = false;

  constructor(
    private sharedService: SharedService,
    private modalToggle: ModalEmitToggleService,
    private translateService: TranslateService,
    public datatableGlobals: DataTableGlobals,
    private propertyService: PropertyService
  ) {}

  ngOnInit() {
    this.setPropertyCountryCode();
    this.modalToggle.changeEmitted$.subscribe(result => {
      setTimeout(() => {
        this.showTable = !this.showTable;
      }, 5);
      this.cleanFields();
    });
    this.setButtonsConfig();
    this.setTableConfig();
    this.setTypeService();
  }

  ngDoCheck(): void {
    this.setSelectedItens();
    if (this.buttonConfirmConfig) {
      if (this.serviceSelectedList.length > 0 || this.pdvSelectedList.length > 0) {
        this.buttonConfirmConfig.isDisabled = false;
      } else {
        this.buttonConfirmConfig.isDisabled = true;
      }
    }
  }

  // Actions
  public cleanFields() {
    this.itemsSelectedList = [];
    this.pdvSelectedList = [];
    this.serviceSelectedList = [];
    this.setTypeService();
  }

  public setTypeService() {
    this.typeServiceButtonConfig.isActive = true;
    this.typePDVButtonConfig.isActive = false;
    this.setSelectedItens();
    this.typeChoose = this.TYPE_SERVICE;
    this.itemsSelectedList = this.serviceSelectedList;
    this.filterListByTypeChoose(this.typeChoose);
  }

  public setTypePdv() {
    this.typePDVButtonConfig.isActive = true;
    this.typeServiceButtonConfig.isActive = false;
    this.setSelectedItens();
    this.typeChoose = this.TYPE_PDV;
    this.itemsSelectedList = this.pdvSelectedList;
    this.filterListByTypeChoose(this.typeChoose);
  }

  private setSelectedItens() {
    if (this.typeChoose == this.TYPE_PDV) {
      this.pdvSelectedList = [...this.itemsSelectedList];
     } else {
      this.serviceSelectedList = [...this.itemsSelectedList];
    }
  }

  public onCancel() {
    this.cancel.emit();
  }

  public onConfirm() {
    const mergeArray = [];
    mergeArray.push(..._.clone(this.serviceSelectedList));
    mergeArray.push(..._.clone(this.pdvSelectedList));
    const billingInvoicePropertyList = [];
    mergeArray.forEach(item => {
      const billingInvoiceProperty = new BillingInvoicePropertySupportedTypeDto();
      billingInvoiceProperty.billingItemId = item.billingItemId;
      billingInvoiceProperty.countrySubdvisionServiceId = this.coutrySubdivisionSelected;
      billingInvoiceProperty.billingItemName = item.billingItemName;
      billingInvoiceProperty.billingItemTypeId = item.billingItemTypeId;
      billingInvoicePropertyList.push(billingInvoiceProperty);
    });
    this.confirm.emit(billingInvoicePropertyList);
  }

  public setSelectedOption(item) {
    this.coutrySubdivisionSelected = item;
  }

  // Config
  private setPropertyCountryCode() {
    this.propertyCountryCode = this.propertyService.getPropertyCountryCode();
  }

  public filterListByTypeChoose(typeChoose) {
    this._serviceList = _.clone(this.originalserviceList ? this.originalserviceList
      .filter( item => item.billingItemTypeId == typeChoose) : this.originalserviceList);
  }

  public setButtonsConfig() {
    this.typeServiceButtonConfig = this.sharedService.getButtonConfig(
      'button-service-config',
      this.setTypeService.bind(this),
      'label.service',
      ButtonType.Secondary,
    );
    this.typeServiceButtonConfig.width = '110px';

    this.typePDVButtonConfig = this.sharedService.getButtonConfig(
      'button-service-config',
      this.setTypePdv.bind(this),
      'label.pos',
      ButtonType.Secondary,
    );
    this.typePDVButtonConfig.width = '110px';

    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancelModalUhType',
      this.onCancel.bind(this),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirmModalUhType',
      this.onConfirm.bind(this),
      'action.confirm',
      ButtonType.Primary,
    );
    this.buttonConfirmConfig.isDisabled = true;
  }

  public setTableConfig() {
    this.columns = [];

    this.translateService.get('label.launchItem').subscribe(value => {
      this.columns.push({ name: value, prop: 'billingItemName' });
    });
    this.translateService.get('label.model').subscribe(value => {
      this.columns.push({ name: value, prop: 'billingInvoiceModelName', cellTemplate: this.defaultCellTemplate });
    });
    this.translateService.get('label.numService').subscribe(value => {
      this.columns.push({ name: value, prop: 'billingInvoicePropertySeries', cellTemplate: this.defaultCellTemplate });
    });
    this.translateService.get('label.code').subscribe(value => {
      this.columns.push({ name: value, prop: 'code', cellTemplate: this.defaultCellTemplate });
    });
  }
}
