import { PropertyService } from './../../../../shared/services/property/property.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { FiscalDocumentsAssociateItemsModalComponent } from './fiscal-documents-associate-items-modal.component';
import { createServiceStub } from 'testing/create-service-stub';

describe('FiscalDocumentsAssociateItemsModalComponent', () => {
  let component: FiscalDocumentsAssociateItemsModalComponent;
  let fixture: ComponentFixture<FiscalDocumentsAssociateItemsModalComponent>;

  const propertyServiceStub = createServiceStub(PropertyService, {
    getPropertyCountryCode(): string { return 'BR'; }
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), SharedModule],
      declarations: [FiscalDocumentsAssociateItemsModalComponent],
      providers: [
        propertyServiceStub
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiscalDocumentsAssociateItemsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
