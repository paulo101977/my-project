import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FiscalDocumentsParameterComponent } from './fiscal-documents-parameter/fiscal-documents-parameter.component';
import {
  FiscalDocumentsParameterCreateEditComponent
} from './fiscal-documents-parameter-create-edit/fiscal-documents-parameter-create-edit.component';

export const fiscalDocumentsRoutes: Routes = [
  { path: 'parameter', component: FiscalDocumentsParameterComponent },
  { path: 'parameter/new', component: FiscalDocumentsParameterCreateEditComponent },
  { path: 'parameter/edit/:billingInvoiceId', component: FiscalDocumentsParameterCreateEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(fiscalDocumentsRoutes)],
  exports: [RouterModule],
})
export class FiscalDocumentsRoutingModule {}
