import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertiesPanelComponent } from './properties-panel.component';
import { PROPERTIES_DATA, PROPERTY_1_DATA } from 'app/property/mock-data/property-mock-data';
import { configureTestSuite } from 'ng-bullet';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { AdminApiService, configureAppName, ThexApiTestingModule } from '@inovacaocmnet/thx-bifrost';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createServiceStub } from '../../../../../testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('PropertiesPanelComponent', () => {
  let component: PropertiesPanelComponent;
  let fixture: ComponentFixture<PropertiesPanelComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [PropertiesPanelComponent],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        HttpClientTestingModule,
        ThexApiTestingModule,
      ],
      providers: [
        configureAppName('pms'),
        createServiceStub(AdminApiService)
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(PropertiesPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should getCompanyId', () => {
    spyOn(component, 'getPropertiesList');
    component.getCompanyId();
    expect(component.getPropertiesList).toHaveBeenCalled();
  });

  it('should setPropertyTokenAndRedirectAvailabilityGrid', () => {
    spyOn(component, 'setPropertyToken');
    component.setPropertyTokenAndRedirectAvailabilityGrid(PROPERTY_1_DATA);
    expect(component.setPropertyToken).toHaveBeenCalledWith(PROPERTY_1_DATA);
  });

  it('should setPropertyToken', () => {
    spyOn(component['propertyService'], 'authenticateOnProperty').and.returnValue(of(true));
    spyOn<any>(component, 'goToAvaibilityGridPage');
    component.setPropertyToken(PROPERTY_1_DATA);
    expect(component['propertyService'].authenticateOnProperty).toHaveBeenCalledWith(component['appName'], PROPERTY_1_DATA);
    expect(component['goToAvaibilityGridPage']).toHaveBeenCalledWith(PROPERTY_1_DATA);
  });

  it('should getPropertiesList', () => {
    spyOn(component.propertyResource, 'getAll').and.returnValue(of({items: PROPERTIES_DATA, hasNext: false}));
    component.getPropertiesList();
    expect(component.propertyResource.getAll).toHaveBeenCalled();
    expect(component.properties).toEqual(PROPERTIES_DATA.map((item) => ({
      ...item,
      photo: undefined
    })));
  });

});
