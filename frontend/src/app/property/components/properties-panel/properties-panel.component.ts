import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APP_NAME, PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { TokenManagerService } from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { Property } from 'app/shared/models/property';
import { PropertyStatusEnum } from 'app/shared/models/property-status-enum';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { PropertyService } from 'app/shared/services/property/property.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';

@Component({
  selector: 'app-properties-panel',
  templateUrl: './properties-panel.component.html',
  styleUrls: ['./properties-panel.component.css']
})
export class PropertiesPanelComponent implements OnInit {
  public properties: Array<Property>;
  public companyId: number;

  constructor(
    @Inject(APP_NAME) private appName,
    public route: Router,
    public propertyResource: PropertyResource,
    private tokenService: TokenManagerService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private propertyService: PropertyService,
    private propertyStorageService: PropertyStorageService
  ) {
  }

  ngOnInit() {
    this.getCompanyId();
  }

  public getCompanyId() {
    this.getPropertiesList();
  }

  public setPropertyTokenAndRedirectAvailabilityGrid(property: Property) {
    this.setPropertyToken(property);
  }

  public setPropertyToken(property: Property) {
    this.propertyService.authenticateOnProperty(this.appName, property)
      .subscribe(() => {
        if (property.propertyStatusId == PropertyStatusEnum.contract) {
          this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant('alert.propertyRegistering'));
        } else {
          this.goToAvaibilityGridPage(property);
        }
      });
  }

  private goToAvaibilityGridPage(property: Property) {
    const urlAvaibilityGrid = '/p/' + property.id + '/dashboard';
    this.route.navigate([urlAvaibilityGrid]);
  }

  public getPropertiesList() {
    this.propertyResource.getAll().subscribe(response => {
      if (response.items) {
        this.properties = response.items.map((item) => ({
          ...item,
          photo: item.photo ? `${item.photo}?_=${new Date().getTime()}` : item.photo
        }));
        this.propertyStorageService.savePropertyList(this.properties, this.appName);
      }
    });
  }

}
