import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { Subscription } from 'rxjs';
import { Property } from '../../../shared/models/property';
import { Location } from 'app/shared/models/location';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { PropertyService } from '../../../shared/services/property/property.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { PropertyResource } from '../../../shared/resources/property/property.resource';
import { BrandResource } from '../../../shared/resources/brand/brand.resource';
import { ChainResource } from '../../../shared/resources/chain/chain.resource';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { HttpService } from '../../../core/services/http/http.service';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';

@Component({
  selector: 'app-property-edit',
  templateUrl: './property-edit.component.html',
})
export class PropertyEditComponent implements OnInit, OnDestroy {
  public formGroupEditProperty: FormGroup;
  public hotelId: number;
  public showLoading: boolean;

  private contact: string;
  private document: string;

  public property: Property;
  public subscription: Subscription;
  public propertyId: number;
  public chainOptionList: Array<SelectObjectOption>;
  public brandOptionList: Array<SelectObjectOption>;

  public itemsOption: Array<any>;

  public locationEdit: Location;

  constructor(
    public translateService: TranslateService,
    public _route: ActivatedRoute,
    public route: Router,
    public propertyResource: PropertyResource,
    public brandResource: BrandResource,
    public chainResource: ChainResource,
    private formBuilder: FormBuilder,
    private toasterEmitService: ToasterEmitService,
    public loadPageEmitService: LoadPageEmitService,
    private propertyService: PropertyService,
    public httpClient: HttpService,
    private sharedService: SharedService,
    private commomService: CommomService,
  ) {}

  ngOnInit() {
    this.setVars();
    this.getAllChainOptionList();
    this.getAllBrandOptionList();
    this.propertyId = this._route.snapshot.params.property;
    this.propertyResource.getPropertyById(this.propertyId).subscribe(result => {
      this.property = result;
      this.loadFormControls();
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  setVars() {
    this.contact = 'contact';
    this.document = 'document';
    this.formGroupEditProperty = this.formBuilder.group({
      name: [{ value: '', disabled: false }],
      chain: [{ value: '', disabled: false }],
      brand: [{ value: '', disabled: false }],
      fantasyName: [{ value: '', disabled: false }],
      socialName: [{ value: '', disabled: false }],
      totalUhs: [{ value: '', disabled: true }],
      telephone: ['', []],
      homePage: ['', []],
      cnpj: [{ value: '', disabled: false }],
      stateRegistration: [{ value: '', disabled: false }],
      municipalRegistration: [{ value: '', disabled: false }],
      address: this.formBuilder.group({
        id: null,
        ownerId: GuidDefaultData,
        postalCode: [null, [Validators.required]],
        streetName: [null, [Validators.required]],
        streetNumber: [null, [Validators.required]],
        additionalAddressDetails: null,
        division: [{value: null, disabled: true}, [Validators.required]],
        subdivision: [{value: null, disabled: true}, [Validators.required]],
        neighborhood: [null, [Validators.required]],
        browserLanguage: window.navigator.language,
        latitude: null,
        longitude: null,
        countryCode: null,
        country: null,
        completeAddress: null,
        countrySubdivisionId: null,
        locationCategoryId: [null, [Validators.required]]
      }),
    });
  }

  private loadFormControls() {
    this.formGroupEditProperty.get('name').setValue(this.property.name);
    this.formGroupEditProperty.get('fantasyName').setValue(this.property.shortName);
    this.formGroupEditProperty.get('socialName').setValue(this.property.tradeName);
    this.formGroupEditProperty.get('totalUhs').setValue(this.property.totalOfRooms);
    this.formGroupEditProperty.get('telephone').setValue(this.property.contactPhone);
    this.formGroupEditProperty.get('homePage').setValue(this.property.contactWebSite);
    if (this.property.locationList && this.property.locationList.length > 0) {
      this.locationEdit = this.property.locationList[0];
    }
    this.formGroupEditProperty.get('cnpj').setValue(this.property.cnpj);
    this.formGroupEditProperty.get('stateRegistration').setValue(this.property.inscEst);
    this.formGroupEditProperty.get('municipalRegistration').setValue(this.property.inscMun);
    this.formGroupEditProperty.get('brand').setValue(this.property.brandId);
    this.formGroupEditProperty.get('chain').setValue(this.property.chainId);
  }

  public cancelDetailPage() {
    const answer = window.confirm(this.translateService.instant('commomData.actionCancel'));

    if (answer) {
      this.route.navigate(['p', this.property.id]);
    }
  }

  public saveProperty() {
    this.updatePropertyContact();
    this.updatePropertyLocation();
    this.updateProperty();

    this.httpClient.put<Property>('properties', this.property).subscribe(property => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('hotelPanelModule.propertyEdit.successMessage'),
      );
    });
  }

  private updateProperty() {
    this.property.cnpj = this.formGroupEditProperty.get('cnpj').value;
    this.property.inscEst = this.formGroupEditProperty.get('stateRegistration').value;
    this.property.inscMun = this.formGroupEditProperty.get('municipalRegistration').value;
    this.property.brandId = this.formGroupEditProperty.get('brand').value;
    this.property.name = this.formGroupEditProperty.get('name').value;
    this.property.tradeName = this.formGroupEditProperty.get('socialName').value;
    this.property.shortName = this.formGroupEditProperty.get('fantasyName').value;
    this.property.propertyStatusId = this.property.propertyStatusId;
  }

  private updatePropertyContact() {
    this.property.contactPhone = this.formGroupEditProperty.get('telephone').value;
    this.property.contactWebSite = this.formGroupEditProperty.get('homePage').value;
  }

  private updatePropertyLocation() {
    this.property.locationList = [];
    const location: Location = this.formGroupEditProperty.get('address').value;
    this.property.locationList.push(location);
  }

  public setValueAddressByEventEmitted(address: Location) {
    this.formGroupEditProperty.get('address').patchValue(address);
  }

  public getAllChainOptionList() {
    this.chainResource.getAllChain().subscribe(response => {
      if (response) {
        this.chainOptionList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(response, 'id', 'name');
      }
    });
  }

  public getAllBrandOptionList() {
    this.brandResource.getAllBrand().subscribe(response => {
      if (response) {
        this.brandOptionList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(response, 'id', 'name');
      }
    });
  }
}
