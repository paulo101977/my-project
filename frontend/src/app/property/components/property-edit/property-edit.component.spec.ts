// import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
// import { ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';
// import { RouterTestingModule } from '@angular/router/testing';
// // import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { TranslateModule } from '@ngx-translate/core';
// import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
// import { Observable } from 'rxjs';
// import { HttpClientModule } from '@angular/common/http';
// import { PropertyEditComponent } from './property-edit.component';
// import { Company } from '../../../shared/models/company';
// import { ShowHide } from '../../../shared/models/show-hide-enum';
// import { PropertyData } from '../../../shared/mock-data/property-data';
// import { SelectModel } from '../../../shared/models/selectModel';
// import { SuccessError } from 'app/shared/models/success-error-enum';
// import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
// import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
// import { SharedService } from '../../../shared/services/shared/shared.service';
// import { CompanyService } from '../../../shared/services/shared/company.service';
// import { PropertyService } from 'app/shared/services/property/property.service';
// import { PropertyResource } from '../../../shared/resources/property/property.resource';
// import { CompanyResource } from '../../../shared/resources/company/company.resource';

// describe('PropertyEditComponent', () => {
//   const streetNumber = '32';
//   let promiseCompany: any;
//   let component: PropertyEditComponent;
//   let fixture: ComponentFixture<PropertyEditComponent>;
//   const company = {
//     id: 1
//   };
//   const propertyMock = PropertyData;
//   const propertyId = 1;

//   beforeAll(() => {
//     TestBed.resetTestEnvironment();
//     TestBed
//       .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
//       .configureTestingModule({
//         imports: [
//           ReactiveFormsModule,
//
//           HttpClientModule,
//           RouterTestingModule,
//           TranslateModule.forRoot()
//         ],
//         declarations: [
//           PropertyEditComponent
//         ],
//         providers: [],
//         schemas: [
//           CUSTOM_ELEMENTS_SCHEMA
//         ]
//       });

//     fixture = TestBed.createComponent(PropertyEditComponent);
//     component = fixture.componentInstance;

//     promiseCompany = spyOn(component.companyService, 'getCompany').and.callFake(() => {
//       return new Promise((resolve, reject) => resolve(company));
//     });
//     component.property = PropertyData;
//     spyOn(component['propertyService'], 'getPropertyShared').and.returnValue(Observable.of(PropertyData));

//     fixture.detectChanges();
//   });

//   describe('on init', () => {
//     it('should setVars and start component', () => {
//       expect(component.property).toEqual(PropertyData);
//       expect(component['contact']).toEqual('contact');
//       expect(component['document']).toEqual('document');
//       expect(component['company'].id).toEqual(1);
//     });

//     it('should instance formGroupEditProperty and controls', () => {
//       expect(component.formGroupEditProperty.controls).toBeDefined();
//       expect(component.formGroupEditProperty instanceof FormGroup).toBe(true);
//       expect(Object.keys(component.formGroupEditProperty.controls)).toEqual([
//         'name', 'chain', 'brand', 'fantasyName', 'socialName', 'totalUhs', 'telephone',
//         'homePage', 'cnpj', 'stateRegistration', 'municipalRegistration', 'address'
//       ]);
//     });

//     it('should set formGroupEditProperty', () => {
//       const tel = '(21) 35118200';
//       const linkHome = 'www.ibis.com';
//       const street = 'RUA SILVA JARDIM';
//       const cnpj = '09.967.852/0143-49';
//       const registration = '123721539';
//       const postalCode = '20050060';

//       expect(component.formGroupEditProperty.get('name').value).toEqual(component.property.name);
//       expect(component.formGroupEditProperty.get('chain').value).toEqual(component.property.brand.chain.name);
//       expect(component.formGroupEditProperty.get('brand').value).toEqual(component.property.brand.name);
//       expect(component.formGroupEditProperty.get('fantasyName').value).toEqual(component.property.company.shortName);
//       expect(component.formGroupEditProperty.get('socialName').value).toEqual(component.property.company.tradeName);
//       expect(component.formGroupEditProperty.get('totalUhs').value).toEqual(component.property.totalOfRooms);
//       expect(component.formGroupEditProperty.get('telephone').value).toEqual(tel);
//       expect(component.formGroupEditProperty.get('homePage').value).toEqual(linkHome);
//       expect(component.formGroupEditProperty.get('address').value.postalCode).toEqual(postalCode);
//       expect(component.formGroupEditProperty.get('address').value.locality).toEqual(street);
//       expect(component.formGroupEditProperty.get('address').value.number).toEqual(streetNumber);
//       expect(component.formGroupEditProperty.get('cnpj').value).toEqual(cnpj);
//       expect(component.formGroupEditProperty.get('stateRegistration').value).toEqual(registration);
//       expect(component.formGroupEditProperty.get('municipalRegistration').value).toEqual(registration);
//     });
//   });

//   describe('on methods', () => {
//     it('should cancelDetailPage', () => {
//       spyOn(component.route, 'navigate');
//       spyOn(window, 'confirm').and.returnValue(true);

//       component.cancelDetailPage();

//       expect(component.route.navigate).toHaveBeenCalledWith(
//         ['p', 1]
//       );
//     });
//   });

//   describe('on saveProperty', () => {
//     beforeAll(() => {
//       spyOn(component.loadPageEmitService, 'emitChange');
//       spyOn(component.route, 'navigate');
//       spyOn(component['toasterEmitService'], 'emitChange');
//     });
//     it('should saveProperty', fakeAsync(() => {
//       spyOn(component['httpClient'], 'put').and.returnValue(Observable.of({}));
//       const location = 22795;
//       const streetName = 'Rua Sérgio Branco Soares';
//       const company = new Company();
//       company.id = 1;
//       component['company'] = company;

//       component.saveProperty();
//       tick();

//       expect(component.loadPageEmitService.emitChange).toHaveBeenCalledWith(ShowHide.show);
//       expect(component.property.locationList.length).toEqual(2);
//       expect(component.property.locationList[0].postalCode).toEqual(location);
//       expect(component.property.locationList[0].streetNumber).toEqual(streetNumber);
//       expect(component.property.locationList[0].streetName).toEqual(streetName);
//       expect(component.route.navigate(['p', 1]));
//       expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
//         SuccessError.success,
//         'hotelPanelModule.propertyEdit.successMessage'
//       );
//     }));
//   });

//   describe('on address', () => {
//     it('should set ValueAddressTypeByEventEmitted', () => {
//       const optionSelected = new SelectModel<string>();
//       optionSelected.formGroupName = 'address.selectValueType';
//       optionSelected.fieldName = 'type';
//       optionSelected.value = 'Avenida';

//       component.setValueAddressByEventEmitted(optionSelected);

//       expect(component.formGroupEditProperty.get(optionSelected.formGroupName + '.' + optionSelected.fieldName).value)
// .toEqual(optionSelected.value);
//     });
//   });
// });
