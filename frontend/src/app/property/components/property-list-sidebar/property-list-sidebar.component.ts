import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-property-list-sidebar',
  templateUrl: './property-list-sidebar.component.html',
  styleUrls: ['./property-list-sidebar.component.scss']
})
export class PropertyListSidebarComponent implements OnInit {
  @Input() menuList: any;

  constructor() { }

  ngOnInit() {
  }

}
