import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PropertyListSidebarComponent } from './property-list-sidebar.component';
import {configureTestSuite} from 'ng-bullet';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {APP_NAME} from '@inovacaocmnet/thx-bifrost';
import {imgCdnStub} from '../../../../../testing';
import {TranslateTestingModule} from 'app/shared/mock-test/translate-testing.module';

describe('PropertyListSidebarComponent', () => {
  let component: PropertyListSidebarComponent;
  let fixture: ComponentFixture<PropertyListSidebarComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        PropertyListSidebarComponent,
        imgCdnStub
      ],
      imports: [
        TranslateTestingModule
      ],
      providers: [
        { provide: APP_NAME, useValue: {} }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
    fixture = TestBed.createComponent(PropertyListSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
