import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PropertyEditComponent } from './components/property-edit/property-edit.component';
import { ParameterComponent } from './components/parameter/parameter.component';
import { GovernanceStatusListComponent } from '../governance/components/governance-status-list/governance-status-list.component';
import { LocationGuardService } from 'app/auth/services/location-guard.service';


export const propertyRoutes: Routes = [
  { path: '', component: PropertyEditComponent },
  { path: 'parameter', component: ParameterComponent },
  { path: 'origin', loadChildren: '../origin/origin.module#OriginModule' },
  { path: 'market-segment', loadChildren: '../market-segment/market-segment.module#MarketSegmentModule' },
  { path: 'user', loadChildren: '../user/user.module#UserModule' },
  { path: 'policies', loadChildren: '../policy/policy.module#PolicyModule' },
  { path: 'governance-status', component: GovernanceStatusListComponent },
  { path: 'category', loadChildren: '../category/category.module#CategoryModule' },
  { path: 'fiscal-documents', loadChildren: './components/fiscal-documents/fiscal-documents.module#FiscalDocumentsModule' },
  { path: 'guest-type', loadChildren: '../config/guest-type/components/guest-type.module#GuestTypeModule' },
  { path: 'tax', loadChildren: '../tax/tax.module#TaxModule', canActivate: [ LocationGuardService ] },
];

@NgModule({
  imports: [ RouterModule.forChild(propertyRoutes) ],
  exports: [RouterModule],
})
export class PropertyRoutingModule {}
