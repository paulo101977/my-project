import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OriginListComponent } from './components/origin-list/origin-list.component';

export const originRoutes: Routes = [{ path: '', component: OriginListComponent }];

@NgModule({
  imports: [RouterModule.forChild(originRoutes)],
  exports: [RouterModule],
})
export class OriginRoutingModule {}
