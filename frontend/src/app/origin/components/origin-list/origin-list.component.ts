import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Origin } from '../../../shared/models/origin';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { OriginResource } from '../../../shared/resources/origin/origin.resource';

@Component({
  selector: 'app-origin-list',
  templateUrl: './origin-list.component.html',
  styleUrls: ['./origin-list.component.css'],
})
export class OriginListComponent implements OnInit {
  public columns: Array<any>;
  public pageItemsList: Origin[];
  public origimItem: Origin;
  public modalForm: FormGroup;
  private activeEdition: boolean;
  private propertyId: number;
  public itemsOption: Array<any>;

  // Modal Dependencies
  public modalTitle: string;

  constructor(
    public translateService: TranslateService,
    private route: ActivatedRoute,
    private originResource: OriginResource,
    private toasterEmitService: ToasterEmitService,
    private modalEmitService: ModalEmitService,
    private modalEmitToggleService: ModalEmitToggleService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.setVars();
  }

  private setVars(): void {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.activeEdition = false;
      this.origimItem = new Origin();
      this.setColumnsName();
      this.getOrigins();
      this.configFormModal();
      this.itemsOption = [
        { title: 'commomData.edit', callbackFunction: this.editOriginConfig },
        { title: 'commomData.delete', callbackFunction: this.confirmRemoveOrigin },
      ];
    });
  }

  private getOrigins(): void {
    this.originResource.getOrigin(this.propertyId).subscribe(result => {
      this.pageItemsList = result;
    });
  }

  public updateOriginStatus(originItem: Origin) {
    this.originResource.updateOriginStatus(originItem).subscribe(result => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant(
          originItem.isActive ? 'originModule.commomData.activatedWithSuccess' : 'originModule.commomData.inactivatedWithSuccess',
        ),
      );
    });
  }

  public confirmRemoveOrigin = (origin: Origin) => {
    this.origimItem = origin;
    this.modalEmitService.emitSimpleModal(
      this.translateService.instant('commomData.delete'),
      this.translateService.instant('originModule.commomData.confirmDelete'),
      this.removeOrigin,
      [],
    );
  }

  public editOriginConfig = (origin: Origin) => {
    this.activeEdition = true;
    this.toggleModal();
    this.modalTitle = 'originModule.modalOrigin.titleEdit';
    this.modalForm.patchValue({
      id: origin.id,
      code: origin.code,
      description: origin.description,
      isActive: origin.isActive,
      property: origin.propertyId,
    });
  }

  public editOrigin(): void {
    this.originResource.updateOrigin(this.modalForm.value).subscribe(result => {
      this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant('originModule.commomData.editSuccessMessage'));
      this.activeEdition = false;
      this.toggleModal();
      this.getOrigins();
    });
  }

  private removeOrigin = () => {
    this.originResource.removeOrigin(this.origimItem).subscribe(result => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('originModule.commomData.deleteSuccessMessage'),
      );
      this.getOrigins();
    });
  }

  private setColumnsName() {
    this.columns = [
      { name: 'originModule.tableRowOrigin.columns.code', prop: 'code' },
      { name: 'originModule.tableRowOrigin.columns.description', prop: 'description' },
    ];
  }

  private configFormModal(): void {
    this.modalForm = this.formBuilder.group({
      id: 0,
      code: ['', [Validators.required, Validators.maxLength(10)]],
      description: ['', [Validators.required, Validators.maxLength(60)]],
      isActive: true,
      propertyId: this.propertyId,
    });
  }

  public toggleModal(): void {
    this.modalTitle = 'originModule.modalOrigin.title';
    this.configFormModal();
    this.modalEmitToggleService.emitToggleWithRef('modalOrigin');
  }

  public createOrigin(): void {
    this.originResource.createOrigin(this.modalForm.value).subscribe(result => {
      this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant('originModule.commomData.successMessage'));
      this.toggleModal();
      this.getOrigins();
    });
  }

  public actionSaveModalOrigin(): void {
    this.activeEdition ? this.editOrigin() : this.createOrigin();
  }

  public runCallbackFunction(item: any, origin: Origin) {
    if (item.callbackFunction) {
      item.callbackFunction(origin, this);
    }
  }
}
