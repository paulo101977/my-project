/*
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormBuilder, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { OriginListComponent } from './origin-list.component';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { Origin } from '../../../shared/models/origin';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { LoadPageEmitService } from '../../../shared/services/shared/load-emit.service';
import { OriginResource } from '../../../shared/resources/origin/origin.resource';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';

describe('OriginListComponent', () => {
  let component: OriginListComponent;
  let fixture: ComponentFixture<OriginListComponent>;
  let promiseOriginList;
  let promiseOriginUpdateStatus;
  const origin = new Origin();
  origin.id = 1;
  origin.description = 'Descrição do item';
  origin.isActive = false;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed
      .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
      .configureTestingModule({
        imports: [
          TranslateModule.forRoot(),

          BrowserDynamicTestingModule,
          FormsModule,
          ReactiveFormsModule,
          RouterTestingModule,
        ],
        declarations: [OriginListComponent],
        providers: [],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      });

    fixture = TestBed.createComponent(OriginListComponent);
    component = fixture.componentInstance;

    promiseOriginList = spyOn(component['originResource'], 'getOrigin').and.callFake(() => {
      return new Promise((resolve, reject) => resolve(origin));
    });

    promiseOriginUpdateStatus = spyOn(component['originResource'], 'updateOriginStatus').and.callFake(() => {
      return new Promise((resolve, reject) => resolve(origin.id));
    });

    fixture.detectChanges();
  });

  describe('onInit', () => {
    it('should create OriginListComponent', () => {
      expect(component).toBeTruthy();
    });

    it('should setVars', () => {
      component['setVars']();

      expect(component['activeEdition']).toEqual(false);
      expect(component.columns[0].name).toEqual('originModule.tableRowOrigin.columns.code');
      expect(component.columns[1].name).toEqual('originModule.tableRowOrigin.columns.description');
    });
  });

  it('should getOrigins', fakeAsync(() => {
    spyOn(component['loadPageEmitService'], 'emitChange');
    component['getOrigins']();
    tick();
    expect(component.pageItemsList).toEqual(origin);
    expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.show);
  }));

  it('should call loadpage e success message on change status Origin', fakeAsync(() => {
    spyOn(component['loadPageEmitService'], 'emitChange');
    spyOn(component['toasterEmitService'], 'emitChange');

    component.updateOriginStatus(origin);

    tick();

    expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.show);
    expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
      SuccessError.success,
      'originModule.commomData.inactivatedWithSuccess',
    );
  }));

  it('should call success message on remove Origin', () => {
    spyOn(component['modalEmitService'], 'emitSimpleModal');

    component.confimRemoveOrigin(origin);

    expect(component.origimItem).toEqual(origin);

    expect(component['modalEmitService'].emitSimpleModal).toHaveBeenCalledWith(
      'commomData.delete',
      'originModule.commomData.confirmDelete',
      component['removeOrigin'],
      []
    );
  });

  it('should set configFormModal', () => {
    component['configFormModal']();

    expect(Object.keys(component.modalForm.controls)).toEqual([
      'id', 'code', 'description', 'isActive', 'propertyId'
    ]);
  });

  it('should call toggleModal', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');

    component.toggleModal();
    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('modalOrigin');
  });

  it('should call editOriginConfig', () => {
    spyOn(component, 'toggleModal');
    component.editOriginConfig(origin);

    expect(component.toggleModal).toHaveBeenCalled();
    expect(component['activeEdition']).toEqual(true);
    expect(component['modalTitle']).toEqual('originModule.modalOrigin.titleEdit');
  });

  it('should call actionSaveModalOrigin with variable activeEdition TRUE', () => {
    spyOn(component, 'editOrigin');
    spyOn(component, 'createOrigin');

    component['activeEdition'] = true;
    component.actionSaveModalOrigin();

    expect(component['activeEdition']).toEqual(true);
    expect(component.editOrigin).toHaveBeenCalled();
    expect(component.createOrigin).not.toHaveBeenCalled();
  });

  it('should call actionSaveModalOrigin with variable activeEdition FALSE', () => {
    spyOn(component, 'editOrigin');
    spyOn(component, 'createOrigin');

    component['activeEdition'] = false;
    component.actionSaveModalOrigin();

    expect(component['activeEdition']).toEqual(false);
    expect(component.editOrigin).not.toHaveBeenCalled();
    expect(component.createOrigin).toHaveBeenCalled();
  });

  it('should call editOrigin', fakeAsync(() => {
    spyOn(component['loadPageEmitService'], 'emitChange');
    spyOn(component['toasterEmitService'], 'emitChange');
    spyOn(component, 'toggleModal');
    spyOn(component, 'getOrigins');

    const promiseRoomType = spyOn(component['originResource'], 'updateOrigin').and.callFake(() => {
      return new Promise((resolve, reject) => resolve({}));
    });

    component.editOrigin();
    expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.show);

    tick();

    expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
    expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
      SuccessError.success,
      component.translateService.instant('originModule.commomData.editSuccessMessage')
    );

    expect(component['activeEdition']).toEqual(false);

    expect(component['toggleModal']).toHaveBeenCalled();
    expect(component['getOrigins']).toHaveBeenCalled();
  }));

  it('should call loadpage and others on create createOrigin', fakeAsync(() => {
    spyOn(component['loadPageEmitService'], 'emitChange');
    spyOn(component['toasterEmitService'], 'emitChange');
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    spyOn(component, 'getOrigins');
    spyOn(component, 'configFormModal');

    const promiseRoomType = spyOn(component['originResource'], 'createOrigin').and.callFake(() => {
      return new Promise((resolve, reject) => resolve({}));
    });

    component.createOrigin();
    tick();

    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('modalOrigin');
    expect(component['loadPageEmitService'].emitChange).toHaveBeenCalledWith(ShowHide.hide);
    expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
      SuccessError.success,
      component.translateService.instant('originModule.commomData.successMessage')
    );

    expect(component['getOrigins']).toHaveBeenCalled();
    expect(component['configFormModal']).toHaveBeenCalled();
  }));
});
*/
