import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { OriginListComponent } from './components/origin-list/origin-list.component';
import { OriginRoutingModule } from './origin-routing.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, TranslateModule.forChild(), SharedModule, FormsModule, ReactiveFormsModule, OriginRoutingModule],
  declarations: [OriginListComponent],
})
export class OriginModule {}
