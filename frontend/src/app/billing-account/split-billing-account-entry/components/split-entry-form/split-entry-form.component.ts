import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RadioGroupOption } from '../../../../shared/components/radio-input/models/radio-group-option';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SplitTypeEnum } from '../../models/split-type-enum';
import { ModalEmitToggleService } from '../../../../shared/services/shared/modal-emit-toggle.service';
import { SplitEntryDto } from '../../models/split-entry-dto';
import { SessionParameterService } from '../../../../shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from '../../../../shared/models/parameter/pameterType.enum';
import { CurrencyService } from '../../../../shared/services/shared/currency-service.service';
import { SplitService } from '../../services/split.service';

@Component({
  selector: 'app-split-entry-form',
  templateUrl: './split-entry-form.component.html',
  styleUrls: ['./split-entry-form.component.css']
})
export class SplitEntryFormComponent implements OnInit {

  private _splitEntry: SplitEntryDto;
  @Input('splitEntry')
  set splitEntry(entry: SplitEntryDto) {
    this._splitEntry = entry;
    if (this._splitEntry) {
      this.percentageRate = this._splitEntry.percentageRate * 100;
      this.total = this.splitEntry.totalValueWithoutRate;
    }
    this.splitEntryChange.emit(this._splitEntry);
  }

  get splitEntry(): SplitEntryDto {
    return this._splitEntry;
  }
  @Input() propertyId;

  @Output() splitEntryChange = new EventEmitter();
  @Output() formStatusChange = new EventEmitter();

  public radioOptions: Array<RadioGroupOption>;

  // form
  public splitForm: FormGroup;
  public optionsCurrencyMask: any;
  public formValid = false;

  public isTypeValue = true;
  public isTypeSlice = false;
  public withRate: boolean;
  public total: number;

  public previewValue: string;
  public percentageRate: number;

  public propertyCurrencySymbol: any;

  constructor(
    private formBuilder: FormBuilder,
    private toggleEmmitModalService: ModalEmitToggleService,
    private sessionParameterService: SessionParameterService,
    private currencyService: CurrencyService,
    private splitService: SplitService ) { }

  // LifeCycle
  ngOnInit() {
    this.setCurrencyMask();
    this.setForm();
    this.setRadioOptions();
    this.getPropertyCurrency();
    this.toggleEmmitModalService.changeEmitted$.subscribe(() => {
      if (this.splitForm) {
        this.resetForm();
      }
    });
  }

  private getPropertyCurrency() {
    this.propertyCurrencySymbol = this.currencyService.getDefaultCurrencySymbol(this.propertyId);
  }

// Config
  private setForm() {
    this.splitForm = this.formBuilder.group({
      splitType: [SplitTypeEnum.TypeValue, Validators.required],
      value: [0 , [Validators.required, Validators.min(0)]],
      parts: [null]
    });
    this.observeSplitType();
    this.observeFormChanges();
  }

  private observeFormChanges() {
    this.splitForm.valueChanges.subscribe(value => {
      if (this.formValid != this.splitForm.valid) {
        this.formValid = !this.splitForm.invalid;
        this.formStatusChange.emit(this.formValid);
      }

      if (this.splitEntry) {
        if (value.splitType == SplitTypeEnum.TypeValue) {
          this.splitEntry.splitValue = value.value;
          this.withRate = false;
          this.total = this.splitEntry.totalValueWithoutRate;
        } else {
          this.splitEntry.splitParts = value.parts;
          this.withRate = true;
          this.total = this.splitEntry.totalValue;
        }
        this.previewValue = this.splitService.getPreviewSplitCalc(this.splitEntry, this.propertyCurrencySymbol, this.withRate);
      }
    });
  }

  private resetForm() {
    this.splitForm.markAsPristine();
    this.splitForm.get('splitType').setValue(SplitTypeEnum.TypeValue);
    this.splitForm.get('value').setValue(0);
  }

  private observeSplitType() {
    this.splitForm.get('splitType').valueChanges.subscribe(value => {
      this.ajustFormValidations(value);
    });
  }

  private ajustFormValidations(value) {
    this.isTypeValue = value == SplitTypeEnum.TypeValue;
    this.isTypeSlice = value == SplitTypeEnum.TypeParts;

    switch (value) {
      case SplitTypeEnum.TypeValue:
        this.splitForm.get('value').setValidators([Validators.required, Validators.min(0)]);
        this.splitForm.get('value').updateValueAndValidity();

        this.splitForm.get('parts').clearValidators();
        this.splitForm.get('parts').updateValueAndValidity();
        return;
      case SplitTypeEnum.TypeParts:
        this.splitForm.get('parts').setValidators([Validators.required, Validators.min(2)]);
        this.splitForm.get('parts').updateValueAndValidity();

        this.splitForm.get('value').clearValidators();
        this.splitForm.get('value').updateValueAndValidity();
        return;
    }
  }

  private setCurrencyMask() {
    this.optionsCurrencyMask = {prefix: '', thousands: '.', decimal: ',', align: 'left'};
  }

  private setRadioOptions() {
    this.radioOptions = [
      {
        id: 'typeValue',
        label: 'label.value',
        value: SplitTypeEnum.TypeValue
      },
      {
        id: 'typeSlice',
        label: 'label.slices',
        value: SplitTypeEnum.TypeParts
      },
    ];
  }
}
