import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitEntryFormComponent } from './split-entry-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SessionParameterService } from '../../../../shared/services/parameter/session-parameter.service';

describe('SplitEntryFormComponent', () => {
  let component: SplitEntryFormComponent;
  let fixture: ComponentFixture<SplitEntryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, FormsModule, ReactiveFormsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [ SplitEntryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitEntryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.propertyId = 1;
    expect(component).toBeTruthy();
  });
});
