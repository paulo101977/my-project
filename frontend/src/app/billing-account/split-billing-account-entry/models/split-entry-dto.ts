export class SplitEntryDto {
  id: string;
  billingAccountId: string;
  billingAccountItemId: string;
  totalValue: number;
  totalValueWithoutRate: number;
  percentageRate: number;
  private value: number;
  private parts: number;

  set splitValue(val) {
    this.value = val;
    this.parts = null;
  }

  get splitValue(): number {
    return this.value;
  }

  set splitParts(part) {
    this.parts = part;
    this.value = null;
  }

  get splitParts(): number {
    return this.parts;
  }
}
