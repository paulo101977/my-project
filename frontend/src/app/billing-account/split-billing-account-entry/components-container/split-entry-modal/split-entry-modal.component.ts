import { Component, EventEmitter, Input, Output, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { BillingAccountItemEntry } from '../../../../shared/models/billing-account/billing-account-item-entry';
import { SplitEntryDto } from '../../models/split-entry-dto';
import { SplitResource } from '../../resources/split.resource';
import { ToasterEmitService } from '../../../../shared/services/shared/toaster-emit.service';
import { SuccessError } from '../../../../shared/models/success-error-enum';

@Component({
  selector: 'app-split-entry-modal',
  templateUrl: './split-entry-modal.component.html'
})
export class SplitEntryModalComponent implements OnInit, OnChanges {

  @Input() modalId: string;
  @Input() propertyId: number;
  @Input() accountId: string;

  private _billingAccountEntry: BillingAccountItemEntry;
  @Input('billingAccountEntry')
  set billingAccountEntry( entryItem: BillingAccountItemEntry ) {
    this._billingAccountEntry = entryItem;
    if (entryItem) {
      this.splitItem(entryItem);
    }
  }

  get billingAccountEntry() {
    return this._billingAccountEntry;
  }

  @Output() cancel = new EventEmitter();
  @Output() confirm = new EventEmitter();

  public splitItemEntry: SplitEntryDto;

  public isOkToSave: false;

  constructor(private splitResource: SplitResource, private toastService: ToasterEmitService) { }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('accountId')) {
      if (this.splitItemEntry) {
        this.splitItemEntry.billingAccountId = changes.accountId.currentValue;
      }
    }
  }

  public splitItem(entry: BillingAccountItemEntry) {
    if (!this.splitItemEntry) {
      this.splitItemEntry = new SplitEntryDto();
    }
    this.splitItemEntry.billingAccountItemId = entry.billingAccountItemId;
    this.splitItemEntry.id = entry.billingAccountItemId;
    this.splitItemEntry.totalValue = entry.total;
    this.splitItemEntry.totalValueWithoutRate = entry.totalWithoutRate;
    this.splitItemEntry.percentageRate = entry.percentageRate;
  }

  cancelModal() {
    this.cancel.emit();
  }

  confirmModal() {
    this.splitResource.splitEntry(this.splitItemEntry).subscribe( result => {
      this.toastService.emitChange(SuccessError.success, 'label.splitSuccess');
      this.confirm.emit();
    });
  }

  canSave(canSave) {
    this.isOkToSave = canSave;
  }
}
