import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { SplitEntryModalComponent } from './components-container/split-entry-modal/split-entry-modal.component';
import { SplitEntryFormComponent } from './components/split-entry-form/split-entry-form.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [SplitEntryModalComponent, SplitEntryFormComponent],
  exports: [ SplitEntryModalComponent ]
})
export class SplitBillingAccountEntryModule { }
