import { Injectable } from '@angular/core';
import { HttpService } from '../../../core/services/http/http.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { SplitEntryDto } from '../models/split-entry-dto';
import { Observable } from 'rxjs/index';

@Injectable({ providedIn: 'root' })
export class SplitResource {

  private url = 'BillingAccountItem/partial';

  constructor(private httpClient: HttpService, private sharedService: SharedService) {
  }

  public splitEntry(splitEntry: SplitEntryDto): Observable<any> {
    return this.httpClient.post(this.url, splitEntry);
  }
}
