import { TestBed, inject } from '@angular/core/testing';

import { SplitService } from './split.service';
import { CurrencyFormatPipe } from '../../../shared/pipes/currency-format.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('SplitService', () => {
  let service;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
      ],
      providers: [SplitService],
    });

    service = TestBed.get(SplitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
