import { Injectable } from '@angular/core';
import { SplitEntryDto } from '../models/split-entry-dto';
import { CurrencyFormatPipe } from '../../../shared/pipes/currency-format.pipe';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class SplitService {

  constructor(private currencyFormatPipe: CurrencyFormatPipe, private translateService: TranslateService) {
  }

  getPreviewSplitCalc(splitEntry: SplitEntryDto, currencySymbol: string, withRate?: boolean): string {

    if (splitEntry && splitEntry.splitValue > 0) {
      let value = splitEntry.splitValue;
      const total = withRate ? splitEntry.totalValue : splitEntry.totalValueWithoutRate;
      if (total < 0) {
        value = value * -1;
      }
      const firstValue = this.currencyFormatPipe
        .transform(value, null, 2, '.', ',', 3, false);
      const secondValue = this.currencyFormatPipe
        .transform(total - value, null, 2, '.', ',', 3, false);
      return `${currencySymbol} ${firstValue} / ${currencySymbol} ${secondValue}`;
    }
    if (splitEntry.splitParts && splitEntry.splitParts > 0) {
      const partsValue = this.currencyFormatPipe
        .transform(splitEntry.totalValue / splitEntry.splitParts, null, 2, '.', ',', 3, false);
      return `${splitEntry.splitParts}x ${this.translateService.instant('label.of')} ${currencySymbol} ${partsValue}`;
    }
    return '-';
  }
}
