import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { ButtonBarButtonConfig } from '../../../shared/components/button-bar/button-bar-button-config';
import { ButtonBarButtonStatus } from '../../../shared/components/button-bar/button-bar-button-status.enum';
import { ButtonBarColor } from '../../../shared/components/button-bar/button-bar-color.enum';
import { ButtonBarConfig } from '../../../shared/components/button-bar/button-bar-config';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { PaymentTypeWithBillingItem } from '../../../shared/models/billingType/payment-type-with-billing-item';
import { BillingAccountWithEntry } from '../../../shared/models/billing-account/billing-account-with-entry';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { BillingAccountItemMapper } from '../../../shared/mappers/billing-account-item-mapper';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { BillingItemServiceResource } from '../../../shared/resources/billingItem/billing-item-service.resource';
import { BillingItemCategoryResource } from '../../../shared/resources/billing-item-category/billing-item-category.resource';
import { BillingAccountItemResource } from '../../../shared/resources/billing-account-item/billing-account-item.resource';

@Component({
  selector: 'billing-account-debit-credit',
  templateUrl: './billing-account-debit-credit.component.html',
})
export class BillingAccountDebitCreditComponent implements OnInit {
  @Input() billingAccount: BillingAccountWithEntry;

  @Output() updateBillingAccountAndHeaderAndCardList = new EventEmitter<string>();

  public paymentTypeWithBillingItemList: Array<PaymentTypeWithBillingItem>;
  public propertyId: number;
  public isDebit: boolean;
  private readonly totalPriceDefault = 0;

  // Form
  public formModal: FormGroup;
  public isDisabled = false;

  // Modal dependencies
  public modalId = 'post-debit-credit';
  public modalTitle = 'billingAccountModule.commomData.release';

  // Buttons dependencies
  public buttonBarConfig: ButtonBarConfig;
  public buttonCancelConfig: ButtonConfig;
  public buttonPostDebitOrCreditConfig: ButtonConfig;

  constructor(
    private route: ActivatedRoute,
    private modalEmitToggleService: ModalEmitToggleService,
    private formBuilder: FormBuilder,
    private billingItemServiceResource: BillingItemServiceResource,
    private billingItemCategoryResource: BillingItemCategoryResource,
    private sharedService: SharedService,
    private billingAccountItemMapper: BillingAccountItemMapper,
    private billingAccountItemResource: BillingAccountItemResource,
    private toasterEmitService: ToasterEmitService,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars(): void {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
    });
    this.isDebit = true;
    this.setConfigButtonBar();
    this.setButtonConfig();
    this.setFormModalPostDebit();
  }

  private setFormModalPostDebit(): void {
    this.formModal = this.formBuilder.group({
      postDebit: this.formBuilder.group(
        {
          billingItemServiceId: null,
          priceWithoutTax: null,
        },
        { validator: this.validateDebitForm },
      ),
      postCredit: this.formBuilder.group(
        {
          paymentTypeId: null,
          card: this.formBuilder.group({
            plasticBrandPropertyId: null,
            nsuCode: null,
          }),
          bankCheck: this.formBuilder.group({
            number: null,
          }),
          price: null,
        },
        { validator: this.validateCreditForm },
      ),
      totalPrice: this.totalPriceDefault.toFixed(2),
    });

    this.formModal.valueChanges.subscribe(value => {
      this.buttonPostDebitOrCreditConfig.isDisabled = this.formModal.invalid;
    });
  }

  public setConfigButtonBar() {
    this.buttonBarConfig = {
      color: ButtonBarColor.blue,
      buttons: [
        {
          title: 'commomData.debit',
          status: ButtonBarButtonStatus.active,
          callBackFunction: () => {
            this.isDebit = true;
            this.setFormModalPostDebit();
            this.buttonPostDebitOrCreditConfig = this.sharedService.getButtonConfig(
              'post-debit-or-credit',
              this.postDebit,
              'billingAccountModule.commomData.launch',
              ButtonType.Primary,
              null,
              this.formModal.invalid,
            );
          },
        } as ButtonBarButtonConfig,
        {
          title: 'commomData.credit',
          callBackFunction: () => {
            this.isDebit = false;
            this.setFormModalPostDebit();
            this.buttonPostDebitOrCreditConfig = this.sharedService.getButtonConfig(
              'post-debit-or-credit',
              this.postCredit,
              'billingAccountModule.commomData.launch',
              ButtonType.Primary,
              null,
              this.formModal.invalid,
            );
          },
        } as ButtonBarButtonConfig,
      ],
    };
  }

  private setButtonConfig(): void {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-debit',
      this.resetFormAndCloseModal,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonPostDebitOrCreditConfig = this.sharedService.getButtonConfig(
      'post-debit-or-credit',
      this.postDebit,
      'billingAccountModule.commomData.launch',
      ButtonType.Primary,
      null,
      true,
    );
  }

  private resetFormAndCloseModal = () => {
    this.resetFormAndConfigButtonBar();
    this.modalEmitToggleService.emitToggleWithRef(this.modalId);
  }

  public resetFormAndConfigButtonBar = () => {
    this.isDebit = true;
    this.setFormModalPostDebit();
    this.setConfigButtonBar();
    this.setButtonConfig();
  }

  private postDebit = () => {
    const billingAccountItem = this.billingAccountItemMapper.toBillintAccountItemForPostDebit(
      this.formModal,
      this.propertyId,
      this.billingAccount.billingAccountId,
    );
    this.billingAccountItemResource.createBillingAccountItemDebit(billingAccountItem).subscribe(response => {
      this.toasterEmitService.emitChange(SuccessError.success, 'billingAccountModule.commomData.billingAccountItemSuccessMessage');
      this.resetFormAndCloseModal();
      this.onUpdateBillingAccountAndHeaderAndCardList(this.billingAccount.billingAccountId);
    });
  }

  private postCredit = () => {
    const billingAccountItem = this.billingAccountItemMapper.toBillintAccountItemForPostCredit(
      this.formModal,
      this.propertyId,
      this.billingAccount.billingAccountId,
      this.paymentTypeWithBillingItemList,
    );
    this.billingAccountItemResource.createBillingAccountItemCredit(billingAccountItem).subscribe(response => {
      this.toasterEmitService.emitChange(SuccessError.success, 'billingAccountModule.commomData.billingAccountItemSuccessMessage');
      this.resetFormAndCloseModal();
      this.onUpdateBillingAccountAndHeaderAndCardList(this.billingAccount.billingAccountId);
    });
  }

  public updatePaymentTypeWithBillingItemList(paymentTypeWithBillingItemList: Array<PaymentTypeWithBillingItem>): void {
    this.paymentTypeWithBillingItemList = paymentTypeWithBillingItemList;
  }

  public onUpdateBillingAccountAndHeaderAndCardList(mainBillingAccountId: string): void {
    this.updateBillingAccountAndHeaderAndCardList.emit(mainBillingAccountId);
  }

  private validateDebitForm = (formGroup: FormGroup) => {
    const billingItemServiceId = formGroup.get('billingItemServiceId').value;
    const priceWithoutTax = formGroup.get('priceWithoutTax').value;
    if (this.isDebit && (!billingItemServiceId || !priceWithoutTax)) {
      return { invalidDebit: true };
    }
    return null;
  }

  private validateCreditForm = (formGroup: FormGroup) => {
    const paymentTypeId = formGroup.get('paymentTypeId').value;
    const price = formGroup.get('price').value;
    if (!this.isDebit && (!paymentTypeId || !price)) {
      return { invalidCredit: true };
    }
    return null;
  }
}
