import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BillingAccountWithAccount } from 'app/shared/models/billing-account/billing-account-with-account';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { ConfigLinkToTab } from 'app/shared/models/config-link-to-tab';
import { BillingAccountWithEntry } from 'app/shared/models/billing-account/billing-account-with-entry';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { KeyValue } from 'app/shared/models/key-value';
import { BillingAccountStatusEnum } from 'app/shared/models/billing-account/billing-account-status-enum';
import { BillingAccountClosurePartial } from 'app/shared/models/billing-account/billing-account-closure-partial';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { PaymentTypeWithBillingItem } from 'app/shared/models/billingType/payment-type-with-billing-item';
import { ConfirmModalDataConfig } from 'app/shared/models/confirm-modal-data-config';
import { BillingAccountClosureMapper } from 'app/shared/mappers/billing-account-closure-mapper';
import { BillingAccountItemMapper } from 'app/shared/mappers/billing-account-item-mapper';
import { DateService } from 'app/shared/services/shared/date.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { BillingAccountItemService } from 'app/shared/services/billing-account-item/billing-account-item.service';
import { BillingAccountResource } from 'app/shared/resources/billing-account/billing-account.resource';
import { BillingAccountClosureResource } from 'app/shared/resources/billing-account/billing-account-closure.resource';
import { NoWhitespaceValidator } from 'app/shared/validators/no-whitespace-validator';
import { LocationManagerService } from '../../../../../projects/bifrost/src/lib/location-manager/location-manager.service';
import { AuthService } from 'app/shared/services/auth-service/auth.service';
import { CountryCodeEnum } from '../../../../../projects/bifrost/src/lib/location-manager/models';
import { FiscalDocumentsService } from 'app/shared/services/billing-account/fiscal-documents.service';
import { PdfPrintService } from 'app/shared/services/pdf-print/pdf-print.service';
import { ReservationStatusEnum } from 'app/shared/models/reserves/reservation-status-enum';

// TODO: create tests for component [DEBIT: 7517]
@Component({
  selector: 'app-billing-account-detail',
  templateUrl: './billing-account-detail.component.html',
  styleUrls: ['./billing-account-detail.component.css'],
})
export class BillingAccountDetailComponent implements OnInit, OnChanges {
  @Input() propertyId: number;
  @Input() billingAccountWithAccount: BillingAccountWithAccount;
  @Input() reservationItemId: number;
  @Input() billingAccountList: any[];
  @Input('reservationStatus')
  set reservationStatus (reservationStatus: number) {
    this._reservationStatus = reservationStatus;
    this.setConfigLinkToAdd();
  }
  get reservationStatus(): number {
    return this._reservationStatus;
  }

  @Output() updateBillingAccountAndHeaderAndCardList = new EventEmitter<string>();

  public _reservationStatus: number;
  public billingAccountWithEntryCurrent: BillingAccountWithEntry;
  public accountModalForm: FormGroup;
  public setDefaultActionButtonsToEntries: boolean;
  public referenceToBillingAccountStatusEnum = BillingAccountStatusEnum;
  public paymentForm: FormGroup;
  public billingAccountCurrentId: string;
  public paymentTypeWithBillingItemList: Array<PaymentTypeWithBillingItem>;
  public billingAccountSelectedToPayment: BillingAccountWithEntry;
  public isPaymentPartial: boolean;
  public billingAccountGuidList = new Array<string>();
  public billingAccountItemGuidList = new Array<string>();
  public billingAccountPartialName: string;
  public paymentPartialTotal: number;
  public billingAccountClosurePartial: BillingAccountClosurePartial;
  private billingAccountIdToBeRemoved: string;
  public confirmModalDataConfig: ConfirmModalDataConfig;
  public showConfirmModal: boolean;

  // Modal dependencies
  public paymentModalId: string;
  private reversePostModalId: string;
  private discountModalId: string;
  public modalTitle: string;

  // Table dependencies
  @ViewChild('myTable') table: DatatableComponent;
  widthColumns = Array<number>();

  // Buttons dependencies
  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;

  // Tab dependencies
  public configLinkToAdd: ConfigLinkToTab;
  public itemsTab: KeyValue[];

  public billingAccountWithEntrySelected: Array<BillingAccountWithEntry>;

  constructor(
    private dateService: DateService,
    private _route: ActivatedRoute,
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private toasterEmitService: ToasterEmitService,
    private billingAccountItemService: BillingAccountItemService,
    private formBuilder: FormBuilder,
    private translateService: TranslateService,
    private billingAccountResource: BillingAccountResource,
    private router: Router,
    private route: ActivatedRoute,
    private billingAccountClosureMapper: BillingAccountClosureMapper,
    private billingAccountClosureResource: BillingAccountClosureResource,
    private billingAccountItemMapper: BillingAccountItemMapper,
    private locationManager: LocationManagerService,
    private authService: AuthService,
    private fiscalDocumentService: FiscalDocumentsService,
    private pdfPrintService: PdfPrintService,
  ) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setVars();
  }

  ngOnChanges(changes: any) {
    this.billingAccountCurrentId = this.route.snapshot.queryParams.billingAccountId;
    this.setItemsTab();
    this.setDefaultActionButtonsToEntries = true;
  }

  public setActionsButtonsForEntries() {
    this.setDefaultActionButtonsToEntries = true;
  }

  private setVars(): void {
    this.setDefaultActionButtonsToEntries = false;
    this.isPaymentPartial = false;
    this.setButtonConfig();
    this.setFormModal();
    this.modalTitle = 'commomData.createAccount';
    this.reversePostModalId = 'reverse-post';
    this.discountModalId = 'discount-modal';
    this.paymentModalId = 'payment-modal';
    this.setItemsTab();
    this.setPaymentForm();
    this.billingAccountSelectedToPayment = new BillingAccountWithEntry();
    this.showConfirmModal = false;
    this.confirmModalDataConfig = new ConfirmModalDataConfig();
  }

  private setPaymentForm(): void {
    this.paymentForm = this.formBuilder.group({
      paymentTypeId: [null, [Validators.required]],
      card: this.formBuilder.group({
        plasticBrandPropertyId: null,
        nsuCode: null,
        installmentsNumber: 1,
      }),
      bankCheck: this.formBuilder.group({
        number: null,
      }),
      price: [null, [Validators.required]],
    });
  }

  private setConfigLinkToAdd(hasLink = true): void {
    if (this.reservationStatus === ReservationStatusEnum.Checkout) {
      this.configLinkToAdd = null;
    } else if (hasLink) {
      this.configLinkToAdd = <ConfigLinkToTab>{
        hasLink: this.reservationStatus >= ReservationStatusEnum.Checkin,
        linkText: 'label.account',
        linkAction: this.canCreateNewAccount,
        hasHeightLimit: true,
      };
    } else {
      this.configLinkToAdd = <ConfigLinkToTab>{
        hasLink: false,
        linkText: '',
      };
    }
  }

  private setItemsTab(): void {
    if (this.billingAccountWithAccount && this.billingAccountWithAccount.billingAccounts.length > 0) {
      this.itemsTab = [];
      this.billingAccountWithAccount.billingAccounts.forEach((billingAccount) => {
        this.itemsTab.push({
            key: billingAccount.billingAccountId,
            value: billingAccount.billingAccountItemName
          });
      });
      this.setTab();
    }
  }

  private setTab() {
    this.billingAccountWithEntryCurrent = this.billingAccountWithAccount.billingAccounts
      .find( item => item.billingAccountId == this.billingAccountCurrentId);
  }

  private setFormModal() {
    this.accountModalForm = this.formBuilder.group({
      accountName: [null, [Validators.required, NoWhitespaceValidator.match]],
    });

    this.accountModalForm.valueChanges.subscribe(() => {
      this.buttonSaveModalConfig.isDisabled = this.accountModalForm.invalid;
    });
  }

  private canCreateNewAccount = () => {
    if (this.reservationStatus === ReservationStatusEnum.Checkout) {
      this.toasterEmitService
        .emitChange(SuccessError.error, this.translateService.instant('text.reservationCheckoutError'));
    } else {
      this.toggleModal();
    }
  }

  private toggleModal() {
    this.accountModalForm.reset();
    this.modalEmitToggleService.emitToggleWithRef('addAccount');
  }

  public closePaymentModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.paymentModalId);
  }

  private setButtonConfig(): void {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'cancelModal',
      () => this.toggleModal(),
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
      'saveModal',
      this.createNewAccount,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
  }

  private createNewAccount = () => {
    if (!this.isPaymentPartial) {
      this.billingAccountResource
        .createBillingAccountWithEntry(
          this.billingAccountWithAccount.mainBillingAccountId,
          this.accountModalForm.get('accountName'
          ).value)
        .subscribe(response => {
          this.onUpdateBillingAccountAndHeaderAndCardList();
          this.toggleModal();
        });
    } else {
      this.billingAccountPartialName = this.accountModalForm.get('accountName').value;
      this.paymentPartialTotal = 0;
      this.modalEmitToggleService.emitToggleWithRef('addAccount');
      this.modalEmitToggleService.emitToggleWithRef(this.paymentModalId);
    }
  }

  private setIsCheckedToFalseOfbillingAccounts(): void {
    this.billingAccountWithAccount.billingAccounts.forEach(item => {
      item.billingAccountItems.forEach(billingAccountItem => (billingAccountItem.isChecked = false));
    });
    this.billingAccountItemService.deleteBillingAccountItemListInStore();
  }

  public onUpdateBillingAccountAndHeaderAndCardList(): void {
    this.updateBillingAccountAndHeaderAndCardList.emit(this.billingAccountWithAccount.mainBillingAccountId);
    this.setIsCheckedToFalseOfbillingAccounts();
  }

  public updateBillingAccountWithEntryCurrent(id: string): void {
    this.billingAccountCurrentId = id;
    this.updateUrl(id);
  }

  private updateUrl( id ) {
    const currentBillingAccountId = this.route.snapshot.queryParams.billingAccountId;
    if (id != currentBillingAccountId ) {
      this.router.navigate(
      [],
      {
        queryParams: { billingAccountId: id },
        replaceUrl: true,
        relativeTo: this.route
      } );
    }
  }

  public openLaunchModal() {
    this.modalEmitToggleService.emitToggleWithRef('post-debit-credit');
  }

  public goToBillingLauncher() {
    this
      .router
      .navigate([`/p/${this.propertyId}/billing-account/launcher`], {
        relativeTo: this.route,
        queryParams: {
          billingAccountId: this.billingAccountWithEntryCurrent.billingAccountId,
          reservationItemId: this.reservationItemId,
          accountName: this.billingAccountWithAccount.holderName
        },
      });
  }

  public removeBillingAcountItem(billingAccountId: string) {
    this.modalEmitToggleService.emitToggleWithRef('confirm-remove-account-modal');
    this.billingAccountIdToBeRemoved = billingAccountId;
    this.setConfirmModalConfig();
  }

  private setConfirmModalConfig() {
    this.showConfirmModal = true;
    this.confirmModalDataConfig.textConfirmModal = 'text.confirmRemoveBillingAccount';
    this.confirmModalDataConfig.actionCancelButton = this.sharedService.getButtonConfig(
      'cancelModal',
      this.cancelConfirmModal,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.confirmModalDataConfig.actionConfirmButton = this.sharedService.getButtonConfig(
      'confirmModal',
      this.confirmConfirmModal,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
    );
  }

  private confirmConfirmModal = () => {
    this.billingAccountResource.deleteBillingAccount(this.billingAccountIdToBeRemoved).subscribe(response => {
      this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant('variable.deletedF'));
      window.location.reload();
    });
  }

  private cancelConfirmModal = () => {
    this.modalEmitToggleService.emitToggleWithRef('confirm-remove-account-modal');
  }

  public transferBillingAccountItem(currentBillingAccountId) {
    const billingAccountItemCheckedList = this.billingAccountItemService.getBillingAccountWithEntryCheckedList(
      this.billingAccountWithAccount,
    );
    this.billingAccountItemService.addBillingAccountItemListInStore(billingAccountItemCheckedList);
    this.router.navigate(['../transfer'], {
      relativeTo: this.route,
      queryParams: {billingAccountId: currentBillingAccountId},
    });
  }

  public makePayment() {
    this.billingAccountClosurePartial = new BillingAccountClosurePartial();
    this.billingAccountClosurePartial.billingAccountList = this.billingAccountGuidList;
    this.billingAccountClosurePartial.billingAccountItemList = this.billingAccountItemGuidList;
    this.billingAccountClosurePartial.billingAccountName = this.billingAccountPartialName;
    this.billingAccountClosurePartial.credit = this.billingAccountItemMapper.toBillintAccountItemForPayment(
      this.paymentForm,
      this.propertyId,
      this.billingAccountCurrentId,
      this.paymentTypeWithBillingItemList,
    );
    this.billingAccountClosurePartial.propertyId = this.propertyId;
    this.billingAccountClosurePartial.id = GuidDefaultData;
    this
      .billingAccountClosureResource
      .payPartialBillingAccount(
        this.billingAccountClosurePartial
      ).subscribe(response => {
        this.updateBillingAccountAndHeaderAndCardList.emit(this.billingAccountCurrentId);
        this.modalEmitToggleService.emitToggleWithRef(this.paymentModalId);

        if ( response ) {
          const token = this.authService.getPropertyToken().token;
          const invoiceList = response['invoiceList'];
          if ( token
            && invoiceList.length
            && this.locationManager.couldShow([CountryCodeEnum.PT], token)) {
            this.printInvoiceNote(invoiceList[0].invoiceId);
          }
        }
    });
  }

  public printInvoiceNote(billingInvoiceId: string) {
    this
      .fiscalDocumentService
      .getNfse(billingInvoiceId)
      .subscribe(
        (byteCodes: any) => {
          if ( byteCodes ) {
            this.pdfPrintService.printPdfFromByteCode(byteCodes);
          }
        }, () => {
          this.showMessagePrintInvoice();
        });
  }

  public showMessagePrintInvoice() {
    this
      .toasterEmitService
      .emitChange(
        SuccessError.error,
        this.translateService.instant('alert.errorPrintInvoice')
      );
  }

  public updatePaymentTypeWithBillingItemList(paymentTypeWithBillingItemList: Array<PaymentTypeWithBillingItem>): void {
    this.paymentTypeWithBillingItemList = paymentTypeWithBillingItemList;
  }

  public paymentPartialBillingAccountItem(currentBillingAccountId: any) {
    const billingAccountItemCheckedList = this.billingAccountItemService.getBillingAccountWithEntryCheckedList(
      this.billingAccountWithAccount,
    );
    this.billingAccountItemService.addBillingAccountItemListInStore(billingAccountItemCheckedList);

    this.setBillingAccountClosurePartial(billingAccountItemCheckedList);
    this.toggleModalPartialPayment(true);
  }

  private toggleModalPartialPayment = (isPaymentPartial?: boolean) => {
    if (this.billingAccountWithAccount.statusId == BillingAccountStatusEnum.Closed && this.billingAccountWithAccount.isHolder) {
      this.toasterEmitService.emitChange(SuccessError.error, this.translateService.instant('variable.mainAccountClosed'));
    } else {
      this.isPaymentPartial = isPaymentPartial;
      this.updateBillingAccountAndHeaderAndCardList.emit(this.billingAccountWithAccount.mainBillingAccountId);
      this.modalEmitToggleService.emitToggleWithRef('addAccount');
      this.setFormModal();
    }
  }

  private setBillingAccountClosurePartial(billingAccountItemCheckedList: Array<BillingAccountWithEntry>) {
    this.billingAccountGuidList = new Array<string>();
    this.billingAccountItemGuidList = new Array<string>();
    this.paymentPartialTotal = 0;
    billingAccountItemCheckedList.forEach(billingAccountItemChecked => {
      this.billingAccountGuidList.push(billingAccountItemChecked.billingAccountId);
      billingAccountItemChecked.billingAccountItems.forEach(billingAccountItem => {
        this.billingAccountItemGuidList.push(billingAccountItem.billingAccountItemId);
        this.paymentPartialTotal += billingAccountItem.total;
      });
    });
  }

  public reverseBillingAccountItem() {
    const billingAccountItemCheckedList = this.billingAccountItemService.getBillingAccountWithEntryCheckedList(
      this.billingAccountWithAccount,
    );

    this.billingAccountItemService.addBillingAccountItemListInStore(billingAccountItemCheckedList);
    this.modalEmitToggleService.emitToggleWithRef(this.reversePostModalId);
  }

  public discountBillingAccountItem() {
    const billingAccountItemCheckedList = this.billingAccountItemService.getBillingAccountWithEntryCheckedList(
      this.billingAccountWithAccount,
    );
    this.billingAccountWithEntrySelected = billingAccountItemCheckedList;
    this.modalEmitToggleService.emitToggleWithRef(this.discountModalId);
  }

  public closeDiscount() {
    this.modalEmitToggleService.emitToggleWithRef(this.discountModalId);
  }

  public onReopenBillingAccountUpdate(billingAccountId: string) {
    this.updateBillingAccountAndHeaderAndCardList.emit(billingAccountId);
  }

  public refreshInfo() {
    this.updateBillingAccountAndHeaderAndCardList.emit(this.billingAccountWithAccount.mainBillingAccountId);
  }

  public loadBillingAccountList(billingAccountId: string) {
    this.updateBillingAccountAndHeaderAndCardList.emit(billingAccountId);
  }
}
