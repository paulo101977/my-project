import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { BillingMoneyDebitCheckContainerComponent } from './billing-money-debit-check-container.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { currencyFormatPipe } from '../../../../../testing';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { billingItemResourceStub } from 'app/shared/resources/billingItem/testing';
import { currencyExchangeResourceStub } from 'app/shared/resources/currency-exchange/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { billingLauncherItemServiceStub } from 'app/billing-account/services/testing';
import { billingAccountItemResourceStub } from 'app/shared/resources/billing-account-item/testing';


describe('BillingMoneyDebitCheckContainerComponent', () => {
  let component: BillingMoneyDebitCheckContainerComponent;
  let fixture: ComponentFixture<BillingMoneyDebitCheckContainerComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        BillingMoneyDebitCheckContainerComponent,
        currencyFormatPipe,
      ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      providers: [
        sharedServiceStub,
        toasterEmitServiceStub,
        billingItemResourceStub,
        currencyExchangeResourceStub,
        billingLauncherItemServiceStub,
        billingAccountItemResourceStub,
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(BillingMoneyDebitCheckContainerComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();

    spyOn<any>(component['route'].snapshot.queryParamMap, 'get').and.returnValue(1);
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngInit', fakeAsync(() => {
    spyOn<any>(component, 'setForm');
    spyOn<any>(component, 'setButtonConfig');
    spyOn<any>(component, 'setComponents');
    spyOn<any>(component, 'getPaymentList').and.returnValue(of(null));
    spyOn<any>(component['route'], 'params').and.returnValue(of({propertyId: 1}));
    spyOn<any>(component['billingLauncherItemService'], 'getCurrencyId').and.returnValue(3);
    spyOn<any>(component['billingLauncherItemService'], 'getCurrencyList').and.returnValue(null);
    spyOn<any>(component, 'getQuotationList').and.returnValue(of(null));

    component.ngOnInit();
    tick();

    expect(component.setForm).toHaveBeenCalled();
    expect(component.setButtonConfig).toHaveBeenCalled();
    expect(component.setComponents).toHaveBeenCalled();
    expect(component.getPaymentList).toHaveBeenCalled();
    expect(component['billingLauncherItemService'].getCurrencyId).toHaveBeenCalled();
    expect(component['billingLauncherItemService'].getCurrencyList).toHaveBeenCalled();
    expect(component.getQuotationList).toHaveBeenCalled();
  }));

  it('should test calculate amout', () => {
    const value = 5;
    spyOn<any>(component['billingLauncherItemService'], 'applyQuotationInValue').and.returnValue(value);
    component.calculateAmount(value);

    expect(component.totalAmount).toEqual(value);
  });
});
