import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { CurrencyExchangeResource } from 'app/shared/resources/currency-exchange/currency-exchange.resource';
import { BillingAccountItem } from 'app/shared/models/billing-account/billing-account-item';
import { BillingAccountItemResource } from 'app/shared/resources/billing-account-item/billing-account-item.resource';
import { BillingItemResource } from 'app/shared/resources/billingItem/billing-item.resource';
import { PaymentTypeWithBillingItem } from 'app/shared/models/billingType/payment-type-with-billing-item';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { TranslateService } from '@ngx-translate/core';
import { BillingLauncherItemService } from 'app/billing-account/services/billing-launcher-item-service';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { PaymentTypeEnum } from 'app/shared/models/reserves/payment-type-enum';


@Component({
  selector: 'billing-money-debit-check-container',
  templateUrl: './billing-money-debit-check-container.component.html',
  styleUrls: ['./billing-money-debit-check-container.component.scss']
})
export class BillingMoneyDebitCheckContainerComponent implements OnInit {

  private readonly messageSuccess = 'text.launcherCreditSuccessText';

  // the enum selected
  public key: PaymentTypeEnum;

  // has info select
  public hasInfoSelect: boolean;
  public labelText: string;
  public infoText: string;

  public form: FormGroup;

  public propertyExchangeRate: number;
  public currentValue: number;
  public totalAmount: number;
  public inputValue: number;
  public propertyId: number;
  public billingAccountId: string;
  public paymentList: Array<PaymentTypeWithBillingItem>;

  public currencySelectedId: string;
  public propertyCurrencyId: string;
  public propertyExchangeRateId: number;
  public currentExchangeRateId: number;
  public quotationList: Array<any>;

  // Button Dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;
  public configHeaderPage: ConfigHeaderPageNew;
  public billingItemId: number;
  public currencyId: string;
  public currencySymbol: string;
  public currencyList: Array<any>;

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private currencyExchangeResource: CurrencyExchangeResource,
    private billingAccountItemResource: BillingAccountItemResource,
    private billingItemResource: BillingItemResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private billingLauncherItemService: BillingLauncherItemService,
  ) { }


  public saveData = () => {
    const {
      propertyId,
      totalAmount,
      billingAccountId,
      key,
      form,
      inputValue,
      currencySelectedId,
      billingItemId
    } = this;

    const serviceDescription = form.get('info').value;
    const intKey = +key;
    const description = this.billingLauncherItemService.translateFromEnum(intKey);

    const obj = <BillingAccountItem>{
      propertyId,
      amount: inputValue,
      amountChange: totalAmount,
      billingAccountId,
      id: GuidDefaultData,
      billingItemId,
      checkNumber: '0',
      serviceDescription,
      externalId: intKey,
      currencyId: currencySelectedId,
      currencyExchangeReferenceId: this.currencySelectedId != this.propertyCurrencyId ? this.currentExchangeRateId : null,
      currencyExchangeReferenceSecId: this.currencySelectedId != this.propertyCurrencyId ? this.propertyExchangeRateId : null,
      billingAccountItemDate: null,
      billingItemName: null,
      creditAmount: null,
      billingInvoiceId: null,
      detailList: [
        {
          code: '',
          amount: inputValue,
          amountChange: totalAmount,
          description,
          quantity: 1,
          groupDescription: description,
          externalId: intKey,
        }
      ]
    };

    this
      .billingAccountItemResource
      .createBillingAccountItemCredit(obj)
      .subscribe( () => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant(this.messageSuccess),
        );

        this.redirectToAccountEdit(billingAccountId);
      });
  }

  public redirectToAccountEdit(billingAccountId) {
    this.router.navigate(
      [`/p/${this.propertyId}/billing-account/edit/`],
      {
        relativeTo: this.route,
        queryParams: {
          billingAccountId
        }
      }
    );
  }


  public cancelData = () => {
    this.goBack();
  }

  // set app-buttons
  public setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-button',
      this.cancelData,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'save-button',
      this.saveData,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal
    );
  }

  public setForm() {
    this.form = this.formBuilder.group({
      info: null,
      price: [ null, [ Validators.required ]]
    });

    this
      .form
      .get('price')
      .valueChanges
      .subscribe( value => {
        this.buttonConfirmConfig.isDisabled = !(this.form.valid && !isNaN(value));
      });
  }


  public calculateAmount = ( value ) => {
    this.inputValue = value;
    this.totalAmount = this.billingLauncherItemService
      .applyQuotationInValue(value, this.currencySelectedId, this.propertyCurrencyId, this.quotationList);
  }

  public moneySymbolChanged(item) {
    const id = item.key;
    const { quotationList } = this;
    this.currencySelectedId = id;

    if ( quotationList ) {
      const element = this.quotationList.find(q => q.currencyId == item.key);
      this.currentExchangeRateId = element.id;

      if (this.currentValue !== undefined) {
        this.calculateAmount(this.currentValue);
      }
    }
  }

  public valuesChange( value ) {
    this.currentValue = value;

    this.calculateAmount(value);
  }

  public goBack() {
    this.location.back();
  }

  public setComponents() {
    switch (this.key) {
      case PaymentTypeEnum.Money:
        this.hasInfoSelect = false;
        this.labelText = 'label.typeTheValueOfCredit';
        return;
      case PaymentTypeEnum.Deposit:
        this.hasInfoSelect = true;
        this.labelText = 'label.creditValue';
        this.infoText = 'label.depositInformation';
        return;
      case PaymentTypeEnum.Check:
        this.hasInfoSelect = true;
        this.labelText = 'label.creditValue';
        this.infoText = 'label.checkInformation';
        return;
    }
  }

  public getPaymentList(): Promise<any> {
    return new Promise( (resolve => {
      this
        .billingItemResource
        .getAllPaymentTypeWithBillingItemListByPropertyId(this.propertyId)
        .subscribe( ({ items }) => {

          this.paymentList = items;
          resolve(items);
        });
    }));
  }

  public getBillingItemId(key: number) {
    const fItem = this.paymentList.find( item => item.id === key );

    if ( fItem ) {
      return fItem.billingItemId;
    }

    return 0;
  }


  public setCurrencySymbol() {
    const { currencyList, propertyCurrencyId } = this;

    if ( currencyList && propertyCurrencyId ) {
      const finded = currencyList.find( item => item.id === propertyCurrencyId) || {};
      this.currencySymbol = finded.symbol;
    }
  }

  ngOnInit() {
    this.billingAccountId = this.route.snapshot.queryParamMap.get('billingAccountId');
    this.key = +this.route.snapshot.queryParamMap.get('key');
    this.billingItemId = +this.route.snapshot.queryParamMap.get('billingItemId');

    if (!this.key) {
      this.goBack();
    }

    this.setForm();
    this.setButtonConfig();

    this.setComponents();

    this.route.params.subscribe(async params => {
      this.propertyId = +params.property;
      this.propertyCurrencyId = this.billingLauncherItemService.getCurrencyId(this.propertyId);
      await this.getQuotationList();

      await this.getPaymentList();


      this.currencyList = await this.billingLauncherItemService.getCurrencyList();
      this.setCurrencySymbol();
    });
  }

  public getQuotationList(): Promise<any> {
    return new Promise<any>( resolve => {
      this.currencyExchangeResource.getQuotationList()
        .subscribe( item => {
          this.quotationList = item;

          if ( this.quotationList ) {
            const propertyQuotation = this.quotationList.find(q => q.currencyId == this.propertyCurrencyId);
            if (propertyQuotation) {
              this.propertyExchangeRateId = propertyQuotation.id;
            }
          }

          resolve(null);
        });
    });
  }
}

