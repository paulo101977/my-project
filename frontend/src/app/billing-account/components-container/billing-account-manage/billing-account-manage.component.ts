import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BillingAccountSidebar } from '../../../shared/models/billing-account/billing-account-sidebar';
import { BillingAccountHeader } from '../../../shared/models/billing-account/billing-account-header';
import { BillingAccountCard } from '../../../shared/models/billing-account/billing-account-card';
import { BillingAccountWithAccount } from '../../../shared/models/billing-account/billing-account-with-account';
import { ShowHide } from './../../../shared/models/show-hide-enum';
import { ItemsResponse } from './../../../shared/models/backend-api/item-response';
import { ShowHideContentEmitService } from './../../../shared/services/shared/show-hide-content-emit-service';
import { LoadPageEmitService } from './../../../shared/services/shared/load-emit.service';
import { BillingAccountService } from './../../../shared/services/billing-account/billing-account.service';
import { BillingAccountResource } from '../../../shared/resources/billing-account/billing-account.resource';

@Component({
  selector: 'billing-account-edit',
  templateUrl: 'billing-account-manage.component.html',
})
export class BillingAccountManageComponent implements OnInit {
  public propertyId: number;
  public billingAccountHeader: BillingAccountHeader;
  public billingAccountCardList: Array<BillingAccountCard>;
  public billingAccountSidebarList: Array<BillingAccountSidebar>;
  public billingAccountWithAccount: BillingAccountWithAccount;
  public billingAccountCurrentId: string;

  constructor(
    private _route: ActivatedRoute,
    private router: Router,
    private showHideContentEmitService: ShowHideContentEmitService,
    private billingAccountResource: BillingAccountResource,
    private loadPageEmitService: LoadPageEmitService,
    private billingAccountService: BillingAccountService,
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      this._route.queryParams.subscribe(queryParams => {
        this.billingAccountCurrentId = queryParams.billingAccountId;
        this.propertyId = +params.property;
        this.loadPageEmitService.emitChange(ShowHide.show);
        this.setBillingAccountAndHeaderAndCardList(this.billingAccountCurrentId);
      });
    });
  }

  private getBillingAccountHeader(): void {
    this.billingAccountResource.getHeaderByBillingAccountId(this.billingAccountCurrentId).subscribe(response => {
      if (response) {
        this.billingAccountHeader = response;
      }
    });
  }

  public hideAllPopovers(): void {
    this.showHideContentEmitService.emitChange(ShowHide.hide);
  }

  public setBillingAccountAndHeaderAndCardList(billingAccountId: string): void {
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.getBillingAccountHeader();
    this.getGuestBillingAccountSidebarListAndSetBillingAccountCurrent(billingAccountId);
  }

  private getGuestBillingAccountSidebarListAndSetBillingAccountCurrent(billingAccountId: string): void {
    if (
      this.billingAccountCardList &&
      this.billingAccountCardList.length > 0 &&
      this.billingAccountService.billingAccountCardIsGroupAccount(this.billingAccountCardList, billingAccountId)
    ) {
      this.billingAccountResource.getAllSidebarByBillingAccountId(this.billingAccountCurrentId).subscribe(response => {
        if (response) {
          this.resolveResponseOfGetAllSidebar(response, billingAccountId);
        }
      });
    } else {
      this.billingAccountResource.getAllSidebarByBillingAccountId(billingAccountId).subscribe(response => {
        if (response) {
          this.resolveResponseOfGetAllSidebar(response, billingAccountId);
        }
      });
    }
  }

  private resolveResponseOfGetAllSidebar(response: ItemsResponse<BillingAccountSidebar>, billingAccountId: string) {
    this.setBillingAccountSidebarOfCardList(response);
    this.setCardToActive(billingAccountId);
    this.setBillingAccount(billingAccountId);
  }

  private setBillingAccountSidebarOfCardList(response): void {
    this.billingAccountCardList = new Array<BillingAccountCard>();
    this.billingAccountSidebarList = new Array<BillingAccountSidebar>();
    const billingAccountSidebarList: Array<BillingAccountSidebar> = response.items;
    billingAccountSidebarList.forEach(billingAccountSidebar => {
      const billingAccountCard = new BillingAccountCard();
      billingAccountCard.billingAccountSidebar = billingAccountSidebar;
      this.billingAccountSidebarList.push(billingAccountSidebar);
      this.billingAccountCardList.push(billingAccountCard);
    });
  }

  private setCardToActive(billingAccountId: string): void {
    let billingAccountCardActive = this.billingAccountCardList.find(b => b.billingAccountSidebar.billingAccountId == billingAccountId);

    if (!billingAccountCardActive) {
      this.billingAccountCardList.forEach( item => {
        item.isActive = false;
          const element = item.billingAccounts.find( account => account.billingAccountId == billingAccountId);
          if (element) {
            billingAccountCardActive = item;
          }
      });
    }
    if (billingAccountCardActive) {
      billingAccountCardActive.isActive = true;
    }
  }

  public setBillingAccount(billingAccountId: string): void {
    this.billingAccountCurrentId = billingAccountId;
    this.getBillingAccountHeader();

    this.billingAccountResource.getBillingAccountWithAccountById(billingAccountId).subscribe(response => {
      if (response) {
        this.loadPageEmitService.emitChange(ShowHide.hide);
        let element = this.billingAccountCardList.find( account => account.billingAccountSidebar.billingAccountId == billingAccountId);
        if (!element) {
          element = this.billingAccountCardList.find( account => account.billingAccountSidebar.billingAccountId == response.groupKey);
        }
        this.billingAccountWithAccount = response;
        element.billingAccounts = this.billingAccountWithAccount.billingAccounts;
        this.setCardToActive(billingAccountId);
        this.updateUrl(billingAccountId);
      }
    });
  }

  private updateUrl( id ) {
    const currentBillingAccountId = this._route.snapshot.queryParams.billingAccountId;
    if (id != currentBillingAccountId ) {
      this.router.navigate(
        [],
        {
          queryParams: { billingAccountId: id },
          replaceUrl: true,
          relativeTo: this._route
        } );
    }
  }

  public reload() {
    this.loadPageEmitService.emitChange(ShowHide.show);
    this.getBillingAccountHeader();
    this.setBillingAccountAndHeaderAndCardList(this.billingAccountCurrentId);
  }
}
