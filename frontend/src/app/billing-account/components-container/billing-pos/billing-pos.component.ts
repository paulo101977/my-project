import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/shared/services/user/user.service';
import { FavoriteModel } from 'app/shared/models/favorites';
import { PosProduct } from 'app/products/models/products/products';
import { FavoriteResource } from 'app/shared/resources/favorite/favorite.resource';
import { ProductsResource } from 'app/shared/resources/products/products.resource';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-billing-pos',
  templateUrl: './billing-pos.component.html',
  styleUrls: ['./billing-pos.component.css']
})
export class BillingPosComponent implements OnInit {

  // common properties
  private propertyId: number;
  private userId: string;

  // data list
  public posList: Array<PosProduct>;
  private posListBack: Array<PosProduct>;
  private favList: Array<FavoriteModel>;
  private billingAccountId: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private productsResource: ProductsResource,
    private favoriteResource: FavoriteResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
  ) { }


  public search($event) {
    const { value } = $event.target;

    if (typeof value === 'string' && value != '') {
      const regex = new RegExp(value, 'i');
      this.posList = this
        .posListBack
        .filter(({billingItemName, id}) => regex.test(billingItemName) || regex.test(id));
    } else {
      this.posList = this.posListBack;
    }
  }

  private loadPosData(): Promise<Array<PosProduct>> {
    return new Promise<Array<PosProduct>>( resolve => {
      this
        .productsResource
        .getAllPos()
        .subscribe( ({ items })  => {
          this.posList = items;
          this.posListBack = this.posList;

          resolve(items);
        });
    });
  }

  public loadAllFav(): Promise<Array<FavoriteModel>> {
    return new Promise<Array<FavoriteModel>>( resolve => {
      this
        .favoriteResource
        .getBillingItemsFav(this.userId)
        .subscribe( ({ items }) => {
          this.favList = items;
          resolve(items);
        });
    });
  }


  public setFavs() {
    if ( this.posList
      && this.favList
      && this.favList.length > 0) {

      this.favList[0].details.forEach( fav  => {
        const item = this.posList.find( pItem => fav.billingItemId === pItem.id);

        if ( item ) {
          item.isFavorite = true;
        }

      });
    }
  }

  public favoriteChanged(obj) {
    const { id, isFavorite } = obj;
    const { userId } = this;

    let message = 'text.posPreferenceDisableSuccessText';

    if ( isFavorite ) {
      message = 'text.posPreferenceActiveSuccessText';
    }

    const item = <FavoriteModel>{
      userId,
      details: [{
        billingItemId: id
      }]
    };


    this
      .favoriteResource
      .setBillingItemFav(item, isFavorite)
      .subscribe( () => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant(message),
        );
      });
  }


  public itemClicked(item) {
    const  { billingAccountId } = this;
    if (item) {
      const { id } = item;

      this.router.navigate(
        [`/p/${this.propertyId}/billing-account/pos/${id}`],
        {
          relativeTo: this.route,
          queryParams: {
            billingAccountId
          }
        }
      );
    }
  }

  ngOnInit() {
    const user = this.userService.getUserLocalStorage();

    if ( user ) {
      this.userId = user.id;
    }

    this.route.params.subscribe( async ({ property }) => {
      this.propertyId = +property;
      this.billingAccountId = this.route.snapshot.queryParamMap.get('billingAccountId');

      // load data
      await  this.loadPosData();
      await this.loadAllFav();

      this.setFavs();
    });
  }
}
