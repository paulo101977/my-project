import { createServiceStub } from '../../../../../../testing';
import { BillingInvoiceResource } from 'app/shared/resources/Invoice/billing-invoice.resource';
import { BillingAccountItemCreditAmountDto } from 'app/shared/models/dto/billing-account-item/billing-account-item-credit-amount-dto';
import { Observable, of } from 'rxjs';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';

export const billingInvoiceResourceStub = createServiceStub(BillingInvoiceResource, {
    createCreditNote(billingAccountItemList: Array<BillingAccountItemCreditAmountDto>): Observable<any> {
        return of( null );
    },

    cancelInvoice(invoice: InvoiceDto, reasonId: number): Observable<any> {
        return of( null );
    },

    getAllByFilters(propertyId: number, invoiceSearchDto: any): Observable<BlobPart> {
        return of( null );
    },

    getInvoicesFromBillingAccountId(id: string): Observable<Array<InvoiceDto>> {
        return of( null );
    },

    resendInvoice(propertyId: number, invoice: InvoiceDto): Observable<any> {
        return of( null );
    },

    resendManyInvoices(propertyId: number, invoiceList: Array<InvoiceDto>): Observable<any> {
        return of( null );
    },
});
