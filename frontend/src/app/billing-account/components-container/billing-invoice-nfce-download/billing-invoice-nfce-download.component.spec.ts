import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BillingInvoiceNfceDownloadComponent } from './billing-invoice-nfce-download.component';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteStub, createAccessorComponent, createServiceStub } from '../../../../../testing';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ButtonConfig, ButtonSize, ButtonType, Type } from 'app/shared/models/button-config';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { billingInvoiceResourceStub } from 'app/billing-account/components-container/billing-invoice-nfce-download/testing';

const datePickerComponent = createAccessorComponent('thx-date-range-picker');

describe('BillingInvoiceNfceDownloadComponent', () => {
  let component: BillingInvoiceNfceDownloadComponent;
  let fixture: ComponentFixture<BillingInvoiceNfceDownloadComponent>;

  const sharedServiceStub = createServiceStub(SharedService, {
    getButtonConfig(
      id: string,
      callback: Function,
      textButton: string,
      buttonType: ButtonType,
      buttonSize?: ButtonSize,
      isDisabled?: boolean,
      type?: Type): ButtonConfig { return new ButtonConfig(); },
    getHeaders(): any { return ''; }
  });


  const toasterEmitServiceStub = createServiceStub(ToasterEmitService, {
    emitChange(isSuccess: SuccessError, message: string) {}
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        ReactiveFormsModule,
        FormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [
        BillingInvoiceNfceDownloadComponent,
        datePickerComponent,
      ],
      providers: [
        FormBuilder,
        sharedServiceStub,
        toasterEmitServiceStub,
        billingInvoiceResourceStub,
        ActivatedRouteStub,
      ]
    });

    fixture = TestBed.createComponent(BillingInvoiceNfceDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
