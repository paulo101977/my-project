import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ButtonConfig, ButtonSize, ButtonType} from 'app/shared/models/button-config';
import {SharedService} from 'app/shared/services/shared/shared.service';
import {BillingInvoiceResource} from 'app/shared/resources/Invoice/billing-invoice.resource';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {SuccessError} from 'app/shared/models/success-error-enum';
import {InvoiceSearchDto} from 'app/shared/models/dto/invoice/invoice-search-dto';
import {ActivatedRoute} from '@angular/router';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-billing-invoice-nfce-download',
  templateUrl: './billing-invoice-nfce-download.component.html'
})
export class BillingInvoiceNfceDownloadComponent implements OnInit {

  private propertyId: number;
  public invoiceDownloadForm: FormGroup;

  // Button dependencies
  public btnDownloadConfig: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private billingInvoiceResource: BillingInvoiceResource,
    private toasterEmitService: ToasterEmitService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setButtonConfig();
    this.setForm();
  }

  private setForm() {
    this.invoiceDownloadForm = this.formBuilder.group({
      period: ['', Validators.required],
      initialNumber: null,
      finalNumber: null
    });

    this.invoiceDownloadForm.valueChanges.subscribe(value => {
      this.btnDownloadConfig.isDisabled = this.invoiceDownloadForm.invalid;
    });
  }

  private setButtonConfig() {
    this.btnDownloadConfig = this.sharedService.getButtonConfig(
      'download-invoice-nfce',
      this.downloadInvoiceNfce,
      'label.makeDownload',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  private downloadInvoiceNfce = () => {
    const invoice = new InvoiceSearchDto();
    invoice.initialDate = this.invoiceDownloadForm.get('period').value.beginDate;
    invoice.finalDate = this.invoiceDownloadForm.get('period').value.endDate;
    invoice.initialNumber = this.invoiceDownloadForm.get('initialNumber').value;
    invoice.finalNumber = this.invoiceDownloadForm.get('finalNumber').value;
    invoice.isXml = true;

    this.billingInvoiceResource.getAllByFilters(this.propertyId, invoice).subscribe( (response: BlobPart) => {
      const blob = new Blob([response], {type: 'octet/stream'});
      const fileName = 'nfce_download.zip';
      saveAs(blob, fileName);
    });
  }

}
