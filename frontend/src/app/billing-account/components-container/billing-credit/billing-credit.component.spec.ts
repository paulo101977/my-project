import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';

import { BillingCreditComponent } from './billing-credit.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { PaymentTypeEnum } from 'app/shared/models/reserves/payment-type-enum';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { paymentTypeResourceStub } from 'app/shared/resources/payment-type/testing';

describe('BillingCreditComponent', () => {
  let component: BillingCreditComponent;
  let fixture: ComponentFixture<BillingCreditComponent>;
  const list = [{
    key: PaymentTypeEnum.CreditCard,
    billingItemId: 2,
    id: '456',
    paymentTypeName: 'crédito'
  },
    {
      key: PaymentTypeEnum.Debit,
      billingItemId: 4,
      id: '4564',
      paymentTypeName: 'débito'
    },
    {
      key: PaymentTypeEnum.Money,
      billingItemId: 5,
      id: '45644',
      paymentTypeName: 'Dinheiro'
    }];

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [BillingCreditComponent],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      providers: [
        paymentTypeResourceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(BillingCreditComponent);
    component = fixture.componentInstance;

    spyOn<any>(component, 'getPaymentTypes').and.returnValue(of(list));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOninit call', () => {

    component.ngOnInit();

    expect(component['getPaymentTypes']).toHaveBeenCalled();
  });


  it('should test if key changes on selectType', fakeAsync(() => {
    spyOn<any>(component['router'], 'navigate').and.returnValue(true);
    spyOn<any>(component, 'selectType').and.callThrough();
    spyOn<any>(component, 'changeRoute').and.callFake( () => {} );

    component.ngOnInit();
    component.selectType(list[2]);

    expect<any>(component.selectedItemKey).toEqual(PaymentTypeEnum.Money);
    expect(component.selectType).toHaveBeenCalledWith(list[2]);
    expect(component.changeRoute).toHaveBeenCalledWith(PaymentTypeEnum.Money, 5);
  }));

});
