import { Component, OnInit } from '@angular/core';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { CardsService } from '../../services/cards.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CardConfig } from '../../models/card-config';
import { CardsEnum } from '../../models/cards.enum';
import { PaymentTypeResource } from 'app/shared/resources/payment-type/payment-type.resource';
import { PaymentTypeEnum } from 'app/shared/models/reserves/payment-type-enum';

@Component({
  selector: 'billing-credit',
  templateUrl: './billing-credit.component.html',
  styleUrls: ['./billing-credit.component.scss']
})
export class BillingCreditComponent implements OnInit {


  public accountName: string;
  public cardConfig: Array<CardConfig>;
  public isFirstEntry = false;

  public selectedItemKey: CardsEnum;
  private billingAccountId: string;
  public configHeaderPage: ConfigHeaderPageNew;
  public propertyId: number;
  public key: number;

  constructor(
    private cardsService: CardsService,
    private router: Router,
    public route: ActivatedRoute,
    private paymentTypeResource: PaymentTypeResource
  ) { }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      backPreviewPageCallBackFunction: this.goToLauncher,
    };
  }

  private goToLauncher = () => {
    const { accountName, billingAccountId } = this;

    this.router.navigate(
      [`/p/${this.propertyId}/billing-account/launcher`],
      {
        relativeTo: this.route,
        queryParams: {
          accountName,
          billingAccountId
        }
      }
    );
  }


  public selectType(item) {
    this.selectedItemKey = item.key;
    this.changeRoute(+item.key, item.billingItemId);
  }

  public changeRoute(key: PaymentTypeEnum, billingItemId: number) {
    const url = this.cardsService.getRouteName(key);
    const { accountName, billingAccountId } = this;
    const currentPath = this.route.snapshot.children[0].url[0].path;

    if (url != currentPath) {
      this.router.navigate([url],
        {
          relativeTo: this.route,
          queryParams: {
            key,
            accountName,
            billingAccountId,
            billingItemId
          }
        });
    }
  }

  ngOnInit() {
    this.selectedItemKey = +this.route.snapshot.queryParamMap.get('selectedItemKey');
    this.selectedItemKey = this.key = +this.route.snapshot.queryParamMap.get('key');
    this.accountName = this.route.snapshot.queryParamMap.get('accountName');
    this.billingAccountId = this.route.snapshot.queryParamMap.get('billingAccountId');
    this.propertyId = +this.route.snapshot.params.property;
    this.getPaymentTypes();
    this.setConfigHeaderPage();
  }

  private getPaymentTypes() {
    this.paymentTypeResource.getPaymentsTypeUsed(this.propertyId).subscribe( result => {
      this.cardConfig = result.items
        .filter( payment => payment.id != PaymentTypeEnum.ToBeBilled)
        .map( payment => {
          return {
            billingItemId: payment.billingItemId,
            key: payment.id,
            value: payment.paymentTypeName,
            src: this.cardsService.getImageUrl(payment.id)
          };
        });
      if (this.isFirstLoad() ) {
        this.selectFirst();
      }
    });
  }

  private selectFirst () {
    if (this.cardConfig && this.cardConfig.length > 0) {
      this.selectType( this.cardConfig[0] );
    }
  }

  private isFirstLoad() {
    return !this.route.snapshot.queryParamMap.get('key');
  }
}
