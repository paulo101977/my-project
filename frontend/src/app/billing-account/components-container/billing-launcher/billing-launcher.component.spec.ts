
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { BillingLauncherComponent } from './billing-launcher.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ActivatedRouteStub, imgCdnStub, userServiceStub } from '../../../../../testing';
import { productsResourceStub } from 'app/shared/resources/products/testing';
import { favoriteResourceStub } from 'app/shared/resources/favorite/testing';
import { billingLauncherItemServiceStub, cardsServiceStub } from 'app/billing-account/services/testing';
import { billingItemCategoryResourceStub } from 'app/shared/resources/billing-item-category/testing';

describe('BillingLauncherComponent', () => {
  let component: BillingLauncherComponent;
  let fixture: ComponentFixture<BillingLauncherComponent>;

  const carouselData = { details: [
    {
      billingItemId: '75',
      code: '484592',
      description: 'Cheese Burger',
      externalId: '02e1f4b3-e5de-4dbb-93de-a9ad00ff4d0f',
      groupDescription: 'Hamburger Especial',
      price: 7.77,
      quantity: 6,
      service: 'Teste Ricardo',
      type: 1
    },
    {
      billingItemId: '1',
      code: '',
      description: 'Crédito',
      externalId: '5',
      groupDescription: 'Crédito',
      price: undefined,
      quantity: 6,
      service: 'Crédito',
      type: 2
    },
    {
      billingItemId: '92',
      code: '',
      description: 'Lavanderia',
      externalId: '17',
      groupDescription: 'Lavanderia',
      price: undefined,
      quantity: 1,
      service: 'Lavanderia',
      type: 0
    }
  ]};

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
          BillingLauncherComponent,
          imgCdnStub,
      ],
      imports: [
        TranslateTestingModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      providers: [
        productsResourceStub,
        ActivatedRouteStub,
        favoriteResourceStub,
        cardsServiceStub,
        userServiceStub,
        billingItemCategoryResourceStub,
        billingLauncherItemServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(BillingLauncherComponent);
    component = fixture.componentInstance;

    spyOn(component, 'loadCarouselData').and.callThrough();
    spyOn(component['favoriteResource'], 'getCardMoreSelectedData')
        .and
        .returnValue(of(carouselData));
    spyOn<any>(component['router'], 'navigate').and.callFake( () => {});

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load carousel', fakeAsync( () => {
    component.loadCarouselData();
    tick();

    expect(component.loadCarouselData).toHaveBeenCalled();
    expect(component.carouselData.length).toBe(1);
    expect(component.carouselData[0]['billingItemId']).toEqual(carouselData.details[1].billingItemId);
  }));

});
