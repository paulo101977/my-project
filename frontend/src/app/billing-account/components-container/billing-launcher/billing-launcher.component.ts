import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsResource } from 'app/shared/resources/products/products.resource';
import { UserService } from 'app/shared/services/user/user.service';
import { FavoriteResource } from 'app/shared/resources/favorite/favorite.resource';
import { LauncherMainEnum } from 'app/billing-account/models/launcher-main.enum';
import { BillingItemCategoryResource } from 'app/shared/resources/billing-item-category/billing-item-category.resource';
import { ProductsBillingItem } from 'app/billing-account/models/products-billing-item';
import { BillingLauncherItemService } from 'app/billing-account/services/billing-launcher-item-service';
import { CardsService } from '@app/billing-account/services/cards.service';
import { CardsEnum } from '@app/billing-account/models/cards.enum';



@Component({
  selector: 'app-billing-launcher',
  templateUrl: './billing-launcher.component.html',
  styleUrls: ['./billing-launcher.component.css']
})
export class BillingLauncherComponent implements OnInit {

  public accountName: string;
  private propertyId: number;
  private billingAccountId: string;
  private keyCredit: CardsEnum;

  // carousel
  // TODO: create model
  public carouselData: Array<any>;
  public carouselConfig: any;

  // common data
  public userId: string;

  public productBillingItemList: Array<ProductsBillingItem>;

  // list of fix groups
  public groupList: Array<any>;

  constructor(
    private productsResource: ProductsResource,
    private router: Router,
    private route: ActivatedRoute,
    private cardsService: CardsService,
    private userService: UserService,
    private favoriteResource: FavoriteResource,
    private billingItemCategoryResource: BillingItemCategoryResource,
    private billingLauncherItemService: BillingLauncherItemService,
  ) { }

  public setCarouselConfig() {
    this.carouselConfig = {
      autoWidth: true,
      items: '6',
      loop: true,
      mouseDrag: true,
      touchDrag: false,
      pullDrag: false,
      dots: false,
      navSpeed: 700,
      navText: ['', ''],
      responsive: {
        0: {
          items: 3
        },
        576: {
          items: 3
        },
        768: {
          items: 4
        },
        1140: {
          items: 6
        }
      },
      nav: true
    };
  }

  private navigate(urlSegment: string, selectedItemKey?: CardsEnum) {
    const { billingAccountId, accountName } = this;


    this.router.navigate(
      [`/p/${this.propertyId}/billing-account/${urlSegment}`],
      {
        relativeTo: this.route,
        queryParams: {
          billingAccountId,
          accountName,
          selectedItemKey
        }
      }
    );
  }

  public selectItemAndChangeRoute( item ) {
    const { type, billingItemId } = item;
    this.billingLauncherItemService.setCurrentSelection(item);

    switch (type) {
      case LauncherMainEnum.Service:
        this.goToService();
        break;
      case LauncherMainEnum.Pos:
        this.goToProduct( billingItemId );
        break;
      case LauncherMainEnum.Credit:
        this.goToCredit(+item.externalId);
        break;
    }
  }

  public goToProduct( id ) {
    this.navigate(`pos/${id}`);
  }

  public goToService() {
    this.navigate('service');
  }

  public goToCredit(key?: CardsEnum) {
    this.navigate('launch-credit', key);
  }

  public goToPos() {
    this.navigate('pos');
  }

  public findItem(externalId: string, type: number, billingItemId?) {
    switch (type) {
      case LauncherMainEnum.Service:
        if ( this.groupList ) {
          return this.groupList.find( item => item.id === (+externalId));
        }
        break;
      case LauncherMainEnum.Pos:
        if ( this.productBillingItemList ) {
          return this.productBillingItemList.find( item => item.productId === externalId
            && (+item.billingItemId) === (+billingItemId) && item.isActive === true);
        }
        break;
      case LauncherMainEnum.Credit:
        this.keyCredit = +externalId;
        return this.keyCredit;
    }

    return null;
  }

  public loadCarouselData() {
    this
      .favoriteResource
      .getCardMoreSelectedData(this.userId)
      .subscribe(
        ({ details }) => {
          this.carouselData = [];
          if ( details ) {
            details.forEach( detail => {
              const { externalId, type, billingItemId, description } = detail;
              const item = this.findItem( externalId , +type, billingItemId );

              if ( item ) {
                this.carouselData.push({
                  ...detail,
                  price: item ? item.unitPrice : null,
                  service: item ? ( item.description ? item.description : description ) : ''
                });
              }
            });
          }
        });
  }

  private async getGroupList() {
    return new Promise(resolve => {
      this
        .billingItemCategoryResource
        .searchBillingItemServiceByCodeOrDescription(this.propertyId, '')
        .subscribe(({ items }) => {
          if ( items ) {
            this.groupList = items.map( item => {
              return {
                id: item['id'],
                description: item['billingItemCategoryName']
              };
            });
          }

          resolve(null);
        });
    });
  }


  public async getProductsBillingItem() {
    return new Promise( resolve => {
      this
        .productsResource
        .getProductBillingItem()
        .subscribe(({items}) => {
          this.productBillingItemList = items;
          resolve(null);
      });
    });
  }

  ngOnInit() {
    const user = this.userService.getUserLocalStorage();

    this.setCarouselConfig();

    this.billingLauncherItemService.setCurrentSelection(null);

    if ( user ) {
      this.userId = user.id;
    }

    this.route.params.subscribe( async (params) => {
      this.billingAccountId = this.route.snapshot.queryParamMap.get('billingAccountId');
      this.accountName = this.route.snapshot.queryParamMap.get('accountName');
      this.propertyId = +params.property;

      await this.getProductsBillingItem();
      await this.getGroupList();

      this.loadCarouselData();
    });
  }

}
