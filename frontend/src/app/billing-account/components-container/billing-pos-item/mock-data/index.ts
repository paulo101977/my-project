import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { ProductGroups } from 'app/products/models/products/products';

export class DATA {
    public static readonly productList = {
        items: [{
            barCode: '9999',
            imageBase64: 'string',
            billingItens: [],
            chainId: '111',
            code: '111',
            id: '111',
            isActive: false,
            isDeleted: false,
            productName: '111',
            ncmId: '111',
            productCategoryId: '111',
            propertyId: '111',
            tenantId: '111',
            categoryName: '111',
            unitMeasurementId: '111',
            associatedPos: '111',
        }],
        hasNext: true,
    };

    public static readonly productGroupList = {
        items: [
            {
                id: 'string',
                categoryName: 'string',
                groupFixedName: 'string',
                iconName: 'string',
                isActive: true,
                chainId: '',
                description: '',
                integrationCode: '',
            },
            {
                categoryName: 'Hamburger2',
                chainId: '2',
                groupFixedName: 'Alimentos',
                iconName: 'lanches.svg',
                id: '4d59121a-b6bb-4714-a257-a9ad00fed64c',
                isActive: true,
                integrationCode: '4444',
                description: '',
            }
        ],
        hasNext: false
    } as ItemsResponse<ProductGroups>;
}
