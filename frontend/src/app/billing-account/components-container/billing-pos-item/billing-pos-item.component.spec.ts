import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { BillingPosItemComponent } from './billing-pos-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { productsResourceStub } from 'app/shared/resources/products/testing';
import { sessionParameterServiceStub } from 'app/shared/services/parameter/testing';
import { billingLauncherItemServiceStub } from 'app/billing-account/services/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { createAccessorComponent, currencyFormatPipe } from '../../../../../testing';
import { RouterTestingModule } from '@angular/router/testing';


const productGroupList = { items: [
  {
    categoryName: 'Hamburger',
    chainId: '1',
    description: 'Hamburger',
    groupFixedName: 'Alimentos',
    iconName: 'lanches.svg',
    id: '4d59121a-b6bb-4714-a257-a9ad00fed64c',
    isActive: true,
    isDeleted: false,
    propertyId: 1,
    standardCategoryId: '5d7b54d1-02f4-4804-9e6a-e57740aba8e9',
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    integrationCode: '1123',
    billingItens: [
      {
        billingItemId: 8,
        creationTime: '2018-12-18T14:13:21.447',
        creatorUserId: 'abee3fae-ff86-4eb3-a333-3e757dd01f53',
        description: 'PDV',
        id: '3a394527-016d-4838-8163-a9ba00ea61b3',
        isActive: true,
        isDeleted: false,
        productId: '531fab44-c1f0-461f-bfab-a9ba00ea61a9',
        tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
        unitPrice: 1.23
      }
    ]
  },
  {
    categoryName: 'Hamburger2',
    chainId: '2',
    description: 'Hamburger2',
    groupFixedName: 'Alimentos',
    iconName: 'lanches.svg',
    id: '4d59121a-b6bb-4714-a257-a9ad00fed64c',
    isActive: true,
    isDeleted: false,
    propertyId: 2,
    standardCategoryId: '5d7b54d1-02f4-4804-9e6a-e57740aba8e9',
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    integrationCode: '4444',
    billingItens: [
      {
        billingItemId: 8,
        creationTime: '2018-12-18T14:27:45.087',
        creatorUserId: 'abee3fae-ff86-4eb3-a333-3e757dd01f53',
        description: 'PDV',
        id: '87ccb9c7-f112-4d1d-b930-a9ba00ee55c7',
        isActive: true,
        isDeleted: false,
        productId: '0312ff5e-0083-40c6-8324-a9ba00ee55c7',
        tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
        unitPrice: 1111.1
      }
    ]
  }
]};


const thxSearchbar = createAccessorComponent('thx-searchbar');

describe('BillingPosItemComponent', () => {
  let component: BillingPosItemComponent;
  let fixture: ComponentFixture<BillingPosItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BillingPosItemComponent,
        currencyFormatPipe,
        thxSearchbar
      ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      providers: [
        sharedServiceStub,
        productsResourceStub,
        sessionParameterServiceStub,
        billingLauncherItemServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(BillingPosItemComponent);
    component = fixture.componentInstance;

    spyOn<any>(component['productsResource'], 'getAllGroupsCategory')
        .and
        .returnValue(of(productGroupList));
    spyOn<any>(component, 'selectProductsByGroup').and.callThrough();

    spyOn<any>(component['productsResource'], 'getProductsByGroupId')
        .and
        .returnValue(of(productGroupList));
    component.propertyId = 1;
    component.billingItemId = 8;

    spyOn<any>(component['router'], 'navigate').and.callFake( () => {} );
    fixture.detectChanges();

    component.searchView.searchInput = {
      nativeElement: { value: '' }
    };
  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test if group has been loaded', fakeAsync( () => {
    component['loadAllProductsGroups']();

    fixture.detectChanges();

    tick();

    expect(component.productGroupList).toEqual(productGroupList.items);
  }));


  it('should test if has listed item after select group', () => {
    component.selectProductsByGroup(productGroupList['items'][0]);

    expect(component.selectedGroup).toEqual(productGroupList.items[0]);
    expect(component.productsToShowList.length).toBe(2);
    expect(component.productsToShowList[0].billingItens[0].billingItemId).toBe(8);
  });


});
