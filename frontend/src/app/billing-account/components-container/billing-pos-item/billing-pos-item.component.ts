import {
  Component,
  OnInit,
  ViewChild,
  KeyValueDiffer,
  KeyValueDiffers,
  DoCheck,
  Input
} from '@angular/core';

import { ProductsResource } from 'app/shared/resources/products/products.resource';
import { ActivatedRoute, Router } from '@angular/router';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';


import { Location } from '@angular/common';
import { SearchbarComponent } from 'app/shared/components/searchbar/searchbar.component';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { ProductGroups } from 'app/products/models/products/products';
import { ProductsChart, ProductsToShow } from 'app/billing-account/models/products-billing-item';
import { BillingLauncherItemService } from 'app/billing-account/services/billing-launcher-item-service';

@Component({
  selector: 'app-billing-pos-item',
  templateUrl: './billing-pos-item.component.html',
  styleUrls: ['./billing-pos-item.component.scss']
})
export class BillingPosItemComponent implements OnInit, DoCheck {

  @ViewChild('searchView') searchView: SearchbarComponent;

  // TODO: comment until fix space between cards
  // public carouselSeeMore = false;
  public carouselSeeMore = true;

  public productGroupList: Array<ProductGroups>;
  public productsToShowList: Array<ProductsToShow>;
  public productsToShowListBack: Array<ProductsToShow>;
  public shoppingChartList: Array<ProductsChart>;
  public selectedGroup: ProductGroups;

  private shoppingChartListDiffer: KeyValueDiffer<string, any>;


  public billingItemId: number;
  public billingAccountId: string;

  public totalAmount = 0;

  // buttons config
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;
  public scannerButtonConfig: ButtonConfig;
  public propertyId: number;
  public currencyId: string;
  public isMobile: boolean;
  public isTablet: boolean;
  public currencyList: any;

  private _callFnScan = false;
  private currentSelectionItem: any;
  public currencySymbol: string;

  get callFnScan(): boolean {
    return this._callFnScan;
  }

  @Input('callFnScan')
  set callFnScan(value: boolean) {
    this._callFnScan = value;
    this.callScan();
  }

  constructor(
    private productsResource: ProductsResource,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private _location: Location,
    private differs: KeyValueDiffers,
    private sessionParameterService: SessionParameterService,
    private router: Router,
    private billingLauncherItemService: BillingLauncherItemService,
    // TODO: remove comments after barcode implementation
    // private deviceDetectorService: DeviceDetectorService,
  ) { }

  // TODO: remove and implement the method
  public callScan() {
    console.log('callScan');
  }

  public setButtons() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'button-cancel',
      this.goBackAndCancel,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'button-confirm',
      this.confirmChart,
      'action.confirm',
      ButtonType.Primary,
      null,
      true,
    );
    this.scannerButtonConfig = this.sharedService.getButtonConfig(
      'button-scanner',
      this.callScanner,
      'action.scan',
      ButtonType.Secondary,
    );

    this.scannerButtonConfig.extraClass = 'thf-btn--sm thf-btn--sm';
  }

  // TODO: implement scanner mobile method
  public callScanner = () => {

  }

  public goBackAndCancel = () => {
    this._location.back();
  }

  public redirectToAccountEdit(billingAccountId) {
    this.router.navigate(
      [`/p/${this.propertyId}/billing-account/edit/`],
      {
        relativeTo: this.route,
        queryParams: {
          billingAccountId
        }
      }
    );
  }

  public confirmChart = () => {
    if ( this.shoppingChartList ) {
      const senderObject = {
        propertyId: this.propertyId,
        billingAccountId: this.billingAccountId,
        billingItemId: this.billingItemId,
        amount: this.totalAmount,
        currencyId: this.currencyId,
        detailList: []
      };

      const detailList = this.shoppingChartList.map( (item) => {
          const { id, code, amount, productName, quantity, groupDescription, ncmCode, barCode } = item;

          return {
            code,
            amount,
            description: productName,
            quantity,
            groupDescription,
            billingItemId: this.billingItemId,
            ncmCode,
            barCode,
            externalId: id, // product id
          };
      });

      senderObject.detailList = detailList;


      // go to initial screen or error message
      this
        .productsResource
        .billingAccountDebit(senderObject)
        .subscribe( () => {
          this.redirectToAccountEdit(this.billingAccountId);
      });
    }

  }

  public getCurrencyExchange() {

      const currencyObj = this
        .sessionParameterService
        .getParameter(this.propertyId, ParameterTypeEnum.DefaultCurrency);

    if ( currencyObj ) {
        this.currencyId = currencyObj.propertyParameterValue;
      }

  }


  public valueChanged( item ) {
    const { shoppingChartList } = this;

    if ( shoppingChartList ) {
      this.updateTotalAmount(shoppingChartList);
    }
  }

  public updateTotalAmount( shoppingChartList ) {
    this.totalAmount = shoppingChartList.reduce( (sum, curr) => sum + curr.amount, 0);
  }

  // set initial amount and detect and enable/disable button
  public shoppingChartChanged(changes) {
    const { shoppingChartList } = this;

    this.confirmButtonConfig.isDisabled = true;
    this.totalAmount = 0;

    if ( shoppingChartList && shoppingChartList.length ) {
      this.confirmButtonConfig.isDisabled = false;

      this.updateTotalAmount(shoppingChartList);
    }
  }

  public loadAllProductsGroups() {
    this
      .productsResource
      .getAllGroupsCategory(this.billingItemId)
      .subscribe( ( { items } ) => {
        if ( items ) {
          this.productGroupList = items.map( item => {
            return {
              ...item,
              description: item.categoryName
            };
          });
        }
    });
  }

  public seeMore() {
    this.carouselSeeMore = !this.carouselSeeMore;
  }

  public selectProductsByGroup(selected: ProductGroups) {
    // clear search bar value
    if (this.searchView) {
      this.searchView.searchInput.nativeElement.value = '';
    }

    if ( selected ) {
      const { shoppingChartList } = this;
      const { id, iconName } = selected;
      this.selectedGroup = selected;

      const { description } = this.selectedGroup;

      this
        .productsResource
        .getProductsByGroupId(id, this.billingItemId)
        .subscribe(({ items }) => {

          if ( items ) {
            this.productsToShowList = <Array<ProductsToShow>>(items
              .map( product => {
                const { billingItens } = product;
                let findedInShopping = null;

                // If shopping already contains product, the latter has already been selected.
                if ( shoppingChartList ) {
                  findedInShopping = shoppingChartList.find(sItem => sItem.id === product.id);
                }

                // get price
                const billingItem = billingItens.find(bItem => bItem.billingItemId === this.billingItemId);


                return {
                  ...product,
                  iconName,
                  unitPrice: billingItem ? billingItem.unitPrice : null,
                  isSelected: !!findedInShopping,
                  groupDescription: description,
                };
            }));

            this.productsToShowListBack = this.productsToShowList;
          }
        });
    }
  }

  public openItem(item) {
    this.selectProductsByGroup(item);
  }

  public search(event) {
    const { value } = event.target;

    if (typeof value === 'string' && value.length > 2) {
      this.productsResource.billingPosSearch(this.billingItemId, value).subscribe( ({ items }) => {

        if ( items ) {
          this.productsToShowList = <Array<ProductsToShow>>(items
            .map( product => {
              const { billingItens } = product;

              // get price
              const billingItem = billingItens.find(bItem => bItem.billingItemId === this.billingItemId);

              return {
                ...product,
                unitPrice: billingItem ? billingItem.unitPrice : null
              };
            })
          );
        }
      });
    } else {
      this.productsToShowList = this.productsToShowListBack;
    }
  }

  public removeItem(item) {
    const { shoppingChartList } = this;

    if ( shoppingChartList ) {
      this.shoppingChartList = shoppingChartList.filter( cartItem => cartItem.id !== item.id);
    }

    if ( this.productsToShowList ) {
      this.productsToShowList.map( product => {
        if ( item.id === product.id) {
          product.isSelected = false;
        }
      });
    }
  }


  public pushSelected(item) {
    if (item && this.shoppingChartList) {
      const found = this.shoppingChartList.find(searched => item.id === searched.id);

      if ( !found ) {
        // first amount
        item.amount = item.unitPrice;

        this.shoppingChartList.push({ ...item });
      }
    }

  }

  public getProductByIdAndSetChart( id, chartItem ): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .productsResource
        .getProductById(id)
        .subscribe( item => {
          const { price } = chartItem;

          this.pushSelected({
            ...item,
            unitPrice: price,
            quantity: 1
          });

          this.billingLauncherItemService.setCurrentSelection(null);

          resolve(null);
        });
    });
  }

  public setCurrencySymbol() {
    const { currencyList, currencyId } = this;

    if ( currencyList && currencyId ) {
      const finded = currencyList.find( item => item.id === currencyId) || {};
      this.currencySymbol = finded.symbol;
    }
  }

  ngOnInit() {
    this.setButtons();

    this.billingAccountId = this.route.snapshot.queryParamMap.get('billingAccountId');
    this.billingItemId = +this.route.snapshot.params.id;


    this.route.params.subscribe(async params => {
      this.propertyId = +params.property;

      this.getCurrencyExchange();
      this.currencyList = await this.billingLauncherItemService.getCurrencyList();
      this.setCurrencySymbol();

      this.shoppingChartList = [];
      this.shoppingChartListDiffer = this
        .differs
        .find(this.shoppingChartList).create();

      this.loadAllProductsGroups();

      this.currentSelectionItem = this.billingLauncherItemService.getCurrentSelection();

      if ( this.currentSelectionItem ) {
        const { externalId } = this.currentSelectionItem;

        this.getProductByIdAndSetChart(externalId, this.currentSelectionItem);
      }
    });
  }

  ngDoCheck(): void {
    if ( this.shoppingChartList ) {
      const changes = this.shoppingChartListDiffer.diff(this.shoppingChartList);
      if (changes) {
        this.shoppingChartChanged(changes);
      }
    }
  }

}
