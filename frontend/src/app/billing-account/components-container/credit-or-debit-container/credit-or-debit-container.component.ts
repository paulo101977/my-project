import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { PlasticCard } from '@app/billing-account/models/plastic-card';
import { BillingItemResource } from '@app/shared/resources/billingItem/billing-item.resource';
import { CommomService } from '@app/shared/services/shared/commom.service';
import { CurrencyExchangeResource } from 'app/shared/resources/currency-exchange/currency-exchange.resource';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { BillingAccountItem } from 'app/shared/models/billing-account/billing-account-item';
import { BillingAccountItemResource } from 'app/shared/resources/billing-account-item/billing-account-item.resource';
import { PaymentTypeWithBillingItem } from 'app/shared/models/billingType/payment-type-with-billing-item';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import { BillingLauncherItemService } from 'app/billing-account/services/billing-launcher-item-service';
import { PaymentTypeEnum } from 'app/shared/models/reserves/payment-type-enum';

@Component({
  selector: 'credit-or-debit-container',
  templateUrl: './credit-or-debit-container.component.html',
  styleUrls: ['./credit-or-debit-container.component.scss']
})
export class CreditOrDebitContainerComponent implements OnInit {

  private readonly messageSuccess = 'text.launcherCreditSuccessText';

  // TODO: create model
  public cardTypeList: Array<any>;
  public cardParcelList: Array<any>;
  public plasticCard: Array<PlasticCard>;

  // the enum selected
  public key: PaymentTypeEnum;
  public form: FormGroup;
  public accountName: string;
  public iconName: string;


  public hasParcel: boolean;
  public propertyId: any;
  public inputValue: number;
  public totalAmount: number;
  private propertyExchangeRate: number;
  private currentValue: number;
  private billingAccountId: string;
  private paymentList: Array<PaymentTypeWithBillingItem>;
  private billingItemId: any;

  private quotationList: Array<any>;
  public propertyExchangeRateId: string;
  public currentExchangeRateId: string;
  public currencyId: string;
  public propertyCurrencyId: string;
  public currencyList: Array<any>;
  public currencySymbol: string;


  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private commomService: CommomService,
    private currencyExchangeResource: CurrencyExchangeResource,
    private billingAccountItemResource: BillingAccountItemResource,
    private billingItemResource: BillingItemResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private billingLauncherItemService: BillingLauncherItemService,
  ) {}

  // Button Dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  public saveData = () => {
    const {
      propertyId,
      totalAmount,
      billingAccountId,
      key,
      form,
      inputValue
    } = this;


    const nsu = form.get('nsu').value;
    const installmentsQuantity = +(form.get('parcel').value);
    const intKey = +key;
    const billingItemId = form.get('plasticBrandId').value;
    const description = this.billingLauncherItemService.translateFromEnum(intKey);

    const obj = <BillingAccountItem>{
      propertyId,
      amount: inputValue,
      amountChange: totalAmount,
      billingAccountId,
      id: GuidDefaultData,
      billingItemId,
      nsu,
      installmentsQuantity,
      externalId: intKey,
      currencyId: this.currencyId,
      currencyExchangeReferenceId: this.currencyId != this.propertyCurrencyId ? this.currentExchangeRateId : null,
      currencyExchangeReferenceSecId: this.currencyId != this.propertyCurrencyId ? this.propertyExchangeRateId : null,
      billingAccountItemDate: null,
      billingItemName: null,
      creditAmount: null,
      billingInvoiceId: null,
      detailList: [
        {
          code: '',
          amount: inputValue,
          amountChange: totalAmount,
          description,
          quantity: 1,
          groupDescription: description,
          externalId: intKey,
        }
      ]
    };

    this
      .billingAccountItemResource
      .createBillingAccountItemCredit(obj)
      .subscribe( () => {

        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant(this.messageSuccess),
        );

        this.redirectToAccountEdit(billingAccountId);
      });
  }

  public cancelData = () => {
    this.goBack();
  }

  public getPaymentList(): Promise<any> {
    return new Promise( (resolve => {
      this
        .billingItemResource
        .getAllPaymentTypeWithBillingItemListByPropertyId(this.propertyId)
        .subscribe( ({ items }) => {

          this.paymentList = items;
          resolve(items);
        });
    }));
  }

  public redirectToAccountEdit(billingAccountId) {
    this.router.navigate(
      [`/p/${this.propertyId}/billing-account/edit/`],
      {
        relativeTo: this.route,
        queryParams: {
          billingAccountId
        }
      }
    );
  }

  // set app-buttons
  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-button',
      this.cancelData,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'save-button',
      this.saveData,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal
    );
  }

  public setForm() {
    this.form = this.formBuilder.group({
      plasticBrandId: [ null, Validators.required ],
      nsu: [null, Validators.required],
      price: [ null, [ Validators.required ]],
      parcel: 1
    });

    this
      .form
      .get('plasticBrandId')
      .valueChanges
      .subscribe
      ( async value => {
        this.searchIcon(value);
        await this.amountParcel(value);
      });

    this
      .form
      .valueChanges
      .subscribe( () => {
        this.buttonConfirmConfig.isDisabled = !this.form.valid;
      });
  }

  public valuesChange(value) {
    this.currentValue = value;

    this.calculateAmount(value);
  }

  public moneySymbolChanged(item) {
    const id = item.key;
    this.currencyId = item.key;

    const { quotationList } = this;

    if ( quotationList ) {
      const element = quotationList.find(q => q.currencyId == item.key);
      this.currentExchangeRateId = element.id;

      if (this.currentValue !== undefined) {
        this.calculateAmount(this.currentValue);
      }
    }
  }

  public goBack() {
    this.location.back();
  }

  public calculateAmount = ( value ) => {
    this.inputValue = value;
    this.totalAmount = this.billingLauncherItemService
      .applyQuotationInValue(value, this.currencyId, this.propertyCurrencyId, this.quotationList);
  }


  public configParcel() {
    if (!this.key) {
      this.key = PaymentTypeEnum.CreditCard;
    }
    if (this.key === PaymentTypeEnum.CreditCard) {
      this.hasParcel = true;
      this.amountParcel(this.form.value['plasticBrandId']);
    } else {
      this.hasParcel = false;
    }
  }

  public getPlastic(): Promise<any> {
    return new Promise( resolve => {
      this
        .billingItemResource
        .getBillingItemPlasticCard( this.key )
        .subscribe( ({ items }) => {

          if (  items && items.length ) {
            const { plasticBrandId } = items[0];
            this.plasticCard = items;

            this.cardTypeList = this
              .convertArraySelect(this.plasticCard, 'id', 'plasticBrandFormatted');
            this.searchIcon(plasticBrandId);
            this.form.get('plasticBrandId').setValue(plasticBrandId);
            resolve(items);
          }


          resolve(null);
        });
    });
  }

  public convertArraySelect(array, id: string, name: string) {
    return this.commomService.toOption(
      array,
      id,
      name,
    );
  }

  public searchIcon(id: number) {
    const { plasticCard } = this;

    if ( plasticCard ) {
      const result = this.plasticCard.find( res  => res.plasticBrandId == id);
      if ( result ) {
        const { iconName } = result;
        this.iconName = iconName;
      }
    }
  }

  public amountParcel(plasticBrandId: number): Promise<any> {
    return new Promise( resolve => {
      this.billingItemResource
        .getBillingItemPaymentType(this.propertyId)
        .subscribe( ({ items })  => {
          let acquirer;
          if (this.plasticCard) {
            acquirer = this.plasticCard.find( res  => res.plasticBrandId == plasticBrandId);
          }

        if ( acquirer ) {
          const pay = items.find(result =>
            result.paymentTypeId === PaymentTypeEnum.CreditCard &&
            result.acquirerName === acquirer.acquirerName
          );

          if (pay) {
            const detailObj = pay.billingItemPaymentTypeDetailList.find(r => r.plasticBrandId == plasticBrandId);

            if ({detailObj}) {
              const {maximumInstallmentsQuantity, id} = detailObj;
              this.parcelOptions(+maximumInstallmentsQuantity);
              this.billingItemId = id;
              resolve(+maximumInstallmentsQuantity);
            }
          }
        }
      });
    });

  }

  public parcelOptions(max) {
    this.cardParcelList = Array.from(Array(max).keys())
      .map((x) => {
        const value = ++x;
        return {key: value, value: `${value}`};
      });
  }

  public setCurrencySymbol() {
    const { currencyList, currencyId } = this;

    if ( currencyList && currencyId ) {
      const finded = currencyList.find( item => item.id === currencyId) || {};
      this.currencySymbol = finded.symbol;
    }
  }

  ngOnInit() {
    this.key = +this.route.snapshot.queryParamMap.get('key');
    this.accountName = this.route.snapshot.queryParamMap.get('accountName');
    this.billingAccountId = this.route.snapshot.queryParamMap.get('billingAccountId');

    this.setForm();
    this.setButtonConfig();
    this.route.params.subscribe(async params => {
      this.propertyId = +params.property;

      this.currencyId = this.billingLauncherItemService.getCurrencyId(this.propertyId);
      this.currencyList = await this.billingLauncherItemService.getCurrencyList();
      this.setCurrencySymbol();


      await this.getQuotationList();

      await this.getPlastic();
      this.configParcel();
      await this.getPaymentList();
    });
  }


  public getQuotationList(): Promise<any> {
    return new Promise<any>( resolve => {
      this.currencyExchangeResource.getQuotationList()
        .subscribe( item => {
          this.quotationList = item;

          if ( this.quotationList ) {
            const propertyQuotation = this.quotationList.find(q => q.currencyId == this.currencyId);
            if (propertyQuotation) {
              this.propertyExchangeRateId = propertyQuotation.id;
              this.propertyCurrencyId = propertyQuotation.currencyId;
            }
          }

          resolve(null);
        });
    });
  }
}
