import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { CreditOrDebitContainerComponent } from './credit-or-debit-container.component';
import { of } from 'rxjs';

import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { currencyFormatPipe } from '../../../../../testing';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { currencyExchangeResourceStub } from 'app/shared/resources/currency-exchange/testing';
import { billingAccountItemServiceStub } from 'app/shared/services/billing-account-item/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { billingItemResourceStub } from 'app/shared/resources/billingItem/testing';
import { billingLauncherItemServiceStub } from 'app/billing-account/services/testing';

describe('CreditOrDebitContainerComponent', () => {
  let component: CreditOrDebitContainerComponent;
  let fixture: ComponentFixture<CreditOrDebitContainerComponent>;

  const plasticCard = {
    items: [
    {
      acquirerId: 1,
      acquirerName: 'Totvs Short 22',
      id: 1,
      maximumInstallmentsQuantity: 1,
      plasticBrandFormatted: 'VISA (Totvs Short 22)',
      plasticBrandId: 1,
      plasticBrandName: 'VISA',
      plasticBrandPropertyId: '30ccbd8e-2da1-48f1-bf3e-10f81eb72503',
      iconName: 'icon_visa.svg'
    },
    {
      acquirerId: 1,
      acquirerName: 'Totvs Short 22',
      id: 22,
      maximumInstallmentsQuantity: 5,
      plasticBrandFormatted: 'MASTERCARD (Totvs Short 22)',
      plasticBrandId: 2,
      plasticBrandName: 'MASTERCARD',
      plasticBrandPropertyId: '"30ccbd8e-2da1-48f1-bf3e-10f81eb72522',
      iconName: 'icon_master.svg'
    }]
  };

  const cardTypeList = {
    items: [
    {
      key: 1,
      value: 'VISA (Totvs Short 22)'
    },
    {
      key: 22,
      value: 'MASTERCARD (Totvs Short 22)'
    }
  ]};

  const cardParcelList = {
    items: [
    {
      key: 1,
      value: '1'
    }
  ]};

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreditOrDebitContainerComponent,
        currencyFormatPipe,
      ],
      imports: [
          TranslateTestingModule,
          HttpClientTestingModule,
          RouterTestingModule,
          ReactiveFormsModule,
          FormsModule,
      ],
      providers: [
        toasterEmitServiceStub,
        commomServiceStub,
        currencyExchangeResourceStub,
        billingAccountItemServiceStub,
        billingItemResourceStub,
        billingLauncherItemServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(CreditOrDebitContainerComponent);
    component = fixture.componentInstance;

    spyOn(component, 'getPlastic').and.callThrough();
    spyOn<any>(component['billingItemResource'], 'getBillingItemPlasticCard')
        .and
        .returnValue(of(plasticCard));
    spyOn<any>(component, 'amountParcel').and.callFake(() => {});
    spyOn(component['commomService'], 'toOption').and.returnValue(cardTypeList.items);

    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load the flags', fakeAsync( () => {
    component.getPlastic();
    tick();


    expect(component.cardTypeList).toEqual(cardTypeList.items);
    expect(component.cardTypeList.length).toBe(2);
    expect(component.cardTypeList[0].key).toBe(1);
  }));

  it('should load the flags image', ( () => {
    component.plasticCard = plasticCard.items;
    component.searchIcon( plasticCard.items[0].id );


    expect(component.iconName).toEqual('icon_visa.svg');
  }));

  it('should load the parcels', ( () => {
    component.parcelOptions(2);
    expect(component.cardParcelList.length).toBe(2);
    expect(component.cardParcelList[0].key).toBe(1);
    expect(component.cardParcelList[0].value).toBe('1');
  }));

  it('should load total amount', ( () => {
    const amount = 2.04;
    spyOn(component['billingLauncherItemService'], 'applyQuotationInValue').and.returnValue(amount);
    component.calculateAmount(amount);
    expect(component.totalAmount).toBe(amount);
  }));

});
