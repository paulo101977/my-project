import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core/';
import { BillingAccountListComponent } from './billing-account-list.component';
import { GetAllBillingAccountData } from 'app/shared/mock-data/get-all-billing-account-data';
import { GetAllBillingAccount } from 'app/shared/models/billing-account/get-all-billing-account';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';
import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';
import { currencyServiceStub, currencyPipeStub } from '@app/shared/mock-data/currency-data';
import { configureTestSuite } from 'ng-bullet';
import { imgCdnStub } from '../../../../../testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('BillingAccountListComponent', () => {
  let component: BillingAccountListComponent;
  let fixture: ComponentFixture<BillingAccountListComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
          BillingAccountListComponent,
          imgCdnStub
      ],
      providers: [ InterceptorHandlerService,
        {
          provide: CurrencyService,
          useValue: currencyServiceStub
        },
        {
          provide: CurrencyFormatPipe,
          useValue: currencyPipeStub
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(BillingAccountListComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should set configHeaderPage, setConfigTable', () => {
      expect(component.configHeaderPage).toEqual({
        keepTitleButton: true,
        keepButtonActive: true,
        callBackFunction: component['goToAccountTypePage'],
      });


      expect(component.columns).toEqual([
        { name: 'label.reservationNumber', prop: 'reservationCode', width: 300 },
        { name: 'label.accountType', prop: 'accountTypeName' },
        { name: 'label.name', prop: 'holderName' },
        { name: 'label.uhType',  prop: 'roomTypeName' },
        { name: 'label.numberUH', prop: 'roomNumber', width: 50 },
        { name: 'label.reservationStatus',  prop: 'reservationStatusName' },
        { name: 'label.arrival', prop: 'strArrivalDate' } ,
        { name: 'label.departure', prop: 'strDepartureDate' },
        { name: 'label.balance', prop: 'strBalance'}
      ]);
    });

    it('should goToAccountTypePage', () => {
      spyOn(component['router'], 'navigate');

      component['goToAccountTypePage']();

      expect(component['router'].navigate).toHaveBeenCalledWith(['new', 'single-account'], { relativeTo: component['route'] });
    });

    it('should newAccountPage', () => {
      spyOn(component['router'], 'navigate');

      component['newAccountPage']();

      expect(component['router'].navigate).toHaveBeenCalledWith(['new', 'single-account'], { relativeTo: component['route'] });
    });
  });

  describe('on filter', () => {
    it('should updateList', () => {
      const list = new Array<GetAllBillingAccount>();
      list.push(GetAllBillingAccountData);

      component.updateList(list);

      expect(component.pageItemsList).toEqual(list);
    });
  });
});
