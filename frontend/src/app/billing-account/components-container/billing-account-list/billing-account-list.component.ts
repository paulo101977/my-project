import {Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { GetAllBillingAccount } from '../../../shared/models/billing-account/get-all-billing-account';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { BillingAccountResource } from './../../../shared/resources/billing-account/billing-account.resource';
import { BillingAccountStatusEnum } from '../../../shared/models/billing-account/billing-account-status-enum';
import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';

@Component({
  selector: 'billing-account-list',
  templateUrl: 'billing-account-list.component.html',
  providers: [DatePipe],
})
export class BillingAccountListComponent implements OnInit {

  public configHeaderPage: ConfigHeaderPageNew;
  public searchableItems: SearchableItems;
  public propertyId: number;

  // Table dependencies
  public columns: Array<any>;
  public pageItemsList: Array<GetAllBillingAccount>;

  // Filter
  public searchButtons: Array<any>;
  public selectedFilterId: any;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private billingAccountResource: BillingAccountResource,
    private datePipe: DatePipe,
    private currencyFormatPipe: CurrencyFormatPipe,
    private currencyService: CurrencyService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.setSearchButtons();
      this.setFilteredOption();
      this.setConfigTable();
      this.setConfigHeaderPage();
      this.setSearchableItems();
    });
    this.route.queryParams.subscribe( () => {
      this.getBillingAccounts(this.selectedFilterId.filterId);
    });
  }

  private setSearchButtons() {
    this.searchButtons = [
      {title: 'label.openedAccounts', filterId: BillingAccountStatusEnum.Open},
      {title: 'label.closedAccounts', filterId: BillingAccountStatusEnum.Closed},
      {title: 'label.pendingAccounts', filterId: BillingAccountStatusEnum.Pending},
      {title: 'label.allAccounts', filterId: 0},
    ];
  }

  private setFilteredOption() {
    const queryParams = this.route.snapshot.queryParams;
    if (queryParams.hasOwnProperty(BillingAccountResource.FILTER_OPENED_ACCOUNTS) &&
      queryParams[BillingAccountResource.FILTER_OPENED_ACCOUNTS]) {
      this.selectedFilterId = this.searchButtons[0];
    } else if (queryParams.hasOwnProperty(BillingAccountResource.FILTER_CLOSED_ACCOUNTS) &&
      queryParams[BillingAccountResource.FILTER_CLOSED_ACCOUNTS]) {
      this.selectedFilterId = this.searchButtons[1];
    } else if (queryParams.hasOwnProperty(BillingAccountResource.FILTER_PENDING_ACCOUNTS) &&
      queryParams[BillingAccountResource.FILTER_PENDING_ACCOUNTS]) {
      this.selectedFilterId = this.searchButtons[2];
    } else {
      this.selectedFilterId = this.searchButtons[3];
    }
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      keepButtonActive: true,
      callBackFunction: this.goToAccountTypePage,
    };
  }

  private setConfigTable() {
    this.setColumnsName();
  }

  private setColumnsName() {
    this.columns = [
      { name: 'label.reservationNumber', prop: 'reservationCode', width: 300 },
      { name: 'label.accountType', prop: 'accountTypeName' },
      { name: 'label.name', prop: 'holderName' },
      { name: 'label.uhType',  prop: 'roomTypeName' },
      { name: 'label.numberUH', prop: 'roomNumber', width: 50 },
      { name: 'label.reservationStatus',  prop: 'reservationStatusName' },
      { name: 'label.arrival', prop: 'strArrivalDate' } ,
      { name: 'label.departure', prop: 'strDepartureDate' },
      { name: 'label.balance', prop: 'strBalance'}
    ];
  }

  private getBillingAccounts(filterBy?: BillingAccountStatusEnum) {
    this.billingAccountResource.getAllBillingAccountByPropertyId(this.propertyId, filterBy).subscribe(response => {
      if (response) {
        this.pageItemsList = [
          ...response.items.map(item => {
            const itemAux = {
              strArrivalDate: this.datePipe.transform(item.arrivalDate, 'dd/MM/yyyy'),
              strDepartureDate: this.datePipe.transform(item.departureDate, 'dd/MM/yyyy'),
              strBalance: item.balanceFormatted
            };
            item = Object.assign(itemAux, item);
            return item;
          }),
        ];
        this.setSearchableItems();
      }
    });
  }

  private goToAccountTypePage = () => {
    this.newAccountPage();
  }

  public goToEditPage(billingAccount: GetAllBillingAccount) {
    this.router.navigate(['edit'], {
      relativeTo: this.route,
      queryParams: {billingAccountId: billingAccount.groupKey},
    });
  }

  public newAccountPage() {
    this.router.navigate(['new', 'single-account'], {
      relativeTo: this.route,
    });
  }

  private setSearchableItems(): void {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.pageItemsList;
    this.searchableItems.placeholderSearchFilter = 'billingAccountModule.commomData.placeholderFilter';
    this.searchableItems.tableRowTypeEnum = TableRowTypeEnum.BillingAccount;
  }

  public updateList(items: Array<any>) {
    if (items) {
      this.pageItemsList = items;
    }
  }

  public filterBy(value) {
    let params: any = null;
    if (value.filterId == BillingAccountStatusEnum.Open) {
      params = {[BillingAccountResource.FILTER_OPENED_ACCOUNTS]: 'true'};
    } else if (value.filterId == BillingAccountStatusEnum.Closed) {
      params = {[BillingAccountResource.FILTER_CLOSED_ACCOUNTS]: 'true'};
    } else if (value.filterId == BillingAccountStatusEnum.Pending) {
      params = {[BillingAccountResource.FILTER_PENDING_ACCOUNTS]: 'true'};
    } else {
      params = {[BillingAccountResource.FILTER_ALL_ACCOUNTS]: 'true'};
    }
    this.router.navigate([], {relativeTo: this.route, queryParams: params});
  }

  public getTotal() {
    const total = this.pageItemsList.reduce((tot, current) => {
      return tot += current.balance;
    }, 0 );

    return this.currencyFormatPipe.transform(total, this.currencyService.getDefaultCurrencySymbol(this.propertyId));
  }

}
