import {  ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { BillingServiceComponent } from './billing-service.component';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { SharedModule } from 'app/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';

// TODO: move mock-data to correctly folder [DEBIT: 7582]
const toSearch = {
  target: {
    value: 'string-custom'
  }
};

const quotationList = [
  {
    currencyName: 'Real',
    currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
    value: 'BRL',
  }
];

const moneyItem = {
  description: 'Real',
  key: '67838acb-9525-4aeb-b0a6-127c1b986c48',
  value: 'BRL',
};

const selectedItem = {
  billingItemCategoryId: 19,
  billingItemCategoryName: 'ISS',
  billingItemCategoryNameIconName: 'iss.svg',
  billingItemIconName: 'iss.svg',
  billingItemName: 'Totvs serviço',
  id: 6,
  isActive: false,
  isSelected: true,
  name: 'Totvs serviço',
  standardCategoryId: 9,
  serviceAndTaxList: [
    {
      beginDate: '0001-01-01T00:00:00',
      identify: '3af76740-a451-48aa-b55e-5de3e88830a6',
      isActive: false,
      name: 'ccccccccc',
      taxPercentage: 2.22,
    }
  ]
};

const billingItemToShow = <ItemsResponse<any>>{
    items: [
      {
        billingItemCategoryId: 19,
        billingItemCategoryName: 'ISS',
        billingItemCategoryNameIconName: 'iss.svg',
        billingItemIconName: 'iss.svg',
        billingItemName: 'Totvs serviço',
        id: 6,
        isActive: false,
        isSelected: true,
        standardCategoryId: 9,
        serviceAndTaxList:
          [{
            beginDate: '0001-01-01T00:00:00',
            identify: '79ca647d-ae21-4a5c-b3c1-63aea316ffcd',
            isActive: false,
            name: 'ccccccccc',
            taxPercentage: 2.22,
          }]
      }],
    hasNext: false,
  };

const selectedGroup = {
  categoryName: 'ISS',
  description: 'ISS',
  iconName: 'iss.svg',
  id: 9,
  isActive: true
};

describe('BillingServiceComponent', () => {
  let component: BillingServiceComponent;
  let fixture: ComponentFixture<BillingServiceComponent>;
  let fg: FormGroup;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        BillingServiceComponent,
      ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        CarouselModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    fixture = TestBed.createComponent(BillingServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    fg = new FormBuilder().group({
      serviceDetail: 123
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call on ngInit', fakeAsync( () => {
    spyOn(component, 'getCurrencyId');
    spyOn(component, 'setForm');
    spyOn(component, 'getList');
    spyOn(component, 'setButtons');
    component['route'].snapshot.params = {};
    spyOn( component, 'getQuotationList').and.returnValue(new Promise(resolve => resolve(null)));
    spyOn( component['billingLauncherItemService'], 'getCurrencyList')
      .and.returnValue(new Promise(resolve => resolve(null)));

    component.ngOnInit();
    tick();

    expect(component.getCurrencyId).toHaveBeenCalled();
    expect(component.setForm).toHaveBeenCalled();
    expect(component.getList).toHaveBeenCalled();
    expect(component.setButtons).toHaveBeenCalled();
    expect(component.getQuotationList).toHaveBeenCalled();
  }));


  it('should check if any method is called at amount process',  fakeAsync(() => {

    component.currentValue = 10;
    component['currencySelectedId'] = moneyItem.key;
    component['currencyId'] = moneyItem.key;
    component['quotationList'] = quotationList;


    spyOn(component, 'calculateAmount').and.callThrough();

    component.pushSelected(billingItemToShow.items[0]);
    component.moneySymbolChanged(moneyItem);

    tick();

    expect(component.taxList).toEqual(billingItemToShow.items[0].serviceAndTaxList);
    expect(component['currencySelectedId']).toEqual(moneyItem.key);
    expect(component.calculateAmount).toHaveBeenCalledWith(10);

  }));

  it('should search item', fakeAsync( () => {
    spyOn(component['billingItemCategoryResource'], 'searchBillingItemServiceByCodeOrDescription')
      .and
      .returnValue(of(billingItemToShow));

    component.search(toSearch);

    tick();

    expect(component.billingItemToShow).toEqual(billingItemToShow.items);
  }));

  it('should check confirm method', fakeAsync( () => {
    spyOn(component['billingAccountItemResource'], 'createBillingAccountItemDebit')
      .and
      .returnValue(of(billingItemToShow));
    spyOn(component, 'emitMessageSuccess');
    spyOn(component, 'redirectToAccountEdit')
      .and
      .callFake( () => {});

    component.chartForm = fg;
    component['billingAccountId'] = '123456';
    component.selectedItem = selectedItem;
    component['selectedGroup'] = selectedGroup;
    component.confirmChart();

    tick();

    expect(component.emitMessageSuccess).toHaveBeenCalled();
    expect(component.redirectToAccountEdit).toHaveBeenCalledWith('123456');
  }));
});
