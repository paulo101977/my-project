import { Component, OnInit } from '@angular/core';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { BillingItemCategoryResource } from 'app/shared/resources/billing-item-category/billing-item-category.resource';
import { ActivatedRoute, Router } from '@angular/router';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { CurrencyExchangeResource } from 'app/shared/resources/currency-exchange/currency-exchange.resource';
import { BillingAccountItemResource } from 'app/shared/resources/billing-account-item/billing-account-item.resource';
import { BillingAccountItem } from 'app/shared/models/billing-account/billing-account-item';
import { BillingItemCategory, ServiceToShow } from 'app/billing-account/models/products-billing-item';
import { GuidDefaultData } from '../../../shared/mock-data/guid-default-data';
import { BillingLauncherItemService } from 'app/billing-account/services/billing-launcher-item-service';
import { BillingItemServiceResource } from 'app/shared/resources/billingItem/billing-item-service.resource';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-billing-service',
  templateUrl: './billing-service.component.html',
  styleUrls: ['./billing-service.component.scss']
})
export class BillingServiceComponent implements OnInit {

  private propertyId: number;

  // buttons config
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;


  // chart
  public hasChart = false;
  public selectedItem: any;

  // header
  public configHeaderPage: ConfigHeaderPageNew;

  // category fix
  public billingItemCategoryList: Array<BillingItemCategory>;

  // TODO: comment until fix space between cards
  // public carouselSeeMore = false;
  public carouselSeeMore = true;

  // the services to show
  public billingItemToShow: Array<ServiceToShow>;
  public taxList: Array<any>;

  public currentValue: number;
  public propertyExchangeRate: number;
  public propertyExchangeRateId: number;
  public currentExchangeRateId: number;
  public totalAmount = 0;
  public inputValue = 0;
  public totalAmountWithTax = 0;

  // form
  public chartForm: FormGroup;
  private currencyId: string;
  private billingAccountId: string;
  private currentSelectionItem: any;
  private currencySelectedId: any;
  private selectedGroup: any;
  private quotationList: Array<any>;
  public currencySymbol: string;
  public currencyList: Array<any>;

  constructor(
    private billingItemCategoryResource: BillingItemCategoryResource,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private _location: Location,
    private router: Router,
    private sessionParameterService: SessionParameterService,
    public currencyExchangeResource: CurrencyExchangeResource,
    private billingAccountItemResource: BillingAccountItemResource,
    private billingLauncherItemService: BillingLauncherItemService,
    private billingItemServiceResource: BillingItemServiceResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
  ) { }

  public setForm() {
    this.chartForm = this.formBuilder.group({
      currencyValue: [ null , Validators.required ],
      serviceDetail: [ null ],
    });

    this
      .chartForm
      .get('currencyValue')
      .valueChanges
      .subscribe( value => {
        this.confirmButtonConfig.isDisabled = !(this.chartForm.valid && !isNaN(value));
    });
  }

  public disableItems() {
    const { billingItemToShow, selectedItem } = this;
    if ( billingItemToShow && selectedItem) {
      this.billingItemToShow = <ServiceToShow[]>billingItemToShow.map( item => {
        item.isSelected = item.id === selectedItem.id;
        return item;
      });
    }
  }

  public pushSelected( item ) {
    const { serviceAndTaxList } = item;

    this.taxList = serviceAndTaxList;

    if ( this.currentValue !== undefined ) {
      this.calculateAmount(this.currentValue);
    }

    this.cleanForm();
    this.hasChart = true;
    this.selectedItem = item;

    this.disableItems();
  }

  public valuesChange( value ) {
    this.currentValue = value;

    this.calculateAmount(value);
  }

  public calculateAmount( value ) {
    const amount = this
      .billingLauncherItemService
      .applyQuotationInValue(
        value,
        this.currencySelectedId,
        this.currencyId,
        this.quotationList
      );
    let tax = 0;

    if ( this.taxList ) {
      tax = this.taxList.reduce((previus, current) => previus + current.taxPercentage, 0);
    }
    this.totalAmount = amount;
    this.inputValue = value;
    this.totalAmountWithTax = amount + ( amount * ( tax / 100) );

    return;
  }

  public seeMore() {
    this.carouselSeeMore = !this.carouselSeeMore;
  }

  public setButtons() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'button-cancel',
      this.goBackAndCancel,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'button-confirm',
      this.confirmChart,
      'action.confirm',
      ButtonType.Primary,
      null,
      true,
    );
  }

  public goBackAndCancel = () => {
    this._location.back();
  }

  public confirmChart = () => {

    const {
      propertyId,
      totalAmount,
      inputValue,
      billingAccountId,
      selectedItem,
      chartForm,
      currencySelectedId,
    } = this;


    const { id,
      billingItemCategoryName,
      billingItemCategoryId,
      billingItemName
    } = selectedItem;
    const serviceDescription = chartForm.get('serviceDetail').value;

    const obj = <BillingAccountItem>{
      id: GuidDefaultData,
      propertyId,
      amount: inputValue,
      amountChange: totalAmount,
      billingAccountId,
      billingItemId: id,
      serviceDescription,
      currencyId: currencySelectedId, // currency operation, not the property currencyId
      propertyExchangeRateId: this.currencySelectedId != this.currencyId ? this.propertyExchangeRateId : null,
      currencyExchangeReferenceId: this.currencySelectedId != this.currencyId ? this.currentExchangeRateId : null,
      currencyExchangeReferenceSecId: this.currencySelectedId != this.currencyId ? this.propertyExchangeRateId : null,
      billingAccountItemDate: null,
      billingItemName: null,
      creditAmount: null,
      billingInvoiceId: null,
      detailList: [
        {
          code: '',
          amount: inputValue,
          amountChange: totalAmount,
          description: billingItemName,
          quantity: 1,
          groupDescription: billingItemCategoryName,
          billingItemId: billingItemCategoryId,
          externalId: id,
        }
      ]
    };

    this
      .billingAccountItemResource
      .createBillingAccountItemDebit(obj)
      .subscribe( item => {
        this.emitMessageSuccess();
        this.redirectToAccountEdit(billingAccountId);
    });
  }

  public emitMessageSuccess() {
    this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('text.serviceSuccessText'),
        );
  }

  public redirectToAccountEdit(billingAccountId) {
    this.router.navigate(
      [`/p/${this.propertyId}/billing-account/edit/`],
      {
        relativeTo: this.route,
        queryParams: {
          billingAccountId
        }
      }
    );
  }

  public search(event) {
    this.billingItemToShow = [];

    if ( event ) {
      this
        .billingItemCategoryResource
        .searchBillingItemServiceByCodeOrDescription(this.propertyId, event)
        .subscribe( ({ items }) => {

          if ( items ) {
            this.billingItemToShow = items;
          }
        });
    }
  }

  public cleanForm() {
    if ( this.chartForm ) {
      this.chartForm.reset({});
    }
  }


  public openItem(event) {
    if ( event ) {
      const { billingItemCategoryId, iconName } = event;
      this.selectedGroup = event;
      this.hasChart = false;
      this.cleanForm();
      this.billingItemToShow = [];
      this
        .billingItemCategoryResource
        .getBillingItemServiceByCategoryId( this.propertyId, billingItemCategoryId )
        .subscribe(({ items }) => {
            this.taxList = [];

          if ( items ) {
              this.billingItemToShow = items.map( item => {
                return {
                  ...item,
                  billingItemName: item.name,
                  billingItemCategoryNameIconName: iconName
                };
              });
            }
        });
    }
  }

  public getList() {
    this
      .billingItemCategoryResource
      .getCategoryListWithService(this.propertyId)
      .subscribe(({items}) => {
        this.billingItemCategoryList = <BillingItemCategory[]>this.normalizeInfo(items);
      });
  }

  public getCurrencyId() {

    const currencyObj = this
      .sessionParameterService
      .getParameter(this.propertyId, ParameterTypeEnum.DefaultCurrency);

    if ( currencyObj ) {
      this.currencyId = currencyObj.propertyParameterValue;
    }

  }

  public normalizeInfo(items: any[]) {
    if (items) {
      return items.reduce((prev, curr) => {
        if (!prev.some(item => item.description == curr.billingItemCategoryName)) {
          prev.push({...curr, description: curr.billingItemCategoryName});
        }
        return prev;
      }, []);
    }
    return [];
  }

  public moneySymbolChanged(item) {
    const id = item.key;
    const { quotationList } = this;
    this.currencySelectedId = id;

    if ( quotationList ) {
      const element = quotationList.find(q => q.currencyId == item.key) || {};
      this.currentExchangeRateId = element.id;

      if (this.currentValue !== undefined) {
        this.calculateAmount(this.currentValue);
      }
    }
  }

  public setServiceById(id, selection): Promise<any> {
    const { propertyId } = this;

    return new Promise<any>( resolve => {
      this
        .billingItemServiceResource
        .getServiceById(id, propertyId)
        .subscribe( item => {

          const { serviceAndTaxList } = item;

          if ( serviceAndTaxList ) {
            item.serviceAndTaxList = serviceAndTaxList.filter( tax => tax.isActive);
          }

          this.pushSelected({
            ...item,
            billingItemName: item.name,
          });

          this.chartForm.get('serviceDetail').setValue(selection.serviceDescription);

          resolve(null);
      });
    });
  }

  public setCurrencySymbol() {
    const { currencyList, currencyId } = this;

    if ( currencyList && currencyId ) {
      const finded = currencyList.find( item => item.id === currencyId) || {};
      this.currencySymbol = finded.symbol;
    }
  }

  ngOnInit() {
    this.setButtons();

    this.route.params.subscribe( async (params) => {
      this.propertyId = +params.property;

      this.billingAccountId = this.route.snapshot.queryParamMap.get('billingAccountId');

      this.getCurrencyId();
      this.currencyList = await this.billingLauncherItemService.getCurrencyList();
      this.setCurrencySymbol();


      await this.getQuotationList();

      this.setForm();
      this.getList();

      this.currentSelectionItem = this.billingLauncherItemService.getCurrentSelection();

      if ( this.currentSelectionItem ) {
        const billingItemId = this.currentSelectionItem['billingItemId'];

        await this.setServiceById(billingItemId, this.currentSelectionItem);
      }
    });
  }

  public getQuotationList(): Promise<any> {
    return new Promise<any>( resolve => {
      this.currencyExchangeResource.getQuotationList()
        .subscribe( item => {
          this.quotationList = item;

          if ( this.quotationList ) {
            const propertyQuotation = this.quotationList.find(q => q.currencyId == this.currencyId);
            if (propertyQuotation) {
              this.propertyExchangeRateId = propertyQuotation.id;
            }
          }

          resolve(null);
        });
    });

  }
}
