export enum CardsEnum {
  Credit = 5,
  Debit = 9,
  Money = 8,
  Deposit = 6,
  Check = 7
}
