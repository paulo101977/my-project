export enum BillingItemIdEnum {
  Service = 0,
  POS = 1,
  Credit = 2
}
