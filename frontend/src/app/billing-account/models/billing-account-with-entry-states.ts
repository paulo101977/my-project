export interface BillingAccountWithEntryStates {
  hasInvoices: boolean;
  hasIssuedInvoices: boolean;
  isOpen: boolean;
}
