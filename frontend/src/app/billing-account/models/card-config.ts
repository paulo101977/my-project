import { CardsEnum } from './cards.enum';

export class CardConfig {
  key: CardsEnum;
  value: string;
  src: string;
}
