export class PaymentBillingItem {
    acquirerId: string;
    acquirerName: string;
    billingItemPaymentTypeDetailList: Array<BillingItemPaymentTypeDetailList>;
    paymentTypeId: number;
}

export class BillingItemPaymentTypeDetailList extends PaymentBillingItem {
    billingItemPaymentTypeId: number;
    id: number;
    isActive: boolean;
    maximumInstallmentsQuantity: number;
    plasticBrandId: number;
    plasticBrandPropertyId: string;
    plasticBrandPropertyName: string;
}
