export class ProductsBillingItem {
  id: string;
  productId: string;
  billingItemId: string;
  description: string;
  unitPrice: number;
  isActive: boolean;
  tenantId: string;
  isDeleted: boolean;
}

export class ProductsToShow {
  barCode: string;
  billingItens: Array<any>;
  categoryName: string;
  chainId: string;
  code: string;
  iconName: string;
  id: string;
  isActive: boolean;
  isDeleted: boolean;
  isSelected: boolean;
  ncmId: string;
  productCategoryId: string;
  productName: string;
  propertyId: string;
  tenantId: string;
  unitMeasurementId: string;
  unitPrice: number;
  groupDescription: string;
}

export class ProductsChart extends ProductsToShow {
  amount: string;
  quantity: number;
  ncmCode: string;
}


export class ServiceToShow {
  billingItemCategoryId: string;
  billingItemCategoryName: string;
  billingItemCategoryNameIconName: string;
  billingItemIconName: string;
  billingItemName: string;
  id: string;
  isActive: boolean;
  isSelected: boolean;
  name: string;
  serviceAndTaxList: Array<any>;
  standardCategoryId: number;
}

export class BillingItemCategory {
  categoryName: string;
  description: string;
  iconName: string;
  id: number;
  isActive: boolean;
}
