
export class PlasticCard {
    acquirerId: number;
    acquirerName: string;
    id: number;
    maximumInstallmentsQuantity: number;
    plasticBrandFormatted: string;
    plasticBrandId: number;
    plasticBrandName: string;
    plasticBrandPropertyId: string;
    iconName: string;
}
