// Agular imports
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';
import * as moment from 'moment';

@Component({
  selector: 'modal-cancel-nfse',
  templateUrl: './modal-cancel-nfse.component.html',
})
export class ModalCancelNfseComponent implements OnInit {
  @Input() modalId: string;

  private _invoice: InvoiceDto;
  @Input('invoice')
  set invoice (invoice: InvoiceDto) {
    this._invoice = invoice;
    if (invoice) {
      this.nfseDate = { date: moment(invoice.emissionDate).format('L - hh:mm:ss') };
      this.emitter = { name: invoice.emissionUserName };
    }
  }
  get invoice(): InvoiceDto {
    return this._invoice;
  }

  @Output() cancelNFSe = new EventEmitter<InvoiceDto>();
  @Output() downloadNFSe = new EventEmitter<InvoiceDto>();
  @Output() closeModal = new EventEmitter();

  public cancelButtonConfig: ButtonConfig;
  public downloadButtonConfig: ButtonConfig;
  public nfseDate: any;
  public emitter: any;

  constructor(private sharedService: SharedService) {}

  ngOnInit() {
    this.setButtonConfig();
  }

  // Actions
  public clickCloseModal() {
    this.closeModal.emit();
  }

  public clickCancelNFSe = () => {
    this.cancelNFSe.emit(this.invoice);
  }

  public clickDownloadNFSe = () => {
    this.downloadNFSe.emit(this.invoice);
  }

  // Config
  public setButtonConfig() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'cancelNfse',
      this.clickCancelNFSe,
      'label.cancelNfse',
      ButtonType.Danger,
    );
    this.cancelButtonConfig.extraClass = 'thf-single-modal-button-size';

    this.downloadButtonConfig = this.sharedService.getButtonConfig(
      'downloadNFSe',
      this.clickDownloadNFSe,
      'label.makeDownload',
      ButtonType.Secondary,
    );
    this.downloadButtonConfig.extraClass = 'thf-single-modal-button-size';
  }
}
