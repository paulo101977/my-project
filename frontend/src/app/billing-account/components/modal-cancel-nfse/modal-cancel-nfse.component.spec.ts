import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalCancelNfseComponent } from './modal-cancel-nfse.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { createAccessorComponent, imgCdnStub } from '../../../../../testing';

const ButtonComponentStub = createAccessorComponent('app-button');

describe('ModalCancelNfseComponent', () => {
  let component: ModalCancelNfseComponent;
  let fixture: ComponentFixture<ModalCancelNfseComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
          TranslateTestingModule
      ],
      declarations: [
        ModalCancelNfseComponent,
        ButtonComponentStub,
        imgCdnStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ModalCancelNfseComponent);
    component = fixture.componentInstance;
    component.invoice = new InvoiceDto();
    component.invoice.billingInvoiceNumber = 2222;
    component.invoice.emissionDate = new Date();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set cancelButtonConfig', async(() => {
    const buttonConfig = new ButtonConfig();
    buttonConfig.id = 'cancelNfse';
    buttonConfig.callback = component.clickCancelNFSe;
    buttonConfig.textButton = 'label.cancelNfse';
    buttonConfig.buttonType = ButtonType.Danger;
    buttonConfig.extraClass = 'thf-single-modal-button-size';

    expect(component.cancelButtonConfig).toEqual(buttonConfig);
  }));

  it('should set downloadButtonConfig', async(() => {
    const buttonConfig = new ButtonConfig();
    buttonConfig.id = 'downloadNFSe';
    buttonConfig.callback = component.clickDownloadNFSe;
    buttonConfig.textButton = 'label.makeDownload';
    buttonConfig.buttonType = ButtonType.Secondary;
    buttonConfig.extraClass = 'thf-single-modal-button-size';

    expect(component.downloadButtonConfig).toEqual(buttonConfig);
  }));

  it( 'should emit cancel event when cancel Button clicked', () => {
    let invoice = null;
    component.cancelNFSe.subscribe( value => invoice = value);

    component.clickCancelNFSe();
    expect(component.invoice).toEqual(invoice);
  });

  it( 'should emit download event when download Button clicked', () => {
    let invoice = null;
    component.downloadNFSe.subscribe( value => invoice = value);

    component.clickDownloadNFSe();
    expect(component.invoice).toEqual(invoice);
  });

});
