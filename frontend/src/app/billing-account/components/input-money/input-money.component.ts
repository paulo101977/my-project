import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnChanges, SimpleChanges
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CurrencyExchangeResource } from 'app/shared/resources/currency-exchange/currency-exchange.resource';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'input-money',
  templateUrl: './input-money.component.html',
  styleUrls: ['./input-money.component.scss']
})
export class InputMoneyComponent implements OnInit {

  @Output() moneySymbolChanged = new EventEmitter();

  // the selected item
  // TODO: create model
  public currentSelected: any;

  public isOpen = false;

  // the currency mask options
  public optionsCurrencyMask: any;

  // the input and label id
  @Input() inputId: string;

  // the form group
  @Input() parent: FormGroup;

  // TODO: create model with key, description and value Array of object
  @Input() moneySymbolList: Array<any>;

  // label inputs
  @Input() hasLabel = false;
  @Input() labelText: string;

  public _isRequired: boolean;
  private propertyId: number;
  private currencyId: string;

  get isRequired(): boolean {
    return this._isRequired;
  }

  @Input() set isRequired(value: boolean) {
    this._isRequired = value;
  }

  // emmit on values changes
  @Output() valuesChange = new EventEmitter();

  constructor(
    private currencyExchangeResource: CurrencyExchangeResource,
    private sessionParameterService: SessionParameterService,
    private route: ActivatedRoute,
  ) { }

  public toggle(event) {
    event.stopPropagation();
    this.isOpen = !this.isOpen;
  }

  public open(event) {
    event.stopPropagation();
    this.isOpen = true;
  }

  public close() {
    this.isOpen = false;
  }

  public selectItem(event, item) {
    event.stopPropagation();
    this.currentSelected = item;
    this.moneySymbolChanged.emit(item);
    this.close();
  }

  public setVars(): void {
    this.optionsCurrencyMask = {
      prefix: '',
      thousands: '.',
      decimal: ',',
      align: 'center',
      allowZero: true,
      allowNegative: false
    };
  }

  public getCurrencyDefault() {
    this.currencyId =
      this.sessionParameterService
        .getParameterValue(this.propertyId, ParameterTypeEnum.DefaultCurrency);
  }

  public getCurrencies() {
    this
      .currencyExchangeResource
      .getCurrencies()
      .subscribe( ({ items }) => {

        if ( items ) {
          this.moneySymbolList = items.map(item => {
            return {
              key: item.id,
              value: item.alphabeticCode,
              description: item.currencyName
            };
          });

          // first selection
          if ( this.moneySymbolList ) {
            this.currentSelected = this.moneySymbolList[0];

            if ( this.currencyId ) {
              this.currentSelected = this
                .moneySymbolList
                .find( currencyItem => currencyItem.key === this.currencyId );
            }

            this.moneySymbolChanged.emit(this.currentSelected);
          }
        }
      });
  }

  public detectValuesChange() {
    if ( this.parent && typeof this.inputId === 'string') {
      this
        .parent
        .get(this.inputId)
        .valueChanges
        .subscribe( value => {
          this.valuesChange.emit(value);
      });
    }
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;

    this.setVars();
    this.detectValuesChange();

    this.getCurrencyDefault();
    this.getCurrencies();
  }
}
