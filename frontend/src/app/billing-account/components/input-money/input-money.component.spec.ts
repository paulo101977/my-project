// TODO: create test

import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { InputMoneyComponent } from './input-money.component';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';


import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteStub, currencyFormatPipe } from '../../../../../testing';
import { currencyExchangeResourceStub } from 'app/shared/resources/currency-exchange/testing';
import { sessionParameterServiceStub } from 'app/shared/services/parameter/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
const moneyList = {
  items: [
    {
      alphabeticCode: 'USD',
      countrySubdivisionId: 0,
      currencyName: 'Dólar',
      exchangeRate: 1,
      id: 'c5ef2cfa-a05e-44ed-ad2e-089b26471432',
      minorUnit: 2,
      numnericCode: '2',
      symbol: '$',
    },
    {
      alphabeticCode: 'RLB',
      countrySubdivisionId: 0,
      currencyName: 'Real',
      exchangeRate: 1,
      id: 'c5ef2cfa-a05e-44ed-ad2e-089b26471411',
      minorUnit: 2,
      numnericCode: '2',
      symbol: 'R$',
    }]
};

describe('InputMoneyComponent', () => {
  let component: InputMoneyComponent;
  let fixture: ComponentFixture<InputMoneyComponent>;

  const formBuilder = new FormBuilder();

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
          InputMoneyComponent,
          currencyFormatPipe,
      ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        currencyExchangeResourceStub,
        sessionParameterServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(InputMoneyComponent);
    component = fixture.componentInstance;

    component.parent = formBuilder.group({
      info: 'my-info',
      price: 10
    });
    component.inputId = 'price';

    spyOn<any>(component['currencyExchangeResource'], 'getCurrencies')
        .and.returnValue(of(moneyList));

    const { id } = moneyList.items[1];
    spyOn<any>(component, 'getCurrencyDefault').and.callFake( (par1, par2) => {});


    component['currencyId'] = id;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check init', () => {
    spyOn(component, 'setVars');
    spyOn(component, 'detectValuesChange');
    spyOn(component, 'getCurrencies');

    component.ngOnInit();

    expect(component.setVars).toHaveBeenCalled();
    expect(component.detectValuesChange).toHaveBeenCalled();
    expect(component.getCurrencyDefault).toHaveBeenCalled();
    expect(component.getCurrencies).toHaveBeenCalled();
  });

  it('should check if default current is setting', fakeAsync( () => {
    component.ngOnInit();

    tick();

    expect(component.getCurrencyDefault).toHaveBeenCalled();

    expect(component.currentSelected['key']).toEqual(moneyList.items[1].id);

    expect(component.moneySymbolList.length).toEqual(2);
  }) );

});
