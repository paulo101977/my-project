import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { QrScannerComponent } from 'angular2-qrscanner';

@Component({
  selector: 'qr-code-scanner',
  styleUrls: ['./qr-code-scanner.component.scss'],
  encapsulation: ViewEncapsulation.None,
  template: `
    <qr-scanner
      #qrScanner
      [debug]="false"
      [canvasWidth]="1080"
      [canvasHeight]="720"
      [stopAfterScan]="true"
      [updateTime]="500">
    </qr-scanner>
  `
})
export class QrCodeScannerComponent implements OnInit, AfterViewInit {

  /*private _callFnScan: boolean;
  get callFnScan(): boolean {
    return this._callFnScan;
  }

  @Input('callFnScan')
  set callFnScan(value: boolean) {
    this._callFnScan = value;
    this.qrCodeScan();
  }*/

  @ViewChild('qrScanner') qrScannerComponent: QrScannerComponent;

  constructor() { }

  public getDevices() {
    console.log(this.qrScannerComponent);
    if ( this.qrScannerComponent ) {
      this
        .qrScannerComponent
        .getMediaDevices()
        .then(devices => {
          console.log(devices);
          const videoDevices: MediaDeviceInfo[] = [];
          for (const device of devices) {
            if (device.kind.toString() === 'videoinput') {
              videoDevices.push(device);
            }
          }
          if (videoDevices.length > 0) {
            let choosenDev;
            for (const dev of videoDevices) {
              if (dev.label.includes('front')) {
                choosenDev = dev;
                break;
              }
            }
            if (choosenDev) {
              this.qrScannerComponent.chooseCamera.next(choosenDev);
            } else {
              this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
            }

            console.log('here', this.qrScannerComponent.chooseCamera);
          }
        });
    }
  }


  ngOnInit() {
    // this.setScan();
  }

  ngAfterViewInit() {
    this.getDevices();
  }

}
