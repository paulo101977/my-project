import { Component, OnInit, Input } from '@angular/core';
import { PlasticCard } from '@app/billing-account/models/plastic-card';

@Component({
  selector: 'credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss']
})
export class CreditCardComponent implements OnInit {

  @Input() accountName: string;
  @Input() iconName: string;
  public plasticCard: PlasticCard;

  constructor(
  ) {

  }

  ngOnInit() {

  }

}
