import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'billing-account-bank-check',
  templateUrl: './billing-account-bank-check.component.html',
})
export class BillingAccountBankCheckComponent implements OnInit {
  @Input() formModal: FormGroup;

  public optionsCurrencyMask: any;

  constructor() {}

  ngOnInit() {
    this.setVars();
  }

  private setVars(): void {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
  }
}
