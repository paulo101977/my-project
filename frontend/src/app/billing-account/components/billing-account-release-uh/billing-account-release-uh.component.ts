import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ReleaseUhResource } from '../../../shared/resources/release-uh/release-uh.resource';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { BillingAccountHeader } from '../../../shared/models/billing-account/billing-account-header';

@Component({
  selector: 'billing-account-release-uh',
  templateUrl: './billing-account-release-uh.component.html',
})
export class BillingAccountReleaseUhComponent implements OnInit {
  @Input() billingAccountHeader: BillingAccountHeader;
  public htmlStatus: string;

  public buttonCancelConfig: ButtonConfig;
  public buttonReversePostConfig: ButtonConfig;

  @Output() modalClose: EventEmitter<any> = new EventEmitter();
  @Output() notifyFinishRealease: EventEmitter<any> = new EventEmitter();

  constructor(
    private sharedService: SharedService,
    private toogleModal: ModalEmitToggleService,
    private translateService: TranslateService,
    private releaseResource: ReleaseUhResource,
  ) {}

  ngOnInit() {
    this.translateService.get('label.pending').subscribe(result => {
      this.htmlStatus = '<span class="thf-status-color--pending" >' + result + '</span>';
    });
    this.setButtonConfig();
  }

  private setButtonConfig(): void {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-reverse',
      this.closeModal.bind(this),
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonReversePostConfig = this.sharedService.getButtonConfig(
      'reverse-post',
      this.releaseUh.bind(this),
      'action.confirm',
      ButtonType.Primary,
      null,
    );
  }
  public closeModal() {
    this.modalClose.emit();
  }
  public releaseUh() {
    this.releaseResource.releaseUh(this.billingAccountHeader.reservationItemId).subscribe(
      result => {
        this.notifyFinishRealease.emit();
      },
      error => {
        this.modalClose.emit();
      },
    );
  }
}
