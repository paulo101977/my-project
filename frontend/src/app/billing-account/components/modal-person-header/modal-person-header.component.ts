import { Location } from 'app/shared/models/location';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { PersonHeaderMapper } from 'app/shared/mappers/person-header-mapper';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { AddressService } from 'app/shared/services/shared/address.service';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { DocumentTypeResource } from 'app/shared/resources/document-type/document-type.resource';
import { AddressType } from 'app/shared/models/address-type-enum';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { debounceTime, switchMap } from 'rxjs/operators';
import { UpdateItemResponse } from 'app/shared/models/autocomplete/update-item-response';
import { SearchPersonsResultDto } from 'app/shared/models/person/search-persons-result-dto';
import { PersonResource } from 'app/shared/resources/person/person.resource';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { DocumentType } from 'app/shared/models/document-type';
import { BillingAccountResource } from '@app/shared/resources/billing-account/billing-account.resource';
import { AutocompleteDisplayEnum, AutocompleteTypeEnum } from '@app/shared/models/autocomplete/autocomplete.enum';
import { AuthService } from 'app/shared/services/auth-service/auth.service';
import { CountryCodeEnum } from '@bifrost/location-manager/models';
import { ValidatorFormService } from 'app/shared/services/shared/validator-form.service';
import { NifValidatorService } from 'app/shared/services/nif-validator/nif-validator.service';
import { DocumentTypeNaturalEnum, DocumentTypeLegalEnum } from 'app/shared/models/document-type';
import { DocumentService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'app-modal-person-header',
  templateUrl: './modal-person-header.component.html',
  styleUrls: ['./modal-person-header.component.css']
})
export class ModalPersonHeaderComponent implements OnInit, OnChanges {

  public readonly MODAL_TITLE = 'title.headerPersonInvoice';

  @Input() modalId: string;
  @Input() billingAccountId: string;

  @Output() closeModal = new EventEmitter();

  public personHeaderForm: FormGroup;

  private personId: string;
  public autocompleteDocumentTypes: Array<DocumentType>;
  public autocompleSelectItemsDocumentTypes: Array<SelectObjectOption>;
  public inputIdDocument: string;
  public paramDisplayDocument: string;
  public autocompleItemsDocument: Array<SearchPersonsResultDto>;
  public searchDataDocument: string;
  public locationEdit: Location;

  public inputIdEmail: string;
  public isRequired = true;

  public locationIsValid: boolean;

  public referenceToDisplayAutocompleteEnum = AutocompleteDisplayEnum;
  public referenceToTypeAutocompleteEnum = AutocompleteTypeEnum;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private documentTypeResource: DocumentTypeResource,
    private commomService: CommomService,
    private addressService: AddressService,
    private router: Router,
    private personHeaderMapper: PersonHeaderMapper,
    private billingAccountResorce: BillingAccountResource,
    private toasterEmitService: ToasterEmitService,
    private route: ActivatedRoute,
    private personResource: PersonResource,
    private authService: AuthService,
    private validatorFormService: ValidatorFormService,
    private nifValidatorService: NifValidatorService,
    private documentService: DocumentService
  ) {}

  ngOnInit() {
    this.setVars();
    this.setFormModal();
    this.setButtonConfig();
    this.setIsRequired();
  }

  public setIsRequired() {
    const { personHeaderForm } = this;
    this.isRequired = this.authService.getPropertyCountryCode() !== CountryCodeEnum.PT;


    if ( !this.isRequired && personHeaderForm ) {
      const emailField = personHeaderForm.get('email');

      emailField.clearValidators();
      personHeaderForm.updateValueAndValidity();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['billingAccountId'] && changes.billingAccountId.currentValue
      && this.billingAccountId) {
      this.getPersonHeader();
    }
  }

  private getPersonHeader() {
    this.billingAccountResorce.getPersonHeaderByBillingAccountId(this.billingAccountId).subscribe(personHeaderDto => {
      if (personHeaderDto) {
        this.personId = personHeaderDto.id;
        this.personHeaderMapper.dtoToFormGroup(this.personHeaderForm, personHeaderDto);
        if (personHeaderDto.locationList[0]) {
          this.locationEdit = <Location>personHeaderDto.locationList[0];
        }
      }
    });
  }

  private setVars(): void {
    this.getDocumentTypes();
  }

  // Config
  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'person-header-cancel',
      this.clickCloseModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'single-account-save',
      this.savePersonHeader,
      'action.save',
      ButtonType.Primary,
    );
  }

  private setFormModal() {
    this.personHeaderForm = this.formBuilder.group({
      personId: [''],
      document: [null, [Validators.required, this.documentIsValid()]],
      documentTypeId: ['', [Validators.required]],
      email: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      phone: [''],
      address: this.formBuilder.group({
        id: null,
        ownerId: GuidDefaultData,
        postalCode: null,
        streetName: null,
        streetNumber: null,
        additionalAddressDetails: null,
        division: null,
        subdivision: null,
        neighborhood: null,
        browserLanguage: window.navigator.language,
        latitude: null,
        longitude: null,
        countryCode: null,
        country: null,
        completeAddress: null,
        countrySubdivisionId: null,
        locationCategoryId: null
      }),
    });
    this.personHeaderForm.valueChanges
      .subscribe((value) => {
        this.buttonSaveConfig.isDisabled = this.personHeaderForm.invalid || !this.locationIsValid;
      });
  }

  private documentIsValid() {
    return (control) => {
      if (this.personHeaderForm && control.value) {
        const documentTypeId = this.personHeaderForm.get('documentTypeId').value;
        const countryCode = this.personHeaderForm.get('address').get('countryCode').value;
        switch (+documentTypeId) {
          case DocumentTypeNaturalEnum.NIF:
          case DocumentTypeLegalEnum.NIF:
            if (countryCode == CountryCodeEnum.PT) {
              return this.documentService
                .nifControlIsValid(documentTypeId, control.value, countryCode);
            }
            return null;
          default:
            return null;
        }
      }
    };
  }

  // Actions
  public clickCloseModal = () => {
    if (!this.personHeaderForm.get('personId').value) {
      this.personHeaderForm.reset();
    }
    this.closeModal.emit();
  }

  public setValueAddressByEventEmitted(address: any) {
    this.locationIsValid = address.valid;
    this.personHeaderForm.get('address').patchValue(address);
  }

  private getDocumentTypes() {
    this.documentTypeResource.getAllByPropertyCountryCode().subscribe(response => {
      this.autocompleSelectItemsDocumentTypes = this.commomService.toOption(
        response.items,
        'id',
        'name',
      );
      this.autocompleteDocumentTypes = response.items;
    });
  }

  private setConfigAutocompleItemsDocument() {
    this.inputIdDocument = this.paramDisplayDocument = 'document';
  }

  private setConfigAutocompleItemsEmail() {
    this.inputIdEmail = 'email';
  }

  public searchItemDocument(searchData: string) {
    const documentType = new DocumentType();
    documentType.id = this.personHeaderForm.get('documentTypeId').value;
    this.searchDataDocument = searchData;

    if (searchData && documentType.id) {
      this.personHeaderForm
        .get('document')
        .valueChanges
        .pipe(
          debounceTime(1000),
          switchMap(criteria =>
            this.personResource.getPersonListByDocument(
              criteria,
              documentType.id
            ),
          )
        )
        .subscribe(persons => {
          this.autocompleItemsDocument = persons;
        });
    }
  }

  public updateItemDocument(itemData: UpdateItemResponse<SearchPersonsResultDto> | string) {
    if (typeof itemData === 'string') {
      this.personHeaderForm.get('document').setValue(itemData);
      if (!this.personId) {
        this.personHeaderForm.get('personId').setValue(null);
      }
    } else {
      this.personHeaderForm.get('document').setValue(itemData.completeItem.document);
      this.personId = itemData.completeItem.id;
      this.setFormGroupWithAutocompleteData(itemData.completeItem);
    }
  }

  private setFormGroupWithAutocompleteData(result: SearchPersonsResultDto): void {
    this.personHeaderForm.patchValue({
      personId: result.id,
      email: result.contactInformation.find(c => c.typeValue == 'Email') == null ? null
        : result.contactInformation.find(c => c.typeValue == 'Email').value,
      fullName: result.name,
      phone: result.contactInformation.find(c => c.typeValue == 'PhoneNumber') == null ? null
        : result.contactInformation.find(c => c.typeValue == 'PhoneNumber').value
    });
    if (result.locationList && result.locationList.length > 0) {
      this.locationEdit = <Location>result.locationList[0];
    }
  }

  private getLocationCategory(locationCategoryId: number): AddressType {
    return this.addressService.getLocationCategoryEnumByLocationCategoryId(locationCategoryId);
  }

  public getDocumentInformation(): string {
    return  this.personHeaderForm.value.document;
  }

  private savePersonHeader = () => {
    const personHeaderDto = this.personHeaderMapper.formGroupToDto(this.personHeaderForm, this.billingAccountId);

    this.billingAccountResorce.associateHeaderPerson(personHeaderDto).subscribe(response => {
      this.toasterEmitService.emitChange(SuccessError.success, 'alert.personHeaderSaveWithSuccess');
      this.personId = response.id;
      this.personHeaderForm.get('personId').setValue(response.id);
      this.clickCloseModal();
    });
  }

}
