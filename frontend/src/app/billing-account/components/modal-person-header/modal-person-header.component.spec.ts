import { LocationDto } from 'app/shared/models/dto/location-dto';
import { UpdateItemResponse } from 'app/shared/models/autocomplete/update-item-response';
import { SearchPersonsResultDto } from 'app/shared/models/person/search-persons-result-dto';
import { SuccessError } from '@app/shared/models/success-error-enum';
import { PersonHeaderDto } from 'app/shared/models/dto/person-header-dto';
import { AddressType } from 'app/shared/models/address-type-enum';
import { ButtonType, Type, ButtonConfig, ButtonSize } from 'app/shared/models/button-config';
import { ActivatedRouteStub, createAccessorComponent } from '../../../../../testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonResource } from '@app/shared/resources/person/person.resource';
import { ToasterEmitService } from '@app/shared/services/shared/toaster-emit.service';
import { BillingAccountResource } from 'app/shared/resources/billing-account/billing-account.resource';
import { PersonHeaderMapper } from 'app/shared/mappers/person-header-mapper';
import { AddressService } from 'app/shared/services/shared/address.service';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { DocumentTypeResource } from 'app/shared/resources/document-type/document-type.resource';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Observable, of } from 'rxjs';

import { ModalPersonHeaderComponent } from './modal-person-header.component';
import { createServiceStub } from 'testing/create-service-stub';
import { configureTestSuite } from 'ng-bullet';
import { authServiceStub } from 'app/shared/services/auth-service/testing';
import { validatorFormServiceStub } from 'app/shared/services/shared/testing/validator-form';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ActivatedRoute } from '@angular/router';

describe('ModalPersonHeaderComponent', () => {
  let component: ModalPersonHeaderComponent;
  let fixture: ComponentFixture<ModalPersonHeaderComponent>;
  let billingAccountResource: BillingAccountResource;
  let personResource: PersonResource;
  const thxErrorStub = createAccessorComponent('thx-error');

  // TODO: move all stubs to correctly folder [DEBIT: 7506]
  const sharedServiceStub = createServiceStub(SharedService, {
    getButtonConfig(id: string,
      callback: Function,
      textButton: string,
      buttonType: ButtonType,
      buttonSize?: ButtonSize,
      isDisabled?: boolean,
      type?: Type): ButtonConfig { return new ButtonConfig(); },
      getHeaders(): any { return ''; }
  });

  const documentTypeResourceStub = createServiceStub(DocumentTypeResource, {
    getAllByPropertyCountryCode (): Observable<any> { return of({items: []}); }
  });

  const commomServiceStub = createServiceStub(CommomService, {
    toOption(objectToConvert: any,
      keyName: string,
      valueName: string): any { return []; }
  });

  const addressServiceStub = createServiceStub(AddressService, {
    getLocationCategoryEnumByLocationCategoryId(locationCategoryId: number): AddressType { return AddressType.Comercial; }
  });

  const billingAccountResourceStub = createServiceStub(BillingAccountResource, {
    getPersonHeaderByBillingAccountId(billingAccountId: string): Observable<PersonHeaderDto> { return of(new PersonHeaderDto()); },
    associateHeaderPerson(dto: PersonHeaderDto): Observable<PersonHeaderDto> { return of(new PersonHeaderDto()); }
  });

  const toasterEmitServiceStub = createServiceStub(ToasterEmitService, {
    emitChange(isSuccess: SuccessError, message: string) {}
  });

  const personResourceStub = createServiceStub(PersonResource, {
    getPersonListByDocument(criteria: string, documentTypeId: number): Observable<any> { return of({items: []}); }
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [
        ModalPersonHeaderComponent,
        thxErrorStub
      ],
      providers: [
        sharedServiceStub,
        documentTypeResourceStub,
        commomServiceStub,
        addressServiceStub,
        PersonHeaderMapper,
        billingAccountResourceStub,
        toasterEmitServiceStub,
        personResourceStub,
        authServiceStub,
        validatorFormServiceStub,
        FormBuilder,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ]
    });

    billingAccountResource = TestBed.get(BillingAccountResource);
    personResource = TestBed.get(PersonResource);

    fixture = TestBed.createComponent(ModalPersonHeaderComponent);
    component = fixture.componentInstance;

    component.modalId = '';
    component.billingAccountId = '';

    spyOn<any>(component, 'getPersonHeader').and.callThrough();
    spyOn<any>(component, 'getDocumentTypes').and.callThrough();
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should getDocumentInformation', () => {
    const document = '123456';
    component.personHeaderForm.get('document').setValue(document);

    expect(component.getDocumentInformation()).toEqual(document);
  });

  it('should updateItemDocument when is completeItem', () => {
      const item = new UpdateItemResponse<SearchPersonsResultDto>();
      item.completeItem = new SearchPersonsResultDto();
      item.completeItem.document = '123456789';
      item.completeItem.documentType = 'NIF';
      item.completeItem.name = 'Teste Um';
      item.completeItem.locationList = new Array<LocationDto>();
      item.completeItem.locationList.push(new LocationDto());
      item.completeItem.contactInformation = new Array<{ type: any, typeValue: string, value: string }>();
      item.completeItem.contactInformation.push({
        type: null,
        typeValue: 'Email',
        value: 'teste1@email.com'
      });
      item.completeItem.contactInformation.push({
        type: null,
        typeValue: 'PhoneNumber',
        value: '123345678'
      });

      component.updateItemDocument(item);

      expect(component.personHeaderForm.get('document').value).toEqual('123456789');
      expect(component.personHeaderForm.get('email').value).toEqual('teste1@email.com');
      expect(component.personHeaderForm.get('phone').value).toEqual('123345678');
      expect(component.personHeaderForm.get('fullName').value).toEqual('Teste Um');
  });

  it('should updateItemDocument when is string and personId is not null', () => {
    const document = '12345678910';
    const personId = '342af01e-c8ad-471d-8116-5f50ee4c1add';
    component.personHeaderForm.get('personId').setValue(personId);
    component['personId'] = personId;

    component.updateItemDocument(document);

    expect(component.personHeaderForm.get('personId').value).toEqual(personId);
    expect(component.personHeaderForm.get('document').value).toEqual(document);
  });

  it('should updateItemDocument when is string and personId is null', () => {
    const document = '12345678910';
    component['personId'] = null;

    component.updateItemDocument(document);

    expect(component.personHeaderForm.get('personId').value).toEqual(null);
    expect(component.personHeaderForm.get('document').value).toEqual(document);
  });

  describe('on setIsRequired', () => {
    let emailField, personHeaderForm;
    beforeEach(() => {
      emailField = component.personHeaderForm.get('email');
      personHeaderForm = component.personHeaderForm;

      spyOn(emailField, 'clearValidators');
      spyOn(personHeaderForm, 'updateValueAndValidity');
    });

    it('should test when return not PT', () => {
      spyOn(component['authService'], 'getPropertyCountryCode').and.returnValue('FR');

      component.setIsRequired();

      expect(component.isRequired).toEqual(true);
      expect(emailField.clearValidators).not.toHaveBeenCalled();
      expect(personHeaderForm.updateValueAndValidity).not.toHaveBeenCalled();
    });

    it('should test when return PT', () => {
      spyOn(component['authService'], 'getPropertyCountryCode').and.returnValue('PT');
      component.setIsRequired();

      expect(component.isRequired).toEqual(false);
      expect(emailField.clearValidators).toHaveBeenCalled();
      expect(personHeaderForm.updateValueAndValidity).toHaveBeenCalled();
    });
  });
});
