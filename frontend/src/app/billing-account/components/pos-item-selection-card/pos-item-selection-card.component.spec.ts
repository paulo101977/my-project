import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { PosItemSelectionCardComponent } from './pos-item-selection-card.component';
import { configureTestSuite } from 'ng-bullet';
import { currencyFormatPipe, imgCdnStub } from '../../../../../testing';


describe('PosItemSelectionCardComponent', () => {
  let component: PosItemSelectionCardComponent;
  let fixture: ComponentFixture<PosItemSelectionCardComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
          PosItemSelectionCardComponent,
          imgCdnStub,
          currencyFormatPipe,
        ],
        schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(PosItemSelectionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
