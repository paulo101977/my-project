import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';

@Component({
  selector: 'pos-item-selection-card',
  templateUrl: './pos-item-selection-card.component.html',
  styleUrls: ['./pos-item-selection-card.component.scss']
})
export class PosItemSelectionCardComponent {

  @Input() currencySymbol: string;
  @Input() item: any;
  @Input() isSelected = false;

  @Output() selectionChange = new EventEmitter();

  public changeSelected() {

    // don't emit two times
    if (!this.isSelected) {
      this.item.isSelected = true;
      this.isSelected = true;
      this.item.quantity = 1;
      this.selectionChange.emit(true);
    }
  }

}
