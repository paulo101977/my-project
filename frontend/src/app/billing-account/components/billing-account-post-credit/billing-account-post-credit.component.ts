import { FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { PaymentTypeWithBillingItem } from '../../../shared/models/billingType/payment-type-with-billing-item';
import { PaymentTypeEnum } from '../../../shared/models/reserves/payment-type-enum';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { BillingItemResource } from '../../../shared/resources/billingItem/billing-item.resource';

@Component({
  selector: 'billing-account-post-credit',
  templateUrl: './billing-account-post-credit.component.html',
})
export class BillingAccountPostCreditComponent implements OnInit {
  @Input() propertyId: number;
  @Input() formModal: FormGroup;

  @Output() paymentTypeWithBillingItemListWasUpdated = new EventEmitter<Array<PaymentTypeWithBillingItem>>();

  public isCard: boolean;
  public isBankCheck: boolean;
  public paymentTypeList: Array<SelectObjectOption>;
  public optionsCurrencyMask: any;
  public paymentTypeIdChosen: number;
  private readonly totalPriceDefault = 0;

  constructor(private billingItemResource: BillingItemResource, private commomService: CommomService) {}

  ngOnInit() {
    this.setVars();
    this.getPaymentTypeList();
    this.setListeners();
  }

  private setVars(): void {
    this.isCard = false;
    this.isBankCheck = false;
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
  }

  private getPaymentTypeList(): void {
    this.paymentTypeList = new Array<SelectObjectOption>();
    this.billingItemResource.getAllPaymentTypeWithBillingItemListByPropertyId(this.propertyId).subscribe(response => {
      this.paymentTypeList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(response, 'id', 'paymentTypeName');
      this.paymentTypeWithBillingItemListWasUpdated.emit(response.items);
    });
  }

  private isCardOrBankCheck(paymentTypeWithBillingItem: PaymentTypeWithBillingItem): boolean {
    return paymentTypeWithBillingItem.id == PaymentTypeEnum.CreditCard || paymentTypeWithBillingItem.id == PaymentTypeEnum.Debit;
  }

  private setListeners(): void {
    this.formModal.get('postCredit.paymentTypeId').valueChanges.subscribe(paymentTypeId => {
      if (paymentTypeId == PaymentTypeEnum.Check) {
        this.isBankCheck = true;
        this.isCard = false;
        this.clearValidatorsInCardFormGroup();
      } else if (paymentTypeId == PaymentTypeEnum.CreditCard || paymentTypeId == PaymentTypeEnum.Debit) {
        this.isBankCheck = false;
        this.isCard = true;
        this.paymentTypeIdChosen = paymentTypeId;
        this.setValidatorsInCardFormGroup();
      } else {
        this.isBankCheck = false;
        this.isCard = false;
        this.clearValidatorsInCardFormGroup();
      }
      this.clearDataOfPostCreditFormGroup();
    });

    this.formModal.get('postCredit.price').valueChanges.subscribe(price => {
      if (price) {
        this.formModal.get('totalPrice').setValue(price.toFixed(2));
      }
    });
  }

  private setValidatorsInCardFormGroup(): void {
    this.formModal.get('postCredit.card.plasticBrandPropertyId').setValidators([Validators.required]);
  }

  private clearValidatorsInCardFormGroup(): void {
    this.formModal.get('postCredit.card.plasticBrandPropertyId').clearValidators();
  }

  private clearDataOfPostCreditFormGroup(): void {
    this.formModal.get('totalPrice').setValue(this.totalPriceDefault.toFixed(2));
    this.formModal.get('postCredit.price').setValue(null);
    this.formModal.get('postCredit.card.plasticBrandPropertyId').setValue(null);
    this.formModal.get('postCredit.card.nsuCode').setValue(null);
    this.formModal.get('postCredit.bankCheck.number').setValue(null);
  }
}
