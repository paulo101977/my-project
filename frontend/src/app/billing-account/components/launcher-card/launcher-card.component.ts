import { Component, Input } from '@angular/core';

@Component({
  selector: 'launcher-card',
  templateUrl: './launcher-card.component.html',
  styleUrls: ['./launcher-card.component.scss']
})
export class LauncherCardComponent {

  @Input() item: any;

}
