import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { BillingAccountWithAccount } from '../../../shared/models/billing-account/billing-account-with-account';
import { ButtonType } from '../../../shared/models/button-config';
import { ButtonConfig } from '../../../shared/models/button-config';
import { BillingAccountTypeEnum } from '../../../shared/models/billing-account/billing-account-type-enum';
import { ButtonSize } from './../../../shared/models/button-config';
import { BillingAccountWithEntry } from './../../../shared/models/billing-account/billing-account-with-entry';
import { BillingAccountStatusEnum } from '../../../shared/models/billing-account/billing-account-status-enum';
import { BillingAccountReopenReason } from '../../../shared/models/billing-account/billing-account-reopen-reason';
import { ReasonEnum } from '../../../shared/models/reason-enum';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ConfirmModalDataConfig } from '../../../shared/models/confirm-modal-data-config';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { SharedService } from './../../../shared/services/shared/shared.service';
import { PropertyService } from './../../../shared/services/property/property.service';
import { ToasterEmitService } from './../../../shared/services/shared/toaster-emit.service';
import { BillingAccountClosureMapper } from './../../../shared/mappers/billing-account-closure-mapper';
import { BillingAccountResource } from '../../../shared/resources/billing-account/billing-account.resource';
import { BillingAccountClosureResource } from './../../../shared/resources/billing-account/billing-account-closure.resource';
import {BillingLauncherComponent} from '../../components-container/billing-launcher/billing-launcher.component';

@Component({
  selector: 'app-billing-account-detail-header',
  templateUrl: './billing-account-detail-header.component.html',
  styleUrls: ['./billing-account-detail-header.component.css'],
})
export class BillingAccountDetailHeaderComponent implements OnInit, OnChanges {
  @Input() reservationItemId: number;
  @Input() billingAccount: BillingAccountWithAccount;
  @Input() billingAccountWithEntryCurrent: BillingAccountWithEntry;
  @Output() reopenBillingAccountUpdate = new EventEmitter();
  @Output() updateBillingAccountAndHeaderAndCardList = new EventEmitter();
  public referenceToBillingAccountTypeEnum = BillingAccountTypeEnum;
  public statusAccountOpen: boolean;
  public reasonToReopenBillingAccountList: Array<BillingAccountReopenReason>;
  public reasonSelected: BillingAccountReopenReason;
  public propertyId: number;
  public subscription: Subscription;

  // Buttons
  public buttonExtractConfig: ButtonConfig;
  public buttonCloseConfig: ButtonConfig;
  public buttonLaunchConfig: ButtonConfig;
  public reopenConfig: ButtonConfig;
  public confirmReopenModalButtonConfig: ButtonConfig;
  public cancelReopenModalButtonConfig: ButtonConfig;
  public confirmFinishModalButtonConfig: ButtonConfig;
  public cancelFinishModalButtonConfig: ButtonConfig;

  // Modal dependencies
  public confirmModalDataConfig: ConfirmModalDataConfig;
  public modalCloseAccountWithTotalZeroId: string;

  constructor(
    private modalEmitToggleService: ModalEmitToggleService,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private billingAccountResource: BillingAccountResource,
    private billingAccountClosureResource: BillingAccountClosureResource,
    private propertyService: PropertyService,
    private toasterEmitService: ToasterEmitService,
    private billingAccountClosureMapper: BillingAccountClosureMapper,
    private translateService: TranslateService,
  ) {}

  ngOnChanges(changes) {
    if (
      changes.billingAccountWithEntryCurrent &&
      changes.billingAccountWithEntryCurrent.currentValue
    ) {
      this.buttonExtractConfig.isDisabled = false;
      this.buttonCloseConfig.isDisabled = false;
    }

    if (
      changes.billingAccountWithEntryCurrent &&
      changes.billingAccountWithEntryCurrent.currentValue &&
      changes.billingAccountWithEntryCurrent.currentValue.statusId
    ) {
      this.statusAccountOpen = changes.billingAccountWithEntryCurrent.currentValue.statusId == BillingAccountStatusEnum.Open;
    }
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.propertyId = params.property;
      this.getReopenReasonList();
    });
    this.setVars();
    this.setButtonConfig();
    this.setConfirmModalDataConfig();
  }

  private setVars() {
    this.modalCloseAccountWithTotalZeroId = 'modal-close-account-with-zero-balance';
  }

  private setConfirmModalDataConfig(): void {
    this.confirmModalDataConfig = new ConfirmModalDataConfig();
    this.confirmModalDataConfig.textConfirmModal = 'text.finishAccountConfirm';
    this.confirmModalDataConfig.actionCancelButton = this.cancelFinishModalButtonConfig;
    this.confirmModalDataConfig.actionConfirmButton = this.confirmFinishModalButtonConfig;
  }

  private getReopenReasonList() {
    this.reasonToReopenBillingAccountList = new Array<BillingAccountReopenReason>();
    this.billingAccountResource.getReasonReopenList(this.propertyId, ReasonEnum.ReopenBillingAccount).subscribe(reasonList => {
      this.reasonToReopenBillingAccountList = reasonList.items;
    });
  }

  public setBillingAccountReopenReason(reasonSelected: BillingAccountReopenReason) {
    if (reasonSelected && reasonSelected.id) {
      this.reasonSelected = reasonSelected;
      this.confirmReopenModalButtonConfig.isDisabled = false;
    } else {
      this.reasonSelected = null;
      this.confirmReopenModalButtonConfig.isDisabled = true;
    }
  }

  private goToBillingLauncher = () => {
    this
      .router
      .navigate([`/p/${this.propertyId}/billing-account/launcher`], {
        relativeTo: this.route,
        queryParams: {
          billingAccountId: this.billingAccountWithEntryCurrent.billingAccountId,
          reservationItemId: this.reservationItemId,
          accountName: this.billingAccount.holderName
        },
      });
  }

  private setButtonConfig(): void {
    this.buttonExtractConfig = this.sharedService.getButtonConfig(
      'extract',
      this.goToStatementPage,
      'billingAccountModule.commomData.extract',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.buttonCloseConfig = this.sharedService.getButtonConfig(
      'close',
      this.finishBillingAccount,
      'billingAccountModule.commomData.close',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.buttonLaunchConfig = this.sharedService.getButtonConfig(
      'launch',
      this.goToBillingLauncher,
      'billingAccountModule.commomData.launch',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.reopenConfig = this.sharedService.getButtonConfig(
      'reopen',
      this.openOrCloseReopenModal,
      'label.reopen',
      ButtonType.Primary,
      ButtonSize.Normal,
    );
    this.cancelReopenModalButtonConfig = this.sharedService.getButtonConfig(
      'cancel-reopen',
      this.openOrCloseReopenModal,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.confirmReopenModalButtonConfig = this.sharedService.getButtonConfig(
      'confirm-reopen',
      this.reopenBillingAccount,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true,
    );
    this.cancelFinishModalButtonConfig = this.sharedService.getButtonConfig(
      'cancel-finish',
      this.openOrCloseFinishModal,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.confirmFinishModalButtonConfig = this.sharedService.getButtonConfig(
      'confirm-finish',
      this.confirmFinishAccount,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
    );
    this.buttonExtractConfig.isDisabled = true;
    this.buttonCloseConfig.isDisabled = true;
  }

  private openOrCloseReopenModal = () => {
    this.modalEmitToggleService.emitToggleWithRef('reopen-billing-account');
  }

  public reopenBillingAccount = () => {
    this.billingAccountResource
      .reopenBillingAccountClosed(this.propertyId, this.billingAccountWithEntryCurrent.billingAccountId, this.reasonSelected.id)
      .subscribe(() => {
        this.reopenBillingAccountUpdate.emit(this.billingAccount.mainBillingAccountId);
        this.modalEmitToggleService.emitToggleWithRef('reopen-billing-account');
      });
  }

  private goToStatementPage = () => {
    this.router.navigate(['../statement'], {
      relativeTo: this.route,
      queryParams: {
        id: this.billingAccountWithEntryCurrent.billingAccountId,
        groupKey: this.billingAccount.groupKey,
        statementIndividual: true,
        reservationItemId: this.reservationItemId,
      },
    });
  }

  private finishBillingAccount = () => {
    if (this.billingAccountWithEntryCurrent.total == 0) {
      this.openOrCloseFinishModal();
    } else {
      this.goToStatementPage();
    }
  }

  private openOrCloseFinishModal = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.modalCloseAccountWithTotalZeroId);
  }

  private toggleDebitOrCreditPostModal = () => {
     this.modalEmitToggleService.emitToggleWithRef('post-debit-credit');
  }

  public confirmFinishAccount = () => {
    const billingAccountClosure = this.billingAccountClosureMapper.toBillingAccountClosureWhenTotalIsZero(
      this.billingAccountWithEntryCurrent.billingAccountId,
      this.propertyId,
    );
    this.billingAccountClosureResource.payBillingAccount(billingAccountClosure).subscribe(
      response => {
        this.openOrCloseFinishModal();
        this.toasterEmitService.emitChangeTwoMessages(
          SuccessError.success,
          this.translateService.instant('text.account'),
          this.translateService.instant('variable.finishDoneSuccessF'),
        );
        this.updateBillingAccountAndHeaderAndCardList.emit();
      }
    );
  }
}
