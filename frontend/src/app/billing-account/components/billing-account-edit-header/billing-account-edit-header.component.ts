import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BillingAccountHeader } from '../../../shared/models/billing-account/billing-account-header';
import { RightButtonTop } from '../../../shared/models/right-button-top';
import { ShowHideContentEmitService } from '../../../shared/services/shared/show-hide-content-emit-service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ReservationStatusEnum } from '../../../shared/models/reserves/reservation-status-enum';
import { BillingAccountSidebar } from '../../../shared/models/billing-account/billing-account-sidebar';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { BillingAccountTypeEnum } from '../../../shared/models/billing-account/billing-account-type-enum';
import { ReservationService } from '../../../shared/services/reservation/reservation.service';
import { AssetsService } from '@app/core/services/assets.service';

@Component({
  selector: 'app-billing-account-edit-header',
  templateUrl: './billing-account-edit-header.component.html',
  styleUrls: ['./billing-account-edit-header.component.css'],
})
export class BillingAccountEditHeaderComponent implements OnInit, OnChanges {
  private MODAL_RELEASE_UH_REF = 'releaseUh';
  public statusColorName: any;

  @Input() reservationItemId: number;
  @Input() billingAccountHeader: BillingAccountHeader;
  @Input() billingAccountSidebarList: Array<BillingAccountSidebar>;

  public configHeaderPage: ConfigHeaderPageNew;
  public hasZeroRemarks: boolean;
  public hasOneRemarks: boolean;
  public hasTwoRemarks: boolean;
  public showPopover: boolean;
  public referenceToBillingAccountTypeEnum = BillingAccountTypeEnum;
  private billingAccountIdInParams: string;
  public topButtonsListConfig: Array<RightButtonTop>;

  @Output() notifyChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private showHideContentEmitService: ShowHideContentEmitService,
    private modalService: ModalEmitToggleService,
    public router: Router,
    public route: ActivatedRoute,
    public reservationService: ReservationService,
    private assetsService: AssetsService,
  ) {}

  public ngOnInit() {
    this.route.queryParams.subscribe(queryParams => {
      this.billingAccountIdInParams = queryParams.billingAccountId;
    });
    this.showPopover = false;
    this.setConfigHeaderPage();
    this.showHideContentEmitService.changeEmitted$.subscribe(response => {
      this.showPopover = false;
    });
  }

  public ngOnChanges(changes: any) {
    this.setRemarksQuantity();
    this.setColorStatus();

    if (this.billingAccountSidebarList && this.billingAccountSidebarList.length > 0) {
      this.setTopButtonsListConfig();
    }
  }

  private setTopButtonsListConfig(): void {
    this.topButtonsListConfig = [];

    let countsClosed = 0;
    this.billingAccountSidebarList.forEach(item => {
      if (item.isClosed) {
        countsClosed++;
      }
    });

    const topButtonStatementTotal = new RightButtonTop();
    topButtonStatementTotal.title = 'label.statementTotal';
    topButtonStatementTotal.callback = this.goToStatementPage.bind(this);
    topButtonStatementTotal.iconDefault = this.assetsService.getImgUrlTo('icon_extrato.min.svg');
    topButtonStatementTotal.iconAlt = 'label.statement';

    const topButtonConsolidatePayment = new RightButtonTop();
    topButtonConsolidatePayment.title = 'action.finish';
    topButtonConsolidatePayment.callback = this.goToStatementPage.bind(this);
    topButtonConsolidatePayment.iconDefault = this.assetsService.getImgUrlTo('credit-card.svg');
    topButtonConsolidatePayment.iconAlt = 'label.statement';

    const topButtonReleaseUh = new RightButtonTop();
    topButtonReleaseUh.title = 'title.releaseUH';
    topButtonReleaseUh.callback = this.callRealaseUh.bind(this);
    topButtonReleaseUh.iconDefault = this.assetsService.getImgUrlTo('bed_blue.svg');
    topButtonReleaseUh.iconAlt = 'img.releaseUh';

    const checkoutButton = new RightButtonTop();
    checkoutButton.title = 'title.checkout';
    checkoutButton.callback = this.checkout.bind(this);
    checkoutButton.iconDefault = this.assetsService.getImgUrlTo('checkout.svg');
    checkoutButton.iconAlt = 'img.checkout';

    this.topButtonsListConfig.push(topButtonStatementTotal);

    if (countsClosed != this.billingAccountSidebarList.length) {
      this.topButtonsListConfig.push(topButtonConsolidatePayment);
      if (this.billingAccountHeader && (
        this.billingAccountHeader.reservationItemStatusId == ReservationStatusEnum.Checkin ||
        this.billingAccountHeader.reservationItemStatusId == ReservationStatusEnum.Checkout
      )) {
        this.topButtonsListConfig.push(topButtonReleaseUh);
      }
    } else {
      if (this.billingAccountHeader && (
        this.billingAccountHeader.reservationItemStatusId == ReservationStatusEnum.Checkin ||
        this.billingAccountHeader.reservationItemStatusId == ReservationStatusEnum.Pending )) {
        this.topButtonsListConfig.push(checkoutButton);
      }
    }

    if (this.doNotShowTopButtonsList()) {
      this.topButtonsListConfig = [];
    }
  }

  private checkout() {
    this.goToStatementPage(true);
  }

  private doNotShowTopButtonsList(): boolean {
    return (
      this.billingAccountHeader &&
      (this.billingAccountHeader.billingAccountTypeId == BillingAccountTypeEnum.Sparse ||
        this.billingAccountHeader.billingAccountTypeId == BillingAccountTypeEnum.GroupAccount)
    );
  }

  public goToStatementPage(withCheckout?: boolean) {
    this.router.navigate(['../statement'], {
      relativeTo: this.route,
      queryParams: {
        id: this.billingAccountIdInParams,
        statementIndividual: false,
        reservationItemId: this.reservationItemId,
        checkout: withCheckout,
      },
    });
  }

  public callRealaseUh() {
    this.modalService.emitToggleWithRef(this.MODAL_RELEASE_UH_REF);
  }

  public closeModalReleaseUh() {
    this.modalService.emitToggleWithRef(this.MODAL_RELEASE_UH_REF);
  }

  private setRemarksQuantity(): void {
    this.hasZeroRemarks = this.billingAccountHeader && this.billingAccountHeader.quantityComments == 0;
    this.hasOneRemarks = this.billingAccountHeader && this.billingAccountHeader.quantityComments == 1;
    this.hasTwoRemarks = this.billingAccountHeader && this.billingAccountHeader.quantityComments == 2;
  }

  public setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  public showRemarksPopover(): void {
    this.showPopover = true;
  }

  public setColorStatus() {
    if (this.billingAccountHeader && this.billingAccountHeader.statusName) {
      this.statusColorName = this.reservationService.getStatusColorByReservationStatus(this.billingAccountHeader.reservationItemStatusId);
    }
  }

  public notifyRelease() {
    this.modalService.emitToggleWithRef(this.MODAL_RELEASE_UH_REF);
    this.notifyChange.emit();
  }
}
