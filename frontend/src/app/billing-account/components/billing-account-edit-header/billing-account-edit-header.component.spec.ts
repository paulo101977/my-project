import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting
} from '@angular/platform-browser-dynamic/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core/';
import { RouterTestingModule } from '@angular/router/testing';
import { BillingAccountEditHeaderComponent } from './billing-account-edit-header.component';
import { ReactiveFormsModule } from '@angular/forms';

import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { currencyFormatPipe, imgCdnStub } from '../../../../../testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('BillingAccountEditHeaderComponent', () => {
  let component: BillingAccountEditHeaderComponent;
  let fixture: ComponentFixture<BillingAccountEditHeaderComponent>;

  configureTestSuite(() => {
    TestBed.resetTestEnvironment();
    TestBed
    .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
    .configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],
      declarations: [
          BillingAccountEditHeaderComponent,
          imgCdnStub,
          currencyFormatPipe
      ],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(BillingAccountEditHeaderComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should set configHeaderPage, setConfigTable', () => {
      expect(component.configHeaderPage).toEqual({
        hasBackPreviewPage: true,
      });
    });
  });
});
