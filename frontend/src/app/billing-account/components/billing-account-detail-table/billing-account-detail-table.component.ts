import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { BillingAccountItemEntry } from 'app/shared/models/billing-account/billing-account-item-entry';
import { BillingAccountWithEntry } from 'app/shared/models/billing-account/billing-account-with-entry';
import { BillingAccountItemTypeEnum } from 'app/shared/models/billing-account/billing-account-item-type-enum';
import { BillingAccountStatusEnum } from 'app/shared/models/billing-account/billing-account-status-enum';
import { DateService } from 'app/shared/services/shared/date.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { BillingAccountItemService } from 'app/shared/services/billing-account-item/billing-account-item.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { BillingAccountService } from 'app/shared/services/billing-account/billing-account.service';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';
import { BillingInvoiceResource } from 'app/shared/resources/Invoice/billing-invoice.resource';
import * as _ from 'lodash';
import { ReasonResource } from 'app/shared/resources/reason/reason.resource';
import { ReasonCategoryEnum } from 'app/shared/models/reason/reason-category-enum';
import { Reason } from 'app/shared/models/dto/reason';
import { FiscalDocumentsService } from 'app/shared/services/billing-account/fiscal-documents.service';
import { TranslateService } from '@ngx-translate/core';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { NfStatusEnum } from 'app/shared/models/dto/nf/nf-status-enum';
import { BillingAccountNotifierService } from '../../services/billing-account-notifier.service';
import { UndoSplitDto } from '../../undo-split/models/undo-split-dto';
import { UndoSplitResource } from '../../undo-split/resources/undo-split.resource';
import { CountryCodeEnum } from '@app/shared/models/country-code-enum';
import { PropertyService } from '@app/shared/services/property/property.service';
import { BillingAccountItemCreditAmountDto } from '@app/shared/models/dto/billing-account-item/billing-account-item-credit-amount-dto';
import { PdfPrintService } from 'app/shared/services/pdf-print/pdf-print.service';
import { BillingAccountWithEntryService } from 'app/billing-account/services/billing-account-with-entry.service';
import { BillingAccountWithEntryStates } from 'app/billing-account/models/billing-account-with-entry-states';

@Component({
  selector: 'billing-account-detail-table',
  templateUrl: 'billing-account-detail-table.component.html',
  styleUrls: ['billing-account-detail-table.component.css'],
})
export class BillingAccountDetailTableComponent implements OnInit, OnChanges {
  public readonly MODAL_STATUS_FISCAL_NOTE = 'fiscalNotes';
  public readonly MODAL_ERROR = 'modalErrorNfes';
  public readonly MODAL_CANCEL_NFSE = 'modalCancelNfe';
  public readonly MODAL_CONFIRM_CANCEL_NFSE = 'modalConfirmCancelNfe';
  public readonly MODAL_SPLIT_ENTRY = 'modalSplitEntry';
  public readonly MODAL_UNDO_SPLIT_ENTRY = 'modalUndoSplitEntry';
  public readonly MODAL_PERSON_HEADER = 'modalPersonHeader';
  public readonly MODAL_EUROPE_FISCAL_DOCUMENT = 'modalEuropeFiscalDocument';
  public readonly MODAL_CREDIT_NOTE = 'modalCreditNote';
  public readonly MODAL_CREDIT_NOTE_PAYMENT_TYPE = 'modalCreditNotePaymentType';
  public readonly MODAL_DOCUMENT_RESEND = 'documentResendModal';

  @Input() holderName: string;
  @Input() propertyId: number;
  @Input() billingAccountWithEntry: BillingAccountWithEntry;
  @Input() setDefaultActionButtonsToEntries: boolean;
  @Input() reservationItemId: number;

  @Output() isCheckedOfBillingAccountItemEntryListUpdated = new EventEmitter();
  @Output() transferBillingAccountItem = new EventEmitter<string>();
  @Output() reverseBillingAccountItem = new EventEmitter<string>();
  @Output() discountBillingAccountItem = new EventEmitter<string>();
  @Output() paymentPartialBillingAccountItem = new EventEmitter<any>();
  @Output() shouldRefresh = new EventEmitter();
  @Output() issuedCreditNote = new EventEmitter<string>();

  public allItemsAreChecked: boolean;
  public referenceToDisplayAutocompleteEnum = BillingAccountItemTypeEnum;
  public referenceToBillingAccountStatusEnum = BillingAccountStatusEnum;
  public discountBtn: boolean;
  public splitBtn: boolean;
  public undoSplitBtn: boolean;
  public transferBtn: boolean;
  public reverseBtn: boolean;
  public paymentPartial: boolean;
  public invoiceWithError: InvoiceDto;
  public invoiceToCancel: InvoiceDto;
  public reasonList: Array<Reason>;
  public billingAccountItemList: Array<BillingAccountItemCreditAmountDto>;
  public billingItemIdToIssueCreditNote: number;
  public bilingItemSelected: InvoiceDto;

  public propertyCountryCode: string;

  // Buttons dependencies
  public buttonTransferConfig: ButtonConfig;
  public buttonReverseConfig: ButtonConfig;
  public buttonSplitConfig: ButtonConfig;
  public buttonUndoSplitConfig: ButtonConfig;
  public buttonDiscountConfig: ButtonConfig;
  public buttonPaymentPartialConfig: ButtonConfig;
  public buttonPrintFiscalNotesConfig: ButtonConfig;
  public buttonPersonHeaderConfig: ButtonConfig;

  public itemToSplit: BillingAccountItemEntry;
  public itemsToUndoSplit: BillingAccountItemEntry[];
  public accountIdToSplit: string;
  public accountIdToUndoSplit: string;
  public billingInvoiceIdToIssueCreditNote: string;
  public billingAccountId;
  public currencySymbol: any;
  public showHeaderButton = false;
  public billingAccountWithEntryStates: BillingAccountWithEntryStates;

  public invoices: Array<InvoiceDto>;

  // Table dependencies
  @ViewChild('myTable') table: DatatableComponent;

  widthColumns = Array<number>();
  public rows = [];

  constructor(
    private sharedService: SharedService,
    private dateService: DateService,
    private modalEmitToggleService: ModalEmitToggleService,
    private toasterEmitService: ToasterEmitService,
    private billingAccountItemService: BillingAccountItemService,
    private billingAccountService: BillingAccountService,
    private router: Router,
    private route: ActivatedRoute,
    private currencyService: CurrencyService,
    private billingInvoiceResource: BillingInvoiceResource,
    private reasonResource: ReasonResource,
    private fiscalDocumentService: FiscalDocumentsService,
    private translateService: TranslateService,
    private billingAccountNotifierService: BillingAccountNotifierService,
    private undoSplitResource: UndoSplitResource,
    private propertyService: PropertyService,
    private pdfPrintService: PdfPrintService,
    private billingAccountWithEntryService: BillingAccountWithEntryService
  ) {}

  ngOnChanges(changes: any) {
    if (this.billingAccountWithEntry) {
      this.rows = [...this.billingAccountWithEntry.billingAccountItems];
      this.billingAccountId = this.billingAccountWithEntry.billingAccountId;

      this.updateBillingAccountStates();
    }
  }

  private updateBillingAccountStates() {
    this.billingAccountWithEntryStates = this.billingAccountWithEntryService.getStates(this.billingAccountWithEntry);
    this.showHeaderButton = this.billingAccountWithEntryStates.isOpen && !this.billingAccountWithEntryStates.hasIssuedInvoices;
  }

  ngOnInit() {
    this.setPropertyCountryCode();
    this.setVars();
    this.getReasonList();
    this.currencySymbol =  this.currencyService.getDefaultCurrencySymbol(this.propertyId);
  }

  private setPropertyCountryCode() {
    this.propertyCountryCode = this.propertyService.getPropertyCountryCode();
  }

  private setVars(): void {
    this.setActionsButtonsForEntries();
    this.buttonTransferConfig = this.sharedService.getButtonConfig(
      'transfer',
      this.transferBillingAccountItemCallback,
      'billingAccountModule.commomData.transfer',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.buttonReverseConfig = this.sharedService.getButtonConfig(
      'reverse',
      this.reverseBillingAccountItemCallback,
      'billingAccountModule.commomData.reverse',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.buttonSplitConfig = this.sharedService.getButtonConfig(
      'slip',
      this.splitBillingAccountItem,
      'action.split',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.buttonUndoSplitConfig = this.sharedService.getButtonConfig(
      'undoSlip',
      this.undoSplitBillingAccountItem,
      'action.undoSplit',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.buttonDiscountConfig = this.sharedService.getButtonConfig(
      'slip',
      this.discountBillingAccountItems,
      'label.discount',
      ButtonType.Secondary,
      ButtonSize.Normal
    );
    this.buttonPaymentPartialConfig = this.sharedService.getButtonConfig(
      'partial',
      this.paymentPartialBillingAccountItemsChecked,
      'action.paymentPartial',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.buttonPrintFiscalNotesConfig = this.sharedService.getButtonConfig(
      'printNotes',
      this.showStatusNote.bind(this),
      this.propertyCountryCode == CountryCodeEnum.BR ? 'label.notesAnsRps' : 'title.fiscalDocuments',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.buttonPersonHeaderConfig = this.sharedService.getButtonConfig(
      'showPersonHeader',
      this.showPersonHeader.bind(this),
      'label.personHeaderNote',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.buttonPrintFiscalNotesConfig.extraClass = 'btn-fiscal-documents';
    this.buttonTransferConfig.extraClass = '';
    this.buttonReverseConfig.extraClass = '';
    this.buttonSplitConfig.extraClass = '';
    this.buttonDiscountConfig.extraClass = '';
    this.buttonPaymentPartialConfig.extraClass = '';
    this.buttonPrintFiscalNotesConfig.extraClass = '';
    this.buttonUndoSplitConfig.extraClass = '';
    this.buttonPersonHeaderConfig.extraClass = '';
  }

  public setActionsButtonsForEntries() {
    this.discountBtn = false;
    this.transferBtn = false;
    this.splitBtn = false;
    this.undoSplitBtn = false;
    this.reverseBtn = false;
    this.paymentPartial = false;
  }

  public getHoursAndMinutes(date: Date): string {
    if (!date) {
      return '';
    }
    const dateData = new Date(date);
    return this.dateService.getHoursAndMinutes(dateData);
  }

  private transferBillingAccountItemCallback = () => {
    this.transferBillingAccountItem.emit(this.billingAccountWithEntry.billingAccountId);
  }

  private reverseBillingAccountItemCallback = () => {
    this.reverseBillingAccountItem.emit();
  }

  private splitBillingAccountItem = () => {
    this.itemToSplit = this.rows.filter(itemEntry => itemEntry.isChecked)[0];
    this.accountIdToSplit = this.billingAccountWithEntry.billingAccountId;
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_SPLIT_ENTRY);

  }

  private discountBillingAccountItems = () => {
    this.discountBillingAccountItem.emit();
  }

  private paymentPartialBillingAccountItemsChecked = () => {
    this.paymentPartialBillingAccountItem.emit(this.billingAccountWithEntry);
  }

  public setIsCheckedOfAllBillingAccountItemEntryToTrueOrFalse(): void {
    this.setActionsButtonsForEntries();
    if (this.allItemsAreChecked) {
      this.billingAccountWithEntry.billingAccountItems
        .filter( item => !item.billingInvoiceId && !item.integrationCode )
        .forEach(entry => {
        entry.isChecked = false;
      });
    } else {
      this.billingAccountWithEntry.billingAccountItems
        .filter( item => !item.billingInvoiceId && !item.integrationCode)
        .forEach(entry => {
        if (entry.billingAccountItemTypeId != BillingAccountItemTypeEnum.Reverse) {
          entry.isChecked = true;
        }
        this.setActionsButtonsForEntries();
        this.showActionsButtonForEntry(entry);
      });
    }
  }

  public setIsCheckedOfBillingAccountItemEntryToTrueOrFalse(billingAccountItemEntry: BillingAccountItemEntry) {
    this.setActionsButtonsForEntries();
    billingAccountItemEntry.isChecked = !billingAccountItemEntry.isChecked;
    this.showActionsButtonForEntry(billingAccountItemEntry);
  }

  private showActionsButtonForEntry(billingAccountItemEntry: BillingAccountItemEntry) {
    const rowsReference = this.rows;
    const entryList = rowsReference.filter(itemEntry => itemEntry.isChecked);
    const hasCreditChecked = rowsReference.find(itemEntry => {
      return itemEntry.isChecked && itemEntry.billingAccountItemTypeId == BillingAccountItemTypeEnum.Credit;
    });

    const hasDebitChecked = rowsReference.find(itemEntry => {
      return itemEntry.isChecked && itemEntry.billingAccountItemTypeId == BillingAccountItemTypeEnum.Debit;
    });

    if (hasCreditChecked && hasDebitChecked) {
      this.showActionsButtonToCreditAndDebit();
    } else {
      this.showActionsButtonToDebit(entryList);
      this.showActionsButtonToCredit(entryList);
    }
    if (entryList && (entryList.length == 1) ) {
      // should show the split button only if it wasn't splitted.
      this.splitBtn = this.canBeSplitted(entryList[0]);

      // should show the undoSplit buton only if it was splitted.
      this.undoSplitBtn = this.canBeUnsplitted(entryList[0]);
    }
  }

  private canBeUnsplitted(item: BillingAccountItemEntry) {
    const qtdItemsWithPartialId = this.rows
      .filter( element => element.partialBillingAccountItemId == item.partialBillingAccountItemId).length;
    return item.partialBillingAccountItemId && qtdItemsWithPartialId > 1;
  }

  private canBeSplitted(item: BillingAccountItemEntry) {
    if (!item.partialBillingAccountItemId) {
      return !item.isProduct;
    } else {
      return this.rows
        .filter( element => element.partialBillingAccountItemId == item.partialBillingAccountItemId).length <= 1;
    }
  }

  private showActionsButtonToCreditAndDebit() {
    this.transferBtn = true;
    this.reverseBtn = true;
  }

  private showActionsButtonToDebit(billingAccountItemEntry: Array<BillingAccountItemEntry>) {
    billingAccountItemEntry.forEach(itemEntry => {
      if (this.billingAccountService.showActionsButtonForDebitWithoutChildrensItems(itemEntry) && itemEntry.isChecked) {
        this.discountBtn = true;
        this.transferBtn = true;
        this.splitBtn = true;
        this.reverseBtn = true;
        this.paymentPartial = true;
      }

      if (this.billingAccountService.showActionsButtonForDebitWithChildrensItems(itemEntry) && itemEntry.isChecked) {
        this.discountBtn = true;
        this.transferBtn = true;
        this.reverseBtn = true;
        this.splitBtn = false;
        this.paymentPartial = true;
      }
    });
  }

  private showActionsButtonToCredit(billingAccountItemEntry: Array<BillingAccountItemEntry>) {
    billingAccountItemEntry.forEach(itemEntry => {
      if (this.billingAccountService.showActionsButtonForCreditWithoutChildrensItems(itemEntry) && itemEntry.isChecked) {
        this.transferBtn = true;
        this.splitBtn = true;
        this.reverseBtn = true;
        this.discountBtn = false;
        this.paymentPartial = false;
      }

      if (this.billingAccountService.showActionsButtonForCreditWithChildrensItems(itemEntry) && itemEntry.isChecked) {
        this.transferBtn = true;
        this.reverseBtn = true;
        this.discountBtn = false;
        this.splitBtn = false;
        this.paymentPartial = false;
      }
    });
  }

  public getRowClass(row: BillingAccountItemEntry) {
    return { 'thf-u-background-color--grey-sauvignon': row.billingAccountItemTypeId == BillingAccountItemTypeEnum.Reverse };
  }

  public getCellClass({ row, column, value }): any {
    return {
      'thf-u-text-decoration--line-through': row.billingAccountItemTypeId == BillingAccountItemTypeEnum.Reverse,
    };
  }

  public toggleExpandRow(row, expanded) {
    this.table.rowDetail.toggleExpandRow(row);
    this.widthColumns = this.table.headerComponent.columns.map(c => c.width);
  }

  public hasDiscount(element: BillingAccountItemEntry) {
    if (element && element.billingAccountChildrenItems) {
      return element.billingAccountChildrenItems.filter(item => {
        if (item.discount) {
          return item;
        }
      }).length > 0;
    }
    return false;
  }

  public getFormatedItemName(name) {
    return this.currencyService.formatToPercent(name);
  }

  public showStatusNote() {
    this.billingInvoiceResource
      .getInvoicesFromBillingAccountId(this.billingAccountWithEntry.billingAccountId)
      .subscribe( items => {
        this.invoices = [...items];
        if (this.propertyCountryCode == CountryCodeEnum.BR) {
          this.modalEmitToggleService.emitToggleWithRef(this.MODAL_STATUS_FISCAL_NOTE);
        } else if (this.propertyCountryCode == CountryCodeEnum.PT) {
          this.modalEmitToggleService.emitToggleWithRef(this.MODAL_EUROPE_FISCAL_DOCUMENT);
        }
      });

  }

  public openErrorModal(invoice: InvoiceDto) {
    this.invoiceWithError = invoice;
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_STATUS_FISCAL_NOTE);
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_ERROR);
  }

  public resendNfes(invoicesToResend: Array<InvoiceDto>) {
    this.billingInvoiceResource.resendManyInvoices(this.propertyId, invoicesToResend).subscribe(result => {
      this.shouldRefresh.emit();
    });
  }

  public onCloseErrorModal = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_STATUS_FISCAL_NOTE);
  }

  public billingAccountItemIsCredit(row: BillingAccountItemEntry): boolean {
    return (
      row &&
      row.total >= 0 &&
      (_.isEmpty(row.billingAccountChildrenItems) ||
        (row.billingAccountChildrenItems &&
          row.billingAccountChildrenItems.some(b => b.billingAccountItemTypeId == BillingAccountItemTypeEnum.Credit)))
    );
  }

  public downloadNFSe(invoice: InvoiceDto) {
    this.openNfse(invoice);
  }

  private openNfse(invoice) {
    // Open new tab and writes a placeholder text
    const nfseTab = window.open('', '_blank');
    nfseTab.document.write(this.translateService.instant('commomData.loading') + '...');

    this.fiscalDocumentService.getNfse(invoice.id).subscribe(
      res => {
        // Change tab location to PDF file
        nfseTab.location.href = URL.createObjectURL(res);
      },
      error => {
        nfseTab.close();
      });
  }

  public closeModalCancelNFSe() {
    this.invoiceToCancel = null;
  }

  public cancelNFSe(invoice: InvoiceDto) {
    this.invoiceToCancel = invoice;
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_CANCEL_NFSE);
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_STATUS_FISCAL_NOTE);
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_CONFIRM_CANCEL_NFSE);
  }

  public closeModalCancelReason() {
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_CONFIRM_CANCEL_NFSE);
    this.invoiceToCancel = null;
  }

  public confirmCancelationNFSe(nfseToCancel) {
    this.cancelNf(nfseToCancel);
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_CONFIRM_CANCEL_NFSE);
  }

  public getReasonList() {
    this.reasonResource
      .getAllReasonListByPropertyIdAndCategoryId(this.propertyId, ReasonCategoryEnum.InvoiceCancellation)
      .subscribe(response => {
        this.reasonList = response.items;
      });
  }

  public openModalDetails( invoice ) {
    this.invoiceToCancel = invoice;
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_STATUS_FISCAL_NOTE);
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_CANCEL_NFSE);
  }

  public cancelNf( item ) {
    this.modalEmitToggleService.closeAllModals();
    if (item) {
      item.invoice.propertyId = this.propertyId;
      this.billingInvoiceResource.cancelInvoice( item.invoice, item.reasonId ).subscribe(result => {
        this.toasterEmitService.emitChange(SuccessError.success, 'text.nfseIsBeingCanceled');
        item.invoice.billingInvoiceStatusId = NfStatusEnum.canceling;
      });
    }
  }

  // Split functions
  public cancelSplit() {
    this.itemToSplit = null;
    this.accountIdToSplit = null;
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_SPLIT_ENTRY);
  }

  public confirmSplit() {
    this.itemToSplit = null;
    this.accountIdToSplit = null;

    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_SPLIT_ENTRY);
    this.billingAccountNotifierService.requestBillingAccountToUpdate();
  }

  // Undo Split methods
  private undoSplitBillingAccountItem = () => {
    const itemSelected = this.rows.filter(itemEntry => itemEntry.isChecked)[0];

    this.itemsToUndoSplit = this.rows
        .filter(itemEntry => itemEntry.partialBillingAccountItemId == itemSelected.partialBillingAccountItemId);
    this.accountIdToUndoSplit = this.billingAccountWithEntry.billingAccountId;
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_UNDO_SPLIT_ENTRY);
  }

  public cancelUndoSplit() {
    this.itemsToUndoSplit = null;
    this.accountIdToUndoSplit = null;
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_UNDO_SPLIT_ENTRY);
  }

  public confirmUndoSplit() {
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_UNDO_SPLIT_ENTRY);
    const undoSplit = new UndoSplitDto();
    undoSplit.id = this.billingAccountWithEntry.billingAccountId;
    undoSplit.billingAccountItemId = this.itemsToUndoSplit.map( item => item.billingAccountItemId);
    undoSplit.propertyId = this.propertyId;
    this.undoSplitResource.undoSplit(undoSplit).subscribe( result => {
      this.toasterEmitService.emitChange(SuccessError.success, 'label.undoSplitSuccess');
      this.billingAccountNotifierService.requestBillingAccountToUpdate();
    });
  }

  public showPersonHeader() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_PERSON_HEADER);
  }

  public closeModalPersonHeader() {
    this.modalEmitToggleService.emitCloseWithRef(this.MODAL_PERSON_HEADER);
  }

  public openModalCreditNote(invoice: InvoiceDto) {
    this.billingInvoiceIdToIssueCreditNote = invoice.id;
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_EUROPE_FISCAL_DOCUMENT);
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_CREDIT_NOTE);
  }

  public openModalResend(invoice: InvoiceDto) {
    this.bilingItemSelected = invoice;
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_EUROPE_FISCAL_DOCUMENT);
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_DOCUMENT_RESEND);
  }

  public closeModalResend() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_DOCUMENT_RESEND);
  }

  public closeModalCreditNote() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_CREDIT_NOTE);
  }

  public closeModalCreditNotePayment() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_CREDIT_NOTE_PAYMENT_TYPE);
  }

  public setBillingAccountItemCreditNoteList(billingAccountItemCreditAmountList: Array<BillingAccountItemCreditAmountDto>) {
    this.billingAccountItemList = [...billingAccountItemCreditAmountList];
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_CREDIT_NOTE_PAYMENT_TYPE);
  }

  public issueCreditNote(item) {
    if (!item || (item && !item.billingItemId)) {
      this.toasterEmitService.emitChange(SuccessError.error, 'alert.selectPaymentType');
      return;
    }

    if (!item.reasonId) {
      this.toasterEmitService.emitChange(SuccessError.error, 'alert.selectReason');
      return;
    }

    this.billingItemIdToIssueCreditNote = item.billingItemId;

    this.billingAccountItemList.forEach(billingAccountItem => {
      billingAccountItem.billingItemId = this.billingItemIdToIssueCreditNote;
      billingAccountItem.reasonId = item.reasonId;
    });

    this
      .billingInvoiceResource
      .createCreditNote(this.billingAccountItemList)
      .subscribe((billingInvoice: InvoiceDto) => {
        this
          .toasterEmitService
          .emitChange(
            SuccessError.success,
            'alert.creditNoteSaveWithSuccess'
          );
        this.issuedCreditNote.emit(this.billingAccountId);
        this.printInvoiceNote(billingInvoice.id);
    });

  }

  public printInvoiceNote(billingInvoiceId: string) {
    this
      .fiscalDocumentService
      .getNfse(billingInvoiceId)
      .subscribe(
      (byteCodes: any) => {
        if ( byteCodes ) {
          this.pdfPrintService.printPdfFromByteCode(byteCodes);
        }
      }, () => {
        this.showMessagePrintInvoice();
      });
  }

  public showMessagePrintInvoice() {
    this
      .toasterEmitService
      .emitChange(
        SuccessError.error,
        this.translateService.instant('alert.errorPrintInvoice')
      );
  }

  public onResendDocument({id}: InvoiceDto) {
    this.closeModalResend();

    if (this.billingAccountWithEntry) {
      const resentInvoice = this.billingAccountWithEntry.invoices.find((invoice) => invoice.id == id);
      if (resentInvoice) {
        resentInvoice.billingInvoiceStatusId = NfStatusEnum.Issued;
      }
    }

    this.updateBillingAccountStates();
    this.shouldRefresh.emit();
  }
}
