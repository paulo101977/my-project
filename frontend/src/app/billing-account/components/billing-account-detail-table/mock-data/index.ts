import { BillingAccountItemCreditAmountDto } from 'app/shared/models/dto/billing-account-item/billing-account-item-credit-amount-dto';

export class DATA {
  public static readonly billingAccountItemList = <Array<BillingAccountItemCreditAmountDto>>[
    {
      id: '123',
      differenceAmountAndCreditNote: 1,
      differenceAmountAndCreditNoteFormatted: 2,
      creditAmount: 10.1,
      billingItemName: 'Balas Juquinha',
      billingInvoiceId: '234',
      billingAccountItemDate: new Date(1560428697459 ),
      billingItemId: 2,
    },
    {
      id: '523',
      differenceAmountAndCreditNote: 2,
      differenceAmountAndCreditNoteFormatted: 3,
      creditAmount: 15.6,
      billingItemName: 'Cocada Baiana',
      billingInvoiceId: '735',
      billingAccountItemDate: new Date(1760428697459 ),
      billingItemId: 3,
    }
  ];
}
