import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BillingAccountDetailTableComponent } from './billing-account-detail-table.component';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';
import { StoreModule } from '@ngrx/store';
import { billingAccountWithEntryList } from 'app/shared/reducers/billing-account/billing-account.reducer';
import { RouterTestingModule } from '@angular/router/testing';
import { ReasonCategoryEnum } from 'app/shared/models/reason/reason-category-enum';
import { Reason } from 'app/shared/models/dto/reason';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { configureTestSuite } from 'ng-bullet';
import { propertyServiceStub } from 'app/shared/services/property/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import {
  amDateFormatPipeStub,
  currencyFormatPipe,
  imgCdnStub,
} from '../../../../../testing';
import { ThexApiTestingModule } from '@inovacaocmnet/thx-bifrost';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { Data } from 'app/billing-account/mock-data';
import { BillingInvoiceProvider } from 'app/shared/resources/Invoice/testing/billing-invoice.resource.provider';
import { DATA } from 'app/billing-account/components/billing-account-detail-table/mock-data';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { billingAccountServiceStub, fiscalDocumentsServiceStub } from 'app/shared/services/billing-account/testing';
import { billingAccountItemServiceStub } from 'app/shared/services/billing-account-item/testing';
import { currencyServiceStub } from 'app/shared/services/shared/testing/currency-service';
import { reasonResourceStub } from 'app/shared/resources/reason/testing';
import { undoSplitResourceStub } from 'app/billing-account/undo-split/resources/testing';
import { pdfPrintServiceStub } from 'app/shared/services/pdf-print/testing';

describe('BillingAccountDetailTableComponent', () => {
  let component: BillingAccountDetailTableComponent;
  let fixture: ComponentFixture<BillingAccountDetailTableComponent>;


  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule,
        RouterTestingModule,
        FormsModule,
        TranslateTestingModule,
        HttpClientTestingModule,
        ThexApiTestingModule,
        StoreModule.forRoot({billingAccountWithEntryList})],
      declarations: [
        BillingAccountDetailTableComponent,
        amDateFormatPipeStub,
        currencyFormatPipe,
        imgCdnStub,
      ],
      providers: [
        propertyServiceStub,
        BillingInvoiceProvider,
        sharedServiceStub,
        dateServiceStub,
        modalToggleServiceStub,
        toasterEmitServiceStub,
        billingAccountServiceStub,
        billingAccountItemServiceStub,
        currencyServiceStub,
        reasonResourceStub,
        fiscalDocumentsServiceStub,
        undoSplitResourceStub,
        pdfPrintServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(BillingAccountDetailTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });



  it('should request reasonCategory.invoiceCancellation', () => {
    component.propertyId = 1;
    const returnObject = {
      itens: new Array<Reason>()
    };
    spyOn<any>(component['reasonResource'], 'getAllReasonListByPropertyIdAndCategoryId')
      .and
      .returnValue(of(returnObject));

    component.getReasonList();

    expect(component['reasonResource'].getAllReasonListByPropertyIdAndCategoryId)
      .toHaveBeenCalledWith(1, ReasonCategoryEnum.InvoiceCancellation);
  });

  it('should set reasonList', () => {
    component.propertyId = 1;
    const returnObject = <ItemsResponse<Reason>>{
      hasNext: false,
      items: new Array<Reason>()
    };
    spyOn(component['reasonResource'], 'getAllReasonListByPropertyIdAndCategoryId').and.returnValue(of(returnObject));

    component.getReasonList();

    expect(component.reasonList).toEqual(returnObject.items);
  });

  it(' should open MODAL_CONFIRM_CANCEL_NFSE when confirmCancelationNFSe fired', () => {
    spyOn(component['modalEmitToggleService'], 'emitCloseWithRef');

    component.propertyId = 1;
    component.confirmCancelationNFSe({invoice: new InvoiceDto(), reasonId: 1});

    expect(component['modalEmitToggleService'].emitCloseWithRef).toHaveBeenCalledWith(component.MODAL_CONFIRM_CANCEL_NFSE);
  });

  it('should close modal MODAL_CONFIRM_CANCEL_NFSE when closeModalCancelReason fired', () => {
    spyOn(component['modalEmitToggleService'], 'emitCloseWithRef');

    component.closeModalCancelReason();

    expect(component['modalEmitToggleService'].emitCloseWithRef).toHaveBeenCalledWith(component.MODAL_CONFIRM_CANCEL_NFSE);
  });

  it('should set to null invoiceToCancel when closeModalCancelReason fired', () => {
    component.closeModalCancelReason();

    expect(component.invoiceToCancel).toBeNull();
  });

  it(' should set invoice when cancelNFSe fired', () => {
    const invoice = new InvoiceDto();
    invoice.id = '1';
    component.cancelNFSe(invoice);

    expect(component.invoiceToCancel).toEqual(invoice);
  });

  it('should close Modal MODAL_CANCEL_NFSE when cancelNFSe fired', () => {
    spyOn(component['modalEmitToggleService'], 'emitCloseWithRef');

    component.cancelNFSe(new InvoiceDto());

    expect(component['modalEmitToggleService'].emitCloseWithRef).toHaveBeenCalledWith(component.MODAL_CANCEL_NFSE);
  });

  it('should open Modal MODAL_CONFIRM_CANCEL_NFSE when cancelNFSe fired', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');

    component.cancelNFSe(new InvoiceDto());

    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.MODAL_CONFIRM_CANCEL_NFSE);
  });

  it('should set invoiceToCancel to null when closeModalCancelNFSe fired', () => {
    component.closeModalCancelNFSe();

    expect(component.invoiceToCancel).toBeNull();
  });

  it('should onResendDocument', () => {
    spyOn(component, 'closeModalResend');
    spyOn<any>(component, 'updateBillingAccountStates');
    spyOn(component.shouldRefresh, 'emit');

    component.billingAccountWithEntry = Data['BILLING_ACCOUNT_WITH_ENTRY'];
    const invoice = new InvoiceDto();
    invoice.id = '1';
    component.onResendDocument(invoice);

    expect(component.closeModalResend).toHaveBeenCalled();
    expect(component['updateBillingAccountStates']).toHaveBeenCalled();
    expect(component.shouldRefresh.emit).toHaveBeenCalled();
  });

  describe( 'on issueCreditNote', () => {
    beforeEach(() => {
      spyOn(component['toasterEmitService'], 'emitChange');
    });

    it('should test when not billingItemIdToIssueCreditNote', () => {
      component.issueCreditNote(null);

      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.error, 'alert.selectPaymentType'
      );
    });

    it('should call printInvoiceNote', fakeAsync(() => {
      const invoice = new InvoiceDto();
      invoice.id = '30';
      spyOn(component['billingInvoiceResource'], 'createCreditNote').and.returnValue(of( invoice ));
      spyOn(component, 'printInvoiceNote');
      spyOn(component['issuedCreditNote'], 'emit');

      const item = {billingItemId: 25, reasonId: 14};
      component.billingAccountItemList = DATA.billingAccountItemList;

      component.issueCreditNote(item);
      tick();

      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.success, 'alert.creditNoteSaveWithSuccess'
      );
      expect( component['billingInvoiceResource'].createCreditNote )
        .toHaveBeenCalledWith(
          component.billingAccountItemList
        );
      expect( component.printInvoiceNote )
        .toHaveBeenCalledWith(
          invoice.id
        );
      expect(component.issuedCreditNote.emit)
        .toHaveBeenCalledWith(
          component.billingAccountId
        );
    }));
  });
});
