import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCancelReasonComponent } from './modal-cancel-reason.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { imgCdnStub } from '../../../../../testing';
import { configureTestSuite } from 'ng-bullet';

describe('ModalCancelReasonComponent', () => {
  let component: ModalCancelReasonComponent;
  let fixture: ComponentFixture<ModalCancelReasonComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ModalCancelReasonComponent,
        imgCdnStub,
      ],
      imports: [
        RouterTestingModule,
        TranslateTestingModule,
      ],
      providers: [
        commomServiceStub,
        sharedServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ModalCancelReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should prepare modal', () => {
    spyOn<any>(component, 'setButtonConfig');

    component.setVars();

    expect(component['setButtonConfig']).toHaveBeenCalled();
  });

  it('should emit cancelEvent when cancelModal fired', () => {
    spyOn<any>(component['closeModal'], 'emit').and.callFake( f => f );

    component.cancelModal();

    expect(component['closeModal'].emit).toHaveBeenCalled();
  });

  it('should emit confirmNFSeCancelation when confirmCancelationModal fired', () => {
    spyOn(component['confirmNFSeCancelation'], 'emit').and.callFake( f => f );

    component.confirmCancelationModal();

    // todo quando um modelo for definido validar se o objetivo emitido é o esperado.
    expect(component[`confirmNFSeCancelation`].emit).toHaveBeenCalled();
  });
});
