import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';
import { ReasonDto } from 'app/shared/models/dto/checkin/reason-dto';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { CommomService } from 'app/shared/services/shared/commom.service';
@Component({
  selector: 'app-modal-cancel-reason',
  templateUrl: './modal-cancel-reason.component.html',
  styleUrls: ['./modal-cancel-reason.component.css']
})
export class ModalCancelReasonComponent implements OnInit {

  public readonly MODAL_TITLE = 'title.confirmNFSeCancel';

  @Input() modalId: string;
  @Input() invoice: InvoiceDto;
  @Input('reasonList')
  set reasonList(list: Array<ReasonDto>) {
    this.prepareReasonList(list);
  }

  @Output() closeModal = new EventEmitter();
  @Output() confirmNFSeCancelation = new EventEmitter<any>();

  // Buttons
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  public reasonOptionList: Array<SelectObjectOption>;
  public reasonIdSelected: number;


  constructor(
    private sharedService: SharedService,
    private commomService: CommomService,
  ) { }

  ngOnInit() {
    this.setVars();
  }

  selectOption(id) {
    this.buttonConfirmConfig.isDisabled = !id;
    this.reasonIdSelected = id;
  }

  public setVars() {
    this.setButtonConfig();
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancelConfirmationNFSeBtn',
      this.cancelModal,
      'commomData.not',
      ButtonType.Secondary,
    );

    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirmNFSeCancelationBtn',
      this.confirmCancelationModal,
      'commomData.yes',
      ButtonType.Primary,
    );
    this.buttonConfirmConfig.isDisabled = true;
  }

  public prepareReasonList(list) {
    if (list) {
      this.reasonOptionList = this.commomService
        .toOption(list, 'id', 'reasonName');
    }
  }

  public cancelModal = () => {
    this.closeModal.emit();
  }

  public confirmCancelationModal = () => {
    this.confirmNFSeCancelation.emit({invoice: this.invoice, reasonId: this.reasonIdSelected});
  }

}
