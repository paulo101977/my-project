import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FiscalStatusNoteModalComponent } from './fiscal-status-note-modal.component';
import { RouterTestingModule } from '@angular/router/testing';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';
import { configureTestSuite } from 'ng-bullet';
import { imgCdnStub } from '../../../../../testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { fiscalDocumentsServiceStub } from 'app/shared/services/billing-account/testing';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { printHelperServiceStub } from 'app/shared/services/shared/testing/print-helper-service';
import { checkoutResourceStub } from 'app/shared/resources/checkout/testing';


describe('FiscalStatusNoteModalComponent', () => {
  let component: FiscalStatusNoteModalComponent;
  let fixture: ComponentFixture<FiscalStatusNoteModalComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule
      ],
      declarations: [
          FiscalStatusNoteModalComponent,
          imgCdnStub,
      ],
      providers: [
        fiscalDocumentsServiceStub,
        sharedServiceStub,
        modalToggleServiceStub,
        printHelperServiceStub,
        checkoutResourceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(FiscalStatusNoteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit seeDetailsNf when clickSeeDetails fired', () => {
    spyOn(component.seeDetailsNf, 'emit');
    const invoice = new InvoiceDto();

    component.clickSeeDetails(invoice);

    expect(component.seeDetailsNf.emit).toHaveBeenCalledWith(invoice);
  });

  it(' should emit cancelNf when clickCancelNf fired', () => {
    spyOn(component.cancelNf, 'emit');
    const invoice = new InvoiceDto();

    component.clickCancelNf(invoice);

    expect(component.cancelNf.emit).toHaveBeenCalledWith(invoice);
  });

  it(' should emit downloadNf when clickDownloadNf fired', () => {
    spyOn<any>(component, 'openNfse');
    const invoice = new InvoiceDto();

    component.clickDownloadNf(invoice);

    expect(component['openNfse']).toHaveBeenCalledWith(invoice);
  });

});
