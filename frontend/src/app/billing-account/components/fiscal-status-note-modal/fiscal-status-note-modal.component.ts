//  Angular imports
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FiscalDocumentsService } from 'app/shared/services/billing-account/fiscal-documents.service';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';
import { NfStatusEnum } from 'app/shared/models/dto/nf/nf-status-enum';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { PrintHelperService } from 'app/shared/services/shared/print-helper.service';
import { CheckoutResource } from 'app/shared/resources/checkout/checkout.resource';
import * as moment from 'moment';
import { PrintRpsComponent } from 'app/revenue-management/rate-plan/print-rps/print-rps.component';
import { PrintComunicator } from 'app/shared/interfaces/print-comunicator';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'fiscal-status-note-modal',
  templateUrl: './fiscal-status-note-modal.component.html',
  styleUrls: ['./fiscal-status-note-modal.component.css'],
})
export class FiscalStatusNoteModalComponent implements OnInit {
  @Input() modalId: string;
  @Input() propertyId: number;
  @Input() accountOwnerName: string;
  @Input() invoices: Array<InvoiceDto>;

  @Output() clickOnError: EventEmitter<InvoiceDto> = new EventEmitter<InvoiceDto>();
  @Output() clickResend: EventEmitter<Array<InvoiceDto>> = new EventEmitter<Array<InvoiceDto>>();
  @Output() seeDetailsNf: EventEmitter<InvoiceDto> = new EventEmitter<InvoiceDto>();
  @Output() cancelNf: EventEmitter<InvoiceDto> = new EventEmitter<InvoiceDto>();
  @Output() downloadNf: EventEmitter<InvoiceDto> = new EventEmitter<InvoiceDto>();

  public resendButtonConfig: ButtonConfig;

  constructor(
    private fiscalDocumentService: FiscalDocumentsService,
    private sharedService: SharedService,
    private modalToggle: ModalEmitToggleService,
    private printService: PrintHelperService,
    private checkoutResource: CheckoutResource,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {
    this.resendButtonConfig = this.sharedService.getButtonConfig(
      'resend-config',
      this.resendConfig.bind(this),
      'label.resendRps',
      ButtonType.Outline,
    );
    this.resendButtonConfig.extraClass = 'thf-u-color--white';
  }

  // Utils
  public resendConfig() {
    const itensToResend = [];
    for (const invoice of this.invoices) {
      if (invoice.billingInvoiceStatusId == NfStatusEnum.NotSend || invoice.billingInvoiceStatusId == NfStatusEnum.Error) {
        itensToResend.push(invoice);
      }
    }
    this.clickResend.emit(itensToResend);
  }

  public getStatusStringOfNfe(status: number) {
    return this.fiscalDocumentService.getStatusStringOfFiscalDocument(status);
  }

  public getStatusColorOfNfe(status: number) {
    return this.fiscalDocumentService.getStatusColorOfFiscalDocument(status);
  }

  public getIconOfNfe(status: number) {
    return this.fiscalDocumentService.getIconOfFiscalDocument(status);
  }

  public isPending(status: number) {
    return this.fiscalDocumentService.isPending(status);
  }

  public isCursorEnabled(invoice) {
    return invoice.externalNumber &&
      invoice.billingInvoiceStatusId != NfStatusEnum.canceling &&
      invoice.billingInvoiceStatusId != NfStatusEnum.canceled;
  }

  public hasError(status: number) {
    return this.fiscalDocumentService.isError(status);
  }

  public needResend() {
    let errors = 0;
    if (this.invoices) {
      for (const invoice of this.invoices) {
        if (invoice.billingInvoiceStatusId == NfStatusEnum.NotSend || invoice.billingInvoiceStatusId == NfStatusEnum.Error) {
          errors++;
        }
      }
    }
    return errors > 0;
  }

  public openLinkNfse(invoice) {
    const statusId = parseFloat(invoice.billingInvoiceStatusId);
    switch (statusId) {
      case NfStatusEnum.Error:
        this.clickOnError.emit(invoice);
        break;
      case NfStatusEnum.Issued:
        this.openNfse(invoice);
        break;
    }
  }

  private openNfse(invoice) {
    // Open new tab and writes a placeholder text
    const nfseTab = window.open('', '_blank');
    nfseTab.document.write(this.translateService.instant('commomData.loading') + '...');

    this.fiscalDocumentService.getNfse(invoice.id).subscribe(
      res => {
        // Change tab location to PDF file
        nfseTab.location.href = URL.createObjectURL(res);
      },
      error => {
        nfseTab.close();
      });
  }

  public printRps(invoice) {
    this.checkoutResource.getInvoiceInfo(this.propertyId, invoice.id, moment.locale()).subscribe(result => {
      this.printService.printComponent(PrintRpsComponent, <PrintComunicator>{
        populateComponent(component) {
          (<PrintRpsComponent>component.instance).rpsPrint = result;
        },
      });
    });
  }

  public nfEmitted( statusId ) {
    return NfStatusEnum.Issued == statusId;
  }

  public clickSeeDetails(invoice) {
    this.seeDetailsNf.emit(invoice);
  }

  public clickCancelNf(invoice) {
    this.cancelNf.emit(invoice);
  }

  public clickDownloadNf( invoice ) {
    this.openNfse(invoice);
  }
}
