import { Component, Input, OnInit } from '@angular/core';
import { BillingAccountCard } from '../../../shared/models/billing-account/billing-account-card';
import { BillingAccountTypeEnum } from '../../../shared/models/billing-account/billing-account-type-enum';
import { ReservationStatusEnum } from '../../../shared/models/reserves/reservation-status-enum';
import { ReservationService } from '../../../shared/services/reservation/reservation.service';
import { BillingAccountService } from '../../../shared/services/billing-account/billing-account.service';

@Component({
  selector: 'app-card-guest-billing-account',
  templateUrl: './card-guest-billing-account.component.html',
  styleUrls: ['./card-guest-billing-account.component.css'],
})
export class CardGuestBillingAccountComponent implements OnInit {
  @Input() billingAccountCard: BillingAccountCard;
  public referenceToBillingAccountTypeEnum = BillingAccountTypeEnum;

  constructor(private reservationService: ReservationService, private billingAccountService: BillingAccountService) {}

  ngOnInit() {}

  public showStatusLabel() {
    return this.billingAccountCard.billingAccountSidebar.guestReservationItemStatusId == ReservationStatusEnum.Checkout;
  }

  public showIconRpsWarnings() {
    const sideBar = this.billingAccountCard.billingAccountSidebar;
    return this.billingAccountService.showIconRpsWarnings(sideBar);
  }

  public isCloseAndOk() {
    const sideBar = this.billingAccountCard.billingAccountSidebar;
    return this.billingAccountService.isCloseAndOk(sideBar);
  }

  public getStatusColor(statusId): any {
    return this.reservationService.getStatusColorByReservationStatus(statusId);
  }
}
