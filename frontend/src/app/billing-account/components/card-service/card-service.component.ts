import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';

@Component({
  selector: 'card-service',
  templateUrl: './card-service.component.html',
  styleUrls: ['./card-service.component.scss']
})
export class CardServiceComponent {

  @Input() isSelected = false;
  @Input() item: any;
  @Output() selectionChange = new EventEmitter();

  public changeSelected() {
    this.isSelected = true;
    this.selectionChange.emit(this.item);
  }
}
