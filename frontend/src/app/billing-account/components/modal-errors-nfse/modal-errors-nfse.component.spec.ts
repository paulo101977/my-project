import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalErrorsNfseComponent } from './modal-errors-nfse.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { imgCdnStub } from '../../../../../testing';

describe('ModalErrorsNfseComponent', () => {
  let component: ModalErrorsNfseComponent;
  let fixture: ComponentFixture<ModalErrorsNfseComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateTestingModule ],
      declarations: [
          ModalErrorsNfseComponent,
          imgCdnStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ModalErrorsNfseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
