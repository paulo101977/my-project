import { Component, Input } from '@angular/core';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';

@Component({
  selector: 'modal-errors-nfse',
  templateUrl: './modal-errors-nfse.component.html',
})
export class ModalErrorsNfseComponent {
  @Input() modalId: string;
  @Input() invoice: InvoiceDto;
  @Input() closeAction: Function;
}
