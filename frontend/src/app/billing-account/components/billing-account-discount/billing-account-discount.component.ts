import { BillingAccountItemService } from './../../../shared/services/billing-account-item/billing-account-item.service';
import { Component, DoCheck, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ReasonResource } from '../../../shared/resources/reason/reason.resource';
import { BillingItemDiscountResource } from '../../../shared/resources/billing-item-discount/billing-item-discount.resource';
import { BillingAccountWithEntry } from '../../../shared/models/billing-account/billing-account-with-entry';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { BillingAccountWithAccount } from '../../../shared/models/billing-account/billing-account-with-account';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { BillingItemDiscountDto } from '../../../shared/models/dto/billing-item-discount-dto';
import { BillingAccountItemTypeEnum } from '../../../shared/models/billing-account/billing-account-item-type-enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';

@Component({
  selector: 'app-billing-account-discount',
  templateUrl: './billing-account-discount.component.html',
  styleUrls: ['./billing-account-discount.component.css'],
})
export class BillingAccountDiscountComponent implements OnInit, OnChanges, DoCheck {
  private CATEGORY_DESCOUNT_ID = 6;

  public modalId = 'discount-modal';
  public discountForm: FormGroup;
  public propertyId: number;

  // Buttons dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;

  public reasonList: Array<any>;

  public totalItems: number;
  public totalWithDiscount: number;
  public qtdItems: number;
  public valueTypeEnabled = false;

  public optionsCurrencyMask: any;
  public currencySymbol: any;

  @Input() billingAccount: BillingAccountWithAccount;
  @Input() itemsSelected: Array<BillingAccountWithEntry>;

  @Output() modalClose: EventEmitter<any> = new EventEmitter<any>();
  @Output() update: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private reasonResource: ReasonResource,
    private commomService: CommomService,
    private activateRoute: ActivatedRoute,
    private toasterEmitService: ToasterEmitService,
    private billinItenDiscountResource: BillingItemDiscountResource,
    private billingAccountItemService: BillingAccountItemService,
    private sessionParameterService: SessionParameterService
  ) {}

  ngOnInit() {
    this.propertyId = this.activateRoute.snapshot.params.property;

    const currency = this.sessionParameterService
      .extractSelectedOptionOfPossibleValues( this.sessionParameterService
        .getParameter(this.propertyId, ParameterTypeEnum.DefaultCurrency));
    if (currency) {
      this.currencySymbol = currency.title; // todo change to currencySymbol after igor change in backend
    }
    this.getReasonList();
    this.setForm();
    this.setButtons();
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
  }

  ngOnChanges() {
    this.cleanForm();
    this.calculateTotal();
    this.calculateNewValue();
    if (this.qtdItems > 1) {
      this.valueTypeEnabled = false;
    } else {
      this.valueTypeEnabled = true;
    }
  }

  ngDoCheck(): void {
    this.calculateNewValue();
    if (isNaN(this.discountForm.get('discountValue').value)) {
      this.discountForm.get('discountValue').setValue(0);
    }
  }

  private cleanForm() {
    if (this.discountForm) {
      this.discountForm.reset({
        discountType: 'percent',
        discountValue: 0,
        reason: '',
      });
    }
  }
  private setForm(): void {
    this.discountForm = this.formBuilder.group({
      discountType: [{ value: 'percent', disabled: !this.valueTypeEnabled }, [Validators.required]],
      discountValue: [null, [Validators.required]],
      reason: [null, [Validators.required]],
    });
  }

  private getReasonList() {
    this.reasonResource.getAllReasonListByPropertyIdAndCategoryId(this.propertyId, this.CATEGORY_DESCOUNT_ID).subscribe(result => {
      this.reasonList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(result, 'id', 'reasonName');
    });
  }

  private setButtons() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-discount',
      this.closeModal.bind(this),
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'save-discount',
      this.saveDiscount.bind(this),
      'action.confirm',
      ButtonType.Primary,
    );
  }

  public calculateTotal() {
    this.qtdItems = 0;
    this.totalItems = 0;
    if (this.itemsSelected && this.itemsSelected.length > 0 && this.itemsSelected[0].billingAccountItems) {
      this.itemsSelected[0].billingAccountItems.forEach(item => {
        if (item.isChecked) {
          this.totalItems += item.totalWithoutRate;
          this.qtdItems++;
        }
      });
    }
  }
  public calculateNewValue() {
    if (this.discountForm) {
      this.totalWithDiscount = this.totalItems;
      const valueOfDiscount = this.discountForm.get('discountValue').value ? this.discountForm.get('discountValue').value : 0;
      let invalid = false;
      if (this.discountForm.get('discountType').value == 'percent') {
        if (valueOfDiscount > 0) {
          this.totalWithDiscount = this.totalItems - (this.totalItems * valueOfDiscount) / 100;
          invalid = valueOfDiscount > 100;
        }
      } else {
        if (this.totalItems < 0) {
          this.totalWithDiscount = this.totalItems + parseFloat(valueOfDiscount);
          invalid = parseFloat(valueOfDiscount) > (-this.totalItems);
        } else {
          this.totalWithDiscount = this.totalItems - parseFloat(valueOfDiscount);
          invalid = parseFloat(valueOfDiscount) > this.totalItems;
        }
      }
      this.buttonSaveConfig.isDisabled = this.discountForm.invalid || invalid;
    }
  }

  private closeModal() {
    this.modalClose.emit();
  }

  private saveDiscount() {
    const discountValue = this.discountForm.get('discountValue').value;
    let canSave = false;

    if (this.discountForm.get('discountType').value == 'percent') {
      if (discountValue < 0 || discountValue > 100) {
        this.toasterEmitService.emitChange(SuccessError.error, 'alert.discountPercentageRange');
      } else {
        canSave = true;
      }
    } else {
      if (discountValue > Math.abs(this.totalItems)) {
        this.toasterEmitService.emitChange(SuccessError.error, 'alert.discountToHigh');
      } else {
        canSave = true;
      }
    }

    if (canSave) {
      const listToSave = this.prepareToSave();
      this.billinItenDiscountResource.applyDiscountTo(listToSave).subscribe(
        result => {
          this.update.emit();
          this.modalClose.emit();
        },
        error => {
          console.error(error);
          // mostrar erro
        },
      );
    }
  }

  prepareToSave() {
    const arrayToSave = [];
    if (this.itemsSelected && this.itemsSelected.length > 0 && this.itemsSelected[0].billingAccountItems) {
      this.itemsSelected[0].billingAccountItems.forEach(item => {
        if (item.isChecked) {
          const discountToSaveAux = new BillingItemDiscountDto();
          discountToSaveAux.billingAccountId = this.itemsSelected[0].billingAccountId;
          if (this.discountForm.get('discountType').value == 'percent') {
            discountToSaveAux.isPercentual = true;
            discountToSaveAux.percentual = this.discountForm.get('discountValue').value;
            discountToSaveAux.amountDiscount = Math.abs(item.total);
          } else {
            discountToSaveAux.isPercentual = false;
            discountToSaveAux.amountDiscount = this.discountForm.get('discountValue').value;
            discountToSaveAux.percentual = 0;
          }
          discountToSaveAux.isCredit = item.billingAccountItemTypeId == BillingAccountItemTypeEnum.Credit ? true : false;
          discountToSaveAux.propertyId = this.propertyId;
          discountToSaveAux.billingAccountItemId = item.billingAccountItemId;
          discountToSaveAux.billingItemId = item.billingItemId;
          discountToSaveAux.reasonId = this.discountForm.get('reason').value;
          arrayToSave.push(discountToSaveAux);
        }
      });
    }
    return arrayToSave;
  }

  public removeNegativeSymbol(value: number): number {
    return Math.abs(value);
  }

  public discountTypeIsValue(): boolean {
    return this.billingAccountItemService.discountTypeIsValue(this.discountForm);
  }

  public discountTypeIsPercent(): boolean {
    return this.billingAccountItemService.discountTypeIsPercent(this.discountForm);
  }
}
