import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BillingAccountDiscountComponent } from './billing-account-discount.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { currencyPipeStub } from 'app/shared/mock-data/currency-data';
import { ThexApiTestingModule } from '@bifrost/thex-api/testing/thex-api.testing.module';
import { ThexApiService } from '@bifrost/thex-api/thex-api.service';
import { ThexApiTestingService } from '@bifrost/thex-api/testing/thex-api.testing.service';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { reasonResourceStub } from 'app/shared/resources/reason/testing';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteStub } from '../../../../../testing';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { billingItemDiscountResourceStug } from 'app/shared/resources/billing-item-discount/testing';
import { billingAccountItemServiceStub } from 'app/shared/services/billing-account-item/testing';
import { sessionParameterServiceStub } from 'app/shared/services/parameter/testing';

describe('BillingAccountDiscountComponent', () => {
  let component: BillingAccountDiscountComponent;
  let fixture: ComponentFixture<BillingAccountDiscountComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        TranslateTestingModule,
        ThexApiTestingModule,
        RouterTestingModule,
      ],
      declarations: [
          BillingAccountDiscountComponent,
          currencyPipeStub,
      ],
      providers: [
        {
          provide: ThexApiService,
          useClass: ThexApiTestingService,
        },
        {
          provide: ActivatedRoute,
          useClass: ActivatedRouteStub,
        },
        commomServiceStub,
        reasonResourceStub,
        toasterEmitServiceStub,
        billingItemDiscountResourceStug,
        billingAccountItemServiceStub,
        sessionParameterServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(BillingAccountDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
