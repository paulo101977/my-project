import {
  Component,
  Input,
  EventEmitter,
  Output
} from '@angular/core';
import { ProductsChart } from 'app/billing-account/models/products-billing-item';

@Component({
  selector: 'launcher-shopping-chart',
  templateUrl: './launcher-shopping-chart.component.html',
  styleUrls: ['./launcher-shopping-chart.component.scss']
})
export class LauncherShoppingChartComponent {

  @Input() shoppingChartList: Array<ProductsChart>;
  @Input() currencySymbol: string;
  @Output() removeItemEvent = new EventEmitter();
  @Output() valueChangeEvent = new EventEmitter();


  public valueChange(item) {
    this.valueChangeEvent.emit(item);
  }

  public removeItem(item) {
    this.removeItemEvent.emit(item);
  }

}
