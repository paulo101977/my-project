import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LauncherShoppingChartComponent } from './launcher-shopping-chart.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';


describe('LauncherShoppingChartComponent', () => {
  let component: LauncherShoppingChartComponent;
  let fixture: ComponentFixture<LauncherShoppingChartComponent>;
  // let toTest;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
          LauncherShoppingChartComponent
    ],
      imports: [
        TranslateTestingModule,
    ],
    schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(LauncherShoppingChartComponent);
    component = fixture.componentInstance;


    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
