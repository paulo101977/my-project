import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { BillingAccountSidebar } from '../../../shared/models/billing-account/billing-account-sidebar';
import { BillingAccountCard } from '../../../shared/models/billing-account/billing-account-card';
import { BillingAccountNotifierService } from '../../services/billing-account-notifier.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sidebar-guest-billing-account',
  templateUrl: './sidebar-guest-billing-account.component.html',
  styleUrls: ['./sidebar-guest-billing-account.component.css'],
})
export class SidebarGuestBillingAccountComponent implements OnInit, OnChanges, OnDestroy {
  @Input() billingAccountCardList: Array<BillingAccountCard>;

  @Output() clickBillingAccount = new EventEmitter<string>();

  private currentCard: BillingAccountCard;
  private subscription: Subscription;

  constructor(private billingAccountNotifierService: BillingAccountNotifierService ) {}

  ngOnInit() {
    this.subscription = this.billingAccountNotifierService
      .billingAccountChangeListener$.subscribe( item => {
        if (this.currentCard) {
          this.chooseItem(this.currentCard);
        }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('billingAccountCardList')) {
      const cardList = changes.billingAccountCardList.currentValue;
      if (cardList) {
        this.currentCard = cardList.filter( item => item.isActive )[0];
      }
    }
  }

  public chooseItem(billingAccountCard: BillingAccountCard): void {
    this.clickBillingAccount.emit(billingAccountCard.billingAccountSidebar.billingAccountId);
    this.resetActiveList();
    billingAccountCard.isActive = true;
    this.currentCard = billingAccountCard;
  }

  public resetActiveList() {
    this.billingAccountCardList.forEach(card => (card.isActive = false));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
