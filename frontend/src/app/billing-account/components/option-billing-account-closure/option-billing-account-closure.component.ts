import { Component, OnInit, Output, EventEmitter, Input, OnChanges, DoCheck } from '@angular/core';
import { ButtonConfig } from './../../../shared/models/button-config';
import { ButtonType } from './../../../shared/models/button-config';
import { SharedService } from './../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from './../../../shared/services/shared/modal-emit-toggle.service';
import { BillingAccountWithEntry } from '../../../shared/models/billing-account/billing-account-with-entry';
import { GetAllBillingAccountForClosure } from '../../../shared/models/billing-account/get-all-billing-account-for-closure';
import { hasOwnProperty } from 'tslint/lib/utils';
import { BillingAccountService } from '../../../shared/services/billing-account/billing-account.service';

@Component({
  selector: 'option-billing-account-closure',
  templateUrl: './option-billing-account-closure.component.html',
})
export class OptionBillingAccountClosureComponent implements OnInit, OnChanges, DoCheck {
  @Input() billingAccountToClose: BillingAccountWithEntry;
  @Input() billingAccoutForClousureList: GetAllBillingAccountForClosure;
  @Output() openModalBillingAccountClosurePerson = new EventEmitter();
  @Output() openModalMultipleBillingAccountClosurePerson = new EventEmitter();
  @Output() openModalPayment = new EventEmitter();
  @Input() isIndividualStatement: boolean;

  // Button dependencies
  public buttonFinishConfig: ButtonConfig;
  public buttonConsolidateAndPay: ButtonConfig;

  constructor(
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private billingAccountService: BillingAccountService,
  ) {}

  ngOnInit() {
    this.setButtonsConfig();
  }

  ngOnChanges() {
    this.setButtonsConfig();
  }

  ngDoCheck() {
    this.setButtonsConfig();
  }

  private setButtonsConfig(): void {
    if (this.isIndividualStatement) {
      this.buttonFinishConfig = this.sharedService.getButtonConfig(
        'button-close-billing-account-closure',
        this.openModalPaymentCallback,
        'action.finish',
        ButtonType.Secondary,
      );
      this.buttonConsolidateAndPay = this.sharedService.getButtonConfig(
        'button-consolidade-and-pay',
        this.openModalBillingAccountClosurePersonCallback,
        'action.consolidateAndPay',
        ButtonType.Secondary,
      );
    } else {
      this.buttonFinishConfig = this.sharedService.getButtonConfig(
        'button-close-billing-account-closure',
        this.openModalPaymentCallback,
        'text.individual',
        ButtonType.Secondary,
      );
      this.buttonConsolidateAndPay = this.sharedService.getButtonConfig(
        'button-multiple-consolidade-and-pay',
        this.openModalMultipleBillingAccountClosurePersonCallback,
        'text.consolidatedM',
        ButtonType.Secondary,
      );
    }
    this.buttonConsolidateAndPay.isDisabled = !this.billingAccountService.hasAccountsToClose(this.billingAccoutForClousureList);
  }

  private openModalBillingAccountClosurePersonCallback = () => {
    this.openModalBillingAccountClosurePerson.emit();
  }

  private openModalMultipleBillingAccountClosurePersonCallback = () => {
    this.openModalMultipleBillingAccountClosurePerson.emit();
  }

  private openModalPaymentCallback = () => {
    this.openModalPayment.emit();
  }
}
