import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalCreditNoteComponent } from './modal-credit-note.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ButtonConfig, ButtonSize, ButtonType, Type} from 'app/shared/models/button-config';
import {SharedService} from 'app/shared/services/shared/shared.service';
import { amDateFormatPipeStub, createServiceStub } from '../../../../../testing';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {SuccessError} from 'app/shared/models/success-error-enum';
import {BillingAccountItemResource} from 'app/shared/resources/billing-account-item/billing-account-item.resource';
import {Observable, of} from 'rxjs';
import { configureTestSuite } from 'ng-bullet';

describe('ModalCreditNoteComponent', () => {
  let component: ModalCreditNoteComponent;
  let fixture: ComponentFixture<ModalCreditNoteComponent>;

  const sharedServiceStub = createServiceStub(SharedService, {
    getButtonConfig(
      id: string,
      callback: Function,
      textButton: string,
      buttonType: ButtonType,
      buttonSize?: ButtonSize,
      isDisabled?: boolean,
      type?: Type): ButtonConfig { return new ButtonConfig();
    },
    getHeaders(): any { return ''; }
  });

  const toasterEmitServiceStub = createServiceStub(ToasterEmitService, {
    emitChange(isSuccess: SuccessError, message: string) {}
  });

  const billingAccountItemResourceStub = createServiceStub(BillingAccountItemResource, {
    getAllBillingAccountItemCreditAmountByInvoiceId(_billingInvoiceId: string):
        Observable<any> { return of({items: []}); }
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ModalCreditNoteComponent,
        amDateFormatPipeStub,
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        sharedServiceStub,
        toasterEmitServiceStub,
        billingAccountItemResourceStub
      ]
    });

    fixture = TestBed.createComponent(ModalCreditNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
