import { SharedService } from 'app/shared/services/shared/shared.service';
import { ButtonConfig, ButtonType, ButtonSize } from 'app/shared/models/button-config';
import { BillingAccountItemResource } from 'app/shared/resources/billing-account-item/billing-account-item.resource';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { BillingAccountItemCreditAmountDto } from 'app/shared/models/dto/billing-account-item/billing-account-item-credit-amount-dto';
import { ToasterEmitService } from '@app/shared/services/shared/toaster-emit.service';
import { SuccessError } from '@app/shared/models/success-error-enum';

@Component({
  selector: 'modal-credit-note',
  templateUrl: './modal-credit-note.component.html',
  styleUrls: ['./modal-credit-note.component.css']
})
export class ModalCreditNoteComponent implements OnInit {
  @ViewChild('rowCheckbox') rowCheckbox: TemplateRef<any>;
  @ViewChild('dateCell') dateCellTemplate: TemplateRef<any>;
  @ViewChild('amountCell') amountCellTemplate: TemplateRef<any>;
  @ViewChild('amountModifiedCell') amountModifiedCellTemplate: TemplateRef<any>;

  @Input() modalId: string;

  private _billingInvoiceId: string;
  @Input()
  set billingInvoiceId(id: string) {
    this._billingInvoiceId = id;
    this.billingAccountItemList = [];
    this.creditNoteAmountList = [];
    if (this._billingInvoiceId) {
      this.billingAccountItemResource.getAllBillingAccountItemCreditAmountByInvoiceId(this._billingInvoiceId).subscribe(response => {
        this.billingAccountItemList = response;
        this.billingAccountItemSelectedList = [...this.billingAccountItemList];
        if (this.billingAccountItemList && this.billingAccountItemList.length > 0) {
          this.buttonConfirmConfig.isDisabled = false;
        }
      });
    }
  }
  get billingInvoiceId(): string {
    return this._billingInvoiceId;
  }

  @Output() createdCreditNote = new EventEmitter<Array<BillingAccountItemCreditAmountDto>>();
  @Output() closeModal = new EventEmitter();

  // Component
  public billingAccountItemList: Array<BillingAccountItemCreditAmountDto>;
  public billingAccountItemSelectedList: Array<BillingAccountItemCreditAmountDto>;
  public optionsCurrencyMask: any;
  public creditNoteAmountList: Array<number>;

  // Table
  public columns: Array<any>;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  constructor(
    private billingAccountItemResource: BillingAccountItemResource,
    private sharedService: SharedService,
    private toasterEmitService: ToasterEmitService
  ) { }

  ngOnInit() {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
    this.setTableColumns();
    this.setButtonConfig();
  }

  private setTableColumns() {
    this.columns = [
      {name: 'label.date', cellTemplate: this.dateCellTemplate},
      {name: 'label.description', prop: 'billingItemName'},
      {name: 'label.value', cellTemplate: this.amountCellTemplate},
      {name: 'label.creditValue', cellTemplate: this.amountModifiedCellTemplate}
    ];
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'person-header-cancel',
      this.clickCloseModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'issue-credit-note',
      this.createCreditNote,
      'action.confirm',
      ButtonType.Primary,
      ButtonSize.Normal,
      true
    );
  }

  private clickCloseModal = () => {
    this.resetBillingAccountItemSelectedList();
    this.closeModal.emit();
  }

  public createCreditNote = () => {
    if (!this.billingAccountItemSelectedList || (this.billingAccountItemSelectedList && this.billingAccountItemSelectedList.length == 0)) {
      this.toasterEmitService.emitChange(SuccessError.error, 'alert.selectABillingAccountItem');
      return;
    }

    this.billingAccountItemSelectedList.forEach((billingAccountItem: BillingAccountItemCreditAmountDto) => {
      billingAccountItem.creditAmount = billingAccountItem.differenceAmountAndCreditNote;
    });

    this.createdCreditNote.emit(this.billingAccountItemSelectedList);
    this.closeModal.emit();

    this.resetBillingAccountItemSelectedList();
  }

  private resetBillingAccountItemSelectedList() {
    this.billingAccountItemSelectedList = [...this.billingAccountItemList];
  }

}
