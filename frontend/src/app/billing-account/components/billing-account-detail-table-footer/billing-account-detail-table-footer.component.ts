import {Component, Input, OnInit} from '@angular/core';
import {CurrencyService} from 'app/shared/services/shared/currency-service.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'billing-account-detail-table-footer',
  templateUrl: './billing-account-detail-table-footer.component.html',
})
export class BillingAccountDetailTableFooterComponent implements OnInit {
  @Input() total: number;
  @Input() accountType: string;
  public currencySymbol: string;
  private propertyId: number;

  constructor(
    private currencyService: CurrencyService,
    public _route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.propertyId = this._route.snapshot.params.property;
    this.currencySymbol =  this.currencyService.getDefaultCurrencySymbol(this.propertyId);
  }
}
