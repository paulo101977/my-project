import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'billing-item-empty-cart',
  templateUrl: './billing-item-empty-cart.component.html',
  styleUrls: ['./billing-item-empty-cart.component.scss']
})
export class BillingItemEmptyCartComponent {}
