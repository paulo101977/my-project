import {
  Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation
} from '@angular/core';

import { ProductsResource } from 'app/shared/resources/products/products.resource';
import { LauncherMainEnum } from 'app/billing-account/models/launcher-main.enum';
import { CardsService } from 'app/billing-account/services/cards.service';


@Component({
  selector: 'launcher-carousel',
  templateUrl: './launcher-carousel.component.html',
  styleUrls: ['./launcher-carousel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LauncherCarouselComponent implements OnInit {
  public symbol: string;

  private readonly DEFAULT_CONFIG = {
    autoWidth: true,
    items: '6',
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 3
      },
      576: {
        items: 4
      },
      768: {
        items: 6
      },
      1140: {
        items: 6
      }
    },
    nav: true
  };

  @Input() data = [];
  @Input() isSelector = true;
  @Input() carouselConfig: any;
  @Output() itemClicked = new EventEmitter();

  constructor(
    private productResource: ProductsResource,
    private cardsService: CardsService,
  ) { }

  public getImgUrl(type: LauncherMainEnum) {
    return this.cardsService.getMainScreenImgUrl(+type);
  }

  onItemClicked(item) {
    this.itemClicked.emit(item);
  }

  private configCarousel() {
    this.carouselConfig = this.carouselConfig || this.DEFAULT_CONFIG;
  }

  ngOnInit() {
    this.configCarousel();
  }

}
