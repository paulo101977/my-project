import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';
import { BillingInvoiceResource } from 'app/shared/resources/Invoice/billing-invoice.resource';
import { ActivatedRoute } from '@angular/router';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { FiscalDocumentsService } from 'app/shared/services/billing-account/fiscal-documents.service';
import { PdfPrintService } from 'app/shared/services/pdf-print/pdf-print.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-document-modal-confirm',
  templateUrl: './document-modal-confirm.component.html'
})
export class DocumentModalConfirmComponent implements OnInit {

  public buttonCancelModalConfig: ButtonConfig;
  public buttonConfirmModalConfig: ButtonConfig;
  public propertyId: number;

  @Output() resentDocument = new EventEmitter<InvoiceDto>();
  @Output() cancelDocumentResend = new EventEmitter<InvoiceDto>();
  @Input() invoice: InvoiceDto;

  constructor(
    private route: ActivatedRoute,
    private modalEmitToggleService: ModalEmitToggleService,
    private sharedService: SharedService,
    private billingInvoiceResource: BillingInvoiceResource,
    private fiscalDocumentService: FiscalDocumentsService,
    private pdfPrintService: PdfPrintService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setButtonConfig();
  }

  public cancelDocument() {
    this.cancelDocumentResend.emit();
  }

  private setButtonConfig() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'document-cancel-modal',
      () => this.cancelDocument(),
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmModalConfig = this.sharedService.getButtonConfig(
      'document-confirm-modal',
      () => this.resend(),
      'action.confirm',
      ButtonType.Primary,
      null,
      false,
    );
  }

  private resend() {
    if (this.invoice) {
      this
        .billingInvoiceResource
        .resendInvoice(this.propertyId, this.invoice)
        .subscribe(() => {
          this.resentDocument.emit(this.invoice);
          this.printInvoiceNote(this.invoice.id);
        }, () => {
          this.cancelDocument();
        });
    }
  }

  public printInvoiceNote(billingInvoiceId: string) {
    this
      .fiscalDocumentService
      .getNfse(billingInvoiceId)
      .subscribe(
        (byteCodes: any) => {
          if (byteCodes) {
            this.pdfPrintService.printPdfFromByteCode(byteCodes);
          }
        }, () => {
          this.showMessagePrintInvoice();
        });
  }

  public showMessagePrintInvoice() {
    this
      .toasterEmitService
      .emitChange(
        SuccessError.error,
        this.translateService.instant('alert.errorPrintInvoice')
      );
  }
}
