import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { DocumentModalConfirmComponent } from './document-modal-confirm.component';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BillingInvoiceProvider } from 'app/shared/resources/Invoice/testing/billing-invoice.resource.provider';
import { ImgCdnPipeStub } from '@inovacao-cmnet/thx-ui';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ActivatedRouteStub, locationStub } from '../../../../../testing';
import { ActivatedRoute } from '@angular/router';
import { Data, INVOICE_LIST } from 'app/billing-account/mock-data';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { of, throwError } from 'rxjs';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { billingInvoiceResourceStub } from 'app/billing-account/components-container/billing-invoice-nfce-download/testing';
import { fiscalDocumentsServiceStub } from 'app/shared/services/billing-account/testing';
import { pdfPrintServiceStub } from 'app/shared/services/pdf-print/testing';

describe('DocumentModalConfirmComponent', () => {
  let component: DocumentModalConfirmComponent;
  let fixture: ComponentFixture<DocumentModalConfirmComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        DocumentModalConfirmComponent,
        ImgCdnPipeStub
      ],
      imports: [
        TranslateTestingModule,
      ],
      providers: [
        BillingInvoiceProvider,
        modalToggleServiceStub,
        toasterEmitServiceStub,
        locationStub,
        sharedServiceStub,
        billingInvoiceResourceStub,
        fiscalDocumentsServiceStub,
        pdfPrintServiceStub,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(DocumentModalConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should resend', fakeAsync(() => {
    component.invoice = Data[INVOICE_LIST];
    spyOn(component['billingInvoiceResource'], 'resendInvoice').and.returnValue( of(null));
    spyOn(component.resentDocument, 'emit');
    spyOn(component, 'printInvoiceNote');

    component['resend']();

    tick();

    expect(component.resentDocument.emit).toHaveBeenCalled();
    expect(component.printInvoiceNote).toHaveBeenCalledWith(component.invoice.id);
  }));

  it('should resend - error', fakeAsync(() => {
    component.invoice = Data[INVOICE_LIST];
    spyOn(component['billingInvoiceResource'], 'resendInvoice').and.returnValue(throwError({}));
    spyOn(component, 'cancelDocument');

    component['resend']();

    tick();

    expect(component.cancelDocument).toHaveBeenCalled();
  }));
});
