import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { BillingAccountItemEntry } from '../../../shared/models/billing-account/billing-account-item-entry';
import { ButtonConfig } from '../../../shared/models/button-config';
import { ButtonType } from '../../../shared/models/button-config';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ReasonCategoryEnum } from '../../../shared/models/reason/reason-category-enum';
import { BillingAccountWithEntry } from '../../../shared/models/billing-account/billing-account-with-entry';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { BillingAccountItemMapper } from '../../../shared/mappers/billing-account-item-mapper';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { BillingAccountItemService } from './../../../shared/services/billing-account-item/billing-account-item.service';

// Resouces
import { ReasonResource } from '../../../shared/resources/reason/reason.resource';
import { BillingAccountWithAccount } from '../../../shared/models/billing-account/billing-account-with-account';
import { BillingAccountItemResource } from '../../../shared/resources/billing-account-item/billing-account-item.resource';

@Component({
  selector: 'billing-account-reverse',
  templateUrl: './billing-account-reverse.component.html',
})
export class BillingAccountReverseComponent implements OnInit {
  @Input() billingAccount: BillingAccountWithEntry;

  @Output() updateBillingAccountAndHeaderAndCardList = new EventEmitter<string>();
  @Output() updateActionButtonsToEntries = new EventEmitter<string>();

  public reasonOptionList: Array<SelectObjectOption>;
  public reverseFormGroup: FormGroup;

  private propertyId: number;

  // Modal dependecies
  public modalId: string;

  // Buttons dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonReversePostConfig: ButtonConfig;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private reasonResource: ReasonResource,
    private commomService: CommomService,
    private billingAccountItemResource: BillingAccountItemResource,
    private billingAccountItemMapper: BillingAccountItemMapper,
    private toasterEmitService: ToasterEmitService,
    private billingAccountItemService: BillingAccountItemService,
  ) {}

  ngOnInit() {
    this.setVars();
    this.getReasonOptionList();
  }

  setVars() {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.modalId = 'reverse-post';
      this.reasonOptionList = new Array<SelectObjectOption>();
      this.setForm();
      this.setButtonConfig();
      this.setListeners();
    });
  }

  private setButtonConfig(): void {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-reverse',
      this.closeModal,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonReversePostConfig = this.sharedService.getButtonConfig(
      'reverse-post',
      this.reversePost,
      'action.confirm',
      ButtonType.Primary,
      null,
      true,
    );
  }

  private closeModal = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.modalId);
  }

  private reversePost = () => {
    const billingAccountItemReverseList = this.billingAccountItemMapper.toBillintAccountItemReverse(
      this.reverseFormGroup,
      this.propertyId,
      this.billingAccount.billingAccountId,
      this.billingAccountItemService.billingAccountWithEntryList,
    );

    this.billingAccountItemResource.reverseBillingAccountItem(billingAccountItemReverseList).subscribe(response => {
      this.toasterEmitService.emitChange(SuccessError.success, 'billingAccountModule.commomData.billingAccountItemReversalMessage');
      this.closeModal();
      this.onUpdateBillingAccountAndHeaderAndCardList(this.billingAccount.billingAccountId);
      this.updateActionButtonsToEntries.emit();
    });
  }

  private setForm(): void {
    this.reverseFormGroup = this.formBuilder.group({
      reason: this.formBuilder.group({
        reasonId: [null, [Validators.required]],
        observation: [null, [Validators.maxLength(50)]],
      }),
    });
  }

  private getReasonOptionList() {
    this.reasonResource.getAllReasonListByPropertyIdAndCategoryId(this.propertyId, ReasonCategoryEnum.Reversal).subscribe(response => {
      this.reasonOptionList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(response, 'id', 'reasonName');
    });
  }

  private setListeners(): void {
    this.reverseFormGroup.valueChanges.subscribe(value => {
      this.buttonReversePostConfig = this.sharedService.getButtonConfig(
        'reverse-post',
        this.reversePost,
        'action.confirm',
        ButtonType.Primary,
        null,
        this.reverseFormGroup.invalid,
      );
    });
  }

  public onUpdateBillingAccountAndHeaderAndCardList(billingAccountId: string): void {
    this.updateBillingAccountAndHeaderAndCardList.emit(billingAccountId);
  }
}
