import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { BillingItemResource } from '../../../shared/resources/billingItem/billing-item.resource';

@Component({
  selector: 'billing-account-card',
  templateUrl: './billing-account-card.component.html',
  styleUrls: ['./billing-account-card.component.css'],
})
export class BillingAccountCardComponent implements OnInit, OnChanges {
  @Input() propertyId: number;
  @Input() formModal: FormGroup;
  @Input() paymentTypeId: number;

  public plasticBrandPropertyList: Array<SelectObjectOption>;
  public optionsCurrencyMask: any;

  constructor(private billingItemResource: BillingItemResource, private commomService: CommomService) {}

  ngOnInit() {
    this.setVars();
  }

  ngOnChanges() {
    this.getPlasticBrandPropertyList();
  }

  private setVars(): void {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
  }

  private getPlasticBrandPropertyList(): void {
    this.plasticBrandPropertyList = new Array<SelectObjectOption>();
    this.billingItemResource.getAllBillingItemPlasticBrandCompanyClientListByPaymentTypeId(this.paymentTypeId).subscribe(response => {
      this.plasticBrandPropertyList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        response,
        'id',
        'plasticBrandFormatted',
      );
    });
  }
}
