import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModalResendNfseComponent } from './modal-resend-nfse.component';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { imgCdnStub } from '../../../../../testing';

describe('ModalResendNfseComponent', () => {
  let component: ModalResendNfseComponent;
  let fixture: ComponentFixture<ModalResendNfseComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
          TranslateTestingModule
      ],
      declarations: [
          ModalResendNfseComponent,
          imgCdnStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalResendNfseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
