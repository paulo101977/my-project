import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';

@Component({
  selector: 'modal-resend-nfse',
  templateUrl: './modal-resend-nfse.component.html',
})
export class ModalResendNfseComponent implements OnInit {
  @Input() modalId: string;
  @Input() documentName: string;
  @Input() documentErroList: Array<String>;

  public resendButtonConfig: ButtonConfig;

  constructor(private sharedService: SharedService) {}

  ngOnInit() {
    this.documentErroList = [];
    this.documentErroList.push('Campo “CPF do Tomador” inválido');
    this.documentErroList.push('Campo “Contato” é obrigatório');

    this.setButtonConfig();
  }

  // Actions
  public resendNfse() {
    // todo implementar método resendNfse
  }

  // Config
  public setButtonConfig() {
    this.resendButtonConfig = this.sharedService.getButtonConfig(
      'resendNfse',
      this.resendNfse.bind(this),
      'label.resendNfse',
      ButtonType.Primary,
    );
    this.resendButtonConfig.extraClass = 'thf-single-modal-button-size';
  }
}
