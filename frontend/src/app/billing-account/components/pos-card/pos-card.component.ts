import {
  Component, OnInit, Input, Output, EventEmitter
} from '@angular/core';

@Component({
  selector: 'pos-card',
  templateUrl: './pos-card.component.html',
  styleUrls: ['./pos-card.component.css']
})
export class PosCardComponent implements OnInit {

  // TODO: implement model
  @Input() item: any;
  @Input() hasFavorite: true;
  @Output() favoriteEmitter = new EventEmitter();
  @Output() itemEmitter = new EventEmitter();

  constructor() { }

  public itemClicked(item) {
    this.itemEmitter.emit(item);
  }

  public changeFavorite(item) {
    if (this.hasFavorite) {
      this.item.isFavorite = !item.isFavorite;
      this.favoriteEmitter.emit(this.item);
    }
  }

  ngOnInit() {
  }

}
