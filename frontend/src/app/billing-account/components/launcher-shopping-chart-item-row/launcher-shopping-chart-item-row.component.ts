import {
  Component,
  ViewEncapsulation,
  Input,
  EventEmitter,
  Output
} from '@angular/core';


@Component({
  selector: 'launcher-shopping-chart-item-row',
  templateUrl: './launcher-shopping-chart-item-row.component.html',
  styleUrls: ['./launcher-shopping-chart-item-row.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LauncherShoppingChartItemRowComponent {

  @Input() item;
  @Input() currencySymbol: string;
  @Output() removeItemEvent = new EventEmitter();
  @Output() valueChangeEvent = new EventEmitter();

  public removeItem() {
    this.removeItemEvent.emit(this.item);
  }

  public increase() {
    const { item } = this;
    if ( item.quantity ) {
      item.quantity += 1;
      item.amount = item.quantity * item.unitPrice;
      this.valueChangeEvent.emit(item);
    }
  }

  public decrease() {
    const { item } = this;
    if ( item.quantity > 1 ) {
      item.quantity -= 1;
      item.amount = item.quantity * item.unitPrice;
      this.valueChangeEvent.emit(item);
    }
  }


  public getValue() {
    const { item } = this;
    if ( item && item.quantity) {
      const { quantity , unitPrice } = item;
      return quantity * unitPrice;
    } else {
      return 0;
    }
  }


  formatQuantity() {
    const { item } = this;

    if ( item ) {
      const { quantity } = item;
      if (quantity) {
        if (quantity < 10) {
          return `0${quantity}`;
        }

        return quantity;
      }
    }
  }

}
