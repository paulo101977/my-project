// TODO: create test

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LauncherShoppingChartItemRowComponent } from './launcher-shopping-chart-item-row.component';
import { configureTestSuite } from 'ng-bullet';
import { currencyFormatPipe } from '../../../../../testing';

describe('LauncherShoppingChartItemRowComponent', () => {
  let component: LauncherShoppingChartItemRowComponent;
  let fixture: ComponentFixture<LauncherShoppingChartItemRowComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        LauncherShoppingChartItemRowComponent,
        currencyFormatPipe,
      ],
    });

    fixture = TestBed.createComponent(LauncherShoppingChartItemRowComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
