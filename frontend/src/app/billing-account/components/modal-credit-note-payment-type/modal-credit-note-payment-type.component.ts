import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SelectObjectOption } from '@app/shared/models/selectModel';
import { BillingItemResource } from '@app/shared/resources/billingItem/billing-item.resource';
import { CommomService } from '@app/shared/services/shared/commom.service';
import { ActivatedRoute } from '@angular/router';
import { ButtonConfig, ButtonType } from '@app/shared/models/button-config';
import { SharedService } from '@app/shared/services/shared/shared.service';
import { PaymentTypeWithBillingItem } from '@app/shared/models/billingType/payment-type-with-billing-item';
import {SuccessError} from 'app/shared/models/success-error-enum';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {PaymentTypeEnum} from 'app/shared/models/reserves/payment-type-enum';
import {ReasonCategoryEnum} from 'app/shared/models/reason/reason-category-enum';
import {ReasonResource} from 'app/shared/resources/reason/reason.resource';

@Component({
  selector: 'modal-credit-note-payment-type',
  templateUrl: './modal-credit-note-payment-type.component.html'
})
export class ModalCreditNotePaymentTypeComponent implements OnInit {
  @Input() modalId: string;

  @Output() issueCreditNote = new EventEmitter();
  @Output() closeModal = new EventEmitter();

  private propertyId: number;
  private paymentTypeWithBillingItemList: Array<PaymentTypeWithBillingItem>;
  public paymentTypeList: Array<SelectObjectOption>;
  public plasticBrandPropertyList: Array<SelectObjectOption>;
  public reasonList: Array<SelectObjectOption>;
  public billingItemId: number;
  public reasonId: number;
  public isCard: boolean;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  constructor(
    private billingItemResource: BillingItemResource,
    private commomService: CommomService,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private toasterEmitService: ToasterEmitService,
    private reasonResource: ReasonResource) {}

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;
    this.setButtonConfig();
    this.setPaymentTypeList();
    this.setReasonList();
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'person-header-cancel',
      this.clickCloseModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'issue-credit-note',
      this.emitChosenCreditNote,
      'action.confirm',
      ButtonType.Primary,
    );
  }

  private setPaymentTypeList(): void {
    this.paymentTypeList = new Array<SelectObjectOption>();
    this.billingItemResource.getAllPaymentTypeWithBillingItemListByPropertyId(this.propertyId).subscribe(response => {
      this.paymentTypeList = this.commomService.toOption(response, 'id', 'paymentTypeName');
      this.paymentTypeWithBillingItemList = response.items;
    });
  }

  private setReasonList(): void {
    this.reasonList = [];
    this.reasonResource
      .getAllReasonListByPropertyIdAndCategoryId(this.propertyId, ReasonCategoryEnum.CreditNoteIssue)
      .subscribe(response => {
        const items = response.items;
        if (response.items) {
          this.reasonList = this.commomService
            .toOption(items, 'id', 'reasonName');
        }
      });
  }

  private clickCloseModal = () => {
    this.closeModal.emit();
  }

  public selectBillingItemId(paymentTypeId: number) {
    if (paymentTypeId) {
      if (paymentTypeId == PaymentTypeEnum.CreditCard || paymentTypeId == PaymentTypeEnum.Debit) {
        this.isCard = true;
        this.setPlasticBrandPropertyList(paymentTypeId);
        this.billingItemId = null;
      } else {
        this.isCard = false;
        this.billingItemId = this.getBillingItemId(paymentTypeId);
      }
    } else {
      this.billingItemId = null;
    }
  }

  private getBillingItemId(paymentTypeId: number): number {
    const paymentTypeWithBillingItem = this.paymentTypeWithBillingItemList.find(p => p.id == paymentTypeId);
    return paymentTypeWithBillingItem ? paymentTypeWithBillingItem.billingItemId : null;
  }

  private emitChosenCreditNote = () => {
    if (!this.billingItemId) {
      this.toasterEmitService.emitChange(SuccessError.error, 'alert.selectPaymentType');
      return;
    }

    if (!this.reasonId) {
      this.toasterEmitService.emitChange(SuccessError.error, 'alert.selectReason');
      return;
    }

    this.issueCreditNote.emit({billingItemId: this.billingItemId, reasonId: this.reasonId });
    this.closeModal.emit();
  }

  private setPlasticBrandPropertyList(paymentTypeId: number): void {
    this.plasticBrandPropertyList = [];
    this.billingItemResource.getAllBillingItemPlasticBrandCompanyClientListByPaymentTypeId(paymentTypeId).subscribe(response => {
      this.plasticBrandPropertyList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        response,
        'id',
        'plasticBrandFormatted',
      );
    });
  }

  public selectPlasticBrandPropertyId(plasticBrandPropertyId: number) {
    this.billingItemId = plasticBrandPropertyId;
  }

  public selectReasonId(id: number) {
    this.reasonId = id;
  }

}
