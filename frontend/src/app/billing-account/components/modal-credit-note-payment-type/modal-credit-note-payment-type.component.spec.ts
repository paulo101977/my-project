import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCreditNotePaymentTypeComponent } from './modal-credit-note-payment-type.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ButtonConfig, ButtonSize, ButtonType, Type} from 'app/shared/models/button-config';
import {SharedService} from 'app/shared/services/shared/shared.service';
import { ActivatedRouteStub, createServiceStub } from '../../../../../testing';
import {BillingItemResource} from 'app/shared/resources/billingItem/billing-item.resource';
import {Observable, of} from 'rxjs';
import {CommomService} from 'app/shared/services/shared/commom.service';
import {SelectObjectOption} from 'app/shared/models/selectModel';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {SuccessError} from 'app/shared/models/success-error-enum';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { ActivatedRoute } from '@angular/router';
import { reasonResourceStub } from 'app/shared/resources/reason/testing';

describe('ModalCreditNotePaymentTypeComponent', () => {
  let component: ModalCreditNotePaymentTypeComponent;
  let fixture: ComponentFixture<ModalCreditNotePaymentTypeComponent>;

  const sharedServiceStub = createServiceStub(SharedService, {
    getButtonConfig(
      id: string,
      callback: Function,
      textButton: string,
      buttonType: ButtonType,
      buttonSize?: ButtonSize,
      isDisabled?: boolean,
      type?: Type): ButtonConfig { return new ButtonConfig(); },
    getHeaders(): any { return ''; }
  });

  const billingItemResourceStub = createServiceStub(BillingItemResource, {
    getAllPaymentTypeWithBillingItemListByPropertyId(propertyId: number): Observable<any> { return of({items: []}); }
  });

  const commomServiceStub = createServiceStub(CommomService, {
    toOption(objectToConvert: any, keyName: string, valueName: string): Array<SelectObjectOption> { return []; }
  });

  const toasterEmitServiceStub = createServiceStub(ToasterEmitService, {
    emitChange(isSuccess: SuccessError, message: string) {}
  });

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
      ],
      declarations: [ ModalCreditNotePaymentTypeComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        sharedServiceStub,
        billingItemResourceStub,
        commomServiceStub,
        toasterEmitServiceStub,
        reasonResourceStub,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ]
    });

    fixture = TestBed.createComponent(ModalCreditNotePaymentTypeComponent);
    component = fixture.componentInstance;

    spyOn<any>(
        component['reasonResource'], 'getAllReasonListByPropertyIdAndCategoryId'
    ).and.returnValue(of({items: []}));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should selectPlasticBrandPropertyId', () => {
    const billingItemId = 1;

    component.selectPlasticBrandPropertyId(billingItemId);

    expect(component.billingItemId).toEqual(billingItemId);
  });

  it('should selectReasonId', () => {
    const reasonId = 1;

    component.selectReasonId(reasonId);

    expect(component.reasonId).toEqual(reasonId);
  });
});
