import { SelectObjectOption } from 'app/shared/models/selectModel';
import { GetAllBillingItemService } from 'app/shared/models/billingType/get-all-billing-item-service';
import { FormGroup } from '@angular/forms';
import { Component, Input, OnChanges, forwardRef } from '@angular/core';
import { AutocompleteDisplayEnum, AutocompleteTypeEnum } from '../../../shared/models/autocomplete/autocomplete.enum';
import { UpdateItemResponse } from '../../../shared/models/autocomplete/update-item-response';
import { BillingItemService } from '../../../shared/models/billingType/billing-item-service';
import { BillingItemTaxServiceAssociate } from '../../../shared/models/billingType/billing-item-tax-service-associate';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { BillingEntryTypeService } from '../../../shared/services/billing-entry-type/billing-entry-type.service';
import { BillingItemServiceResource } from 'app/shared/resources/billingItem/billing-item-service.resource';

@Component({
  selector: 'billing-account-post-debit',
  templateUrl: './billing-account-post-debit.component.html',
  styleUrls: ['./billing-account-post-debit.component.css'],
})
export class BillingAccountPostDebitComponent implements OnChanges {
  @Input() formModal: FormGroup;

  @Input() isDisabled: boolean;

  @Input() propertyId: number;

  public optionsCurrencyMask: any;
  public billingItemTaxServiceAssociateList: Array<BillingItemTaxServiceAssociate>;
  private billingItemServiceList: Array<BillingItemService>;

  // Autocomplete dependencies
  public billingItemServiceSelectList: Array<SelectObjectOption>;

  constructor(
    private sharedService: SharedService,
    private billingEntryTypeService: BillingEntryTypeService,
    private billingItemServiceResource: BillingItemServiceResource,
  ) {}

  ngOnChanges() {
    this.setVars();
    this.setListeners();
  }

  private setVars(): void {
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
    this.billingItemTaxServiceAssociateList = new Array<BillingItemTaxServiceAssociate>();
    this.setBillingItemServiceList();
  }

  public setBillingItemServiceList(): void {
    this.billingItemServiceList = [];
    this.billingItemServiceSelectList = [];
    this.billingItemServiceResource.searchBillingItemServiceListByCriteria(this.propertyId).subscribe(response => {
      this.billingItemServiceList = response.items;
      this.billingItemServiceList.forEach(billingItemService => {
        const selectObjectOption = new SelectObjectOption();
        selectObjectOption.key = billingItemService.id;
        selectObjectOption.value = `${billingItemService.name} - ${billingItemService.billingItemCategoryName}`;
        this.billingItemServiceSelectList.push(selectObjectOption);
      });
    });
  }

  private setListeners(): void {
    this.formModal.get('postDebit.priceWithoutTax').valueChanges.subscribe(priceWithoutTax => {
      if (isNaN(priceWithoutTax) || priceWithoutTax == 0) {
        this.formModal.get('postDebit.priceWithoutTax').setValue(null);
      } else {
        this.setPriceTotal(priceWithoutTax);
      }
    });
  }

  private setPriceTotal(priceWithoutTax: number): void {
    const totalPriceDefault = 0;
    if (this.billingItemTaxServiceAssociateList && this.billingItemTaxServiceAssociateList.length > 0 && !isNaN(priceWithoutTax)) {
      const totalTaxPercentage = this.billingEntryTypeService.getTotalTaxPercentage(this.billingItemTaxServiceAssociateList);
      const priceWithTax = this.sharedService.getValueWithPercentage(priceWithoutTax, totalTaxPercentage);
      this.formModal.get('totalPrice').setValue(priceWithTax.toFixed(2));
    } else if (isNaN(priceWithoutTax) || !priceWithoutTax) {
      this.formModal.get('totalPrice').setValue(totalPriceDefault.toFixed(2));
    } else {
      this.formModal.get('totalPrice').setValue(priceWithoutTax.toFixed(2));
    }
  }

  public removeBillingItemServiceFromSelect() {
    this.formModal.get('postDebit.billingItemServiceId').reset();
    this.billingItemTaxServiceAssociateList = [];
  }

  public changeBillingItemServiceInSelect(billingItemId: number) {
    const billingItemService = this.billingItemServiceList.find(b => b.id == billingItemId);
    this.billingItemTaxServiceAssociateList = billingItemService.serviceAndTaxList;
    this.setPriceTotal(this.formModal.get('postDebit.priceWithoutTax').value);
  }
}
