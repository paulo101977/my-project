import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { InvoiceService } from 'app/billing-account/services/invoice.service';
import { InvoiceDto } from 'app/shared/models/dto/invoice/invoice-dto';

@Component({
  selector: 'modal-europe-fiscal-document',
  templateUrl: './modal-europe-fiscal-document.component.html',
  styleUrls: [ './modal-europe-fiscal-document.component.css' ]
})
export class ModalEuropeFiscalDocumentComponent implements OnInit {
  public readonly MODAL_EUROPE_DATATABLE_LIMIT = 5;

  @ViewChild('statusCell') statusCellTemplate: TemplateRef<any>;
  @ViewChild('billingInvoiceTemplate') billingInvoiceTemplate: TemplateRef<any>;

  @Input() modalId: string;
  @Input() accountOwnerName: string;

  @Input()
  set invoices(list: Array<InvoiceDto>) {
    if (!list) {
      return;
    }

    this.pageItemsList = list.map((item) => {
      const options = [];
      const { billingInvoiceStatusId, billingInvoiceTypeId } = item;

      if (this.invoiceService.canResendDocument(billingInvoiceStatusId, billingInvoiceTypeId)) {
        options.push({
          title: 'variable.resend',
          callbackFunction: this.openModalResend
        });
      }

      if (this.invoiceService.canIssueCreditNote(billingInvoiceStatusId, billingInvoiceTypeId)) {
        options.push({
          title: 'label.issueCreditNote',
          callbackFunction: this.closeModalAndOpenModalCreditNote
        });
      }

      return {
        ...item,
        ...(options ? { options } : {})
      };
    });
  }

  get invoices(): Array<InvoiceDto> {
    return this.pageItemsList;
  }

  @Output() issueCreditNote = new EventEmitter<InvoiceDto>();
  @Output() issueDocumentResend = new EventEmitter<InvoiceDto>();

  // Table
  public columns: Array<any>;
  public pageItemsList: Array<any>;
  public itemsOption: Array<any>;

  constructor(
    private cdr: ChangeDetectorRef,
    private invoiceService: InvoiceService,
  ) {
  }

  ngOnInit() {
    this.setTableColumns();
  }

  private setTableColumns() {
    if (!this.pageItemsList) {
      this.pageItemsList = [];
    }

    this.columns = [
      { name: 'label.number', cellTemplate: this.billingInvoiceTemplate },
      { name: 'label.type', prop: 'billingInvoiceTypeName' },
      { name: 'label.status', cellTemplate: this.statusCellTemplate }
    ];
  }

  public openModalResend = (invoice) => {
    this.issueDocumentResend.emit(invoice);
    this.cdr.detectChanges();
  }

  public closeModalAndOpenModalCreditNote = (invoice: InvoiceDto) => {
    this.issueCreditNote.emit(invoice);
    this.cdr.detectChanges();
  }

  public invoiceIsIssued(id: number) {
    return this.invoiceService.isIssued(id);
  }
}
