import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ModalEuropeFiscalDocumentComponent } from './modal-europe-fiscal-document.component';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { imgCdnStub } from '../../../../../testing';

describe('ModalEuropeFiscalDocumentComponent', () => {
  let component: ModalEuropeFiscalDocumentComponent;
  let fixture: ComponentFixture<ModalEuropeFiscalDocumentComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
      ],
      declarations: [
          ModalEuropeFiscalDocumentComponent,
          imgCdnStub,
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(ModalEuropeFiscalDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
