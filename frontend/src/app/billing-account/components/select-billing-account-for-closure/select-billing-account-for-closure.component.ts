import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { GetAllBillingAccountForClosure } from '../../../shared/models/billing-account/get-all-billing-account-for-closure';
import { BillingAccountForClosure } from '../../../shared/models/billing-account/billing-account-for-closure';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../shared/models/button-config';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { BillingAccountCheckedListWithTotal } from '../../../shared/models/billing-account/billing-account-checked-list-with-total';

@Component({
  selector: 'select-billing-account-for-closure',
  templateUrl: 'select-billing-account-for-closure.component.html',
  styleUrls: ['select-billing-account-for-closure.component.css'],
})
export class SelectBillingAccountForClosureComponent implements OnInit, OnChanges {
  @Input() getAllBillingAccountForClosure: GetAllBillingAccountForClosure;
  @Output() openModalPayment = new EventEmitter();
  @Output() closeBillingAccountClosureModal = new EventEmitter();
  @Output() openStatementModal = new EventEmitter();
  @Output() billingAccountForClosureCheckedListWasUpdated = new EventEmitter<Array<BillingAccountForClosure>>();

  // Component properties
  public billingAccountForClosureList: Array<BillingAccountForClosure>;
  public billingAccountForClosureCheckedList: Array<BillingAccountForClosure>;
  public totalBillingAccountForClosureCheckedList: number;

  // Button dependencies
  public buttonCancelBillingAccountClosure: ButtonConfig;
  public buttonPaymentModal: ButtonConfig;
  public buttonStatementBillingAccountClosure: ButtonConfig;

  constructor(private sharedService: SharedService) {}

  ngOnInit() {
    this.setVars();
    this.setButtonConfig();
  }

  ngOnChanges(changes) {
    this.setBillingAccountForClosureList();
  }

  private setVars() {
    this.setBillingAccountForClosureList();
  }

  private setBillingAccountForClosureList(): void {
    this.getAllBillingAccountForClosure.total = Math.abs(this.getAllBillingAccountForClosure.total);
    this.billingAccountForClosureList = this.getAllBillingAccountForClosure.billingAccountForClosureList;
    if (this.billingAccountForClosureList) {
      this.getBillingAccountForClosureCheckedList(this.billingAccountForClosureList);
    }
  }

  private setButtonConfig() {
    this.buttonStatementBillingAccountClosure = this.sharedService.getButtonConfig(
      'statement',
      this.showStatementModal,
      'label.statement',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.buttonCancelBillingAccountClosure = this.sharedService.getButtonConfig(
      'cancelModal',
      this.onCloseBillingAccountClosureModal,
      'action.cancel',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );
    this.buttonPaymentModal = this.sharedService.getButtonConfig(
      'action.pay',
      this.openModalPaymentCallback,
      'action.pay',
      ButtonType.Primary,
      ButtonSize.Normal,
    );

    this.buttonStatementBillingAccountClosure.isDisabled = true;
    this.buttonPaymentModal.isDisabled = true;
  }

  public showStatementModal = () => {
    const guidBillingAccountList = new Array<string>();
    const billingAccountCheckedListWithTotal = new BillingAccountCheckedListWithTotal();

    this.billingAccountForClosureCheckedList.forEach(billingAccountForClosure => {
      guidBillingAccountList.push(billingAccountForClosure.id);
    });

    billingAccountCheckedListWithTotal.billingAccountCheckedList = guidBillingAccountList;
    billingAccountCheckedListWithTotal.total = this.totalBillingAccountForClosureCheckedList;

    this.openStatementModal.emit(billingAccountCheckedListWithTotal);
  }

  public onCloseBillingAccountClosureModal = () => {
    this.setIsCheckedOfBillingAccountForClosureListToTrue();
    this.closeBillingAccountClosureModal.emit();
  }

  private setIsCheckedOfBillingAccountForClosureListToTrue(): void {
    this.getAllBillingAccountForClosure.billingAccountForClosureList.forEach(billingAccountForClosure => {
      billingAccountForClosure.isChecked = true;
    });
  }

  public openModalPaymentCallback = () => {
    this.openModalPayment.emit();
  }

  public getBillingAccountForClosureCheckedList(billingAccountForClosureCheckedList: Array<BillingAccountForClosure>) {
    let total = 0;
    this.billingAccountForClosureCheckedList = billingAccountForClosureCheckedList;
    this.billingAccountForClosureCheckedList.forEach(billingAccountForClosureChecked => {
      total += billingAccountForClosureChecked.total;
    });

    if (billingAccountForClosureCheckedList.length >= 2) {
      this.buttonStatementBillingAccountClosure.isDisabled = false;
      this.buttonPaymentModal.isDisabled = false;
    } else {
      this.buttonStatementBillingAccountClosure.isDisabled = true;
      this.buttonPaymentModal.isDisabled = true;
    }

    this.billingAccountForClosureCheckedListWasUpdated.emit(this.billingAccountForClosureCheckedList);
    this.totalBillingAccountForClosureCheckedList = Math.abs(total);
  }
}
