import { BillingAccountWithEntry } from 'app/shared/models/billing-account/billing-account-with-entry';
import {InvoiceDto} from 'app/shared/models/dto/invoice/invoice-dto';

export const INVOICE_LIST = 'INVOICE_LIST';

export class Data {

  public static readonly INVOICE_LIST = <InvoiceDto>{
    billingAccountId: 'ea6ed63b-33b1-4252-84b6-41f4bf91231f',
    billingInvoicePropertyId: 'fe43f5f6-e33e-4f46-aab7-cb8ca994c7ac',
    billingInvoiceStatusId: 10,
    billingInvoiceTypeId: 4,
    billingInvoiceTypeName: 'Factura',
    id: '42fa5fdf-b21b-4e56-bb55-c083a8268622'
  };

  public static readonly INVOICE: InvoiceDto = {
    aditionalDetails: '',
    billingAccountId: '',
    billingInvoiceCancelReasonId: '',
    billingInvoiceNumber: 0,
    billingInvoicePropertyId: '',
    billingInvoiceSeries: 0,
    billingInvoiceStatusId: 0,
    billingInvoiceStatusName: 0,
    billingInvoiceTypeId: 0,
    billingInvoiceTypeName: '',
    cancelDate: undefined,
    cancelUserId: '',
    emissionDate: undefined,
    emissionUserId: '',
    emissionUserName: '',
    errorDate: undefined,
    externalCodeNumber: '',
    externalEmissionDate: undefined,
    externalNumber: 0,
    externalRps: 0,
    externalSeries: 0,
    id: '',
    integratorId: '',
    integratorLink: '',
    integratorXml: '',
    propertyId: 0
  };

  public static readonly BILLING_ACCOUNT_WITH_ENTRY: BillingAccountWithEntry = {
    billingAccountId: '',
    billingAccountItemName: '',
    billingAccountItems: [],
    invoices: [],
    isBlocked: false,
    percentageRate: 0,
    startDate: undefined,
    statusId: 0,
    statusName: '',
    total: 0,
    totalWithoutRate: 0
  };
}
