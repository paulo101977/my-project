import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class BillingAccountNotifierService {

  private billingAccountChange = new Subject<any>();
  billingAccountChangeListener$ = this.billingAccountChange.asObservable();

  constructor() { }

  public requestBillingAccountToUpdate() {
    this.billingAccountChange.next();
  }
}
