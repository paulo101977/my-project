import { Injectable } from '@angular/core';
import { BillingInvoiceTypeEnum } from 'app/shared/models/dto/invoice/billing-invoice-type-enum';
import { NfStatusEnum } from 'app/shared/models/dto/nf/nf-status-enum';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor() { }

  canResendDocument(billingInvoiceStatusId: number, billingInvoiceTypeId: number): boolean {
    const hasCorrectStatusId = [NfStatusEnum.NotSend, NfStatusEnum.Error].includes(billingInvoiceStatusId);
    const hasCorrectTypeId = [
      BillingInvoiceTypeEnum.CreditNoteTotal,
      BillingInvoiceTypeEnum.CreditNotePartial,
      BillingInvoiceTypeEnum.Factura
    ].includes(billingInvoiceTypeId);

    return hasCorrectStatusId && hasCorrectTypeId;
  }

  canIssueCreditNote(billingInvoiceStatusId: number, billingInvoiceTypeId: number): boolean {
    const hasCorrectStatusId = NfStatusEnum.Issued == billingInvoiceStatusId;
    const hasCorrectTypeId = BillingInvoiceTypeEnum.Factura == billingInvoiceTypeId;

    return hasCorrectStatusId && hasCorrectTypeId;
  }

  isIssued(billingInvoiceStatusId: number): boolean {
    return billingInvoiceStatusId == NfStatusEnum.Issued;
  }
}
