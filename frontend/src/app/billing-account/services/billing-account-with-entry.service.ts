import { Injectable } from '@angular/core';
import { BillingAccountWithEntryStates } from 'app/billing-account/models/billing-account-with-entry-states';
import { BillingAccountStatusEnum } from 'app/shared/models/billing-account/billing-account-status-enum';
import { BillingAccountWithEntry } from 'app/shared/models/billing-account/billing-account-with-entry';
import { NfStatusEnum } from 'app/shared/models/dto/nf/nf-status-enum';

@Injectable({
  providedIn: 'root'
})
export class BillingAccountWithEntryService {

  constructor() { }

  public getStates(billingAccountWithEntry: BillingAccountWithEntry): BillingAccountWithEntryStates {
    return {
      hasInvoices: this.hasInvoices(billingAccountWithEntry),
      hasIssuedInvoices: this.hasIssuedInvoices(billingAccountWithEntry),
      isOpen: this.isOpen(billingAccountWithEntry)
    };
  }

  public hasInvoices(billingAccountWithEntry: BillingAccountWithEntry): boolean {
    return !!billingAccountWithEntry.invoices && billingAccountWithEntry.invoices.length > 0;
  }

  public hasIssuedInvoices(billingAccountWithEntry: BillingAccountWithEntry): boolean {
    if (!this.hasInvoices(billingAccountWithEntry)) {
      return false;
    }

    return billingAccountWithEntry.invoices.some((invoice) => {
      return invoice.billingInvoiceStatusId == NfStatusEnum.Issued;
    });
  }

  public isOpen(billingAccountWithEntry: BillingAccountWithEntry): boolean {
    return billingAccountWithEntry.statusId == BillingAccountStatusEnum.Open;
  }
}
