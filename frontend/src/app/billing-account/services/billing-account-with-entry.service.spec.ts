import { TestBed } from '@angular/core/testing';
import { Data } from 'app/billing-account/mock-data';
import { BillingAccountStatusEnum } from 'app/shared/models/billing-account/billing-account-status-enum';
import { BillingAccountWithEntry } from 'app/shared/models/billing-account/billing-account-with-entry';
import { NfStatusEnum } from 'app/shared/models/dto/nf/nf-status-enum';

import { BillingAccountWithEntryService } from './billing-account-with-entry.service';

describe('BillingAccountWithEntryService', () => {
  let service: BillingAccountWithEntryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ BillingAccountWithEntryService ]
    });

    service = TestBed.get(BillingAccountWithEntryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getStates', () => {
    it('should get all billing account with entry states', () => {
      spyOn(service, 'hasInvoices').and.returnValue(true);
      spyOn(service, 'hasIssuedInvoices').and.returnValue(true);
      spyOn(service, 'isOpen').and.returnValue(true);

      expect(service.getStates(Data.BILLING_ACCOUNT_WITH_ENTRY)).toEqual({
        hasInvoices: true,
        hasIssuedInvoices: true,
        isOpen: true,
      });
    });
  });

  describe('#hasInvoices', () => {
    it('should return false if it doesn\'t has invoices', () => {
      const data  = { ...Data.BILLING_ACCOUNT_WITH_ENTRY };

      expect(service.hasInvoices(data)).toBe(false);
    });

    it('should return false if it has invoices', () => {
      const data  = {
        ...Data.BILLING_ACCOUNT_WITH_ENTRY,
        invoices: [
          { ...Data.INVOICE }
        ]
      };

      expect(service.hasInvoices(data)).toBe(true);
    });
  });

  describe('#hasIssuedInvoices', () => {
    it('should return false if it doesn\'t has invoices', () => {
      const data  = { ...Data.BILLING_ACCOUNT_WITH_ENTRY };

      expect(service.hasIssuedInvoices(data)).toBe(false);
    });

    it('should return false if it doesn\'t has issued invoices', () => {
      const data: BillingAccountWithEntry  = {
        ...Data.BILLING_ACCOUNT_WITH_ENTRY,
        invoices: [
          {
            ...Data.INVOICE,
            billingInvoiceStatusId: NfStatusEnum.NotSend,
          }
        ]
      };

      expect(service.hasIssuedInvoices(data)).toBe(false);
    });

    it('should return true if it has issued invoices', () => {
      const data: BillingAccountWithEntry  = {
        ...Data.BILLING_ACCOUNT_WITH_ENTRY,
        invoices: [
          {
            ...Data.INVOICE,
            billingInvoiceStatusId: NfStatusEnum.Issued,
          }
        ]
      };

      expect(service.hasIssuedInvoices(data)).toBe(true);
    });
  });

  describe('#isOpen', () => {
    it('should return false if status isn\'t Open', () => {
      const data = {
        ...Data.BILLING_ACCOUNT_WITH_ENTRY,
        statusId: BillingAccountStatusEnum.Closed
      };

      expect(service.isOpen(data)).toBe(false);
    });

    it('should return true if status is Open', () => {
      const data = {
        ...Data.BILLING_ACCOUNT_WITH_ENTRY,
        statusId: BillingAccountStatusEnum.Open
      };

      expect(service.isOpen(data)).toBe(true);
    });
  });
});
