import { createServiceStub } from '../../../../../testing';
import { BillingLauncherItemService } from 'app/billing-account/services/billing-launcher-item-service';
import { PaymentTypeEnum } from 'app/shared/models/reserves/payment-type-enum';
import { CardsService } from 'app/billing-account/services/cards.service';
import { CardsEnum } from 'app/billing-account/models/cards.enum';
import { LauncherMainEnum } from 'app/billing-account/models/launcher-main.enum';

export const billingLauncherItemServiceStub = createServiceStub(BillingLauncherItemService, {
    applyQuotationInValue(
        value: number, currencySelectedId: string, propertyCurrencyId: string, quotationList: Array<any>
    ): number {
        return 1;
    },

    getCurrencyId(propertyId): string {
        return '';
    },

    getCurrencyList(): Promise<any> {
        return null;
    },

    getCurrentSelection() {
        return null;
    },

    setCurrentSelection(item) {
    },

    translateFromEnum(key: PaymentTypeEnum): string | any {
        return null;
    }
});

export const cardsServiceStub = createServiceStub(CardsService, {
    getCardsEnumNames(): any[] {
        return null;
    },

    getCardsKeys(): any[] {
        return null;
    },

    getCardsTranslations(key: CardsEnum): string {
        return '';
    },

    getImageUrl(key: PaymentTypeEnum): string {
        return '';
    },

    getKeysByKeyName(keyNames: string[]): any[] {
        return null;
    },

    getMainScreenImgUrl(key: LauncherMainEnum): string {
        return '';
    },

    getRouteName(key: PaymentTypeEnum): string {
        return '';
    }
});

