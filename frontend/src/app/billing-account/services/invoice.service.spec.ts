import { TestBed } from '@angular/core/testing';
import { BillingInvoiceTypeEnum } from 'app/shared/models/dto/invoice/billing-invoice-type-enum';
import { NfStatusEnum } from 'app/shared/models/dto/nf/nf-status-enum';

import { InvoiceService } from './invoice.service';

describe('InvoiceService', () => {
  let service: InvoiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InvoiceService]
    });

    service = TestBed.get(InvoiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#canResendDocument', () => {
    it('should be true when status is NotSend or Error, and type is CreditNoteTotal, CreditNotePartial or Factura', () => {
      const verifications = [
        service.canResendDocument(NfStatusEnum.NotSend, BillingInvoiceTypeEnum.CreditNoteTotal),
        service.canResendDocument(NfStatusEnum.NotSend, BillingInvoiceTypeEnum.CreditNotePartial),
        service.canResendDocument(NfStatusEnum.NotSend, BillingInvoiceTypeEnum.Factura),
        service.canResendDocument(NfStatusEnum.Error, BillingInvoiceTypeEnum.CreditNoteTotal),
        service.canResendDocument(NfStatusEnum.Error, BillingInvoiceTypeEnum.CreditNotePartial),
        service.canResendDocument(NfStatusEnum.Error, BillingInvoiceTypeEnum.Factura),
      ];

      expect(verifications).toEqual([true, true, true, true, true, true]);
    });

    it('should be false when status isn\'t NotSend or Error, or type isn\'t CreditNoteTotal, CreditNotePartial or Factura', () => {
      const verifications = [
        service.canResendDocument(NfStatusEnum.NotSend, BillingInvoiceTypeEnum.DebitNote),
        service.canResendDocument(NfStatusEnum.Error, BillingInvoiceTypeEnum.NFCE),
        service.canResendDocument(NfStatusEnum.Issued, BillingInvoiceTypeEnum.CreditNoteTotal),
        service.canResendDocument(NfStatusEnum.Issued, BillingInvoiceTypeEnum.CreditNotePartial),
        service.canResendDocument(NfStatusEnum.Issued, BillingInvoiceTypeEnum.Factura),
      ];

      expect(verifications).toEqual([false, false, false, false, false]);
    });
  });

  describe('#canIssueCreditNote', () => {
    it('should be true when status is Issued and type is Factura', () => {
      const verification = service.canIssueCreditNote(NfStatusEnum.Issued, BillingInvoiceTypeEnum.Factura);

      expect(verification).toEqual(true);
    });

    it('should be false when status isn\'t Issued and type isn\'t Factura', () => {
      const verifications = [
        service.canIssueCreditNote(NfStatusEnum.Issued, BillingInvoiceTypeEnum.NFCE),
        service.canIssueCreditNote(NfStatusEnum.Error, BillingInvoiceTypeEnum.Factura),
      ];

      expect(verifications).toEqual([false, false]);
    });
  });

  describe('#isIssued', () => {
    it('should be true when status is Issued', () => {
      const verification = service.isIssued(NfStatusEnum.Issued);

      expect(verification).toEqual(true);
    });

    it('should be false when status isn\'t Issued', () => {
      const verification = service.isIssued(NfStatusEnum.Error);

      expect(verification).toEqual(false);
    });
  });
});
