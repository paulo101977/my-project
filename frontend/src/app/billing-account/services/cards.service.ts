import { Injectable } from '@angular/core';
import { CardsEnum } from '../models/cards.enum';
import { LauncherMainEnum } from 'app/billing-account/models/launcher-main.enum';
import { PaymentTypeEnum } from 'app/shared/models/reserves/payment-type-enum';
import { AssetsCdnService } from '@inovacao-cmnet/thx-ui';

@Injectable({
  providedIn: 'root'
})
export class CardsService {

  constructor( private assetsService: AssetsCdnService) { }

  getMainScreenImgUrl(key: LauncherMainEnum): string {
    switch (key) {
      case LauncherMainEnum.Service:
        return 'services.svg';
      case LauncherMainEnum.Pos:
        return 'pos.svg';
      case LauncherMainEnum.Credit:
        return 'credit.svg';
      default:
        return '';
    }
  }

  public getImageUrl(key: PaymentTypeEnum) {
    switch (key) {
      case PaymentTypeEnum.CreditCard:
        return this.assetsService.getImgUrlTo('credit_card.svg');
      case PaymentTypeEnum.Debit:
        return this.assetsService.getImgUrlTo('debit_card.svg');
      case PaymentTypeEnum.Money:
        return this.assetsService.getImgUrlTo('money_card.svg');
      case PaymentTypeEnum.Deposit:
        return this.assetsService.getImgUrlTo('deposit_card.svg');
      case PaymentTypeEnum.Check:
        return this.assetsService.getImgUrlTo('check_card.svg');
      default:
        return '';
    }
  }

  public getRouteName(key: PaymentTypeEnum): string {
    switch (key) {
      case PaymentTypeEnum.CreditCard:
        return 'credit';
      case PaymentTypeEnum.Debit:
        return 'debit';
      case PaymentTypeEnum.Money:
        return 'money';
      case PaymentTypeEnum.Deposit:
        return 'deposit';
      case PaymentTypeEnum.Check:
        return 'check';
      default:
        return '';
    }
  }

  public getCardsTranslations(key: CardsEnum): string {
    switch (key) {
      case CardsEnum.Credit:
        return 'text.cardCredit';
      case CardsEnum.Debit:
        return 'text.cardDebit';
      case CardsEnum.Money:
        return 'text.cardMoney';
      case CardsEnum.Deposit:
        return 'text.cardDeposit';
      case CardsEnum.Check:
        return 'text.cardCheck';
      default:
        return '';
    }
  }

  public getKeysByKeyName( keyNames: string[] ): any[] {

    return keyNames.map( name => {
      return CardsEnum[name];
    });

  }

  public getCardsEnumNames() {
    const response = [];

    for (const prop in CardsEnum) {
      if ( isNaN(<any>prop) ) {
        response.push( prop );
      }
    }

    return response;
  }

  public getCardsKeys() {

    const response = [];

    for (const prop in CardsEnum) {
      if ( !isNaN(<any>prop) ) {
        response.push( prop );
      }
    }

    return response;
  }
}
