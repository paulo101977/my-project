import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { PaymentTypeEnum } from 'app/shared/models/reserves/payment-type-enum';
import { CurrencyExchangeResource } from 'app/shared/resources/currency-exchange/currency-exchange.resource';


@Injectable({
  providedIn: 'root'
})
export class BillingLauncherItemService {

  // create model
  private currentSelection: null;

  constructor(
    private translateService: TranslateService,
    private sessionParameterService: SessionParameterService,
    private currencyExchangeResource: CurrencyExchangeResource,
  ) {}

  public setCurrentSelection( item ) {
    this.currentSelection = item;
  }

  public getCurrentSelection() {
    return this.currentSelection;
  }

  public getCurrencyList(): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .currencyExchangeResource
        .getCurrencies()
        .subscribe( ({items}) => {
          resolve(items);
        });
    });
  }


  public translateFromEnum(key: PaymentTypeEnum) {
    let translate = '';
    switch ( key ) {
      case PaymentTypeEnum.Money:
        translate = 'text.cardMoney';
        break;
      case PaymentTypeEnum.Check:
        translate = 'text.cardCheck';
        break;
      case PaymentTypeEnum.Deposit:
        translate = 'text.cardDeposit';
        break;
      case PaymentTypeEnum.CreditCard:
        translate = 'text.cardCredit';
        break;
      case PaymentTypeEnum.Debit:
        translate = 'text.cardDebit';
        break;
    }

    return this.translateService.instant(translate);
  }

  public applyQuotationInValue(
      value: number,
      currencySelectedId: string,
      propertyCurrencyId: string,
      quotationList: Array<any>
  ): number {
    let total;
    if ( currencySelectedId === propertyCurrencyId ) {
      return value;
    }

    const currencySelectedExchangeItem = quotationList.find( item => item.currencyId == currencySelectedId );
    const propertyCurrencyExchangeItem = quotationList.find( item => item.currencyId == propertyCurrencyId );
    if ( currencySelectedExchangeItem  && propertyCurrencyExchangeItem ) {
      const currencySelectedExchangeRate = currencySelectedExchangeItem.exchangeRate;
      const propertyCurrencyExchangeRate = propertyCurrencyExchangeItem.exchangeRate;
      total = value / currencySelectedExchangeRate * propertyCurrencyExchangeRate;
      return +total.toFixed(2);
    }

    return value;
  }

  public getCurrencyId(propertyId): string {

    const currencyObj = this
      .sessionParameterService
      .getParameter(propertyId, ParameterTypeEnum.DefaultCurrency);

    if ( currencyObj ) {
      return currencyObj.propertyParameterValue;
    }

    return '';
  }
}
