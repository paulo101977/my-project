import { TestBed, inject } from '@angular/core/testing';

import { BillingAccountNotifierService } from './billing-account-notifier.service';

describe('BillingAccountNotifierService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BillingAccountNotifierService]
    });
  });

  it('should be created', inject([BillingAccountNotifierService], (service: BillingAccountNotifierService) => {
    expect(service).toBeTruthy();
  }));
});
