import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingAccountListComponent } from './components-container/billing-account-list/billing-account-list.component';
import { BillingAccountManageComponent } from './components-container/billing-account-manage/billing-account-manage.component';
import { BillingLauncherComponent } from './components-container/billing-launcher/billing-launcher.component';
import { BillingPosComponent } from './components-container/billing-pos/billing-pos.component';
import { BillingServiceComponent } from './components-container/billing-service/billing-service.component';
import { BillingCreditComponent } from './components-container/billing-credit/billing-credit.component';
import { BillingPosItemComponent } from './components-container/billing-pos-item/billing-pos-item.component';
import { CreditOrDebitContainerComponent } from './components-container/credit-or-debit-container/credit-or-debit-container.component';
import {
  BillingMoneyDebitCheckContainerComponent
} from './components-container/billing-money-debit-check-container/billing-money-debit-check-container.component';
import {
  BillingInvoiceNfceDownloadComponent
} from 'app/billing-account/components-container/billing-invoice-nfce-download/billing-invoice-nfce-download.component';


export const billingAccountRoutes: Routes = [
  { path: '', component: BillingAccountListComponent },
  { path: 'edit', component: BillingAccountManageComponent },
  { path: 'new', loadChildren: '../account-type/account-type.module#AccountTypeModule' },
  { path: 'statement', loadChildren: '../statement/statement.module#StatementModule' },
  { path: 'transfer', loadChildren: '../transfer/transfer.module#TransferModule' },
  { path: 'launcher', component: BillingLauncherComponent },
  { path: 'pos', component: BillingPosComponent },
  { path: 'pos/:id', component: BillingPosItemComponent },
  { path: 'service', component: BillingServiceComponent },
  {
    path: 'launch-credit',
    component: BillingCreditComponent,
    children: [
      {
        path: '',
        redirectTo: 'credit',
        pathMatch: 'full'
      },
      {
        path: 'credit',
        component: CreditOrDebitContainerComponent
      },
      {
        path: 'debit',
        component: CreditOrDebitContainerComponent
      },
      {
        path: 'money',
        component: BillingMoneyDebitCheckContainerComponent
      },
      {
        path: 'deposit',
        component: BillingMoneyDebitCheckContainerComponent
      },
      {
        path: 'check',
        component: BillingMoneyDebitCheckContainerComponent
      }
    ]
  },
  { path: 'invoice-nfce-download', component: BillingInvoiceNfceDownloadComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(billingAccountRoutes) ],
  exports: [ RouterModule ],
})
export class BillingAccountRoutingModule {}
