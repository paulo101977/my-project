import { createServiceStub } from '../../../../../../testing';
import { UndoSplitResource } from 'app/billing-account/undo-split/resources/undo-split.resource';
import { UndoSplitDto } from 'app/billing-account/undo-split/models/undo-split-dto';
import { Observable, of } from 'rxjs';

export const undoSplitResourceStub = createServiceStub(UndoSplitResource, {
    undoSplit(undoSplit: UndoSplitDto): Observable<any> {
        return of(null);
    }
});

