import { Injectable } from '@angular/core';
import { HttpService } from '../../../core/services/http/http.service';
import { Observable } from 'rxjs/index';
import { UndoSplitDto } from '../models/undo-split-dto';

@Injectable({ providedIn: 'root' })
export class UndoSplitResource {

  private url = 'BillingAccountItem/undopartial';

  constructor(private httpClient: HttpService) {
  }

  public undoSplit(undoSplit: UndoSplitDto): Observable<any> {
    return this.httpClient.post(this.url, undoSplit);
  }
}
