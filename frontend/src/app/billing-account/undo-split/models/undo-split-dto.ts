export class UndoSplitDto {
  id: string;
  propertyId: number;
  billingAccountItemId: Array<string>;
}
