import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { BillingAccountRoutingModule } from './billing-account-routing.module';
import { BillingAccountListComponent } from './components-container/billing-account-list/billing-account-list.component';
import { BillingAccountRouterComponent } from './components-container/billing-account-router/billing-account-router.component';
import { BillingAccountManageComponent } from './components-container/billing-account-manage/billing-account-manage.component';
import { BillingAccountEditHeaderComponent } from './components/billing-account-edit-header/billing-account-edit-header.component';
import { CardGuestBillingAccountComponent } from './components/card-guest-billing-account/card-guest-billing-account.component';
import { SidebarGuestBillingAccountComponent } from './components/sidebar-guest-billing-account/sidebar-guest-billing-account.component';
import { BillingAccountDetailComponent } from './components-container/billing-account-detail/billing-account-detail.component';
import { BillingAccountDetailHeaderComponent } from './components/billing-account-detail-header/billing-account-detail-header.component';
import {
  BillingAccountDebitCreditComponent
} from './components-container/billing-account-debit-credit/billing-account-debit-credit.component';
import { BillingAccountPostDebitComponent } from './components/billing-account-post-debit/billing-account-post-debit.component';
import { BillingAccountPostCreditComponent } from './components/billing-account-post-credit/billing-account-post-credit.component';
import { BillingAccountCardComponent } from './components/billing-account-card/billing-account-card.component';
import { BillingAccountBankCheckComponent } from './components/billing-account-bank-check/billing-account-bank-check.component';
import { BillingAccountReverseComponent } from './components/billing-account-reverse/billing-account-reverse.component';
import { BillingAccountDetailTableComponent } from './components/billing-account-detail-table/billing-account-detail-table.component';
import {
  BillingAccountDetailTableFooterComponent
} from './components/billing-account-detail-table-footer/billing-account-detail-table-footer.component';
import { OptionBillingAccountClosureComponent } from './components/option-billing-account-closure/option-billing-account-closure.component';
import { BillingAccountReleaseUhComponent } from './components/billing-account-release-uh/billing-account-release-uh.component';
import { BillingAccountDiscountComponent } from './components/billing-account-discount/billing-account-discount.component';
import {
  SelectBillingAccountForClosureComponent
} from './components/select-billing-account-for-closure/select-billing-account-for-closure.component';
import {
  SelectMultipleBillingAccountForClosureComponent
} from './components/select-multiple-billing-account-for-closure/select-multiple-billing-account-for-closure.component';
import { FiscalStatusNoteModalComponent } from './components/fiscal-status-note-modal/fiscal-status-note-modal.component';
import { ModalResendNfseComponent } from './components/modal-resend-nfse/modal-resend-nfse.component';
import { ModalCancelNfseComponent } from './components/modal-cancel-nfse/modal-cancel-nfse.component';
import { ModalErrorsNfseComponent } from './components/modal-errors-nfse/modal-errors-nfse.component';
import { ModalCancelReasonComponent } from './components/modal-cancel-reason/modal-cancel-reason.component';
import { SplitBillingAccountEntryModule } from './split-billing-account-entry/split-billing-account-entry.module';
import { UndoSplitModule } from './undo-split/undo-split.module';
import { BillingLauncherComponent } from './components-container/billing-launcher/billing-launcher.component';
import { LauncherCarouselComponent } from './components/launcher-carousel/launcher-carousel.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { BillingPosComponent } from './components-container/billing-pos/billing-pos.component';
import { BillingCreditComponent } from './components-container/billing-credit/billing-credit.component';
import { BillingServiceComponent } from './components-container/billing-service/billing-service.component';
import { PosCardComponent } from './components/pos-card/pos-card.component';
import { BillingPosItemComponent } from './components-container/billing-pos-item/billing-pos-item.component';
import { LauncherCardComponent } from './components/launcher-card/launcher-card.component';
import { PosItemSelectionCardComponent } from './components/pos-item-selection-card/pos-item-selection-card.component';
import { LauncherShoppingChartComponent } from './components/launcher-shopping-chart/launcher-shopping-chart.component';
import {
  LauncherShoppingChartItemRowComponent
} from './components/launcher-shopping-chart-item-row/launcher-shopping-chart-item-row.component';
import { CreditCardComponent } from './components/credit-card/credit-card.component';
import { CreditOrDebitContainerComponent } from './components-container/credit-or-debit-container/credit-or-debit-container.component';
import { InputMoneyComponent } from './components/input-money/input-money.component';
import {
  BillingMoneyDebitCheckContainerComponent
} from './components-container/billing-money-debit-check-container/billing-money-debit-check-container.component';
import { BillingItemEmptyCartComponent } from './components/billing-item-empty-cart/billing-item-empty-cart.component';
import { QrCodeScannerComponent } from './components/qr-code-scanner/qr-code-scanner.component';
import { CardServiceComponent } from './components/card-service/card-service.component';
import { ModalPersonHeaderComponent } from './components/modal-person-header/modal-person-header.component';
import { ModalEuropeFiscalDocumentComponent } from './components/modal-europe-fiscal-document/modal-europe-fiscal-document.component';
import {
  BillingInvoiceNfceDownloadComponent
} from './components-container/billing-invoice-nfce-download/billing-invoice-nfce-download.component';
import { ModalCreditNoteComponent } from './components/modal-credit-note/modal-credit-note.component';
import { ModalCreditNotePaymentTypeComponent } from './components/modal-credit-note-payment-type/modal-credit-note-payment-type.component';
import { DocumentModalConfirmComponent } from './components/document-modal-confirm/document-modal-confirm.component';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    SharedModule,
    BillingAccountRoutingModule,
    SplitBillingAccountEntryModule,
    UndoSplitModule,
    CarouselModule
  ],
  declarations: [
    BillingAccountRouterComponent,
    BillingAccountListComponent,
    BillingAccountManageComponent,
    BillingAccountEditHeaderComponent,
    CardGuestBillingAccountComponent,
    SidebarGuestBillingAccountComponent,
    BillingAccountDetailComponent,
    BillingAccountDetailHeaderComponent,
    BillingAccountDebitCreditComponent,
    BillingAccountPostDebitComponent,
    BillingAccountPostCreditComponent,
    BillingAccountCardComponent,
    BillingAccountBankCheckComponent,
    BillingAccountReverseComponent,
    BillingAccountDetailTableComponent,
    BillingAccountDetailTableFooterComponent,
    BillingAccountDiscountComponent,
    SelectBillingAccountForClosureComponent,
    OptionBillingAccountClosureComponent,
    BillingAccountReleaseUhComponent,
    SelectMultipleBillingAccountForClosureComponent,
    FiscalStatusNoteModalComponent,
    ModalResendNfseComponent,
    ModalCancelNfseComponent,
    FiscalStatusNoteModalComponent,
    ModalErrorsNfseComponent,
    ModalCancelReasonComponent,
    BillingLauncherComponent,
    LauncherCarouselComponent,
    BillingPosComponent,
    BillingCreditComponent,
    BillingServiceComponent,
    PosCardComponent,
    BillingPosItemComponent,
    LauncherCardComponent,
    PosItemSelectionCardComponent,
    LauncherShoppingChartComponent,
    LauncherShoppingChartItemRowComponent,
    CreditCardComponent,
    CreditOrDebitContainerComponent,
    InputMoneyComponent,
    BillingMoneyDebitCheckContainerComponent,
    BillingItemEmptyCartComponent,
    QrCodeScannerComponent,
    CardServiceComponent,
    ModalPersonHeaderComponent,
    ModalEuropeFiscalDocumentComponent,
    ModalCreditNoteComponent,
    ModalCreditNotePaymentTypeComponent,
    BillingInvoiceNfceDownloadComponent,
    DocumentModalConfirmComponent,
  ],
  exports: [
    BillingAccountDetailTableFooterComponent,
    OptionBillingAccountClosureComponent,
    SelectBillingAccountForClosureComponent,
    SelectMultipleBillingAccountForClosureComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class BillingAccountModule {}
