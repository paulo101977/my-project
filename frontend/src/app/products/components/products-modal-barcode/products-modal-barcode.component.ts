import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { CropperSettings, ImageCropperComponent } from 'ngx-img-cropper';

@Component({
  selector: 'app-products-modal-barcode',
  templateUrl: './products-modal-barcode.component.html',
})
export class ProductsModalBarcodeComponent implements OnInit {
  private item: string;
  @ViewChild('cropper', undefined) cropper: ImageCropperComponent;

  cropperSettings: CropperSettings;
  data: any;

  @Input()
  get selectedItem() {
    return this.item;
  }

  set selectedItem(val) {
    if (val) {
      this.item = val;
      this.loadImage();
    }
  }

  @Output() dataChange = new EventEmitter<string>();
  @Output() exitChange = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.configCropper();
  }


  public configCropper() {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.width = 550;
    this.cropperSettings.croppedWidth = 550;
    this.cropperSettings.croppedHeight = 100;
    this.cropperSettings.canvasWidth = 550;
    this.cropperSettings.canvasHeight = 350;

    this.data = {};
  }

  public loadImage() {
    const image: any = new Image();

    if (this.selectedItem != '') {
      image.src = this.selectedItem;
      image.onload = () => {
        this.cropper.setImage(image);
      };
    }
  }

  public cancelRequest() {
    this.exitChange.emit();
  }

  public changeData(): void {
    this.dataChange.emit(this.selectedItem);
  }
}
