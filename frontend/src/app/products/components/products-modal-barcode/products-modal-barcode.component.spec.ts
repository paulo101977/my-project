import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductsModalBarcodeComponent } from './products-modal-barcode.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('ProductsModalBarcodeComponent', () => {
  let component: ProductsModalBarcodeComponent;
  let fixture: ComponentFixture<ProductsModalBarcodeComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsModalBarcodeComponent ],
      imports: [
        TranslateTestingModule,
      ],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ProductsModalBarcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show input', () => {
    component.selectedItem = 'data:image/';
    component.loadImage();
    expect(component.selectedItem).toEqual('data:image/');
  });
});
