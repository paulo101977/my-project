import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ProductsResource } from '../../../shared/resources/products/products.resource';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';


@Component({
  selector: 'products-modal-group-delete',
  templateUrl: './products-modal-group-delete.component.html'
})
export class ProductsModalGroupDeleteComponent implements OnInit {

  // TODO: create or add model
  private row;
  private readonly GROUP_MODAL = 'product-group-delete-modal';

  // emit on save or edit
  @Output() saveOrEditCompleted = new EventEmitter();

  // button dependencies
  public buttonCancelModalConfig: ButtonConfig;
  public buttonExcludeModalConfig: ButtonConfig;

  constructor(
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private productResource: ProductsResource,
    private toasterEmitService: ToasterEmitService,
  ) {}

  private setButtonConfig() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'reason-cancel-modal',
      this.toggleModal ,
      'action.no',
      ButtonType.Secondary,
    );
    this.buttonExcludeModalConfig = this.sharedService.getButtonConfig(
      'reason-save-modal',
      this.exclude ,
      'action.yes',
      ButtonType.Primary,
      null,
      false,
    );
  }

  private exclude = () => {
    this
      .productResource
      .deleteProductGroup(this.row)
      .subscribe( item => {
        this.saveOrEditCompleted.emit(item);
        this
          .toasterEmitService
          .emitChange(SuccessError.success, 'text.groupProductRemoveSuccess');
        this.toggleModal();
    }, () => {
        this.toggleModal();
    });
  }

  ngOnInit() {
    this.setButtonConfig();
  }

  public toggleModal = ($event?) => {
    this.modalEmitToggleService.emitToggleWithRef(this.GROUP_MODAL);

    this.row = $event;
  }
}
