import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { ProductGroups } from '../../models/products/products';
import { ProductsResource } from '../../../shared/resources/products/products.resource';
import { ProductsModalGroupEditAddComponent } from '../products-modal-group-edit-add/products-modal-group-edit-add.component';
import { ProductsModalGroupDeleteComponent } from '../products-modal-group-delete/products-modal-group-delete.component';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-products-new-group',
  templateUrl: './products-new-group.component.html'
})
export class ProductsNewGroupComponent implements OnInit {

  // header
  configHeaderPage: ConfigHeaderPageNew;

  // table
  public columns: Array<any>;
  public pageItemsList: Array<ProductGroups>;
  public pageItemsListBack: Array<ProductGroups>;
  public itemsOption: Array<any>;

  // modal ref
  @ViewChild('modalAddEdit') modalAddEdit: ProductsModalGroupEditAddComponent;
  @ViewChild('modalDelete') modalDelete: ProductsModalGroupDeleteComponent;

  constructor(
    private produtcResource: ProductsResource,
    private toastEmitter: ToasterEmitService,
    private translateService: TranslateService,
  ) { }

  private setColumns() {
    this.columns = [{
      name: 'productModule.listPage.columns.description',
      prop: 'categoryName'
    },
    {
      name: 'label.groupFix',
      prop: 'groupFixedName'
    }];
  }

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: false,
      keepTitleButton: true,
      callBackFunction: this.openModalAdd
    };
  }

  public openModalAdd = () => {
    this.modalAddEdit.mode = 'save';
    this.modalAddEdit.toggleModalSave();
  }

  public search(event: any) {
    const { value } = event.target;
    if (typeof value === 'string' && value != '') {
      const regexp = new RegExp(value, 'i');
      this.pageItemsList = this.pageItemsListBack.filter(function (v) {
        return regexp.test(v.groupFixedName)
          || regexp.test(v.categoryName);
      });
    } else {
      // the original resource
      this.pageItemsList = this.pageItemsListBack;
    }
  }

  public updateStatus($event) {
    const group = <ProductGroups>$event;
    const { isActive } = group;

    this.produtcResource.editToogleGroup(group.id).subscribe(
      item => {
        this.showToastStatus(SuccessError.success, isActive);
      },
      err => {
        this.showToastStatus(SuccessError.success, isActive);
      }
    );
  }

  public showToastStatus(typeMessage: SuccessError, isActive) {
    let message = '';

    if (typeMessage == SuccessError.success && isActive) {
      message = 'label.groupStatusActivateSuccess';
    }
    if (typeMessage == SuccessError.success && !isActive) {
      message = 'label.groupStatusInactivateSuccess';
    }
    if (typeMessage == SuccessError.error && isActive) {
      message = 'label.groupStatusActivateError';
    }
    if (typeMessage == SuccessError.error && !isActive) {
      message = 'label.groupStatusInactivateError';
    }

    this
      .toastEmitter
      .emitChangeTwoMessages(typeMessage, this.translateService.instant(message), '');
  }

  public rowItemClicked($event) {
    this.toggleModalToEditAction($event);
  }

  public loadData(): void {
    this
      .produtcResource
      .getAllGroups()
      .subscribe(( { items } ) => {
        this.pageItemsList = items;
        this.pageItemsListBack = this.pageItemsList;
    });
  }

  private toggleModalToEditAction = ($event) => {
    this.modalAddEdit.mode = 'edit';
    this.modalAddEdit.toggleModalEdit($event);
  }

  private toggleModalToExcludeAction = ($event) => {
    this.modalDelete.toggleModal($event);
  }

  ngOnInit() {
    this.setHeaderConfig();

    this.setColumns();

    this.loadData();

    // datable submenu option config
    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.toggleModalToEditAction },
      { title: 'commomData.delete', callbackFunction: this.toggleModalToExcludeAction },
    ];
  }

}
