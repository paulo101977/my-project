import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ProductsResource } from 'app/shared/resources/products/products.resource';

@Component({
  selector: 'app-products-modal-delete',
  templateUrl: './products-modal-delete.component.html'
})
export class ProductsModalDeleteComponent implements OnInit {

  private row;
  private readonly PRODUCT_MODAL = 'product-delete-modal';

  @Output() deleteCompleted = new EventEmitter();

  // button dependencies
  public buttonCancelModalConfig: ButtonConfig;
  public buttonExcludeModalConfig: ButtonConfig;

  constructor(
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private productResource: ProductsResource,
  ) { }

  private setButtonConfig() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'reason-cancel-modal',
      this.toggleModal ,
      'action.no',
      ButtonType.Secondary,
    );
    this.buttonExcludeModalConfig = this.sharedService.getButtonConfig(
      'reason-save-modal',
      this.exclude ,
      'action.yes',
      ButtonType.Primary,
      null,
      false,
    );
  }


  private exclude = () => {
    if ( this.row ) {
      const { id } = this.row;
      this
        .productResource
        .deleteProduct(id)
        .subscribe(item => {
          this.deleteCompleted.emit(item);
          this.toggleModal(null);
        });
    }
  }

  ngOnInit() {
    this.setButtonConfig();
  }

  public toggleModal = ($event) => {
    this.modalEmitToggleService.emitToggleWithRef(this.PRODUCT_MODAL);

    this.row = $event;
  }

}
