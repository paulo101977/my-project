import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';
import { ProductsNewEditComponent } from './products-new-edit.component';

// common and necessary imports
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { DATA, POS_FORM_LIST } from 'app/products/mock-data';
import {configureTestSuite} from 'ng-bullet';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { productsResourceStub } from 'app/shared/resources/products/testing';
import { barCodeServiceStub, productsServiceStub } from 'app/products/services';
import { modalEmitServiceStub } from 'app/shared/services/shared/testing/modal-emit-service';
import { assetsServiceStub } from 'app/core/services';
import { createAccessorComponent, currencyFormatPipe } from '../../../../../testing';


const fillItems = () => {
  const myItems = [];

  for (let i = 0; i < 10; i++) {
    myItems.push({
      isActive: i % 2 == 0
    });
  }

  return myItems;
};

const createArray = () => {
  const pos = [];

  for (let i = 0; i < 20; i++) {
    const id = i * Math.floor(1 + Math.random() * 1000);
    pos.push({
      id,
      billingItemId: id,
      isActive: i % 3 === 0,
      billingItemName: `abc${id}`,
      description: `${id}abc`,
      unitPrice: i
    });
  }

  return pos;
};

const posList = [
  {
    billingItemCategoryId: 11,
    billingItemCategoryName: 'Restaurante',
    billingItemName: 'Teste Ricardo',
    billingItemTypeId: 4,
    id: '75',
    isActive: false,
    isDeleted: false,
    propertyId: '1',
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    unitPrice: 7.77
  },
  {
    billingItemCategoryId: 11,
    billingItemCategoryName: 'Telecomunicações',
    billingItemName: 'Teste Ricardo 2',
    billingItemTypeId: 4,
    id: '75',
    isActive: false,
    isDeleted: false,
    propertyId: '1',
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    unitPrice: 7.77
  }
];

const thxError = createAccessorComponent('thx-error');

const items = fillItems();

describe('ProductsNewEditComponent', () => {
  let component: ProductsNewEditComponent;
  let fixture: ComponentFixture<ProductsNewEditComponent>;
  let getAllPos;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductsNewEditComponent,
        currencyFormatPipe,
        thxError,
      ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        sharedServiceStub,
        commomServiceStub,
        toasterEmitServiceStub,
        productsResourceStub,
        productsServiceStub,
        modalEmitServiceStub,
        assetsServiceStub,
        barCodeServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(ProductsNewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn<any>(component['productsResource'], 'getProductById').and.returnValue(of((DATA[POS_FORM_LIST])));
    spyOn<any>(component['productsResource'], 'searchNcm').and.returnValue(of({items}));
    spyOn<any>(component['productsResource'], 'getAllGroups').and.returnValue(of({items}));
    spyOn<any>(component['productsResource'], 'getAllUnitys').and.returnValue(of({items}));
    spyOn<any>(component['router'], 'navigate').and.callFake(() => {});
    getAllPos = spyOn<any>(component['productsResource'], 'getAllPos').and.returnValue(of({items}));
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call init functions', () => {
    spyOn<any>(component, 'setFormProducts');
    spyOn(component, 'setColumns');

    component.ngOnInit();

    expect(component['setFormProducts']).toHaveBeenCalled();
    expect(component['setColumns']).toHaveBeenCalled();
  });

  it('should call loadAllPos method', fakeAsync(() => {
    const newItems = fillItems();
    let billingItens;
    getAllPos.and.returnValue(of({items: newItems}));

    component['loadAllPos']();
    tick();

    billingItens = component.formProduct.get('billingItens').value;

    expect(component['posList']).toEqual(newItems);
    expect(component['posListBack']).toEqual(newItems);
    expect(component['toInsertInTableBillingItens'].length).toEqual(5);
    expect(billingItens).toEqual(newItems);
  }));

  it('should call updateStatus method', fakeAsync(() => {

    // fill the posList
    component['posList'] = createArray();

    // get a range of elements and fill insertedItems
    component['toInsertInTableBillingItens'] = component['posList'].slice(3, 7);

    let item = null;
    let billingItens;
    let finded;

    // update status of item with index 4
    item = component['posList'][4];
    item.isActive = true;

    // update the array status for the selected item
    component.updateStatus(item);

    // get the array from form
    billingItens = component.formProduct.get('billingItens').value;

    // find the item changed
    finded = billingItens.find( bItem => bItem['id'] === item['id']);

    expect(finded.isActive).toEqual(true);
  }));


  it('should call search method', fakeAsync(() => {

    const first  = { target: { value : 'Ricardo' }};
    const second  = { target: { value : 'Restaurante' }};
    const third  = { target: { value : 'Telecomunicações' }};
    const four  = { target: { value : 'blabla' }}; // not find it
    const empty = { target: { value : '' }};

    component['posList'] = posList;
    component['posListBack'] = posList;

    component['search'](first);
    expect(component['posList'].length).toEqual(2);

    component['search'](second);
    expect(component['posList'].length).toEqual(1);

    component['search'](third);
    expect(component['posList'].length).toEqual(1);

    component['search'](four);
    expect(component['posList'].length).toEqual(0);

    component['search'](empty);
    expect(component['posList'].length).toEqual(2);
  }));

  it('should call loadProductById method', fakeAsync(() => {

    component['posList'] = createArray();

    component['loadProductById']('1234');
    tick();

    expect(component['formProduct'].value).toEqual((DATA[POS_FORM_LIST]));
  }));

  it('should ncmCode is required', () => {
    component.configCountry = false;
    component['setFormProducts']();
    component['formProduct'].setValue(DATA[POS_FORM_LIST]);

    expect(component.formProduct.status).toEqual('INVALID');
  });

  it('should ncmCode is not required', () => {
    component.configCountry = true;
    component['setFormProducts']();

    component['formProduct'].setValue(DATA[POS_FORM_LIST]);

    expect(component.formProduct.status).toEqual('VALID');
  });
});
