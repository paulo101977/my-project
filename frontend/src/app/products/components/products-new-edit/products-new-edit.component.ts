import {Component, OnInit, TemplateRef, ViewChild, ElementRef} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ConfigHeaderPageNew
} from 'app/shared/components/header-page-new/config-header-page-new';
import { AutocompleteConfig } from 'app/shared/models/autocomplete/autocomplete-config';
import { ProductsResource } from 'app/shared/resources/products/products.resource';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { TranslateService } from '@ngx-translate/core';
import {InputKeys, Product} from '@app/products/models/products/products';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { PosProduct, ProductGroups, ProductUnity, NCM } from 'app/products/models/products/products';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { RightButtonTop } from '@app/shared/models/right-button-top';
import { AssetsService } from '@app/core/services/assets.service';
import { ModalEmitToggleService } from '@app/shared/services/shared/modal-emit-toggle.service';
import { BarCodeService } from 'app/products/services/barcode.services';
import {ProductsService} from 'app/products/services/products.service';


@Component({
  selector: 'app-products-new-edit',
  templateUrl: './products-new-edit.component.html',
  styles: [
    `
      /* TODO: remove the ::ng-deep */
      :host ::ng-deep .datatable-body-cell-label .input-container{
        margin-top: -13px;
      }

      :host ::ng-deep .datatable-body-cell-label .row-edit-pencil{
        float: right;
        width: 40px;
      }

      :host ::ng-deep .datatable-body-cell-label .row-edit-mode{
        float: right;
        width: 40px;
      }
    `
  ]
})
export class ProductsNewEditComponent implements OnInit {

  private propertyId;
  public ncmAutocompleteConfig: AutocompleteConfig = {
    dataModel: 'ncmId',
    callbackToSearch: (search: string) => this.searchNcm(search),
    callbackToGetSelectedValue: (selectedItem: any) => this.selectNcm(selectedItem),
    fieldObjectToDisplay: 'code',
    displayIcon: false,
    extraClasses: 'thf-input thf-u-padding--1',
    resultList: [],
    placeholder: null,
    forceSelection: true
  };
  public formProduct: FormGroup;
  public configCountry =  false;

  // header
  configHeaderPage: ConfigHeaderPageNew;

  // optionsCurrencyMask
  public optionsCurrencyMask: any;

  public topButtonsListConfig: Array<RightButtonTop>;

  public pageTitle = 'productModule.createEdit.titleInsert';
  public mode = 'insert';
  public itemSelected = '';

  // datatable
  @ViewChild('editfield') editfield: TemplateRef<any>;
  @ViewChild('pricefield') pricefield: TemplateRef<any>;
  @ViewChild('inputBarCode') inputBarCode: ElementRef;
  public columns: Array<any>;

  // group selector
  public groupList: Array<InputKeys> = [];
  public unityList: Array<InputKeys> = [];


  public posList: Array<PosProduct>;
  public posListBack: Array<PosProduct>;
  public toInsertInTableBillingItens: Array<PosProduct>;
  public ncmList: Array<NCM>;

  public readonly EDIT = 'edit';
  public readonly ADD = 'add';

  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private productsResource: ProductsResource,
    private toastEmitter: ToasterEmitService,
    private translateService: TranslateService,
    private commomService: CommomService,
    private sharedService: SharedService,
    private assetsService: AssetsService,
    private modalEmitToggleService: ModalEmitToggleService,
    private barCodeService: BarCodeService,
    private productsService: ProductsService
  ) {}

  public searchNcm(search: any) {
    if (search.query == null || search.query.length < 3) {
      return;
    }

    this.productsResource.searchNcm(search.query)
      .subscribe((response) => {
        this.ncmAutocompleteConfig.resultList = [...response.items];
        this.ncmList = [...response.items];
      });
  }

  public selectNcm(selectedItem) {
    this.formProduct.patchValue({
      ncmCode: selectedItem.code.trim(),
      ncmId: selectedItem.id
    });
  }

  public setColumns() {
    this.columns = [{
      name: 'label.pos',
      prop: 'billingItemName'
    },
    {
      name: 'label.group',
      prop: 'billingItemCategoryName'
    },
    {
      name: 'label.unitPrice',
      cellTemplate: this.pricefield
    },
    {
      cellTemplate: this.editfield
    }];
  }

  public editRow(row, index) {
    row.isEdit = true;
    row.before = row.unitPrice;
  }

  public cancelRow(row, index) {
    const before = row.before;

    row.isEdit = false;
    row.unitPrice = before;
  }

  public confirmRow(row, index) {
    const { billingItens } = this.formProduct.value;
    row.isEdit = false;

    if ( billingItens ) {
      if ( billingItens[index] ) {
        billingItens[index].unitPrice = row.unitPrice;
      } else {
        billingItens.push(row);
      }

      this.formProduct.patchValue(
        billingItens
      );
    }
  }

  private setFormProducts() {
    this.formProduct = this.formBuilder.group({
      id: '',
      code: [null, [Validators.required]],
      productName: [null, [Validators.required]],
      productCategoryId: [null, [Validators.required]],
      unitMeasurementId: [null, [Validators.required]],
      ncmId: this.configCountry ? [null] : [null, [Validators.required]],
      ncmCode: this.configCountry ? [null] : [null, [Validators.required]],
      barCode: [null, [Validators.required]],
      isActive: true,
      billingItens: [ null ]
    });

    this.formProduct.valueChanges.subscribe( values => {
      this.buttonSaveConfig.isDisabled = !this.formProduct.valid;
    });
  }

  // load on edit mode
  public loadProductById(id: string): void {
    this.productsResource
      .getProductById(id)
      .subscribe(async (item) => {
        if (item) {
          const { billingItens } = item;

          this.formProduct.patchValue(item);


          // override associated POS values if it exist
          if ( billingItens && billingItens.length && this.posList) {

            // retrieve data from server (diff billingItens from posList)
            this.posList.forEach((current, index) => {
              const finded = billingItens
                .find(billingItem => current.id === billingItem.billingItemId );

              // setup the current finded item at array
              if ( finded ) {
                this.posList[index]['unitPrice'] = finded.unitPrice;
                this.posList[index]['isActive'] = finded.isActive;
              } else {
                this.posList[index]['isActive'] = false;
              }

            });
          } else if ( this.posList ) {
            this.posList.forEach( pos => pos.isActive = false );
          }
        }
    });
  }

  async loadGroups(): Promise<ProductGroups[]> {
    return new Promise<ProductGroups[]>((resolve) => {
      this
        .productsResource
        .getAllGroups()
        .subscribe(({items}) => resolve(<ProductGroups[]>items));
    });
  }

  async loadUnits(): Promise<ProductUnity[]> {
    return new Promise<ProductUnity[]>((resolve) => {
      this
        .productsResource
        .getAllUnitys()
        .subscribe(({ items }) => resolve(<ProductUnity[]>items));
    });
  }

  async loadAllPos(id?): Promise<PosProduct[]> {
    return new Promise<any>((resolve) => {
      this
        .productsResource
        .getAllPos()
        .subscribe(({items}) => {
          if (items) {
            this.toInsertInTableBillingItens = (
              <Array<PosProduct>>items.filter(item => item.isActive)
            );
          }

          this.posList = items;
          if (id === undefined) {
            this.posList.forEach( p => p.isActive = false );
          }
          this.posListBack = this.posList;

          this.formProduct.patchValue({
            billingItens: items
          });

          resolve(items);
        });
    });
  }

  public mapToGroup (items: Array<ProductGroups>) {
    this.groupList = [];

    if ( items ) {
      this.groupList =
        this
          .commomService
          .toOption(items, 'id', 'categoryName');
    }
  }

  public mapToUnity (items: Array<ProductUnity>) {
    this.unityList = [];

    if ( items ) {
      this.unityList =
        this
          .commomService
          .toOption(items, 'id', 'name');

    }
  }

  public updateStatus($event) {
    const { toInsertInTableBillingItens, posList } = this;
     const billingItens = toInsertInTableBillingItens;

    if ( posList ) {
      this.toInsertInTableBillingItens = billingItens;
      this.formProduct.patchValue({
        billingItens
      });
    }
  }

  public search(event: any) {
    const { value } = event.target;

    if ( value !== '') {
      const regex = new RegExp(value, 'i');

      if ( this.posList ) {
        this.posList = this.posListBack.filter( res => {
          const { billingItemName, billingItemCategoryName } = res;
          return regex.test(billingItemName)
            || regex.test(billingItemCategoryName);
        });
      }
    } else {
      this.posList = this.posListBack;
    }
  }

  // empty function
  public gotoNewPos = () => {

  }

  public cancelFormAction = () => {
    history.back();
  }

  public navigateBack() {
    this.router.navigate(
      [
        `/p/${this.propertyId}/products/list`
      ],
      { relativeTo: this.route}
    );
  }

  // Utils
  public showToastStatus(modify) {
    const typeMessage = SuccessError.success;

    let message = 'label.productEditedSuccess';

    if (modify === this.ADD) {
      message = 'label.productInsertSuccess';
    }

    this
      .toastEmitter
      .emitChange(typeMessage, this.translateService.instant(message));
  }

  public showToastErrorPrice() {
    this
      .toastEmitter
      .emitChange(SuccessError.error, this.translateService.instant('label.withoutProductPrice'));
  }

  public removeIdFromDtoAndReturnItObject() {
    const billingItens = this.formProduct.get('billingItens');
    let temp = [];

    if ( billingItens ) {
      const { value } = billingItens;
      const clone = JSON.parse(JSON.stringify(value));
      temp = clone.map( item  => {
        const id = item.id;

        // don't conflict with backend
        delete item['id'];

        return {
          billingItemId: id,
          ...item
        };
      });
    }

    this.formProduct.get('billingItens').patchValue(temp);

    return temp;
  }

  public saveFormAction = () => {
    const { mode } = this;
    const { value } = this.formProduct;

    const billingItens = this.removeIdFromDtoAndReturnItObject();

    value.billingItens = billingItens.filter(x => x.isActive);

    delete value['ncmCode'];
    if (mode === this.EDIT) {
      this
        .productsResource
        .setProduct(value)
        .subscribe(
        () => {
          this.showToastStatus(this.EDIT);
          this.navigateBack();
      });

    } else {
      // remove to not conflict with backend
      delete value['id'];

      this
        .productsResource
        .addProduct(value)
        .subscribe( () => {
          this.showToastStatus(this.ADD);
          this.navigateBack();
      });
    }
  }

  public checkIsValid() {
    return !this.formProduct.valid;
  }

  setInputMask() {
    // the input number mask
    this.optionsCurrencyMask = {
      prefix: '',
      thousands: '.',
      decimal: ',',
      align: 'left',
      allowZero: true,
      allowNegative: false
    };
  }

  setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'cancel-button',
      this.cancelFormAction ,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'save-button',
      this.saveFormAction ,
      'commomData.confirm',
      ButtonType.Primary
    );
  }

  public setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  private setTopButtonsListConfig(): void {
    this.topButtonsListConfig = [];

    const topButtonBarCode = new RightButtonTop();
    topButtonBarCode.iconAlt = 'label.scan';
    topButtonBarCode.title = 'label.scan';
    topButtonBarCode.callback = this.barcodeClick;
    topButtonBarCode.iconDefault = this.assetsService.getImgUrlTo('codbarras.svg');

    this.topButtonsListConfig.push(topButtonBarCode);
  }

  private barcodeClick = () => {
    this.inputBarCode.nativeElement.click();
  }

  public imageBarCodeUpload($event) {
    const file: File = $event.target.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (loadEvent: any) => {
        this.itemSelected = loadEvent.target.result;
        this.inputBarCode.nativeElement.value = '';

        this.toggleModalBarcode();
    };

    myReader.readAsDataURL(file);
  }

  public translateBarcode() {
    this.barCodeService.translateImageBarCode(<Product>{
      imageBase64: this.itemSelected
    }).subscribe((res: Product) => {
      this.formProduct.patchValue({
        'barCode': res.barCode
      });

      this.toggleModalBarcode();
    });
  }

  public cancelModalBarcode() {
    this
      .modalEmitToggleService
      .emitToggleWithRef('products-modal-barcode');
  }

  public toggleModalBarcode() {
    this
      .modalEmitToggleService
      .emitToggleWithRef('products-modal-barcode');
  }

  ngOnInit() {
    this.configCountry = this.productsService.showLocationPT();
    this.setFormProducts();

    this.setColumns();
    this.setConfigHeaderPage();
    this.setTopButtonsListConfig();
    this.setButtonConfig();

    this.route.params.subscribe( async params => {
      const { mode, productId, property } = params;
      this.mode = mode;
      this.propertyId = +property;

      // the input number mask
      this.setInputMask();

      // load all selectors data
      this.mapToGroup(await this.loadGroups());
      this.mapToUnity(await  this.loadUnits());

      // load pos
      await this.loadAllPos(productId);

      if ( productId ) {
        this.mode = 'edit';
        this.pageTitle =  'title.productTitleEdit';
        this.loadProductById(productId);
      }
    });
  }
}
