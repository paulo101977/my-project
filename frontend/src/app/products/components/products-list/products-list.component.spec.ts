// TODO: create test

import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ProductsListComponent } from './products-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { Product } from 'app/products/models/products/products';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { productsResourceStub } from 'app/shared/resources/products/testing';
import { loadPageEmitServiceStub } from '../../../../../testing';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';

const target = [];

describe('ProductsListComponent', () => {
  let component: ProductsListComponent;
  let fixture: ComponentFixture<ProductsListComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsListComponent ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
      ],
      providers: [
        productsResourceStub,
        loadPageEmitServiceStub,
        toasterEmitServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ProductsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn<any>(component['router'], 'navigate').and.callFake( () => {} );

    spyOn<any>(component['productsResource'], 'getAllProducts').and.returnValue(of({items: target}));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call init functions', () => {
    spyOn<any>(component, 'setHeaderConfig');
    spyOn<any>(component, 'setColumnsName');

    component.ngOnInit();

    expect(component['setHeaderConfig']).toHaveBeenCalled();
    expect(component['setColumnsName']).toHaveBeenCalled();

  });

  it('should call load data method', fakeAsync(() => {

    component['loadData']();
    tick();

    expect(component['pageItemsList']).toEqual(target);
  }));

  it('should test the search method and your return', () => {
    const searched = { target: { value : 'test' }};
    const items = [
      {
        'productName': 'info-test-0',
        'categoryName': 'my-category'
      },
      {
        'productName': 'tes999t',
        'categoryName': 'my-test-correctly'
      },
      {
        'productName': '111111',
        'categoryName': '222222'
      },
      {
        'productName': '5555',
        'categoryName': '123test'
      }
    ];

    component.ngOnInit();
    component['pageItemsListBack'] = <Product[]>items;
    component['search'](searched);

    expect(component['pageItemsList'].length).toBe(3);
  });
});
