import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { ProductsResource } from 'app/shared/resources/products/products.resource';
import { LoadPageEmitService } from 'app/shared/services/shared/load-emit.service';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { Product } from '../../models/products/products';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { TranslateService } from '@ngx-translate/core';
import { ProductsModalDeleteComponent } from 'app/products/components/products-modal-delete/products-modal-delete.component';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html'
})
export class ProductsListComponent implements OnInit {

  // header
  public configHeaderPage: ConfigHeaderPageNew;

  // table columns
  columns: Array<any>;

  private propertyId: number;
  private result: string;

  // the table data
  public pageItemsList: Array<Product>;
  public pageItemsListBack: Array<Product>;

  @ViewChild('modalDelete') modalDelete: ProductsModalDeleteComponent;
  @ViewChild('rowAssociatedPOS') rowAssociatedPOS: TemplateRef<any>;
  public itemsOption: Array<any>;

  // table methods:
  private setColumnsName(): void {
    this.columns = [{
      name: 'label.code',
      prop: 'code',
    },
    {
      name: 'label.description',
      prop: 'productName',
    },
    {
      name: 'label.group',
      prop: 'categoryName',
    },
    {
      name: 'label.posAssociated',
      prop: 'associatedPos',
      cellTemplate: this.rowAssociatedPOS
    }];
  }


  constructor(
    private productsResource: ProductsResource,
    private loaderService: LoadPageEmitService,
    private toastEmitter: ToasterEmitService,
    private translateService: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: false,
      keepTitleButton: true,
      callBackFunction: this.gotoNewProduct
    };
  }


  public deleteCompleted(row) {
    this
      .toastEmitter
      .emitChange(
        SuccessError.success,
        this.translateService.instant('text.productRemoveSuccess'));

    this.loadData();
  }

  public rowItemClicked(row): void {
    this.gotoEdit(row);
  }

  public updateStatus($event): void {
    const product = <Product>$event;
    const { isActive } = product;

    this.productsResource.setToggleProduct(product.id).subscribe(
      () => {
        this.showToastStatus(isActive);
      }
    );
  }

  // Utils
  public showToastStatus(modify: boolean) {
    const typeMessage = SuccessError.success;

    let message = 'label.productStatusInactivateSuccess';

    if ( modify ) {
      message = 'label.productStatusActivateSuccess';
    }

    this
      .toastEmitter
      .emitChange(typeMessage, this.translateService.instant(message));
  }

  public search(item: any) {

    const { value } = item.target;

    if (value != '') {
      const regexp = new RegExp(value, 'i');
      this.pageItemsList = this
          .pageItemsListBack
          .filter(function (v) {
            return regexp.test(v.productName)
              || regexp.test(v.categoryName)
              || regexp.test(v.code);
      });
    } else {
      // the original resource
      this.pageItemsList = this.pageItemsListBack;
    }
  }

  // modal methods
  public gotoNewProduct = () => {
    this.router.navigate(
      [`/p/${this.propertyId}/products/product`],
      { relativeTo: this.route }
    );
  }

  public gotoEdit = (row) => {
    const { id } = row;

    this.router.navigate(
      [`/p/${this.propertyId}/products/product`, id],
      { relativeTo: this.route}
    );
  }


  private loadData(): void {
    this
      .loaderService
      .emitChange(ShowHide.show);

    this
      .productsResource
      .getAllProducts()
      .subscribe( ({items}) =>  {
        if ( items ) {
          items.map( item => {
            if ( item.billingItens ) {
              const { billingItens } = item;

              item.associatedPos = '';

              if ( billingItens.length > 1) {
                item.associatedPos = billingItens.reduce((previus, curr) => {
                  if (curr.isActive) {
                    if (!curr.billingItemName) {
                      return previus;
                    } else if (previus) {
                      return `${previus}, ${curr.billingItemName}`;
                    } else {
                      return curr.billingItemName;
                    }
                  } else if (!curr.isActive && previus) {
                    return previus;
                  }
                }, '');
              } else if ( billingItens.length === 1 ) {
                if (billingItens[0].isActive) {
                  item.associatedPos = billingItens[0].billingItemName;
                }
              }
            }
          });
        }

        this.pageItemsList = items;
        this.pageItemsListBack = this.pageItemsList;
        this.loaderService.emitChange(ShowHide.hide);
      });
  }

  public callExclude = ($event) => {
    this.modalDelete.toggleModal($event);
  }

  ngOnInit() {

    // init head config
    this.setHeaderConfig();

    // table
    this.setColumnsName();


    this.route.params.subscribe( ({ property, result }) => {
      this.propertyId = +property;

      this.result = result ? result : '';

      this.loadData();

      // datable submenu option config
      this.itemsOption = [
        { title: 'commomData.edit', callbackFunction: this.gotoEdit },
        { title: 'commomData.delete', callbackFunction: this.callExclude },
      ];
    });
  }

}
