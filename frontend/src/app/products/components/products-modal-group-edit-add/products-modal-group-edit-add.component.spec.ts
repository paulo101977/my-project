// TODO: create test

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ProductsModalGroupEditAddComponent } from './products-modal-group-edit-add.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { productsResourceStub } from 'app/shared/resources/products/testing';
import { createAccessorComponent, imgCdnStub } from '../../../../../testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const thxError = createAccessorComponent('thx-error');

// TODO: improve the test when backend's integration
describe('ProductsModalGroupEditAddComponent', () => {
  let component: ProductsModalGroupEditAddComponent;
  let fixture: ComponentFixture<ProductsModalGroupEditAddComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductsModalGroupEditAddComponent,
        thxError,
        imgCdnStub,
      ],
      imports: [
        TranslateTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        sharedServiceStub,
        commomServiceStub,
        modalToggleServiceStub,
        toasterEmitServiceStub,
        productsResourceStub,
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    fixture = TestBed.createComponent(ProductsModalGroupEditAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn<any>(component, 'getGroupFixList').and.callFake( f => f );
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call init functions', () => {
    spyOn(component, 'setFormModal');
    spyOn(component, 'setButtonConfig');
    spyOn(component, 'loadIconList');

    component.ngOnInit();

    expect(component['setFormModal']).toHaveBeenCalled();
    expect(component['setButtonConfig']).toHaveBeenCalled();
    expect(component['loadIconList']).toHaveBeenCalled();
    expect(component['getGroupFixList']).toHaveBeenCalled();
  });

  it('should check if clean form', () => {
    spyOn<any>(component, 'clearForm');

    component.formModalNew.patchValue({
      id: '1',
      standardCategoryId: '2',
      categoryName: '3',
      iconName: '4',
      isActive: false
    });

    expect(component.formModalNew.get('categoryName').value).toBe('3');

    component.ngOnInit();

    component['clearForm']();

    expect(component['clearForm']).toHaveBeenCalled();

    expect(component.formModalNew.get('categoryName').value).toBe(null);
  });
});
