import {Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { ProductsResource } from '../../../shared/resources/products/products.resource';
import { ProductGroups } from '../../models/products/products';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { GroupsService } from 'app/products/services/groups.services';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';

// TODO: remove all ::ng-deep
@Component({
  selector: 'products-modal-group-edit-add',
  templateUrl: './products-modal-group-edit-add.component.html',
  styles: [`
    :host ::ng-deep .icon-container {
      max-height: 170px;
      overflow: scroll;
    }

    :host ::ng-deep .thf-button{
      outline: none !important;
    }

    :host ::ng-deep .selected-icon{
      border: 2px solid var(--color-blue) !important;
    }
  `]
})

export class ProductsModalGroupEditAddComponent implements OnInit {

  // emit on save or edit
  @Output() saveOrEditCompleted = new EventEmitter();

  // modal
  private readonly GROUP_MODAL = 'product-group-modal';
  public formModalNew: FormGroup;
  public modalTitle = 'title.createNewGroup';
  public mode = 'save';

  // icon
  public iconList: Array<any> = [];
  public selectedIconSrc: string;

  // button dependencies
  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;

  // selector
  public groupFixList: Array<any>;

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private modalEmitToggleService: ModalEmitToggleService,
    private productResource: ProductsResource,
    private commomService: CommomService,
    private groupsService: GroupsService,
    private toasterEmitService: ToasterEmitService,
  ) { }

  public setFormModal() {
    this.formModalNew = this.formBuilder.group({
      id: null,
      standardCategoryId:  [null, [Validators.required]], // ok
      categoryName: [null, [Validators.required]], // ok
      integrationCode: null,
      iconName: [null, [Validators.required]], // ok
      isActive: true
    });

    this.formModalNew.valueChanges.subscribe( () => {
      this.buttonSaveModalConfig.isDisabled = !this.formModalNew.valid;
    });
  }

  private confirmSave = () => {

    const { value } = this.formModalNew;

    // delete id to don't conflict with backend id
    delete value.id;

    this
      .productResource
      .addGroup(<ProductGroups>value)
      .subscribe( item => {
        this.saveOrEditCompleted.emit(item);
        this.emitMessage('text.groupProductSaveSuccess');
        this.toggleModal(true);
    });
  }

  private clearForm() {
    this.formModalNew.setValue({
      id: null,
      standardCategoryId: null,
      categoryName: null,
      integrationCode: null,
      iconName: null,
      isActive: true
    });

    this.selectedIconSrc = '';
    this.formModalNew.reset({});
  }

  private confirmEdit = () => {
    this
      .productResource
      .editGroup(this.formModalNew.value)
      .subscribe( item => {
        this.saveOrEditCompleted.emit(item);
        this.emitMessage('text.groupProductEditedSuccess');
        this.toggleModal(true);
      });
  }

  public emitMessage(message) {
    this.toasterEmitService.emitChange(SuccessError.success, message);
  }

  public toggleModalSave() {
    this.modalTitle = 'title.createNewGroup';
    this.toggleModal(true);
  }

  public toggleModalEdit(item) {
    this.selectedIconSrc = item.iconName;

    this.modalTitle = 'title.productGroupTitleEdit';
    this.formModalNew.patchValue(item);
    this.toggleModal();
  }

  public clickComponent($event, iconName) {
    $event.preventDefault();
    $event.stopPropagation();

    this.selectedIconSrc = iconName;

    this.formModalNew.patchValue({
      iconName
    });
  }

  public mapToGroupFix (items: Array<ProductGroups>) {
    this.groupFixList = [];

    if ( items ) {
      this.groupFixList =
        this
          .commomService
          .convertObjectToSelectOptionObjectByIdAndValueProperties(items, 'id', 'categoryName');
    }
  }

  public getGroupFixList() {

    this
      .productResource
      .getGroupFixList()
      .subscribe( ({ items }) => {
        if ( items ) {
          this.mapToGroupFix(items);
        }
      });
  }

  public cancel = () => {
    this.toggleModal(true);
  }

  public setButtonConfig() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'reason-cancel-modal',
      this.cancel ,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
      'reason-save-modal',
      () => this.mode === 'save' ? this.confirmSave() : this.confirmEdit() ,
      'commomData.confirm',
      ButtonType.Primary,
      null,
      true,
    );
  }
  private toggleModal = (clearForm?) => {
    if (clearForm) {
      this.clearForm();
    }
    this.modalEmitToggleService.emitToggleWithRef(this.GROUP_MODAL);
  }


  public setTitle(modalTitle) {
    this.modalTitle = modalTitle;
  }

  public loadIconList() {
    this.iconList = this.groupsService.getImagesSrc();
  }

  ngOnInit() {
    this.setFormModal();
    this.setButtonConfig();

    this.loadIconList();

    this.getGroupFixList();
  }

}
