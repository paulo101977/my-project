export const POS_FORM_LIST = 'POS_FORM_LIST';

export class DATA {
  public static readonly POS_FORM_LIST = {
    id: '1234',
    code: '001',
    productName: 'Product Name',
    productCategoryId: 'product Category',
    unitMeasurementId: 1,
    ncmId: null,
    ncmCode: null,
    barCode: 'bar code',
    isActive: true,
    billingItens: {
      billingItemId: 200,
      billingItemName: 'Área de Jogos',
      creationTime: '2019-05-28T12:21:28.367',
      creatorUserId: '2493892d-d3d1-412d-84ff-698c941e175f',
      id: '327c8819-1866-4c14-aedf-aa5b00cba6cf',
      isActive: true,
      isDeleted: false,
      productId: '7cbe484e-e00a-4248-bb92-aa5b00cba6cd',
      tenantId: '63e362ab-1c8a-49df-ba93-7e6763790995',
      unitPrice: 450
    }
  };
}
