export enum GroupOfProductsImg {
  sopas = 'sopas.svg',
  peixes = 'peixes.svg',
  carnes = 'carnes.svg',
  aves = 'aves.svg',
  lanches = 'lanches.svg',
  tabacaria = 'tabacaria.svg',
  snacks = 'snacks.svg',
  massa = 'massa.svg',
  sobremesa = 'sobremesa.svg',
  chasecafes = 'chasecafes.svg',
  naoalcoolicos = 'naoalcoolicos.svg',
  acompanhamentos = 'acompanhamentos.svg',
  saladas = 'saladas.svg',
  alcoolicos = 'alcoolicos.svg'
}
