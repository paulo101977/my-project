export class InputKeys {
  key: string;
  value: string;
}

export class ProductUnity {
  id: string;
  name: string;
}

export class ProductGroups {
  id: string;
  categoryName: string;
  groupFixedName: string;
  iconName: string;
  isActive: boolean;
  chainId: string;
  description: string;
  integrationCode: string;
}

export class PosProduct {
  id: string;
  billingItemTypeId: number;
  billingItemName: string;
  description?: string;
  propertyId: string;
  tenantId: string;
  isActive: boolean;
  isDeleted: boolean;
  isFavorite?: boolean;
  billingItemCategoryName: string;
}

export class Product {
  barCode: string;
  imageBase64: string;
  billingItens: Array<any>;
  chainId: string;
  code: string;
  id: string;
  isActive: boolean;
  isDeleted: boolean;
  productName: string;
  ncmId: string;
  productCategoryId: string;
  propertyId: string; // from app
  tenantId: string; // from app
  categoryName: string;
  unitMeasurementId: string; // unity id
  associatedPos: string; // to list at screen
}

export class NCM {
  id: string;
  code: string;
}

