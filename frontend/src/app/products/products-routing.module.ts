import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsListComponent } from './components/products-list/products-list.component';
import { ProductsNewEditComponent } from './components/products-new-edit/products-new-edit.component';
import { ProductsNewGroupComponent } from './components/products-new-group/products-new-group.component';

export const routes: Routes = [
  { path: 'list', component: ProductsListComponent },
  { path: 'product', component: ProductsNewEditComponent }, // add
  { path: 'product/:productId', component: ProductsNewEditComponent }, // edit
  { path: 'group', component: ProductsNewGroupComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class ProductsRoutingModule {}
