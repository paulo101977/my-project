import { Injectable } from '@angular/core';
import { LocationManagerService } from '@inovacaocmnet/thx-bifrost';
import { CountryCodeEnum } from '@inovacaocmnet/thx-bifrost';
import { AuthService } from 'app/shared/services/auth-service/auth.service';

@Injectable({ providedIn: 'root' })
export class ProductsService {

  constructor(
    private locationService: LocationManagerService,
    private authService: AuthService,
  ) { }

  public showLocationPT (): boolean {
    const { token } = this.authService.getPropertyToken();
    return !!(token && this.locationService.couldShow([CountryCodeEnum.PT], token));
  }

}
