import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { Product } from 'app/products/models/products/products';
import { PdvApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({ providedIn: 'root' })
export class BarCodeService {

  constructor(
    private pdvService: PdvApiService
  ) {}

  public translateImageBarCode(item: Product): Observable<Product> {
    return this
      .pdvService
      .post<Product>(`product/getImageBarCode`, item);
  }

}
