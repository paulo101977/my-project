import { createServiceStub } from '../../../../testing';
import { ProductsService } from 'app/products/services/products.service';
import { BarCodeService } from 'app/products/services/barcode.services';
import { Observable, of } from 'rxjs';
import { Product } from 'app/products/models/products/products';

export const productsServiceStub = createServiceStub(ProductsService, {
    showLocationPT(): boolean {
        return false;
    }
});

export const barCodeServiceStub = createServiceStub(BarCodeService, {
    translateImageBarCode(item: Product): Observable<Product> {
        return of(null);
    }
});
