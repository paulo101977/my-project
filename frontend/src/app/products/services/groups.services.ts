import { GroupOfProductsImg } from 'app/products/models/groups-img-enum/groups.enum';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class GroupsService {

  public getImagesSrc (): string[] {
    return Object.keys(GroupOfProductsImg).map( item => `${item}.svg`);
  }

}
