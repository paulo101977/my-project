import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { ProductsModalPosComponent } from './components/products-modal-pos/products-modal-pos.component';
import { ProductsNewEditComponent } from './components/products-new-edit/products-new-edit.component';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsNewGroupComponent } from './components/products-new-group/products-new-group.component';
import { ProductsModalGroupEditAddComponent } from './components/products-modal-group-edit-add/products-modal-group-edit-add.component';
import { ProductsModalGroupDeleteComponent } from './components/products-modal-group-delete/products-modal-group-delete.component';
import { ProductsModalBarcodeComponent } from 'app/products/components/products-modal-barcode/products-modal-barcode.component';
import { ProductsModalDeleteComponent } from './components/products-modal-delete/products-modal-delete.component';

@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    ProductsListComponent,
    ProductsModalPosComponent,
    ProductsNewEditComponent,
    ProductsNewGroupComponent,
    ProductsModalGroupEditAddComponent,
    ProductsModalGroupDeleteComponent,
    ProductsModalBarcodeComponent,
    ProductsModalDeleteComponent
  ],
  exports: [
    ProductsListComponent,
    ProductsModalPosComponent,
    ProductsNewEditComponent,
    ProductsNewGroupComponent
  ]
})
export class ProductsModule { }
