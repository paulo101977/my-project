import { Component, OnInit } from '@angular/core';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { BillingItemTax } from 'app/shared/models/billingType/billing-item-tax';
import { BillingItemTaxServiceAssociateData } from '../../../shared/mock-data/billing-item-tax-service-associate-data';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { Column } from '../../../shared/models/table/column';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { BillingItemTaxServiceAssociate } from 'app/shared/models/billingType/billing-item-tax-service-associate';
import { GetAllBillingItemTax } from '../../../shared/models/billingType/get-all-billing-item-tax';
import { GetAllBillingItemTaxData } from '../../../shared/mock-data/get-all-billing-item-tax-data';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { DataTableGlobals } from 'app/shared/models/DataTableGlobals';
import { ModalEmitService } from 'app/shared/services/shared/modal-emit.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { BillingItemTaxResource } from 'app/shared/resources/billingItem/billing-item-tax.resource';

@Component({
  selector: 'app-tax-list',
  templateUrl: './tax-list.component.html',
  styleUrls: ['./tax-list.component.css'],
})
export class TaxListComponent implements OnInit {
  public propertyId: number;
  public configHeaderPage: ConfigHeaderPageNew;
  public searchableItems: SearchableItems;
  private billingItemTaxToBeDeleted: GetAllBillingItemTax;

  // Table dependencies
  public pageItemsList: Array<GetAllBillingItemTax>;
  public itemsOption: Array<any>;
  public columns: Array<any>;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private billingItemTaxResource: BillingItemTaxResource,
    private modalEmitService: ModalEmitService,
    private translateService: TranslateService,
    private toasterEmitService: ToasterEmitService,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.setConfigHeaderPage();
      this.getList();
      this.setSearchableItems();

      this.itemsOption = [
        { title: 'commomData.edit', callbackFunction: this.goToEditPage, context: this },
        { title: 'commomData.delete', callbackFunction: this.confirmDeleteItem, context: this },
      ];

      this.columns = [];
      this.columns.push({
        name: 'label.taxName',
        prop: 'billingItemName',
      });
      this.columns.push({
        name: 'label.group',
        prop: 'billingItemCategoryName',
      });
    });
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      keepButtonActive: true,
      callBackFunction: this.goToNewPage,
    };
  }

  private getList() {
    this.pageItemsList = new Array<GetAllBillingItemTax>();
    this.billingItemTaxResource.getAllBillingItemTaxListByPropertyId(this.propertyId).subscribe(response => {
      if (response) {
        this.pageItemsList = response.items;
      }
      this.setSearchableItems();
    });
  }

  public goToNewPage = () => {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

  public goToEditPage(item, context?: TaxListComponent) {
    if (!context) {
      context = this;
    }
    context.router.navigate(['edit', item.id], { relativeTo: context.route });
  }

  private setSearchableItems(): void {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.pageItemsList;
    this.searchableItems.placeholderSearchFilter = 'entryTypeModule.tax.pagelist.header.placeholderFilter';
    this.searchableItems.tableRowTypeEnum = TableRowTypeEnum.BillingTypeTax;
  }

  public updateList(items: Array<GetAllBillingItemTax>) {
    if (items) {
      this.pageItemsList = items;
    }
  }

  public updateStatus(billingItemTax: GetAllBillingItemTax) {
    this.billingItemTaxResource.updateBillingItemTaxStatus(billingItemTax.id).subscribe(response => {
      if (billingItemTax.isActive) {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('entryTypeModule.tax.commomData.activatedWithSuccess'),
        );
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('entryTypeModule.tax.commomData.inactivatedWithSuccess'),
        );
      }
    });
  }

  public confirmDeleteItem(billingItemTax: GetAllBillingItemTax, context?: TaxListComponent) {
    if (!context) {
      context = this;
    }
    context.billingItemTaxToBeDeleted = billingItemTax;
    context.modalEmitService.emitSimpleModal(
      context.translateService.instant('commomData.delete'),
      context.translateService.instant('entryTypeModule.tax.commomData.confirmDeleteMessage') +
        ' ' +
        context.billingItemTaxToBeDeleted.billingItemName +
        '?',
      context.deleteItem,
      [],
    );
  }

  public deleteItem = () => {
    this.billingItemTaxResource.deleteBillingItemTax(this.billingItemTaxToBeDeleted.id, this.propertyId).subscribe(response => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('entryTypeModule.tax.commomData.deleteMessage'),
      );
      this.getList();
    });
  }

  public runCallbackFunction(item: any, tax: GetAllBillingItemTax) {
    if (item.callbackFunction) {
      item.callbackFunction(tax, this);
    }
  }
}
