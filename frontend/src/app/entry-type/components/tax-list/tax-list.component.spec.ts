
import {of as observableOf,  Observable } from 'rxjs';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http/';
import { TaxListComponent } from './tax-list.component';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { GetAllBillingItemTaxData, GetAllBillingItemTaxInactiveData } from '../../../shared/mock-data/get-all-billing-item-tax-data';
import { GetAllBillingItemTax } from '../../../shared/models/billingType/get-all-billing-item-tax';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ActivatedRoute } from '@angular/router';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('TaxListComponent', () => {
  let component: TaxListComponent;
  let fixture: ComponentFixture<TaxListComponent>;
  const pageItemsList = new Array<GetAllBillingItemTax>();
  const response = {
    items: pageItemsList,
  };

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), RouterTestingModule, HttpClientModule],
      declarations: [TaxListComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: observableOf({ property: 1 }),
          },
        },
        InterceptorHandlerService
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(TaxListComponent);
    component = fixture.componentInstance;

    spyOn<any>(component['billingItemTaxResource'], 'getAllBillingItemTaxListByPropertyId')
        .and.returnValue(observableOf(response));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on init', () => {
    it('should setVars', () => {
      spyOn<any>(component, 'setConfigHeaderPage');
      // spyOn(component, 'setConfigTable');

      component['setVars']();

      expect(component.propertyId).toEqual(1);
      expect(component['setConfigHeaderPage']).toHaveBeenCalled();
      // expect(component['setConfigTable']).toHaveBeenCalled();
      expect(component.pageItemsList).toEqual(pageItemsList);
      expect(component.searchableItems.items).toEqual(response.items);
      expect(component.searchableItems.placeholderSearchFilter).toEqual('entryTypeModule.tax.pagelist.header.placeholderFilter');
      expect(component.searchableItems.tableRowTypeEnum).toEqual(TableRowTypeEnum.BillingTypeTax);
    });

    it('should set configHeaderPage, setConfigTable', () => {
      expect(component.configHeaderPage).toEqual({
        keepTitleButton: true,
        keepButtonActive: true,
        callBackFunction: component.goToNewPage,
      });
      expect(component.columns).toEqual([
        {
          name: 'label.taxName',
          prop: 'billingItemName',
        },
        {
          name: 'label.group',
          prop: 'billingItemCategoryName',
        },
      ]);
    });
  });

  describe('on methods', () => {
    it('should goToNewPage', () => {
      spyOn(component['router'], 'navigate');

      component['goToNewPage']();

      expect(component['router'].navigate).toHaveBeenCalledWith(['new'], { relativeTo: component['route'] });
    });

    it(
      'should goToEditPage',
      fakeAsync(() => {
        const item = { id: '9ae937da-4df3-4f2a-843d-b49286f41dfd' };

        spyOn(component.router, 'navigate');
        component.goToEditPage(item);
        expect(component.router.navigate).toHaveBeenCalledWith(['edit', item.id], { relativeTo: component['route'] });
      }),
    );

    it('should updateList', () => {
      const list = new Array<GetAllBillingItemTax>();
      list.push(GetAllBillingItemTaxData);

      component.updateList(list);

      expect(component.pageItemsList).toEqual(list);
    });

    it('should confirmDeleteItem', () => {
      spyOn(component['modalEmitService'], 'emitSimpleModal');
      component.confirmDeleteItem(GetAllBillingItemTaxData);

      expect(component['billingItemTaxToBeDeleted']).toEqual(GetAllBillingItemTaxData);
      expect(component['modalEmitService'].emitSimpleModal).toHaveBeenCalledWith(
        'commomData.delete',
        'entryTypeModule.tax.commomData.confirmDeleteMessage' + ' ' + component['billingItemTaxToBeDeleted'].billingItemName + '?',
        component.deleteItem,
        [],
      );
    });

    it('should updateStatus when isActive is true', () => {
      spyOn<any>(component['billingItemTaxResource'], 'updateBillingItemTaxStatus')
          .and.returnValue(observableOf({}));
      spyOn(component['toasterEmitService'], 'emitChange');
      GetAllBillingItemTaxData.isActive = true;

      component.updateStatus(GetAllBillingItemTaxData);

      expect(component['billingItemTaxResource'].updateBillingItemTaxStatus).toHaveBeenCalledWith(GetAllBillingItemTaxData.id);
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'entryTypeModule.tax.commomData.activatedWithSuccess',
      );
    });

    it('should updateStatus when isActive is false', () => {
      spyOn<any>(component['billingItemTaxResource'], 'updateBillingItemTaxStatus')
          .and.returnValue(observableOf({}));
      spyOn(component['toasterEmitService'], 'emitChange');

      component.updateStatus(GetAllBillingItemTaxInactiveData);

      expect(component['billingItemTaxResource'].updateBillingItemTaxStatus).toHaveBeenCalledWith(GetAllBillingItemTaxInactiveData.id);
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'entryTypeModule.tax.commomData.inactivatedWithSuccess',
      );
    });
  });
});
