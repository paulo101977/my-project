
import {of as observableOf,  Observable } from 'rxjs';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http/';
import { ServiceListComponent } from './service-list.component';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { GetAllBillingItemService } from '../../../shared/models/billingType/get-all-billing-item-service';
import {
  GetAllBillingItemServiceData,
  GetAllBillingItemServiceInactiveData,
} from '../../../shared/mock-data/get-all-billing-item-service-data';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { ActivatedRoute } from '@angular/router';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('ServiceListComponent', () => {
  let component: ServiceListComponent;
  let fixture: ComponentFixture<ServiceListComponent>;
  const pageItemsList = new Array<GetAllBillingItemService>();
  pageItemsList.push(GetAllBillingItemServiceData);
  const response = {
    items: pageItemsList,
  };

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), RouterTestingModule, HttpClientModule],
      declarations: [ServiceListComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: observableOf({ property: 1 }),
          },
        },
        InterceptorHandlerService
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ServiceListComponent);
    component = fixture.componentInstance;

    spyOn<any>(component['billingItemServiceResource'], 'getAllBillingItemServiceListByPropertyId')
        .and.returnValue(observableOf(response));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on init', () => {
    it('should setVars', () => {
      spyOn<any>(component, 'setConfigHeaderPage');
      spyOn<any>(component, 'setConfigTable');

      component['setVars']();

      expect(component.propertyId).toEqual(1);
      expect(component['setConfigHeaderPage']).toHaveBeenCalled();
      expect(component['setConfigTable']).toHaveBeenCalled();
      expect(component.pageItemsList).toEqual(pageItemsList);
      expect(component.searchableItems.items).toEqual(response.items);
      expect(component.searchableItems.placeholderSearchFilter).toEqual('entryTypeModule.service.pagelist.header.placeholderFilter');
      expect(component.searchableItems.tableRowTypeEnum).toEqual(TableRowTypeEnum.BillingTypeService);
    });

    it('should set configHeaderPage, setConfigTable', () => {
      expect(component.configHeaderPage).toEqual({
        keepTitleButton: true,
        keepButtonActive: true,
        callBackFunction: component.goToNewPage,
      });
      expect(component.columns).toEqual([
        {
          name: 'label.description',
          prop: 'billingItemName',
        },
        {
          name: 'label.group',
          prop: 'billingItemCategoryName',
        },
      ]);
    });
  });

  describe('on methods', () => {
    it('should goToNewPage', () => {
      spyOn(component['router'], 'navigate');

      component['goToNewPage']();

      expect(component['router'].navigate).toHaveBeenCalledWith(['new'], { relativeTo: component['route'] });
    });

    it(
      'should goToEditPage',
      fakeAsync(() => {
        const item = { id: '9ae937da-4df3-4f2a-843d-b49286f41dfd' };

        spyOn(component.router, 'navigate');
        component.goToEditPage(item);
        expect(component.router.navigate).toHaveBeenCalledWith(['edit', item.id], { relativeTo: component['route'] });
      }),
    );

    it('should updateList', () => {
      const list = new Array<GetAllBillingItemService>();
      list.push(GetAllBillingItemServiceData);

      component.updateList(list);

      expect(component.pageItemsList).toEqual(list);
    });

    it('should confirmDeleteItem', () => {
      spyOn(component['modalEmitService'], 'emitSimpleModal');
      component.confirmDeleteItem(GetAllBillingItemServiceData);

      expect(component['billingItemServiceToBeDeleted']).toEqual(GetAllBillingItemServiceData);
      expect(component['modalEmitService'].emitSimpleModal).toHaveBeenCalledWith(
        'commomData.delete',
        'entryTypeModule.service.commomData.confirmDeleteMessage' + ' ' + component['billingItemServiceToBeDeleted'].billingItemName + '?',
        component.deleteItem,
        [],
      );
    });

    it('should updateStatus when isActive is true', () => {
      spyOn<any>(component['billingItemServiceResource'], 'updateBillingItemServiceStatus')
          .and.returnValue(observableOf({}));
      spyOn(component['toasterEmitService'], 'emitChange');
      GetAllBillingItemServiceData.isActive = true;

      component.updateStatus(GetAllBillingItemServiceData);

      expect(component['billingItemServiceResource'].updateBillingItemServiceStatus).toHaveBeenCalledWith(GetAllBillingItemServiceData.id);
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'entryTypeModule.service.commomData.activatedWithSuccess',
      );
    });

    it('should updateStatus when isActive is false', () => {
      spyOn<any>(component['billingItemServiceResource'], 'updateBillingItemServiceStatus')
          .and.returnValue(observableOf({}));
      spyOn(component['toasterEmitService'], 'emitChange');

      component.updateStatus(GetAllBillingItemServiceInactiveData);

      expect(component['billingItemServiceResource'].updateBillingItemServiceStatus).toHaveBeenCalledWith(
        GetAllBillingItemServiceInactiveData.id,
      );
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'entryTypeModule.service.commomData.inactivatedWithSuccess',
      );
    });
  });
});
