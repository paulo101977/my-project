import { Component, OnInit } from '@angular/core';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { Column } from '../../../shared/models/table/column';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { BillingItemServiceTaxAssociate } from '../../../shared/models/billingType/billing-item-service-tax-associate';
import { GetAllBillingItemService } from '../../../shared/models/billingType/get-all-billing-item-service';
import { GetAllBillingItemServiceData } from '../../../shared/mock-data/get-all-billing-item-service-data';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ModalEmitService } from 'app/shared/services/shared/modal-emit.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { BillingItemTaxResource } from 'app/shared/resources/billingItem/billing-item-tax.resource';
import { BillingItemServiceResource } from 'app/shared/resources/billingItem/billing-item-service.resource';

@Component({
  selector: 'app-service-list',
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.css'],
})
export class ServiceListComponent implements OnInit {
  public propertyId: number;
  public configHeaderPage: ConfigHeaderPageNew;
  public searchableItems: SearchableItems;
  private billingItemServiceToBeDeleted: GetAllBillingItemService;

  // Table dependencies
  public columns: Array<any>;
  public pageItemsList: Array<GetAllBillingItemService>;
  public itemsOption: Array<any>;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private billingItemServiceResource: BillingItemServiceResource,
    private modalEmitService: ModalEmitService,
    private translateService: TranslateService,
    private toasterEmitService: ToasterEmitService,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.setConfigTable();
      this.setConfigHeaderPage();
      this.getList();
      this.setSearchableItems();
    });
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      keepButtonActive: true,
      callBackFunction: this.goToNewPage,
    };
  }

  private setConfigTable() {
    this.setColumnsName();
    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.goToEditPage, context: this },
      { title: 'commomData.delete', callbackFunction: this.confirmDeleteItem, context: this },
    ];
  }

  private setColumnsName() {
    this.columns = [];
    this.columns.push({
      name: 'label.description',
      prop: 'billingItemName',
    });
    this.columns.push({
      name: 'label.group',
      prop: 'billingItemCategoryName',
    });
  }

  private getList() {
    this.pageItemsList = new Array<GetAllBillingItemService>();
    this.billingItemServiceResource.getAllBillingItemServiceListByPropertyId(this.propertyId).subscribe(response => {
      if (response) {
        this.pageItemsList = response.items;
      }
      this.setSearchableItems();
    });
  }

  public goToNewPage = () => {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

  public goToEditPage(item, context?: ServiceListComponent) {
    if (!context) {
      context = this;
    }
    context.router.navigate(['edit', item.id], { relativeTo: context.route });
  }

  private setSearchableItems(): void {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.pageItemsList;
    this.searchableItems.placeholderSearchFilter = 'entryTypeModule.service.pagelist.header.placeholderFilter';
    this.searchableItems.tableRowTypeEnum = TableRowTypeEnum.BillingTypeService;
  }

  public updateList(items: Array<GetAllBillingItemService>) {
    if (items) {
      this.pageItemsList = items;
    }
  }

  public updateStatus(billingItemService: GetAllBillingItemService) {
    this.billingItemServiceResource.updateBillingItemServiceStatus(billingItemService.id).subscribe(response => {
      if (billingItemService.isActive) {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('entryTypeModule.service.commomData.activatedWithSuccess'),
        );
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('entryTypeModule.service.commomData.inactivatedWithSuccess'),
        );
      }
    });
  }

  public confirmDeleteItem(billingItemService: GetAllBillingItemService, context?: ServiceListComponent) {
    if (!context) {
      context = this;
    }
    context.billingItemServiceToBeDeleted = billingItemService;
    context.modalEmitService.emitSimpleModal(
      context.translateService.instant('commomData.delete'),
      context.translateService.instant('entryTypeModule.service.commomData.confirmDeleteMessage') +
        ' ' +
        context.billingItemServiceToBeDeleted.billingItemName +
        '?',
      context.deleteItem,
      [],
    );
  }

  public deleteItem = () => {
    this.billingItemServiceResource.deleteBillingItemService(this.billingItemServiceToBeDeleted.id, this.propertyId).subscribe(response => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('entryTypeModule.service.commomData.deleteMessage'),
      );
      this.getList();
    });
  }
}
