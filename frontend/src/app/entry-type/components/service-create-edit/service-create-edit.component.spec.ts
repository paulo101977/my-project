
import {of as observableOf,  Observable } from 'rxjs';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ServiceCreateEditComponent } from './service-create-edit.component';
import { ButtonSize, ButtonType } from '../../../shared/models/button-config';
import {
  BillingItemServiceTaxAssociateData,
  BillingItemServiceTaxAssociateData2,
} from '../../../shared/mock-data/billing-item-service-tax-associate-data';
import { BillingItemServiceData, BillingItemServiceData1 } from '../../../shared/mock-data/billing-item-service-data';
import { GetAllBillingItemTax } from '../../../shared/models/billingType/get-all-billing-item-tax';
import { ItemsResponse } from '../../../shared/models/backend-api/item-response';
import { GetAllBillingItemTaxData } from '../../../shared/mock-data/get-all-billing-item-tax-data';
import { BillingItemCategory } from '../../../shared/models/billingType/billing-item-category';
import { BillingItemCategoryData } from '../../../shared/mock-data/billing-type-group-data';
import { MyDatePickerModule } from 'mydatepicker';

xdescribe('ServiceCreateEditComponent', () => {
  let component: ServiceCreateEditComponent;
  let fixture: ComponentFixture<ServiceCreateEditComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        ReactiveFormsModule,
        RouterTestingModule,
        MyDateRangePickerModule,
        MyDatePickerModule,
        HttpClientTestingModule,
        FormsModule,
      ],
      declarations: [ServiceCreateEditComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ServiceCreateEditComponent);
    component = fixture.componentInstance;

    const billingItemCategoryList = <ItemsResponse<BillingItemCategory>>{
      hasNext: false,
      items: [BillingItemCategoryData],
      total: 1,
    };
    const billingItemTaxList = <ItemsResponse<GetAllBillingItemTax>>{
      hasNext: false,
      items: [GetAllBillingItemTaxData],
      total: 1,
    };

    spyOn(component['billingItemCategoryResource'], 'getAllBillingItemCategoryListByPropertyId').and.returnValue(
      observableOf(billingItemCategoryList),
    );
    spyOn<any>(component['billingItemServiceResource'], 'getAllBillingItemCategoryActiveListByPropertyId')
        .and.returnValue(
          observableOf(billingItemTaxList),
        );
    spyOn(component['billingItemTaxResource'], 'getAllBillingItemTaxListByPropertyId').and.returnValue(observableOf(billingItemTaxList));

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
      expect(Object.keys(component.form['controls'])).toEqual(['isActive', 'name', 'group', 'id']);
      expect(Object.keys(component.formModalAssociate['controls'])).toEqual([
        'isActive',
        'id',
        'taxPercentage',
        'effectiveDate',
        'selectValueTax',
      ]);
      expect(component.buttonSaveConfig.id).toEqual('group-create-edit-save');
      expect(component.buttonSaveConfig.textButton).toEqual('commomData.confirm');
      expect(component.buttonSaveConfig.buttonType).toEqual(ButtonType.Primary);
      expect(component.buttonCancelConfig.id).toEqual('group-create-edit-cancel');
      expect(component.buttonCancelConfig.textButton).toEqual('commomData.cancel');
      expect(component.buttonCancelConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.buttonImplementConfig.id).toEqual('group-create-edit-implementation');
      expect(component.buttonImplementConfig.textButton).toEqual('entryTypeModule.service.createEdit.associatedFees.buttonNew');
      expect(component.buttonImplementConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.buttonImplementConfig.buttonSize).toEqual(ButtonSize.Small);
      expect(component.buttonCancelModalConfig.id).toEqual('group-create-edit-cancel-modal');
      expect(component.buttonCancelModalConfig.textButton).toEqual('commomData.cancel');
      expect(component.buttonCancelModalConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.buttonSaveModalConfig.id).toEqual('group-create-edit-save-modal');
      expect(component.buttonSaveModalConfig.textButton).toEqual('commomData.confirm');
      expect(component.buttonSaveModalConfig.buttonType).toEqual(ButtonType.Primary);
    });

    it('should setConfigTable', () => {
      // expect(component.quantityOfItemsToDisplayPerPage).toEqual(10);
      // expect(component.tableRowTypeEnum).toEqual(TableRowTypeEnum.BillingTypeServiceTax);
      expect(component.columns).toEqual([
        {
          name: 'Nome da Taxa',
          prop: 'name',
        },
        {
          name: 'Grupo',
          prop: 'categoryName',
        },
        {
          name: 'Alíquota',
          prop: 'strTaxPercentage',
        },
        {
          name: 'Data de Vigência',
          prop: 'strVigence',
        },
      ]);
    });
  });

  describe('on methods', () => {
    it('should toggleModal', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
      spyOn<any>(component, 'setFormModal');
      component.toggleModal();
      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('modalAssociate');
      expect(component['setFormModal']).toHaveBeenCalled();
    });

    it('should saveAssociateTax - editing (in datalist)', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
      component.formModalAssociate.get('selectValueTax.taxAssociate').setValue(BillingItemServiceTaxAssociateData.billingItemTaxId);
      component.formModalAssociate.get('isActive').setValue(!BillingItemServiceTaxAssociateData.isActive);
      component.formModalAssociate.get('taxPercentage').setValue(4);
      component.formModalAssociate.get('effectiveDate').setValue({
        beginJsDate: new Date(),
        endJsDate: new Date().getDate() + 1,
      });
      component.taxList = [];
      component.taxList.push(BillingItemServiceTaxAssociateData);

      component.saveAssociateTax();

      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('modalAssociate');
      expect(component.taxList[0].isActive).toBeTruthy();
      expect(component.taxList[0].taxPercentage).toEqual(6);
    });

    it('should saveAssociateTax - creating (there no in taxList) with taxAssociateList', () => {
      spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
      component.formModalAssociate.get('selectValueTax.taxAssociate').setValue(BillingItemServiceTaxAssociateData.billingItemTaxId);
      component.formModalAssociate.get('isActive').setValue(!BillingItemServiceTaxAssociateData.isActive);
      component.formModalAssociate.get('taxPercentage').setValue(4);
      component.formModalAssociate.get('effectiveDate').setValue({
        beginJsDate: new Date(),
        endJsDate: new Date().getDate() + 1,
      });
      component.taxList = [];
      component.taxList.push(BillingItemServiceTaxAssociateData);
      component['taxAssociateList'] = [];
      component['taxAssociateList'].push(GetAllBillingItemTaxData);

      component.saveAssociateTax();

      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('modalAssociate');
      expect(component.taxList.length).toEqual(2);
      expect(component.taxList[0].isActive).toBeTruthy();
      expect(component.taxList[0].taxPercentage).toEqual(6);
    });

    it('should removeItem', () => {
      component.taxList = [BillingItemServiceTaxAssociateData2];

      component.removeItem(BillingItemServiceTaxAssociateData2);

      expect(component.taxList).not.toContain(BillingItemServiceTaxAssociateData2);
    });

    it('should goToEdit', () => {
      component.goToEdit(BillingItemServiceTaxAssociateData2, '', 1);

      const beginDate = BillingItemServiceTaxAssociateData2.beginDate;
      const endDate = BillingItemServiceTaxAssociateData2.endDate;

      expect(component.formModalAssociate.get('isActive').value).toEqual(true);
      expect(component.formModalAssociate.get('taxPercentage').value).toEqual(2);
      expect(component.formModalAssociate.get('selectValueTax.taxAssociate').value).toEqual(3);
      expect(component.formModalAssociate.get('effectiveDate').value).toEqual({
        beginDate: { year: beginDate.getFullYear(), month: beginDate.getMonth() + 1, day: beginDate.getDate() },
        endDate: { year: endDate.getFullYear(), month: endDate.getMonth() + 1, day: endDate.getDate() },
        beginJsDate: beginDate,
        endJsDate: endDate,
      });
    });
  });

  describe('on actions', () => {
    it('should save a new ServiceWithTaxAssociated ', () => {
      spyOn(component['billingItemTypeMapper'], 'toSaveServiceWithTaxAssociated').and.returnValue(BillingItemServiceData);
      spyOn(component['billingItemServiceResource'], 'createBillingItemServicesWithTaxAssociated').and.returnValue(
        observableOf(BillingItemServiceData),
      );
      spyOn(component.router, 'navigate');

      BillingItemServiceData.id = null;
      component.saveServiceWithTaxAssociated();

      expect(component['billingItemTypeMapper'].toSaveServiceWithTaxAssociated).toHaveBeenCalledWith(component.form, component.taxList);
      expect(component['billingItemServiceResource'].createBillingItemServicesWithTaxAssociated).toHaveBeenCalledWith(
        BillingItemServiceData,
      );
      expect(component.router.navigate).toHaveBeenCalledWith(['../'], { relativeTo: component['route'] });
    });

    it('should save a existing ServiceWithTaxAssociated', () => {
      spyOn(component['billingItemTypeMapper'], 'toSaveServiceWithTaxAssociated').and.returnValue(BillingItemServiceData1);
      spyOn<any>(component['billingItemServiceResource'], 'updateBillingItemServicesWithTaxAssociated').and.returnValue(
        observableOf(BillingItemServiceData1),
      );
      spyOn(component.router, 'navigate');

      component.saveServiceWithTaxAssociated();

      expect(component['billingItemTypeMapper'].toSaveServiceWithTaxAssociated).toHaveBeenCalledWith(component.form, component.taxList);
      expect(component['billingItemServiceResource'].updateBillingItemServicesWithTaxAssociated).toHaveBeenCalledWith(
        BillingItemServiceData1,
      );
      expect(component.router.navigate).toHaveBeenCalledWith(['../../'], { relativeTo: component['route'] });
    });

    it('should getServiceById', () => {
      spyOn(component['billingItemServiceResource'], 'getServiceById').and.returnValue(observableOf(BillingItemServiceData));

      component.getServiceById(BillingItemServiceData.id);

      expect(component['billingItemServiceResource'].getServiceById).toHaveBeenCalledWith(BillingItemServiceData.id, component.propertyId);
      expect(component.form.get('isActive').value).toEqual(BillingItemServiceData.isActive);
      expect(component.form.get('name').value).toEqual(BillingItemServiceData.name);
      expect(component.form.get('group').value).toEqual(BillingItemServiceData.billingItemCategoryId);
      expect(component.form.get('id').value).toEqual(BillingItemServiceData.id);
      expect(component.taxList).toEqual(component['prepareList'](BillingItemServiceData.serviceAndTaxList));
    });

    it('should getTaxAssociateListToInsertInTaxesListService', () => {
      component['getTaxAssociateListToInsertInTaxesListService']();

      expect(component['billingItemTaxResource'].getAllBillingItemTaxListByPropertyId).toHaveBeenCalledWith(component.propertyId);
      expect(component['taxAssociateList'].length).toEqual(1);
      expect(component['taxAssociateList'][0].id).toEqual(GetAllBillingItemTaxData.id);
      expect(component['taxAssociateList'][0].billingItemName).toEqual(GetAllBillingItemTaxData.billingItemName);
      expect(component['taxAssociateList'][0].billingItemCategoryName).toEqual(GetAllBillingItemTaxData.billingItemCategoryName);
    });
  });
});
