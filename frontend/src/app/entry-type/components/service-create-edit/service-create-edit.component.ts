import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe, Location } from '@angular/common';
import { CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';
import { TranslateService } from '@ngx-translate/core';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../shared/models/button-config';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { BillingItemServiceTaxAssociate } from '../../../shared/models/billingType/billing-item-service-tax-associate';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { GetAllBillingItemTax } from '../../../shared/models/billingType/get-all-billing-item-tax';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { BillingItemTypeMapper } from '../../../shared/mappers/billing-item-type-mapper';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { BillingItemService } from '../../../shared/models/billingType/billing-item-service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { BillingEntryTypeService } from '../../../shared/services/billing-entry-type/billing-entry-type.service';
import { BillingItemServiceResource } from '../../../shared/resources/billingItem/billing-item-service.resource';
import { BillingItemCategoryResource } from '../../../shared/resources/billing-item-category/billing-item-category.resource';
import { BillingItemTaxResource } from '../../../shared/resources/billingItem/billing-item-tax.resource';
import { DateService } from '../../../shared/services/shared/date.service';
import { GuidDefaultData } from '../../../shared/mock-data/guid-default-data';
import { ValidatorFormService } from '../../../shared/services/shared/validator-form.service';

@Component({
  selector: 'app-service-create-edit',
  templateUrl: './service-create-edit.component.html',
  styleUrls: ['./service-create-edit.component.css'],
  providers: [DatePipe],
})
export class ServiceCreateEditComponent implements OnInit {
  public propertyId: number;
  public form: FormGroup;
  private taxAssociateList: Array<GetAllBillingItemTax>;
  private taxAssociateIndexToUpdate: number;
  private serviceId: number;
  public pageTitle: string;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;
  public buttonImplementConfig: ButtonConfig;
  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;

  // Table dependencies
  public columns: Array<any>;
  public taxList: BillingItemServiceTaxAssociate[] = [];
  private readonly NOT_FOUND = -1;
  public itemsOption: Array<any>;

  // ModalAssociateTax dependencies
  public formModalAssociate: FormGroup;
  public readonly MODAL_ASSOCIATE = 'modalAssociate';

  // Select dependencies
  public groupList: Array<SelectObjectOption>;
  public taxAssociateOptionList: Array<SelectObjectOption>;


  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private modalEmitToggleService: ModalEmitToggleService,
    private dateService: DateService,
    private sharedService: SharedService,
    private billingItemTypeMapper: BillingItemTypeMapper,
    private billingItemServiceResource: BillingItemServiceResource,
    private toasterEmitService: ToasterEmitService,
    public route: ActivatedRoute,
    private commomService: CommomService,
    private billingItemCategoryResource: BillingItemCategoryResource,
    private billingItemTaxResource: BillingItemTaxResource,
    public router: Router,
    private translateService: TranslateService,
    private validatorFormService: ValidatorFormService
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.propertyId = +this.route.snapshot.params.property;
    this.serviceId = +this.route.snapshot.params.id;
    this.setForm();
    this.setFormModal();
    this.setButtonConfig();
    this.getAllCategoryListForItem();
    this.setConfigTable();
    this.getTaxAssociateListToSelect();
    this.getTaxAssociateListToInsertInTaxesListService();

    if (this.isEditPage()) {
      this.pageTitle = 'entryTypeModule.service.createEdit.header.editTitle';
      this.getServiceById(this.serviceId);
    } else {
      this.pageTitle = 'entryTypeModule.service.createEdit.header.title';
    }

    this.itemsOption = [
      {title: 'commomData.edit', callbackFunction: this.goToEdit},
      {title: 'commomData.delete', callbackFunction: this.removeItem},
    ];
  }

  private isEditPage() {
    return this.serviceId;
  }

  private setForm() {
    this.form = this.formBuilder.group({
      isActive: true,
      name: [null, [Validators.required]],
      group: [null, [Validators.required]],
      integrationCode: [null],
      id: 0,
    });

    this.form.valueChanges.subscribe(value => {
      this.buttonSaveConfig.isDisabled = this.form.invalid;
    });
  }

  private setFormModal() {
    this.formModalAssociate = this.formBuilder.group({
        isActive: true,
        id: [GuidDefaultData],
        taxPercentage: [null, [Validators.required]],
        beginDate: [null, [Validators.required]],
        endDate: [null, [Validators.required]],
        selectValueTax: this.formBuilder.group({
          taxAssociate: [null, [Validators.required]],
        }),
      }, {
        validator: [
          this.validatorFormService.endDateIsMajorThanBeginDate,
          this.samePeriodAndType
        ]
      }
    );
    this.formModalAssociate.valueChanges.subscribe(value => {
      this.buttonSaveModalConfig.isDisabled = this.formModalAssociate.invalid;
    });
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'group-create-edit-cancel',
      () => this.location.back(),
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'group-create-edit-save',
      this.saveServiceWithTaxAssociated,
      'commomData.confirm',
      ButtonType.Primary,
    );
    this.buttonImplementConfig = this.sharedService.getButtonConfig(
      'group-create-edit-implementation',
      this.addTaxAssociate,
      'entryTypeModule.service.createEdit.associatedFees.buttonNew',
      ButtonType.Secondary,
      ButtonSize.Small,
    );
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'group-create-edit-cancel-modal',
      this.toggleModal,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
      'group-create-edit-save-modal',
      this.saveAssociateTax,
      'commomData.confirm',
      ButtonType.Primary,
    );
  }

  private setConfigTable() {
    this.setColumnsName();
  }

  private setColumnsName() {
    this.columns = [
      {name: 'label.taxName', prop: 'name'},
      {name: 'label.group', prop: 'categoryName'},
      {name: 'label.taxPercentage', prop: 'strTaxPercentage'},
      {name: 'label.validityBegin', prop: 'strBeginDate'},
      {name: 'label.validityEnd', prop: 'strEndDate'}
    ];
  }

  private setTaxAssociatedList(list: Array<BillingItemServiceTaxAssociate>) {
    this.taxList = [...this.prepareList(list)];
  }

  private prepareList(list): any {
    return list.map(item => {
      item['strBeginDate'] = this.dateService.removeHourFromUniversalDate(item.beginDate);
      item['strEndDate'] = this.dateService.removeHourFromUniversalDate(item.endDate);
      item['strTaxPercentage'] = item.taxPercentage + '%';
      return item;
    });
  }

  public toggleModal = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_ASSOCIATE);
    this.formModalAssociate.reset({
      isActive: true,
      taxPercentage: null,
      beginDate: null,
      endDate: null,
      selectValueTax: {
        taxAssociate: null,
      },
      id: GuidDefaultData,
      integrationCode: null
    });
  }

  private addTaxAssociate = () => {
    this.taxAssociateIndexToUpdate = this.NOT_FOUND;
    this.toggleModal();
  }

  public goToEdit = (event, context, index) => {
    this.taxAssociateIndexToUpdate = index;
    this.toggleModal();
    this.setBillingItemTaxAssociateForm(event);
  }

  public removeItem = (event) => {
    const billingItemTax = event;
    if (this.sharedService.elementExistsInList(billingItemTax, this.taxList)) {
      this.sharedService.removeElementFromList(billingItemTax, this.taxList);
      this.taxList = [...this.taxList];
    }
  }

  public saveAssociateTax = () => {
    this.taxAssociateIndexToUpdate == this.NOT_FOUND ?
      this.createAssociateTax() :
      this.editAssociateTax();

    this.toggleModal();
  }

  private getTaxAssociateListToSelect() {
    this.taxAssociateOptionList = [];
    this.billingItemServiceResource.getAllBillingItemTaxToAssociateList(this.propertyId).subscribe(response => {
      if (response && response.items.length > 0) {
        this.taxAssociateOptionList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
          response,
          'id',
          'billingItemName',
        );
      }
    });
  }

  private getTaxAssociateListToInsertInTaxesListService() {
    this.taxAssociateList = [];
    this.billingItemTaxResource.getAllBillingItemTaxListByPropertyId(this.propertyId).subscribe(response => {
      if (response && response.items.length > 0) {
        this.taxAssociateList = response.items;
      }
    });
  }

  public updateStatusTaxAssociated(tax: BillingItemServiceTaxAssociate) {
    this.formModalAssociate.get('isActive').setValue(tax.isActive);
  }

  public saveServiceWithTaxAssociated = () => {
    const billingItemService = this.billingItemTypeMapper.toSaveServiceWithTaxAssociated(this.form, this.taxList);
    billingItemService.propertyId = this.propertyId;
    if (billingItemService.id) {
      this.billingItemServiceResource.updateBillingItemServicesWithTaxAssociated(billingItemService)
        .subscribe(() => {
        this.router.navigate(['../../'], {relativeTo: this.route})
          .then(() => {
          this.toasterEmitService
            .emitChange(SuccessError.success, 'entryTypeModule.service.commomData.updateMessageSuccess');
        });
      });
    } else {
      this.billingItemServiceResource.createBillingItemServicesWithTaxAssociated(billingItemService)
        .subscribe(() => {
        this.router.navigate(['../'], {relativeTo: this.route})
          .then(() => {
            this.toasterEmitService
              .emitChange(SuccessError.success, 'entryTypeModule.service.commomData.saveMessageSuccess');
          });
      });
    }
  }

  public getServiceById(id: number) {
    this.billingItemServiceResource.getServiceById(id, this.propertyId).subscribe(response => {
      const billingItemService: BillingItemService = response;
      this.form.patchValue({
        isActive: billingItemService.isActive,
        name: billingItemService.name,
        group: billingItemService.billingItemCategoryId,
        id: billingItemService.id,
        integrationCode: billingItemService.integrationCode
      });
      this.setTaxAssociatedList(billingItemService.serviceAndTaxList);
    });
  }

  private setBillingItemTaxAssociateForm(billingItemServiceTaxAssociate: BillingItemServiceTaxAssociate) {
    this.formModalAssociate.patchValue({
      isActive: billingItemServiceTaxAssociate.isActive,
      taxPercentage: billingItemServiceTaxAssociate.taxPercentage,
      beginDate: billingItemServiceTaxAssociate.beginDate,
      endDate: billingItemServiceTaxAssociate.endDate,
      selectValueTax: {
        taxAssociate: billingItemServiceTaxAssociate.billingItemTaxId,
      },
      id: billingItemServiceTaxAssociate.id ? billingItemServiceTaxAssociate.id : GuidDefaultData,
      integrationCode: billingItemServiceTaxAssociate.integrationCode
    });
  }

  private editAssociateTax() {
    const editBillingItemTaxToAssociate = this.taxAssociateList.find(
      taxAssociate =>
        taxAssociate.id == this.formModalAssociate.get('selectValueTax.taxAssociate').value,
    );

    const taxChanged = this.billingItemTypeMapper
      .setTaxToService(this.formModalAssociate, editBillingItemTaxToAssociate);
    this.taxList.splice(this.taxAssociateIndexToUpdate, 1, taxChanged);
    this.setTaxAssociatedList(this.taxList);
  }

  private createAssociateTax() {
    const newBillingItemTaxToAssociate = this.taxAssociateList.find(
      taxAssociate =>
        taxAssociate.id == this.formModalAssociate.get('selectValueTax.taxAssociate').value,
    );

    const tax = this.billingItemTypeMapper
      .setTaxToService(this.formModalAssociate, newBillingItemTaxToAssociate);

    this.taxList.push(tax);
    this.setTaxAssociatedList(this.taxList);
  }

  private getAllCategoryListForItem() {
    this.groupList = new Array<SelectObjectOption>();
    this.billingItemCategoryResource.getAllBillingItemCategoryActiveListByPropertyId(this.propertyId).subscribe(response => {
      if (response) {
        this.groupList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
          response,
          'id',
          'billingItemCategoryName',
        );
      }
    });
  }

  private samePeriodAndType = (form: AbstractControl) => {
    const hasTax = this.taxList.some((t, index) => {
        if (this.taxAssociateIndexToUpdate != index) {
          return t.billingItemTaxId == form.get('selectValueTax.taxAssociate').value &&
            (this.dateService
              .isBetweenInPeriod(form.get('beginDate').value, t.beginDate, t.endDate) ||
            this.dateService
              .isBetweenInPeriod(form.get('endDate').value, t.beginDate, t.endDate));
        }
        return false;
      }
    );
    return hasTax ? {hasTaxInPeriod: this.translateService.instant('alert.hasTaxInPeriod')} : null;
  }
}
