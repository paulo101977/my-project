import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { DatePipe, Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { ButtonConfig, ButtonSize, ButtonType } from '../../../shared/models/button-config';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { BillingItemTaxServiceAssociate } from '../../../shared/models/billingType/billing-item-tax-service-associate';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { DateService } from '../../../shared/services/shared/date.service';
import { BillingItemTypeMapper } from '../../../shared/mappers/billing-item-type-mapper';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { BillingItemTaxResource } from '../../../shared/resources/billingItem/billing-item-tax.resource';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { GetAllBillingItemService } from 'app/shared/models/billingType/get-all-billing-item-service';
import { BillingItemCategoryResource } from '../../../shared/resources/billing-item-category/billing-item-category.resource';
import * as _ from 'lodash';
import { GuidDefaultData } from '../../../shared/mock-data/guid-default-data';
import { ValidatorFormService } from '../../../shared/services/shared/validator-form.service';
import { BillingItemTax } from '../../../shared/models/billingType/billing-item-tax';
import {BillingItemServiceTaxAssociate} from '../../../shared/models/billingType/billing-item-service-tax-associate';

@Component({
  selector: 'tax-create-edit',
  templateUrl: './tax-create-edit.component.html',
  styleUrls: ['./tax-create-edit.component.css'],
  providers: [DatePipe],
})
export class TaxCreateEditComponent implements OnInit {

  public propertyId: number;
  public billingItemTaxForm: FormGroup;
  private taxId: number;
  public pageTitle: string;
  private getAllBillingItemServiceList: GetAllBillingItemService[];
  private serviceAssociateIndexToUpdate: number;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonSaveConfig: ButtonConfig;
  public buttonImplementConfig: ButtonConfig;
  public buttonCancelModalConfig: ButtonConfig;
  public buttonSaveModalConfig: ButtonConfig;

  // Table dependencies
  public columns: Array<any>;
  public taxServiceAssociateList: BillingItemTaxServiceAssociate[] = [];
  private readonly NOT_FOUND = -1;
  public itemsOption: Array<any>;

  // Modal Dependencies
  public billingItemTaxServiceAssociateForm: FormGroup;
  public readonly MODAL_ASSOCIATE = 'modalAssociate';

  // Select dependencies
  public groupList: SelectObjectOption[];
  public serviceAssociateOptionList: SelectObjectOption[];

  constructor(private formBuilder: FormBuilder,
              private location: Location,
              private modalEmitToggleService: ModalEmitToggleService,
              private dateService: DateService,
              private sharedService: SharedService,
              private billingItemTaxResource: BillingItemTaxResource,
              private billingItemTypeMapper: BillingItemTypeMapper,
              private toasterEmitService: ToasterEmitService,
              public route: ActivatedRoute,
              private commomService: CommomService,
              public router: Router,
              private billingItemCategoryResource: BillingItemCategoryResource,
              private translateService: TranslateService,
              private validatorFormService: ValidatorFormService
  ) {
  }

  ngOnInit() {
    this.setVars();
  }

  private isEditPage() {
    return this.taxId;
  }

  private setVars() {
    this.propertyId = +this.route.snapshot.params.property;
    this.taxId = +this.route.snapshot.params.id;
    this.setForms();
    this.setModalForm();
    this.taxServiceAssociateList = [];
    this.setButtonConfig();
    this.getAllCategoryListForItem();
    this.setConfigTable();
    this.getServiceAssociateListToSelect();

    if (this.isEditPage()) {
      this.pageTitle = 'entryTypeModule.tax.createEdit.header.editTitle';
      this.getTaxById(this.taxId);
    } else {
      this.pageTitle = 'entryTypeModule.tax.createEdit.header.title';
    }

    this.itemsOption = [
      { title: 'commomData.edit', callbackFunction: this.goToEdit },
      { title: 'commomData.delete', callbackFunction: this.removeItem }
    ];
  }

  private setForms() {
    this.billingItemTaxForm = this.formBuilder.group({
      isActive: [true, Validators.required],
      description: [null, Validators.required],
      id: 0,
      group: [null, Validators.required],
      integrationCode: [null]
    });

    this.billingItemTaxForm.valueChanges.subscribe(() => {
      this.buttonSaveConfig.isDisabled = this.billingItemTaxForm.invalid;
    });
  }

  private setModalForm() {
    this.billingItemTaxServiceAssociateForm = this.formBuilder.group({
      id: [GuidDefaultData],
      description: null,
      isActive: [true, Validators.required],
      beginDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]],
      taxPercentage: [null, Validators.required],
      selectValueService: this.formBuilder.group({
        serviceAssociate: [null, Validators.required],
      }),
    }, {
      validator: [
        this.validatorFormService.endDateIsMajorThanBeginDate,
        this.samePeriodAndType
      ]
    });

    this.billingItemTaxServiceAssociateForm.valueChanges.subscribe(() => {
      this.buttonSaveModalConfig.isDisabled = this.billingItemTaxServiceAssociateForm.invalid;
    });
  }

  private setConfigTable() {
    this.setColumnsName();
  }

  private setColumnsName() {
    this.columns = [
      { name: 'label.description', prop: 'name' },
      { name: 'label.group', prop: 'categoryName' },
      { name: 'label.taxPercentage', prop: 'strServicePercentage' },
      { name: 'label.validityBegin', prop: 'strBeginDate' },
      { name: 'label.validityEnd', prop: 'strEndDate' }
    ];
  }

  private setServiceAssociatedList(list: Array<BillingItemTaxServiceAssociate>) {
    this.taxServiceAssociateList = [...this.prepareList(list)];
  }

  private prepareList(list): any {
    return list.map(item => {
      item['strBeginDate'] = this.dateService.removeHourFromUniversalDate(item.beginDate);
      item['strEndDate'] = this.dateService.removeHourFromUniversalDate(item.endDate);
      item['strServicePercentage'] = item.taxPercentage + '%';
      return item;
    });
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'tax-create-edit-cancel',
      () => this.location.back(),
      'commomData.cancel',
      ButtonType.Secondary
    );
    this.buttonSaveConfig = this.sharedService.getButtonConfig(
      'tax-create-edit-save',
      this.saveTaxWithServiceAssociate,
      'commomData.confirm',
      ButtonType.Primary,
    );
    this.buttonImplementConfig = this.sharedService.getButtonConfig(
      'tax-create-edit-implementation',
      this.addServiceAssociate,
      'entryTypeModule.tax.createEdit.header.buttonText',
      ButtonType.Secondary,
      ButtonSize.Small,
    );
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'tax-create-edit-cancel',
      this.toggleModal,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveModalConfig = this.sharedService.getButtonConfig(
      'tax-modal-update-service',
      this.saveAssociateService,
      'commomData.confirm',
      ButtonType.Primary,
    );
  }

  public toggleModal = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_ASSOCIATE);
    this.billingItemTaxServiceAssociateForm.patchValue({
      id: GuidDefaultData,
      description: null,
      isActive: true,
      beginDate: null,
      endDate: null,
      taxPercentage: null,
      selectValueService: {
        serviceAssociate: null,
      }
    });
  }

  private addServiceAssociate = () => {
    this.serviceAssociateIndexToUpdate = this.NOT_FOUND;
    this.toggleModal();
  }

  public goToEdit = (event, context, index) => {
    this.serviceAssociateIndexToUpdate = index;
    this.toggleModal();
    this.setBillingItemTaxServiceAssociateForm(event);
  }

  public saveAssociateService = () => {
    this.serviceAssociateIndexToUpdate == this.NOT_FOUND ?
      this.createAssociateService() :
      this.editAssociateService();

    this.toggleModal();
  }

  public removeItem = (event) => {
    const billingItemService = event;
    if (this.sharedService.elementExistsInList(billingItemService, this.taxServiceAssociateList)) {
      this.sharedService.removeElementFromList(billingItemService, this.taxServiceAssociateList);
      this.taxServiceAssociateList = [...this.taxServiceAssociateList];
    }
  }

  public updateStatusServiceAssociated(service: BillingItemTaxServiceAssociate) {
    this.billingItemTaxServiceAssociateForm.get('isActive').setValue(service.isActive);
  }

  public saveTaxWithServiceAssociate = () => {
    const billingItemTax = this.billingItemTypeMapper.toBillingItemTax(this.billingItemTaxForm, this.taxServiceAssociateList);
    billingItemTax.propertyId = this.propertyId;
    if (billingItemTax.id) {
      this.billingItemTaxResource.updateBillingItemTaxWithService(billingItemTax).subscribe(response => {
        this.router.navigate(['../../'], {relativeTo: this.route})
          .then(() => {
            this.toasterEmitService.emitChange(SuccessError.success, 'entryTypeModule.tax.commomData.updateMessageSuccess');
          });
      });
    } else {
      this.billingItemTaxResource.saveBillingItemTaxAndServicesAssociated(billingItemTax).subscribe(response => {
        this.router.navigate(['../'], {relativeTo: this.route})
          .then(() => {
            this.toasterEmitService.emitChange(SuccessError.success, 'entryTypeModule.tax.commomData.saveMessageSuccess');
          });
      });
    }
  }

  private getTaxById(taxId: number) {
    this.billingItemTaxResource.getBillingItemTaxServicesAssociated(taxId, this.propertyId).subscribe(response => {
      const billingItemTax: BillingItemTax = response;
      this.billingItemTaxForm.patchValue({
        isActive: billingItemTax.isActive,
        description: billingItemTax.name,
        group: billingItemTax.billingItemCategoryId,
        id: billingItemTax.id,
        integrationCode: billingItemTax.integrationCode
      });
      this.setServiceAssociatedList(billingItemTax.serviceAndTaxList);
    });
  }

  private setBillingItemTaxServiceAssociateForm(billingItemTaxServiceAssociate: BillingItemTaxServiceAssociate) {
    this.billingItemTaxServiceAssociateForm.patchValue({
      id: billingItemTaxServiceAssociate.id ? billingItemTaxServiceAssociate.id : GuidDefaultData,
      description: billingItemTaxServiceAssociate.name,
      isActive: billingItemTaxServiceAssociate.isActive,
      beginDate: billingItemTaxServiceAssociate.beginDate,
      endDate: billingItemTaxServiceAssociate.endDate,
      taxPercentage: billingItemTaxServiceAssociate.taxPercentage,
      selectValueService: {
        serviceAssociate: billingItemTaxServiceAssociate.billingItemServiceId,
      }
    });
  }

  private editAssociateService() {
    const editBillingItemTaxToAssociate = this.getAllBillingItemServiceList.find(
      taxAssociate =>
        taxAssociate.id == this.billingItemTaxServiceAssociateForm.get('selectValueService.serviceAssociate').value,
    );

    const taxChanged = this.billingItemTypeMapper
      .toSaveTaxWithServiceAssociated(this.billingItemTaxServiceAssociateForm, editBillingItemTaxToAssociate);
    this.taxServiceAssociateList.splice(this.serviceAssociateIndexToUpdate, 1, taxChanged);
    this.setServiceAssociatedList(this.taxServiceAssociateList);
  }

  private createAssociateService() {
    const newBillingItemTaxToAssociate = this.getAllBillingItemServiceList.find(
      taxAssociate =>
        taxAssociate.id == this.billingItemTaxServiceAssociateForm.get('selectValueService.serviceAssociate').value,
    );

    const tax = this.billingItemTypeMapper
      .toSaveTaxWithServiceAssociated(this.billingItemTaxServiceAssociateForm, newBillingItemTaxToAssociate);

    this.taxServiceAssociateList.push(tax);
    this.setServiceAssociatedList(this.taxServiceAssociateList);
  }

  private getServiceAssociateListToSelect() {
    this.serviceAssociateOptionList = [];
    this.getAllBillingItemServiceList = [];
    if (this.propertyId) {
      this.billingItemTaxResource.getServiceAssociateList(this.propertyId).subscribe(response => {
        if (response && !_.isEmpty(response.items)) {
          this.getAllBillingItemServiceList = response.items;
          this.serviceAssociateOptionList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
            response,
            'id',
            'billingItemName',
          );
        }
      });
    }
  }

  private getAllCategoryListForItem() {
    this.groupList = [];
    this.billingItemCategoryResource.getAllBillingItemCategoryActiveListByPropertyId(this.propertyId)
      .subscribe((response) => {
        if (response) {
          this.groupList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
            response,
            'id',
            'billingItemCategoryName'
          );
        }
      });
  }

  private samePeriodAndType = (form: AbstractControl) => {
    const hasTax = this.taxServiceAssociateList.some((t, index) => {
        if (this.serviceAssociateIndexToUpdate != index) {
          return t.billingItemServiceId == form.get('selectValueService.serviceAssociate').value &&
            (this.dateService
                .isBetweenInPeriod(form.get('beginDate').value, t.beginDate, t.endDate) ||
              this.dateService
                .isBetweenInPeriod(form.get('endDate').value, t.beginDate, t.endDate));
        }
        return false;
      }
    );
    return hasTax ? {hasServiceInPeriod: this.translateService.instant('alert.hasTaxInPeriod')} : null;
  }
}
