import {of as observableOf } from 'rxjs';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TaxCreateEditComponent } from './tax-create-edit.component';
import { ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { BillingItemTaxServiceAssociateData } from 'app/shared/mock-data/billing-item-tax-service-associate-data';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { BillingItemTaxData } from 'app/shared/mock-data/billing-item-tax-data';
import { PeriodData } from 'app/shared/mock-data/date-range-picker-data';

xdescribe('TaxCreateEditComponent', () => {
  let component: TaxCreateEditComponent;
  let fixture: ComponentFixture<TaxCreateEditComponent>;
  const messageSave = 'entryTypeModule.tax.createEdit.saveMessageSuccess';
  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        MyDateRangePickerModule,
      ],
      declarations: [TaxCreateEditComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(TaxCreateEditComponent);
    component = fixture.componentInstance;
    const itemsResponse = {
      items: [BillingItemTaxServiceAssociateData],
    };

    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    spyOn(component['toasterEmitService'], 'emitChange');
    spyOn(component['billingItemTypeMapper'], 'toBillingItemTax').and.returnValue(BillingItemTaxData);
    spyOn<any>(component['billingItemTaxResource'], 'getBillingItemTaxServicesAssociatedList')
        .and.returnValue(observableOf(itemsResponse));
    spyOn<any>(component['billingItemTaxResource'], 'saveBillingItemTaxAndServicesAssociated')
        .and.returnValue(observableOf(messageSave));
    spyOn(component['billingItemTaxResource'], 'updateBillingItemTaxWithService').and.returnValue(
      observableOf(BillingItemTaxServiceAssociateData),
    );
    spyOn(component['billingItemTaxResource'], 'getBillingItemTaxServicesAssociated')
        .and.returnValue(observableOf(BillingItemTaxData));
    spyOn<any>(component['billingItemTaxResource'], 'getServiceAssociateList')
        .and.returnValue(observableOf(itemsResponse));
    spyOn(component['dateService'], 'getDateRangePicker').and.returnValue(PeriodData);

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should setVars', () => {
      expect(component.pageTitle).toEqual('entryTypeModule.tax.createEdit.header.title');
      expect(component.propertyId).toEqual(1);
      // setForm
      expect(component.billingItemTaxForm.get('isActive').value).toBeTruthy();
      expect(component.billingItemTaxForm.get('description').value).toBeNull();
      expect(component.billingItemTaxServiceAssociateForm.get('isActive').value).toEqual(true);
      expect(component.billingItemTaxServiceAssociateForm.get('taxPercentage').value).toEqual(null);
      expect(component.billingItemTaxServiceAssociateForm.get('description').value).toEqual(null);
      expect(component.billingItemTaxServiceAssociateForm.get('period').value).toEqual(null);
      expect(component.billingItemTaxServiceAssociateForm.get('selectValueSevice.serviceAssociate').value).toEqual(null);
      // setConfigTable
      expect(component.columns).toEqual([
        { name: 'label.description', prop: 'name' },
        { name: 'label.group', prop: 'categoryName' },
        { name: 'label.taxPercentage', prop: 'strTaxPercentage' },
        { name: 'label.validityBegin', prop: 'strBeginDate' },
        { name: 'label.validityEnd', prop: 'strEndDate' }
      ]);
      // setButtonConfig
      expect(component.buttonSaveConfig.id).toEqual('tax-create-edit-save');
      expect(component.buttonSaveConfig.textButton).toEqual('commomData.confirm');
      expect(component.buttonSaveConfig.buttonType).toEqual(ButtonType.Primary);
      expect(component.buttonCancelConfig.id).toEqual('tax-create-edit-cancel');
      expect(component.buttonCancelConfig.textButton).toEqual('commomData.cancel');
      expect(component.buttonCancelConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.buttonImplementConfig.id).toEqual('tax-create-edit-implementation');
      expect(component.buttonImplementConfig.textButton).toEqual('entryTypeModule.tax.createEdit.header.buttonText');
      expect(component.buttonImplementConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.buttonImplementConfig.buttonSize).toEqual(ButtonSize.Small);
      expect(component.buttonCancelModalConfig.id).toEqual('tax-create-edit-cancel');
      expect(component.buttonCancelModalConfig.textButton).toEqual('commomData.cancel');
      expect(component.buttonCancelModalConfig.callback).toEqual(component.toggleModal);
      expect(component.buttonCancelModalConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.buttonSaveModalConfig.id).toEqual('tax-create-edit-save');
      expect(component.buttonSaveModalConfig.textButton).toEqual('commomData.confirm');
      expect(component.buttonSaveModalConfig.buttonType).toEqual(ButtonType.Primary);
    });
  });

  describe('on methods', () => {
    it('should toggleModal', () => {
      component.toggleModal();
      expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('newBillingItemTaxServiceAssociate');
    });

    it('should saveTaxWithServiceAssociate', () => {
      spyOn(component.router, 'navigate');

      component.saveTaxWithServiceAssociate();
      expect(component['billingItemTaxResource'].saveBillingItemTaxAndServicesAssociated).toHaveBeenCalledWith(BillingItemTaxData);
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(SuccessError.success, messageSave);
      expect(component.router.navigate).toHaveBeenCalledWith(['../'], { relativeTo: component['route'] });
    });
  });

  describe('on editPage', () => {
    beforeEach(() => {
      spyOn<any>(component.route.params, 'subscribe').and.callFake(success => {
        success({ taxId: '1' });
      });
      component['ngOnInit']();
    });

    it('should setVars edit', () => {
      fixture.whenStable().then(() => {
        expect(component.buttonSaveConfig.id).toEqual('tax-create-edit-save');
        expect(component.buttonSaveConfig.textButton).toEqual('commomData.confirm');
        expect(component.buttonSaveConfig.buttonType).toEqual(ButtonType.Primary);
        expect(component.buttonSaveConfig.callback).toEqual(component['saveChangesAtBillingItemTaxServiceAssociate']);
        expect(component['taxId']).toEqual(1);
        expect(component.pageTitle).toEqual('entryTypeModule.tax.createEdit.header.editTitle');
        expect(component.billingItemTaxForm.get('isActive').value).toBeTruthy();
        expect(component.billingItemTaxForm.get('description').value).toEqual(BillingItemTaxData.name);
        expect(component.taxServiceAssociateList).toBeTruthy(BillingItemTaxData.serviceAndTaxList);
      });
    });
  });
});
