
import {of as observableOf,  Observable } from 'rxjs';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http/';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GroupCreateEditComponent } from './group-create-edit.component';
// Helpers
import { ButtonType } from '../../../shared/models/button-config';
import { BillingItemCategory } from '../../../shared/models/billingType/billing-item-category';
import { BillingItemCategoryData, BillingItemCategoryData1 } from '../../../shared/mock-data/billing-type-group-data';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('GroupCreateEditComponent', () => {
  let component: GroupCreateEditComponent;
  let fixture: ComponentFixture<GroupCreateEditComponent>;
  const billingItemCategoryFixedList = new Array<BillingItemCategory>();
  billingItemCategoryFixedList.push(BillingItemCategoryData1);
  const response = {
    total: 1,
    items: billingItemCategoryFixedList,
    hasNext: false,
  };

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), FormsModule, ReactiveFormsModule, RouterTestingModule, HttpClientModule],
      declarations: [GroupCreateEditComponent],
      providers: [ InterceptorHandlerService ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(GroupCreateEditComponent);
    component = fixture.componentInstance;

    spyOn(component['billingItemCategoryResource'], 'getBillingItemCategoryById').and.returnValue(observableOf(BillingItemCategoryData));
    spyOn(component['billingItemCategoryResource'], 'getBillingItemCategoryFixedList').and.returnValue(observableOf(response));
    spyOn<any>(component['route'].params, 'subscribe').and.callFake(success => {
      success({ id: '1' });
    });

    fixture.detectChanges();
  });

  describe('on init', () => {
    it('should create', () => {
      expect(component.configHeaderPage).toEqual({ hasBackPreviewPage: true });
      expect(Object.keys(component.billingItemCategoryForm['controls'])).toEqual(['standardCategoryId', 'categoryName', 'isActive']);
      expect(component.buttonCancelConfig).not.toBeNull();
      expect(component.buttonCancelConfig.id).toEqual('group-create-edit-cancel');
      expect(component.buttonCancelConfig.textButton).toEqual('commomData.cancel');
      expect(component.buttonCancelConfig.buttonType).toEqual(ButtonType.Secondary);
      expect(component.billingItemCategoryFixedList).toEqual([
        { key: BillingItemCategoryData1.id, value: BillingItemCategoryData1.categoryName },
      ]);
      expect(component['propertyId']).toEqual(BillingItemCategoryData.propertyId);
      expect(component.billingItemCategoryForm.get('categoryName').value).toEqual(BillingItemCategoryData.categoryName);
      expect(component.billingItemCategoryForm.get('standardCategoryId').value).toEqual(BillingItemCategoryData.standardCategoryId);
      expect(component.billingItemCategoryForm.get('isActive').value).toEqual(BillingItemCategoryData.isActive);
    });
  });

  describe('on saveBillingItemCategory', () => {
    beforeAll(() => {
      component.billingItemCategoryForm.get('categoryName').setValue(BillingItemCategoryData.categoryName);
      component.billingItemCategoryForm.get('standardCategoryId').setValue(BillingItemCategoryData.standardCategoryId);
      component.billingItemCategoryForm.get('isActive').setValue(BillingItemCategoryData.isActive);
    });

    it(
      'should create a new BillingItemCategory',
      fakeAsync(() => {
        spyOn<any>(component['billingItemCategoryResource'], 'createBillingItemCategory')
            .and.returnValue(observableOf({}));
        spyOn(component['toasterEmitService'], 'emitChange');
        spyOn(component['router'], 'navigate');
        component.billingItemCategoryId = null;

        // tick();
        component.saveBillingItemCategory();

        expect(component['billingItemCategoryResource'].createBillingItemCategory).toHaveBeenCalledWith({
          standardCategoryId: BillingItemCategoryData.standardCategoryId,
          categoryName: BillingItemCategoryData.categoryName,
          isActive: BillingItemCategoryData.isActive,
          propertyId: component['propertyId'],
        });
        expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
          SuccessError.success,
          'entryTypeModule.group.commomData.saveMessageSuccess',
        );
        expect(component['router'].navigate).toHaveBeenCalledWith(['../'], { relativeTo: component['route'] });
      }),
    );

    it(
      'should update a BillingItemCategory',
      fakeAsync(() => {
        spyOn<any>(component['billingItemCategoryResource'], 'updateBillingItemCategory')
            .and.returnValue(observableOf({}));
        spyOn(component['toasterEmitService'], 'emitChange');
        spyOn(component['router'], 'navigate');
        component.billingItemCategoryId = 1;

        // tick();
        component.saveBillingItemCategory();

        expect(component['billingItemCategoryResource'].updateBillingItemCategory).toHaveBeenCalledWith(component.billingItemCategoryId, {
          standardCategoryId: BillingItemCategoryData.standardCategoryId,
          categoryName: BillingItemCategoryData.categoryName,
          isActive: BillingItemCategoryData.isActive,
          propertyId: component['propertyId'],
        });
        expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
          SuccessError.success,
          'entryTypeModule.group.commomData.updateMessageSuccess',
        );
        expect(component['router'].navigate).toHaveBeenCalledWith(['../../'], { relativeTo: component['route'] });
      }),
    );
  });
});
