import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { SelectObjectOption } from '../../../shared/models/selectModel';

// Helpers
import { BillingItemCategoryMapper } from '../../../shared/mappers/billing-item-category-mapper';
import { NoWhitespaceValidator } from '../../../shared/validators/no-whitespace-validator';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { BillingItemCategoryResource } from '../../../shared/resources/billing-item-category/billing-item-category.resource';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { CommomService } from '../../../shared/services/shared/commom.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-group-create-edit',
  templateUrl: './group-create-edit.component.html',
})
export class GroupCreateEditComponent implements OnInit {
  private propertyId: number;
  public configHeaderPage: ConfigHeaderPageNew;
  public billingItemCategoryForm: FormGroup;
  public billingItemCategoryFixedList: Array<SelectObjectOption>;
  public billingItemCategoryId: number;

  public buttonCancelConfig: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private billingItemCategoryResource: BillingItemCategoryResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private billingItemCategoryMapper: BillingItemCategoryMapper,
    private route: ActivatedRoute,
    private router: Router,
    private commomService: CommomService,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars(): void {
    this.setForm();
    this.setConfigHeaderPage();
    this.setButtonConfig();
    this.getBillingItemCategoryFixedList();
    this.route.params.subscribe(params => {
      this.propertyId = +params['property'];
      this.billingItemCategoryId = +params['id'];
      if (this.billingItemCategoryId) {
        this.getBillingItemCategory();
      }
    });
  }

  private setConfigHeaderPage(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
    };
  }

  private setForm(): void {
    this.billingItemCategoryForm = this.formBuilder.group({
      standardCategoryId: [null, [Validators.required]],
      categoryName: ['', [Validators.required, NoWhitespaceValidator.match]],
      isActive: true,
    });
  }

  private setButtonConfig(): void {
    this.buttonCancelConfig = new ButtonConfig();
    this.buttonCancelConfig.id = 'group-create-edit-cancel';
    this.buttonCancelConfig.callback = () => {
      this.location.back();
    };
    this.buttonCancelConfig.textButton = 'commomData.cancel';
    this.buttonCancelConfig.buttonType = ButtonType.Secondary;
  }

  private getBillingItemCategoryFixedList(): void {
    this.billingItemCategoryFixedList = [];
    this.billingItemCategoryResource.getBillingItemCategoryFixedList().subscribe(response => {
      if (response && !_.isEmpty(response.items)) {
        this.billingItemCategoryFixedList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
          response,
          'id',
          'categoryName',
        );
      }
    });
  }

  private getBillingItemCategory(): void {
    this.billingItemCategoryResource.getBillingItemCategoryById(this.billingItemCategoryId).subscribe(billingItemCategory => {
      this.propertyId = billingItemCategory.propertyId;
      this.billingItemCategoryForm = this.billingItemCategoryMapper.getFormGroupByBillingItemCategoryToSendServer(
        this.billingItemCategoryForm,
        billingItemCategory,
      );
    });
  }

  public saveBillingItemCategory(): void {
    const billingItemCategory = this.billingItemCategoryMapper.getBillingItemCategoryByFormGroupToSendServer(this.billingItemCategoryForm);
    billingItemCategory.propertyId = this.propertyId;
    if (this.billingItemCategoryId) {
      this.billingItemCategoryResource.updateBillingItemCategory(this.billingItemCategoryId, billingItemCategory).subscribe(response => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('entryTypeModule.group.commomData.updateMessageSuccess'),
        );
        this.router.navigate(['../../'], { relativeTo: this.route });
      });
    } else {
      this.billingItemCategoryResource.createBillingItemCategory(billingItemCategory).subscribe(response => {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('entryTypeModule.group.commomData.saveMessageSuccess'),
        );
        this.router.navigate(['../'], { relativeTo: this.route });
      });
    }
  }
}
