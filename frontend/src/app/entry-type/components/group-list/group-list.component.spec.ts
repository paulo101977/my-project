
import {of as observableOf,  Observable } from 'rxjs';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http/';
import { GroupListComponent } from './group-list.component';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { BillingItemCategoryData, BillingItemCategoryInactiveData } from '../../../shared/mock-data/billing-type-group-data';
import { BillingItemCategory } from '../../../shared/models/billingType/billing-item-category';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { InterceptorHandlerService } from '@inovacaocmnet/thx-bifrost';

describe('GroupListComponent', () => {
  let component: GroupListComponent;
  let fixture: ComponentFixture<GroupListComponent>;
  const pageItemsList = new Array<BillingItemCategory>();
  const response = {
    items: pageItemsList,
  };

  beforeAll(() => {
    TestBed.resetTestEnvironment();

    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      imports: [TranslateModule.forRoot(), RouterTestingModule, HttpClientModule],
      declarations: [GroupListComponent],
      providers: [ InterceptorHandlerService ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(GroupListComponent);
    component = fixture.componentInstance;

    spyOn<any>(component['billingItemCategoryResource'], 'getAllBillingItemCategoryListByPropertyId')
        .and.returnValue(observableOf(response));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on init', () => {
    it('should set configHeaderPage, setConfigTable and getList', () => {
      expect(component.configHeaderPage).toEqual({
        keepTitleButton: true,
        keepButtonActive: true,
        callBackFunction: component.goToNewPage,
      });

      expect(component.columns).toEqual([
        {
          name: 'label.groupDescription',
          prop: 'categoryName',
        },
        {
          name: 'label.fixedGroup',
          prop: 'categoryFixedName',
        },
      ]);
      expect(component.pageItemsList).toEqual(pageItemsList);
      expect(component.searchableItems.items).toEqual(response.items);
      expect(component.searchableItems.placeholderSearchFilter).toEqual('entryTypeModule.group.pagelist.header.placeholderFilter');
      expect(component.searchableItems.tableRowTypeEnum).toEqual(TableRowTypeEnum.BillingTypeCategory);
    });
  });

  describe('on methods', () => {
    it('should goToNewPage', () => {
      spyOn(component['router'], 'navigate');

      component['goToNewPage']();

      expect(component['router'].navigate).toHaveBeenCalledWith(['new'], { relativeTo: component['route'] });
    });

    it(
      'should goToEditPage',
      fakeAsync(() => {
        const item = { id: '9ae937da-4df3-4f2a-843d-b49286f41dfd' };

        spyOn(component.router, 'navigate');
        component.goToEditPage(item);
        expect(component.router.navigate).toHaveBeenCalledWith(['edit', item.id], { relativeTo: component['route'] });
      }),
    );

    it('should updateList', () => {
      const list = new Array<BillingItemCategory>();
      list.push(BillingItemCategoryData);

      component.updateList(list);

      expect(component.pageItemsList).toEqual(list);
    });

    it('should confirmDeleteItem', () => {
      spyOn(component['modalEmitService'], 'emitSimpleModal');
      component.confirmDeleteItem(BillingItemCategoryData);

      expect(component['billingItemCategoryToBeDeleted']).toEqual(BillingItemCategoryData);
      expect(component['modalEmitService'].emitSimpleModal).toHaveBeenCalledWith(
        'commomData.delete',
        'entryTypeModule.group.commomData.confirmDeleteMessage' + ' ' + component['billingItemCategoryToBeDeleted'].categoryName + '?',
        component.deleteItem,
        [],
      );
    });

    it('should updateStatus when isActive is true', () => {
      spyOn<any>(component['billingItemCategoryResource'], 'updateBillingItemCategoryStatus')
          .and.returnValue(observableOf({}));
      spyOn(component['toasterEmitService'], 'emitChange');
      BillingItemCategoryData.isActive = true;

      component.updateStatus(BillingItemCategoryData);

      expect(component['billingItemCategoryResource'].updateBillingItemCategoryStatus).toHaveBeenCalledWith(BillingItemCategoryData.id);
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'entryTypeModule.group.commomData.activatedWithSuccess',
      );
    });

    it('should updateStatus when isActive is false', () => {
      spyOn<any>(component['billingItemCategoryResource'], 'updateBillingItemCategoryStatus')
          .and.returnValue(observableOf({}));
      spyOn(component['toasterEmitService'], 'emitChange');

      component.updateStatus(BillingItemCategoryInactiveData);

      expect(component['billingItemCategoryResource'].updateBillingItemCategoryStatus).toHaveBeenCalledWith(
        BillingItemCategoryInactiveData.id,
      );
      expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
        SuccessError.success,
        'entryTypeModule.group.commomData.inactivatedWithSuccess',
      );
    });
  });
});
