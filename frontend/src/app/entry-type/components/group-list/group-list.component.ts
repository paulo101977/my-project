import { Component, OnInit } from '@angular/core';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ConfigHeaderPageNew } from '../../../shared/components/header-page-new/config-header-page-new';
import { BillingItemCategoryData, BillingItemCategoryData1 } from '../../../shared/mock-data/billing-type-group-data';
import { BillingItemCategory } from '../../../shared/models/billingType/billing-item-category';
import { TableRowTypeEnum } from '../../../shared/models/table-row-type-enum';
import { Column } from '../../../shared/models/table/column';
import { SearchableItems } from '../../../shared/models/filter-search/searchable-item';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { ModalEmitService } from '../../../shared/services/shared/modal-emit.service';
import { BillingItemCategoryResource } from '../../../shared/resources/billing-item-category/billing-item-category.resource';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css'],
})
export class GroupListComponent implements OnInit {
  public propertyId: number;
  public configHeaderPage: ConfigHeaderPageNew;
  public searchableItems: SearchableItems;
  private billingItemCategoryToBeDeleted: BillingItemCategory;

  // Table dependencies
  public columns: Array<any>;
  public pageItemsList: Array<BillingItemCategory>;
  public itemsOption: Array<any>;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private billingItemCategoryResource: BillingItemCategoryResource,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
    private modalEmitService: ModalEmitService,
  ) {}

  ngOnInit() {
    this.setVars();
  }

  private setVars() {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.setConfigTable();
      this.setConfigHeaderPage();
      this.getList();
      this.setSearchableItems();

      this.itemsOption = [
        { title: 'commomData.edit', callbackFunction: this.goToEditPage, context: this },
        { title: 'commomData.delete', callbackFunction: this.confirmDeleteItem, context: this },
      ];
    });
  }

  private setConfigHeaderPage() {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      keepTitleButton: true,
      keepButtonActive: true,
      callBackFunction: this.goToNewPage,
    };
  }

  private setConfigTable() {
    this.setColumnsName();
  }

  private setColumnsName() {
    this.columns = [];
    this.columns.push({
      name: 'label.groupDescription',
      prop: 'categoryName',
    });
    this.columns.push({
      name: 'label.fixedGroup',
      prop: 'categoryFixedName',
    });
  }

  private getList() {
    this.pageItemsList = new Array<BillingItemCategory>();
    this.billingItemCategoryResource.getAllBillingItemCategoryListByPropertyId(this.propertyId).subscribe(response => {
      if (response) {
        this.pageItemsList = response.items;
      }
      this.setSearchableItems();
    });
  }

  public goToNewPage = () => {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

  public goToEditPage(item, context?: GroupListComponent) {
    if (!context) {
      context = this;
    }
    context.router.navigate(['edit', item.id], { relativeTo: context.route });
  }

  private setSearchableItems(): void {
    this.searchableItems = new SearchableItems();
    this.searchableItems.items = this.pageItemsList;
    this.searchableItems.placeholderSearchFilter = 'entryTypeModule.group.pagelist.header.placeholderFilter';
    this.searchableItems.tableRowTypeEnum = TableRowTypeEnum.BillingTypeCategory;
  }

  public updateList(items: Array<BillingItemCategory>) {
    if (items) {
      this.pageItemsList = items;
    }
  }

  public updateStatus(billingItemCategory: BillingItemCategory) {
    this.billingItemCategoryResource.updateBillingItemCategoryStatus(billingItemCategory.id).subscribe(response => {
      if (billingItemCategory.isActive) {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('entryTypeModule.group.commomData.activatedWithSuccess'),
        );
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('entryTypeModule.group.commomData.inactivatedWithSuccess'),
        );
      }
    });
  }

  public confirmDeleteItem(billingItemCategory: BillingItemCategory, context?: GroupListComponent) {
    if (!context) {
      context = this;
    }
    context.billingItemCategoryToBeDeleted = billingItemCategory;
    context.modalEmitService.emitSimpleModal(
      context.translateService.instant('commomData.delete'),
      context.translateService.instant('entryTypeModule.group.commomData.confirmDeleteMessage') +
        ' ' +
        context.billingItemCategoryToBeDeleted.categoryName +
        '?',
      context.deleteItem,
      [],
    );
  }

  public deleteItem = () => {
    this.billingItemCategoryResource.deleteBillingItemCategory(this.billingItemCategoryToBeDeleted.id).subscribe(response => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('entryTypeModule.group.commomData.deleteMessage'),
      );
      this.getList();
    });
  }
}
