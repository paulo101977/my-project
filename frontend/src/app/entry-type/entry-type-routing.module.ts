import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServiceListComponent } from './components/service-list/service-list.component';
import { ServiceCreateEditComponent } from './components/service-create-edit/service-create-edit.component';
import { TaxListComponent } from './components/tax-list/tax-list.component';
import { TaxCreateEditComponent } from 'app/entry-type/components/tax-create-edit/tax-create-edit.component';
import { GroupListComponent } from './components/group-list/group-list.component';
import { GroupCreateEditComponent } from './components/group-create-edit/group-create-edit.component';

export const entryTypeRoutes: Routes = [
  {
    path: 'service',
    children: [
      { path: '', component: ServiceListComponent },
      { path: 'new', component: ServiceCreateEditComponent },
      { path: 'edit/:id', component: ServiceCreateEditComponent },
    ],
  },
  {
    path: 'tax',
    children: [
      { path: '', component: TaxListComponent },
      { path: 'new', component: TaxCreateEditComponent },
      { path: 'edit/:id', component: TaxCreateEditComponent },
    ],
  },
  {
    path: 'group',
    children: [
      { path: '', component: GroupListComponent },
      { path: 'new', component: GroupCreateEditComponent },
      { path: 'edit/:id', component: GroupCreateEditComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(entryTypeRoutes)],
  exports: [RouterModule],
})
export class EntryTypeRoutingModule {}
