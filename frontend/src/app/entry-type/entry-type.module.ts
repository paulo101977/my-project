import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { ServiceListComponent } from './components/service-list/service-list.component';
import { ServiceCreateEditComponent } from './components/service-create-edit/service-create-edit.component';
import { GroupCreateEditComponent } from './components/group-create-edit/group-create-edit.component';
import { TaxCreateEditComponent } from './components/tax-create-edit/tax-create-edit.component';
import { TaxListComponent } from './components/tax-list/tax-list.component';
import { GroupListComponent } from './components/group-list/group-list.component';
import { RouterModule } from '@angular/router';
import { EntryTypeRoutingModule } from './entry-type-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    MyDateRangePickerModule,
    MyDatePickerModule,
    EntryTypeRoutingModule
  ],
  declarations: [
    ServiceListComponent,
    ServiceCreateEditComponent,
    GroupCreateEditComponent,
    TaxCreateEditComponent,
    TaxListComponent,
    GroupListComponent,
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class EntryTypeModule {}
