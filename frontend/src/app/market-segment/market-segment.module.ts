import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { MarketSegmentListComponent } from './components/market-segment-list/market-segment-list.component';
import { MarketSementRoutingModule } from './market-segment-routing.module';

@NgModule({
  imports: [CommonModule, SharedModule, MarketSementRoutingModule],
  declarations: [MarketSegmentListComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class MarketSegmentModule {}
