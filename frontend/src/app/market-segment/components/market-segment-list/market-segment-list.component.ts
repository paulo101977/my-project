import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { MarketSegment } from '../../../shared/models/dto/market-segment';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ButtonConfig } from '../../../shared/models/button-config';
import { ButtonType } from '../../../shared/models/button-config';
import { ButtonSize } from '../../../shared/models/button-config';
import { SelectObjectOption } from '../../../shared/models/selectModel';
import { SuccessError } from '../../../shared/models/success-error-enum';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { ModalEmitToggleService } from '../../../shared/services/shared/modal-emit-toggle.service';
import { CommomService } from '../../../shared/services/shared/commom.service';
import { ToasterEmitService } from '../../../shared/services/shared/toaster-emit.service';
import { MarketSegmentResource } from '../../../shared/resources/market-segment/market-segment.resource';
import { NumberRangeValidator } from '../../../shared/validators/number-range-validator';
import { NoWhitespaceValidator } from '../../../shared/validators/no-whitespace-validator';

@Component({
  selector: 'app-market-segment-list',
  templateUrl: './market-segment-list.component.html',
  styleUrls: ['./market-segment-list.component.css'],
})
export class MarketSegmentListComponent implements OnInit {
  @ViewChild('myTable') table: DatatableComponent;

  private propertyId: number;

  // Button dependencies
  public buttonNewPrimarySegmentConfig: ButtonConfig;
  public buttonNewSecondarySegmentConfig: ButtonConfig;
  public buttonCancelPrimarySegmentModalConfig: ButtonConfig;
  public buttonSavePrimarySegmentModalConfig: ButtonConfig;
  public buttonCancelSecondarySegmentModalConfig: ButtonConfig;
  public buttonSaveSecondarySegmentModalConfig: ButtonConfig;
  public buttonCancelRemoveConfig: ButtonConfig;
  public buttonConfirmRemoveConfig: ButtonConfig;

  // Modal dependencies
  public formModalNewPrimary: FormGroup;
  public formModalNewSecondary: FormGroup;

  // Select dependencies
  public primarySegmentOptionList: Array<SelectObjectOption>;
  public parentSegmentCodeChoosen: string;
  public marketSegmentToRemove: MarketSegment;

  rows: MarketSegment[] = [];
  expanded: any = {};
  widthColumns = Array<number>();
  itemsOption: Array<any>;

  constructor(
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private modalEmitToggleService: ModalEmitToggleService,
    private marketSegmentResource: MarketSegmentResource,
    private commomService: CommomService,
    private toasterEmitService: ToasterEmitService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.setButtonConfig();
      this.getSegmentList();
      this.setFormModalPrimarySegment();
      this.setFormModalSecondarySegment();
      this.itemsOption = [{ title: 'commomData.edit' }, { title: 'commomData.delete' }];
    });
  }

  private setButtonConfig() {
    this.buttonNewPrimarySegmentConfig = this.sharedService.getButtonConfig(
      'market-primary-segment-new',
      this.toggleNewPrimary,
      'marketSegmentModule.listPage.header.buttonNewPrimarySegment',
      ButtonType.Secondary,
      ButtonSize.Small,
    );
    this.buttonNewSecondarySegmentConfig = this.sharedService.getButtonConfig(
      'market-secondary-segment-new',
      this.toggleNewSecondary,
      'marketSegmentModule.listPage.header.buttonNewSecondarySegment',
      ButtonType.Secondary,
      ButtonSize.Small,
    );
    this.buttonCancelPrimarySegmentModalConfig = this.sharedService.getButtonConfig(
      'market-primary-segment-cancel-modal',
      this.toggleNewPrimary,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSavePrimarySegmentModalConfig = this.sharedService.getButtonConfig(
      'market-primary-segment-save-modal',
      this.savePrimarySegment,
      'commomData.confirm',
      ButtonType.Primary,
      null,
      true,
    );
    this.buttonCancelSecondarySegmentModalConfig = this.sharedService.getButtonConfig(
      'market-secondary-segment-cancel-modal',
      this.toggleNewSecondary,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveSecondarySegmentModalConfig = this.sharedService.getButtonConfig(
      'market-secondary-segment-save-modal',
      this.saveSecondarySegment,
      'commomData.confirm',
      ButtonType.Primary,
      null,
      true,
    );
    this.buttonCancelRemoveConfig = this.sharedService.getButtonConfig(
      'market-remove-segment-cancel-modal',
      this.toggleRemoveModal,
      'commomData.not',
      ButtonType.Secondary,
    );
    this.buttonConfirmRemoveConfig = this.sharedService.getButtonConfig(
      'market-remove-segment-confirm-modal',
      this.deleteRow,
      'commomData.yes',
      ButtonType.Primary,
    );
  }

  private setFormModalPrimarySegment() {
    this.formModalNewPrimary = this.formBuilder.group({
      id: 0,
      isActive: true,
      name: [null, [Validators.required, NoWhitespaceValidator.match]],
      abbreviation: [null, [Validators.required, NoWhitespaceValidator.match]],
      code: [null, [Validators.required]],
    });

    this.formModalNewPrimary.valueChanges.subscribe(value => {
      this.buttonSavePrimarySegmentModalConfig.isDisabled = this.formModalNewPrimary.invalid;
    });
  }

  private setFormModalSecondarySegment() {
    this.formModalNewSecondary = this.formBuilder.group({
      id: 0,
      isActive: true,
      name: [null, [Validators.required, NoWhitespaceValidator.match]],
      abbreviation: [null, [Validators.required, NoWhitespaceValidator.match]],
      primary: [null, Validators.required],
      code: [null, [Validators.required, NumberRangeValidator.range(0, 99)]],
    });

    this.formModalNewSecondary.valueChanges.subscribe(value => {
      this.buttonSaveSecondarySegmentModalConfig.isDisabled = this.formModalNewSecondary.invalid;
    });

    this.parentSegmentCodeObservable();
  }

  public toggleNewPrimary = () => {
    this.modalEmitToggleService.emitToggleWithRef('market-primary-segment-new');
    this.setFormModalPrimarySegment();
  }

  public toggleNewSecondary = () => {
    this.modalEmitToggleService.emitToggleWithRef('market-secondary-segment-new');
    this.setFormModalSecondarySegment();
  }

  public toggleRemoveModal = () => {
    this.modalEmitToggleService.emitToggleWithRef('market-remove-segment');
  }

  public toggleExpandRow(row, expanded) {
    this.table.rowDetail.toggleExpandRow(row);
    this.widthColumns = this.table.headerComponent.columns.map(c => c.width);
  }

  public editRowPrimary(row: MarketSegment) {
    this.toggleNewPrimary();
    this.formModalNewPrimary.patchValue({
      id: row.id,
      isActive: row.isActive,
      name: row.name,
      abbreviation: row.segmentAbbreviation,
      code: row.code,
    });
  }

  public editRowSecondary(row: MarketSegment) {
    this.toggleNewSecondary();
    if (row.code.length > 2) {
      const codeFormated = row.code.split('.', 2);
      row.code = codeFormated[1];
    }
    this.formModalNewSecondary.patchValue({
      id: row.id,
      isActive: row.isActive,
      name: row.name,
      abbreviation: row.segmentAbbreviation,
      primary: row.parentMarketSegmentId,
      parentCode: row.code,
      code: row.code,
    });
  }

  public confimRemoveRow(row: MarketSegment) {
    this.marketSegmentToRemove = row;
    this.toggleRemoveModal();
  }

  public deleteRow = () => {
    this.marketSegmentResource.deleteMarketSegment(this.marketSegmentToRemove.id).subscribe(response => {
      if (response) {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('marketSegmentModule.listPage.removedWithSuccess'),
        );
        this.getSegmentList();
        this.toggleRemoveModal();
      }
    });
  }

  public rowAction(event, row: MarketSegment) {
    if (event.title == 'commomData.delete') {
      this.confimRemoveRow(row);
    } else if (event.title == 'commomData.edit') {
      if (this.isSecondarySegment(row)) {
        this.editRowSecondary(row);
      } else {
        this.editRowPrimary(row);
      }
    }
  }

  private getSegmentList() {
    this.marketSegmentResource.getAllMarketSegmentList(this.propertyId, true).subscribe(response => {
      if (response) {
        this.rows = response.items;
        this.getPrimarySegmentOptionList();
      }
    });
  }

  public savePrimarySegment = () => {
    this.formModalNewPrimary.get('code').setValue(this.verifyAndAdjustCode(this.formModalNewPrimary.get('code').value));

    const marketSegment = new MarketSegment();
    marketSegment.id = this.formModalNewPrimary.get('id').value;
    marketSegment.code = this.formModalNewPrimary.get('code').value;
    marketSegment.name = this.formModalNewPrimary.get('name').value;
    marketSegment.segmentAbbreviation = this.formModalNewPrimary.get('abbreviation').value;
    marketSegment.isActive = this.formModalNewPrimary.get('isActive').value;
    marketSegment.parentMarketSegmentId = null;
    marketSegment.propertyId = this.propertyId;

    this.saveSegment(marketSegment);
    this.toggleNewPrimary();
  }

  public saveSecondarySegment = () => {
    const primarySegmentCode = this.getCodeParentSegmentById(this.formModalNewSecondary.get('primary').value);

    this.formModalNewSecondary.get('code').setValue(this.verifyAndAdjustCode(this.formModalNewSecondary.get('code').value));

    const marketSegmentSecondary = new MarketSegment();
    marketSegmentSecondary.id = this.formModalNewSecondary.get('id').value;
    marketSegmentSecondary.code = primarySegmentCode + '.' + this.formModalNewSecondary.get('code').value;
    marketSegmentSecondary.name = this.formModalNewSecondary.get('name').value;
    marketSegmentSecondary.segmentAbbreviation = this.formModalNewSecondary.get('abbreviation').value;
    marketSegmentSecondary.isActive = this.formModalNewSecondary.get('isActive').value;
    marketSegmentSecondary.parentMarketSegmentId = this.formModalNewSecondary.get('primary').value;
    marketSegmentSecondary.propertyId = this.propertyId;

    this.saveSegment(marketSegmentSecondary);
    this.toggleNewSecondary();
  }

  private getPrimarySegmentOptionList() {
    const primarySegmentList = this.getPrimarySegmentList();
    if (primarySegmentList.length > 0) {
      this.primarySegmentOptionList = this.commomService.convertObjectToSelectOptionObjectByIdAndValueProperties(
        { items: primarySegmentList },
        'id',
        'name',
      );
    }
  }

  private getPrimarySegmentList(): Array<MarketSegment> {
    return this.rows.filter(ms => {
      if (!this.isSecondarySegment(ms)) {
        return ms;
      }
    });
  }

  private isSecondarySegment(segment: MarketSegment) {
    if (segment.parentMarketSegmentId != null) {
      return true;
    }
    return false;
  }

  public itemWasUpdated(segment: MarketSegment) {
    this.marketSegmentResource.updateMarketSegmentStatus(segment).subscribe(response => {
      if (segment.isActive) {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('marketSegmentModule.listPage.activatedWithSuccess'),
        );
      } else {
        this.toasterEmitService.emitChange(
          SuccessError.success,
          this.translateService.instant('marketSegmentModule.listPage.inactivatedWithSuccess'),
        );
      }
      this.getSegmentList();
    });
  }

  private saveSegment(marketSegment: MarketSegment) {
    const message = this.isSecondarySegment(marketSegment)
      ? 'marketSegmentModule.createEditSecondary.saveMessageSuccess'
      : 'marketSegmentModule.createEditPrimary.saveMessageSuccess';

    if (marketSegment.id) {
      this.marketSegmentResource.updateMarketSegment(marketSegment).subscribe(response => {
        if (response) {
          this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant(message));
          this.getSegmentList();
        }
      });
    } else {
      this.marketSegmentResource.createMarketSegment(marketSegment).subscribe(response => {
        if (response) {
          this.toasterEmitService.emitChange(SuccessError.success, this.translateService.instant(message));
          this.getSegmentList();
        }
      });
    }
  }

  public getCodeParentSegmentById(idParent: number): string {
    const primarySegment = this.rows.find(seg => seg.id == idParent);
    return primarySegment.code;
  }

  private parentSegmentCodeObservable() {
    this.formModalNewSecondary.get('primary').valueChanges.subscribe(value => {
      if (value) {
        this.parentSegmentCodeChoosen = this.getCodeParentSegmentById(value);
      }
    });
  }

  private verifyAndAdjustCode(code: number) {
    if (+code < 10) {
      const newCode = this.sharedService.padLeftZero(code, 1);
      return newCode;
    }
    return code;
  }
}
