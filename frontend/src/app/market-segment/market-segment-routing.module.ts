import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MarketSegmentListComponent } from './components/market-segment-list/market-segment-list.component';

export const marketSegmentRoutes: Routes = [{ path: '', component: MarketSegmentListComponent }];

@NgModule({
  imports: [RouterModule.forChild(marketSegmentRoutes)],
  exports: [RouterModule],
})
export class MarketSementRoutingModule {}
