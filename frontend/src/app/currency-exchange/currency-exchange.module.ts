import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { CurrencyExchangeManageComponent } from './components/currency-exchange-manage/currency-exchange-manage.component';
import { CurrentCurrencyExchangeComponent } from './components/current-currency-exchange/current-currency-exchange.component';
import { CurrencyExchangeFormComponent } from './components/currency-exchange-form/currency-exchange-form.component';
import { CurrencyExchangeRoutingModule } from './currency-exchange-routing.module';
import { ThxInputLabelEditModule } from '@inovacao-cmnet/thx-ui';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    CurrencyExchangeRoutingModule,
    ThxInputLabelEditModule,
    MyDateRangePickerModule,
  ],
  declarations: [
    CurrencyExchangeManageComponent,
    CurrentCurrencyExchangeComponent,
    CurrencyExchangeFormComponent
  ],
  // TODO: remove NO_ERRORS_SCHEMA DEBIT 7319
  schemas: [NO_ERRORS_SCHEMA],
})
export class CurrencyExchangeModule {}
