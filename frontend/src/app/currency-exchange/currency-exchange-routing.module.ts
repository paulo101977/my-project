import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurrencyExchangeManageComponent } from './components/currency-exchange-manage/currency-exchange-manage.component';

export const currencyExchangeRoutes: Routes = [{ path: '', component: CurrencyExchangeManageComponent }];

@NgModule({
  imports: [RouterModule.forChild(currencyExchangeRoutes)],
  exports: [RouterModule],
})
export class CurrencyExchangeRoutingModule {}
