import { Currency } from 'app/shared/models/currency';
import { CurrencyExchange } from 'app/shared/models/currency-exchange';

export class DATA {
  public static readonly CURRENCY_LIST = <Array<Currency>>[
    new Currency(),
    new Currency(),
  ];

  public static readonly QUOTATION_LIST = <Array<CurrencyExchange>>[
    {
      currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
      currentDate: '20/05/2019',
      currencyName: 'blabala',
      exchangeRate: 0,
      id: '692d850d-f44d-444c-667c-08d6da19f0e3',
      propertyExchangeRate: 11.11,
      propertyId: 1,
      symbol: 'R$',
    },
    {
      currencyId: '67838acb-9525-4aeb-b0a6-127c1b986c48',
      currentDate: '20/05/2019',
      currencyName: 'blabala 2',
      exchangeRate: 0,
      id: '692d850d-f44d-444c-667c-08d6da19f0e4',
      propertyExchangeRate: 11.11,
      propertyId: 1,
      symbol: '$',
    }
  ];

  public static readonly FORM = {
    period: {
      beginDate: '20/10/2019',
      endDate: '20/10/2019',
    },
    currencyId: '123',
    otherCurrency: 'blabla',
    propertyExchangeRate: 'propertyExchangeRate blabala'
  };
}
