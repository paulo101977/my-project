import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { CurrencyExchange, InsertQuotationForm } from 'app/shared/models/currency-exchange';
import { Currency } from 'app/shared/models/currency';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { CurrencyFormatPipe } from 'app/shared/pipes/currency-format.pipe';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { DateService } from 'app/shared/services/shared/date.service';
import { CurrencyExchangeResource } from 'app/shared/resources/currency-exchange/currency-exchange.resource';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { ParameterTypeEnum } from 'app/shared/models/parameter/pameterType.enum';
import { PropertyDateService } from 'app/shared/services/shared/property-date.service';
import { RightButtonTop } from 'app/shared/models/right-button-top';
import { AssetsCdnService } from '@inovacao-cmnet/thx-ui';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';

@Component({
  selector: 'app-currency-exchange-manage',
  templateUrl: './currency-exchange-manage.component.html',
  styleUrls: ['./currency-exchange-manage.component.css'],
})
export class CurrencyExchangeManageComponent implements OnInit {
  private propertyId: number;

  // Form/Modal dependencies
  public form: FormGroup;
  public modalTitle: string;
  public currencies: Array<Currency>;
  public currencyOptionList: Array<Currency>;
  public currencyToCompare: Currency;
  public optionsCurrencyMask: any;

  // Table dependencies
  public columns: Array<any>;
  public currentQuotation: CurrencyExchange;
  public pageItemsList: CurrencyExchange[];

  // Button dependencies
  public buttonSearchConfig: ButtonConfig;
  public buttonConfigToday: ButtonConfig;
  public rightButtonList: Array<RightButtonTop>;

  // Other
  public defaultCurrency: any;
  public currencyHeader: string;
  public today: string;
  public dayBefore: string;
  public defaultSymbol: string;

  @ViewChild('exchangeRateCell') exchangeRateCellTemplate: TemplateRef<any>;
  @ViewChild('editComponent') editComponent: TemplateRef<any>;
  @ViewChild('todayLabel') todayLabel: TemplateRef<any>;
  @ViewChild('currencyHeaderTemplate') currencyHeaderTemplate: TemplateRef<any>;

  public createQuotation = modalForm => {
    const quotationForm = this.mapperInsertFormToCurrencyExchange(modalForm);
    this.currencyExchangeResource.createQuotation(quotationForm).subscribe(() => {
      const { period } = modalForm;
      const { value } = this.form;

      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('variable.lbSaveSuccessF', {
          labelName: this.translateService.instant('label.propertyCurrencyExchange'),
        }),
      );

      this.form.patchValue({ period });

      this.makeQuotationByPeriodRequest(
        period.beginDate,
        period.endDate,
        value
      );
    });
  }

  public toggleModal = () => {
    this.modalTitle = this.translateService.instant('currencyExchangeModule.commomData.newQuotation');
    this.modalEmitToggleService.emitToggleWithRef(this.modalTitle);
  }

  constructor(
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private toasterEmitService: ToasterEmitService,
    private modalEmitToggleService: ModalEmitToggleService,
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private currencyExchangeResource: CurrencyExchangeResource,
    private currencyFormatPipe: CurrencyFormatPipe,
    private dateService: DateService,
    private commonService: CommomService,
    private location: Location,
    private sessionParameter: SessionParameterService,
    private propertyDateService: PropertyDateService,
    private assetsService: AssetsCdnService,
    private currencyService: CurrencyService,
  ) {
  }

  ngOnInit() {
    this.setVars();
    this.getDefaultCurrency();
    this.setRightButtonConfig();

    this.today = this.getToday();
  }

  private getToday(): string {
    return this.propertyDateService.getTodayDatePropertyTimezone(this.propertyId);
  }


  private setRightButtonConfig() {
    const currencyExchangeCreate = new RightButtonTop();
    currencyExchangeCreate.title = 'currencyExchangeModule.commomData.newQuotation';
    currencyExchangeCreate.callback = this.toggleModal.bind(this);
    currencyExchangeCreate.iconDefault = this.assetsService.getImgUrlTo('event.svg');
    currencyExchangeCreate.iconAlt = 'label.statement';
    this.rightButtonList = [currencyExchangeCreate];
  }

  private setVars() {
    this.setColumnsName();
    this.configFormSearch();
    this.setButtonsConfig();
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.getCurrencies();
    });
    this.optionsCurrencyMask = {prefix: '', thousands: '.', decimal: ',', align: 'left'};
  }

  private getDefaultCurrency() {
    this.defaultCurrency = this.sessionParameter.extractSelectedOptionOfPossibleValues(
      this.sessionParameter.getParameter(this.propertyId, ParameterTypeEnum.DefaultCurrency),
    );
    this.defaultSymbol = this.currencyService.getDefaultCurrencySymbol(this.propertyId);
  }

  private setColumnsName() {
    this.columns = [
      {
        name: '',
        flexGrow: 0.2,
        cellTemplate: this.todayLabel,
        cellClass: 'thf-u-text-center',
        headerClass: 'thf-u-text-center',
      },
      {
        name: 'currencyExchangeModule.commomData.date',
        prop: 'currentDate',
        cellClass: 'thf-u-text-center',
        headerClass: 'thf-u-text-center',
      },
      {
        name: 'currencyExchangeModule.commomData.exchangeRate',
        prop: 'currentDate',
        cellTemplate: this.exchangeRateCellTemplate,
        headerTemplate: this.currencyHeaderTemplate,
        cellClass: 'thf-u-text-center',
        headerClass: 'thf-u-text-center',
      },
      {
        name: 'currencyExchangeModule.commomData.propertyExchangeRate',
        cellTemplate: this.editComponent,
        cellClass: 'thf-u-no-padding thf-u-padding-right--1',
        headerTemplate: this.currencyHeaderTemplate,
      },
    ];
  }

  private setButtonsConfig() {
    this.buttonSearchConfig = this.sharedService.getButtonConfig(
      'search-quotation',
      () => this.getQuotationByPeriod(true),
      'action.searchOnly',
      ButtonType.Primary,
    );
    this.buttonSearchConfig.extraClass = 'thf-u-margin-top--4';

    this.buttonConfigToday = this.sharedService.getButtonConfig(
      'show-today',
      () => this.showToday(),
      'text.showToday',
      ButtonType.Secondary,
      ButtonSize.Normal,
    );

  }

  public showToday() {
    const { value } = this.form;
    this.makeQuotationByPeriodRequest(
      this.today,
      this.today,
      value
    );
  }

  private configFormSearch() {
    this.form = this.formBuilder.group({
      period: [null, [Validators.required]],
      othersCurrency: [null, [Validators.required]],
    });

    this.form.valueChanges.subscribe(value => {
      if (value.othersCurrency) {
        this.currencyToCompare = this.currencies.find(op => op.id == value.othersCurrency);
        const { symbol, currencyName } = this.currencyToCompare;

        this.currencyHeader = this
          .translateService
          .instant('label.currencyExchangeHeaderSymbol', {
            symbol,
            currencyName,
          });
      }
    });
  }

  private getCurrencies() {
    this.currencyExchangeResource.getCurrencies(true).subscribe(result => {
      this.currencyOptionList = this.commonService.toOption(result, 'id', 'currencyName');
      this.currencies = result.items;

      if (this.currencies) {
        this.form.get('othersCurrency').setValue(this.currencies[0].id);
      }
      this.getQuotationByPeriod();
    });
  }

  private getQuotationByPeriod(event?) {
    if (event) {
      if (this.form.invalid) {
        this.toasterEmitService
          .emitChange(SuccessError.error, 'text.pleaseSelectAPeriod');
        return false;
      }
    }
    const { value } = this.form;
    const start = value['period'] ? value['period']['beginDate'] : this.getToday();
    const end = value['period'] ? value['period']['endDate'] : this.getToday();

    this.makeQuotationByPeriodRequest(start, end, value);
  }

  public makeQuotationByPeriodRequest(start: string, end: string, formValue: any) {
    this.currencyExchangeResource.getQuotationByPeriod(
      this.propertyId, start, end, formValue['othersCurrency'],
    ).subscribe(({ items }) => {
      items.forEach(q => this.mapperPropertiesQuotation(q));
      this.pageItemsList = items;
    });
  }

  public confirmValue(value: number, row: CurrencyExchange) {
    const { propertyExchangeRate } = row;
    const currentValue = propertyExchangeRate;

    // update value
    row.propertyExchangeRate = value;

    this.currencyExchangeResource.updateQuotation(
      row
    ).subscribe( () => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('variable.lbEditSuccessF', {
          labelName: this.translateService.instant('label.propertyCurrencyExchange'),
        }),
      );
    }, () => {
      // error, revert value
      row.propertyExchangeRate = currentValue;
    });
  }

  private mapperPropertiesQuotation(row: CurrencyExchange) {
    row['canEdit'] = this.currencyExchangeDateIsSameOrMajorThanCurrentDate(row);
    row['isToday'] = this.currencyExchangeDateIsSame(row);
    row.currentDate = moment(row.currentDate).format('L');
  }

  private mapperInsertFormToCurrencyExchange(formValue) {
    const quotationForm = new InsertQuotationForm();
    quotationForm.startDate = formValue['period']['beginDate'];
    quotationForm.endDate = formValue['period']['endDate'];
    quotationForm.currencyId = formValue['otherCurrency'];
    quotationForm.propertyExchangeRate = formValue['propertyExchangeRate'];
    quotationForm.propertyId = this.propertyId;
    return quotationForm;
  }

  private currencyExchangeDateIsSame(ce: CurrencyExchange): boolean {
    return moment(ce.currentDate)
      .isSame(this.today, 'day');
  }

  private currencyExchangeDateIsSameOrMajorThanCurrentDate(ce: CurrencyExchange): boolean {
    return moment(ce.currentDate)
      .isSameOrAfter(this.today, 'day');
  }

  public couldShow(row: CurrencyExchange, property: string) {
    return row[property] != null;
  }
}
