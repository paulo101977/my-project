import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { CurrencyExchangeManageComponent } from './currency-exchange-manage.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  ActivatedRouteStub,
  createAccessorComponent,
  createServiceStub,
  currencyFormatPipe,
  locationStub
} from '../../../../../testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { propertyDateServiceStub } from 'app/shared/services/shared/testing/property-date';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ActivatedRoute } from '@angular/router';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { dateServiceStub } from 'app/shared/services/shared/testing/date-service';
import { sessionParameterServiceStub } from 'app/shared/services/parameter/testing';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';
import { currencyExchangeResourceStub } from 'app/shared/resources/currency-exchange/testing';
import { AssetsCdnService } from '@inovacao-cmnet/thx-ui';
import { of, throwError } from 'rxjs';
import { DATA } from 'app/currency-exchange/mock-data';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { CurrencyExchange } from 'app/shared/models/currency-exchange';
import { currencyServiceStub } from 'app/shared/services/shared/testing/currency-service';


const thxPicker = createAccessorComponent('thx-date-range-picker');

// TODO: fix bifrost compilation stub to export from bifrost it module
const assetsCdnServiceStub = createServiceStub(AssetsCdnService, {
  getImgUrlTo: (item: string) => '',
});

describe('CurrencyExchangeManageComponent', () => {
  let component: CurrencyExchangeManageComponent;
  let fixture: ComponentFixture<CurrencyExchangeManageComponent>;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        FormsModule,
        TranslateTestingModule,
      ],
      declarations: [
        CurrencyExchangeManageComponent,
        currencyFormatPipe,
        thxPicker,
      ],
      providers: [
        propertyDateServiceStub,
        locationStub,
        sharedServiceStub,
        toasterEmitServiceStub,
        modalToggleServiceStub,
        dateServiceStub,
        sessionParameterServiceStub,
        commomServiceStub,
        currencyExchangeResourceStub,
        assetsCdnServiceStub,
        currencyServiceStub,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(CurrencyExchangeManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', () => {
    const today = '12/03/2019';

    spyOn<any>(component, 'setVars');
    spyOn<any>(component, 'getDefaultCurrency');
    spyOn<any>(component, 'setRightButtonConfig');
    spyOn<any>(component, 'getToday').and.returnValue(today);

    component.ngOnInit();

    expect(component['setVars']).toHaveBeenCalled();
    expect(component['getDefaultCurrency']).toHaveBeenCalled();
    expect(component['setRightButtonConfig']).toHaveBeenCalled();
    expect(component.today).toEqual(today);
  });

  it('should test getCurrencies', fakeAsync(() => {
    const result = {items: DATA.CURRENCY_LIST, hasNext: true};

    const othersCurrency = DATA.CURRENCY_LIST[0].id = '123';

    component['form'].patchValue({othersCurrency}, {emitEvent: false});

    spyOn(component['currencyExchangeResource'], 'getCurrencies')
      .and
      .returnValue(of(result));
    spyOn<any>(component, 'getQuotationByPeriod');
    spyOn(component['commonService'], 'toOption');

    component['getCurrencies']();

    tick();

    expect(component['getQuotationByPeriod']).toHaveBeenCalled();
    expect(component['commonService'].toOption).toHaveBeenCalledWith(result, 'id', 'currencyName');
    expect(component['form'].getRawValue().othersCurrency).toEqual(othersCurrency);
  }));

  describe('on getQuotationByPeriod', () => {
    const today = '11/10/2019';
    beforeEach(() => {
      spyOn(component, 'makeQuotationByPeriodRequest');
      spyOn<any>(component, 'getToday').and.returnValue(today);
    });

    it('should test when event is not null and form invalid', () => {
      spyOn(component['toasterEmitService'], 'emitChange');

      component['getQuotationByPeriod'](true);

      expect(component['toasterEmitService'].emitChange)
        .toHaveBeenCalledWith(SuccessError.error, 'text.pleaseSelectAPeriod');
    });

    it('should test when has period', () => {
      const beginDate = '10/09/2018';
      const endDate = '11/10/2018';

      component['form'].patchValue({
        period: {
          beginDate,
          endDate,
        },
      }, {emitEvent: false});

      const { value } = component['form'];

      component['getQuotationByPeriod'](false);

      expect(component.makeQuotationByPeriodRequest).toHaveBeenCalledWith(beginDate, endDate, value);
    });

    it('should test when has not period', () => {
      component['form'].patchValue({
        period: null,
      }, {emitEvent: false});


      component['getQuotationByPeriod'](false);
      const { value } = component['form'];

      expect(component.makeQuotationByPeriodRequest).toHaveBeenCalledWith(today, today, value);
    });
  });

  it( 'should test makeQuotationByPeriodRequest', fakeAsync(() => {
    const items = DATA.QUOTATION_LIST;
    const start = '30/03/2011';
    const end = '30/03/2011';
    const propertyId = 1;
    const formValue = {othersCurrency: 'othersCurrency'};
    spyOn<any>(component, 'mapperPropertiesQuotation');
    spyOn(component['currencyExchangeResource'], 'getQuotationByPeriod').and.returnValue(
      of( {items, hasNext: true}),
    );

    component.makeQuotationByPeriodRequest(start, end, formValue);
    tick();

    expect(component.pageItemsList).toEqual(items);
    expect(component['mapperPropertiesQuotation']).toHaveBeenCalledTimes(2);
  }));

  it( 'should test mapperPropertiesQuotation', () => {
    const row = new CurrencyExchange();
    row.currentDate = '2019-10-05:10:10:10.000z';

    spyOn<any>(component, 'currencyExchangeDateIsSameOrMajorThanCurrentDate').and.returnValue(false);
    spyOn<any>(component, 'currencyExchangeDateIsSame').and.returnValue(true);

    component['mapperPropertiesQuotation'](row);

    expect(row['canEdit']).toEqual(false);
    expect(row['isToday']).toEqual(true);
  });

  it( 'should test mapperInsertFormToCurrencyExchange', () => {
    const propertyId = 123;
    const form = DATA.FORM;

    component['propertyId'] = propertyId;

    const result = component['mapperInsertFormToCurrencyExchange'](form);

    expect(result['currencyId']).toEqual(form.otherCurrency);
    expect<any>(result['propertyExchangeRate']).toEqual(form.propertyExchangeRate);
    expect(result['propertyId']).toEqual(propertyId);
    expect(result['startDate']).toEqual(form.period.beginDate);
    expect(result['endDate']).toEqual(form.period.endDate);
  });

  describe('on confirmValue', () => {
    const value = 2.3;
    let row;
    beforeEach(() => {
      row = new CurrencyExchange();
    });

    it( 'should test when response is ok', fakeAsync(() => {
      spyOn(component['toasterEmitService'], 'emitChange');
      spyOn(component['currencyExchangeResource'], 'updateQuotation').and.returnValue(of( null ));

      component.confirmValue(value, row);
      tick();

      expect(component['toasterEmitService'].emitChange).toHaveBeenCalled();
      expect(row.propertyExchangeRate).toEqual(value);
    }));

    it( 'should test when response throw error', fakeAsync(() => {
      const newValue = 3.4;
      spyOn(component['currencyExchangeResource'], 'updateQuotation').and.returnValue(throwError('error') );
      row.propertyExchangeRate = newValue;

      component.confirmValue(value, row);
      tick();

      expect(row.propertyExchangeRate).toEqual(newValue);
    }));

    it('should test createQuotation', fakeAsync(() => {
        const form = DATA.FORM;
        const { period: { beginDate, endDate }} = form;
        spyOn(component['currencyExchangeResource'], 'createQuotation').and.returnValue(of(null));
        spyOn(component['toasterEmitService'], 'emitChange');
        spyOn(component, 'makeQuotationByPeriodRequest');

        component['form'].patchValue(form, {emitEvent: false});

        component.createQuotation(form);
        tick();

        expect(component['toasterEmitService'].emitChange).toHaveBeenCalled();
        expect(component['makeQuotationByPeriodRequest']).toHaveBeenCalled();
    }));
  });

  describe('on could show', () => {
    let row;
    beforeAll(() => {
      row = new CurrencyExchange();
    });

    it('should test case property is null', () => {
        row.currentDate = null;

        expect(component.couldShow(row, 'currentDate')).toEqual(false);
    });

    it('should test case property is not null and not null', () => {
      row.propertyExchangeRate = 1;

      expect(component.couldShow(row, 'propertyExchangeRate')).toEqual(true);
    });
  });
});
