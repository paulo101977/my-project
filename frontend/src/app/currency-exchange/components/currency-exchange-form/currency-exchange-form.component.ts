import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Currency } from '../../../shared/models/currency';
import { ButtonConfig, ButtonType } from '../../../shared/models/button-config';
import { SharedService } from '../../../shared/services/shared/shared.service';
import { TranslateService } from '@ngx-translate/core';
import { CurrencyExchange } from '../../../shared/models/currency-exchange';
import { DateService } from '../../../shared/services/shared/date.service';
import { IMyDrpOptions } from 'mydaterangepicker';

@Component({
  selector: 'currency-exchange-form',
  templateUrl: './currency-exchange-form.component.html',
  styleUrls: ['./currency-exchange-form.component.css'],
})
export class CurrencyExchangeFormComponent implements OnInit {
  public myDateRangePickerOptions: IMyDrpOptions;
  public UNIT_OF_CURRENCY = '1,00';
  public currencyToCompare: Currency;
  public optionsCurrencyMask: any;

  // Form dependencies
  public currencyForm: FormGroup;

  // Button dependencies
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  private _currencies: Array<Currency>;
  @Input()
  set currencies(currencies: Array<Currency>) {
    this._currencies = currencies;
    if (this.currencyForm) {
      this.currencyForm.get('otherCurrency').setValue( this.currencies && this.currencies.length > 0 ? this.currencies[0].id : null );
    }
  }

  get currencies(): Array<Currency> {
    return this._currencies;
  }

  @Input() currentQuotation: CurrencyExchange;
  @Input() currencyOptionList: Array<Currency>;
  @Input() defaultCurrency: any;

  @Output() cancel = new EventEmitter();
  @Output() confirm = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private translateService: TranslateService,
    private dateService: DateService,
  ) {}

  ngOnInit() {
    this.configForm();
    this.setButtonsConfig();
    this.myDateRangePickerOptions = this.dateService.getClearConfigDateRangePicker();
    this.optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
  }

  private setButtonsConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig('cancel', this.cancelCallback, 'action.cancel', ButtonType.Secondary);
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'confirm',
      this.confirmCallback,
      'action.conclude',
      ButtonType.Primary,
      null,
      true,
    );
  }

  private configForm(): void {
    this.currencyForm = this.formBuilder.group({
      period: [null, [Validators.required]],
      otherCurrency: [this.currencies && this.currencies.length > 0 ? this.currencies[0].id : null, [Validators.required]],
      propertyExchangeRate: [0, [Validators.required, control => (control.value > 0 ? null : { invalid: true })]],
    });

    this.currencyForm.valueChanges.subscribe(value => {
      this.buttonConfirmConfig.isDisabled = this.currencyForm.invalid;
      this.currencyToCompare = this.currencies.find(op => op.id == value.otherCurrency);
    });
  }

  private cancelCallback = () => {
    this.currencyForm.reset();
    this.cancel.emit();
  }

  private confirmCallback = () => {
    if (this.currencyForm.valid) {
      this.confirm.emit(this.currencyForm.value);
      this.cancelCallback();
    }
  }
}
