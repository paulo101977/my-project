import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CurrentCurrencyExchangeComponent } from './current-currency-exchange.component';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { currencyFormatPipe } from '../../../../../testing';

describe('CurrentCurrencyExchangeComponent', () => {
  let component: CurrentCurrencyExchangeComponent;
  let fixture: ComponentFixture<CurrentCurrencyExchangeComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
          TranslateTestingModule,
      ],
      declarations: [
        CurrentCurrencyExchangeComponent,
        currencyFormatPipe,
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(CurrentCurrencyExchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
