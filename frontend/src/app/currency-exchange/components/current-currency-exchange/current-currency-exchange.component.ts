import { Component, Input } from '@angular/core';

@Component({
  selector: 'current-currency-exchange',
  templateUrl: './current-currency-exchange.component.html',
  styleUrls: ['./current-currency-exchange.component.css'],
})
export class CurrentCurrencyExchangeComponent {
  @Input() currentQuotation;
  @Input() currencyToCompare;

  public optionsCurrencyMask = { prefix: '', thousands: '.', decimal: ',', align: 'left' };
  public UNIT_OF_CURRENCY = '1,00';
}
