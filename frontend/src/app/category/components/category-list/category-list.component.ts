import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PropertyCompanyClientCategory } from 'app/shared/models/property-company-client-category';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { GuidDefaultData } from 'app/shared/mock-data/guid-default-data';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { ModalEmitService } from 'app/shared/services/shared/modal-emit.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { CategoryResource } from 'app/shared/resources/category/category.resource';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css'],
})
export class CategoryListComponent implements OnInit {
  public columns: Array<any>;
  public pageItemsList: PropertyCompanyClientCategory[];
  public origimItem: PropertyCompanyClientCategory;
  public modalForm: FormGroup;
  private activeEdition: boolean;
  private propertyId: number;
  public itemsOption: Array<any>;

  // Modal Dependencies
  public modalTitle: string;

  // Buttons
  public buttonnewEditCategory: ButtonConfig;
  public buttonCancelModal: ButtonConfig;
  public buttonSaveModal: ButtonConfig;
  public buttonDeleteModal: ButtonConfig;
  public buttonConfirmDeleteModal: ButtonConfig;

  constructor(
    public translateService: TranslateService,
    private route: ActivatedRoute,
    private categoryResource: CategoryResource,
    private toasterEmitService: ToasterEmitService,
    private modalEmitService: ModalEmitService,
    private modalEmitToggleService: ModalEmitToggleService,
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
  ) {}

  ngOnInit(): void {
    this.setVars();
  }

  private setVars(): void {
    this.route.params.subscribe(params => {
      this.propertyId = +params.property;
      this.activeEdition = false; // is save
      this.origimItem = new PropertyCompanyClientCategory();
      this.setColumnsName();
      this.configFormModal();
      this.setButtonsConfig();
      this.getCategories();
      this.itemsOption = [
        { title: 'commomData.edit', callbackFunction: this.editCategoryConfig },
        { title: 'commomData.delete', callbackFunction: this.confirmRemoveCategory },
      ];
    });
  }

  private setButtonsConfig() {
    this.buttonnewEditCategory = this.sharedService.getButtonConfig(
      'new-edit-category',
      this.openSaveModal,
      '',
      ButtonType.Secondary,
      ButtonSize.Small,
    );
    this.buttonCancelModal = this.sharedService.getButtonConfig(
      'cancel-modal',
      this.toggleModal,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonSaveModal = this.sharedService.getButtonConfig(
      'save-modal',
      this.actionSaveModalCategory,
      'commomData.confirm',
      ButtonType.Primary,
      null,
      true,
    );
    this.buttonDeleteModal = this.sharedService.getButtonConfig(
      'cancel-modal-delete',
      this.toggleModalDelete,
      'commomData.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmDeleteModal = this.sharedService.getButtonConfig(
      'save-modal-delete',
      this.removeCategory,
      'commomData.confirm',
      ButtonType.Primary,
    );
  }

  public openSaveModal = () => {
    this.activeEdition = false;
    this.toggleModal();
  }

  private getCategories(): void {
    this.categoryResource.getCategory(this.propertyId, true).subscribe(result => {
      this.pageItemsList = result.items;
    });
  }

  public updateCategoryStatus(categoryItem: PropertyCompanyClientCategory) {
    this.categoryResource.updateCategoryStatus(categoryItem).subscribe(result => {
      const message = categoryItem.isActive
        ? this.translateService.instant('variable.lbActivatedWithSuccessF', {
            labelName: this.translateService.instant('label.category'),
          })
        : this.translateService.instant('variable.lbInactivatedWithSuccessF', {
            labelName: this.translateService.instant('label.category'),
          });
      this.toasterEmitService.emitChange(SuccessError.success, message);
    });
  }

  public confirmRemoveCategory = (category: PropertyCompanyClientCategory) => {
    this.origimItem = category;
    this.toggleModalDelete();
  }

  public editCategoryConfig = (category: PropertyCompanyClientCategory) => {
    this.activeEdition = true;
    this.toggleModal();
    this.modalTitle = this.translateService.instant('action.lbEdit', {
      labelName: this.translateService.instant('label.category'),
    });
    this.modalForm.patchValue({
      id: category.id,
      propertyCompanyClientCategoryName: category.propertyCompanyClientCategoryName,
      isActive: category.isActive,
      property: category.propertyId,
    });
  }

  public editCategory(): void {
    this.activeEdition = false;
    this.categoryResource.updateCategory(this.modalForm.value).subscribe(result => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('variable.lbEditSuccessF', {
          labelName: this.translateService.instant('label.category'),
        }),
      );
      this.toggleModal();
      this.getCategories();
    });
  }

  public removeCategory = () => {
    this.categoryResource.removeCategory(this.origimItem).subscribe(result => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('variable.lbDeleteSuccessF', {
          labelName: this.translateService.instant('label.category'),
        }),
      );
      this.toggleModalDelete();
      this.getCategories();
    });
  }

  private setColumnsName() {
    this.columns = [];
    this.translateService.get('categoryModule.commomData.name').subscribe(value => {
      this.columns.push({ name: value, prop: 'propertyCompanyClientCategoryName' });
    });
  }

  private configFormModal(): void {
    this.modalForm = this.formBuilder.group({
      id: GuidDefaultData,
      propertyCompanyClientCategoryName: ['', [Validators.required, Validators.maxLength(60)]],
      isActive: true,
      propertyId: this.propertyId,
    });
    this.modalForm.valueChanges.subscribe(
      value =>
        (this.buttonSaveModal = this.sharedService.getButtonConfig(
          'save-modal',
          this.actionSaveModalCategory,
          'commomData.confirm',
          ButtonType.Primary,
          null,
          this.modalForm.invalid,
        )),
    );
  }

  public toggleModal = () => {
    this.modalTitle = this.translateService.instant('action.lbNew', {
      labelName: this.translateService.instant('label.category'),
    });
    this.configFormModal();
    this.modalEmitToggleService.emitToggleWithRef('modalCategory');
  }

  public toggleModalDelete = () => {
    this.modalEmitToggleService.emitToggleWithRef('modalCategoryDelete');
  }

  public createCategory(): void {
    this.categoryResource.createCategory(this.modalForm.value).subscribe(result => {
      this.toasterEmitService.emitChange(
        SuccessError.success,
        this.translateService.instant('variable.lbSaveSuccessF', {
          labelName: this.translateService.instant('label.category'),
        }),
      );
      this.toggleModal();
      this.getCategories();
    });
  }

  public actionSaveModalCategory = () => {
    if (this.activeEdition) {
      this.editCategory();
    } else {
      this.createCategory();
    }
  }

  public runCallbackFunction(item: any, category: PropertyCompanyClientCategory) {
    if (item.callbackFunction) {
      item.callbackFunction(category, this);
    }
  }
}
