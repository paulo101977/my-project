import { of } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CategoryListComponent } from './category-list.component';
import { PropertyCompanyClientCategory } from 'app/shared/models/property-company-client-category';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ActivatedRoute } from '@angular/router';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { modalEmitServiceStub } from 'app/shared/services/shared/testing/modal-emit-service';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { categoryResourceStub } from 'app/shared/resources/category/testing';
import { configureTestSuite } from 'ng-bullet';

describe('CategoryListComponent', () => {
  let component: CategoryListComponent;
  let fixture: ComponentFixture<CategoryListComponent>;
  const category = new PropertyCompanyClientCategory();
  category.id = 1;
  category.propertyCompanyClientCategoryName = 'Descrição do item';
  category.isActive = false;

  const categories = [category];

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      declarations: [CategoryListComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ property: 1 }),
          },
        },
        sharedServiceStub,
        modalToggleServiceStub,
        modalEmitServiceStub,
        toasterEmitServiceStub,
        categoryResourceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(CategoryListComponent);
    component = fixture.componentInstance;

    spyOn<any>(component['categoryResource'], 'updateCategoryStatus').and.returnValue(of({}));

    fixture.detectChanges();
  });

  describe('onInit', () => {
    it('should create CategoryListComponent', () => {
      expect(component).toBeTruthy();
    });

    it('should setVars', () => {
      spyOn<any>(component, 'setColumnsName');
      spyOn<any>(component, 'getCategories');
      spyOn<any>(component, 'configFormModal');

      component['setVars']();

      expect(component['setColumnsName']).toHaveBeenCalled();
      expect(component['getCategories']).toHaveBeenCalled();
      expect(component['configFormModal']).toHaveBeenCalled();
      expect(component['activeEdition']).toEqual(false);
    });
  });

  it('should getCategories', () => {
    spyOn<any>(component['categoryResource'], 'getCategory').and.returnValue(of({ items: categories }));

    component['getCategories']();

    expect(component['categoryResource'].getCategory).toHaveBeenCalledWith(component['propertyId'], true);
    expect(component.pageItemsList).toEqual(categories);
  });

  it('should call loadpage e success message on change status PropertyCompanyClientCategory', () => {
    spyOn(component['toasterEmitService'], 'emitChange');

    component.updateCategoryStatus(category);

    expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(SuccessError.success, 'variable.lbInactivatedWithSuccessF');
  });

  it('should call success message on remove PropertyCompanyClientCategory', () => {
    spyOn(component, 'toggleModalDelete');

    component.confirmRemoveCategory(category);

    expect(component.origimItem).toEqual(category);

    expect(component.toggleModalDelete).toHaveBeenCalled();
  });

  it('should set configFormModal', () => {
    component['configFormModal']();

    expect(Object.keys(component.modalForm.controls)).toEqual(['id', 'propertyCompanyClientCategoryName', 'isActive', 'propertyId']);
  });

  it('should call toggleModal', () => {
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');

    component.toggleModal();
    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith('modalCategory');
  });

  it('should call editCategoryConfig', () => {
    spyOn(component, 'toggleModal');

    component.editCategoryConfig(category);

    expect(component.toggleModal).toHaveBeenCalled();
    expect(component['activeEdition']).toEqual(true);
    expect(component['modalTitle']).toEqual('action.lbEdit');
  });

  it('should call actionSaveModalCategory with variable activeEdition TRUE', () => {
    spyOn(component, 'editCategory');
    spyOn(component, 'createCategory');

    component['activeEdition'] = true;
    component.actionSaveModalCategory();

    expect(component['activeEdition']).toEqual(true);
    expect(component.editCategory).toHaveBeenCalled();
    expect(component.createCategory).not.toHaveBeenCalled();
  });

  it('should call actionSaveModalCategory with variable activeEdition FALSE', () => {
    spyOn(component, 'editCategory');
    spyOn(component, 'createCategory');

    component['activeEdition'] = false;
    component.actionSaveModalCategory();

    expect(component['activeEdition']).toEqual(false);
    expect(component.editCategory).not.toHaveBeenCalled();
    expect(component.createCategory).toHaveBeenCalled();
  });

  it('should call editCategory', () => {
    spyOn(component['toasterEmitService'], 'emitChange');
    spyOn(component, 'toggleModal');
    spyOn<any>(component, 'getCategories');

    spyOn<any>(component['categoryResource'], 'updateCategory').and.returnValue(of({}));

    component.editCategory();

    expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
      SuccessError.success,
      component.translateService.instant('variable.lbEditSuccessF'),
    );
    expect(component['activeEdition']).toEqual(false);
    expect(component['toggleModal']).toHaveBeenCalled();
    expect(component['getCategories']).toHaveBeenCalled();
  });

  it('should call loadpage and others on create createCategory', () => {
    spyOn(component['toasterEmitService'], 'emitChange');
    spyOn(component, 'toggleModal');
    spyOn<any>(component, 'getCategories');

    spyOn<any>(component['categoryResource'], 'createCategory').and.returnValue(of({}));

    component.createCategory();

    expect(component['toasterEmitService'].emitChange).toHaveBeenCalledWith(
      SuccessError.success,
      component.translateService.instant('variable.lbSaveSuccessF'),
    );
    expect(component['toggleModal']).toHaveBeenCalled();
    expect(component['getCategories']).toHaveBeenCalled();
  });
});
