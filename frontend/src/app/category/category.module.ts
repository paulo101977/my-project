import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { CategoryRoutingModule } from './category-routing.module';

@NgModule({
  imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule, CategoryRoutingModule],
  declarations: [CategoryListComponent],
})
export class CategoryModule {}
