import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { AdminGuard } from '../auth/services/admin-guard.service';

export const categoryRoutes: Routes = [
  {path: '', component: CategoryListComponent, canActivate: [AdminGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(categoryRoutes)],
  exports: [RouterModule],
})
export class CategoryRoutingModule {}
