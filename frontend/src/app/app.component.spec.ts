import { CustomDropdownComponent } from './shared/components/custom-dropdown/custom-dropdown.component';
import { async, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { appRoutes } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { MomentModule } from 'ngx-moment';
import { AppComponent } from './app.component';
import { PropertyCardComponent } from './property/components/property-card/property-card.component';
import { PropertyEditComponent } from './property/components/property-edit/property-edit.component';
import { PropertyContentContainerComponent } from './property/components/property-content-container/property-content-container.component';
import { RoomTypeListComponent } from './room-type/components/room-type-list/room-type-list.component';
import { RoomTypeCreateComponent } from './room-type/components/room-type-create/room-type-create.component';
import { RoomTypeEditComponent } from './room-type/components/room-type-edit/room-type-edit.component';
import { RoomListComponent } from './room/components/room-list/room-list.component';
import { ReservationCreateComponent } from './reservation/components/reservation-create/reservation-create.component';
import { ReservationListComponent } from './reservation/components/reservation-list/reservation-list.component';
import { RoomCreateComponent } from './room/components/room-create/room-create.component';
import { RoomEditComponent } from './room/components/room-edit/room-edit.component';
import { FilterListPipe } from './shared/pipes/filter-select.pipe';
import { GuestEditComponent } from './guest/components/guest-edit/guest-edit.component';
import { GuestEditRoomlistComponent } from './guest/components/guest-edit-roomlist/guest-edit-roomlist.component';
import { OriginListComponent } from './origin/components/origin-list/origin-list.component';
import { MarkTextInBlackPipe } from './shared/pipes/mark-text-in-black.pipe';
import { ClientListComponent } from './client/components/client-list/client-list.component';
import { ClientAddressComponent } from './client/components/client-address/client-address.component';
import { ClientCreateEditComponent } from './client/component-container/client-create-edit/client-create-edit.component';
import {
  PropertyConfigContainerComponent
} from './property/components-containers/property-config-container/property-config-container.component';
import { PropertyAvailabilityGridComponent } from './availability/property-availability-grid/property-availability-grid.component';
import { GuestFnrhCreateEditComponent } from './checkin/component-container/guest-fnrh-create-edit/guest-fnrh-create-edit.component';
import { GuestAccountComponent } from './account-type/components/guest-account/guest-account.component';
import { BillingAccountListComponent } from './billing-account/components-container/billing-account-list/billing-account-list.component';
import {
  BillingAccountManageComponent
} from './billing-account/components-container/billing-account-manage/billing-account-manage.component';
import {
  BillingAccountRouterComponent
} from './billing-account/components-container/billing-account-router/billing-account-router.component';
import { SingleAccountComponent } from './account-type/components/single-account/single-account.component';
import { ClientAccountComponent } from './account-type/components/client-account/client-account.component';
import { AccountTypeCreateComponent } from './account-type/components/account-type-create/account-type-create.component';
import { ServiceListComponent } from './entry-type/components/service-list/service-list.component';
import { ServiceCreateEditComponent } from './entry-type/components/service-create-edit/service-create-edit.component';
import { GroupListComponent } from './entry-type/components/group-list/group-list.component';
import { GroupCreateEditComponent } from './entry-type/components/group-create-edit/group-create-edit.component';
import { TaxListComponent } from './entry-type/components/tax-list/tax-list.component';
import { TaxCreateEditComponent } from './entry-type/components/tax-create-edit/tax-create-edit.component';
import { MarketSegmentListComponent } from './market-segment/components/market-segment-list/market-segment-list.component';
import { ReasonListComponent } from './reason/components/reason-list/reason-list.component';
import { UserListComponent } from './user/components/user-list/user-list.component';
import { SignupComponent } from './auth/components/signup/signup.component';
import { CreditCardPaymentComponent } from './payment-type/components/credit-card/credit-card.component';
import { DebitPaymentComponent } from './payment-type/components/debit/debit.component';
import { PaymentTypeListComponent } from './payment-type/components/payment-type-list/payment-type-list.component';
import { PaymentTypeConfigComponent } from './payment-type/components-container/payment-type-config/payment-type-config.component';
import { PolicyListComponent } from './policy/components/policy-list/policy-list.component';
import { StatementDetailComponent } from './statement/component-container/statement-detail/statement-detail.component';
import { CurrencyFormatPipe } from './shared/pipes/currency-format.pipe';
import { ResetPasswordComponent } from './auth/components/reset-password/reset-password.component';
import { NewPasswordFormComponent } from './auth/components/new-password-form/new-password-form.component';
import { ParameterInputTypeComponent } from './property/components/parameter-input-type/parameter-input-type.component';
import {
  BillingAccountTransferComponent
} from './transfer/components-container/billing-account-transfer/billing-account-transfer.component';
import { GovernanceStatusFormComponent } from './governance/components/governance-status-form/governance-status-form.component';
import { GovernanceRoomListComponent } from './governance/components/governance-room-list/governance-room-list.component';
import { CalendarComponent } from './revenue-management/calendar/components/calendar/calendar.component';
import {
  MealPlanTypeManageComponent
} from './revenue-management/meal-plan-type/components-containers/meal-plan-type-manage/meal-plan-type-manage.component';
import { ReservationSlipComponent } from './reservation/components/reservation-slip/reservation-slip.component';
import { ReservationResumeModalComponent } from './reservation/components/reservation-resume-modal/reservation-resume-modal.component';
import { BaseRateComponent } from './revenue-management/base-rate/components/base-rate/base-rate.component';
import { BaseRateTableComponent } from './revenue-management/base-rate/components/base-rate-table/base-rate-table.component';
import { RatePlanListComponent } from './revenue-management/rate-plan/rate-plan-list/rate-plan-list.component';
import { RatePlanCreateEditComponent } from './revenue-management/rate-plan/rate-plan-create-edit/rate-plan-create-edit.component';
import {
  ReservationBudgetManageComponent
} from './revenue-management/reservation-budget/components-containers/reservation-budget-manage/reservation-budget-manage.component';
import { RoomChangeManagerComponent } from './room-change/components-containers/room-change-manager/room-change-manager.component';
import { CategoryListComponent } from './category/components/category-list/category-list.component';
import {
  FiscalDocumentsParameterComponent
} from './property/components/fiscal-documents/fiscal-documents-parameter/fiscal-documents-parameter.component';
import {
  FiscalDocumentsParameterCreateEditComponent
} from './property/components/fiscal-documents/fiscal-documents-parameter-create-edit/fiscal-documents-parameter-create-edit.component';
import {
  FiscalDocumentsAssociateItemsBoxComponent
} from './property/components/fiscal-documents/fiscal-documents-associate-items-box/fiscal-documents-associate-items-box.component';
import { BudgetCompareComponent } from './room-change/components/budget-compare/budget-compare.component';
import { AuditContainerComponent } from './audit/components/audit-container/audit-container.component';
import { LoginComponent } from './auth/components/login/login.component';
import { ParameterComponent } from './property/components/parameter/parameter.component';
import { GovernanceStatusListComponent } from './governance/components/governance-status-list/governance-status-list.component';
import { TruncatePipe } from './shared/pipes/truncate.pipe';
import { MomentCalendarPipe } from './shared/pipes/moment-calendar.pipe';
import {
  CurrencyExchangeManageComponent
} from './currency-exchange/components/currency-exchange-manage/currency-exchange-manage.component';
import { ModalErrorsNfseComponent } from './billing-account/components/modal-errors-nfse/modal-errors-nfse.component';
import {
  FiscalDocumentsAssociateItemsModalComponent
} from './property/components/fiscal-documents/fiscal-documents-associate-items-modal/fiscal-documents-associate-items-modal.component';
import { PosListComponent } from './config/pos/components/pos-list/pos-list.component';
import { PosCreateEditComponent } from './config/pos/components/pos-create-edit/pos-create-edit.component';
import { PadPipe } from './shared/pipe/pad.pipe';
import { ThxCurrencyPipe } from './shared/pipes/thx-currency.pipe';
import { GuestTypeListComponent } from './config/guest-type/components/guest-type-list/guest-type-list.component';
import { InternationalizationService } from './shared/services/shared/internationalization.service';
import { SellingBudgetModalComponent } from './room-change/components/selling-budget-modal/selling-budget-modal.component';
import { StatementThermalPrinterComponent } from './shared/components/statement-thermal-printer/statement-thermal-printer.component';
import { CoreModule } from '@app/core/core.module';
import { configureAppName, APP_NAME } from '@inovacaocmnet/thx-bifrost';
import { configureTestSuite } from 'ng-bullet';
import { PropertiesPanelComponent } from 'app/property/components/properties-panel/properties-panel.component';

describe('AppComponent', () => {
  let location: Location;
  let router: Router;
  let fixture, component, element, fixtureCard, componentPropertyCard;
  let promisePropertyByCompany;
  const propertiesMock = [
    {name: 'sdfasdf', brand: {name: 'fasdfa', parentBrand: {name: 'dsafasd', parentBrand: null, id: 1002}, id: 1003}, id: 1},
    {name: 'blue stars', brand: {name: 'blue', parentBrand: {name: 'BOW BOW', parentBrand: null, id: 1006}, id: 1007}, id: 3},
  ];

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        PadPipe,
        PropertiesPanelComponent,
        PropertyCardComponent,
        PropertyEditComponent,
        PropertyContentContainerComponent,
        PropertyAvailabilityGridComponent,
        ParameterComponent,
        ParameterInputTypeComponent,
        RoomTypeListComponent,
        RoomTypeCreateComponent,
        RoomTypeEditComponent,
        RoomListComponent,
        RoomCreateComponent,
        RoomEditComponent,
        FilterListPipe,
        ReservationCreateComponent,
        ReservationListComponent,
        GuestEditComponent,
        GuestEditRoomlistComponent,
        OriginListComponent,
        MarkTextInBlackPipe,
        ClientListComponent,
        ClientCreateEditComponent,
        ClientAddressComponent,
        PropertyConfigContainerComponent,
        PropertyAvailabilityGridComponent,
        GuestFnrhCreateEditComponent,
        BillingAccountRouterComponent,
        BillingAccountManageComponent,
        BillingAccountListComponent,
        SingleAccountComponent,
        GuestAccountComponent,
        AccountTypeCreateComponent,
        ClientAccountComponent,
        ServiceListComponent,
        ServiceCreateEditComponent,
        TaxListComponent,
        TaxCreateEditComponent,
        GroupListComponent,
        GroupCreateEditComponent,
        MarketSegmentListComponent,
        LoginComponent,
        UserListComponent,
        ReasonListComponent,
        SignupComponent,
        DebitPaymentComponent,
        CreditCardPaymentComponent,
        PaymentTypeListComponent,
        PaymentTypeConfigComponent,
        PolicyListComponent,
        StatementDetailComponent,
        CurrencyFormatPipe,
        ResetPasswordComponent,
        NewPasswordFormComponent,
        TruncatePipe,
        BillingAccountTransferComponent,
        GovernanceStatusListComponent,
        GovernanceStatusFormComponent,
        GovernanceRoomListComponent,
        MomentCalendarPipe,
        CalendarComponent,
        MealPlanTypeManageComponent,
        BaseRateComponent,
        BaseRateTableComponent,
        ReservationResumeModalComponent,
        ReservationSlipComponent,
        RatePlanListComponent,
        RatePlanCreateEditComponent,
        ReservationSlipComponent,
        ReservationBudgetManageComponent,
        RoomChangeManagerComponent,
        BudgetCompareComponent,
        CategoryListComponent,
        FiscalDocumentsParameterComponent,
        FiscalDocumentsParameterCreateEditComponent,
        FiscalDocumentsAssociateItemsBoxComponent,
        SellingBudgetModalComponent,
        FiscalDocumentsAssociateItemsModalComponent,
        AuditContainerComponent,
        CurrencyExchangeManageComponent,
        ModalErrorsNfseComponent,
        PosListComponent,
        PosCreateEditComponent,
        CustomDropdownComponent,
        ThxCurrencyPipe,
        PadPipe,
        GuestTypeListComponent,
        StatementThermalPrinterComponent
      ],
      imports: [
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule,
        TranslateModule.forRoot(),
        RouterTestingModule.withRoutes(appRoutes),
        MomentModule,
        CoreModule
      ],
      providers: [
        {
          provide: InternationalizationService,
          useClass: class InternationalizationServiceStub {
          }
        },
        configureAppName('pms')
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    localStorage.setItem(
      'token',
      'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6WyIwMTk3MzA0ZS0yYTk3LTRhOTEtYWY0OS05YjEzYzVkMWM2MmEiLCIwMTk3MzA0Z' +
      'S0yYTk3LTRhOTEtYWY0OS05YjEzYzVkMWM2MmEiXSwianRpIjoiYWE1YzlkMTE4MWMzNGViMWIwZTVlYmNiMGE2YjQwMzMiLCJuYmYiOjE1MTkwODkwNDAsImV' +
      '4cCI6MTUyMTY4MTA0MCwiaWF0IjoxNTE5MDg5MDQwLCJpc3MiOiJ0aGV4SXNzdWVyIiwiYXVkIjoidGhleEF1ZGllbmNlIn0.Z7gQP5RYkbEjSjStYs67ELm3Q' +
      'KFvujvFGHK3SRbQ1bSZHrxDE9c0Zp5MF-CyqjhQ1xNKYRSjtpIGfKzUjQF1T5d_taLsRTZ5BaLdRM0vSm7w0SgzjUSYG7pseMrV9dju_ePrgxKkXmM-QYnfFeV' +
      '9g8Wx17XH6YJPWPM0eR2r6FdnsoDBOSk8Ba0U6NSd0v6WAoMjYBpwGmv2kMlSZPT_gmyNUb5I_BBlkaObYJma4kalN-IBA5Eodcna5LbWo1-bkap9k4-Zw_DNJ' +
      '29V28AXvbfyNPo13pXyPKxY8K9uABq99oTyz20rCeCesWVyd-waS00rzAw0BkxgaoRWdWNATg',
    );

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    component.companyId = 1;

    fixture.detectChanges();
  });

  it('should create the AppComponent', () => {
    expect(component).toBeTruthy();
  });

  describe('Routes', () => {
    beforeEach(async(() => {
      fixtureCard = TestBed.createComponent(PropertyCardComponent);
      componentPropertyCard = fixtureCard.componentInstance;

      promisePropertyByCompany = spyOn(componentPropertyCard.propertyResource, 'getAll').and.callFake(() => {
        return new Promise((resolve, reject) => resolve(propertiesMock));
      });

      router.navigate(['']);
      router.initialNavigation();
      fixture.detectChanges();
    }));
  });
});
