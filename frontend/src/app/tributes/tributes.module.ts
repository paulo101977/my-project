import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TributesRoutingModule } from 'app/tributes/tributes-routing.module';
import { ProductTributesListComponent } from 'app/tributes/components-container/product-tributes-list/product-tributes-list.component';
import { ProductTributesAddViewComponent } from './components-container/product-tributes-add-view/product-tributes-add-view.component';
import { ModalPublicSearchComponent } from './components/modal-public-search/modal-public-search.component';
import { ModalAddHotelComponent } from './components/modal-add-hotel/modal-add-hotel.component';
import { ServiceTributesListComponent } from './components-container/service-tributes-list/service-tributes-list.component';
import { ServiceTributesAddNewComponent } from './components-container/service-tributes-add-new/service-tributes-add-new.component';
import { ClientTributsWithheldListComponent
} from 'app/tributes/components-container/client-tributs-withheld-list/client-tributs-withheld-list.component';
import { ModalAddClientComponent } from './components/modal-add-client/modal-add-client.component';
import { ModalDeleteClientComponent } from './components/modal-delete-client/modal-delete-client.component';
import { IvaAssociateComponent } from './components-container/iva-associate/iva-associate.component';
import { ThxImgModule } from '@inovacao-cmnet/thx-ui';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TributesRoutingModule,
    ThxImgModule
  ],
  declarations: [
    ProductTributesListComponent,
    ProductTributesAddViewComponent,
    ModalPublicSearchComponent,
    ModalAddHotelComponent,
    ServiceTributesListComponent,
    ServiceTributesAddNewComponent,
    ClientTributsWithheldListComponent,
    ModalAddClientComponent,
    ModalDeleteClientComponent,
    IvaAssociateComponent,
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class TributesModule {}
