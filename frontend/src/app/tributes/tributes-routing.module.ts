import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  ProductTributesListComponent
} from './components-container/product-tributes-list/product-tributes-list.component';
import {
  ProductTributesAddViewComponent
} from './components-container/product-tributes-add-view/product-tributes-add-view.component';
import { ServiceTributesListComponent } from './components-container/service-tributes-list/service-tributes-list.component';
import {
  ServiceTributesAddNewComponent
} from './components-container/service-tributes-add-new/service-tributes-add-new.component';
import {
  ClientTributsWithheldListComponent
} from 'app/tributes/components-container/client-tributs-withheld-list/client-tributs-withheld-list.component';
import { IvaAssociateComponent } from 'app/tributes/components-container/iva-associate/iva-associate.component';


export const tributesRoutes: Routes = [{
    path: 'products',
    component: ProductTributesListComponent,
  },
  {
    path: 'services',
    component: ServiceTributesListComponent
  },
  {
    path: 'products/add-view',
    component: ProductTributesAddViewComponent
  },
  {
    path: 'services/add-view',
    component: ServiceTributesAddNewComponent
  },
  {
    path: 'client/withheld',
    component: ClientTributsWithheldListComponent
  },
  {
    path: 'iva-associate',
    component: IvaAssociateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(tributesRoutes)],
  exports: [RouterModule],
})
export class TributesRoutingModule {}
