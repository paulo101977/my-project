import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { AuthService } from 'app/shared/services/auth-service/auth.service';
import { createAccessorComponent, createServiceStub } from '../../../../../testing';

import { ServiceTributesAddNewComponent } from './service-tributes-add-new.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { PropertyService } from 'app/shared/services/property/property.service';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { tributesResourceStub } from 'app/tributes/resource/testing';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { propertyResourceStub } from 'app/shared/resources/property/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ButtonConfig } from 'app/shared/models/button-config';
import { configureTestSuite } from 'ng-bullet';

const hotelList = [
  {
    id: 1,
    propertyUId: '6146da81-9cc3-46ef-b855-825551594bfe'
  },
  {
    id: 2,
    propertyUId: '22'

  },
  {
    id: 3,
    propertyUId: '1'

  },
  {
    id: 4,
    propertyUId: '44'
  }
];

const hotel = [
  {
    id: 1,
    locationList: [],
    name: 'Hotel Totvs Resorts Rio de Janeiro',
    propertyContactPersonList: [],
    propertyGuestPrefsList: [],
    propertyGuestTypeList: [],
    propertyStatusId: 19,
    propertyTypeId: 1,
    propertyUId: '6146da81-9cc3-46ef-b855-825551594bfe',
    reservationList: [],
    roomList: [],
    roomTypeList: [],
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    totalOfRooms: 0,
    brandId: 1,
    chainId: 0,
    companyId: 1,
    brand: {
      name: 'custom 1'
    }
  },
  {
    id: 2,
    locationList: [],
    name: 'Hotel Totvs Resorts Rio de Janeiro',
    propertyContactPersonList: [],
    propertyGuestPrefsList: [],
    propertyGuestTypeList: [],
    propertyStatusId: 19,
    propertyTypeId: 1,
    propertyUId: '6146da81-9cc3-46ef-b855-825551594bfe',
    reservationList: [],
    roomList: [],
    roomTypeList: [],
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    totalOfRooms: 0,
    brandId: 1,
    chainId: 0,
    companyId: 1,
    brand: {
      name: 'custom 2'
    }
  },
  {
    id: 3,
    locationList: [],
    name: 'Hotel Totvs Resorts Rio de Janeiro',
    propertyContactPersonList: [],
    propertyGuestPrefsList: [],
    propertyGuestTypeList: [],
    propertyStatusId: 19,
    propertyTypeId: 1,
    propertyUId: '6146da81-9cc3-46ef-b855-825551594bfe',
    reservationList: [],
    roomList: [],
    roomTypeList: [],
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    totalOfRooms: 0,
    brandId: 1,
    chainId: 0,
    companyId: 1,
    brand: {
      name: 'custom 3'
    }
  },
  {
    id: 4,
    locationList: [],
    name: 'Hotel Totvs Resorts Rio de Janeiro',
    propertyContactPersonList: [],
    propertyGuestPrefsList: [],
    propertyGuestTypeList: [],
    propertyStatusId: 19,
    propertyTypeId: 1,
    propertyUId: '6146da81-9cc3-46ef-b855-825551594bfe',
    reservationList: [],
    roomList: [],
    roomTypeList: [],
    tenantId: '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    totalOfRooms: 0,
    brandId: 1,
    chainId: 0,
    companyId: 1,
    brand: {
      name: 'custom 4'
    }
  }
];


const cfopCodeArr = { items:
  [
  {
    code: '6949',
    description: '11description'
  },
  {
    code: '6977',
    description: '00description22'
  }
  ]
};

const nbsCodeArr = { items:
  [
  {
    code: '10.3',
    description: '1description'
  },
  {
    code: '1.04.2',
    description: '00description'
  }
  ]
};

const tribute = {
  name: 'Teste Paulo',
  cfopId: '2823c956-0602-4a78-a41c-c6425a66c772',
  cfopCode: '6949',
  description: '11description',
  cstPisCofinsId: 'cba5ddce-f6a5-416f-8a52-98c4516cb291',
  natureId: '6a3fff92-eae3-4f51-9456-939c9fd81881',
  nbsId: 'c4a5b4a9-a13c-4aff-b381-99aff8fd5675',
  nbsCode: '1.03',
  csosnId: '44444333443',
  csosn: '44444333443',
  taxPercentual: 11.11,
  taxPercentualBase: 11.11,
  serviceTypeId: '5a0eb99d-85e0-48b7-a3a4-9ac8a1ee60af',
  type: 1,
  isActive: true,
  propertyId: 1,
  propertyUId: '12312',
  taxCalculationId: '40e098a2-4ea5-436e-abf7-9593b587b3aa',
  propertyList: [{
    propertyUId: '6146da81-9cc3-46ef-b855-825551594bfe',
    tenantId: '1'
  }]
};
const obj = {
  nameOfTributeRule: tribute.name,
  nbs: tribute.nbsCode,
  isActive: tribute.isActive,
  cfop: `${ tribute.cfopCode } - ${ tribute.description }`,
  aliquot: tribute.taxPercentual,
  basePercentage: tribute.taxPercentualBase,
  serviceType: tribute.serviceTypeId,
  cstPisCofis: tribute.cstPisCofinsId,
  determinationSystem: tribute.taxCalculationId,
  natureOfRevenue: tribute.natureId,
};
const propertyServiceStub: Partial<PropertyService> = {
  getPropertyUIdfrom(propertyId: number): string {
    return '123';
  }
};

const AuthProvider = createServiceStub(AuthService, {
  getPropertyCountryCode(): any { return 'br'; }
});

const thxError = createAccessorComponent('thx-error');

describe('ServiceTributesAddNewComponent', () => {
  let component: ServiceTributesAddNewComponent;
  let fixture: ComponentFixture<ServiceTributesAddNewComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ServiceTributesAddNewComponent,
        thxError,
      ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: PropertyService,
          useValue: propertyServiceStub
        },
        AuthProvider,
        sharedServiceStub,
        tributesResourceStub,
        toasterEmitServiceStub,
        propertyResourceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ServiceTributesAddNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn<any>(component['route'], 'params').and.returnValue( of({property: 1} ));
    spyOn<any>(component, 'setHeaderConfig');
    spyOn(component, 'setButtons').and.callThrough();
    spyOn(component, 'setForm').and.callThrough();
    spyOn(component, 'setColumns');
    spyOn(component, 'setInputMask');
    spyOn(component, 'getServiceTypeList');
    spyOn(component, 'getPisCofins');
    spyOn(component, 'getDeterminationSystemList');
    spyOn(component, 'getNatureOfRevenueList');
    spyOn(component, 'getTributeServiceById').and.callThrough();
    spyOn<any>(component['tributesResource'], 'getTributeById')
        .and
        .returnValue(of(tribute));

    spyOn(component, 'getSearchCFOP').and.callThrough();
    spyOn<any>(component['tributesResource'], 'getSearchCFOP')
        .and
        .returnValue(of(cfopCodeArr));

    spyOn(component, 'getSearchNBS').and.callThrough();
    spyOn<any>(component['tributesResource'], 'getSearchNBS')
        .and
        .returnValue(of(nbsCodeArr));
    spyOn<any>(component['sharedService'], 'getButtonConfig').and.returnValue(new ButtonConfig());
    spyOn<any>(component, 'getAllHotel').and.returnValue( hotel );
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', fakeAsync(() => {

    component.ngOnInit();
    tick();

    expect(component['setHeaderConfig']).toHaveBeenCalled();
    expect(component['setButtons']).toHaveBeenCalled();
    expect(component['setForm']).toHaveBeenCalled();
    expect(component['setColumns']).toHaveBeenCalled();
    expect(component['setInputMask']).toHaveBeenCalled();
    expect(component['getServiceTypeList']).toHaveBeenCalled();
    expect(component['getPisCofins']).toHaveBeenCalled();
    expect(component['getDeterminationSystemList']).toHaveBeenCalled();
    expect(component['getNatureOfRevenueList']).toHaveBeenCalled();
  }));

  it('should test if has serviceId', fakeAsync(() => {

    spyOn<any>(component['route']['snapshot']['queryParamMap'], 'get')
    .and
    .returnValue(of('1'));

    component.ngOnInit();
    component.form.patchValue(obj);
    spyOn<any>(component['form'], 'disable').and.callFake( f => f );

    tick();

    expect(component.isReadonly).toEqual(true);
    expect(component['form'].disable).toHaveBeenCalled();


    expect(component.setColumns).toHaveBeenCalled();
  }));

  it('should test if check form correctly', fakeAsync(() => {
    spyOn(component['form'], 'setValue');


    spyOn<any>(component['propertyResource'], 'getAll')
      .and.returnValue(of({items: hotel}));


    component.isReadonly = true;
    component.getTributeServiceById('1');
    tick();

    expect(component.getTributeServiceById).toHaveBeenCalledWith('1');
    expect(component['form'].setValue).toHaveBeenCalledWith(obj);
    expect(component.hotelItemsList.length).toEqual(1);
  }));

  it('should test hotelSelectedMethod', fakeAsync(() => {

    spyOn(component, 'hotelHasBeenSelected').and.callThrough();

    component.hotelHasBeenSelected(hotelList);

    expect(component.hotelHasBeenSelected).toHaveBeenCalledWith(hotelList);
    expect(component.hotelItemsList).toEqual(hotelList);
    expect(component.hotelRemoved).toEqual(null);
  }));

  it('should test if hotel has been removed from list', fakeAsync(() => {

    spyOn(component, 'removeHotelFromList').and.callThrough();

    component.hotelItemsList = [...hotelList];

    component.removeHotelFromList(hotelList[2]);

    expect(component.removeHotelFromList).toHaveBeenCalledWith(hotelList[2]);
    expect(component.hotelItemsList.length).toEqual(3);
  }));
});
