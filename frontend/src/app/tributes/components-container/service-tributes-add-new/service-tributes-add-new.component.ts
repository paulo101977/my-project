import { AutocompleteConfig } from './../../../shared/models/autocomplete/autocomplete-config';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ModalAddHotelComponent } from 'app/tributes/components/modal-add-hotel/modal-add-hotel.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { ModalPublicSearchComponent } from 'app/tributes/components/modal-public-search/modal-public-search.component';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { ActivatedRoute } from '@angular/router';
import { TributesResource } from 'app/tributes/resource/tributes.resource';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { Location } from '@angular/common';
import { CFOP, CST, DeterminationSystem, HotelSender, NatureOfRevenue, NBS, ServiceType } from 'app/tributes/models/tributes';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { TranslateService } from '@ngx-translate/core';
import { PropertyService } from 'app/shared/services/property/property.service';

@Component({
  selector: 'app-service-tributes-add-new',
  templateUrl: './service-tributes-add-new.component.html',
  styleUrls: ['./service-tributes-add-new.component.scss']
})
export class ServiceTributesAddNewComponent implements OnInit {

  @ViewChild('modalSearch') modalSearch: ModalPublicSearchComponent;
  @ViewChild('modalAddHotel') modalAddHotel: ModalAddHotelComponent;

  // header
  public configHeaderPage: ConfigHeaderPageNew;

  // buttons config
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;
  public buttonInsertHotel: ButtonConfig;

  // datatable
  public columns: Array<any>;

  public form: FormGroup;

  public autocompleteConfigCFOP: AutocompleteConfig;
  public autocompleteConfigNBS: AutocompleteConfig;
  public hotelItemsList: Array<any>;
  public cfopList: Array<CFOP>;
  public serviceTypeList: Array<ServiceType>;
  public cstPisCofinsList: Array<CST>;
  public determinationSystemList: Array<DeterminationSystem>;
  public natureOfRevenueList: Array<NatureOfRevenue>;
  public nbsList: Array<NBS>;
  public propertyId: number;
  public propertyUId: string;
  public isReadonly: boolean;
  public country: string;
  public hotelRemoved: any;
  public itemSelectedCFOP: any;
  public itemSelectedNBS: any;
  public title: string;

  // optionsCurrencyMask
  public optionsCurrencyMask: any;

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private tributesResource: TributesResource,
    private route: ActivatedRoute,
    private toasterService: ToasterEmitService,
    private location: Location,
    private propertyResource: PropertyResource,
    private translate: TranslateService,
    private propertyService: PropertyService
  ) { }

  public setInputMask() {
    // the input number mask
    this.optionsCurrencyMask = {
      prefix: '',
      thousands: '.',
      decimal: ',',
      align: 'left',
      allowZero: true,
      allowNegative: false
    };
  }

  public getServiceTypeList(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getServiceTypeList()
        .subscribe( ({items}) => {
          this.serviceTypeList = items;
          this.serviceTypeList = items.map( ret => {
            return {
              ...ret,
              key: ret.id,
              value: `${ret.code} - ${ret.description}`
            };
          });
          resolve(null);
        });
    });
  }

  public normalizeValue(arr) {
    return arr.map( item => {
      return {
        ...item,
        value: `${item.value}`
      };
    });
  }

  public getPisCofins(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getPisCofins()
        .subscribe( ({items}) => {
          this.cstPisCofinsList = items.map( ret => {
            return {
              ...ret,
              key: ret.id,
              value: `${ret.code} - ${ret.description}`
            };
          });
          resolve(null);
        });
    });
  }


  public getDeterminationSystemList(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getDeterminationSystemList()
        .subscribe( ({items}) => {
          this.determinationSystemList = items.map( ret => {
            return {
              ...ret,
              key: ret.id,
              value: `${ret.description}`
            };
          });
          resolve(null);
        });
    });
  }


  public getNatureOfRevenueList(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getNatureOfRevenueList()
        .subscribe( ({items}) => {
          this.natureOfRevenueList = items.map( ret => {
            return {
              ...ret,
              key: ret.id,
              value: `${ret.code} - ${ret.description}`
            };
          });
          resolve(null);
        });
    });
  }

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
      // TODO: comment until next sprint
      // keepTitleButton: true,
      // callBackFunction: this.callModalSearchPublicDatabase
    };
  }

  public setColumns() {
    this.columns = [
      {
        name: 'label.name',
        prop: 'name',
      },
      {
        name: 'label.chain',
        prop: 'brand',
      }
    ];
  }

  public setButtons() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'button-cancel',
      this.goBack,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'button-confirm',
      this.confirm,
      'action.confirm',
      ButtonType.Primary,
      null,
      true,
    );
    this.buttonInsertHotel = this.sharedService.getButtonConfig(
      'button-confirm',
      this.insertHotel,
      'action.insertHotel',
      ButtonType.Secondary,
      ButtonSize.Small
    );
  }

  public setForm() {
    this.form = this.formBuilder.group({
      nameOfTributeRule: [ null, [ Validators.required ]],
      nbs: [ null, [ Validators.required ]],
      isActive: [ true ],
      cfop: [ null, [ Validators.required ] ],
      aliquot: [ null, [ Validators.required ] ],
      basePercentage: [ null, [ Validators.required ] ],
      serviceType: [ null, [ Validators.required ] ],
      cstPisCofis: [ null, [ Validators.required ]],
      determinationSystem: [ null, [ Validators.required ]],
      natureOfRevenue: [ null, [ Validators.required ]],
    });
  }

  public insertHotel = () => {
    this.modalAddHotel.toggleModal();
  }

  public removeHotelFromList(itemToRemove) {
    if ( this.hotelItemsList ) {
      this.hotelRemoved = itemToRemove;
      this.hotelItemsList = this.hotelItemsList.filter( item => {
        return item.id !== itemToRemove.id;
      });
    }
  }

  public hotelHasBeenSelected(items) {
    this.hotelRemoved = null;
    this.hotelItemsList = items;
  }

  public confirm = () => {

    const {
      hotelItemsList,
      itemSelectedCFOP,
      itemSelectedNBS
    } = this;

    const {
      nameOfTributeRule,
      isActive,
      aliquot,
      basePercentage,
      serviceType,
      cstPisCofis,
      determinationSystem,
      natureOfRevenue,
    } = this.form.getRawValue();


    const objToSend = {
      name: nameOfTributeRule,
      cfopId: itemSelectedCFOP,
      cstPisCofinsId: cstPisCofis,
      natureId: natureOfRevenue,
      nbsId: itemSelectedNBS,
      taxPercentual: aliquot,
      taxPercentualBase: basePercentage,
      serviceTypeId: serviceType,
      type: 1,
      isActive,
      propertyUId: this.propertyUId,
      taxCalculationId: determinationSystem,
      propertyList: null
    };

    if ( hotelItemsList ) {
      objToSend['propertyList'] = <Array<HotelSender>>hotelItemsList.map( hotel => {
        return {
          propertyUId: hotel.propertyUId,
          tenantId: hotel.tenantId
        };
      });
    }

    this
      .tributesResource
      .insertNewTribute(objToSend)
      .subscribe( () => {
        this
          .toasterService
          .emitChange(SuccessError.success, 'label.tributeInsertSuccessMessage');
        this.goBack();
      });
  }

  public goBack = () => {
    this.location.back();
  }

  // TODO: comment until next sprint
  // public callModalSearchPublicDatabase = () => {
  //   if ( !this.isReadonly ) {
  //     this.modalSearch.toggleModal();
  //   }
  // }

  public  getTributeServiceById(id) {
    this
      .tributesResource
      .getTributeById(id)
      .subscribe( async item => {
        const hotelList = await this.getAllHotel();

        const {
          name,
          cfopCode,
          nbsCode,
          isActive,
          taxPercentual,
          taxPercentualBase,
          propertyList,
          serviceTypeId,
          cstPisCofinsId,
          taxCalculationId,
          natureId,
        } = item;

        await this.getSearchCFOP(cfopCode);

        this.hotelItemsList = [];

        this.form.setValue({
          nameOfTributeRule: name,
          nbs: nbsCode,
          isActive,
          cfop: this.itemSelectedCFOP,
          aliquot: taxPercentual,
          basePercentage: taxPercentualBase,
          serviceType: serviceTypeId,
          cstPisCofis: cstPisCofinsId,
          determinationSystem: taxCalculationId,
          natureOfRevenue: natureId,
        });

        propertyList.forEach( pItem => {
          const finded = hotelList.find( hItem => hItem.propertyUId === pItem.propertyUId);

          if ( finded ) {
            this.hotelItemsList.push({
              name: finded.name,
              brand: finded.brand ? finded.brand.name : '',
            });
          }
        });
      });
  }

  public getAllHotel(): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .propertyResource
        .getAll()
        .subscribe( ({ items }) => {
          resolve(items);
        });
    });
  }

  ngOnInit() {
    this.setHeaderConfig();
    this.setButtons();
    this.setForm();
    this.setColumns();
    this.setInputMask();
    this.configureAutoComplete();
    this.title = 'title.insertTributeServiceNew';

    this.route.params.subscribe(async params => {
      this.propertyId = +params.property;
      this.propertyUId = this.propertyService.getPropertyUIdfrom(this.propertyId);

      const serviceId =  this.route.snapshot.queryParamMap.get('serviceId');

      await this.getServiceTypeList();
      await this.getPisCofins();
      await this.getDeterminationSystemList();
      await this.getNatureOfRevenueList();

      this.form.valueChanges.subscribe( () => {
        this.confirmButtonConfig.isDisabled = !this.form.valid;
      });

      if ( serviceId ) {
        this.form.disable();
        this.isReadonly = true;
        this.title = 'title.consultTributeService';
        this.setColumns();
        this.buttonInsertHotel.isDisabled = true;

        this.getTributeServiceById( serviceId );
      }
    });
  }

  private configureAutoComplete(): void {
    // CFOP
    this.autocompleteConfigCFOP = new AutocompleteConfig();
    this.autocompleteConfigCFOP.dataModel = 'code';
    this.autocompleteConfigCFOP.resultList = [];
    this.autocompleteConfigCFOP.callbackToSearch = $event => this.getSearchCFOP($event);
    this.autocompleteConfigCFOP.callbackToGetSelectedValue = data => {
      const { id, code } = data;
      this.itemSelectedCFOP = id;
    };
    this.autocompleteConfigCFOP.fieldObjectToDisplay = 'template';
    this.autocompleteConfigCFOP.placeholder = this.translate.instant('placeholder.searchByDescriptionOrCFOP');

    // NBS
    this.autocompleteConfigNBS = new AutocompleteConfig();
    this.autocompleteConfigNBS.dataModel = 'code';
    this.autocompleteConfigNBS.resultList = [];
    this.autocompleteConfigNBS.callbackToSearch = $event => this.getSearchNBS($event);
    this.autocompleteConfigNBS.callbackToGetSelectedValue = data => {
      const { id, code } = data;
      this.itemSelectedNBS = id;
    };
    this.autocompleteConfigNBS.fieldObjectToDisplay = 'template';
    this.autocompleteConfigNBS.placeholder = this.translate.instant('placeholder.searchByCodeNBS');
  }

  public getSearchCFOP($event): Promise<any> {
    return new Promise( resolve => {
      const term: string = $event.query === undefined ? $event : $event.query;
      const regex = new RegExp(term, 'i');

      this
      .tributesResource
      .getSearchCFOP(term)
      .subscribe( ({items}) => {
        this.cfopList = items
        .filter(({code, description}) => regex.test(code.toString()) || regex.test(description))
        .map(item => {
          if ( this.isReadonly ) {
            this.itemSelectedCFOP = `${item.code} - ${item.description}`;
          }
          item['template'] = `${item.code} - ${item.description}`;
        return item;
        });
        resolve(null);
      });

    });
  }

  public getSearchNBS($event) {

    const term: string = $event.query;
    const regex = new RegExp(term, 'i');

    this
      .tributesResource
      .getSearchNBS(term)
      .subscribe( ({items}) => {
        this.nbsList = items
        .filter(({code, description }) => regex.test(code.toString()) || regex.test(description))
        .map(item => {
          item['template'] = `${item.code}`;
        return item;
        });
      }
    );
  }

}

