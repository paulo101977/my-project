import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { AuthService } from 'app/shared/services/auth-service/auth.service';
import { createAccessorComponent, createServiceStub } from '../../../../../testing';
import { ProductTributesAddViewComponent } from './product-tributes-add-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { PropertyService } from 'app/shared/services/property/property.service';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { tributesResourceStub } from 'app/tributes/resource/testing';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { propertyResourceStub } from 'app/shared/resources/property/testing';

const hotelList = [
  {
    id: 1,
  },
  {
    id: 2,
  },
  {
    id: 3,
  },
  {
    id: 4,
  }
];

const tribute = {
  name: 'Teste Paulo',
  cfopId: '2823c956-0602-4a78-a41c-c6425a66c772',
  cfopCode: '6949',
  description: '11description',
  cstPisCofinsId: 'cba5ddce-f6a5-416f-8a52-98c4516cb291',
  natureId: '6a3fff92-eae3-4f51-9456-939c9fd81881',
  ncmId: 'c4a5b4a9-a13c-4aff-b381-99aff8fd5675',
  ncmCode: '1.03',
  taxPercentual: 11.11,
  taxPercentualSub: 11.11,
  type: 0,
  isActive: true,
  propertyUId: '123',
  taxCalculationId: '40e098a2-4ea5-436e-abf7-9593b587b3aa',
  barCode: '11111111',
  csosnId: '44444333443',
  cstAid: '233233232',
  cstBid: '34444444',
  ufId: 'oo999',
  propertyList: [{
    propertyId: 1,
    tenantId: '2'
  }]
};

const trib = {
  name: 'Teste Paulo',
  cfopId: '2823c956-0602-4a78-a41c-c6425a66c772',
  cstPisCofinsId: 'cba5ddce-f6a5-416f-8a52-98c4516cb291',
  natureId: '6a3fff92-eae3-4f51-9456-939c9fd81881',
  ncmId: 'c4a5b4a9-a13c-4aff-b381-99aff8fd5675',
  taxPercentual: 11.11,
  taxPercentualSub: 11.11,
  type: 0,
  isActive: true,
  propertyId: 1,
  taxCalculationId: '40e098a2-4ea5-436e-abf7-9593b587b3aa',
  barCode: '11111111',
  csosnId: '44444333443',
  csonsn: '44444333443',
  cstAid: '233233232',
  cstBid: '34444444',
  ufId: 'oo999',
  propertyList: [{
    propertyUId: '12312',
    tenantId: '2'
  }]
};

const hotel = [
  {
    id: 1,
    name: 'Hotel Totvs Resorts Rio de Janeiro',
    brand: {
      name: 'custom 1'
    }
  },
  {
    id: 2,
    name: 'Hotel Totvs Resorts Rio de Janeiro',
    brand: {
      name: 'custom 2'
    }
  },
  {
    id: 3,
    name: 'Hotel Totvs Resorts Rio de Janeiro',
    brand: {
      name: 'custom 3'
    }
  },
  {
    id: 4,
    name: 'Hotel Totvs Resorts Rio de Janeiro',
    brand: {
      name: 'custom 4'
    }
  }
];

const cfopCodeArr = { items:
  [
  {
    id: '2823c956-0602-4a78-a41c-c6425a66c772',
    code: '6949',
    description: '11description'
  },
  {
    id: '2823c956-0602-4a78-a41c-c6425a66c999',
    code: '6977',
    description: '00description22'
  }
  ]
};

const ncmCodeArr = { items:
  [
  {
    id: 'c4a5b4a9-a13c-4aff-b381-99aff8fd5675',
    ncmCode: '1.03'
  },
  {
    id: 'c4a5b4a9-a13c-4aff-b381-99aff8fd3874',
    ncmCode: '10.02'
  }
  ]
};

const propertyServiceStub: Partial<PropertyService> = {
  getPropertyUIdfrom(propertyId: number): string {
    return '123';
  }
};

const AuthProvider = createServiceStub(AuthService, {
  getPropertyCountryCode(): any { return 'br'; }
});

const thxError = createAccessorComponent('thx-error');

describe('ProductTributesAddViewComponent', () => {
  let component: ProductTributesAddViewComponent;
  let fixture: ComponentFixture<ProductTributesAddViewComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductTributesAddViewComponent,
        thxError
      ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: PropertyService,
          useValue: propertyServiceStub
        },
        AuthProvider,
        sharedServiceStub,
        tributesResourceStub,
        toasterEmitServiceStub,
        propertyResourceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(ProductTributesAddViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn<any>(component, 'setHeaderConfig');
    spyOn(component, 'setButtons');
    spyOn(component, 'setForm');
    spyOn(component, 'setColumns');
    spyOn(component, 'setInputMask');
    spyOn(component, 'getCSOSN');
    spyOn(component, 'getPisCofins');
    spyOn(component, 'getCstPartAList');
    spyOn(component, 'getCstPartBList');
    spyOn(component, 'getUf');
    spyOn(component, 'getDeterminationSystemList');
    spyOn(component, 'getNatureOfRevenueList');
    spyOn<any>(component, 'getTributeProductById').and.callFake( f => f );
    spyOn<any>(component['tributesResource'], 'getTributeById')
        .and
        .returnValue(of(tribute));

    spyOn(component, 'getSearchCFOP').and.callThrough();
    spyOn<any>(component['tributesResource'], 'getSearchCFOP')
        .and
        .returnValue(of(cfopCodeArr));

    spyOn(component, 'getSearchNCM').and.callThrough();
    spyOn<any>(component['tributesResource'], 'getSearchNCM')
        .and
        .returnValue(of(ncmCodeArr));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', fakeAsync(() => {

    component.ngOnInit();
    tick();

    expect(component['setHeaderConfig']).toHaveBeenCalled();
    expect(component['setButtons']).toHaveBeenCalled();
    expect(component['setForm']).toHaveBeenCalled();
    expect(component['setColumns']).toHaveBeenCalled();
    expect(component['setInputMask']).toHaveBeenCalled();

    expect(component['getCSOSN']).toHaveBeenCalled();
    expect(component['getCstPartAList']).toHaveBeenCalled();
    expect(component['getCstPartBList']).toHaveBeenCalled();
    expect(component['getUf']).toHaveBeenCalled();
    expect(component['getPisCofins']).toHaveBeenCalled();
    expect(component['getDeterminationSystemList']).toHaveBeenCalled();
    expect(component['getNatureOfRevenueList']).toHaveBeenCalled();
  }));

  it('should test if has productId', fakeAsync(() => {

    spyOn<any>(component['route']['snapshot']['queryParamMap'], 'get')
      .and
      .returnValue(of('1'));

    spyOn(component['form'], 'disable');

    component.ngOnInit();
    tick();

    expect(component.isReadonly).toEqual(true);
    expect(component['form'].disable).toHaveBeenCalled();
    expect(component.setColumns).toHaveBeenCalled();
    expect(component.buttonInsertHotel.isDisabled).toEqual(true);
  }));

  xit('should test if check form correctly', fakeAsync(() => {

    spyOn(component['form'], 'setValue');
    spyOn(component, 'getAllHotel').and.callThrough();


    spyOn<any>(component['propertyResource'], 'getAll')
      .and.returnValue(of({items: hotel}));

    component.isReadonly = true;
    component.getTributeProductById('1');
    tick();

    const obj = {
      nameOfTributeRule: tribute.name,
      ncm: tribute.ncmCode,
      isActive: tribute.isActive,
      barCode: tribute.barCode,
      cfop: component.itemSelectedCFOP,
      csosnId: tribute.csosnId,
      csosn: tribute.csosnId,
      cstPartA: tribute.cstAid,
      cstPartB: tribute.cstBid,
      aliquot: tribute.taxPercentual,
      aliquotB: tribute.taxPercentualSub,
      uf: tribute.ufId,
      cstPisCofis: tribute.cstPisCofinsId,
      determinationSystem: tribute.taxCalculationId,
      natureOfRevenue: tribute.natureId,
    };

    expect(component.getTributeProductById).toHaveBeenCalledWith('1');
    expect(component['form'].setValue).toHaveBeenCalledWith(obj);
    expect(component.hotelItemsList.length).toEqual(1);
  }));

  xit('should test confirm method', fakeAsync(() => {

    spyOn(component['tributesResource'], 'insertNewTribute')
      .and
      .returnValue(of(true));
    spyOn(component['toasterService'], 'emitChange');

    spyOn(component, 'goBack');

    spyOn(component['form']['valueChanges'], 'subscribe').and.callThrough();

    component.country = 'pt';
    component.propertyId = 1;
    component.propertyUId = '123';
    component.hotelItemsList = [
      {
        id: 1,
        tenantId: '2'
      }
    ];
    component.itemSelectedCFOP = cfopCodeArr.items[0]['id'];
    component.itemSelectedNCM = ncmCodeArr.items[0]['id'];

    const obj = {
      aliquot: tribute.taxPercentual,
      aliquotB: tribute.taxPercentualSub,
      barCode: tribute.barCode,
      cfop: tribute.cfopId,
      csosn: tribute.csosnId,
      cstPartA: tribute.cstAid,
      cstPartB: tribute.cstBid,
      cstPisCofis: tribute.cstPisCofinsId,
      determinationSystem: tribute.taxCalculationId,
      isActive: tribute.isActive,
      nameOfTributeRule: tribute.name,
      natureOfRevenue: tribute.natureId,
      ncm: tribute.ncmId,
      uf: tribute.ufId
    };

    component['form'].setValue(obj);
    component.confirm();
    tick();

    expect(component['tributesResource'].insertNewTribute)
      .toHaveBeenCalledWith(trib);

    expect(component['toasterService'].emitChange)
      .toHaveBeenCalledWith(true, 'label.tributeInsertSuccessMessage');
    expect(component.goBack).toHaveBeenCalled();
    expect(component['form'].valid).toEqual(true);
  }));

  it('should test hotelSelectedMethod', () => {

    spyOn(component, 'hotelHasBeenSelected').and.callThrough();

    component.hotelHasBeenSelected(hotelList);

    expect(component.hotelHasBeenSelected).toHaveBeenCalledWith(hotelList);
    expect(component.hotelItemsList).toEqual(hotelList);
    expect(component.hotelRemoved).toEqual(null);
  });

  it('should test if hotel has been removed from list', () => {

    spyOn(component, 'removeHotelFromList').and.callThrough();

    component.hotelItemsList = [...hotelList];

    component.removeHotelFromList(hotelList[2]);

    expect(component.removeHotelFromList).toHaveBeenCalledWith(hotelList[2]);
    expect(component.hotelItemsList.length).toEqual(3);
  });
});
