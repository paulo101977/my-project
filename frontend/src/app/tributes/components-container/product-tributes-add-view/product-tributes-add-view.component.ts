import { AutocompleteConfig } from 'app/shared/models/autocomplete/autocomplete-config';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { ButtonConfig, ButtonSize, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalPublicSearchComponent } from 'app/tributes/components/modal-public-search/modal-public-search.component';
import { ModalAddHotelComponent } from 'app/tributes/components/modal-add-hotel/modal-add-hotel.component';
import { TributesResource } from 'app/tributes/resource/tributes.resource';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { CFOP, CSOSN, CST, DeterminationSystem, HotelSender, NatureOfRevenue, NCM, UF } from 'app/tributes/models/tributes';
import { PropertyResource } from 'app/shared/resources/property/property.resource';
import { TranslateService } from '@ngx-translate/core';
import { PropertyService } from 'app/shared/services/property/property.service';

@Component({
  selector: 'app-product-tributes-add-view',
  templateUrl: './product-tributes-add-view.component.html',
  styleUrls: ['./product-tributes-add-view.component.scss']
})
export class ProductTributesAddViewComponent implements OnInit {

  @ViewChild('modalSearch') modalSearch: ModalPublicSearchComponent;
  @ViewChild('modalAddHotel') modalAddHotel: ModalAddHotelComponent;

  public isReadonly = false;

  // optionsCurrencyMask
  public optionsCurrencyMask: any;

  // header
  public configHeaderPage: ConfigHeaderPageNew;

  // buttons config
  public cancelButtonConfig: ButtonConfig;
  public confirmButtonConfig: ButtonConfig;
  public buttonInsertHotel: ButtonConfig;

  // datatable
  public columns: Array<any>;

  public form: FormGroup;

  // TODO: create model to hotelItemList
  public hotelItemsList: Array<any>;
  public cfopList: Array<CFOP>;
  public csosnList: Array<CSOSN>;
  public cstPartAList: Array<CST>;
  public cstPartBList: Array<CST>;
  public ufList: Array<UF>;
  public cstPisCofinsList: Array<CST>;
  public determinationSystemList: Array<DeterminationSystem>;
  public natureOfRevenueList: Array<NatureOfRevenue>;
  public ncmList: Array<NCM>;
  public propertyId: number;
  public propertyUId: string;
  public hotelRemoved: any;
  public country: string;
  public itemSelectedCFOP: any;
  public itemSelectedNCM: any;
  public autocompleteConfigCFOP: AutocompleteConfig;
  public autocompleteConfigNCM: AutocompleteConfig;
  public title: string;

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private tributesResource: TributesResource,
    private route: ActivatedRoute,
    private toasterService: ToasterEmitService,
    private location: Location,
    private propertyResource: PropertyResource,
    private translate: TranslateService,
    private propertyService: PropertyService
  ) { }

  public setInputMask() {
    // the input number mask
    this.optionsCurrencyMask = {
      prefix: '',
      thousands: '.',
      decimal: ',',
      align: 'left',
      allowZero: true,
      allowNegative: false
    };
  }

  public normalizeValue(arr) {
    return arr.map( item => {
      return {
        ...item,
        value: `${item.value}`
      };
    });
  }

  public getCSOSN(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getCSOSN()
        .subscribe( ({items}) => {
          this.csosnList = items.map( ret => {
            return {
              ...ret,
              key: ret.id,
              value: `${ret.code} - ${ret.description}`
            };
          });
          resolve(null);
        });
    });
  }

  public getCstPartAList(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getCstPartAList()
        .subscribe( ({items}) => {
          this.cstPartAList = items.map( ret => {
            return {
              ...ret,
              key: ret.id,
              value: ret.description
            };
          });
          resolve(null);
        });
    });
  }

  public getCstPartBList(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getCstPartBList()
        .subscribe( ({items}) => {
          this.cstPartBList = items.map( ret => {
            return {
              ...ret,
              key: ret.id,
              value: `${ret.code} - ${ret.description}`
            };
          });
          resolve(null);
        });
    });
  }

  public getUf(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getUf()
        .subscribe( ({items}) => {
          this.ufList = items.map( ret => {
            return {
              ...ret,
              key: ret.id,
              value: ret.initials
            };
          });
          resolve(null);
        });
    });
  }

  public getPisCofins(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getPisCofins()
        .subscribe( ({items}) => {
          this.cstPisCofinsList = items.map( ret => {
            return {
              ...ret,
              key: ret.id,
              value: `${ret.code} - ${ret.description}`
            };
          });
          resolve(null);
        });
    });
  }

  public getDeterminationSystemList(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getDeterminationSystemList()
        .subscribe( ({items}) => {
          this.determinationSystemList = items.map( ret => {
              return {
                ...ret,
                key: ret.id,
                value: ret.description
              };
            });

          resolve(null);
        });
    });
  }

  public getNatureOfRevenueList(): Promise<any> {
    return new Promise( resolve => {
      this
        .tributesResource
        .getNatureOfRevenueList()
        .subscribe( ({items}) => {
          this.natureOfRevenueList = items.map( ret => {
            return {
              ...ret,
              key: ret.id,
              value: `${ret.code} - ${ret.description}`
            };
          });
          resolve(null);
        });
    });
  }

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: true,
      // TODO: comment until next sprint
      // keepTitleButton: true,
      // callBackFunction: this.callModalSearchPublicDatabase
    };
  }

  public setColumns() {
    this.columns = [
      {
        name: 'label.name',
        prop: 'name',
      },
      {
        name: 'label.chain',
        prop: 'brand',
      }
    ];
  }

  public setButtons() {
    this.cancelButtonConfig = this.sharedService.getButtonConfig(
      'button-cancel',
      this.goBack,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.confirmButtonConfig = this.sharedService.getButtonConfig(
      'button-confirm',
      this.confirm,
      'action.confirm',
      ButtonType.Primary,
      null,
      true,
    );
    this.buttonInsertHotel = this.sharedService.getButtonConfig(
      'button-confirm',
      this.insertHotel,
      'action.insertHotel',
      ButtonType.Secondary,
      ButtonSize.Small
    );
  }

  public setForm() {
    this.form = this.formBuilder.group({
      nameOfTributeRule: [ null, [ Validators.required ]],
      ncm: [ null, [ Validators.required ]],
      isActive: [ true ],
      barCode: [ null ],
      cfop: [ null, [ Validators.required ] ],
      csosn: [ null ],
      csosnId: [null],
      cstPartA: [ null, [ Validators.required ] ],
      cstPartB: [ null, [ Validators.required ] ],
      aliquot: [ null, [ Validators.required ] ],
      aliquotB: [ null, [ Validators.required ] ],
      uf: [ null ],
      cstPisCofis: [ null, [ Validators.required ]],
      determinationSystem: [ null, [ Validators.required ]],
      natureOfRevenue: [ null, [ Validators.required ]],
    });
  }

  public insertHotel = () => {
    this.modalAddHotel.toggleModal();
  }

  public goBack = () => {
    this.location.back();
  }

  public confirm = () => {

    const item = this.form.getRawValue();

    const {
      hotelItemsList,
      propertyId,
      itemSelectedCFOP,
      itemSelectedNCM
    } = this;

    const {
      aliquot,
      aliquotB,
      barCode,
      csosn,
      csosnId,
      cstPartA,
      cstPartB,
      cstPisCofis,
      determinationSystem,
      isActive,
      nameOfTributeRule,
      natureOfRevenue,
      uf,
    } = item;


    const objToSend = {
      isActive,
      ncmId: itemSelectedNCM,
      cstAid: cstPartA,
      cstBid: cstPartB,
      cstPisCofinsId: cstPisCofis,
      barCode,
      taxPercentual: aliquot,
      taxPercentualSub: aliquotB,
      natureId: natureOfRevenue,
      taxCalculationId: determinationSystem,
      type: 0,
      ufId: uf,
      cfopId: itemSelectedCFOP,
      csosnId: csosnId,
      name: nameOfTributeRule,
      propertyUId: this.propertyUId,
      propertyList: null
    };

    if ( hotelItemsList ) {
      objToSend['propertyList'] = <Array<HotelSender>>hotelItemsList.map( hotel => {
        return {
          propertyId: hotel.id,
          propertyUId: hotel.propertyUId,
          tenantId: hotel.tenantId
        };
      });
    }


    this
      .tributesResource
      .insertNewTribute(objToSend)
      .subscribe( () => {
        this
          .toasterService
          .emitChange(SuccessError.success, 'label.tributeInsertSuccessMessage');
        this.goBack();
    });
  }

  // TODO: comment until next sprint
  // public callModalSearchPublicDatabase = () => {
  //   if ( !this.isReadonly ) {
  //     this.modalSearch.toggleModal();
  //   }
  // }

  public removeHotelFromList(itemToRemove) {
    if ( this.hotelItemsList ) {
      this.hotelRemoved = itemToRemove;
      this.hotelItemsList = this.hotelItemsList.filter( item => {
        return item.id !== itemToRemove.id;
      });
    }
  }


  public getTributeProductById(id) {
    this
      .tributesResource
      .getTributeById(id)
      .subscribe( async item => {
        const hotelList = await this.getAllHotel();

        const {
          name,
          cfopCode,
          ncmCode,
          isActive,
          taxPercentual,
          propertyList,
          cstPisCofinsId,
          taxCalculationId,
          natureId,
          csosnId,
          csosn,
          cstAid,
          cstBid,
          ufId,
          taxPercentualSub,
          barCode
        } = item;

        await this.getSearchCFOP(cfopCode);

        this.form.setValue({
          nameOfTributeRule: name,
          ncm: ncmCode,
          isActive: isActive,
          barCode: barCode ? barCode : '',
          cfop: this.itemSelectedCFOP,
          csosnId: csosnId || null,
          csosn: csosn || null,
          cstPartA: cstAid,
          cstPartB: cstBid,
          aliquot: taxPercentual,
          aliquotB: taxPercentualSub,
          uf: ufId,
          cstPisCofis: cstPisCofinsId,
          determinationSystem: taxCalculationId,
          natureOfRevenue: natureId,
        });

        this.hotelItemsList = [];

        propertyList.forEach( pItem => {
          const finded = hotelList.find( hItem => hItem.propertyUId === pItem.propertyUId);

          if ( finded ) {
            this.hotelItemsList.push({
              name: finded.name,
              brand: finded.brand ? finded.brand.name : '',
            });
          }
        });
      });
  }

  public getAllHotel(): Promise<any> {
    return new Promise<any>( resolve => {
      this
        .propertyResource
        .getAll()
        .subscribe( ({ items }) => {
          resolve(items);
        });
    });
  }

  public hotelHasBeenSelected(items) {
    this.hotelRemoved = null;
    this.hotelItemsList = items;
  }

  ngOnInit() {
    this.setHeaderConfig();
    this.setButtons();
    this.setForm();
    this.setColumns();
    this.setInputMask();
    this.configureAutoComplete();

    this.title = 'title.insertTributeProductNew';

    this.route.params.subscribe(async params => {
      this.propertyId = +params.property;
      this.propertyUId = this.propertyService.getPropertyUIdfrom(this.propertyId);

      const productId =  this.route.snapshot.queryParamMap.get('productId');

      await this.getCSOSN();
      await this.getCstPartAList();
      await this.getCstPartBList();
      await this.getUf();
      await this.getPisCofins();
      await this.getDeterminationSystemList();
      await this.getNatureOfRevenueList();

      this.form.valueChanges.subscribe( () => {
        this.confirmButtonConfig.isDisabled = !this.form.valid;
      });


      if ( productId ) {
       this.form.disable();
       this.isReadonly = true;
       this.title = 'title.consultTributeProduct';
       this.setColumns();
       this.buttonInsertHotel.isDisabled = true;

       this.getTributeProductById( productId );
      }
    });
  }

  private configureAutoComplete(): void {
    // CFOP
    this.autocompleteConfigCFOP = new AutocompleteConfig();
    this.autocompleteConfigCFOP.dataModel = 'code';
    this.autocompleteConfigCFOP.resultList = [];
    this.autocompleteConfigCFOP.callbackToSearch = $event => this.getSearchCFOP($event);
    this.autocompleteConfigCFOP.callbackToGetSelectedValue = data => {
      const { id } = data;
      this.itemSelectedCFOP = id;
    };
    this.autocompleteConfigCFOP.fieldObjectToDisplay = 'template';
    this.autocompleteConfigCFOP.placeholder = this.translate.instant('placeholder.searchByDescriptionOrCFOP');

    // NBS
    this.autocompleteConfigNCM = new AutocompleteConfig();
    this.autocompleteConfigNCM.dataModel = 'code';
    this.autocompleteConfigNCM.resultList = [];
    this.autocompleteConfigNCM.callbackToSearch = $event => this.getSearchNCM($event);
    this.autocompleteConfigNCM.callbackToGetSelectedValue = data => {
      const { id } = data;
      this.itemSelectedNCM = id;
    };
    this.autocompleteConfigNCM.fieldObjectToDisplay = 'template';
    this.autocompleteConfigNCM.placeholder = this.translate.instant('placeholder.searchByCodeNBS');
  }

  public getSearchCFOP($event): Promise<any> {
    return new Promise( resolve => {
      const term: string = $event.query === undefined ? $event : $event.query;
      const regex = new RegExp(term, 'i');

      this
      .tributesResource
      .getSearchCFOP(term)
      .subscribe( ({items}) => {
        this.cfopList = items
        .filter(({code, description}) => regex.test(code.toString()) || regex.test(description))
        .map(item => {
          if ( this.isReadonly ) {
            this.itemSelectedCFOP = `${item.code} - ${item.description}`;
          }
          item['template'] = `${item.code} - ${item.description}`;
        return item;
        });
        resolve(null);
      });

    });
  }

  public getSearchNCM($event) {
    const term: string = $event.query;
    const regex = new RegExp(term, 'i');

    if (term.length > 3) {
      this
        .tributesResource
        .getSearchNCM(term)
        .subscribe( ({items}) => {
          this.ncmList = items
          .filter(({code, description }) => regex.test(code.toString()) || regex.test(description))
          .map(item => {
            item['template'] = `${item.code}`;
          return item;
          });
        }
      );
    }
  }

}
