import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientTributsWithheldListComponent } from './client-tributs-withheld-list.component';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ButtonConfig, ButtonSize, ButtonType, Type} from 'app/shared/models/button-config';
import {SharedService} from 'app/shared/services/shared/shared.service';
import {createServiceStub} from '../../../../../testing/create-service-stub';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {SuccessError} from 'app/shared/models/success-error-enum';
import {ClientResource} from 'app/shared/resources/client/client.resource';
import {Observable, of} from 'rxjs';

describe('ClientTributsWithheldListComponent', () => {
  let component: ClientTributsWithheldListComponent;
  let fixture: ComponentFixture<ClientTributsWithheldListComponent>;

  const sharedServiceStub = createServiceStub(SharedService, {
    getButtonConfig(id: string,
                    callback: Function,
                    textButton: string,
                    buttonType: ButtonType,
                    buttonSize?: ButtonSize,
                    isDisabled?: boolean,
                    type?: Type): ButtonConfig { return new ButtonConfig(); },
    getHeaders(): any { return ''; }
  });

  const toasterEmitServiceStub = createServiceStub(ToasterEmitService, {
    emitChange(isSuccess: SuccessError, message: string) {}
  });

  const clientResource = createServiceStub(ClientResource, {
    getAllWithTributeWithheld(): Observable<any> { return of([]); }
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        CommonModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ ClientTributsWithheldListComponent ],
      providers: [
        sharedServiceStub,
        toasterEmitServiceStub,
        clientResource
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientTributsWithheldListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
