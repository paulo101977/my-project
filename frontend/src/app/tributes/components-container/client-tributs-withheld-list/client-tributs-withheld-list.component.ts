import { Component, OnInit } from '@angular/core';
import {CompanyClientDto} from 'app/shared/models/dto/rate-plan/company-client-dto';
import {ButtonConfig, ButtonSize, ButtonType} from 'app/shared/models/button-config';
import {ConfigHeaderPageNew} from 'app/shared/components/header-page-new/config-header-page-new';
import {SharedService} from 'app/shared/services/shared/shared.service';
import {ModalEmitToggleService} from 'app/shared/services/shared/modal-emit-toggle.service';
import {ClientResource} from 'app/shared/resources/client/client.resource';

@Component({
  selector: 'app-client-tributs-withheld-list',
  templateUrl: './client-tributs-withheld-list.component.html',
  styleUrls: ['./client-tributs-withheld-list.component.css']
})
export class ClientTributsWithheldListComponent implements OnInit {
  public readonly MODAL_ADD_CLIENT_IN_TRIBUTES_WITHHELD = 'modalAddClientInTributesWithheld';
  public readonly MODAL_DELETE_CLIENT_IN_TRIBUTES_WITHHELD = 'modalDeleteClientInTributesWithheld';

  public configHeaderPage: ConfigHeaderPageNew;
  public clientList: Array<CompanyClientDto> = [];
  public companyClientIdToDelete: string;

  // Button
  public buttonInsertClient: ButtonConfig;

  // Table
  public columns: Array<any>;

  constructor(
    private sharedService: SharedService,
    private modalEmitToggleService: ModalEmitToggleService,
    private clientResource: ClientResource
  ) { }

  ngOnInit() {
    this.setClientList();
    this.setButtonConfig();
    this.setTableColumns();
  }

  public setClientList() {
    this.clientResource.getAllWithTributeWithheld().subscribe(response => {
      this.clientList = response;
    });
  }

  private setTableColumns() {
    this.columns = [
      {name: 'label.name', prop: 'name'},
      {name: 'label.document', prop: 'document'}
    ];
  }

  private setButtonConfig() {
    this.buttonInsertClient = this.sharedService.getButtonConfig(
      'show-modal-add-client-in-tributes-withheld',
      this.showAddClientModal,
      'action.associateClient',
      ButtonType.Secondary,
      ButtonSize.Small
    );
  }

  private showAddClientModal = () => {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_ADD_CLIENT_IN_TRIBUTES_WITHHELD);
  }

  public closeAddClientModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_ADD_CLIENT_IN_TRIBUTES_WITHHELD);
  }

  public showDeleteClientModal = (client: CompanyClientDto) => {
    this.companyClientIdToDelete = client.id;
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_DELETE_CLIENT_IN_TRIBUTES_WITHHELD);
  }

  public closeDeleteClientModal() {
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_DELETE_CLIENT_IN_TRIBUTES_WITHHELD);
  }

}
