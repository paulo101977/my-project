import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IvaAssociateComponent } from './iva-associate.component';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ImgCdnPipeStub } from '@inovacao-cmnet/thx-ui';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import {
  IVA0,
  IVA10,
  IVA15,
  IVA20,
  IVA5,
  IVA_SELECT_OPTION_LIST_DATA,
  TAX_RULE_ASSOCIATE,
  TAX_RULE_PRODUCT_CHOCOLATE,
  TAX_RULE_PRODUCT_COKE,
  TAX_RULE_PRODUCT_LIST,
  TAX_RULE_SERVICE_LIST,
  TAX_RULE_SERVICE_NO_SHOW,
  TAX_RULE_SERVICE_TELEFONIA
} from 'app/tributes/mock/iva-select-option-data';
import { ButtonType } from 'app/shared/models/button-config';
import { ivaResourceStub } from 'app/tributes/resource/testing';
import { commomServiceStub } from 'app/shared/services/shared/testing/common-service';

describe('IvaAssociateComponent', () => {
  let component: IvaAssociateComponent;
  let fixture: ComponentFixture<IvaAssociateComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateTestingModule,
        RouterTestingModule
      ],
      declarations: [
        IvaAssociateComponent,
        ImgCdnPipeStub
      ],
      providers: [
        FormBuilder,
        commomServiceStub,
        ivaResourceStub
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(IvaAssociateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should getIvaOptions', () => {
    spyOn<any>(component['ivaResource'], 'getAllIva').and.returnValue(of({items: IVA_SELECT_OPTION_LIST_DATA}));
    component['getIvaOptions']();
    expect(component.ivaList).toEqual(IVA_SELECT_OPTION_LIST_DATA);
  });

  it('should getAllTaxRulesServices', () => {
    spyOn(component['ivaResource'], 'getAllTaxRulesServices').and.returnValue(of(TAX_RULE_SERVICE_LIST));
    component['getAllTaxRulesServices']();
    expect(component.taxRuleServiceList).toEqual(TAX_RULE_SERVICE_LIST);
  });

  it('should getAllTaxRulesServicesByIvaId - has item with ID 5', () => {
    spyOn(component['ivaResource'], 'getAllTaxRulesServicesByIvaId').and.returnValue(of(TAX_RULE_SERVICE_LIST));
    component['getAllTaxRulesServicesByIvaId'](IVA5.id);
    expect(component.taxRuleServiceAssociatedList.length).toEqual(2);
    expect(component.taxRuleServiceAssociatedList).toEqual(TAX_RULE_SERVICE_LIST);
  });

  it('should getAllTaxRulesServicesByIvaId - no has item with ID 10', () => {
    spyOn(component['ivaResource'], 'getAllTaxRulesServicesByIvaId').and.returnValue(of([]));
    component['getAllTaxRulesServicesByIvaId'](IVA10.id);
    expect(component.taxRuleServiceAssociatedList.length).toEqual(0);
    expect(component.taxRuleServiceAssociatedList).toEqual([]);
  });

  it('should getAllTaxRulesProducts', () => {
    spyOn(component['ivaResource'], 'getAllTaxRulesProducts').and.returnValue(of(TAX_RULE_PRODUCT_LIST));
    component['getAllTaxRulesProducts']();
    expect(component.taxRuleProductList).toEqual(TAX_RULE_PRODUCT_LIST);
  });

  it('should getAllTaxRulesProductsByIvaId - has item with ID 15', () => {
    spyOn(component['ivaResource'], 'getAllTaxRulesProductsByIvaId').and.returnValue(of(TAX_RULE_PRODUCT_LIST));
    component['getAllTaxRulesProductsByIvaId'](IVA15.id);
    expect(component.taxRuleProductAssociatedList.length).toEqual(2);
    expect(component.taxRuleProductAssociatedList).toEqual(TAX_RULE_PRODUCT_LIST);
  });

  it('should getAllTaxRulesProductsByIvaId - no has item with ID 20', () => {
    spyOn(component['ivaResource'], 'getAllTaxRulesProductsByIvaId').and.returnValue(of([]));
    component['getAllTaxRulesProductsByIvaId'](IVA20.id);
    expect(component.taxRuleProductAssociatedList.length).toEqual(0);
    expect(component.taxRuleProductAssociatedList).toEqual([]);
  });

  it('should setForm', () => {
    component['setForm']();
    expect(Object.keys(component['form'].controls)).toEqual(['id']);
  });

  it('should setExemptIvaForm', () => {
    component['setExemptIvaForm']();
    expect(Object.keys(component['exemptIvaForm'].controls)).toEqual(['code']);
  });

  it('should setTableConfig', () => {
    spyOn<any>(component, 'setAssociateServicesTableConfig');
    component['setTableConfig']();
    expect(component.columnsService).toEqual([{name: 'label.nonAssociatedServices', prop: 'serviceName'}]);
    expect(component.columnsProduct).toEqual([{name: 'label.nonAssociatedProducts', prop: 'productName'}]);
    expect(component.columnsProductAssociated).toEqual([{name: 'label.associatedProducts', prop: 'productName'}]);
    expect(component['setAssociateServicesTableConfig']).toHaveBeenCalled();
  });

  it('should updateAliquot - Aliquot is null', () => {
    component['updateAliquot']();
    expect(component.aliquotPercentage).toBeNull();
  });

  it('should updateAliquot - Aliquot is NONE and is Exempt', () => {
    component['updateAliquot'](IVA0.id);
    expect(component.isExempt).toBeTruthy();
    expect(component.aliquotPercentage)
      .toEqual(component['translate'].instant('label.percentageRate', {rate: IVA0.tax}));
  });

  it('should updateAliquot - Aliquot is 20 and is NOT Exempt', () => {
    spyOn<any>(component, 'setAssociateServicesTableConfig');
    component['updateAliquot'](IVA20.id);
    expect(component.isExempt).toBeFalsy();
    expect(component.aliquotPercentage)
      .toEqual(component['translate'].instant('label.percentageRate', {rate: IVA20.tax}));
  });

  it('should setAssociateServicesBtnConfig', () => {
    component['setAssociateServicesBtnConfig']();
    expect(component.btnAssociateServiceConfig.id).toEqual('associate-service-button');
    expect(component.btnAssociateServiceConfig.textButton).toEqual('action.associate');
    expect(component.btnAssociateServiceConfig.buttonType).toEqual(ButtonType.Secondary);
    expect(component.btnAssociateServiceConfig.buttonSize).toBeUndefined();
    expect(component.btnAssociateServiceConfig.isDisabled).toBeTruthy();
  });

  it('should setAssociateServicesTableConfig - is Exempt', () => {
    component.isExempt = true;
    component['setAssociateServicesTableConfig']();
    expect(component.columnsServiceAssociated)
      .toEqual([
        {name: 'label.associatedServices', prop: 'serviceName'}, {name: '', prop: 'exemptionCode'}
      ]);
  });

  it('should setAssociateServicesTableConfig - NOT is Exempt', () => {
    component.isExempt = false;
    component['setAssociateServicesTableConfig']();
    expect(component.columnsServiceAssociated)
      .toEqual([
        {name: 'label.associatedServices', prop: 'serviceName'},
        {}
      ]);
  });

  it('should toggleModal', () => {
    spyOn(component['exemptIvaForm'], 'reset');
    spyOn(component['modalEmitToggleService'], 'emitToggleWithRef');
    component['toggleModal']();
    expect(component['exemptIvaForm'].reset).toHaveBeenCalled();
    expect(component['modalEmitToggleService'].emitToggleWithRef).toHaveBeenCalledWith(component.MODAL_IVA_ASSOCIATE_EXEMPT);
  });

  it('should updateIvaText - IvaId is null', () => {
    component['updateIvaText']();
    expect(component.ivaText).toEqual(component['translate'].instant('text.ivaCalculate'));
  });

  it('should updateIvaText - Aliquot is 0', () => {
    component['updateIvaText'](IVA0.id);
    expect(component.ivaText).toEqual(component['translate'].instant('text.iva0Calculate'));
  });

  it('should updateIvaText - Aliquot is 10', () => {
    component['updateIvaText'](IVA10.id);
    expect(component.ivaText).toEqual(component['translate'].instant('text.ivaCalculate'));
  });

  it('should initLists', () => {
    component['initLists']();
    expect(component.taxRuleServiceList).toEqual([]);
    expect(component.taxRuleServiceSelectedList).toEqual([]);
    expect(component.taxRuleServiceAssociatedList).toEqual([]);
    expect(component.taxRuleServiceSelectedAssociatedList).toEqual([]);
    expect(component.taxRuleProductList).toEqual([]);
    expect(component.taxRuleProductSelectedList).toEqual([]);
    expect(component.taxRuleProductAssociatedList).toEqual([]);
    expect(component.taxRuleProductSelectedAssociatedList).toEqual([]);
  });

  it('should setButtonConfig', () => {
    spyOn<any>(component, 'setAssociateServicesBtnConfig');
    spyOn<any>(component, 'checkButtonsState');
    component['setButtonConfig']();
    expect(component.btnDisassociateServiceConfig.id).toEqual('disassociate-service-button');
    expect(component.btnDisassociateServiceConfig.textButton).toEqual('action.disassociate');
    expect(component.btnDisassociateServiceConfig.buttonType).toEqual(ButtonType.Secondary);
    expect(component.btnDisassociateServiceConfig.buttonSize).toBeUndefined();
    expect(component.btnDisassociateServiceConfig.isDisabled).toBeTruthy();

    expect(component.btnAssociateProductConfig.id).toEqual('associate-product-button');
    expect(component.btnAssociateProductConfig.textButton).toEqual('action.associate');
    expect(component.btnAssociateProductConfig.buttonType).toEqual(ButtonType.Secondary);
    expect(component.btnAssociateProductConfig.buttonSize).toBeUndefined();
    expect(component.btnAssociateProductConfig.isDisabled).toBeTruthy();

    expect(component.btnDisassociateProductConfig.id).toEqual('disassociate-product-button');
    expect(component.btnDisassociateProductConfig.textButton).toEqual('action.disassociate');
    expect(component.btnDisassociateProductConfig.buttonType).toEqual(ButtonType.Secondary);
    expect(component.btnDisassociateProductConfig.buttonSize).toBeUndefined();
    expect(component.btnDisassociateProductConfig.isDisabled).toBeTruthy();

    expect(component.btnSaveConfig.id).toEqual('save-iva-associate');
    expect(component.btnSaveConfig.textButton).toEqual('action.save');
    expect(component.btnSaveConfig.buttonType).toEqual(ButtonType.Primary);
    expect(component.btnSaveConfig.buttonSize).toBeUndefined();
    expect(component.btnSaveConfig.isDisabled).toBeTruthy();

    expect(component.btnModalCancelAssociateConfig.id).toEqual('modal-cancel-iva-associate');
    expect(component.btnModalCancelAssociateConfig.textButton).toEqual('action.cancel');
    expect(component.btnModalCancelAssociateConfig.buttonType).toEqual(ButtonType.Secondary);
    expect(component.btnModalCancelAssociateConfig.buttonSize).toBeUndefined();
    expect(component.btnModalCancelAssociateConfig.isDisabled).toBeUndefined();

    expect(component.btnModalSaveAssociateConfig.id).toEqual('modal-save-iva-associate');
    expect(component.btnModalSaveAssociateConfig.textButton).toEqual('action.save');
    expect(component.btnModalSaveAssociateConfig.buttonType).toEqual(ButtonType.Primary);
    expect(component.btnModalSaveAssociateConfig.buttonSize).toBeUndefined();
    expect(component.btnModalSaveAssociateConfig.isDisabled).toBeTruthy();

    expect(component['setAssociateServicesBtnConfig']).toHaveBeenCalled();
    expect(component['checkButtonsState']).toHaveBeenCalled();
  });

  it('should serviceSelectedListChange', () => {
    spyOn(component['emitButton'], 'next');
    component['serviceSelectedListChange'](TAX_RULE_SERVICE_LIST);
    expect(component.taxRuleServiceSelectedList).toEqual(TAX_RULE_SERVICE_LIST);
    expect(component['emitButton'].next).toHaveBeenCalled();
  });

  it('should serviceSelectedAssociatedChange', () => {
    spyOn(component['emitButton'], 'next');
    component['serviceSelectedAssociatedChange'](TAX_RULE_SERVICE_LIST);
    expect(component.taxRuleServiceSelectedAssociatedList).toEqual(TAX_RULE_SERVICE_LIST);
    expect(component['emitButton'].next).toHaveBeenCalled();
  });

  it('should productSelectedListChange', () => {
    spyOn(component['emitButton'], 'next');
    component['productSelectedListChange'](TAX_RULE_PRODUCT_LIST);
    expect(component.taxRuleProductSelectedList).toEqual(TAX_RULE_PRODUCT_LIST);
    expect(component['emitButton'].next).toHaveBeenCalled();
  });

  it('should productSelectedAssociatedListChange', () => {
    spyOn(component['emitButton'], 'next');
    component['productSelectedAssociatedListChange'](TAX_RULE_PRODUCT_LIST);
    expect(component.taxRuleProductSelectedAssociatedList).toEqual(TAX_RULE_PRODUCT_LIST);
    expect(component['emitButton'].next).toHaveBeenCalled();
  });

  it('should associateService', () => {
    component.taxRuleServiceSelectedList = [TAX_RULE_SERVICE_NO_SHOW];
    component.taxRuleServiceList = [...TAX_RULE_SERVICE_LIST];
    component.taxRuleServiceAssociatedList = [];
    component['associateService']();
    expect(component.taxRuleServiceSelectedList).toEqual([]);
    expect(component.taxRuleServiceList).toEqual([TAX_RULE_SERVICE_TELEFONIA]);
    expect(component.taxRuleServiceAssociatedList).toEqual([TAX_RULE_SERVICE_NO_SHOW]);
  });

  it('should disassociateService', () => {
    component.taxRuleServiceSelectedAssociatedList = [TAX_RULE_SERVICE_TELEFONIA];
    component.taxRuleServiceAssociatedList = [...TAX_RULE_SERVICE_LIST];
    component.taxRuleServiceList = [];
    component['disassociateService']();
    expect(component.taxRuleServiceSelectedAssociatedList).toEqual([]);
    expect(component.taxRuleServiceAssociatedList).toEqual([TAX_RULE_SERVICE_NO_SHOW]);
    expect(component.taxRuleServiceList).toEqual([TAX_RULE_SERVICE_TELEFONIA]);
  });

  it('should associateProduct', () => {
    component.taxRuleProductSelectedList = [TAX_RULE_PRODUCT_COKE];
    component.taxRuleProductList = [...TAX_RULE_PRODUCT_LIST];
    component.taxRuleProductAssociatedList = [];
    component['associateProduct']();
    expect(component.taxRuleProductSelectedList).toEqual([]);
    expect(component.taxRuleProductList).toEqual([TAX_RULE_PRODUCT_CHOCOLATE]);
    expect(component.taxRuleProductAssociatedList).toEqual([TAX_RULE_PRODUCT_COKE]);
  });

  it('should disassociateProduct', () => {
    component.taxRuleProductSelectedAssociatedList = [TAX_RULE_PRODUCT_CHOCOLATE];
    component.taxRuleProductAssociatedList = [...TAX_RULE_PRODUCT_LIST];
    component.taxRuleProductList = [];
    component['disassociateProduct']();
    expect(component.taxRuleProductSelectedAssociatedList).toEqual([]);
    expect(component.taxRuleProductAssociatedList).toEqual([TAX_RULE_PRODUCT_COKE]);
    expect(component.taxRuleProductList).toEqual([TAX_RULE_PRODUCT_CHOCOLATE]);
  });

  it('should associateExemptIva', () => {
    spyOn(component['ivaService'], 'associateExemptIva').and.returnValue(TAX_RULE_SERVICE_LIST);
    spyOn<any>(component, 'associateService');
    spyOn<any>(component, 'toggleModal');
    component.exemptIvaForm.patchValue({
      code: '1234'
    });
    component['associateExemptIva']();
    expect(component.taxRuleServiceSelectedList).toEqual(TAX_RULE_SERVICE_LIST);
    expect(component['associateService']).toHaveBeenCalled();
    expect(component['toggleModal']).toHaveBeenCalled();
  });

  it('should associateExemptIva NOT', () => {
    spyOn<any>(component, 'associateService');
    spyOn<any>(component, 'toggleModal');
    component.exemptIvaForm.patchValue({
      code: ''
    });
    component['associateExemptIva']();
    expect(component['associateService']).not.toHaveBeenCalled();
    expect(component['toggleModal']).not.toHaveBeenCalled();
  });

  it('should associateIva', () => {
    spyOn(component['ivaService'], 'mapperAssociate').and.returnValue(TAX_RULE_ASSOCIATE);
    spyOn<any>(component['ivaResource'], 'associateIva').and.returnValue(of({}));
    spyOn<any>(component, 'initLists');
    spyOn<any>(component, 'getAllTaxRulesServices');
    spyOn<any>(component, 'getAllTaxRulesProducts');
    component['associateIva']();
    expect(component['ivaService'].mapperAssociate).toHaveBeenCalled();
    expect(component['ivaResource'].associateIva).toHaveBeenCalled();
    expect(component['initLists']).toHaveBeenCalled();
    expect(component.form.value.id).toEqual('');
    expect(component['getAllTaxRulesServices']).toHaveBeenCalled();
    expect(component['getAllTaxRulesProducts']).toHaveBeenCalled();
  });

  it('should associateIva - NOT', () => {
    spyOn(component['ivaService'], 'mapperAssociate').and.returnValue(null);
    spyOn<any>(component['ivaResource'], 'associateIva').and.returnValue(of({}));
    spyOn<any>(component, 'initLists');
    spyOn<any>(component, 'getAllTaxRulesServices');
    spyOn<any>(component, 'getAllTaxRulesProducts');
    component['associateIva']();
    expect(component['ivaService'].mapperAssociate).toHaveBeenCalled();
    expect(component['ivaResource'].associateIva).not.toHaveBeenCalled();
    expect(component['initLists']).not.toHaveBeenCalled();
    expect(component['getAllTaxRulesServices']).not.toHaveBeenCalled();
    expect(component['getAllTaxRulesProducts']).not.toHaveBeenCalled();
  });

});
