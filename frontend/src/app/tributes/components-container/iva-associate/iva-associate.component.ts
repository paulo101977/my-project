import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IvaSelectOption, TaxRuleProduct, TaxRuleService } from 'app/tributes/models/iva';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { TableColumn } from '@swimlane/ngx-datatable';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { associateListToOtherList } from '@inovacaocmnet/thx-bifrost';
import { IvaResource } from 'app/tributes/resource/iva.resource';
import { TranslateService } from '@ngx-translate/core';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { Subject } from 'rxjs';
import { IvaService } from 'app/tributes/services/iva.service';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';

@Component({
  selector: 'app-iva-associate',
  templateUrl: './iva-associate.component.html',
  styleUrls: ['./iva-associate.component.scss']
})
export class IvaAssociateComponent implements OnInit {

  public form: FormGroup;
  public exemptIvaForm: FormGroup;
  public ivaList: IvaSelectOption[];
  public ivaOptionList: SelectObjectOption[];
  public aliquotPercentage: string;
  public ivaText: string;
  public isExempt = false;
  public readonly MODAL_IVA_ASSOCIATE_EXEMPT: 'MODAL_IVA_ASSOCIATE_EXEMPT';

  // Table
  public columnsService: TableColumn[] = [];
  public columnsServiceAssociated: TableColumn[] = [];
  public taxRuleServiceList: TaxRuleService[];
  public taxRuleServiceSelectedList: TaxRuleService[] = [];
  public taxRuleServiceAssociatedList: TaxRuleService[] = [];
  public taxRuleServiceSelectedAssociatedList: TaxRuleService[];
  public columnsProduct: TableColumn[] = [];
  public columnsProductAssociated: TableColumn[] = [];
  public taxRuleProductList: TaxRuleProduct[];
  public taxRuleProductSelectedList: TaxRuleProduct[];
  public taxRuleProductAssociatedList: TaxRuleProduct[];
  public taxRuleProductSelectedAssociatedList: TaxRuleProduct[];

  // Button
  public btnAssociateServiceConfig: ButtonConfig;
  public btnDisassociateServiceConfig: ButtonConfig;
  public btnAssociateProductConfig: ButtonConfig;
  public btnDisassociateProductConfig: ButtonConfig;
  public btnSaveConfig: ButtonConfig;
  public btnModalCancelAssociateConfig: ButtonConfig;
  public btnModalSaveAssociateConfig: ButtonConfig;

  // Subscription
  private emitButton = new Subject<any>();
  private changeButtonEmitted$ = this.emitButton.asObservable();

  constructor(
    private fb: FormBuilder,
    private commonService: CommomService,
    private sharedService: SharedService,
    private ivaResource: IvaResource,
    private translate: TranslateService,
    private ivaService: IvaService,
    private modalEmitToggleService: ModalEmitToggleService,
  ) {
  }

  ngOnInit() {
    this.initLists();
    this.setForm();
    this.setExemptIvaForm();
    this.setButtonConfig();
    this.setTableConfig();
    this.getIvaOptions();
    this.getAllTaxRulesServices();
    this.getAllTaxRulesProducts();
  }

  private getIvaOptions() {
    this.ivaResource.getAllIva()
      .subscribe(response => {
        if (response.items) {
          this.ivaList = response.items;
          this.ivaOptionList = this.commonService
            .toOption(response.items, 'id', 'description');
        }
      });
  }

  private getAllTaxRulesServices() {
    this.ivaResource.getAllTaxRulesServices()
      .subscribe(response => {
        if (response) {
          this.taxRuleServiceList = [...response];
        }
      });
  }

  private getAllTaxRulesServicesByIvaId(ivaId: string) {
    this.ivaResource.getAllTaxRulesServicesByIvaId(ivaId)
      .subscribe(response => {
        if (response) {
          this.taxRuleServiceAssociatedList = [...response];
        }
      });
  }

  private getAllTaxRulesProducts() {
    this.ivaResource.getAllTaxRulesProducts()
      .subscribe(response => {
        if (response) {
          this.taxRuleProductList = [...response];
        }
      });
  }

  private getAllTaxRulesProductsByIvaId(ivaId: string) {
    this.ivaResource.getAllTaxRulesProductsByIvaId(ivaId)
      .subscribe(response => {
        if (response) {
          this.taxRuleProductAssociatedList = [...response];
        }
      });
  }

  private setForm() {
    this.form = this.fb.group({
      id: ['', [Validators.required]]
    });

    this.form.valueChanges
      .subscribe(({id}) => {
        this.initLists();
        this.updateAliquot();
        this.updateIvaText();
        this.getAllTaxRulesServices();
        this.getAllTaxRulesProducts();
        if (id) {
          this.updateAliquot(id);
          this.updateIvaText(id);
          this.setAssociateServicesBtnConfig();
          this.setAssociateServicesTableConfig();
          this.getAllTaxRulesServicesByIvaId(id);
          this.getAllTaxRulesProductsByIvaId(id);
        }
        this.emitButton.next(true);
      });
  }

  private setExemptIvaForm() {
    this.exemptIvaForm = this.fb.group({
      code: ['', [Validators.required, Validators.maxLength(10)]]
    });

    this.exemptIvaForm.valueChanges
      .subscribe(() => {
        this.btnModalSaveAssociateConfig.isDisabled = this.exemptIvaForm.invalid;
      });
  }

  private setTableConfig() {
    this.columnsService = [
      {name: 'label.nonAssociatedServices', prop: 'serviceName'}
    ];
    this.setAssociateServicesTableConfig();
    this.columnsProduct = [
      {name: 'label.nonAssociatedProducts', prop: 'productName'}
    ];
    this.columnsProductAssociated = [
      {name: 'label.associatedProducts', prop: 'productName'}
    ];
  }

  private updateAliquot(ivaId?: string) {
    if (!ivaId) {
      this.aliquotPercentage = null;
      return;
    }
    const aliquot = this.ivaService.getAliquotFromIva(ivaId, this.ivaList);
    this.isExempt = !aliquot;
    this.aliquotPercentage = this.translate
      .instant('label.percentageRate', {
        rate: aliquot
      });
  }

  private setAssociateServicesBtnConfig() {
    this.btnAssociateServiceConfig = this.sharedService.getButtonConfig(
      'associate-service-button',
      this.isExempt
        ? () => this.toggleModal()
        : () => this.associateService(),
      'action.associate',
      ButtonType.Secondary,
      null,
      true
    );
  }

  private setAssociateServicesTableConfig() {
    this.columnsServiceAssociated = [
      {name: 'label.associatedServices', prop: 'serviceName'},
      this.isExempt ? {name: '', prop: 'exemptionCode'} : {}
    ];
  }

  private toggleModal() {
    this.exemptIvaForm.reset();
    this.modalEmitToggleService.emitToggleWithRef(this.MODAL_IVA_ASSOCIATE_EXEMPT);
  }

  private updateIvaText(ivaId?: string) {
    if (!ivaId) {
      this.ivaText = this.translate.instant('text.ivaCalculate');
      return;
    }
    const aliquot = this.ivaService.getAliquotFromIva(ivaId, this.ivaList);
    this.ivaText = aliquot
      ? this.translate.instant('text.ivaCalculate')
      : this.translate.instant('text.iva0Calculate');
  }

  private initLists() {
    this.taxRuleServiceList = [];
    this.taxRuleServiceSelectedList = [];
    this.taxRuleServiceAssociatedList = [];
    this.taxRuleServiceSelectedAssociatedList = [];
    this.taxRuleProductList = [];
    this.taxRuleProductSelectedList = [];
    this.taxRuleProductAssociatedList = [];
    this.taxRuleProductSelectedAssociatedList = [];
  }

  public setButtonConfig() {
    this.setAssociateServicesBtnConfig();
    this.btnDisassociateServiceConfig = this.sharedService.getButtonConfig(
      'disassociate-service-button',
      () => this.disassociateService(),
      'action.disassociate',
      ButtonType.Secondary,
      null,
      true
    );
    this.btnAssociateProductConfig = this.sharedService.getButtonConfig(
      'associate-product-button',
      () => this.associateProduct(),
      'action.associate',
      ButtonType.Secondary,
      null,
      true
    );
    this.btnDisassociateProductConfig = this.sharedService.getButtonConfig(
      'disassociate-product-button',
      () => this.disassociateProduct(),
      'action.disassociate',
      ButtonType.Secondary,
      null,
      true
    );
    this.btnSaveConfig = this.sharedService.getButtonConfig(
      'save-iva-associate',
      () => this.associateIva(),
      'action.save',
      ButtonType.Primary,
      null,
      true
    );
    this.btnModalCancelAssociateConfig = this.sharedService.getButtonConfig(
      'modal-cancel-iva-associate',
      () => this.toggleModal(),
      'action.cancel',
      ButtonType.Secondary
    );
    this.btnModalSaveAssociateConfig = this.sharedService.getButtonConfig(
      'modal-save-iva-associate',
      () => this.associateExemptIva(),
      'action.save',
      ButtonType.Primary,
      null,
      true
    );

    this.checkButtonsState();
  }

  private checkButtonsState() {
    this.changeButtonEmitted$.subscribe(() => {
      this.btnAssociateServiceConfig.isDisabled = this.ivaService
        .invalidButton(this.form.value.id, this.taxRuleServiceSelectedList);
      this.btnDisassociateServiceConfig.isDisabled = this.ivaService
        .invalidButton(this.form.value.id, this.taxRuleServiceSelectedAssociatedList);
      this.btnAssociateProductConfig.isDisabled = this.ivaService
        .invalidButton(this.form.value.id, this.taxRuleProductSelectedList);
      this.btnDisassociateProductConfig.isDisabled = this.ivaService
        .invalidButton(this.form.value.id, this.taxRuleProductSelectedAssociatedList);
      this.btnSaveConfig.isDisabled = !this.form.value.id;
    });
  }

  public serviceSelectedListChange(list) {
    this.taxRuleServiceSelectedList = [...list];
    this.emitButton.next(true);
  }

  public serviceSelectedAssociatedChange(list) {
    this.taxRuleServiceSelectedAssociatedList = [...list];
    this.emitButton.next(true);
  }

  public productSelectedListChange(list) {
    this.taxRuleProductSelectedList = [...list];
    this.emitButton.next(true);
  }

  public productSelectedAssociatedListChange(list) {
    this.taxRuleProductSelectedAssociatedList = [...list];
    this.emitButton.next(true);
  }

  private associateService() {
    if (this.taxRuleServiceSelectedList) {
      const {
        selectedList,
        sourceList,
        destinationList
      } = associateListToOtherList(
        this.taxRuleServiceSelectedList,
        this.taxRuleServiceList,
        this.taxRuleServiceAssociatedList
      );
      this.taxRuleServiceSelectedList = [...selectedList];
      this.taxRuleServiceList = [...sourceList];
      this.taxRuleServiceAssociatedList = [...destinationList];
      this.emitButton.next(true);
    }
  }

  private disassociateService() {
    if (this.taxRuleServiceSelectedAssociatedList) {
      const {
        selectedList,
        sourceList,
        destinationList
      } = associateListToOtherList(
        this.taxRuleServiceSelectedAssociatedList,
        this.taxRuleServiceAssociatedList,
        this.taxRuleServiceList
      );
      this.taxRuleServiceSelectedAssociatedList = [...selectedList];
      this.taxRuleServiceAssociatedList = [...sourceList];
      this.taxRuleServiceList = [...destinationList];
      this.emitButton.next(true);
    }
  }

  private associateProduct() {
    if (this.taxRuleProductSelectedList) {
      const {
        selectedList,
        sourceList,
        destinationList
      } = associateListToOtherList(
        this.taxRuleProductSelectedList,
        this.taxRuleProductList,
        this.taxRuleProductAssociatedList
      );
      this.taxRuleProductSelectedList = [...selectedList];
      this.taxRuleProductList = [...sourceList];
      this.taxRuleProductAssociatedList = [...destinationList];
      this.emitButton.next(true);
    }
  }

  private disassociateProduct() {
    if (this.taxRuleProductSelectedAssociatedList) {
      const {
        selectedList,
        sourceList,
        destinationList
      } = associateListToOtherList(
        this.taxRuleProductSelectedAssociatedList,
        this.taxRuleProductAssociatedList,
        this.taxRuleProductList
      );
      this.taxRuleProductSelectedAssociatedList = [...selectedList];
      this.taxRuleProductAssociatedList = [...sourceList];
      this.taxRuleProductList = [...destinationList];
      this.emitButton.next(true);
    }
  }

  private associateExemptIva() {
    const {code} = this.exemptIvaForm.value;

    if (code) {
      this.taxRuleServiceSelectedList = this.ivaService
        .associateExemptIva(code, this.taxRuleServiceSelectedList);

      this.associateService();
      this.toggleModal();
    }
  }

  private associateIva() {
    const taxRuleAssociate = this.ivaService
      .mapperAssociate(this.form.value.id, this.taxRuleServiceAssociatedList, this.taxRuleProductAssociatedList);

    if (taxRuleAssociate) {
      this.ivaResource.associateIva(taxRuleAssociate)
        .subscribe(response => {
          this.translate.instant('variable.lbAssociateSuccessM', {
            labelName: this.translate.instant('label.iva')
          });
          this.initLists();
          this.form.reset({id: ''});
          this.getAllTaxRulesServices();
          this.getAllTaxRulesProducts();
        });
    }
  }

}
