import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { AuthService } from 'app/shared/services/auth-service/auth.service';
import { ActivatedRouteStub, createServiceStub } from '../../../../../testing';

import { ServiceTributesListComponent } from './service-tributes-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { of } from 'rxjs';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';
import { ActivatedRoute } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { tributesResourceStub } from 'app/tributes/resource/testing';

const serviceList = {
  items: [
    {
      id: '1',
      name: 'first',
      ncmCode: '1234',
      barCode: '9876',
      uf: 'RJ',
      isActive: false,
      cfopId: '',
      csosnId: '',
      cstAid: '',
      cstBid: '',
      cstPisCofinsId: '',
      natureId: '',
      taxCalculationId: '',
      ncmId: '',
      serviceTypeId: '',
      ufId: '',
      taxPercentual: 0,
      taxPercentualSub: 0,
      taxPercentualBase: 0,
      type: 0,
      cfop: '',
      csosn: '',
      cstA: '',
      cstB: '',
      cstPisCofins: '',
      nature: '',
      taxCalculaation: '',
      serviceType: '',
      nbsCode: '1234',
      ufInitials: '',
      propertyId: '',
      tenantId: '',
      propertyList: [],
      nbsId: ''
    },
    {
      id: '2',
      name: 'second',
      ncmCode: '5678',
      barCode: '5432',
      uf: 'SP',
      isActive: true,
      cfopId: '',
      csosnId: '',
      cstAid: '',
      cstBid: '',
      cstPisCofinsId: '',
      natureId: '',
      taxCalculationId: '',
      ncmId: '',
      serviceTypeId: '',
      ufId: '',
      taxPercentual: 0,
      taxPercentualSub: 0,
      taxPercentualBase: 0,
      type: 0,
      cfop: '',
      csosn: '',
      cstA: '',
      cstB: '',
      cstPisCofins: '',
      nature: '',
      taxCalculaation: '',
      serviceType: '',
      nbsCode: '',
      ufInitials: '',
      propertyId: '',
      tenantId: '',
      propertyList: [],
      nbsId: ''
    }
  ]
};

const makeEvent = (toSearch: string) => {
  return {
    target: {
      value: toSearch
    }
  };
};

const AuthProvider = createServiceStub(AuthService, {
  getPropertyCountryCode(): any { return 'br'; }
});

describe('ServiceTributesListComponent', () => {
  let component: ServiceTributesListComponent;
  let fixture: ComponentFixture<ServiceTributesListComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceTributesListComponent ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      providers: [
        AuthProvider,
        toasterEmitServiceStub,
        tributesResourceStub,
        {provide: ActivatedRoute, useClass: ActivatedRouteStub},
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    fixture = TestBed.createComponent(ServiceTributesListComponent);
    component = fixture.componentInstance;

    spyOn<any>(component['router'], 'navigate').and.callFake( () => {} );

    spyOn(component['tributesResource'], 'toggleTributeById')
        .and
        .returnValue(of(null));

    spyOn<any>(component, 'loadData')
        .and
        .returnValue(of(serviceList));

    spyOn(component['toasterEmitService'], 'emitChange')
        .and
        .callThrough();

    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', () => {
    spyOn<any>(component, 'setHeaderConfig');
    spyOn(component, 'setColumns');

    component.ngOnInit();

    expect(component['setHeaderConfig']).toHaveBeenCalled();
    expect(component['setColumns']).toHaveBeenCalled();
    expect(component['loadData']).toHaveBeenCalled();
  });


  it('should test search', () => {
    spyOn(component, 'search').and.callThrough();

    const { items } = serviceList;
    component.pageItemsList = items;
    component.pageItemsListBack = items;

    component.search(makeEvent('first'));

    expect(component.pageItemsList.length).toEqual(1);

    component.search(makeEvent(''));

    expect(component.pageItemsList.length).toEqual(2);

    component.search(makeEvent('SP'));

    expect(component.pageItemsList.length).toEqual(0);

    component.search(makeEvent('23'));

    expect(component.pageItemsList.length).toEqual(1);
  });


  it('should call toasterEmitService activated', fakeAsync(() => {
    component.updateStatus({isActive: true});

    tick();

    expect(component['toasterEmitService'].emitChange)
      .toHaveBeenCalledWith(SuccessError.success, 'variable.isActiveM');
  }));

  it('should call toasterEmitService disabled', fakeAsync(() => {
    component.updateStatus({isActive: false});

    tick();

    expect(component['toasterEmitService'].emitChange)
      .toHaveBeenCalledWith(SuccessError.success, 'variable.isInactiveM');
  }));
});
