import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { ProductTributesListComponent } from './product-tributes-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { TributesOfProducts } from 'app/tributes/models/tributes';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { tributesResourceStub } from 'app/tributes/resource/testing';
import { toasterEmitServiceStub } from 'app/shared/services/shared/testing/toaster-emitter-service';

const productsList = {
  items: [
    {
      id: '1',
      name: 'first',
      ncmCode: '1234',
      barCode: '9876',
      uf: 'RJ',
      isActive: false,
      cfopId: '',
      csosnId: '',
      cstAid: '',
      cstBid: '',
      cstPisCofinsId: '',
      natureId: '',
      taxCalculationId: '',
      ncmId: '',
      serviceTypeId: '',
      ufId: '',
      taxPercentual: 0,
      taxPercentualSub: 0,
      taxPercentualBase: 0,
      type: 0,
      cfop: '',
      csosn: '',
      cstA: '',
      cstB: '',
      cstPisCofins: '',
      nature: '',
      taxCalculaation: '',
      serviceType: '',
      ufInitials: '',
      propertyId: '',
      tenantId: '',
      propertyList: []
    },
    {
      id: '2',
      name: 'second',
      ncmCode: '5678',
      barCode: '5432',
      uf: 'SP',
      isActive: true,
      cfopId: '',
      csosnId: '',
      cstAid: '',
      cstBid: '',
      cstPisCofinsId: '',
      natureId: '',
      taxCalculationId: '',
      ncmId: '',
      serviceTypeId: '',
      ufId: '',
      taxPercentual: 0,
      taxPercentualSub: 0,
      taxPercentualBase: 0,
      type: 0,
      cfop: '',
      csosn: '',
      cstA: '',
      cstB: '',
      cstPisCofins: '',
      nature: '',
      taxCalculaation: '',
      serviceType: '',
      ufInitials: '',
      propertyId: '',
      tenantId: '',
      propertyList: []
    }
  ]
};

const makeEvent = (toSearch: string) => {
  return {
    target: {
      value: toSearch
    }
  };
};

describe('ProductTributesListComponent', () => {
  let component: ProductTributesListComponent;
  let fixture: ComponentFixture<ProductTributesListComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTributesListComponent ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      providers: [
        tributesResourceStub,
        toasterEmitServiceStub,
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    fixture = TestBed.createComponent(ProductTributesListComponent);
    component = fixture.componentInstance;

    spyOn<any>(component, 'loadData')
        .and
        .returnValue(of(productsList));

    spyOn(component['tributesResource'], 'toggleTributeById')
        .and
        .returnValue(of(null));

    spyOn(component['toasterEmitService'], 'emitChange')
        .and
        .callThrough();

    spyOn<any>(component['router'], 'navigate').and.callFake( () => { });
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', () => {
    spyOn<any>(component, 'setHeaderConfig');
    spyOn(component, 'setColumns');

    component.ngOnInit();

    expect(component['setHeaderConfig']).toHaveBeenCalled();
    expect(component['setColumns']).toHaveBeenCalled();
    expect(component['loadData']).toHaveBeenCalled();
  });


  it('should test search', () => {
    spyOn(component, 'search').and.callThrough();

    const { items } = productsList;
    component.pageItemsList = <Array<TributesOfProducts>>items;
    component.pageItemsListBack = <Array<TributesOfProducts>>items;

    component.search(makeEvent('first'));

    expect(component.pageItemsList.length).toEqual(1);

    component.search(makeEvent(''));

    expect(component.pageItemsList.length).toEqual(2);

    component.search(makeEvent('SP'));

    expect(component.pageItemsList.length).toEqual(0);

    component.search(makeEvent('23'));

    expect(component.pageItemsList.length).toEqual(1);
  });


  it('should call toasterEmitService activated', fakeAsync(() => {
    component.updateStatus({isActive: true});

    tick();

    expect(component['toasterEmitService'].emitChange)
      .toHaveBeenCalledWith(SuccessError.success, 'variable.isActiveM');
  }));

  it('should call toasterEmitService disabled', fakeAsync(() => {
    component.updateStatus({isActive: false});

    tick();

    expect(component['toasterEmitService'].emitChange)
      .toHaveBeenCalledWith(SuccessError.success, 'variable.isInactiveM');
  }));
});
