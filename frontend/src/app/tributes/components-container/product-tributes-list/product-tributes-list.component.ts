import { Component, OnInit } from '@angular/core';
import { ConfigHeaderPageNew } from 'app/shared/components/header-page-new/config-header-page-new';
import { TributesResource } from 'app/tributes/resource/tributes.resource';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { TributesOfProducts } from 'app/tributes/models/tributes';

@Component({
  selector: 'app-product-tributes-list',
  templateUrl: './product-tributes-list.component.html',
  styleUrls: ['./product-tributes-list.component.css']
})
export class ProductTributesListComponent implements OnInit {

  // header
  public configHeaderPage: ConfigHeaderPageNew;

  // table
  public columns: Array<any>;
  public itemsOption: Array<any>;

  // create model
  public pageItemsList: Array<TributesOfProducts>;
  public pageItemsListBack: Array<TributesOfProducts>;
  public country: string;

  constructor(
    private tributesResource: TributesResource,
    private router: Router,
    private route: ActivatedRoute,
    private toasterEmitService: ToasterEmitService,
  ) { }

  public setColumns() {
    this.columns = [
      {
        name: 'label.description',
        prop: 'name'
      },
      {
        name: 'label.ncm',
        prop: 'ncmCode'
      },
      {
        name: 'label.barCode',
        prop: 'barCode'
      },
      {
        name: 'label.uf',
        prop: 'ufInitials',
        flexGrow: 0.2
      }];
  }

  public goToAdd = () => {
    this.navigateTo();
  }

  public navigateTo(productId?) {
    this.router.navigate(['add-view'], { relativeTo: this.route, queryParams: { productId } });
  }

  public updateStatus(row) {
    this
      .tributesResource
      .toggleTributeById(row.id)
      .subscribe( () => {
        let message = 'variable.isInactiveM';

        if ( row.isActive ) {
          message = 'variable.isActiveM';
        }

        this
          .toasterEmitService
          .emitChange(SuccessError.success, message);

        this.loadData();
      });
  }

  public rowItemClicked(row) {
    const { id } = row;

    this.navigateTo(id);
  }


  public search(event) {
    const { value } = event.target;

    if ( typeof value === 'string' && value.length > 0) {
      const regexExp = new RegExp(value, 'i');
      this.pageItemsList = this.pageItemsListBack.filter( item => {
        return regexExp.test( item.name) ||
          regexExp.test( item.ncmCode) ||
          regexExp.test( item.barCode);
      });
    } else {
      this.pageItemsList = this.pageItemsListBack;
    }
  }


  public loadData() {
    this
      .tributesResource
      .getProductTributesList()
      .subscribe( ({items}) => {
        this.pageItemsList = items;
        this.pageItemsListBack = this.pageItemsList;
    });
  }


  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: false,
      keepTitleButton: true,
      callBackFunction: this.goToAdd
    };
  }

  ngOnInit() {
    this.setHeaderConfig();

    this.setColumns();

    this.loadData();
  }
}
