import { Injectable } from '@angular/core';
import {
  AssociateProduct,
  AssociateService,
  IvaSelectOption,
  TaxRuleAssociate,
  TaxRuleService
} from 'app/tributes/models/iva';

@Injectable({
  providedIn: 'root'
})
export class IvaService {

  constructor() {
  }

  public getAliquotFromIva(id, ivaList: IvaSelectOption[]): number {
    let tax = 0;
    if (id && ivaList) {
      const iva = ivaList.find(ivaItem => ivaItem.id == id);
      if (iva) {
        tax = +iva.tax || 0;
      }
    }
    return tax;
  }

  public invalidButton(ivaId: string, list: any[]): boolean {
    return !ivaId || (list && !list.length);
  }

  public mapperAssociate(ivaId: string, serviceList, productList): TaxRuleAssociate {
    if (ivaId) {
      let serviceAssociatedList: AssociateService[] = [];
      let productAssociatedList: AssociateProduct[] = [];

      if (serviceList) {
        serviceAssociatedList = serviceList;
      }
      if (productList) {
        productAssociatedList = productList;
      }

      return <TaxRuleAssociate>{
        ivaId: ivaId,
        serviceList: serviceAssociatedList,
        productList: productAssociatedList
      };
    }
    return null;
  }

  public associateExemptIva(code: string, list: TaxRuleService[]): TaxRuleService[] {
    if (code && list) {
      return list.map(service => {
        service.exemptionCode = code;
        return service;
      });
    }
    return [];
  }
}
