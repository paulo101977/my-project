import { TestBed } from '@angular/core/testing';

import { IvaService } from './iva.service';
import { configureTestSuite } from 'ng-bullet';
import {
  IVA15,
  IVA_SELECT_OPTION_LIST_DATA,
  TAX_RULE_PRODUCT_LIST,
  TAX_RULE_SERVICE_LIST
} from 'app/tributes/mock/iva-select-option-data';
import { TaxRuleAssociate } from 'app/tributes/models/iva';

describe('IvaService', () => {
  let service: IvaService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({});

    service = TestBed.get(IvaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should getAliquotFromIva ID 5', () => {
    const aliquot = service.getAliquotFromIva('5', IVA_SELECT_OPTION_LIST_DATA);
    expect(aliquot).toEqual(5);
  });

  it('should getAliquotFromIva ID 20', () => {
    const aliquot = service.getAliquotFromIva('20', IVA_SELECT_OPTION_LIST_DATA);
    expect(aliquot).toEqual(20);
  });

  it('should invalidButton - INVALID without ivaID', () => {
    const invalidButton = service.invalidButton(null, IVA_SELECT_OPTION_LIST_DATA);
    expect(invalidButton).toBeTruthy();
  });

  it('should invalidButton - INVALID without ivaList', () => {
    const invalidButton = service.invalidButton('5', []);
    expect(invalidButton).toBeTruthy();
  });

  it('should invalidButton - VALID', () => {
    const invalidButton = service.invalidButton('5', IVA_SELECT_OPTION_LIST_DATA);
    expect(invalidButton).toBeFalsy();
  });

  it('should mapperAssociate', () => {
    const taxRuleAssociate: TaxRuleAssociate = service.mapperAssociate(IVA15.id, TAX_RULE_SERVICE_LIST, TAX_RULE_PRODUCT_LIST);
    expect(taxRuleAssociate.ivaId).toEqual(IVA15.id);
    expect(taxRuleAssociate.serviceList).toEqual(TAX_RULE_SERVICE_LIST);
    expect(taxRuleAssociate.productList).toEqual(TAX_RULE_PRODUCT_LIST);
  });

  it('should associateExemptIva to List', () => {
    const newList = service.associateExemptIva('TESTE CODE IN LIST', TAX_RULE_SERVICE_LIST);
    newList.forEach(item => {
      expect(item.exemptionCode).toEqual('TESTE CODE IN LIST');
      expect(item.exemptionCode).toEqual('TESTE CODE IN LIST');
    });
  });

});
