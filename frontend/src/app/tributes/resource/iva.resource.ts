import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { IvaSelectOption, TaxRuleAssociate, TaxRuleProduct, TaxRuleService } from 'app/tributes/models/iva';
import { TaxRuleApiService } from '@inovacaocmnet/thx-bifrost';
import { HttpParams } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class IvaResource {

  private readonly url = 'pt';

  constructor(
    private taxRuleApi: TaxRuleApiService
  ) {
  }

  public getAllIva(): Observable<ItemsResponse<IvaSelectOption>> {
    return this
      .taxRuleApi
      .get<ItemsResponse<IvaSelectOption>>(`${this.url}/iva`);
  }

  public getAllTaxRulesServices(): Observable<TaxRuleService[]> {
    return this
      .taxRuleApi
      .get<TaxRuleService[]>(`${this.url}/getalltaxrulesservices`);
  }

  public getAllTaxRulesServicesByIvaId(ivaId: string): Observable<TaxRuleService[]> {
    let params = new HttpParams();
    if (ivaId) {
      params = params.set('IvaId', ivaId);
    }

    return this
      .taxRuleApi
      .get<TaxRuleService[]>(`${this.url}/getalltaxrulesservices`, {params: params});
  }

  public getAllTaxRulesProducts(): Observable<TaxRuleProduct[]> {
    return this
      .taxRuleApi
      .get<TaxRuleProduct[]>(`${this.url}/getalltaxrulesproducts`);
  }

  public getAllTaxRulesProductsByIvaId(ivaId: string): Observable<TaxRuleProduct[]> {
    let params = new HttpParams();
    if (ivaId) {
      params = params.set('IvaId', ivaId);
    }

    return this
      .taxRuleApi
      .get<TaxRuleProduct[]>(`${this.url}/getalltaxrulesproducts`, {params: params});
  }

  public associateIva(taxRuleAssociate: TaxRuleAssociate): Observable<TaxRuleProduct> {
    return this
      .taxRuleApi
      .put<TaxRuleProduct>(`${this.url}/associateservicesandiva`, taxRuleAssociate);
  }
}
