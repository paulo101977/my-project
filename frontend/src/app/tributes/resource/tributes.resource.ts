import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TributesApiService } from '@inovacaocmnet/thx-bifrost';
import { AuthService } from 'app/shared/services/auth-service/auth.service';
import { Observable, of } from 'rxjs';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import {
  CFOP,
  CSOSN,
  CST,
  DeterminationSystem,
  NatureOfRevenue,
  NBS,
  NCM,
  ServiceType,
  TributesOfProducts,
  TributesOfServices,
  UF,
  Tributes,
  TributeToSender
} from '../models/tributes';
import {TributeWithheld} from 'app/tributes/models/tributes';
import {CompanyClientDto} from 'app/shared/models/dto/rate-plan/company-client-dto';

@Injectable({ providedIn: 'root' })
export class TributesResource {

  constructor(
    private tributesApi: TributesApiService,
    private authService: AuthService
  ) {}


  public getProductTributesList(): Observable<ItemsResponse<TributesOfProducts>> {
    return this
      .tributesApi
      .get<ItemsResponse<TributesOfProducts>>(`taxrule/products`);
  }


  public insertNewTribute(item: TributeToSender): Observable<any> {
    return this.tributesApi.post(`taxrule`, item);
  }


  public getServiceTributesList(): Observable<ItemsResponse<TributesOfServices>> {
    return this
      .tributesApi
      .get<ItemsResponse<TributesOfServices>>(`taxrule/services`);
  }


  public toggleTributeById(id: string): Observable<any> {
    return this.tributesApi.patch(`taxrule/${id}/toggleactivation`);
  }


  public getServiceTypeList(): Observable<ItemsResponse<ServiceType>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<ServiceType>>(`${countryCode}/tiposervico`);
  }


  public getTributeById(propertyId): Observable<Tributes> {
    return this
      .tributesApi
      .get<Tributes>(`taxrule/${propertyId}`);
  }

  public getCFOP(): Observable<ItemsResponse<CFOP>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<CFOP>>(`${countryCode}/cfop`);
  }

  public getSearchCFOP(param: string): Observable<ItemsResponse<CFOP>> {
    const params = new HttpParams()
      .set('Param', param);
    const countryCode = this.getCountryCode();

    return this
      .tributesApi
      .get<ItemsResponse<CFOP>>(`${countryCode}/cfop/search`, { params });
  }

  public getNCM(): Observable<ItemsResponse<NCM>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<NCM>>(`${countryCode}/ncm`);
  }

  public getSearchNCM(param: string): Observable<ItemsResponse<NCM>> {
    const params = new HttpParams()
      .set('Param', param);
    const countryCode = this.getCountryCode();

    return this
      .tributesApi
      .get<ItemsResponse<NCM>>(`${countryCode}/ncm/search`, { params });
  }

  public getNBS(): Observable<ItemsResponse<NBS>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<NBS>>(`${countryCode}/nbs`);
  }

  public getSearchNBS(param: string): Observable<ItemsResponse<NBS>> {
    const params = new HttpParams()
      .set('Param', param);
    const countryCode = this.getCountryCode();

    return this
      .tributesApi
      .get<ItemsResponse<CFOP>>(`${countryCode}/nbs/search`, { params });
  }

  public getCSOSN(): Observable<ItemsResponse<CSOSN>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<CSOSN>>(`${countryCode}/csosn`);
  }


  public getCstPartAList(): Observable<ItemsResponse<CST>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<CST>>(`${countryCode}/cst/getallbytype/0`);
  }


  public getCstPartBList(): Observable<ItemsResponse<CST>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<CST>>(`${countryCode}/cst/getallbytype/1`);
  }


  public getUf(): Observable<ItemsResponse<UF>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<UF>>(`${countryCode}/uf`);
  }


  public getPisCofins(): Observable<ItemsResponse<CST>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<CST>>(`${countryCode}/cst/getallbytype/2`);
  }


  public getDeterminationSystemList(): Observable<ItemsResponse<DeterminationSystem>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<DeterminationSystem>>(`${countryCode}/regimeapuracao`);
  }


  public getNatureOfRevenueList(): Observable<ItemsResponse<NatureOfRevenue>> {
    const countryCode = this.getCountryCode();
    return this
      .tributesApi
      .get<ItemsResponse<NatureOfRevenue>>(`${countryCode}/naturezareceita`);
  }

  public postTributeWithheldInClientList(itemList: Array<CompanyClientDto>): Observable<ItemsResponse<TributeWithheld>> {
    const countryCode = this.getCountryCode();
    return this.tributesApi.post(`${countryCode}/createimpostoretidolist`, itemList);
  }

  private getCountryCode() {
    return this.authService.getPropertyCountryCode();
  }
}
