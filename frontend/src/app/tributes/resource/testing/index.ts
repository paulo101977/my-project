import { createServiceStub } from '../../../../../testing';
import { TributesResource } from 'app/tributes/resource/tributes.resource';
import { Observable, of } from 'rxjs';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import {
    CFOP,
    CSOSN,
    CST,
    DeterminationSystem,
    NatureOfRevenue,
    NBS,
    NCM,
    ServiceType,
    Tributes,
    TributesOfProducts,
    TributesOfServices,
    TributeToSender,
    TributeWithheld,
    UF
} from 'app/tributes/models/tributes';
import { CompanyClientDto } from 'app/shared/models/dto/rate-plan/company-client-dto';
import { IvaResource } from 'app/tributes/resource/iva.resource';
import { IvaSelectOption, TaxRuleAssociate, TaxRuleProduct, TaxRuleService } from 'app/tributes/models/iva';

export const tributesResourceStub = createServiceStub(TributesResource, {
    getCFOP(): Observable<ItemsResponse<CFOP>> {
        return of( null );
    },

    getCSOSN(): Observable<ItemsResponse<CSOSN>> {
        return of( null );
    },

    getCstPartAList(): Observable<ItemsResponse<CST>> {
        return of( null );
    },

    getCstPartBList(): Observable<ItemsResponse<CST>> {
        return of( null );
    },

    getDeterminationSystemList(): Observable<ItemsResponse<DeterminationSystem>> {
        return of( null );
    },

    getNatureOfRevenueList(): Observable<ItemsResponse<NatureOfRevenue>> {
        return of( null );
    },

    getNBS(): Observable<ItemsResponse<NBS>> {
        return of( null );
    },

    getNCM(): Observable<ItemsResponse<NCM>> {
        return of( null );
    },

    getPisCofins(): Observable<ItemsResponse<CST>> {
        return of( null );
    },

    getProductTributesList(): Observable<ItemsResponse<TributesOfProducts>> {
        return of( null );
    },

    getSearchCFOP(param: string): Observable<ItemsResponse<CFOP>> {
        return of( null );
    },

    getSearchNBS(param: string): Observable<ItemsResponse<NBS>> {
        return of( null );
    },

    getSearchNCM(param: string): Observable<ItemsResponse<NCM>> {
        return of( null );
    },

    getServiceTributesList(): Observable<ItemsResponse<TributesOfServices>> {
        return of( null );
    },

    getServiceTypeList(): Observable<ItemsResponse<ServiceType>> {
        return of( null );
    },

    getTributeById(propertyId): Observable<Tributes> {
        return of( null );
    },

    getUf(): Observable<ItemsResponse<UF>> {
        return of( null );
    },

    insertNewTribute(item: TributeToSender): Observable<any> {
        return of( null );
    },

    postTributeWithheldInClientList(itemList: Array<CompanyClientDto>): Observable<ItemsResponse<TributeWithheld>> {
        return of( null );
    },

    toggleTributeById(id: string): Observable<any> {
        return of( null );
    },

});

export const ivaResourceStub = createServiceStub(IvaResource, {
  getAllTaxRulesServicesByIvaId(ivaId: string): Observable<TaxRuleService[]> {
    return of(null);
  },
  getAllIva(): Observable<ItemsResponse<IvaSelectOption>> {
    return of(null);
  },
  associateIva(taxRuleAssociate: TaxRuleAssociate): Observable<TaxRuleProduct> {
    return of(null);
  },
  getAllTaxRulesProducts(): Observable<TaxRuleProduct[]> {
    return of(null);
  },
  getAllTaxRulesProductsByIvaId(ivaId: string): Observable<TaxRuleProduct[]> {
    return of(null);
  },
  getAllTaxRulesServices(): Observable<TaxRuleService[]> {
    return of(null);
  }
});
