export class Tributes {
  id: string;
  propertyList: Array<any>;
  name: string;
  barCode: string;
  cfopId: string;
  cfopCode: string;
  csosnId: string;
  cstAid: string;
  cstBid: string;
  cstPisCofinsId: string;
  natureId: string;
  taxCalculationId: string;
  nbsId: string;
  ncmId: string;
  serviceTypeId: string;
  ufId: string;
  taxPercentual: number;
  taxPercentualSub: number;
  taxPercentualBase: number;
  type: number;
  cfop: string;
  csosn: string;
  cstA: string;
  cstB: string;
  cstPisCofins: string;
  nature: string;
  taxCalculation: string;
  serviceType: string;
  nbsCode: string;
  ncmCode: string;
  ufInitials: string;
  isActive: boolean;
  propertyId: number;
  tenantId: string;
}

export class TributesOfProducts {
  id: string;
  name: string;
  barCode: string;
  cfopId: string;
  csosnId: string;
  cstAid: string;
  cstBid: string;
  cstPisCofinsId: string;
  natureId: string;
  taxCalculationId: string;
  ncmId: string;
  serviceTypeId: string;
  ufId: string;
  taxPercentual: number;
  taxPercentualSub: number;
  taxPercentualBase: number;
  type: number;
  cfop: string;
  csosn: string;
  cstA: string;
  cstB: string;
  cstPisCofins: string;
  nature: string;
  taxCalculaation: string;
  serviceType: string;
  ncmCode: string;
  ufInitials: string;
  isActive: boolean;
  propertyId: string;
  tenantId: string;
  propertyList: Array<any>;
}

export class TributesOfServices extends TributesOfProducts {
  nbsId: string;
  nbsCode: string;
}

export class TributeToSender {
  propertyList: Array<HotelSender>;
  name: string;
  barCode?: string;
  cfopId: string;
  csosnId?: string;
  csosn?: string;
  cstAid?: string;
  cstBid?: string;
  cstPisCofinsId: string;
  natureId: string;
  taxCalculationId: string;
  ncmId?: string;
  type: number;
  ufId?: string;
  taxPercentual: number;
  taxPercentualSub?: number;
  isActive: boolean;
  propertyUId: string;
}

export class HotelSender {
  propertyUId: number;
  tenantId: string;
}

export class TributesBase {
  id: string;
  code: number;
  description: string;
  isActive: boolean;
}

export class NCM extends TributesBase {}

export class UF {
  id: string;
  ibgeCode: string;
  initials: string;
}

export class NBS extends TributesBase {}

export class CFOP extends TributesBase {}

export class CSOSN  extends TributesBase {}

export class CST extends TributesBase {
  type: number;
}

export class NatureOfRevenue extends TributesBase {
  ncmId: string;
}

export class  DeterminationSystem {
  id: string;
  description: string;
}

export class  ServiceType {
  id: string;
  code: number;
  description: string;
}

export class TributeWithheld {
  id: string;
  ownerId: string;
  propertyUid: string;
}

