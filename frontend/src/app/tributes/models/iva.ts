export class IvaSelectOption {
  id: string;
  description: string;
  location: string;
  tax: number;
}

export class TaxRuleService {
  serviceId: number;
  serviceName: string;
  exemptionCode: string;
  ivaId: string;
}

export class TaxRuleProduct {
  productId: string;
  productName: string;
  ivaId: string;
}

export class TaxRuleAssociate {
  ivaId: string;
  serviceList: AssociateService[];
  productList: AssociateProduct[];
}

export class AssociateService {
  serviceId: number;
  exemptionCode: string;
}

export class AssociateProduct {
  productId: string;
}
