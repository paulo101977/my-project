import { IvaSelectOption, TaxRuleAssociate, TaxRuleProduct, TaxRuleService } from 'app/tributes/models/iva';

export const IVA0: IvaSelectOption = {
  id: '0',
  description: 'IVA 0%',
  location: '',
  tax: 0,
};

export const IVA5: IvaSelectOption = {
  id: '5',
  description: 'IVA 5%',
  location: '',
  tax: 5,
};

export const IVA10: IvaSelectOption = {
  id: '10',
  description: 'IVA 10%',
  location: '',
  tax: 10,
};

export const IVA15: IvaSelectOption = {
  id: '15',
  description: 'IVA 15%',
  location: '',
  tax: 15,
};

export const IVA20: IvaSelectOption = {
  id: '20',
  description: 'IVA 20%',
  location: '',
  tax: 20,
};

export const IVA_SELECT_OPTION_LIST_DATA: IvaSelectOption[] = [IVA5, IVA10, IVA15, IVA20];

export const TAX_RULE_SERVICE_NO_SHOW: TaxRuleService = {
  serviceId: 100,
  serviceName: 'No-Show',
  exemptionCode: 'CODE No-Show',
  ivaId: '5'
};

export const TAX_RULE_SERVICE_TELEFONIA: TaxRuleService = {
  serviceId: 200,
  serviceName: 'Telefonia',
  exemptionCode: 'CODE TELEFONIA',
  ivaId: '5'
};

export const TAX_RULE_SERVICE_LIST: TaxRuleService[] = [TAX_RULE_SERVICE_NO_SHOW, TAX_RULE_SERVICE_TELEFONIA];

export const TAX_RULE_PRODUCT_COKE: TaxRuleProduct = {
  productId: '300',
  productName: 'COKE',
  ivaId: '15'
};

export const TAX_RULE_PRODUCT_CHOCOLATE: TaxRuleProduct = {
  productId: '400',
  productName: 'CHOCOLATE',
  ivaId: '15'
};

export const TAX_RULE_PRODUCT_LIST: TaxRuleProduct[] = [TAX_RULE_PRODUCT_COKE, TAX_RULE_PRODUCT_CHOCOLATE];

export const TAX_RULE_ASSOCIATE: TaxRuleAssociate = {
  ivaId: '5',
  serviceList: [],
  productList: []
};
