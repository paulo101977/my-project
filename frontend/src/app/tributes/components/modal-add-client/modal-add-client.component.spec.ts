import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddClientComponent } from './modal-add-client.component';
import { SharedService } from '@app/shared/services/shared/shared.service';
import { ButtonType, ButtonSize, Type } from 'projects/ui/src/public_api';
import { ButtonConfig } from '@app/shared/models/button-config';
import {Observable, of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {createServiceStub} from '../../../../../testing/create-service-stub';
import { ClientResource } from '@app/shared/resources/client/client.resource';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

describe('ModalAddClientComponent', () => {
  let component: ModalAddClientComponent;
  let fixture: ComponentFixture<ModalAddClientComponent>;

  const sharedServiceStub = createServiceStub(SharedService, {
    getButtonConfig(id: string,
                    callback: Function,
                    textButton: string,
                    buttonType: ButtonType,
                    buttonSize?: ButtonSize,
                    isDisabled?: boolean,
                    type?: Type): ButtonConfig { return new ButtonConfig(); },
    getHeaders(): any { return ''; }
  });

  const clientResource = createServiceStub(ClientResource, {
    getAllCompanyClientResultDto(): Observable<any> { return of([]); }
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        CommonModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ ModalAddClientComponent ],
      providers: [
        sharedServiceStub,
        clientResource
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
