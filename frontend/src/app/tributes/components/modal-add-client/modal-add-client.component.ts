import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CompanyClientDto} from 'app/shared/models/dto/rate-plan/company-client-dto';
import {ButtonConfig, ButtonType} from 'app/shared/models/button-config';
import {SharedService} from 'app/shared/services/shared/shared.service';
import {ConfigHeaderPageNew} from 'app/shared/components/header-page-new/config-header-page-new';
import {SuccessError} from 'app/shared/models/success-error-enum';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {ClientResource} from 'app/shared/resources/client/client.resource';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-modal-add-client',
  templateUrl: './modal-add-client.component.html',
  styleUrls: ['./modal-add-client.component.css']
})
export class ModalAddClientComponent implements OnInit {
  @Input() modalId: string;

  @Output() closeModal = new EventEmitter();
  @Output() addClient = new EventEmitter();

  public configHeaderPage: ConfigHeaderPageNew;
  public clientList: Array<CompanyClientDto> = [];
  public clientSelectedList: Array<CompanyClientDto>;
  private propertyId: number;

  // Table
  public columns: Array<any>;
  public buttonCancelConfig: ButtonConfig;
  public buttonConfirmConfig: ButtonConfig;

  constructor(
    private sharedService: SharedService,
    private toasterEmitService: ToasterEmitService,
    private route: ActivatedRoute,
    private clientResource: ClientResource
  ) { }

  ngOnInit() {
    this.propertyId = this.route.snapshot.params.property;

    this.setButtonConfig();
    this.setTableColumns();
    this.setClientList();
  }

  private setClientList() {
    this.clientSelectedList = [];

    this.clientResource.getAllCompanyClientResultDto(this.propertyId).subscribe(response => {
      this.clientList = response.items;
    });
  }

  private setTableColumns() {
    this.columns = [
      {name: 'label.name', prop: 'name'},
      {name: 'label.document', prop: 'document'}
    ];
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'modal-add-client-cancel',
      this.clickCloseModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmConfig = this.sharedService.getButtonConfig(
      'issue-credit-note',
      this.save,
      'action.confirm',
      ButtonType.Primary,
    );
  }

  private clickCloseModal = () => {
    this.clientSelectedList = [];
    this.closeModal.emit();
  }

  private save = () => {
    if (!this.clientSelectedList || (this.clientSelectedList && this.clientSelectedList.length == 0)) {
      this.toasterEmitService.emitChange(SuccessError.error, 'alert.selectAtLeastOneClient');
      return;
    }

    this.clientResource.associateClientListWithTributsWithheld(this.clientSelectedList).subscribe( () => {
      this.clientSelectedList = [];
      this.toasterEmitService.emitChange(SuccessError.success, 'alert.associationMadeWithSuccess');
      this.closeModal.emit();
      this.addClient.emit();
    });
  }

}
