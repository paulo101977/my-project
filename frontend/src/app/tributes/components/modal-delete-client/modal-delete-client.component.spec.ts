import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDeleteClientComponent } from './modal-delete-client.component';
import { SharedService } from '@app/shared/services/shared/shared.service';
import { ButtonSize, ButtonType, Type } from 'projects/ui/src/public_api';
import { ButtonConfig } from '@app/shared/models/button-config';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { createServiceStub } from '../../../../../testing/create-service-stub';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ThexApiTestingModule, configureApi, ApiEnvironment } from '@inovacaocmnet/thx-bifrost';

describe('ModalDeleteClientComponent', () => {
  let component: ModalDeleteClientComponent;
  let fixture: ComponentFixture<ModalDeleteClientComponent>;

  const sharedServiceStub = createServiceStub(SharedService, {
    getButtonConfig(id: string,
                    callback: Function,
                    textButton: string,
                    buttonType: ButtonType,
                    buttonSize?: ButtonSize,
                    isDisabled?: boolean,
                    type?: Type): ButtonConfig { return new ButtonConfig(); },
    getHeaders(): any { return ''; }
  });


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        CommonModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ThexApiTestingModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ ModalDeleteClientComponent ],
      providers: [
        sharedServiceStub,
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDeleteClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
