import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ButtonConfig, ButtonType} from 'app/shared/models/button-config';
import {SharedService} from 'app/shared/services/shared/shared.service';
import {SuccessError} from 'app/shared/models/success-error-enum';
import {ToasterEmitService} from 'app/shared/services/shared/toaster-emit.service';
import {ClientResource} from 'app/shared/resources/client/client.resource';

@Component({
  selector: 'app-modal-delete-client',
  templateUrl: './modal-delete-client.component.html'
})
export class ModalDeleteClientComponent implements OnInit {
  @Input() modalId: string;
  @Input() companyClientId: string;
  @Output() closeModal = new EventEmitter();
  @Output() deleteClient = new EventEmitter();

  buttonCancelConfig: ButtonConfig;
  buttonDeleteConfig: ButtonConfig;

  constructor(
    private sharedService: SharedService,
    private toasterEmitService: ToasterEmitService,
    private clientResource: ClientResource) { }

  ngOnInit() {
    this.setButtonConfig();
  }

  private setButtonConfig() {
    this.buttonCancelConfig = this.sharedService.getButtonConfig(
      'reason-cancel-modal',
      this.cancel ,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonDeleteConfig = this.sharedService.getButtonConfig(
      'reason-save-modal',
      this.confirmDelete ,
      'action.confirm',
      ButtonType.Primary
    );
  }

  private cancel = () => {
    this.closeModal.emit();
  }

  private confirmDelete = () => {
    this.clientResource.deleteAssociationWithTributeWithheld(this.companyClientId).subscribe(response => {
      this.toasterEmitService.emitChange(SuccessError.success, 'alert.deleteDoneSuccessfully');
      this.closeModal.emit();
      this.deleteClient.emit();
    });
  }

}
