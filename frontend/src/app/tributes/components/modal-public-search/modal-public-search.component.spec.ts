// TODO: improve tests

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPublicSearchComponent } from './modal-public-search.component';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('ModalPublicSearchComponent', () => {
  let component: ModalPublicSearchComponent;
  let fixture: ComponentFixture<ModalPublicSearchComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPublicSearchComponent ],
      imports: [
        TranslateTestingModule,
      ],
      providers: [
        modalToggleServiceStub,
        sharedServiceStub,
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(ModalPublicSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
