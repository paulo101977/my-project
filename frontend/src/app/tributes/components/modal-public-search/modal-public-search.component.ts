import { Component, EventEmitter, OnChanges, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-modal-public-search',
  templateUrl: './modal-public-search.component.html',
  styleUrls: ['./modal-public-search.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalPublicSearchComponent implements OnInit {

  public readonly MODAL_ID = 'modal-public-search';
  public readonly ROWS_LIMIT = 4;

  public buttonCancelModalConfig: ButtonConfig;
  public buttonConfirmModalConfig: ButtonConfig;

  // TODO: create model
  public dataTableList: Array<any>;
  public dataTableListBack: Array<any>;
  public selectedItem: any;

  public columns: Array<any>;

  public observableSelected = new Subject<any>();

  @Output() emitSelected = new EventEmitter();

  constructor(
    private modalEmitToggleService: ModalEmitToggleService,
    private sharedService: SharedService,
  ) { }

  // TODO: change if necessary
  public setColumns() {
    this.columns = [{
      name: 'label.ncm',
      prop: 'ncm'
    },
    {
      name: 'label.barCode',
      prop: 'barCode'
    },
    {
      name: 'label.uf',
      prop: 'uf',
      flexGrow: 0.3
    }];
  }

  public toggleModal = () => {
    this
      .modalEmitToggleService
      .emitToggleWithRef(this.MODAL_ID);
  }

  // TODO: implement
  public selectItem(item) {
    this.selectedItem = item;
    this.observableSelected.next(item);
  }

  // TODO: remove mock data when back end
  public loadDataTableList() {
    this.dataTableList = [
      {
        id: 1,
        ncm: '2222',
        barCode: '44ddd-444',
        uf: 'RJ'
      },
      {
        id: 2,
        ncm: '2122',
        barCode: '1155d-444',
        uf: 'SP'
      },
      {
        id: 3,
        ncm: '4444',
        barCode: '1155d-777',
        uf: 'SC'
      },
      {
        id: 4,
        ncm: '5555',
        barCode: '6655d-777',
        uf: 'TC'
      },
      {
        id: 4,
        ncm: '5555',
        barCode: '6655d-777',
        uf: 'TC'
      },
      {
        id: 4,
        ncm: '5555',
        barCode: '6655d-777',
        uf: 'TC'
      },
      {
        id: 4,
        ncm: '5555',
        barCode: '6655d-777',
        uf: 'TC'
      }
    ];

    this.dataTableListBack = this.dataTableList;
  }

  // TODO: change if necessary
  public search = (event) => {
    const { value } = event.target;

    if ( typeof value === 'string' && value.length > 0 && this.dataTableListBack) {
      const regExp = new RegExp(value, 'i');
      this.dataTableList = this
        .dataTableListBack
        .filter( item => {
          return regExp.test(item.ncm) || regExp.test(item.barCode);
      });
    } else {
      this.dataTableList = this.dataTableListBack;
    }
  }

  public confirm = () => {
    this.emitSelected.emit(this.selectedItem);
  }

  public setButtons() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'button-cancel',
      this.toggleModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmModalConfig = this.sharedService.getButtonConfig(
      'button-confirm',
      this.confirm,
      'action.confirm',
      ButtonType.Primary,
      null,
      true,
    );
  }

  ngOnInit() {
    this.setButtons();
    this.setColumns();

    this.loadDataTableList();

    this.observableSelected.subscribe( (value) => {
      this.buttonConfirmModalConfig.isDisabled = true;

      if ( value ) {
        this.buttonConfirmModalConfig.isDisabled = false;
      }
    });
  }

}
