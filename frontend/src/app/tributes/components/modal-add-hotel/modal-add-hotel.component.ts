import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { Subject } from 'rxjs';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { PropertyResource } from 'app/shared/resources/property/property.resource';

@Component({
  selector: 'app-modal-add-hotel',
  templateUrl: './modal-add-hotel.component.html',
  styleUrls: ['./modal-add-hotel.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalAddHotelComponent implements OnInit, OnDestroy {

  public readonly MODAL_ID = 'modal-add-hotel';
  public readonly ROWS_LIMIT = 4;

  public buttonCancelModalConfig: ButtonConfig;
  public buttonConfirmModalConfig: ButtonConfig;

  // TODO: create model
  public dataTableList: Array<any>;
  public dataTableListBack: Array<any>;
  public selectedItems: Array<any>;
  public selectedItemToRemove: any;

  public columns: Array<any>;

  public observableSelected = new Subject<Array<any>>();

  public isAllSelected = false;


  @ViewChild('rowCheckbox') rowCheckbox: TemplateRef<any>;
  @ViewChild('headerCheckboxTemplate') headerCheckboxTemplate: TemplateRef<any>;

  @Input() set itemsHasBeenRemoved(item) {
    this.selectedItemToRemove = item;

    if ( item ) {
      this.removeItem(item);
    }
  }
  get itemsHasBeenRemoved() {
    return this.selectedItemToRemove;
  }

  @Output() emitSelected = new EventEmitter();

  constructor(
    private modalEmitToggleService: ModalEmitToggleService,
    private sharedService: SharedService,
    private propertyResource: PropertyResource,
  ) { }


  public setColumns() {
    this.columns = [{
      headerTemplate: this.headerCheckboxTemplate,
      cellTemplate: this.rowCheckbox,
      headerHeight: 45
    },
    {
      name: 'label.insertHotel',
      prop: 'name'
    },
    {
      name: 'label.chain',
      prop: 'brand'
    }];
  }

  public toggleModal = () => {
    this
      .modalEmitToggleService
      .emitToggleWithRef(this.MODAL_ID);
  }

  public removeItem( item ) {
    if ( this.selectedItems && this.dataTableList ) {
      const toChange = this.dataTableList.find( nItem => nItem.id === item.id);

      if ( toChange ) {
        toChange.isSelected = false;

        this.isAllSelected = false;
      }

      if ( this.selectedItems.length ) {
        this.selectedItems = this.selectedItems.filter( nItem => nItem.id !== item.id );

        this.observableSelected.next(this.selectedItems);
      }
    }
  }

  public pushOrRemoveItem( item ) {
    if ( this.selectedItems ) {
      const finded = this.selectedItems.find( nItem => nItem.id === item.id);

      if ( finded ) {
        this.selectedItems = this.selectedItems.filter( nItem => nItem.id !== item.id );
      } else {
        this.selectedItems.push( item );
      }
    } else {
      this.selectedItems = [];
      this.selectedItems.push(item);
    }
  }

  public toggleItem(event, item) {
    event.stopPropagation();

    this.pushOrRemoveItem(item);

    this.observableSelected.next(this.selectedItems);
  }

  public selectAllItems(event) {
    if ( this.dataTableList ) {
      this.selectedItems  = [];

      this.dataTableList.forEach( item => {
        item.isSelected = event;
      });

      if ( event ) {
        this.selectedItems = [...this.dataTableList];
      }

      this.observableSelected.next(this.selectedItems);
    }
  }


  public loadDataTableList() {
    this
      .propertyResource
      .getAll()
      .subscribe( ({items}) => {
        if ( items ) {
          this.dataTableList = items.map( item => {
            return {
              ...item,
              brand: item.brand ? item.brand.name : ''
            };
          });
          this.dataTableListBack = this.dataTableList;
        }

    });
  }


  public search = (event) => {
    const { value } = event.target;

    if ( typeof value === 'string' && value.length > 0 && this.dataTableListBack) {
      const regExp = new RegExp(value, 'i');
      this.dataTableList = this
        .dataTableListBack
        .filter( item => {
          return regExp.test(item.name);
        });
    } else {
      this.dataTableList = this.dataTableListBack;
    }
  }

  public confirm = () => {
    this.emitSelected.emit(this.selectedItems);

    this.toggleModal();
  }

  public setButtons() {
    this.buttonCancelModalConfig = this.sharedService.getButtonConfig(
      'button-cancel',
      this.toggleModal,
      'action.cancel',
      ButtonType.Secondary,
    );
    this.buttonConfirmModalConfig = this.sharedService.getButtonConfig(
      'button-confirm',
      this.confirm,
      'action.confirm',
      ButtonType.Primary,
      null,
      true,
    );
  }

  ngOnInit() {
    this.setButtons();
    this.setColumns();

    this.loadDataTableList();

    this.observableSelected.subscribe( (value) => {
      this.buttonConfirmModalConfig.isDisabled = !(value && value.length);
    });
  }

  ngOnDestroy() {
    if ( this.observableSelected ) {
      this.observableSelected.unsubscribe();
    }
  }

}
