import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { ModalAddHotelComponent } from './modal-add-hotel.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { propertyResourceStub } from 'app/shared/resources/property/testing';
import { modalToggleServiceStub } from 'app/shared/services/shared/testing/modal-emit-toogle-service';
import { sharedServiceStub } from 'app/shared/services/shared/testing/shared-service';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';

const hotelList = [
  {
    brand: { name: 'Bandeira Totvs Hoteis Resorts' },
    name: 'Hotel 1',
    id: 1
  },
  {
    brand: { name: 'Bandeira Totvs Hoteis Resorts' },
    name: 'Hotel 2',
    id: 2
  }
];

const eventMaker = (str: string) => {
  return {
    target: {
      value: str
    }
  };
};

describe('ModalAddHotelComponent', () => {
  let component: ModalAddHotelComponent;
  let fixture: ComponentFixture<ModalAddHotelComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        ModalAddHotelComponent,
      ],
      imports: [
        TranslateTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      providers: [
        propertyResourceStub,
        modalToggleServiceStub,
        sharedServiceStub,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddHotelComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();

    spyOn(component, 'loadDataTableList').and.callThrough();
    spyOn<any>(component['propertyResource'], 'getAll')
        .and
        .returnValue(of({ items: hotelList }));
    spyOn(component, 'search').and.callThrough();
    spyOn(component['observableSelected'], 'next').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', () => {
    spyOn(component, 'setButtons');
    spyOn(component, 'setColumns');


    component.ngOnInit();

    expect(component.setButtons).toHaveBeenCalled();
    expect(component.setColumns).toHaveBeenCalled();
    expect(component.loadDataTableList).toHaveBeenCalled();
    spyOn(component, 'pushOrRemoveItem').and.callThrough();
  });

  it('should test search', fakeAsync(() => {

    component.ngOnInit();
    tick();

    component.search(eventMaker('Hotel 1'));

    expect(component.dataTableList.length).toEqual(1);

    component.search(eventMaker(''));

    expect(component.dataTableList.length).toEqual(2);

    component.search(eventMaker('Hotel 2'));

    expect(component.dataTableList.length).toEqual(1);

    component.search(eventMaker('Hotel 2222'));

    expect(component.dataTableList.length).toEqual(0);

  }));

  it('should test confirm', () => {
    spyOn(component['emitSelected'], 'emit');
    spyOn(component, 'toggleModal');

    component.selectedItems = [hotelList[0]];

    component.confirm();

    expect(component['emitSelected'].emit).toHaveBeenCalledWith([hotelList[0]]);
    expect(component.toggleModal).toHaveBeenCalled();

  });

  it('should test selectAll methods', () => {
    spyOn(component, 'selectAllItems').and.callThrough();

    component.dataTableList = [...hotelList];

    component.selectAllItems(false);

    expect(component.selectedItems).toEqual([]);
    expect(component['observableSelected'].next).toHaveBeenCalledWith([]);


    component.selectAllItems(true);

    expect(component.selectedItems).toEqual(hotelList);
    expect(component['observableSelected'].next).toHaveBeenCalledWith(hotelList);

  });


  it('should test removeItem method', () => {
    component.dataTableList = [...hotelList];
    component.selectedItems = [];

    component.selectAllItems(true);
    component.removeItem(hotelList[0]);

    expect(component.isAllSelected).toEqual(false);
    expect(hotelList[0]['isSelected']).toEqual(false);
    expect(component['observableSelected'].next).toHaveBeenCalledWith(component.selectedItems);


    component.selectAllItems(true);
    component.removeItem(hotelList[1]);

    expect(component.isAllSelected).toEqual(false);
    expect(hotelList[1]['isSelected']).toEqual(false);
    expect(component['observableSelected'].next).toHaveBeenCalledWith(component.selectedItems);
  });


  it('should test pushOrRemoveItem method', () => {

    component.dataTableList = [...hotelList];
    component.selectedItems = [];

    component.pushOrRemoveItem(hotelList[0]);

    expect(component.selectedItems).toEqual([{...hotelList[0]}]);


    component.pushOrRemoveItem(hotelList[0]);

    expect(component.selectedItems).toEqual([]);
  });


  it('should test toggleItem method', () => {
    const event = { stopPropagation : () => {} };

    component.dataTableList = [...hotelList];
    component.selectedItems = [];

    component.toggleItem(event, hotelList[0]);

    expect(component.selectedItems).toEqual([{...hotelList[0]}]);
    expect(component['observableSelected'].next).toHaveBeenCalledWith(component.selectedItems);


    component.toggleItem(event, hotelList[0]);

    expect(component.selectedItems).toEqual([]);
    expect(component['observableSelected'].next).toHaveBeenCalledWith(component.selectedItems);
  });
});
