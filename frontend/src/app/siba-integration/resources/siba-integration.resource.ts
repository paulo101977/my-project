import { Injectable } from '@angular/core';
import { ThexApiService } from '@inovacaocmnet/thx-bifrost';
import { Observable, throwError } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Injectable({ providedIn: 'root' })
export class SibaIntegrationResource {

  constructor(
    private api: ThexApiService,
    private translate: TranslateService,
  ) {
  }

  public send(period: any): Observable<any> {
    if (period) {
      const send = {
        StartDate: period.beginDate,
        EndDate: period.endDate
      };

      return this.api.post<any>('sibaintegration/send', send);
    }
    return throwError(this.translate.instant('text.invalidPeriod'));
  }

}
