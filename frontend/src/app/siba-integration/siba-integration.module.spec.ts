import { SibaIntegrationModule } from './siba-integration.module';

describe('SibaIntegrationModule', () => {
  let sibaIntegrationModule: SibaIntegrationModule;

  beforeEach(() => {
    sibaIntegrationModule = new SibaIntegrationModule();
  });

  it('should create an instance', () => {
    expect(sibaIntegrationModule).toBeTruthy();
  });
});
