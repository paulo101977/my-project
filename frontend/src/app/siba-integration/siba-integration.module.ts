import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SibaIntegrationComponent } from './components/siba-integration/siba-integration.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [SibaIntegrationComponent],
  declarations: [SibaIntegrationComponent]
})
export class SibaIntegrationModule { }
