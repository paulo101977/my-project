import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SibaIntegrationComponent } from './siba-integration.component';
import { configureTestSuite } from 'ng-bullet';
import { FormBuilder } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ButtonType } from 'app/shared/models/button-config';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ThexApiTestingModule } from '@inovacaocmnet/thx-bifrost';
import { of } from 'rxjs';

describe('SibaIntegrationComponent', () => {
  let component: SibaIntegrationComponent;
  let fixture: ComponentFixture<SibaIntegrationComponent>;

  configureTestSuite(() => {
    TestBed
      .configureTestingModule({
        imports: [
          HttpClientTestingModule,
          TranslateTestingModule,
          ThexApiTestingModule
        ],
        providers: [
          FormBuilder
        ],
        declarations: [SibaIntegrationComponent],
        schemas: [NO_ERRORS_SCHEMA],
      });

    fixture = TestBed.createComponent(SibaIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on settings', () => {
    it('should setForm', () => {
      component['setForm']();

      expect(Object.keys(component.sibaForm.controls)).toEqual([
        'period',
      ]);
    });

    it('should setButtonConfig', () => {
      component['setButtonConfig']();

      expect(component.buttonSearch.id).toEqual('siba-search');
      expect(component.buttonSearch.textButton).toEqual('action.send');
      expect(component.buttonSearch.buttonType).toEqual(ButtonType.Primary);
    });

    it('should sendSiba', () => {
      spyOn(component['sibaIntegrationResource'], 'send').and.returnValue(of({}));

      const period = {
        beginDate: '',
        endDate: ''
      };
      component.sibaForm.patchValue({
        period: period
      });
      component['sendSiba']();

      expect(component['sibaIntegrationResource'].send).toHaveBeenCalledWith(period);
    });
  });
});
