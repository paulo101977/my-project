import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ButtonConfig, ButtonType } from 'app/shared/models/button-config';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { SibaIntegrationResource } from 'app/siba-integration/resources/siba-integration.resource';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-siba-integration',
  templateUrl: './siba-integration.component.html',
  styleUrls: ['./siba-integration.component.css']
})
export class SibaIntegrationComponent implements OnInit {

  public sibaForm: FormGroup;
  public buttonSearch: ButtonConfig;

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private sibaIntegrationResource: SibaIntegrationResource,
    private toaster: ToasterEmitService,
    private translate: TranslateService,
  ) {
  }

  ngOnInit() {
    this.setForm();
    this.setButtonConfig();
  }

  private setForm() {
    this.sibaForm = this.formBuilder.group({
      period: [null, Validators.required],
    });
  }

  private setButtonConfig() {
    this.buttonSearch = this.sharedService.getButtonConfig(
      'siba-search',
      () => this.sendSiba(),
      'action.send',
      ButtonType.Primary
    );
  }

  public sendSiba() {
    const { period } = this.sibaForm.value;
    const successMessage = this.translate.instant('variable.lbSentSuccessF', {
      labelName: this.translate.instant('label.sibaIntegration')
    });

    this.sibaIntegrationResource.send(period)
      .subscribe(
        () => this.toaster.emitChange(SuccessError.success, successMessage)
      );
  }
}
