[# ThsFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

![Build Status](https://totvs-hospitality-system.visualstudio.com/_apis/public/build/definitions/70b5b0b5-2135-47f8-934d-bea76b0f0628/19/badge)

Teste 1

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

[ Life Cycle Angular ](https://angular.io/docs/ts/latest/guide/lifecycle-hooks.html)

# Our Code Patterns

* Public and private methods must be preceded by a _public_ or _private_
  * Example: **public** myPublicMethod() {}

* Public and private attributes must be preceded by a _public_ or _private_ and should have type
  * Example: **public** isChecked: boolean;

* Avoid type **any** for Attributes, Methods params, Methods return

## Folder structure
  * The **Module** folder must be have _.module_ and _-routing.module_ files
  * All the folders that have _.component_, _.service_ files should have in the same place the **_.spec_** file
  * The folder **Component**, **Container** must be have _?.css_, _.html_, _.ts_, _.spec_
  * The folder **Service**, **Pipes** must be have _.ts_, _.spec_
  * The folder **Mock-data** must be have _data.ts_
  
  * Create **Components-containers** folder to smartComponents
  * Create **Components** folder to dumbComponents
  * Create **Services** folder to services
  * Create **Resources** folder to resources
  * Create **Models** folder to models
  * Create **Interfaces** folder to interfaces
  * Create **Pipes** folder to pipes
  * Create **Mock-data** folder to mock data
  

## Convention Variables name, class and methods
  ### Variables
    * To array put suffix **List**
      * Example: myVariableNameList
    * To object the pattern is only CameCasel
    * To services follow the service name more the suffix **Service**
      * Example: roomTypeService
    * To resources follow the resource name more the suffix **Resource**
      * Example: roomTypeResource
    * To components inject by "Controller" (ComponentFactoryResolver etc) follow the component name more the suffix **Component**
      * Example: roomTypeComponent
    * To models follow only the model name
      * Example: roomType
    * To mock data follow the data name more the suffix **Data**
  
  ### Class
    * To models follow only the model name
      * Example: export class RoomType {}
    * To interfaces follow the interface name must be preceded **I**
      * Example: export interface IRoomType {}
    * To enum follow the enum name more the suffix **Enum**
      * Example: export enum RoomTypeEnum {}
    * To components follow the component name more the suffix **Component**
      * Example: export class RoomTypeComponent {}
    * To resources follow the resource name more the suffix **Resource**
      * Example: export class RoomTypeResource {}
    * To services follow the service name more the suffix **Service**
      * Example: export class RoomTypeService {}

  ### Methods
    * The methods name in components "controller" must be preceded by **get**, **set** and **update**
    * The listeners methods in components "controller" must be suffix **Listener**
      * private roomTypeListener() {}
    * The methods name in "services" must be preceded by **get** and **check**
    * The methods name in "resources" must be preceded by **get**, **update**, **delete**, **create** and **set**
    * Method name to set variables must be **private setVars()**
    * Method name to set listeners must be **private setListeners()**

## Convention Models
  * When model is used in only one compoment use the convention default
  * When model is used in many components in different modules create model with suffix **moduleName-view**
   * Example: model reservation-item -> reservation-item-guest-view and reservation-item-view(model and module has the same name)  

## Convention nomeclature to files

  * All file should have suffix _.service_ or  _.component_ or _.interface_ or _.resource_ or _.module_

## Unit Tests
  * Always instanciate TestBed in beforeAll.
    * Example: 
      ```js
        beforeAll(() => {
          TestBed.resetTestEnvironment();
          TestBed
            .initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting())
            .configureTestingModule({
              imports: [...],
              declarations: [...],
              providers: [...],
              schemas: [...]
            });
          fixture = TestBed.createComponent(MyComponent);
          component = fixture.componentInstance;
          fixture.detectChanges();
      });
      ```
  * The describe must be preceded **on**
    * Example: 
      * describe('on init', () => {})
      * describe('on popovers', () => {})
  * The it must be preceded **should**
    * Example:
      * it('should setVars', () => {})
