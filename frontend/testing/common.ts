import { createServiceStub } from './create-service-stub';
import { Location } from '@angular/common';
import { FnrhService } from 'app/shared/services/fnrh/fnrh.service';
import { LoadPageEmitService } from 'app/shared/services/shared/load-emit.service';
import { ShowHide } from 'app/shared/models/show-hide-enum';
import { ModalEmitToggleService } from 'app/shared/services/shared/modal-emit-toggle.service';
import { UserService } from 'app/shared/services/user/user.service';
import { ShowHideContentEmitService } from 'app/shared/services/shared/show-hide-content-emit-service';
import { AssetsService } from 'app/core/services/assets.service';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { SharedService } from 'app/shared/services/shared/shared.service';
import { ButtonConfig, ButtonSize, ButtonType, Type  as TypeButton } from 'app/shared/models/button-config';
import { ModalRemarksFormEmitService } from 'app/shared/services/shared/modal-remarks-form-emit.service';
import { createPipeStub } from './create-pipe-stub';
import { NavigationExtras, Router } from '@angular/router';
import { CurrencyService } from 'app/shared/services/shared/currency-service.service';

export const locationStub = createServiceStub(Location, {
  back(): void {}
});

export const fnrhServiceStub = createServiceStub(FnrhService, {
  fnrhAddressToApply: null
});

export const loadPageEmitServiceStub = createServiceStub(LoadPageEmitService, {
  emitChange(show: ShowHide) {}
});


export const  modalToggleServiceStub = createServiceStub(ModalEmitToggleService, {
  emitCloseWithRef(modalId: any): void {},
  closeAllModals(): void {},
  emitToggleWithRef(modalId: any): void {},
  emitOpenWithRef(modalId: any): void {},
});

export const userServiceStub = createServiceStub(UserService, {
  getUserpreferredLanguage: () => '',

  getUserLocalStorage: () => null,
});

export const showHideContentEmitServiceStub = createServiceStub(ShowHideContentEmitService, {
  emitChange(show: ShowHide) {}
});


export const assetsServiceStub = createServiceStub(AssetsService, {
  getComponentsUrlTo: (item: string) => '',
  getImgUrlTo: (item: string) => '',
});


export const commomServiceStub = createServiceStub(CommomService, {
  toOption: (objectToConvert: any, keyName: string, valueName: string) => []
});

export const toasterEmitServiceStub = createServiceStub(ToasterEmitService, {
  emitChange(isSuccess: SuccessError, message: string) {},
  emitChangeTwoMessages(isSuccess: SuccessError, message: string, variable: string) {}
});

export const sharedServiceStub = createServiceStub(SharedService, {
  getButtonConfig: (
    id: string,
    callback: Function,
    textButton: string,
    buttonType: ButtonType,
    buttonSize?: ButtonSize,
    isDisabled?: boolean,
    type?: TypeButton
  ) => new ButtonConfig(),
  getHeaders(): any {}
});

export const modalRemarksFormEmitServiceStub = createServiceStub(ModalRemarksFormEmitService, {
  emitSimpleModal(internalComents: string, externalComents: string, partnerComents: string) {}
});

export const routerStub = createServiceStub(Router, {
  navigate: (commands: any[], extras?: NavigationExtras) =>  new Promise<boolean>( resolve => true )
});

export const currencyServiceStub = createServiceStub(CurrencyService, {
  getDefaultCurrencySymbol: (propertyId: number) => ''
});

export const imgCdnStub = createPipeStub({name: 'imgCdn'});
export const padStub = createPipeStub({name: 'pad'});
export const currencyFormatPipe = createPipeStub({name: 'currencyFormat'});
export const amDateFormatPipeStub = createPipeStub({name: 'amDateFormat'});
