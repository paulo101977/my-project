import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ValueAcessorStub } from './value-acessor-stub';

/**
 * create stub for components and apply the interface ValueAccessor
 *
 * @param selector the component selector
 * @return Component, the Component with selector and interface ValueAcessor
 */
export const createAccessorComponent = (selector) => {
  @Component({
    selector: selector,
    template: '',
    providers: [
      {
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => AccessorComponent),
        multi: true,
      },
    ],
  })
  // @ts-ignore
  class AccessorComponent extends ValueAcessorStub {} // tslint:disable-line


  return AccessorComponent;
};
