import { Pipe, PipeTransform } from '@angular/core';

export function createPipeStub(obj: Pipe, transform = (...args) => '') {
  @Pipe(obj)
  class StubPipe implements PipeTransform {
    transform(value: any, ...args: any[]): any {
      return transform(value, ...args);
    }
  }

  return StubPipe;
}
