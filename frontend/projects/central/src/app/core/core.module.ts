import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { httpInterceptorProviders } from '@central/interceptors/http-interceptors.providers';
import { configureAppName } from '@inovacaocmnet/thx-bifrost';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [ CommonModule, SharedModule ],
  providers: [
    configureAppName('crs'),
    httpInterceptorProviders
  ],
})
export class CoreModule { }
