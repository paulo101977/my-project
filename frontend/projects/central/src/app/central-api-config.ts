import { ThexApiService, CentralApiService } from '@inovacaocmnet/thx-bifrost';

export const CentralApiConfig = { provide: ThexApiService, useClass: CentralApiService};
