import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailabilityGridPageComponent } from './availability-grid-page.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CentralApiModule, configureApi, ApiEnvironment } from '@inovacaocmnet/thx-bifrost';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('AvailabilityGridPageComponent', () => {
  let component: AvailabilityGridPageComponent;
  let fixture: ComponentFixture<AvailabilityGridPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateTestingModule, HttpClientTestingModule, CentralApiModule, RouterTestingModule ],
      declarations: [ AvailabilityGridPageComponent ],
      providers: [
        configureApi({
          environment: ApiEnvironment.Development
        })
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityGridPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
