import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { AvailabilityResource } from '@central/availability/resources/availability.resource';
import {
  Availability,
  AvailabilityGridLabels,
  Footer,
  Property,
  Reservation,
  RoomType,
  ThxMultiPropertyAvailabilityGridComponent
} from '@inovacao-cmnet/thx-ui';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { APP_NAME, PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { tap } from 'rxjs/operators';
import { DateService } from '@app/shared/services/shared/date.service';

@Component({
  selector: 'central-availability-grid-page',
  templateUrl: './availability-grid-page.component.html',
  styleUrls: ['./availability-grid-page.component.css']
})
export class AvailabilityGridPageComponent implements OnInit {

  // Grid Items
  public propertyList: Property[];
  public availabilityList: Availability[];
  public footerList: Footer[];
  public reservationList: Reservation[] = [];
  public roomTypeList: RoomType[];
  public expandedRoomType: number;

  public startDate: string;
  public days: number;
  public labels: AvailabilityGridLabels;

  @ViewChild('grid')
    public availabilityGridComponent: ThxMultiPropertyAvailabilityGridComponent;



  constructor(
    @Inject(APP_NAME) private appName: string,
    private availabilityResource: AvailabilityResource,
    private translateService: TranslateService,
    public router: Router,
    private parameterSession: SessionParameterService,
    private propertyStorageService: PropertyStorageService,
    private dateService: DateService
  ) {}

  ngOnInit() {
    this.setLabels();
  }

  public filter(info) {
    this.startDate = info.initialDate;
    console.log(new Date(this.startDate));
    this.days = info.period == 30 ? this.dateService.getTotalDaysFromMonth(new Date(this.startDate)) : info.period;
    this.availabilityResource
      .getAvailabilityProperties(info.initialDate, this.days).subscribe( data => {
      if (this.expandedRoomType) {
        const expandedRoomType = data.propertyList.find(property => property.id == this.expandedRoomType);
        expandedRoomType.expanded = true;
        this.getRoomTypes(this.expandedRoomType, this.startDate, this.days);
      }
      this.propertyList = data.propertyList;
      this.availabilityList = data.availabilityList;
      this.footerList = data.footerList;
    });
  }

  public getRoomTypes(propertyId: number, startDate: string, days: number) {
    this.expandedRoomType = propertyId;
    this.availabilityResource.getRoomTypes(propertyId, startDate, days)
      .pipe(
        tap(data => {
            data.roomTypeList.map( item => item.propertyId = propertyId);
            data.availabilityList.map( item => item.propertyId = propertyId);
        })
      )
      .subscribe( data => {
        this.availabilityGridComponent.setRoomTypes(propertyId, data.roomTypeList);
      const setAvailability = new Set([...this.availabilityList, ...data.availabilityList]);
      this.availabilityList = Array.from(setAvailability);
      this.roomTypeList = data.roomTypeList;
      this.reservationList = data.reservationList;
    });
  }

  public setLabels() {
    const labels = [
      'propertyModule.availabilityGrid.labels.overbook',
      'propertyModule.availabilityGrid.labels.legend',
      'propertyModule.availabilityGrid.labels.checkin',
      'propertyModule.availabilityGrid.labels.checkout',
      'propertyModule.availabilityGrid.labels.toConfirm',
      'propertyModule.availabilityGrid.labels.confirmed',
      'propertyModule.availabilityGrid.labels.showToday',
      'propertyModule.availabilityGrid.labels.reservationCode',
      'propertyModule.availabilityGrid.labels.roomType',
      'propertyModule.availabilityGrid.labels.status',
      'propertyModule.availabilityGrid.labels.period',
      'propertyModule.availabilityGrid.labels.groupName',
      'propertyModule.availabilityGrid.labels.reservationDetails',
      'propertyModule.availabilityGrid.labels.newReservation',
      'propertyModule.availabilityGrid.labels.cornerLabel',
      'propertyModule.availabilityGrid.labels.rateProposal'
    ];

    this.translateService.get(labels).subscribe(data => {
      this.labels = {
        overbook: data['propertyModule.availabilityGrid.labels.overbook'],
        legend: data['propertyModule.availabilityGrid.labels.legend'],
        checkin: data['propertyModule.availabilityGrid.labels.checkin'],
        checkout: data['propertyModule.availabilityGrid.labels.checkout'],
        toConfirm: data['propertyModule.availabilityGrid.labels.toConfirm'],
        confirmed: data['propertyModule.availabilityGrid.labels.confirmed'],
        showToday: data['propertyModule.availabilityGrid.labels.showToday'],
        reservationCode: data['propertyModule.availabilityGrid.labels.reservationCode'],
        roomType: data['propertyModule.availabilityGrid.labels.roomType'],
        status: data['propertyModule.availabilityGrid.labels.status'],
        period: data['propertyModule.availabilityGrid.labels.period'],
        groupName: data['propertyModule.availabilityGrid.labels.groupName'],
        reservationDetails: data['propertyModule.availabilityGrid.labels.reservationDetails'],
        newReservation: 'Reservar período',
        cornerLabel: data['propertyModule.availabilityGrid.labels.cornerLabel'],
        unlockRoom: 'Desbloquear UH',
        blockRoom: 'Bloquear UH',
        reservationRateProposal: data['propertyModule.availabilityGrid.labels.rateProposal'],
      };
    });
  }

  public onInitialDateChange(date: string) {
    this.startDate = date;
    this.filter({initialDate: this.startDate, period: this.days});
  }

  public goToRateProposal(period: any) {
    this.parameterSession.populateSessionParameters(2);
    this.router.navigate(['central', 'reservation', 'rate-proposal'], { queryParams: period });
  }

  public goToReservation(reservation: any) {
    this.propertyStorageService
      .setCurrentPropertyFromPropertyList(reservation.propertyId, this.appName);
    this.router.navigate(['central', 'reservation', 'create'], { queryParams: reservation });
  }
}
