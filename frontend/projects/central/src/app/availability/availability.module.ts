import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvailabilityRoutingModule } from './availability-routing.module';
import { AvailabilityGridPageComponent } from './grid/availability-grid-page/availability-grid-page.component';
import { CoreModule } from '@central/core/core.module';
import { GridSearchFormComponent } from './components/grid-search-form/grid-search-form.component';
import { SharedModule } from 'app/shared/shared.module';
import { ThxMultiPropertyAvailabilityGridModule } from '@inovacao-cmnet/thx-ui';


@NgModule({
  imports: [
    CommonModule,
    AvailabilityRoutingModule,
    CoreModule,
    SharedModule,
    ThxMultiPropertyAvailabilityGridModule
  ],
  declarations: [AvailabilityGridPageComponent, GridSearchFormComponent]
})
export class AvailabilityModule { }
