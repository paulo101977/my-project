import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AvailabilityGridPageComponent} from '@central/availability/grid/availability-grid-page/availability-grid-page.component';

const routes: Routes = [
  { path: '', component: AvailabilityGridPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AvailabilityRoutingModule { }
