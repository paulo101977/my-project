import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ButtonBarConfig } from 'app/shared/components/button-bar/button-bar-config';
import { ButtonBarColor } from 'app/shared/components/button-bar/button-bar-color.enum';
import { AvailabilityGridPeriodEnum } from 'app/shared/models/availability-grid/availability-grid-period.enum';
import { ButtonBarButtonStatus } from 'app/shared/components/button-bar/button-bar-button-status.enum';
import { ButtonBarButtonConfig } from 'app/shared/components/button-bar/button-bar-button-config';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AvailabilityGridFilter } from 'app/shared/models/availability-grid/availability-grid-filter';
import { DateService } from 'app/shared/services/shared/date.service';
import * as moment from 'moment';

@Component({
  selector: 'central-grid-search-form',
  templateUrl: './grid-search-form.component.html',
  styleUrls: ['./grid-search-form.component.css']
})
export class GridSearchFormComponent implements OnInit {

  public buttonBarConfig: ButtonBarConfig;
  public gridFilter: FormGroup;
  public initialDate: string;
  public period: number;
  public propertyId: number;

  @Output() filterBy = new EventEmitter();

  constructor( private formBuilder: FormBuilder,
               private dateService: DateService) { }

  ngOnInit() {
    this.setButtonBarConfig();
    this.setAvailabilityGridFilterForm();
  }

  private setAvailabilityGridFilterForm() {
    const today = new Date();
    this.gridFilter = this.formBuilder.group({
      initialDate: [ today , [Validators.required]],
      period: [AvailabilityGridPeriodEnum.weekly, [Validators.required]],
    });

    this.gridFilter.valueChanges.subscribe(data => this.filter(this.gridFilter));

    this.filter(this.gridFilter);
  }

  public filter({ value }: { value: AvailabilityGridFilter }) {
    const myData = this.dateService.convertUniversalDateToIMyDate( value.initialDate );
    const year = myData.date.year;
    const month = myData.date.month;
    const day = myData.date.day;

    const initialDate = new Date(year, month - 1, day);

    this.initialDate = moment(initialDate)
      .format(DateService.DATE_FORMAT_UNIVERSAL)
      .toString();
    this.period = value.period;

    this.filterBy.emit( {
      initialDate: this.initialDate,
      period: this.period
    });
  }

  private setPeriod(period: number) {
    this.gridFilter.patchValue({ period: period });
    this.buttonBarConfig.buttons.map(
      button => (button.status = button.id == period ? ButtonBarButtonStatus.active : ButtonBarButtonStatus.default),
    );
  }

  public setButtonBarConfig() {
    this.buttonBarConfig = {
      color: ButtonBarColor.black,
      buttons: [
        {
          id: AvailabilityGridPeriodEnum.weekly,
          title: 'propertyModule.availabilityGrid.periodTypes.weekly',
          status: ButtonBarButtonStatus.active,
          callBackFunction: () => this.setPeriod(AvailabilityGridPeriodEnum.weekly),
        } as ButtonBarButtonConfig,
        {
          title: 'propertyModule.availabilityGrid.periodTypes.fortnightly',
          callBackFunction: () => this.setPeriod(AvailabilityGridPeriodEnum.fortnightly),
        } as ButtonBarButtonConfig,
        {
          title: 'propertyModule.availabilityGrid.periodTypes.monthly',
          callBackFunction: () => this.setPeriod(AvailabilityGridPeriodEnum.monthly),
          cssClass: 'hidden-sm-down',
        } as ButtonBarButtonConfig,
      ],
    };
  }

}
