import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridSearchFormComponent } from './grid-search-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ThxDatatableModule } from '@inovacao-cmnet/thx-ui';
import { SharedModule } from 'app/shared/shared.module';
import { configureTestSuite } from 'ng-bullet';

describe('GridSearchFormComponent', () => {
  let component: GridSearchFormComponent;
  let fixture: ComponentFixture<GridSearchFormComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        TranslateTestingModule,
        HttpClientTestingModule,
        ThxDatatableModule,
        SharedModule
      ],
      declarations: [ GridSearchFormComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    });
    fixture = TestBed.createComponent(GridSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
