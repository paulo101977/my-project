import { TestBed, inject } from '@angular/core/testing';
import { AvailabilityResource } from './availability.resource';
import { CentralApiModule, configureApi, ApiEnvironment  } from '@inovacaocmnet/thx-bifrost';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AvailabilityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, CentralApiModule ],
      providers: [
        AvailabilityResource,
        configureApi({
          environment: ApiEnvironment.Development
        })
      ],
    });
  });

  it('should be created', inject([AvailabilityResource], (service: AvailabilityResource) => {
    expect(service).toBeTruthy();
  }));
});
