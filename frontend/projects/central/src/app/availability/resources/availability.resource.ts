import { Injectable } from '@angular/core';
import { CentralApiService } from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class AvailabilityResource {

  constructor(private centralApi: CentralApiService) { }

  public getAvailabilityProperties(initialDate: string, period: number): Observable<any> {
    return this.centralApi.get('Availability/properties', { params: {initialDate, period}});
  }

  public getRoomTypes(propertyId: number, initialDate: string, period: number): Observable<any> {
    return this.centralApi.get(`Availability/${propertyId}/${propertyId}/roomTypes`, { params: {initialDate, period}});
  }
}
