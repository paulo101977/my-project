import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseContainerComponent } from './base-container/base-container/base-container.component';
import { PropertyCardVisibilityResolver } from '@central/property/components/property-card-selector/property-card-visibility.resolver';

const centralRoutes: Routes = [
  {path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'},
  {path: 'availability', loadChildren: './availability/availability.module#AvailabilityModule'},
  {path: 'reservation', loadChildren: './reservation/reservation.module#ReservationModule'}
];

export const appRoutes: Routes = [
  {path: '*', redirectTo: '/', pathMatch: 'full'},
  {path: '', redirectTo: 'auth', pathMatch: 'full'},
  {path: 'login', redirectTo: 'auth', pathMatch: 'full'},
  {path: 'auth', loadChildren: './auth/auth.module#AuthModule'},
  {path: 'central', component: BaseContainerComponent, children: centralRoutes , resolve: { propertyCard: PropertyCardVisibilityResolver },
  runGuardsAndResolvers: 'always'
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      paramsInheritanceStrategy: 'always',
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
