import { Component, Inject, OnInit } from '@angular/core';
import { MenuOption } from 'app/shared/models/menu-option';
import { APP_NAME, ProductEnum, User } from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { PropertyCardSelectorService } from '@central/property/components/property-card-selector/property-card-selector.service';
import { pluck } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CentralMenuPermissions } from '@central/permissions/central-menu-permissions';
import { UserPermissionService, UserPermissionMenuItem } from '@thex/security';

@Component({
  selector: 'central-base-container',
  templateUrl: './base-container.component.html',
  styleUrls: ['./base-container.component.css']
})
export class BaseContainerComponent implements OnInit {
  public menuOptions: MenuOption[];
  public user: User;
  public menuOptions$: Observable<UserPermissionMenuItem[]>;


  constructor(@Inject(APP_NAME) private appName,
              private translateService: TranslateService,
              private propertyCardService: PropertyCardSelectorService,
              public userPermissionService: UserPermissionService) {}

  ngOnInit() {
    this.getMenu();
  }

  public getMenu() {
    this.menuOptions$ = this.userPermissionService
      .getUserPermissions(this.appName, ProductEnum.Central, CentralMenuPermissions)
      .pipe(
        pluck('menuList')
      );
  }

  public needShowPropertySelector() {
    return this.propertyCardService.propertyCardVisibility();
  }
}
