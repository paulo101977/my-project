import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservationRoutingModule } from './reservation-routing.module';
import { ReservationListPageComponent } from './list/reservation-list-page/reservation-list-page.component';
import { CoreModule } from '@central/core/core.module';
import { RateProposalModule } from './rate-proposal/rate-proposal.module';
import { ReservationModule as PMSReservationModule } from '@app/reservation/reservation.module';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';
import { ReservationListService } from '@central/reservation/services/reservation-list.service';
import { CentralApiConfig } from '@central/central-api-config';
import { RoomModule } from 'app/room/room.module';
import { CreateEditPageComponent } from './create-edit-page/create-edit-page.component';
import { SharedModule } from 'app/shared/shared.module';
import { CheckinModule } from 'app/checkin/checkin.module';
import { GuestModule } from 'app/guest/guest.module';
import { ReservationBudgetModule } from 'app/revenue-management/reservation-budget/reservation-budget.module';
import { SingleRoomListPageComponent } from './single-room-list-page/single-room-list-page.component';
import { MultipleRoomListPageComponent } from './multiple-room-list-page/multiple-room-list-page.component';
import { BudgetPageComponent } from './budget-page/budget-page.component';

@NgModule({
  imports: [
    CommonModule,
    RateProposalModule,
    CoreModule,
    SharedModule,
    CheckinModule,
    PMSReservationModule,
    ReservationRoutingModule,
    ReservationBudgetModule,
    GuestModule,
    RoomModule
  ],
  providers: [
    {provide: ReservationService, useClass: ReservationListService},
    CentralApiConfig
  ],
  declarations: [ ReservationListPageComponent,
    CreateEditPageComponent,
    SingleRoomListPageComponent,
    MultipleRoomListPageComponent,
    BudgetPageComponent ],
})
export class ReservationModule { }
