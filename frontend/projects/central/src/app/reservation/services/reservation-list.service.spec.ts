import { inject, TestBed } from '@angular/core/testing';
import { ReservationListService } from './reservation-list.service';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { ThexApiModule } from '@inovacaocmnet/thx-bifrost';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

describe('ReservationListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        ThexApiModule ],
      providers: [ReservationListService]
    });
  });

  it('should be created', inject([ReservationListService], (service: ReservationListService) => {
    expect(service).toBeTruthy();
  }));
});
