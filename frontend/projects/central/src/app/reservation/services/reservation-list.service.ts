import { Injectable } from '@angular/core';
import { ReservationService } from 'app/shared/services/reservation/reservation.service';
import { SearchReservationResultDto } from 'app/shared/models/dto/search-reservation-result-dto';
import { RateProposalSearchModel } from 'app/reservation/rate-proposal/models/rate-proposal-search-model';
import { RateProposalRateTypeItem } from 'app/reservation/rate-proposal/models/rate-proposal-rate-type-item';

@Injectable({
  providedIn: 'root'
})
export class ReservationListService extends ReservationService {

  public showCheckinButton(itemData: SearchReservationResultDto): boolean {
    return false;
  }

  public showAddGuestEntranceButton(itemData: SearchReservationResultDto, roomTypeList: Array<any>): boolean {
    return false;
  }

  public showBillingAccountButton(billbingAccountId) {
    return false;
  }

  public showGuestFileButton(itemData: SearchReservationResultDto): boolean {
    return false;
  }

  public goToEditReservationPage(id: number, propertyId: string) {
    this.router.navigate(['central', 'reservation', 'create'], {queryParams: {reservation: id}});
  }

  public goToNewReservation(propertyId: string) {
    return this.router.navigate(['central', 'reservation', 'create']);
  }

  public goToGuestPage(item: any, propertyId: string) {
    // todo redirect to correct place
    // this.router.navigate(['p', propertyId, 'guest', 'fnrh', 'edit', item.reservationItemId,
    // item.reservationCode, 'guest', item.guestReservationItemId]);
  }

  public goToBudgetPage(id: number, propertyId: string) {
    this.router.navigate(['central', 'reservation', 'budget', id]);
  }

  public goToRoomList(id: string|number, reservationItemCount: number, propertyId?: number ) {
    if (reservationItemCount > 1) {
      return this.router.navigate(['central', 'reservation', 'guest', 'room-list', id]);
    }
    return this.router.navigate(['central', 'reservation', 'guest', id]);
  }

  public goToSlip(reservationId?: number|string, propertyId?: number|string) {
    return this.router.navigate(['central', 'reservation', 'slip', reservationId]);
  }

  public goToSlipPage(item: SearchReservationResultDto, propertyId: string) {
    return this.router.navigate(['central', 'reservation', 'slip', item.id]);
  }

  public goToReservationList(propertyId?: number) {
    return this.router.navigate(['central', 'reservation', 'list' ]);
  }

  public goToReservationConfirm(
      rateProposalSearch: RateProposalSearchModel,
      rateProposalItemSelected: RateProposalRateTypeItem
  ) {
    const token = this.rateProposalSharedInfo.storeRateProposal(
        rateProposalSearch,
        rateProposalItemSelected,
    );

    this.router.navigate([
      'central', 'reservation', 'create'
    ], { queryParams: { rateProposal: token} });
  }

  public goToViewReservationPage(id: number, propertyId: string) {
    this
      .router
      .navigate(
      ['central', 'reservation', 'viewmode'],
      {queryParams: {reservation: id}}
      );
  }
}
