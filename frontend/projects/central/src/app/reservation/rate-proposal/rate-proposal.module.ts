import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RateProposalPageComponent } from './rate-proposal-page/rate-proposal-page.component';
import { RateProposalModule as PMSRateProposalModule } from 'app/reservation/rate-proposal/rate-proposal.module';
import { RateProposalService as PMSRateProposalService } from 'app/reservation/rate-proposal/services/rate-proposal.service';
import { RateProposalService } from '@central/reservation/rate-proposal/rate-proposal-page/services/rate-proposal.service';


@NgModule({
  imports: [
    CommonModule,
    PMSRateProposalModule,
  ],
  declarations: [RateProposalPageComponent],
  exports: [RateProposalPageComponent],
  providers: [ { provide: PMSRateProposalService, useClass: RateProposalService} ]
})
export class RateProposalModule { }
