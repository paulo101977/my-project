import { Injectable } from '@angular/core';
import {
  RateProposalService as PMSRateProposalService
} from 'app/reservation/rate-proposal/services/rate-proposal.service';

@Injectable({
  providedIn: 'root'
})
export class RateProposalService extends PMSRateProposalService {

  public navigateConfirm(propertyId: number, token) {
    this._route.navigate(['central', 'reservation', 'new'], {queryParams: {rateProposal: token}});
  }
}
