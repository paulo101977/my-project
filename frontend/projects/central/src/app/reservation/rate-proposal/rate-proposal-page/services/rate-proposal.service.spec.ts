import { inject, TestBed } from '@angular/core/testing';
import { RateProposalService } from './rate-proposal.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ApiEnvironment, configureApi, configureAppName, ThexApiModule } from '@inovacaocmnet/thx-bifrost';
import { RouterTestingModule } from '@angular/router/testing';

describe('RateProposalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ ThexApiModule, HttpClientTestingModule, RouterTestingModule ],
      providers: [
        RateProposalService,
        configureApi({
          environment: ApiEnvironment.Development
        }),
        configureAppName('crs')
      ]
    });
  });

  it('should be created', inject([RateProposalService], (service: RateProposalService) => {
    expect(service).toBeTruthy();
  }));
});
