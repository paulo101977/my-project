import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateProposalPageComponent } from './rate-proposal-page.component';
import { ThexApiModule, CentralApiModule } from '@inovacaocmnet/thx-bifrost';
import { configureTestSuite } from 'ng-bullet';
import { ReservationListPageComponent } from '@central/reservation/list/reservation-list-page/reservation-list-page.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('RateProposalPageComponent', () => {
  let component: RateProposalPageComponent;
  let fixture: ComponentFixture<RateProposalPageComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [ ThexApiModule, CentralApiModule],
      declarations: [ RateProposalPageComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });
    fixture = TestBed.createComponent(RateProposalPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
