import { Component, OnDestroy, OnInit } from '@angular/core';
import { PropertyCardSelectorService } from '@central/property/components/property-card-selector/property-card-selector.service';

@Component({
  selector: 'central-multiple-room-list-page',
  template: `
    <div class="col-12">
      <app-guest-edit-roomlist></app-guest-edit-roomlist>
    </div>
  `,
})
export class MultipleRoomListPageComponent implements OnInit, OnDestroy {

  constructor( private propertyCardService: PropertyCardSelectorService ) { }

  ngOnInit() {
    this.propertyCardService.disablePropertyCard();
  }

  ngOnDestroy(): void {
    this.propertyCardService.enablePropertyCard();
  }

}
