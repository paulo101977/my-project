import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleRoomListPageComponent } from './multiple-room-list-page.component';
import { configureTestSuite } from 'ng-bullet';

describe('MultipleRoomListPageComponent', () => {
  let component: MultipleRoomListPageComponent;
  let fixture: ComponentFixture<MultipleRoomListPageComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleRoomListPageComponent ]
    });
    fixture = TestBed.createComponent(MultipleRoomListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
