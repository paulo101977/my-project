import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReservationListPageComponent } from '@central/reservation/list/reservation-list-page/reservation-list-page.component';
import { RateProposalPageComponent } from '@central/reservation/rate-proposal/rate-proposal-page/rate-proposal-page.component';
import { CreateEditPageComponent } from '@central/reservation/create-edit-page/create-edit-page.component';
import { SingleRoomListPageComponent } from '@central/reservation/single-room-list-page/single-room-list-page.component';
import { MultipleRoomListPageComponent } from '@central/reservation/multiple-room-list-page/multiple-room-list-page.component';
import { BudgetPageComponent } from '@central/reservation/budget-page/budget-page.component';

const routes: Routes = [
  { path: 'list', component: ReservationListPageComponent },
  { path: 'rate-proposal', component: RateProposalPageComponent },
  { path: 'create', component: CreateEditPageComponent },
  { path: 'viewmode', component: CreateEditPageComponent },
  { path: 'guest/:reservationItemId', component: SingleRoomListPageComponent },
  { path: 'guest/room-list/:reservationId', component: MultipleRoomListPageComponent },
  { path: 'budget/:reservationId', component: BudgetPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationRoutingModule { }
