import { Component, OnDestroy, OnInit } from '@angular/core';
import { PropertyCardSelectorService } from '@central/property/components/property-card-selector/property-card-selector.service';

@Component({
  selector: 'central-budget-page',
  template: `
    <div class="col-12">
      <reservation-budget-manage></reservation-budget-manage>
    </div>
  `
})
export class BudgetPageComponent implements OnInit, OnDestroy {
  constructor( private propertyCardService: PropertyCardSelectorService ) { }

  ngOnInit() {
    this.propertyCardService.disablePropertyCard();
  }

  ngOnDestroy(): void {
    this.propertyCardService.enablePropertyCard();
  }

}
