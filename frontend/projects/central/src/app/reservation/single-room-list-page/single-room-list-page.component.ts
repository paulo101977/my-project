import { Component, OnDestroy, OnInit } from '@angular/core';
import { PropertyCardSelectorService } from '@central/property/components/property-card-selector/property-card-selector.service';

@Component({
  selector: 'central-single-room-list-page',
  template: `
    <div class="col-12">
      <app-guest-edit></app-guest-edit>
    </div>
  `
})
export class SingleRoomListPageComponent implements OnInit, OnDestroy {
  constructor( private propertyCardService: PropertyCardSelectorService ) { }

  ngOnInit() {
      this.propertyCardService.disablePropertyCard();
  }

  ngOnDestroy(): void {
    this.propertyCardService.enablePropertyCard();
  }
}
