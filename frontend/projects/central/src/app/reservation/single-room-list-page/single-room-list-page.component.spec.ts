import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleRoomListPageComponent } from './single-room-list-page.component';
import { configureTestSuite } from 'ng-bullet';

describe('SingleRoomListPageComponent', () => {
  let component: SingleRoomListPageComponent;
  let fixture: ComponentFixture<SingleRoomListPageComponent>;


  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleRoomListPageComponent ]
    });
    fixture = TestBed.createComponent(SingleRoomListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
