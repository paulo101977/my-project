import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  PropertyCardSelectorService
} from '@central/property/components/property-card-selector/property-card-selector.service';

@Component({
  selector: 'central-create-edit-page',
  templateUrl: './create-edit-page.component.html',
  styleUrls: ['./create-edit-page.component.css']
})
export class CreateEditPageComponent implements OnInit, OnDestroy {

  constructor( private route: ActivatedRoute,
               private propertyCardService: PropertyCardSelectorService
  ) { }

  ngOnInit() {
    const queryParams = this.route.snapshot.queryParams;
    if ( queryParams.reservation ) {
      this.propertyCardService.disablePropertyCard();
    }
  }

  ngOnDestroy(): void {
    this.propertyCardService.enablePropertyCard();
  }

}
