import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationListPageComponent } from './reservation-list-page.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';

describe('ReservationListPageComponent', () => {
  let component: ReservationListPageComponent;
  let fixture: ComponentFixture<ReservationListPageComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationListPageComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });
    fixture = TestBed.createComponent(ReservationListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
