import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ThxToasterV2Module } from '@inovacao-cmnet/thx-ui';
import { AppComponent } from './app.component';
import {
  AdminApiModule,
  CentralApiModule,
  configureApi,
  configureAppName,
  I18nModule
} from '@inovacaocmnet/thx-bifrost';
import { environment } from '@environment';
import { AppRoutingModule } from '@central/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ThfNotificationModule } from '@totvs/thf-ui';
import { CoreModule } from '@central/core/core.module';
import { BaseContainerComponent } from './base-container/base-container/base-container.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { PropertyModule } from '@central/property/property.module';
import { CentralApiConfig } from '@central/central-api-config';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PermissionsModule } from '@thex/security';

const ApiConfigProvider = configureApi(environment.apiConfig);

@NgModule({
  declarations: [
    AppComponent,
    BaseContainerComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    AppRoutingModule,
    I18nModule.forRoot(),
    HttpClientModule,
    SharedModule,
    ThfNotificationModule,
    RouterModule,
    PropertyModule,
    AdminApiModule,
    CentralApiModule,
    PermissionsModule,
    ThxToasterV2Module
  ],
  exports: [ BaseContainerComponent ],
  bootstrap: [AppComponent],
  providers: [ApiConfigProvider, configureAppName('crs'), CentralApiConfig],
})
export class AppModule { }
