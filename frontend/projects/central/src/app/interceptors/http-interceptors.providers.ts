import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CultureInterceptor, TokenInterceptor } from '@inovacaocmnet/thx-bifrost';
import { ErrorHandlerInterceptor } from 'app/core/interceptors/error-handler.interceptor';
import { RequestedPropertyInterceptor } from '@central/property/interceptor/requested-property.interceptor';

export function addInterceptor(interceptor: any) {
  return {
    provide: HTTP_INTERCEPTORS,
    useClass: interceptor,
    multi: true
  };
}

export const httpInterceptorProviders = [
  addInterceptor(ErrorHandlerInterceptor),
  addInterceptor(CultureInterceptor),
  addInterceptor(TokenInterceptor),
  addInterceptor(RequestedPropertyInterceptor)
];
