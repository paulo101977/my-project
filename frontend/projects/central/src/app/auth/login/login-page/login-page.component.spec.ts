import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPageComponent } from './login-page.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureAppName, configureApi, AdminApiModule, CentralApiModule } from '@inovacaocmnet/thx-bifrost';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from '@environment';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginPageComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateTestingModule,
        AdminApiModule,
        CentralApiModule
      ],
      declarations: [ LoginPageComponent ],
      providers: [configureAppName('crs'), configureApi(environment.apiConfig)],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
