import {Component, Inject, OnInit} from '@angular/core';
import { AuthService, ProductEnum, APP_NAME } from '@inovacaocmnet/thx-bifrost';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToasterEmitService } from 'app/shared/services/shared/toaster-emit.service';
import { SuccessError } from 'app/shared/models/success-error-enum';
import { environment } from '@environment';

@Component({
  selector: 'central-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  public appVersion: string;
  public readonly versionName = 'RIO';

  constructor(
    @Inject(APP_NAME) public appName,
    private authService: AuthService,
    private router: Router,
    private toastService: ToasterEmitService,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.appVersion = environment.appVersion;
  }


  onResetPassword(userEmail) {
    this.authService.recoverPassword(userEmail).subscribe(() => this.toastForgotMessage(userEmail));
  }

  toastForgotMessage(userEmail) {
    const forgotPassMessage = this.translateService.instant('variable.sentEmail', { email: userEmail} );
    this.toastService.emitChange(SuccessError.success, forgotPassMessage);
  }

  onForm(userInfo) {
    const { email, password } = userInfo;
    const productId = ProductEnum.Central;


    this.authService.authenticate(this.appName , {login: email, password, productId})
      .subscribe( () => this.router.navigate(['central', 'availability']),
        error => this.toastError(error));

  }

  public toastError(error) {
    const errorMessage = this.authService.extractErrorMessage(error);
    this.toastService.emitChange(SuccessError.error, errorMessage );
  }
}
