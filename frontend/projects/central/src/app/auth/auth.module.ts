import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginPageComponent } from './login/login-page/login-page.component';
import {
  configureRecaptcha,
  LoginModule,
  ThxFormLoginModule,
  ThxImgModule,
  ThxToasterV2Module
} from '@inovacao-cmnet/thx-ui';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'app/shared/shared.module';
import { CoreModule } from '@central/core/core.module';

const RecaptchaConfiguration = configureRecaptcha({
  sitekey: '6Lc_K44UAAAAAJ07K1ygtZT3BrmNIvzA73QdjmOr'
});


@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    ThxFormLoginModule,
    ThxImgModule,
    TranslateModule,
    SharedModule,
    CoreModule,
    LoginModule,
    ThxToasterV2Module
  ],
  declarations: [LoginPageComponent],
  providers: [RecaptchaConfiguration]
})
export class AuthModule { }
