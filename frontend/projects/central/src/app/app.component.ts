import { Component, OnInit } from '@angular/core';
import { I18nService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'central-root',
  template: `<router-outlet></router-outlet>`,
})

export class AppComponent implements OnInit {
  constructor(private i18nService: I18nService) {
    this.i18nService.setInitialLanguage();
  }

  ngOnInit(): void {}
}
