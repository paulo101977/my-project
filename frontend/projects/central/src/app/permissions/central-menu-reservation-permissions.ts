export const CentralMenuReservationPermissions = {
  CENTRAL_Menu_Reserve: {
    img: 'reserva_menu.svg'
  },
  CENTRAL_Menu_Consult_Reservation: {
    routerLink: 'central/reservation/list',
  },
  CENTRAL_Menu_New_Booking: {
    routerLink: 'central/reservation/create'
  },
  CENTRAL_Menu_Availability: {
    routerLink: 'central/availability'
  },
  CENTRAL_Menu_Tariff_Suggestion: {
    routerLink: 'central/reservation/rate-proposal'
  }
};
