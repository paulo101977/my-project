import { CentralMenuReservationPermissions } from '@central/permissions/central-menu-reservation-permissions';

export const CentralMenuPermissions = {
    ...CentralMenuReservationPermissions
};
