import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PropertyStorageService } from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: 'root'
})
export class RequestedPropertyInterceptor implements HttpInterceptor {

  constructor(private propertyStorage: PropertyStorageService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const propertyId = this.propertyStorage.getCurrentPropertyId();

    if (req.params.has('requestedPropertyId')) {
      req.params.delete('requestedPropertyId');
    }
    req = req.clone({
      params: req.params.append('requestedPropertyId', `${propertyId}`)
    });

    return next.handle(req);
  }
}
