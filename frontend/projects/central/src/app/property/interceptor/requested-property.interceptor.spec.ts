import { TestBed, inject } from '@angular/core/testing';

import { RequestedPropertyInterceptor } from './requested-property.interceptor';

describe('RequestedPropertyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequestedPropertyInterceptor]
    });
  });

  it('should be created', inject([RequestedPropertyInterceptor], (service: RequestedPropertyInterceptor) => {
    expect(service).toBeTruthy();
  }));
});
