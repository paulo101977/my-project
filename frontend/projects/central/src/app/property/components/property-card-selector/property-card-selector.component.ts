import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Property } from 'app/shared/models/property';
import { PropertyResource } from '@central/property/resources/property.resource';
import { Location } from 'app/shared/models/location';
import { SelectObjectOption } from 'app/shared/models/selectModel';
import { CommomService } from 'app/shared/services/shared/commom.service';
import { APP_NAME, PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { PropertyCardSelectorService } from '@central/property/components/property-card-selector/property-card-selector.service';
import { SessionParameterService } from 'app/shared/services/parameter/session-parameter.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'central-property-card-selector',
  templateUrl: './property-card-selector.component.html',
  styleUrls: ['./property-card-selector.component.css']
})
export class PropertyCardSelectorComponent implements OnInit, OnDestroy {

  propertyList: Array<Property>;
  propertyOptionList: Array<SelectObjectOption>;
  propertySelected: Property;
  propertyLocation: Location;
  initialPropertyId: number;
  propertySubscription: Subscription;

  constructor(
    @Inject(APP_NAME) private appName: string,
    private propertyResource: PropertyResource,
    private commomService: CommomService,
    private propertyStorage: PropertyStorageService,
    public propertyCardService: PropertyCardSelectorService,
    private parameterSession: SessionParameterService,
  ) {}

  ngOnInit() {
    this.getPropertyList();
    this.propertySubscription = this.propertyStorage
      .currentPropertyChangeEmitted$.subscribe(property => {
      if (property) {
        this.initialPropertyId = property.id;
        this.selectOption(property.id);
      }
    });
  }

  public getPropertyList() {
    this.propertyResource.getAllProperties().subscribe( result => {
      const list = result.items;
      this.propertyList = list;
      this.propertyStorage.savePropertyList(this.propertyList, this.appName);
      this.propertyOptionList = this.commomService.toOption(list, 'id', 'name');
      if (list && list.length) {
        let currentPropertyId = this.propertyStorage.getCurrentPropertyId();
        if ( !currentPropertyId ) {
          currentPropertyId = list[0].id;
        }
        this.initialPropertyId =  currentPropertyId;
      }
    });
  }

  public getPropertyInitials() {
    return this.propertyCardService.getPropertyInitials(this.propertySelected);
  }

  public selectOption(opt) {
    if (opt) {
      this.propertySelected = this.propertyStorage.setCurrentPropertyFromPropertyList(opt, this.appName);
      this.propertyLocation = this.propertySelected.locationList[0] || null;
      this.parameterSession.populateSessionParameters(this.propertySelected.id);
    }
  }

  ngOnDestroy(): void {
    if (this.propertySubscription) {
      this.propertySubscription.unsubscribe();
    }
  }
}
