import { TestBed, inject } from '@angular/core/testing';

import { PropertyCardSelectorService } from './property-card-selector.service';
import { Property } from 'app/shared/models/property';

describe('PropertyCardSelectorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PropertyCardSelectorService]
    });
  });

  it('should return initials', inject([PropertyCardSelectorService], (service: PropertyCardSelectorService) => {
    const property = { name: 'Hotel do Elton Coelho'};
    const initials = service.getPropertyInitials(<Property>property);

    expect(initials).toEqual('HdEC');
  }));

  describe('#card visibility', () => {
    it('should set visible true' , inject([PropertyCardSelectorService], (service: PropertyCardSelectorService) => {
      service.showPropertyCard();
      expect(service['propertyCardVisible']).toBeTruthy();
    }));

    it('should set visible false' , inject([PropertyCardSelectorService], (service: PropertyCardSelectorService) => {
      service.hidePropertyCard();
      expect(service['propertyCardVisible']).toBeFalsy();
    }));

    it('should get visibility true' , inject([PropertyCardSelectorService], (service: PropertyCardSelectorService) => {
      service['propertyCardVisible'] = true;

      expect(service.propertyCardVisibility()).toBeTruthy();
    }));

    it('should get visibility false' , inject([PropertyCardSelectorService], (service: PropertyCardSelectorService) => {
      service['propertyCardVisible'] = false;

      expect(service.propertyCardVisibility()).toBeFalsy();
    }));
  });
});
