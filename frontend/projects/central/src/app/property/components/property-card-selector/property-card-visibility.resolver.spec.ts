import { TestBed, inject } from '@angular/core/testing';

import { PropertyCardVisibilityResolver } from './property-card-visibility.resolver';

describe('PropertyCardVisibility.ResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PropertyCardVisibilityResolver]
    });
  });

  it('should be created', inject([PropertyCardVisibilityResolver], (service: PropertyCardVisibilityResolver) => {
    expect(service).toBeTruthy();
  }));
});
