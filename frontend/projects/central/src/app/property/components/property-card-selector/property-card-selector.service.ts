import { Injectable } from '@angular/core';
import { Property } from 'app/shared/models/property';

@Injectable({
  providedIn: 'root'
})
export class PropertyCardSelectorService {
  private propertyCardVisible = true;
  private propertyCardDisable = false;

  constructor() { }

  public getPropertyInitials( property: Property) {
    if ( property ) {
      const nameArray = property.name.split(' ');
      return nameArray.reduce( (prev, current) => `${prev}${current[0]}` , '');
    }
    return '';
  }

  // Card Enable
  public isPropertyCardDisabled(): boolean {
    return this.propertyCardDisable;
  }

  public enablePropertyCard() {
    this.propertyCardDisable = false;
  }

  public disablePropertyCard() {
    this.propertyCardDisable = true;
  }

  // Card visibility
  public propertyCardVisibility(): boolean {
    return this.propertyCardVisible;
  }

  public showPropertyCard() {
    this.propertyCardVisible = true;
  }

  public hidePropertyCard() {
    this.propertyCardVisible = false;
  }
}
