import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { PropertyCardSelectorService } from '@central/property/components/property-card-selector/property-card-selector.service';

@Injectable({ providedIn: 'root'})
export class PropertyCardVisibilityResolver implements Resolve<any> {

  /**
   * Add here URls that don't require propertySelectorCard
   */
  private readonly routesNoCard = [
    'availability',
  ];

  constructor( private propertyCardService: PropertyCardSelectorService ) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    this.propertyCardService.showPropertyCard();
    state.url.split('/').forEach( item => {
      if (this.routesNoCard.find(i => i == item)) {
        this.propertyCardService.hidePropertyCard();
      }
    });
  }
}
