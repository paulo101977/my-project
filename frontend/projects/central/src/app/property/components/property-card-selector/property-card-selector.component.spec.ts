import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { PropertyCardSelectorComponent } from './property-card-selector.component';
import { Property } from 'app/shared/models/property';
import { CentralApiModule, PropertyStorageService } from '@inovacaocmnet/thx-bifrost';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'app/shared/shared.module';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { configureTestSuite } from 'ng-bullet';

describe('PropertyCardSelectorComponent', () => {
  let component: PropertyCardSelectorComponent;
  let fixture: ComponentFixture<PropertyCardSelectorComponent>;

  const propertyList: Property[] = <Property[]>[
    { id: 1, name: 'Hotel 1', locationList: [ {streetName: 'Algum'}]},
    {id: 2, name: 'Hotel 2', locationList: [ {streetName: 'Algum 2'}]}];


  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateTestingModule, SharedModule, CentralApiModule, RouterTestingModule, HttpClientTestingModule],
      providers: [  PropertyStorageService],
      declarations: [ PropertyCardSelectorComponent ]
    });
    fixture = TestBed.createComponent(PropertyCardSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    spyOn(component['propertyResource'], 'getAllProperties').and.returnValue( of({ items: propertyList} ) );
    spyOn(component['parameterSession'], 'populateSessionParameters').and.callFake( () => {} );
    spyOn(component['propertyStorage'], 'setCurrentProperty').and.callFake( () => {} );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should getPropertyListon init', () => {
    spyOn(component, 'getPropertyList');
    component.ngOnInit();
    expect(component.getPropertyList).toHaveBeenCalled();
  });

  it('should getInitials', () => {
    spyOn(component['propertyCardService'], 'getPropertyInitials').and.callFake(() => {});

    component.propertySelected = new Property();
    component.getPropertyInitials();

    expect(component['propertyCardService'].getPropertyInitials).toHaveBeenCalledWith(component.propertySelected);
  });

  describe('#selectOption', () => {
    beforeEach( () => {
      component.propertyList = propertyList;
    });

    it('should set propertySelected and propertyLocation', () => {
      component.selectOption(1);

      expect(component.propertySelected).toBe(component.propertyList[0]);
      expect(component.propertyLocation).toBe(component.propertyList[0].locationList[0]);
    });

    it('should set currentProperty', () => {
      component.selectOption(1);

      expect(component['propertyStorage'].setCurrentProperty).toHaveBeenCalledWith(component.propertyList[0]);
    });
  });

  describe('#getProperty', () => {

    it('should call getAllProperties', () => {
      component.getPropertyList();

      expect(component['propertyResource'].getAllProperties).toHaveBeenCalled();
    });


    it('should set info after get properties', fakeAsync(() => {
      spyOn(component['commomService'], 'toOption').and.returnValue(propertyList);
      component.getPropertyList();
      tick();

      expect(component.propertyList).toBe(propertyList);
      expect(component['commomService'].toOption).toHaveBeenCalledWith(propertyList, 'id', 'name');
      expect(component.initialPropertyId).toEqual(1);
    }));
  });


});
