import { Injectable } from '@angular/core';
import { CentralApiService } from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';
import { Property } from 'app/shared/models/property';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';

@Injectable({ providedIn: 'root' })
export class PropertyResource {

  constructor( private centralApi: CentralApiService) { }

  public getAllProperties(): Observable<ItemsResponse<Property>> {
    return this.centralApi.get('Properties');
  }

}
