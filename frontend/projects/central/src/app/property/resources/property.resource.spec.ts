import { inject, TestBed } from '@angular/core/testing';

import { PropertyResource } from './property.resource';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CentralApiModule, configureApi, ApiEnvironment, configureAppName } from '@inovacaocmnet/thx-bifrost';

describe('PropertyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ CentralApiModule,
        HttpClientTestingModule,
      ],
      providers: [
        PropertyResource,
        configureApi({
          environment: ApiEnvironment.Development
        }),
        configureAppName('crs')
      ]
    });
  });

  it('should be created', inject([PropertyResource], (service: PropertyResource) => {
    expect(service).toBeTruthy();
  }));
});
