import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertyCardSelectorComponent } from '@central/property/components/property-card-selector/property-card-selector.component';
import { CoreModule } from '@central/core/core.module';
import { CentralApiModule } from '@inovacaocmnet/thx-bifrost';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    CentralApiModule,
    SharedModule
  ],
  declarations: [
    PropertyCardSelectorComponent
  ],
  exports: [
    PropertyCardSelectorComponent
  ]
})
export class PropertyModule { }
