import { RmsPremiseModule } from './premise/rms-premise.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    RmsPremiseModule
  ],
  declarations: [  ],
  exports: [
    RmsPremiseModule
  ]
})
export class RMSModule { }
