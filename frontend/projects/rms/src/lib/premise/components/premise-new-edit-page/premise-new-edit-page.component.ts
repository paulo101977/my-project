import { Component, OnInit } from '@angular/core';
import { RoomTypeParametersConfiguration, Parameter } from '@inovacao-cmnet/thx-ui';
import { PropertyPremise } from '../../models/PropertyPremise';
// import { PropertyPremiseHeader } from '../../models/PropertyPremiseHeader'; // TODO: inserir depois

@Component({
  selector: 'rms-premise-new-edit-page',
  templateUrl: './premise-new-edit-page.component.html'
})
export class PremiseNewEditPageComponent implements OnInit {

  public propertyParameterList: Array<Parameter>;
  public roomTypeWithBaseRateList: Array<RoomTypeParametersConfiguration>;

  public mokItemTable: Array<PropertyPremise> = [
    {
      Id: '1',
      PropertyBaseRateHistoryId: '',
      PropertyId: 1,
      RoomTypeId: 101,
      Pax1: 50,
      Pax2: 100,
      Pax3: 150,
      Pax4: '',
      Pax5: '',
      Pax6: '',
      Pax7: '',
      Pax8: '',
      Pax9: '',
      Pax10: '',
      Pax1Operator: '+',
      Pax2Operator: '+',
      Pax3Operator: '+',
      Pax4Operator: '',
      Pax5Operator: '',
      Pax6Operator:  '',
      Pax7Operator: '',
      Pax8Operator: '',
      Pax9Operator:  '',
      Pax10Operator: '',
      PaxAdditional: '',
      PaxAdditionalOperator: '',
      Child1: 50,
      Child2: '',
      Child3: '',
      Child1Operator: '+',
      Child2Operator: '',
      Child3Operator: '',
      PropertyPremiseHeaderId: ''
    }
  ];


  constructor() { }

  ngOnInit() {

  }

  public setRoomTypeWithBaseRateUpdatedList(configRoomTypeWithBaseRateValidate?: any) {
    // if (this.baseRateService.hasBaseRateConfigWithValue(configRoomTypeWithBaseRateValidate.roomTypeList)) {
    //   this.baseRateTableList = this.baseRateMapper.getBaseRateTableListModel(configRoomTypeWithBaseRateValidate.roomTypeList) || [];
    //   this.propertyRate.baseRateConfigurationRoomTypeList = [...this.baseRateTableList];
    //   this.roomTypeWithBaseRateListValid = configRoomTypeWithBaseRateValidate.validBaseRate;
    // }
    // this.checkPropertyRate();
  }

}
