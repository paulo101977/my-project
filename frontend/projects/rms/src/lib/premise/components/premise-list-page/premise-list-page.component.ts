import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfigHeaderPageNew } from '@inovacao-cmnet/thx-ui';

@Component({
  selector: 'rms-premise-list-page',
  templateUrl: './premise-list-page.component.html',
  styleUrls: ['./premise-list-page.component.css']
})
export class PremiseListPageComponent implements OnInit {

  // @output() busca elastica
  // @output() toggleStatus

  @Input() premisesArray: any;
  @Output() rowClick = new EventEmitter<any>();
  @Output() newPremise = new EventEmitter<any>();
  @Output() searchEvent = new EventEmitter<any>();

  public configHeaderPage: ConfigHeaderPageNew;
  public moneySymbol: string;
  public columns: any;
  public moneySymbolArray: Array<any>;
  public roomType: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    this.setColumns();
    this.setHeaderConfig();
  }

  public setColumns() {
    this.columns = [{
      name: 'label.namePremise',
      prop: 'name'
    },
    {
      name: 'label.validityPeriod',
      prop: 'period'
    },
    {
      name: 'label.roomType',
      prop: 'roomTypeReferenceName'
    },
    {
      name: 'label.qtdPax',
      prop: 'paxReference'
    },
    {
      name: 'label.currency',
      prop: 'currencyName'
    }
    ];
  }

  private setHeaderConfig(): void {
    this.configHeaderPage = <ConfigHeaderPageNew>{
      hasBackPreviewPage: false,
      keepTitleButton: true,
      callBackFunction: this.gotoNewPremise
    };
  }

  public gotoNewPremise = () => {
    this.newPremise.emit();
  }

  public gotoEdit = (row) => {
    this.rowClick.emit(row);
  }

  public search(event) {
    console.log('busca', event);
    this.searchEvent.emit();
  }
}
