export class PropertyPremiseHeader {
    id: string;
    propertyId: number;
    initialDate: any;
    finalDate: any;
    currencyId: string;
    isActive: boolean;
    name: string;
    paxReference: number;
    roomTypeReferenceId: number;
    roomTypeReferenceName: string;
}
