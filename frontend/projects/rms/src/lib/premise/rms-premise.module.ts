import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThfFieldModule } from '@totvs/thf-ui';
import {
  ThxDatatableModule,
  HeaderPageNewModule,
  HeaderTitleModule,
  TextButtonModule,
  SearchBarModule,
  ThxIconButtonModule,
  BaseRateTableModule,
  ErrorMsgMaxMinModule,
  ImgCdnModule
} from '@inovacao-cmnet/thx-ui';
import { TranslateModule } from '@ngx-translate/core';

import { PremiseListPageComponent } from './components/premise-list-page/premise-list-page.component';
import { PremiseNewEditPageComponent } from './components/premise-new-edit-page/premise-new-edit-page.component';
import { FormsModule } from '@angular/forms';
import { CurrencyMaskModule } from 'ng2-currency-mask';

@NgModule({
  imports: [
    CommonModule,
    ThfFieldModule,
    FormsModule,
    ThxDatatableModule,
    TranslateModule.forChild(),
    CurrencyMaskModule,
    HeaderPageNewModule,
    HeaderTitleModule,
    TextButtonModule,
    SearchBarModule,
    ThxIconButtonModule,
    BaseRateTableModule,
    ErrorMsgMaxMinModule,
    ImgCdnModule
  ],
  declarations: [
    PremiseListPageComponent,
    PremiseNewEditPageComponent
  ],
  exports: [
    PremiseListPageComponent,
    PremiseNewEditPageComponent
  ]
})
export class RmsPremiseModule { }
