import { RolePermission } from './role-permission';
import { RoleTranslation } from './role-translation';

export interface RoleRequest {
  name: string;
  isActive: boolean;
  identityProductId: number;
  rolePermissionList?: RolePermission[];
  // TODO make it required when there are options
  translationList?: RoleTranslation[];
}
