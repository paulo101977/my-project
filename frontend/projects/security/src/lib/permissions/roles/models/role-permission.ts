export interface RolePermission {
  identityPermissionId: string;
  // TODO make it required when it exists in form
  isActive?: boolean;
}
