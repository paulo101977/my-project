export interface RoleTranslation {
  languageIsoCode: string;
  name: string;
  description?: string;
}
