import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { RoleRequest } from './models/role-request';
import { RoleResourceService } from './role-resource.service';
import { AdminApiService, ListQueryParams } from '@inovacaocmnet/thx-bifrost';

describe('RoleResourceService', () => {
  let service: RoleResourceService;
  let adminApiService: AdminApiService;

  const adminApiServiceStub: Partial<AdminApiService> = {
    get<T>(path: any, options?: any): Observable<T> { return of(null); },
    post<T>(path: any, body?: any, options?: any): Observable<T> { return of(null); },
    put<T>(path: any, body?: any, options?: any): Observable<T> { return of(null); }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RoleResourceService,
        {
          provide: AdminApiService,
          useValue: adminApiServiceStub
        }
      ]
    });

    service = TestBed.get(RoleResourceService);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call AdminApiService#get', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(adminApiService, 'get');

      service.list(mock);
      expect(adminApiService.get).toHaveBeenCalledWith('Role', { params: mock });
    });
  });

  describe('#listAll', () => {
    it('should call #list with defined params', () => {
      const searchData = 'search';
      const calledWith: ListQueryParams = {Page: 1, PageSize: 999, SearchData: searchData};
      spyOn(service, 'list');

      service.listAll(searchData);
      expect(service.list).toHaveBeenCalledWith(calledWith);
    });
  });

  describe('#create', () => {
    it('should call AdminApiService#post', () => {
      const mock: RoleRequest = {
        name: '',
        isActive: true,
        identityProductId: 1,
        translationList: [],
        rolePermissionList: []
      };
      spyOn(adminApiService, 'post');

      service.create(mock);
      expect(adminApiService.post).toHaveBeenCalledWith('Role', mock);
    });
  });

  describe('#getById', () => {
    it('should call AdminApiService#getById', () => {
      const id = '1';
      spyOn(adminApiService, 'get');

      service.getById(id);
      expect(adminApiService.get).toHaveBeenCalledWith(`Role/${id}`, { params: { id } });
    });
  });

  describe('#update', () => {
    it('should call AdminApiService#put', () => {
      const id = '1';
      const mock: RoleRequest = {
        name: '',
        isActive: true,
        identityProductId: 1,
        translationList: [],
        rolePermissionList: []
      };
      spyOn(adminApiService, 'put');

      service.update(id, mock);
      expect(adminApiService.put).toHaveBeenCalledWith(`Role/${id}`, mock);
    });
  });
});
