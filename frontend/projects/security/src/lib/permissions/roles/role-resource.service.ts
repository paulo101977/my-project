import { Injectable } from '@angular/core';
import { RoleRequest } from './models/role-request';
import { AdminApiService, ListQueryParams } from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: 'root'
})
export class RoleResourceService {

  constructor(private adminApi: AdminApiService) { }

  public getById(id: string) {
    return this.adminApi.get(`Role/${id}`, { params: { id }});
  }

  public list(params: ListQueryParams) {
    return this.adminApi.get('Role', { params });
  }

  public listAll(search: string = '') {
    const listAllParams: ListQueryParams = {
      SearchData: search,
      Page: 1,
      PageSize: 999
    };
    return this.list(listAllParams);
  }

  public create(params: RoleRequest) {
    return this.adminApi.post('Role', params);
  }

  public update(id: string, params: RoleRequest) {
    return this.adminApi.put(`Role/${id}`, params);
  }
}
