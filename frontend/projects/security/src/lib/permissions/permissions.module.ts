import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminApiModule } from '@inovacaocmnet/thx-bifrost';

@NgModule({
  imports: [
    CommonModule,
    AdminApiModule
  ],
  declarations: []
})
export class PermissionsModule { }
