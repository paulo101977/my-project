import { Injectable } from '@angular/core';
import { map, switchMap, tap } from 'rxjs/operators';
import { iif, Observable, of } from 'rxjs';
import {
  ApplicationStorageBaseService,
  ProductEnum,
  TokenManagerService } from '@inovacaocmnet/thx-bifrost';
import { UserPermissionApiService } from './user-permission-api.service';
import { UserPermission } from './models/user-permission';

@Injectable({
  providedIn: 'root'
})
export class UserPermissionService {


  constructor( private tokenService: TokenManagerService,
               private userPermissionApi: UserPermissionApiService,
               private appStorage: ApplicationStorageBaseService) {
  }

  public getUserPermissions(appName: string, productId: ProductEnum, referencePermissionMap: any) {
    const localPermissions$ = this.getUserPermissionsFromLocal(appName);
    const requestPermissions$ = this.getUserPermissionsFromRequest(appName, productId, referencePermissionMap);
    return localPermissions$.pipe(
      switchMap((localPermissions) => iif(
        () => !!localPermissions,
        of(localPermissions),
        requestPermissions$
      ))
    );
  }

  private getLocal(appName: string, key: string) {
    return JSON.parse(localStorage.getItem(this.getStorageKeyName(appName, key)));
  }

  private setLocal(appName: string, key: string, value: any) {
    localStorage.setItem(this.getStorageKeyName(appName, key), JSON.stringify(value));
  }

  private saveMenuList(appName: string) {
    return tap(({menuList}: UserPermission) => this.setLocal(appName, `menuList`, menuList));
  }

  private saveKeyList(appName: string) {
    return tap(({keyList}: UserPermission) => this.setLocal(appName, `keyList`, keyList));
  }

  private getUserPermissionsFromRequest(appName: string,
                                        productId: ProductEnum,
                                        referencePermissionMap: any): Observable<UserPermission> {
    const tokenDecoded = this.tokenService.extractTokenPayload(
      this.tokenService.getCurrentPropertyToken());
    return this.userPermissionApi
      .getPermissions(productId, referencePermissionMap, tokenDecoded ? tokenDecoded.PropertyCountryCode : '')
      .pipe(
        map( ({ menuList, keyList }) => ({
          menuList:  this.getPermissionsAllUsers(menuList),
          keyList
        })),
        this.saveMenuList(appName),
        this.saveKeyList(appName)
      );
  }

  private getUserPermissionsFromLocal(appName: string): Observable<UserPermission | {}> {
    const keyList = this.getLocal(appName, `keyList`);
    const menuList = this.getLocal(appName, `menuList`);

    return iif(() => keyList && menuList, of({ keyList, menuList }), of(null));
  }

  public getStorageKeyName(appName, key) {
    return this.appStorage.getStorageKeyName(appName, key);
  }

  public getPermissionsAllUsers(request) {
    const howTo = {
      img: 'thex-help.svg',
      key: '',
      name: 'text.thexHelp',
      orderNumber: 10,
      routerLink: 'how-to'
    };

    return ([
      ...request,
      howTo
    ]);
  }
}
