import { Injectable } from '@angular/core';
import { forkJoin, from, iif, of, pipe } from 'rxjs';
import { filter, map, switchMap, toArray } from 'rxjs/operators';
import { UserPermission } from './models/user-permission';
import { UserPermissionMenuItem } from './models/user-permission-menu-item';
import { UserPermissionReferenceList } from './models/user-permission-reference-list';
import { UserPermissionResourceService } from './user-permission-resource.service';
import { ProductEnum } from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: 'root'
})
export class UserPermissionApiService {

  constructor(
    private resource: UserPermissionResourceService
  ) { }

  public getPermissions(productId: ProductEnum,
                        referenceList: UserPermissionReferenceList,
                        countryCode: string) {
    return this.resource.getPermissions(productId)
      .pipe(
        this.transformPermissions(referenceList, countryCode)
      );
  }

  private transformPermissions(referenceList: UserPermissionReferenceList, countryCode: string) {
    return pipe(
      switchMap(({keyList, menuList}: UserPermission) => {
        const keyList$ = of(keyList);
        const menuList$ = this.getMenuList(menuList, referenceList, countryCode);
        return forkJoin(keyList$, menuList$);
      }),
      map(([keyList, menuList]: [string[], UserPermissionMenuItem[]]) => ({keyList, menuList}))
    );
  }

  private transformMenuRecursively(menuList: UserPermissionMenuItem[], mapMenu) {
    const mapChildren = pipe(
      switchMap((menuItem: UserPermissionMenuItem) => forkJoin(
        of(menuItem),
        this.transformMenuRecursively(menuItem.children, mapMenu)
      )),
      map(([menuItem, children]: [UserPermissionMenuItem, UserPermissionMenuItem[]]) => {
        menuItem.children = children;
        return menuItem;
      })
    );

    return from(menuList).pipe(
      mapMenu,
      this.mapIifChildren(mapChildren),
      toArray()
    );
  }

  private mapIifChildren(operator) {
    return switchMap((menuItem: UserPermissionMenuItem) => {
      const menuItem$ = of(menuItem);
      return iif(
        () => !!(menuItem.children && menuItem.children.length),
        menuItem$.pipe(operator),
        menuItem$
      );
    });
  }

  private getMenuList(menuList: UserPermissionMenuItem[],
                      referenceList: UserPermissionReferenceList,
                      countryCode: string) {
    const mapMenu = pipe(
      filter((item: UserPermissionMenuItem) => {
        const referenceItem = referenceList[item.key];
        return !!referenceItem && referenceItem.countries
          ? referenceItem.countries.includes(countryCode)
          : !!referenceItem;
      }),
      map((item: UserPermissionMenuItem) => ({
        ...item,
        ...referenceList[item.key]
      }))
    );

    return this.transformMenuRecursively(menuList, mapMenu);
  }
}
