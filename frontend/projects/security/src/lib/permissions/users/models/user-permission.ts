import { UserPermissionMenuItem } from './user-permission-menu-item';

export interface UserPermission {
  keyList: string[];
  menuList: UserPermissionMenuItem[];
}
