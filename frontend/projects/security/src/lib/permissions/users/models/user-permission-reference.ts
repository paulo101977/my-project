export interface UserPermissionReference {
  img?: string;
  routerLink?: string;
  countries?: string[];
}
