export interface UserPermissionMenuItem {
  orderNumber: number;
  name: string;
  key: string;
  children?: UserPermissionMenuItem[];
  img?: string;
  routerLink?: string;
}
