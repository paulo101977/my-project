import { UserPermissionReference } from './user-permission-reference';

export interface UserPermissionReferenceList {
  [key: string]: UserPermissionReference;
}
