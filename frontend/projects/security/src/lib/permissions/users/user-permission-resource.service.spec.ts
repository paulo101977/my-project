import { TestBed, inject } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing/create-service-stub';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';

import { UserPermissionResourceService } from './user-permission-resource.service';
import { ProductEnum } from '../../../../../bifrost/src/lib/http/resources';

describe('UserPermissionResourceService', () => {
  let service: UserPermissionResourceService;
  let adminApi: AdminApiService;

  const AdminApiServiceStub = createServiceStub<AdminApiService>(AdminApiService, {
    get<T>(path, options?): Observable<T> { return of(null); }
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserPermissionResourceService,
        AdminApiServiceStub
      ]
    });

    service = TestBed.get(UserPermissionResourceService);
    adminApi = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getPermissions', () => {
    it('should call AdminService#get', () => {
      const productId = ProductEnum.PMS;
      const url = `UserPermission/productId/${productId}/menu/true`;

      spyOn(adminApi, 'get');
      service.getPermissions(productId);

      expect(adminApi.get).toHaveBeenCalledWith(url);
    });
  });
});
