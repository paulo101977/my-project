import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing/create-service-stub';
import { UserPermission } from './models/user-permission';
import { UserPermissionReferenceList } from './models/user-permission-reference-list';
import { UserPermissionApiService } from './user-permission-api.service';
import { UserPermissionResourceService } from './user-permission-resource.service';
import { ProductEnum } from '@inovacaocmnet/thx-bifrost';

describe('UserPermissionApiService', () => {
  let service: UserPermissionApiService;
  let resource: UserPermissionResourceService;

  const UserPermissionResourceServiceStub = createServiceStub<UserPermissionResourceService>(
    UserPermissionResourceService,
    {
      getPermissions(productId: ProductEnum): Observable<UserPermission> {
        return null;
      }
    }
  );

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserPermissionApiService,
        UserPermissionResourceServiceStub
      ]
    });

    service = TestBed.get(UserPermissionApiService);
    resource = TestBed.get(UserPermissionResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getPermissions', () => {
    it('should call UserPermissionResourceService#getPermissions', () => {
      const productId = ProductEnum.PMS;
      const referenceList: UserPermissionReferenceList = {};
      const response: UserPermission = {
        keyList: [],
        menuList: []
      };

      spyOn(resource, 'getPermissions').and.returnValue(of(response));

      service.getPermissions(productId, referenceList, '');

      expect(resource.getPermissions).toHaveBeenCalledWith(productId);
    });

    it('should change the menuList item when ID does match the referenceList', () => {
      const productId = ProductEnum.PMS;
      const referenceList: UserPermissionReferenceList = {
        '123': {
          routerLink: 'yes'
        }
      };
      const response: UserPermission = {
        keyList: [],
        menuList: [
          {
            orderNumber: 1,
            key: '123',
            name: 'this name'
          }
        ]
      };

      spyOn(resource, 'getPermissions').and.returnValue(of(response));

      let returned: UserPermission;
      service.getPermissions(productId, referenceList, '')
        .subscribe((userPermission: UserPermission) => {
          returned = userPermission;
        });

      expect(resource.getPermissions).toHaveBeenCalledWith(productId);
      expect(returned.keyList.length).toEqual(response.keyList.length);
      expect(returned.menuList.length).toEqual(response.menuList.length);
      expect(returned.menuList[0]).toEqual(jasmine.objectContaining({
        routerLink: 'yes'
      }));
    });

    it('shouldn\'t change the menuList item when ID doesn\'t match the referenceList', () => {
      const productId = ProductEnum.PMS;
      const referenceList: UserPermissionReferenceList = {
        '456': {
          routerLink: 'yes'
        }
      };
      const response: UserPermission = {
        keyList: [],
        menuList: [
          {
            orderNumber: 1,
            key: '123',
            name: 'this name'
          }
        ]
      };

      spyOn(resource, 'getPermissions').and.returnValue(of(response));

      let returned: UserPermission;
      service.getPermissions(productId, referenceList, '')
        .subscribe((userPermission: UserPermission) => {
          returned = userPermission;
        });

      expect(resource.getPermissions).toHaveBeenCalledWith(productId);
      expect(returned.keyList.length).toEqual(response.keyList.length);
      expect(returned.menuList.length).toEqual(0);
    });

    it('shouldn\'t merge a menuItem with country restriction mismatch', () => {
      const productId = ProductEnum.PMS;
      const referenceList: UserPermissionReferenceList = {
        '456': {
          routerLink: 'yes',
          countries: ['PT-BR']
        },
        '999': {
          routerLink: 'no',
          countries: ['FR'],
        },
        '111': {
          routerLink: 'maybe'
        }
      };
      const response: UserPermission = {
        keyList: [],
        menuList: [
          {
            orderNumber: 1,
            key: '123',
            name: 'this name'
          },
          { key: '999', name: 'name', orderNumber: 2 },
          { key: '111', name: 'eman', orderNumber: 3 },
          { key: '456', name: 'nema', orderNumber: 4 },
        ]
      };

      spyOn(resource, 'getPermissions').and.returnValue(of(response));

      let returned: UserPermission;
      service.getPermissions(productId, referenceList, 'PT-BR')
        .subscribe((userPermission: UserPermission) => {
          returned = userPermission;
        });

      expect(resource.getPermissions).toHaveBeenCalledWith(productId);
      expect(returned.keyList.length).toEqual(response.keyList.length);
      expect(returned.menuList.length).toEqual(2);
    });

    it('should apply the change recursively on children', () => {
      const productId = ProductEnum.PMS;
      const referenceList: UserPermissionReferenceList = {
        '123': {
          img: 'other'
        },
        '456': {
          img: 'yes'
        },
        '789': {
          routerLink: 'no'
        }
      };
      const response: UserPermission = {
        keyList: [],
        menuList: [
          {
            orderNumber: 1,
            key: '123',
            name: 'this name',
            children: [
              {
                key: '456',
                name: 'that name',
                orderNumber: 2,
                children: [
                  {
                    key: '789',
                    name: 'those names',
                    orderNumber: 3,
                    children: []
                  }
                ]
              }
            ]
          }
        ]
      };

      spyOn(resource, 'getPermissions').and.returnValue(of(response));

      let returned: UserPermission;
      service.getPermissions(productId, referenceList, '')
        .subscribe((userPermission: UserPermission) => {
          returned = userPermission;
        });

      expect(resource.getPermissions).toHaveBeenCalledWith(productId);
      expect(returned.keyList.length).toEqual(response.keyList.length);
      expect(returned.menuList.length).toEqual(response.menuList.length);
      expect(returned.menuList[0].children[0]).toEqual(jasmine.objectContaining({
        img: 'yes',
        children: jasmine.arrayContaining([
          jasmine.objectContaining({
            routerLink: 'no'
          })
        ])
      }));
    });
  });
});
