import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminApiService, ProductEnum } from '@inovacaocmnet/thx-bifrost';
import { UserPermission } from './models/user-permission';

@Injectable({
  providedIn: 'root'
})
export class UserPermissionResourceService {

  constructor(
    private adminApi: AdminApiService
  ) { }

  getPermissions(productId: ProductEnum): Observable<UserPermission> {
    return this.adminApi.get(`UserPermission/productId/${productId}/menu/true`);
  }
}
