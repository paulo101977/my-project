import { TestBed } from '@angular/core/testing';
import { UserPermissionService } from './user-permission.service';
import { createServiceStub } from '../../../../../../testing';
import { Observable, of } from 'rxjs';
import { UserPermissionApiService } from './user-permission-api.service';
import { AdminApiModule, ApiEnvironment, configureApi, ProductEnum } from '@inovacaocmnet/thx-bifrost';
import { UserPermissionMenuItem } from './models/user-permission-menu-item';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { authServiceStub } from 'app/shared/services/auth-service/testing';
import { configureTestSuite } from 'ng-bullet';

describe('UserPermissionService', () => {

  const MENU_LIST = 'pms:menuList';
  const KEY_LIST = 'pms:keyList';
  const storage = {};

  const setLocal = (key: string, value: any) => {
    storage[key] = JSON.stringify(value);
  };
  const local = {
    menuList: 'LOCAL_VALUE',
    keyList: 'LOCAL_ANOTHER_VALUE'
  };

  const request = {
    menuList: 'VALUE',
    keyList: 'ANOTHER_VALUE'
  };

  let service: UserPermissionService;
  let userPermissionApiService: UserPermissionApiService;

  const UserPermissionApiServiceStub = createServiceStub<UserPermissionApiService>(UserPermissionApiService, {
    getPermissions(
      productId: ProductEnum,
      referenceList: any,
      countryCode: string
    ): Observable<{ keyList: string[]; menuList: UserPermissionMenuItem[] }> { return null; }
  });


  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [AdminApiModule, HttpClientTestingModule],
      providers: [
        UserPermissionService,
        UserPermissionApiServiceStub,
        authServiceStub,
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });
  });

  beforeEach(() => {

    service = TestBed.get(UserPermissionService);
    userPermissionApiService = TestBed.get(UserPermissionApiService);

    spyOn(localStorage, 'setItem').and.callFake((key, value) => storage[key] = value);
    spyOn(localStorage, 'getItem').and.callFake((key) => storage[key] || null );
    spyOn(localStorage, 'removeItem').and.callFake((key) => delete storage[key] );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getUserPermissions', () => {
    afterEach(() => {
      localStorage.removeItem(MENU_LIST);
      localStorage.removeItem(KEY_LIST);
    });

    it('should request the user permissions if keyList isn\'t set', () => {
      setLocal(MENU_LIST, local.menuList);
      spyOn(userPermissionApiService, 'getPermissions').and.returnValue(of(request));

      let permissions;
      service.getUserPermissions('pms', ProductEnum.PMS, {})
        .subscribe((response) => {
          permissions = response;
        });

      expect(permissions).toEqual(request);
      expect(permissions).not.toEqual(local);
    });

    it('should request the user permissions if menuList isn\'t set', () => {
      setLocal(KEY_LIST, local.keyList);
      spyOn(userPermissionApiService, 'getPermissions').and.returnValue(of(request));

      let permissions;
      service.getUserPermissions('pms', ProductEnum.PMS, {})
        .subscribe((response) => {
          permissions = response;
        });

      expect(permissions).toEqual(request);
      expect(permissions).not.toEqual(local);
    });

    it('should get user permissions from local if permissions are set', () => {
      setLocal(MENU_LIST, local.menuList);
      setLocal(KEY_LIST, local.keyList);
      spyOn<any>(userPermissionApiService, 'getPermissions').and.returnValue(of(request));

      let permissions;
      service.getUserPermissions('pms', ProductEnum.PMS, request)
        .subscribe((response) => {
          permissions = response;
        });

      expect(permissions).not.toEqual(request);
      expect(permissions).toEqual(local);
    });
  });

});
