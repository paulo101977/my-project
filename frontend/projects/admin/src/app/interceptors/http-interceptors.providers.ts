import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CultureInterceptor, ErrorInterceptor, TokenInterceptor } from '@inovacaocmnet/thx-bifrost';

export function addInterceptor(interceptor: any) {
  return {
    provide: HTTP_INTERCEPTORS,
    useClass: interceptor,
    multi: true
  };
}

export const httpInterceptorProviders = [
  addInterceptor(TokenInterceptor),
  addInterceptor(ErrorInterceptor),
  addInterceptor(CultureInterceptor),
];
