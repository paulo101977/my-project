import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigUserRoutingModule } from './config-user-routing.module';
import { ConfigUserListComponent } from './components/config-user-list/config-user-list.component';
import { UserModule } from '../../user/user.module';

@NgModule({
  imports: [
    CommonModule,
    ConfigUserRoutingModule,
    UserModule
  ],
  declarations: [ConfigUserListComponent]
})
export class ConfigUserModule { }
