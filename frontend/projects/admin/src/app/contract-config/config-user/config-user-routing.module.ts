import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserFormPageComponent } from '../../user/components/user-form-page/user-form-page.component';
import { ConfigUserListComponent } from './components/config-user-list/config-user-list.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigUserListComponent
  },
  {
    path: 'new',
    component: UserFormPageComponent
  },
  {
    path: 'edit/:id',
    component: UserFormPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigUserRoutingModule { }
