import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigUserListComponent } from './config-user-list.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('ConfigUserListComponent', () => {
  let component: ConfigUserListComponent;
  let fixture: ComponentFixture<ConfigUserListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConfigUserListComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigUserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
