import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrandModule } from '../../brand/brand.module';

import { ConfigBrandRoutingModule } from './config-brand-routing.module';
import { ConfigBrandListComponent } from './components/config-brand-list/config-brand-list.component';

@NgModule({
  imports: [
    CommonModule,
    ConfigBrandRoutingModule,
    BrandModule
  ],
  declarations: [ConfigBrandListComponent]
})
export class ConfigBrandModule { }
