import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigBrandListComponent } from './config-brand-list.component';

describe('ConfigBrandListComponent', () => {
  let component: ConfigBrandListComponent;
  let fixture: ComponentFixture<ConfigBrandListComponent>;

  @Component({selector: 'admin-brand-list-page', template: ''})
  class BrandListPageStubComponent {}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigBrandListComponent, BrandListPageStubComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigBrandListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
