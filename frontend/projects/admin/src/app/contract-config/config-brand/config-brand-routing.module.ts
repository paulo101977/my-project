import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UpdateComponent } from '../../brand/components/update/update.component';
import { ConfigBrandListComponent } from './components/config-brand-list/config-brand-list.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigBrandListComponent
  },
  {
    path: 'edit/:id',
    component: UpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigBrandRoutingModule { }
