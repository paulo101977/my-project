import { ConfigChainModule } from './config-chain.module';

describe('ConfigChainModule', () => {
  let configChainModule: ConfigChainModule;

  beforeEach(() => {
    configChainModule = new ConfigChainModule();
  });

  it('should create an instance', () => {
    expect(configChainModule).toBeTruthy();
  });
});
