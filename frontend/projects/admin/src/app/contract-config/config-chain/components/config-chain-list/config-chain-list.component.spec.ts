import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigChainListComponent } from './config-chain-list.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('ConfigChainListComponent', () => {
  let component: ConfigChainListComponent;
  let fixture: ComponentFixture<ConfigChainListComponent>;

  @Component({ selector: 'admin-chain-list-page', template: ''})
  class ChainListPageStubComponent {}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigChainListComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigChainListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
