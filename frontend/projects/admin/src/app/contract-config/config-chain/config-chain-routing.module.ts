import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigChainListComponent } from './components/config-chain-list/config-chain-list.component';
import { UpdateComponent } from '../../chain/components/update/update.component';

const routes: Routes = [
  {path: '', component: ConfigChainListComponent},
  {path: 'edit/:id', component: UpdateComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigChainRoutingModule {
}
