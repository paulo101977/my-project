import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigChainRoutingModule } from './config-chain-routing.module';
import { ConfigChainListComponent } from './components/config-chain-list/config-chain-list.component';
import { ChainModule } from '../../chain/chain.module';

@NgModule({
  imports: [
    CommonModule,
    ConfigChainRoutingModule,
    ChainModule
  ],
  declarations: [ConfigChainListComponent]
})
export class ConfigChainModule { }
