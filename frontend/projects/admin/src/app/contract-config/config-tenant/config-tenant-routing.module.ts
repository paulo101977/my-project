import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigTenantListComponent } from './components/config-tenant-list/config-tenant-list.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigTenantListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigTenantRoutingModule { }
