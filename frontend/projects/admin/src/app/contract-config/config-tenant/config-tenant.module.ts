import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigTenantRoutingModule } from './config-tenant-routing.module';
import { TenantModule } from '../../tenant/tenant.module';
import { ConfigTenantListComponent } from './components/config-tenant-list/config-tenant-list.component';

@NgModule({
  imports: [
    CommonModule,
    ConfigTenantRoutingModule,
    TenantModule
  ],
  declarations: [ConfigTenantListComponent]
})
export class ConfigTenantModule { }
