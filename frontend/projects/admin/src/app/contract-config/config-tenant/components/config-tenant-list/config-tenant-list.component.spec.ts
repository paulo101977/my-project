import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigTenantListComponent } from './config-tenant-list.component';

describe('ConfigTenantListComponent', () => {
  let component: ConfigTenantListComponent;
  let fixture: ComponentFixture<ConfigTenantListComponent>;

  @Component({selector: 'admin-tenant-list-page', template: ''})
  class TenantListPageStubComponent {}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigTenantListComponent, TenantListPageStubComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigTenantListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
