import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractConfigRoutingModule } from './contract-config-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ContractConfigRoutingModule
  ],
  declarations: []
})
export class ContractConfigModule { }
