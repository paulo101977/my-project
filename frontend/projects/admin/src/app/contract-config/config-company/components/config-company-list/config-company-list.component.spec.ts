import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigCompanyListComponent } from './config-company-list.component';

describe('ConfigCompanyListComponent', () => {
  let component: ConfigCompanyListComponent;
  let fixture: ComponentFixture<ConfigCompanyListComponent>;

  @Component({selector: 'admin-company-list-page', template: ''})
  class CompanyListPageStubComponent {}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigCompanyListComponent, CompanyListPageStubComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
