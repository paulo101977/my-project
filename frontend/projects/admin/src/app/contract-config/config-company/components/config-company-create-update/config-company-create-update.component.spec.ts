import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigCompanyCreateUpdateComponent } from './config-company-create-update.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('ConfigCompanyCreateUpdateComponent', () => {
  let component: ConfigCompanyCreateUpdateComponent;
  let fixture: ComponentFixture<ConfigCompanyCreateUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigCompanyCreateUpdateComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigCompanyCreateUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
