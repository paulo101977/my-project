import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyModule } from '../../company/company.module';

import { ConfigCompanyRoutingModule } from './config-company-routing.module';
import { ConfigCompanyListComponent } from './components/config-company-list/config-company-list.component';
import { ConfigCompanyCreateUpdateComponent } from './components/config-company-create-update/config-company-create-update.component';

@NgModule({
  imports: [
    CommonModule,
    ConfigCompanyRoutingModule,
    CompanyModule
  ],
  declarations: [ConfigCompanyListComponent, ConfigCompanyCreateUpdateComponent]
})
export class ConfigCompanyModule { }
