import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigCompanyListComponent } from './components/config-company-list/config-company-list.component';
import { CompanyCreateUpdateComponent } from '../../company/components/company-create-update/company-create-update.component';


const routes: Routes = [
  {path: '', component: ConfigCompanyListComponent},
  {path: 'new', component: CompanyCreateUpdateComponent},
  {path: 'edit/:id', component: CompanyCreateUpdateComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigCompanyRoutingModule { }
