import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigHotelListComponent } from './config-hotel-list.component';

describe('ConfigHotelListComponent', () => {
  let component: ConfigHotelListComponent;
  let fixture: ComponentFixture<ConfigHotelListComponent>;

  @Component({selector: 'admin-hotel-list-page', template: ''})
  class HotelListPageStubComponent {}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigHotelListComponent, HotelListPageStubComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigHotelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
