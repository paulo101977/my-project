import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HotelFormPageComponent } from '../../hotel/components/hotel-form-page/hotel-form-page.component';
import { ConfigHotelListComponent } from './components/config-hotel-list/config-hotel-list.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigHotelListComponent
  },
  {
    path: 'new',
    component: HotelFormPageComponent
  },
  {
    path: 'edit/:id',
    component: HotelFormPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigHotelRoutingModule { }
