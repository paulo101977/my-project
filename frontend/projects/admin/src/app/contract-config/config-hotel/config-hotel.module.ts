import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HotelModule } from '../../hotel/hotel.module';

import { ConfigHotelRoutingModule } from './config-hotel-routing.module';
import { ConfigHotelListComponent } from './components/config-hotel-list/config-hotel-list.component';

@NgModule({
  imports: [
    CommonModule,
    ConfigHotelRoutingModule,
    HotelModule
  ],
  declarations: [ConfigHotelListComponent]
})
export class ConfigHotelModule { }
