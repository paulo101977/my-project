import { ContractConfigModule } from './contract-config.module';

describe('ContractConfigModule', () => {
  let contractConfigModule: ContractConfigModule;

  beforeEach(() => {
    contractConfigModule = new ContractConfigModule();
  });

  it('should create an instance', () => {
    expect(contractConfigModule).toBeTruthy();
  });
});
