import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'chain',
    loadChildren: './config-chain/config-chain.module#ConfigChainModule'
  },
  {
    path: 'brand',
    loadChildren: './config-brand/config-brand.module#ConfigBrandModule'
  },
  {
    path: 'company',
    loadChildren: './config-company/config-company.module#ConfigCompanyModule'
  },
  {
    path: 'hotel',
    loadChildren: './config-hotel/config-hotel.module#ConfigHotelModule'
  },
  {
    path: 'user',
    loadChildren: './config-user/config-user.module#ConfigUserModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractConfigRoutingModule { }
