import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { createPipeStub } from '../../../../../../../testing/create-pipe-stub';
import { createServiceStub } from '../../../../../../../testing/create-service-stub';
import { DashboardResource } from '../../../dashboard/resources/dashboard.resource';

import { HotelBlockModalComponent } from './hotel-block-modal.component';

describe('HotelBlockModalComponent', () => {
  let component: HotelBlockModalComponent;
  let fixture: ComponentFixture<HotelBlockModalComponent>;

  const TranslatePipeStub = createPipeStub({name: 'translate'});
  const TranslateServiceStub = createServiceStub(TranslateService, {
    instant(key: string | Array<string>, interpolateParams?: Object): string | any { return ''; }
  });

  const DashboardResourceStub = createServiceStub(DashboardResource, {
    toggleblockproperty(id: string): Observable<any> { return of(null); }
  });

  // tslint:disable-next-line
  @Component({ selector: 'thf-modal', template: '' })
  class ThfModalComponent {
    open() {}
    close() {}
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [
        HotelBlockModalComponent,
        TranslatePipeStub,
        ThfModalComponent
      ],
      providers: [
        TranslateServiceStub,
        DashboardResourceStub,
        FormBuilder
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelBlockModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#open', () => {
    it('should open the modal with the selected hotel', () => {
      const hotelName = 'my hotel';
      const hotelId = '5678';
      const hotelIsBlocked = true;

      spyOn(component.modal, 'open');

      expect(component.hotelName).toBeUndefined();
      expect(component.hotelId).toBeUndefined();

      component.open(hotelName, hotelId, hotelIsBlocked);

      expect(component.hotelName).toBe(hotelName);
      expect(component.hotelId).toBe(hotelId);
      expect(component.modal.open).toHaveBeenCalled();
    });
  });

  describe('#close', () => {
    it('should close the modal and reset the values', () => {
      const hotelName = 'my hotel';
      const hotelId = '5678';

      spyOn(component.modal, 'close');

      component.hotelName = hotelName;
      component.hotelId = hotelId;
      component.form.patchValue({
        hotelName: 'mi otel'
      });

      component.close();

      expect(component.hotelName).toBe(null);
      expect(component.hotelId).toBe(null);
      expect(component.form.value.hotelName).toBe(null);
      expect(component.modal.close).toHaveBeenCalled();
    });
  });

  describe('when trying to submit the modal', () => {
    let messages = [];
    let destroy$ = new Subject();

    beforeEach(() => {
      component.toggled
        .pipe(takeUntil(destroy$))
        .subscribe((hasToggled) => {
          messages.push(hasToggled);
        });
    });

    afterEach(() => {
      destroy$.complete();
      messages = [];
      destroy$ = new Subject();
    });

    it('should do nothing if the form is invalid', () => {
      expect(messages.length).toBe(0);

      component.modalSettings.primaryAction.action();

      expect(messages.length).toBe(0);
    });

    it('should toggle the hotel block state', () => {
      const hotelName = 'yoy';
      component.hotelName = hotelName;
      component.form.patchValue({ hotelName });

      expect(messages.length).toBe(0);

      component.modalSettings.primaryAction.action();

      expect(messages.length).toBe(2);
    });
  });
});
