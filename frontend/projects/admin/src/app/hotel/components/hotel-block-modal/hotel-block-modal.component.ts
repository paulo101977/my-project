import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfModalComponent } from '@totvs/thf-ui';
import { DashboardResource } from '../../../dashboard/resources/dashboard.resource';
import { ModalSettings } from '../../../shared/models/modal-settings';

@Component({
  selector: 'admin-hotel-block-modal',
  templateUrl: './hotel-block-modal.component.html',
  styleUrls: ['./hotel-block-modal.component.css']
})
export class HotelBlockModalComponent implements OnInit {
  @ViewChild('modal') modal: ThfModalComponent;
  @Output() public toggled = new EventEmitter<boolean>();
  @Output() public accepted = new EventEmitter<boolean>();
  @Output() public refused = new EventEmitter<boolean>();

  public modalSettings: ModalSettings;
  public form: FormGroup;
  public hotelName: string;
  public hotelId: string;
  public hotelIsBlocked: boolean;
  public i18n: any;

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private dashboardResource: DashboardResource
  ) {
  }

  ngOnInit() {
    this.setI18n();
    this.setFormToggleBlock();
    this.setModalSettings();
  }

  public open(hotelName: string, hotelId: string, hotelIsBlocked: boolean) {
    this.hotelName = hotelName;
    this.hotelId = hotelId;
    this.hotelIsBlocked = hotelIsBlocked;
    this.i18n.currentConfirmMessage = hotelIsBlocked ? this.i18n.alert.confirmUnblock : this.i18n.alert.confirmBlock;
    this.modal.open();
  }

  public close() {
    this.form.reset();
    this.hotelName = null;
    this.hotelId = null;
    this.i18n.currentConfirmMessage = '';
    this.modal.close();
  }

  private setFormToggleBlock() {
    this.form = this.fb.group({
      hotelName: ['', this.matchHotelName()]
    });

    this.form.statusChanges
      .subscribe(values => {
        this.modalSettings.primaryAction.disabled = !this.form.valid;
      });
  }

  private setModalSettings() {
    const primaryAction = () => this.accepted.observers.length ? this.accept() : this.toggleBlock();

    this.modalSettings = {
      title: this.i18n.title,
      primaryAction: {
        action: () => this.form.valid && primaryAction(),
        label: this.i18n.action.confirm,
        disabled: true
      },
      secondaryAction: {
        action: () => this.refuse(),
        label: this.i18n.action.cancel
      },
      size: 'sm'
    };
  }

  private setI18n() {
    this.i18n = {
      title: this.translate.instant('title.areYouSure'),
      action: {
        confirm: this.translate.instant('action.confirmAction'),
        cancel: this.translate.instant('action.cancelAction'),
      },
      alert: {
        confirmBlock: this.translate.instant('alert.confirmActionToBlockHotel'),
        confirmUnblock: this.translate.instant('alert.confirmActionToUnblockHotel'),
      },
      currentConfirmMessage: ''
    };
  }

  private accept() {
    this.accepted.emit(this.hotelIsBlocked);
  }

  private refuse() {
    this.modal.close();
    this.refused.emit(this.hotelIsBlocked);
  }

  private matchHotelName() {
    return (control) => {
      return !(this.hotelName == control.value) ? {'name': {value: 'Invalid'}} : null;
    };
  }

  private toggleBlock() {
    this.toggled.emit(false);
    this.dashboardResource.toggleblockproperty(this.hotelId)
      .subscribe(() => {
        this.toggled.emit(true);
        this.close();
      });
  }
}
