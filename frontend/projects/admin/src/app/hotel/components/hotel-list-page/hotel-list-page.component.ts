import { Location } from '@angular/common';
import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ThfPageAction } from '@totvs/thf-ui';
import { DashboardProperty } from '../../../dashboard/model/dashboard-property';
import { PageSettings } from '../../../shared/models/page-settings';
import { PageTableSettings } from '../../../widgets/page-list-wrapper/models/page-table-settings';
import { HotelApiService } from '../../services/hotel-api.service';
import { HotelBlockModalComponent } from '../hotel-block-modal/hotel-block-modal.component';

@Component({
  selector: 'admin-hotel-list-page',
  templateUrl: './hotel-list-page.component.html',
  styleUrls: ['./hotel-list-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HotelListPageComponent implements OnInit {
  @ViewChild('modal') modal: HotelBlockModalComponent;
  @Input() hasBackAction = false;
  public pageSettings: PageSettings;
  public pageTableSettings: PageTableSettings;
  private i18n: any;
  private currentHotel: DashboardProperty;

  constructor(
    private location: Location,
    private translate: TranslateService,
    private hotelApi: HotelApiService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.setI18n();
    this.setPageSettings();
    this.setPageTableSettings();
  }

  private setPageSettings() {
    this.pageSettings = {
      title: this.i18n.title,
      actions: this.getActions(),
      filterSettings: {
        placeholder: this.i18n.placeholders.search
      }
    };
  }

  private setPageTableSettings() {
    this.pageTableSettings = {
      columns: [
        {
          property: 'blocked',
          label: this.i18n.labels.blocked,
          type: 'icon',
          width: '10%',
          icons: [
            {
              value: 'blocked',
              icon: 'thf-icon-info',
              action: (item) => this.openBlockModal(item),
              color: (item) => this.isBlocked(item)
            }
          ]
        },
        { property: 'name', width: '20%', label: this.i18n.labels.hotel, type: 'string' },
        { property: 'companyName', width: '20%', label: this.i18n.labels.company, type: 'string' },
        { property: 'tenantName', width: '20%', label: this.i18n.labels.tenant, type: 'string' },
        { property: 'chainName', width: '20%', label: this.i18n.labels.chain, type: 'string' },
        {
          property: 'actions',
          label: this.i18n.labels.actions,
          type: 'icon',
          width: '10%',
          icons: [
            { value: 'edit', icon: 'thf-icon-edit', action: (item) => this.goToUpdateHotelPage(item) }
          ]
        }
      ],
      transformData: this.transformData,
      getList: this.getList.bind(this),
    };
  }

  public getList(params) {
    return this.hotelApi.list(params);
  }

  public transformData(response) {
    return {
      ...response,
      items: response.items.map((item) => {
        item.blocked = ['blocked'];
        item.actions = ['edit'];
        return item;
      })
    };
  }

  public onBlockToggle(toggled: boolean) {
    if (toggled) {
      this.currentHotel.isBlocked = !this.currentHotel.isBlocked;
      this.currentHotel = null;
    }
  }

  public onBlockToggleRefusal() {
    this.currentHotel = null;
  }

  private getActions() {
    const backAction: ThfPageAction = {
      label: this.i18n.actions.back,
      icon: 'thf-icon-arrow-left',
      action: () => { this.location.back(); }
    };
    return [
      {
        label: this.i18n.actions.insert,
        icon: 'thf-icon-plus',
        action: () => { this.goToNewHotelPage(); }
      },
      ...(this.hasBackAction ? [backAction] : [])
    ];
  }

  private setI18n() {
    this.i18n = {
      title: this.translate.instant('title.hotel'),
      placeholders: {
        search: this.translate.instant('placeholder.searchHotelCompanyTenantOrBrand')
      },
      labels: {
        blocked: this.translate.instant('label.blocked'),
        hotel: this.translate.instant('label.hotel'),
        company: this.translate.instant('label.company'),
        tenant: this.translate.instant('label.tenant'),
        brand: this.translate.instant('label.brand'),
        actions: this.translate.instant('label.actions')
      },
      actions: {
        back: this.translate.instant('action.back'),
        insert: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.hotel')
        })
      }
    };
  }

  private isBlocked(hotel) {
    return !hotel.isBlocked ? 'success' : 'danger';
  }

  private openBlockModal(hotel: DashboardProperty) {
    const {name, id, isBlocked} = hotel;
    this.currentHotel = hotel;
    this.modal.open(name, id, isBlocked);
  }

  private goToNewHotelPage() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  private goToUpdateHotelPage(item) {
    this.router.navigate([`edit/${item.id}`], {relativeTo: this.route});
  }
}
