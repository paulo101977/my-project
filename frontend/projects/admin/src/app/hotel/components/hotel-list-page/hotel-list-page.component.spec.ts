import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Location } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { HotelApiService } from '../../services/hotel-api.service';
import { HotelListPageComponent } from './hotel-list-page.component';
import { of } from 'rxjs';

describe('HotelListPageComponent', () => {
  let component: HotelListPageComponent;
  let fixture: ComponentFixture<HotelListPageComponent>;

  // @ts-ignore
  const locationStub: Partial<Location> = {
    back: () => {}
  };

  let location: Location;
  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {}
  };

  let translateService: TranslateService;
  // @ts-ignore
  const hotelApiStubService: Partial<HotelApiService> = {
    list: () => of( null )
  };

  let hotelApiService: HotelApiService;
  @Component({selector: 'admin-page-list-wrapper', template: ''})

  class PageListWrapperStubComponent {}
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelListPageComponent, PageListWrapperStubComponent ],
      imports: [ RouterTestingModule ],
      providers: [
        {
          provide: Location,
          useValue: locationStub
        },
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: HotelApiService,
          useValue: hotelApiStubService
        }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
      .compileComponents();
    location = TestBed.get(Location);
    translateService = TestBed.get(TranslateService);
    hotelApiService = TestBed.get(HotelApiService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
