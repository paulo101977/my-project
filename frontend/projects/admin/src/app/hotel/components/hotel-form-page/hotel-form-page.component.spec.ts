import { Location } from '@angular/common';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { ThfDialogService, ThfNotificationService } from '@totvs/thf-ui';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../../testing/create-service-stub';
import { HotelForm } from '../../models/hotel-form';
import { PropertyRequest } from '../../models/property-request';
import { HotelService } from '../../services/hotel.service';

import { HotelFormPageComponent } from './hotel-form-page.component';

describe('HotelFormPageComponent', () => {
  let component: HotelFormPageComponent;
  let fixture: ComponentFixture<HotelFormPageComponent>;
  let hotelService: HotelService;
  let thfNotificationService: ThfNotificationService;
  let thfDialogService: ThfDialogService;
  let route: ActivatedRoute;
  const hotelResponse = {
    name: 'hotel',
    companyId: 2,
    brandId: 4,
    tenantName: 'tenant',
    locationList: [
      {
        locationCategoryId: 1,
        locationCategory: {
          name: 'name'
        }
      }
    ]
  };

  const generateFormValues = (
    hotelName: string = '',
    companyId: number = 1,
    brandId: number = 1,
    locations: any[] = []
  ) => {
    return { hotelName, companyId, brandId, locations };
  };

  const HotelServiceProvider = createServiceStub(HotelService, {
    save(propertyRequest: PropertyRequest, id: number): Observable<any> { return of(null); },
    listCompanyLocations(companyId: number): Observable<any> { return of(null); },
    getData(hotelId?: number): Observable<any> { return of(
      {
        hotel: hotelResponse,
        companies: [{}],
        brands: [{}]
      }
    ); }
  });

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };

  const locationStub: Partial<Location> = {
    back(): void {}
  };

  // @ts-ignore
  const thfNotificationServiceStub: Partial<ThfNotificationService> = {
    success: () => {
    }
  };

  // @ts-ignore
  const thfDialogServiceStub: Partial<ThfDialogService> = {
    confirm: () => {
    }
  };

  @Component({selector: 'admin-hotel-form', template: ''})
  class HotelFormStubComponent {
    private hotelForm: HotelForm = new HotelForm();
    public form: FormGroup = this.hotelForm.form;
  }

  @Component({selector: 'admin-hotel-integration-partner-list', template: ''})
  class HotelIntegrationPartnerListStubComponent {
    public getSelected() {
      return [];
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        HotelFormPageComponent,
        HotelFormStubComponent,
        HotelIntegrationPartnerListStubComponent
      ],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: ThfNotificationService,
          useValue: thfNotificationServiceStub
        },
        {
          provide: ThfDialogService,
          useValue: thfDialogServiceStub
        },
        {
          provide: Location,
          useValue: locationStub
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {id: undefined}
            }
          }
        },
        FormBuilder,
        HotelServiceProvider
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();

    hotelService = TestBed.get(HotelService);
    thfNotificationService = TestBed.get(ThfNotificationService);
    thfDialogService = TestBed.get(ThfDialogService);
    route = TestBed.get(ActivatedRoute);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelFormPageComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should set initial settings', () => {
      expect(component.i18n).toBeUndefined();
      expect(component.literals).toBeUndefined();

      fixture.detectChanges();

      expect(component.i18n).toBeDefined();
      expect(component.literals).toBeDefined();
      expect(component.literals.cancel).toEqual(component.i18n.action.cancel);
      expect(component.literals.save).toEqual(component.i18n.action.confirm);
    });

    it('should not get hotel data when there is no id', () => {
      expect(component.companyOptions.length).toBe(0);
      expect(component.brandOptions.length).toBe(0);

      fixture.detectChanges();
      expect(component.companyOptions.length).toBe(1);
      expect(component.brandOptions.length).toBe(1);
    });

    it('should get hotel data when there is an id', () => {
      route.snapshot.params.id = 1;

      fixture.detectChanges();

      component.hotelForm['hotelForm'].setFormData(component.hotelData);
      component.hotelForm['hotelForm'].resetLocations(component.locations);
      const { hotelName, companyId, brandId, locations } = component.hotelForm.form.value;

      expect(component.hotelData.tenantName).toBeDefined();
      expect(component.locations.length).toBe(1);
      expect(component.companyOptions.length).toBe(1);
      expect(component.brandOptions.length).toBe(1);

      expect(hotelName).toEqual(hotelResponse.name);
      expect(companyId).toEqual(hotelResponse.companyId);
      expect(brandId).toEqual(hotelResponse.brandId);
      expect(locations.length).toBe(1);
    });
  });

  describe('#cancel', () => {
    beforeEach(() => {
      fixture.detectChanges();
      spyOn(thfDialogService, 'confirm');
    });

    it('should not open confirm dialog when form is pristine', () => {
      component.cancel();

      expect(thfDialogService.confirm).not.toHaveBeenCalled();
    });

    it('should not open confirm dialog when form is only touched', () => {
      component.hotelForm.form.markAsTouched();
      component.cancel();

      expect(thfDialogService.confirm).not.toHaveBeenCalled();
    });

    it('should not open confirm dialog when form is only dirty', () => {
      component.hotelForm.form.markAsDirty();
      component.cancel();

      expect(thfDialogService.confirm).not.toHaveBeenCalled();
    });

    it('should open confirm dialog when form is dirty and touched', () => {
      component.hotelForm.form.markAsDirty();
      component.hotelForm.form.markAsTouched();
      component.cancel();

      expect(thfDialogService.confirm).toHaveBeenCalled();
    });
  });

  describe('#save', () => {
    beforeEach(() => {
      spyOn(hotelService, 'save').and.callThrough();
    });

    it('shouldn\'t submit when form is invalid', () => {
      fixture.detectChanges();
      component.hotelForm.form.patchValue({hotelName: null});
      component.save();

      expect(hotelService.save).not.toHaveBeenCalled();
    });

    it('should create an hotel when there is no id', () => {
      const hotelValues = generateFormValues('name', 1, 2, [{locationCategoryId: 1}]);
      route.snapshot.params.id = null;
      fixture.detectChanges();
      component.hotelForm.form.patchValue(hotelValues);
      component.hotelForm['hotelForm'].resetLocations(hotelValues.locations);
      component.save();

      expect(hotelService.save).toHaveBeenCalled();
    });

    it('should update an hotel when there is an id', () => {
      const hotelValues = generateFormValues('name', 1, 2, [{locationCategoryId: 1}]);
      route.snapshot.params.id = 1;
      fixture.detectChanges();
      component.hotelForm.form.patchValue(hotelValues);
      component.hotelForm['hotelForm'].resetLocations(hotelValues.locations);
      component.save();

      expect(hotelService.save).toHaveBeenCalled();
    });
  });

  describe('#setCompanyLocationsAsLocations', () => {
    it('should use the company locations', () => {
      const companyLocations: any[] = [
        { id: 1, locationCategory: {} },
        { id: 2, locationCategory: {} }
      ];

      spyOn(hotelService, 'listCompanyLocations').and.returnValue(of({
        items: companyLocations
      }));

      expect(component.locations.length).toEqual(0);

      fixture.detectChanges();
      component.setCompanyLocationsAsLocations();

      expect(component.locations.length).toEqual(2);
    });

    it('should replace old locations', () => {
      component.locations = [1, 2, 3, 4];

      const companyLocations: any[] = [
        { id: 1, locationCategory: {} },
        { id: 2, locationCategory: {} }
      ];

      spyOn(hotelService, 'listCompanyLocations').and.returnValue(of({
        items: companyLocations
      }));

      expect(component.locations.length).toEqual(4);

      fixture.detectChanges();
      component.setCompanyLocationsAsLocations();

      expect(component.locations.length).toEqual(2);
    });
  });
});
