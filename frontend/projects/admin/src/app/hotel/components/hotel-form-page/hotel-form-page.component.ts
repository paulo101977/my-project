import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ThfDialogService, ThfNotificationService, ThfPageEditLiterals } from '@totvs/thf-ui';
import { addAddressTypeField } from '../../../address/helpers/helpers';
import { Property } from '../../models/property';
import { PropertyRequest } from '../../models/property-request';
import { HotelService } from '../../services/hotel.service';
import { HotelBlockModalComponent } from '../hotel-block-modal/hotel-block-modal.component';
import { HotelFormComponent } from '../hotel-form/hotel-form.component';
import {
  HotelIntegrationPartnerListComponent
} from '../hotel-integration-partner-list/hotel-integration-partner-list.component';

@Component({
  selector: 'admin-hotel-form-page',
  templateUrl: './hotel-form-page.component.html',
  styleUrls: ['./hotel-form-page.component.css']
})
export class HotelFormPageComponent implements OnInit {
  @ViewChild('modal') hotelBlockModal: HotelBlockModalComponent;
  @ViewChild('hotelForm') hotelForm: HotelFormComponent;
  @ViewChild('integrationPartnerList') integrationPartnerList: HotelIntegrationPartnerListComponent;

  public hotelId: number;
  public i18n: any;
  public literals: ThfPageEditLiterals;
  public hotelData: any = {};
  public loadingContent = true;
  public companyOptions: any[] = [];
  public brandOptions: any[] = [];
  public integrationPartners: any[] = [];
  public locations: any[] = [];
  public formInvalid = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private thfNotification: ThfNotificationService,
    private thfDialog: ThfDialogService,
    private location: Location,
    private hotelService: HotelService
  ) {}

  ngOnInit() {
    this.hotelId = this.route.snapshot.params.id;
    this.setI18n();
    this.setPageSettings();
    this.getData();
  }

  private getData() {
    this.hotelService
      .getData(this.hotelId)
      .subscribe(({companies, brands, hotel, integrationPartners}) => {
        this.loadingContent = false;
        this.companyOptions = companies;
        this.brandOptions = brands;
        this.integrationPartners = integrationPartners;

        if (this.hotelId) {
          this.setHotelData(hotel);
        }
      });
  }

  private setHotelData(hotel: Property): void {
    this.loadToHotelData(hotel);
    this.loadLocations(hotel.locationList);
  }

  private setI18n() {
    this.i18n = {
      title: this.translate.instant('action.lbEdit', {
        labelName: this.translate.instant('label.hotel')
      }),
      group: {
        hotel: this.translate.instant('variable.dataOf.M', {
          labelName: this.translate.instant('label.hotel')
        }),
      },
      action: {
        confirm: this.translate.instant('action.confirm'),
        cancel: this.translate.instant('action.cancel'),
        useCompanyAddress: this.translate.instant('action.useCompanyAddress')
      },
      variable: {
        editSuccess: this.translate.instant('variable.lbEditSuccessF', {
          labelName: this.translate.instant('label.hotel')
        })
      },
      alert: {
        title: this.translate.instant('alert.redirectConfirm'),
        body: this.translate.instant('alert.dataHasNotBeenSavedYet')
      }
    };
  }

  private setPageSettings() {
    this.literals = {
      cancel: this.i18n.action.cancel,
      save: this.i18n.action.confirm
    };
  }

  private loadToHotelData(hotel: Property) {
    this.hotelData = {
      ...hotel
    };
  }

  private loadLocations(locations) {
    if (locations) {
      this.locations = locations.map(addAddressTypeField);
    }
  }

  private beforeRedirect() {
    if (this.hotelForm.form.dirty && this.hotelForm.form.touched) {
      this.thfDialog.confirm({
        title: this.i18n.alert.title,
        message: this.i18n.alert.body,
        confirm: () => this.redirectToList()
      });

    } else {
      this.redirectToList();
    }
  }

  public cancel() {
    this.beforeRedirect();
  }

  public save() {
    this.submit();
  }

  private submit() {
    if (this.hotelForm.form.invalid) {
      return;
    }

    const { hotelName, companyId, brandId, locations, blocked, maxUh } = this.hotelForm.form.value;
    const params: PropertyRequest = {
      name: hotelName,
      locationList: locations,
      brandId,
      companyId,
      isBlocked: !!blocked,
      maxUh,
      integrationPartnerPropertyList: this.integrationPartnerList.getSelected()
    };

    this.hotelService
      .save(params, this.hotelId)
      .subscribe(() => {
        this.thfNotification.success(this.i18n.variable.editSuccess);
        this.redirectToList();
      });
  }

  private redirectToList() {
    this.location.back();
  }

  private listCompanyLocations() {
    const companyId: number = this.hotelForm.form.get('companyId').value;
    return this.hotelService.listCompanyLocations(companyId);
  }

  public setCompanyLocationsAsLocations() {
    this.listCompanyLocations()
      .subscribe(({items}) => {
        this.loadLocations(items);
      });
  }

  public updateLocations(locations) {
    this.loadLocations(locations);
  }

  public updateFormStatus(isValid) {
    this.formInvalid = !isValid;
  }
}
