import { Component, Input } from '@angular/core';
import { ThfTableColumnIcon } from '@totvs/thf-ui';
import { TableSettings } from '../../../shared/models/table-settings';

@Component({
  selector: 'admin-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css']
})
export class HotelListComponent {
  @Input() hotels: any[];
  @Input() columns: string[] = [];
  @Input() actions: ThfTableColumnIcon[] = [];
  @Input() tableSettings: TableSettings;

  public tableColumns = [
    { property: 'name', label: 'label.hotel', type: 'string' },
    { property: 'brandName', label: 'label.brand', type: 'string' },
  ];
}
