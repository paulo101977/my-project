import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { debounceTime } from 'rxjs/operators';
import { HotelForm } from '../../models/hotel-form';
import { Property } from '../../models/property';
import { HotelBlockModalComponent } from '../hotel-block-modal/hotel-block-modal.component';

@Component({
  selector: 'admin-hotel-form',
  templateUrl: './hotel-form.component.html',
  styleUrls: ['./hotel-form.component.css']
})
export class HotelFormComponent implements OnInit, OnChanges {
  @ViewChild('modal') hotelBlockModal: HotelBlockModalComponent;

  @Input() public hotelId: string;
  @Input() public companyOptions: any[] = [];
  @Input() public brandOptions: any[] = [];
  @Input() public locations: any[] = [];
  @Input() public hotel: Property;
  @Output() public statusChanged = new EventEmitter<boolean>();

  private hotelForm: HotelForm;
  public i18n: any;
  public form: FormGroup;

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setI18n();
    this.setFormSettings();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const {hotel, locations} = changes;

    if (hotel && !hotel.isFirstChange()) {
      this.loadToForm(hotel.currentValue);
    }

    if (locations && !locations.isFirstChange()) {
      this.resetFormLocations(locations.currentValue);
    }
  }

  public acceptHotelBlock() {
    this.hotelBlockModal.close();
  }

  public refuseHotelBlock(blocked) {
    this.form.patchValue({blocked: !blocked});
  }

  public openConfirmModal(formValue) {
    if (!this.hotelId) {
      return;
    }

    const {hotelName, blocked} = formValue;
    this.hotelBlockModal.open(hotelName, this.hotelId, blocked);
  }

  private setFormSettings() {
    this.hotelForm = new HotelForm();
    this.form = this.hotelForm.form;

    this.form.statusChanges
      .pipe(debounceTime(50))
      .subscribe(() => {
        this.statusChanged.emit(this.form.valid);
      });
  }

  private setI18n() {
    this.i18n = {
      label: {
        name: this.translate.instant('label.name'),
        hotel: this.translate.instant('variable.nameOf.M', {
          labelName: this.translate.instant('label.hotel')
        }),
        company: this.translate.instant('label.company'),
        tenant: this.translate.instant('label.tenant'),
        brand: this.translate.instant('label.brand'),
        blocked: this.translate.instant('label.blocked'),
        maxUh: this.translate.instant('label.maxUh'),
        registeredUhs: this.translate.instant('label.registeredUhs')
      }
    };
  }

  private loadToForm(hotel: Property) {
    this.hotelForm.setFormData(hotel);
  }

  private resetFormLocations(locations) {
    this.hotelForm.resetLocations(locations);
  }
}
