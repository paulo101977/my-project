import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { HotelBlockModalComponent } from '../hotel-block-modal/hotel-block-modal.component';

import { HotelFormComponent } from './hotel-form.component';

describe('HotelFormComponent', () => {
  let component: HotelFormComponent;
  let fixture: ComponentFixture<HotelFormComponent>;

  @Component({ selector: 'admin-hotel-block-modal', template: '' })
  class HotelBlockModalStubComponent {
    open = jasmine.createSpy('open');
    close = jasmine.createSpy('close');
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        HotelFormComponent,
        HotelBlockModalStubComponent
      ],
      imports: [TranslateTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#acceptHotelBlock', () => {
    it('should close the hotel block modal and keep the blocked state', () => {
      const blocked = true;
      component.form.patchValue({ blocked });
      component.acceptHotelBlock();

      expect(component.form.value.blocked).toBe(blocked);
      expect(component.hotelBlockModal.close).toHaveBeenCalled();
    });
  });

  describe('#refuseHotelBlock', () => {
    it('should reset the blocked state', () => {
      const blocked = true;
      component.refuseHotelBlock(blocked);

      expect(component.form.value.blocked).toBe(!blocked);
    });
  });

  describe('#openConfirmModal', () => {
    it('shouldn\'t open the modal with a new hotel', () => {
      component.hotelId = null;

      component.openConfirmModal({});
      expect(component.hotelBlockModal.open).not.toHaveBeenCalled();
    });

    it('should open the confirm modal with the hotel info', () => {
      const hotel = {
        id: '1',
        hotelName: 'opa',
        blocked: false
      };

      component.hotelId = hotel.id;
      component.openConfirmModal(hotel);
      expect(component.hotelBlockModal.open).toHaveBeenCalledWith(hotel.hotelName, hotel.id, hotel.blocked);
    });
  });
});
