import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThfTableColumnIcon } from '@totvs/thf-ui';
import {
  IntegrationPartnerFormComponent
} from '../../../integration-partner/components/integration-partner-form/integration-partner-form.component';
import { UserService } from '../../../user/services/user.service';

@Component({
  selector: 'admin-hotel-integration-partner-list',
  templateUrl: './hotel-integration-partner-list.component.html',
  styleUrls: ['./hotel-integration-partner-list.component.css']
})
export class HotelIntegrationPartnerListComponent implements OnInit, OnChanges {
  @ViewChild('modal') modal: IntegrationPartnerFormComponent;
  @Input() integrationPartners: any[];
  @Input() hotel: any;

  public i18n: any;
  public selectedIntegrationPartners: any[] = [];
  public actions: ThfTableColumnIcon[] = [
    {
      value: 'edit', icon: 'thf-icon thf-icon-edit', action: (partner: any) => {
        this.openEditModal(partner);
      }
    },
    {
      value: 'active', icon: 'thf-icon thf-icon-totvs', action: (partner: any) => {
        this.toggleActiveState(partner);
      }
    },
    {
      value: 'delete', icon: 'thf-icon-delete', action: (partner: any) => {
        this.deleteIntegrationPartner(partner);
      }
    }
  ];

  constructor(
    private userService: UserService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.setI18n();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.onHotelChanges(changes);
  }

  public getSelected() {
    return this.selectedIntegrationPartners;
  }

  public handleSubmit(partner: any) {
    const { integrationCode, isActive, partnerId, partnerIndex } = partner;
    const preparedPartner = this.prepareSelectedPartner({integrationCode, isActive, partnerId, partnerIndex});

    if (partnerIndex > -1) {
      this.selectedIntegrationPartners[partnerIndex] = preparedPartner;
      return;
    }

    this.selectedIntegrationPartners.push(preparedPartner);
  }

  private setI18n() {
    this.i18n = {
      integrationPartner: this.translate.instant('label.integrationPartner'),
      associateIntegrationPartner: this.translate.instant('action.lbAssociate', {
        labelName: this.translate.instant('label.integrationPartner')
      }),
      activeStatus: {
        yes: this.translate.instant('commomData.yes'),
        no: this.translate.instant('commomData.not')
      }
    };
  }

  private onHotelChanges({hotel}: SimpleChanges): void {
    const hasHotel: boolean = !!(hotel && hotel.currentValue);
    if (!hasHotel) {
      return;
    }

    const hasUsefulValue = Object.keys(hotel.currentValue).length;
    if (!hasUsefulValue) {
      return;
    }

    this.selectedIntegrationPartners = this.hotel.integrationPartnerPropertyList
      .map(partner => this.prepareSelectedPartner(partner));
  }

  private deleteIntegrationPartner(partner: any) {
    this.selectedIntegrationPartners = this.selectedIntegrationPartners
      .filter(selectedPartner => {
        const hasEqualIntegrationCode = selectedPartner.integrationCode === partner.integrationCode;
        const hasEqualPartnerId = selectedPartner.partnerId === partner.partnerId;
        return !(hasEqualIntegrationCode && hasEqualPartnerId);
      });
  }

  private prepareSelectedPartner(partner: any) {
    const { yes, no } = this.i18n.activeStatus;
    return {
      ...partner,
      actions: ['active', 'edit', 'delete'],
      activeStatus: partner.isActive ? yes : no
    };
  }

  private toggleActiveState(partner: any) {
    const { yes, no } = this.i18n.activeStatus;
    partner.isActive = !partner.isActive;
    partner.activeStatus = partner.isActive ? yes : no;
  }

  private openEditModal(partner: any) {
    const partnerIndex = this.selectedIntegrationPartners.indexOf(partner);
    this.modal.open(partnerIndex, partner);
  }
}
