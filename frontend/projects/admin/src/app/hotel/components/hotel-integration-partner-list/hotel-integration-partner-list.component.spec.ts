import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { createServiceStub } from '../../../../../../../testing/create-service-stub';
import { UserService } from '../../../user/services/user.service';

import { HotelIntegrationPartnerListComponent } from './hotel-integration-partner-list.component';

describe('HotelIntegrationPartnerListComponent', () => {
  let component: HotelIntegrationPartnerListComponent;
  let fixture: ComponentFixture<HotelIntegrationPartnerListComponent>;

  const UserProvider = createServiceStub(UserService, {});

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ HotelIntegrationPartnerListComponent ],
      imports: [ TranslateTestingModule ],
      providers: [
        UserProvider
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelIntegrationPartnerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
