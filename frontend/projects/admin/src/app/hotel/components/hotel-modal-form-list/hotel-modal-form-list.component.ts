import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfModalAction, ThfModalComponent } from '@totvs/thf-ui';
import { Observable } from 'rxjs';
import { debounceTime, map, startWith } from 'rxjs/operators';
import { TableSettings } from '../../../shared/models/table-settings';

@Component({
  selector: 'admin-hotel-modal-form-list',
  templateUrl: './hotel-modal-form-list.component.html',
  styleUrls: ['./hotel-modal-form-list.component.css']
})
export class HotelModalFormListComponent implements OnInit, OnChanges {
  @ViewChild('modal') modal: ThfModalComponent;
  @Input() hotels: any[];
  @Input() selectedHotels: any[];
  @Output() submitted = new EventEmitter<any[]>();

  public i18n: any;
  public search = new FormControl();
  public filteredHotels$: Observable<any[]>;
  public displayedColumns = ['brandName', 'name'];
  public tableSettings: Partial<TableSettings> = {
    checkbox: true
  };
  public primaryAction: ThfModalAction = {
    action: () => this.submitSelectedHotels(),
    label: this.translate.instant('action.confirm'),
  };
  public secondaryAction: ThfModalAction = {
    action: () => this.cancelSubmit(),
    label: this.translate.instant('action.cancel')
  };
  private modalHotels: any[];

  constructor(
    private translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.setI18n();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.onHotelsChange(changes);
  }

  public open() {
    this.modal.open();
  }

  public close() {
    this.modal.close();
  }

  public submitSelectedHotels() {
    const selectedHotels = this.modalHotels
      .filter((hotel: any) => {
        return hotel.$selected;
      })
      .map((hotel) => ({
        ...hotel,
        $selected: false
      }));

    this.submitted.emit(selectedHotels);
    this.modal.close();
  }

  public cancelSubmit() {
    this.modal.close();
  }

  private setI18n() {
    this.i18n = {
      modalTitle: this.translate.instant('action.lbAssociate', {
        labelName: this.translate.instant('label.hotel')
      }),
      placeholder: this.translate.instant('action.lbSearchFor', {
        text: this.translate.instant('label.hotel')
      })
    };
  }

  private onHotelsChange({hotels}: SimpleChanges) {
    if (!hotels || !hotels.currentValue) {
      return;
    }

    this.modalHotels = JSON.parse(JSON.stringify(this.hotels));
    this.filteredHotels$ = this.getFilteredHotels();
  }

  private getFilteredHotels() {
    return this.search
      .valueChanges
      .pipe(
        startWith(''),
        debounceTime(200),
        this.searchHotels(),
        this.markSelected(this.selectedHotels)
      );
  }

  private searchHotels() {
    return map((search: string) => {
      return this.modalHotels.filter((hotel) => {
        const nothingToSearch = !search;
        const isSelected = hotel.$selected;
        const searchExpression = new RegExp(search, 'i');
        const found = hotel.name.search(searchExpression) > -1;

        return nothingToSearch || isSelected || found;
      });
    });
  }

  private markSelected(selectedHotels: any[] = []) {
    const hotelIsSelected = (hotel: any): boolean => {
      return selectedHotels.some(selectedHotel => selectedHotel.id == hotel.id);
    };

    return map((hotels: any) => hotels.map(hotel => {
      hotel.$selected = hotel.$selected || hotelIsSelected(hotel);
      return hotel;
    }));
  }
}
