import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  ThfButtonModule,
  ThfFieldModule,
  ThfInfoModule,
  ThfModalModule,
  ThfNotificationModule,
  ThfPageModule,
  ThfTableModule
} from '@totvs/thf-ui';
import { I18nModule } from '@inovacaocmnet/thx-bifrost';
import { AddressModule } from '../address/address.module';
import { IntegrationPartnerModule } from '../integration-partner/integration-partner.module';
import { SharedModule } from '../shared/shared.module';
import { BasicListWrapperModule } from '../widgets/basic-list-wrapper/basic-list-wrapper.module';
import { PageListWrapperModule } from '../widgets/page-list-wrapper/page-list-wrapper.module';
import { HotelFormPageComponent } from './components/hotel-form-page/hotel-form-page.component';
import { HotelListPageComponent } from './components/hotel-list-page/hotel-list-page.component';
import { HotelBlockModalComponent } from './components/hotel-block-modal/hotel-block-modal.component';
import { HotelFormComponent } from './components/hotel-form/hotel-form.component';
import { HotelListComponent } from './components/hotel-list/hotel-list.component';
import { HotelModalFormListComponent } from './components/hotel-modal-form-list/hotel-modal-form-list.component';
import { HotelIntegrationPartnerListComponent } from './components/hotel-integration-partner-list/hotel-integration-partner-list.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PageListWrapperModule,
    ThfModalModule,
    ThfButtonModule,
    ThfFieldModule,
    ThfNotificationModule,
    ThfTableModule,
    ThfPageModule,
    ThfInfoModule,
    SharedModule,
    AddressModule,
    I18nModule,
    BasicListWrapperModule,
    IntegrationPartnerModule
  ],
  declarations: [
    HotelListPageComponent,
    HotelFormPageComponent,
    HotelBlockModalComponent,
    HotelFormComponent,
    HotelListComponent,
    HotelModalFormListComponent,
    HotelIntegrationPartnerListComponent
  ],
  exports: [
    HotelListPageComponent,
    HotelFormPageComponent,
    HotelBlockModalComponent,
    HotelFormComponent,
    HotelListComponent,
    HotelModalFormListComponent
  ],
})
export class HotelModule {
}
