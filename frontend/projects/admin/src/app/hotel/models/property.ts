import { Brand } from '../../brand/models/brand';

export class Property {
  brandId: number;
  brand: Brand;
  chainId: number;
  companyId: number;
  companyUid: string;
  id: number;
  name: string;
  tradeName: string;
  propertyStatusId: number;
  propertyTypeId: number;
  propertyUId: string;
  tenantId: string;
  tenantName: string;
  locationList: any[];
  isBlocked: boolean;
  maxUh: number;
  registeredUhs: number;
}
