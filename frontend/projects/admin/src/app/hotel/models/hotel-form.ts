import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocationCategoryEnum } from 'app/shared/models/location-category-enum';
import { minimumUh, requiredLocationCategory, uniqueField } from './hotel-validators';
import { Property } from './property';

export class HotelForm {
  private fb = new FormBuilder();
  public form: FormGroup;

  private get locations(): FormArray {
    return this.form.get('locations') as FormArray;
  }

  constructor() {
    this.form = this.fb.group(
      {
        hotelName: [null, Validators.required],
        companyId: [null, Validators.required],
        brandId: [null, Validators.required],
        blocked: [null],
        maxUh: [0],
        registeredUhs: [0],
        locations: this.fb.array([], [
          Validators.required,
          requiredLocationCategory(LocationCategoryEnum.Comercial),
          uniqueField('locationCategoryId')
        ])
      },
      {
        validator: [minimumUh]
      }
    );
  }

  private locationToGroupObject(location) {
    return {
      id: location.id,
      locationCategoryId: location.locationCategoryId,
      latitude: location.latitude,
      longitude: location.longitude,
      streetName: location.streetName,
      streetNumber: location.streetNumber,
      additionalAddressDetails: location.additionalAddressDetails,
      neighborhood: location.neighborhood,
      cityId: location.cityId,
      postalCode: location.postalCode,
      countryCode: location.countryCode,
      subdivision: location.subdivision,
      division: location.division,
      country: location.country,
    };
  }

  public setFormData(property: Property) {
    this.form.patchValue({
      hotelName: property.name,
      companyId: property.companyId,
      brandId: property.brandId,
      blocked: property.isBlocked,
      maxUh: property.maxUh,
      registeredUhs: property.registeredUhs
    });
  }

  public setLocations(locations) {
    locations.map((location) => {
      const locationGroup = this.fb.group(this.locationToGroupObject(location));
      this.locations.push(locationGroup);
    });
  }

  public resetLocations(locations) {
    this.removeLocations();
    this.setLocations(locations);
  }

  public removeLocations() {
    this.removeAllItemsFromFormArray(this.locations);
  }

  // TODO move it to Bifrost
  private removeAllItemsFromFormArray(formArray: FormArray) {
    if (!formArray.controls.length) {
      return;
    }

    formArray.removeAt(0);

    if (formArray.controls.length) {
      this.removeAllItemsFromFormArray(formArray);
    }
  }
}
