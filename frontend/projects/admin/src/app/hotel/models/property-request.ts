import { IntegrationPartnerProperty } from '../../integration-partner/models/integration-partner-property-list';
import { LocationRequest } from '../../shared/models/location-request';

export interface PropertyRequest {
  id?: number;
  companyId: number;
  brandId: number;
  name: string;
  isBlocked: boolean;
  maxUh: number;
  locationList: LocationRequest[];
  integrationPartnerPropertyList: IntegrationPartnerProperty[];
}
