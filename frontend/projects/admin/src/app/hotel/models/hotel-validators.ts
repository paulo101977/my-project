import { AbstractControl, FormArray } from '@angular/forms';

function mapField(field) {
  return (formArray: FormArray) => {
    return formArray.value.map((item) => item[field]);
  };
}

export function requiredLocationCategory(locationCategoryId: number) {
  const getLocationCategoryIdArray = mapField('locationCategoryId');
  const error = { requiredLocationCategory: locationCategoryId };

  return (formArray: FormArray) => {
    const locationsIds = getLocationCategoryIdArray(formArray);
    const hasLocationCategory = locationsIds.includes(locationCategoryId);

    return !hasLocationCategory ? error : null;
  };
}

export function uniqueField(field) {
  const getFieldArray = mapField(field);
  const error = {
    uniqueField: {
      [field]: true
    }
  };

  return (formArray: FormArray) => {
    const fieldArray = getFieldArray(formArray);
    const uniqueFieldSet = new Set(fieldArray);
    const hasOnlyUniqueValues = fieldArray.length === uniqueFieldSet.size;

    return !hasOnlyUniqueValues ? error : null;
  };
}

export function minimumUh(control: AbstractControl) {
  const { maxUh, registeredUhs } = control.value;
  const error = { minimumUh: registeredUhs };

  return registeredUhs > maxUh ? error : null;
}
