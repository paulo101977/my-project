import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing/create-service-stub';
import { BrandApiService } from '../../brand/services/brand-api.service';
import { CompanyApiService } from '../../company/services/company-api.service';
import { IntegrationPartnerResourceService } from '../../integration-partner/services/integration-partner-resource.service';
import { brandResponseMock } from '../mock/brand-response-mock';
import { companyResponseMock } from '../mock/company-response-mock';
import { hotelResponseMock } from '../mock/hotel-response-mock';
import { PropertyRequest } from '../models/property-request';
import { HotelApiService } from './hotel-api.service';

import { HotelService } from './hotel.service';

describe('HotelService', () => {
  let service: HotelService;

  const HotelApiProvider = createServiceStub(HotelApiService, {
    getById(id: number): Observable<any> { return of(hotelResponseMock); },
    createHotel(propertyRequest: PropertyRequest): Observable<any> { return of('create'); },
    updateHotel(propertyRequest: PropertyRequest): Observable<any> { return of('update'); }
  });
  const CompanyApiProvider = createServiceStub(CompanyApiService, {
    listAll(): Observable<any> { return of(companyResponseMock); },
    listCompanyLocations(id: number): Observable<any> { return of('companyLocation'); }
  });
  const BrandApiProvider = createServiceStub(BrandApiService, {
    listAll(): Observable<any> { return of(brandResponseMock); }
  });
  const IntegrationPartnerResourceProvider = createServiceStub(IntegrationPartnerResourceService, {
    listAll(): Observable<any> { return of({items: []}); }
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HotelService,
        HotelApiProvider,
        CompanyApiProvider,
        BrandApiProvider,
        IntegrationPartnerResourceProvider
      ]
    });

    service = TestBed.get(HotelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getData', () => {
    it('should always return the companies and brands data', () => {
      let data;

      service
        .getData()
        .subscribe((response) => data = response);

      const {companies, brands} = data;

      expect(companies).toBeDefined();
      expect(companies.length).toBe(companyResponseMock.totalItems);
      expect(companies[0]).toEqual(jasmine.objectContaining({
        label: companyResponseMock.items[0].tradeName,
        value: companyResponseMock.items[0].id
      }));
      expect(brands).toBeDefined();
      expect(brands.length).toBe(brandResponseMock.totalItems);
      expect(brands[0]).toEqual(jasmine.objectContaining({
        label: brandResponseMock.items[0].name,
        value: brandResponseMock.items[0].id
      }));
    });

    it('shouldn\'t get hotel data if there is no hotelId', () => {
      let data;

      service
        .getData()
        .subscribe((response) => data = response);

      const {hotel} = data;

      expect(hotel).toBeUndefined();
    });

    it('should get hotel data if there is hotelId', () => {
      let data;

      service
        .getData(hotelResponseMock.id)
        .subscribe((response) => data = response);

      const {hotel} = data;

      expect(hotel).toBeDefined();
      expect(hotel).toBe(hotelResponseMock);
    });
  });

  describe('#listCompanyLocations', () => {
    it('should call CompanyApiService#listCompanyLocations', () => {
      const companyId = 1;
      let data;

      service
        .listCompanyLocations(companyId)
        .subscribe((response) => data = response);

      expect(data).toEqual('companyLocation');
    });
  });

  describe('#save', () => {
    it('should call HotelApiProvider#createHotel when there is no hotel ID', () => {
      let data;
      const params: PropertyRequest = {
        brandId: 0,
        companyId: 0,
        id: 0,
        isBlocked: false,
        locationList: [],
        name: '',
        maxUh: 0,
        integrationPartnerPropertyList: []
      };

      service
        .save(params)
        .subscribe(response => data = response);

      expect(data).toEqual('create');
    });

    it('should call HotelApiProvider#updateHotel when there is hotel ID', () => {
      let data;
      const id = 1;
      const params: PropertyRequest = {
        brandId: 0,
        companyId: 0,
        id: 0,
        isBlocked: false,
        locationList: [],
        name: '',
        maxUh: 0,
        integrationPartnerPropertyList: []
      };

      service
        .save(params, id)
        .subscribe(response => data = response);

      expect(data).toEqual('update');
    });
  });
});
