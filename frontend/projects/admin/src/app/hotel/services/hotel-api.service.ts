import { Injectable } from '@angular/core';
import { I18nService } from '@inovacaocmnet/thx-bifrost';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { PropertyRequest } from '../models/property-request';
import { HotelResourceService } from '../resources/hotel-resource.service';

@Injectable({
  providedIn: 'root'
})
export class HotelApiService {

  constructor(
    private resource: HotelResourceService,
    private i18nService: I18nService
  ) { }

  public list(params: ListQueryParams) {
    return this.resource.list(params);
  }

  public listAll() {
    const listAllParams = {
      SearchData: '',
      PageSize: 999,
      Page: 1
    };

    return this.list(listAllParams);
  }

  public getById(id: number) {
    return this.resource.getById(id);
  }

  public createHotel(propertyRequest: PropertyRequest) {
    const property = this.addBrowserLanguage(propertyRequest);
    return this.resource.createHotel(property);
  }

  public updateHotel(propertyRequest: PropertyRequest) {
    const property = this.addBrowserLanguage(propertyRequest);
    return this.resource.updateHotel(property);
  }

  private addBrowserLanguage(property: PropertyRequest): PropertyRequest {
    const locations = property.locationList.map((location) => {
      return {
        ...location,
        browserLanguage: this.i18nService.getLanguage()
      };
    });

    return {
      ...property,
      locationList: locations
    };
  }
}
