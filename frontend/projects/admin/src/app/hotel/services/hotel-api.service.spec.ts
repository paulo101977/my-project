import { TestBed } from '@angular/core/testing';
import { I18nService } from '@inovacaocmnet/thx-bifrost';
import { HotelResourceService } from '../resources/hotel-resource.service';

import { HotelApiService } from './hotel-api.service';
import { of } from 'rxjs';

describe('HotelApiService', () => {
  let service: HotelApiService;
  // @ts-ignore
  const hotelResourceStubService: Partial<HotelResourceService> = {
    list: () => of(null),
    getById: () => of(null),
  };
  let hotelResourceService: HotelResourceService;

  const i18nServiceStub: Partial<I18nService> = {
    getLanguage(): string { return ''; }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HotelApiService,
        {
          provide: HotelResourceService,
          useValue: hotelResourceStubService
        },
        {
          provide: I18nService,
          useValue: i18nServiceStub
        }
      ]
    });

    service = TestBed.get(HotelApiService);
    hotelResourceService = TestBed.get(HotelResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call BrandResourceService#list', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(hotelResourceService, 'list');

      service.list(mock);
      expect(hotelResourceService.list).toHaveBeenCalledWith(mock);
    });
  });

  describe('#getById', () => {
    it('should call BrandResourceService#getById', () => {
      const id = 1;
      spyOn(hotelResourceService, 'getById');

      service.getById(id);
      expect(hotelResourceService.getById).toHaveBeenCalledWith(id);
    });
  });
});
