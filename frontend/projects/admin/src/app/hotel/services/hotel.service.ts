import { Injectable } from '@angular/core';
import { toThfOptions } from '@inovacaocmnet/thx-bifrost';
import { forkJoin, from, Observable, pipe } from 'rxjs';
import { map, pluck, switchMap, toArray } from 'rxjs/operators';
import { BrandApiService } from '../../brand/services/brand-api.service';
import { CompanyApiService } from '../../company/services/company-api.service';
import { IntegrationPartnerResourceService } from '../../integration-partner/services/integration-partner-resource.service';
import { PropertyRequest } from '../models/property-request';
import { HotelApiService } from './hotel-api.service';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(
    private hotelApi: HotelApiService,
    private companyApi: CompanyApiService,
    private brandApi: BrandApiService,
    private integrationPartnerResource: IntegrationPartnerResourceService
  ) { }

  public getData(hotelId?: number): Observable<any> {
    const shouldGetHotel = !!hotelId;
    const sources = [
      this.listAllCompanies(),
      this.listAllBrands(),
      this.listAllIntegrationPartners()
    ];

    if (shouldGetHotel) {
      sources.push(this.getHotel(hotelId));
    }

    return forkJoin(sources)
      .pipe(
        map(([companies, brands, integrationPartners, hotel]) => ({
          companies,
          brands,
          integrationPartners,
          hotel
        }))
      );
  }

  public listCompanyLocations(companyId: number): Observable<any> {
    return this.companyApi.listCompanyLocations(companyId);
  }

  public save(propertyRequest: PropertyRequest, id?: number): Observable<any> {
    const shouldCreate = !id;
    const params: PropertyRequest = {
      ...propertyRequest,
      locationList: this.mapLocationList(propertyRequest.locationList),
      integrationPartnerPropertyList: this.cleanIntegrationPartnerList(propertyRequest.integrationPartnerPropertyList)
    };
    return shouldCreate ? this.createHotel(params) : this.updateHotel(params, id);
  }

  private getHotel(hotelId: number): Observable<any> {
    return this.hotelApi.getById(hotelId);
  }

  private listAllCompanies(): Observable<any> {
    return this.companyApi
      .listAll()
      .pipe(this.toOptions('id', 'tradeName'));
  }

  private listAllBrands(): Observable<any> {
    return this.brandApi
      .listAll()
      .pipe(this.toOptions('id', 'name'));
  }

  private listAllIntegrationPartners(): Observable<any> {
    return this.integrationPartnerResource.listAll()
      .pipe(
        pluck('items')
      );
  }

  private toOptions(key: string, label: string) {
    return pipe(
      pluck('items'),
      switchMap((list: any[]) => from(list)),
      toThfOptions(label, key),
      toArray()
    );
  }

  private createHotel(propertyRequest: PropertyRequest): Observable<any> {
    return this.hotelApi.createHotel(propertyRequest);
  }

  private updateHotel(createPropertyRequest: PropertyRequest, id: number): Observable<any> {
    const propertyRequest: PropertyRequest = {
      ...createPropertyRequest,
      id
    };
    return this.hotelApi.updateHotel(propertyRequest);
  }

  private mapLocationList(locationList) {
    return locationList.map(({cityId, id, ...location}) => {
      return location;
    });
  }

  private cleanIntegrationPartnerList(integrationPartners: any[]) {
    return integrationPartners.map(({id, integrationCode, isActive, partnerId}) => {
      const partner: any = {
        integrationCode,
        isActive,
        partnerId
      };
      if (id) {
        partner.id = id;
      }
      return partner;
    });
  }
}
