import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { I18nService } from '@inovacaocmnet/thx-bifrost';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { PropertyRequest } from '../models/property-request';

@Injectable({
  providedIn: 'root'
})
export class HotelResourceService {

  constructor(
    private i18nService: I18nService,
    private adminApi: AdminApiService
  ) { }

  public list(params: ListQueryParams) {
    return this.adminApi.get<any>('property', { params });
  }

  public getById(id) {
    return this.adminApi.get(`property/${id}`);
  }

  public createHotel(propertyRequest: PropertyRequest) {
    const params = {
      CompanyId: propertyRequest.companyId,
      BrandId: propertyRequest.brandId,
      Name: propertyRequest.name,
      LocationList: propertyRequest.locationList,
      IsBlocked: propertyRequest.isBlocked,
      MaxUh: propertyRequest.maxUh,
      IntegrationPartnerPropertyList: propertyRequest.integrationPartnerPropertyList,
      BrowserLanguage: this.i18nService.getLanguage()
    };
    return this.adminApi.post('property', params);
  }

  public updateHotel(propertyRequest: PropertyRequest) {
    const params = {
      CompanyId: propertyRequest.companyId,
      BrandId: propertyRequest.brandId,
      Name: propertyRequest.name,
      LocationList: propertyRequest.locationList,
      IsBlocked: propertyRequest.isBlocked,
      MaxUh: propertyRequest.maxUh,
      IntegrationPartnerPropertyList: propertyRequest.integrationPartnerPropertyList,
      BrowserLanguage: this.i18nService.getLanguage()
    };
    return this.adminApi.put(`property/${propertyRequest.id}`, params);
  }
}
