import { TestBed } from '@angular/core/testing';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { createServiceStub } from '../../../../../../testing/create-service-stub';
import { I18nService } from '@inovacaocmnet/thx-bifrost';
import { PropertyRequest } from '../models/property-request';

import { HotelResourceService } from './hotel-resource.service';
import { of } from 'rxjs';

describe('HotelResourceService', () => {
  let service: HotelResourceService;

  // @ts-ignore
  const adminApiServiceStub: Partial<AdminApiService> = {
    get: () => of( null ),
    post: () => of( null ),
    put: () => of( null )
  };
  const I18nProvider = createServiceStub(I18nService, {
    getLanguage(): string { return 'pt-BR'; }
  });

  let adminApiService: AdminApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HotelResourceService,
        {
          provide: AdminApiService,
          useValue: adminApiServiceStub
        },
        I18nProvider
      ]
    });
    service = TestBed.get(HotelResourceService);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call AdminApiService#get', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(adminApiService, 'get');
      service.list(mock);
      expect(adminApiService.get).toHaveBeenCalledWith('property', { params: mock });
    });
  });

  describe('#getById', () => {
    it('should call AdminApiService#get', () => {
      const id = 1;
      spyOn(adminApiService, 'get');
      service.getById(id);
      expect(adminApiService.get).toHaveBeenCalledWith(`property/${id}`);
    });
  });

  describe('#createHotel', () => {
    it('should call AdminApiService#post', () => {
      const mock: PropertyRequest = {
        companyId: 1,
        brandId: 1,
        locationList: [],
        name: 'name',
        isBlocked: false,
        maxUh: 0,
        integrationPartnerPropertyList: []
      };

      spyOn(adminApiService, 'post');
      service.createHotel(mock);
      expect(adminApiService.post).toHaveBeenCalledWith('property', {
        CompanyId: mock.companyId,
        BrandId: mock.brandId,
        Name: mock.name,
        LocationList: mock.locationList,
        IsBlocked: mock.isBlocked,
        MaxUh: mock.maxUh,
        IntegrationPartnerPropertyList: mock.integrationPartnerPropertyList,
        BrowserLanguage: 'pt-BR'
      });
    });
  });

  describe('#updateHotel', () => {
    it('should call AdminApiService#put', () => {
      const mock: PropertyRequest = {
        id: 1,
        name: '',
        locationList: [],
        brandId: 1,
        companyId: 2,
        isBlocked: false,
        maxUh: 0,
        integrationPartnerPropertyList: []
      };

      spyOn(adminApiService, 'put');
      service.updateHotel(mock);
      expect(adminApiService.put).toHaveBeenCalledWith(`property/${mock.id}`, {
        CompanyId: mock.companyId,
        BrandId: mock.brandId,
        Name: mock.name,
        LocationList: mock.locationList,
        IsBlocked: mock.isBlocked,
        MaxUh: mock.maxUh,
        IntegrationPartnerPropertyList: mock.integrationPartnerPropertyList,
        BrowserLanguage: 'pt-BR'
      });
    });
  });
});
