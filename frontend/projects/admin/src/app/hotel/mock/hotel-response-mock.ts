export const hotelResponseMock = {
  'id': 1,
  'propertyTypeId': 0,
  'name': 'Hotel Totvs Resorts Rio de Janeiro',
  'shortName': 'Hotel Totvs Resorts Rio de Janeiro',
  'tradeName': 'Hotelaria Accor Brasil S/A',
  'propertyUId': '6146da81-9cc3-46ef-b855-825551594bfe',
  'tenantId': '23eb803c-726a-4c7c-b25b-2c22a56793d9',
  'propertyStatusId': 0,
  'isBlocked': false,
  'maxUh': 100,
  'registeredUhs': 31,
  'locationList': [{
    'id': 1735,
    'ownerId': '6146da81-9cc3-46ef-b855-825551594bfe',
    'locationCategoryId': 4,
    'latitude': 0.000000,
    'longitude': 0.000000,
    'streetName': 'Rua Ajuratuba',
    'streetNumber': '111',
    'additionalAddressDetails': 'CASA',
    'neighborhood': 'Flodoaldo Pontes Pinto',
    'cityId': 45,
    'postalCode': 76820,
    'countryCode': 'BR',
    'subdivision': 'Porto Velho',
    'division': 'Rondônia',
    'country': 'Brasil',
    'locationCategory': {'id': 4, 'name': 'Residencial', '_expandables': []},
    'completeAddress': 'Rua Ajuratuba, 111 - CASA - Flodoaldo Pontes Pinto'
  }],
  'integrationPartnerPropertyList': [],
  'companyId': 1,
  'brandId': 1,
  'chainId': 1,
  'chainName': 'Rede Totvs Hoteis',
  'tenantName': 'Property Totvs',
  'brandName': 'Bandeira Totvs Hoteis Resorts',
  'companyUid': '28e91463-9182-4ca5-9ad4-4339e544b2b5',
  '_expandables': []
};
