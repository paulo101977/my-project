export const brandResponseMock = {
  'totalItems': 27,
  'hasNext': false,
  'items': [{
    'id': 19,
    'chainId': 1,
    'chainName': 'Rede Totvs Hoteis',
    'tenantId': 'a0db133a-f60d-45ea-aa59-d6173b2d8f8e',
    'tenantName': '11',
    'name': '11',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 21,
    'chainId': 2,
    'chainName': 'Rede Atlas Hoteis',
    'tenantId': '01227827-d3ab-4cb4-89c5-f99b9ff0fe21',
    'tenantName': '112',
    'name': '112',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 17,
    'chainId': 1,
    'chainName': 'Rede Totvs Hoteis',
    'tenantId': 'f4e0a43a-4349-46ae-b3a4-0c8497ddb102',
    'tenantName': '122121231',
    'name': '122121231',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 18,
    'chainId': 2,
    'chainName': 'Rede Atlas Hoteis',
    'tenantId': '6a9c5df7-7b86-4f2a-952f-04954fa2780b',
    'tenantName': '123123',
    'name': '123123',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 26,
    'chainId': 21,
    'chainName': '55/RIO Rede',
    'tenantId': '567919b3-b7ef-4d32-87ac-9318468c97b1',
    'tenantName': '55/RIO Hotel',
    'name': '55/RIO Bandeira',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 22,
    'chainId': 17,
    'chainName': 'X',
    'tenantId': 'a34b7828-9123-4948-a172-0bb0d41324c8',
    'tenantName': 'AAA',
    'name': 'AAAAAAAAA',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 20,
    'chainId': 1,
    'chainName': 'Rede Totvs Hoteis',
    'tenantId': '53a5a182-c6ca-4c0d-b3b9-775436078e86',
    'tenantName': 'aasdasd',
    'name': 'aasdasd',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 5,
    'chainId': 2,
    'chainName': 'Rede Atlas Hoteis',
    'tenantId': '23eb803c-726a-4c7c-b25b-2c22a5679323',
    'tenantName': 'Property Atlas',
    'name': 'Bandeira Atlas Hoteis Maste',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 23,
    'chainId': 18,
    'chainName': 'Rede Spartans',
    'tenantId': '3415a2c1-dc30-4ad2-8a17-06b94fe63b37',
    'tenantName': 'Bandeira Spartans',
    'name': 'Bandeira Spartans 2',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 24,
    'chainId': 19,
    'chainName': 'REDE TESTEE',
    'tenantId': '39c4fe30-debb-4e4a-857d-8e7acf0262d4',
    'tenantName': 'BANDEIRA TESTE',
    'name': 'BANDEIRA TESTEeee',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 2,
    'chainId': 1,
    'chainName': 'Rede Totvs Hoteis',
    'tenantId': '00000000-0000-0000-0000-000000000000',
    'tenantName': '',
    'name': 'Bandeira Totvs Hoteis Budget',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 3,
    'chainId': 1,
    'chainName': 'Rede Totvs Hoteis',
    'tenantId': '00000000-0000-0000-0000-000000000000',
    'tenantName': '',
    'name': 'Bandeira Totvs Hoteis Classic',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 1,
    'chainId': 1,
    'chainName': 'Rede Totvs Hoteis',
    'tenantId': '00000000-0000-0000-0000-000000000000',
    'tenantName': '',
    'name': 'Bandeira Totvs Hoteis Resorts',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 4,
    'chainId': 1,
    'chainName': 'Rede Totvs Hoteis',
    'tenantId': '23eb803c-726a-4c7c-b25b-2c22a56793d9',
    'tenantName': 'Property Totvs',
    'name': 'Bandeira Totvs Hoteis Style',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 8,
    'chainId': 5,
    'chainName': 'Rede Caio',
    'tenantId': '956b8941-f583-438d-96c7-a994a1e137a3',
    'tenantName': 'Caio',
    'name': 'Caio Brand',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 12,
    'chainId': 9,
    'chainName': 'Rede Celio Junior',
    'tenantId': '3ae94396-d993-4d68-8b51-9d20fbf6b187',
    'tenantName': 'Celio Junior',
    'name': 'Celio Junior Brand',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 11,
    'chainId': 8,
    'chainName': 'Rede Diego',
    'tenantId': '43887a06-4f2e-43c5-b433-c790b74c30e6',
    'tenantName': 'Diego',
    'name': 'Diego Brand',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 10,
    'chainId': 7,
    'chainName': 'Rede Elton',
    'tenantId': '8bd110d7-2790-4fd3-88a2-508f7c1d9840',
    'tenantName': 'Elton',
    'name': 'Elton Brand',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 27,
    'chainId': 22,
    'chainName': 'Homolog Rede',
    'tenantId': '6f27f3cc-ed24-41d7-80f1-ff44879d6e29',
    'tenantName': 'Homolog Bandeira',
    'name': 'Homolog Bandeira',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 15,
    'chainId': 12,
    'chainName': 'Rede Juliana Barbosa',
    'tenantId': '534a2208-e8ee-455b-8eae-90e4960324a7',
    'tenantName': 'Juliana Barbosa',
    'name': 'Juliana Barbosa Brand',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 13,
    'chainId': 10,
    'chainName': 'Rede Moacyr Cavallo',
    'tenantId': '55a65986-8405-43e0-a0c3-530405117a83',
    'tenantName': 'Moacyr Cavallo',
    'name': 'Moacyr Cavallo Brand',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 16,
    'chainId': 2,
    'chainName': 'Rede Atlas Hoteis',
    'tenantId': '36ee2354-c390-4ae1-b816-a8e8feee288d',
    'tenantName': 'Novo Brand 3',
    'name': 'Novo Brand 3',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 25,
    'chainId': 20,
    'chainName': 'Premium Rede',
    'tenantId': '0a2f77e5-604d-4dee-a1ec-fc88f74ff6d3',
    'tenantName': 'Premium Hotel',
    'name': 'Premium Bandeira',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 14,
    'chainId': 11,
    'chainName': 'Rede Rafael Dias',
    'tenantId': '43052d50-5ad1-401b-b4a4-bd83e953c350',
    'tenantName': 'Rafael Dias',
    'name': 'Rafael Dias Brand',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 9,
    'chainId': 6,
    'chainName': 'Rede Ricardo',
    'tenantId': 'a18e2780-8c6a-4012-9180-38f232116692',
    'tenantName': 'Ricardo',
    'name': 'Ricardo Brand',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 6,
    'chainId': 3,
    'chainName': 'Rede Saulo brito',
    'tenantId': '95a4049e-34a9-4b46-b5c4-917b2f19973a',
    'tenantName': 'Saulo Brito',
    'name': 'Saulo Brito Brand',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }, {
    'id': 7,
    'chainId': 4,
    'chainName': 'Rede Yoseph',
    'tenantId': '56244479-58d0-4976-adff-75318c058716',
    'tenantName': 'Yoseph',
    'name': 'Yoseph Brand',
    'isTemporary': false,
    'propertyList': [],
    '_expandables': []
  }]
};
