import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'admin-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  @Input() title: string;
  @Input() isOpen = false;
  @Output() opened = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  toggleContentVisibility() {
    this.isOpen = !this.isOpen;
    this.opened.emit(this.isOpen);
  }
}
