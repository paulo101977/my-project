import { ThfPageAction, ThfPageFilter } from '@totvs/thf-ui';

export interface PageSettings {
  title: string;
  actions: ThfPageAction[];
  filterSettings: ThfPageFilter;
}
