export interface LocationRequest {
  id?: number;
  browserLanguage: string;
  locationCategoryId: string;
  latitude: string;
  longitude: string;
  streetName: string;
  streetNumber: string;
  additionalAddressDetails?: any;
  neighborhood: string;
  cityId: string;
  postalCode: string;
  countryCode: string;
  subdivision: string;
  division: string;
  country: string;
}
