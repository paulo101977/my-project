import { ThfTableColumn, ThfTableLiterals } from '@totvs/thf-ui';

export interface TableSettings {
 items: any[];
 columns: ThfTableColumn[];
 striped: boolean;
 sort: boolean;
 loading: boolean;
 showMore: Function;
 hasMoreToShow: boolean;
 height: number;
 literals: ThfTableLiterals;
 checkbox?: boolean;
}
