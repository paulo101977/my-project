import { ThfModalAction } from '@totvs/thf-ui';

export interface ModalSettings {
  title: string;
  primaryAction?: ThfModalAction;
  secondaryAction?: ThfModalAction;
  size?: string;
  clickOut?: boolean;
}
