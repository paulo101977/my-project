import { TranslateService } from '@ngx-translate/core';
import { ThfMenuItem } from '@totvs/thf-ui/components/thf-menu';
import { Component, OnInit } from '@angular/core';
import { ThfToolbarAction, ThfToolbarProfile } from '@totvs/thf-ui';
import { Router } from '@angular/router';

@Component({
  selector: 'admin-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.css']
})
export class MainContainerComponent implements OnInit {
  menuItems: Array<ThfMenuItem> = [];
  actions: Array<ThfToolbarAction> = [];
  profileActions: Array<ThfToolbarAction> = [];
  profile: ThfToolbarProfile;
  title: string;

  constructor(
    private router: Router,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setMenuConfig();
    this.setToolbarConfig();
  }

  setMenuConfig() {
    this.menuItems = [
      {
        label: 'Dashboard',
        link: 'dashboard',
        shortLabel: 'Dashboard',
        icon: 'thf-icon-home'
      },
      // TODO uncomment when needed
      // {
      //   label: this.translate.instant('menu.profileManager'),
      //   link: 'profile',
      //   icon: 'thf-icon-user',
      //   shortLabel: this.translate.instant('menu.profileManager')
      // },
      {
        label: this.translate.instant('menu.config'),
        icon: 'thf-icon-settings',
        shortLabel: this.translate.instant('menu.config'),
        subItems: [
          {
            label: this.translate.instant('menu.chain'),
            link: 'config/chain',
            shortLabel: this.translate.instant('menu.chain')
          },
          {
            label: this.translate.instant('menu.brand'),
            link: 'config/brand',
            shortLabel: this.translate.instant('menu.brand')
          },
          {
            label: this.translate.instant('menu.company'),
            link: 'config/company',
            shortLabel: this.translate.instant('menu.company')
          },
          {
            label: this.translate.instant('menu.hotel'),
            link: 'config/hotel',
            shortLabel: this.translate.instant('menu.hotel')
          },
          {
            label: this.translate.instant('menu.user'),
            link: 'config/user',
            shortLabel: this.translate.instant('menu.user')
          }
        ]
      },
    ];
  }

  setToolbarConfig() {
    this.title = 'User name';
    this.profile = {
      avatar: 'https://thf.totvs.com.br/assets/graphics/logo-thf.png',
      subtitle: 'dev@totvs.com.br',
      title: 'Mr. Dev Totvs'
    };
    this.profileActions = [
      {
        icon: 'thf-icon-user', label: 'User data', action: item => () => {
        }
      },
      {
        icon: 'thf-icon-company', label: 'Company data', action: item => () => {
        }
      },
      {
        icon: 'thf-icon-settings', label: 'Settings', action: item => () => {
        }
      },
      {
        icon: 'thf-icon-exit', label: 'Exit', type: 'danger', separator: true, action: item => () => {
        }
      }
    ];
  }

}
