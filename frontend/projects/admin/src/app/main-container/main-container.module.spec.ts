import { MainContainerModule } from './main-container.module';

describe('MainContainerModule', () => {
  let mainContainerModule: MainContainerModule;

  beforeEach(() => {
    mainContainerModule = new MainContainerModule();
  });

  it('should create an instance', () => {
    expect(mainContainerModule).toBeTruthy();
  });
});
