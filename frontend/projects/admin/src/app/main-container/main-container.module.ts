import { ThfModule } from '@totvs/thf-ui';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MainContainerComponent } from './main-container.component';
import { MainContainerRoutingModule } from './main-container-routing.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ThfModule,
    MainContainerRoutingModule
  ],
  declarations: [MainContainerComponent]
})
export class MainContainerModule { }
