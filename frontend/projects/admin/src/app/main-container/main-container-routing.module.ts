import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainContainerComponent } from './main-container.component';

const routes: Routes = [
  {
    path: '',
    component: MainContainerComponent,
    children: [
      {path: '', redirectTo: 'dashboard'},
      {path: 'dashboard', loadChildren: './../dashboard/dashboard.module#DashboardModule'},
      {path: 'config', loadChildren: './../contract-config/contract-config.module#ContractConfigModule'},
      // TODO uncomment when needed
      // {path: 'profile', loadChildren: './../profile/profile.module#ProfileModule'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainContainerRoutingModule {
}
