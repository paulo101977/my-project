import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';

import { MainContainerComponent } from './main-container.component';

describe('MainContainerComponent', () => {
  let component: MainContainerComponent;
  let fixture: ComponentFixture<MainContainerComponent>;

  @Component({selector: 'thf-toolbar', template: ''}) // tslint:disable-line
  class ToolbarStubComponent {}

  @Component({selector: 'thf-menu', template: ''}) // tslint:disable-line
  class MenuStubComponent {}

  const translateServiceStub: Partial<TranslateService> = {
    instant(key: string | Array<string>, interpolateParams?: Object): string | any { return null; }
  };
  let translateService: TranslateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [ MainContainerComponent, ToolbarStubComponent, MenuStubComponent ],
      providers: [
        {
          provide: TranslateService,
          useValue: translateServiceStub
        }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();

    translateService = TestBed.get(TranslateService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
