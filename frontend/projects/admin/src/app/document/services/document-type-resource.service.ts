import { Injectable } from '@angular/core';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { ClientDocumentTypesDto } from 'app/shared/models/dto/client/client-document-types';
import { Observable } from 'rxjs';
import { SupportApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: 'root'
})
export class DocumentTypeResourceService {

  constructor(
    private api: SupportApiService
  ) { }

  public getAll(): Observable<any> {
    return this.api.get<ItemsResponse<ClientDocumentTypesDto>>(`Documenttypes`);
  }
}
