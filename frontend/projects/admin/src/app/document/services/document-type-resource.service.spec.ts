import { TestBed, inject } from '@angular/core/testing';
import { createServiceStub } from '../../../../../../testing/create-service-stub';
import { SupportApiService } from '@inovacaocmnet/thx-bifrost';

import { DocumentTypeResourceService } from './document-type-resource.service';

describe('DocumentTypeResourceService', () => {
  const SupportApiProvider = createServiceStub(SupportApiService);
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DocumentTypeResourceService,
        SupportApiProvider
      ]
    });
  });

  it('should be created', inject([DocumentTypeResourceService], (service: DocumentTypeResourceService) => {
    expect(service).toBeTruthy();
  }));
});
