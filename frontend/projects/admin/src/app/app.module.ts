import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {
  AdminApiModule,
  ThexApiModule,
  configureApi,
  configureAppName,
  I18nModule,
  SupportApiModule
} from '@inovacaocmnet/thx-bifrost';
import { ThfModule } from '@totvs/thf-ui';
import { environment } from '@environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { httpInterceptorProviders } from './interceptors/http-interceptors.providers';

const ApiConfigProvider = configureApi(environment.apiConfig);
const AppNameConfigProvider = configureAppName('admin');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    I18nModule.forRoot(),
    // TODO: mover esse import para dentro do bifrost
    HttpClientModule,
    AdminApiModule,
    ThexApiModule,
    SupportApiModule,
    ThfModule
  ],
  providers: [ApiConfigProvider, ...httpInterceptorProviders, AppNameConfigProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
