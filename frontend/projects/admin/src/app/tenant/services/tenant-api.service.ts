import { Injectable } from '@angular/core';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { TenantResourceService } from '../resources/tenant-resource.service';

@Injectable({
  providedIn: 'root'
})
export class TenantApiService {

  constructor(private resource: TenantResourceService) { }

  public list(params: ListQueryParams) {
    return this.resource.list(params);
  }

  public createTenant(name: string) {
    return this.resource.createTenant(name);
  }

  public updateTenant(id: number, name: string) {
    return this.resource.updateTenant(id, name);
  }
}
