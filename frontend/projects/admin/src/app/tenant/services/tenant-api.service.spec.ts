import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { TenantResourceService } from '../resources/tenant-resource.service';
import { TenantApiService } from './tenant-api.service';

describe('TenantApiService', () => {
  let service: TenantResourceService;
  // @ts-ignore
  const tenantResourceStubService: Partial<TenantResourceService> = {
    list: () => of(null),
    createTenant(name: string): Observable<any> { return null; },
    updateTenant(id: number, name: string): Observable<any> { return null; }
  };
  let tenantResourceService: TenantResourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TenantApiService,
        {
          provide: TenantResourceService,
          useValue: tenantResourceStubService
        }
      ]
    });

    service = TestBed.get(TenantApiService);
    tenantResourceService = TestBed.get(TenantResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call TenantResourceService#list', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(tenantResourceService, 'list');

      service.list(mock);
      expect(tenantResourceService.list).toHaveBeenCalledWith(mock);
    });
  });

  describe('#createTenant', () => {
    it('should call TenantResourceService#list', () => {
      const tenantName = 'name';
      spyOn(tenantResourceService, 'createTenant');

      service.createTenant(tenantName);
      expect(tenantResourceService.createTenant).toHaveBeenCalledWith(tenantName);
    });
  });

  describe('#updateTenant', () => {
    it('should call TenantResourceService#list', () => {
      const id = 123;
      const tenantName = 'name';
      spyOn(tenantResourceService, 'updateTenant');

      service.updateTenant(id, tenantName);
      expect(tenantResourceService.updateTenant).toHaveBeenCalledWith(id, tenantName);
    });
  });
});
