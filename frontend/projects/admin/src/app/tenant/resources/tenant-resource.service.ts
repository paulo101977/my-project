import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { ListQueryParams } from '../../shared/models/list-query-params';

@Injectable({
  providedIn: 'root'
})
export class TenantResourceService {

  constructor(private adminApi: AdminApiService) { }

  public list(params: ListQueryParams) {
    return this.adminApi.get<any>('tenant', { params });
  }

  public listWithoutAssociation(params) {
    return this.adminApi.get('tenant/withoutassociation', { params });
  }

  public getById(id) {
    return this.adminApi.get(`tenant/${id}`);
  }

  public createTenant(name: string) {
    const params = {
      TenantName: name
    };
    return this.adminApi.post('tenant', params);
  }

  public updateTenant(id: number, name: string) {
    const params = {
      id: id,
      tenantName: name
    };
    return this.adminApi.put(`tenant/${id}`, params);
  }
}
