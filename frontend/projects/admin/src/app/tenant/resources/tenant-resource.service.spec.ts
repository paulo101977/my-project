import { TestBed } from '@angular/core/testing';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';

import { TenantResourceService } from './tenant-resource.service';
import { of } from 'rxjs';

describe('TenantResourceService', () => {
  let service: TenantResourceService;
  // @ts-ignore
  const adminApiServiceStub: Partial<AdminApiService> = {
    get: () => of( null ),
    post: () => of( null ),
    put: () => of( null )
  };
  let adminApiService: AdminApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TenantResourceService,
        {
          provide: AdminApiService,
          useValue: adminApiServiceStub
        }
      ]
    });

    service = TestBed.get(TenantResourceService);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call AdminApiService#get', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(adminApiService, 'get');

      service.list(mock);
      expect(adminApiService.get).toHaveBeenCalledWith('tenant', { params: mock });
    });
  });

  describe('#listWithoutAssociation', () => {
    it('should call AdminApiService#get', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(adminApiService, 'get');

      service.listWithoutAssociation(mock);
      expect(adminApiService.get).toHaveBeenCalledWith('tenant/withoutassociation', { params: mock });
    });
  });

  describe('#getById', () => {
    it('should call AdminApiService#get', () => {
      const id = 1;
      spyOn(adminApiService, 'get');

      service.getById(id);
      expect(adminApiService.get).toHaveBeenCalledWith(`tenant/${id}`);
    });
  });

  describe('#createTenant', () => {
    it('should call AdminApiService#post', () => {
      const mock = { TenantName: 'name' };
      spyOn(adminApiService, 'post');

      service.createTenant(mock.TenantName);
      expect(adminApiService.post).toHaveBeenCalledWith('tenant', mock);
    });
  });

  describe('#updateTenant', () => {
    it('should call AdminApiService#put', () => {
      const mock = { id: 1, tenantName: 'name' };
      spyOn(adminApiService, 'put');

      service.updateTenant(mock.id, mock.tenantName);
      expect(adminApiService.put).toHaveBeenCalledWith(`tenant/${mock.id}`, mock);
    });
  });
});
