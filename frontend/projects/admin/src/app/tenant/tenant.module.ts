import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ThfButtonModule, ThfFieldModule, ThfModalModule, ThfNotificationModule } from '@totvs/thf-ui';
import { PageListWrapperModule } from '../widgets/page-list-wrapper/page-list-wrapper.module';
import { CreateComponent } from './components/create/create.component';
import { TenantListPageComponent } from './components/tenant-list-page/tenant-list-page.component';
import { UpdateComponent } from './components/update/update.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PageListWrapperModule,
    ThfModalModule,
    ThfButtonModule,
    ThfFieldModule,
    ThfNotificationModule
  ],
  declarations: [CreateComponent, UpdateComponent, TenantListPageComponent],
  exports: [CreateComponent, UpdateComponent, TenantListPageComponent],
})
export class TenantModule { }
