import { Location } from '@angular/common';
import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfModalComponent, ThfNotificationService, ThfPageAction } from '@totvs/thf-ui';
import { Observable } from 'rxjs';
import { PageSettings } from '../../../shared/models/page-settings';
import { PageTableSettings } from '../../../widgets/page-list-wrapper/models/page-table-settings';
import { PageListWrapperComponent } from '../../../widgets/page-list-wrapper/page-list-wrapper.component';
import { TenantApiService } from '../../services/tenant-api.service';

@Component({
  selector: 'admin-tenant-list-page',
  templateUrl: './tenant-list-page.component.html',
  styleUrls: ['./tenant-list-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TenantListPageComponent implements OnInit {
  @ViewChild('modal') modal: ThfModalComponent;
  @ViewChild('pageList') pageListWrapper: PageListWrapperComponent;
  @Input() hasBackAction = false;

  public pageSettings: PageSettings;
  public pageTableSettings: PageTableSettings;
  public modalSettings: any = {};
  public form: FormGroup;

  public i18n: any;

  constructor(
    private location: Location,
    private translate: TranslateService,
    private tenantApi: TenantApiService,
    private fb: FormBuilder,
    private notification: ThfNotificationService
  ) {}

  ngOnInit(): void {
    this.setI18n();
    this.createForm();
    this.setPageSettings();
    this.setPageTableSettings();
    this.setModalSettings();
  }

  private setPageSettings() {
    this.pageSettings = {
      title: this.i18n.title.tenant,
      actions: this.getActions(),
      filterSettings: {
        placeholder: this.i18n.placeholder.search
      }
    };
  }

  private setPageTableSettings() {
    this.pageTableSettings = {
      columns: [
        { property: 'tenantName', width: '23%', label: this.i18n.label.tenant, type: 'string' },
        { property: 'chainName', width: '22%', label: this.i18n.label.chain, type: 'string' },
        { property: 'brandName', width: '23%', label: this.i18n.label.brand, type: 'string' },
        { property: 'propertyName', width: '22%', label: this.i18n.label.property, type: 'string' },
        {
          property: 'actions',
          label: this.i18n.label.actions,
          type: 'icon',
          width: '10%',
          icons: [
            { value: 'edit', icon: 'thf-icon-edit', action: (item) => this.openEditModal(item) }
          ]
        }
      ],
      transformData: this.transformData,
      getList: this.getList.bind(this),
    };
  }

  public getList(params) {
    return this.tenantApi.list(params);
  }

  public transformData(response) {
    return {
      ...response,
      items: response.items.map((item) => {
        item.actions = ['edit'];
        return item;
      })
    };
  }

  private getActions() {
    const backAction: ThfPageAction = {
      label: this.i18n.action.back,
      icon: 'thf-icon-arrow-left',
      action: () => { this.location.back(); }
    };

    return [
      {
        label: this.i18n.action.insert,
        icon: 'thf-icon-plus',
        action: () => { this.openInsertModal(); }
      },
      ...(this.hasBackAction ? [backAction] : [])
    ];
  }

  private openInsertModal() {
    this.changeModalToInsertMode();
    this.modal.open();
  }

  private setI18n() {
    this.i18n = {
      title: {
        tenant: this.translate.instant('title.tenant'),
        insertTenant: this.translate.instant('title.insertTenant'),
        editTenant: this.translate.instant('title.editTenant'),
      },
      placeholder: {
        search: this.translate.instant('placeholder.searchTenantChainBrandOrHotel')
      },
      label: {
        tenant: this.translate.instant('label.tenant'),
        chain: this.translate.instant('label.chain'),
        brand: this.translate.instant('label.brand'),
        hotel: this.translate.instant('label.hotel'),
        actions: this.translate.instant('label.actions'),
        tenantName: this.translate.instant('label.tenantName')
      },
      action: {
        back: this.translate.instant('action.back'),
        confirm: this.translate.instant('action.confirm'),
        cancel: this.translate.instant('action.cancel'),
        insert: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.tenant')
        })
      },
      variable: {
        createSuccess: this.translate.instant('variable.lbSaveSuccessM', {
          labelName: this.translate.instant('label.tenant')
        }),
        updateSuccess: this.translate.instant('variable.lbEditSuccessM', {
          labelName: this.translate.instant('label.tenant')
        }),
      }
    };
  }

  private setModalSettings() {
    this.modalSettings = {
      title: '',
      input: {
        label: this.i18n.label.tenantName
      },
      confirm: {
        label: this.i18n.action.confirm,
        disabled: this.form.invalid,
        action: () => { this.submit(); }
      },
      cancel: {
        label: this.i18n.action.cancel,
        action: () => { this.modal.close(); }
      },
      size: 'sm',
      clickOut: true
    };

    this.form.statusChanges.subscribe(() => {
      this.modalSettings.confirm.disabled = this.form.invalid;
    });
  }

  private changeModalToInsertMode() {
    this.resetForm();
    this.modalSettings = {
      ...this.modalSettings,
      title: this.i18n.title.insertTenant,
    };
  }

  private createForm() {
    this.form = this.fb.group({
      tenantId: [''],
      tenantName: ['', Validators.required]
    });
  }

  private resetForm(item: any = {}) {
    this.form.reset({
      tenantId: item.id || '',
      tenantName: item.tenantName || ''
    });
  }

  private openEditModal(item) {
    this.changeModalToEditMode(item);
    this.modal.open();
  }

  private changeModalToEditMode(item) {
    this.resetForm(item);
    this.modalSettings = {
      ...this.modalSettings,
      title: this.i18n.title.editTenant,
    };
  }

  private setModalToLoadConfirmState(loadingState: boolean) {
    this.modalSettings = {
      ...this.modalSettings,
      confirm: {
        ...this.modalSettings.confirm,
        loading: loadingState
      }
    };
  }

  private submit() {
    if (this.form.invalid) {
      return;
    }

    const { createSuccess, updateSuccess } = this.i18n.variable;
    const shouldCreate = !this.form.value.tenantId;
    const operation = shouldCreate ? this.createTenant(this.form.value) : this.updateTenant(this.form.value);
    const successMessage = shouldCreate ? createSuccess : updateSuccess;

    this.setModalToLoadConfirmState(true);

    operation
      .subscribe(() => {
        this.setModalToLoadConfirmState(false);
        this.modal.close();
        this.pageListWrapper.reloadList();
        this.notification.success(successMessage);
      });
  }

  private createTenant({tenantName}): Observable<any> {
    return this.tenantApi.createTenant(tenantName);
  }

  private updateTenant({tenantId, tenantName}): Observable<any> {
    return this.tenantApi.updateTenant(tenantId, tenantName);
  }
}
