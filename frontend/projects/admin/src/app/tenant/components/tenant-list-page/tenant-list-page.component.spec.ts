import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { Location } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfNotificationService } from '@totvs/thf-ui';
import { TenantApiService } from '../../services/tenant-api.service';

import { TenantListPageComponent } from './tenant-list-page.component';
import { of } from 'rxjs';

describe('TenantListPageComponent', () => {
  let component: TenantListPageComponent;
  let fixture: ComponentFixture<TenantListPageComponent>;

  // @ts-ignore
  const locationStub: Partial<Location> = {
    back: () => {}
  };
  let location: Location;

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {}
  };
  let translateService: TranslateService;

  // @ts-ignore
  const tenantApiStubService: Partial<TenantApiService> = {
    list: () => of( null )
  };
  let tenantApiService: TenantApiService;

  // @ts-ignore
  const notificationServiceStub: Partial<ThfNotificationService> = {
    success: () => {}
  };
  let notificationService: ThfNotificationService;

  let formBuilder: FormBuilder;

  @Component({selector: 'admin-page-list-wrapper', template: ''})
  class PageListWrapperStubComponent {}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TenantListPageComponent, PageListWrapperStubComponent ],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: Location,
          useValue: locationStub
        },
        {
          provide: TenantApiService,
          useValue: tenantApiStubService
        },
        {
          provide: ThfNotificationService,
          useValue: notificationServiceStub
        },
        FormBuilder
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();

    location = TestBed.get(Location);
    translateService = TestBed.get(TranslateService);
    tenantApiService = TestBed.get(TenantApiService);
    notificationService = TestBed.get(ThfNotificationService);
    formBuilder = TestBed.get(FormBuilder);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
