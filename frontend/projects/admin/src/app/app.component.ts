import { Component } from '@angular/core';
import { I18nService } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'admin-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {
  constructor(private i18nService: I18nService) {
    this.i18nService.setInitialLanguage();
  }
}
