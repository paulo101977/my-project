export interface IntegrationPartnerProperty {
  partnerId: number;
  integrationCode: string;
  isActive: boolean;
}
