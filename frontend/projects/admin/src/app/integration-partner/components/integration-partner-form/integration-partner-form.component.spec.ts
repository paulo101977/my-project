import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

import { IntegrationPartnerFormComponent } from './integration-partner-form.component';

describe('IntegrationPartnerFormComponent', () => {
  let component: IntegrationPartnerFormComponent;
  let fixture: ComponentFixture<IntegrationPartnerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ IntegrationPartnerFormComponent ],
      imports: [ TranslateTestingModule ],
      providers: [ FormBuilder ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationPartnerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
