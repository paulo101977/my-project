import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfModalAction, ThfModalComponent } from '@totvs/thf-ui';
import { from, Observable } from 'rxjs';
import { take, toArray } from 'rxjs/operators';
import { toThfOptions } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'admin-integration-partner-form',
  templateUrl: './integration-partner-form.component.html',
  styleUrls: ['./integration-partner-form.component.css']
})
export class IntegrationPartnerFormComponent implements OnInit, OnChanges {
  @ViewChild('modal') modal: ThfModalComponent;
  @Input() integrationPartners: any[] = [];
  @Input() selectedIntegrationPartners: any[] = [];
  @Output() submitted = new EventEmitter<any[]>();

  private partnerIndex: number;
  public form: FormGroup;
  public i18n: any;
  public integrationPartnersOptions$: Observable<any[]>;
  public primaryAction: ThfModalAction = {
    action: () => this.submitForm(),
    label: this.translate.instant('action.confirm'),
  };
  public secondaryAction: ThfModalAction = {
    action: () => this.cancelSubmit(),
    label: this.translate.instant('action.cancel')
  };

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.setI18n();
    this.setForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.onIntegrationPartnersChanges(changes);
  }

  public open(partnerIndex: number = -1, integrationPartner?: any) {
    this.partnerIndex = partnerIndex;
    this.form.reset({
      isActive: true,
      ...(integrationPartner ? integrationPartner : {}),
    });
    this.form.markAsPristine();
    this.form.markAsUntouched();
    this.modal.open();
  }

  public close() {
    this.modal.close();
  }

  public submitForm() {
    if (this.form.invalid) {
      return;
    }

    const {partnerId, ...formValue} = this.form.value;
    const integrationPartner = {
      partnerIndex: this.partnerIndex,
      ...formValue,
      partnerId,
      ...this.integrationPartners.find(integration => integration.id == partnerId)
    };

    this.submitted.emit(integrationPartner);
    this.close();
  }

  public cancelSubmit() {
    this.close();
  }

  private setI18n() {
    this.i18n = {
      modalTitle: this.translate.instant('action.lbAssociate', {
        labelName: this.translate.instant('label.integrationPartner')
      }),
      label: {
        integrationCode: this.translate.instant('label.integrationCode'),
        integrationPartner: this.translate.instant('label.integrationPartner'),
        active: this.translate.instant('label.active')
      }
    };
  }

  private setForm() {
    this.form = this.formBuilder.group({
      integrationCode: [null, Validators.required],
      partnerId: [null, Validators.required],
      isActive: [true]
    }, {
      validator: [this.uniqueIntegrationPartner()]
    });
  }

  private onIntegrationPartnersChanges({integrationPartners}: SimpleChanges) {
    const hasValue: boolean = !!(integrationPartners && integrationPartners.currentValue);

    if (hasValue) {
      this.integrationPartnersOptions$ = from(this.integrationPartners)
        .pipe(
          toThfOptions('partnerName', 'id'),
          toArray(),
          take(1)
        );
    }
  }

  private uniqueIntegrationPartner(): ValidatorFn {
    return (control: AbstractControl) => {
      if (!this.selectedIntegrationPartners || !this.selectedIntegrationPartners.length) {
        return null;
      }

      const { integrationCode, partnerId } = control.value;
      const isUnique = !this.selectedIntegrationPartners.some(selectedPartner => {
        const hasEqualIntegrationCode = selectedPartner.integrationCode === integrationCode;
        const hasEqualpartnerId = selectedPartner.partnerId === partnerId;
        return hasEqualIntegrationCode && hasEqualpartnerId;
      });

      return isUnique ? null : {uniqueIntegrationPartner: true};
    };
  }
}
