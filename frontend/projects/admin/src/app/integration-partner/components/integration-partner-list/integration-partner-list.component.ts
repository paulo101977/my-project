import { Component, Input } from '@angular/core';
import { ThfTableColumnIcon } from '@totvs/thf-ui';
import { TableSettings } from '../../../shared/models/table-settings';

@Component({
  selector: 'admin-integration-partner-list',
  templateUrl: './integration-partner-list.component.html',
  styleUrls: ['./integration-partner-list.component.css']
})
export class IntegrationPartnerListComponent {
  @Input() integrationPartners: any[];
  @Input() columns: string[] = [];
  @Input() actions: ThfTableColumnIcon[] = [];
  @Input() tableSettings: Partial<TableSettings>;

  public tableColumns = [
    { property: 'partnerName', label: 'label.partnerName', type: 'string' },
    { property: 'integrationCode', label: 'label.integrationCode', type: 'string' },
    { property: 'activeStatus', label: 'label.isActive', type: 'string' },
  ];
}
