import { IntegrationPartnerModule } from './integration-partner.module';

describe('IntegrationPartnerModule', () => {
  let partnerModule: IntegrationPartnerModule;

  beforeEach(() => {
    partnerModule = new IntegrationPartnerModule();
  });

  it('should create an instance', () => {
    expect(partnerModule).toBeTruthy();
  });
});
