import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ThfButtonModule, ThfFieldModule, ThfModalModule } from '@totvs/thf-ui';
import { BasicListWrapperModule } from '../widgets/basic-list-wrapper/basic-list-wrapper.module';
import {
  IntegrationPartnerListComponent
} from './components/integration-partner-list/integration-partner-list.component';
import {
  IntegrationPartnerFormComponent
} from './components/integration-partner-form/integration-partner-form.component';

@NgModule({
  imports: [
    CommonModule,
    BasicListWrapperModule,
    ReactiveFormsModule,
    ThfModalModule,
    ThfFieldModule,
    ThfButtonModule
  ],
  declarations: [IntegrationPartnerListComponent, IntegrationPartnerFormComponent],
  exports: [IntegrationPartnerListComponent, IntegrationPartnerFormComponent]
})
export class IntegrationPartnerModule { }
