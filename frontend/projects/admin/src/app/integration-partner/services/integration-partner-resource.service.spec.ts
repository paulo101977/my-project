import { TestBed, inject } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing/create-service-stub';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';

import { IntegrationPartnerResourceService } from './integration-partner-resource.service';

describe('IntegrationPartnerResourceService', () => {
  const AdminApiProvider = createServiceStub(AdminApiService, {
    get<T>(path, options?): Observable<any> { return of(null); }
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        IntegrationPartnerResourceService,
        AdminApiProvider
      ]
    });
  });

  it('should be created', inject([IntegrationPartnerResourceService], (service: IntegrationPartnerResourceService) => {
    expect(service).toBeTruthy();
  }));
});
