import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IntegrationPartnerResourceService {

  constructor(
    private adminApi: AdminApiService
  ) { }

  listAll(): Observable<any> {
    return this.adminApi.get('IntegrationPartner/getallpartners');
  }
}
