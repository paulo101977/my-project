import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { I18nService } from '@inovacaocmnet/thx-bifrost';
import { AppComponent } from './app.component';
import { BrowserTestingModule } from '@angular/platform-browser/testing';

describe('AppComponent', () => {
  const i18nServiceStub: Partial<I18nService> = {
    setInitialLanguage(): void {}
  };
  let i18nService: I18nService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, BrowserTestingModule ],
      declarations: [
        AppComponent
      ],
      providers: [
        {
          provide: I18nService,
          useValue: i18nServiceStub
        }
      ]
    }).compileComponents();

    i18nService = TestBed.get(I18nService);
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
