import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WizardUserComponent } from './wizard-user.component';

const routes: Routes = [
  {
    path: '',
    component: WizardUserComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WizardUserRoutingModule {}
