import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WizardUserComponent } from './wizard-user.component';
import { WizardUserRoutingModule } from './wizard-user-routing.module';

@NgModule({
  imports: [
    CommonModule,
    WizardUserRoutingModule
  ],
  declarations: [WizardUserComponent]
})
export class WizardUserModule { }
