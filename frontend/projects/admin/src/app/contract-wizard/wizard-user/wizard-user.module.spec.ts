import { WizardUserModule } from './wizard-user.module';

describe('WizardUserModule', () => {
  let wizardUserModule: WizardUserModule;

  beforeEach(() => {
    wizardUserModule = new WizardUserModule();
  });

  it('should create an instance', () => {
    expect(wizardUserModule).toBeTruthy();
  });
});
