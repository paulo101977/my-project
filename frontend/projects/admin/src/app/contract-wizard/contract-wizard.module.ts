import { ThfModule } from '@totvs/thf-ui';
import { ContractWizardRoutingModule } from './contract-wizard-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WizardComponent } from './wizard/wizard.component';
import { RouterModule } from '@angular/router';
import { StepMenuModule } from '@inovacao-cmnet/thx-ui';

@NgModule({
  imports: [
    CommonModule,
    ContractWizardRoutingModule,
    RouterModule,
    ThfModule,
    StepMenuModule
  ],
  declarations: [WizardComponent]
})
export class ContractWizardModule {
  constructor() {
    console.log('CONTRACT MODULE LOADED');
  }
}
