import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WizardHotelComponent } from './wizard-hotel.component';

const routes: Routes = [
  {
    path: '',
    component: WizardHotelComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WizardHotelRoutingModule {}
