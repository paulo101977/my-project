import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardHotelComponent } from './wizard-hotel.component';

describe('WizardHotelComponent', () => {
  let component: WizardHotelComponent;
  let fixture: ComponentFixture<WizardHotelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardHotelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardHotelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
