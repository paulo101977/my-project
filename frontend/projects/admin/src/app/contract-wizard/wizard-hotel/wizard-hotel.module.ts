import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WizardHotelComponent } from './wizard-hotel.component';
import { WizardHotelRoutingModule } from './wizard-hotel-routing.module';

@NgModule({
  imports: [
    CommonModule,
    WizardHotelRoutingModule
  ],
  declarations: [WizardHotelComponent]
})
export class WizardHotelModule { }
