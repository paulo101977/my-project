import { WizardHotelModule } from './wizard-hotel.module';

describe('WizardHotelModule', () => {
  let wizardHotelModule: WizardHotelModule;

  beforeEach(() => {
    wizardHotelModule = new WizardHotelModule();
  });

  it('should create an instance', () => {
    expect(wizardHotelModule).toBeTruthy();
  });
});
