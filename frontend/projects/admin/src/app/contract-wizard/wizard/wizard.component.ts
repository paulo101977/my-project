import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { StepMenuItem, StepMenuItemState } from '@inovacao-cmnet/thx-ui';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'admin-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.css']
})
export class WizardComponent {

  menuItems: StepMenuItem[];

  constructor(
    private router: Router,
    private translate: TranslateService
  ) {
    this.menuItems = this.getMenuItems();
  }

  getMenuItems() {
    return [
      {
        label: this.translate.instant('label.tenant'),
        imageSrc: 'http://google.com/image',
        state: StepMenuItemState.Enabled,
        click: (item) => {
          this.router.navigate(['admin/contract/tenant']);
        }
      },
      {
        label: this.translate.instant('label.chain'),
        imageSrc: 'http://google.com/image',
        state: StepMenuItemState.Enabled,
        click: (item) => {
          this.router.navigate(['admin/contract/chain']);
        }
      },
      {
        label: this.translate.instant('label.brand'),
        imageSrc: 'http://google.com/image',
        state: StepMenuItemState.Enabled,
        click: (item) => {
          this.router.navigate(['admin/contract/brand']);
        }
      },
      {
        label: this.translate.instant('label.company'),
        imageSrc: 'http://google.com/image',
        state: StepMenuItemState.Enabled,
        click: (item) => {
          this.router.navigate(['admin/contract/company']);
        }
      },
      {
        label: this.translate.instant('label.hotel'),
        imageSrc: 'http://google.com/image',
        state: StepMenuItemState.Enabled,
        click: (item) => {
          this.router.navigate(['admin/contract/hotel']);
        }
      },
      {
        label: this.translate.instant('label.user'),
        imageSrc: 'http://google.com/image',
        state: StepMenuItemState.Enabled,
        click: (item) => {
          this.router.navigate(['admin/contract/user']);
        }
      },
    ];
  }
}
