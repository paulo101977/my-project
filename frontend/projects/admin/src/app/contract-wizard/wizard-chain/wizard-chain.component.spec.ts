import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardChainComponent } from './wizard-chain.component';

describe('WizardChainComponent', () => {
  let component: WizardChainComponent;
  let fixture: ComponentFixture<WizardChainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardChainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardChainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
