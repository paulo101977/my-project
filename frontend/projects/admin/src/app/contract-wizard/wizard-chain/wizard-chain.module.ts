import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WizardChainRoutingModule } from './wizard-chain-routing.module';
import { WizardChainComponent } from './wizard-chain.component';

@NgModule({
  imports: [
    CommonModule,
    WizardChainRoutingModule
  ],
  declarations: [WizardChainComponent]
})
export class WizardChainModule { }
