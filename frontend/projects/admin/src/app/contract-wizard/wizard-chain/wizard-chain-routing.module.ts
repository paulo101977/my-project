import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WizardChainComponent } from './wizard-chain.component';

const routes: Routes = [
  {
    path: '',
    component: WizardChainComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WizardChainRoutingModule {}
