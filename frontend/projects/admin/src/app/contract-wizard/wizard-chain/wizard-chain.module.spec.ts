import { WizardChainModule } from './wizard-chain.module';

describe('WizardChainModule', () => {
  let wizardChainModule: WizardChainModule;

  beforeEach(() => {
    wizardChainModule = new WizardChainModule();
  });

  it('should create an instance', () => {
    expect(wizardChainModule).toBeTruthy();
  });
});
