import { ContractWizardModule } from './contract-wizard.module';

describe('ContractWizardModule', () => {
  let contractConfigModule: ContractWizardModule;

  beforeEach(() => {
    contractConfigModule = new ContractWizardModule();
  });

  it('should create an instance', () => {
    expect(contractConfigModule).toBeTruthy();
  });
});
