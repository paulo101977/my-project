import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardTenantComponent } from './wizard-tenant.component';

describe('WizardTenantComponent', () => {
  let component: WizardTenantComponent;
  let fixture: ComponentFixture<WizardTenantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardTenantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardTenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
