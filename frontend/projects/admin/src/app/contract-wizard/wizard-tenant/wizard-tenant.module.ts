import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TenantModule } from '../../tenant/tenant.module';
import { WizardTenantComponent } from './wizard-tenant.component';
import { WizardTenantRoutingModule } from './wizard-tenant-routing.module';

@NgModule({
  imports: [
    CommonModule,
    WizardTenantRoutingModule,
    TenantModule
  ],
  declarations: [WizardTenantComponent]
})
export class WizardTenantModule { }
