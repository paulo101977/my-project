import { WizardTenantModule } from './wizard-tenant.module';

describe('TenantModule', () => {
  let tenantModule: WizardTenantModule;

  beforeEach(() => {
    tenantModule = new WizardTenantModule();
  });

  it('should create an instance', () => {
    expect(tenantModule).toBeTruthy();
  });
});
