import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WizardTenantComponent } from './wizard-tenant.component';

const routes: Routes = [
  {
    path: '',
    component: WizardTenantComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WizardTenantRoutingModule {}
