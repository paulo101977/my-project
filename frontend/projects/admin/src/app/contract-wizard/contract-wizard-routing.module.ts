import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WizardComponent } from './wizard/wizard.component';

const routes: Routes = [
  {
    path: '',
    component: WizardComponent,
    children: [
      {
        path: 'tenant',
        loadChildren: './wizard-tenant/wizard-tenant.module#WizardTenantModule',
      },
      {
        path: 'chain',
        loadChildren: './wizard-chain/wizard-chain.module#WizardChainModule',
      },
      {
        path: 'brand',
        loadChildren: './wizard-brand/wizard-brand.module#WizardBrandModule',
      },
      {
        path: 'company',
        loadChildren: './wizard-company/wizard-company.module#WizardCompanyModule',
      },
      {
        path: 'hotel',
        loadChildren: './wizard-hotel/wizard-hotel.module#WizardHotelModule',
      },
      {
        path: 'user',
        loadChildren: './wizard-user/wizard-user.module#WizardUserModule',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractWizardRoutingModule {}
