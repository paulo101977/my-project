import { WizardBrandModule } from './wizard-brand.module';

describe('WizardBrandModule', () => {
  let wizardBrandModule: WizardBrandModule;

  beforeEach(() => {
    wizardBrandModule = new WizardBrandModule();
  });

  it('should create an instance', () => {
    expect(wizardBrandModule).toBeTruthy();
  });
});
