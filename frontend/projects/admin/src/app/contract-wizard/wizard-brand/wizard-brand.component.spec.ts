import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardBrandComponent } from './wizard-brand.component';

describe('WizardBrandComponent', () => {
  let component: WizardBrandComponent;
  let fixture: ComponentFixture<WizardBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
