import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WizardBrandRoutingModule } from './wizard-brand-routing.module';
import { WizardBrandComponent } from './wizard-brand.component';

@NgModule({
  imports: [
    CommonModule,
    WizardBrandRoutingModule
  ],
  declarations: [WizardBrandComponent]
})
export class WizardBrandModule { }
