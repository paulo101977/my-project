import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WizardBrandComponent } from './wizard-brand.component';

const routes: Routes = [
  {
    path: '',
    component: WizardBrandComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WizardBrandRoutingModule {}
