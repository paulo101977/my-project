import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardCompanyComponent } from './wizard-company.component';

describe('WizardCompanyComponent', () => {
  let component: WizardCompanyComponent;
  let fixture: ComponentFixture<WizardCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizardCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
