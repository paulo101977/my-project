import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WizardCompanyRoutingModule } from './wizard-company-routing.module';
import { WizardCompanyComponent } from './wizard-company.component';

@NgModule({
  imports: [
    CommonModule,
    WizardCompanyRoutingModule
  ],
  declarations: [WizardCompanyComponent]
})
export class WizardCompanyModule { }
