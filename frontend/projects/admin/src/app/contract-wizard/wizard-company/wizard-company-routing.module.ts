import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WizardCompanyComponent } from './wizard-company.component';

const routes: Routes = [
  {
    path: '',
    component: WizardCompanyComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WizardCompanyRoutingModule {}
