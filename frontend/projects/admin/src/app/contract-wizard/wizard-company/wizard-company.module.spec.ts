import { WizardCompanyModule } from './wizard-company.module';

describe('WizardCompanyModule', () => {
  let wizardCompanyModule: WizardCompanyModule;

  beforeEach(() => {
    wizardCompanyModule = new WizardCompanyModule();
  });

  it('should create an instance', () => {
    expect(wizardCompanyModule).toBeTruthy();
  });
});
