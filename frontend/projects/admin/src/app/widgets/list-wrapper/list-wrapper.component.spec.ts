import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';

import { ListWrapperComponent } from './list-wrapper.component';

describe('ListWrapperComponent', () => {
  let component: ListWrapperComponent;
  let fixture: ComponentFixture<ListWrapperComponent>;

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {}
  };
  let translateService: TranslateService;

  @Component({selector: 'thf-table', template: ''}) // tslint:disable-line
  class TableStubComponent {}

  let getListSpy: any;
  const setGetListResponse = (items = [], totalItems = 0, hasNext = false) => {
    const response = { items, totalItems, hasNext };
    getListSpy.and.returnValue(of(response));
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListWrapperComponent, TableStubComponent ],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();

    translateService = TestBed.get(TranslateService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListWrapperComponent);
    component = fixture.componentInstance;
    component.getList = () => null;
    getListSpy = spyOn(component, 'getList');
    setGetListResponse();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#filterItems', () => {
    it('should replace items with the new filtered items', () => {
      const originalItems = [1, 2, 3];
      const newItems = [4, 5, 6];
      component.tableSettings.items = originalItems;
      component.tableSettings.hasMoreToShow = true;
      setGetListResponse(newItems);

      expect(component.tableSettings.items).toEqual(originalItems);
      component.filterItems('');
      expect(component.tableSettings.items).toBe(newItems);
    });
  });

  describe('#showMore', () => {
    it('should add items to the items list', () => {
      component.tableSettings.items = [1, 2, 3];
      component.tableSettings.hasMoreToShow = true;
      setGetListResponse([4, 5, 6]);

      expect(component.tableSettings.items.length).toBe(3);
      component.showMore();
      expect(component.tableSettings.items.length).toBe(6);
    });

    it('shouldn\'t run when there is no more items to look after', () => {
      component.tableSettings.items = [1, 2, 3];
      component.tableSettings.hasMoreToShow = false;
      setGetListResponse([4, 5, 6]);

      expect(component.tableSettings.items.length).toBe(3);
      component.showMore();
      expect(component.tableSettings.items.length).toBe(3);
    });
  });
});
