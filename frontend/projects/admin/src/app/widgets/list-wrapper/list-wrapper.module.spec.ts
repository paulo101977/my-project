import { ListWrapperModule } from './list-wrapper.module';

describe('ListWrapperModule', () => {
  let listWrapperModule: ListWrapperModule;

  beforeEach(() => {
    listWrapperModule = new ListWrapperModule();
  });

  it('should create an instance', () => {
    expect(listWrapperModule).toBeTruthy();
  });
});
