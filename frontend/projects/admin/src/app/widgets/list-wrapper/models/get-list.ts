import { Observable } from 'rxjs';
import { ListQueryParams } from '../../../shared/models/list-query-params';

export type GetList = (params: ListQueryParams) => Observable<any>;
