import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThfTableModule } from '@totvs/thf-ui';
import { ListWrapperComponent } from './list-wrapper.component';

@NgModule({
  imports: [
    CommonModule,
    ThfTableModule
  ],
  declarations: [ListWrapperComponent],
  exports: [ListWrapperComponent]
})
export class ListWrapperModule { }
