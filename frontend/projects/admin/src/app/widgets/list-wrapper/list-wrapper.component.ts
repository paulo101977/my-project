import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThfTableColumn } from '@totvs/thf-ui';
import { Subject } from 'rxjs';
import { debounceTime, map, tap } from 'rxjs/operators';
import { TableSettings } from '../../shared/models/table-settings';
import { GetList } from './models/get-list';

@Component({
  selector: 'admin-list-wrapper',
  templateUrl: './list-wrapper.component.html',
  styleUrls: ['./list-wrapper.component.css']
})
export class ListWrapperComponent implements OnInit, OnChanges {
  @Input() search: string;
  @Input() pageSize = 5;
  @Input() columns: ThfTableColumn[];
  @Input() getList: GetList;

  private searchChange = new Subject();
  private searchChanged$ = this.searchChange.asObservable();

  public currentPage = 1;
  public tableSettings: TableSettings;

  @Input() transformData = (response) => response;

  constructor(private translate: TranslateService) {}

  ngOnInit(): void {
    this.setTableSettings();
    this.getTableItems();
    this.listenToSearchChanges();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.search) {
      this.searchChange.next(this.search);
    }
  }

  public filterItems(input) {
    this.list(input, 1)
      .subscribe((items) => {
        this.tableSettings.items = items;
      });
  }

  public showMore() {
    if (!this.tableSettings.hasMoreToShow) {
      return;
    }

    const nextPage = this.currentPage + 1;
    this.list(this.search, nextPage)
      .subscribe((items) => {
        this.tableSettings.items.push(...items);
      });
  }

  private listenToSearchChanges() {
    this.searchChanged$
      .pipe(
        debounceTime(500)
      )
      .subscribe((search) => {
        this.filterItems(search);
      });
  }

  private list(search: string, page: number) {
    const params = {
      SearchData: search,
      Page: page,
      PageSize: this.pageSize
    };

    this.tableSettings.loading = true;

    return this.getList(params)
      .pipe(
        map(response => this.transformData(response)),
        tap(() => { this.currentPage = page; }),
        tap((response) => { this.updateTableSettings(response); }),
        map(({items}) => items)
      );
  }

  public getTableItems(): void {
    this.list('', 1)
      .subscribe((items) => {
        this.tableSettings.items = items;
      });
  }

  private setTableSettings() {
    const rowHeight = 66;
    this.tableSettings = {
      columns: this.columns,
      items: [],
      sort: true,
      striped: true,
      loading: false,
      showMore: () => { this.showMore(); },
      hasMoreToShow: false,
      height: rowHeight * this.pageSize,
      literals: {
        loadMoreData: this.translate.instant('variable.showMoreItems', {current: 0, of: 10})
      }
    };
  }

  private updateTableSettings({hasNext, items, totalItems}) {
    const literalShowMore = this.getShowMoreLiteral(this.currentPage, this.pageSize, items.length, totalItems);

    this.tableSettings = {
      ...this.tableSettings,
      loading: false,
      hasMoreToShow: hasNext,
      literals: {
        ...this.tableSettings.literals,
        loadMoreData: literalShowMore
      }
    };
  }

  private getShowMoreLiteral(page: number, pageSize: number, currentItems: number, totalItems: number) {
    return this.translate.instant('variable.showMoreItems', {
      current: (page - 1) * pageSize + currentItems,
      of: totalItems
    });
  }
}
