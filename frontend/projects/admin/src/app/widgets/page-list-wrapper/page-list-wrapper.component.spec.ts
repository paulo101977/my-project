import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageListWrapperComponent } from './page-list-wrapper.component';

describe('PageListWrapperComponent', () => {
  let component: PageListWrapperComponent;
  let fixture: ComponentFixture<PageListWrapperComponent>;

  @Component({selector: 'thf-page-list', template: ''}) // tslint:disable-line
  class PageListStubComponent {}

  @Component({selector: 'admin-list-wrapper', template: ''}) // tslint:disable-line
  class ListWrapperStubComponent {}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageListWrapperComponent, PageListStubComponent, ListWrapperStubComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageListWrapperComponent);
    component = fixture.componentInstance;
    component.pageSettings = {filterSettings: {}, actions: [], title: ''};
    component.pageTableSettings = {transformData: () => {}, columns: [], getList: () => null};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
