import { PageListWrapperModule } from './page-list-wrapper.module';

describe('PageListWrapperModule', () => {
  let pageListWrapperModule: PageListWrapperModule;

  beforeEach(() => {
    pageListWrapperModule = new PageListWrapperModule();
  });

  it('should create an instance', () => {
    expect(pageListWrapperModule).toBeTruthy();
  });
});
