import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThfPageModule } from '@totvs/thf-ui';
import { ListWrapperModule } from '../list-wrapper/list-wrapper.module';
import { PageListWrapperComponent } from './page-list-wrapper.component';

@NgModule({
  imports: [
    CommonModule,
    ThfPageModule,
    ListWrapperModule
  ],
  declarations: [PageListWrapperComponent],
  exports: [PageListWrapperComponent]
})
export class PageListWrapperModule { }
