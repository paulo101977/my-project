import { ThfTableColumn } from '@totvs/thf-ui';
import { GetList } from '../../list-wrapper/models/get-list';

export interface PageTableSettings {
  columns: ThfTableColumn[];
  getList: GetList;
  transformData: Function;
}
