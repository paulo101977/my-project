import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { PageSettings } from '../../shared/models/page-settings';
import { ListWrapperComponent } from '../list-wrapper/list-wrapper.component';
import { PageTableSettings } from './models/page-table-settings';

@Component({
  selector: 'admin-page-list-wrapper',
  templateUrl: './page-list-wrapper.component.html',
  styleUrls: ['./page-list-wrapper.component.css']
})
export class PageListWrapperComponent implements OnInit {
  @Input() pageSettings: PageSettings;
  @Input() pageTableSettings: PageTableSettings;
  @ViewChild('list') listWrapper: ListWrapperComponent;

  public searchValue: any = '';

  constructor() { }

  ngOnInit() {
    this.setPageSettings();
  }

  private setPageSettings() {
    this.pageSettings = {
      ...this.pageSettings,
      filterSettings: {
        ...this.pageSettings.filterSettings,
        ngModel: 'searchValue',
      }
    };
  }

  public reloadList() {
    this.listWrapper.getTableItems();
  }
}
