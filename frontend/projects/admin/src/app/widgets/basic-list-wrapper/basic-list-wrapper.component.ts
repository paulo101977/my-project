import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThfTableColumn, ThfTableColumnIcon } from '@totvs/thf-ui';
import { TableSettings } from '../../shared/models/table-settings';

@Component({
  selector: 'admin-basic-list-wrapper',
  templateUrl: './basic-list-wrapper.component.html',
  styleUrls: ['./basic-list-wrapper.component.css']
})
export class BasicListWrapperComponent implements OnChanges {
  @Input() items: any[];
  @Input() selectedColumns: string[] = [];
  @Input() searchableColumns: ThfTableColumn[];
  @Input() actions: ThfTableColumnIcon[] = [];
  @Input() tableExtras: Partial<TableSettings>;

  private readonly ACTIONS_COLUMN_SIZE = 10;
  public tableSettings: Partial<TableSettings>;

  constructor(
    private translate: TranslateService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.onSearchableColumnsChange(changes);
    this.onItemsChanges(changes);
  }

  private onSearchableColumnsChange({ searchableColumns }: SimpleChanges) {
    if (searchableColumns && searchableColumns.currentValue) {
      this.setTableSettings();
    }
  }

  private onItemsChanges({ items }: SimpleChanges) {
    if (items && items.currentValue) {
      this.setTableItems(items.currentValue);
    }
  }

  private setTableItems(items: any[]) {
    if (!this.tableSettings) {
      return;
    }

    this.tableSettings.items = items;
  }

  private setTableSettings() {
    this.tableSettings = {
      columns: this.getTableColumns(),
      items: this.items,
      sort: true,
      height: 330,
      striped: true,
      ...(this.tableExtras || {})
    };
  }

  private getTableColumns(): ThfTableColumn[] {
    const actionsColumn = this.getTableActions();
    const columns = this.searchableColumns
      .filter(this.searchForColumns())
      .map(this.calculateColumnWidth(!!actionsColumn));

    return [
      ...columns,
      ...(actionsColumn ? [actionsColumn] : [])
    ].map(this.translateLabels());
  }

  private getTableActions(): ThfTableColumn {
    if (!this.actions.length) {
      return;
    }

    return {
      property: 'actions',
      label: 'label.actions',
      type: 'icon',
      width: `${this.ACTIONS_COLUMN_SIZE}%`,
      icons: this.actions
    };
  }

  private searchForColumns() {
    return (column) => {
      return !this.selectedColumns.length || this.selectedColumns.includes(column.property);
    };
  }

  private calculateColumnWidth(hasActionsColumn: boolean) {
    return (column, index, list) => {
      const usedSize = (hasActionsColumn ? this.ACTIONS_COLUMN_SIZE : 0);
      const columnSize = Math.floor((100 - usedSize) / list.length);
      return { ...column, width: `${columnSize}%` };
    };
  }

  private translateLabels() {
    return (column: ThfTableColumn): ThfTableColumn => {
      return {
        ...column,
        label: this.translate.instant(column.label)
      };
    };
  }
}
