import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThfTableModule } from '@totvs/thf-ui';
import { BasicListWrapperComponent } from './basic-list-wrapper.component';

@NgModule({
  imports: [
    CommonModule,
    ThfTableModule
  ],
  declarations: [BasicListWrapperComponent],
  exports: [BasicListWrapperComponent]
})
export class BasicListWrapperModule { }
