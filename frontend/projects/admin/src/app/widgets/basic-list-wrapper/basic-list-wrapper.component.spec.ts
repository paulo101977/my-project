import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

import { BasicListWrapperComponent } from './basic-list-wrapper.component';

describe('BasicListWrapperComponent', () => {
  let component: BasicListWrapperComponent;
  let fixture: ComponentFixture<BasicListWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ BasicListWrapperComponent ],
      imports: [ TranslateTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicListWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
