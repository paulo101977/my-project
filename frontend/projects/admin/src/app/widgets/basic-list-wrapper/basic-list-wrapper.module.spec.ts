import { BasicListWrapperModule } from './basic-list-wrapper.module';

describe('BasicListWrapperModule', () => {
  let basicListWrapperModule: BasicListWrapperModule;

  beforeEach(() => {
    basicListWrapperModule = new BasicListWrapperModule();
  });

  it('should create an instance', () => {
    expect(basicListWrapperModule).toBeTruthy();
  });
});
