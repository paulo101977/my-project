import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfFieldModule } from '@totvs/thf-ui';
import { Observable, of } from 'rxjs';
import { ProductApiService } from '../../../product/services/product-api/product-api.service';

import { ProfileFormComponent } from './profile-form.component';

describe('ProfileFormComponent', () => {
  let component: ProfileFormComponent;
  let fixture: ComponentFixture<ProfileFormComponent>;

  const productApiServiceStub: Partial<ProductApiService> = {
    listAllAsOptions(): Observable<any> { return of([]); }
  };

  const translateServiceStub: Partial<TranslateService> = {
    instant(key: string | Array<string>, interpolateParams?: Object): string | any { return 'null'; }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ ProfileFormComponent ],
      imports: [ReactiveFormsModule, ThfFieldModule],
      providers: [
        {
          provide: ProductApiService,
          useValue: productApiServiceStub
        },
        {
          provide: TranslateService,
          useValue: translateServiceStub
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
