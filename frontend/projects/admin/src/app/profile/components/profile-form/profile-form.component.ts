import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfSelectOption } from '@totvs/thf-ui';
import { Observable, of, Subscription } from 'rxjs';
import { ProductApiService } from '../../../product/services/product-api/product-api.service';
import { ProfileCreateRequest } from '../../models/profile-create-request';

@Component({
  selector: 'admin-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css']
})
export class ProfileFormComponent implements OnInit, OnDestroy {
  @Input() profile: any = {};
  @Output() formStatusChanged = new EventEmitter<boolean>();

  public productsOptions$: Observable<ThfSelectOption[]>;
  public form: FormGroup;
  public i18n: any;

  private formStatusSubscription: Subscription;

  constructor(
    private productApi: ProductApiService,
    private fb: FormBuilder,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.productsOptions$ = this.getProducts();
    this.setI18n();
    this.setForm();
  }

  ngOnDestroy(): void {
    this.formStatusSubscription.unsubscribe();
  }

  public getValue(): Observable<ProfileCreateRequest> {
    return of(this.form.value);
  }

  private setI18n() {
    this.i18n = {
      label: {
        profile: this.translate.instant('variable.nameOf.M', {
          labelName: this.translate.instant('label.profile')
        }),
        product: this.translate.instant('label.product'),
        isActive: `${this.translate.instant('label.isActive')}?`,
        activated: this.translate.instant('label.activated'),
        deactivated: this.translate.instant('label.deactivated')
      },
    };
  }

  private getProducts() {
    return this.productApi.listAllAsOptions();
  }

  private setForm() {
    this.form = this.fb.group({
      name: [this.profile.name, Validators.required],
      identityProductId: [this.profile.identityProductId, Validators.required],
      isActive: [this.profile.isActive]
    });

    this.formStatusChanged.emit(this.form.invalid);

    this.formStatusSubscription = this.form.statusChanges
      .subscribe(() => {
        this.formStatusChanged.emit(this.form.invalid);
      });
  }
}
