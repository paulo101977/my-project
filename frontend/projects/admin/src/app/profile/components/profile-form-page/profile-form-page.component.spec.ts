import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { ThfNotification, ThfNotificationService } from '@totvs/thf-ui';
import { Observable, of } from 'rxjs';
import { ProfileCreateRequest } from '../../models/profile-create-request';
import { ProfileApiService } from '../../services/profile-api.service';

import { ProfileFormPageComponent } from './profile-form-page.component';

describe('ProfileFormPageComponent', () => {
  let component: ProfileFormPageComponent;
  let fixture: ComponentFixture<ProfileFormPageComponent>;

  const profileApiServiceStub: Partial<ProfileApiService> = {
    create(params: ProfileCreateRequest): Observable<any> { return of({}); },
    getById(id: string): Observable<any> { return of({}); },
    update(id: string, params: ProfileCreateRequest): Observable<any> { return of({}); }
  };

  const translateServiceStub: Partial<TranslateService> = {
    instant(key: string | Array<string>, interpolateParams?: Object): string | any { return 'null'; }
  };

  const notificationServiceStub: Partial<ThfNotificationService> = {
    success(notification: ThfNotification | string): void {}
  };

  @Component({ selector: 'admin-permission-list', template: '' })
  class PermissionListStubComponent {
    getSelectedPermissions() {}
  }

  @Component({ selector: 'admin-profile-form', template: '' })
  class ProfileFormStubComponent {
    form = new FormGroup({});
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ ProfileFormPageComponent, PermissionListStubComponent, ProfileFormStubComponent ],
      imports: [ RouterTestingModule ],
      providers: [
        {
          provide: ProfileApiService,
          useValue: profileApiServiceStub
        },
        {
          provide: TranslateService,
          useValue: translateServiceStub
        },
        {
          provide: ThfNotificationService,
          useValue: notificationServiceStub
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileFormPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
