import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeIf } from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { ThfNotificationService } from '@totvs/thf-ui';
import { combineLatest, iif, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { PermissionsListComponent } from '../../../permission/components/permissions-list/permissions-list.component';
import { ProfileCreateRequest } from '../../models/profile-create-request';
import { ProfileApiService } from '../../services/profile-api.service';
import { ProfileFormComponent } from '../profile-form/profile-form.component';

@Component({
  selector: 'admin-profile-form-page',
  templateUrl: './profile-form-page.component.html',
  styleUrls: ['./profile-form-page.component.css']
})
export class ProfileFormPageComponent implements OnInit {
  @ViewChild('permissionsTable') permissionsTable: PermissionsListComponent;
  @ViewChild('profileForm') profileForm: ProfileFormComponent;

  public i18n: any;
  public pageSettings: any = {};
  public profileId: string;
  public profile$: Observable<any>;
  public formIsInvalid = true;

  constructor(
    private profileApi: ProfileApiService,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: ThfNotificationService
  ) { }

  ngOnInit() {
    this.checkForProfileId();
    this.setI18n();
    this.setPageSettings();
  }

  public save() {
    if (this.formIsInvalid) {
      return;
    }

    this.getRequestData()
      .pipe(
        take(1),
        mergeIf(() => !!this.profileId, this.updateProfile(), this.createProfile())
      )
      .subscribe((response) => {
        this.notificationService.success(this.i18n.message.success);
        this.goToProfileListPage();
      });
  }

  public cancel() {
    this.goToProfileListPage();
  }

  public onFormStatusChange(invalid: boolean) {
    this.formIsInvalid = invalid;
  }

  private checkForProfileId() {
    this.profileId = this.route.snapshot.params.id;
    const getProfile$ = this.profileApi.getById(this.profileId);
    const fallback$ = of({});

    this.profile$ = iif(() => !!this.profileId, getProfile$, fallback$);
  }

  private setI18n() {
    this.i18n = {
      title: {
        profileInsert: this.translate.instant('title.lbInsert', {
          labelName: this.translate.instant('label.profile')
        }),
        profileEdit: this.translate.instant('title.lbEdit', {
          labelName: this.translate.instant('label.profile')
        }),
        profileForm: this.translate.instant('variable.formOf', {
          labelName: this.translate.instant('label.profile')
        }),
        permissionsList: this.translate.instant('variable.listOf', {
          labelName: this.translate.instant('label.permissions')
        })
      },
      action: {
        back: this.translate.instant('action.back'),
        confirm: this.translate.instant('action.save')
      },
      message: {
        success: this.translate.instant('variable.lbSaveSuccessM', {
          labelName: this.translate.instant('label.profile')
        })
      }
    };
  }

  private setPageSettings() {
    const isInEditMode = this.profileId;
    this.pageSettings = {
      title: isInEditMode ? this.i18n.title.profileEdit : this.i18n.title.profileInsert,
      literals: {
        cancel: this.i18n.action.back,
        save: this.i18n.action.confirm
      }
    };
  }

  private createProfile() {
    return (profile: ProfileCreateRequest) => this.profileApi.create(profile);
  }

  private updateProfile() {
    return (profile: ProfileCreateRequest) => this.profileApi.update(this.profileId, profile);
  }

  private getRequestData(): Observable<any> {
    const formValue$ = this.profileForm.getValue();
    const selectedPermissions$ = this.permissionsTable.getSelectedPermissions();

    return combineLatest(formValue$, selectedPermissions$)
      .pipe(
        map(([form, rolePermissionList]) => ({
          ...form,
          rolePermissionList
        }))
      );
  }

  private goToProfileListPage() {
    this.router.navigate(['admin/profile']);
  }
}
