import { Location } from '@angular/common';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ThfPageAction } from '@totvs/thf-ui';
import { ListQueryParams } from '../../../shared/models/list-query-params';
import { PageSettings } from '../../../shared/models/page-settings';
import { PageTableSettings } from '../../../widgets/page-list-wrapper/models/page-table-settings';
import { ProfileApiService } from '../../services/profile-api.service';

@Component({
  selector: 'admin-profile-list-page',
  templateUrl: './profile-list-page.component.html',
  styleUrls: ['./profile-list-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileListPageComponent implements OnInit {
  public pageSettings: PageSettings;
  public pageTableSettings: PageTableSettings;
  public modalSettings: any = {};
  public form: FormGroup;

  public i18n: any;

  constructor(
    private location: Location,
    private translate: TranslateService,
    private profileApi: ProfileApiService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.setI18n();
    this.setPageSettings();
    this.setPageTableSettings();
  }

  private setPageSettings() {
    this.pageSettings = {
      title: this.i18n.title.profileManagement,
      actions: this.getActions(),
      filterSettings: {
        placeholder: this.i18n.placeholder.search
      }
    };
  }

  private setPageTableSettings() {
    this.pageTableSettings = {
      columns: [
        { property: 'name', width: '45%', label: this.i18n.label.profile, type: 'string' },
        { property: 'identityProductName', width: '45%', label: this.i18n.label.product, type: 'string' },
        {
          property: 'actions',
          label: this.i18n.label.actions,
          type: 'icon',
          width: '10%',
          icons: [
            { value: 'edit', icon: 'thf-icon-edit', action: (item) => this.goToEditPage(item) }
          ]
        }
      ],
      transformData: this.transformData,
      getList: this.getList.bind(this),
    };
  }

  public getList(params: ListQueryParams) {
    return this.profileApi.listAll(params);
  }

  public transformData(response) {
    return {
      ...response,
      items: response.items.map((item) => {
        item.actions = ['edit'];
        return item;
      })
    };
  }

  private getActions() {
    const backAction: ThfPageAction = {
      label: this.i18n.action.back,
      icon: 'thf-icon-arrow-left',
      action: () => { this.location.back(); }
    };

    return [
      {
        label: this.i18n.action.insert,
        icon: 'thf-icon-plus',
        action: () => { this.goToInsertPage(); }
      },
      backAction
    ];
  }

  private setI18n() {
    this.i18n = {
      title: {
        profileManagement: this.translate.instant('title.profileManagement')
      },
      placeholder: {
        search: this.translate.instant('placeholder.searchProfileOrProduct')
      },
      label: {
        profile: this.translate.instant('label.profile'),
        product: this.translate.instant('label.product'),
      },
      action: {
        back: this.translate.instant('action.back'),
        insert: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.profile')
        })
      }
    };
  }

  private goToEditPage(item) {
    this.router.navigate([`edit/${item.id}`], {relativeTo: this.route});
  }

  private goToInsertPage() {
    this.router.navigate(['create'], {relativeTo: this.route});
  }
}
