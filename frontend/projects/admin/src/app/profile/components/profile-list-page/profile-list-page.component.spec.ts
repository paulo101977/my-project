import { Location } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { ListQueryParams } from '../../../shared/models/list-query-params';
import { ProfileApiService } from '../../services/profile-api.service';

import { ProfileListPageComponent } from './profile-list-page.component';

describe('ProfileListPageComponent', () => {
  let component: ProfileListPageComponent;
  let fixture: ComponentFixture<ProfileListPageComponent>;

  const locationStub: Partial<Location> = {
    back(): void {}
  };
  const translateServiceStub: Partial<TranslateService> = {
    instant(key: string | Array<string>, interpolateParams?: Object): string | any { return 'null'; }
  };
  const profileApiServiceStub: Partial<ProfileApiService> = {
    list(params: ListQueryParams): Observable<any> { return of(); }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ ProfileListPageComponent ],
      imports: [ RouterTestingModule ],
      providers: [
        {
          provide: Location,
          useValue: locationStub
        },
        {
          provide: TranslateService,
          useValue: translateServiceStub
        },
        {
          provide: ProfileApiService,
          useValue: profileApiServiceStub
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
