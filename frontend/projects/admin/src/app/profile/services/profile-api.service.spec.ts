import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { I18nService } from '@inovacaocmnet/thx-bifrost';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { ProfileCreateRequest } from '../models/profile-create-request';
import { ProfileResourceService } from '../resources/profile-resource.service';

import { ProfileApiService } from './profile-api.service';

describe('ProfileApiService', () => {
  let service: ProfileApiService;
  let profileResourceService: ProfileResourceService;

  const profileResourceServiceStub: Partial<ProfileResourceService> = {
    list(params: ListQueryParams): Observable<any> { return of(null); },
    create(params: ProfileCreateRequest): Observable<any> { return of(null); },
    getById(id: string): Observable<any> { return of({}); },
    update(id: string, params: ProfileCreateRequest): Observable<any> { return of(null); }
  };

  const i18nServiceStub: Partial<I18nService> = {
    getLanguage(): string { return 'null'; }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProfileApiService,
        {
          provide: ProfileResourceService,
          useValue: profileResourceServiceStub
        },
        {
          provide: I18nService,
          useValue: i18nServiceStub
        }
      ]
    });

    service = TestBed.get(ProfileApiService);
    profileResourceService = TestBed.get(ProfileResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call profileResourceService#list', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(profileResourceService, 'list');

      service.list(mock);
      expect(profileResourceService.list).toHaveBeenCalledWith(mock);
    });
  });

  describe('#listAll', () => {
    it('should call profileResourceService#list', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      const listAllParams = { ...mock, PageSize: 999 };
      spyOn(profileResourceService, 'list');

      service.listAll(mock);
      expect(profileResourceService.list).toHaveBeenCalledWith(listAllParams);
    });
  });

  describe('#create', () => {
    it('should call profileResourceService#create', () => {
      const mock: ProfileCreateRequest = {
        name: '',
        isActive: true,
        identityProductId: 1,
        translationList: [],
        rolePermissionList: []
      };
      spyOn(profileResourceService, 'create');

      service.create(mock);
      expect(profileResourceService.create).toHaveBeenCalledWith(mock);
    });

    it('should add isActive to rolePermissionList items', () => {
      // TODO remove this test when isActive is not added through ProfileApiService
      const mock: ProfileCreateRequest = {
        name: '',
        isActive: true,
        identityProductId: 1,
        translationList: [],
        rolePermissionList: [{identityPermissionId: '123'}]
      };
      spyOn(profileResourceService, 'create');

      service.create(mock);
      expect(profileResourceService.create).toHaveBeenCalledWith(jasmine.objectContaining({
        rolePermissionList: jasmine.arrayContaining([
          jasmine.objectContaining({
            isActive: true
          })
        ])
      }));
    });
  });

  describe('#getById', () => {
    it('should call profileResourceService#getById', () => {
      const id = '1';
      spyOn(profileResourceService, 'getById');

      service.getById(id);
      expect(profileResourceService.getById).toHaveBeenCalledWith(id);
    });
  });

  describe('#update', () => {
    it('should call profileResourceService#update', () => {
      const id = '1';
      const mock: ProfileCreateRequest = {
        name: '',
        isActive: true,
        identityProductId: 1,
        translationList: [],
        rolePermissionList: []
      };
      spyOn(profileResourceService, 'update');

      service.update(id, mock);
      expect(profileResourceService.update).toHaveBeenCalledWith(id, mock);
    });

    it('should add isActive to rolePermissionList items', () => {
      // TODO remove this test when isActive is not added through ProfileApiService
      const id = '1';
      const mock: ProfileCreateRequest = {
        name: '',
        isActive: true,
        identityProductId: 1,
        translationList: [],
        rolePermissionList: [{identityPermissionId: '123'}]
      };
      spyOn(profileResourceService, 'update');

      service.update(id, mock);
      expect(profileResourceService.update).toHaveBeenCalledWith(id, jasmine.objectContaining({
        rolePermissionList: jasmine.arrayContaining([
          jasmine.objectContaining({
            isActive: true
          })
        ])
      }));
    });
  });
});
