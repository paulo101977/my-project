import { Injectable } from '@angular/core';
import { I18nService } from '@inovacaocmnet/thx-bifrost';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { ProfileCreateRequest } from '../models/profile-create-request';
import { RoleTranslationRequest } from '../models/role-translation-request';
import { ProfileResourceService } from '../resources/profile-resource.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileApiService {

  constructor(
    private resource: ProfileResourceService,
    private i18nService: I18nService
  ) { }

  public getById(id: string) {
    return this.resource.getById(id);
  }

  public list(params: ListQueryParams) {
    return this.resource.list(params);
  }

  public listAll(params: ListQueryParams) {
    const listAllParams: ListQueryParams = {
      ...params,
      PageSize: 999
    };
    return this.resource.list(listAllParams);
  }

  public create(params: ProfileCreateRequest) {
    params = this.mutateProfileParams(params);

    return this.resource.create(params);
  }

  public update(id: string, params: ProfileCreateRequest) {
    params = this.mutateProfileParams(params);

    return this.resource.update(id, params);
  }

  private mutateProfileParams(params: ProfileCreateRequest) {
    // TODO remove it when it exists in form
    if (params.rolePermissionList) {
      params.rolePermissionList = this.addIsActive(params.rolePermissionList);
    }

    // TODO remove it when it exists in form
    params.translationList = this.getTranslationList(params.name);

    return params;
  }

  // TODO move it to Bifrost
  private addIsActive(list: any[]) {
    return list.map((item) => {
      item.isActive = true;
      return item;
    });
  }

  // TODO remove it when it exists in form
  private getTranslationList(name: string): RoleTranslationRequest[] {
    return [
      {
        languageIsoCode: this.i18nService.getLanguage(),
        name
      }
    ];
  }
}
