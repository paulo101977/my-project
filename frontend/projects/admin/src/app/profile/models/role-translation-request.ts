export interface RoleTranslationRequest {
  languageIsoCode: string;
  name: string;
  description?: string;
}
