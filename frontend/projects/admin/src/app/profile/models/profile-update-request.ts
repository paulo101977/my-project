import { ProfileCreateRequest } from './profile-create-request';

export interface ProfileUpdateRequest extends ProfileCreateRequest {
  id: string;
}
