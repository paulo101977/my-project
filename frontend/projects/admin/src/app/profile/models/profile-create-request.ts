import { RolePermissionRequest } from './role-permission-request';
import { RoleTranslationRequest } from './role-translation-request';

export interface ProfileCreateRequest {
  name: string;
  isActive: boolean;
  identityProductId: number;
  rolePermissionList?: RolePermissionRequest[];
  // TODO make it required when there are options
  translationList?: RoleTranslationRequest[];
}
