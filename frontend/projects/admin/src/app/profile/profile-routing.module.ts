import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileFormPageComponent } from './components/profile-form-page/profile-form-page.component';
import { ProfileListPageComponent } from './components/profile-list-page/profile-list-page.component';

const routes: Routes = [
  {path: '', component: ProfileListPageComponent},
  {path: 'create', component: ProfileFormPageComponent},
  {path: 'edit/:id', component: ProfileFormPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
