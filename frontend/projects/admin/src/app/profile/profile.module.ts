import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ThfButtonModule, ThfFieldModule, ThfPageModule, ThfTableModule } from '@totvs/thf-ui';
import { PermissionModule } from '../permission/permission.module';
import { SharedModule } from '../shared/shared.module';
import { PageListWrapperModule } from '../widgets/page-list-wrapper/page-list-wrapper.module';
import { ProfileFormPageComponent } from './components/profile-form-page/profile-form-page.component';
import { ProfileListPageComponent } from './components/profile-list-page/profile-list-page.component';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileFormComponent } from './components/profile-form/profile-form.component';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    PageListWrapperModule,
    ThfButtonModule,
    ThfPageModule,
    ThfTableModule,
    SharedModule,
    ReactiveFormsModule,
    ThfFieldModule,
    PermissionModule
  ],
  declarations: [ProfileListPageComponent, ProfileFormPageComponent, ProfileFormComponent]
})
export class ProfileModule { }
