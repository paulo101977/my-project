import { TestBed } from '@angular/core/testing';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { Observable, of } from 'rxjs';
import { ProfileCreateRequest } from '../models/profile-create-request';

import { ProfileResourceService } from './profile-resource.service';

describe('ProfileResourceService', () => {
  let service: ProfileResourceService;
  let adminApiService: AdminApiService;

  const adminApiServiceStub: Partial<AdminApiService> = {
    get<T>(path: any, options?: any): Observable<T> { return of(null); },
    post<T>(path: any, body?: any, options?: any): Observable<T> { return of(null); },
    put<T>(path: any, body?: any, options?: any): Observable<T> { return of(null); }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProfileResourceService,
        {
          provide: AdminApiService,
          useValue: adminApiServiceStub
        }
      ]
    });

    service = TestBed.get(ProfileResourceService);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call AdminApiService#get', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(adminApiService, 'get');

      service.list(mock);
      expect(adminApiService.get).toHaveBeenCalledWith('Role', { params: mock });
    });
  });

  describe('#create', () => {
    it('should call AdminApiService#post', () => {
      const mock: ProfileCreateRequest = {
        name: '',
        isActive: true,
        identityProductId: 1,
        translationList: [],
        rolePermissionList: []
      };
      spyOn(adminApiService, 'post');

      service.create(mock);
      expect(adminApiService.post).toHaveBeenCalledWith('Role', mock);
    });
  });

  describe('#getById', () => {
    it('should call AdminApiService#getById', () => {
      const id = '1';
      spyOn(adminApiService, 'get');

      service.getById(id);
      expect(adminApiService.get).toHaveBeenCalledWith(`Role/${id}`, { params: { id } });
    });
  });

  describe('#update', () => {
    it('should call AdminApiService#put', () => {
      const id = '1';
      const mock: ProfileCreateRequest = {
        name: '',
        isActive: true,
        identityProductId: 1,
        translationList: [],
        rolePermissionList: []
      };
      spyOn(adminApiService, 'put');

      service.update(id, mock);
      expect(adminApiService.put).toHaveBeenCalledWith(`Role/${id}`, mock);
    });
  });
});
