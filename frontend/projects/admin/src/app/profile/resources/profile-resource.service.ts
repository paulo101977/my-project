import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { ProfileCreateRequest } from '../models/profile-create-request';

@Injectable({
  providedIn: 'root'
})
export class ProfileResourceService {

  constructor(private api: AdminApiService) { }

  public getById(id: string) {
    return this.api.get(`Role/${id}`, { params: { id }});
  }

  public list(params: ListQueryParams) {
    return this.api.get('Role', { params });
  }

  public create(params: ProfileCreateRequest) {
    return this.api.post('Role', params);
  }

  public update(id: string, params: ProfileCreateRequest) {
    return this.api.put(`Role/${id}`, params);
  }
}
