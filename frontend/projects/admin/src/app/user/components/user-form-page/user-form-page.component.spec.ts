import { Location } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { Observable, of } from 'rxjs';
import { ActivatedRouteStub } from '../../../../../../../testing/activated-route-stub';
import { createServiceStub } from '../../../../../../../testing/create-service-stub';
import { UserService } from '../../services/user.service';
import { UserResponse } from '../model/user-response';

import { UserFormPageComponent } from './user-form-page.component';

describe('UserFormPageComponent', () => {
  let component: UserFormPageComponent;
  let fixture: ComponentFixture<UserFormPageComponent>;

  const UserProvider = createServiceStub(UserService, {
    getData(userId?: string): Observable<{ user: UserResponse; chainOptions: any[]; brands: any[]; hotels: any[] }> {
      return of({
        brands: [],
        chainOptions: [],
        hotels: [],
        user: {
          brandList: [],
          chainId: 0,
          chainName: '',
          companyId: 0,
          email: '',
          id: '',
          isActive: false,
          name: '',
          preferredCulture: '',
          preferredLanguage: '',
          propertyId: 0,
          propertyList: [],
          tenantId: '',
          userName: ''
        }
      });
    },
    getFilteredBrands(brands: any[], chainId: number = 0): Observable<any[]> { return of([]); },
    getFilteredHotels(hotels: any[], brands: any[], user: UserResponse): Observable<any[]> {
      return of([]);
    }
  });

  const LocationProvider = createServiceStub(Location, {
    back(): void {}
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ UserFormPageComponent ],
      imports: [ TranslateTestingModule ],
      providers: [
        UserProvider,
        LocationProvider,
        { provide: ActivatedRoute, useClass: ActivatedRouteStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFormPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
