import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ThfNotificationService } from '@totvs/thf-ui';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserService } from '../../services/user.service';
import { UserResponse } from '../model/user-response';
import { UserBrandListFormComponent } from '../user-brand-list-form/user-brand-list-form.component';
import { UserDataFormComponent } from '../user-data-form/user-data-form.component';
import { UserHotelListFormComponent } from '../user-hotel-list-form/user-hotel-list-form.component';

@Component({
  selector: 'admin-user-form-page',
  templateUrl: './user-form-page.component.html',
  styleUrls: ['./user-form-page.component.css']
})
export class UserFormPageComponent implements OnInit {
  @ViewChild('userDataForm') userDataForm: UserDataFormComponent;
  @ViewChild('userBrandList') userBrandList: UserBrandListFormComponent;
  @ViewChild('userHotelList') userHotelList: UserHotelListFormComponent;

  public userId: string;
  public data$: Observable<{user: UserResponse, chainOptions: any[], brands: any[], hotels: any[]}>;
  public filteredBrands$: Observable<any[]>;
  public filteredHotels$: Observable<any[]>;
  public formTitle = this.translate.instant('label.generalData');
  public i18n: any;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private thfNotification: ThfNotificationService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.userId = this.route.snapshot.params.id;
    this.setI18n();
    this.setData();
  }

  public save() {
    const user = this.userDataForm.getUserData();
    const brandList = this.userBrandList.getSelectedIds();
    const hotelList = this.userHotelList.getSelectedIds();

    user.id = this.userId;

    this.userService.save(user, brandList, hotelList)
      .subscribe(() => {
        const { createSuccess, updateSuccess } = this.i18n.variable;
        this.thfNotification.success(this.userId ? updateSuccess : createSuccess);
        this.redirectToList();
      });
  }

  public cancel() {
    this.redirectToList();
  }

  private setI18n() {
    this.i18n = {
      variable: {
        createSuccess: this.translate.instant('variable.lbSaveSuccessM', {
          labelName: this.translate.instant('label.user')
        }),
        updateSuccess: this.translate.instant('variable.lbEditSuccessM', {
          labelName: this.translate.instant('label.user')
        })
      }
    };
  }

  private setData() {
    this.data$ = this.userService.getData(this.userId)
      .pipe(
        tap(({brands, user}) => this.setFilteredBrands(brands, user)),
        tap(({hotels, brands, user}) => this.setFilteredHotels(hotels, brands, user))
      );
  }

  private setFilteredBrands(brands: any[], user: Partial<UserResponse> = {chainId: 0}) {
    this.filteredBrands$ = this.userService.getFilteredBrands(brands, user.chainId);
  }

  private setFilteredHotels(hotels: any[], brands: any[], user: UserResponse) {
    this.filteredHotels$ = this.userService.getFilteredHotels(hotels, brands, user);
  }

  private redirectToList() {
    this.location.back();
  }
}
