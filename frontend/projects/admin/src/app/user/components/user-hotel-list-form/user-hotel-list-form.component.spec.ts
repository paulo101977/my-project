import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../../testing/create-service-stub';
import { UserService } from '../../services/user.service';
import { UserPropertyResponse } from '../model/user-property-response';
import { UserResponse } from '../model/user-response';

import { UserHotelListFormComponent } from './user-hotel-list-form.component';

describe('UserHotelListFormComponent', () => {
  let testHostComponent: TestHostComponent;
  let component: UserHotelListFormComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  @Component({
    template: `
      <admin-user-hotel-list-form
        [hotels]="hotels"
        [user]="user"
      ></admin-user-hotel-list-form>
    `
  })
  class TestHostComponent {
    public hotels: any[];
    public user: UserResponse;
  }

  const baseUser: UserResponse = {
    brandList: [],
    chainId: 0,
    chainName: '',
    companyId: 0,
    email: '',
    id: '',
    isActive: false,
    name: '',
    preferredCulture: '',
    preferredLanguage: '',
    propertyId: 0,
    propertyList: [],
    tenantId: '',
    userName: ''
  };

  const baseProperty: UserPropertyResponse = {
    brandId: 0,
    chainId: 0,
    companyId: 0,
    companyUid: '',
    id: 0,
    integrationPartnerPropertyList: [],
    isBlocked: false,
    maxUh: 0,
    name: '',
    propertyStatusId: 0,
    propertyTypeId: 0,
    propertyUId: '',
    registeredUhs: 0,
    tenantId: ''
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ UserHotelListFormComponent, TestHostComponent ],
      imports: [ TranslateTestingModule ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    component = fixture.debugElement.children[0].componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('@Input user OnChange', () => {
    it('shouldn\'t select hotels when user doesn\'t have properties list', () => {
      testHostComponent.user = { ...baseUser };
      fixture.detectChanges();

      expect(component.selectedHotels.length).toBe(0);
    });

    it('should select hotels when user have property list', () => {
      const hotel = { ...baseProperty, id: 1};
      testHostComponent.user = { ...baseUser, propertyList: [hotel] };
      fixture.detectChanges();

      expect(component.selectedHotels).toBeDefined();
      expect(component.selectedHotels).toEqual(jasmine.arrayContaining([
        jasmine.objectContaining({ actions: ['delete']})
      ]));
    });
  });
});
