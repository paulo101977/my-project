import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThfTableColumnIcon } from '@totvs/thf-ui';
import { UserResponse } from '../model/user-response';

@Component({
  selector: 'admin-user-hotel-list-form',
  templateUrl: './user-hotel-list-form.component.html',
  styleUrls: ['./user-hotel-list-form.component.css']
})
export class UserHotelListFormComponent implements OnInit, OnChanges {
  @Input() hotels: any[];
  @Input() user: UserResponse;

  public i18n: any;

  public selectedHotels: any[] = [];
  public actions: ThfTableColumnIcon[] = [
    { value: 'delete', icon: 'thf-icon-delete', action: (hotel: any) => this.deleteHotel(hotel) }
  ];

  constructor(
    private translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.setI18n();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.onUserInputChange(changes);
    this.onHotelsInputChange(changes);
  }

  public getSelectedIds() {
    return this.selectedHotels.map(hotel => hotel.id);
  }

  public handleSelectedHotels(hotels) {
    this.selectHotels(hotels);
  }

  private setI18n() {
    this.i18n = {
      hotelPlus: this.translate.instant('label.hotelPlus'),
      associateHotel: this.translate.instant('action.lbAssociate', {
        labelName: this.translate.instant('label.hotel')
      })
    };
  }

  private onUserInputChange({user}: SimpleChanges): void {
    const hasUser: boolean = !!(user && user.currentValue);

    if (hasUser) {
      this.extractSelectedHotels();
    }
  }

  private onHotelsInputChange({hotels}: SimpleChanges): void {
    const hasHotels: boolean = !!(hotels && hotels.currentValue);
    if (!hasHotels) {
      return;
    }

    const valuesAreDifferent: boolean = hotels.currentValue !== hotels.previousValue;
    const hasPreviousValue: boolean = !!hotels.previousValue;

    if (valuesAreDifferent && hasPreviousValue) {
      this.selectHotels([]);
    }
  }

  private extractSelectedHotels() {
    const {propertyList} = this.user;
    if (!propertyList.length) {
      return;
    }

    this.selectHotels(propertyList);
  }

  private selectHotels(hotels: any[]) {
    this.selectedHotels = hotels.map((hotel: any) => {
      return {
        ...hotel,
        actions: ['delete']
      };
    });
  }

  private deleteHotel(hotel: any) {
    const selectedHotels = this.selectedHotels
      .filter(selectedHotel => selectedHotel !== hotel);

    this.selectHotels(selectedHotels);
  }
}
