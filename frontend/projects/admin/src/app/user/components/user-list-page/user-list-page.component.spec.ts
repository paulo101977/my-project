import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { UserListPageComponent } from './user-list-page.component';
import { TranslateService } from '@ngx-translate/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { Location } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { UserResourceService } from '../../resources/user-resource.service';

describe('UserListPageComponent', () => {
  let component: UserListPageComponent;
  let fixture: ComponentFixture<UserListPageComponent>;

  // @ts-ignore
  const locationStub: Partial<Location> = {
    back: () => {
    }
  };
  let location: Location;
  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };
  let translateService: TranslateService;
  // @ts-ignore
  const userResourceServiceStub: Partial<CompanyApiService> = {
    list: () => of(null)
  };
  let userResourceService: UserResourceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [UserListPageComponent],
      providers: [
        {
          provide: Location,
          useValue: locationStub
        },
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: UserResourceService,
          useValue: userResourceServiceStub
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();

    location = TestBed.get(Location);
    translateService = TestBed.get(TranslateService);
    userResourceService = TestBed.get(UserResourceService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListPageComponent);
    component = fixture.componentInstance;
    component.hasBackAction = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setActions', () => {
      expect(component['setActions']().length).toEqual(2);
      expect(component['setActions']()[0].label).toEqual(component.i18n.action.insert);
      expect(component['setActions']()[0].icon).toEqual('thf-icon-plus');
      expect(component['setActions']()[1].label).toEqual(component.i18n.action.back);
      expect(component['setActions']()[1].icon).toEqual('thf-icon-arrow-left');
    });

    it('should setPageSettings', () => {
      component['setPageSettings']();
      expect(component['pageSettings'].title).toEqual(component.i18n.titlePage);
      expect(component['pageSettings'].filterSettings.placeholder).toEqual(component.i18n.placeholder.search);
    });

    it('should setPageTableSettings', () => {
      component['setPageTableSettings']();
      expect(component['pageTableSettings'].columns.length).toEqual(4);
      expect(component['pageTableSettings'].columns[0].property).toEqual('isActive');
      expect(component['pageTableSettings'].columns[1].property).toEqual('name');
      expect(component['pageTableSettings'].columns[2].property).toEqual('chainName');
      expect(component['pageTableSettings'].columns[3].property).toEqual('actions');
      expect(component['pageTableSettings'].transformData).toEqual(jasmine.any(Function));
      expect(component['pageTableSettings'].getList).toEqual(jasmine.any(Function));
    });
  });

  describe('resources', () => {
    it('should getList', fakeAsync(() => {

      spyOn(userResourceService, 'list').and.returnValue(of({}));

      component['getList']({});

      tick();

      expect(userResourceService.list)
        .toHaveBeenCalledWith({});
    }));
  });
});
