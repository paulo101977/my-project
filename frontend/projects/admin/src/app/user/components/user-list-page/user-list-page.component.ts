import { Component, Input, OnInit } from '@angular/core';
import { PageSettings } from '../../../shared/models/page-settings';
import { PageTableSettings } from '../../../widgets/page-list-wrapper/models/page-table-settings';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ThfPageAction } from '@totvs/thf-ui';
import { UserResourceService } from '../../resources/user-resource.service';
import { User } from '../model/user';

@Component({
  selector: 'admin-user-list-page',
  templateUrl: './user-list-page.component.html',
  styleUrls: ['./user-list-page.component.css']
})
export class UserListPageComponent implements OnInit {

  @Input() hasBackAction = false;
  public pageSettings: PageSettings;
  public pageTableSettings: PageTableSettings;
  public i18n: any;

  constructor(
    private location: Location,
    private translate: TranslateService,
    private userApi: UserResourceService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.setI18n();
    this.setPageSettings();
    this.setPageTableSettings();
  }

  private setI18n() {
    this.i18n = {
      titlePage: this.translate.instant('title.lbRegister', {
        labelName: this.translate.instant('label.user')
      }),
      placeholder: {
        search: this.translate.instant('placeholder.searchByUserHotelCompanyBrandChain')
      },
      label: {
        active: this.translate.instant('label.active'),
        user: this.translate.instant('label.user'),
        chain: this.translate.instant('label.chain')
      },
      action: {
        insert: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.user')
        }),
        back: this.translate.instant('action.back')
      }
    };
  }

  private setPageSettings() {
    this.pageSettings = {
      title: this.i18n.titlePage,
      actions: this.setActions(),
      filterSettings: {
        placeholder: this.i18n.placeholder.search
      }
    };
  }

  private setPageTableSettings() {
    this.pageTableSettings = {
      columns: [
        {property: 'isActive', label: this.i18n.label.active, type: 'boolean'},
        {property: 'name', label: this.i18n.label.user, type: 'string'},
        {property: 'chainName', label: this.i18n.label.chain, type: 'string'},
        {
          property: 'actions',
          label: ' ',
          type: 'icon',
          width: '10%',
          icons: [
            {value: 'edit', icon: 'thf-icon-edit', action: (item) => this.goToEdit(item)}
          ]
        }
      ],
      transformData: this.transformData,
      getList: this.getList.bind(this),
    };
  }

  public getList(params) {
    return this.userApi.list(params);
  }

  public transformData(response) {
    return {
      ...response,
      items: response.items.map((item) => {
        item.actions = ['edit'];
        return item;
      })
    };
  }

  private setActions() {
    const backAction: ThfPageAction = {
      label: this.i18n.action.back,
      icon: 'thf-icon-arrow-left',
      action: () => this.location.back()
    };
    return [
      {
        label: this.i18n.action.insert,
        icon: 'thf-icon-plus',
        action: () => this.goToNew()
      },
      ...(this.hasBackAction ? [backAction] : [])
    ];
  }

  private goToNew() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  private goToEdit(user: User) {
    this.router.navigate([`edit/${user.id}`], {relativeTo: this.route});
  }

}
