import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { createServiceStub } from '../../../../../../../testing/create-service-stub';
import { UserService } from '../../services/user.service';

import { UserDataFormComponent } from './user-data-form.component';

describe('UserDataFormComponent', () => {
  let component: UserDataFormComponent;
  let fixture: ComponentFixture<UserDataFormComponent>;

  const UserProvider = createServiceStub(UserService);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDataFormComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ TranslateTestingModule ],
      providers: [
        FormBuilder,
        UserProvider
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDataFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
