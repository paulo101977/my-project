import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../../services/user.service';
import { UserResponse } from '../model/user-response';

@Component({
  selector: 'admin-user-data-form',
  templateUrl: './user-data-form.component.html',
  styleUrls: ['./user-data-form.component.css']
})
export class UserDataFormComponent implements OnInit, OnChanges {
  @Input() user: UserResponse;
  @Input() chainOptions: any[] = [];

  public form: FormGroup;

  public i18n: any;

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private userService: UserService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    const { user } = changes;

    if (user && user.currentValue) {
      this.updateFormData(user.currentValue);
    }
  }

  ngOnInit() {
    this.setI18n();
    this.setForm();
  }

  public getUserData() {
    return this.form.value;
  }

  private setI18n() {
    this.i18n = {
      label: {
        name: this.translate.instant('label.name'),
        nickname: this.translate.instant('label.nickname'),
        userEmail: this.translate.instant('label.userEmail'),
        chain: this.translate.instant('label.chain'),
        active: this.translate.instant('label.active')
      }
    };
  }

  private setForm() {
    if (this.form) {
      return;
    }

    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      chainId: ['', [Validators.required]],
      isActive: [true]
    });

    this.form.get('chainId').valueChanges.subscribe((chainId: number) => {
      this.userService.selectChain(chainId);
    });
  }

  private updateFormData(user: UserResponse) {
    this.setForm();

    this.form.patchValue({
      name: user.name,
      email: user.email,
      chainId: user.chainId,
      isActive: user.isActive
    });
  }
}
