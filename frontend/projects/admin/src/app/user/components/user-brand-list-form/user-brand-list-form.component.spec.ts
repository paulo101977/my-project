import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../../testing/create-service-stub';
import { UserService } from '../../services/user.service';
import { UserPropertyResponse } from '../model/user-property-response';
import { UserResponse } from '../model/user-response';

import { UserBrandListFormComponent } from './user-brand-list-form.component';

describe('UserBrandListFormComponent', () => {
  let testHostComponent: TestHostComponent;
  let component: UserBrandListFormComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let userService: UserService;

  @Component({
    template: `
      <admin-user-brand-list-form
        [brands]="brands"
        [user]="user"
      ></admin-user-brand-list-form>
    `
  })
  class TestHostComponent {
    public brands: any[];
    public user: UserResponse;
  }

  const baseUser: UserResponse = {
    brandList: [],
    chainId: 0,
    chainName: '',
    companyId: 0,
    email: '',
    id: '',
    isActive: false,
    name: '',
    preferredCulture: '',
    preferredLanguage: '',
    propertyId: 0,
    propertyList: [],
    tenantId: '',
    userName: ''
  };

  const baseProperty: UserPropertyResponse = {
    brandId: 0,
    chainId: 0,
    companyId: 0,
    companyUid: '',
    id: 0,
    integrationPartnerPropertyList: [],
    isBlocked: false,
    maxUh: 0,
    name: '',
    propertyStatusId: 0,
    propertyTypeId: 0,
    propertyUId: '',
    registeredUhs: 0,
    tenantId: ''
  };

  const UserProvider = createServiceStub(UserService, {
    getFilteredBrands(brands: any[]): Observable<any[]> { return of(null); },
    selectBrands(brandsIds: number[]) {},
    getUserBrandsIds(brands: any[], user?: UserResponse): any[] { return []; }
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ UserBrandListFormComponent, TestHostComponent ],
      imports: [ TranslateTestingModule ],
      providers: [
        UserProvider
      ]
    })
    .compileComponents();

    userService = TestBed.get(UserService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    component = fixture.debugElement.children[0].componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('@Input user OnChange', () => {
    it('shouldn\'t select brands while without brands ', () => {
      testHostComponent.user = { ...baseUser };
      fixture.detectChanges();

      expect(component.selectedBrands.length).toBe(0);
    });

    it('shouldn\'t select brands when user doesn\'t have brands or properties list', () => {
      testHostComponent.user = { ...baseUser };
      fixture.detectChanges();

      expect(component.selectedBrands.length).toBe(0);
    });

    it('should select brands given brands and user have brand list', () => {
      const brand = {id: 1};
      testHostComponent.brands = [{id: 1}, {id: 2}];
      testHostComponent.user = { ...baseUser, brandList: [brand] };
      spyOn(userService, 'getUserBrandsIds').and.returnValue([brand.id]);
      fixture.detectChanges();

      expect(component.selectedBrands).toBeDefined();
      expect(component.selectedBrands).toEqual(jasmine.arrayContaining([
        jasmine.objectContaining({ actions: ['delete']})
      ]));
    });

    it('should select brands given brands and user with property list', () => {
      const property = { ...baseProperty, brandId: 1 };
      testHostComponent.brands = [{id: 1}, {id: 2}];
      testHostComponent.user = { ...baseUser, propertyList: [property] };
      spyOn(userService, 'getUserBrandsIds').and.returnValue([property.brandId]);
      fixture.detectChanges();

      expect(component.selectedBrands).toBeDefined();
      expect(component.selectedBrands.length).toBe(1);
      expect(component.selectedBrands).toEqual(jasmine.arrayContaining([
        jasmine.objectContaining({ actions: ['delete']})
      ]));
    });

    it('shouldn\'t select brands given user with property list, but no brands to filter', () => {
      const property = { ...baseProperty, brandId: 1 };
      testHostComponent.user = { ...baseUser, propertyList: [property] };
      fixture.detectChanges();

      expect(component.selectedBrands.length).toBe(0);
    });
  });
});
