import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThfTableColumnIcon } from '@totvs/thf-ui';
import { UserService } from '../../services/user.service';
import { UserResponse } from '../model/user-response';

@Component({
  selector: 'admin-user-brand-list-form',
  templateUrl: './user-brand-list-form.component.html',
  styleUrls: ['./user-brand-list-form.component.css']
})
export class UserBrandListFormComponent implements OnInit, OnChanges {
  @Input() brands: any[];
  @Input() user: UserResponse;

  public i18n: any;

  public selectedBrands: any[] = [];
  public actions: ThfTableColumnIcon[] = [
    { value: 'delete', icon: 'thf-icon-delete', action: (brand: any) => this.deleteBrand(brand) }
  ];

  constructor(
    private userService: UserService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.setI18n();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.onUserChange(changes);
    this.onBrandsChange(changes);
  }

  public getSelectedIds() {
    return this.selectedBrands.map(brand => brand.id);
  }

  public handleSelectedBrands(brands: any[]) {
    this.selectBrands(brands);
  }

  private setI18n() {
    this.i18n = {
      brand: this.translate.instant('label.brand'),
      associateBrand: this.translate.instant('action.lbAssociate', {
        labelName: this.translate.instant('label.brand')
      })
    };
  }

  private onUserChange({user, brands}: SimpleChanges): void {
    const hasUser: boolean = !!(user && user.currentValue);
    const hasBrands: boolean = !!(brands && brands.currentValue);

    if (hasUser && hasBrands) {
      const selectedBrands = this.getSelectedBrands(this.brands, this.user);
      this.selectBrands(selectedBrands);
    }
  }

  private onBrandsChange({brands}: SimpleChanges): void {
    const hasBrands: boolean = !!(brands && brands.currentValue);
    if (!hasBrands) {
      return;
    }

    const valuesAreDifferent: boolean = brands.currentValue !== brands.previousValue;
    const hasPreviousValue: boolean = !!brands.previousValue;

    if (valuesAreDifferent && hasPreviousValue) {
      this.selectBrands([]);
    }
  }

  private getSelectedBrands(brands: any[], user: UserResponse) {
    const brandsIds = this.userService.getUserBrandsIds(brands, user);
    return brands.filter(brand => brandsIds.includes(brand.id));
  }

  private selectBrands(brands: any[]) {
    this.selectedBrands = brands.map((brand: any) => {
      return {
        ...brand,
        actions: ['delete']
      };
    });
    this.userService.selectBrands(brands.map(brand => brand.id));
  }

  private deleteBrand(brand: any) {
    const selectedBrands = this.selectedBrands
      .filter(selectedBrand => selectedBrand !== brand);

    this.selectBrands(selectedBrands);
  }
}
