export class User {
  brandList: any[];
  chainId: number;
  companyId: number;
  companyName: string;
  id: string;
  isActive: boolean;
  name: string;
  propertyId: number;
  propertyList: any[];
  propertyName: string;
  tenantId: string;
  tenantName: string;
}
