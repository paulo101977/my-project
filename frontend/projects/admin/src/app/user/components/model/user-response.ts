import { UserPropertyResponse } from './user-property-response';

export interface UserResponse {
  id: string;
  name: string;
  userName?: string;
  email: string;
  chainId: number;
  chainName: string;
  propertyId: number;
  companyId: number;
  tenantId: string;
  isActive: boolean;
  preferredLanguage: string;
  preferredCulture: string;
  brandList: any[];
  propertyList: UserPropertyResponse[];
}
