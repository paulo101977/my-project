export interface UserPropertyResponse {
  id: number;
  propertyTypeId: number;
  name: string;
  propertyUId: string;
  tenantId: string;
  propertyStatusId: number;
  isBlocked: boolean;
  maxUh: number;
  registeredUhs: number;
  integrationPartnerPropertyList: any[];
  companyId: number;
  brandId: number;
  chainId: number;
  companyUid: string;
}
