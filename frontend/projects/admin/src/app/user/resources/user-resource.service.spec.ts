import { TestBed } from '@angular/core/testing';

import { UserResourceService } from './user-resource.service';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { of } from 'rxjs';

describe('UserResourceService', () => {
  let service: UserResourceService;
  // @ts-ignore
  const adminApiServiceStub: Partial<AdminApiService> = {
    get: () => of(null),
    post: () => of(null),
    put: () => of(null)
  };
  let adminApiService: AdminApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserResourceService,
        {
          provide: AdminApiService,
          useValue: adminApiServiceStub
        }
      ]
    });

    service = TestBed.get(UserResourceService);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call AdminApiService#get', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(adminApiService, 'get');

      service.list(mock);
      expect(adminApiService.get).toHaveBeenCalledWith('UserTotvs', {params: mock});
    });
  });
});
