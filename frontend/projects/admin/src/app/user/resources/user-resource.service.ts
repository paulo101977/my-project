import { Injectable } from '@angular/core';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: 'root'
})
export class UserResourceService {

  constructor(private adminApi: AdminApiService) {
  }

  public list(params: ListQueryParams) {
    return this.adminApi.get('UserTotvs', { params });
  }
}
