import { Injectable } from '@angular/core';
import { UserTotvsResourceService, UserTotvsRequest } from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';
import { UserResponse } from '../components/model/user-response';

@Injectable({
  providedIn: 'root'
})
export class UserTotvsApiService {

  constructor(
    private resource: UserTotvsResourceService
  ) { }

  public getUser(id: string): Observable<UserResponse> {
    return this.resource.getUser(id);
  }

  public createUser(user: UserTotvsRequest) {
    return this.resource.createUser(user);
  }

  public updateUser(user: UserTotvsRequest, id: string) {
    return this.resource.updateUser(user, id);
  }
}
