import { Injectable } from '@angular/core';
import { I18nService, toThfOptions, UserTotvsRequest } from '@inovacaocmnet/thx-bifrost';
import { forkJoin, from, iif, Observable, of, Subject } from 'rxjs';
import { filter, map, pluck, startWith, switchMap, toArray } from 'rxjs/operators';
import { BrandApiService } from '../../brand/services/brand-api.service';
import { ChainResource } from '../../chain/resources/chain-resource';
import { HotelApiService } from '../../hotel/services/hotel-api.service';
import { UserPropertyResponse } from '../components/model/user-property-response';
import { UserResponse } from '../components/model/user-response';
import { UserTotvsApiService } from './user-totvs-api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private selectedChain = new Subject<number>();
  private selectedChain$ = this.selectedChain.asObservable();

  private selectedBrands = new Subject<number[]>();
  private selectedBrands$ = this.selectedBrands.asObservable();

  constructor(
    private userTotvsApi: UserTotvsApiService,
    private chainResource: ChainResource,
    private brandApi: BrandApiService,
    private hotelApi: HotelApiService,
    private i18n: I18nService
  ) {}

  public getData(userId?: string): Observable<{user: UserResponse, chainOptions: any[], brands: any[], hotels: any[]}> {
    const user$ = this.getUser(userId);
    const chainOptions$ = this.getChainOptions();
    const brands$ = this.getBrands();
    const hotels$ = this.getHotels();

    return forkJoin(user$, chainOptions$, brands$, hotels$)
      .pipe(
        map(([user, chainOptions, brands, hotels]) => ({user, chainOptions, brands, hotels}))
      );
  }

  public save(user: any, brandList: number[], propertyList: number[]) {
    const userTotvs = this.prepareUserTotvs(user, brandList, propertyList);
    const create$ = this.userTotvsApi.createUser(userTotvs);
    const update$ = this.userTotvsApi.updateUser(userTotvs, userTotvs.id);
    return userTotvs.id ? update$ : create$;
  }

  public getBrands(): Observable<any> {
    return this.brandApi
      .listAll()
      .pipe(
        pluck('items')
      );
  }

  public getHotels(): Observable<any> {
    return this.hotelApi
      .listAll()
      .pipe(
        pluck('items')
      );
  }

  public selectChain(chainId: number) {
    this.selectedChain.next(chainId);
  }

  public selectBrands(brandsIds: number[]) {
    this.selectedBrands.next(brandsIds);
  }

  public getFilteredBrands(brands: any[], chainId: number = 0): Observable<any[]> {
    const selectedChain$ = this.selectedChain$.pipe(startWith(chainId));
    const getBrandsByChain = (selectedChain: number) => (brand: any) => {
      return selectedChain && brand.chainId == selectedChain;
    };

    return this.filterOnSubject(selectedChain$, brands, getBrandsByChain);
  }

  public getFilteredHotels(hotels: any[], brands: any[], user: UserResponse): Observable<any[]> {
    const brandsIds: number[] = this.getUserBrandsIds(brands, user);
    const selectedBrands$ = this.selectedBrands$.pipe(startWith(brandsIds));
    const getHotelsByBrands = (selectedBrands: number[]) => (hotel: any) => {
      return selectedBrands.length && selectedBrands.includes(hotel.brandId);
    };

    return this.filterOnSubject(selectedBrands$, hotels, getHotelsByBrands);
  }

  public getUserBrandsIds(brands: any[], user?: UserResponse) {
    if (!user) {
      return [];
    }

    if (user.brandList.length) {
      return user.brandList.map(brand => brand.id);
    }

    return this.getUserPropertiesBrandsIds(user.propertyList, brands);
  }

  private getUser(id?: string): Observable<UserResponse> {
    const user$ = this.userTotvsApi.getUser(id);
    return iif(() => !!id, user$, of(undefined));
  }

  private getChainOptions(): Observable<any> {
    const listAllParams = {
      SearchData: '',
      PageSize: 999,
      Page: 1
    };

    return this.chainResource
      .list(listAllParams)
      .pipe(
        pluck('items'),
        switchMap((items: any[]) => from(items)),
        toThfOptions('name', 'id'),
        toArray()
      );
  }

  private filterOnSubject<T, R>(
    subjectObservable$: Observable<T>,
    items: R[],
    predicateGenerator: (project: T) => (item: R) => boolean
  ): Observable<R[]> {
    return subjectObservable$
      .pipe(
        switchMap(emittedValue => from(items)
            .pipe(
              filter(predicateGenerator(emittedValue)),
              toArray()
            )
        )
      );
  }

  private getUserPropertiesBrandsIds(propertyList: UserPropertyResponse[], brands: any[]) {
    const propertyBrandsIds = propertyList.map(property => property.brandId);
    const uniquePropertyBrands = Array.from(new Set(propertyBrandsIds));
    const propertyBrands = brands.filter(brand => uniquePropertyBrands.includes(brand.id));

    return propertyBrands.map(brand => brand.id);
  }

  private prepareUserTotvs(user: any, brandList: number[], propertyList: number[]): UserTotvsRequest {
    const { chainId, ...userData } = user;
    const shouldUseChainId = !(brandList.length || propertyList.length);

    if (propertyList.length) {
      brandList = [];
    }

    return {
      ...userData,
      ...(shouldUseChainId ? {chainId} : {}),
      // TODO use roles logic
      isAdmin: true,
      brandList: brandList.map(brand => ({id: brand})),
      propertyList: propertyList.map(property => ({ id: property })),
      preferredLanguage: this.i18n.getLanguage()
    };
  }
}
