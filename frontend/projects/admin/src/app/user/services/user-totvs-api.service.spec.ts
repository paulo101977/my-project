import { TestBed } from '@angular/core/testing';
import { UserTotvsResourceService } from '@inovacaocmnet/thx-bifrost';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing/create-service-stub';

import { UserTotvsApiService } from './user-totvs-api.service';

describe('UserTotvsApiService', () => {
  let service: UserTotvsApiService;
  let userTotvsResource: UserTotvsResourceService;

  const UserTotvsResourceProvider = createServiceStub(UserTotvsResourceService, {
    getUser(id: string): Observable<any> { return of(null); }
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserTotvsApiService,
        UserTotvsResourceProvider
      ]
    });

    service = TestBed.get(UserTotvsApiService);
    userTotvsResource = TestBed.get(UserTotvsResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getUser', () => {
    it('should call UserTotvsResourceService#getUser', () => {
      spyOn(userTotvsResource, 'getUser');
      const id = 'a98sfd798as6d-087hjg908hj5';
      service.getUser(id);
      expect(userTotvsResource.getUser).toHaveBeenCalledWith(id);
    });
  });
});
