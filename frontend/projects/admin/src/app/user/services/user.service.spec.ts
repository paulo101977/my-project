import { TestBed } from '@angular/core/testing';
import { I18nService } from '@inovacaocmnet/thx-bifrost';
import { Observable, of } from 'rxjs';
import { createServiceStub } from '../../../../../../testing/create-service-stub';
import { BrandApiService } from '../../brand/services/brand-api.service';
import { ChainResource } from '../../chain/resources/chain-resource';
import { HotelApiService } from '../../hotel/services/hotel-api.service';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { UserResponse } from '../components/model/user-response';
import { chainOptionsMock } from '../mocks/chain-options-mock';
import { userResponseMock } from '../mocks/user-response-mock';
import { UserTotvsApiService } from './user-totvs-api.service';

import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;
  let userTotvsApi: UserTotvsApiService;
  let chainResource: ChainResource;
  const chainOptions = [{
    label: chainOptionsMock.items[0].name,
    value: chainOptionsMock.items[0].id
  }];

  const UserTotvsApiProvider = createServiceStub(UserTotvsApiService, {
    getUser(id: string): Observable<UserResponse> { return of(userResponseMock); }
  });
  const ChainResourceProvider = createServiceStub(ChainResource, {
    list(params: ListQueryParams): Observable<any> { return of(chainOptionsMock); }
  });
  const BrandApiProvider = createServiceStub(BrandApiService, {
    listAll(): Observable<any> { return of({items: []}); }
  });
  const HotelApiProvider = createServiceStub(HotelApiService, {
    list(params: ListQueryParams): Observable<any> { return of({items: []}); },
    listAll(): Observable<any> { return of({items: []}); }
  });
  const I18nProvider = createServiceStub(I18nService, {
    getLanguage(): string { return 'null'; }
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserService,
        UserTotvsApiProvider,
        ChainResourceProvider,
        BrandApiProvider,
        HotelApiProvider,
        I18nProvider
      ]
    });

    service = TestBed.get(UserService);
    userTotvsApi = TestBed.get(UserTotvsApiService);
    chainResource = TestBed.get(ChainResource);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getData', () => {
    it('should always get chainOptions data', () => {
      let data;
      service
        .getData()
        .subscribe(response => data = response);

      expect(data.chainOptions).toEqual(chainOptions);
    });

    it('shouldn\'t get user data when userId wasn\'t given', () => {
      let data;
      service
        .getData()
        .subscribe(response => data = response);

      expect(data.user).toBeUndefined();
    });

    it('should get user data when userId was given', () => {
      const id = '0sad98f70a-a89sd98a7sd';
      let data;
      service
        .getData(id)
        .subscribe(response => data = response);

      expect(data.user).toEqual(userResponseMock);
    });
  });
});
