import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ThfButtonModule, ThfFieldModule, ThfNotificationModule, ThfPageModule, ThfTableModule } from '@totvs/thf-ui';
import { BrandModule } from '../brand/brand.module';
import { HotelModule } from '../hotel/hotel.module';
import { SharedModule } from '../shared/shared.module';
import { PageListWrapperModule } from '../widgets/page-list-wrapper/page-list-wrapper.module';
import { UserBrandListFormComponent } from './components/user-brand-list-form/user-brand-list-form.component';
import { UserDataFormComponent } from './components/user-data-form/user-data-form.component';
import { UserFormPageComponent } from './components/user-form-page/user-form-page.component';
import { UserHotelListFormComponent } from './components/user-hotel-list-form/user-hotel-list-form.component';
import { UserListPageComponent } from './components/user-list-page/user-list-page.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PageListWrapperModule,
    ThfTableModule,
    ThfPageModule,
    ThfFieldModule,
    ThfButtonModule,
    ThfNotificationModule,
    SharedModule,
    BrandModule,
    HotelModule
  ],
  declarations: [
    UserListPageComponent,
    UserFormPageComponent,
    UserDataFormComponent,
    UserBrandListFormComponent,
    UserHotelListFormComponent
  ],
  exports: [UserListPageComponent, UserFormPageComponent, UserDataFormComponent],
})
export class UserModule {}
