export const userResponseMock = {
  brandList: [],
  chainId: 0,
  chainName: '',
  companyId: 0,
  email: '',
  id: '',
  isActive: false,
  name: '',
  preferredCulture: '',
  preferredLanguage: '',
  propertyId: 0,
  propertyList: [],
  tenantId: '',
  userName: ''
};
