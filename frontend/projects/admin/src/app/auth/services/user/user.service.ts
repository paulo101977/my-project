import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Subject } from 'rxjs';
import { I18nService } from '@inovacaocmnet/thx-bifrost';
import { User } from '../../models/user';
import { UserToken } from '../../models/user-token';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly KEY_USER = 'admin.user';
  private jwtHelperService: JwtHelperService;
  private userLocalStorageChange = new Subject<User>();
  userLocalStorageChanged$ = this.userLocalStorageChange.asObservable();

  constructor(
    private i18nService: I18nService
  ) {
    this.jwtHelperService = new JwtHelperService();
  }

  public createUserFromToken(token): User {
    const userToken: UserToken = this.jwtHelperService.decodeToken(token);
    return {
      email: userToken.LoggerUserEmail,
      id: userToken.LoggedUserUid,
      isAdmin: userToken.IsAdmin === 'true',
      name: userToken.LoggedUserName,
      preferredCulture: userToken.PreferredCulture,
      preferredLanguage: userToken.PreferredLanguage,
      tenantId: userToken.TenantId,
      userName: userToken.LoggedUserName,

      // TODO: preencher valor quando existir
      isActive: false,
      lastLink: '',
      personId: '',
    };
  }

  public setUser(user: User) {
    this.i18nService.setLanguage(user.preferredCulture);
    this.setInStorage(user);
    this.userLocalStorageChange.next(user);
  }

  public getUser(): User {
    return this.getInStorage();
  }

  private getInStorage(): User {
    return JSON.parse(localStorage.getItem(this.KEY_USER));
  }

  private setInStorage(user: User) {
    localStorage.setItem(this.KEY_USER, JSON.stringify(user));
  }
}
