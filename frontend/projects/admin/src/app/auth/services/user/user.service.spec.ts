import { TestBed, inject } from '@angular/core/testing';
import { I18nService } from '@inovacaocmnet/thx-bifrost';

import { UserService } from './user.service';

describe('UserService', () => {
  const i18nStubService: Partial<I18nService> = {
    setLanguage(newLanguage: string) {}
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserService,
        { provide: I18nService, useValue: i18nStubService }
      ]
    });
  });

  it('should be created', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));
});
