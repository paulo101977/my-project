import { TestBed } from '@angular/core/testing';
import { User } from '../../models/user';
import { AuthResource } from '../../resources/auth/auth.resource';
import { UserService } from '../user/user.service';

import { AuthService } from './auth.service';
import { of } from 'rxjs';

describe('AuthService', () => {
  let service: AuthService;
  // @ts-ignore
  const authResourceStub: Partial<AuthResource> = {
    login: () => of(null)
  };
  let authResource: AuthResource;

  const userStubService: Partial<UserService> = {
    createUserFromToken(token): User {
      return {
        email: '',
        id: '',
        isActive: false,
        isAdmin: false,
        lastLink: '',
        name: '',
        personId: '',
        preferredCulture: '',
        preferredLanguage: '',
        tenantId: '',
        userName: ''
      };
    },
    setUser(user: User) {}
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: AuthResource, useValue: authResourceStub },
        { provide: UserService, useValue: userStubService }
      ]
    });

    authResource = TestBed.get(AuthResource);
    service = TestBed.get(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#authenticate', () => {
    it('should call AuthResource#login', () => {
      const mock = {login: '', password: ''};
      const user: User = {
        email: '',
        id: '',
        isActive: false,
        isAdmin: false,
        lastLink: '',
        name: '',
        personId: '',
        preferredCulture: '',
        preferredLanguage: '',
        tenantId: '',
        userName: ''
      };
      spyOn(authResource, 'login').and.returnValue(of(user));

      service.authenticate(mock);
      expect(authResource.login).toHaveBeenCalledWith(mock);
    });
  });
});
