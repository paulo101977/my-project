import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { LoginForm } from '../../models/login-form';
import { User } from '../../models/user';
import { AuthResource } from '../../resources/auth/auth.resource';
import { UserService } from '../user/user.service';
import { TokenManagerService } from '@inovacaocmnet/thx-bifrost';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly  CONTEXT_KEY_NAME = 'admin';

  constructor(
    private resource: AuthResource,
    private userService: UserService,
    private tokenManagerService: TokenManagerService
  ) {}

  public authenticate(params: LoginForm) {
    const setToken = tap(({ newPasswordToken }) => this.setToken(newPasswordToken));
    const getUserFromToken = map(({ newPasswordToken }) => this.userService.createUserFromToken(newPasswordToken));
    const upperCaseLanguage = (languageIso) => {
      const [language, country] = languageIso.split('-');
      return `${language}-${country.toUpperCase()}`;
    };
    const upperCaseUserLanguage = map((user: User) => {
      return {
        ...user,
        preferredLanguage: upperCaseLanguage(user.preferredLanguage),
        preferredCulture: upperCaseLanguage(user.preferredCulture)
      };
    });
    const setUser = tap((user: User) => this.userService.setUser(user));
    params.grantType = 'password';

    return this.resource
      .login(params)
      .pipe(
        setToken,
        getUserFromToken,
        upperCaseUserLanguage,
        setUser
      );
  }

  public resetPassword(email: string) {
    return this.resource.resetPassword(email);
  }

  public setToken(token: string): void {
    this.tokenManagerService.setToken(this.CONTEXT_KEY_NAME, token);
  }
}
