export class User {
  userName: string;
  tenantId: string;
  personId: string;
  preferredLanguage: string;
  preferredCulture: string;
  name: string;
  lastLink: string;
  isAdmin: boolean;
  isActive: boolean;
  id: string;
  email: string;
}
