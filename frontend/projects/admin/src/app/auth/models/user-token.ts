export class UserToken {
  BrandId: string;
  ChainId: string;
  IsAdmin: string;
  LoggedUserName: string;
  LoggedUserUid: string;
  LoggerUserEmail: string;
  PreferredCulture: string;
  PreferredLanguage: string;
  PropertyCulture: string;
  PropertyId: string;
  PropertyLanguage: string;
  TenantId: string;
  TimeZoneName: string;
  aud: string;
  exp: number;
  iat: number;
  iss: string;
  jti: string;
  nbf: number;
  sub: string;
  unique_name: string;
}
