import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginForm } from '../../models/login-form';
import { AuthService } from '../../services/auth/auth.service';
import { ProductEnum } from '@inovacaocmnet/thx-bifrost';

@Component({
  selector: 'admin-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(
    private authService: AuthService,
    public route: Router,
  ) {}

  public onSubmit(loginForm) {
    const loginFormData: LoginForm = {
      login: loginForm.email,
      password: loginForm.password,
      productId: ProductEnum.SuperAdmin
    };
    this.authenticate(loginFormData)
      .subscribe(() => { this.navigateToHome(); });
  }

  public onResetPassword(email: string) {
    const confirmPasswordWasReset = (response) => {
      // TODO exibir a mensagem pro usuário
      console.log('password was reset');
    };
    const confirmThereWasAnError = (err) => {
      // TODO exibir a mensagem pro usuário
      console.log('something went wrong');
    };

    this.resetPassword(email)
      .subscribe(confirmPasswordWasReset, confirmThereWasAnError);
  }

  private resetPassword(email: string) {
    return this.authService.resetPassword(email);
  }

  public authenticate(loginForm: LoginForm) {
    return this.authService.authenticate(loginForm);
  }

  public navigateToHome() {
    this.route.navigateByUrl('/admin');
  }
}
