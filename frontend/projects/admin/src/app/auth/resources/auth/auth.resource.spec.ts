import { TestBed } from '@angular/core/testing';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';

import { AuthResource } from './auth.resource';
import { of } from 'rxjs';

describe('AuthResource', () => {
  let service: AuthResource;
  // @ts-ignore
  const adminApiServiceStub: Partial<AdminApiService> = {
    post: () => of(null)
  };
  let adminApiService: AdminApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthResource,
        { provide: AdminApiService, useValue: adminApiServiceStub }
      ]
    });

    service = TestBed.get(AuthResource);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#login', () => {
    it('should call AdminApiService#post', () => {
      const mock = {login: '', password: ''};
      spyOn(adminApiService, 'post');

      service.login(mock);
      expect(adminApiService.post).toHaveBeenCalledWith('UserTotvs/login', mock);
    });
  });
});
