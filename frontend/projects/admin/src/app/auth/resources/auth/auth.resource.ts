import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { LoginForm } from '../../models/login-form';

@Injectable({
  providedIn: 'root'
})
export class AuthResource {

  constructor(private adminApi: AdminApiService) { }

  public login(params: LoginForm) {
    return this.adminApi.post('UserTotvs/login', params);
  }

  public getUserById(userId) {
    return this.adminApi.get(`UserTotvs/GetByUid/${userId}`);
  }

  public resetPassword(email: string) {
    return this.adminApi.post(`UserTotvs/forgotpassword/${email}`);
  }
}
