import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './components/login/login.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { LoginModule, configureRecaptcha } from '@inovacao-cmnet/thx-ui';

const RecaptchaConfiguration = configureRecaptcha({
  sitekey: '6Lc_K44UAAAAAJ07K1ygtZT3BrmNIvzA73QdjmOr'
});

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    LoginModule
  ],
  declarations: [
    LoginComponent,
    ResetPasswordComponent
  ],
  providers: [
    RecaptchaConfiguration
  ]
})
export class AuthModule { }
