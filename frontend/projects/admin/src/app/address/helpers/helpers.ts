import { AddressType } from 'app/shared/models/address-type-enum';

export function addAddressTypeField(location) {
  return {
    ...location,
    addressType: location.locationCategory.name
  };
}

export function extractAddressFromGooglePlaces(googlePlacesResponse) {
  const address: any = {
    postalCode: '',
    streetNumber: ''
  };

  if (googlePlacesResponse) {
    const { address_components: addressComponents, geometry } = googlePlacesResponse;

    address.latitude = geometry.location.lat();
    address.longitude = geometry.location.lng();

    return addressComponents.reduce((newAddress, addressComponent) => {
      return getAddressDataFromAddressComponent(newAddress, addressComponent);
    }, address);
  }

  return address;
}

function getAddressDataFromAddressComponent(address, addressComponent) {
  const newAddress = { ...address };
  const type = addressComponent.types[0];
  const addressComponentTypesProps = {
    route: ['streetName', 'long_name'],
    sublocality_level_1: ['neighborhood', 'long_name'],
    administrative_area_level_1: ['division', 'short_name'],
    administrative_area_level_2: ['subdivision', 'long_name'],
    administrative_area_level_3: ['subdivision', 'long_name'],
    locality: ['subdivision', 'long_name'],
    sublocality: ['subdivision', 'long_name'],
    postal_code: ['postalCode', 'long_name'],
    country: ['countryCode', 'short_name'],
    street_number: ['streetNumber', 'long_name'],
  };
  const typeProps = addressComponentTypesProps[type];

  if (typeProps) {
    const [ toProp, fromProp ] = typeProps;
    newAddress[toProp] = addressComponent[fromProp];
  }

  return newAddress;
}

export function getAddressTypes() {
  return [
    { label: 'sharedModule.components.address.commercial', value: AddressType.Comercial },
    { label: 'sharedModule.components.address.residential', value: AddressType.Residencial },
    { label: 'sharedModule.components.address.correspondence', value: AddressType.Correspondencia },
    { label: 'sharedModule.components.address.billing', value: AddressType.Cobranca },
    { label: 'sharedModule.components.address.delivery', value: AddressType.Entrega },
  ];
}
