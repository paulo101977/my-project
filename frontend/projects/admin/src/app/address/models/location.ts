import { LocationCategoryEnum } from 'app/shared/models/location-category-enum';

export interface Location {
  id?: number;
  locationCategoryId: LocationCategoryEnum;
  latitude: number;
  longitude: number;
  streetName: string;
  streetNumber: string;
  additionalAddressDetails: string;
  neighborhood: string;
  cityId: number;
  postalCode: number;
  countryCode: number;
  subdivision: string;
  division: string;
  country: string;
}
