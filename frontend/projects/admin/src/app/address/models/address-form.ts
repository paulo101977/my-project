import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddressType } from 'app/shared/models/address-type-enum';
import { Location } from './location';

export class AddressForm {
  public form: FormGroup;
  private fb = new FormBuilder();
  private defaultValues = {
    addressType: AddressType.Comercial,
    country: 'BR',
    latitude: 0,
    longitude: 0
  };

  constructor() {
    this.form = this.fb.group({
      id: [''],
      search: [''],
      country: [this.defaultValues.country, Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      district: ['', Validators.required],
      locality: ['', Validators.required],
      number: ['', Validators.required],
      complement: [''],
      postalCode: ['', Validators.required],
      addressType: [this.defaultValues.addressType, Validators.required],
      latitude: [this.defaultValues.latitude, Validators.required],
      longitude: [this.defaultValues.longitude, Validators.required],
    });
  }

  public setFormData(location: Location) {
    const formData = this.locationToFormData(location);
    this.form.patchValue(formData);
  }

  public resetForm(location?: Location) {
    const formData = location ? this.locationToFormData(location) : this.defaultValues;
    this.form.reset(formData);
  }

  private locationToFormData(location: Location) {
    return {
      id: location.id,
      country: location.countryCode || this.defaultValues.country,
      state: location.division,
      city: location.subdivision,
      district: location.neighborhood,
      locality: location.streetName,
      number: location.streetNumber,
      complement: location.additionalAddressDetails,
      postalCode: location.postalCode,
      addressType: location.locationCategoryId || this.defaultValues.addressType,
      latitude: location.latitude || this.defaultValues.latitude,
      longitude: location.longitude || this.defaultValues.longitude,
    };
  }
}
