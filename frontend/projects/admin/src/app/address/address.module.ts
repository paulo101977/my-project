import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ThfButtonModule, ThfFieldModule, ThfModalModule, ThfTableModule } from '@totvs/thf-ui';
import { AngularGooglePlaceModule } from 'ngx-google-places';
import { SharedModule } from '../shared/shared.module';
import { AddressFormModalComponent } from './components/address-form-modal/address-form-modal.component';
import { AddressListComponent } from './components/address-list/address-list.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    AngularGooglePlaceModule,
    ThfButtonModule,
    ThfTableModule,
    ThfModalModule,
    ThfFieldModule
  ],
  declarations: [AddressListComponent, AddressFormModalComponent],
  exports: [AddressListComponent, AddressFormModalComponent]
})
export class AddressModule { }
