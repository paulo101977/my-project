import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThfModalComponent } from '@totvs/thf-ui';
import { ModalSettings } from '../../../shared/models/modal-settings';
import { AddressFormModalComponent } from '../address-form-modal/address-form-modal.component';

@Component({
  selector: 'admin-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.css']
})
export class AddressListComponent implements OnInit, OnChanges {
  @ViewChild('addressModal') addressModal: AddressFormModalComponent;
  @ViewChild('deleteModal') deleteModal: ThfModalComponent;
  @Input() locations: any[];
  @Output() updatedLocations = new EventEmitter();

  public i18n: any;
  public tableSettings;
  public locationToDelete = null;
  public modalSettings: ModalSettings;

  private currentLocationIndex = null;

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setI18n();
    this.setTableSettings();
    this.setModalSettings();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.locations && this.tableSettings) {
      this.setLocationsAsTableItems(this.locations);
    }
  }

  private setTableSettings() {
    this.tableSettings = {
      columns: [
        {property: 'addressType', label: this.i18n.address.addressType, type: 'string'},
        {property: 'country', label: this.i18n.address.country, type: 'string'},
        {property: 'postalCode', label: this.i18n.address.postalCode, type: 'number'},
        {property: 'uf', label: this.i18n.address.federativeUnit, type: 'string'},
        {property: 'subdivision', label: this.i18n.address.city, type: 'string'},
        {property: 'neighborhood', label: this.i18n.address.district, type: 'string'},
        {property: 'streetName', label: this.i18n.address.locality, type: 'string'},
        {property: 'streetNumber', label: this.i18n.address.number, type: 'number'},
        {property: 'additionalAddressDetails', label: this.i18n.address.complement, type: 'string'},
      ],
      actions: [
        {
          label: this.i18n.action.edit,
          action: (address) => this.openEditAddressModal(address)
        },
        {
          label: this.i18n.action.delete,
          action: (address) => this.confirmDeleteAddress(address)
        },
      ]
    };
    this.setLocationsAsTableItems(this.locations);
  }

  private setI18n() {
    this.i18n = {
      title: {
        delete: this.translate.instant('action.lbDelete', {
          labelName: this.translate.instant('label.address')
        })
      },
      group: {
        address: this.translate.instant('label.address'),
      },
      action: {
        insertAddress: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.address').toLowerCase()
        }),
        edit: this.translate.instant('action.edit'),
        delete: this.translate.instant('action.delete'),
        confirm: this.translate.instant('action.confirm'),
        cancel: this.translate.instant('action.cancel')
      },
      message: {
        confirmDelete: this.translate.instant('variable.lbConfirmDeleteM', {
          labelName: this.translate.instant('label.address')
        })
      },
      address: {
        postalCode: this.translate.instant('commomData.address.postalCode'),
        addressType: this.translate.instant('commomData.address.addressType'),
        locality: this.translate.instant('commomData.address.locality'),
        number: this.translate.instant('commomData.address.number'),
        complement: this.translate.instant('commomData.address.complement'),
        federativeUnit: this.translate.instant('commomData.address.federativeUnit'),
        district: this.translate.instant('commomData.address.district'),
        city: this.translate.instant('commomData.address.city'),
        country: this.translate.instant('commomData.address.country'),
      },
    };
  }

  private setModalSettings() {
    this.modalSettings = {
      title: this.i18n.title.delete,
      primaryAction: {
        action: () => this.deleteLocation(this.locationToDelete),
        label: this.i18n.action.confirm
      },
      secondaryAction: {
        action: () => this.deleteModal.close(),
        label: this.i18n.action.cancel
      }
    };

  }

  public openCreateAddressModal() {
    this.currentLocationIndex = null;
    this.addressModal.openCreateModal();
  }

  private openEditAddressModal(location: any) {
    this.currentLocationIndex = this.locations.indexOf(location);
    this.addressModal.openUpdateModal(location);
  }

  public onAddressFormSubmit(formValues) {
    const updatedLocations = this.getUpdatedLocations(this.locations, formValues);
    this.updatedLocations.emit(updatedLocations);
    this.addressModal.closeModal();
  }

  private getUpdatedLocations(locations = [], location) {
    const updateOldLocation = this.currentLocationIndex !== null;

    if (updateOldLocation) {
      return this.updateList(locations, location);
    }

    return [
      ...locations,
      location
    ];
  }

  private updateList(list, item) {
    const itemIndex = this.currentLocationIndex;
    const newList = [ ...list ];
    const index = itemIndex > - 1 ? itemIndex : newList.length;

    this.currentLocationIndex = null;

    newList[index] = item;
    return newList;
  }

  private deleteLocation(location: any) {
    const index = this.locations.indexOf(location);
    const updatedLocations = [
      ...this.locations.slice(0, index),
      ...this.locations.slice(index + 1, this.locations.length)
    ];

    this.updatedLocations.emit(updatedLocations);
    this.deleteModal.close();
  }

  private confirmDeleteAddress(location: any) {
    this.deleteModal.open();
    this.locationToDelete = location;
  }

  private setLocationsAsTableItems(locations) {
    this.tableSettings.items = locations || [];
  }
}
