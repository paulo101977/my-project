import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';

import { AddressListComponent } from './address-list.component';

describe('AddressListComponent', () => {
  let component: AddressListComponent;
  let fixture: ComponentFixture<AddressListComponent>;

  const translateStubService: Partial<TranslateService> = {
    instant(key: string | Array<string>, interpolateParams?: Object): string | any { return 'null'; }
  };

  @Component({ selector: 'admin-address-form-modal', template: '' })
  class AddressFormModalStubComponent {
    openCreateModal() {}
    closeModal() {}
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressListComponent, AddressFormModalStubComponent ],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('OnInit', () => {
    it('should setTableSettings', () => {
      const locations = [];
      component.locations = locations;
      fixture.detectChanges();

      const columns = [
        {property: 'addressType', label: component.i18n.address.addressType, type: 'string'},
        {property: 'country', label: component.i18n.address.country, type: 'string'},
        {property: 'postalCode', label: component.i18n.address.postalCode, type: 'number'},
        {property: 'uf', label: component.i18n.address.federativeUnit, type: 'string'},
        {property: 'subdivision', label: component.i18n.address.city, type: 'string'},
        {property: 'neighborhood', label: component.i18n.address.district, type: 'string'},
        {property: 'streetName', label: component.i18n.address.locality, type: 'string'},
        {property: 'streetNumber', label: component.i18n.address.number, type: 'number'},
        {property: 'additionalAddressDetails', label: component.i18n.address.complement, type: 'string'},
      ];

      expect(component.tableSettings.columns).toEqual(columns);
      expect(component.tableSettings.items).toEqual(locations);
    });
  });

  describe('#openCreateAddressModal', () => {
    it('should call addressModal#openCreateModal', () => {
      spyOn(component.addressModal, 'openCreateModal').and.callThrough();

      component.openCreateAddressModal();

      expect(component.addressModal.openCreateModal).toHaveBeenCalled();
    });
  });

  describe('#onAddressFormSubmit', () => {
    it('should add a location if it doesn\'t have an id', () => {
      let emittedLocations = [];
      const newLocation = {};

      component.updatedLocations.subscribe((locations) => {
        emittedLocations = locations;
      });

      expect(emittedLocations.length).toEqual(0);

      component.onAddressFormSubmit(newLocation);

      expect(emittedLocations.length).toEqual(1);
      expect(emittedLocations[0]).toEqual(newLocation);
    });

    it('should add a location if it doesn\'t match any id', () => {
      let emittedLocations = [];
      const newLocation = { id: 7 };
      component.locations = [ { id: 1 }, 7, { id: 3 } ];

      component.updatedLocations.subscribe((locations) => {
        emittedLocations = locations;
      });

      expect(emittedLocations.length).toEqual(0);

      component.onAddressFormSubmit(newLocation);

      expect(emittedLocations.length).toEqual(4);
      expect(emittedLocations[3]).toEqual(newLocation);
    });

    it('should replace a location on index match', () => {
      let emittedLocations = [];
      const newLocation = { id: 7, prop: 'val' };
      component.locations = [ { id: 1 }, { id: 7 }, { id: 3 } ];
      component['currentLocationIndex'] = 1;

      component.updatedLocations.subscribe((locations) => {
        emittedLocations = locations;
      });

      expect(emittedLocations.length).toEqual(0);

      component.onAddressFormSubmit(newLocation);

      expect(emittedLocations.length).toEqual(3);
      expect(emittedLocations[1]).toEqual(newLocation);
    });
  });
});
