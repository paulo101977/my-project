import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ThexAddressApiService } from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

import { AddressFormModalComponent } from './address-form-modal.component';

describe('AddressFormModalComponent', () => {
  let component: AddressFormModalComponent;
  let fixture: ComponentFixture<AddressFormModalComponent>;

  const translateStub: Partial<TranslateService> = {
    instant(key: string | Array<string>, interpolateParams?: Object): string | any { return 'null'; }
  };

  const thexAddressApiStub: Partial<ThexAddressApiService> = {
    getCountries(): Observable<any> { return of({items: []}); }
  };

  @Component({selector: 'thf-modal', template: ''}) // tslint:disable-line
  class ThfModalComponentStubComponent {
    open() {}
    close() {}
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressFormModalComponent, ThfModalComponentStubComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        FormBuilder,
        {
          provide: TranslateService,
          useValue: translateStub
        },
        {
          provide: ThexAddressApiService,
          useValue: thexAddressApiStub
        },
        FormBuilder
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressFormModalComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set all settings on ngOnInit', () => {
    expect(component.form).toBeUndefined();
    expect(component.modalSettings).toBeUndefined();

    fixture.detectChanges();

    expect(component.form).toBeDefined();
    expect(component.modalSettings).toBeDefined();
  });

  describe('#openCreateModal', () => {
    beforeEach(() => {
      fixture.detectChanges();
    });

    it('should change the modalSettings', () => {
      expect(component.modalSettings.title).toBe('');

      component.openCreateModal();

      expect(component.modalSettings.title).not.toBe('');
    });
  });

  describe('#openUpdateModal', () => {
    beforeEach(() => {
      fixture.detectChanges();
    });

    it('should change the modalSettings', () => {
      expect(component.modalSettings.title).toBe('');

      component.openUpdateModal({
        additionalAddressDetails: '',
        cityId: 0,
        country: '',
        countryCode: 0,
        division: '',
        id: 0,
        latitude: 0,
        locationCategoryId: undefined,
        longitude: 0,
        neighborhood: '',
        postalCode: 0,
        streetName: '',
        streetNumber: '',
        subdivision: ''
      });

      expect(component.modalSettings.title).not.toBe('');
    });
  });

  describe('#getAddress', () => {
    const defaultValue = {
      postalCode: '',
      streetNumber: ''
    };
    const location = {
      lat: () => 'latitude',
      lng: () => 'longitude'
    };

    beforeEach(() => {
      fixture.detectChanges();
      spyOn(component['addressForm'], 'setFormData');
    });

    it('should return a default address when nothing is passed', () => {
      component.getAddress(undefined);
      expect(component['addressForm'].setFormData).toHaveBeenCalledWith(defaultValue);
    });

    it('should get the latitude and longitude from address', () => {
      const googlePlacesResponse = {
        address_components: [],
        geometry: { location }
      };
      const response = {
        ...defaultValue,
        latitude: location.lat(),
        longitude: location.lng()
      };

      component.getAddress(googlePlacesResponse);
      expect(component['addressForm'].setFormData).toHaveBeenCalledWith(response);
    });

    it('should get the address data from google places', () => {
      const response = {
        latitude: location.lat(),
        longitude: location.lng(),
        countryCode: 'BR',
        division: 'RM',
        neighborhood: 'engenho of tal',
        postalCode: '20950-300',
        streetName: 'tal street',
        streetNumber: 110,
        subdivision: 'river of month'
      };
      const googlePlacesResponse = {
        address_components: [
          { types: ['route'], long_name: response.streetName },
          { types: ['sublocality_level_1'], long_name: response.neighborhood },
          { types: ['administrative_area_level_1'], short_name: response.division },
          { types: ['administrative_area_level_2'], long_name: response.subdivision },
          { types: ['postal_code'], long_name: response.postalCode },
          { types: ['country'], short_name: response.countryCode },
          { types: ['street_number'], long_name: response.streetNumber },
        ],
        geometry: { location }
      };

      component.getAddress(googlePlacesResponse);
      expect(component['addressForm'].setFormData).toHaveBeenCalledWith(response);
    });
  });

  describe('#setGooglePlacesCountry', () => {
    it('should set the google places component restriction', () => {
      component.optionGooglePlaceApi = { componentRestrictions: { country: 'BR' } };
      component.setGooglePlacesCountry('BRUS');

      expect(component.optionGooglePlaceApi.componentRestrictions).toEqual({ country: 'BRUS' });
    });
  });
});
