import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfModalComponent } from '@totvs/thf-ui';
import { ThexAddressApiService } from '@inovacaocmnet/thx-bifrost';
import { AddressType } from 'app/shared/models/address-type-enum';
import { map, tap } from 'rxjs/operators';
import { itemsToOptions } from '@inovacaocmnet/thx-bifrost';
import { extractAddressFromGooglePlaces, getAddressTypes } from '../../helpers/helpers';
import { AddressForm } from '../../models/address-form';
import { Location } from '../../models/location';

@Component({
  selector: 'admin-address-form-modal',
  templateUrl: './address-form-modal.component.html',
  styleUrls: ['./address-form-modal.component.css']
})
export class AddressFormModalComponent implements OnInit {
  @ViewChild('modal') modal: ThfModalComponent;
  @Output() submitted = new EventEmitter<any>();

  public optionGooglePlaceApi: any;
  public form: FormGroup;
  public i18n: any;
  public modalSettings: any;
  public countries: any[] = [];
  public addressTypes: AddressType[] = [];
  private addressForm: AddressForm;
  private mappedCountries = {};
  private mappedAddressTypes = {};

  constructor(
    private translate: TranslateService,
    private thexAddressApi: ThexAddressApiService
  ) {}

  ngOnInit() {
    this.setForm();
    this.setGooglePlacesSettings();
    this.setCountries();
    this.setAddressTypes();
    this.setI18n();
    this.setModalSettings();
  }

  public openCreateModal() {
    this.addressForm.resetForm();
    this.changeModalToCreateMode();
    this.modal.open();
  }

  public openUpdateModal(location: Location) {
    this.addressForm.resetForm(location);
    this.changeModalToEditMode();
    this.modal.open();
  }

  public getAddress(place) {
    const address = extractAddressFromGooglePlaces(place);
    this.addressForm.setFormData(address);
  }

  public setGooglePlacesCountry(country) {
    this.optionGooglePlaceApi = {
      componentRestrictions: { country },
    };
  }

  private setGooglePlacesSettings() {
    this.setGooglePlacesCountry(this.form.value.country);
  }

  private setForm() {
    this.addressForm = new AddressForm();
    this.form = this.addressForm.form;
  }

  private setCountries() {
    const getItems = map(({items}) => items);
    const mapCountriesIntoOptions = map(itemsToOptions('name', 'twoLetterIsoCode'));
    const setMappedCountries = tap((options) => this.mappedCountries = this.optionsToMap(options));

    this.thexAddressApi.getCountries()
      .pipe(
        getItems,
        mapCountriesIntoOptions,
        setMappedCountries
      )
      .subscribe((response: any[]) => {
        this.countries = response;
      });
  }

  private setAddressTypes() {
    const translateLabel = (item) => {
      item.label = this.translate.instant(item.label);
      return item;
    };

    this.addressTypes = getAddressTypes().map(translateLabel);
    this.mappedAddressTypes = this.optionsToMap(this.addressTypes);
  }

  private setI18n() {
    this.i18n = {
      title: {
        insert: this.translate.instant('title.lbInsert', {
          labelName: this.translate.instant('label.address')
        }),
        edit: this.translate.instant('title.lbEdit', {
          labelName: this.translate.instant('label.address')
        })
      },
      action: {
        confirm: this.translate.instant('action.confirm'),
        cancel: this.translate.instant('action.cancel'),
      },
      address: {
        search: this.translate.instant('commomData.address.search'),
        postalCode: this.translate.instant('commomData.address.postalCode'),
        addressType: this.translate.instant('commomData.address.addressType'),
        locality: this.translate.instant('commomData.address.locality'),
        number: this.translate.instant('commomData.address.number'),
        complement: this.translate.instant('commomData.address.complement'),
        federativeUnit: this.translate.instant('commomData.address.federativeUnit'),
        district: this.translate.instant('commomData.address.district'),
        city: this.translate.instant('commomData.address.city'),
        country: this.translate.instant('commomData.address.country'),
      },
      placeholder: {
        addressInput: this.translate.instant('placeholder.addressInput')
      }
    };
  }

  private setModalSettings() {
    this.modalSettings = {
      title: '',
      confirm: {
        label: this.i18n.action.confirm,
        disabled: this.form.invalid,
        action: () => { this.submit(); }
      },
      cancel: {
        label: this.i18n.action.cancel,
        action: () => { this.modal.close(); }
      },
      size: 'md',
      clickOut: true
    };

    this.form.statusChanges.subscribe(() => {
      this.modalSettings.confirm.disabled = this.form.invalid;
    });
  }

  private changeModalToCreateMode() {
    this.modalSettings = {
      ...this.modalSettings,
      title: this.i18n.title.insert
    };
  }

  private changeModalToEditMode() {
    this.modalSettings = {
      ...this.modalSettings,
      title: this.i18n.title.edit
    };
  }

  private submit() {
    if (this.form.invalid) {
      return;
    }

    const formData = this.transformFormIntoData(this.form.value);
    this.submitted.emit(formData);
  }

  public closeModal() {
    this.modal.close();
  }

  public transformFormIntoData(formValue) {
    const locationCategory = {
      id: formValue.addressType,
      name: this.mappedAddressTypes[formValue.addressType],
    };

    return {
      id: formValue.id,
      additionalAddressDetails: formValue.complement,
      country: this.mappedCountries[formValue.country],
      countryCode: formValue.country,
      division: formValue.state,
      latitude: formValue.latitude,
      locationCategory,
      locationCategoryId: formValue.addressType,
      longitude: formValue.longitude,
      neighborhood: formValue.district,
      postalCode: formValue.postalCode,
      streetName: formValue.locality,
      streetNumber: formValue.number,
      subdivision: formValue.city,
    };
  }

  // TODO move this to Bifrost
  private optionsToMap(options) {
    return options.reduce((mapped, option) => {
      return {
        ...mapped,
        [option.value]: option.label
      };
    }, {});
  }
}
