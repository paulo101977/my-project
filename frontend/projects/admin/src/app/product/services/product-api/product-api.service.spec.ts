import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { ProductResourceService } from '../../resources/product-resource/product-resource.service';

import { ProductApiService } from './product-api.service';

describe('ProductApiService', () => {
  let service: ProductApiService;

  const productResourceServiceStub: Partial<ProductResourceService> = {
    listAll(): Observable<any> { return null; }
  };
  let productResourceService: ProductResourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProductApiService,
        {
          provide: ProductResourceService,
          useValue: productResourceServiceStub
        }
      ]
    });

    service = TestBed.get(ProductApiService);
    productResourceService = TestBed.get(ProductResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#listAll', () => {
    it('should call ProductResourceService#listAll', () => {
      spyOn(productResourceService, 'listAll').and.returnValue(of({items: []}));

      service.listAll();

      expect(productResourceService.listAll).toHaveBeenCalled();
    });

    it('should return the response items', function () {
      spyOn(productResourceService, 'listAll').and.returnValue(of({items: [1 , 2, 3]}));

      let items = [];
      service.listAll()
        .subscribe((responseItems) => {
          items = responseItems;
        });

      expect(items.length).toBe(3);
    });
  });

  describe('#listAllAsOptions', () => {
    it('should call ProductResourceService#listAll', () => {
      spyOn(productResourceService, 'listAll').and.returnValue(of({items: []}));

      service.listAllAsOptions();

      expect(productResourceService.listAll).toHaveBeenCalled();
    });

    it('should return the products as options', () => {
      const mock = [
        { id: 1, name: 'foo', subName: true },
        { id: 2, name: 'bar', subName: false },
        { id: 3, name: 'baz', subName: true }
      ];
      spyOn(productResourceService, 'listAll').and.returnValue(of({items: mock}));

      let options = [];
      service.listAllAsOptions()
        .subscribe((responseOptions) => {
          options = responseOptions;
        });

      expect(options.length).toBe(3);
      expect(options).toEqual([
        { label: 'foo', value: 1 },
        { label: 'bar', value: 2 },
        { label: 'baz', value: 3 }
      ]);
    });
  });
});
