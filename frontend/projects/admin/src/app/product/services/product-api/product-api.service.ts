import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ProductResourceService } from '../../resources/product-resource/product-resource.service';

@Injectable({
  providedIn: 'root'
})
export class ProductApiService {

  constructor(private resource: ProductResourceService) { }

  public listAll() {
    return this.resource
      .listAll()
      .pipe(
        this.getItems()
      );
  }

  public listAllAsOptions() {
    return this
      .listAll()
      .pipe(
        this.transformIntoOptions('name', 'id')
      );
  }

  // TODO move it to Bifrost
  private getItems() {
    return map(({items}) => items);
  }

  // TODO move it to Bifrost
  private transformIntoOptions(label: string, value: string) {
    return map((items: any) => {
      return items.map((item) => {
        return {
          label: item[label],
          value: item[value]
        };
      });
    });
  }
}
