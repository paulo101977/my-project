import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: 'root'
})
export class ProductResourceService {

  constructor(private api: AdminApiService) { }

  public listAll() {
    return this.api.get<any>('IdentityProduct');
  }
}
