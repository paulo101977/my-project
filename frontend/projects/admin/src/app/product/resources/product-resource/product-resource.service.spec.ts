import { TestBed } from '@angular/core/testing';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';

import { ProductResourceService } from './product-resource.service';

describe('ProductResourceService', () => {
  let service: ProductResourceService;

  const adminApiServiceStub: Partial<AdminApiService> = {
    get<T>(path: any, options?: any): Observable<T> { return null; }
  };

  let adminApiService: AdminApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProductResourceService,
        {
          provide: AdminApiService,
          useValue: adminApiServiceStub
        }
      ]
    });
    service = TestBed.get(ProductResourceService);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#listAll', () => {
    it('should call AdminApiService#get', () => {
      spyOn(adminApiService, 'get');
      service.listAll();
      expect(adminApiService.get).toHaveBeenCalledWith('IdentityProduct');
    });
  });
});
