import { TestBed } from '@angular/core/testing';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';

import { ChainResource } from './chain-resource';
import { of } from 'rxjs';

describe('ChainResource', () => {
  let service: ChainResource;
  // @ts-ignore
  const adminApiServiceStub: Partial<AdminApiService> = {
    get: () => of( null ),
    post: () => of( null ),
    put: () => of( null )
  };
  let adminApiService: AdminApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ChainResource,
        {
          provide: AdminApiService,
          useValue: adminApiServiceStub
        }
      ]
    });

    service = TestBed.get(ChainResource);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call AdminApiService#get', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(adminApiService, 'get');

      service.list(mock);
      expect(adminApiService.get).toHaveBeenCalledWith('chain', { params: mock });
    });
  });

  describe('#listWithoutAssociation', () => {
    it('should call AdminApiService#get', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(adminApiService, 'get');

      service.listWithoutAssociation(mock);
      expect(adminApiService.get).toHaveBeenCalledWith('chain/withoutassociation', { params: mock });
    });
  });

  describe('#getById', () => {
    it('should call AdminApiService#get', () => {
      const id = 1;
      spyOn(adminApiService, 'get');

      service.getById(id);
      expect(adminApiService.get).toHaveBeenCalledWith(`chain/${id}`);
    });
  });

  describe('#createChain', () => {
    it('should call AdminApiService#post', () => {
      const chainName = 'name';
      spyOn(adminApiService, 'post');

      service.createChain(chainName);
      expect(adminApiService.post).toHaveBeenCalledWith('chain', {Name: chainName});
    });
  });

  describe('#updateChain', () => {
    it('should call AdminApiService#put', () => {
      const mock = { Id: 1, Name: 'name' };
      spyOn(adminApiService, 'put');

      service.updateChain(mock.Id, mock.Name);
      expect(adminApiService.put).toHaveBeenCalledWith(`chain/${mock.Id}`, mock);
    });
  });
});
