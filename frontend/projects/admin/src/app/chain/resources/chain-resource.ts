import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { Observable } from 'rxjs';
import { Chain } from '../models/chain';

@Injectable({
  providedIn: 'root'
})
export class ChainResource {

  constructor(private adminApi: AdminApiService) {
  }

  public list(params: ListQueryParams) {
    return this.adminApi.get<any>('chain', {params});
  }

  public listWithoutAssociation(params) {
    return this.adminApi.get('chain/withoutassociation', {params});
  }

  public getById(id): Observable<Chain> {
    return this.adminApi.get(`chain/${id}`);
  }

  public createChain(name: string) {
    const params = {
      Name: name
    };
    return this.adminApi.post('chain', params);
  }

  public updateChain(id: number, name: string) {
    const params = {
      Id: id,
      Name: name
    };
    return this.adminApi.put(`chain/${id}`, params);
  }
}
