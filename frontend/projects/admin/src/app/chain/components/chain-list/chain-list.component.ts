import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';
import { TableSettings } from '../../../shared/models/table-settings';
import { TranslateService } from '@ngx-translate/core';
import { debounceTime, map, tap } from 'rxjs/operators';
import { ThfTableColumn } from '@totvs/thf-ui';
import { ChainResource } from '../../resources/chain-resource';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'admin-chain-list',
  templateUrl: './chain-list.component.html',
  styleUrls: ['./chain-list.component.css']
})
export class ChainListComponent implements OnInit, OnChanges {

  @Input() search: string;
  @Output() editItem = new EventEmitter();

  private searchChange = new Subject();
  private searchChanged$ = this.searchChange.asObservable();
  private pageSize = 5;

  public currentPage = 1;
  public tableSettings: TableSettings;

  constructor(
    private translate: TranslateService,
    private router: Router,
    public route: ActivatedRoute,
    private chainResource: ChainResource
  ) {
    this.setTableSettings();
  }

  ngOnInit(): void {
    this.getTableItems();
    this.listenToSearchChanges();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.search) {
      this.searchChange.next(this.search);
    }
  }

  public filterItems(input) {
    this.listChains(input, 1)
      .subscribe((items) => {
        this.tableSettings.items = items;
      });
  }

  public showMore() {
    if (!this.tableSettings.hasMoreToShow) {
      return;
    }

    const nextPage = this.currentPage + 1;
    this.listChains(this.search, nextPage)
      .subscribe((items) => {
        this.tableSettings.items.push(...items);
      });
  }

  private listenToSearchChanges() {
    this.searchChanged$
      .pipe(
        debounceTime(500)
      )
      .subscribe((search) => {
        this.filterItems(search);
      });
  }

  private listChains(search: string, page: number) {
    const params = {
      SearchData: search,
      Page: page,
      PageSize: this.pageSize
    };

    this.tableSettings.loading = true;

    return this.chainResource.list(params)
      .pipe(
        tap(() => { this.currentPage = page; }),
        tap((response) => { this.updateTableSettings(response); }),
        map(({items}) => items.map(item => {
          item.actions = ['edit'];
          return item;
        }))
      );
  }

  public getTableItems(): void {
    this.listChains('', 1)
      .subscribe((items) => {
        this.tableSettings.items = items;
      });
  }

  private getTableColumns(): ThfTableColumn[] {
    return [
      { property: 'name', label: this.translate.instant('label.chain'), type: 'string' },
      { property: 'tenantName', label: this.translate.instant('label.tenant'), type: 'string' },
      {
        property: 'actions',
        label: this.translate.instant('label.actions'),
        type: 'icon',
        width: '10%',
        icons: [
          { value: 'edit', icon: 'thf-icon-edit', action: (item) => this.goToEdit(item) }
        ]
      }
    ];
  }

  private goToEdit(item) {
    this.router.navigate([`edit/${item.id}`], {relativeTo: this.route});
  }

  private setTableSettings() {
    const rowHeight = 66;
    this.tableSettings = {
      columns: this.getTableColumns(),
      items: [],
      sort: true,
      striped: true,
      loading: false,
      showMore: () => { this.showMore(); },
      hasMoreToShow: false,
      height: rowHeight * this.pageSize,
      literals: {
        loadMoreData: this.translate.instant('variable.showMoreItems', {current: 0, of: 10})
      }
    };
  }

  private updateTableSettings({hasNext, items, totalItems}) {
    const literalShowMore = this.getShowMoreLiteral(this.currentPage, this.pageSize, items.length, totalItems);

    this.tableSettings = {
      ...this.tableSettings,
      loading: false,
      hasMoreToShow: hasNext,
      literals: {
        ...this.tableSettings.literals,
        loadMoreData: literalShowMore
      }
    };
  }

  private getShowMoreLiteral(page: number, pageSize: number, currentItems: number, totalItems: number) {
    return this.translate.instant('variable.showMoreItems', {
      current: (page - 1) * pageSize + currentItems,
      of: totalItems
    });
  }
}
