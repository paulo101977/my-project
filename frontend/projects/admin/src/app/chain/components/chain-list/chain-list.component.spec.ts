import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChainListComponent } from './chain-list.component';
import { TranslateService } from '@ngx-translate/core';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { ChainResource } from '../../resources/chain-resource';
import { RouterTestingModule } from '@angular/router/testing';

describe('ChainListComponent', () => {
  let component: ChainListComponent;
  let fixture: ComponentFixture<ChainListComponent>;

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };
  let translateService: TranslateService;

  // @ts-ignore
  const chainApiStubService: Partial<ChainResource> = {
    list: () => of( null )
  };
  let chainResource: ChainResource;
  let chainListSpy: any;

  @Component({selector: 'thf-table', template: ''}) // tslint:disable-line
  class TableStubComponent {
  }

  const setChainListResponse = (items = [], totalItems = 0, hasNext = false) => {
    const response = {items, totalItems, hasNext};
    chainListSpy.and.returnValue(of(response));
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ChainListComponent, TableStubComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: ChainResource,
          useValue: chainApiStubService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();

    translateService = TestBed.get(TranslateService);
    chainResource = TestBed.get(ChainResource);

    chainListSpy = spyOn(chainResource, 'list');
  }));

  beforeEach(() => {
    setChainListResponse();
    fixture = TestBed.createComponent(ChainListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#filterItems', () => {
    it('should replace items with the new filtered items', () => {
      const originalItems = [[1], [2], [3]];
      const newItems = [{actions: ['edit']}];
      component.tableSettings.items = originalItems;
      component.tableSettings.hasMoreToShow = true;
      setChainListResponse(newItems);

      expect(component.tableSettings.items).toEqual(originalItems);
      component.filterItems('');
      expect(component.tableSettings.items).toEqual(newItems);
    });
  });

  describe('#showMore', () => {
    it('should add items to the items list', () => {
      component.tableSettings.items = [[1], [2], [3]];
      component.tableSettings.hasMoreToShow = true;
      setChainListResponse([[4], [5], [6]]);

      expect(component.tableSettings.items.length).toBe(3);
      component.showMore();
      expect(component.tableSettings.items.length).toBe(6);
    });

    it('shouldn\'t run when there is no more items to look after', () => {
      component.tableSettings.items = [[1], [2], [3]];
      component.tableSettings.hasMoreToShow = false;
      setChainListResponse([[4], [5], [6]]);

      expect(component.tableSettings.items.length).toBe(3);
      component.showMore();
      expect(component.tableSettings.items.length).toBe(3);
    });
  });
});
