import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Chain } from '../../models/chain';

import { UpdateComponent } from './update.component';
import { of } from 'rxjs';
import { ChainResource } from '../../resources/chain-resource';
import { ThfDialogService, ThfNotificationService } from '@totvs/thf-ui';
import { chainMock } from '../../mock/chain-mock';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';

describe('UpdateComponent', () => {
  let component: UpdateComponent;
  let fixture: ComponentFixture<UpdateComponent>;
  let chainResource: ChainResource;
  let thfNotificationService: ThfNotificationService;
  let thfDialogService: ThfDialogService;

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };

  // @ts-ignore
  const chainResourceStub: Partial<ChainResource> = {
    getById: (id) => of( null ),
    updateChain: (id, name) => of(null)
  };

  // @ts-ignore
  const thfNotificationServiceStub: Partial<ThfNotificationService> = {
    success: () => {
    }
  };

  // @ts-ignore
  const thfDialogServiceStub: Partial<ThfDialogService> = {
    confirm: () => {
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [UpdateComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: ChainResource,
          useValue: chainResourceStub
        },
        {
          provide: ThfNotificationService,
          useValue: thfNotificationServiceStub
        },
        {
          provide: ThfDialogService,
          useValue: thfDialogServiceStub
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {id: 1}
            }
          }
        },
        FormBuilder
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();

    chainResource = TestBed.get(ChainResource);
    thfNotificationService = TestBed.get(ThfNotificationService);
    thfDialogService = TestBed.get(ThfDialogService);
    spyOn(chainResource, 'getById').and.returnValue(of(<Chain>{}));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setPageSettings', () => {
      component['setPageSettings']();
      expect(component.literals.cancel).toEqual(component.i18n.action.cancel);
      expect(component.literals.save).toEqual(component.i18n.action.confirm);
    });

    it('should setFormSettings', () => {
      component['setFormSettings']();

      expect(component['form'].contains('chainName')).toBeTruthy();
    });

    it('should setTableSettings', () => {
      component['setTableSettings']();

      expect(component.tableBrandSettings.columns)
        .toEqual([{property: 'name', label: component.i18n.label.name, type: 'string'}]);
      expect(component.tableBrandSettings.items).toEqual([]);
      expect(component.tablePropertySettings.columns)
        .toEqual([{property: 'name', label: component.i18n.label.name, type: 'string'}]);
      expect(component.tablePropertySettings.items).toEqual([]);
    });
  });

  describe('load', () => {
    it('should loadToForm', () => {
      component['loadToForm'](chainMock);

      expect(component['form'].value.chainName).toEqual('name chain mock');
    });

    it('should loadToTableBrand', () => {
      component['loadToTableBrand'](chainMock);

      expect(component.tableBrandSettings.items.length).toEqual(1);
      expect(component.tableBrandSettings.items[0]).toEqual(chainMock.brandDtoList[0]);
    });

    it('should loadToTableProperty', () => {
      component['loadToTableProperty'](chainMock);

      expect(component.tablePropertySettings.items.length).toEqual(1);
      expect(component.tablePropertySettings.items[0]).toEqual(chainMock.propertyDtoList[0]);
    });
  });

  describe('actions', () => {
    it('should cancel', () => {
      spyOn<any>(component, 'beforeRedirect');

      component.cancel();

      expect(component['beforeRedirect']).toHaveBeenCalled();
    });

    it('should beforeRedirect when data changed', () => {
      spyOn(thfDialogService, 'confirm');
      component['form'].markAsDirty();

      component['beforeRedirect']();

      expect(thfDialogService.confirm).toHaveBeenCalledWith({
        title: component.i18n.alert.title,
        message: component.i18n.alert.body,
        confirm: jasmine.any(Function)
      });
    });

    it('should beforeRedirect when data not changed', () => {
      spyOn<any>(component, 'redirectToList');

      component['beforeRedirect']();

      expect(component['redirectToList']).toHaveBeenCalled();
    });

    it('should redirectToList#edit', () => {
      spyOn(component['router'], 'navigate');

      component['redirectToList']();

      expect(component['router'].navigate)
        .toHaveBeenCalledWith(['../../'], {relativeTo: component['route']});
    });
  });

  describe('resources', () => {
    it('should getChain', fakeAsync(() => {
      spyOn<any>(component, 'loadToForm');
      spyOn<any>(component, 'loadToTableBrand');
      spyOn<any>(component, 'loadToTableProperty');

      component['getChain']();

      tick();

      expect(chainResource.getById)
        .toHaveBeenCalledWith(1);
      expect(component['loadToForm']).toHaveBeenCalledWith({});
      expect(component['loadToTableBrand']).toHaveBeenCalledWith({});
      expect(component['loadToTableProperty']).toHaveBeenCalledWith({});
    }));

    it('should saveChain#edit', fakeAsync(() => {
      component['form'].patchValue({
        id: 1,
        chainName: 'chain test new'
      });

      spyOn(chainResource, 'updateChain').and.returnValue(of({}));
      spyOn(thfNotificationService, 'success');
      spyOn(component['form'], 'reset');
      spyOn<any>(component, 'redirectToList');

      component['save']();

      tick();

      expect(chainResource.updateChain)
        .toHaveBeenCalledWith(1, 'chain test new');
      expect(thfNotificationService.success).toHaveBeenCalledWith(component.i18n.variable.editSuccess);
      expect(component['form'].reset).toHaveBeenCalled();
      expect(component['redirectToList']).toHaveBeenCalled();
    }));
  });
});
