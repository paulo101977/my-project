import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfDialogService, ThfNotificationService, ThfPageEditLiterals } from '@totvs/thf-ui';
import { ChainResource } from '../../resources/chain-resource';
import { Chain } from '../../models/chain';

@Component({
  selector: 'admin-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  private chainId: number;
  public form: FormGroup;
  public i18n: any;
  public literals: ThfPageEditLiterals;

  // Lists
  public tableBrandSettings;
  public tablePropertySettings;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private translate: TranslateService,
    private chainResource: ChainResource,
    private thfNotification: ThfNotificationService,
    private thfDialog: ThfDialogService
  ) {
  }

  ngOnInit() {
    this.chainId = this.route.snapshot.params.id;
    this.setI18n();
    this.setPageSettings();
    this.setFormSettings();
    this.setTableSettings();
    this.getChain();
  }

  private setI18n() {
    this.i18n = {
      title: this.translate.instant('action.lbEdit', {
        labelName: this.translate.instant('label.chain')
      }),
      group: {
        chain: this.translate.instant('label.chainData'),
        brand: this.translate.instant('label.brandsAssociate'),
        properties: this.translate.instant('label.propertiesAssociate')
      },
      label: {
        name: this.translate.instant('label.name'),
        chainName: this.translate.instant('variable.nameOf.F', {
          labelName: this.translate.instant('label.chain')
        })
      },
      action: {
        confirm: this.translate.instant('action.confirm'),
        cancel: this.translate.instant('action.cancel')
      },
      variable: {
        editSuccess: this.translate.instant('variable.lbEditSuccessF', {
          labelName: this.translate.instant('label.chain')
        })
      },
      alert: {
        title: this.translate.instant('alert.redirectConfirm'),
        body: this.translate.instant('alert.dataHasNotBeenSavedYet')
      }
    };
  }

  private setPageSettings() {
    this.literals = {
      cancel: this.i18n.action.cancel,
      save: this.i18n.action.confirm
    };
  }

  private setFormSettings() {
    this.form = this.fb.group({
      chainName: ['', Validators.required]
    });
  }

  private setTableSettings() {
    this.tableBrandSettings = {
      columns: [
        {property: 'name', label: this.i18n.label.name, type: 'string'},
      ],
      items: []
    };
    this.tablePropertySettings = {
      columns: [
        {property: 'name', label: this.i18n.label.name, type: 'string'},
      ],
      items: []
    };
  }

  private getChain() {
    this.chainResource.getById(this.chainId)
      .subscribe(chain => {
        this.loadToForm(chain);
        this.loadToTableBrand(chain);
        this.loadToTableProperty(chain);
      });
  }

  private loadToForm(chain: Chain) {
    this.form.patchValue({
      chainName: chain.name
    });
  }

  private loadToTableBrand(chain: Chain) {
    if (chain.brandDtoList) {
      this.tableBrandSettings.items = chain.brandDtoList;
    }
  }

  private loadToTableProperty(chain: Chain) {
    if (chain.propertyDtoList) {
      this.tablePropertySettings.items = chain.propertyDtoList;
    }
  }

  private beforeRedirect() {
    if (this.form.dirty) {
      this.thfDialog.confirm({
        title: this.i18n.alert.title,
        message: this.i18n.alert.body,
        confirm: () => this.redirectToList()
      });

    } else {
      this.redirectToList();
    }
  }

  public cancel() {
    this.beforeRedirect();
  }

  public save() {
    if (this.form.invalid) {
      return;
    }

    this.chainResource.updateChain(this.chainId, this.form.value.chainName)
      .subscribe(
        () => {
          this.thfNotification.success(this.i18n.variable.editSuccess);
          this.form.reset();
          this.redirectToList();
        },
        error => {
          if (error) {
            this.thfNotification
              .warning(error.error.details.map(detail => detail.message).join('\n'));
          }
        });
  }

  private redirectToList() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }
}
