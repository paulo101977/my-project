import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { PageSettings } from '../../../shared/models/page-settings';
import { TranslateService } from '@ngx-translate/core';
import { ThfModalComponent, ThfNotificationService } from '@totvs/thf-ui';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalSettings } from '../../../shared/models/modal-settings';
import { ChainResource } from '../../resources/chain-resource';
import { ChainListComponent } from '../chain-list/chain-list.component';

@Component({
  selector: 'admin-chain-list-page',
  templateUrl: './chain-list-page.component.html',
  styleUrls: ['./chain-list-page.component.css']
})
export class ChainListPageComponent implements OnInit {

  @Input() hasBackAction = false;
  @ViewChild('modal') thfModal: ThfModalComponent;
  @ViewChild('list') list: ChainListComponent;

  public searchValue = '';
  public pageSettings: PageSettings;
  public modalSettings: ModalSettings;
  public form: FormGroup;
  public i18n: any;

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private thfNotification: ThfNotificationService,
    private chainResource: ChainResource
  ) {
  }

  ngOnInit(): void {
    this.setI18n();
    this.setPageSettings();
    this.setModalSettings();
    this.setFormSettings();
  }

  private setI18n() {
    this.i18n = {
      title: {
        chain: this.translate.instant('title.chain')
      },
      placeholder: {
        search: this.translate.instant('placeholder.searchChainOrTenant')
      },
      label: {
        chain: this.translate.instant('label.chain'),
        chainName: this.translate.instant('variable.nameOf.F', {
          labelName: this.translate.instant('label.chain')
        })
      },
      action: {
        back: this.translate.instant('action.back'),
        confirm: this.translate.instant('action.confirm'),
        cancel: this.translate.instant('action.cancel'),
        insert: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.chain')
        })
      },
      variable: {
        insertSuccess: this.translate.instant('variable.lbSaveSuccessF', {
          labelName: this.translate.instant('label.chain')
        })
      }
    };
  }

  private getActions() {
    const backAction = {
      label: this.i18n.action.back,
      icon: 'thf-icon-arrow-left',
      action: () => history.back()
    };

    return [
      {
        label: this.i18n.action.insert,
        icon: 'thf-icon-plus',
        action: () => this.openModal()
      },
      ...(this.hasBackAction ? [backAction] : [])
    ];
  }

  private setPageSettings() {
    this.pageSettings = {
      title: this.i18n.title.chain,
      actions: this.getActions(),
      filterSettings: {
        placeholder: this.i18n.placeholder.search,
        ngModel: 'searchValue'
      }
    };
  }

  private setModalSettings() {
    this.modalSettings = {
      title: this.i18n.action.insert,
      secondaryAction: {
        action: () => this.closeModal(),
        label: this.i18n.action.cancel
      },
      primaryAction: {
        action: () => this.saveChain(),
        label: this.i18n.action.confirm
      }
    };
  }

  private setFormSettings() {
    this.form = this.fb.group({
      id: '',
      chainName: ['', Validators.required]
    });

    this.form.statusChanges.subscribe(() => {
      this.modalSettings.primaryAction.disabled = this.form.invalid;
    });
  }

  public closeModal() {
    this.form.reset();
    this.thfModal.close();
  }

  public openModal() {
    this.modalSettings.title = this.i18n.action.insert;
    this.form.reset();
    this.thfModal.open();
  }

  private saveChain() {
    if (this.form.invalid) {
      return;
    }

    this.modalSettings.primaryAction.loading = true;
    this.chainResource.createChain(this.form.value.chainName)
      .subscribe(
        () => {
          this.thfNotification.success(this.i18n.variable.insertSuccess);
          this.modalSettings.primaryAction.loading = false;
          this.closeModal();
          this.list.getTableItems();
        },
        error => {
          if (error) {
            this.thfNotification.warning(error.error.details.map(detail => detail.message).join('\n'));
            this.modalSettings.primaryAction.loading = false;
          }
        });
  }
}
