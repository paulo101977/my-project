import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { ChainListPageComponent } from './chain-list-page.component';
import { ChainResource } from '../../resources/chain-resource';
import { FormBuilder } from '@angular/forms';
import { ThfNotificationService } from '@totvs/thf-ui';
import { of } from 'rxjs';

describe('ChainListPageComponent', () => {
  let component: ChainListPageComponent;
  let fixture: ComponentFixture<ChainListPageComponent>;
  let translateService: TranslateService;
  let chainResource: ChainResource;
  let formBuilder: FormBuilder;
  let thfNotificationService: ThfNotificationService;

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };

  // @ts-ignore
  const chainResourceStub: Partial<ChainResource> = {
    list: () => of( null ),
    createChain: (name) => of( null )
  };

  // @ts-ignore
  const thfNotificationServiceStub: Partial<ThfNotificationService> = {
    success: () => {}
  };

  @Component({selector: 'admin-chain-list', template: ''})
  class ChainListStubComponent {
    public getTableItems() {}
  }

  // tslint:disable
  @Component({selector: 'thf-modal', template: ''})
  class ThfModalComponentMockComponent {
    open = jasmine.createSpy('open');
    close = jasmine.createSpy('close');
  }
  // tslint:enable

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ChainListPageComponent,
        ChainListStubComponent,
        ThfModalComponentMockComponent
      ],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: ChainResource,
          useValue: chainResourceStub
        },
        FormBuilder,
        {
          provide: ThfNotificationService,
          useValue: thfNotificationServiceStub
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();

    translateService = TestBed.get(TranslateService);
    chainResource = TestBed.get(ChainResource);
    formBuilder = TestBed.get(FormBuilder);
    thfNotificationService = TestBed.get(ThfNotificationService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChainListPageComponent);
    component = fixture.componentInstance;
    component.hasBackAction = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should getActions', () => {
      expect(component['getActions']().length).toEqual(2);
      expect(component['getActions']()[0].label).toEqual(component.i18n.action.back);
      expect(component['getActions']()[0].icon).toEqual('thf-icon-plus');
      expect(component['getActions']()[1].label).toEqual(component.i18n.action.confirm);
      expect(component['getActions']()[1].icon).toEqual('thf-icon-arrow-left');
    });

    it('should setPageSettings', () => {
      component['setPageSettings']();
      expect(component['pageSettings'].title).toEqual(component.i18n.title.chain);
      expect(component['pageSettings'].filterSettings.ngModel).toEqual('searchValue');
    });

    it('should setModalSettings', () => {
      component['setModalSettings']();
      expect(component['modalSettings'].title).toEqual(component.i18n.action.insert);
      expect(component['modalSettings'].secondaryAction.label).toEqual(component.i18n.action.cancel);
      expect(component['modalSettings'].primaryAction.label).toEqual(component.i18n.action.confirm);
    });

    it('should setFormSettings', () => {
      component['setFormSettings']();
      expect(component['form'].contains('id')).toBeTruthy();
      expect(component['form'].contains('chainName')).toBeTruthy();
      expect(component['form'].contains('tenant')).toBeFalsy();

      component['form'].get('chainName').setValue('');
      expect(component['modalSettings'].primaryAction.disabled).toBeTruthy();
    });
  });

  describe('actions', () => {
    it('should closeModal', () => {
      spyOn(component['form'], 'reset');

      component.closeModal();

      expect(component['form'].reset).toHaveBeenCalled();
      expect(component.thfModal.close).toHaveBeenCalled();
    });

    it('should openModal', () => {
      spyOn(component['form'], 'reset');

      component.openModal();

      expect(component['modalSettings'].title).toEqual(component.i18n.action.insert);
      expect(component['form'].reset).toHaveBeenCalled();
      expect(component.thfModal.open).toHaveBeenCalled();
    });
  });

  describe('resources', () => {
    it('should saveChain#new', fakeAsync(() => {
      component['form'].patchValue({
        id: 1,
        chainName: 'chain test new'
      });

      spyOn(chainResource, 'createChain').and.returnValue(of({}));
      spyOn(component, 'closeModal');

      component['saveChain']();

      tick();

      expect(component['modalSettings'].primaryAction.loading).toBeFalsy();
      expect(chainResource.createChain)
        .toHaveBeenCalledWith('chain test new');
      expect(component.closeModal).toHaveBeenCalled();
    }));
  });
});
