import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateComponent } from './components/update/update.component';
import { ChainListComponent } from './components/chain-list/chain-list.component';
import { ChainListPageComponent } from './components/chain-list-page/chain-list-page.component';
import { ThfButtonModule, ThfFieldModule, ThfModalModule, ThfPageModule, ThfTableModule } from '@totvs/thf-ui';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ThfPageModule,
    ThfTableModule,
    ThfModalModule,
    ThfFieldModule,
    ThfButtonModule,
    SharedModule
  ],
  declarations: [UpdateComponent, ChainListComponent, ChainListPageComponent],
  exports: [UpdateComponent, ChainListComponent, ChainListPageComponent],
})
export class ChainModule {
}
