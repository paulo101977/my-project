import { Chain } from '../models/chain';
import { Brand } from '../../brand/models/brand';
import { Property } from '../../hotel/models/property';

export const chainMock = new Chain();
chainMock.name = 'name chain mock';
const brand = new Brand();
chainMock.brandDtoList = [brand];
const property = new Property();
chainMock.propertyDtoList = [property];
