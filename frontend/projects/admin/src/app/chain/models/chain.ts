import { Brand } from '../../brand/models/brand';
import { Property } from '../../hotel/models/property';

export class Chain {
  id: number;
  name: string;
  brandDtoList: Brand[];
  brandId: number;
  companyId: number;
  isTemporary: boolean;
  propertyDtoList: Property[];
  propertyId: number;
  tenantId: string;
  tenantName: string;
}
