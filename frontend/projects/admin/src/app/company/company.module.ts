import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressModule } from '../address/address.module';
import { PageListWrapperModule } from '../widgets/page-list-wrapper/page-list-wrapper.module';
import { CompanyListPageComponent } from './components/company-list-page/company-list-page.component';
import { CompanyCreateUpdateComponent } from './components/company-create-update/company-create-update.component';
import { ThfButtonModule, ThfFieldModule, ThfModalModule, ThfPageModule, ThfTableModule } from '@totvs/thf-ui';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DocumentCreateEditComponent } from './components/document-create-edit/document-create-edit.component';
import { CompanyDocumentComponent } from './components/company-document/company-document.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PageListWrapperModule,
    ThfPageModule,
    ThfFieldModule,
    ThfButtonModule,
    SharedModule,
    ThfTableModule,
    ThfModalModule,
    AddressModule
  ],
  declarations: [CompanyListPageComponent, CompanyCreateUpdateComponent, DocumentCreateEditComponent, CompanyDocumentComponent],
  exports: [CompanyListPageComponent, CompanyCreateUpdateComponent],
})
export class CompanyModule {
}
