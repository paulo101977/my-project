import { TestBed } from '@angular/core/testing';
import { CompanyResourceService } from '../resources/company-resource.service';

import { CompanyApiService } from './company-api.service';
import { of } from 'rxjs';

describe('CompanyApiService', () => {
  let service: CompanyApiService;
  // @ts-ignore
  const companyResourceStubService: Partial<CompanyResourceService> = {
    list: () => of( null ),
  };
  let companyResourceService: CompanyResourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CompanyApiService,
        {
          provide: CompanyResourceService,
          useValue: companyResourceStubService
        }
      ]
    });

    service = TestBed.get(CompanyApiService);
    companyResourceService = TestBed.get(CompanyResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call BrandResourceService#list', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(companyResourceService, 'list');

      service.list(mock);
      expect(companyResourceService.list).toHaveBeenCalledWith(mock);
    });
  });

  describe('#listAll', () => {
    it('should call BrandResourceService#listAll', () => {
      const mock = {Page: 1, PageSize: 999, SearchData: ''};
      spyOn(companyResourceService, 'list');

      service.listAll();
      expect(companyResourceService.list).toHaveBeenCalledWith(mock);
    });
  });
});
