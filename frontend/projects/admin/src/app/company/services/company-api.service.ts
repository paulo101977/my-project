import { Injectable } from '@angular/core';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { CompanyResourceService } from '../resources/company-resource.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyApiService {

  constructor(private resource: CompanyResourceService) { }

  public list(params: ListQueryParams) {
    return this.resource.list(params);
  }

  public listAll() {
    const params: ListQueryParams = {
      PageSize: 999,
      Page: 1,
      SearchData: ''
    };

    return this.list(params);
  }

  public listCompanyLocations(id: number) {
    return this.resource.listCompanyLocations(id);
  }
}
