import { Property } from '../../hotel/models/property';
import { DocumentForLegalAndNaturalPerson } from 'app/shared/models/document';

export class Company {
  id: number;
  shortName: string;
  tradeName: string;
  personId: string;
  personType: string;
  email: string;
  homePage: string;
  documentList: DocumentForLegalAndNaturalPerson[];
  locationList: any[];
  propertyList: Property[];

  constructor() {
    this.documentList = [];
    this.propertyList = [];
  }
}
