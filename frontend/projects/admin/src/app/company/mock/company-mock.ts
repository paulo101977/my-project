import { Company } from '../models/company';
import { Property } from '../../hotel/models/property';
import { Brand } from '../../brand/models/brand';

export const companyMock = new Company();
companyMock.id = 1;
companyMock.personType = 'L';
companyMock.shortName = 'BBTOUR Viagens/Cristalia';
companyMock.tradeName = 'BBTOUR viagens e turismo LTDA.';
companyMock.email = 'entradanfe@bbtur.com.br';
companyMock.homePage = 'www.bbtour.com.br';
companyMock.locationList = [
  {
    locationCategory: {
      id: 1,
      name: 'Comercial'
    }
  }
];
const property = new Property();
const brand = new Brand();
brand.name = 'Caio Hotel';
property.brand = brand;
companyMock.propertyList = [property];
companyMock.documentList = [
  {
    documentInformation: '20.525.364/0001-00',
    documentType: {
      id: 2,
      name: 'CNPJ',
      countryCode: '',
      isMandatory: true,
      personType: '',
      regexValidationExpression: '',
      stringFormatMask: '',
      countrySubdivisionId: 1
    },
    documentTypeId: 2,
    id: 0,
    ownerId: '91e432e8-733f-4a9e-bf97-f1a05e1e5281'
  }
];
