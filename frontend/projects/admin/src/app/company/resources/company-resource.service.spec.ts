import { TestBed } from '@angular/core/testing';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { createServiceStub } from '../../../../../../testing/create-service-stub';
import { I18nService } from '@inovacaocmnet/thx-bifrost';

import { CompanyResourceService } from './company-resource.service';
import { companyMock } from '../mock/company-mock';
import { of } from 'rxjs';

describe('CompanyResourceService', () => {
  let service: CompanyResourceService;
  // @ts-ignore
  const adminApiServiceStub: Partial<AdminApiService> = {
    get: () => of( null ),
    post: () => of( null ),
    put: () => of( null )
  };
  let adminApiService: AdminApiService;

  const I18nServiceStub = createServiceStub<I18nService>(I18nService, {
    getLanguage(): string { return ''; }
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CompanyResourceService,
        {
          provide: AdminApiService,
          useValue: adminApiServiceStub
        },
        I18nServiceStub
      ]
    });

    service = TestBed.get(CompanyResourceService);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call AdminApiService#get', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(adminApiService, 'get');

      service.list(mock);
      expect(adminApiService.get).toHaveBeenCalledWith('company', { params: mock });
    });
  });

  describe('#listWithoutAssociation', () => {
    it('should call AdminApiService#get', () => {
      const id = 1;
      spyOn(adminApiService, 'get');

      service.listCompanyLocations(id);
      expect(adminApiService.get).toHaveBeenCalledWith(`company/${id}/locations`);
    });
  });

  describe('#getById', () => {
    it('should call AdminApiService#get', () => {
      const id = 1;
      spyOn(adminApiService, 'get');

      service.getById(id);
      expect(adminApiService.get).toHaveBeenCalledWith(`company/${id}`);
    });
  });

  describe('#createCompany', () => {
    it('should call AdminApiService#post', () => {
      spyOn(adminApiService, 'post');

      companyMock.locationList.forEach((location) => {
        location.browserLanguage = '';
      });

      service.createCompany(companyMock);
      expect(adminApiService.post).toHaveBeenCalledWith('company', jasmine.objectContaining(companyMock));
    });
  });

  describe('#updateCompany', () => {
    it('should call AdminApiService#put', () => {
      spyOn(adminApiService, 'put');

      companyMock.locationList.forEach((location) => {
        location.browserLanguage = '';
      });

      service.updateCompany(companyMock);
      expect(adminApiService.put).toHaveBeenCalledWith(`company/${companyMock.id}`, jasmine.objectContaining(companyMock));
    });
  });
});
