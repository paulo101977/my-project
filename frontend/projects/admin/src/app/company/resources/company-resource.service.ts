import { Injectable } from '@angular/core';
import { AdminApiService, I18nService } from '@inovacaocmnet/thx-bifrost';
import { PropertyRequest } from '../../hotel/models/property-request';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { Company } from '../models/company';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyResourceService {

  constructor(
    private adminApi: AdminApiService,
    private i18nService: I18nService
  ) { }

  public list(params: ListQueryParams) {
    return this.adminApi.get<any>('company', { params });
  }

  public getById(id): Observable<Company> {
    return this.adminApi.get(`company/${id}`);
  }

  public listCompanyLocations(id) {
    return this.adminApi.get(`company/${id}/locations`);
  }

  public createCompany(company: Company) {
    const params = this.addBrowserLanguage(company);
    return this.adminApi.post('company', params);
  }

  public updateCompany(company: Company) {
    const params = this.addBrowserLanguage(company);
    return this.adminApi.put(`company/${company.id}`, params);
  }

  private addBrowserLanguage(company: Company): Company {
    const locations = company.locationList.map((location) => {
      return {
        ...location,
        browserLanguage: this.i18nService.getLanguage()
      };
    });

    return {
      ...company,
      locationList: locations
    };
  }
}
