import { Location } from '@angular/common';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThfPageAction } from '@totvs/thf-ui';
import { PageSettings } from '../../../shared/models/page-settings';
import { PageTableSettings } from '../../../widgets/page-list-wrapper/models/page-table-settings';
import { CompanyApiService } from '../../services/company-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Company } from '../../models/company';

@Component({
  selector: 'admin-company-list-page',
  templateUrl: './company-list-page.component.html',
  styleUrls: ['./company-list-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CompanyListPageComponent implements OnInit {
  @Input() hasBackAction = false;
  public pageSettings: PageSettings;
  public pageTableSettings: PageTableSettings;

  constructor(
    private location: Location,
    private translate: TranslateService,
    private companyApi: CompanyApiService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.setPageSettings();
    this.setPageTableSettings();
  }

  private setPageSettings() {
    this.pageSettings = {
      title: this.translate.instant('title.company'),
      actions: this.getActions(),
      filterSettings: {
        placeholder: this.translate.instant('placeholder.searchCompanyOrChain')
      }
    };
  }

  private setPageTableSettings() {
    this.pageTableSettings = {
      columns: [
        {property: 'tradeName', width: '45%', label: this.translate.instant('label.company'), type: 'string'},
        {property: 'chainName', width: '45%', label: this.translate.instant('label.chain'), type: 'string'},
        {
          property: 'actions',
          label: this.translate.instant('label.actions'),
          type: 'icon',
          width: '10%',
          icons: [
            {value: 'edit', icon: 'thf-icon-edit', action: (item) => this.goToEdit(item)}
          ]
        }
      ],
      transformData: this.transformData,
      getList: this.getList.bind(this),
    };
  }

  public getList(params) {
    return this.companyApi.list(params);
  }

  public transformData(response) {
    return {
      ...response,
      items: response.items.map((item) => {
        item.actions = ['edit'];
        return item;
      })
    };
  }

  private getActions() {
    const backAction: ThfPageAction = {
      label: this.translate.instant('action.back'),
      icon: 'thf-icon-arrow-left',
      action: () => {
        this.location.back();
      }
    };
    return [
      {
        label: this.translate.instant('action.insertCompany'),
        icon: 'thf-icon-plus',
        action: () => {
          this.goToNew();
        }
      },
      ...(this.hasBackAction ? [backAction] : [])
    ];
  }

  private goToNew() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  private goToEdit(company: Company) {
    this.router.navigate([`edit/${company.id}`], {relativeTo: this.route});
  }
}
