import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { CompanyApiService } from '../../services/company-api.service';
import { CompanyListPageComponent } from './company-list-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

describe('CompanyListPageComponent', () => {
  let component: CompanyListPageComponent;
  let fixture: ComponentFixture<CompanyListPageComponent>;
  // @ts-ignore
  const locationStub: Partial<Location> = {
    back: () => {}
  };
  let location: Location;
  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {}
  };
  let translateService: TranslateService;
  // @ts-ignore
  const companyApiStubService: Partial<CompanyApiService> = {
    list: () => of(null)
  };
  let companyApiService: CompanyApiService;
  @Component({selector: 'admin-page-list-wrapper', template: ''})
  class PageListWrapperStubComponent {}
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ CompanyListPageComponent, PageListWrapperStubComponent ],
      providers: [
        {
          provide: Location,
          useValue: locationStub
        },
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: CompanyApiService,
          useValue: companyApiStubService
        }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
      .compileComponents();
    location = TestBed.get(Location);
    translateService = TestBed.get(TranslateService);
    companyApiService = TestBed.get(CompanyApiService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
