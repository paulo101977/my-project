import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyDocumentComponent } from './company-document.component';
import { companyMock } from '../../mock/company-mock';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

describe('CompanyDocumentComponent', () => {
  let component: CompanyDocumentComponent;
  let fixture: ComponentFixture<CompanyDocumentComponent>;

  // tslint:disable
  @Component({selector: 'thf-modal', template: ''})
  class ThfModalComponentMockComponent {
    open = jasmine.createSpy('open');
    close = jasmine.createSpy('close');
  }
  // tslint:enable

  // tslint:disable
  @Component({selector: 'admin-document-create-edit', template: ''})
  class DocumentCreateEditMockComponent {
    formDocument = {
      reset: jasmine.createSpy('reset'),
      valid: true,
      value: {
        documentType: null,
        documentInformation: '',
        documentTypeName: ''
      }
    }
  }
  // tslint:enable

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CompanyDocumentComponent,
        ThfModalComponentMockComponent,
        DocumentCreateEditMockComponent
      ],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setTableSettings', () => {
      component['setTableSettings']();

      expect(component.tableSettings.columns.length).toEqual(3);
      expect(component.tableSettings.columns).toEqual([
        {property: `documentTypeName`, label: component.i18n.label.documentType, type: 'string'},
        {property: 'uf', label: component.i18n.label.uf, type: 'string'},
        {property: 'documentInformation', label: component.i18n.label.document, type: 'string'}
      ]);
      expect(component.tableSettings.actions).toEqual([
        { label: component.i18n.action.edit, action: jasmine.any(Function) },
        { label: component.i18n.action.delete, action: jasmine.any(Function) }
      ]);
      expect(component.tableSettings.items).toEqual([]);
    });

    it('should setModalSettings', () => {
      component['setModalSettings']();

      expect(component.modalSettings).toEqual({
        title: component.i18n.title.new,
        secondaryAction: {
          action: jasmine.any(Function),
          label: component.i18n.action.cancel
        },
        primaryAction: {
          action: jasmine.any(Function),
          label: component.i18n.action.confirm
        }
      });

      expect(component.modalDeleteSettings).toEqual({
        title: component.i18n.title.delete,
        secondaryAction: {
          action: jasmine.any(Function),
          label: component.i18n.action.cancel
        },
        primaryAction: {
          action: jasmine.any(Function),
          label: component.i18n.action.confirm
        }
      });
    });
  });

  describe('load', () => {
    it('should loadToTable', () => {
      const list = component['loadToTable'](companyMock.documentList);

      expect(list.length).toEqual(1);
      expect(list[0]).toEqual(companyMock.documentList[0]);
      expect(list[0].documentTypeName).toEqual(companyMock.documentList[0].documentType.name);
    });
  });

  describe('actions', () => {
    it('should openModal#new', () => {
      component.openModal();

      expect(component.modalSettings.title).toEqual(component.i18n.title.new);
      expect(component.toEdit).toBeNull();
      expect(component.thfModal.open).toHaveBeenCalled();
    });

    it('should openModal#edit', () => {
      component.documents = companyMock.documentList;

      component.openModal(companyMock.documentList[0]);

      expect(component.modalSettings.title).toEqual(component.i18n.action.edit);
      expect(component.toEdit).toEqual(companyMock.documentList[0]);
      expect(component.editIndex).toEqual(0);
      expect(component.thfModal.open).toHaveBeenCalled();
    });

    it('should closeModal', () => {
      component.closeModal();

      expect(component.thfModal.close).toHaveBeenCalled();
      expect(component.form.formDocument.reset).toHaveBeenCalled();
      expect(component.toEdit).toBeNull();
    });

    it('should closeModalDelete', () => {
      component.closeModalDelete();

      expect(component.thfModalDelete.close).toHaveBeenCalled();
    });

    it('should observeForm', () => {
      component.observeForm({value: '', valid: true});

      expect(component.modalSettings.primaryAction.disabled).toBeFalsy();
    });

    it('should save#new', () => {
      spyOn(component, 'closeModal');
      spyOn<any>(component, 'parsingData').and.returnValue(companyMock.documentList[0]);

      component.save();

      expect(component.closeModal).toHaveBeenCalled();
      expect(component['parsingData']).toHaveBeenCalledWith(component.form.formDocument.value);
    });

    it('should save#edit', () => {
      spyOn(component, 'closeModal');
      spyOn<any>(component, 'parsingOldDataToNewValues').and.returnValue([companyMock.documentList[0]]);

      component.toEdit = companyMock.documentList[0];
      component.save();

      expect(component.closeModal).toHaveBeenCalled();
      expect(component['parsingOldDataToNewValues']).toHaveBeenCalledWith(component.form.formDocument.value);
    });

    it('should askDeleteModal', () => {
      component.askDeleteModal(companyMock.documentList[0]);

      expect(component.thfModalDelete.open).toHaveBeenCalled();
      expect(component.toDelete).toEqual(companyMock.documentList[0]);
    });

    it('should delete', () => {
      spyOn(component, 'closeModalDelete');

      component.documents = [companyMock.documentList[0]];
      component.toDelete = companyMock.documentList[0];
      component.delete();

      expect(component.documents.length).toEqual(0);
      expect(component.closeModalDelete).toHaveBeenCalled();
    });
  });

});
