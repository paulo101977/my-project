import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ClientDocumentDto } from 'app/shared/models/dto/client/client-document-dto';
import { DocumentForLegalAndNaturalPerson } from 'app/shared/models/document';
import { ModalSettings } from '../../../shared/models/modal-settings';
import { ThfModalComponent } from '@totvs/thf-ui';
import { DocumentCreateEditComponent } from '../document-create-edit/document-create-edit.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'admin-company-document',
  templateUrl: './company-document.component.html',
  styleUrls: ['./company-document.component.css']
})
export class CompanyDocumentComponent implements OnInit {

  // documents dependencies
  public tableSettings;
  public modalSettings: ModalSettings;
  public modalDeleteSettings: ModalSettings;
  public toEdit: DocumentForLegalAndNaturalPerson;
  public toDelete: DocumentForLegalAndNaturalPerson;
  public editIndex: number;
  public i18n: any;
  @ViewChild('modal') thfModal: ThfModalComponent;
  @ViewChild('modalDelete') thfModalDelete: ThfModalComponent;
  @ViewChild('form') form: DocumentCreateEditComponent;

  documentsList = [];

  @Input()
  get documents() {
    if (this.tableSettings && this.tableSettings.items) {
      return this.tableSettings.items;
    }
  }

  @Output() documentsChange = new EventEmitter();

  set documents(val) {
    if (val && this.tableSettings) {
      this.tableSettings.items = this.documentsList = this.loadToTable(val);
      this.documentsChange.emit(this.documentsList);
    }
  }

  constructor(
    private translate: TranslateService
  ) {
  }

  ngOnInit() {
    this.setI18n();
    this.setTableSettings();
    this.setModalSettings();
  }

  private setI18n() {
    this.i18n = {
      title: {
        new: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.document')
        }),
        edit: this.translate.instant('action.lbEdit', {
          labelName: this.translate.instant('label.document')
        }),
        delete: this.translate.instant('action.lbDelete', {
          labelName: this.translate.instant('label.document')
        })
      },
      label: {
        uf: this.translate.instant('label.uf'),
        documentType: this.translate.instant('label.documentType'),
        document: this.translate.instant('label.document'),
      },
      group: {
        documents: this.translate.instant('label.documents'),
      },
      variable: {
        confirmDelete: this.translate.instant('variable.lbConfirmDeleteM', {
          labelName: this.translate.instant('label.document')
        })
      },
      action: {
        confirm: this.translate.instant('action.confirm'),
        cancel: this.translate.instant('action.cancel'),
        edit: this.translate.instant('action.edit'),
        delete: this.translate.instant('action.delete')
      }
    };
  }

  private setTableSettings() {
    this.tableSettings = {
      columns: [
        {property: `documentTypeName`, label: this.i18n.label.documentType, type: 'string'},
        {property: 'uf', label: this.i18n.label.uf, type: 'string'},
        {property: 'documentInformation', label: this.i18n.label.document, type: 'string'}
      ],
      items: [],
      actions: [
        {
          label: this.i18n.action.edit,
          action: (address) => this.openModal(address)
        },
        {
          label: this.i18n.action.delete,
          action: (address) => this.askDeleteModal(address)
        }
      ]
    };
  }

  private setModalSettings() {
    this.modalSettings = {
      title: this.i18n.title.new,
      secondaryAction: {
        action: () => this.closeModal(),
        label: this.i18n.action.cancel
      },
      primaryAction: {
        action: this.save,
        label: this.i18n.action.confirm
      }
    };
    this.modalDeleteSettings = {
      title: this.i18n.title.delete,
      secondaryAction: {
        action: () => this.closeModalDelete(),
        label: this.i18n.action.cancel
      },
      primaryAction: {
        action: this.delete,
        label: this.i18n.action.confirm
      }
    };
  }

  private loadToTable(documents) {
    return documents
      .map(document => {
        if (document.documentType && document.documentType.name) {
          document['documentTypeName'] = document.documentType.name;
        }
        return document;
      });
  }

  public openModal(item?: DocumentForLegalAndNaturalPerson) {
    if (item) {
      this.modalSettings.title = this.i18n.title.edit;
      this.toEdit = item;
      this.editIndex = this.documents
        .findIndex(doc => doc.documentInformation == item.documentInformation);
    } else {
      this.modalSettings.title = this.i18n.title.new;
      this.toEdit = null;
    }
    this.thfModal.open();
  }

  public closeModal() {
    this.thfModal.close();
    this.form.formDocument.reset();
    this.toEdit = null;
  }

  public closeModalDelete() {
    this.thfModalDelete.close();
  }

  public save = () => {
    if (this.form.formDocument.valid) {
      this.documents = this.toEdit
        ? this.parsingOldDataToNewValues(this.form.formDocument.value)
        : [
          this.parsingData(this.form.formDocument.value),
          ...this.tableSettings.items
        ];

      this.closeModal();
    }
  }

  private parsingData(dataDocument): ClientDocumentDto {
    return <ClientDocumentDto>{
      documentTypeId: dataDocument.documentType,
      documentType: {
        id: dataDocument.documentType,
        name: dataDocument.documentTypeName
      },
      documentTypeName: dataDocument.documentTypeName,
      documentInformation: dataDocument.documentInformation,
    };
  }

  private parsingOldDataToNewValues(document: ClientDocumentDto) {
    const newDocument = this.parsingData(document);
    this.documents.splice(this.editIndex, 1, newDocument);
    return [...this.documents];
  }

  public observeForm({value, valid}) {
    this.modalSettings.primaryAction.disabled = !valid;
  }

  public askDeleteModal = item => {
    this.thfModalDelete.open();
    this.toDelete = item;
  }

  public delete = () => {
    const removeIndex = this.documents.indexOf(this.toDelete);
    this.documents.splice(removeIndex, 1);
    this.documents = [...this.documents];
    this.closeModalDelete();
  }

}
