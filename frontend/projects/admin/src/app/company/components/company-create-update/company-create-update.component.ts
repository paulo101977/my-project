import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ThfDialogService, ThfNotificationService, ThfPageEditLiterals, ThfSelectOption } from '@totvs/thf-ui';
import { ClientDocumentTypeEnum } from 'app/shared/models/dto/client/client-document-types';
import { from } from 'rxjs';
import { filter, toArray } from 'rxjs/operators';
import { Company } from '../../models/company';
import { CompanyResourceService } from '../../resources/company-resource.service';

@Component({
  selector: 'admin-company-create-update',
  templateUrl: './company-create-update.component.html',
  styleUrls: ['./company-create-update.component.css']
})
export class CompanyCreateUpdateComponent implements OnInit {

  public company: Company;
  private companyId: number;
  public form: FormGroup;
  public i18n: any;
  public literals: ThfPageEditLiterals;
  public isEdit: boolean;

  // select dependencies
  public personTypeOptionList: ThfSelectOption[];

  // address dependencies
  public tableAddressSettings;

  // property dependencies
  public tablePropertySettings;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private translate: TranslateService,
    private companyResource: CompanyResourceService,
    private thfNotification: ThfNotificationService,
    private thfDialog: ThfDialogService,
  ) {
  }

  ngOnInit() {
    this.companyId = this.isEdit = this.route.snapshot.params.id;
    this.company = new Company();
    this.setI18n();
    this.setPageSettings();
    this.setFormSettings();
    this.setTableSettings();
    this.setPersonTypeOptionList();
    if (this.isEdit) {
      this.getCompany();
    }
  }

  private setI18n() {
    this.i18n = {
      title: this.translate.instant('action.lbEdit', {
        labelName: this.translate.instant('label.company')
      }),
      group: {
        company: this.translate.instant('label.companyData'),
        address: this.translate.instant('label.address'),
        documents: this.translate.instant('label.documents'),
        propertiesAssociate: this.translate.instant('label.propertiesAssociate')
      },
      label: {
        name: this.translate.instant('label.name'),
        personType: this.translate.instant('label.companyType'),
        shortName: this.translate.instant('label.fantasyName'),
        tradeName: this.translate.instant('label.tradeName'),
        email: this.translate.instant('label.email'),
        homePage: this.translate.instant('label.homePage'),
        personTypeLegal: this.translate.instant('label.personTypeLegal'),
        brand: this.translate.instant('label.brand'),
        documentType: this.translate.instant('label.documentType'),
        document: this.translate.instant('label.document'),
      },
      action: {
        confirm: this.translate.instant('action.confirm'),
        cancel: this.translate.instant('action.cancel'),
        newDocument: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.document')
        }),
        editDocument: this.translate.instant('action.lbEdit', {
          labelName: this.translate.instant('label.document')
        })
      },
      variable: {
        insertSuccess: this.translate.instant('variable.lbSaveSuccessF', {
          labelName: this.translate.instant('label.chain')
        }),
        editSuccess: this.translate.instant('variable.lbEditSuccessF', {
          labelName: this.translate.instant('label.company')
        })
      },
      alert: {
        title: this.translate.instant('alert.redirectConfirm'),
        body: this.translate.instant('alert.dataHasNotBeenSavedYet')
      }
    };
  }

  private setPageSettings() {
    this.literals = {
      cancel: this.i18n.action.cancel,
      save: this.i18n.action.confirm
    };
  }

  private setFormSettings() {
    this.form = this.fb.group({
      personType: [{value: ClientDocumentTypeEnum.Legal, disabled: true}, [Validators.required]],
      shortName: ['', Validators.required],
      tradeName: ['', Validators.required],
      email: ['', Validators.required],
      homePage: ['', Validators.required]
    });
  }

  private setPersonTypeOptionList() {
    this.personTypeOptionList = [
      {label: this.i18n.label.personTypeLegal, value: ClientDocumentTypeEnum.Legal}
    ];
  }

  private setTableSettings() {
    this.tableAddressSettings = {
      items: [],
      updateLocations: (locationList: any[]) => this.loadLocations(locationList)
    };
    this.tablePropertySettings = {
      columns: [
        {property: `name`, label: this.i18n.label.name, type: 'string'},
        {property: 'brandName', label: this.i18n.label.brand, type: 'string'}
      ],
      items: []
    };
  }

  private getCompany() {
    this.companyResource.getById(this.companyId)
      .subscribe(company => {
        this.company = company;
        this.loadToForm(company);
        this.loadToTableAddress(company);
        this.loadToTableProperty(company);
      });
  }

  private loadToForm(company: Company) {
    this.form.patchValue({
      shortName: company.shortName,
      tradeName: company.tradeName,
      personType: company.personType,
      email: company.email,
      homePage: company.homePage
    });
  }

  private loadToTableAddress(company: Company) {
    if (company.locationList) {
      this.loadLocations(company.locationList);
    }
  }

  private loadToTableProperty(company: Company) {
    if (company.propertyList) {
      this.tablePropertySettings.items = company.propertyList
        .map(property => {
          if (property.brand && property.brand.name) {
            property['brandName'] = property.brand.name;
          }
          property['actions'] = ['edit'];
          return property;
        });
    }
  }

  private loadLocations(locationList: any[]) {
    this.tableAddressSettings.items = locationList
      .map(location => {
        if (location.locationCategory && location.locationCategory.name) {
          location['addressType'] = location.locationCategory.name;
          location['postalCode'] = +`${location.postalCode}`.replace('-', '');
        }
        location.actions = ['edit'];
        return location;
      });
  }

  private beforeRedirect() {
    if (this.form.dirty) {
      this.thfDialog.confirm({
        title: this.i18n.alert.title,
        message: this.i18n.alert.body,
        confirm: () => this.redirectToList()
      });

    } else {
      this.redirectToList();
    }
  }

  public cancel() {
    this.beforeRedirect();
  }

  public save() {
    if (this.form.invalid) {
      return;
    }

    let resource;
    let message;
    this.mapperToSave();

    if (this.isEdit) {
      resource = this.companyResource.updateCompany(this.company);
      message = this.i18n.variable.editSuccess;
    } else {
      resource = this.companyResource.createCompany(this.company);
      message = this.i18n.variable.insertSuccess;
    }
    resource
      .subscribe(() => {
        this.thfNotification.success(message);
        this.form.reset();
        this.redirectToList();
      });
  }

  private mapperToSave() {
    const addOwnerId = <T>(list: T[]): T[] => {
      return list.map((item: T) => {
        item['ownerId'] = this.company.personId;
        return item;
      });
    };

    this.company.shortName = this.form.get('shortName').value;
    this.company.tradeName = this.form.get('tradeName').value;
    this.company.personType = this.form.get('personType').value;
    this.company.email = this.form.get('email').value;
    this.company.homePage = this.form.get('homePage').value;
    this.company.locationList = this.tableAddressSettings.items;

    this.company.locationList = addOwnerId(this.company.locationList);
    this.company.documentList = addOwnerId(this.company.documentList);

    this.company.locationList.map((location) => {
      delete location['id'];
      return location;
    });
  }

  private redirectToList() {
    const route = this.isEdit ? '../../' : '../';
    this.router.navigate([route], {relativeTo: this.route});
  }

}
