import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Company } from '../../models/company';

import { CompanyCreateUpdateComponent } from './company-create-update.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThfDialogService, ThfNotificationService } from '@totvs/thf-ui';
import { ActivatedRoute } from '@angular/router';
import { CompanyResourceService } from '../../resources/company-resource.service';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { companyMock } from '../../mock/company-mock';

describe('CompanyCreateUpdateComponent', () => {
  let component: CompanyCreateUpdateComponent;
  let fixture: ComponentFixture<CompanyCreateUpdateComponent>;
  let companyResource: CompanyResourceService;
  let thfDialogService: ThfDialogService;
  let thfNotificationService: ThfNotificationService;

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };

  // @ts-ignore
  const companyResourceStub: Partial<CompanyResourceService> = {
    getById: (id) =>  of( null ),
    createCompany: (company) => of( null ),
    updateCompany: (company) => of( null )
  };

  // @ts-ignore
  const thfNotificationServiceStub: Partial<ThfNotificationService> = {
    success: () => {
    }
  };

  // @ts-ignore
  const thfDialogServiceStub: Partial<ThfDialogService> = {
    confirm: () => {
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [CompanyCreateUpdateComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: CompanyResourceService,
          useValue: companyResourceStub
        },
        {
          provide: ThfNotificationService,
          useValue: thfNotificationServiceStub
        },
        {
          provide: ThfDialogService,
          useValue: thfDialogServiceStub
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {id: 1}
            }
          }
        },
        FormBuilder,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();

    thfDialogService = TestBed.get(ThfDialogService);
    companyResource = TestBed.get(CompanyResourceService);
    thfNotificationService = TestBed.get(ThfNotificationService);
    spyOn(companyResource, 'getById').and.returnValue(of(<Company>{}));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreateUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setPageSettings', () => {
      component['setPageSettings']();
      expect(component.literals.cancel).toEqual(component.i18n.action.cancel);
      expect(component.literals.save).toEqual(component.i18n.action.confirm);
    });

    it('should setFormSettings', () => {
      component['setFormSettings']();

      expect(component['form'].contains('personType')).toBeFalsy(); // disabled field
      expect(component['form'].contains('shortName')).toBeTruthy();
      expect(component['form'].contains('tradeName')).toBeTruthy();
      expect(component['form'].contains('email')).toBeTruthy();
      expect(component['form'].contains('homePage')).toBeTruthy();
    });

    it('should setPersonTypeOptionList', () => {
      component['setPersonTypeOptionList']();

      expect(component.personTypeOptionList.length).toEqual(1);
      expect(component.personTypeOptionList[0].label).toEqual(component.i18n.label.personTypeLegal);
      expect(component.personTypeOptionList[0].value).toEqual('L');
    });

    it('should setTableSettings', () => {
      component['setTableSettings']();

      expect(component.tableAddressSettings.items).toEqual([]);

      expect(component.tablePropertySettings.columns.length).toEqual(2);
      expect(component.tablePropertySettings.columns).toEqual([
        {property: `name`, label: component.i18n.label.name, type: 'string'},
        {property: 'brandName', label: component.i18n.label.brand, type: 'string'}
      ]);
      expect(component.tablePropertySettings.items).toEqual([]);
    });
  });

  describe('load', () => {
    it('should loadToForm', () => {
      component['loadToForm'](companyMock);

      expect(component['form'].value.shortName).toEqual('BBTOUR Viagens/Cristalia');
      expect(component['form'].value.tradeName).toEqual('BBTOUR viagens e turismo LTDA.');
      expect(component['form'].value.email).toEqual('entradanfe@bbtur.com.br');
      expect(component['form'].value.homePage).toEqual('www.bbtour.com.br');
    });

    it('should loadToTableAddress', () => {
      component['loadToTableAddress'](companyMock);

      expect(component.tableAddressSettings.items.length).toEqual(1);
      expect(component.tableAddressSettings.items[0]).toEqual(companyMock.locationList[0]);
      expect(component.tableAddressSettings.items[0].addressType).toEqual(companyMock.locationList[0].locationCategory.name);
      expect(component.tableAddressSettings.items[0].actions).toEqual(['edit']);
    });

    it('should loadToTableProperty', () => {
      component['loadToTableProperty'](companyMock);

      expect(component.tablePropertySettings.items.length).toEqual(1);
      expect(component.tablePropertySettings.items[0]).toEqual(companyMock.propertyList[0]);
      expect(component.tablePropertySettings.items[0].brandName).toEqual(companyMock.propertyList[0].brand.name);
      expect(component.tablePropertySettings.items[0].actions).toEqual(['edit']);
    });
  });

  describe('actions', () => {
    it('should cancel', () => {
      spyOn<any>(component, 'beforeRedirect');

      component.cancel();

      expect(component['beforeRedirect']).toHaveBeenCalled();
    });

    it('should beforeRedirect when data changed', () => {
      spyOn(thfDialogService, 'confirm');
      component['form'].markAsDirty();

      component['beforeRedirect']();

      expect(thfDialogService.confirm).toHaveBeenCalledWith({
        title: component.i18n.alert.title,
        message: component.i18n.alert.body,
        confirm: jasmine.any(Function)
      });
    });

    it('should beforeRedirect when data not changed', () => {
      spyOn<any>(component, 'redirectToList');

      component['beforeRedirect']();

      expect(component['redirectToList']).toHaveBeenCalled();
    });

    it('should redirectToList#new', () => {
      spyOn(component['router'], 'navigate');

      component.isEdit = false;
      component['redirectToList']();

      expect(component['router'].navigate)
        .toHaveBeenCalledWith(['../'], {relativeTo: component['route']});
    });

    it('should redirectToList#edit', () => {
      spyOn(component['router'], 'navigate');

      component.isEdit = true;
      component['redirectToList']();

      expect(component['router'].navigate)
        .toHaveBeenCalledWith(['../../'], {relativeTo: component['route']});
    });

    it('should mapperToSave', () => {
      component.form.patchValue({
        shortName: 'short name',
        tradeName: 'trade name',
        personType: 'person type',
        email: 'email',
        homePage: 'home page'
      });

      component.company = {
        ...component.company,
        locationList: [],
        documentList: []
      };

      component['mapperToSave']();

      expect(component.company.shortName).toEqual('short name');
      expect(component.company.tradeName).toEqual('trade name');
      expect(component.company.personType).toEqual('person type');
      expect(component.company.email).toEqual('email');
      expect(component.company.homePage).toEqual('home page');
    });
  });

  describe('resources', () => {
    it('should getCompany', fakeAsync(() => {
      spyOn<any>(component, 'loadToForm');
      spyOn<any>(component, 'loadToTableAddress');

      component['getCompany']();

      tick();

      expect(companyResource.getById)
        .toHaveBeenCalledWith(1);
      expect(component['loadToForm']).toHaveBeenCalledWith({});
      expect(component['loadToTableAddress']).toHaveBeenCalledWith({});
    }));

    it('should save#new', fakeAsync(() => {
      spyOn<any>(component, 'mapperToSave');
      spyOn(companyResource, 'createCompany').and.returnValue(of({}));
      spyOn(thfNotificationService, 'success');
      spyOn(component.form, 'reset');
      spyOn<any>(component, 'redirectToList');

      component.form.patchValue({
        shortName: companyMock.shortName,
        tradeName: companyMock.tradeName,
        personType: companyMock.personType,
        email: companyMock.email,
        homePage: companyMock.homePage
      });
      component.isEdit = false;
      component['save']();

      tick();

      expect(component['mapperToSave']).toHaveBeenCalled();
      expect(companyResource.createCompany)
        .toHaveBeenCalledWith({});
      expect(thfNotificationService.success).toHaveBeenCalledWith(component.i18n.variable.insertSuccess);
      expect(component.form.reset).toHaveBeenCalled();
      expect(component['redirectToList']).toHaveBeenCalled();
    }));

    it('should save#edit', fakeAsync(() => {
      spyOn<any>(component, 'mapperToSave');
      spyOn(companyResource, 'updateCompany').and.returnValue(of({}));
      spyOn(thfNotificationService, 'success');
      spyOn(component.form, 'reset');
      spyOn<any>(component, 'redirectToList');

      component.form.patchValue({
        shortName: companyMock.shortName,
        tradeName: companyMock.tradeName,
        personType: companyMock.personType,
        email: companyMock.email,
        homePage: companyMock.homePage
      });
      component.isEdit = true;
      component['save']();

      tick();

      expect(component['mapperToSave']).toHaveBeenCalled();
      expect(companyResource.updateCompany)
        .toHaveBeenCalledWith({});
      expect(thfNotificationService.success).toHaveBeenCalledWith(component.i18n.variable.editSuccess);
      expect(component.form.reset).toHaveBeenCalled();
      expect(component['redirectToList']).toHaveBeenCalled();
    }));
  });
});
