import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { DocumentCreateEditComponent } from './document-create-edit.component';
import { of } from 'rxjs';
import { DocumentTypeResource } from 'app/shared/resources/document-type/document-type.resource';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

// TODO testes quebrando por causa dos componentes THF
xdescribe('DocumentCreateEditComponent', () => {
  let component: DocumentCreateEditComponent;
  let fixture: ComponentFixture<DocumentCreateEditComponent>;
  let documentTypeResource: DocumentTypeResource;

  // @ts-ignore
  const documentTypeResourceStub: Partial<DocumentTypeResource> = {
    getAll: () => of( null )
  };

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [DocumentCreateEditComponent],
      providers: [
        {
          provide: DocumentTypeResource,
          useValue: documentTypeResourceStub
        },
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        FormBuilder
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();

    documentTypeResource = TestBed.get(DocumentTypeResource);
    spyOn(documentTypeResource, 'getAll').and.returnValue(of({}));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentCreateEditComponent);
    component = fixture.componentInstance;
    component.document = null;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('resources', () => {
    it('should getCompany', fakeAsync(() => {
      component['setDocumentTypeOptionList']();

      tick();

      expect(documentTypeResource.getAll).toHaveBeenCalled();
      expect(component['transformDocumentType']).toHaveBeenCalledWith({});
    }));
  });
});
