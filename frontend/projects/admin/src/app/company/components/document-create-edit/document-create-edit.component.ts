import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfSelectOption } from '@totvs/thf-ui';
import { ClientDocumentTypeEnum } from 'app/shared/models/dto/client/client-document-types';
import { getDocumentMask } from '../../../../../../bifrost/src/lib/helpers/document-mask';


import { DocumentTypeResourceService } from '../../../document/services/document-type-resource.service';

@Component({
  selector: 'admin-document-create-edit',
  templateUrl: './document-create-edit.component.html',
  styleUrls: ['./document-create-edit.component.css']
})
export class DocumentCreateEditComponent implements OnInit, OnChanges {

  public formDocument: FormGroup;
  public documentTypeList: ThfSelectOption[] = [];
  public i18n: any;
  public inputConfig: any;

  @Input() document;
  @Output() formChanged = new EventEmitter();

  constructor(
    private translate: TranslateService,
    private fb: FormBuilder,
    private documentTypeResource: DocumentTypeResourceService
  ) {
  }

  ngOnInit() {
    this.setI18n();
    this.setFormDocumentSettings();
    this.setDocumentTypeOptionList();

    this.inputConfig = {
      mask: '',
      hasMask: false,
      help: ''
    };
  }

  ngOnChanges() {
    if (this.document) {
      this.loadFormDocument();
    }
  }

  private setI18n() {
    this.i18n = {
      title: this.translate.instant('action.lbEdit', {
        labelName: this.translate.instant('label.company')
      }),
      label: {
        documentType: this.translate.instant('label.documentType'),
        document: this.translate.instant('label.document')
      },
      action: {
        newDocument: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.document')
        }),
        editDocument: this.translate.instant('action.lbEdit', {
          labelName: this.translate.instant('label.document')
        })
      }
    };
  }

  private setFormDocumentSettings() {
    this.formDocument = this.fb.group({
      documentType: [null, Validators.required],
      documentInformation: ['', Validators.required],
      documentTypeName: ''
    });

    this.formDocument.valueChanges.subscribe(values => {
      values.documentTypeName = this.loadDocumentTypeName(values.documentType);
    });

    this.formDocument.statusChanges.subscribe(() => {
      this.formChanged.emit({
        value: this.formDocument.value,
        valid: this.formDocument.valid
      });
    });
  }

  private setDocumentTypeOptionList() {
    this.documentTypeResource.getAll()
      .subscribe(({items}) => {
        this.documentTypeList = this.transformDocumentType(items);
      });
  }

  private transformDocumentType(items: any) {
    if (items) {
      return items
        .filter(documentType => documentType.personType == ClientDocumentTypeEnum.Legal)
        .map(documentType => {
          return {label: documentType.name, value: documentType.id};
        });
    }
    return [];
  }

  private loadFormDocument() {
    this.formDocument.patchValue({
      documentType: this.document.documentTypeId,
      documentInformation: this.document.documentInformation,
      documentTypeName: this.document.documentTypeName
    });
  }

  private loadDocumentTypeName(documentTypeId: number) {
    if (documentTypeId) {
      return this.documentTypeList.find(doc => doc.value == documentTypeId).label;
    }
  }

  public applyMask(text: string) {
    this.inputConfig.hasMask = false;
    this.inputConfig.help = '';
    this.formDocument.get('documentInformation').setValue('');
    this.formDocument.get('documentTypeName')
      .setValue(this.loadDocumentTypeName(this.formDocument.get('documentType').value));

    if (text) {
      this.inputConfig.mask = getDocumentMask(this.formDocument.get('documentType').value);
      this.inputConfig.hasMask = this.inputConfig.mask;
      this.inputConfig.help = this.inputConfig.mask;
    }
  }

}
