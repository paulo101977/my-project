import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ThfModalAction, ThfModalComponent } from '@totvs/thf-ui';
import { Observable } from 'rxjs';
import { debounceTime, map, startWith } from 'rxjs/operators';
import { TableSettings } from '../../../shared/models/table-settings';

@Component({
  selector: 'admin-brand-modal-form-list',
  templateUrl: './brand-modal-form-list.component.html',
  styleUrls: ['./brand-modal-form-list.component.css']
})
export class BrandModalFormListComponent implements OnInit, OnChanges {
  @ViewChild('modal') modal: ThfModalComponent;
  @Input() brands: any[];
  @Input() selectedBrands: any[];
  @Output() submitted = new EventEmitter<any[]>();

  public i18n: any;
  public search = new FormControl();
  public filteredBrands$: Observable<any[]>;
  public displayedColumns = ['name', 'chainName'];
  public tableSettings: Partial<TableSettings> = {
    checkbox: true
  };
  public primaryAction: ThfModalAction = {
    action: () => this.submitSelectedBrands(),
    label: this.translate.instant('action.confirm'),
  };
  public secondaryAction: ThfModalAction = {
    action: () => this.cancelSubmit(),
    label: this.translate.instant('action.cancel')
  };
  private modalBrands: any[];

  constructor(
    private translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.setI18n();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.onBrandsChange(changes);
  }

  public open() {
    this.modal.open();
  }

  public close() {
    this.modal.close();
  }

  public submitSelectedBrands() {
    const selectedBrands = this.modalBrands
      .filter((brand: any) => {
        return brand.$selected;
      })
      .map((brand) => ({
        ...brand,
        $selected: false
      }));

    this.submitted.emit(selectedBrands);
    this.modal.close();
  }

  public cancelSubmit() {
    this.modal.close();
  }

  private setI18n() {
    this.i18n = {
      modalTitle: this.translate.instant('action.lbAssociate', {
        labelName: this.translate.instant('label.hotel')
      }),
      placeholder: this.translate.instant('action.lbSearchFor', {
        text: this.translate.instant('label.hotel')
      })
    };
  }

  private onBrandsChange({brands}: SimpleChanges) {
    if (!brands || !brands.currentValue) {
      return;
    }

    this.modalBrands = JSON.parse(JSON.stringify(this.brands));
    this.filteredBrands$ = this.getFilteredBrands();
  }

  private getFilteredBrands() {
    return this.search
      .valueChanges
      .pipe(
        startWith(''),
        debounceTime(200),
        this.searchBrands(),
        this.markSelected(this.selectedBrands)
      );
  }

  private searchBrands() {
    return map((search: string) => {
      return this.modalBrands.filter((brand) => {
        const nothingToSearch = !search;
        const isSelected = brand.$selected;
        const searchExpression = new RegExp(search, 'i');
        const found = brand.name.search(searchExpression) > -1;

        return nothingToSearch || isSelected || found;
      });
    });
  }

  private markSelected(selectedBrands: any[] = []) {
    const brandIsSelected = (brand: any): boolean => {
      return selectedBrands.some(selectedBrand => selectedBrand.id == brand.id);
    };

    return map((brands: any) => brands.map(brand => {
      brand.$selected = brand.$selected || brandIsSelected(brand);
      return brand;
    }));
  }
}
