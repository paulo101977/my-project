import { Component, Input } from '@angular/core';
import { ThfTableColumnIcon } from '@totvs/thf-ui';
import { TableSettings } from '../../../shared/models/table-settings';

@Component({
  selector: 'admin-brand-list',
  templateUrl: './brand-list.component.html',
  styleUrls: ['./brand-list.component.css']
})
export class BrandListComponent {
  @Input() brands: any[];
  @Input() columns: string[] = [];
  @Input() actions: ThfTableColumnIcon[] = [];
  @Input() tableSettings: Partial<TableSettings>;

  public tableColumns = [
    { property: 'name', label: 'label.brand', type: 'string' },
    { property: 'tenantName', label: 'label.chain', type: 'string' },
    { property: 'chainName', label: 'label.tenant', type: 'string' },
  ];
}
