import { Location } from '@angular/common';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { ThfNotification, ThfNotificationService } from '@totvs/thf-ui';
import { Observable, of } from 'rxjs';
import { ChainResource } from '../../../chain/resources/chain-resource';
import { ListQueryParams } from '../../../shared/models/list-query-params';
import { BrandApiService } from '../../services/brand-api.service';

import { BrandListPageComponent } from './brand-list-page.component';

describe('BrandListPageComponent', () => {
  let component: BrandListPageComponent;
  let fixture: ComponentFixture<BrandListPageComponent>;

  // @ts-ignore
  const locationStub: Partial<Location> = {
    back: () => {}
  };
  let location: Location;

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {}
  };
  let translateService: TranslateService;

  // @ts-ignore
  const brandApiStubService: Partial<BrandApiService> = {
    list: () => of(null)
  };
  let brandApiService: BrandApiService;

  const chainResourceStub: Partial<ChainResource> = {
    list(params: ListQueryParams): Observable<any> { return of({items: [{}]}); }
  };
  let chainResource: ChainResource;

  const notificationStub: Partial<ThfNotificationService> = {
    success(notification: ThfNotification | string): void {}
  };
  let notificationService: ThfNotificationService;

  @Component({selector: 'admin-page-list-wrapper', template: ''})
  class PageListWrapperStubComponent {}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandListPageComponent, PageListWrapperStubComponent ],
      imports: [ RouterTestingModule ],
      providers: [
        {
          provide: Location,
          useValue: locationStub
        },
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: BrandApiService,
          useValue: brandApiStubService
        },
        {
          provide: ChainResource,
          useValue: chainResourceStub
        },
        {
          provide: ThfNotificationService,
          useValue: notificationStub
        },
        FormBuilder
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();

    location = TestBed.get(Location);
    translateService = TestBed.get(TranslateService);
    brandApiService = TestBed.get(BrandApiService);
    chainResource = TestBed.get(ChainResource);
    notificationService = TestBed.get(ThfNotificationService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
