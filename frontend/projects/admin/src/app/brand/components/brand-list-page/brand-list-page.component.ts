import { Location } from '@angular/common';
import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ThfModalComponent, ThfNotificationService, ThfPageAction } from '@totvs/thf-ui';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ChainResource } from '../../../chain/resources/chain-resource';
import { ListQueryParams } from '../../../shared/models/list-query-params';
import { PageSettings } from '../../../shared/models/page-settings';
import { PageTableSettings } from '../../../widgets/page-list-wrapper/models/page-table-settings';
import { PageListWrapperComponent } from '../../../widgets/page-list-wrapper/page-list-wrapper.component';
import { CreateBrandRequest } from '../../models/create-brand-request';
import { BrandApiService } from '../../services/brand-api.service';

@Component({
  selector: 'admin-brand-list-page',
  templateUrl: './brand-list-page.component.html',
  styleUrls: ['./brand-list-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BrandListPageComponent implements OnInit {
  @ViewChild('modal') modal: ThfModalComponent;
  @ViewChild('pageList') pageListWrapper: PageListWrapperComponent;
  @Input() hasBackAction = false;

  public pageSettings: PageSettings;
  public pageTableSettings: PageTableSettings;
  public modalSettings: any = {};
  public form: FormGroup;
  public i18n: any;
  public chainList: any = [];

  constructor(
    private location: Location,
    private translate: TranslateService,
    private brandApi: BrandApiService,
    private chainApi: ChainResource,
    private fb: FormBuilder,
    private notification: ThfNotificationService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.setChainList();
    this.setI18n();
    this.createForm();
    this.setPageSettings();
    this.setPageTableSettings();
    this.setModalSettings();
  }

  private setPageSettings() {
    this.pageSettings = {
      title: this.i18n.title.brand,
      actions: this.getActions(),
      filterSettings: {
        placeholder: this.i18n.placeholder.search
      }
    };
  }

  private setPageTableSettings() {
    this.pageTableSettings = {
      columns: [
        { property: 'name', width: '30%', label: this.i18n.label.brand, type: 'string' },
        { property: 'tenantName', width: '30%', label: this.i18n.label.tenant, type: 'string' },
        { property: 'chainName', width: '30%', label: this.i18n.label.chain, type: 'string' },
        {
          property: 'actions',
          label: this.i18n.label.actions,
          type: 'icon',
          width: '10%',
          icons: [
            { value: 'edit', icon: 'thf-icon-edit', action: (item) => this.clickOnEditItem(item) }
          ]
        }
      ],
      transformData: this.transformData,
      getList: this.getList.bind(this),
    };
  }

  public getList(params) {
    return this.brandApi.list(params);
  }

  public transformData(response) {
    return {
      ...response,
      items: response.items.map((item) => {
        item.actions = ['edit'];
        return item;
      })
    };
  }

  private getActions() {
    const backAction: ThfPageAction = {
      label: this.i18n.action.back,
      icon: 'thf-icon-arrow-left',
      action: () => { this.location.back(); }
    };

    return [
      {
        label: this.i18n.action.insert,
        icon: 'thf-icon-plus',
        action: () => { this.openInsertModal(); }
      },
      ...(this.hasBackAction ? [backAction] : [])
    ];
  }

  private setI18n() {
    this.i18n = {
      title: {
        brand: this.translate.instant('title.brand'),
        insertBrand: this.translate.instant('title.lbInsert', {
          labelName: this.translate.instant('label.brand')
        }),
        editBrand: this.translate.instant('title.lbEdit', {
          labelName: this.translate.instant('label.brand')
        }),
      },
      placeholder: {
        search: this.translate.instant('placeholder.searchBrandTenantOrChain')
      },
      label: {
        brand: this.translate.instant('label.brand'),
        chain: this.translate.instant('label.chain'),
        tenant: this.translate.instant('label.tenant'),
        actions: this.translate.instant('label.actions'),
        brandName: this.translate.instant('label.brandName')
      },
      action: {
        back: this.translate.instant('action.back'),
        confirm: this.translate.instant('action.confirm'),
        cancel: this.translate.instant('action.cancel'),
        insert: this.translate.instant('action.lbNew', {
          labelName: this.translate.instant('label.brand')
        })
      },
      variable: {
        createSuccess: this.translate.instant('variable.lbSaveSuccessM', {
          labelName: this.translate.instant('label.brand')
        }),
        updateSuccess: this.translate.instant('variable.lbEditSuccessM', {
          labelName: this.translate.instant('label.brand')
        }),
      }
    };
  }

  private openInsertModal() {
    this.changeModalToInsertMode();
    this.modal.open();
  }

  private setModalSettings() {
    this.modalSettings = {
      title: '',
      brandName: {
        label: this.i18n.label.brandName
      },
      chainId: {
        label: this.i18n.label.chain
      },
      confirm: {
        label: this.i18n.action.confirm,
        disabled: this.form.invalid,
        action: () => { this.submit(); }
      },
      cancel: {
        label: this.i18n.action.cancel,
        action: () => { this.modal.close(); }
      },
      size: 'sm',
      clickOut: true
    };

    this.form.statusChanges.subscribe(() => {
      this.modalSettings.confirm.disabled = this.form.invalid;
    });
  }

  private changeModalToInsertMode() {
    this.resetForm();
    this.modalSettings = {
      ...this.modalSettings,
      title: this.i18n.title.insertBrand,
    };
  }

  private createForm() {
    this.form = this.fb.group({
      brandName: ['', Validators.required],
      chainId: ['', Validators.required]
    });
  }

  private resetForm(item: any = {}) {
    this.form.reset({
      brandId: item.id || '',
      brandName: item.brandName || ''
    });
  }

  private clickOnEditItem(item) {
    this.router.navigate([ `edit/${item.id}` ], { relativeTo: this.route });
  }

  private setModalToLoadConfirmState(loadingState: boolean) {
    this.modalSettings = {
      ...this.modalSettings,
      confirm: {
        ...this.modalSettings.confirm,
        loading: loadingState
      }
    };
  }

  private submit() {
    if (this.form.invalid) {
      return;
    }

    this.setModalToLoadConfirmState(true);

    this.createBrand(this.form.value)
      .subscribe(() => {
        this.setModalToLoadConfirmState(false);
        this.modal.close();
        this.pageListWrapper.reloadList();
        this.notification.success(this.i18n.variable.createSuccess);
      });
  }

  private createBrand({brandName, chainId}): Observable<any> {
    const brandRequest: CreateBrandRequest = {
      name: brandName,
      chainId
    };
    return this.brandApi.createBrand(brandRequest);
  }

  private setChainList() {
    // TODO remove this query params and method when this is fixed on backend
    const fakeListQuery: ListQueryParams = {
      SearchData: '',
      Page: 1,
      PageSize: 999
    };

    const searchListToSelectOptions = ({items}) => {
      return items.map((item) => {
        return {
          label: item.name,
          value: item.id
        };
      });
    };

    this.chainApi.list(fakeListQuery)
      .pipe(
        map(searchListToSelectOptions)
      )
      .subscribe((items) => {
        this.chainList = items;
      });
  }
}
