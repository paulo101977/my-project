import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ThfDialogService, ThfNotificationService, ThfPageEditLiterals } from '@totvs/thf-ui';
import { Brand } from '../../models/brand';
import { UpdateBrandRequest } from '../../models/update-brand-request';
import { BrandApiService } from '../../services/brand-api.service';

@Component({
  selector: 'admin-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  private brandId: number;
  public form: FormGroup;
  public i18n: any;
  public literals: ThfPageEditLiterals;
  public brandData: any = {};

  // Lists
  public tablePropertySettings;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private translate: TranslateService,
    private brandApi: BrandApiService,
    private thfNotification: ThfNotificationService,
    private thfDialog: ThfDialogService
  ) {
  }

  ngOnInit() {
    this.brandId = this.route.snapshot.params.id;
    this.setI18n();
    this.setPageSettings();
    this.setFormSettings();
    this.setTableSettings();
    this.getBrand();
  }

  private setI18n() {
    this.i18n = {
      title: this.translate.instant('action.lbEdit', {
        labelName: this.translate.instant('label.brand')
      }),
      group: {
        brand: this.translate.instant('label.brandData'),
        hotel: this.translate.instant('label.propertiesAssociate'),
      },
      label: {
        name: this.translate.instant('label.name'),
        brandName: this.translate.instant('variable.nameOf.F', {
          labelName: this.translate.instant('label.brand')
        }),
        tenant: this.translate.instant('label.tenant'),
        chain: this.translate.instant('label.chain')
      },
      action: {
        confirm: this.translate.instant('action.confirm'),
        cancel: this.translate.instant('action.cancel')
      },
      variable: {
        editSuccess: this.translate.instant('variable.lbEditSuccessF', {
          labelName: this.translate.instant('label.brand')
        })
      },
      alert: {
        title: this.translate.instant('alert.redirectConfirm'),
        body: this.translate.instant('alert.dataHasNotBeenSavedYet')
      }
    };
  }

  private setPageSettings() {
    this.literals = {
      cancel: this.i18n.action.cancel,
      save: this.i18n.action.confirm
    };
  }

  private setFormSettings() {
    this.form = this.fb.group({
      brandName: ['', Validators.required]
    });
  }

  private setTableSettings() {
    this.tablePropertySettings = {
      columns: [
        {property: 'name', label: this.i18n.label.name, type: 'string'},
      ],
      items: []
    };
  }

  private getBrand() {
    this.brandApi.getById(this.brandId)
      .subscribe((brand: any) => {
        this.loadToBrandData(brand);
        this.loadToForm(brand);
        this.loadToTableProperty(brand);
      });
  }

  private loadToBrandData(brand: Brand) {
    this.brandData = {
      tenant: brand.tenantName,
      chain: brand.chainName
    };
  }

  private loadToForm(brand: Brand) {
    this.form.patchValue({
      brandName: brand.name
    });
  }

  private loadToTableProperty(brand: Brand) {
    if (brand.propertyList) {
      this.tablePropertySettings.items = brand.propertyList;
    }
  }

  private beforeRedirect() {
    if (this.form.dirty) {
      this.thfDialog.confirm({
        title: this.i18n.alert.title,
        message: this.i18n.alert.body,
        confirm: () => this.redirectToList()
      });

    } else {
      this.redirectToList();
    }
  }

  public cancel() {
    this.beforeRedirect();
  }

  public save() {
    if (this.form.invalid) {
      return;
    }

    const params: UpdateBrandRequest = {
      id: this.brandId,
      name: this.form.value.brandName
    };

    this.brandApi
      .updateBrand(params)
      .subscribe(
        () => {
          this.thfNotification.success(this.i18n.variable.editSuccess);
          this.form.reset();
          this.redirectToList();
        },
        error => {
          if (error) {
            this.thfNotification
              .warning(error.error.details.map(detail => detail.message).join('\n'));
          }
        });
  }

  private redirectToList() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }
}
