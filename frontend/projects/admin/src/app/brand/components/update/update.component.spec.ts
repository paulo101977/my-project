import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { ThfDialogService, ThfNotificationService } from '@totvs/thf-ui';
import { Observable, of } from 'rxjs';
import { brandMock } from '../../mock/brand-mock';
import { BrandApiService } from '../../services/brand-api.service';

import { UpdateComponent } from './update.component';
import { UpdateBrandRequest } from '../../models/update-brand-request';

describe('UpdateComponent', () => {
  let component: UpdateComponent;
  let fixture: ComponentFixture<UpdateComponent>;
  let brandApi: BrandApiService;
  let thfNotificationService: ThfNotificationService;
  let thfDialogService: ThfDialogService;

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };

  // @ts-ignore
  const brandApiStub: Partial<BrandApiService> = {
    getById: (id) => of(null),
    updateBrand(brandRequest: UpdateBrandRequest): Observable<any> {
      return of(null);
    }
  };

  // @ts-ignore
  const thfNotificationServiceStub: Partial<ThfNotificationService> = {
    success: () => {
    }
  };

  // @ts-ignore
  const thfDialogServiceStub: Partial<ThfDialogService> = {
    confirm: () => {
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [UpdateComponent],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        },
        {
          provide: BrandApiService,
          useValue: brandApiStub
        },
        {
          provide: ThfNotificationService,
          useValue: thfNotificationServiceStub
        },
        {
          provide: ThfDialogService,
          useValue: thfDialogServiceStub
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {id: 1}
            }
          }
        },
        FormBuilder
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();

    brandApi = TestBed.get(BrandApiService);
    thfNotificationService = TestBed.get(ThfNotificationService);
    thfDialogService = TestBed.get(ThfDialogService);
    spyOn(brandApi, 'getById').and.returnValue(of({}));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setPageSettings', () => {
      component['setPageSettings']();
      expect(component.literals.cancel).toEqual(component.i18n.action.cancel);
      expect(component.literals.save).toEqual(component.i18n.action.confirm);
    });

    it('should setFormSettings', () => {
      component['setFormSettings']();

      expect(component['form'].contains('brandName')).toBeTruthy();
    });

    it('should setTableSettings', () => {
      component['setTableSettings']();

      expect(component.tablePropertySettings.columns)
        .toEqual([{property: 'name', label: component.i18n.label.name, type: 'string'}]);
      expect(component.tablePropertySettings.items).toEqual([]);
    });
  });

  describe('load', () => {
    it('should loadToForm', () => {
      component['loadToForm'](brandMock);

      expect(component['form'].value.brandName).toEqual(brandMock.name);
    });

    it('should loadToTableProperty', () => {
      component['loadToTableProperty'](brandMock);

      expect(component.tablePropertySettings.items.length).toEqual(1);
      expect(component.tablePropertySettings.items[0]).toEqual(brandMock.propertyList[0]);
    });
  });

  describe('actions', () => {
    it('should cancel', () => {
      spyOn<any>(component, 'beforeRedirect');

      component.cancel();

      expect(component['beforeRedirect']).toHaveBeenCalled();
    });

    it('should beforeRedirect when data changed', () => {
      spyOn(thfDialogService, 'confirm');
      component['form'].markAsDirty();

      component['beforeRedirect']();

      expect(thfDialogService.confirm).toHaveBeenCalledWith({
        title: component.i18n.alert.title,
        message: component.i18n.alert.body,
        confirm: jasmine.any(Function)
      });
    });

    it('should beforeRedirect when data not changed', () => {
      spyOn<any>(component, 'redirectToList');

      component['beforeRedirect']();

      expect(component['redirectToList']).toHaveBeenCalled();
    });

    it('should redirectToList#edit', () => {
      spyOn(component['router'], 'navigate');

      component['redirectToList']();

      expect(component['router'].navigate)
        .toHaveBeenCalledWith(['../../'], {relativeTo: component['route']});
    });
  });

  describe('resources', () => {
    it('should getBrand', fakeAsync(() => {
      spyOn<any>(component, 'loadToForm');
      spyOn<any>(component, 'loadToTableProperty');

      component['getBrand']();

      tick();

      expect(brandApi.getById)
        .toHaveBeenCalledWith(1);
      expect(component['loadToForm']).toHaveBeenCalledWith({});
      expect(component['loadToTableProperty']).toHaveBeenCalledWith({});
    }));

    it('should saveBrand#edit', fakeAsync(() => {
      component['form'].patchValue({
        id: 1,
        brandName: 'brand test new'
      });

      spyOn(brandApi, 'updateBrand').and.returnValue(of({}));
      spyOn(thfNotificationService, 'success');
      spyOn(component['form'], 'reset');
      spyOn<any>(component, 'redirectToList');

      component['save']();

      tick();

      expect(brandApi.updateBrand)
        .toHaveBeenCalledWith({id: 1, name: 'brand test new'});
      expect(thfNotificationService.success).toHaveBeenCalledWith(component.i18n.variable.editSuccess);
      expect(component['form'].reset).toHaveBeenCalled();
      expect(component['redirectToList']).toHaveBeenCalled();
    }));
  });
});
