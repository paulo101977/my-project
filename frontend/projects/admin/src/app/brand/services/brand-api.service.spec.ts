import { inject, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { CreateBrandRequest } from '../models/create-brand-request';
import { BrandResourceService } from '../resources/brand-resource.service';

import { BrandApiService } from './brand-api.service';
import { UpdateBrandRequest } from '../models/update-brand-request';

describe('BrandApiService', () => {
  let service: BrandApiService;

  // @ts-ignore
  const brandResourceStubService: Partial<BrandResourceService> = {
    list(params: ListQueryParams): Observable<any> { return null; },
    createBrand(brandRequest: CreateBrandRequest): Observable<any> { return null; },
    updateBrand({id, name}: UpdateBrandRequest): Observable<any> {
      return null;
    }
  };
  let brandResourceService: BrandResourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BrandApiService,
        {
          provide: BrandResourceService,
          useValue: brandResourceStubService
        }
      ]
    });

    service = TestBed.get(BrandApiService);
    brandResourceService = TestBed.get(BrandResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call BrandResourceService#list', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(brandResourceService, 'list');

      service.list(mock);
      expect(brandResourceService.list).toHaveBeenCalledWith(mock);
    });
  });

  describe('#listAll', () => {
    it('should call BrandResourceService#listAll', () => {
      const mock = {Page: 1, PageSize: 999, SearchData: ''};
      spyOn(brandResourceService, 'list');

      service.listAll();
      expect(brandResourceService.list).toHaveBeenCalledWith(mock);
    });
  });

  describe('#createBrand', () => {
    it('should call BrandResourceService#list', () => {
      const mock: CreateBrandRequest = {
        chainId: 1,
        name: 'name'
      };
      spyOn(brandResourceService, 'createBrand');

      service.createBrand(mock);
      expect(brandResourceService.createBrand).toHaveBeenCalledWith(mock);
    });
  });
});
