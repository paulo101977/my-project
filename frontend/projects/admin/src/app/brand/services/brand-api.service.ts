import { Injectable } from '@angular/core';

import { ListQueryParams } from '../../shared/models/list-query-params';
import { CreateBrandRequest } from '../models/create-brand-request';
import { UpdateBrandRequest } from '../models/update-brand-request';
import { BrandResourceService } from '../resources/brand-resource.service';

@Injectable({
  providedIn: 'root'
})
export class BrandApiService {

  constructor(private resource: BrandResourceService) { }

  public list(params: ListQueryParams) {
    return this.resource.list(params);
  }

  public listAll() {
    const params: ListQueryParams = {
      PageSize: 999,
      Page: 1,
      SearchData: ''
    };

    return this.list(params);
  }

  public getById(id: number) {
    return this.resource.getById(id);
  }

  public createBrand(brandRequest: CreateBrandRequest) {
    return this.resource.createBrand(brandRequest);
  }

  public updateBrand(brandRequest: UpdateBrandRequest) {
    return this.resource.updateBrand(brandRequest);
  }
}
