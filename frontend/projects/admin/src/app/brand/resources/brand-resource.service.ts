import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { ListQueryParams } from '../../shared/models/list-query-params';
import { CreateBrandRequest } from '../models/create-brand-request';
import { UpdateBrandRequest } from '../models/update-brand-request';

@Injectable({
  providedIn: 'root'
})
export class BrandResourceService {

  constructor(private adminApi: AdminApiService) { }

  public list(params: ListQueryParams) {
    return this.adminApi.get<any>('brand', { params });
  }

  public getById(id) {
    return this.adminApi.get(`brand/${id}`);
  }

  public createBrand(brandRequest: CreateBrandRequest) {
    const params = {
      Name: brandRequest.name,
      ChainId: brandRequest.chainId
    };
    return this.adminApi.post('brand', params);
  }

  public updateBrand({ id, name }: UpdateBrandRequest) {
    const params = {
      Name: name
    };
    return this.adminApi.put(`brand/${id}`, params);
  }
}
