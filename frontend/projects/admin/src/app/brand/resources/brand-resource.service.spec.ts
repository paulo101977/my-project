import { TestBed } from '@angular/core/testing';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { CreateBrandRequest } from '../models/create-brand-request';
import { UpdateBrandRequest } from '../models/update-brand-request';

import { BrandResourceService } from './brand-resource.service';
import { of } from 'rxjs';

describe('BrandResourceService', () => {
  let service: BrandResourceService;
  // @ts-ignore
  const adminApiServiceStub: Partial<AdminApiService> = {
    get: () => of(null),
    post: () => of(null),
    put: () => of(null)
  };
  let adminApiService: AdminApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BrandResourceService,
        {
          provide: AdminApiService,
          useValue: adminApiServiceStub
        }
      ]
    });

    service = TestBed.get(BrandResourceService);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should call AdminApiService#get', () => {
      const mock = {Page: 1, PageSize: 1, SearchData: ''};
      spyOn(adminApiService, 'get');

      service.list(mock);
      expect(adminApiService.get).toHaveBeenCalledWith('brand', { params: mock });
    });
  });

  describe('#getById', () => {
    it('should call AdminApiService#get', () => {
      const id = 1;
      spyOn(adminApiService, 'get');

      service.getById(id);
      expect(adminApiService.get).toHaveBeenCalledWith(`brand/${id}`);
    });
  });

  describe('#createBrand', () => {
    it('should call AdminApiService#post', () => {
      const mock: CreateBrandRequest = {
        name: 'name',
        chainId: 1
      };
      spyOn(adminApiService, 'post');

      service.createBrand(mock);
      expect(adminApiService.post).toHaveBeenCalledWith('brand', {
        Name: mock.name,
        ChainId: mock.chainId
      });
    });
  });

  describe('#updateBrand', () => {
    it('should call AdminApiService#put', () => {
      const mock: UpdateBrandRequest = {
        id: 1,
        name: 'name'
      };
      spyOn(adminApiService, 'put');

      service.updateBrand(mock);
      expect(adminApiService.put).toHaveBeenCalledWith(`brand/${mock.id}`, {
        Name: mock.name
      });
    });
  });
});
