import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  ThfButtonModule,
  ThfFieldModule,
  ThfInfoModule,
  ThfModalModule,
  ThfNotificationModule,
  ThfPageModule,
  ThfTableModule
} from '@totvs/thf-ui';
import { SharedModule } from '../shared/shared.module';
import { BasicListWrapperModule } from '../widgets/basic-list-wrapper/basic-list-wrapper.module';
import { PageListWrapperModule } from '../widgets/page-list-wrapper/page-list-wrapper.module';
import { BrandListPageComponent } from './components/brand-list-page/brand-list-page.component';
import { BrandListComponent } from './components/brand-list/brand-list.component';
import { BrandModalFormListComponent } from './components/brand-modal-form-list/brand-modal-form-list.component';
import { CreateComponent } from './components/create/create.component';
import { UpdateComponent } from './components/update/update.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PageListWrapperModule,
    ThfModalModule,
    ThfButtonModule,
    ThfFieldModule,
    ThfNotificationModule,
    ThfTableModule,
    ThfPageModule,
    ThfInfoModule,
    SharedModule,
    BasicListWrapperModule
  ],
  declarations: [
    CreateComponent,
    UpdateComponent,
    BrandListPageComponent,
    BrandListComponent,
    BrandModalFormListComponent
  ],
  exports: [
    CreateComponent,
    UpdateComponent,
    BrandListPageComponent,
    BrandListComponent,
    BrandModalFormListComponent
  ],
})
export class BrandModule { }



