import { Property } from '../../hotel/models/property';
import { Brand } from '../models/brand';

export const brandMock = new Brand();
const propertyMock = new Property();

brandMock.id = 1;
brandMock.name = 'Brand';
brandMock.chainName = 'Chain';
brandMock.propertyList = [propertyMock];

propertyMock.name = 'Property';
