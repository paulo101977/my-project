export interface CreateBrandRequest {
  name: string;
  chainId: number;
}
