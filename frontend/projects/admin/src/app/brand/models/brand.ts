import { Property } from '../../hotel/models/property';

export class Brand {
  chainId: number;
  chainName: string;
  id: number;
  isTemporary: boolean;
  name: string;
  propertyList: Property[];
  tenantId: string;
  tenantName: string;
}
