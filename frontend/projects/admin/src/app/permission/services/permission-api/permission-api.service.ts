import { Injectable } from '@angular/core';
import { cleanList, extractField, selectListItems } from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PermissionResourceService } from '../../resources/permission-resource/permission-resource.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionApiService {

  constructor(private resource: PermissionResourceService) { }

  public listAll(): Observable<any> {
    return this.resource
      .listAll()
      .pipe(
        extractField('items')
      );
  }

  public listAllGroupedByModules(selectedIds): Observable<any> {
    return this
      .listAll()
      .pipe(
        selectListItems((item) => selectedIds.includes(item.id)),
        this.groupBy('identityModuleId', 'identityModuleName'),
        cleanList()
      );
  }

  private groupBy(id: string, label: string) {
    return map((items: any[]) => {
      return items
        .reduce((group, item) => {
          const newGroupItem = {
            id: item[id],
            label: item[label],
            list: []
          };
          const groupItem = group[item[id]] || newGroupItem;

          groupItem.list.push(item);

          group[item[id]] = groupItem;
          return group;
        }, []);
    });
  }
}
