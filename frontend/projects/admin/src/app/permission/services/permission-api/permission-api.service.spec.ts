import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { PermissionResourceService } from '../../resources/permission-resource/permission-resource.service';

import { PermissionApiService } from './permission-api.service';

describe('PermissionApiService', () => {
  let service: PermissionApiService;

  const permissionResourceServiceStub: Partial<PermissionResourceService> = {
    listAll(): Observable<any> { return null; }
  };
  let permissionResourceService: PermissionResourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PermissionApiService,
        {
          provide: PermissionResourceService,
          useValue: permissionResourceServiceStub
        }
      ]
    });

    service = TestBed.get(PermissionApiService);
    permissionResourceService = TestBed.get(PermissionResourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#listAll', () => {
    it('should call PermissionResourceService#listAll', () => {
      spyOn(permissionResourceService, 'listAll').and.returnValue(of({items: []}));

      service.listAll();
      expect(permissionResourceService.listAll).toHaveBeenCalled();
    });
  });

  describe('#listAllGroupedByModules', () => {
    it('should call PermissionApiService#listAllGroupedByModules', () => {
      spyOn(service, 'listAll').and.returnValue(of([]));

      service.listAllGroupedByModules([]);
      expect(service.listAll).toHaveBeenCalled();
    });

    it('should return the permissions grouped by it\'s module', () => {
      const permissions = [
        { identityModuleId: 1, identityModuleName: 'first', id: 123 },
        { identityModuleId: 2, identityModuleName: 'first', id: 456 },
        { identityModuleId: 1, identityModuleName: 'first', id: 789 },
      ];
      spyOn(service, 'listAll').and.returnValue(of(permissions));

      let groupedPermissions = [];
      service
        .listAllGroupedByModules([])
        .subscribe((responsePermissions) => {
          groupedPermissions = responsePermissions;
        });

      expect(groupedPermissions.length).toBe(2);
      expect(groupedPermissions[0].list.length).toBe(2);
      expect(groupedPermissions[1].list.length).toBe(1);
    });

    it('should return the permissions selected', () => {
      const permissions = [
        { identityModuleId: 1, identityModuleName: 'first', id: 123 },
        { identityModuleId: 2, identityModuleName: 'first', id: 456 },
        { identityModuleId: 1, identityModuleName: 'first', id: 789 },
      ];
      spyOn(service, 'listAll').and.returnValue(of(permissions));

      let groupedPermissions = [];
      service
        .listAllGroupedByModules([123, 456])
        .subscribe((responsePermissions) => {
          groupedPermissions = responsePermissions;
        });

      expect(groupedPermissions.length).toBe(2);
      expect(groupedPermissions[0].list.length).toBe(2);
      expect(groupedPermissions[1].list.length).toBe(1);
      expect(groupedPermissions[0].list).toEqual(jasmine.arrayContaining([
        jasmine.objectContaining({id: 123, $selected: true})
      ]));
      expect(groupedPermissions[0].list).not.toEqual(jasmine.arrayContaining([
        jasmine.objectContaining({id: 789, $selected: true})
      ]));
      expect(groupedPermissions[1].list).toEqual(jasmine.arrayContaining([
        jasmine.objectContaining({id: 456, $selected: true})
      ]));
    });
  });
});
