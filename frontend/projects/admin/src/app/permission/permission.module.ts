import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThfTableModule } from '@totvs/thf-ui';
import { PermissionsListComponent } from './components/permissions-list/permissions-list.component';

@NgModule({
  imports: [
    CommonModule,
    ThfTableModule
  ],
  declarations: [PermissionsListComponent],
  exports: [PermissionsListComponent]
})
export class PermissionModule { }
