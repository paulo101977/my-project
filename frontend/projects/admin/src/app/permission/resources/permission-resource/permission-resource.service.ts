import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PermissionResourceService {

  constructor(private api: AdminApiService) { }

  public listAll(): Observable<any> {
    return this.api.get<any>('Permission');
  }
}
