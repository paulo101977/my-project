import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { PermissionApiService } from '../../services/permission-api/permission-api.service';

import { PermissionsListComponent } from './permissions-list.component';

describe('PermissionsListComponent', () => {
  let component: PermissionsListComponent;
  let fixture: ComponentFixture<PermissionsListComponent>;

  const translateServiceStub: Partial<TranslateService> = {
    instant(key: string | Array<string>, interpolateParams?: Object): string | any { return 'null'; }
  };

  const permissionApiServiceStub: Partial<PermissionApiService> = {
    listAll(): Observable<any> { return of([]); },
    listAllGroupedByModules(): Observable<any> { return of([]); }
  };

  @Component({ selector: 'thf-table', template: '' }) // tslint:disable-line
  class ThfTableStubComponent {
    items = [];
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ PermissionsListComponent, ThfTableStubComponent ],
      providers: [
        {
          provide: PermissionApiService,
          useValue: permissionApiServiceStub
        },
        {
          provide: TranslateService,
          useValue: translateServiceStub
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
