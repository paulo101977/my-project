import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { extractField, filterBy, mergeNestedLists } from '@inovacaocmnet/thx-bifrost';
import { TranslateService } from '@ngx-translate/core';
import { ThfTableColumn, ThfTableComponent, ThfTableDetail } from '@totvs/thf-ui';
import { from, Observable } from 'rxjs';
import { map, toArray } from 'rxjs/operators';
import { PermissionApiService } from '../../services/permission-api/permission-api.service';

@Component({
  selector: 'admin-permissions-list',
  templateUrl: './permissions-list.component.html',
  styleUrls: ['./permissions-list.component.css']
})
export class PermissionsListComponent implements OnInit {
  @Input() selectedPermissions: any[];
  @ViewChild('permissionsTable') permissionsTable: ThfTableComponent;
  public permissionsList$: Observable<any[]>;
  public permissionsTableSettings: any;
  public i18n: any;

  constructor(
    private permissionApi: PermissionApiService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.permissionsList$ = this.getPermissions();
    this.setI18n();
    this.setPermissionsTableSettings();
  }

  public getSelectedPermissions(): Observable<any[]> {
    return from(this.permissionsTable.items)
      .pipe(
        extractField('list'),
        toArray(),
        mergeNestedLists(),
        filterBy(item => item.$selected),
        this.mapPermissionId()
      );
  }

  private getPermissions() {
    const selectedIds = this.getSelectedIds(this.selectedPermissions);
    return this.permissionApi.listAllGroupedByModules(selectedIds);
  }

  private setI18n() {
    this.i18n = {
      label: {
        module: this.translate.instant('label.module'),
        permission: this.translate.instant('label.permission'),
        permissions: this.translate.instant('label.permissions')
      }
    };
  }

  private setPermissionsTableSettings() {
    this.permissionsTableSettings = {
      items: [],
      columns: this.getPermissionsTableColumns(),
      striped: true,
      sort: true,
      loading: false,
      height: 330
    };
  }

  private getPermissionsTableColumns(): ThfTableColumn[] {
    const permissionsDetail: ThfTableDetail = {
      columns: [
        { column: 'name', label: this.i18n.label.permission }
      ],
      typeHeader: 'top'
    };

    return [
      { property: 'label', label: this.i18n.label.module, type: 'string', width: '930px' },
      { property: 'list', label: this.i18n.label.permissions, type: 'detail', detail: permissionsDetail }
    ];
  }

  private mapPermissionId() {
    return map((list: any[]) => list.map((item: any) => {
      return { identityPermissionId: item.id };
    }));
  }

  private getSelectedIds(selectedPermissions = []) {
    return selectedPermissions
      .map((selectedPermission) => {
        return selectedPermission.identityPermissionId;
      });
  }
}
