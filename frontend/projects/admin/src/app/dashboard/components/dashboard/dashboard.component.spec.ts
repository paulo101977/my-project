import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { DashboardProperty } from '../../model/dashboard-property';

import { DashboardComponent } from './dashboard.component';
import { Component, CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DashboardResource } from '../../resources/dashboard.resource';
import { of as observableOf } from 'rxjs';
import { propertiesMock } from '../../mock/dashboard-mock';
import { FormBuilder } from '@angular/forms';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let translateService: TranslateService;
  let dashboardResource: DashboardResource;

  class DashboardResourceStub {
    public getAllProperties() {
    }

    public toggleblockproperty(id) {
    }

    public advanceHotelStep(id) {
    }
  }

  @Pipe({name: 'translate'})
  class TranslateStubPipe implements PipeTransform {
    transform() {
    }
  }

  // tslint:disable
  @Component({selector: 'thf-modal', template: ''})
  class ThfModalComponentMockComponent {
    open = jasmine.createSpy('open');
    close = jasmine.createSpy('close');
  }

  @Component({selector: 'admin-hotel-block-modal', template: ''})
  class HotelBlockModalComponentMockComponent {
    open = jasmine.createSpy('open');
    close = jasmine.createSpy('close');
  }

  // tslint:enable

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent,
        ThfModalComponentMockComponent,
        HotelBlockModalComponentMockComponent,
        TranslateStubPipe
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: TranslateService,
          useClass: class TranslateServiceStub {
            public instant() {
              return 'default';
            }
          },
        },
        {
          provide: DashboardResource,
          useClass: DashboardResourceStub
        },
        FormBuilder
      ]
    })
      .compileComponents();

    translateService = TestBed.get(TranslateService);
    dashboardResource = TestBed.get(DashboardResource);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;

    spyOn(dashboardResource, 'getAllProperties')
      .and.returnValue(observableOf({items: propertiesMock}));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setVars', () => {
      spyOn<any>(component, 'setI18n');
      spyOn(translateService, 'instant').and.callThrough();
      spyOn(component, 'setModalSettings');

      component['setVars']();

      expect(component['setI18n']).toHaveBeenCalled();
      expect(component.title).toEqual(component.i18n.title);
      expect(component.filterModel).toEqual('');
      expect(component.filter).toEqual({
        placeholder: component.i18n.placeholder,
        ngModel: 'filterModel'
      });
      expect(component.setModalSettings).toHaveBeenCalled();
    });

    it('should setModalSettings', () => {
      component.setModalSettings();

      expect(component.moveHotelModal.title).toEqual(component.i18n.modalMoveHotel.title);
      expect(component.moveHotelModal.secondaryAction).toEqual({
        action: jasmine.any(Function),
        label: component.i18n.modalMoveHotel.action.cancel
      });
      expect(component.moveHotelModal.primaryAction).toEqual({
        action: jasmine.any(Function),
        label: component.i18n.modalMoveHotel.action.confirm
      });
    });
  });

  describe('actions', () => {
    it('should openModal', () => {

      component.openModal();
      expect(component.thfModal.open).toHaveBeenCalled();
    });

    it('should closeModal', () => {

      component.closeModal();
      expect(component.hotelToMove).toBeNull();
      expect(component.thfModal.close).toHaveBeenCalled();
    });

    it('should openModalToggleBlock', () => {
      component.openModalToggleBlock({id: '', name: ''} as DashboardProperty);
      expect(component.hotelBlockModal.open).toHaveBeenCalled();
    });
  });

  describe('resources', () => {
    it('should getAllProperties', fakeAsync(() => {
      component.getAllProperties();

      tick();

      expect(dashboardResource.getAllProperties).toHaveBeenCalled();
      expect(component.properties.length).toEqual(7);
      expect(component.showLoad).toBeFalsy();
    }));

    it('should moveHotel', fakeAsync(() => {
      spyOn(dashboardResource, 'advanceHotelStep').and.returnValue(observableOf({}));
      spyOn(component, 'closeModal');
      spyOn(component, 'getAllProperties');

      component.hotelToMove = propertiesMock[0];
      component['moveHotel']();

      tick();

      expect(dashboardResource.advanceHotelStep).toHaveBeenCalledWith(component.hotelToMove.id);
      expect(component.closeModal).toHaveBeenCalled();
      expect(component.getAllProperties).toHaveBeenCalled();
    }));
  });


});
