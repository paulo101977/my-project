import { Component, OnInit, ViewChild } from '@angular/core';
import { ThfPageFilter } from '@totvs/thf-ui/components/thf-page';
import { TranslateService } from '@ngx-translate/core';
import { HotelBlockModalComponent } from '../../../hotel/components/hotel-block-modal/hotel-block-modal.component';
import { DashboardProperty } from '../../model/dashboard-property';
import { DashboardResource } from '../../resources/dashboard.resource';
import { ThfModalComponent } from '@totvs/thf-ui';
import { ModalSettings } from '../../../shared/models/modal-settings';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'admin-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public i18n: any;
  public title: string;
  public filterModel: string;
  public filter: ThfPageFilter;
  public properties: DashboardProperty[];
  public moveHotelModal: ModalSettings;
  public toggleHotelModal: ModalSettings;
  public hotelToMove: DashboardProperty;
  @ViewChild('modal') thfModal: ThfModalComponent;
  @ViewChild('modalToggle') hotelBlockModal: HotelBlockModalComponent;
  public showLoad: boolean;

  constructor(
    private translate: TranslateService,
    private dashboardResource: DashboardResource,
    private dashboardService: DashboardService,
  ) {}

  ngOnInit() {
    this.setVars();
    this.getAllProperties();
  }

  private setI18n() {
    this.i18n = {
      title: this.translate.instant('label.dashboard'),
      placeholder: this.translate.instant('placeholder.searchByNameProperty'),
      modalMoveHotel: {
        title: this.translate.instant('title.advanceStep'),
        text: this.translate.instant('alert.doYouWantToMoveTheHotel'),
        action: {
          confirm: this.translate.instant('action.confirm'),
          cancel: this.translate.instant('action.cancel'),
        },
      },
    };
  }

  private setVars() {
    this.setI18n();
    this.title = this.i18n.title;
    this.filterModel = '';
    this.filter = {
      placeholder: this.i18n.placeholder,
      ngModel: 'filterModel'
    };
    this.setModalSettings();
  }

  public getAllProperties() {
    this.showLoad = true;
    this.dashboardResource.getAllProperties()
      .subscribe(({items}) => {
        this.properties = items;
        this.showLoad = false;
      });
  }

  public setModalSettings() {
    this.moveHotelModal = {
      title: this.i18n.modalMoveHotel.title,
      secondaryAction: {
        action: () => this.closeModal(),
        label: this.i18n.modalMoveHotel.action.cancel
      },
      primaryAction: {
        action: () => this.moveHotel(),
        label: this.i18n.modalMoveHotel.action.confirm
      }
    };

    this.dashboardService.moveHotelCalled$
      .subscribe((hotelToMove: DashboardProperty) => {
        this.hotelToMove = hotelToMove;
        this.openModal();
      });

    this.dashboardService.blockHotelCalled$
      .subscribe((hotelToggleBlock: DashboardProperty) => {
        this.openModalToggleBlock(hotelToggleBlock);
      });
  }

  public openModal() {
    this.thfModal.open();
  }

  public closeModal() {
    this.hotelToMove = null;
    this.thfModal.close();
  }

  private moveHotel() {
    if (this.hotelToMove) {
      this.showLoad = true;
      this.dashboardResource.advanceHotelStep(this.hotelToMove.id)
        .subscribe(
          () => {
            this.closeModal();
            this.getAllProperties();
          });
    }
  }

  public openModalToggleBlock({name, id, isBlocked}: DashboardProperty) {
    this.hotelBlockModal.open(name, id, isBlocked);
  }

  public onHotelBlockToggle(hasToggled: boolean) {
    if (!hasToggled) {
      this.showLoad = true;
      return;
    }

    this.getAllProperties();
  }

}
