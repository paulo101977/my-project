import { AfterViewInit, Component, ElementRef, Input, OnChanges, Renderer2, ViewChild } from '@angular/core';
import { DashboardProperty, PropertyStatusEnum } from '../../model/dashboard-property';
import { ThfPopoverComponent, ThfPopupAction } from '@totvs/thf-ui';
import { TranslateService } from '@ngx-translate/core';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'admin-card-dashboard-property',
  templateUrl: './card-dashboard-property.component.html',
  styleUrls: ['./card-dashboard-property.component.css']
})
export class CardDashboardPropertyComponent implements OnChanges, AfterViewInit {

  @Input() property: DashboardProperty;
  @ViewChild('target') targetPopover: ElementRef;
  @ViewChild('popover') popover: ThfPopoverComponent;
  public propertyStatus = PropertyStatusEnum;
  public actions: ThfPopupAction[];

  constructor(
    private renderer: Renderer2,
    private translate: TranslateService,
    private dashBoardService: DashboardService) {
  }

  ngOnChanges() {
    this.setActions();
  }

  private setActions() {
    this.actions = [
      // TODO implement details redirect
      {label: this.translate.instant('label.details'), action: () => console.log('to implement')}
    ];

    if (this.property.propertyStatusId != PropertyStatusEnum.production) {
      this.actions.push(
        {
          label: this.translate.instant('action.advance'),
          action: () => this.dashBoardService.moveHotelToNextStep(this.property)
        }
      );
    }
    const labelToggleBlock = this.property.isBlocked
      ? this.translate.instant('action.unblock')
      : this.translate.instant('action.block');

    this.actions.push(
      {
        label: labelToggleBlock,
        action: () => this.dashBoardService.toggleBlock(this.property),
        type: this.property.isBlocked ? 'danger' : '',
        selected: this.property.isBlocked
      }
    );
  }

  ngAfterViewInit() {
    if (this.popover) {
      const popover = this.popover.popoverElement.nativeElement;

      this.renderer
        .setStyle(popover, 'backgroundColor', 'var(--color-grey-feldgrau)');
      this.renderer
        .setStyle(popover.querySelector('.thf-popover-arrow'), 'backgroundColor', 'var(--color-grey-feldgrau)');
      this.renderer
        .setStyle(popover, 'color', 'var(--color-grey)');
    }
  }

}
