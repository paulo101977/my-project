import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDashboardPropertyComponent } from './card-dashboard-property.component';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { MomentModule } from 'ngx-moment';
import { ThfPopoverComponent } from '@totvs/thf-ui';
import { TranslateService } from '@ngx-translate/core';
import { propertiesMock } from '../../mock/dashboard-mock';

describe('CardDashboardPropertyComponent', () => {
  let component: CardDashboardPropertyComponent;
  let fixture: ComponentFixture<CardDashboardPropertyComponent>;

  @Pipe({name: 'translate'})
  class TranslateStubPipe implements PipeTransform {
    transform() {
    }
  }

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };

  @Pipe({ name: 'amDateFormat' })
  class AmDateFormatStubPipe implements PipeTransform {
    transform(value: any, ...args: any[]): any {
      return 'null';
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MomentModule],
      declarations: [
        CardDashboardPropertyComponent,
        TranslateStubPipe,
        ThfPopoverComponent,
        AmDateFormatStubPipe
      ],
      providers: [
        {provide: TranslateService, useValue: translateStubService}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDashboardPropertyComponent);
    component = fixture.componentInstance;
    component.property = propertiesMock[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set actions', () => {
    component['setActions']();

    expect(component.actions).toEqual([
      {label: component['translate'].instant('label.details'), action: jasmine.any(Function)},
      {label: component['translate'].instant('action.advance'), action: jasmine.any(Function)},
      {label: component['translate'].instant('action.block'), action: jasmine.any(Function), type: '', selected: false}
    ]);
  });
});
