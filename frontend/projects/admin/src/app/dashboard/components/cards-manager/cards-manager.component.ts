import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { DashboardProperty, PropertyStatusEnum } from '../../model/dashboard-property';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'admin-cards-manager',
  templateUrl: './cards-manager.component.html',
  styleUrls: ['./cards-manager.component.css']
})
export class CardsManagerComponent implements OnInit, OnChanges {

  public i18n: any;
  public columnsTitle: any[];
  public columnsProperties: any[];
  public propertiesFiltered: DashboardProperty[] = [];
  public propertiesStatusRegister: DashboardProperty[] = [];
  public propertiesStatusTraining: DashboardProperty[] = [];
  public propertiesStatusContract: DashboardProperty[] = [];
  public propertiesStatusProduction: DashboardProperty[] = [];

  @Input() properties: DashboardProperty[];
  @Input() filterModel: string;

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
    this.i18n = {
      label: {
        register: this.translate.instant('label.register'),
        training: this.translate.instant('label.training'),
        contract: this.translate.instant('label.contract'),
        production: this.translate.instant('label.production')
      }
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('properties')
      || changes.hasOwnProperty('filterModel')) {
      this.filterProperties();
    }
    this.setColumnsTitle();
    this.setColumnsProperties();
  }

  private setColumnsTitle() {
    if (this.i18n) {
      this.columnsTitle = [
        {label: this.i18n.label.register, propertyQuantity: this.propertiesStatusRegister.length},
        {label: this.i18n.label.contract, propertyQuantity: this.propertiesStatusContract.length},
        {label: this.i18n.label.training, propertyQuantity: this.propertiesStatusTraining.length},
        {label: this.i18n.label.production, propertyQuantity: this.propertiesStatusProduction.length}
      ];
    }
  }

  private setColumnsProperties() {
    if (this.i18n) {
      this.columnsProperties = [
        {
          statusName: this.i18n.label.register,
          hasProperties: this.propertiesStatusRegister.length > 0,
          propertyList: this.propertiesStatusRegister
        },
        {
          statusName: this.i18n.label.contract,
          hasProperties: this.propertiesStatusContract.length > 0,
          propertyList: this.propertiesStatusContract
        },
        {
          statusName: this.i18n.label.training,
          hasProperties: this.propertiesStatusTraining.length > 0,
          propertyList: this.propertiesStatusTraining
        },
        {
          statusName: this.i18n.label.production,
          hasProperties: this.propertiesStatusProduction.length > 0,
          propertyList: this.propertiesStatusProduction
        }
      ];
    }
  }

  private setListsByPropertyStatus() {
    if (this.propertiesFiltered) {
      this.propertiesStatusRegister = this.propertiesFiltered
        .filter(property => {
          return property.propertyStatusId == PropertyStatusEnum.register;
        });
      this.propertiesStatusTraining = this.propertiesFiltered
        .filter(property => {
          return property.propertyStatusId == PropertyStatusEnum.training;
        });
      this.propertiesStatusContract = this.propertiesFiltered
        .filter(property => {
          return property.propertyStatusId == PropertyStatusEnum.contract;
        });
      this.propertiesStatusProduction = this.propertiesFiltered
        .filter(property => {
          return property.propertyStatusId == PropertyStatusEnum.production;
        });
    }
  }

  public filterProperties() {
    if (this.properties) {
      this.propertiesFiltered = this.properties
        .filter(property => {
          return property.name.toLocaleLowerCase().startsWith(this.filterModel.toLocaleLowerCase());
        });
      this.setListsByPropertyStatus();
    }
  }

}
