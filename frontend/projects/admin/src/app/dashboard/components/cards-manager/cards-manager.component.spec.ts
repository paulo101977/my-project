import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsManagerComponent } from './cards-manager.component';
import { propertiesMock } from '../../mock/dashboard-mock';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

describe('CardsManagerComponent', () => {
  let component: CardsManagerComponent;
  let fixture: ComponentFixture<CardsManagerComponent>;

  @Pipe({name: 'translate'})
  class TranslateStubPipe implements PipeTransform {
    transform() {
    }
  }

  // @ts-ignore
  const translateStubService: Partial<TranslateService> = {
    instant: () => {
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CardsManagerComponent, TranslateStubPipe],
      providers: [
        {
          provide: TranslateService,
          useValue: translateStubService
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('set settings', () => {
    it('should setListsByPropertyStatus - Register, Contract, Training and Production', () => {
      component.propertiesFiltered = propertiesMock;

      component['setListsByPropertyStatus']();

      expect(component.propertiesStatusRegister.length).toEqual(3);
      expect(component.propertiesStatusContract.length).toEqual(1);
      expect(component.propertiesStatusTraining.length).toEqual(2);
      expect(component.propertiesStatusProduction.length).toEqual(1);
    });

    it('should filterProperties by name', () => {
      spyOn<any>(component, 'setListsByPropertyStatus');

      component.properties = propertiesMock;

      component.filterModel = 'Ibis';
      component.filterProperties();
      expect(component.propertiesFiltered.length).toEqual(1);

      component.filterModel = 'H';
      component.filterProperties();
      expect(component.propertiesFiltered.length).toEqual(2);

      expect(component['setListsByPropertyStatus']).toHaveBeenCalledTimes(2);
    });

    it('should setColumnsTitle', () => {
      component.propertiesFiltered = propertiesMock;

      component['setColumnsTitle']();

      expect(component.columnsTitle).toEqual([
        {label: component.i18n.label.register, propertyQuantity: component.propertiesStatusRegister.length},
        {label: component.i18n.label.contract, propertyQuantity: component.propertiesStatusContract.length},
        {label: component.i18n.label.training, propertyQuantity: component.propertiesStatusTraining.length},
        {label: component.i18n.label.production, propertyQuantity: component.propertiesStatusProduction.length}
      ]);
    });

    it('should setColumnsProperties', () => {
      component.propertiesFiltered = propertiesMock;

      component['setColumnsProperties']();

      expect(component.columnsProperties).toEqual([
        {
          statusName: component.i18n.label.register,
          hasProperties: component.propertiesStatusRegister.length > 0,
          propertyList: component.propertiesStatusRegister
        },
        {
          statusName: component.i18n.label.contract,
          hasProperties: component.propertiesStatusContract.length > 0,
          propertyList: component.propertiesStatusContract
        },
        {
          statusName: component.i18n.label.training,
          hasProperties: component.propertiesStatusTraining.length > 0,
          propertyList: component.propertiesStatusTraining
        },
        {
          statusName: component.i18n.label.production,
          hasProperties: component.propertiesStatusProduction.length > 0,
          propertyList: component.propertiesStatusProduction
        }
      ]);
    });
  });
});
