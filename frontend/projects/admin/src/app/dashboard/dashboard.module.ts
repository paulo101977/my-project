import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HotelModule } from '../hotel/hotel.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { RouterModule } from '@angular/router';
import { ThfModule } from '@totvs/thf-ui';
import { I18nModule } from '@inovacaocmnet/thx-bifrost';
import { CardsManagerComponent } from './components/cards-manager/cards-manager.component';
import { MomentModule } from 'ngx-moment';
import { CardDashboardPropertyComponent } from './components/card-dashboard-property/card-dashboard-property.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ThfModule,
    DashboardRoutingModule,
    I18nModule,
    MomentModule,
    ReactiveFormsModule,
    HotelModule
  ],
  declarations: [
    DashboardComponent,
    CardsManagerComponent,
    CardDashboardPropertyComponent
  ],
  exports: [
    MomentModule
  ]
})
export class DashboardModule {
}
