export class DashboardProperty {
  id: string;
  name: string;
  creationDate: string;
  initialsName: string;
  isActive: boolean;
  isBlocked: boolean;
  propertyStatusId: PropertyStatusEnum;
  propertyStatusHistoryDtoList: PropertyStatusHistory[];
}

export class PropertyStatusHistory {
  id: string;
  creationDate: string;
  propertyStatusId: PropertyStatusEnum;
  propertyStatusName: string;
}

export enum PropertyStatusEnum {
  register = 13,
  contract = 14,
  training = 15,
  production = 19,
  blocked = 20,
  unblocked = 21
}
