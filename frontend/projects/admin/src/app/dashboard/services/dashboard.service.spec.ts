import { inject, TestBed } from '@angular/core/testing';

import { DashboardService } from './dashboard.service';
import { propertiesMock } from '../mock/dashboard-mock';

describe('DashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DashboardService]
    });
  });

  it('should be created', inject([DashboardService], (service: DashboardService) => {
    expect(service).toBeTruthy();
  }));

  it('should moveHotelToNextStep', inject([DashboardService], (service: DashboardService) => {
    spyOn(service['moveHotelCall'], 'next');

    service.moveHotelToNextStep(propertiesMock[0]);

    expect(service['moveHotelCall'].next).toHaveBeenCalled();
  }));
});
