import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { DashboardProperty } from '../model/dashboard-property';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private moveHotelCall = new Subject();
  public moveHotelCalled$ = this.moveHotelCall.asObservable();
  private blockHotelCall = new Subject();
  public blockHotelCalled$ = this.blockHotelCall.asObservable();

  constructor() {
  }

  public moveHotelToNextStep(hotel: DashboardProperty) {
    this.moveHotelCall.next(hotel);
  }

  public toggleBlock(hotel: DashboardProperty) {
    this.blockHotelCall.next(hotel);
  }
}
