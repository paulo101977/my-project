import { Injectable } from '@angular/core';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';

@Injectable({
  providedIn: 'root'
})
export class DashboardResource {

  constructor(private adminApi: AdminApiService) {
  }

  public getAllProperties() {
    return this.adminApi.get(`Dashboard`);
  }

  public advanceHotelStep(id: string) {
    return this.adminApi.patch(`Dashboard/next/${id}`);
  }

  public toggleblockproperty(id: string) {
    return this.adminApi.patch(`Dashboard/${id}/toggleblockproperty`);
  }
}
