import { inject, TestBed } from '@angular/core/testing';
import { AdminApiService } from '@inovacaocmnet/thx-bifrost';

import { DashboardResource } from './dashboard.resource';
import { of } from 'rxjs';

describe('DashboardResource', () => {
  // @ts-ignore
  const adminApiServiceStub: Partial<AdminApiService> = {
    get: () => of( null )
  };
  let adminApiService: AdminApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: AdminApiService,
          useValue: adminApiServiceStub
        },
        DashboardResource
      ]
    });

    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', inject([DashboardResource], (service: DashboardResource) => {
    expect(service).toBeTruthy();
  }));
});
