import { DashboardProperty, PropertyStatusEnum } from '../model/dashboard-property';

export const propertiesMock: DashboardProperty[] = [
  {
    id: '',
    name: 'Ramada Encore',
    creationDate: '07/02/2018',
    initialsName: '',
    isActive: true,
    propertyStatusId: PropertyStatusEnum.register,
    propertyStatusHistoryDtoList: null,
    isBlocked: false
  },
  {
    id: '',
    name: 'Hudson Resort',
    creationDate: '07/02/2018',
    initialsName: '',
    isActive: true,
    propertyStatusId: PropertyStatusEnum.training,
    propertyStatusHistoryDtoList: null,
    isBlocked: false
  },
  {
    id: '',
    name: 'Red Velvet Hotel',
    creationDate: '07/02/2018',
    initialsName: '',
    isActive: true,
    propertyStatusId: PropertyStatusEnum.contract,
    propertyStatusHistoryDtoList: null,
    isBlocked: false
  },
  {
    id: '',
    name: 'Hilton Co',
    creationDate: '07/02/2018',
    initialsName: '',
    isActive: true,
    propertyStatusId: PropertyStatusEnum.production,
    propertyStatusHistoryDtoList: null,
    isBlocked: false
  },
  {
    id: '',
    name: 'Windsor Atlântica',
    creationDate: '07/02/2018',
    initialsName: '',
    isActive: true,
    propertyStatusId: PropertyStatusEnum.register,
    propertyStatusHistoryDtoList: null,
    isBlocked: false
  },
  {
    id: '',
    name: 'Victoria',
    creationDate: '07/02/2018',
    initialsName: '',
    isActive: true,
    propertyStatusId: PropertyStatusEnum.training,
    propertyStatusHistoryDtoList: null,
    isBlocked: false
  },
  {
    id: '',
    name: 'Ibis Budget',
    creationDate: '07/02/2018',
    initialsName: '',
    isActive: true,
    propertyStatusId: PropertyStatusEnum.register,
    propertyStatusHistoryDtoList: null,
    isBlocked: false
  }
];
