import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AssetsCdnService {

  private url = 'https://cdn.totvscmnet-cloud.net';
  private version = '0.45.10';

  constructor() { }

  configCdn(baseUrl: string, version?: string) {
    this.url = baseUrl;
    this.version = version;
  }

  getComponentsUrlTo(item: string): string {
    return this.getUrlTo('components', item);
  }

  getImgUrlTo(item: string): string {
    return this.getUrlTo('img', item);
  }

  private getUrlTo(path: string, item: string): string {
    return `${this.url}/${path}/thex/${this.version}/${item}`;
  }
}
