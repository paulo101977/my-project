import { TestBed, inject } from '@angular/core/testing';

import { AssetsCdnService } from './assets-cdn.service';

describe('AssetsCdnService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetsCdnService]
    });
  });

  it('should be created', inject([AssetsCdnService], (service: AssetsCdnService) => {
    expect(service).toBeTruthy();
  }));
});
