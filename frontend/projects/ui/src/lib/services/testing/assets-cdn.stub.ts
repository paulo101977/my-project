import { createServiceStub } from '../../../../../../testing';
import { AssetsCdnService } from '../assets-service/assets-cdn.service';

export const assetsCdnServiceStub = createServiceStub(AssetsCdnService, {
  getImgUrlTo (item: string): string {
    return 'hotel.jpg';
  }
});
