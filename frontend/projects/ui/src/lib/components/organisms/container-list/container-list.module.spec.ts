import { ContainerListModule } from './container-list.module';

describe('ContainerListModule', () => {
  let containerListModule: ContainerListModule;

  beforeEach(() => {
    containerListModule = new ContainerListModule();
  });

  it('should create an instance', () => {
    expect(containerListModule).toBeTruthy();
  });
});
