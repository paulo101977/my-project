export class Notes {
    private notes = `
## Html:

~~~json
<thx-container-list
    [title]="'My Test Title'"
    [limit]="2"
    [itemTemplate]="externalTemplate"
    [items]="scope.items">
</thx-container-list>

<ng-template #externalTemplate let-item="item">
    <div class="d-flex justify-content-center">
      <div>{{ item?.title }}</div>
    </div>
</ng-template>
~~~

## Typescript:

~~~typescript

~~~
`;

    public getNotes(): string {
        return this.notes;
    }
}
