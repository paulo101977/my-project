import { Component, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'thx-container-list',
  templateUrl: './container-list.component.html',
  styleUrls: ['./container-list.component.scss']
})
export class ContainerListComponent {
  private _items: Array<any>;

  @Input() title: string;
  @Input() limit: number;

  // set/get items
  @Input() set items(_items: Array<any>) {
    const { limit } = this;
    if ( _items ) {
      // deep clone the array (ensures no item is removed from original array)
      const clonedItems = JSON.parse(JSON.stringify(_items));

      if ( limit ) {
        this._items = clonedItems.slice(0, limit);
        return;
      }

      this._items = clonedItems;
    }
  }
  get items() {
    return this._items;
  }

  @Input() itemTemplate: TemplateRef<any>;
  @Input() emptyTemplate: TemplateRef<any>;

  public getItemsLength() {
    const { items } = this;

    return items ? items.length : 0;
  }
}
