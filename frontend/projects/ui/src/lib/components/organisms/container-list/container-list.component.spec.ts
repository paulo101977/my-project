import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerListComponent } from './container-list.component';

import { configureTestSuite } from 'ng-bullet';

describe('ContainerListComponent', () => {
  let component: ContainerListComponent;
  let fixture: ComponentFixture<ContainerListComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerListComponent ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
