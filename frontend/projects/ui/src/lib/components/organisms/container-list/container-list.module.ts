import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerListComponent } from './container-list.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ContainerListComponent],
  exports: [ContainerListComponent],
})
export class ContainerListModule { }
