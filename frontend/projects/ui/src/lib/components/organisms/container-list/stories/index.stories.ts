import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';
import { ContainerListComponent } from '../container-list.component';
import { componentFactory } from '.storybook/component-factory';


const items = [
    { title: 'test 1' },
    { title: 'test 2' },
    { title: 'test 3' },
];

// params: selector, template, scope
const DefaultComponent = componentFactory('default', `
    <thx-container-list
        [title]="'My Test Title'"
        [itemTemplate]="externalTemplate"
        [limit]="2"
        [items]="scope.items">
    </thx-container-list>

    <ng-template #externalTemplate let-item="item">
        <div class="d-flex justify-content-center">
          <div>{{ item?.title }}</div>
        </div>
    </ng-template>
`, {items});

// params: selector, template, scope
const ListWithScrollComponent = componentFactory('list-with-scroll', `
    <div style="height: 120px;">
        <thx-container-list
          [title]="'My Test Title'"
          [itemTemplate]="externalTemplate"
          [items]="scope.items">
      </thx-container-list>
    </div>

    <ng-template #externalTemplate let-item="item">
        <div class="d-flex justify-content-center">
          <div>{{ item?.title }}</div>
        </div>
    </ng-template>
`, {items});

const WithoutLimitComponent = componentFactory('without', `
    <thx-container-list
        [title]="'My Test Title'"
        [itemTemplate]="externalTemplate"
        [items]="scope.items">
    </thx-container-list>

    <ng-template #externalTemplate let-item="item">
        <div class="d-flex justify-content-center">
          <div>{{ item?.title }}</div>
        </div>
    </ng-template>
`, {items});

const NullArrayComponent = componentFactory('null', `
    <thx-container-list
        [title]="'My Test Title'"
        [limit]="2"
        [itemTemplate]="externalTemplate"
        [items]="scope.items">
    </thx-container-list>

    <ng-template #externalTemplate let-item="item">
        <div class="d-flex justify-content-center">
          <div>{{ item?.title }}</div>
        </div>
    </ng-template>
`, {});

const EmptyListComponent = componentFactory('empty', `
    <thx-container-list
        [title]="'My Test Title'"
        [limit]="2"
        [itemTemplate]="externalTemplate"
        [emptyTemplate]="emptyTemplate"
        [items]="scope.items">
    </thx-container-list>

    <ng-template #externalTemplate let-item="item">
        <div class="d-flex justify-content-center">
          <div>{{ item?.title }}</div>
        </div>
    </ng-template>

    <ng-template #emptyTemplate>
      empty
    </ng-template>
`, {});

const notes = new Notes().getNotes();
storiesOf('Ui | Organisms/ContainerListComponent', module)
    .addDecorator(
        moduleMetadata({
            imports: [CommonModule],
            declarations: [ContainerListComponent]
        })
    )
    .add('Default', () => ({
            component: DefaultComponent
        }),
        {notes: { markdown: notes}})
    .add(
      'list with scroll',
      () => ({ component: ListWithScrollComponent }),
      {notes: { markdown: notes}}
    )
    .add('Without Limit', () => ({
            component: WithoutLimitComponent
        }),
        {notes: { markdown: notes}})
    .add('Null array', () => ({
            component: NullArrayComponent
        }),
        {notes: { markdown: notes}})
    .add(
      'Empty list',
      () => ({ component: EmptyListComponent }),
      {notes: { markdown: notes}}
    );
