import { ThxFormLoginModule } from './thx-form-login.module';

describe('ThxFormLoginSuperAdminModule', () => {
  let thxFormLoginSuperAdminModule: ThxFormLoginModule;

  beforeEach(() => {
    thxFormLoginSuperAdminModule = new ThxFormLoginModule();
  });

  it('should create an instance', () => {
    expect(thxFormLoginSuperAdminModule).toBeTruthy();
  });
});
