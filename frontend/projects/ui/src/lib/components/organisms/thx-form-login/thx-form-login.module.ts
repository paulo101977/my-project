import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThxRecaptchaModule } from '../../molecules/thx-recaptcha/thx-recaptcha.module';
import { ThxFormLoginComponent } from './thx-form-login.component';
import { ThxInputModule } from '../../atoms/thx-input/thx-input.module';
import { ThxLinkModule } from '../../atoms/thx-link/thx-link.module';
import { ThxButtonModule } from '../../atoms/thx-button/thx-button.module';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';
import { TranslateModule } from '@ngx-translate/core';
import { ThxInputV2Module } from '../../molecules/thx-input-v2/thx-input-v2.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ThxInputModule,
    ThxLinkModule,
    ThxButtonModule,
    ThxRecaptchaModule,
    ThxImgModule,
    TranslateModule,
    ThxInputV2Module,
    ReactiveFormsModule
  ],
  exports: [
    ThxFormLoginComponent,
  ],
  declarations: [
    ThxFormLoginComponent,
  ]
})
export class ThxFormLoginModule { }
