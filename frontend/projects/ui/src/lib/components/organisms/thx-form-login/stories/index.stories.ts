import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { configureRecaptcha } from '../../../molecules/thx-recaptcha/models/recaptcha-global-config';
import { Notes } from './notes';

import { ThxFormLoginModule } from '../thx-form-login.module';

import { DefaultComponent } from './default';


storiesOf('Ui | Organisms/ThxFormLoginSuperAdmin', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule, ThxFormLoginModule ],
    providers: [
      configureRecaptcha({sitekey: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'})
    ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
