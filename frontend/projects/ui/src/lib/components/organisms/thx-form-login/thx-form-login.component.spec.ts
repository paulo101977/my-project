import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxFormLoginComponent } from './thx-form-login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ThxButtonModule } from '../../atoms/thx-button/thx-button.module';
import { ThxLinkModule } from '../../atoms/thx-link/thx-link.module';
import { ThxIconModule } from '../../atoms/thx-icon/thx-icon.module';
import { ThxLabelModule } from '../../atoms/thx-label/thx-label.module';
import { ThxInputModule } from '../../atoms/thx-input/thx-input.module';
import { ThxErrorFormModule } from '../../molecules/thx-error-form/thx-error-form.module';
import { ThxFormGroupModule } from '../../molecules/thx-form-group/thx-form-group.module';
import { NgForm } from '@angular/forms';

describe('ThxFormLoginComponent', () => {
  let component: ThxFormLoginComponent;
  let fixture: ComponentFixture<ThxFormLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxFormLoginComponent ],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        ThxFormGroupModule,
        ThxErrorFormModule,
        ThxInputModule,
        ThxLabelModule,
        ThxIconModule,
        ThxLinkModule,
        ThxButtonModule
      ],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxFormLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create and set init config', () => {
    expect(component).toBeTruthy();
    expect(component.loginForm.value).toEqual({
      email: '',
      password: ''
    });
  });

  it('should call submitForm', () => {
    spyOn(component.submitForm, 'emit');
    const ngFormMock = {
      value: component.loginForm
    };

    component.onSubmit(ngFormMock);

    expect(component.submitForm.emit).toHaveBeenCalledWith({
      email: '',
      password: ''
    });
  });

  it('should call resetPassword', () => {
    spyOn(component.resetedPassword, 'emit');
    const email = 'kraus@kkkk.com';
    component.resetPassword(email);

    expect(component.resetedPassword.emit).toHaveBeenCalledWith(email);
  });
});
