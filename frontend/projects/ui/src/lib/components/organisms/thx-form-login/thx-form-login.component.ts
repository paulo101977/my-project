import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ThxButtonColorType } from '../../atoms/thx-button/enums/thx-button-color-type.enum';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// TODO: implement the form logic
@Component({
  selector: 'thx-form-login',
  templateUrl: './thx-form-login.component.html',
  styleUrls: ['./thx-form-login.component.scss']
})
export class ThxFormLoginComponent implements OnInit {

  constructor(private fb: FormBuilder) {
  }

  @Input() productLogoImg: string;
  @Output() submitForm = new EventEmitter();
  @Output() resetedPassword = new EventEmitter();
  public loginForm: FormGroup;
  public forgotPassword: string;
  public recaptcha: string;
  public buttonColorType: ThxButtonColorType = ThxButtonColorType.Primary;

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  public onSubmit(form) {
    this.submitForm.emit(form.value);
  }

  public resetPassword(email) {
    this.resetedPassword.emit(email);
  }

  public onRecaptchaSuccess(token: string[]) {
    this.recaptcha = token[0];
  }

  public onRecaptchaFail() {
    this.recaptcha = '';
  }
}
