import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';

@Component({
  selector: 'thx-error-msg-max-min',
  templateUrl: 'error-msg-max-min.component.html',
})
export class ErrorMsgMaxMinComponent implements OnInit, OnChanges {
  @Input() value: any;
  @Input() maxValue: any;
  @Input() minValue: any;
  @Output() isValid = new EventEmitter();

  constructor() {}

  ngOnChanges(changes) {
    this.checkIsValid();
  }

  ngOnInit() {
    this.checkIsValid();
  }

  private checkIsValid() {
    if (this.value > this.maxValue && this.value < this.minValue) {
      this.isValid.emit(true);
    }
  }
}
