import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ErrorMsgMaxMinComponent } from './error-msg-max-min.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule.forChild()
  ],
  declarations: [
    ErrorMsgMaxMinComponent
  ],
  exports: [
    ErrorMsgMaxMinComponent,
    FormsModule
  ]
})
export class ErrorMsgMaxMinModule { }
