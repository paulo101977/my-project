import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderTitleComponent } from './header-title.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule.forChild(),
  ],
  declarations: [
    HeaderTitleComponent
  ],
  exports: [
    HeaderTitleComponent
  ]
})
export class HeaderTitleModule { }
