export interface ConfigHeaderPageNew {
  hasBackPreviewPage?: boolean;
  backPreviewPageCallBackFunction?: Function;
  keepTitleButton?: boolean;
  keepButtonActive?: boolean;
  callBackFunction?: Function;
}
