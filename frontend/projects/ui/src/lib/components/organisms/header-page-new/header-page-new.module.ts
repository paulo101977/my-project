import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderPageNewComponent } from './header-page-new.component';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule.forChild(),
    ImgCdnModule
  ],
  declarations: [
    HeaderPageNewComponent
  ],
  exports: [
    HeaderPageNewComponent
  ]
})
export class HeaderPageNewModule { }
