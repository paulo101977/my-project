import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { PieChartItem } from './models/pie-chart-item';
import { PieChartService } from './services/pie-chart.service';

@Component({
  selector: 'thx-pie-chart',
  template: `
    <div>
      <ngx-charts-pie-chart
        [results]="results"
        [doughnut]="true"
        [scheme]="colors"
        (select)="onSelect($event)"
      ></ngx-charts-pie-chart>
    </div>
    <div>
      <div class="pie-chart-legend">
        <thx-legend-item *ngFor="let item of items" [color]="item.color"> {{ item.value + ' ' + item.name }}
        </thx-legend-item>
      </div>
    </div>
  `,
  styleUrls: ['./thx-pie-chart.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ThxPieChartComponent implements OnInit {

  private _items: Array<PieChartItem>;
  @Input('items')
  set items(items: Array<PieChartItem>) {
    this._items = items;
    if (items) {
      this.results = this.chartService.extractResultFromList(items);
      this.colors = {domain: this.chartService.extractColorsFromList(items)};
    }
  }

  get items(): Array<PieChartItem> {
    return this._items;
  }

  @Output() chartItemSelected = new EventEmitter();

  public results: Array<any>;
  public colors: any;

  constructor(private chartService: PieChartService) {}

  ngOnInit(): void {}

  onSelect(event) {
    this.chartItemSelected.emit(event);
  }

}
