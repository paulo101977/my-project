import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxPieChartComponent } from './thx-pie-chart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ThxLegendItemModule } from '../../molecules/thx-legend-item/thx-legend-item.module';


@NgModule({
  imports: [
    CommonModule, NgxChartsModule, ThxLegendItemModule],
  declarations: [ThxPieChartComponent],
  exports: [ThxPieChartComponent]
})
export class ThxPieChartModule { }
