export class PieChartItem {
  name: string;
  value: number;
  color: string;
}
