import { Injectable } from '@angular/core';
import { PieChartItem } from '../models/pie-chart-item';

@Injectable({
  providedIn: 'root'
})
export class PieChartService {

  constructor() { }

  public extractResultFromList(items: Array<PieChartItem>): Array<any> {
    return items ? items.map( item => ({'name': item.name, 'value': item.value})) : [];
  }

  public extractColorsFromList(items: Array<PieChartItem>): Array<any> {
    return items ? items.map( item => item.color.toLocaleUpperCase()) : [];
  }
}

