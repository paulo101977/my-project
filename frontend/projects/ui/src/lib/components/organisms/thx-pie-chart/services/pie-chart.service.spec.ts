import { TestBed, inject } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { PieChartService } from './pie-chart.service';

describe('PieChartService', () => {
  let items;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [PieChartService]
    });
    items = [
      {
        name: 'Item 1',
        value: 10,
        color: '#5AA454'
      },
      {
        name: 'Item 2',
        value: 30,
        color: '#c1c2c3'
      },
      {
        name: 'Item 3',
        value: 1,
        color: '#a1a2a3'
      },
    ];
  });

  it('should be created', inject([PieChartService], (service: PieChartService) => {
    expect(service).toBeTruthy();
  }));

  it('should extract result from list', inject([PieChartService], (service: PieChartService) =>  {
    const result = service.extractResultFromList(items);

    expect(result.length).toEqual(3);
    expect(result[0]).toEqual({ name: 'Item 1', value: 10 } );
  }));

  it('should extract colors from list', inject([PieChartService], (service: PieChartService) =>  {
    const result = service.extractColorsFromList(items);

    expect(result.length).toEqual(3);
    expect(result[0]).toEqual({ color: '#5AA454' } );
  }));

});
