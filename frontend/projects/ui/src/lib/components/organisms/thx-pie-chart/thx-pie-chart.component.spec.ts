import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ThxPieChartComponent } from './thx-pie-chart.component';

describe('PieChartComponent', () => {
  let component: ThxPieChartComponent;
  let fixture: ComponentFixture<ThxPieChartComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxPieChartComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxPieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
