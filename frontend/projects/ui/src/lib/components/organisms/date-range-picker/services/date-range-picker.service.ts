import { Injectable } from '@angular/core';
import { IMyDate, IMyDrpOptions } from 'mydaterangepicker';

@Injectable({
  providedIn: 'root'
})
export class DateRangePickerService {
    constructor() {}

    public getClearConfigDateRangePicker( disableUntil?: IMyDate, disableSince?: IMyDate): IMyDrpOptions {
        const obj = {
          dateFormat: 'dd/mm/yyyy',
          firstDayOfWeek: 'mo',
          sunHighlight: false,
          markCurrentDay: true,
          height: '44px',
          width: '100%',
          inline: false,
          showClearBtn: false,
          showApplyBtn: false,
          showSelectDateText: false,
          alignSelectorRight: false,
          indicateInvalidDateRange: true,
          showClearDateRangeBtn: false,
          editableDateRangeField: false,
          openSelectorOnInputClick: true,
          showSelectorArrow: false,
        };

        if (disableUntil) {
          obj['disableUntil'] = disableUntil;
        }
        if (disableSince) {
          obj['disableSince'] = disableSince;
        }
        return obj;
      }
}
