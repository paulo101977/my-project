import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateRangePickerComponent } from './date-range-picker.component';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { ErrorMsgMaxMinModule } from '../input-error-msg/error-msg-max-min.module';
import { MyDateRangePickerModule } from 'mydaterangepicker';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule.forChild(),
    NgxDatatableModule,
    CurrencyMaskModule,
    ErrorMsgMaxMinModule,
    MyDateRangePickerModule
  ],
  declarations: [
    DateRangePickerComponent,
  ],
  exports: [
    DateRangePickerComponent,
    MyDateRangePickerModule,
    FormsModule
  ]
})
export class DateRangePickerModule { }
