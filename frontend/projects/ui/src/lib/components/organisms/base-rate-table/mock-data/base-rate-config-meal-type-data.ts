import { BaseRateConfigurationMealPlanType } from '../models/revenue-management/meal-type-fix-rate';

export const baseRateMealTypeData1: BaseRateConfigurationMealPlanType = {
  mealPlanTypeId: 1,
  mealPlanTypeName: 'A',
  adultMealPlanAmount: 150.00,
  child1MealPlanAmount: 100.00,
  child2MealPlanAmount: 100.00,
  child3MealPlanAmount: 100.00,
};

export const baseRateMealTypeData2: BaseRateConfigurationMealPlanType = {
  mealPlanTypeId: 2,
  mealPlanTypeName: 'B',
  adultMealPlanAmount: 250.00,
  child1MealPlanAmount: 200.00,
  child2MealPlanAmount: 200.00,
  child3MealPlanAmount: 200.00,
};

export const baseRateMealTypeData3: BaseRateConfigurationMealPlanType = {
  mealPlanTypeId: 3,
  mealPlanTypeName: 'C',
  adultMealPlanAmount: null,
  child1MealPlanAmount: null,
  child2MealPlanAmount: null,
  child3MealPlanAmount: null,
};

export const baseRateMealTypeListData: BaseRateConfigurationMealPlanType[] = [
  baseRateMealTypeData1, baseRateMealTypeData2, baseRateMealTypeData3
];
