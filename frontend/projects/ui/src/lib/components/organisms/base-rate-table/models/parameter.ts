export enum ParameterGroupType {
  GroupChildren = 3,
}

export enum ParameterType {
  Time = 'Time',
  Date = 'Date',
  Number = 'Number',
  Default = 'Text',
  Select = 'ComboBox',
  CheckBox = 'CheckBox',
}

export class Parameter {
  propertyParameterValue: string;
  id: string;
  applicationParameterId: number;
  propertyId: number;
  parameterDescription: string;
  // required: boolean; removido temporáriamente
  parameterTypeName: string;
  propertyParameterMinValue: string;
  propertyParameterMaxValue: string;
  propertyParameterPossibleValues: string;
  isActive: boolean;
  canInactive: boolean;
  featureGroupId: number;
}

export class ParameterOption {
  title: string;
  value: string;

  constructor(title?: string, value?: string) {
    this.title = title;
    this.value = value;
  }
}

export class ParameterGroup {
  featureGroupId: number;
  featureGroupName: string;
  parameters: Array<Parameter>;
}

export class ParameterReturn {
  propertyParameters: Array<ParameterGroup>;
}
