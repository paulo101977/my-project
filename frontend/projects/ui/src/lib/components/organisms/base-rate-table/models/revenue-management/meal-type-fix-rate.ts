export class BaseRateConfigurationMealPlanType {
  mealPlanTypeId: number;
  mealPlanTypeName: string;
  adultMealPlanAmount: number;
  child1MealPlanAmount: number;
  child2MealPlanAmount: number;
  child3MealPlanAmount: number;
}
