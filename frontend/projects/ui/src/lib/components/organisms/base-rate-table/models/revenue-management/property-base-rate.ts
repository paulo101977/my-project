// Models
import { DaysEnum } from '../days.enum';
import { BaseRateConfigurationMealPlanType } from './meal-type-fix-rate';

export class PropertyRate {
  mealPlanTypeDefaultId: number;
  ratePlanId: number;
  currencyId: string;
  startDate: Date;
  finalDate: Date;
  baseRateConfigurationRoomTypeList: Array<BaseRateTable>;
  baseRateConfigurationMealPlanTypeList: Array<BaseRateConfigurationMealPlanType>;
  daysOfWeekDto: Array<DaysEnum>;
  propertyId: number;
  id: number;
}

export class BaseRateTable {
  name: string;
  roomTypeId: number;
  pax1Amount: number;
  pax2Amount: number;
  pax3Amount: number;
  pax4Amount: number;
  pax5Amount: number;
  paxAdditionalAmount: number;
  child1Amount: number;
  child2Amount: number;
  child3Amount: number;
}
