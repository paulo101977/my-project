import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import * as _ from 'lodash';
import { ButtonConfig, ButtonSize, ButtonType, Type } from './../../models/button-config';
import { KeyValue } from '../../models/key-value';

@Injectable({ providedIn: 'root' })
export class SharedService {
  private headers = new Headers({ 'Content-Type': 'application/json' });

  public getHeaders(): any {
    return this.headers;
  }

  public handleError(error: any): Promise<any> {
    return Promise.reject(error);
  }

  public objectIsNullOrUndefined(object: any): boolean {
    return object == null || object == undefined;
  }

  public createRange(range: number): Array<number> {
    const items = new Array<number>();
    for (let i = 1; i <= range; i++) {
      items.push(i);
    }
    return items;
  }

  public elementExistsInList(obj: any, list: Array<any>, callbackFunction = null): boolean {
    if (list) {
      if (callbackFunction) {
        return list.findIndex(callbackFunction) > -1;
      } else {
        return list.indexOf(obj) > -1;
      }
    } else {
      return false;
    }
  }

  public hasMoreOfOneElementInList(list: Array<any>, index: number, callbackFunction): boolean {
    let hasElementRepeated = false;
    const listForComparation = [...list];
    this.removeElementThatWillNotBeComparedOfList(listForComparation, index);
    listForComparation.forEach((element, i) => {
      if (this.elementExistsInList(null, listForComparation, callbackFunction)) {
        hasElementRepeated = true;
      }
    });
    return hasElementRepeated;
  }

  private removeElementThatWillNotBeComparedOfList(list: Array<any>, index: number): void {
    list.splice(index, 1);
  }

  public getIndexInList(obj: any, list: Array<any>, callbackFunction = null): number {
    if (callbackFunction) {
      return list.findIndex(callbackFunction);
    } else {
      return list.indexOf(obj);
    }
  }

  public getElementInList(obj: any, list: Array<any>, callbackFunction = null): any {
    if (callbackFunction) {
      return list.find(callbackFunction);
    } else {
      return list.find(obj);
    }
  }

  public removeElementFromList(obj: any, list: Array<any>): void {
    const index: number = list.indexOf(obj);
    if (index !== -1) {
      list.splice(index, 1);
    }
  }

  public getValuesEnum(enumType: any) {
    const options: string[] = Object.keys(enumType);
    return options.slice(options.length / 2);
  }

  public tranlasteEnumToView(enumType: any, tranlastePath: string) {
    const values = this.getValuesEnum(enumType);
    values.forEach((value, index) => {
      values[index] = tranlastePath + '.' + value;
    });
    return values;
  }

  public getButtonConfig(
    id: string,
    callback: Function,
    textButton: string,
    buttonType: ButtonType,
    buttonSize?: ButtonSize,
    isDisabled?: boolean,
    type?: Type
  ) {
    const buttonConfig = new ButtonConfig();
    buttonConfig.id = id;
    buttonConfig.callback = callback;
    buttonConfig.textButton = textButton;
    buttonConfig.buttonType = buttonType;

    if (buttonSize) {
      buttonConfig.buttonSize = buttonSize;
    }

    if (isDisabled) {
      buttonConfig.isDisabled = isDisabled;
    }

    if ( type ) {
      buttonConfig.type = type;
    }

    buttonConfig.extraClass = 'thf-u-no-padding';

    return buttonConfig;
  }

  public padLeftZero(num: number, size: number): string {
    let s = num + '';
    while (s.length <= size) {
      s = '0' + s;
    }
    return s;
  }

  public getValueWithPercentage(value: number, percentage: number): number {
    return value + value * (percentage / 100);
  }

  public getRandomText(size) {
    const strings = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    let random = '';
    for (let i = 0; i < size; i++) {
      const rnum = Math.floor(Math.random() * strings.length);
      random += strings.substring(rnum, rnum + 1);
    }
    return random;
  }

  public getKeyValue(value: any) {
    const objToItemTab = new KeyValue();
    objToItemTab.value = value;
    objToItemTab.key = this.getRandomText(7);

    return objToItemTab;
  }

  public containsObjectInList(obj: any, list: Array<any>, index: number) {
    for (let i = 0; i < list.length; i++) {
      if (list[i] === obj) {
        return true;
      }
    }

    return false;
  }

  public windowPrint = () => {
    window.print();
  }

  public isEmptyOrWhiteSpace(text: string): boolean {
    return text ? text.trim() == '' : true;
  }

  public getValueWithDiscount(value: number, discount: number): number {
    if (isNaN(value) || isNaN(discount) || !_.isNumber(value) || !_.isNumber(discount) || value <= 0 || discount <= 0) {
      return value;
    } else if (discount > 100) {
      return 0;
    } else {
      return _.floor((value - (value * (discount / 100))), 2);
    }
  }

  public getDiscountPercentage(manualValue: number, baseValue: number): number {
    if (isNaN(manualValue) || isNaN(baseValue) || !_.isNumber(manualValue) || !_.isNumber(baseValue) || manualValue < 0 || baseValue < 0) {
      return 0;
    } else if (manualValue == 0) {
      return 100;
    } else {
      return _.floor(100 - (manualValue * 100) / baseValue, 2);
    }
  }

  public assignObjectWithMethods(target, source) {
    Object.defineProperties(
      target,
      Object.keys(source).reduce((descriptors, key) => {
        descriptors[key] = Object.getOwnPropertyDescriptor(source, key);
        return descriptors;
      }, {}),
    );
    Object.setPrototypeOf(target, Object.create(Object.getPrototypeOf(source)));
    return target;
  }
}
