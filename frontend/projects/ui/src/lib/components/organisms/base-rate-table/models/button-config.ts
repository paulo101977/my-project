export class ButtonConfig {
  callback: Function;
  buttonType: ButtonType;
  buttonSize?: ButtonSize;
  textButton: string;
  textComplementButton: string;
  id: string;
  isDisabled: boolean;
  extraClass: string;
  width: string;
  isActive: boolean;
  type: Type;
}

// TODO: add more types if necessary
export enum Type {
  Submit = 'submit',
  Button = 'button'
}

export enum ButtonType {
  Primary = 1,
  Secondary = 2,
  Widget = 3,
  None = 4,
  Danger = 5,
  Outline = 6,
}

export enum ButtonSize {
  Normal = 1,
  Small = 2,
  Full = 3,
}
