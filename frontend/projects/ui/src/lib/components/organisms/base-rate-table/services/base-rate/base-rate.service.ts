import { Injectable } from '@angular/core';
import { RoomTypeParametersConfiguration } from '../../models/revenue-management/room-type-parameters-configuration';
import { BaseRateTable } from '../../models/revenue-management/property-base-rate';
import { BaseRateConfigurationMealPlanType } from '../../models/revenue-management/meal-type-fix-rate';

@Injectable({ providedIn: 'root' })
export class BaseRateService {
  constructor() {}

  public hasBaseRateConfigWithValue(baseRateList: Array<RoomTypeParametersConfiguration>) {
    if (baseRateList) {
      return baseRateList.filter(this.hasBaseRateValueInputed);
    }

    return false;
  }

  private hasBaseRateValueInputed = baseRate => {
    return (
      baseRate.paxOne ||
      baseRate.paxTwo ||
      baseRate.paxThree ||
      baseRate.paxFour ||
      baseRate.paxFive ||
      baseRate.paxAdditional ||
      baseRate.child1Amount ||
      baseRate.child2Amount ||
      baseRate.child3Amount
    );
  }

  public getBaseRateValueListIsValid(baseRateList: Array<BaseRateTable>) {
    let baseRateValueValidList: boolean[] = [];
    if (baseRateList) {
      baseRateValueValidList = baseRateList.map(baseRate => this.checkBaseRateValues(baseRate));
    }
    return baseRateValueValidList.some(this.baseRateListHasValidValue);
  }

  public baseRateListHasValidValue(isValid: boolean) {
    return isValid;
  }

  public checkBaseRateValues(baseRate: BaseRateTable) {
    const hasValue =
      baseRate.pax1Amount +
      baseRate.pax2Amount +
      baseRate.pax3Amount +
      baseRate.pax4Amount +
      baseRate.pax5Amount +
      baseRate.paxAdditionalAmount;
    return hasValue > 0;
  }

  public checkMealTypeValues(mealPlanTypeList: BaseRateConfigurationMealPlanType[]): boolean {
    return mealPlanTypeList.some(mP => {
      return mP.adultMealPlanAmount != null && +mP.adultMealPlanAmount >= 0.00;
    });
  }
}
