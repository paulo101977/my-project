import { Injectable } from '@angular/core';
import { RoomTypeParametersConfiguration } from '../../models/revenue-management/room-type-parameters-configuration';
import { BaseRateTable } from '../../models/revenue-management/property-base-rate';

@Injectable({ providedIn: 'root' })
export class BaseRateMapper {
  public getBaseRateTableListModel(roomTypeWithBaseRateList: Array<RoomTypeParametersConfiguration>): Array<BaseRateTable> {
    const baseRateTableList = new Array<BaseRateTable>();

    roomTypeWithBaseRateList.forEach(roomTypeWithBaseRate => {
      const baseRateTable = new BaseRateTable();
      baseRateTable.name = roomTypeWithBaseRate.name;
      baseRateTable.roomTypeId = roomTypeWithBaseRate.id;
      baseRateTable.pax1Amount = roomTypeWithBaseRate.paxOne ? +roomTypeWithBaseRate.paxOne : null;
      baseRateTable.pax2Amount = roomTypeWithBaseRate.paxTwo ? +roomTypeWithBaseRate.paxTwo : null;
      baseRateTable.pax3Amount = roomTypeWithBaseRate.paxThree ? +roomTypeWithBaseRate.paxThree : null;
      baseRateTable.pax4Amount = roomTypeWithBaseRate.paxFour ? +roomTypeWithBaseRate.paxFour : null;
      baseRateTable.pax5Amount = roomTypeWithBaseRate.paxFive ? +roomTypeWithBaseRate.paxFive : null;
      baseRateTable.paxAdditionalAmount = roomTypeWithBaseRate.paxAdditional ? +roomTypeWithBaseRate.paxAdditional : null;
      baseRateTable.child1Amount = roomTypeWithBaseRate.child1Amount ? +roomTypeWithBaseRate.child1Amount : null;
      baseRateTable.child2Amount = roomTypeWithBaseRate.child2Amount ? +roomTypeWithBaseRate.child2Amount : null;
      baseRateTable.child3Amount = roomTypeWithBaseRate.child3Amount ? +roomTypeWithBaseRate.child3Amount : null;

      baseRateTableList.push(baseRateTable);
    });

    return baseRateTableList;
  }
}
