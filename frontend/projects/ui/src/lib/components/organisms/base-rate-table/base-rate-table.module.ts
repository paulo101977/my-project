import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseRateTableComponent } from './base-rate-table.component';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';
import { CustomCurrencyMaskConfig } from './models/currency-config';
import { ErrorMsgMaxMinModule } from '../input-error-msg/error-msg-max-min.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule.forChild(),
    NgxDatatableModule,
    CurrencyMaskModule,
    ErrorMsgMaxMinModule
  ],
  declarations: [
    BaseRateTableComponent
   ],
  exports: [
    BaseRateTableComponent,
    FormsModule
  ],
  providers: [
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
  ],
})
export class BaseRateTableModule { }
