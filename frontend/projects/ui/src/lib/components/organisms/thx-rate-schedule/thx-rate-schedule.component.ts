import {
  ApplicationRef,
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ElementRef,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  Renderer2,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ThxPopoverComponent } from '../thx-popover/thx-popover.component';
import { DayPilot, DayPilotMonthComponent } from 'daypilot-pro-angular';
import { ThxRateScheduleCellComponent } from './thx-rate-schedule-cell/thx-rate-schedule-cell.component';
import { ThxRateScheduleCellHeaderComponent } from './thx-rate-schedule-cell-header/thx-rate-schedule-cell-header.component';
import * as momentNs from 'moment';
import { RateScheduleLabels, PopoverOption, RateScheduleCell } from './thx-rate-schedule.model';
import { Placement } from 'popper.js';

const moment = momentNs;

@Component({
  selector: 'thx-rate-schedule',
  templateUrl: './thx-rate-schedule.component.html',
  styleUrls: ['./thx-rate-schedule.component.scss'],
})
export class ThxRateScheduleComponent implements OnInit, OnChanges {
  @Input() public dayList: RateScheduleCell[];

  private _initialDate: DayPilot.Date;

  @Input() public popoverOptionList: PopoverOption[];

  @Input() public labels: RateScheduleLabels;

  @Output() public initialDateChange: EventEmitter<DayPilot.Date> = new EventEmitter();

  @ViewChild('templateRef') public templateRef: TemplateRef<any>;

  @ViewChild('calendar') public calendar: DayPilotMonthComponent;

  @ViewChild('popoverPipelineInfo') public popoverPipelineInfo: TemplateRef<any>;

  @ViewChild('popoverTimeRangeOptions') public popoverTimeRangeOptions: TemplateRef<any>;

  public config: any;

  public selectedRange: { start: DayPilot.Date; end: DayPilot.Date };

  private cellHeaderFactory: ComponentFactory<ThxRateScheduleCellHeaderComponent>;
  private cellContentFactory: ComponentFactory<ThxRateScheduleCellComponent>;

  private popoverContainer: ThxPopoverComponent;
  private isPopoverShown: boolean;
  private globalPopoverClick: Function;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private viewContainerRef: ViewContainerRef,
    private appRef: ApplicationRef,
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private injector: Injector,
  ) {}

  get initialDate(): DayPilot.Date {
    return this._initialDate;
  }

  @Input()
  set initialDate(value: DayPilot.Date) {
    this._initialDate = value;
    if (this.config) {
      this.config.startDate = value;
    }
  }

  ngOnInit() {
    this.setConfig();
    this.setFactories();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.calendar && this.calendar.control && this.calendar.control.update();
  }

  private setConfig() {
    this.config = {
      eventMoveHandling: 'Disabled',
      eventResizeHandling: 'Disabled',
      headerHeight: 50,
      cellHeight: 113,
      startDate: this.initialDate || new DayPilot.Date(),
      weekStarts: 1,
      viewType: 'Weeks',
      weeks: 4,
      onBeforeHeaderRender: this.onBeforeHeaderRender,
      onBeforeCellRender: this.onBeforeCellRender,
      onBeforeEventRender: this.onBeforeEventRender,
      onEventClick: this.onEventClick,
      onTimeRangeSelected: this.onTimeRangeSelected,
      onAfterRender: this.onAfterRender,
    };
  }

  private setFactories() {
    this.cellContentFactory = this.componentFactoryResolver.resolveComponentFactory(ThxRateScheduleCellComponent);
    this.cellHeaderFactory = this.componentFactoryResolver.resolveComponentFactory(ThxRateScheduleCellHeaderComponent);
  }

  private onBeforeCellRender = (args: any) => {
    const cellHeader = this.cellHeaderFactory.create(this.injector);
    const cellContent = this.cellContentFactory.create(this.injector);

    cellHeader.instance.date = args.cell.start;
    cellHeader.instance.labels = this.labels;

    cellContent.instance.date = args.cell.start;
    cellContent.instance.labels = this.labels;

    if (this.dayList) {
      const day = this.dayList && this.dayList.find(cell => args.cell.start == cell.date);

      if (day) {
        cellHeader.instance.occupation = day.occupation;

        cellContent.instance.oldValue = day.oldValue;
        cellContent.instance.currentValue = day.currentValue;
        cellContent.instance.oldValueString = day.oldValueString;
        cellContent.instance.currentValueString = day.currentValueString;
        cellContent.instance.levelCode = day.levelCode;
        cellContent.instance.isLevel = day.isLevel;
      }
    }

    cellHeader.changeDetectorRef.detectChanges();
    cellContent.changeDetectorRef.detectChanges();

    const dayOfWeek = args.cell.start.getDayOfWeek();
    args.cell.cssClass = dayOfWeek == 0 || dayOfWeek == 6 ? ' weekend' : '';
    args.cell.cssClass += args.cell.start < new DayPilot.Date().getDatePart() ? ' disabled' : '';

    args.cell.headerHtml = cellHeader.location.nativeElement.innerHTML;
    args.cell.html = cellContent.location.nativeElement.innerHTML;
  }

  private onBeforeEventRender = (args: any) => {
    args.e.html = `<span class="pipeline-${args.data.id}">${args.data.text}</span>`;
  }

  private onBeforeHeaderRender = (args: any) => {
    args.header.html = moment()
      .day(args.header.dayOfWeek)
      .format('ddd');
  }

  private onEventClick = (args: any) => {
    const hElement = args.div.querySelector('.pipeline-' + args.e.data.id);
    this.openPopover(hElement, this.popoverPipelineInfo, 'right');
  }

  private onTimeRangeSelected = (args: any) => {
    this.selectedRange = {
      start: args.start,
      end: args.end,
    };

    const today = new DayPilot.Date().getDatePart();
    if (this.selectedRange.start < today || this.selectedRange.end < today) {
      this.calendar.control.clearSelection();
      this.hidePopover(); // Hide any open popover
      return;
    }

    const hElement = <HTMLElement>document.querySelector('.month_default_shadow:last-of-type');
    this.openPopover(hElement, this.popoverTimeRangeOptions, 'bottom-end');
  }

  private onAfterRender = (args: any) => {
    // ------------ SPOILER ALERT: This gambiarra is super necessary -------------
    // Replaces DayPilot Month _onCellMouseDown function and calls _onMainMouseMove after execute the original function.
    // This is necessary to display the selection shadow. Popover uses it as reference to positioning
    // When upgrading daypilot, find these functions using the .debug.js file
    const calendar = <any>this.calendar;

    if (!calendar.control.obOriginal) {
      calendar.control.obOriginal = calendar.control.ob;
      calendar.control.ob = function(e) {
        calendar.control.obOriginal.call(this, e);
        calendar.control.tc.call(this, e);
      };
      calendar.control.update();
    }
  }

  public runPopoverOptionCallback(popoverOption: PopoverOption, selectedRange: any) {
    if (popoverOption.multipleCells || selectedRange.start === selectedRange.end.addDays(-1)) {
      this.hidePopover();
      this.calendar.control.clearSelection();
      popoverOption.callback(selectedRange.start, selectedRange.end);
    }
  }

  public goNext($event: any) {
    if ($event) {
      $event.preventDefault();
    }

    const startDate = <DayPilot.Date>this.calendar.control.startDate;
    this.navigate(startDate.addDays(28));
  }

  public goPrevious($event: any) {
    $event && $event.preventDefault();
    const startDate = <DayPilot.Date>this.calendar.control.startDate;
    this.navigate(startDate.addDays(-28));
  }

  public goToday() {
    this.navigate(new DayPilot.Date());
  }

  private navigate(startDate: DayPilot.Date) {
    this.initialDate = startDate;
    this.initialDateChange.emit(startDate);
    this.isPopoverShown && this.hidePopover();
    this.calendar.control.clearSelection();
  }

  /*
   * Popover methods
   * I couldn't apply the directive directly into daypilot items because it only returns static html
   * **************************************************************************************************/
  public openPopover(referenceObject: HTMLElement, template: TemplateRef<any>, placement: Placement) {
    if (!this.popoverContainer) {
      this.popoverContainer = ThxPopoverComponent.create(
        this.componentFactoryResolver,
        this.appRef,
        this.viewContainerRef,
        template,
        referenceObject,
      );
    }

    this.popoverContainer.referenceObject = referenceObject;
    this.popoverContainer.popperOptions = { placement: placement };

    this.showPopover();
  }

  public onClickOutside(event) {
    if (this.isPopoverShown && !this.popoverContainer.popoverContent.nativeElement.parentElement.children[0].contains(event.target)) {
      this.hidePopover();
      this.calendar.control.clearSelection();
    }
  }

  public hidePopover() {
    if (this.popoverContainer) {
      this.popoverContainer.hide();
      this.isPopoverShown = false;
      this.globalPopoverClick();
    }
  }

  public showPopover() {
    this.popoverContainer.show();
    this.isPopoverShown = true;
    this.globalPopoverClick && this.globalPopoverClick();
    this.globalPopoverClick = this.renderer.listen('document', 'mousedown', this.onClickOutside.bind(this));
  }
}
