import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxRateScheduleComponent } from './thx-rate-schedule.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ThxRateScheduleCellHeaderComponent } from './thx-rate-schedule-cell-header/thx-rate-schedule-cell-header.component';
import { ThxRateScheduleCellComponent } from './thx-rate-schedule-cell/thx-rate-schedule-cell.component';
import { ThxRateScheduleModule } from './thx-rate-schedule.module';

describe('ThxRateScheduleComponent', () => {
  let component: ThxRateScheduleComponent;
  let fixture: ComponentFixture<ThxRateScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ThxRateScheduleModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxRateScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
