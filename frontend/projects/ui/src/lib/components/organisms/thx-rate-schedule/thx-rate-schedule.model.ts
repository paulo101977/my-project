import { DayPilot } from 'daypilot-pro-angular';

export class RateScheduleCell {
  date: string;
  oldValue: number;
  oldValueString?: string;
  currentValue: number;
  currentValueString?: string;
  occupation: number;
  levelCode: string;
  isLevel: boolean;
}

export class PopoverOption {
  label: string;
  callback?: (start: DayPilot.Date, end: DayPilot.Date) => void;
  multipleCells?: boolean;
}

export class RateScheduleLabels {
  noRate: string;
  overbook: string;
  occupation: string;
  today: string;
}
