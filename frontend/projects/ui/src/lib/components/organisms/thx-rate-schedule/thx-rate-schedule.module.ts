import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxRateScheduleComponent } from './thx-rate-schedule.component';
import { DayPilotModule } from 'daypilot-pro-angular';
import { MomentModule } from 'ngx-moment';
import { ThxRateScheduleCellComponent } from './thx-rate-schedule-cell/thx-rate-schedule-cell.component';
import { ThxRateScheduleCellHeaderComponent } from './thx-rate-schedule-cell-header/thx-rate-schedule-cell-header.component';
import { ThxPopoverModule } from '../thx-popover/thx-popover.module';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';

@NgModule({
  imports: [
    CommonModule,
    DayPilotModule,
    ThxPopoverModule,
    MomentModule,
    ImgCdnModule
  ],
  declarations: [
    ThxRateScheduleComponent,
    ThxRateScheduleCellComponent,
    ThxRateScheduleCellHeaderComponent
  ],
  exports: [
    ThxRateScheduleComponent,
    ThxRateScheduleCellComponent,
    ThxRateScheduleCellHeaderComponent
  ],
  entryComponents: [
    ThxRateScheduleCellComponent,
    ThxRateScheduleCellHeaderComponent
  ],
})
export class ThxRateScheduleModule {}
