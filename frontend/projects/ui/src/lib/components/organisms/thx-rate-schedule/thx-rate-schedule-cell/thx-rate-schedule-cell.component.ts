import { Component, OnInit } from '@angular/core';
import { DayPilot } from 'daypilot-pro-angular';
import { RateScheduleLabels } from '../thx-rate-schedule.model';

@Component({
  selector: 'thx-rate-schedule-cell',
  templateUrl: './thx-rate-schedule-cell.component.html',
  styleUrls: ['./thx-rate-schedule-cell.component.scss'],
})
export class ThxRateScheduleCellComponent implements OnInit {
  labels: RateScheduleLabels;

  oldValue: number;
  currentValue: number;

  oldValueString: string;
  currentValueString: string;
  levelCode: string;
  isLevel: boolean;

  date: DayPilot.Date;

  constructor() {
  }

  public isToday() {
    return this.date && this.date.getDatePart().getTime() === new DayPilot.Date().getDatePart().getTime();
  }

  ngOnInit(): void {
    if (this.levelCode) {
      this.levelCode = this.formatLevelCode(this.levelCode);
    }
  }

  private formatLevelCode(levelCode: string) {
    return levelCode.length > 10
      ? levelCode.substring(0, 10) + '...'
      : levelCode;
  }
}
