import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxRateScheduleCellHeaderComponent } from './thx-rate-schedule-cell-header.component';

describe('ThxRateScheduleCellHeaderComponent', () => {
  let component: ThxRateScheduleCellHeaderComponent;
  let fixture: ComponentFixture<ThxRateScheduleCellHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThxRateScheduleCellHeaderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxRateScheduleCellHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
