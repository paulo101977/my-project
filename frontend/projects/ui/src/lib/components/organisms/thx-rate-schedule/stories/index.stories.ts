import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';

import { ThxRateScheduleModule } from '../thx-rate-schedule.module';
import { ThxPopoverModule } from '../../thx-popover/thx-popover.module';

import { IndexComponent } from './index.component';

import { Notes } from './notes';


storiesOf('Ui | Organisms/ThxRateSchedule', module)
  .addDecorator(
    moduleMetadata({
      imports: [ CommonModule, ThxRateScheduleModule , ThxPopoverModule ],
      schemas: [],
      declarations: [],
      providers: []
    })
  )
  .add('Default', () => ({
    component: IndexComponent,
    props: {},
  }),
      { notes: { markdown: new Notes().getNotes() }}
);
