import {
    Component, OnInit
} from '@angular/core';


import { ThxRateScheduleComponent } from '../thx-rate-schedule.component';

import { RateScheduleLabels, RateScheduleCell, PopoverOption } from '../thx-rate-schedule.model';
import { DayPilot } from 'daypilot-pro-angular';

@Component({
  selector: 'thx-index-cp',
  templateUrl: './index.component.html',
  entryComponents: [ThxRateScheduleComponent]
})
export class IndexComponent implements OnInit {

    public rateScheduleLabels: RateScheduleLabels;
    public popoverOptionsList: PopoverOption[] = [];
    public dayList: RateScheduleCell[] = [];

    ngOnInit(): void {

        this.popoverOptionsList.push(
            <PopoverOption>{
                label: 'Test',
                callback: (start, end) => { console.log(start, end); },
                multipleCells: true
            }
        );

        this.rateScheduleLabels = <RateScheduleLabels>{
            noRate: '1',
            overbook: '2',
            occupation: '3',
            today: '4'
        };

        for (let i = 0; i < 30; i++) {
            this.dayList.push(<RateScheduleCell>{
                date: DayPilot.Date.today().firstDayOfMonth().toString(),
                oldValue: 1,
                oldValueString: '1',
                currentValue: 1,
                currentValueString: '1',
                occupation: 1
            });
        }

    }

    onInitialDateChange(event: any): void {
        console.log(event);
    }
}
