import { Component } from '@angular/core';
import { DayPilot } from 'daypilot-pro-angular';
import * as momentNs from 'moment';
import { RateScheduleLabels } from '../thx-rate-schedule.model';

const moment = momentNs;

@Component({
  selector: 'thx-rate-schedule-cell-header',
  templateUrl: './thx-rate-schedule-cell-header.component.html',
  styleUrls: ['./thx-rate-schedule-cell-header.component.scss'],
})
export class ThxRateScheduleCellHeaderComponent {
  private _date: DayPilot.Date;
  public isToday: boolean;
  public isFirstDay: boolean;
  public month: string;
  public labels: RateScheduleLabels;
  public occupation: number;

  constructor() {}

  get date(): DayPilot.Date {
    return this._date;
  }

  set date(value: DayPilot.Date) {
    this._date = value;

    this.setIsToday();
    this.setIsFirstDay();
    this.setMonth();
  }

  private setIsToday() {
    this.isToday = this.date ? this.date.getDatePart().getTime() === new DayPilot.Date().getDatePart().getTime() : null;
  }

  private setIsFirstDay() {
    this.isFirstDay = this.date ? this.date.getDatePart().getDay() === 1 : null;
  }

  private setMonth() {
    this.month = this.date
      ? moment()
          .month(this.date.getMonth())
          .format('MMMM')
      : null;
  }
}
