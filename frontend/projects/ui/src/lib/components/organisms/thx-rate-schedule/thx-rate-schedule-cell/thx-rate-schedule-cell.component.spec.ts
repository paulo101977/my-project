import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { imgCdnStub } from '../../../../../../../../testing';

import { ThxRateScheduleCellComponent } from './thx-rate-schedule-cell.component';

describe('ThxRateScheduleCellComponent', () => {
  let component: ThxRateScheduleCellComponent;
  let fixture: ComponentFixture<ThxRateScheduleCellComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ThxRateScheduleCellComponent, imgCdnStub],
    });

    fixture = TestBed.createComponent(ThxRateScheduleCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
