import { Component, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { ThxSubtittleV2SizesEnum } from '../../../atoms/thx-subtitle-v2/models/thx-subtittle-v2-sizes-enum';


@Component({
  selector: 'thx-box-base',
  templateUrl: './thx-box-base.component.html',
  styleUrls: ['./thx-box-base.component.scss']
})
export class ThxBoxBaseComponent implements OnInit {
  @Input() subtitle: string;
  @Input() headerFontSize =  ThxSubtittleV2SizesEnum.DefaultSize;

  constructor(
      private el: ElementRef,
      private renderer: Renderer2
  ) {}

  ngOnInit(): void {
    // alternative to ::ng-deep
    const subContentInfoDiv = this.el.nativeElement.querySelector('.sub-content-info-div');
    if ( subContentInfoDiv ) {
      // subContentInfoDiv.style.paddingTop = '0'; it is other alternative to renderer2
      this.renderer.setStyle(subContentInfoDiv, 'padding-top', '0');
    }
  }
}
