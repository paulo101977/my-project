import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { componentFactory } from '.storybook/component-factory';
import { ThxBoxBaseModule } from '../../thx-box-base.module';
import { ThxSubtittleV2SizesEnum } from '../../../../atoms/thx-subtitle-v2/models/thx-subtittle-v2-sizes-enum';


const DefaultComponent = componentFactory('thx-box-base-default', `
    <div style="background: black; padding: 10px;">
      <thx-box-base
        [subtitle]="'Title Size Default'"
        [headerFontSize]=""></thx-box-base>

      <br>

      <thx-box-base
        [subtitle]="'Title Size MediumSize'"
        [headerFontSize]="scope.ThxSubtittleV2SizesEnum.MediumSize">

      </thx-box-base>

      <br>
      <thx-box-base
        [subtitle]="'With Header'"
        [headerFontSize]="scope.ThxSubtittleV2SizesEnum.MediumSize">
        <div headerContent style="flex: 1; text-align: right;">
            Header Content
        </div>
      </thx-box-base>

      <br>

      <thx-box-base
        [subtitle]="'With body'"
        [headerFontSize]="scope.ThxSubtittleV2SizesEnum.MediumSize">
        <div body>
            Body
        </div>
      </thx-box-base>

      <br>

      <thx-box-base
        [subtitle]="'With body and footer'"
        [headerFontSize]="scope.ThxSubtittleV2SizesEnum.MediumSize">
        <div body>
            Body
        </div>

        <div footer>
            Footer
        </div>
      </thx-box-base>
    </div>

`, {ThxSubtittleV2SizesEnum});

storiesOf('Ui | Organisms/ThxBoxBase', module)
.addDecorator(
  moduleMetadata({
    imports: [
        CommonModule,
        ThxBoxBaseModule,
    ],
    declarations: [ DefaultComponent ],

  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
