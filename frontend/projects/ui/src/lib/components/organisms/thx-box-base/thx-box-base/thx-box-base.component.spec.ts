import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxBoxBaseComponent } from './thx-box-base.component';
import { configureTestSuite } from 'ng-bullet';

describe('ThxBoxBaseComponent', () => {
  let component: ThxBoxBaseComponent;
  let fixture: ComponentFixture<ThxBoxBaseComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxBoxBaseComponent ]
    });

    fixture = TestBed.createComponent(ThxBoxBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
