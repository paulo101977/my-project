import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxBoxBaseComponent } from './thx-box-base/thx-box-base.component';
import { ThxSubtitleV2Module } from '../../atoms/thx-subtitle-v2/thx-subtitle-v2.module';

@NgModule({
  declarations: [
      ThxBoxBaseComponent
  ],
  exports: [
    ThxBoxBaseComponent,
    ThxSubtitleV2Module
  ],
  imports: [
    CommonModule,
    ThxSubtitleV2Module,
  ]
})
export class ThxBoxBaseModule { }
