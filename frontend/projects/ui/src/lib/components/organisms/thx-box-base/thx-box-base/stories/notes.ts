export class Notes {
    private notes = `
## Html:

~~~html
<thx-box-base
    [subtitle]="'Any Subtitle'"
    [headerFontSize]="ThxSubtittleV2SizesEnum.MediumSize">
    <div headerContent style="text-align: right;">
        Header Content
    </div>

    <div body>
        Body
    </div>

    <div footer>
        Footer
    </div>
 </thx-box-base>
~~~

## Typescript:

~~~ typescript

~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
