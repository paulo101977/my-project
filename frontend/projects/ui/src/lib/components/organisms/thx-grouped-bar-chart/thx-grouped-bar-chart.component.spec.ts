import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ThxGroupedBarChartComponent } from './thx-grouped-bar-chart.component';

describe('ThxStackedBarChartComponent', () => {
  let component: ThxGroupedBarChartComponent;
  let fixture: ComponentFixture<ThxGroupedBarChartComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxGroupedBarChartComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxGroupedBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
