import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxGroupedBarChartComponent } from './thx-grouped-bar-chart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ThxLegendItemModule } from '../../molecules/thx-legend-item/thx-legend-item.module';

@NgModule({
  imports: [
    CommonModule,
    NgxChartsModule,
    ThxLegendItemModule
  ],
  declarations: [ThxGroupedBarChartComponent],
  exports: [ThxGroupedBarChartComponent]
})
export class ThxGroupedBarChartModule { }
