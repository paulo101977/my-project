import { ChartItem } from './chart-item';

export class GroupChartItem {
  name: string;
  series: Array<ChartItem>;
}
