import { Component, Input, ViewEncapsulation } from '@angular/core';
import { GroupChartItem } from './models/group-chart-item';

@Component({
  selector: 'thx-grouped-bar-chart',
  template: `
    <ngx-charts-bar-vertical-2d
      *ngIf="colorScheme && items.length > 0"
      [results]="items"
      [scheme]="colorScheme"
      [xAxis]="showXAxis"
      [yAxis]="showYAxis"
      [showXAxisLabel]="showXAxisLabel"
      [showYAxisLabel]="showYAxisLabel"
      [xAxisLabel]="labelx"
      [yAxisLabel]="labely">
    </ngx-charts-bar-vertical-2d>
    <div class="pie-chart-legend">
      <thx-legend-item *ngFor="let color of colorScheme?.domain; let i = index" [color]="color"> {{ items[0].series[i].name }}
      </thx-legend-item>
    </div>
  `,
  styleUrls: ['thx-grouped-bar-chart.component.css'],
  encapsulation: ViewEncapsulation.None // this view encapsulation is recomended by vendor

})
export class ThxGroupedBarChartComponent {

  @Input() items: Array<GroupChartItem> = [];
  @Input() labelx = '';
  @Input() labely = '';
  @Input('colorList')
  set colorList (colorList: Array<string>) {
    if (colorList) {
      this.colorScheme = { domain: colorList };
    }
  }


  // options
  public colorScheme = null;
  public showXAxis = true;
  public showYAxis = true;
  public showXAxisLabel = true;
  public showYAxisLabel = true;


}
