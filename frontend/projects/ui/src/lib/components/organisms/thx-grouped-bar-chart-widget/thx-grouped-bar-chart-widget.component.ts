import { Component, Input } from '@angular/core';
import { PieChartItem } from '../thx-pie-chart/models/pie-chart-item';
import { GroupChartItem } from '../thx-grouped-bar-chart/models/group-chart-item';

@Component({
  selector: 'thx-grouped-bar-chart-widget',
  templateUrl: './thx-grouped-bar-chart-widget.component.html',
  styleUrls: ['./thx-grouped-bar-chart-widget.component.css']
})
export class ThxGroupedBarChartWidgetComponent {

  @Input() title: string;
  @Input() items: Array<GroupChartItem> = [];
  @Input() labelx = '';
  @Input() labely = '';
  @Input() colorList: Array<string>;

}
