import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ThxGroupedBarChartWidgetComponent } from './thx-grouped-bar-chart-widget.component';

describe('ThxGroupedBarChartWidgetComponent', () => {
  let component: ThxGroupedBarChartWidgetComponent;
  let fixture: ComponentFixture<ThxGroupedBarChartWidgetComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxGroupedBarChartWidgetComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxGroupedBarChartWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
