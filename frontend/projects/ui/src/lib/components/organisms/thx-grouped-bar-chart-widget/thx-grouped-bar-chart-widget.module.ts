import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxGroupedBarChartWidgetComponent } from './thx-grouped-bar-chart-widget.component';
import { ThxWidgetBoxModule } from '../../atoms/thx-widget-box/thx-widget-box.module';
import { ThxGroupedBarChartModule } from '../thx-grouped-bar-chart/thx-grouped-bar-chart.module';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';

@NgModule({
  imports: [
    CommonModule,
    ThxWidgetBoxModule,
    ThxGroupedBarChartModule,
    ThxImgModule
  ],
  declarations: [ThxGroupedBarChartWidgetComponent],
  exports: [ThxGroupedBarChartWidgetComponent]
})
export class ThxGroupedBarChartWidgetModule { }
