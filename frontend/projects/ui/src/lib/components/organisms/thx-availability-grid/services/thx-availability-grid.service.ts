// Angular imports
import { Injectable } from '@angular/core';
// Models
import {
  Availability,
  BedTypeEnum,
  Cell,
  Footer,
  FooterType,
  Image,
  AvailabilityGridLabels,
  Reservation,
  ReservationStatusEnum,
  Room,
  RoomType,
} from '../thx-availability-grid.model';
import { AssetsCdnService } from '../../../../services/assets-service/assets-cdn.service';

// Mocks
@Injectable({ providedIn: 'root' })
export class ThxAvailabilityGridService {
  constructor( private assetsService: AssetsCdnService) {}

  public mapReservationList(reservationList: Reservation[]): Reservation[] {
    const conjugatedReservations = reservationList.filter(reservation => reservation.isConjugated);

    return reservationList.map(reservation => {
      reservation.resource = this.getRoomId(reservation.roomId);
      reservation.resizeDisabled = true;
      reservation.barHidden = true;

      if (reservation.status > ReservationStatusEnum.Confirmed || reservation.isConjugated) {
        reservation.moveHDisabled = true;
        reservation.moveVDisabled = true;
      }

      if (reservation.isConjugated) {
        reservation.id = reservation.id + '.' + reservation.roomId;
        reservation.join = conjugatedReservations.find(
          conjugated => conjugated.reservationId === reservation.reservationId && conjugated.roomId !== reservation.roomId,
        ).reservationId;
      }

      reservation.cssClass = this.getReservationClass(reservation.status);

      return reservation;
    });
  }

  public getReservationClass(reservationStatus: ReservationStatusEnum) {
    switch (reservationStatus) {
      case ReservationStatusEnum.Checkin:
        return 'status-checkin';
      case ReservationStatusEnum.Checkout:
        return 'status-checkout';
      case ReservationStatusEnum.ToConfirm:
        return 'status-to-confirm';
      case ReservationStatusEnum.Confirmed:
        return 'status-confirmed';
    }
  }

  public getReservationStatusName(reservationStatus: ReservationStatusEnum, labels: AvailabilityGridLabels) {
    switch (reservationStatus) {
      case ReservationStatusEnum.Checkin:
        return labels.checkin;
      case ReservationStatusEnum.Checkout:
        return labels.checkout;
      case ReservationStatusEnum.ToConfirm:
        return labels.toConfirm;
      case ReservationStatusEnum.Confirmed:
        return labels.confirmed;
    }
  }

  public mapAvailabilityList(roomTypeList: RoomType[], availabilityList: Availability[], footerList: Footer[]) {
    const resultList = [];

    for (const availability of availabilityList) {
      availability.roomTypeId = this.getRoomTypeId(availability.roomTypeId);

      const resource = roomTypeList.find(resourceItem => availability.roomTypeId === resourceItem.id);

      // Insert RoomType availability info
      resultList.push(availability);

      /*
       * When a RoomType has overbook, insert it's child in
       * the list, in order to have the overbook style propagated
       */
      if (availability.availableRooms < 0 && resource && resource.children) {
        for (const child of resource.children) {
          resultList.push({
            resourceId: child.id,
            date: availability.date,
            availableRooms: availability.availableRooms,
          });
        }
      }
    }

    for (const footer of footerList) {
      for (const footerValue of footer.values) {
        resultList.push({
          resourceId: footer.id,
          date: footerValue.date,
          availableRooms: footerValue.number,
        });
      }
    }

    return resultList;
  }

  public mapResourceList(roomTypeList: RoomType[], footerList: Footer[]) {
    const resourceList = roomTypeList.map(roomType => {
      const resource: RoomType = Object.assign({}, roomType);
      resource.id = this.getRoomTypeId(resource.id);

      if (resource.children) {
        resource.children.map(child => (child.id = this.getRoomId(child.roomId)));
      } else {
        resource.children = [{ id: '', roomId: 0, roomTypeId: 0, name: '' }];
      }

      return resource;
    });

    for (const footer of footerList) {
      resourceList.push(footer);
    }

    return resourceList;
  }

  public checkOverbook(availabilityList: Availability[], cell: Cell) {
    return availabilityList.find(
      item => item.date === cell.start.toString('yyyy-MM-dd') && item.roomTypeId === cell.resource && item.availableRooms < 0,
    );
  }

  public getBedIcon(bed: BedTypeEnum): Image {
    switch (bed) {
      case BedTypeEnum.Double:
        return { src: this.assetsService.getImgUrlTo('camaCasal.min.svg'), alt: 'commomData.images.doubleBed' };
      case BedTypeEnum.Single:
        return { src: this.assetsService.getImgUrlTo('camaSolteiro.min.svg'), alt: 'commomData.images.singleBed' };
      case BedTypeEnum.Reversible:
        return { src: this.assetsService.getImgUrlTo('camaReversivel.min.svg'), alt: 'commomData.images.reversibleBed' };
      default:
        return { src: '', alt: '' };
    }
  }

  public checkIsFooter(resource: number | string) {
    return (
      resource &&
      (resource.toString() === FooterType.total ||
        resource.toString() === FooterType.availablePercentage ||
        resource.toString() === FooterType.blockedRooms)
    );
  }

  public getRoomId(id: string | number) {
    return 'Room-' + id;
  }

  public getRoomTypeId(id: string | number) {
    return 'RoomType-' + id;
  }

  public mapRoomList(roomTypeId: number, roomList: Room[]) {
    return roomList.map(room => {
      room.id = this.getRoomId(room.roomId);
      room.roomTypeId = roomTypeId;
      return room;
    });
  }
}
