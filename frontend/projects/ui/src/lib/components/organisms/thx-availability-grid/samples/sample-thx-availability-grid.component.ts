import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';

import * as momentNs from 'moment';
import {
  Availability,
  AvailabilityGridLabels,
  Footer,
  FooterType,
  Reservation,
  Room,
  RoomBlock,
  RoomType
} from '../thx-availability-grid.model';
import { ThxAvailabilityGridComponent } from '../thx-availability-grid.component';

const moment = momentNs;

const RESERVATION_MOCK: Reservation[] = [
  {
    id: 1,
    roomId: 5,
    roomTypeId: 103,
    reservationId: 1,
    start: moment()
      .add(2, 'days')
      .format('YYYY-MM-DD'),
    end: moment()
      .add(7, 'days')
      .format('YYYY-MM-DD'),
    guestName: 'Dan Imhoff',
    adultCount: 1,
    childCount: 0,
    status: 3,
    isConjugated: false,
    reservationCode: '1Z1EFH7F3',
    roomNumber: '101',
    roomTypeName: 'STD',
    statusName: 'Check-out',
  },
  {
    id: 2,
    roomId: 4,
    roomTypeId: 104,
    reservationId: 2,
    start: moment()
      .add(3, 'days')
      .format('YYYY-MM-DD'),
    end: moment()
      .add(4, 'days')
      .format('YYYY-MM-DD'),
    guestName: 'Brandy Scarney',
    adultCount: 1,
    childCount: 0,
    status: 0,
    isConjugated: false,
    reservationCode: '1Z1EFH7F3',
    roomNumber: '101',
    roomTypeName: 'STD',
    statusName: 'A confirmar',
  },
  {
    id: 3,
    roomId: 1,
    roomTypeId: 101,
    reservationId: 3,
    start: moment()
      .add(-4, 'days')
      .format('YYYY-MM-DD'),
    end: moment()
      .add(4, 'days')
      .format('YYYY-MM-DD'),
    guestName: 'Ricardo Mello',
    adultCount: 1,
    childCount: 0,
    status: 2,
    isConjugated: false,
    reservationCode: '1Z1EFH7F3',
    roomNumber: '101',
    roomTypeName: 'STD',
    statusName: 'Check-in',
  },
  {
    id: 4,
    roomId: 1,
    roomTypeId: 101,
    reservationId: 4,
    start: moment()
      .add(8, 'days')
      .format('YYYY-MM-DD'),
    end: moment()
      .add(15, 'days')
      .format('YYYY-MM-DD'),
    guestName: 'Adam Bradley',
    adultCount: 3,
    childCount: 0,
    status: 0,
    isConjugated: false,
    reservationCode: '1Z1EFH7F3',
    roomNumber: '101',
    roomTypeName: 'STD',
    statusName: 'A confirmar',
  },
  {
    id: 5,
    roomId: 2,
    roomTypeId: 102,
    reservationId: 5,
    start: moment()
      .add(5, 'days')
      .format('YYYY-MM-DD'),
    end: moment()
      .add(9, 'days')
      .format('YYYY-MM-DD'),
    guestName: 'Max Lynch',
    adultCount: 2,
    childCount: 0,
    status: 1,
    isConjugated: false,
    reservationCode: '1Z1EFH7F3',
    roomNumber: '101',
    roomTypeName: 'STD',
    statusName: 'Confirmada',
  },
];

export const ROOMS_MOCK: { roomTypeList: RoomType[]; availabilityList: Availability[]; footerList: Footer[]; roomsBlocked: RoomBlock[] } = {
  roomTypeList: [
    {
      id: 1,
      name: 'Standard',
      total: 6,
    },
    {
      id: 2,
      name: 'Luxo',
      total: 7,
    },
    {
      id: 3,
      name: 'Presidential',
      total: 2,
    },
  ],
  availabilityList: [
    { roomTypeId: 1, date: moment().format('YYYY-MM-DD'), availableRooms: 4 },
    {
      roomTypeId: 1,
      date: moment()
        .add(1, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(2, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(3, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(4, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: -1,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(5, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(6, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(7, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(8, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(9, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(10, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(11, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(12, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(13, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(14, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(15, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(16, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    {
      roomTypeId: 1,
      date: moment()
        .add(17, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 4,
    },
    { roomTypeId: 2, date: moment().format('YYYY-MM-DD'), availableRooms: 3 },
    {
      roomTypeId: 2,
      date: moment()
        .add(1, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(2, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(3, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(4, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(5, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(6, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(7, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(8, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(9, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(10, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(11, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(12, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(13, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(14, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(15, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(16, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 2,
      date: moment()
        .add(17, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    { roomTypeId: 3, date: moment().format('YYYY-MM-DD'), availableRooms: 3 },
    {
      roomTypeId: 3,
      date: moment()
        .add(1, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(2, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(3, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(4, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(5, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(6, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(7, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(8, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(9, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(10, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(11, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(12, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(13, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(14, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(15, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(16, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
    {
      roomTypeId: 3,
      date: moment()
        .add(17, 'days')
        .format('YYYY-MM-DD'),
      availableRooms: 3,
    },
  ],
  footerList: [
    {
      id: FooterType.total,
      name: 'Total',
      total: 27,
      values: [
        { date: moment().format('YYYY-MM-DD'), number: 25 },
        {
          date: moment()
            .add(1, 'days')
            .format('YYYY-MM-DD'),
          number: 25,
        },
        {
          date: moment()
            .add(2, 'days')
            .format('YYYY-MM-DD'),
          number: 20,
        },
        {
          date: moment()
            .add(3, 'days')
            .format('YYYY-MM-DD'),
          number: 20,
        },
        {
          date: moment()
            .add(4, 'days')
            .format('YYYY-MM-DD'),
          number: 25,
        },
        {
          date: moment()
            .add(5, 'days')
            .format('YYYY-MM-DD'),
          number: 25,
        },
        {
          date: moment()
            .add(6, 'days')
            .format('YYYY-MM-DD'),
          number: 25,
        },
        {
          date: moment()
            .add(7, 'days')
            .format('YYYY-MM-DD'),
          number: 25,
        },
      ],
    },
    {
      id: FooterType.availablePercentage,
      name: '% Disp.',
      total: null,
      values: [
        { date: moment().format('YYYY-MM-DD'), number: null },
        {
          date: moment()
            .add(1, 'days')
            .format('YYYY-MM-DD'),
          number: null,
        },
        {
          date: moment()
            .add(2, 'days')
            .format('YYYY-MM-DD'),
          number: null,
        },
        {
          date: moment()
            .add(3, 'days')
            .format('YYYY-MM-DD'),
          number: null,
        },
        {
          date: moment()
            .add(4, 'days')
            .format('YYYY-MM-DD'),
          number: null,
        },
        {
          date: moment()
            .add(5, 'days')
            .format('YYYY-MM-DD'),
          number: null,
        },
        {
          date: moment()
            .add(6, 'days')
            .format('YYYY-MM-DD'),
          number: null,
        },
      ],
    },
  ],
  roomsBlocked: [
    {
      propertyId: 0,
      roomId: 2,
      blockingStartDate: moment()
        .add(1, 'days')
        .format('YYYY-MM-DD'),
      blockingEndDate: moment()
        .add(4, 'days')
        .format('YYYY-MM-DD'),
      reasonId: 0,
      comments: null,
    },
    {
      propertyId: 0,
      roomId: 1,
      blockingStartDate: moment()
        .add(4, 'days')
        .format('YYYY-MM-DD'),
      blockingEndDate: moment()
        .add(5, 'days')
        .format('YYYY-MM-DD'),
      reasonId: 0,
      comments: null,
    },
  ],
};

export const ROOM_MOCK: { roomList: Room[]; reservationList: Reservation[] } = {
  roomList: [
    {
      id: 1,
      roomId: 1,
      name: '101',
    },
    {
      id: 1,
      roomId: 2,
      name: '102',
    },
    {
      id: 1,
      roomId: 3,
      name: '103',
    },
    {
      id: 1,
      roomId: 4,
      name: '104',
    },
    {
      id: 1,
      roomId: 5,
      name: '105',
    },
    {
      id: 1,
      roomId: 6,
      name: '106',
    },
  ],
  reservationList: [
    {
      id: 8,
      roomId: 1,
      reservationId: 4,
      roomTypeId: 1,
      guestName: 'Ricardo Alves',
      adultCount: 2,
      childCount: 0,
      status: 2,
      statusName: '',
      isConjugated: false,
      reservationCode: '1Z1EFH7F3',
      roomNumber: '101',
      roomTypeName: 'STD',
      start: '2018-04-10T21:12:35',
      end: '2018-04-15T21:12:35',
    },
    {
      id: 18,
      roomId: 2,
      reservationId: 12,
      roomTypeId: 1,
      guestName: 'Elton Coelho 1',
      adultCount: 2,
      childCount: 0,
      status: 1,
      statusName: '',
      isConjugated: false,
      reservationCode: '49LK1I81B',
      roomNumber: '102',
      roomTypeName: 'STD',
      start: '2018-04-19T00:00:00',
      end: '2018-04-20T00:00:00',
    },
    {
      id: 19,
      roomId: 3,
      reservationId: 13,
      roomTypeId: 1,
      guestName: 'Guest 1',
      adultCount: 2,
      childCount: 0,
      status: 2,
      statusName: '',
      isConjugated: false,
      reservationCode: '49LK7GP11',
      roomNumber: '103',
      roomTypeName: 'STD',
      start: '2018-04-11T13:29:41',
      end: '2018-04-12T00:00:00',
    },
    {
      id: 97,
      roomId: 3,
      reservationId: 68,
      roomTypeId: 1,
      guestName: 'Algo 1',
      adultCount: 1,
      childCount: 0,
      status: 1,
      statusName: '',
      isConjugated: false,
      reservationCode: '49LKEQUU3',
      roomNumber: '103',
      roomTypeName: 'STD',
      start: '2018-04-14T00:00:00',
      end: '2018-04-15T00:00:00',
    },
    {
      id: 98,
      roomId: 4,
      reservationId: 69,
      roomTypeId: 1,
      guestName: 'Algo 2',
      adultCount: 1,
      childCount: 0,
      status: 1,
      statusName: '',
      isConjugated: false,
      reservationCode: '49LE2TL46',
      roomNumber: '104',
      roomTypeName: 'STD',
      start: '2018-04-14T00:00:00',
      end: '2018-04-15T00:00:00',
    },
    {
      id: 101,
      roomId: 5,
      reservationId: 71,
      roomTypeId: 1,
      guestName: 'Algo',
      adultCount: 1,
      childCount: 0,
      status: 1,
      statusName: '',
      isConjugated: false,
      reservationCode: '49LRIEQG3',
      roomNumber: '105',
      roomTypeName: 'STD',
      start: '2018-04-14T00:00:00',
      end: '2018-04-15T00:00:00',
    },
  ],
};

@Component({
  selector: 'thx-sample-thx-availability-grid',
  templateUrl: './sample-thx-availability-grid.component.html',
})
export class SampleThxAvailabilityGridComponent implements OnInit {
  @ViewChild('availabilityGrid') public availabilityGridComponent: ThxAvailabilityGridComponent;

  roomTypeList: RoomType[];
  blockedRoomList: RoomBlock[];
  availabilityList: Availability[];
  footerList: Footer[];
  reservationList: Reservation[] = [];
  labels: AvailabilityGridLabels;

  constructor() {
    this.labels = {
      overbook: 'overbook',
      legend: 'Legenda',
      checkin: 'Check-in',
      checkout: 'Check-out',
      toConfirm: 'A confirmar',
      confirmed: 'Confirmada',
      showToday: 'Exibir hoje',
      reservationCode: 'Nº da Reserva',
      roomType: 'Tipo de UH',
      status: 'Status',
      period: 'Período',
      groupName: 'Nome do grupo',
      reservationDetails: 'Detalhes da Reserva',
      newReservation: 'Reservar período',
      cornerLabel: 'Tipo UH',
      unlockRoom: 'Desbloquear UH',
      blockRoom: 'Bloquear UH',
      reservationRateProposal: 'Split'
    };
  }

  ngOnInit() {
    setTimeout(() => {
      this.loadRoomTypeList();
      this.loadReservationList();
    }, 500);
  }

  loadRoomTypeList() {
    const roomTypeList = ROOMS_MOCK;
    this.roomTypeList = roomTypeList.roomTypeList;
    this.availabilityList = roomTypeList.availabilityList;
    this.footerList = roomTypeList.footerList;
    this.blockedRoomList = roomTypeList.roomsBlocked;
  }

  loadReservationList() {}

  getRooms() {
    this.availabilityGridComponent.setRooms(<any>ROOMS_MOCK.roomTypeList[0].id, ROOM_MOCK.roomList);
    this.reservationList = RESERVATION_MOCK;
  }

  onReservationClick(reservation: Reservation) {}

  validateReservationMove(reservation: Reservation) {
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(true);
      }, 200);
    });
  }
}
