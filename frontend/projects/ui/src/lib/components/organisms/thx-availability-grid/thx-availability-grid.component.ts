
import {finalize} from 'rxjs/operators';
import {
  ApplicationRef,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  OnChanges,
  Renderer2,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { ThxAvailabilityGridBaseComponent } from './thx-availability-grid-base.component';
import { ThxPopoverComponent } from '../thx-popover/thx-popover.component';
import { ThxAvailabilityGridService } from './services/thx-availability-grid.service';
import { DayPilot } from 'daypilot-pro-angular';
import { Observable } from 'rxjs';

import { Cell, Reservation, Room, Row, TimeRange } from './thx-availability-grid.model';
import { ThxAvailabilityGridCellService } from './services/thx-availability-grid-cell.service';
import { ThxAvailabilityGridRowService } from './services/thx-availability-grid-row.service';
import { ThxAvailabilityGridHeaderService } from './services/thx-availability-grid-header.service';
import * as momentNs from 'moment';
import { AssetsCdnService } from '../../../services/assets-service/assets-cdn.service';

const moment = momentNs;

/**
 * @docsExtends ThxAvailabilityGridBaseComponent
 *
 * @example
 * <example name="thx-availability-grid" title="Exemplo de grid de disponibilidade" >
 *  <file name='sample-thx-availability-grid.component.html'> </file>
 *  <file name="sample-thx-availability-grid.component.ts"> </file>
 * </example>
 */
@Component({
  selector: 'thx-availability-grid',
  templateUrl: './thx-availability-grid.component.html',
  styleUrls: ['./thx-availability-grid.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ThxAvailabilityGridComponent extends ThxAvailabilityGridBaseComponent implements OnChanges {
  @ViewChild('popoverReservationInfo') public popoverReservationInfo: TemplateRef<any>;

  @ViewChild('popoverSelectedPeriod') public popoverSelectedPeriod: TemplateRef<any>;

  private popoverContainer: ThxPopoverComponent;

  private isPopoverShown: boolean;

  private globalPopoverClick: any;

  constructor(
    private gridService: ThxAvailabilityGridService,
    private cellService: ThxAvailabilityGridCellService,
    private rowService: ThxAvailabilityGridRowService,
    private headerService: ThxAvailabilityGridHeaderService,
    public viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    public appRef: ApplicationRef,
    public elementRef: ElementRef,
    public renderer: Renderer2,
    private assetsCdnService: AssetsCdnService,
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.config) {
      this.updateLists(changes);
    } else if (this.labels) {
      this.setVars();
    }
  }

  setVars(): void {
    this.days = this.days || 7;
    this.momentDateFormat = 'YYYY-MM-DD';
    this.daypilotDateFormat = 'yyyy-MM-dd';

    this.setConfig({
      days: this.days,
      startDate: new DayPilot.Date(this.startDate),
    });
  }

  updateLists(changes: SimpleChanges): void {
    if (changes.roomTypeList || changes.footerList) {
      const footerList = changes.footerList ? changes.footerList.currentValue : this.footerList;
      const roomTypeList = changes.roomTypeList ? changes.roomTypeList.currentValue : this.roomTypeList;

      this.resourceList = this.gridService.mapResourceList(roomTypeList, footerList);
      this.footerList = footerList;
      this.config.resources = this.resourceList;
    }

    if (changes.availabilityList) {
      this.availabilityList = this.gridService.mapAvailabilityList(this.resourceList, this.availabilityList, this.footerList);
    }

    if (changes.reservationList) {
      this.reservationList = this.gridService.mapReservationList(changes.reservationList.currentValue);
      this.scheduler.control.events.list = <any>this.reservationList;
    }

    if (changes.days) {
      this.scheduler.control.days = changes.days.currentValue;
      this.config.days = this.scheduler.control.days;
    }

    if (changes.startDate) {
      this.scheduler.control.startDate = new DayPilot.Date(changes.startDate.currentValue);
      this.config.startDate = this.scheduler.control.startDate;
    }
  }

  setRooms(roomTypeId: number, roomList: Room[]) {
    const index = this.config.resources.findIndex(resource => resource.id === this.gridService.getRoomTypeId(roomTypeId));

    this.roomList = this.gridService.mapRoomList(roomTypeId, roomList);
    this.config.resources[index].children = this.roomList;
  }

  setConfig(params: any): void {
      this.config = {
      snapToGrid: false,
      timeHeaders: [{ groupBy: 'Day', format: 'd' }],
      eventHeight: 37,
      headerHeight: 60,
      jointEventsMove: true,
      scale: 'Day',
      days: params.days,
      startDate: params.startDate,
      treeEnabled: true,
      treeIndent: 5,
      treeImageMarginLeft: 10,
      treeImageMarginTop: 4,
      useEventBoxes: 'Never',
      allowEventOverlap: false,
      cellWidthSpec: 'Auto',
      rowMarginTop: 5,
      rowMarginBottom: 5,
      rowHeaderWidth: 125,
      rowHeaderScrolling: false,
      treeAutoExpand: false,
      cornerHtml: this.labels.cornerLabel,
      onBeforeRowHeaderRender: this.onBeforeRowHeaderRender,
      onBeforeTimeHeaderRender: this.onBeforeTimeHeaderRender,
      onBeforeCellRender: this.onBeforeCellRender,
      onBeforeEventRender: this.onBeforeEventRender,
    };

    if (!this.disableClick) {
      this.config = { ...this.config,
        onResourceExpand: this.onResourceExpand,
        onEventMove: this.onEventMove,
        onEventMoving: this.onEventMoving,
        timeRangeSelectedHandling: 'Callback',
        onRowClick: this.onRowClick,
        onTimeRangeSelect: this.onTimeRangeSelect,
        onTimeRangeClick: this.onTimeRangeClick,
        onTimeRangeSelected: this.onTimeRangeSelected,
        onTimeRangeSelecting: this.onTimeRangeSelecting,
      };
    }
  }

  onEventMove = args => {
    const scheduler = args.control; // DayPilot Scheduler
    const newRowData = scheduler.rows.find(args.newResource).data;

    const reservation: Reservation = Object.assign({}, args.e.data, newRowData);
    reservation.id = reservation.reservationId;
    reservation.start = args.newStart;
    reservation.end = args.newEnd;

    if (reservation.moveHDisabled || reservation.moveVDisabled) {
      return false; // Stops when moving is disabled
    }

    const validation = this.validateReservationMove && this.validateReservationMove(reservation);

    if (validation) {
      args.async = true;

      if (validation instanceof Promise) {
        validation.catch(() => args.preventDefault()).then(() => args.loaded());
      } else if (validation instanceof Observable) {
        validation.pipe(finalize(() => args.loaded())).subscribe(success => {}, error => args.preventDefault());
      } else {
        console.error('Invalid validateReservationMove Handler: ', validation);
      }
    } else {
      console.error('Validator must return a promise or a observable');
    }
  }

  onEventMoving = args => {
    const {start, end} = this.cellService.configPeriodByPropertyParams(args.start, args.end, this.propertyParams);
    args.start = new DayPilot.Date(start);
    args.end = new DayPilot.Date(end);
    if (args.e.data.moveHDisabled || args.e.data.moveVDisabled) {
      args.allowed = false;
      return false; // Stops when moving is disabled
    }

    if (this.gridService.checkIsFooter(args.resource)) {
      args.allowed = false;
      return false;
    }

    // Validate if the cell is blocked
    if (this.cellService.isTimeRangeSelectionValid(args, this.blockedRoomList)) {
      const blocked = this.cellService.isBlocked(args, this.blockedRoomList);
      args.allowed = !blocked;
    } else {
      args.allowed = false;
    }
  }

  onBeforeRowHeaderRender = args => {
    const row: Row = args.row;
    row.classes = [];

    this.rowService.setRowTotalRoomsBadge(row);

    if (this.gridService.checkIsFooter(row.id)) {
      this.rowService.setRowAsFooter(row);
    } else {
      this.rowService.setRowAsParent(row);
      this.rowService.setRowStatusCssClass(row);
      this.rowService.setRowBedIcon(row);
    }

    row.cssClass = row.classes.join(' ');
  }

  onBeforeTimeHeaderRender = args => {
    const header = args.header;
    header.classes = [];

    this.headerService.setTimeHeader(args.header, this.momentDateFormat);
    this.headerService.setTimeHeaderContent(args.header, this.momentDateFormat);

    header.cssClass = header.classes.join(' ');
    delete header.classes;
  }

  onResourceExpand = args => {
    this.scheduler.control.clearSelection();
    this.scheduler.control.rows.all().forEach(row => (args.resource.id !== row.id ? row.collapse() : null));
    const id = args.resource.id.replace('RoomType-', '');
    this.roomTypeExpanded.emit(id);
  }

  onRowClick = args => {
    if (args.row.parent() || this.gridService.checkIsFooter(args.resource.id)) {
      return;
    }
    args.resource.toggle();
  }

  onBeforeCellRender = args => {
    const cell: Cell = args.cell;
    cell.classes = [];

    if (this.gridService.checkIsFooter(cell.resource)) {
      this.cellService.setCellFooter(cell, this.footerList, this.daypilotDateFormat);
    } else {
      this.cellService.setOverbook(cell, this.availabilityList, this.labels.overbook);
      this.cellService.checkTime(cell, this.momentDateFormat);
    }

    this.cellService.setAvailability(cell, this.availabilityList, this.daypilotDateFormat);
    this.cellService.setDisabled(cell, this.blockedRoomList);

    // Alternate even-odd
    cell.classes.push(args.cell.y % 2 ? ' even ' : ' odd ');

    cell.cssClass = cell.classes.join(' ');
    delete cell.classes;
  }

  onBeforeEventRender = args => {
    const data: Reservation = args.data;

    args.data.cssClass = this.gridService.getReservationClass(data.status);

    args.e.html = `
          <div class="text">${data.guestName}</div>
          <div class="caption">
            <img src="${ this.assetsCdnService.getImgUrlTo('person-white.svg')}" class="adult-count"> ${data.adultCount}
            <img src="${ this.assetsCdnService.getImgUrlTo('child_icon-white.svg')}" class="child-count"> ${data.childCount}
          </div>
        `;

    args.e.areas = [
      {
        right: 2,
        top: 3,
        width: 31,
        height: 31,
        visibility: 'Visible',
        html: `<i id="event-popover-${args.data.id}" class="thf-u-font--25 mdi mdi-dots-vertical event-popover-${args.data.id}"></i>`,
        css: 'event_action_show_more',
        action: 'JavaScript',
        js: e => {
          const hElement: HTMLElement = this.elementRef.nativeElement;
          const allDivs = hElement.getElementsByClassName('event-popover-' + args.data.id);

          this.selectedReservation = args.data;
          this.selectedReservation.statusName = this.gridService.getReservationStatusName(this.selectedReservation.status, this.labels);

          this.openPopover(<HTMLElement>allDivs[0], this.popoverReservationInfo);
          this.scheduler.control.clearSelection();
        },
      },
    ];
  }

  onTimeRangeSelected = args => {
    const hElement: HTMLElement = this.elementRef.nativeElement;
    const allDivs = hElement.getElementsByClassName('scheduler_default_shadow');

    const room = this.roomList.find(roomItem => roomItem.id === args.resource);
    const roomTypeFilter = room ? room.roomTypeId : args.resource.replace('RoomType-', '');
    const roomType = this.roomTypeList.find(item => item.id == roomTypeFilter);

    const blocked = this.cellService.isBlocked(args, this.blockedRoomList);
    if (blocked) {
      args.start = new DayPilot.Date(blocked.blockingStartDate);
      args.end = new DayPilot.Date(blocked.blockingEndDate).addDays(1);
    }

    const {start, end} = this.cellService.configPeriodByPropertyParams(args.start, args.end, this.propertyParams);
    args.start = new DayPilot.Date(start);
    args.end = new DayPilot.Date(end);
    this.selectedTimeRange = new TimeRange();
    this.selectedTimeRange.start = start;
    this.selectedTimeRange.end = end;

    if (room) {
      this.selectedTimeRange.roomId = room.roomId;
      this.selectedTimeRange.roomName = room.name;
    }

    if (roomType) {
      this.selectedTimeRange.roomTypeId = <number>roomType.id;
      this.selectedTimeRange.roomTypeName = roomType.name;
    }

    if (this.cellService.isTimeRangeSelectionValid(args, this.blockedRoomList)) {
      const roomBlock = this.cellService.isBlocked(args, this.blockedRoomList);
      if (roomBlock) {
        this.selectedTimeRange.roomBlock = roomBlock;
      }
    }

    this.openPopover(<HTMLElement>allDivs[0], this.popoverSelectedPeriod);
  }

  onTimeRangeSelect = args => {
    // args.preventDefault()
  }

  onTimeRangeClick = args => {
    this.scheduler.control.clearSelection();
  }

  onTimeRangeSelecting = args => {
    const {start, end} = this.cellService.configPeriodByPropertyParams(args.start, args.end, this.propertyParams);
    args.start = new DayPilot.Date(start);
    args.end = new DayPilot.Date(end);
    if (this.gridService.checkIsFooter(args.resource)) {
      args.allowed = false;
    } else if (this.cellService.isTimeRangeSelectionValid(args, this.blockedRoomList)) {
      const blocked = this.cellService.isBlocked(args, this.blockedRoomList);
      if (blocked) {
        args.start = new DayPilot.Date(blocked.blockingStartDate);
        args.end = new DayPilot.Date(blocked.blockingEndDate).addDays(1);
      }
    } else {
      args.allowed = false;
    }
  }

  goPrevious($event) {
    $event.preventDefault();

    const currDate = <DayPilot.Date>this.scheduler.control.startDate;
    this.scheduler.control.startDate = currDate.addDays(-this.scheduler.control.days);
    this.initialDateChange.emit(moment(this.scheduler.control.startDate.toString()).format(this.momentDateFormat));
  }

  goNext($event) {
    $event.preventDefault();

    const currDate = <DayPilot.Date>this.scheduler.control.startDate;
    this.scheduler.control.startDate = currDate.addDays(this.scheduler.control.days);
    this.initialDateChange.emit(moment(this.scheduler.control.startDate.toString()).format(this.momentDateFormat));
  }

  goToday() {
    this.scheduler.control.startDate = new DayPilot.Date().getDatePart();
    this.initialDateChange.emit(moment(this.scheduler.control.startDate.toString()).format(this.momentDateFormat));
  }

  /*
   * Popover methods
   * I couldn't apply the directive directly into daypilot items because it only returns static html
   * **************************************************************************************************/
  public openPopover(referenceObject: HTMLElement, template: TemplateRef<any>) {
    if (!this.popoverContainer) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ThxPopoverComponent);
      const viewContainerRef = this.appRef.components[0].instance.viewContainerRef || this.viewContainerRef;
      const componentRef = viewContainerRef.createComponent(componentFactory);

      this.popoverContainer = componentRef.instance as ThxPopoverComponent;
      this.popoverContainer.popoverColor = 'white';
      this.popoverContainer.popperOptions = {
        placement: 'top',
      };
    }

    this.popoverContainer.template = template;

    this.popoverContainer.referenceObject = referenceObject;

    if (this.isPopoverShown) {
      this.hidePopover();
    } else {
      this.showPopover();
    }
  }

  public onClickOutside(event) {
    if (this.isPopoverShown && !this.popoverContainer.popoverContent.nativeElement.parentElement.contains(event.target)) {
      this.hidePopover();
    }
  }

  public hidePopover() {
    this.popoverContainer.hide();
    this.globalPopoverClick();
    this.isPopoverShown = false;
  }

  public showPopover() {
    this.popoverContainer.show();
    this.isPopoverShown = true;
    this.globalPopoverClick = this.renderer.listen('document', 'mousedown', this.onClickOutside.bind(this));
  }

  public emitNewReservation($event: any, timeRange: TimeRange) {
    $event.preventDefault();
    this.newReservationClick.emit(timeRange);
    this.scheduler.control.clearSelection();
    this.hidePopover();
  }

  public emitBlockRoom($event: any, timeRange: TimeRange) {
    $event.preventDefault();
    this.blockRoomClick.emit(timeRange);
    this.scheduler.control.clearSelection();
    this.hidePopover();
  }

  public emitUnlockRoom($event: any, timeRange: TimeRange) {
    $event.preventDefault();
    this.unlockRoomClick.emit(timeRange);
    this.scheduler.control.clearSelection();
    this.hidePopover();
  }

  public emitReservationClick(reservation: Reservation) {
    this.reservationClick.emit(reservation);
    this.scheduler.control.clearSelection();
    this.hidePopover();
  }

  public emitRateProposalClick($event: any, timeRange: TimeRange) {
    $event.preventDefault();
    this.rateProposalClick.emit(timeRange);
    this.scheduler.control.clearSelection();
    this.hidePopover();
  }
}
