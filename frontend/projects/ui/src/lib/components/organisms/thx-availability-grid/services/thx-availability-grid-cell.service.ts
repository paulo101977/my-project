import { Injectable } from '@angular/core';
import { Availability, Cell, Footer, FooterType, RoomBlock } from '../thx-availability-grid.model';
import { DayPilot } from 'daypilot-pro-angular';
import { ThxAvailabilityGridService } from './thx-availability-grid.service';
import * as momentNs from 'moment';
import { AssetsCdnService } from '../../../../services/assets-service/assets-cdn.service';

const moment = momentNs;

@Injectable({ providedIn: 'root' })
export class ThxAvailabilityGridCellService {
  private readonly DAYPILOT_DATE_FORMAT = 'yyyy-MM-dd';

  constructor(private gridService: ThxAvailabilityGridService, private assetsService: AssetsCdnService) {}

  public setCellFooter(cell: Cell, footerList: Footer[], dateFormat: string) {
    cell.classes.push('footer');
    const resourceId = cell.resource.toString();

    switch (resourceId) {
      case FooterType.total:
        cell.classes.push('total');
        break;

      case FooterType.availablePercentage:
        cell.classes.push('available-percentage');
        break;
    }

    // Set cell value
    const footer = footerList.find(footerItem => footerItem.id == resourceId);

    if (footer) {
      const currentDate = footer.values.find(value => value.date == cell.start.toString(dateFormat));

      if (currentDate) {
        cell.html = <string>currentDate.number;
      }
    }
  }

  public setOverbook(cell: Cell, availabilityList: Availability[], label: string) {
    const overbook = this.gridService.checkOverbook(availabilityList, cell);

    if (overbook) {
      cell.classes.push('overbook');

      if (overbook.availableRooms && cell.isParent) {
        cell.html = cell.html || '';
        cell.html += `<div class="label">${label}</div>`;
      }
    }
  }

  public setAvailability(cell: Cell, availabilityList: Availability[], dateFormat: string) {
    if (cell.isParent || cell.resource == FooterType.availablePercentage || cell.resource == FooterType.total) {
      const availability = availabilityList.find(
        availabilityItem => availabilityItem.roomTypeId === cell.resource && availabilityItem.date === cell.start.toString(dateFormat),
      );

      if (availability) {
        cell.html = cell.html || '';
        cell.html += availability.availableRooms;
      }
    }
  }

  public checkTime(cell: Cell, dateFormat: string) {
    const date: DayPilot.Date = cell.start;

    const momentDate = moment(date.toString(), dateFormat);
    const momentToday = moment();

    if (cell.start.getDayOfWeek() === 0 || cell.start.getDayOfWeek() === 6) {
      cell.classes.push('weekend');
    }

    if (momentDate.isSame(momentToday, 'day')) {
      cell.classes.push('today');
    } else if (momentDate.isBefore(momentToday, 'day')) {
      cell.classes.push('past');
    } else {
      cell.classes.push('default');
    }
  }

  public setDisabled(cell: Cell, blockedRoomList: RoomBlock[]) {
    if (!blockedRoomList) {
      return;
    }

    const blocked = this.isBlocked(cell, blockedRoomList);
    if (blocked) {
      cell.classes.push('blocked');
      cell.html = cell.html || '';
      cell.html += `<img src="${this.assetsService.getImgUrlTo('lock-outline.svg')}">`;

      if (cell.start.getDatePart() == new DayPilot.Date(blocked.blockingStartDate).getDatePart()) {
        cell.classes.push('start');
      } else if (cell.start == new DayPilot.Date(blocked.blockingEndDate)) {
        cell.classes.push('end');
      }
    }
  }

  public isBlocked(cell: any, blockedRoomList: RoomBlock[]): RoomBlock {
    if (!blockedRoomList) {
      return null;
    }

    return blockedRoomList.find(function(range) {
      const start = new DayPilot.Date(range.blockingStartDate);
      const end = new DayPilot.Date(range.blockingEndDate).addDays(1);
      return DayPilot.Util.overlaps(start, end, cell.start, cell.end) && 'Room-' + range.roomId == cell.resource;
    });
  }

  public isTimeRangeSelectionValid(args: any, blockedRoomList: RoomBlock[]) {
    let startDate: DayPilot.Date = args.start;
    const endDate: DayPilot.Date = args.end;
    const days = [];

    while (startDate < endDate) {
      const day = {
        start: startDate,
        end: startDate.addDays(1),
        resource: args.resource,
      };
      startDate = day.end;
      days.push(day);
    }

    let blocked = null;
    let unblocked = null;

    for (const day of days) {
      if (this.isBlocked(day, blockedRoomList)) {
        blocked = day;
      } else {
        unblocked = day;
      }

      if (blocked && unblocked) {
        return false;
      }
    }

    return true;
  }

  public configPeriodByPropertyParams(cellStart: DayPilot.Date, cellEnd: DayPilot.Date, propertyParams)
    : { start, end } {
    if (cellStart && cellEnd && propertyParams) {
      const start = cellStart.toString(this.DAYPILOT_DATE_FORMAT) + 'T' + propertyParams.checkin + ':00.000';
      const end = cellEnd.toString(this.DAYPILOT_DATE_FORMAT) + 'T' + propertyParams.checkout + ':00.000';
      return {start, end};
    }
  }

}
