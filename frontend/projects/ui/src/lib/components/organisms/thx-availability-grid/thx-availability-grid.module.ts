import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DayPilotModule } from 'daypilot-pro-angular';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';
import { ThxAvailabilityGridComponent } from './thx-availability-grid.component';
import { MomentModule } from 'ngx-moment';
import { ThxAvailabilityGridService } from './services/thx-availability-grid.service';
import { ThxAvailabilityGridRowService } from './services/thx-availability-grid-row.service';
import { ThxAvailabilityGridCellService } from './services/thx-availability-grid-cell.service';
import { ThxAvailabilityGridHeaderService } from './services/thx-availability-grid-header.service';
import { ThxPopoverModule } from '../thx-popover/thx-popover.module';
import { AssetsServiceModule } from '../../../services/assets-service/assets-service.module';

@NgModule({
  imports: [
    CommonModule,
    DayPilotModule,
    ThxPopoverModule,
    MomentModule,
    AssetsServiceModule,
    ImgCdnModule
  ],
  declarations: [ThxAvailabilityGridComponent],
  exports: [ThxAvailabilityGridComponent],
  providers: [
    ThxAvailabilityGridService,
    ThxAvailabilityGridRowService,
    ThxAvailabilityGridCellService,
    ThxAvailabilityGridHeaderService
  ],
})
export class ThxAvailabilityGridModule {
}
