import { DayPilot } from 'daypilot-pro-angular';

export enum BedTypeEnum {
  Single = 1,
  Double = 2,
  Reversible = 3,
  Extra = 4,
  Crib = 5,
}

export enum RoomStatusEnum {
  Clean = 1,
  Dirty = 2,
  Maintenace = 3,
}

export enum ReservationStatusEnum {
  ToConfirm = 0,
  Confirmed = 1,
  Checkin = 2,
  Checkout = 3,
  Pending = 4,
  NoShow = 5,
  Canceled = 6,
  WaitList = 7,
  ReservationProposal = 8,
}

export class Image {
  src: string;
  alt: string;
}

export class AvailabilityGridLabels {
  overbook: string;
  legend: string;
  checkin: string;
  checkout: string;
  toConfirm: string;
  confirmed: string;
  showToday: string;
  reservationCode: string;
  roomType: string;
  status: string;
  period: string;
  groupName: string;
  reservationDetails: string;
  reservationRateProposal: string;
  newReservation: string;
  cornerLabel: string;
  unlockRoom: string;
  blockRoom: string;
}

export class FooterType {
  static total = '1';
  static availablePercentage = '2';
  static blockedRooms = '3';
}

export class Row extends DayPilot.Row {
  classes: string[];
  cssClass: string;
  html: string;
}

export interface Cell extends DayPilot.Cell {
  classes: string[];
  resource: string;
  cssClass: string;
  html: string;
}

export class RoomType {
  id: number | string;
  name: string;
  total?: number;
  propertyId?: number | string;
  propertyName?: string;
  children?: Room[];
}

export class Property {
  id: number | string;
  name: string;
  total?: number;
  children?: RoomType[];
}

export class AvailabilityItem {
  id: number | string;
  name: string;
  total?: number;
}

export class RoomBlock {
  propertyId: number;
  roomId: number;
  blockingStartDate: string;
  blockingEndDate: string;
  reasonId: number;
  comments: string;
}

export class Room {
  id: number | string;
  roomId: number;
  roomTypeId?: number | string;
  name: string;
  status?: RoomStatusEnum;
  bedIcon?: Image;
}

export class Availability {
  roomTypeId: number | string;
  date: string;
  availableRooms?: number;
  rooms?: number;
  occupation?: number;
  propertyId?: number | string;
}

export class Footer {
  id: number | string;
  name: string;
  total: number;
  values: FooterValue[];
}

export class FooterValue {
  date: string;
  number: number | string;
}

export class Reservation {
  id: number | string;
  start: string;
  end: string;
  reservationId: number;
  roomId: number;
  roomTypeId: number;
  guestName: string;
  adultCount: number;
  childCount: number;
  status: number;
  statusName: string;
  isConjugated: boolean;
  reservationCode: string;
  roomNumber: string;
  roomTypeName?: string;
  roomName?: string;
  statusText?: string;
  // Internal attributes
  cssClass?: string;
  reservationIdentifier?: string;
  resource?: number | string;
  resizeDisabled?: boolean;
  moveVDisabled?: boolean;
  moveHDisabled?: boolean;
  text?: string;
  barHidden?: boolean;
  join?: number;
}

export class TimeRange {
  start: string;
  end: string;
  roomId: number;
  roomTypeId: number;
  roomName?: string;
  roomTypeName?: string;
  roomBlock?: RoomBlock;
  propertyId?: number;
  propertyName?: string;
}
