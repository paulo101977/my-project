import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';
import { DefaultComponent } from './default';
import { ThxImgModule } from '../../../atoms/thx-img/thx-img.module';
import { InputBaseEfectV2Module } from '../../../atoms/input-base-efect-v2/input-base-efect-v2.module';
import { TranslateModule } from '@ngx-translate/core';
import { ThxSelectV2Module } from '../../../molecules/thx-select-v2/thx-select-v2.module';
import { ThxInputV2Module } from '../../../molecules/thx-input-v2/thx-input-v2.module';
import { ThxErrorV2Module } from '../../../molecules/thx-error-v2/thx-error-v2.module';
import { InputSelectGroupModule } from '../input-select-group.module';


storiesOf('Ui | Organisms/InputSelectGroup', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule,
      FormsModule,
      ReactiveFormsModule,
      ThxImgModule,
      TranslateModule.forRoot(),
      ThxErrorV2Module,
      InputBaseEfectV2Module,
      ThxInputV2Module,
      ThxSelectV2Module,
      InputSelectGroupModule ],
    declarations: [ DefaultComponent ],

  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
