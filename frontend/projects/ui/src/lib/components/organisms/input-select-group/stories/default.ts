import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'thx-default-input-select-group',
  template: `
    <div style="padding: 100px">
      <div class="row">
        <div class="col-6">
          <thx-input-select-group
            [title]="title"
            [optionList]="list"
            [(ngModel)]="value"
          ></thx-input-select-group>
          <br>
          <div>
            NGMODEL: {{ value | json }}
          </div>
        </div>
        <div class="col-6">
          <thx-input-select-group
            [title]="title2"
            [optionList]="list2"
            [infoText]="infoText2"
            [(ngModel)]="value2"
          ></thx-input-select-group>
          <br>
          <div>
            NGMODEL: {{ value2 | json}}
          </div>
        </div>
      </div>
    </div>
  `,
  styles: []
})
export class DefaultComponent implements OnInit {

  public title = 'Documento';
  public title2 = 'Algo legal';

  public infoText = 'Utilize um documento real';
  public infoText2 = 'Que tal digitar algo legal aqui?!';

  public value = {
    inputValue: '12660143730',
    selectValue: 2
  };

  public value2 = {
    inputValue: 'Video Games',
    selectValue: 1
  };

  public list = [
    {
      text: 'CPF',
      value: '1'
    },
    {
      text: 'NIF',
      value: '2'
    }
  ];

  public list2 = [
    {
      text: 'Games',
      value: '1'
    },
    {
      text: 'Esportes',
      value: '2'
    }
  ];

  form: FormGroup;

  constructor(private fb: FormBuilder) {
  }



  ngOnInit() {
      this.form = this.fb.group({
        name: ['Um Bom Nome', [Validators.required]],
      });
  }
}
