export interface InputSelectGroup {
  inputValue: string;
  selectValue: string;
}
