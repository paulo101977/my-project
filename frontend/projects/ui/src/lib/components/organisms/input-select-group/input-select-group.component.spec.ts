import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputSelectGroupComponent } from './input-select-group.component';
import { configureTestSuite } from 'ng-bullet';

describe('InputSelectGroupComponent', () => {
  let component: InputSelectGroupComponent;
  let fixture: ComponentFixture<InputSelectGroupComponent>;


  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ InputSelectGroupComponent ]
    });
    fixture = TestBed.createComponent(InputSelectGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
