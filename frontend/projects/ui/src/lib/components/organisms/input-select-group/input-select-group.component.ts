import { Attribute, Component, forwardRef, Input, OnInit } from '@angular/core';
import { InputSelectGroup } from './input-select-group';
import { ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { SelectOptionItem } from '../../molecules/thx-select-v2/select-option-item';

@Component({
  selector: 'thx-input-select-group',
  templateUrl: './input-select-group.component.html',
  styleUrls: ['./input-select-group.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputSelectGroupComponent),
      multi: true
    }
  ]
})
export class InputSelectGroupComponent implements OnInit, ControlValueAccessor {

  private static readonly NULL_VALUE = {
    inputValue: '',
    selectValue: ''
  };

  @Input() title: string;
  @Input() optionList: SelectOptionItem[];
  @Input() infoText: string;

  public value: InputSelectGroup = InputSelectGroupComponent
    .NULL_VALUE;

  public disabled: boolean;

  onChange = (value: InputSelectGroup) => {};
  onTouched = () => {};

  constructor( @Attribute('required') public required ) { }

  ngOnInit() {
  }


  // ControlValueAccessor
  writeValue(val: InputSelectGroup): void {
    this.value = val ? val : InputSelectGroupComponent.NULL_VALUE;
    this.onChange(this.value);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  // Allows Angular to disable the input.
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

}
