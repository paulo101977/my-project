import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputSelectGroupComponent } from './input-select-group.component';
import { ThxInputV2Module } from '../../molecules/thx-input-v2/thx-input-v2.module';
import { ThxSelectV2Module } from '../../molecules/thx-select-v2/thx-select-v2.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [InputSelectGroupComponent],
  imports: [
    CommonModule,
    ThxInputV2Module,
    ThxSelectV2Module,
    FormsModule
  ],
  exports: [InputSelectGroupComponent]
})
export class InputSelectGroupModule { }
