import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { StepMenuModule } from '../step-menu.module';
import { DefaultComponent } from './default';
import { WithMultiplesItemsComponent } from './with-multiples-items';
import { WithSingleItemComponent } from './with-single-item';

storiesOf('Ui | Organisms/ThxStepMenu', module)
  .addDecorator(
    moduleMetadata({
      imports: [ CommonModule, StepMenuModule ]
    })
  )
  .add('Default', () => ({
    component: DefaultComponent
  }))
  .add('With Single Item', () => ({
    component: WithSingleItemComponent
  }))
  .add('With Multiples Items', () => ({
    component: WithMultiplesItemsComponent
  }));
