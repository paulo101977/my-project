import { StepMenuItemState } from '../models/step-menu-item-state.enum';
import { MenuStateIconPipe } from './menu-state-icon.pipe';

describe('MenuStateIconPipe', () => {
  it('create an instance', () => {
    const pipe = new MenuStateIconPipe();
    expect(pipe).toBeTruthy();
  });

  describe('#transform', () => {
    it('should return a value when called with a menu state', () => {
      const pipe = new MenuStateIconPipe();
      expect(pipe.transform(StepMenuItemState.Enabled)).toBeDefined();
      expect(pipe.transform(StepMenuItemState.Disabled)).toBeDefined();
      expect(pipe.transform(StepMenuItemState.Done)).toBeDefined();
      expect(pipe.transform(StepMenuItemState.Warning)).toBeDefined();
    });

    it('should return undefined when called with anything else', () => {
      const pipe = new MenuStateIconPipe();
      // @ts-ignore
      expect(pipe.transform('state')).toBeUndefined();
    });
  });
});
