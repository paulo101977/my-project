import { Component  } from '@angular/core';
import { StepMenuItem } from '../models/step-menu-item';
import { StepMenuItemState } from '../models/step-menu-item-state.enum';

@Component({
  selector: 'thx-default-step-menu',
  template: `
    <div class="step-story-container">
      <thx-step-menu [items]="items"></thx-step-menu>
    </div>
  `,
  styleUrls: ['./stories.scss']
})
export class WithSingleItemComponent {
  public items: StepMenuItem[];

  constructor() {
    this.items = [
      {
        label: 'Enabled',
        imageSrc: 'http://google.com/image',
        state: StepMenuItemState.Enabled,
        active: true,
        click: () => { console.log('clicked'); }
      },
    ];
  }
}
