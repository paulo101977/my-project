import { EventEmitter, Injectable } from '@angular/core';
import { StepMenuItem } from '../models/step-menu-item';
import { StepMenuItemState } from '../models/step-menu-item-state.enum';

@Injectable({
  providedIn: 'root'
})
export class StepMenuService {
  private selectedItem: StepMenuItem;
  private items: StepMenuItem[];
  private clickableStates = [
    StepMenuItemState.Enabled,
    StepMenuItemState.Done,
    StepMenuItemState.Warning
  ];

  public updatedItems = new EventEmitter<StepMenuItem[]>();

  constructor() { }

  private updateItems() {
    this.updatedItems.emit([ ...this.items ]);
  }

  public isClickable(item: StepMenuItem): boolean {
    return this.clickableStates.indexOf(item.state) > -1;
  }

  public setItems(items: StepMenuItem[]) {
    if (!items || !items.length) {
      return;
    }

    this.items = items.map((item, index) => {
      const previousClickCallback = item.click;
      const newItem = {
        ...item,
        click: () => {
          this.selectItem(index, previousClickCallback);
        }
      };
      if (newItem.active) {
        this.selectedItem = newItem;
      }

      return newItem;
    });

    this.updateItems();
  }

  public selectItem(itemIndex: number, callback?: Function) {
    const itemToSelect = this.items[itemIndex];
    if (!this.isClickable(itemToSelect)) {
      return;
    }

    if (this.selectedItem) {
      this.selectedItem.active = false;
    }

    this.selectedItem = itemToSelect;

    this.selectedItem.active = true;
    if (callback) {
      callback(this.selectedItem);
    }

    this.updateItems();
  }
}
