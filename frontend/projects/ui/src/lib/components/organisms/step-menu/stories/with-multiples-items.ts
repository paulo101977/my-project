import { Component, OnInit } from '@angular/core';
import { StepMenuItem } from '../models/step-menu-item';
import { StepMenuItemState } from '../models/step-menu-item-state.enum';
import { StepMenuService } from '../services/step-menu.service';

@Component({
  selector: 'thx-default-step-menu',
  template: `
    <div class="step-story-container">
      <thx-step-menu [items]="items"></thx-step-menu>

      <div style="display: flex; margin: 16px;">
        <button (click)="selectItem(0)">Select first</button>
        <button (click)="selectItem(1)">Select second</button>
        <button (click)="selectItem(2)">Select third</button>
        <button (click)="selectItem(3)">Select forth</button>
      </div>
    </div>
  `,
  styleUrls: ['./stories.scss']
})
export class WithMultiplesItemsComponent {
  public items: StepMenuItem[] = [
    {
      label: 'Done',
      imageSrc: 'http://google.com/image',
      state: StepMenuItemState.Done,
      click: (item) => {
        console.log('click 1', item);
      }
    },
    {
      label: 'Enabled',
      imageSrc: 'http://google.com/image',
      state: StepMenuItemState.Enabled,
      active: true,
      click: (item) => {
        console.log('click 2', item);
      }
    },
    {
      label: 'Warning',
      imageSrc: 'http://google.com/image',
      state: StepMenuItemState.Warning,
      click: (item) => {
        console.log('click 3', item);
      }
    },
    {
      label: 'Disabled',
      imageSrc: 'http://google.com/image',
      state: StepMenuItemState.Disabled,
      click: (item) => {
        console.log('click 4', item);
      }
    }
  ];

  constructor(private stepMenuService: StepMenuService) {}

  public selectItem(itemIndex: number) {
    this.stepMenuService.selectItem(itemIndex);
  }
}
