import { Pipe, PipeTransform } from '@angular/core';
import { StepMenuItemState } from '../models/step-menu-item-state.enum';

@Pipe({
  name: 'menuStateIcon'
})
export class MenuStateIconPipe implements PipeTransform {

  private statesToIcon = {
    [StepMenuItemState.Done]: '',
    [StepMenuItemState.Warning]: '',
    [StepMenuItemState.Enabled]: '',
    [StepMenuItemState.Disabled]: '',
  };

  transform(value: StepMenuItemState, args?: any): any {
    return this.statesToIcon[value];
  }

}
