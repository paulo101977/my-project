import { TestBed } from '@angular/core/testing';
import { Subscription } from 'rxjs';
import { StepMenuItem } from '../models/step-menu-item';
import { StepMenuItemState } from '../models/step-menu-item-state.enum';

import { StepMenuService } from './step-menu.service';

describe('StepMenuService', () => {
  let service: StepMenuService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StepMenuService]
    });

    service = TestBed.get(StepMenuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('listen to updated items', () => {
    const mockItems: StepMenuItem[] = [
      {
        click: () => {},
        imageSrc: '',
        label: '',
        state: StepMenuItemState.Done
      },
      {
        click: () => {},
        imageSrc: '',
        label: '',
        state: StepMenuItemState.Warning
      },
      {
        click: () => {},
        imageSrc: '',
        label: '',
        state: StepMenuItemState.Enabled
      },
      {
        click: () => {},
        imageSrc: '',
        label: '',
        state: StepMenuItemState.Disabled
      }
    ];
    let updatedItems: StepMenuItem[] = [];
    const mockFns = {
      onUpdate: (items) => {
        updatedItems = items;
      }
    };
    let subscription: Subscription;

    beforeEach(() => {
      spyOn(mockFns, 'onUpdate').and.callThrough();
      subscription = service.updatedItems.subscribe(mockFns.onUpdate);
    });

    afterEach(() => {
      updatedItems = [];
      subscription.unsubscribe();
    });

    it('#setItems should set the items', function () {
      service.setItems(mockItems);

      expect(mockFns.onUpdate).toHaveBeenCalled();
      expect(updatedItems.length).toBe(mockItems.length);
    });

    it('#setItems should replace existing items', function () {
      const newMockItems: StepMenuItem[] = [
        {
          imageSrc: '',
          label: '',
          state: StepMenuItemState.Enabled,
          click: () => {}
        }
      ];
      let previousItems;
      service.setItems(mockItems);
      previousItems = [ ...updatedItems ];
      service.setItems(newMockItems);

      expect(mockFns.onUpdate).toHaveBeenCalledTimes(2);
      expect(updatedItems.length).toBe(newMockItems.length);
      expect(updatedItems).not.toEqual(previousItems);
    });

    it('#selectItem should break when selecting without items', () => {
      try {
        service.selectItem(0);
      } catch (error) {
        expect(error.message).toBe('Cannot read property \'0\' of undefined');
      }
    });

    it('#selectItem should select an existing item', () => {
      service.setItems(mockItems);
      service.selectItem(0);
      expect(updatedItems[0].active).toBe(true);
    });

    it('#selectItem should deactivate a previous selected item', () => {
      service.setItems(mockItems);
      service.selectItem(0);
      service.selectItem(1);
      expect(updatedItems[0].active).toBe(false);
      expect(updatedItems[1].active).toBe(true);
    });

    it('should select the clicked item', () => {
      service.setItems(mockItems);
      expect(service['items'].length).toBe(4);
      expect(updatedItems[0].active).toBeUndefined();
      updatedItems[0].click();
      expect(updatedItems[0].active).toBe(true);
    });

    it('shouldn\'t click or select an disabled item', () => {
      const disabledItemIndex = 3;
      service.setItems(mockItems);
      service.selectItem(disabledItemIndex);
      updatedItems[disabledItemIndex].click();
      expect(updatedItems[disabledItemIndex].active).toBeUndefined();
    });
  });
});
