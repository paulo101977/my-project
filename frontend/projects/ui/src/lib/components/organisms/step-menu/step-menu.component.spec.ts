import { Component, EventEmitter, NO_ERRORS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StepMenuItem } from './models/step-menu-item';
import { StepMenuItemState } from './models/step-menu-item-state.enum';
import { StepMenuService } from './services/step-menu.service';

import { StepMenuComponent } from './step-menu.component';

describe('StepMenuComponent', () => {
  let component: StepMenuComponent;
  let fixture: ComponentFixture<StepMenuComponent>;
  let stepMenuServiceSpy: any;
  const mockDoneItem: StepMenuItem = {
    state: StepMenuItemState.Done,
    label: '',
    imageSrc: '',
    click: () => {}
  };
  const mockWarningItem = {
    ...mockDoneItem,
    state: StepMenuItemState.Warning
  };
  const mockEnabledItem = {
    ...mockDoneItem,
    state: StepMenuItemState.Enabled
  };
  const mockDisabledItem = {
    ...mockDoneItem,
    state: StepMenuItemState.Disabled
  };

  @Component({selector: 'thx-img', template: ''})
  class ImgStubComponent {}

  @Pipe({ name: 'menuStateIcon' })
  class MenuStateIconStubPipe implements PipeTransform {
    transform(value: any, ...args: any[]): any {}
  }

  beforeEach(async(() => {
    const spy = jasmine.createSpyObj('StepMenuService', ['isClickable', 'setItems']);

    TestBed.configureTestingModule({
      declarations: [
        StepMenuComponent,
        ImgStubComponent,
        MenuStateIconStubPipe
      ],
      providers: [
        { provide: StepMenuService, useValue: spy }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();

    stepMenuServiceSpy = TestBed.get(StepMenuService);
    stepMenuServiceSpy.updatedItems = new EventEmitter<StepMenuItem[]>();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#isClickable should return stubbed value from a spy', () => {
    const stubValue = true;
    stepMenuServiceSpy.isClickable.and.returnValue(stubValue);
    expect(component.isClickable(mockDoneItem)).toBe(stubValue);
  });

  describe('#isActive', () => {
    it('#isActive should return true when active', () => {
      const activeItem = {
        ...mockDoneItem,
        active: true
      };
      expect(component.isActive(activeItem)).toBe(true);
    });

    it('#isActive should return false when inactive', () => {
      expect(component.isActive(mockDoneItem)).toBe(false);
    });
  });

  describe('#isDone', () => {
    it('#isDone should return true when state is Done', () => {
      expect(component.isDone(mockDoneItem)).toBe(true);
    });

    it('#isDone should return false when state other than Done', () => {
      expect(component.isDone(mockWarningItem)).toBe(false);
      expect(component.isDone(mockEnabledItem)).toBe(false);
      expect(component.isDone(mockDisabledItem)).toBe(false);
    });
  });

  describe('#isWarning', () => {
    it('should return true when state is Warning', () => {
      expect(component.isWarning(mockWarningItem)).toBe(true);
    });

    it('should return false when state other than Warning', () => {
      expect(component.isWarning(mockDoneItem)).toBe(false);
      expect(component.isWarning(mockEnabledItem)).toBe(false);
      expect(component.isWarning(mockDisabledItem)).toBe(false);
    });
  });

  describe('#shouldShowDivider', () => {
    it('should return true when there are items and item isn\'t the last one', () => {
      component.items = [
        mockDoneItem,
        mockDoneItem,
        mockDoneItem,
      ];
      expect(component.shouldShowDivider(1)).toBe(true);
    });

    it('should return false when there are items and item is the last one', () => {
      component.items = [
        mockDoneItem,
        mockDoneItem,
        mockDoneItem,
      ];
      expect(component.shouldShowDivider(2)).toBe(false);
    });

    it('should return false when there are no items', () => {
      component.items = [];
      expect(component.shouldShowDivider(1)).toBe(false);
    });
  });
});
