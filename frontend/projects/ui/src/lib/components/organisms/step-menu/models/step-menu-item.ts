import { StepMenuItemState } from './step-menu-item-state.enum';

export class StepMenuItem {
  public label: string;
  public imageSrc: string;
  public state: StepMenuItemState;
  public active?: boolean;
  public click: Function;
}
