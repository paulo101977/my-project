import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-step-menu',
  template: `
    <div class="step-story-container">
      <thx-step-menu></thx-step-menu>
    </div>
  `,
  styleUrls: ['./stories.scss']
})
export class DefaultComponent {
}
