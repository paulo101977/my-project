import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { StepMenuItem } from './models/step-menu-item';
import { StepMenuItemState } from './models/step-menu-item-state.enum';
import { StepMenuService } from './services/step-menu.service';

@Component({
  selector: 'thx-step-menu',
  templateUrl: './step-menu.component.html',
  styleUrls: ['./step-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StepMenuComponent implements OnInit, OnChanges, OnDestroy {
  @Input() items: StepMenuItem[];
  public menuItems: StepMenuItem[];

  private updatedItemsSub: Subscription;

  constructor(private stepMenuService: StepMenuService) {
    this.listenToUpdatedItems();
  }

  ngOnInit(): void {
    this.setItems(this.items);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.items) {
      this.setItems(this.items);
    }
  }

  ngOnDestroy(): void {
    this.updatedItemsSub.unsubscribe();
  }

  public isActive(item: StepMenuItem): boolean {
    return !!item.active;
  }

  public isDone(item: StepMenuItem): boolean {
    return item.state === StepMenuItemState.Done;
  }

  public isWarning(item: StepMenuItem): boolean {
    return item.state === StepMenuItemState.Warning;
  }

  public isClickable(item: StepMenuItem): boolean {
    return this.stepMenuService.isClickable(item);
  }

  public shouldShowDivider(itemIndex): boolean {
    return this.hasMoreThanOneItem() && !this.isLastItem(itemIndex);
  }

  private isLastItem(itemIndex): boolean {
    const lastItemIndex = this.items.length - 1;
    return lastItemIndex === itemIndex;
  }

  private hasMoreThanOneItem(): boolean {
    return this.items.length > 1;
  }

  private listenToUpdatedItems() {
    this.updatedItemsSub = this.stepMenuService.updatedItems
      .subscribe((items) => {
        this.menuItems = items;
      });
  }

  private setItems(items) {
    this.stepMenuService.setItems(items);
  }
}
