export enum StepMenuItemState {
  Enabled = 0,
  Disabled = 1,
  Done = 2,
  Warning = 3
}
