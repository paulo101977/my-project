import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';
import { StepMenuComponent } from './step-menu.component';
import { MenuStateIconPipe } from './pipes/menu-state-icon.pipe';

@NgModule({
  imports: [
    CommonModule,
    ThxImgModule
  ],
  declarations: [StepMenuComponent, MenuStateIconPipe],
  exports: [StepMenuComponent]
})
export class StepMenuModule { }
