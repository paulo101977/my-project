import { Injectable } from '@angular/core';
import { IMyDate, IMyDpOptions } from 'mydatepicker';

// ISSUE: https://github.com/jvandemo/generator-angular2-library/issues/221
import * as moment_ from 'moment';
const moment = moment_;

import { ThxInputRangePickerV2Component } from '../thx-input-range-picker-v2.component';

@Injectable({
  providedIn: 'root'
})
export class ThxInputRangePickerV2Service {
  public static DATE_FORMAT_UNIVERSAL = 'YYYY-MM-DDTHH:mm:ss';


  public convertDateToStringWithFormat(date: Date, format: string) {
    if (typeof date === 'object') {
      return moment(date).format(format);
    }
    return null;
  }

  public convertUniversalDateToIMyDate(universalDate: string) {
    const momentDate = moment(
        universalDate,
        ThxInputRangePickerV2Service.DATE_FORMAT_UNIVERSAL
    ).toDate();
    let dateValue;
    if (momentDate) {
      dateValue = {
        date: <IMyDate>{
          day: momentDate.getDate(),
          month: momentDate.getMonth() + 1,
          year: momentDate.getFullYear(),
        },
      };
      return dateValue;
    }
    return null;
  }

  public getClearConfigDatePicker(): IMyDpOptions {
    return {
      dateFormat: 'dd/mm/yyyy',
      showClearDateBtn: false,
      height: '44px',
      markCurrentDay: true,
      editableDateField: false,
      indicateInvalidDate: true,
      openSelectorOnInputClick: true,
    };
  }

  public configDatePicker(instance: ThxInputRangePickerV2Component) {
    const {
      dateToday,
      minDateToday,
      maxDateToday,
      disableUntil,
      disableSince
    } = instance;

    instance.myDatePickerOptions = this.getClearConfigDatePicker();
    instance.myDatePickerOptions.showInputField = true;
    instance.myDatePickerOptions.editableDateField = true;
    instance.myDatePickerOptions.indicateInvalidDate = true;
    instance.myDatePickerOptions.showClearDateBtn = instance.showClearDateBtn;

    /**
    * The dateToday variable sets the current day and can also be used to set the system date in the range picker.
    */
    if ( dateToday ) {
      const propertyDateTodayMoment = moment(dateToday);
      const propertyDateTodayIMyDate = <IMyDate>{
        day: +propertyDateTodayMoment.subtract(1, 'days').format('D'),
        month: +propertyDateTodayMoment.format('M'),
        year: +propertyDateTodayMoment.format('YYYY'),
      };

      if (minDateToday) {
        instance.myDatePickerOptions.disableUntil = propertyDateTodayIMyDate;
      } else if (maxDateToday) { // disable the calendar after today
        instance.myDatePickerOptions.disableSince = propertyDateTodayIMyDate;
      }
    }


    // disable functions
    if (disableUntil) {
      instance.myDatePickerOptions.disableUntil = disableUntil;
    }
    if (disableSince) { // disable the calendar after the date provide
      instance.myDatePickerOptions.disableSince = disableSince;
    }
  }
}
