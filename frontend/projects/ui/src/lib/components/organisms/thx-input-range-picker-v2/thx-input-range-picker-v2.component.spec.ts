import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxInputRangePickerV2Component } from './thx-input-range-picker-v2.component';
import { configureTestSuite } from 'ng-bullet';

describe('ThxInputRangePickComponent', () => {
  let component: ThxInputRangePickerV2Component;
  let fixture: ComponentFixture<ThxInputRangePickerV2Component>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxInputRangePickerV2Component ]
    });

    fixture = TestBed.createComponent(ThxInputRangePickerV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
