import { CommonModule } from '@angular/common';
import { storiesOf, moduleMetadata  } from '@storybook/angular';


import { Notes } from './notes';
import { componentFactory } from '.storybook/component-factory';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThxInputRangePickerV2Module } from '../thx-input-range-picker-v2.module';
import { TranslateModule } from '@ngx-translate/core';
import { DisabledMinAndMaxComponent } from './disabled';

const form = new FormBuilder().group({
    value: null,
    value2: null,
});

// params: selector, template, scope
const DefaultComponent = componentFactory('default', `
    <br>
    <br>
    <br>
    <br>
    <form [formGroup]="scope.form">
        <thx-input-range-picker-v2
              [id]="'period'"
              [title]="'data'"
              [formControlName]="'value'"
              required="true">
        </thx-input-range-picker-v2>
        <br>
        <br>
        <thx-input-range-picker-v2
              [id]="'period'"
              [title]="'data'"
              [formControlName]="'value2'"
              required="true">
        </thx-input-range-picker-v2>
    </form>

    <div><span>Date :</span> {{ scope.form.value | json }}</div>
`, {form});



storiesOf('Ui | Organisms/ThxInputRangePickerV2Component', module)
    .addDecorator(
        moduleMetadata({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                ThxInputRangePickerV2Module,
                TranslateModule.forRoot(),
            ],
            declarations: [
            ],
        })
    )
    .add('Default', () => ({
            component: DefaultComponent
        }),
        { notes: { markdown: new Notes().getNotes() }})
    .add('DisabledMinAndMax', () => ({
            component: DisabledMinAndMaxComponent
        }),
        { notes: { markdown: new Notes().getNotes() }});
