import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    ThxInputRangePickerV2Component
} from './thx-input-range-picker-v2.component';
import { ThxInputV2Module } from '../../molecules/thx-input-v2/thx-input-v2.module';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { FormsModule } from '@angular/forms';



@NgModule({
    imports: [
        CommonModule,
        ThxInputV2Module,
        MyDateRangePickerModule,
        FormsModule,
    ],
    declarations: [
        ThxInputRangePickerV2Component,
    ],
    entryComponents: [
        ThxInputRangePickerV2Component,
    ],
    exports: [
        ThxInputRangePickerV2Component,
    ]
})
export class ThxInputRangePickerV2Module { }
