import {
  Attribute,
  Component,
  forwardRef,
  Input, OnChanges,
  OnInit, SimpleChanges,
  ViewChild
} from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IMyDate, IMyDpOptions } from 'mydatepicker';
import { IMyDrpOptions, MyDateRangePicker } from 'mydaterangepicker';
import { ThxInput } from '../../molecules/thx-input-v2/thx-input-base/thx-input';
import { ThxInputRangePickerV2Service } from './services/thx-input-range-picker-v2.service';


@Component({
  selector: 'thx-input-range-picker-v2',
  templateUrl: './thx-input-range-picker-v2.component.html',
  styleUrls: ['./thx-input-range-picker-v2.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ThxInputRangePickerV2Component),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ThxInputRangePickerV2Component),
      multi: true
    }
  ],
})
export class ThxInputRangePickerV2Component extends ThxInput implements OnInit, OnChanges {

  public startDateStr: string;
  public endDateStr: string;

  private date: Date;

  constructor(
      @Attribute('required') public required,
      private thxInputRangePickerV2Service: ThxInputRangePickerV2Service
  ) {
    super(required);
  }

  @ViewChild('dateElement') dateElement: MyDateRangePicker;

  // common
  @Input() id: string;

  // datepicker config
  @Input() myDatePickerOptions: IMyDpOptions;
  @Input() disableUntil: IMyDate;
  @Input() disableSince: IMyDate;
  @Input() minDateToday: boolean;
  @Input() maxDateToday: boolean;
  @Input() showClearDateBtn = false;
  /**
  * The dateToday variable sets the current day and can also be used to set the system date in the range picker.
  */
  @Input() dateToday: string;

  ngOnInit() {
    this.thxInputRangePickerV2Service.configDatePicker(this);
  }

  writeValue(myDate: any): void {
    let propagateValue = null;

    this.inFocus = false;

    if ( myDate ) {
      if (myDate['beginJsDate'] && myDate['endJsDate'] ) {
        propagateValue = {
          beginDate: this.thxInputRangePickerV2Service.convertDateToStringWithFormat(
              myDate.beginJsDate,
              ThxInputRangePickerV2Service.DATE_FORMAT_UNIVERSAL
          ),
          endDate: this.thxInputRangePickerV2Service.convertDateToStringWithFormat(
              myDate.endJsDate,
              ThxInputRangePickerV2Service.DATE_FORMAT_UNIVERSAL
          )
        };
      }
    }

    if (propagateValue) {
      this.value = {
        beginDate: this.thxInputRangePickerV2Service.convertUniversalDateToIMyDate(propagateValue.beginDate).date,
        endDate: this.thxInputRangePickerV2Service.convertUniversalDateToIMyDate(propagateValue.endDate).date
      };

      this.changeDisplayValues(this.value.beginDate, this.value.endDate);
    } else {
      this.value = null;
      this.startDateStr = '';
      this.endDateStr = '';
    }
    this.onChange(propagateValue);
  }

  private changeDisplayValues(begin: IMyDate, end: IMyDate) {
    const { padStart } = this;
    this.startDateStr = `${padStart(begin.day, 2)}/${padStart(begin.month, 2)}/${padStart(begin.year, 2)}`;
    this.endDateStr = `${padStart(end.day, 2)}/${padStart(end.month, 2)}/${padStart(end.year, 2)}`;
  }

  public padStart = function padStart(target: number, targetLength: number, padString = '0') {
    const targetStr = target.toString();
    padString = String(typeof padString !== 'undefined' ? padString : ' ');
    if (targetStr.length >= targetLength) {
      return targetStr;
    } else {
      targetLength = targetLength - targetStr.length;
      if (targetLength > padString.length) {
        padString += padString.repeat(targetLength / padString.length);
      }
      return padString.slice(0, targetLength) + targetStr;
    }
  };

  ngOnChanges(changes: SimpleChanges): void {
    if ( this.myDatePickerOptions ) {
      this.thxInputRangePickerV2Service.configDatePicker(this);
    }
  }
}
