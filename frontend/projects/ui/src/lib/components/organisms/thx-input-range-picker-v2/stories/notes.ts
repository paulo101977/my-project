export class Notes {
    private notes = `
## Html Usage Example:

~~~html
<form [formGroup]="form">
    <thx-input-range-picker-v2
          [id]="'period'"
          [title]="{{ data | translate }}"
          [formControlName]="'period'"
          required="true">
    </thx-input-range-picker-v2>
</form>
~~~

## Typescript Usage Example:

~~~js

@Component({
  selector: 'thx-implement',
  templateUrl: './thx-implement.component.html',
  styleUrls: ['./thx-implement.component.scss'],
})
export class ThxImplementComponent implements OnInit {

  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
        period: null
    );
  }
}

~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
