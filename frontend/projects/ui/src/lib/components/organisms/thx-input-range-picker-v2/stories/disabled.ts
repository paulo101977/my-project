import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ThxInputRangePickerV2Service } from '../services/thx-input-range-picker-v2.service';
import { IMyDate } from 'mydatepicker';

@Component({
    selector: 'thx-disabled-component-range-picker',
    template: `
        <form [formGroup]="form">
            <br>
            <br>
            <thx-input-range-picker-v2
                    [minDateToday]="true"
                    [id]="'period'"
                    [title]="'data'"
                    [dateToday]="dateToday"
                    [formControlName]="'period'"
                    [disableSince]="disableSince"
                    [disableUntil]="disableUntil"
                    required="true">
            </thx-input-range-picker-v2>
        </form>
    `
})
export class DisabledMinAndMaxComponent implements OnInit {
    public form;
    public disableSince: IMyDate;
    public disableUntil: IMyDate;
    public dateToday: string;

    constructor(
        private fb: FormBuilder,
        private rangePickerService: ThxInputRangePickerV2Service,
    ) {
    }

    ngOnInit(): void {
        this.form = this.fb.group({
            period: [{
                beginJsDate: new Date('2019-09-28T12:00:00'),
                endJsDate: new Date('2019-10-10T12:00:00'),
            }, Validators.required],
        });
        this.disableUntil = this.rangePickerService.convertUniversalDateToIMyDate('2019-09-27T12:00:00').date;
        this.disableSince = this.rangePickerService.convertUniversalDateToIMyDate('2019-10-11T12:00:00').date;
    }
}
