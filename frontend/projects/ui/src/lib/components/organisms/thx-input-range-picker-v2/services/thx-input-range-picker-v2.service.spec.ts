import { TestBed } from '@angular/core/testing';

import { ThxInputRangePickerV2Service } from './thx-input-range-picker-v2.service';

describe('ThxInputRangePickerV2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ThxInputRangePickerV2Service = TestBed.get(ThxInputRangePickerV2Service);
    expect(service).toBeTruthy();
  });
});
