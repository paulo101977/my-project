import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { TextButtonComponent } from './text-button.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule.forChild(),
  ],
  declarations: [
    TextButtonComponent
  ],
  exports: [
    TextButtonComponent
  ]
})
export class TextButtonModule { }
