import { EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { DayPilotSchedulerComponent } from 'daypilot-pro-angular';
import {
  Availability,
  Footer,
  Image,
  AvailabilityGridLabels,
  Reservation,
  Room,
  RoomBlock,
  TimeRange, Property, RoomType,
} from '../thx-availability-grid/thx-availability-grid.model';

/**
 * @description
 * O componente thx-availability-grid é o responsável por renderizar o grid de disponibilidade.
 *
 */
export class ThxMultiPropertyAvailabilityGridBaseComponent {
  // Daypilot params
  @Input() public startDate: string;

  @Input() public days: number;

  // Lists
  @Input() public reservationList: Reservation[] = [];

  @Input() public propertyList: Property[] = [];

  @Input() public availabilityList: Availability[] = [];

  @Input() public footerList: Footer[] = [];

  @Input() public blockedRoomList: RoomBlock[] = [];

  public roomList: Room[] = [];
  public roomTypeList: RoomType[] = [];

  // Labels/icons
  @Input() public labels: AvailabilityGridLabels;

  @Input() public bedTypeIcons: Image[];

  // Validation callback
  @Input() public validateReservationMove: (reservation: Reservation) => Observable<boolean> | Promise<boolean>;

  // Reservation click event
  @Output() public reservationClick: EventEmitter<Reservation> = new EventEmitter<Reservation>();

  // RateProposal click event
  @Output() public rateProposalClick: EventEmitter<TimeRange> = new EventEmitter<Reservation>();

  // New Reservation click event
  @Output() public newReservationClick: EventEmitter<TimeRange> = new EventEmitter<TimeRange>();

  // Block room click event
  @Output() public blockRoomClick: EventEmitter<TimeRange> = new EventEmitter<TimeRange>();

  // Unlock room click event
  @Output() public unlockRoomClick: EventEmitter<TimeRange> = new EventEmitter<TimeRange>();

  // Event emitted when navigating
  @Output() public initialDateChange: EventEmitter<string> = new EventEmitter<string>();

  // RoomType expand event
  @Output() public roomTypeExpanded: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('scheduler') public scheduler: DayPilotSchedulerComponent;

  public resourceList: Property[] = [];

  public config: any;

  protected daypilotDateFormat: string;
  protected momentDateFormat: string;

  public selectedReservation: Reservation;

  public selectedTimeRange: TimeRange;
}
