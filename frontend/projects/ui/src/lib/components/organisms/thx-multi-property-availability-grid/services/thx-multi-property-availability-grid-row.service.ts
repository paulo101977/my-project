import { Injectable } from '@angular/core';
import { FooterType, RoomStatusEnum, Row } from '../../thx-availability-grid/thx-availability-grid.model';

@Injectable({ providedIn: 'root' })
export class ThxMultiPropertyAvailabilityGridRowService {
  constructor() {}

  public setRowTotalRoomsBadge(row: Row): void {
    if (row.data.total !== undefined && row.data.total !== null && row.id != FooterType.availablePercentage) {
      row.html += `<div class="badge">${row.data.total}</div>`;
    }
    // todo uncomment when bug currected
    // if ( typeof row.data.id === 'string' && row.data.id.indexOf('property') >= 0) {
    //   row.html += `<div class="grid-ocuppation"><small>Ocupação %</small></div>`;
    // }
  }

  public setRowStatusCssClass(row: Row): void {
    switch (row.data.status) {
      case RoomStatusEnum.Clean:
        row.classes.push('clean');
        break;
      case RoomStatusEnum.Maintenace:
        row.classes.push('clean');
        break;
      case RoomStatusEnum.Dirty:
        row.classes.push('dirty');
        break;
    }
  }

  public setRowAsParent(row: Row): void {
    if (!row.parent()) {
      row.classes.push('parent');
    }
  }

  public setRowAsFooter(row: Row): void {
    row.classes.push('footer');

    if (row.id.toString() === FooterType.availablePercentage) {
      row.classes.push('available-percentage');
    }
  }

  public setRowBedIcon(row: Row): void {
    row.html += row.data.bedIcon ? `<img class="bed-icon" src="${row.data.bedIcon.src}" alt="${row.data.bedIcon.alt}"/>` : '';
  }
}
