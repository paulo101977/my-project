// Angular imports
import { Injectable } from '@angular/core';
// Models
import {
  Availability,
  BedTypeEnum,
  Cell,
  Footer,
  FooterType,
  Image,
  AvailabilityGridLabels,
  Reservation,
  ReservationStatusEnum,
  Room,
  RoomType, Property,
} from '../../thx-availability-grid/thx-availability-grid.model';
import { AssetsCdnService } from '../../../../services/assets-service/assets-cdn.service';

// Mocks
@Injectable({ providedIn: 'root' })
export class ThxMultiPropertyAvailabilityGridService {
  constructor( private assetsService: AssetsCdnService) {}

  public mapReservationList(reservationList: Reservation[]): Reservation[] {
    const conjugatedReservations = reservationList.filter(reservation => reservation.isConjugated);

    return reservationList.map(reservation => {
      reservation.resource = this.getRoomTypeId(reservation.roomTypeId);
      reservation.resizeDisabled = true;
      reservation.barHidden = true;

      if (reservation.status > ReservationStatusEnum.Confirmed || reservation.isConjugated) {
        reservation.moveHDisabled = true;
        reservation.moveVDisabled = true;
      }

      if (reservation.isConjugated) {
        reservation.id = reservation.id + '.' + reservation.roomId;
        reservation.join = conjugatedReservations.find(
          conjugated => conjugated.reservationId === reservation.reservationId && conjugated.roomId !== reservation.roomId,
        ).reservationId;
      }

      reservation.cssClass = this.getReservationClass(reservation.status);

      return reservation;
    });
  }

  public getReservationClass(reservationStatus: ReservationStatusEnum) {
    switch (reservationStatus) {
      case ReservationStatusEnum.Checkin:
        return 'status-checkin';
      case ReservationStatusEnum.Checkout:
        return 'status-checkout';
      case ReservationStatusEnum.ToConfirm:
        return 'status-to-confirm';
      case ReservationStatusEnum.Confirmed:
        return 'status-confirmed';
    }
  }

  public getReservationStatusName(reservationStatus: ReservationStatusEnum, labels: AvailabilityGridLabels) {
    switch (reservationStatus) {
      case ReservationStatusEnum.Checkin:
        return labels.checkin;
      case ReservationStatusEnum.Checkout:
        return labels.checkout;
      case ReservationStatusEnum.ToConfirm:
        return labels.toConfirm;
      case ReservationStatusEnum.Confirmed:
        return labels.confirmed;
    }
  }

  public mapAvailabilityList(propertyList: Property[], availabilityList: Availability[], footerList: Footer[]) {
    const resultList = [];

    for (const availability of availabilityList) {
      availability.propertyId = this.getPropertyId(availability.propertyId);
      availability.roomTypeId = this.getRoomTypeId(availability.roomTypeId);

      const resource = propertyList.find(resourceItem => availability.propertyId === resourceItem.id);

      // Insert RoomType availability info
      resultList.push(availability);

      /*
       * When a RoomType has overbook, insert it's child in
       * the list, in order to have the overbook style propagated
       */
      if (availability.availableRooms < 0 && resource && resource.children) {
        for (const child of resource.children) {
          resultList.push({
            resourceId: child.id,
            date: availability.date,
            availableRooms: availability.availableRooms,
          });
        }
      }
    }

    for (const footer of footerList) {
      for (const footerValue of footer.values) {
        resultList.push({
          resourceId: footer.id,
          date: footerValue.date,
          availableRooms: footerValue.number,
        });
      }
    }

    return resultList;
  }

  public mapResourceList(propertyList: Property[], footerList: Footer[]) {
    const resourceList = propertyList.map(property => {
      const resource: Property = Object.assign({}, property);
      resource.id = this.getPropertyId(resource.id);
      if (resource.children) {
        resource.children.map(child => (child.id = this.getRoomTypeId(child.id)));
      } else {
        resource.children = [{ id: '', name: '' }];
      }

      return resource;
    });

    for (const footer of footerList) {
      resourceList.push(footer);
    }

    return resourceList;
  }

  public checkOverbook(availabilityList: Availability[], cell: Cell) {
    return availabilityList.find(
      item => item.date === cell.start.toString('yyyy-MM-dd') && item.roomTypeId === cell.resource && item.availableRooms < 0,
    );
  }

  public getBedIcon(bed: BedTypeEnum): Image {
    switch (bed) {
      case BedTypeEnum.Double:
        return { src: this.assetsService.getImgUrlTo('camaCasal.min.svg'), alt: 'commomData.images.doubleBed' };
      case BedTypeEnum.Single:
        return { src: this.assetsService.getImgUrlTo('camaSolteiro.min.svg'), alt: 'commomData.images.singleBed' };
      case BedTypeEnum.Reversible:
        return { src: this.assetsService.getImgUrlTo('camaReversivel.min.svg'), alt: 'commomData.images.reversibleBed' };
      default:
        return { src: '', alt: '' };
    }
  }

  public checkIsFooter(resource: number | string) {
    return (
      resource &&
      (resource.toString() === FooterType.total ||
        resource.toString() === FooterType.availablePercentage ||
        resource.toString() === FooterType.blockedRooms)
    );
  }

  public getRoomTypeId(id: string | number) {
    if (typeof id != 'string' || typeof id == 'string' && id.indexOf('roomType-') < 0) {
      return 'roomType-' + id;
    } else {
      return id;
    }
  }

  public getPropertyId(id: string | number) {
    if (typeof id != 'string' || typeof id == 'string' && id.indexOf('property-') < 0) {
      return 'property-' + id;
    } else {
      return id;
    }
  }

  public mapRoomList(roomTypeId: number, roomList: Room[]) {
    return roomList.map(room => {
      room.id = this.getRoomTypeId(room.roomTypeId);
      room.roomTypeId = roomTypeId;
      return room;
    });
  }

  public mapRoomTypeList(propertyId: number, roomTypeList: RoomType[], propertyName = '') {
    return roomTypeList.map(room => {
      room.id = this.getRoomTypeId(room.id);
      room.propertyId = propertyId;
      room.propertyName = propertyName;
      return room;
    });
  }
}
