import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DayPilotModule } from 'daypilot-pro-angular';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';
import { ThxMultiPropertyAvailabilityGridComponent } from './thx-multi-property-availability-grid.component';
import { MomentModule } from 'ngx-moment';
import { ThxMultiPropertyAvailabilityGridService } from './services/thx-multi-property-availability-grid.service';
import { ThxMultiPropertyAvailabilityGridRowService } from './services/thx-multi-property-availability-grid-row.service';
import { ThxMultiPropertyAvailabilityGridCellService } from './services/thx-multi-property-availability-grid-cell.service';
import { ThxMultiPropertyAvailabilityGridHeaderService } from './services/thx-multi-property-availability-grid-header.service';
import { ThxPopoverModule } from '../thx-popover/thx-popover.module';
import { AssetsServiceModule } from '../../../services/assets-service/assets-service.module';

@NgModule({
  imports: [
    CommonModule,
    DayPilotModule,
    ThxPopoverModule,
    MomentModule,
    AssetsServiceModule,
    ImgCdnModule,
  ],
  declarations: [ThxMultiPropertyAvailabilityGridComponent],
  exports: [ThxMultiPropertyAvailabilityGridComponent],
  providers: [
    ThxMultiPropertyAvailabilityGridService,
    ThxMultiPropertyAvailabilityGridRowService,
    ThxMultiPropertyAvailabilityGridCellService,
    ThxMultiPropertyAvailabilityGridHeaderService
  ],
})
export class ThxMultiPropertyAvailabilityGridModule {
}
