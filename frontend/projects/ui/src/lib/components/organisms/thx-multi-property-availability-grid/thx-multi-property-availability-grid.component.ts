
import {finalize} from 'rxjs/operators';
import {
  ApplicationRef,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  OnChanges,
  Renderer2,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { ThxMultiPropertyAvailabilityGridBaseComponent } from './thx-multi-property-availability-grid-base.component';
import { ThxPopoverComponent } from '../thx-popover/thx-popover.component';
import { ThxMultiPropertyAvailabilityGridService } from './services/thx-multi-property-availability-grid.service';
import { DayPilot } from 'daypilot-pro-angular';
import { Observable } from 'rxjs';

import { Cell, Reservation, Room, RoomType, Row, TimeRange } from '../thx-availability-grid/thx-availability-grid.model';
import { ThxMultiPropertyAvailabilityGridCellService } from './services/thx-multi-property-availability-grid-cell.service';
import { ThxMultiPropertyAvailabilityGridRowService } from './services/thx-multi-property-availability-grid-row.service';
import { ThxMultiPropertyAvailabilityGridHeaderService } from './services/thx-multi-property-availability-grid-header.service';
import * as momentNs from 'moment';
import { AssetsCdnService } from '../../../services/assets-service/assets-cdn.service';
const moment = momentNs;

@Component({
  selector: 'thx-multi-property-availability-grid',
  templateUrl: './thx-multi-property-availability-grid.component.html',
  styleUrls: ['./thx-multi-property-availability-grid.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ThxMultiPropertyAvailabilityGridComponent extends ThxMultiPropertyAvailabilityGridBaseComponent implements OnChanges {
  @ViewChild('popoverReservationInfo') public popoverReservationInfo: TemplateRef<any>;

  @ViewChild('popoverSelectedPeriod') public popoverSelectedPeriod: TemplateRef<any>;

  private popoverContainer: ThxPopoverComponent;

  private isPopoverShown: boolean;

  private globalPopoverClick: any;

  constructor(
    private gridService: ThxMultiPropertyAvailabilityGridService,
    private cellService: ThxMultiPropertyAvailabilityGridCellService,
    private rowService: ThxMultiPropertyAvailabilityGridRowService,
    private headerService: ThxMultiPropertyAvailabilityGridHeaderService,
    public viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    public appRef: ApplicationRef,
    public elementRef: ElementRef,
    public renderer: Renderer2,
    private assetsCdnService: AssetsCdnService,
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.config) {
      this.updateLists(changes);
    } else if (this.labels) {
      this.setVars();
    }
  }

  setVars(): void {
    this.days = this.days || 7;
    this.momentDateFormat = 'YYYY-MM-DD';
    this.daypilotDateFormat = 'yyyy-MM-dd';

    this.setConfig({
      days: this.days,
      startDate: new DayPilot.Date(this.startDate),
    });
  }

  updateLists(changes: SimpleChanges): void {
    if (changes.propertyList || changes.footerList) {
      const footerList = changes.footerList ? changes.footerList.currentValue : this.footerList;
      const propertyList = changes.propertyList ? changes.propertyList.currentValue : this.propertyList;

      this.resourceList = this.gridService.mapResourceList(propertyList, footerList);
      this.footerList = footerList;
      this.config.resources = this.resourceList;
    }

    if (changes.availabilityList) {
      this.availabilityList = this.gridService.mapAvailabilityList(this.resourceList, this.availabilityList, this.footerList);
    }

    if (changes.reservationList) {
      this.reservationList = this.gridService.mapReservationList(changes.reservationList.currentValue);
      this.scheduler.control.events.list = <any>this.reservationList;
    }

    if (changes.days) {
      this.scheduler.control.days = changes.days.currentValue;
      this.config.days = this.scheduler.control.days;
    }

    if (changes.startDate) {
      this.scheduler.control.startDate = new DayPilot.Date(changes.startDate.currentValue);
      this.config.startDate = this.scheduler.control.startDate;
    }
  }

  setRoomTypes(propertyId: number, roomTypeList: RoomType[]) {
    const index = this.config.resources.findIndex(resource => resource.id === this.gridService.getPropertyId(propertyId));
    this.roomTypeList = this.gridService.mapRoomTypeList(propertyId, roomTypeList, this.config.resources[index].name);
    this.config.resources[index].children = this.roomTypeList;
  }


  setConfig(params: any): void {
    this.config = {
      timeHeaders: [{ groupBy: 'Day', format: 'd' }],
      eventHeight: 37,
      headerHeight: 60,
      jointEventsMove: true,
      scale: 'Day',
      days: params.days,
      startDate: params.startDate,
      treeEnabled: true,
      treeIndent: 5,
      treeImageMarginLeft: 10,
      treeImageMarginTop: 4,
      useEventBoxes: 'Never',
      allowEventOverlap: false,
      cellWidthSpec: 'Auto',
      rowMarginTop: 5,
      rowMarginBottom: 5,
      rowHeaderWidth: 125,
      rowHeaderScrolling: false,
      treeAutoExpand: false,
      cornerHtml: this.labels.cornerLabel,
      timeRangeSelectedHandling: 'Callback',
      onEventMove: this.onEventMove,
      onEventMoving: this.onEventMoving,
      onBeforeRowHeaderRender: this.onBeforeRowHeaderRender,
      onBeforeTimeHeaderRender: this.onBeforeTimeHeaderRender,
      onResourceExpand: this.onResourceExpand,
      onRowClick: this.onRowClick,
      onBeforeCellRender: this.onBeforeCellRender,
      onBeforeEventRender: this.onBeforeEventRender,
      onTimeRangeSelect: this.onTimeRangeSelect,
      onTimeRangeClick: this.onTimeRangeClick,
      onTimeRangeSelected: this.onTimeRangeSelected,
      onTimeRangeSelecting: this.onTimeRangeSelecting,
    };
  }

  onEventMove = args => {
    const reservation: Reservation = Object.assign({}, args.e.data);
    reservation.id = reservation.reservationId;
    reservation.start = args.newStart;
    reservation.end = args.newEnd;

    if (reservation.moveHDisabled || reservation.moveVDisabled) {
      return false; // Stops when moving is disabled
    }

    const validation = this.validateReservationMove && this.validateReservationMove(reservation);

    if (validation) {
      args.async = true;

      if (validation instanceof Promise) {
        validation.catch(() => args.preventDefault()).then(() => args.loaded());
      } else if (validation instanceof Observable) {
        validation.pipe(finalize(() => args.loaded())).subscribe(success => {}, error => args.preventDefault());
      } else {
        console.error('Invalid validateReservationMove Handler: ', validation);
      }
    } else {
      console.error('Validator must return a promise or a observable');
    }
  }

  onEventMoving = args => {
    if (args.e.data.moveHDisabled || args.e.data.moveVDisabled) {
      args.allowed = false;
      return false; // Stops when moving is disabled
    }

    if (this.gridService.checkIsFooter(args.resource)) {
      args.allowed = false;
      return false;
    }

    // Validate if the cell is blocked
    if (this.cellService.isTimeRangeSelectionValid(args, this.blockedRoomList)) {
      const blocked = this.cellService.isBlocked(args, this.blockedRoomList);
      args.allowed = !blocked;
    } else {
      args.allowed = false;
    }
  }

  onBeforeRowHeaderRender = args => {
    const row: Row = args.row;
    row.classes = [];

    this.rowService.setRowTotalRoomsBadge(row);

    if (this.gridService.checkIsFooter(row.id)) {
      this.rowService.setRowAsFooter(row);
    } else {
      this.rowService.setRowAsParent(row);
      this.rowService.setRowStatusCssClass(row);
      this.rowService.setRowBedIcon(row);
    }

    row.cssClass = row.classes.join(' ');
  }

  onBeforeTimeHeaderRender = args => {
    const header = args.header;
    header.classes = [];

    this.headerService.setTimeHeader(args.header, this.momentDateFormat);
    this.headerService.setTimeHeaderContent(args.header, this.momentDateFormat);

    header.cssClass = header.classes.join(' ');
    delete header.classes;
  }

  onResourceExpand = args => {
    this.scheduler.control.clearSelection();
    this.scheduler.control.rows.all().forEach(row => (args.resource.id !== row.id ? row.collapse() : null));
    const id = args.resource.id.replace('property-', '');
    this.roomTypeExpanded.emit(id);
  }

  onRowClick = args => {
    if (args.row.parent() || this.gridService.checkIsFooter(args.resource.id)) {
      return;
    }
    args.resource.toggle();
  }

  onBeforeCellRender = args => {
    const cell: Cell = args.cell;
    cell.classes = [];

    if (this.gridService.checkIsFooter(cell.resource)) {
      this.cellService.setCellFooter(cell, this.footerList, this.daypilotDateFormat);
    } else {
      this.cellService.setOverbook(cell, this.availabilityList, this.labels.overbook);
      this.cellService.checkTime(cell, this.momentDateFormat);
    }

    this.cellService.setAvailability(cell, this.availabilityList, this.daypilotDateFormat);
    this.cellService.setDisabled(cell, this.blockedRoomList);

    // Alternate even-odd
    cell.classes.push(args.cell.y % 2 ? ' even ' : ' odd ');

    cell.cssClass = cell.classes.join(' ');
    delete cell.classes;
  }

  onBeforeEventRender = args => {
    const data: Reservation = args.data;

    args.data.cssClass = this.gridService.getReservationClass(data.status);

    args.e.html = `
          <div class="text">${data.guestName}</div>
          <div class="caption">
            <img src="${ this.assetsCdnService.getImgUrlTo('person-white.svg')}" class="adult-count"> ${data.adultCount}
            <img src="${ this.assetsCdnService.getImgUrlTo('child_icon-white.svg')}" class="child-count"> ${data.childCount}
          </div>
        `;

    args.e.areas = [
      {
        right: 2,
        top: 3,
        width: 31,
        height: 31,
        visibility: 'Visible',
        html: `<i id="event-popover-${args.data.id}" class="thf-u-font--25 mdi mdi-dots-vertical event-popover-${args.data.id}"></i>`,
        css: 'event_action_show_more',
        action: 'JavaScript',
        js: e => {
          const hElement: HTMLElement = this.elementRef.nativeElement;
          const allDivs = hElement.getElementsByClassName('event-popover-' + args.data.id);

          this.selectedReservation = args.data;
          this.selectedReservation.statusName = this.gridService.getReservationStatusName(this.selectedReservation.status, this.labels);

          this.openPopover(<HTMLElement>allDivs[0], this.popoverReservationInfo);
          this.scheduler.control.clearSelection();
        },
      },
    ];
  }

  onTimeRangeSelected = args => {
    const hElement: HTMLElement = this.elementRef.nativeElement;
    const allDivs = hElement.getElementsByClassName('scheduler_default_shadow');
    this.selectedTimeRange = new TimeRange();
    this.selectedTimeRange.start = args.start.toString(this.daypilotDateFormat);
    this.selectedTimeRange.end = args.end.addDays(-1).toString(this.daypilotDateFormat);

    const resource = `${args.resource}`;

    if (resource.indexOf('property-') >= 0) {
      const propertyId = resource.replace('property-', '');
      const property = this.propertyList.find(item => item.id == propertyId);
      if (property) {
        this.selectedTimeRange.propertyId = +property.id;
        this.selectedTimeRange.propertyName = property.name;
      }
    } else if (resource.indexOf('roomType-') >= 0) {
      const roomType = this.roomTypeList.find(item => item.id == resource);
      const roomTypeId = resource.replace('roomType-', '');
      if (roomType) {
        this.selectedTimeRange.roomTypeId = +roomTypeId;
        this.selectedTimeRange.roomTypeName = roomType.name;
        this.selectedTimeRange.propertyName = roomType.propertyName;
        this.selectedTimeRange.propertyId = +roomType.propertyId;
      }
    }

    if (this.cellService.isTimeRangeSelectionValid(args, this.blockedRoomList)) {
      const roomBlock = this.cellService.isBlocked(args, this.blockedRoomList);
      if (roomBlock) {
        this.selectedTimeRange.roomBlock = roomBlock;
      }
    }

    this.openPopover(<HTMLElement>allDivs[0], this.popoverSelectedPeriod);
  }

  onTimeRangeSelect = args => {
    // args.preventDefault()
  }

  onTimeRangeClick = args => {
    this.scheduler.control.clearSelection();
  }

  onTimeRangeSelecting = args => {
    if (this.gridService.checkIsFooter(args.resource)) {
      args.allowed = false;
    } else if (this.cellService.isTimeRangeSelectionValid(args, this.blockedRoomList)) {
      const blocked = this.cellService.isBlocked(args, this.blockedRoomList);
      if (blocked) {
        args.start = new DayPilot.Date(blocked.blockingStartDate);
        args.end = new DayPilot.Date(blocked.blockingEndDate).addDays(1);
      }
    } else {
      args.allowed = false;
    }
  }

  goPrevious($event) {
    $event.preventDefault();

    const currDate = <DayPilot.Date>this.scheduler.control.startDate;
    this.scheduler.control.startDate = currDate.addDays(-this.scheduler.control.days);
    this.initialDateChange.emit(moment(this.scheduler.control.startDate.toString()).format(this.momentDateFormat));
  }

  goNext($event) {
    $event.preventDefault();

    const currDate = <DayPilot.Date>this.scheduler.control.startDate;
    this.scheduler.control.startDate = currDate.addDays(this.scheduler.control.days);
    this.initialDateChange.emit(moment(this.scheduler.control.startDate.toString()).format(this.momentDateFormat));
  }

  goToday() {
    this.scheduler.control.startDate = new DayPilot.Date().getDatePart();
    this.initialDateChange.emit(moment(this.scheduler.control.startDate.toString()).format(this.momentDateFormat));
  }

  /*
   * Popover methods
   * I couldn't apply the directive directly into daypilot items because it only returns static html
   * **************************************************************************************************/
  public openPopover(referenceObject: HTMLElement, template: TemplateRef<any>) {
    if (!this.popoverContainer) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ThxPopoverComponent);
      const viewContainerRef = this.appRef.components[0].instance.viewContainerRef || this.viewContainerRef;
      const componentRef = viewContainerRef.createComponent(componentFactory);

      this.popoverContainer = componentRef.instance as ThxPopoverComponent;
      this.popoverContainer.popoverColor = 'white';
      this.popoverContainer.popperOptions = {
        placement: 'top',
      };
    }

    this.popoverContainer.template = template;

    this.popoverContainer.referenceObject = referenceObject;

    if (this.isPopoverShown) {
      this.hidePopover();
    } else {
      this.showPopover();
    }
  }

  public onClickOutside(event) {
    if (this.isPopoverShown && !this.popoverContainer.popoverContent.nativeElement.parentElement.contains(event.target)) {
      this.hidePopover();
    }
  }

  public hidePopover() {
    this.popoverContainer.hide();
    this.globalPopoverClick();
    this.isPopoverShown = false;
  }

  public showPopover() {
    this.popoverContainer.show();
    this.isPopoverShown = true;
    this.globalPopoverClick = this.renderer.listen('document', 'mousedown', this.onClickOutside.bind(this));
  }

  public emitNewReservation($event: any, timeRange: TimeRange) {
    $event.preventDefault();
    this.newReservationClick.emit(timeRange);
    this.scheduler.control.clearSelection();
    this.hidePopover();
  }

  public emitBlockRoom($event: any, timeRange: TimeRange) {
    $event.preventDefault();
    this.blockRoomClick.emit(timeRange);
    this.scheduler.control.clearSelection();
    this.hidePopover();
  }

  public emitUnlockRoom($event: any, timeRange: TimeRange) {
    $event.preventDefault();
    this.unlockRoomClick.emit(timeRange);
    this.scheduler.control.clearSelection();
    this.hidePopover();
  }

  public emitReservationClick(reservation: Reservation) {
    this.reservationClick.emit(reservation);
    this.scheduler.control.clearSelection();
    this.hidePopover();
  }

  public emitRateProposalClick($event: any, timeRange: TimeRange) {
    $event.preventDefault();
    this.rateProposalClick.emit(timeRange);
    this.scheduler.control.clearSelection();
    this.hidePopover();
  }
}
