import { Injectable } from '@angular/core';
import * as momentNs from 'moment';
import { DayPilot } from 'daypilot-pro-angular';
const moment = momentNs;

@Injectable({ providedIn: 'root' })
export class ThxMultiPropertyAvailabilityGridHeaderService {
  constructor() {}

  public setTimeHeader(header: any, momentDateFormat: string) {
    const date: DayPilot.Date = header.start;

    const momentDate = moment(date.toString(), momentDateFormat);
    const momentToday = moment();

    if (momentDate.format(momentDateFormat) === momentToday.format(momentDateFormat)) {
      header.classes.push('today');
    } else if (momentDate.weekday() === 0 || momentDate.weekday() === 6) {
      header.classes.push('weekend');
    }

    if (momentDate.isBefore(momentToday, 'day')) {
      header.classes.push('past');
    }
  }

  public setTimeHeaderContent(header: any, momentDateFormat: string) {
    const date: DayPilot.Date = header.start;
    const momentDate = moment(date.toString(), momentDateFormat);

    const day = momentDate.format('DD');
    const month = momentDate.format('MMM').toUpperCase();
    const dayOfWeek = momentDate.format('ddd');

    header.html = `
      <div class="day">${day}</div>
      <div class="caption">
        <span class="month">${month}</span>
        <span class="day-of-week">(${dayOfWeek})</span>
      </div>
    `;
  }
}
