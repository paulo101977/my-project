import { Component, Input, OnInit } from '@angular/core';
import { PieChartItem } from '../thx-pie-chart/models/pie-chart-item';

@Component({
  selector: 'thx-pie-chart-widget',
  templateUrl: './thx-pie-chart-widget.component.html',
  styleUrls: ['./thx-pie-chart-widget.component.css']
})
export class ThxPieChartWidgetComponent {

  @Input() title: string;
  private _items: Array<PieChartItem>;
  @Input('items')
  set items (items: Array<PieChartItem>) {
    this._items = items;
    if (items) {
      this._items = items;
      this.total = items.reduce((val, current) => val += current.value, 0);
    }
  }
  get items(): Array<PieChartItem> {
    return this._items;
  }

  @Input() emptyText: string;

  public total = 0;

}
