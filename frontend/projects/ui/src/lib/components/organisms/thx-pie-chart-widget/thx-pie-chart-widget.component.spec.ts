import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ThxPieChartWidgetComponent } from './thx-pie-chart-widget.component';

describe('ThxPieChartBoxComponent', () => {
  let component: ThxPieChartWidgetComponent;
  let fixture: ComponentFixture<ThxPieChartWidgetComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxPieChartWidgetComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxPieChartWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
