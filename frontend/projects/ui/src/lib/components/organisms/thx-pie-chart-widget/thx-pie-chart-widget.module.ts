import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxPieChartWidgetComponent } from './thx-pie-chart-widget.component';
import { ThxWidgetBoxModule } from '../../atoms/thx-widget-box/thx-widget-box.module';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';
import { ThxPieChartModule } from '../thx-pie-chart/thx-pie-chart.module';

@NgModule({
  imports: [
    CommonModule,
    ThxWidgetBoxModule,
    ThxPieChartModule,
    ThxImgModule
  ],
  declarations: [ThxPieChartWidgetComponent],
  exports: [ThxPieChartWidgetComponent]
})
export class ThxPieChartWidgetModule { }
