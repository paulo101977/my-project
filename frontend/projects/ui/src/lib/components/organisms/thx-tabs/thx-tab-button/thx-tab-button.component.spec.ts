import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxTabButtonComponent } from './thx-tab-button.component';

describe('ThfButtonComponent', () => {
  let component: ThxTabButtonComponent;
  let fixture: ComponentFixture<ThxTabButtonComponent>;
  let element;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThxTabButtonComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxTabButtonComponent);
    component = fixture.componentInstance;

    element = fixture.debugElement.nativeElement;
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should click', () => {

    component.disabled = false;

    spyOn(component.clickCallback, 'emit');
    spyOn(component.clickTab, 'emit');

    component.onClick(event);

    expect(component.clickCallback.emit).toHaveBeenCalled();
    expect(component.clickTab.emit).toHaveBeenCalled();
  });

  it('should not click when disabled', () => {

    component.disabled = true;

    spyOn(component.clickCallback, 'emit');
    spyOn(component.clickTab, 'emit');

    component.onClick(event);

    expect(component.clickCallback.emit).not.toHaveBeenCalled();
    expect(component.clickTab.emit).not.toHaveBeenCalled();
  });

  it('should toggle hide style', () => {

    component.hide = false;
    component.ngAfterViewChecked();
    expect(element.style.display).toBe('');

    component.hide = true;
    component.ngAfterViewChecked();
    expect(element.style.display).toBe('none');
  });

  it('should toggle disabled class', () => {

    component.controlDisabled();
    expect(element.classList).not.toContain('thx-tab-disabled');

    component.disabled = true;
    component.controlDisabled();
    expect(element.classList).toContain('thx-tab-disabled');
  });

  it('should toggle active class', () => {

    component.controlActive();
    expect(element.classList).not.toContain('thx-tab-active');

    component.active = true;
    component.controlActive();
    expect(element.classList).toContain('thx-tab-active');
  });

});
