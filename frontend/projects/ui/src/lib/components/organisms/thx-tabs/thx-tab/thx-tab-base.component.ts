import { Input, Output, EventEmitter } from '@angular/core';
import { convertToBoolean } from '../../../../utils/util';

/**
 * @description
 *
 * Cada thx-tab gera uma aba com o conteúdo HTML inserido dentro dele.
 */
export class ThxTabBaseComponent {

  /** Label que aparecerá na aba */
  @Input() label: string;

  /** Para a aba parecer ativa e mostrar o conteúdo quando iniciar */
  active = false;
  @Input('active') set setActive(active: string) {
    (active === '') ? this.active = true : this.active = convertToBoolean(active);
  }

  /** Esconder a aba */
  hide = false;
  @Input('hide') set setHide(hide: string) {
    (hide === '') ? this.hide = true : this.hide = convertToBoolean(hide);
  }

  /** Desabilitar a aba */
  disabled = false;
  @Input('disabled') set setDisabled(disabled: string) {
    (disabled === '') ? this.disabled = true : this.disabled = convertToBoolean(disabled);
  }

  /** ID da aba */
  @Input() id?: string = String(Math.random());

  /** Método disparado ao clicar na aba */
  @Output() click = new EventEmitter();

}
