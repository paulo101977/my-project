import { Input } from '@angular/core';
import { convertToBoolean } from '../../../utils/util';

/**
 * @description
 *
 * O thx-tabs é o componente de abas que facilita a organização de conteúdos
 */
export class ThxTabsBaseComponent {

  /** Usado para indicar se a aba será pequena */
  small = false;

  @Input('small') set setSmall(small: string) {
    (small === '') ? this.small = true : this.small = convertToBoolean(small);
  }

  /** Indica se as tabs devem preencher 100% da largura */
  fullWidth = false;

  @Input('fullWidth') set setFullWidth(fullWidth: string) {
    (fullWidth === '') ? this.fullWidth = true : this.fullWidth = convertToBoolean(fullWidth);
  }
}
