import { NgModule, Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';

import { ThxTabsComponent } from './thx-tabs.component';
import { ThxTabComponent } from './thx-tab/thx-tab.component';
import { ThxTabButtonComponent } from './thx-tab-button/thx-tab-button.component';
import { ThxSliderButtonsModule } from '../thx-slider-buttons/thx-slider-buttons.module';

@NgModule({
  imports: [ CommonModule, ThxSliderButtonsModule ],
  declarations: [ThxTabButtonComponent],
  exports: [ ThxSliderButtonsModule ],
  entryComponents: [
    ThxTabButtonComponent,
  ]
})
class TestModule {}

@Component({
  template: `
    <thx-tabs small="false">
      <thx-tab id="aba1" label="Aba 1" active>
        <h1>Conteúdo da aba 1</h1>
      </thx-tab>
      <thx-tab id="aba2" label="Aba 2" (click)="metodo()">
        <h1>Conteúdo da aba 2</h1>
      </thx-tab>
      <thx-tab id="aba3" label="Aba 3" disabled>
        <h1>Conteúdo da aba 3</h1>
      </thx-tab>
      <thx-tab id="aba4" label="Aba 4" active="false">
        <h1>Conteúdo da aba 4</h1>
      </thx-tab>
      <thx-tab id="aba5" label="Aba 5">
        <h1>Conteúdo da aba 5</h1>
      </thx-tab>
      <thx-tab id="aba6" label="Aba 6">
        <h1>Conteúdo da aba 6</h1>
      </thx-tab>
    </thx-tabs>
  `
})
class ContentProjectionPatternTab1Component {
  metodo() { }
}
describe('ThfTabs', () => {
  let component: ContentProjectionPatternTab1Component;
  let fixture: ComponentFixture<ContentProjectionPatternTab1Component>;

  let fixtureAction: ComponentFixture<ThxTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TestModule ],
      declarations: [ ThxTabsComponent, ThxTabComponent, ContentProjectionPatternTab1Component ]
    })
      .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(ContentProjectionPatternTab1Component);
    component = fixture.componentInstance;

    fixture.detectChanges();

    fixtureAction = TestBed.createComponent(ThxTabsComponent);

    fixtureAction.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have a tab with label', () => {
    expect(fixture.nativeElement.textContent).toContain('Aba 1');
  });

  it('should be to click in first tab', () => {
    const buttons = fixture.debugElement.nativeElement.querySelector('.thx-tabs-parent');
    const button = fixture.debugElement.nativeElement.querySelector('thx-tab-button');

    const evt = document.createEvent('HTMLEvents');
    evt.initEvent('click', false, true);
    button.dispatchEvent(evt);
    fixture.detectChanges();

    const activeButton = buttons.querySelector('.thx-tab-active');
    expect(activeButton.textContent).toContain('Aba 1');
  });
});

@Component({
  template: `
    <thx-tabs small="">
      <thx-tab id="aba1" label="Aba 1">
        <h1>Conteúdo da aba 1</h1>
      </thx-tab>
      <thx-tab id="aba2" label="Aba 2" (click)="metodo()">
        <h1>Conteúdo da aba 2</h1>
      </thx-tab>
      <thx-tab id="aba3" label="Aba 3" active="false">
        <h1>Conteúdo da aba 3</h1>
      </thx-tab>
      <thx-tab id="aba4" label="Aba 4">
        <h1>Conteúdo da aba 4</h1>
      </thx-tab>
      <thx-tab id="aba5" label="Aba 5" hide>
        <h1>Conteúdo da aba 5</h1>
      </thx-tab>
      <thx-tab id="aba6" label="Aba 6" disabled active>
        <h1>Conteúdo da aba 6</h1>
      </thx-tab>
    </thx-tabs>
  `
})
class ContentProjectionPatternTab2Component {
  metodo() { }
}
describe('ThfTabs', () => {
  let component: ContentProjectionPatternTab2Component;
  let fixture: ComponentFixture<ContentProjectionPatternTab2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TestModule ],
      declarations: [ ThxTabsComponent, ThxTabComponent, ContentProjectionPatternTab2Component ]
    })
      .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(ContentProjectionPatternTab2Component);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have a tab with label', () => {
    expect(fixture.nativeElement.textContent).toContain('Aba 1');
  });

  it('should have the "Aba 4" actived', () => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const button = fixture.debugElement.nativeElement.querySelector('.thx-tab-active');

      // A aba ativa é a Aba 4 porque a 5 e 6 estão desabilitadas ou escondidas
      expect(button.textContent).toContain('Aba 4');
    });
  });

  it('should have a hide tab', () => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const buttons = fixture.debugElement.nativeElement.querySelectorAll('thx-tab-button');
      expect(buttons[4].style.display).toContain('none');
    });
  });

  it('should have a class disabled', () => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const buttons = fixture.debugElement.nativeElement.querySelectorAll('thx-tab-button');
      expect(buttons[5].classList).toContain('thx-tab-disabled');
    });
  });

});

@Component({
  template: `
    <thx-tabs small="">
      <thx-tab id="aba1" label="Aba 1" active disabled>
        <h1>Conteúdo da aba 1</h1>
      </thx-tab>
      <thx-tab id="aba2" label="Aba 2">
        <h1>Conteúdo da aba 2</h1>
      </thx-tab>
    </thx-tabs>
  `
})
class ContentProjectionPatternTab3Component {
  metodo() { }
}
describe('ThfTabs', () => {
  let fixture: ComponentFixture<ContentProjectionPatternTab3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ TestModule ],
      declarations: [ ThxTabsComponent, ThxTabComponent, ContentProjectionPatternTab3Component ]
    })
      .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(ContentProjectionPatternTab3Component);

    fixture.detectChanges();
  });

  it('should have the "Aba 2" actived', () => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const button = fixture.debugElement.nativeElement.querySelector('.thx-tab-active');

      // A aba ativa é a Aba 4 porque a 1 está desabilitada
      expect(button.textContent).toContain('Aba 2');
    });
  });

});
