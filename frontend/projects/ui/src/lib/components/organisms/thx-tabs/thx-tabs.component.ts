import {
  AfterContentInit,
  AfterViewChecked,
  Component,
  ComponentFactoryResolver,
  ContentChildren, HostListener,
  OnDestroy,
  QueryList,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { Subscription } from 'rxjs';

import { ThxTabsBaseComponent } from './thx-tabs-base.component';
import { ThxTabButtonComponent } from './thx-tab-button/thx-tab-button.component';
import { ThxTabComponent } from './thx-tab/thx-tab.component';

@Component({
  selector: 'thx-tabs',
  templateUrl: './thx-tabs.component.html',
  styleUrls: ['./thx-tabs.component.scss']
})
export class ThxTabsComponent extends ThxTabsBaseComponent implements AfterContentInit, AfterViewChecked, OnDestroy {

  // Descobre os componentes das abas filhas
  @ContentChildren(ThxTabComponent) tabsChild: QueryList<ThxTabComponent>;

  // Obtem o ViewContainerRef do um objeto cuja variável local é #nav
  @ViewChild('nav', {read: ViewContainerRef}) nav: ViewContainerRef;

  @ViewChild('tabsParent', {read: ViewContainerRef}) tabsParent;

  @ViewChild('tabs', {read: ViewContainerRef}) tabs;

  // Id do componente
  listButtons: Array<any> = [];
  created: boolean;
  showSliderButtons: boolean;

  private buttonSubscription: Subscription;
  private SCROLL_PIXELS = 150;
  private LONG_CLICK_SCROLL_PIXELS = 50;

  constructor(private resolver: ComponentFactoryResolver) {
    super();
  }

  ngAfterContentInit() {
    this.createTabButtons();

    this.tabsChild.changes.subscribe(
      () => {
        this.created = false;
        this.createTabButtons();
        this.enableActiveButton();
      }
    );
  }

  ngAfterViewChecked() {
    this.enableActiveButton();
  }

  createTabButtons() {
    const ComponentFactory = this.resolver.resolveComponentFactory(ThxTabButtonComponent);

    // Clear nav buttons
    this.listButtons = [];
    this.nav.clear();

    // Percorre tabs
    this.tabsChild.forEach(tab => {

      // Cria os botões
      const compButton = this.nav.createComponent(ComponentFactory);

      compButton.instance.label = tab.label;
      compButton.instance.id = tab.id;
      compButton.instance.active = tab.active;
      compButton.instance.hide = tab.hide;
      compButton.instance.disabled = tab.disabled;
      compButton.instance.small = this.small;
      compButton.instance.clickCallback = tab.click;

      // É necessário esta arrow function para não perder o contexto.
      // Caso contrário, a funcão não teria mais acesso ao "this" deste component
      this.buttonSubscription = compButton.instance.clickTab.subscribe( id => this.selectedTab(id));
      this.listButtons.push(compButton.instance);
    });

    // const compSlider = this.nav.createComponent(sliderComponentFactory);
    this.created = true;
  }

  enableActiveButton() {
    if (this.created) {
      // Váriaveis utilizadas para trocar aba ativa,
      // quando a aba ativa está desabilitada ou escondida
      let changeTab = false;
      let lastEnabledTab = null;
      let lastEnabledButton = null;

      this.tabsChild.forEach(tab => {
        this.listButtons.forEach(button => {
          if (button.id === tab.id) {
            if (tab.disabled || tab.hide) {
              // Verifica se uma desativada ou escondida está ativa
              if (tab.active) {
                tab.active = false;
                button.active = false;
                changeTab = true;
              }
            } else {
              // Armazena a aba que não esteja escondida para trocar de aba automaticamente
              // caso a aba ativa seja escondida ou desabilitada.
              if (!changeTab) {
                lastEnabledTab = tab;
                lastEnabledButton = button;
              } else if (!lastEnabledTab) {
                lastEnabledTab = tab;
                lastEnabledButton = button;
              }
            }

            // Atualiza as propriedades disabled e hide das abas
            button.disabled = tab.disabled;
            button.hide = tab.hide;
          }
        });
      });

      // Habilita uma aba ativa
      if (changeTab && lastEnabledTab) {
        lastEnabledTab.active = true;
        lastEnabledButton.active = true;
      }

      setTimeout(() => this.onResize());
    }
  }

  @HostListener('window:resize')
  onResize(): void {
    const boxWidth = this.tabs.element.nativeElement.getBoundingClientRect().width;
    const toScrollWidth = this.tabsParent.element.nativeElement.scrollWidth;

    this.showSliderButtons = toScrollWidth > (boxWidth + 1);
  }

  ngOnDestroy() {
    if (this.buttonSubscription) {
      this.buttonSubscription.unsubscribe();
    }
  }

  // Função disparada para trocar abas
  selectedTab(id) {
    this.tabsChild.forEach(tab => {
      tab.active = (tab.id === id);
    });

    this.listButtons.forEach(button => {
      if (button.id === id) {
        button.active = true;
      } else {
        button.active = false;
      }
    });
  }

  public prevScroll() {
    this.nav.element.nativeElement.parentElement.scrollBy(-this.SCROLL_PIXELS, 0);
  }

  public nextScroll() {
    this.nav.element.nativeElement.parentElement.scrollBy(this.SCROLL_PIXELS, 0);
  }

  public longPrevScroll() {
    this.nav.element.nativeElement.parentElement.scrollBy(-this.LONG_CLICK_SCROLL_PIXELS, 0);
  }

  public longNextScroll() {
    this.nav.element.nativeElement.parentElement.scrollBy(this.LONG_CLICK_SCROLL_PIXELS, 0);
  }

}
