import { Component, ElementRef, AfterViewChecked} from '@angular/core';

import { ThxTabBaseComponent } from './thx-tab-base.component';

/**
 * @docsExtends ThfTabBaseComponent
 */
@Component({
  selector: 'thx-tab',
  templateUrl: './thx-tab.component.html'
})
export class ThxTabComponent extends ThxTabBaseComponent implements AfterViewChecked {

  constructor(private el: ElementRef) {
    super();
  }

  ngAfterViewChecked() {
    this.el.nativeElement.style.display = (this.active) ? '' : 'none';
  }

}
