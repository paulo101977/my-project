import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThxTabsComponent } from './thx-tabs.component';
import { ThxTabComponent } from './thx-tab/thx-tab.component';
import { ThxTabButtonComponent } from './thx-tab-button/thx-tab-button.component';
import { ThxSliderButtonsModule } from '../thx-slider-buttons/thx-slider-buttons.module';
/**
 * @description
 * Módulo do componente thx-tabs
 */
@NgModule({
  imports: [
    CommonModule,
    ThxSliderButtonsModule
  ],
  declarations: [
    ThxTabsComponent,
    ThxTabComponent,
    ThxTabButtonComponent,
  ],
  entryComponents: [
    ThxTabButtonComponent,
  ],
  exports: [
    ThxTabsComponent,
    ThxTabComponent
  ]
})
export class ThxTabsModule { }
