import { Component, AfterViewChecked, Input, Output, EventEmitter, ElementRef,
  ViewChild, HostListener } from '@angular/core';

@Component({
  selector: 'thx-tab-button',
  templateUrl: './thx-tab-button.component.html',
  styleUrls: ['./thx-tab-button.component.scss']
})
export class ThxTabButtonComponent implements AfterViewChecked {

  @Input() id: string;
  @Input() active: boolean;
  @Input() label: string;
  @Input() small: boolean;
  @Input() disabled: boolean;
  @Input() hide: boolean;

  // Método recebido do usuário para ser disparado quando clicar na aba
  @Output() clickCallback = new EventEmitter();

  @Output() clickTab = new EventEmitter();

  @ViewChild('tab') tab: ElementRef;

  @HostListener('click', ['$event'])
  onClick(e) {
    if (!this.disabled) {
      this.clickTab.emit(this.id);
      this.clickCallback.emit(this.id);
    }
  }

  constructor(private el: ElementRef) { }

  controlDisabled() {
    if (this.disabled) {
      this.el.nativeElement.classList.add('thx-tab-disabled');
    } else {
      this.el.nativeElement.classList.remove('thx-tab-disabled');
    }
  }

  controlActive() {
    if (this.active) {
      this.el.nativeElement.classList.add('thx-tab-active');
    } else {
      this.el.nativeElement.classList.remove('thx-tab-active');
    }
  }

  ngAfterViewChecked() {
    if (this.hide) {
      this.el.nativeElement.style.display = 'none';
    } else {
      this.el.nativeElement.style.display = '';
    }

    this.controlDisabled();
    this.controlActive();
  }
}
