import { Component, ElementRef, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { fromEvent, interval, Subscription } from 'rxjs';
import { delay, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'thx-slider-buttons',
  templateUrl: './thx-slider-buttons.component.html',
  styleUrls: ['./thx-slider-buttons.component.scss']
})
export class ThxSliderButtonsComponent implements OnChanges {

  @Input() itemsListHtml: ElementRef;
  @Output() prevScroll = new EventEmitter();
  @Output() longPrevScroll = new EventEmitter();
  @Output() nextScroll = new EventEmitter();
  @Output() longNextScroll = new EventEmitter();

  private scrollPressed: Subscription;

  public isPrevDisabled = false;
  public isNextDisabled = false;

  ngOnChanges() {
    this.checkButtonsDisabled();
  }

  public clickPrev() {
    this.checkButtonsDisabled();
    this.prevScroll.emit();
    this.emitLongClick(this.longPrevScroll);
  }

  public clickNext() {
    this.nextScroll.emit();
    this.checkButtonsDisabled();
    this.emitLongClick(this.longNextScroll);
  }

  private emitLongClick(emitter: EventEmitter<any>) {
    this.scrollPressed = interval(100)
      .pipe(
        delay(700),
        takeUntil(fromEvent(document.body, 'mouseup'))
      )
      .subscribe(() => emitter.emit());
  }

  checkButtonsDisabled() {
    if (this.itemsListHtml) {
      const boxPosition = this.itemsListHtml.nativeElement.parentElement.getBoundingClientRect();
      const toScrollPosition = this.itemsListHtml.nativeElement.getBoundingClientRect();
      this.isPrevDisabled = Math.trunc(boxPosition.left) == Math.trunc(toScrollPosition.left);
      this.isNextDisabled = Math.trunc(boxPosition.right) == Math.trunc(toScrollPosition.right);
    }
  }



}
