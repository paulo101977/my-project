import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { imgCdnStub } from '../../../../../../../testing';

import { ThxSliderButtonsComponent } from './thx-slider-buttons.component';

describe('SliderButtonsComponent', () => {
  let component: ThxSliderButtonsComponent;
  let fixture: ComponentFixture<ThxSliderButtonsComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule],
      declarations: [ThxSliderButtonsComponent, imgCdnStub],
    });

    fixture = TestBed.createComponent(ThxSliderButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
