import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxSliderButtonsComponent } from './thx-slider-buttons.component';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';

/**
 * @description
 * Módulo do componente thx-tabs
 */
@NgModule({
  imports: [
    CommonModule,
    ImgCdnModule
  ],
  declarations: [
    ThxSliderButtonsComponent,
  ],
  exports: [
    ThxSliderButtonsComponent,
  ]
})
export class ThxSliderButtonsModule { }
