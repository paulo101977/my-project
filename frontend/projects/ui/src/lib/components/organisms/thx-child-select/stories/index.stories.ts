import { CommonModule } from '@angular/common';
import { storiesOf, moduleMetadata  } from '@storybook/angular';


import { Notes } from './notes';


import { ThxChildSelectModule } from '../thx-child-select.module';
import { DefaultComponent } from './default';


storiesOf('Ui | Organisms/ThxChildSelect', module)
  .addDecorator(
    moduleMetadata({
      imports: [ CommonModule, ThxChildSelectModule ]
    })
  )
  .add('Default', () => ({
      component: DefaultComponent
    }),
      { notes: { markdown: new Notes().getNotes() }});
