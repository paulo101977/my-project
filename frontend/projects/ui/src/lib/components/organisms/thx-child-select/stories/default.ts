import { Component, DoCheck } from '@angular/core';
import { ChildSelect } from '../models/child-select';
import { ChildTextConfig } from '../models/child-text-config';

@Component({
  selector: 'thx-default-child-select',
  template: `
        <div>
          <div>
            <div>Adiciona somente pelo botão +</div>
            <thx-child-select [textConfig]="textConfig" [childMaxAge]="'16'" ></thx-child-select>
          </div>
          <div>
            <div>Adiciona somente pelo input</div>
            <thx-child-select [textConfig]="textConfig" [childMaxAge]="'16'" [showAddButton]="false" ></thx-child-select>
          </div>
          <div>
            <div>Adiciona mas não pode deletar</div>
            <thx-child-select [textConfig]="textConfig" [childMaxAge]="'16'" [showAddButton]="false" [showDeleteButton]="false" >
            </thx-child-select>
          </div>
          <div>
            <div>Usando com ngModel</div>
            <thx-child-select [textConfig]="textConfig" [childMaxAge]="'16'" [(ngModel)]="value" (onModelChange)="change($event)" >
            </thx-child-select>
          </div>
        </div>
  `,
  styles: []
})
export class DefaultComponent {
  public textConfig: ChildTextConfig;
  public value: ChildSelect;

  constructor() {
    this.textConfig = new ChildTextConfig();
    this.textConfig.okButton = 'Ok';
    this.textConfig.cancelButton = 'Cancelar';
    this.textConfig.title = 'Idade das crianças';

    this.value = new ChildSelect();
    this.value.childrenAges = [ 2 , 3, 8];

  }

}
