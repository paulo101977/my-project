import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxChildSelectComponent } from './thx-child-select.component';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';
import { FormsModule } from '@angular/forms';
import { ThxPopoverModule } from '../thx-popover/thx-popover.module';
import { ThxButtonModule } from '../../atoms/thx-button/thx-button.module';

@NgModule({
  imports: [ CommonModule, ThxImgModule, ThxButtonModule, FormsModule, ThxPopoverModule ],
  declarations: [ ThxChildSelectComponent ],
  exports: [ ThxChildSelectComponent ]
})
export class ThxChildSelectModule { }
