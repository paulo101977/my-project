import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ChildSelect } from './models/child-select';
import { ThxButtonColorType } from '../../atoms/thx-button/enums/thx-button-color-type.enum';
import { ChildService } from './services/child.service';
import { ChildTextConfig } from './models/child-text-config';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'thx-child-select',
  templateUrl: './thx-child-select.component.html',
  styleUrls: ['./thx-child-select.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ThxChildSelectComponent),
    multi: true
  }]
})
export class ThxChildSelectComponent implements OnInit, ControlValueAccessor {

  @Input() textConfig: ChildTextConfig;
  @Input() childMaxAge;
  @Input() showAddButton = true;
  @Input() showDeleteButton = true;

  public childrenSelectOriginal: ChildSelect = new ChildSelect();
  public childrenSelect: ChildSelect = new ChildSelect();

  public COLOR_TYPE_CANCEL = ThxButtonColorType.Secondary;
  public COLOR_TYPE_OK = ThxButtonColorType.Primary;

  public childBoxEnabled = false;

  //  the implementation methods call that functions
  onChange = (_: any) => {};
  onTouched = () => {};

  constructor(public childrenService: ChildService) {
  }

  ngOnInit() {
    if (!this.childMaxAge || this.childMaxAge == 0) {
      console.error('ThxChildSelectComponent => The field childMaxAge must be informed');
    }
  }

  quantityChange() {
    this.childrenSelect = this.childrenService.updateChild(this.childrenSelect);
    this.childBoxEnabled = true;
  }

  addChild() {
    this.childBoxEnabled = true;
    this.childrenSelect = this.childrenService.addChild(this.childrenSelect);
  }

  deleteChild(childIndex) {
    this.childrenSelect = this.childrenService.deleteChild(this.childrenSelect, childIndex);
    if (this.childrenSelect.quantity == 0) {
      this.childBoxEnabled = false;
    }
  }

  openChildBox() {
    if (!this.showAddButton && this.childrenSelect.quantity > 0) {
      this.childBoxEnabled = true;
    }
  }

  clickCancel() {
    this.childBoxEnabled = false;
    this.childrenSelect = this.childrenSelectOriginal;
    this.writeValue(this.childrenSelect);
    this.onChange(this.childrenSelect);
  }

  clickOk() {
    this.childBoxEnabled = false;
    this.writeValue(this.childrenSelect);
    this.onChange(this.childrenSelect);
  }

  /* ControlValueAccessor */

  writeValue(value: ChildSelect): void {
    this.childrenSelect = value;
    if (!this.childrenSelect) {
      this.childrenSelect = new ChildSelect();
    }
    this.childrenSelectOriginal = {
      quantity: this.childrenSelect.quantity,
      childrenAges: this.childrenSelect.childrenAges ? [...this.childrenSelect.childrenAges] : [],
    };
  }

  registerOnChange(fn: (_: any) => {}): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => {}): void {
    this.onTouched = fn;
  }

}
