import { TestBed, inject } from '@angular/core/testing';

import { ChildService } from './child.service';
import { ChildSelect } from '../models/child-select';

describe('ChildrenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChildService]
    });
  });

  it('should be created', inject([ChildService], (service: ChildService) => {
    expect(service).toBeTruthy();
  }));

  it('should return a list of ages', inject([ChildService], (service: ChildService) => {
    const ageList = service.getChildrenAgeList(10);

    expect(ageList.length).toBe(11);
  }));

  describe('Add child', () => {

    it('add child', inject([ChildService], (service: ChildService) => {
      let childSelect = new ChildSelect();

      childSelect = service.addChild(childSelect);

      expect(childSelect.quantity).toBe(1);
      expect(childSelect.childrenAges.length).toBe(1);
    }));

    it('add child with age', inject([ChildService], (service: ChildService) => {
      let childSelect = new ChildSelect();
      const ageToAdd = 8;
      childSelect = service.addChild(childSelect, ageToAdd);

      expect(childSelect.childrenAges[0]).toBe(ageToAdd);
    }));

  });

  describe('Delete child', () => {

    it('delete child', inject([ChildService], (service: ChildService) => {
      let childSelect = new ChildSelect();
      childSelect = service.addChild(childSelect);

      childSelect = service.deleteChild(childSelect, 0);

      expect(childSelect.quantity).toBe(0);
      expect(childSelect.childrenAges.length).toBe(0);
    }));
  });

  describe('Update child list', () => {

    it('update empty child list ', inject([ChildService], (service: ChildService) => {
      let childSelect = new ChildSelect();
      childSelect.quantity = 6;

      childSelect = service.updateChild(childSelect);

      expect(childSelect.quantity).toBe(6);
      expect(childSelect.childrenAges.length).toBe(6);
    }));

    it('update populated child list ', inject([ChildService], (service: ChildService) => {
      let childSelect = new ChildSelect();
      childSelect = service.addChild(childSelect);
      childSelect = service.addChild(childSelect);
      childSelect.quantity = 10;

      childSelect = service.updateChild(childSelect);

      expect(childSelect.quantity).toBe(10);
      expect(childSelect.childrenAges.length).toBe(10);
    }));
  });


});
