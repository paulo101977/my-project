export class ChildTextConfig {
  title: string;
  cancelButton: string;
  okButton: string;
}
