import { Injectable } from '@angular/core';
import { ChildSelect } from '../models/child-select';

@Injectable({
  providedIn: 'root'
})
export class ChildService {

  constructor() { }

  public getChildrenAgeList(higherChildrenAge: number) {
    const listAges = [];
    if (higherChildrenAge > 0) {
      for (let i = 0; i <= higherChildrenAge; i++) {
        listAges.push({ value: i, text: i < 10 ? `0${i}` : `${i}`});
      }
    }
    return listAges;
  }

  public addChild(childrenSelect: ChildSelect, age?: number): ChildSelect {
    if (!childrenSelect.childrenAges) {
      childrenSelect.childrenAges = [];
    }
    childrenSelect.childrenAges[childrenSelect.childrenAges.length] = age ? age : 0;
    childrenSelect.quantity = childrenSelect.childrenAges.length;

    return childrenSelect;
  }

  public deleteChild(childrenSelect: ChildSelect, childIndex: number): ChildSelect {
    childrenSelect.childrenAges.splice(childIndex, 1);
    childrenSelect.quantity = childrenSelect.childrenAges.length;
    return childrenSelect;
  }

  public updateChild(childrenSelect: ChildSelect): ChildSelect {
    if (childrenSelect.quantity >= 0) {
      const qtd = childrenSelect.quantity;
      childrenSelect.childrenAges = [];
      for (let i = 0; i < qtd; i++) {
        childrenSelect.childrenAges[i] = 0;
      }
      childrenSelect.quantity = childrenSelect.childrenAges.length;
    }
    return childrenSelect;
  }

  public parse(ages: Array<number>): ChildSelect {
    const childSelect = new ChildSelect();
    childSelect.childrenAges = ages ? ages : [];
    return childSelect;
  }
}
