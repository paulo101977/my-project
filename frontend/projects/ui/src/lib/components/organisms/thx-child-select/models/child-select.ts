export class ChildSelect {
  private _childrenAges?: Array<number>;
  quantity = 0;

  public set childrenAges(ages: Array<number>) {
    this._childrenAges = ages ? ages : [];
    this.quantity = this._childrenAges.length;
  }

  public get childrenAges(): Array<number> {
    return this._childrenAges;
  }
}
