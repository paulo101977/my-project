import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ThxChildSelectComponent } from './thx-child-select.component';

describe('ChildSelectComponent', () => {
  let component: ThxChildSelectComponent;
  let fixture: ComponentFixture<ThxChildSelectComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxChildSelectComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxChildSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
