
import {fromEvent as observableFromEvent,  Subscription } from 'rxjs';
import {debounceTime, map} from 'rxjs/operators';
import { AfterViewInit, Component, ElementRef, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'thx-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SearchbarComponent),
      multi: true,
    },
  ],
})
export class SearchbarComponent implements AfterViewInit, ControlValueAccessor {
  @ViewChild('searchInput') public searchInput: ElementRef;

  @Input() public placeholder: string;

  @Output() public search: EventEmitter<string> = new EventEmitter<string>();

  public innerValue: string;

  private inputListener: Subscription;

  // Placeholders for the callbacks which are later provided by the Control Value Accessor
  private onTouchedCallback: () => void = () => {};
  private onChangeCallback: (_: any) => void = () => {};

  constructor() {
    this.placeholder = this.placeholder || 'sharedModule.components.thfSearchbar.placeholder';
  }

  public ngAfterViewInit() {
    this.setListeners();
  }

  public setListeners() {
    this.inputListener = observableFromEvent(this.searchInput.nativeElement, 'input')
      .pipe(
        map((event: Event) => (<HTMLInputElement>event.target).value),
        debounceTime(600)
      )
      .subscribe(data => this.emitSearchData());
  }

  public emitSearchData() {
    this.search.emit(this.innerValue);
  }

  get value(): any {
    return this.innerValue;
  }

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  onBlur() {
    this.onTouchedCallback();
  }

  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
