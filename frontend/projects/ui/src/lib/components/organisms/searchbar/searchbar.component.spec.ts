import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { SearchbarComponent } from './searchbar.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { CoreModule } from '@app/core/core.module';

describe('ThfSearchbarComponent', () => {
  let component: SearchbarComponent;
  let fixture: ComponentFixture<SearchbarComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()).configureTestingModule({
      declarations: [SearchbarComponent],
      imports: [CommonModule, FormsModule, TranslateTestingModule, CoreModule],
    });

    fixture = TestBed.createComponent(SearchbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(
    'should activate search when input is changed',
    fakeAsync(() => {
      const searchText = 'search';
      component.setListeners();

      component.search.subscribe(data => {
        expect(data).toEqual(searchText);
      });

      component.value = searchText;
      component.searchInput.nativeElement.value = searchText;
      component.searchInput.nativeElement.dispatchEvent(new Event('input'));

      tick(1000);
    }),
  );
});
