import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { DataTableGlobals } from '../thx-datatable/models/DataTableGlobals';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'thx-datatable',
  templateUrl: './thx-datatable.component.html',
})
export class ThxDatatableComponent implements OnInit {
  @ViewChild('defaultTemplate') defaultTemplate: TemplateRef<any>;
  @ViewChild('statusTemplate') statusTemplate: TemplateRef<any>;
  @ViewChild('optionsTemplate') optionsTemplate: TemplateRef<any>;
  @ViewChild('defaultLastCellTemplate') defaultLastCellTemplate: TemplateRef<any>;
  @ViewChild('deletLastCellTemplate') deletLastCellTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;

  @Input() columnMode = 'flex';
  @Input() tableHasScroll = false;
  @Input()
  set itens(itens: Array<any>) {
    if (itens) {
      this._itens = [...itens];
    }
  }

  get itens() {
    return this._itens;
  }

  @Input() statusIdentifier: string;
  @Input() statusTitle = 'label.active';
  @Input() statusPropName = 'isActive';
  @Input() itemsOption: Array<any>;
  @Input() rowHeight = 45;
  @Input() headerHeight = 45;
  @Input() footerHeight = 50;
  @Input() emptyMessage: string;
  @Input() emptyMessageContent: string;
  @Input() completeEmptyMessageContent: string;
  @Input() customTemplate: TemplateRef<any>;
  @Input() cellClass = 'no-submenu-options';
  @Input() hasLastCell = true;
  @Input() rowsLimit: number;
  @Input() tableExtraClass: string;
  @Input() deleteModeOption: boolean;

  @Output() statusChanged: EventEmitter<any> = new EventEmitter();
  @Output() rowItemClicked: EventEmitter<any> = new EventEmitter();
  @Output() emptyLinkClicked: EventEmitter<any> = new EventEmitter();
  @Output() deleteClicked: EventEmitter<any> = new EventEmitter();

  private _columns: Array<any>;
  private _itens: Array<any>;

  @Input()
  set columns(columns: Array<any>) {
    this._columns = [];
    if (columns) {
      if (this.statusIdentifier) {
        this._columns.push({
          cellTemplate: this.statusTemplate,
          headerTemplate: this.headerTemplate,
          name: this.statusTitle,
          prop: this.statusPropName,
          width: 100,
          draggable: false,
          canAutoResize: false,
          headerClass: 'thf-u-text-center',
          cellClass: 'thf-u-text-center',
        });
      } else if (this.customTemplate) {
        this._columns.push({
          cellTemplate: this.customTemplate,
        });
      }
      columns = columns.map(col => {
        const aux = col;
        if (!aux.hasOwnProperty('headerTemplate')) {
          aux.headerTemplate = this.headerTemplate;
        }
        if (!aux.hasOwnProperty('cellTemplate')) {
          aux.cellTemplate = this.defaultTemplate;
        }
        if (!col.hasOwnProperty('flexGrow')) {
          aux.flexGrow = 1;
        }
        return aux;
      });
      this._columns = this._columns.concat(columns);
      if (this.hasLastCell) {
        if (this.itemsOption) {
          this._columns.push({
            cellTemplate: this.optionsTemplate,
            name: '',
            cellClass: 'thf-u-no-padding',
            width: 50,
            resizeable: false,
            draggable: false,
            canAutoResize: false,
          });
        } else if (this.deleteModeOption) {
          this._columns.push({
            cellTemplate: this.deletLastCellTemplate,
            name: '',
            cellClass: 'thf-u-no-padding',
            width: 50,
            resizeable: false,
            draggable: false,
            canAutoResize: false,
          });
        } else {
          this._columns.push({
            cellTemplate: this.defaultLastCellTemplate,
            name: '',
            width: 80,
            resizeable: false,
            draggable: false,
            canAutoResize: false,
          });
        }
      }
    }
  }

  get columns() {
    return this._columns;
  }

  constructor(private dataTableGlobals: DataTableGlobals, public translateService: TranslateService) {
    if (!this.rowsLimit) {
      this.rowsLimit = dataTableGlobals.limitItensPage;
    }
  }

  ngOnInit() {}

  public itemWasUpdated($event): void {
    this.statusChanged.emit($event);
  }

  public runCallbackFunction(item: any, element: any, index: any) {
    if (item.callbackFunction) {
      item.callbackFunction(element, item.context, index);
    }
  }

  public goToCreatePage() {
    this.emptyLinkClicked.emit();
  }

  public clickRow(item) {
    if (event.type == 'click') {
      if (item.column.cellTemplate != this.statusTemplate && item.column.cellTemplate != this.optionsTemplate) {
        this.rowItemClicked.emit(item.row);
      }
    }
  }

  public delete(element) {
    this.deleteClicked.emit(element);
  }
}
