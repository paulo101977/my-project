import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxDatatableComponent } from './thx-datatable.component';
import { FormsModule } from '@angular/forms';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';
import { SubmenuOptionsModule } from '../../molecules/thx-submenu-options/submenu-options.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ImgCdnModule,
    SubmenuOptionsModule,
    TranslateModule,
    NgxDatatableModule
  ],
  declarations: [ ThxDatatableComponent ],
  exports: [ ThxDatatableComponent ]
})
export class ThxDatatableModule { }
