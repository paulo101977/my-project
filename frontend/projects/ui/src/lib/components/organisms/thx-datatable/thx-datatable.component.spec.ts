import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxDatatableComponent } from './thx-datatable.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';
import { SubmenuOptionsComponent } from '../../molecules/thx-submenu-options/submenu-options.component';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { CoreModule } from '@app/core/core.module';

describe('ThxDatatableComponent', () => {
  let component: ThxDatatableComponent;
  let fixture: ComponentFixture<ThxDatatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThxDatatableComponent, SubmenuOptionsComponent],
      imports: [NgxDatatableModule, FormsModule, TranslateTestingModule, CoreModule],
      providers: [],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
