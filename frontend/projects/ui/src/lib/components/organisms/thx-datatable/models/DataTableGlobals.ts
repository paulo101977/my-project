import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DataTableGlobals {
  limitItensPage = 10;
  limitItensModalPage = 4;
}
