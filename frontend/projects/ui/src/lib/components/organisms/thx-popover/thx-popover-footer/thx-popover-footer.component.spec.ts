import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxPopoverFooterComponent } from './thx-popover-footer.component';

describe('ThxPopoverFooterComponent', () => {
  let component: ThxPopoverFooterComponent;
  let fixture: ComponentFixture<ThxPopoverFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThxPopoverFooterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxPopoverFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
