import { ApplicationRef, Component, ComponentFactoryResolver, ElementRef, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import Popper from 'popper.js';
import PopperOptions = Popper.PopperOptions;

@Component({
  selector: 'thx-popover',
  templateUrl: './thx-popover.component.html',
  styleUrls: ['./thx-popover.component.scss'],
})
export class ThxPopoverComponent {
  @ViewChild('popoverContent') public popoverContent: ElementRef;

  public template: TemplateRef<any>;

  public referenceObject: HTMLElement;

  public popperOptions: PopperOptions;

  public popperInstance;

  public opacity: number;

  public displayType: string;

  public popoverColor: 'white' | 'blue' | 'blue-dark' = 'white';

  public static create(
    componentFactoryResolver: ComponentFactoryResolver,
    appRef: ApplicationRef,
    viewContainerRef: ViewContainerRef,
    templateRef: TemplateRef<any>,
    referenceObject: HTMLElement,
  ): ThxPopoverComponent {
    const componentFactory = componentFactoryResolver.resolveComponentFactory(ThxPopoverComponent);
    const _viewContainerRef = appRef.components[0].instance.viewContainerRef || viewContainerRef;
    const componentRef = _viewContainerRef.createComponent(componentFactory);

    componentRef.instance.template = templateRef;
    componentRef.instance.referenceObject = referenceObject;
    return componentRef.instance;
  }

  constructor() {
    this.popperOptions = {
      placement: 'bottom',
    };
  }

  public show(): void {
    if (!this.referenceObject) {
      return;
    }

    this.popperOptions.modifiers = {
      arrow: {
        enabled: true,
        element: this.popoverContent.nativeElement.parentElement.querySelector('.thf-popover-arrow'),
      },
    };

    this.popperInstance = new Popper(
      this.referenceObject,
      this.popoverContent.nativeElement.parentElement.parentElement,
      this.popperOptions,
    );

    this.popperInstance.scheduleUpdate();
    this.toggleVisibility(true);
  }

  public hide(): void {
    this.popperInstance.destroy();
    this.toggleVisibility(false);
  }

  public toggleVisibility(state: boolean) {
    if (!state) {
      this.opacity = 0;
      this.displayType = 'none';
    } else {
      this.opacity = 1;
      this.displayType = 'block';
    }
  }
}
