import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxPopoverComponent } from './thx-popover.component';
import { ThxPopoverHeaderComponent } from './thx-popover-header/thx-popover-header.component';
import { ThxPopoverFooterComponent } from './thx-popover-footer/thx-popover-footer.component';
import { ThxPopoverContentComponent } from './thx-popover-content/thx-popover-content.component';
import { ThxPopoverDirective } from './directives/thx-popover.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [
    ThxPopoverComponent,
    ThxPopoverHeaderComponent,
    ThxPopoverFooterComponent, ThxPopoverContentComponent,
    ThxPopoverDirective
  ],
  exports: [
    ThxPopoverComponent,
    ThxPopoverHeaderComponent,
    ThxPopoverFooterComponent,
    ThxPopoverContentComponent,
    ThxPopoverDirective
  ],
  entryComponents: [ThxPopoverComponent],
})
export class ThxPopoverModule {}
