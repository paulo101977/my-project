import {
  ApplicationRef,
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  HostListener,
  Input,
  Renderer2,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { ThxPopoverComponent } from '../thx-popover.component';

@Directive({
  selector: '[thxPopover]',
})
export class ThxPopoverDirective {
  @Input() public thxPopover: TemplateRef<any>;

  @Input() public placement: 'top' | 'left' | 'right' | 'bottom' = 'bottom';

  @Input() public popoverColor: 'white' | 'blue' | 'blue-dark' = 'white';

  @Input() public closeOnClick = false;

  private isShown: boolean;

  private globalClick: any;

  private popoverContainer: ThxPopoverComponent;

  constructor(
    public viewContainerRef: ViewContainerRef,
    public appRef: ApplicationRef,
    public elementRef: ElementRef,
    public renderer: Renderer2,
    private componentFactoryResolver: ComponentFactoryResolver,
  ) {}

  @HostListener('click')
  public onClick() {
    if (this.isShown) {
      this.hidePopover();
    } else {
      this.showPopover();
    }
  }

  public onClickOutside(event) {
    if (
      this.isShown && (
      !this.elementRef.nativeElement.contains(event.target) &&
      !this.popoverContainer.popoverContent.nativeElement.parentElement.contains(event.target) ) ||
      this.closeOnClick
    ) {
      this.hidePopover();
    }
  }

  public hidePopover() {
    this.popoverContainer.hide();
    this.globalClick();
    this.isShown = false;
  }

  public showPopover() {
    if (!this.popoverContainer) {
      this.createPopover();
    }
    this.globalClick = this.renderer.listen('document', 'click', this.onClickOutside.bind(this));
    this.popoverContainer.show();
    this.isShown = true;
  }

  public createPopover() {
    this.popoverContainer = ThxPopoverComponent.create(
      this.componentFactoryResolver,
      this.appRef,
      this.viewContainerRef,
      this.thxPopover,
      this.elementRef.nativeElement,
    );

    this.popoverContainer.popoverColor = this.popoverColor;

    this.popoverContainer.popperOptions = {
      placement: this.placement,
    };
  }
}
