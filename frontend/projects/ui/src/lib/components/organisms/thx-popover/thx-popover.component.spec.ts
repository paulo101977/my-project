import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxPopoverComponent } from './thx-popover.component';

describe('ThxPopoverComponent', () => {
  let component: ThxPopoverComponent;
  let fixture: ComponentFixture<ThxPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThxPopoverComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
