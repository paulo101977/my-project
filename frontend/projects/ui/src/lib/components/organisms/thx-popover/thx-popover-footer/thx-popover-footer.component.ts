import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'thx-popover-footer',
  templateUrl: './thx-popover-footer.component.html',
  styleUrls: ['thx-popover-footer.component.css'],
})
export class ThxPopoverFooterComponent implements OnInit {
  @Input() color: 'blue' | 'white' = 'white';

  constructor() {}

  ngOnInit() {}
}
