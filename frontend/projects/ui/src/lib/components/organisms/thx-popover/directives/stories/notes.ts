export class Notes {
    private notes = `
## Html:

~~~json
<div>
    <button #popover thxPopover [thxPopover]="templateTest">Open</button>

    <ng-template #templateTest>
        <thx-popover-header>
            <div>header test</div>
        </thx-popover-header>
        <thx-popover-content>
            <div>content test</div>
        </thx-popover-content>

        <thx-popover-footer color="blue">
            <div>footer test</div>
        </thx-popover-footer>
    </ng-template>
</div>
~~~

## Typescript:

~~~typescript
import {
    Component,
    ViewChild
} from '@angular/core';


import { ThxPopoverDirective } from '../thx-popover.directive';
import { ThxPopoverComponent } from '../../../components/thx-popover/thx-popover.component';

@Component({
    selector: 'thx-index-cp',
    templateUrl: './index.component.html',
    entryComponents: [ThxPopoverComponent]
})


export class IndexComponent {

    @ViewChild(ThxPopoverDirective) popover: ThxPopoverDirective;

}
~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
