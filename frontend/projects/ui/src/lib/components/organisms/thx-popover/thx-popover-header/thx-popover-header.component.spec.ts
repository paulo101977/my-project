import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxPopoverHeaderComponent } from './thx-popover-header.component';

describe('ThxPopoverHeaderComponent', () => {
  let component: ThxPopoverHeaderComponent;
  let fixture: ComponentFixture<ThxPopoverHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThxPopoverHeaderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxPopoverHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
