import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxPopoverContentComponent } from './thx-popover-content.component';

describe('ThxPopoverContentComponent', () => {
  let component: ThxPopoverContentComponent;
  let fixture: ComponentFixture<ThxPopoverContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThxPopoverContentComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxPopoverContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
