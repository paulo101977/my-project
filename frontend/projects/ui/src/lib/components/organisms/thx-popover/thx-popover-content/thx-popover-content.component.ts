import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'thx-popover-content',
  templateUrl: './thx-popover-content.component.html',
})
export class ThxPopoverContentComponent implements OnInit {
  @Input() padding = true;

  constructor() {}

  ngOnInit() {}
}
