import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';


import { IndexComponent } from './index.component';

import { Notes } from './notes';
// import to highlight generator
import { ThxPopoverModule } from '../../thx-popover.module';

storiesOf('Ui | Molecules/ThxPopover', module)
    .addDecorator(
        moduleMetadata({
          imports: [CommonModule, ThxPopoverModule]
        })
    )
    .add('Test markdown highlight',
      () => ({
        component: IndexComponent
      }),
      { notes: { markdown: new Notes().getNotes() } }
    );
