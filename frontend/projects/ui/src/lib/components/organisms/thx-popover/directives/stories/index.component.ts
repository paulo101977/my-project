import {
    Component,
    ViewChild
} from '@angular/core';


import { ThxPopoverDirective } from '../thx-popover.directive';
import { ThxPopoverComponent } from '../../thx-popover.component';

@Component({
  selector: 'thx-index-cp',
  templateUrl: './index.component.html',
  entryComponents: [ThxPopoverComponent]
})
export class IndexComponent {

    @ViewChild(ThxPopoverDirective) popover: ThxPopoverDirective;

}
