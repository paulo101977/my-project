import { CommonModule } from '@angular/common';
import { storiesOf, moduleMetadata  } from '@storybook/angular';

import { configureRecaptcha } from '../../../molecules/thx-recaptcha/models/recaptcha-global-config';
import { Notes } from './notes';

import { DefaultComponent } from './default';
import { LoginModule } from '../login.module';


storiesOf('Ui | Templates/Login', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule, LoginModule ],
    providers: [
      configureRecaptcha({sitekey: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'})
    ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
