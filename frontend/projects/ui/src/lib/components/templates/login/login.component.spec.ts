import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call submitForm', () => {
    spyOn(component.submitForm, 'emit');
    const form = {
      email: '',
      password: ''
    };

    component.onForm(form);

    expect(component.submitForm.emit).toHaveBeenCalledWith(form);
  });

  it('should call resetedPassword', () => {
    spyOn(component.resetedPassword, 'emit');
    const email = 'yoseph@yo.com';

    component.onResetPassword(email);

    expect(component.resetedPassword.emit).toHaveBeenCalledWith(email);
  });
});
