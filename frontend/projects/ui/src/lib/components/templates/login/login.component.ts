import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { ImgCdnPipe } from '../../../pipes/img-cdn/img-cdn.pipe';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'thx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @HostBinding('style.background') backgroundImage: SafeStyle;
  @Input() image: string;
  @Input() versionName = 'Horseshoe';
  @Input() versionNumber = '1.0';
  @Input() productName = 'PMS';
  @Input() productLogoImg = '';
  @Output() submitForm = new EventEmitter();
  @Output() resetedPassword = new EventEmitter();

  private readonly CONFIG_BACKGROUND_IMAGE = 'no-repeat center center / cover';
  private readonly CONFIG_OVERLAY_BACKGROUND_IMAGE = 'linear-gradient(0deg,rgba(0,0,0,0.15),rgba(0,0,0,0.15))';
  public year = new Date().getFullYear();

  constructor(
    private imgCdnPipe: ImgCdnPipe,
    private sanitizer: DomSanitizer
  ) {
  }

  ngOnInit() {
    if (this.image) {
      const image = this.imgCdnPipe.transform(this.image);
      this.backgroundImage = this.sanitizer.bypassSecurityTrustStyle(
        `${this.CONFIG_OVERLAY_BACKGROUND_IMAGE},url("${image}") ${this.CONFIG_BACKGROUND_IMAGE}`
      );
    }
  }

  public onForm(form) {
    this.submitForm.emit(form);
  }

  public onResetPassword(email) {
    this.resetedPassword.emit(email);
  }

}
