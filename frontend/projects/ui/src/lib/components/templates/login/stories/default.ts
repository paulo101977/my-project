import { Component  } from '@angular/core';
import { TitleConfig } from '../models/title-config.model';

@Component({
  selector: 'thx-default-login',
  template: `
    <thx-login
      [title]="titleConfig"
    >
    </thx-login>
  `,
  styles: []
})
export class DefaultComponent {
  public titleConfig: TitleConfig;

  constructor() {
    this.setTitleData();
  }

  private setTitleData() {
    this.titleConfig = new TitleConfig();
    this.titleConfig.welcome = 'Bem vindo ao';
    this.titleConfig.projectName = 'Super admin THEx';
  }
}
