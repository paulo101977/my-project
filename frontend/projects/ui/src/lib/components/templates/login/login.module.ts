import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { ThxFormLoginModule } from '../../organisms/thx-form-login/thx-form-login.module';
import { ImgCdnPipe } from '../../../pipes/img-cdn/img-cdn.pipe';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    ThxFormLoginModule,
    TranslateModule
  ],
  providers: [ImgCdnPipe],
  exports: [LoginComponent],
  declarations: [LoginComponent]
})

export class LoginModule {
}
