import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxBaseTopBarComponent } from './thx-base-top-bar.component';

@NgModule({
  declarations: [ThxBaseTopBarComponent],
  imports: [
    CommonModule
  ],
  exports: [ThxBaseTopBarComponent]
})
export class ThxBaseTopBarModule { }
