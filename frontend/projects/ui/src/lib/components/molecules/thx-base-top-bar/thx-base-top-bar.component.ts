import { Component, Input } from '@angular/core';

@Component({
  selector: 'thx-base-top-bar',
  templateUrl: './thx-base-top-bar.component.html',
  styleUrls: ['./thx-base-top-bar.component.scss']
})
export class ThxBaseTopBarComponent {

  @Input() title: string;
}
