import { withNotes } from '@storybook/addon-notes';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ThxBaseTopBarComponent } from '../thx-base-top-bar.component';
import { Notes } from './notes';
import { componentFactory } from '.storybook/component-factory';
import * as mdRef from '.storybook/utils';

const title = 'Meu Hotel';

const DefaultComponent = componentFactory(
    'default', `
    <thx-base-top-bar [title]="scope.title"></thx-base-top-bar>
 `, {title});

const SideContentComponent = componentFactory(
  'reactive-form', `
    <thx-base-top-bar [title]="scope.title">
        <div>Conteudo lateral</div>
    </thx-base-top-bar>
 `, {title});

const notes = mdRef.getHtmlString(new Notes().getNotes());
storiesOf('Ui | Molecules/ThxBaseTopBar', module)
    .addDecorator(withNotes)
    .addDecorator(
        moduleMetadata({
            declarations: [ ThxBaseTopBarComponent ],
        })
    )
    .add('Default', () => ({
            component: DefaultComponent
        }), { notes })
    .add(
      'Side Content',
      () => ({
        component: SideContentComponent
      }),
      { notes }
    );
