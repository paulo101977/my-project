import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ThxBaseTopBarComponent } from './thx-base-top-bar.component';

describe('ThxBaseTopBarComponent', () => {
  let component: ThxBaseTopBarComponent;
  let fixture: ComponentFixture<ThxBaseTopBarComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxBaseTopBarComponent ]
    });

    fixture = TestBed.createComponent(ThxBaseTopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
