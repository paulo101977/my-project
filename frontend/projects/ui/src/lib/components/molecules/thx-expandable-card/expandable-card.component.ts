import { Component, EventEmitter, Output } from '@angular/core';
import { ThxIconButtonSize } from '../thx-icon-button/enums/thx-icon-button-size.enum';
import { ThxIconButtonType } from '../thx-icon-button/enums/thx-icon-button-type.enum';

@Component({
  selector: 'thx-expandable-card',
  templateUrl: './expandable-card.component.html',
  styleUrls: ['./expandable-card.component.css']
})
export class ExpandableCardComponent  {

  // Button config
  public buttonSize = ThxIconButtonSize.Medium;
  public buttonType = ThxIconButtonType.Default;
  public buttonIcon = 'mdi-plus';

  // Component logic
  public cardOpen = false;

  @Output() toggleCard: EventEmitter<boolean> = new EventEmitter();

  public toggle() {
    this.cardOpen = !this.cardOpen;
    this.controlIcon();
    this.toggleCard.emit(this.cardOpen);
  }

  private controlIcon() {
    this.buttonIcon = this.cardOpen ? 'mdi-minus' : 'mdi-plus';
  }
}
