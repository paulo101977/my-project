import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandableCardComponent } from './expandable-card.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ThxIconButtonModule } from '../thx-icon-button/thx-icon-button.module';

describe('ExpandableCardComponent', () => {
  let component: ExpandableCardComponent;
  let fixture: ComponentFixture<ExpandableCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpandableCardComponent ],
      imports: [ ThxIconButtonModule ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandableCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
