import { Component } from '@angular/core';

@Component({
  selector: 'thx-default-expandable-card',
  template: `
  <thx-expandable-card>
    <div class="card-text-header">
      <div class="center" >Um header de card</div>
    </div>

    <div class="card-body-content">
      <div>Linha 1</div>
      <div>Linha 2</div>
      <div>Linha 3</div>
      <div>Linha 4</div>
      <div>Linha 5</div>
    </div>
  </thx-expandable-card>
  `,
  styles: ['.center { align-self: center} ']
})
export class DefaultComponent {

}
