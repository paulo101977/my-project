import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default';
import { ExpandableCardModule } from '../expandable-card.module';


storiesOf('Ui | Molecules/ThxExpandableCard', module)
  .addDecorator(
    moduleMetadata({
      imports: [CommonModule, ExpandableCardModule]
    })
  )
  .add('Default', () => ({
      component: DefaultComponent
    }),
      { notes: { markdown: new Notes().getNotes() }});
