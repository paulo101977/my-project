import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpandableCardComponent } from './expandable-card.component';
import { ThxIconButtonModule } from '../thx-icon-button/thx-icon-button.module';

@NgModule({
  imports: [
    CommonModule,
    ThxIconButtonModule
  ],
  declarations: [ExpandableCardComponent],
  exports: [ExpandableCardComponent]
})
export class ExpandableCardModule { }
