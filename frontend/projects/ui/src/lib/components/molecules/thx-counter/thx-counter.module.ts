import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ThxCounterComponent } from './thx-counter.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [ThxCounterComponent],
  declarations: [ThxCounterComponent]
})
export class ThxCounterModule { }
