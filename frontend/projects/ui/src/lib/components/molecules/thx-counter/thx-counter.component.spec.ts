import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { configureTestSuite } from 'ng-bullet';

import { ThxCounterComponent } from './thx-counter.component';

describe('ThxCounterComponent', () => {
  let component: ThxCounterComponent;
  let fixture: ComponentFixture<ThxCounterComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxCounterComponent ],
      imports: [ FormsModule ],
    });

    fixture = TestBed.createComponent(ThxCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
