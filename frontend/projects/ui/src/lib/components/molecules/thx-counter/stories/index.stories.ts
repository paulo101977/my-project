import { componentFactory } from '.storybook/component-factory';
import * as mdRef from '.storybook/utils';
import { withNotes } from '@storybook/addon-notes';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ThxCounterComponent } from '../thx-counter.component';
import { Notes } from './notes';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';

const value = 5;
const maxValue = 10;

const DefaultComponent = componentFactory(
    'default', `
    <thx-counter
      [(ngModel)]="scope.value"
     [maxValue]="scope.maxValue" >
    </thx-counter>
 `, {value, maxValue});

const ReactiveFormComponent = componentFactory(
  'reactive-form', `
    <div [formGroup]="scope.form">
      <thx-counter formControlName="adult"></thx-counter>
    </div>
 `, {form: new FormGroup({adult: new FormControl('1')})});

const notes = mdRef.getHtmlString(new Notes().getNotes());
storiesOf('Ui | Molecules/ThxCounter', module)
    .addDecorator(withNotes)
    .addDecorator(
        moduleMetadata({
            imports: [ ReactiveFormsModule ],
            declarations: [ ThxCounterComponent ],
        })
    )
    .add('Default', () => ({
            component: DefaultComponent
        }), { notes })
    .add(
      'Reactive Form',
      () => ({
        component: ReactiveFormComponent
      }),
      { notes }
    );
