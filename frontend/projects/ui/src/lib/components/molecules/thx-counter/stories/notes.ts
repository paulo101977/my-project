export class Notes {
    private notes = `
## Html:

~~~json
    <thx-counter
        [(ngModel)]="value">
    </thx-counter>

    <thx-counter
        [(ngModel)]="value"
        [maxValue]="maxValue" >
    </thx-counter>

~~~

## Typescript:

~~~typescript
    private maxValue = 10;
    public value = 5;
~~~
`;

    public getNotes(): string {
        return this.notes;
    }
}
