import { Component, Input, forwardRef} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'thx-counter',
  templateUrl: './thx-counter.component.html',
  styleUrls: ['./thx-counter.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ThxCounterComponent),
      multi: true

    }
  ]
})
export class ThxCounterComponent implements ControlValueAccessor {


  @Input() maxValue = -1;

  public value = 0;

  onTouch: () => void;
  onChange = (_: any) => {};

  writeValue(value: number): void {
    this.value = value;
    this.onChange(this.value);
  }

  registerOnChange(fn: (_: any) => {}): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => {}): void {
    this.onTouch = fn;
  }

  increment() {
    if ( this.maxValue != -1 || +this.value < this.maxValue ) {
      this.writeValue(this.value + 1);
    }
  }

  decrement() {
    if (+this.value > 0) {
      this.writeValue(this.value - 1);
    }
  }
}
