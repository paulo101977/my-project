export enum ThxIconButtonSize {
  Small = 'small',
  Medium = 'medium',
  Larger = 'larger'
}
