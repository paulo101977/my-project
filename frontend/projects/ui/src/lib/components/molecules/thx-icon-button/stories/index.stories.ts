import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';
import { ThxIconButtonModule } from '../thx-icon-button.module';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default';
import { OutlineComponent } from './outline';


storiesOf('Ui | Molecules/ThxIconButton', module)
  .addDecorator(
    moduleMetadata({
      imports: [CommonModule, ThxIconButtonModule]
    })
  )
  .add('Default', () => ({
      component: DefaultComponent
    }),
      { notes: { markdown: new Notes().getNotes() }})
  .add('Outline', () => ({
      component: OutlineComponent
    }),
      { notes: { markdown: new Notes().getNotes() }});
