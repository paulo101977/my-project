import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxIconButtonComponent } from './thx-icon-button.component';
import { ThxButtonModule } from '../../atoms/thx-button/thx-button.module';
import { ThxIconModule } from '../../atoms/thx-icon/thx-icon.module';

describe('ThxIconButtonComponent', () => {
  let component: ThxIconButtonComponent;
  let fixture: ComponentFixture<ThxIconButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxIconButtonComponent ],
      imports: [
        ThxButtonModule,
        ThxIconModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxIconButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
