import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { ThxIconButtonType } from './enums/thx-icon-button-type.enum';
import { ThxIconButtonSize } from './enums/thx-icon-button-size.enum';
import { ThxIconColor } from '../../atoms/thx-icon/enums/thx-icon-color.enum';

@Component({
  selector: 'thx-icon-button',
  template: `
    <a class="thx-icon-btn-round"
       (click)="handeClick($event)"
       [ngClass]="[type, size]">
      <thx-icon [type]="icon" [color]="color"></thx-icon>
    </a>
  `,
  styleUrls: ['./thx-icon-button.component.scss']
})
export class ThxIconButtonComponent implements OnInit {

  @Input() size: ThxIconButtonSize = ThxIconButtonSize.Small;
  @Input() type: ThxIconButtonType = ThxIconButtonType.Default;
  @Input() icon = '';
  color: ThxIconColor = ThxIconColor.Secondary;
  @Output() buttonClick = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.color = this.type == ThxIconButtonType.Default
      ? ThxIconColor.Secondary
      : ThxIconColor.Primary;
  }

  public handeClick(event) {
    this.buttonClick.emit(event);
  }

  @HostListener('mouseover', ['$event']) handleMouseOver(event: Event) {
    this.color = this.type == ThxIconButtonType.Default
      ? ThxIconColor.Primary
      : ThxIconColor.Secondary;
  }

  @HostListener('mouseleave', ['$event']) handleMouseLeave(event: Event) {
    this.color = this.type == ThxIconButtonType.Default
      ? ThxIconColor.Secondary
      : ThxIconColor.Primary;
  }

}
