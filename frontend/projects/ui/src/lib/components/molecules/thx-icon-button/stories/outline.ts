import { Component } from '@angular/core';
import { ThxIconButtonType } from '../enums/thx-icon-button-type.enum';
import { ThxIconButtonSize } from '../enums/thx-icon-button-size.enum';

@Component({
  selector: 'thx-outline-icon-button',
  template: `
    <div class="d-flex justify-content-center">
      <div class="col thf-u-text-center">
        <div>16px</div>
        <thx-icon-button [type]="type" [size]="small" [icon]="'mdi-account'">
        </thx-icon-button>
      </div>
      <div class="col thf-u-text-center">
        <div>32px</div>
        <thx-icon-button [type]="type" [size]="medium" [icon]="'mdi-plus'">
        </thx-icon-button>
      </div>
      <div class="col thf-u-text-center">
        <div>64px</div>
        <thx-icon-button [type]="type" [size]="larger" [icon]="'mdi-eye'">
        </thx-icon-button>
      </div>
    </div>
  `,
  styles: [`
    :host {
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
    }
  `
  ]
})
export class OutlineComponent {
  type = ThxIconButtonType.Outline;
  small = ThxIconButtonSize.Small;
  medium = ThxIconButtonSize.Medium;
  larger = ThxIconButtonSize.Larger;
}
