import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxIconButtonComponent } from './thx-icon-button.component';
import { ThxIconModule } from '../../atoms/thx-icon/thx-icon.module';
import { ThxButtonModule } from '../../atoms/thx-button/thx-button.module';


@NgModule({
  imports: [CommonModule, ThxButtonModule, ThxIconModule],
  declarations: [ThxIconButtonComponent],
  exports: [ThxIconButtonComponent]

})
export class ThxIconButtonModule {
}
