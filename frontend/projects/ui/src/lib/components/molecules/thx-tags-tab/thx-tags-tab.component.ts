import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Tag } from './models/tag';

@Component({
  selector: 'thx-tags-tab',
  templateUrl: './thx-tags-tab.component.html',
  styleUrls: ['./thx-tags-tab.component.scss']
})
export class ThxTagsTabComponent {
  private selectedTag: Tag;
  private hoveredTag: Tag;

  @Input() tags: Tag[];
  @Output() selected = new EventEmitter<Tag>();
  @Output() unselected = new EventEmitter<Tag>();

  public onMouseEnterTag(tag: Tag): void {
    this.hoveredTag = tag;
  }

  public onMouseLeaveTag(tag: Tag): void {
    if (this.hoveredTag == tag) {
      this.hoveredTag = null;
    }
  }

  public selectTag(tag: Tag): void {
    if (tag == this.selectedTag) {
      return;
    }

    this.selectedTag = tag;
    this.selected.emit(this.selectedTag);
  }

  public unselectTag(): void {
    if (!this.selectedTag) {
      return;
    }

    const tag: Tag = this.selectedTag;
    this.selectedTag = null;
    this.unselected.emit(tag);
  }

  public getBorderColor(tag: Tag): string {
    const isSelected = tag != null && this.selectedTag == tag;
    return isSelected ? tag.color : 'transparent';
  }

  public getSvg(tag: Tag): string {
    if (tag == null) {
      return '';
    }

    const isSelected = this.selectedTag == tag;
    const isHovered = this.hoveredTag == tag;
    return (isSelected || isHovered) ? tag.activeSvg : tag.idleSvg;
  }
}
