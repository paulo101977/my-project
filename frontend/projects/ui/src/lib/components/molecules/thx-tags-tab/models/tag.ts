import { Card } from '../../thx-card/model/card';

export interface Tag {
  id: any;
  title: string;
  idleSvg: string;
  activeSvg: string;
  color: string;
  cardList?: Array<Card>;
}
