import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ImgCdnPipeStub } from '../../../pipes/img-cdn/testing/img-cdn.pipe.stub';
import { Tag } from './models/tag';

import { ThxTagsTabComponent } from './thx-tags-tab.component';

describe('ThxTagsTabComponent', () => {
  let component: ThxTagsTabComponent;
  let fixture: ComponentFixture<ThxTagsTabComponent>;

  const tag: Tag = {
    activeSvg: 'activeSvg',
    color: 'color',
    id: 'id',
    idleSvg: 'idleSvg',
    title: 'title'
  };
  const newTag = { ...tag };

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxTagsTabComponent, ImgCdnPipeStub ]
    });

    fixture = TestBed.createComponent(ThxTagsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#onMouseEnterTag', () => {
    it('should set the tag as the hoveredTag', () => {
      expect(component['hoveredTag']).toBeUndefined();

      component.onMouseEnterTag(tag);

      expect(component['hoveredTag']).toBe(tag);
    });
  });

  describe('#onMouseLeaveTag', () => {
    it('should unset the hoveredTag if it equals to the tag', () => {
      component.onMouseEnterTag(tag);
      expect(component['hoveredTag']).toBe(tag);

      component.onMouseLeaveTag(tag);

      expect(component['hoveredTag']).toBeNull();
    });

    it('shouldn\'t unset the hoveredTag if it differs from the tag', () => {
      component.onMouseEnterTag(tag);
      expect(component['hoveredTag']).toBe(tag);

      component.onMouseLeaveTag(newTag);

      expect(component['hoveredTag']).toBe(tag);
    });
  });

  describe('#selectTag', () => {
    it('should select the tag', () => {
      expect(component['selectedTag']).toBeUndefined();
      component.selectTag(tag);

      expect(component['selectedTag']).toBe(tag);
    });

    it('should replace the selectedTag if it passes a new one', () => {
      component.selectTag(tag);
      expect(component['selectedTag']).toBe(tag);

      component.selectTag(newTag);
      expect(component['selectedTag']).toBe(newTag);
    });

    it('should do nothing if tag equals to selectedTag', () => {
      spyOn(component.selected, 'emit');

      component.selectTag(tag);
      component.selectTag(tag);

      expect(component.selected.emit).toHaveBeenCalledTimes(1);
    });
  });

  describe('#unselectTag', () => {
    it('should unselect the tag', () => {
      component.selectTag(tag);
      expect(component['selectedTag']).toBe(tag);

      component.unselectTag();
      expect(component['selectedTag']).toBe(null);
    });

    it('should do nothing if there is no selectedTag', () => {
      spyOn(component.unselected, 'emit');

      component.selectTag(tag);
      component.unselectTag();
      component.unselectTag();

      expect(component.unselected.emit).toHaveBeenCalledTimes(1);
    });
  });

  describe('#getBorderColor', () => {
    it('should return the tag color if the tag is the selectedTag', () => {
      component.selectTag(tag);
      const color = component.getBorderColor(tag);

      expect(color).toBe(tag.color);
    });

    it('should return transparent if the tag isn\'t the selectedTag', () => {
      component.selectTag(tag);
      const color = component.getBorderColor(newTag);

      expect(color).toBe('transparent');
    });

    it('should return transparent if the tag is null', () => {
      component.selectTag(tag);
      const color = component.getBorderColor(null);

      expect(color).toBe('transparent');
    });
  });

  describe('#getSvg', () => {
    it('should return the tag activeSvg if the tag is the selectedTag', () => {
      component.selectTag(tag);
      const svg = component.getSvg(tag);

      expect(svg).toBe(tag.activeSvg);
    });

    it('should return the tag activeSvg if the tag is the hoveredTag', () => {
      component.onMouseEnterTag(tag);
      const svg = component.getSvg(tag);

      expect(svg).toBe(tag.activeSvg);
    });

    it('should return the an empty string if the tag null', () => {
      const svg = component.getSvg(null);

      expect(svg).toBe('');
    });
  });
});
