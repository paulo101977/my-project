import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';
import { ThxTagsTabComponent } from './thx-tags-tab.component';

@NgModule({
  imports: [
    CommonModule,
    ImgCdnModule
  ],
  exports: [
    ThxTagsTabComponent
  ],
  declarations: [ ThxTagsTabComponent ],
})
export class ThxTagsTabModule { }
