import { Component } from '@angular/core';

@Component({
  selector: 'thx-card-base',
  templateUrl: './card-base.component.html',
  styleUrls: ['./card-base.component.scss']
})
export class CardBaseComponent {}
