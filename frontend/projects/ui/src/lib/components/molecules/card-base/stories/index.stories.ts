import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';
import { CardBaseComponent } from '../card-base.component';
import { componentFactory } from '.storybook/component-factory';

const DefaultComponent = componentFactory(
    'default', `
    <thx-card-base>
        <div contentLeft class="col-3">Left</div>
        <div contentRight class="col-9">Right</div>
    </thx-card-base>
 `);

const notes = new Notes().getNotes();
storiesOf('Ui | Molecules/CardBase', module)
    .addDecorator(
        moduleMetadata({
            declarations: [ CardBaseComponent ],
        })
    )
    .add('Default', () => ({
            component: DefaultComponent
        }), { notes: { markdown: notes } });
