import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';


import { ThxIconTextComponent } from './thx-icon-text.component';

@NgModule({
  imports: [
    CommonModule,
    ThxImgModule
  ],
  declarations: [ThxIconTextComponent],
  exports: [ThxIconTextComponent]
})
export class ThxIconTextModule { }
