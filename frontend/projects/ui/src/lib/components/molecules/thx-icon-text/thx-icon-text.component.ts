import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'thx-icon-text',
  templateUrl: './thx-icon-text.component.html',
  styleUrls: ['./thx-icon-text.component.scss']
})
export class ThxIconTextComponent implements OnInit {

  @Input() src: string;
  @Input() alt: string;
  @Input() title: string;
  @Input() count: number;
  @Input() forceIcon = false;

  constructor() { }

  ngOnInit() {
  }

}
