import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ThxIconTextComponent } from './thx-icon-text.component';

describe('ThxIconTextComponent', () => {
  let component: ThxIconTextComponent;
  let fixture: ComponentFixture<ThxIconTextComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxIconTextComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxIconTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
