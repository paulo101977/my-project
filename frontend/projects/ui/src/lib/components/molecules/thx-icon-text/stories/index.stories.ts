import { componentFactory } from '.storybook/component-factory';
import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ThxIconTextModule } from '../thx-icon-text.module';
import { Notes } from './notes';


const src = 'berco.min.svg';
const title = 'Berço';
const alt = title;
const count = 1;

const DefaultComponent = componentFactory('default', `
    <div class="row">
      <div class="col-3">
        <thx-icon-text
          [src]="scope.src"
          [alt]="scope.alt"
          [title]="scope.title"
          [count]="scope.count"
        >
        </thx-icon-text>
      </div>
    </div>
    <div class="row">
      <div class="col-2">
        <thx-icon-text
          [src]="scope.src"
          [alt]="scope.alt"
          [title]="scope.title"
          [count]="scope.count"
        >
        </thx-icon-text>
      </div>
    </div>
`, { src, title, alt, count });

const WithoutIconComponent = componentFactory('without-icon', `
    <div class="row">
      <div class="col-3">
        <thx-icon-text
          [title]="scope.title"
          [count]="scope.count"
        >
        </thx-icon-text>
      </div>
    </div>
`, { title, count });

const ForcedIconComponent = componentFactory('force-icon', `
    <div class="row">
      <div class="col">
        <thx-icon-text
          [src]="scope.src"
          [alt]="scope.alt"
          [title]="scope.title"
          [count]="scope.count"
        >
        </thx-icon-text>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <thx-icon-text
          [src]="'doubleBed-icon.svg'"
          [alt]="'Cama Dupla'"
          [title]="'Cama Dupla'"
          [count]="scope.count"
        >
        </thx-icon-text>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <thx-icon-text
          [forceIcon]="true"
          [title]="scope.title"
          [count]="scope.count"
        >
        </thx-icon-text>
      </div>
    </div>
`, { src, title, alt, count });

const WithoutCountComponent = componentFactory('without-count', `
    <div class="row">
      <div class="col">
        <thx-icon-text
          [src]="scope.src"
          [alt]="scope.alt"
          [title]="scope.title"
        >
        </thx-icon-text>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <thx-icon-text
          [src]="'doubleBed-icon.svg'"
          [alt]="'Cama Dupla'"
          [title]="'Cama Dupla'"
        >
        </thx-icon-text>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <thx-icon-text
          [forceIcon]="true"
          [title]="scope.title"
        >
        </thx-icon-text>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <thx-icon-text
          [title]="scope.title"
        >
        </thx-icon-text>
      </div>
    </div>
`, { src, title, alt, count });

storiesOf('Ui | Molecules/ThxIconText', module)
  .addDecorator(
    moduleMetadata({
      imports: [CommonModule, ThxIconTextModule],
      declarations: []
    })
  )
  .add('Default', () => ({
    component: DefaultComponent
  }),
      { notes: { markdown: new Notes().getNotes() }})
  .add(
    'Without Icon',
    () => ({
      component: WithoutIconComponent
    }),
    { notes: { markdown: new Notes().getNotes() }}
  )
  .add(
    'Forced Icon',
    () => ({
      component: ForcedIconComponent
    }),
    { notes: { markdown: new Notes().getNotes() }}
  )
  .add(
    'Without Count',
    () => ({
      component: WithoutCountComponent
    }),
    { notes: { markdown: new Notes().getNotes() }}
  );
