export class Notes {
  private notes = `
## Html:

~~~json
<thx-icon-text
    [src]="scope.src"
    [alt]="scope.alt"
    [title]="scope.title"
    [total]="scope.total"
    [hasIcon]="scope.hasIcon"
>
</thx-icon-text>

~~~

## Typescript:

~~~typescript

    public src = 'berco.min.svg';
    public title = 'Berço';
    public alt = title;
    public total = 1;
    public hasIcon = true;

~~~
    `;

  public getNotes(): string {
    return this.notes;
  }
}
