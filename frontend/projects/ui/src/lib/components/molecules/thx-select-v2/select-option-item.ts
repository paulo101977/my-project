export interface SelectOptionItem {
  leftIcon?: string;
  text: string;
  value: string;
  rightIcon?: string;
}

export const toSelectOption = (list: any[], text?: string, value?: string) => {
  if (list) {
    return list.map( item => {
      return {
        text: item[text],
        value: item[value]
      };
    });
  }
  return [];
};
