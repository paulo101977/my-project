import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxSelectV2Component } from './thx-select-v2.component';

describe('ThxSelectV2Component', () => {
  let component: ThxSelectV2Component;
  let fixture: ComponentFixture<ThxSelectV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxSelectV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxSelectV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
