import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';
import { DefaultComponent } from './default';
import { TranslateModule } from '@ngx-translate/core';
import { ThxInputV2Module } from '../../thx-input-v2/thx-input-v2.module';
import { ThxSelectV2Component } from '../thx-select-v2.component';
import { ThxImgModule } from '../../../atoms/thx-img/thx-img.module';
import { InputBaseEfectV2Module } from '../../../atoms/input-base-efect-v2/input-base-efect-v2.module';
import { ThxInputBaseComponent } from '../../thx-input-v2/thx-input-base/thx-input-base.component';


storiesOf('Ui | Molecules/V2/ThxSelectV2', module)
  .addDecorator(
    moduleMetadata({
      imports: [ CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        ThxImgModule,
        InputBaseEfectV2Module,
        ThxInputV2Module
      ],
      declarations: [ DefaultComponent, ThxSelectV2Component ],
    })
  )
  .add('Default', () => ({
      component: DefaultComponent
    }),
    { notes: { markdown: new Notes().getNotes() }});
