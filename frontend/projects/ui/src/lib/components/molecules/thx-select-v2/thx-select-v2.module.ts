import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxSelectV2Component } from './thx-select-v2.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThxInputV2Module } from '../thx-input-v2/thx-input-v2.module';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';
import { InputBaseEfectV2Module } from '../../atoms/input-base-efect-v2/input-base-efect-v2.module';

@NgModule({
  declarations: [ThxSelectV2Component],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ThxImgModule,
    InputBaseEfectV2Module,
    ThxInputV2Module
  ],
  exports: [ThxSelectV2Component]
})
export class ThxSelectV2Module { }
