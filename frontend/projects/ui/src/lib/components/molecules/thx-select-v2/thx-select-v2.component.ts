import { Attribute, Component, ElementRef, forwardRef, HostListener, Input } from '@angular/core';
import { ThxInput } from '../thx-input-v2/thx-input-base/thx-input';
import { SelectOptionItem } from './select-option-item';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'thx-select-v2',
  templateUrl: './thx-select-v2.component.html',
  styleUrls: ['./thx-select-v2.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ThxSelectV2Component),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ThxSelectV2Component),
      multi: true
    }
  ]
})
export class ThxSelectV2Component extends ThxInput {

  constructor( @Attribute('required') public required, private eRef: ElementRef ) {
    super(required);
  }
  @Input() tabindex = 0;

  private _optionList: Array<SelectOptionItem>;
  @Input('optionList')
  set optionList ( list: Array<SelectOptionItem>) {
    this._optionList = list;
    if (this.value && !this.selectedItem) {
      this.setSelectedItemByValue(this.value);
    }
  }
  get optionList(): Array<SelectOptionItem> {
    return this._optionList;
  }

  public isClosed = true ;
  public selectedItem: SelectOptionItem = null;


  isLabelTopMode(): any {
    return this.selectedItem;
  }

  public chooseItem(item: SelectOptionItem) {
    this.writeValue(item.value);
    this.isClosed = true;
  }

  @HostListener('document:click', ['$event'])
  close(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.isClosed = true;
    }
  }

  public toggleSelect() {
    this.isClosed = false;
      if (this.control) {
        this.control.markAsTouched();
      }
  }

  writeValue(val: string): void {
    this.value = val;
    this.setSelectedItemByValue(val);
    this.onChange(val);
  }

  public setSelectedItemByValue(value: string) {
    if (this.optionList) {
      this.selectedItem = this.optionList.find( item => item.value == value);
    }
  }
}
