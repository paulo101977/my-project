import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectOptionItem } from '../select-option-item';

@Component({
  selector: 'thx-default-textarea-v2',
  template: `
    <div style="padding: 100px">
      <div class="row">
        <div class="col-4" >
          <thx-select-v2 [(ngModel)]="val1" [title]="title" [optionList]="optionList" [infoText]="infoText"></thx-select-v2>
        </div>

        <div class="col-4" >
          <thx-select-v2 [(ngModel)]="value" [title]="title" [optionList]="optionList" [infoText]="infoText"></thx-select-v2>
          <br>
          <div>NGMODEL : {{value}}</div>
        </div>
        <div class="col-4" [formGroup]="form" >
          <thx-select-v2 formControlName="item" [title]="title" [optionList]="optionList" [infoText]="infoText"></thx-select-v2>
          <br>
          <div>FORM : {{form?.value | json }}</div>
        </div>
      </div>
      <br><br>
      <div class="row">
        <div class="col-4" >
          <thx-select-v2 [(ngModel)]="val3" [title]="title" [optionList]="optionListImg" required="true" ></thx-select-v2>
        </div>
        <div class="col-4" >
          <thx-select-v2 [title]="title" [optionList]="optionListRightImg"></thx-select-v2>
        </div>
      </div>
      <br>
      <br>
      <br>
    </div>`,
  styles: []
})
export class DefaultComponent implements OnInit {

  public title = 'Escolha o tipo de UH';
  public infoText = 'Um select de coisas legais';
  public value = '2';
  public val3;
  public val1;
  public optionList: SelectOptionItem[] = [
    {
      text: `Item 1`,
      value: `1`
    },
    {
      text: `Item 2`,
      value: `2`
    },
    {
      text: `Item 3`,
      value: `3`
    },
    {
      text: `Item 4`,
      value: `4`
    },
    {
      text: `Item 5`,
      value: `5`
    },
    {
      text: `Item 6`,
      value: `6`
    },
  ];

  public optionListImg = (<SelectOptionItem[]>JSON.parse(JSON.stringify(this.optionList))).map( item => {
    if ( +item.value % 2 == 0) {
      item.leftIcon = 'qtdAdulto.min.svg';
    } else {
      item.leftIcon = 'QtdCriancas.min.svg';
    }
    return item;
  });

  public optionListRightImg = (<SelectOptionItem[]>JSON.parse(JSON.stringify(this.optionList))).map( item => {
    if ( +item.value % 2 == 0) {
      item.rightIcon = 'qtdAdulto.min.svg';
    } else {
      item.rightIcon = 'QtdCriancas.min.svg';
    }
    return item;
  });

  form: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      item: [null, [Validators.required]],
    });

  }
}
