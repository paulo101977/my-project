import { Component, DoCheck, Input, OnInit } from '@angular/core';
import { AbstractControl, AbstractControlDirective } from '@angular/forms';
import { ThxErrorService } from './services/thx-error.service';

@Component({
  selector: 'thx-error-v2',
  templateUrl: './thx-error-v2.component.html',
  styleUrls: ['./thx-error-v2.component.css'],
})
export class ThxErrorV2Component implements OnInit, DoCheck {

  @Input() public control: AbstractControlDirective | AbstractControl;
  public error: { key: string; message: any };

  constructor( private errorService: ThxErrorService ) {}

  ngOnInit() {}

  ngDoCheck() {
    this.showErrorMessage();
  }

  showErrorMessage() {
    this.error = this.errorService.getError(this.control);
  }
}
