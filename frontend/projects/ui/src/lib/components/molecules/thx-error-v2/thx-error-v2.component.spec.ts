import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { ThxErrorV2Component } from './thx-error-v2.component';

describe('ThxErrorV2Component', () => {
  let component: ThxErrorV2Component;
  let fixture: ComponentFixture<ThxErrorV2Component>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ThxErrorV2Component],
      imports: [TranslateTestingModule],
    });

    fixture = TestBed.createComponent(ThxErrorV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
