import { inject, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';

import { ThxErrorService } from './thx-error.service';

describe('ThxErrorService', () => {
  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [ThxErrorService],
      imports: [TranslateTestingModule]
    });
  });

  it('should be created', inject([ThxErrorService], (service: ThxErrorService) => {
    expect(service).toBeTruthy();
  }));
});
