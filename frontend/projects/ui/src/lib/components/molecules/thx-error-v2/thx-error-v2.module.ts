import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ThxErrorV2Component } from './thx-error-v2.component';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ThxImgModule,
    TranslateModule
  ],
  declarations: [ ThxErrorV2Component ],
  exports: [ ThxErrorV2Component ]
})
export class ThxErrorV2Module { }
