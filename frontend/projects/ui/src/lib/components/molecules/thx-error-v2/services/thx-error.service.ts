import { Injectable } from '@angular/core';
import { AbstractControl, AbstractControlDirective } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class ThxErrorService {

  private errorMessages = {
    required: () => this.translateService.instant('validationError.required'),
    email: () => this.translateService.instant('validationError.email'),
    minlength: params => this.translateService.instant('validationError.minLength', { requiredLength: params.requiredLength }),
    maxlength: params => this.translateService.instant('validationError.maxLength', { requiredLength: params.requiredLength }),
    // TODO: analyze to remove the errors below and unify the keys
    requiredAndMajorThanZero: () => this.translateService.instant('commomData.errorMessages.requiredAndMajorThanZero'),
    notAcceptNegatives: () => this.translateService.instant('commomData.errorMessages.notAcceptNegatives'),
    majorThanZero: () => this.translateService.instant('commomData.errorMessages.majorThanZero'),
    invalidEmail: () => this.translateService.instant('commomData.errorMessages.invalidEmail'),
    matchPassword: () => this.translateService.instant('commomData.errorMessages.matchPassword'),
    onlyNumbers: () => this.translateService.instant('text.onlyNumbers'),
    invalidPortugueseNif: () => this.translateService.instant('validationError.invalidPortugueseNif'),
    invalidDocument: params => this.translateService.instant('validationError.invalidDocument', {document: params.document}),
    emptyString: () => this.translateService.instant('validationError.emptyString')
  };

  constructor(
    private translateService: TranslateService,
  ) {}

  private errorMessageHandler = (message) => {
    return this.translateService.instant(message);
  }

  public getError(control: AbstractControlDirective | AbstractControl): any {
    if (control && control.touched && control.invalid) {
      const error = this.getFirstError(control);
      if (error) {
        return {
          key: error,
          message: this.getMessage(error, control.errors[error]),
        };
      }
    }
    return null;
  }

  private getFirstError(control: AbstractControlDirective | AbstractControl ): string {
    const errors = this.getErrors(control);
    return errors && errors[0];
  }

  private getErrors(control: AbstractControlDirective | AbstractControl): string[] {
    return Object.keys(control.errors);
  }

  private getMessage(type: string, params: any) {
    return !this.errorMessages[type]
      ? this.errorMessageHandler(params)
      : this.errorMessages[type] && this.errorMessages[type](params);
  }
}
