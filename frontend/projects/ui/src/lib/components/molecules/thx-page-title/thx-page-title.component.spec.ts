import { ComponentFixture, TestBed } from '@angular/core/testing';
import { locationStub } from '../../../../../../../testing';

import { ThxPageTitleComponent } from './thx-page-title.component';
import { configureTestSuite } from 'ng-bullet';

describe('ThxPageTitleComponent', () => {
  let component: ThxPageTitleComponent;
  let fixture: ComponentFixture<ThxPageTitleComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ThxPageTitleComponent],
      providers: [locationStub]
    });

    fixture = TestBed.createComponent(ThxPageTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
