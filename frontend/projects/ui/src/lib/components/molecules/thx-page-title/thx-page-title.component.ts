import { Component, Input, OnInit } from '@angular/core';
import { ThxIconButtonSize } from '../thx-icon-button/enums/thx-icon-button-size.enum';
import { ThxIconButtonType } from '../thx-icon-button/enums/thx-icon-button-type.enum';
import { Location } from '@angular/common';

@Component({
  selector: 'thx-page-title',
  templateUrl: './thx-page-title.component.html',
  styleUrls: ['./thx-page-title.component.css']
})
export class ThxPageTitleComponent implements OnInit {

  @Input() title = '';
  size: ThxIconButtonSize = ThxIconButtonSize.Medium;
  type: ThxIconButtonType = ThxIconButtonType.Outline;

  constructor(
    private location: Location
  ) {
  }

  ngOnInit() {
  }

  public goBackPreviewPage() {
    this.location.back();
  }
}
