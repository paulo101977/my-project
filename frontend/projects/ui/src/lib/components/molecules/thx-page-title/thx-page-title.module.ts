import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxPageTitleComponent } from './thx-page-title.component';
import { ThxIconButtonModule } from '../thx-icon-button/thx-icon-button.module';

@NgModule({
  imports: [
    CommonModule,
    ThxIconButtonModule
  ],
  exports: [
    ThxPageTitleComponent
  ],
  declarations: [ThxPageTitleComponent]
})
export class ThxPageTitleModule { }
