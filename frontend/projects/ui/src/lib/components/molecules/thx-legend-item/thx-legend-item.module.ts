import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxLegendItemComponent } from './thx-legend-item.component';
import { ThxSquareColorModule } from '../../atoms/thx-square-color/thx-square-color.module';

@NgModule({
  imports: [
    CommonModule,
    ThxSquareColorModule
  ],
  declarations: [ThxLegendItemComponent],
  exports: [ThxLegendItemComponent]
})
export class ThxLegendItemModule { }
