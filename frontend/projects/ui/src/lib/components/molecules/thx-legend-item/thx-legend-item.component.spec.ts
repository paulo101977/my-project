import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxLegendItemComponent } from './thx-legend-item.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ThxSquareColorModule } from '../../atoms/thx-square-color/thx-square-color.module';

describe('ThxLegendItemComponent', () => {
  let component: ThxLegendItemComponent;
  let fixture: ComponentFixture<ThxLegendItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxLegendItemComponent ],
      imports: [ ThxSquareColorModule ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxLegendItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
