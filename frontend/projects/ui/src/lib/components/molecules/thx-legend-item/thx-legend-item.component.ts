import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'thx-legend-item',
  template: `
    <thx-square-color [color]="color" [width]="width" [height]="height"></thx-square-color>
    <div class="legend-item-text">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./thx-legend-item.component.css']
})
export class ThxLegendItemComponent {

  @Input() color: string;
  @Input() width = 10;
  @Input() height = 10;
}
