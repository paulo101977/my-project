import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoTooltipComponent } from './info-tooltip.component';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';

@NgModule({
  imports: [
    CommonModule,
    ImgCdnModule
  ],
  exports: [
    InfoTooltipComponent
  ],
  declarations: [InfoTooltipComponent]
})
export class InfoTooltipModule { }
