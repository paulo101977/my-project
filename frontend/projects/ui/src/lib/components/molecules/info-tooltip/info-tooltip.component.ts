import { Component, HostListener, Input, OnInit } from '@angular/core';

// TODO create a tooltip component/directive separated and use here [debt 7750]
@Component({
  selector: 'thx-info-tooltip',
  templateUrl: './info-tooltip.component.html',
  styleUrls: ['./info-tooltip.component.css']
})
export class InfoTooltipComponent implements OnInit {
  @Input() text: string;
  @Input() direction: 'left' | 'right' = 'right';

  public tooltipShow = false;
  ngOnInit() {}

  @HostListener('mouseenter') onMouseEnter() {
    this.tooltipShow = true;
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.tooltipShow = false;
  }

  public getClassDirection() {
    // TODO: IMPLEMENT DIRECTION AFTER
      return 'thx-tooltip left';
  }
}
