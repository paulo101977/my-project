import { InfoTooltipModule } from './info-tooltip.module';

describe('InfoTooltipModule', () => {
  let infoTooltipModule: InfoTooltipModule;

  beforeEach(() => {
    infoTooltipModule = new InfoTooltipModule();
  });

  it('should create an instance', () => {
    expect(infoTooltipModule).toBeTruthy();
  });
});
