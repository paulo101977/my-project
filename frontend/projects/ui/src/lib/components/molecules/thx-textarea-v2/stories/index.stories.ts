import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';
import { DefaultComponent } from './default';
import { ThxTextareaV2Component } from '../thx-textarea-v2.component';
import { TranslateModule } from '@ngx-translate/core';
import { ThxInputV2Module } from '../../thx-input-v2/thx-input-v2.module';
import { InputBaseEfectV2Module } from '../../../atoms/input-base-efect-v2/input-base-efect-v2.module';


storiesOf('Ui | Molecules/V2/ThxTextarea', module)
  .addDecorator(
    moduleMetadata({
      imports: [ CommonModule,
        FormsModule,
        ReactiveFormsModule, InputBaseEfectV2Module, TranslateModule.forRoot(), ThxInputV2Module   ],
      declarations: [ DefaultComponent, ThxTextareaV2Component  ],
    })
  )
  .add('Default', () => ({
      component: DefaultComponent
    }),
    { notes: { markdown: new Notes().getNotes() }});
