import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxTextareaV2Component } from './thx-textarea-v2.component';
import { InputBaseEfectV2Module } from '../../atoms/input-base-efect-v2/input-base-efect-v2.module';
import { ThxInputV2Module } from '../thx-input-v2/thx-input-v2.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ThxTextareaV2Component],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InputBaseEfectV2Module,
    ThxInputV2Module
  ],
  exports: [ThxTextareaV2Component]
})
export class ThxTextareaV2Module { }
