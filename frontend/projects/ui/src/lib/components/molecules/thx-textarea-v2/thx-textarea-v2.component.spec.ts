import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ThxTextareaV2Component } from './thx-textarea-v2.component';

describe('ThxTextareaV2Component', () => {
  let component: ThxTextareaV2Component;
  let fixture: ComponentFixture<ThxTextareaV2Component>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxTextareaV2Component ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxTextareaV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
