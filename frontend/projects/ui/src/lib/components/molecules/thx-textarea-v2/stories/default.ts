import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'thx-default-textarea-v2',
  template: `
    <div style="padding: 100px">
      <div class="row">
        <div class="col-8" >
          <thx-textarea-v2 [title]="title" limit="100"></thx-textarea-v2>
        </div>
      </div>
      <br>
      <br>
      <div class="row">
        <div class="col-12" style="height: 100px" [formGroup]="form" >
          <thx-textarea-v2 [title]="title" [limit]="10" formControlName="obs" ></thx-textarea-v2>
        </div>
      </div>
    </div>`,
  styles: []
})
export class DefaultComponent implements OnInit {

  public title = 'Observações sobre a UH';
  form: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      obs: ['Um Bom Nome', [Validators.required, Validators.maxLength(10)]],
    });

  }
}
