import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ThxInput } from '../thx-input-v2/thx-input-base/thx-input';
import { InputCounterModel } from '../thx-input-v2/thx-input-base/Input-counter-model';

@Component({
  selector: 'thx-textarea-v2',
  templateUrl: './thx-textarea-v2.component.html',
  styleUrls: ['./thx-textarea-v2.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ThxTextareaV2Component),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ThxTextareaV2Component),
      multi: true
    }
  ]
})
export class ThxTextareaV2Component extends ThxInput {
  private _limit = -1;
  @Input('limit')
  set limit(limit) {
    this._limit = limit;
    this.counter();
  }
  get limit() {
    return this._limit;
  }

  public limitCounter: InputCounterModel;

  counter() {
    if (this.limit) {
      this.limitCounter = {
        length: this.value ? this.value.length : 0,
        limit: this.limit
      };
    }
  }
}
