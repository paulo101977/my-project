import { ComponentFixture, TestBed } from '@angular/core/testing';

import {
  ThxInputLabelEditCurrencyFormatComponent
} from './thx-input-label-edit-currency-format.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { imgCdnStub } from '../../../../../../../../testing';

describe('ThxInputLabelEditCurrencyFormatComponent', () => {
  let component: ThxInputLabelEditCurrencyFormatComponent;
  let fixture: ComponentFixture<ThxInputLabelEditCurrencyFormatComponent>;

  beforeAll(() => {
    TestBed.configureTestingModule({
      declarations: [
        ThxInputLabelEditCurrencyFormatComponent,
        imgCdnStub
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(ThxInputLabelEditCurrencyFormatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on coldShow', () => {
    it('should test when not editModeOn and inputText', () => {
      component.editModeOn = false;
      component.inputText = '123';

      expect(component.coldShow()).toEqual(true);
    });

    it('should test when editModeOn and inputText', () => {
      component.editModeOn = true;
      component.inputText = '123';

      expect(component.coldShow()).toEqual(false);
    });

    it('should test when not editModeOn and inputText is null', () => {
      component.editModeOn = false;
      component.inputText = null;

      expect(component.coldShow()).toEqual(false);
    });
  });
});
