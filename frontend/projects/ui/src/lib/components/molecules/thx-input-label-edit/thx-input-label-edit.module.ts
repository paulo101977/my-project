import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxInputLabelEditComponent } from './edit/thx-input-label-edit.component';
import { FormsModule } from '@angular/forms';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import {
  ThxInputLabelEditCurrencyFormatComponent
} from './currency-format/thx-input-label-edit-currency-format.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ImgCdnModule,
    CurrencyMaskModule,
  ],
  declarations: [
    ThxInputLabelEditComponent,
    ThxInputLabelEditCurrencyFormatComponent
  ],
  exports: [
    ThxInputLabelEditComponent,
    ThxInputLabelEditCurrencyFormatComponent
  ]
})
export class ThxInputLabelEditModule { }
