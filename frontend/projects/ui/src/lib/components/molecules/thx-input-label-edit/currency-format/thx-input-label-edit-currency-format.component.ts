import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ThxInputEditBase } from '../base';
import { CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';

@Component({
  selector: 'thx-input-label-edit-currency-format',
  templateUrl: './thx-input-label-edit-currency-format.component.html',
  styleUrls: ['../base/thx-input-label-edit-base.component.css']
})
export class ThxInputLabelEditCurrencyFormatComponent extends  ThxInputEditBase implements OnChanges {
  public symbolStr: string;
  public maskConfig: CurrencyMaskConfig;
  public inputTextValue: string;

  // TODO: use from bifrost when DEBIT 7317 is resolved
  @Input() currencyFormatPipe;
  @Input() set symbol(_symbol) { this.symbolStr = _symbol; }

  constructor() {
    super();

    this.maskConfig = {
      align: 'center',
      allowNegative: false,
      allowZero: true,
      decimal: ',',
      precision: 2,
      prefix: '',
      thousands: '',
      suffix: '',
    };
  }

  // TODO: use from bifrost when DEBIT 7317 is resolved
  ngOnChanges(changes: SimpleChanges): void {
    const { currencyFormatPipe, inputText } = this;

    if ( currencyFormatPipe && inputText ) {
      this.inputTextValue = this
        .currencyFormatPipe
        .transform(this.inputText, '', 2, '.', ',', 3, false);
    }
  }

  public coldShow() {
    const { editModeOn, inputText } = this;
    return !editModeOn && inputText != null;
  }
}
