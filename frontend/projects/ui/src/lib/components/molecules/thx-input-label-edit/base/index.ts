import { EventEmitter, Input, OnInit, Output } from '@angular/core';

export class ThxInputEditBase implements OnInit {

  @Input('initialValue')
  set initialValue( txt: string) {
    this.inputText = txt;
    this.originalInputText = txt;
  }
  get initialValue(): string {
    return this.initialValue;
  }
  @Output() confirmValue = new EventEmitter<string>();

  private originalInputText: string;
  public  inputText: string;

  public editModeOn = false;

  constructor() { }

  ngOnInit() {
  }

  public confirmText() {
    this.originalInputText = this.inputText;
    this.confirmValue.emit(this.inputText);
    this.toggleEditMode();
  }

  public toggleEditMode() {
    this.editModeOn = !this.editModeOn;
    if (!this.editModeOn) {
      this.inputText = this.originalInputText;
    }
  }
}
