import { Component } from '@angular/core';
import { ThxInputEditBase } from '../base';
import { CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';

@Component({
  selector: 'thx-input-label-edit',
  templateUrl: './thx-input-label-edit.component.html',
  styleUrls: ['../base/thx-input-label-edit-base.component.css']
})
export class ThxInputLabelEditComponent extends  ThxInputEditBase {}
