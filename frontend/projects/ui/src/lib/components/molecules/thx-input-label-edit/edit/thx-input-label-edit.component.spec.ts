import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { configureTestSuite } from 'ng-bullet';
import { imgCdnStub } from '../../../../../../../../testing';

import { ThxInputLabelEditComponent } from './thx-input-label-edit.component';

describe('ThxInputLabelEditComponent', () => {
  let component: ThxInputLabelEditComponent;
  let fixture: ComponentFixture<ThxInputLabelEditComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxInputLabelEditComponent, imgCdnStub ],
      imports: [FormsModule]
    });

    fixture = TestBed.createComponent(ThxInputLabelEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
