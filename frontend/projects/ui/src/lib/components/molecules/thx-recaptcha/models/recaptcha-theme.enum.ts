export enum RecaptchaTheme {
  Light = 'light',
  Dark = 'dark',
}
