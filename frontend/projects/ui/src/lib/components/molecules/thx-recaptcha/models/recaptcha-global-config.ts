import { InjectionToken } from '@angular/core';
import { RecaptchaSize } from './recaptcha-size.enum';
import { RecaptchaTheme } from './recaptcha-theme.enum';

export class RecaptchaGlobalConfig {
  sitekey: string;
  theme?: RecaptchaTheme;
  size?: RecaptchaSize;
}

export const RECAPTCHA_GLOBAL_CONFIG = new InjectionToken<RecaptchaGlobalConfig>('recaptcha.global.config');

export function configureRecaptcha(config: RecaptchaGlobalConfig) {
  return {
    provide: RECAPTCHA_GLOBAL_CONFIG,
    useValue: config
  };
}
