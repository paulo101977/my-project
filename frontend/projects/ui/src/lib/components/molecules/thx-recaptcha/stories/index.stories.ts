import { moduleMetadata, storiesOf } from '@storybook/angular';
import { configureRecaptcha } from '../models/recaptcha-global-config';
import { ThxRecaptchaModule } from '../thx-recaptcha.module';
import { DefaultComponent } from './default';
import { Notes } from './notes';


storiesOf('Ui | Molecules/ThxRecaptcha', module)
.addDecorator(
  moduleMetadata({
    imports: [ ThxRecaptchaModule ],
    providers: [
      configureRecaptcha({sitekey: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'})
    ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
