import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ThxRecaptchaComponent } from './thx-recaptcha.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ThxRecaptchaComponent],
  exports: [ThxRecaptchaComponent]
})
export class ThxRecaptchaModule {}
