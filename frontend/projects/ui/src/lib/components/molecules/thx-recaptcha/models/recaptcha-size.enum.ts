export enum RecaptchaSize {
  Normal = 'normal',
  Compact = 'compact',
}
