import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { configureRecaptcha } from './models/recaptcha-global-config';

import { ThxRecaptchaComponent } from './thx-recaptcha.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ThxRecaptchaComponent', () => {
  let component: ThxRecaptchaComponent;
  let fixture: ComponentFixture<ThxRecaptchaComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxRecaptchaComponent ],
      providers: [configureRecaptcha({sitekey: 'fake'})],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxRecaptchaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
