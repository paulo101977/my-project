import { Component, ElementRef, EventEmitter, Inject, Input, NgZone, OnInit, Output, ViewChild } from '@angular/core';
import { RecaptchaRenderConfig } from './models/recaptcha-render-config';
import { RecaptchaSize } from './models/recaptcha-size.enum';
import { RecaptchaTheme } from './models/recaptcha-theme.enum';
import { RECAPTCHA_GLOBAL_CONFIG, RecaptchaGlobalConfig } from './models/recaptcha-global-config';

@Component({
  selector: 'thx-recaptcha',
  templateUrl: './thx-recaptcha.component.html',
})
export class ThxRecaptchaComponent implements OnInit {
  @Output() loaded: EventEmitter<any> = new EventEmitter<any>();
  @Output() succeeded: EventEmitter<any> = new EventEmitter<any>();
  @Output() expired: EventEmitter<any> = new EventEmitter<any>();
  @Output() failed: EventEmitter<any> = new EventEmitter<any>();

  @Input() theme: RecaptchaTheme;
  @Input() size: RecaptchaSize;
  @Input() tabIndex = 0;

  @ViewChild('recaptcha') recaptchaElm: ElementRef;

  private grecaptcha: any;
  private captchaWidget: any;

  constructor(
    @Inject(RECAPTCHA_GLOBAL_CONFIG) private recaptchaGlobalConfig: RecaptchaGlobalConfig,
    private zone: NgZone
  ) {}

  ngOnInit() {
    this.loadScript();
  }

  private loadScript() {
    const firstScriptElm = document.getElementsByTagName('script')[0];
    const recaptchaScriptElm = this.createRecaptchaScriptElement();

    // @ts-ignore
    window.onLoadCaptcha = () => this.onLoadScript();
    firstScriptElm.parentNode.insertBefore(recaptchaScriptElm, firstScriptElm);
  }

  private createRecaptchaScriptElement() {
    const scriptElm = document.createElement('script');
    scriptElm.src = 'https://www.google.com/recaptcha/api.js?onload=onLoadCaptcha&render=explicit';
    scriptElm.async = true;
    scriptElm.defer = true;

    return scriptElm;
  }

  private onLoadScript() {
    // @ts-ignore
    this.grecaptcha = window.grecaptcha || {};
    this.renderCaptcha();
    this.zone.run(() => this.loaded.emit());
  }

  private getRenderAttributes(): RecaptchaRenderConfig {
    const renderAttributes: any = {
      sitekey: this.recaptchaGlobalConfig.sitekey,
      theme: this.theme || this.recaptchaGlobalConfig.theme,
      size: this.size || this.recaptchaGlobalConfig.size,
      tabindex: this.tabIndex
    };

    if (this.outputHasObservers(this.succeeded)) {
      renderAttributes.callback = (...args) => this.onSucceeded(...args);
    }

    if (this.outputHasObservers(this.expired)) {
      renderAttributes['expired-callback'] = (...args) => this.onExpired(...args);
    }

    if (this.outputHasObservers(this.failed)) {
      renderAttributes['error-calback'] = (...args) => this.onFailed(...args);
    }

    return renderAttributes;
  }

  private onSucceeded(...args) {
    this.zone.run(() => this.succeeded.emit(args));
  }

  private onExpired(...args) {
    this.zone.run(() => this.expired.emit(args));
  }

  private onFailed(...args) {
    this.zone.run(() => this.failed.emit(args));
  }

  private outputHasObservers(output: EventEmitter<any>) {
    return output.observers.length > 0;
  }

  private renderCaptcha() {
    const recaptchaElement = this.recaptchaElm.nativeElement;
    const attributes: RecaptchaRenderConfig = this.getRenderAttributes();
    this.captchaWidget = this.grecaptcha.render(recaptchaElement, attributes);
  }

  private resetCaptcha() {
    this.grecaptcha.reset(this.captchaWidget);
  }

}
