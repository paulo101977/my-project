import { RecaptchaSize } from './recaptcha-size.enum';
import { RecaptchaTheme } from './recaptcha-theme.enum';

export class RecaptchaRenderConfig {
  sitekey: string;
  theme?: RecaptchaTheme = RecaptchaTheme.Light;
  size?: RecaptchaSize = RecaptchaSize.Normal;
  tabindex = 0;
  callback?: Function;
  'expired-callback'?: Function;
  'error-callback'?: Function;
}
