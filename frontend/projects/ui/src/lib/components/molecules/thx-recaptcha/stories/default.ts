import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-recaptcha',
  template: `
    <thx-recaptcha
      (succeeded)="onSuccess($event)"
      (loaded)="onLoad($event)"
      (expired)="onExpire($event)"
      (failed)="onFail($event)"
    ></thx-recaptcha>
  `
})
export class DefaultComponent {

  onSuccess(args) {
    console.log('success', args);
  }

  onFail(args) {
    console.log('fail', args);
  }

  onExpire(args) {
    console.log('expire', args);
  }

  onLoad(args) {
    console.log('load', args);
  }
}
