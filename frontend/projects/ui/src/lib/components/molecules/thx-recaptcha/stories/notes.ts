export class Notes {
    private notes = `
## Html:

~~~json
<!-- recaptcha component usage: -->
<thx-recaptcha
    (loaded)="onLoad()"
    (expired)="onExpire($event)"
    (succeeded)="onSuccess($event)"
    (failed)="onFail($event)"
    [theme]="'light'"
    [size]="'compact'"
    [tabIndex]="0"
></thx-recaptcha>
~~~

## Typescript:

~~~typescript
import { Component } from '@angular/core';
import { RecaptchaTheme, RecaptchaSize } from '@inovacao-cmnet/thx-ui';

@NgModule({
  imports: [ ThxRecaptchaModule ],
  providers: [
    configureRecaptcha({sitekey: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'})
  ]
})
export class SampleAppCoreModule {}

@Component({
  selector: 'thx-sample-recaptcha',
  template: \`
    <thx-recaptcha
      (succeeded)="onSuccess($event)"
      (loaded)="onLoad($event)"
      (expired)="onExpire($event)"
      (failed)="onFail($event)"
      [theme]="theme"
      [size]="size"
      [tabIndex]="tabIndex"
    ></thx-recaptcha>
  \`
})
export class SampleRecaptchaComponent {

  tabIndex = 7;
  theme = RecaptchaTheme.Light;
  size = RecaptchaSize.Compact;

  onSuccess(args) {
    console.log('success', args);
  }

  onFail(args) {
    console.log('fail', args);
  }

  onExpire(args) {
    console.log('expire', args);
  }

  onLoad(args) {
    console.log('load', args);
  }
}

~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
