import { ThxRecaptchaModule } from './thx-recaptcha.module';

describe('ThxRecaptchaModule', () => {
  let thxRecaptchaModule: ThxRecaptchaModule;

  beforeEach(() => {
    thxRecaptchaModule = new ThxRecaptchaModule();
  });

  it('should create an instance', () => {
    expect(thxRecaptchaModule).toBeTruthy();
  });
});
