import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SubmenuOptionsModel } from './models/submenu-options';

@Component({
  selector: 'thx-submenu-options',
  templateUrl: './submenu-options.component.html',
  styleUrls: ['./submenu-options.component.css'],
})
export class SubmenuOptionsComponent {
  @Output() itemWasAction = new EventEmitter();
  @Input() items: any[] = [];
  @Input() fullWidth: boolean;

  constructor() {}

  public onItemWasAction(event, model: SubmenuOptionsModel) {
    event.stopPropagation();

    this.itemWasAction.emit(model);
  }
}
