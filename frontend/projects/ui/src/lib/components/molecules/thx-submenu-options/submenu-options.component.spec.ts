import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';
import { SubmenuOptionsModel } from './models/submenu-options';
import { SubmenuOptionsComponent } from './submenu-options.component';

describe('SubmenuOptionsComponent', () => {
  let component: SubmenuOptionsComponent;
  let fixture: ComponentFixture<SubmenuOptionsComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule],
      declarations: [SubmenuOptionsComponent],
    });

    fixture = TestBed.createComponent(SubmenuOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call itemWasUpdated', () => {
    const event = {
      type: 'click',
      stopPropagation: function() {},
    };

    const itemsOption = new SubmenuOptionsModel();
    itemsOption.title = 'mock string';
    itemsOption.callBackFunction = function() {};

    spyOn(component.itemWasAction, 'emit');
    spyOn(event, 'stopPropagation');

    component.onItemWasAction(event, itemsOption);

    expect(event.stopPropagation).toHaveBeenCalled();
    expect(component.itemWasAction.emit).toHaveBeenCalled();
  });
});
