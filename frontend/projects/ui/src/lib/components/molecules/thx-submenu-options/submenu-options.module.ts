import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SubmenuOptionsComponent } from './submenu-options.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [SubmenuOptionsComponent],
  exports: [SubmenuOptionsComponent]
})
export class SubmenuOptionsModule {}
