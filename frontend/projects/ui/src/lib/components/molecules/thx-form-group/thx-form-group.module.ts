import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { FormGroupComponent } from './thx-form-group.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [ FormGroupComponent ],
  exports: [ FormGroupComponent ]
})
export class ThxFormGroupModule { }
