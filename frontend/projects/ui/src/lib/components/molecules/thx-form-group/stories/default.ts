import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-button',
  template: `
    <thx-form-group>
      <!-- input example -->
      <thx-input
        [type]="'email'">
      </thx-input>
    </thx-form-group>
  `,
  styles: []
})
export class DefaultComponent {

}
