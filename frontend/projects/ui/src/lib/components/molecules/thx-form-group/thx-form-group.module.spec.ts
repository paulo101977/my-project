import { ThxFormGroupModule } from './thx-form-group.module';

describe('ThxFormGroupModule', () => {
  let thxFormGroupModule: ThxFormGroupModule;

  beforeEach(() => {
    thxFormGroupModule = new ThxFormGroupModule();
  });

  it('should create an instance', () => {
    expect(thxFormGroupModule).toBeTruthy();
  });
});
