import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-button',
  template: `
    <form #form="ngForm">
      <thx-form-group>
        <!-- icon content example -->
        <thx-label [labelFor]="'myinput'">
          <thx-icon
            [type]="'thx-input-icon mdi-account thx-input-icon-right'">
          </thx-icon>
        </thx-label>

        <!-- input email validate example -->
        <thx-input
          name="mymodel"
          [inputId]="'myinput'"
          [(ngModel)]="mymodel"
          [ngClass]="'thx-input-icon-text--right'"
          [type]="'email'"
          required
          email>
        </thx-input>

      </thx-form-group>

      model: {{ mymodel }}
      validate: {{ form.valid }}
    </form>
  `,
  styles: []
})
export class EmailValidationComponent {
  mymodel: any;
}
