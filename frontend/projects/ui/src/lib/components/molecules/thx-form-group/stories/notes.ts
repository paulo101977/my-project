export class Notes {
    private notes = `
## Html:

~~~json
    <!-- default with one input -->
    <thx-form-group>
      <!-- input example -->
      <thx-input
        [type]="'email'">
      </thx-input>
    </thx-form-group>


    <!-- form validation example -->

    <form #form="ngForm">
      <thx-form-group>

        <!-- icon content example -->
        <thx-label [labelFor]="'myinput'">
          <thx-icon
            [type]="'thx-input-icon mdi-account thx-input-icon-right'">
          </thx-icon>
        </thx-label>

        <!-- input email validate example -->
        <thx-input
          name="mymodel"
          [inputId]="'myinput'"
          [(ngModel)]="mymodel"
          [ngClass]="'thx-input-icon-text--right'"
          [type]="'email'"
          required
          email>
        </thx-input>

      </thx-form-group>

      model: {{ mymodel }}
      validate: {{ form.valid }}
    </form>
~~~

## Typescript:

~~~typescript

~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
