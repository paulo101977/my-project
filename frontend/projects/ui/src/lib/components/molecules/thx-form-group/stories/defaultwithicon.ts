import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-button',
  template: `
    <thx-form-group>
      <thx-label>
        <thx-icon
          [type]="'thx-input-icon mdi-account thx-input-icon-right'">
        </thx-icon>
      </thx-label>

      <!-- input example -->
      <thx-input
        [ngClass]="'thx-input-icon-text--right'"
        [type]="'email'">
      </thx-input>
    </thx-form-group>
  `,
  styles: []
})
export class DefaultWithIconComponent {

}
