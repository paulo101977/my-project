import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { ThxIconComponent } from '../../../atoms/thx-icon/thx-icon.component';
import { ThxInputComponent } from '../../../atoms/thx-input/thx-input.component';
import { ThxLabelComponent } from '../../../atoms/thx-label/thx-label.component';
import { ThxFormGroupModule } from '../thx-form-group.module';

import { DefaultComponent } from './default';
import { DefaultWithIconComponent } from './defaultwithicon';
import { WithIconLockComponent } from './withiconlock';
import { EmailValidationComponent } from './emailvalidation';


storiesOf('Ui | Molecules/ThxFormGroup', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule, ThxFormGroupModule ],
    declarations: [ ThxIconComponent, ThxInputComponent, ThxLabelComponent  ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }})
.add('With Icon', () => ({
  component: DefaultWithIconComponent
}),
    { notes: { markdown: new Notes().getNotes() }})
.add('With Icon Lock', () => ({
  component: WithIconLockComponent
}),
    { notes: { markdown: new Notes().getNotes() }})
.add('Email validation', () => ({
  component: EmailValidationComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
