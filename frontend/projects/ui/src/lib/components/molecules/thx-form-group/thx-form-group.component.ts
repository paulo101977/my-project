import { Component } from '@angular/core';

@Component({
  selector: 'thx-form-group',
  template: `
    <div class="form-group">
        <ng-content select="thx-label"></ng-content>

        <ng-content select="thx-input"></ng-content>

    </div>
  `,
  styleUrls: ['./thx-form-group.component.scss']
})
export class FormGroupComponent { }
