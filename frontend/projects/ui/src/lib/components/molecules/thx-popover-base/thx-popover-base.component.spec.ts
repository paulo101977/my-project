import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxPopoverBaseComponent } from './thx-popover-base.component';
import { configureTestSuite } from 'ng-bullet';

describe('ThxPopoverBaseComponent', () => {
  let component: ThxPopoverBaseComponent;
  let fixture: ComponentFixture<ThxPopoverBaseComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxPopoverBaseComponent ]
    });
    fixture = TestBed.createComponent(ThxPopoverBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
