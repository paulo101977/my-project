import {Component, Input} from '@angular/core';
import { PopoverPositionEnum } from './popover-position-enum';
import {trigger, transition, style, animate} from '@angular/animations';

@Component({
  selector: 'thx-popover-base',
  templateUrl: './thx-popover-base.component.html',
  styleUrls: ['./thx-popover-base.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({opacity: 0}),
        animate(100, style({opacity: 1}))
      ]),
      transition(':leave', [
        animate(100, style({opacity: 0}))
      ])
    ])
  ]
})
export class ThxPopoverBaseComponent {

  private _position: PopoverPositionEnum;
  private _isActive: boolean;

  get position(): PopoverPositionEnum {
    return this._position;
  }

  @Input()
  set position(value: PopoverPositionEnum) {
    this._position = value;
  }

  get isActive(): boolean {
    if (typeof this._isActive === 'boolean') {
      return this._isActive;
    }
    return false;
  }

  @Input()
  set isActive(value: boolean) {
    this._isActive = value;
  }

}
