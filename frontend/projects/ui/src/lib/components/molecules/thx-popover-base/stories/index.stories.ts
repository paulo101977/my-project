import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';

import { componentFactory } from '.storybook/component-factory';
import { Notes } from './notes';
import { ThxPopoverBaseComponent } from '../thx-popover-base.component';

const BottomLeftComponent = componentFactory('bottom-left', `
  <div style="width:200px; height:30px; border: 1px solid; position: absolute; margin: auto; left: 0; right: 0; top: 0; bottom: 0;">
    <thx-popover-base position="bottom-left" [isActive]="true">
        <div style="padding: 10px;">POPOVER CONTENT!</div>
    </thx-popover-base>
  </div>
`);
const BottomRightComponent = componentFactory('bottom-right', `
  <div style="width:200px; height:30px; border: 1px solid; position: absolute; margin: auto; left: 0; right: 0; top: 0; bottom: 0;">
    <thx-popover-base position="bottom-right" [isActive]="true">
        <div style="padding: 10px;">POPOVER CONTENT!</div>
    </thx-popover-base>
  </div>
`);
const TopLeftComponent = componentFactory('top-left', `
  <div style="width:200px; height:30px; border: 1px solid; position: absolute; margin: auto; left: 0; right: 0; top: 0; bottom: 0;">
    <thx-popover-base position="top-left" [isActive]="true">
        <div style="padding: 10px;">POPOVER CONTENT!</div>
    </thx-popover-base>
  </div>
`);
const TopRightComponent = componentFactory('top-right', `
  <div style="width:200px; height:30px; border: 1px solid; position: absolute; margin: auto; left: 0; right: 0; top: 0; bottom: 0;">
    <thx-popover-base position="top-right" [isActive]="true">
        <div style="padding: 10px;">POPOVER CONTENT!</div>
    </thx-popover-base>
  </div>
`);

storiesOf('Ui | Molecules/ThxPopoverBase', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule
      ],
      declarations: [ ThxPopoverBaseComponent ]
    })
  )
  .add('BottomLeft', () => ({
      component: BottomLeftComponent
    }),
    { notes: { markdown: new Notes().getNotes('bottomLeft') }})
  .add('BottomRight', () => ({
      component: BottomRightComponent
    }),
    { notes: { markdown: new Notes().getNotes('bottomRight') }})
  .add('TopLeft', () => ({
      component: TopLeftComponent
    }),
    { notes: { markdown: new Notes().getNotes('topLeft') }})
  .add('TopRight', () => ({
      component: TopRightComponent
    }),
    { notes: { markdown: new Notes().getNotes('topRight') }});
