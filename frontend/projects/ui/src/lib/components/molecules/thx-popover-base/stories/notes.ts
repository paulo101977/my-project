export class Notes {
  private bottomRight = `
## Html:

~~~json
    <!-- Popover posicionado em bottom-right -->
    <thx-popover-base position="bottom-right" [isActive]="true">
        <div>POPOVER CONTENT!</div>
    </thx-popover-base>
    <!-- Input text com form -->
~~~
    `;
  private bottomLeft = `
## Html:

~~~json
    <!-- Popover posicionado em bottom-left -->
    <thx-popover-base position="bottom-left" [isActive]="true">
        <div>POPOVER CONTENT!</div>
    </thx-popover-base>
    <!-- Input text com form -->
~~~
  `;

  private topRight = `
  ## Html:

~~~json
    <!-- Popover posicionado em top-right -->
    <thx-popover-base position="top-right" [isActive]="true">
        <div>POPOVER CONTENT!</div>
    </thx-popover-base>
    <!-- Input text com form -->
~~~
  `;

  private topLeft = `
  ## Html:

~~~json
    <!-- Popover posicionado em top-left -->
    <thx-popover-base position="top-left" [isActive]="true">
        <div>POPOVER CONTENT!</div>
    </thx-popover-base>
    <!-- Input text com form -->
~~~
  `;

  public getNotes(kind: string): string {
    switch (kind) {
      case 'bottomRight':
        return this.bottomRight;
      case 'bottomLeft':
        return this.bottomLeft;
      case 'topRight':
        return this.topRight;
      case 'topLeft':
        return this.topLeft;
      default:
        return this.bottomRight;
    }
  }
}
