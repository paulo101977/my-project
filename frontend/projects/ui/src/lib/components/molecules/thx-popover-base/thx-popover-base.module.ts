import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ThxPopoverBaseComponent} from './thx-popover-base.component';

@NgModule({
  declarations: [ThxPopoverBaseComponent],
  imports: [CommonModule],
  exports: [ThxPopoverBaseComponent]
})
export class ThxPopoverBaseModule { }
