import { Component , Input, ViewEncapsulation } from '@angular/core';
import { AssetsCdnService } from '../../../services/assets-service/assets-cdn.service';

@Component({
  selector: 'thx-error-form',
  template: `
    <thx-description
      class="thx-form-error"
      *ngIf="show">

      <thx-img [src]="imgSrc"></thx-img>
      <thx-span>{{ errorMsg }}</thx-span>

    </thx-description>
  ` ,
  styleUrls: ['./thx-error-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ThxErrorFormComponent {

  // default img
  @Input() imgSrc = this.assetsCdnService.getImgUrlTo('input-error.svg');
  @Input() errorMsg = '';
  @Input() show = false;

  constructor( private assetsCdnService: AssetsCdnService) {}

}
