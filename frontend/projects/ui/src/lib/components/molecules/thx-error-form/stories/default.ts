import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-error',
  template: `
    <thx-error-form
      [show]="true"
      [errorMsg]="'Error message'">
    </thx-error-form>
  `
})
export class DefaultComponent {

}
