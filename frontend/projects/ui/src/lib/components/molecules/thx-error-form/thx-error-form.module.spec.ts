import { ThxErrorFormModule } from './thx-error-form.module';

describe('ThxErrorFormModule', () => {
  let thxErrorFormModule: ThxErrorFormModule;

  beforeEach(() => {
    thxErrorFormModule = new ThxErrorFormModule();
  });

  it('should create an instance', () => {
    expect(thxErrorFormModule).toBeTruthy();
  });
});
