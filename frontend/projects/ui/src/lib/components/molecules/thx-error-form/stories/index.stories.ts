import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { ThxErrorFormModule } from '../thx-error-form.module';


import { DefaultComponent } from './default';


storiesOf('Ui | Molecules/ThxError', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule, ThxErrorFormModule ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
