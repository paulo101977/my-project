import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxErrorFormComponent } from './thx-error-form.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';

describe('ThxErrorFormComponent', () => {
  let component: ThxErrorFormComponent;
  let fixture: ComponentFixture<ThxErrorFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxErrorFormComponent ],
      imports: [ ThxImgModule ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxErrorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
