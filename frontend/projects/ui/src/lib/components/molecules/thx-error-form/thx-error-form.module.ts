import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxErrorFormComponent } from './thx-error-form.component';
import { ThxDescriptionModule } from '../../atoms/thx-description/thx-description.module';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';
import { ThxSpanModule } from '../../atoms/thx-span/thx-span.module';

@NgModule({
  imports: [
    CommonModule,
    ThxDescriptionModule,
    ThxImgModule,
    ThxSpanModule
  ],
  declarations: [
    ThxErrorFormComponent
  ],
  exports: [
    ThxErrorFormComponent,
  ]
})
export class ThxErrorFormModule { }
