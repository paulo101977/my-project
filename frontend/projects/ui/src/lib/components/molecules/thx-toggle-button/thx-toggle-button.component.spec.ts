import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxToggleButtonComponent } from './thx-toggle-button.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';

describe('ThxToggleButtonComponent', () => {
  let component: ThxToggleButtonComponent;
  let fixture: ComponentFixture<ThxToggleButtonComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxToggleButtonComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });
    fixture = TestBed.createComponent(ThxToggleButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
