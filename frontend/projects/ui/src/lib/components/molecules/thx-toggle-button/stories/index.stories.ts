import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { DefaultComponent } from './default';
import { ThxToggleButtonModule } from '../thx-toggle-button.module';


storiesOf('Ui | Molecules/ThxToggleButtonComponent', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule, ThxToggleButtonModule ],
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
