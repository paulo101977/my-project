import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxToggleButtonComponent } from './thx-toggle-button.component';
import { ThxButtonV2Module } from '../../atoms/thx-button-v2/thx-button-v2.module';

@NgModule({
  imports: [
    CommonModule,
    ThxButtonV2Module,
  ],
  declarations: [ ThxToggleButtonComponent ],
  exports: [ ThxToggleButtonComponent ]
})
export class ThxToggleButtonModule { }
