export class Notes {
    private notes = `
## Html:

~~~html
<thx-toggle-button
    (selectedButton)="buttonSelection($event)"
    [buttonGroupList]="buttons">
</thx-toggle-button>
~~~

## Typescript:

~~~js
@Component({
  selector: 'thx-implement',
  templateUrl: './thx-implement.component.html',
  styleUrls: ['./thx-implement.component.scss'],
})
export class ThxImplementComponent implements OnInit {

  public buttons: Array<ThxToggleButtonConfig>;
  public selected: number;

  ngOnInit() {
    this.buttons = [
        {
            text: 'My Text',
        },
        {
            text: 'My Text 1',
        },
        {
            text: 'My Text 1',
            disabled: true
        }
    ];
  }

  private buttonSelection(_selection: number) {
    this.selected = _selection;
  }
}
~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
