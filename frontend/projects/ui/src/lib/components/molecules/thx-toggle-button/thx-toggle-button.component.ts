import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ThxToggleButtonConfig } from './models/thx-toggle-button-config';


@Component({
  selector: 'thx-toggle-button',
  templateUrl: './thx-toggle-button.component.html',
  styleUrls: ['./thx-toggle-button.component.scss']
})
export class ThxToggleButtonComponent implements OnInit, OnChanges {
  private _disabled = false;

  @Input() initialIndex: number;
  @Input() buttonGroupList: Array<ThxToggleButtonConfig>;
  @Input() set disabled(_disabled: boolean) {
    this._disabled = _disabled;
    this.setButtonProperty('disabled', _disabled);
  }
  get disabled() {
    return this._disabled;
  }

  @Output() selectedButton = new EventEmitter<number>();

  ngOnInit(): void {
    this.setButtonConfig();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.setButtonConfig();
  }

  private setButtonConfig() {
    const { buttonGroupList, initialIndex } = this;
    if ( buttonGroupList && buttonGroupList.length ) {
      this.setButtonProperty('primary', true);
      this.setButtonProperty('outline', true);

      this.buttonGroupList[0].outline = false;

      if ( !isNaN(initialIndex) && initialIndex >= 0 && initialIndex < buttonGroupList.length ) {
        this.buttonGroupList[0].outline = true;
        this.buttonGroupList[initialIndex].outline = false;
      }
    }
  }

  public buttonHasBeenClicked(index: number) {
      const { buttonGroupList } = this;
      if ( !this.disabled ) {
        this.setButtonProperty('outline', true);

        if ( buttonGroupList && buttonGroupList[index] ) {
          buttonGroupList[index].outline = false;
          this.selectedButton.emit(index);
        }
      }
  }

  private setButtonProperty(property: string, _disabled = false) {
    const { buttonGroupList } = this;
    if ( this.buttonGroupList ) {
      buttonGroupList.forEach( item => {
        item[property] = _disabled;
      });
    }
  }
}
