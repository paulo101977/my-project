export class ThxToggleButtonConfig {
    primary?: boolean;
    iconBefore?: string;
    iconAfter?: string;
    text: string;
    outline?: boolean;
    disabled?: boolean;
}
