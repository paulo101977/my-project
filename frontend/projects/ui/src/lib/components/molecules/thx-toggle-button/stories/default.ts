import { Component  } from '@angular/core';
import { ThxToggleButtonConfig } from '../models/thx-toggle-button-config';

@Component({
  selector: 'thx-default-toggle-test',
  template: `
    <div>With One Item disabled:</div>
    <thx-toggle-button
        (selectedButton)="selected = $event"
        [buttonGroupList]="arr">
    </thx-toggle-button>

    <br>

    <div>All Disabled:</div>
    <thx-toggle-button
      (selectedButton)="selected = $event"
      [disabled]="true"
      [buttonGroupList]="arr2">
    </thx-toggle-button>

    <br>
    <div>With buttons:</div>
    <thx-toggle-button
        (selectedButton)="selected = $event"
        [buttonGroupList]="arr3">
    </thx-toggle-button>

    <br>
    <div>With initial index:</div>
    <thx-toggle-button
        (selectedButton)="selected = $event"
        [initialIndex]="2"
        [buttonGroupList]="arr4">
    </thx-toggle-button>
    <br>
    Selected: {{ selected }}
  `,
  styles: []
})
export class DefaultComponent {
  public selected: number;

  public arr = <Array<ThxToggleButtonConfig>>[
      {
        text: 'My Text',
      },
      {
        text: 'My Text 1',
      },
      {
        text: 'My Text 2',
      },
      {
        text: 'My Text 3',
        disabled: true,
      }
    ];

  public arr2 = <Array<ThxToggleButtonConfig>>[
    {
      text: 'My Text',
    },
    {
      text: 'My Text 1',
    },
    {
      text: 'My Text 2',
    },
    {
      text: 'My Text 3',
    }
  ];


  public arr3 = <Array<ThxToggleButtonConfig>>[
    {
      iconBefore: 'adult-icon.svg',
      text: 'My Text',
    },
    {
      iconBefore: 'adult-icon.svg',
      text: 'My Text 1',
    },
    {
      iconBefore: 'adult-icon.svg',
      text: 'My Text 2',
    },
    {
      iconBefore: 'adult-icon.svg',
      text: 'My Text 3',
    }
  ];


  public arr4 = <Array<ThxToggleButtonConfig>>[
    {
      text: 'My Text',
    },
    {
      text: 'My Text 1',
    },
    {
      text: 'My Text 2',
    },
    {
      text: 'My Text 3',
    }
  ];

}
