import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'thx-expandable-box',
  templateUrl: './expandable-box.component.html',
  styleUrls: ['./expandable-box.component.scss']
})
export class ExpandableBoxComponent implements OnInit {

  @Input() title: string;
  @Input() expanded = false;

  constructor() { }

  ngOnInit() {
  }

}
