import { Component } from '@angular/core';

@Component({
  selector: 'thx-default-expandable-box',
  template: `
    <div style="padding: 100px; background-color: #f6f7f9">
      <thx-expandable-box [title]="'Adulto 1'">
        <div class="expandable-box-title">
          complemento do titulo
        </div>
        <div class="expandable-box-content">
          conteúdo
        </div>
      </thx-expandable-box>
    </div>
  `,
  styles: ['.center { align-self: center} ']
})
export class DefaultComponent {

}
