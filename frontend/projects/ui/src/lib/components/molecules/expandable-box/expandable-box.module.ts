import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpandableBoxComponent } from './expandable-box.component';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';

@NgModule({
  declarations: [ExpandableBoxComponent],
  imports: [
    CommonModule,
    ThxImgModule
  ],
  exports: [ExpandableBoxComponent]
})
export class ExpandableBoxModule { }
