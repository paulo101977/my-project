import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default';
import { ExpandableBoxModule } from '../expandable-box.module';
import { ThxImgModule } from '../../../atoms/thx-img/thx-img.module';


storiesOf('Ui | Molecules/ExpandableBox', module)
  .addDecorator(
    moduleMetadata({
      imports: [CommonModule, ExpandableBoxModule, ThxImgModule]
    })
  )
  .add('Default', () => ({
      component: DefaultComponent
    }),
      { notes: { markdown: new Notes().getNotes() }});
