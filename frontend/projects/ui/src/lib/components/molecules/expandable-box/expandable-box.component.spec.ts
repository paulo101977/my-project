import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandableBoxComponent } from './expandable-box.component';
import { configureTestSuite } from 'ng-bullet';

describe('ExpandableBoxComponent', () => {
  let component: ExpandableBoxComponent;
  let fixture: ComponentFixture<ExpandableBoxComponent>;


  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpandableBoxComponent ]
    });
    fixture = TestBed.createComponent(ExpandableBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
