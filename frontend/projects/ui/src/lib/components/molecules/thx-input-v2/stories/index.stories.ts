import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';
import { DefaultComponent } from './default';
import { ThxImgModule } from '../../../atoms/thx-img/thx-img.module';
import { ThxErrorV2Module } from '../../thx-error-v2/thx-error-v2.module';
import { InputBaseEfectV2Module } from '../../../atoms/input-base-efect-v2/input-base-efect-v2.module';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { ThxInputBaseComponent } from '../thx-input-base/thx-input-base.component';
import { ThxInputCurrencyComponent } from '../thx-input-currency/thx-input-currency.component';
import { ThxInputTextComponent } from '../thx-input-text/thx-input-text.component';
import { TranslateModule } from '@ngx-translate/core';
import { ThxInputLocaleComponent } from '../thx-input-locale/thx-input-locale.component';
import { AngularGooglePlaceModule } from 'ngx-google-places';
import {ThxInputDateComponent} from '../thx-input-date/thx-input-date.component';
import {MyDatePickerModule} from 'mydatepicker';


storiesOf('Ui | Molecules/V2/ThxInput', module)
.addDecorator(
  moduleMetadata({
    imports: [
      AngularGooglePlaceModule,
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      ThxImgModule,
      TranslateModule.forRoot(),
      ThxErrorV2Module,
      InputBaseEfectV2Module,
      CurrencyMaskModule,
      MyDatePickerModule
    ],
    declarations: [
      DefaultComponent,
      ThxInputTextComponent,
      ThxInputBaseComponent,
      ThxInputCurrencyComponent,
      ThxInputLocaleComponent,
      ThxInputDateComponent
    ],

  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
