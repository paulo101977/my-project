import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThxImgModule } from '../../atoms/thx-img/thx-img.module';
import { ThxErrorV2Module } from '../thx-error-v2/thx-error-v2.module';
import { InputBaseEfectV2Module } from '../../atoms/input-base-efect-v2/input-base-efect-v2.module';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { ThxInputCurrencyComponent } from './thx-input-currency/thx-input-currency.component';
import { ThxInputBaseComponent } from './thx-input-base/thx-input-base.component';
import { ThxInputTextComponent } from './thx-input-text/thx-input-text.component';
import { ThxInputLocaleComponent } from './thx-input-locale/thx-input-locale.component';
import { AngularGooglePlaceModule } from 'ngx-google-places';
import { ThxInputDateComponent } from './thx-input-date/thx-input-date.component';
import { MyDatePickerModule } from 'mydatepicker';



@NgModule({
  imports: [
    AngularGooglePlaceModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ThxImgModule,
    ThxErrorV2Module,
    InputBaseEfectV2Module,
    CurrencyMaskModule,
    MyDatePickerModule,
  ],
  declarations: [ThxInputTextComponent,
    ThxInputBaseComponent,
    ThxInputCurrencyComponent,
    ThxInputLocaleComponent,
    ThxInputDateComponent],
  exports: [ThxInputTextComponent,
    ThxInputCurrencyComponent,
    ThxInputBaseComponent,
    ThxInputLocaleComponent,
    ThxInputDateComponent]
})
export class ThxInputV2Module { }
