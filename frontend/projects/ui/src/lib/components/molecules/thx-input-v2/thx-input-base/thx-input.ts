import { Attribute, Input } from '@angular/core';
import { ControlValueAccessor, FormControl, Validator } from '@angular/forms';

export class ThxInput implements ControlValueAccessor, Validator {
  @Input() title: string;
  @Input() infoText: string;
  @Input() disabled = false;
  @Input() leftIcon: string;
  @Input() rightIcon: string;

  public value: any;
  public control: FormControl;
  public inFocus: boolean;
  constructor( @Attribute('required') public required ) { }

  inputFocus() {
    this.inFocus = true;
  }

  inputBlur() {
    this.inFocus = false;
    this.onTouched();
  }

  isLabelTopMode(): any {
    return (this.inFocus || this.value ) && this.title;
  }

  valueChange(val: any) {
    this.writeValue(val);
  }

  // Control value accessor
  // Function to call when the rating changes.
  onChange = (value: string | any) => {};

  // Function to call when the input is touched (when a star is clicked).
  onTouched = () => {};

  writeValue(val: any): void {
    this.value = val;
    this.onChange(this.value);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  // Allows Angular to disable the input.
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public validate(c: FormControl) {
    if (!this.control) {
      this.control = c;
    }
    return null;
  }
}
