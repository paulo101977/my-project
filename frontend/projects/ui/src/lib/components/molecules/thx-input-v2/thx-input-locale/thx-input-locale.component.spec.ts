import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxInputLocaleComponent } from './thx-input-locale.component';
import { configureTestSuite } from 'ng-bullet';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ThxInputLocaleComponent', () => {
  let component: ThxInputLocaleComponent;
  let fixture: ComponentFixture<ThxInputLocaleComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxInputLocaleComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxInputLocaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
