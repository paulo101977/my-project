import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'thx-default-input-v2',
  template: `
    <div style="padding: 100px">
      <div class="row">
        <div class="col-12">
          <h1> Inputs de texto </h1>
        </div>

      <div class="col-4">
        <div>
          Input Normal
        </div>
        <br>
        <thx-input-text [(ngModel)]="value1" [title]="title">
        </thx-input-text>
      </div>
      <div class="col-4">
        <div>
          Input Valor com prefixo
        </div>
        <br>
        <thx-input-text [(ngModel)]="valueCurrency" [title]="title" [prefix]="'BR'">
        </thx-input-text>
      </div>
      <div class="col-4">
        <div>
          Input Com Descrição
        </div>
        <br>
        <thx-input-text [(ngModel)]="value2" [title]="title" [infoText]="infoText" [rightIcon]="img" required="true">
        </thx-input-text>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-4">
        <div>
          Input Desabilitado
        </div>
        <br>
        <thx-input-text [(ngModel)]="valueDisabled" [title]="title" [infoText]="infoText" required="true" disabled>
        </thx-input-text>
      </div>
      <div class="col-4">
        <div>
          Input Com Descrição e Imagem
        </div>
        <br>
        <thx-input-text [(ngModel)]="value3" [title]="title" [infoText]="infoText" [leftIcon]="img" required="true">
        </thx-input-text>
      </div>
      <div class="col-4">
        <div>
          Input Com Form
        </div>
        <br>
        <form [formGroup]="form">
          <thx-input-text formControlName="name" [title]="title" [infoText]="infoText">
          </thx-input-text>
        </form>
      </div>
    </div>
      <hr>
    <div class="row">
      <div class="col-12">
        <h1>Inputs de moeda</h1>
      </div>
      <div class="col-4">
        <div>
          Input de moeda
        </div>
        <br>
        <div>
          <thx-input-currency [(ngModel)]="money" [title]="currencyTitle" [infoText]="currencyText" currencySymbol="R$">
          </thx-input-currency>
        </div>
      </div>
    </div>
      <br>
      <br>
      <div class="row">
        <div class="col-12">
          <h1>Inputs de Local</h1>
        </div>
        <div class="col-4">
          <div>
            Input de Local
          </div>
          <br>
          <div>
            <thx-input-locale  [title]="addressTitle" (addressSelected)="addressSelected($event)" >
            </thx-input-locale>
          </div>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-12">
          <h1>Inputs de Data</h1>
        </div>
        <div class="col-4">
          <div>
            Input de data
          </div>
          <br>
          <div>
            <thx-input-date [title]="'Data legal'" [(ngModel)]="date1" ></thx-input-date>
          </div>
        </div>

        <div class="col-4">
          <div>
            Input de data
          </div>
          <br>
          <div [formGroup]="dateForm" >
            <thx-input-date [title]="'Data legal'"  formControlName="date" ></thx-input-date>
          </div>
        </div>
      </div>
      <br>
      <hr>
      <div><span>MODEL 1:</span> {{value1}}</div>
      <div><span>MODEL 2:</span> {{value2}}</div>
      <div><span>MODEL 3:</span> {{value3}}</div>
      <div><span>Form 3:</span> {{this.form?.value | json }}</div>
      <div><span>MODEL date 1: </span> {{ date1 }}</div>
      <div><span>Date FORM: </span> {{ this.dateForm?.value | json }}</div>
    </div>
  `,
  styles: []
})
export class DefaultComponent implements OnInit {

  money: 100;
  currencyTitle = 'Digite o valor que quer pagar';
  currencyText = 'Me dê sua grana';

  title = 'Nome Completo';
  infoText = 'Ex: Elton Coelho';
  currency = 'R$';

  value1 = 'Input funciona';
  valueCurrency: string;
  value2: string;
  value3: string;
  date1: any;

  valueDisabled = 'temos sim';
  img = 'credit.svg';

  private optionsCurrencyMask: any;

  form: FormGroup;
  dateForm: FormGroup;

  addressTitle = 'Local';

  constructor(private fb: FormBuilder) {
  }



  ngOnInit() {
      this.form = this.fb.group({
        name: ['Um Bom Nome', [Validators.required]],
      });

    this.dateForm = this.fb.group({
      date: [`2019-01-01T00:00:00`, [Validators.required]],
    });

    this.optionsCurrencyMask = {
      prefix: '',
      thousands: '.',
      decimal: ',',
      align: 'left',
      allowNegative: false
    };
  }

  addressSelected(address) {
    console.log('address', address);
  }
}
