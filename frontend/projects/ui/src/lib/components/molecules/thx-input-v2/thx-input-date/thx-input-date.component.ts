import { Component, forwardRef, OnInit, ViewChild } from '@angular/core';
import { ThxInput} from '../thx-input-base/thx-input';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IMyDateModel, IMyDpOptions, IMyInputFieldChanged, MyDatePicker } from 'mydatepicker';

@Component({
  selector: 'thx-input-date',
  templateUrl: './thx-input-date.component.html',
  styleUrls: ['./thx-input-date.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ThxInputDateComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ThxInputDateComponent),
      multi: true
    }
  ],
})
export class ThxInputDateComponent extends  ThxInput implements OnInit {
  @ViewChild('dateElement') dateElement: MyDatePicker;
  public myDatePickerOptions: IMyDpOptions;

  ngOnInit() {
    this.configDatePicker();
  }

  private configDatePicker() {

    this.myDatePickerOptions = {
      dateFormat: 'dd/mm/yyyy',
      showClearDateBtn: false,
      height: '38px',
      markCurrentDay: true,
      editableDateField: true,
      indicateInvalidDate: true,
      openSelectorOnInputClick: false,
      showInputField: true
    };
  }

  writeValue(myDate: any): void {
    if (myDate) {
      const date = new Date(myDate);
      if (date instanceof Date && !isNaN(date.getTime())) {
        this.value = {
          date: {
            day: date.getDate(),
            month: date.getMonth() + 1,
            year: date.getFullYear(),
          },
        };
        this.onChange(myDate);
      } else {
        this.onChange(null);
      }
    }
  }

  emitDateChanged(event: IMyDateModel) {
    const date = event.date;
    this.writeValue(`${date.year}-${this.pad(date.month)}-${this.pad(date.day)}T00:00:00`);
  }

  inputFocusBlur() {
    this.inFocus = !this.inFocus;
  }

  isLabelTopMode(): any {
    if (this.dateElement && this.dateElement.inputBoxEl)  {
      return (this.inFocus || this.value || (this.dateElement.inputBoxEl.nativeElement.value) ) && this.title;
    }
    return (this.inFocus || this.value) && this.title;
  }

  inputChanged(event: IMyInputFieldChanged) {
    if (this.dateElement && this.dateElement.inputBoxEl) {
      const elementHtml = this.dateElement.inputBoxEl.nativeElement;
      const result = event.value.replace(/[a-zA-Z.,!@#$%¨&*();:]/gi, '');
      elementHtml.value = result;
      event.value = elementHtml.value;
      if (!event.valid) {
        if (elementHtml.value.length == 3 && elementHtml.value.indexOf('/') < 0) {
          elementHtml.value = event.value.slice(0, 2) + '/' + event.value.slice(2, event.value.length);
          event.value = elementHtml.value;
        }
        if (elementHtml.value.length == 6 && (elementHtml.value.match(new RegExp('/', 'g')) || []).length < 2) {
          elementHtml.value = event.value.slice(0, 5) + '/' + event.value.slice(5, event.value.length);
          event.value = elementHtml.value;
        }
        if (elementHtml.value.length > 10) {
          elementHtml.value = event.value.slice(0, 10);
          event.value = elementHtml.value;
        }
      }
      this.dateElement.invalidDate = !event.valid;
    }
  }

  private pad(val) {
    if (val && val < 10) {
      return `0${val}`;
    }
    return val;
  }

}
