export interface InputCounterModel {
  length: number;
  limit: number;
}
