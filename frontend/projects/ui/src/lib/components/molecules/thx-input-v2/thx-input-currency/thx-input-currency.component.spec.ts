import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ThxInputCurrencyComponent } from './thx-input-currency.component';

describe('ThxInputCurrencyComponent', () => {
  let component: ThxInputCurrencyComponent;
  let fixture: ComponentFixture<ThxInputCurrencyComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxInputCurrencyComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxInputCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
