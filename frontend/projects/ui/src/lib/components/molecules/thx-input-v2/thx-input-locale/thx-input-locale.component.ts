import { Component, EventEmitter, Output, Input, OnInit } from '@angular/core';
import { ThxInput } from '../thx-input-base/thx-input';

@Component({
  selector: 'thx-input-locale',
  templateUrl: './thx-input-locale.component.html',
  styleUrls: ['./thx-input-locale.component.scss'],
})
export class ThxInputLocaleComponent extends ThxInput implements OnInit {

  @Input() optionGooglePlaceApi: any;
  @Output() addressSelected = new EventEmitter();

  ngOnInit() {}

  getAddress(address) {
    this.addressSelected.emit(address);
  }
}
