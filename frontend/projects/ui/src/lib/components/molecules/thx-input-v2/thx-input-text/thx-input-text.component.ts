import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator } from '@angular/forms';
import { ThxInput } from '../thx-input-base/thx-input';

@Component({
  selector: 'thx-input-text',
  templateUrl: './thx-input-text.component.html',
  styleUrls: ['./thx-input-text.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ThxInputTextComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ThxInputTextComponent),
      multi: true
    }
  ]
})
export class ThxInputTextComponent extends ThxInput implements OnInit, ControlValueAccessor, Validator {
  @Input() prefix: string;
  @Input() type = 'text';

  ngOnInit(): void {
  }
}
