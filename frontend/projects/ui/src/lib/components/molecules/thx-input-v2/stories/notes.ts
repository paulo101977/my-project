export class Notes {
    private notes = `
## Html:

~~~json
    <!-- Input text com NgModel -->
    <thx-input-text
        [(ngModel)]="value"
        [title]="title"
        [infoText]="infoText"
        [leftIcon]="img"
        required="true">
        </thx-input-text>

    <!-- Input text com form -->
    <form [formGroup]="form" >
            <thx-input-text
              formControlName="name"
              [title]="title"
              [infoText]="infoText"
              required="true"  >
            </thx-input-text>
    </form>

    <!-- Input text com NgModel -->
    <thx-input-currency
        [(ngModel)]="valueCurrency"
        [title]="title"
        [infoText]="infoText"
        [leftIcon]="img"
        currencySymbol="R$"
        required="true">
        </thx-input-currency>
~~~

## Typescript:

~~~ typescript

  // configurações gerais

  public title = 'Nome Completo';
  public infoText = 'Ex: Elton Coelho';
  public img = 'credit.svg';

  // ngModel
  public value = "Input funciona";
  public valueCurrency = 10;

  // Form
  public form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      name: ['Um Bom Nome', [ Validators.required ]],
    });
  }
~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
