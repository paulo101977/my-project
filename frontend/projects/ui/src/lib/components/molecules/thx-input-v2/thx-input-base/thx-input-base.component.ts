import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ThxErrorService } from '../../thx-error-v2/services/thx-error.service';
import { InputCounterModel } from './Input-counter-model';

@Component({
  selector: 'thx-input-base',
  templateUrl: './thx-input-base.component.html',
  styleUrls: ['./thx-input-base.component.scss']
})
export class ThxInputBaseComponent implements OnInit {

  @Input() title: string;
  @Input() infoText: string;
  @Input() prefix: string;
  @Input() disabled = false;
  @Input() leftIcon: string;
  @Input() rightIcon: string;
  @Input() required: boolean;
  @Input() isLabelTopMode: boolean;
  @Input() control: FormControl;
  @Input() autoHeight = false;
  @Input() hasFocus = false;
  @Input() counter: InputCounterModel;

  constructor( public errorService: ThxErrorService ) { }


  ngOnInit() {
  }

}
