import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';

import { ThxInputBaseComponent } from './thx-input-base.component';

describe('ThxInputBaseComponent', () => {
  let component: ThxInputBaseComponent;
  let fixture: ComponentFixture<ThxInputBaseComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxInputBaseComponent ],
      imports: [TranslateTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxInputBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
