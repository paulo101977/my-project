import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ThxInputTextComponent } from './thx-input-text.component';

describe('ThxInputTextComponent', () => {
  let component: ThxInputTextComponent;
  let fixture: ComponentFixture<ThxInputTextComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxInputTextComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxInputTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
