import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxInputDateComponent } from './thx-input-date.component';

describe('ThxInputDateComponent', () => {
  let component: ThxInputDateComponent;
  let fixture: ComponentFixture<ThxInputDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxInputDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxInputDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
