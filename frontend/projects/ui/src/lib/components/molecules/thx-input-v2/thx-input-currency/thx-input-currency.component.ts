import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ThxInput } from '../thx-input-base/thx-input';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'thx-input-currency',
  templateUrl: './thx-input-currency.component.html',
  styleUrls: ['./thx-input-currency.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ThxInputCurrencyComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ThxInputCurrencyComponent),
      multi: true
    }
  ]
})
export class ThxInputCurrencyComponent extends ThxInput implements OnInit {

  @Input() currencySymbol: string;
  @Input() allowNegative = false;
  public optionsCurrencyMask: any;

  ngOnInit() {
    this.optionsCurrencyMask = {
      prefix: '',
      thousands: '.',
      decimal: ',',
      align: 'left',
      allowNegative: this.allowNegative,
    };
  }

  valueChange(val: string) {
    if (+val === 0 || isNaN(+val)) {
      val = '0';
    }
    this.writeValue(val);
  }

}
