import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Card } from './model/card';

@Component({
  selector: 'thx-card',
  templateUrl: './thx-card.component.html',
  styleUrls: ['./thx-card.component.scss']
})
export class ThxCardComponent {

  @Input() cardList: Array<Card>;
  @Output() selected = new EventEmitter<Card>();

  public select(card: Card): void {
    this.selected.emit(card);
  }
}
