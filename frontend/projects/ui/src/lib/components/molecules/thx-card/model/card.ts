export interface Card {
  idTag?: string;
  id: string;
  headerCard: string;
  imgCard: string;
  tagLeft: string;
  tagRight: string;
  time: string;
}
