import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';
import { ThxCardComponent } from './thx-card.component';

@NgModule({
  imports: [
    CommonModule,
    ImgCdnModule,
  ],
  exports: [
    ThxCardComponent
  ],
  declarations: [ThxCardComponent]
})
export class ThxCardModule { }
