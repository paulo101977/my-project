import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';

import { ThxCardComponent } from './thx-card.component';

describe('ThxCardComponent', () => {
  let component: ThxCardComponent;
  let fixture: ComponentFixture<ThxCardComponent>;

  configureTestSuite( () => {
    TestBed.configureTestingModule({
      declarations: [ ThxCardComponent ],
      imports: [ImgCdnModule]
    });

    fixture = TestBed.createComponent(ThxCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
