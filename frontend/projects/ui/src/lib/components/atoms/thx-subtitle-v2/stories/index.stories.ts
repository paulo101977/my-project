import { componentFactory } from '.storybook/component-factory';
import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ThxSubtitleV2Component } from './../thx-subtitle-v2.component';
import { Notes } from './notes';
import { ThxSubtittleV2SizesEnum } from '../models/thx-subtittle-v2-sizes-enum';


const subtitle = 'Capacidade';
const DefaultComponent = componentFactory('default', `
    Subtitle with font small

  <thx-subtitle-v2
  [size]="scope.ThxSubtittleV2SizesEnum.SmallSize"
  [subtitle]="scope.subtitle">
  </thx-subtitle-v2>

  Subtitle with font default

  <thx-subtitle-v2
  [subtitle]="scope.subtitle">
  </thx-subtitle-v2>

  Subtitle with font Medium

  <thx-subtitle-v2
  [size]="scope.ThxSubtittleV2SizesEnum.MediumSize"
  [subtitle]="scope.subtitle">
  </thx-subtitle-v2>

  Subtitle with font Medium

  <thx-subtitle-v2
  [size]="scope.ThxSubtittleV2SizesEnum.BigSize"
  [subtitle]="scope.subtitle">
  </thx-subtitle-v2>

  Subtitle and content

  <thx-subtitle-v2
  [subtitle]="scope.subtitle">
    <div>
      <input type="text">
    </div>

  </thx-subtitle-v2>

  `, {subtitle, ThxSubtittleV2SizesEnum });

storiesOf('Ui | Atoms/ThxSubtitleV2', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule ],
    declarations: [ ThxSubtitleV2Component ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
