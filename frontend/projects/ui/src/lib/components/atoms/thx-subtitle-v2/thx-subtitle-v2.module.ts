import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ThxSubtitleV2Component } from './thx-subtitle-v2.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ThxSubtitleV2Component],
  exports: [ThxSubtitleV2Component]
})
export class ThxSubtitleV2Module { }
