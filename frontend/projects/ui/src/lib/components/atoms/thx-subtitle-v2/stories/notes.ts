export class Notes {
    private notes = `
## Html:

~~~json
    <subtitle-content
    [subtitle]="subtitle"
    >
    </subtitle-content>

~~~
## Typescript:

~~~typescript
    public subtitle = "Subtítulo do componente"
~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
