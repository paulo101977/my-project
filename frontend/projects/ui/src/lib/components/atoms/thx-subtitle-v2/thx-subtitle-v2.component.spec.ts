import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ThxSubtitleV2Component } from './thx-subtitle-v2.component';


describe('ThxSubtitleV2Component', () => {
  let component: ThxSubtitleV2Component;
  let fixture: ComponentFixture<ThxSubtitleV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxSubtitleV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxSubtitleV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
