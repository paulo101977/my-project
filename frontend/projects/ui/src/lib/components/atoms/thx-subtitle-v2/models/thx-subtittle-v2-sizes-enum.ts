export enum ThxSubtittleV2SizesEnum {
    SmallSize,
    DefaultSize,
    MediumSize,
    BigSize
}
