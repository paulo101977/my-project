import { Component, Input } from '@angular/core';
import { ThxSubtittleV2SizesEnum } from './models/thx-subtittle-v2-sizes-enum';

@Component({
  selector: 'thx-subtitle-v2',
  templateUrl: './thx-subtitle-v2.component.html',
  styleUrls: ['./thx-subtitle-v2.component.scss']
})
export class ThxSubtitleV2Component {
  @Input() subtitle: string;
  @Input() size: ThxSubtittleV2SizesEnum = ThxSubtittleV2SizesEnum.DefaultSize;

  public getClasses() {
    switch ( this.size ) {
      case ThxSubtittleV2SizesEnum.SmallSize:
        return 'thex-font-size-small';
      case ThxSubtittleV2SizesEnum.DefaultSize:
        return 'thex-font-size-small-11';
      case ThxSubtittleV2SizesEnum.MediumSize:
        return 'thex-font-size-items-two';
      case ThxSubtittleV2SizesEnum.BigSize:
        return 'thex-font-size-body-big';
      default:
        return 'thex-font-size-small-11';
    }
  }
}
