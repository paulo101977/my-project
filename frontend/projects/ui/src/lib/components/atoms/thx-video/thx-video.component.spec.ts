import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { configureTestSuite } from 'ng-bullet';
import { BypassUrlPipeStub } from '../../../pipes/bypass-url/testing/bypass-url.pipe.stub';

import { ThxVideoComponent } from './thx-video.component';

describe('ThxVideoComponent', () => {
  let component: ThxVideoComponent;
  let fixture: ComponentFixture<ThxVideoComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxVideoComponent, BypassUrlPipeStub ],
      imports: [ BrowserModule ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });

    fixture = TestBed.createComponent(ThxVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
