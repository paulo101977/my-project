import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxVideoComponent } from './thx-video.component';
import { BypassUrlModule } from '../../../pipes/bypass-url/bypass-url.module';

@NgModule({
  imports: [
    CommonModule,
    BypassUrlModule
  ],
  exports: [
    ThxVideoComponent
  ],
  declarations: [ ThxVideoComponent ],
})
export class ThxVideoModule { }
