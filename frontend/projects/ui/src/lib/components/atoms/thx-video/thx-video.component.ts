import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'thx-video',
  templateUrl: './thx-video.component.html',
  styleUrls: ['./thx-video.component.css']
})
export class ThxVideoComponent implements OnInit {

  @Input() width = 560;
  @Input() height = 315;
  @Input() frameBorder = 0;
  @Input() allow = 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture';
  @Input() allowFullscreen = true;
  @Input() src: string;

  constructor() { }

  ngOnInit() {
  }

}
