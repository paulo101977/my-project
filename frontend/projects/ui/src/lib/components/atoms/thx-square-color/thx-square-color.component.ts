import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'thx-square-color',
  template: `<ng-content></ng-content>`,
  styles: [':host-context { display: block; border-radius: 2px}']
})
export class ThxSquareColorComponent  {

  @Input() @HostBinding('style.background') color: string;
  @Input() @HostBinding('style.width.px') width = 10;
  @Input() @HostBinding('style.height.px') height = 10;

}
