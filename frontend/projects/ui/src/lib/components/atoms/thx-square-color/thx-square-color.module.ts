import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxSquareColorComponent } from './thx-square-color.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ThxSquareColorComponent],
  exports: [ThxSquareColorComponent]
})
export class ThxSquareColorModule { }

