import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxTitleV2Component } from './thx-title-v2.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ThxTitleV2Component],
  exports: [ThxTitleV2Component]
})
export class ThxTitleV2Module { }
