export class Notes {
    private notes = `
## Html:

~~~json
    <!-- title usage: -->
    <thx-title-v2
    [title]="title"
    [description]="description"
    >
    <div content-info></div>
    </thx-title-v2>
~~~

## Typescript:

~~~typescript

public title = "Titulo do componente"
public description = "Descrição do Componente"
~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
