import { componentFactory } from '.storybook/component-factory';
import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ThxTitleV2Component } from './../thx-title-v2.component';
import { Notes } from './notes';


const title = 'Tarifação';
const description = 'Limite mínimo e máximo de tarifa de UH';

const DefaultComponent = componentFactory('default', `
    Just title

    <thx-title-v2
    [title]="scope.title"
    >
    </thx-title-v2>
    Title and Description

    <thx-title-v2
      [title]="scope.title"
      [description]="scope.description"
    >
    </thx-title-v2>

    Title, description and content

    <thx-title-v2
      [title]="scope.title"
      [description]="scope.description"
    >
      <div>
        <input type="text" />
      </div>
    </thx-title-v2>
`

, {title, description});

storiesOf('Ui | Atoms/ThxTitleV2', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule ],
    declarations: [ ThxTitleV2Component ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
