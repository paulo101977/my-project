import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxTitleV2Component } from './thx-title-v2.component';

describe('ThxTitleV2.ComponentComponent', () => {
  let component: ThxTitleV2Component;
  let fixture: ComponentFixture<ThxTitleV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxTitleV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxTitleV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
