import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'thx-title-v2',
  templateUrl: './thx-title-v2.component.html',
  styleUrls: ['./thx-title-v2.component.css']
})
export class ThxTitleV2Component implements OnInit {

  @Input() title: string;
  @Input() description: string;

  constructor() { }

  ngOnInit() {
  }

}
