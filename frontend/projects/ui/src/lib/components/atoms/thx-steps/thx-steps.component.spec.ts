import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ImgCdnPipeStub } from '../../../pipes/img-cdn/testing/img-cdn.pipe.stub';
import { StepsData } from './mock-data/steps-data';

import { ThxStepsComponent } from './thx-steps.component';

describe('ThxStepsComponent', () => {
  let component: ThxStepsComponent;
  let fixture: ComponentFixture<ThxStepsComponent>;

  const steps = StepsData.STEPS;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxStepsComponent, ImgCdnPipeStub ]
    });

    fixture = TestBed.createComponent(ThxStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#select', () => {
    it('should send the selected step', () => {
      spyOn<any>(component, 'getActiveStep').and.returnValue(steps[0]);
      spyOn(component.selected, 'emit');

      const step = steps[2];

      component.select(step);
      expect(component.selected.emit).toHaveBeenCalledWith(step);
    });

    it('should do nothing if the step is already selected', () => {
      spyOn<any>(component, 'getActiveStep').and.returnValue(steps[1]);
      spyOn(component.selected, 'emit');
      const step = steps[1];

      component.select(step);
      expect(component.selected.emit).not.toHaveBeenCalled();
    });

    it('should do nothing if the step is disabled', () => {
      spyOn<any>(component, 'getActiveStep').and.returnValue(steps[2]);
      spyOn(component.selected, 'emit');
      const step = {...steps[1], disabled: true};

      component.select(step);
      expect(component.selected.emit).not.toHaveBeenCalled();
    });
  });

  describe('#onStepsChange', () => {
    it('should do nothing if there is an active step', () => {
      spyOn(component, 'select');

      component.onStepsChange(steps);

      expect(component.select).not.toHaveBeenCalled();
    });

    it('should select the first step if none is active', () => {
      spyOn(component, 'select');

      component.onStepsChange([]);

      expect(component.select).toHaveBeenCalled();
    });
  });

  describe('#getStepCssClasses', () => {
    it('should get the step\'s CSS classes', () => {
      const [step1, step2] = steps;
      const classes1 = component.getStepCssClasses(step1);
      const classes2 = component.getStepCssClasses(step2);

      expect(classes1).not.toEqual(classes2);
      expect(classes1).toEqual({
        '--complete': !!step1.complete,
        '--active': !!step1.active,
        '--disabled': !!step1.disabled,
      });
      expect(classes2).toEqual({
        '--complete': !!step2.complete,
        '--active': !!step2.active,
        '--disabled': !!step2.disabled,
      });
    });
  });
});
