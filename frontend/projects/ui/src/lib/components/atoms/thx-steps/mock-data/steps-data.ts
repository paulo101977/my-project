import { Step } from '../model/step';

export class StepsData {
  public static readonly STEPS: Step[] = [
    {
      id: '1',
      complete: false,
      title: 'UH'
    },
    {
      id: '2',
      complete: false,
      active: true,
      title: 'Tipo de UH'
    },
    {
      id: '3',
      complete: true,
    },
    {
      id: '4',
      complete: false,
    },
  ];
}
