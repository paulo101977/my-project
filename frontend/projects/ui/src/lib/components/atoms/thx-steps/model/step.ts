export interface Step {
  id: string;
  title?: string;
  complete?: boolean;
  active?: boolean;
  disabled?: boolean;
}
