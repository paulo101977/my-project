import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';
import { ThxStepsComponent } from './thx-steps.component';

@NgModule({
  imports: [
    CommonModule,
    ImgCdnModule
  ],
  declarations: [ThxStepsComponent],
  exports: [ThxStepsComponent]
})
export class ThxStepsModule { }
