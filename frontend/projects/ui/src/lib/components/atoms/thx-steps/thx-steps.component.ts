import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Step } from './model/step';

@Component({
  selector: 'thx-steps',
  templateUrl: './thx-steps.component.html',
  styleUrls: ['./thx-steps.component.scss']
})
export class ThxStepsComponent implements OnChanges {
  @Input() isHorizontal = false;
  @Input() steps: Step[];
  @Output() selected = new EventEmitter<Step>();

  ngOnChanges(changes: SimpleChanges): void {
    const { steps } = changes;

    if (steps && steps.currentValue) {
      this.onStepsChange(steps.currentValue);
    }
  }

  public getStepCssClasses(step: Step) {
    return {
      '--complete': !!step.complete,
      '--active': !!step.active,
      '--disabled': !!step.disabled,
    };
  }

  public onStepsChange(steps: Step[]) {
    if (!this.getActiveStep(steps)) {
      this.select(steps[0]);
    }
  }

  public select(step: Step) {
    if (this.getActiveStep(this.steps) === step || step.disabled) {
      return;
    }

    this.selected.emit(step);
  }

  private getActiveStep(steps: Step[]) {
    if (!steps || !steps.length) {
      return;
    }

    return steps.find(step => step.active);
  }
}
