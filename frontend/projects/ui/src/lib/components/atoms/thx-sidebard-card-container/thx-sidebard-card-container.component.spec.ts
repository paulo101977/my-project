import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ThxSidebardCardContainerComponent } from './thx-sidebard-card-container.component';

describe('ThxSidebardCardContainerComponent', () => {
  let component: ThxSidebardCardContainerComponent;
  let fixture: ComponentFixture<ThxSidebardCardContainerComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxSidebardCardContainerComponent ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxSidebardCardContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
