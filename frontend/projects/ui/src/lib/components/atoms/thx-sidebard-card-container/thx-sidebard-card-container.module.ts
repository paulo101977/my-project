import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxSidebardCardContainerComponent } from './thx-sidebard-card-container.component';

@NgModule({
  declarations: [
    ThxSidebardCardContainerComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ThxSidebardCardContainerComponent
  ]
})
export class ThxSidebardCardContainerModule { }
