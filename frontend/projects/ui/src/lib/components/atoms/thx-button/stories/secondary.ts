import { Component } from '@angular/core';
import { ThxButtonColorType } from '../enums/thx-button-color-type.enum';

@Component({
  selector: 'thx-secondary-button',
  template: `
    <thx-button [colorType]="color">
      my custom btn secondary
    </thx-button>
  `,
  styles: []
})
export class SecondaryComponent {

  public color: ThxButtonColorType = ThxButtonColorType.Secondary;
}
