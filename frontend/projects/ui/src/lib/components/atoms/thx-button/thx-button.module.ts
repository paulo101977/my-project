import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxButtonComponent } from './thx-button.component';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ ThxButtonComponent ],
  exports: [ ThxButtonComponent ]
})
export class ThxButtonModule { }
