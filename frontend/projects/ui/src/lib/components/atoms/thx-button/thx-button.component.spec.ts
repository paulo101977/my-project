import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxButtonComponent } from './thx-button.component';

describe('ThxButtonV2Component', () => {
  let component: ThxButtonComponent;
  let fixture: ComponentFixture<ThxButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
