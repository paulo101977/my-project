import { Component  } from '@angular/core';

@Component({
  selector: 'thx-disabled-button',
  template: `
    <thx-button [disabled]="true" [ngClass]="'thx-btn-primary'">
      my custom button disabled
    </thx-button>
  `,
  styles: []
})
export class DisabledComponent {

}
