import { Component } from '@angular/core';
import { ThxButtonColorType } from '../enums/thx-button-color-type.enum';

@Component({
  selector: 'thx-primary-button',
  template: `
    <thx-button [colorType]="color">
      my custom btn primary
    </thx-button>
  `,
  styles: []
})
export class PrimaryComponent {

  public color: ThxButtonColorType = ThxButtonColorType.Primary;
}
