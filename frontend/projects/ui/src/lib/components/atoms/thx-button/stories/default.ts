import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-button',
  template: `
    <thx-button >
      my custom button default
    </thx-button>
  `
})
export class DefaultComponent {

}
