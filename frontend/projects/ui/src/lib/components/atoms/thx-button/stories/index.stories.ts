import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ThxButtonComponent } from '../thx-button.component';

import { DefaultComponent } from './default';
import { PrimaryComponent } from './primary';
import { DisabledComponent } from './disabled';
import { SecondaryComponent } from './secondary';
import { Notes } from './notes';

const noteString = new Notes().getNotes();
storiesOf('Ui | Atoms/ThxButton', module)
    .addDecorator(
        moduleMetadata({
            imports: [CommonModule],
            declarations: [ThxButtonComponent]
        })
    )
    .add('Default', () => ({
            component: DefaultComponent
        }),
        {notes: { markdown: noteString}} )
    .add('Disabled', () => ({
            component: DisabledComponent
        }),
        {notes: { markdown: noteString}})
    .add('Primary', () => ({
            component: PrimaryComponent
        }),
        {notes: noteString})
    .add('Secondary', () => ({
            component: SecondaryComponent
        }),
        {notes: { markdown: noteString}});
