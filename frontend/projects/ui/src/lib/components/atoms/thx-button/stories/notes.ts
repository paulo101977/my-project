export class Notes {
    private notes = `
## Html:

~~~json
    <!-- disabled -->
    <thx-button [disabled]="true" [ngClass]="'thx-btn-primary'">
      my custom button disabled
    </thx-button>

    <!-- default -->
    <thx-button >
      my custom button default
    </thx-button>

    <!-- primary -->
    <thx-button (click)="clicked()" [ngClass]="'thx-btn-primary'">
      my custom btn primary
    </thx-button>
~~~

## Typescript:

~~~typescript

~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
