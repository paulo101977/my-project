import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ThxButtonType } from './enums/thx-button-type.enum';
import { ThxButtonColorType } from './enums/thx-button-color-type.enum';

@Component({
  selector: 'thx-button',
  template: `
    <button class="thx-btn"
            (click)="clicked($event)"
            [disabled]="disabled"
            [ngClass]="{
          'thx-btn-primary': isPrimary,
          'thx-btn-secondary': isSecondary,
          'thx-btn-outline': isOutline
        }">
      <ng-content></ng-content>
    </button>
  `,
  styleUrls: ['./thx-button.component.scss']
})
export class ThxButtonComponent implements OnChanges {
  @Input() disabled = false;
  @Input() type;
  @Input() colorType;
  @Output() click = new EventEmitter();

  public isPrimary: boolean;
  public isSecondary: boolean;
  public isOutline: boolean;

  ngOnChanges() {
    switch (this.type) {
      case ThxButtonType.Outline:
        this.isOutline = true;
        break;
    }
    switch (this.colorType) {
      case ThxButtonColorType.Primary:
        this.isPrimary = true;
        break;
      case ThxButtonColorType.Secondary:
        this.isSecondary = true;
        break;
    }
  }

  public clicked(e) {
    if (!this.disabled) {
      this.click.emit(e);
    }
  }
}
