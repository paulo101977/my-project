import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ThxInputComponent } from './thx-input.component';

@NgModule({
  imports: [ CommonModule, FormsModule ],
  declarations: [ ThxInputComponent ],
  exports: [ ThxInputComponent ]
})
export class ThxInputModule { }
