export class Notes {
    private notes = `
## Html:

~~~json
    <!-- default with form usage: -->
    <form>
      <thx-input
        [type]="'email'"
        [placeholder]="'Type your email'"
        [(ngModel)]="mymodel"
        name="mymodel"
        [inputId]="'name'"
        [required]="true">
      </thx-input>
    </form>
    <p>
      model test: {{mymodel}}
    </p>

    <!-- password example: -->
    <thx-input
        [type]="'password'">
    </thx-input>

    <!-- disabled example -->
    <thx-input [disabled]="true"></thx-input>

~~~

## Typescript:

~~~typescript

~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
