import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { ThxInputComponent } from '../thx-input.component';

import { DefaultComponent } from './default';
import { DisabledComponent } from './disabled';
import { PasswordComponent } from './password';


storiesOf('Ui | Atoms/ThxInput', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule, FormsModule ],
    declarations: [ ThxInputComponent ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }})

.add('Disabled', () => ({
  component: DisabledComponent
}),
    { notes: { markdown: new Notes().getNotes() }})

.add('Password type', () => ({
  component: PasswordComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
