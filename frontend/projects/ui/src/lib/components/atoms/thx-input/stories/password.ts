import { Component  } from '@angular/core';

@Component({
  selector: 'thx-password-input',
  template: `
    <thx-input
      [type]="'password'">
    </thx-input>
  `,
  styles: []
})
export class PasswordComponent {

}
