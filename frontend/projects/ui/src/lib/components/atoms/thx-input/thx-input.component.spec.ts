import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxInputComponent } from './thx-input.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ThxInputComponent', () => {
  let component: ThxInputComponent;
  let fixture: ComponentFixture<ThxInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxInputComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
