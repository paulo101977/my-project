import { Component  } from '@angular/core';

@Component({
  selector: 'thx-disabled-input',
  template: `
    <thx-input [disabled]="true"></thx-input>
  `,
  styles: []
})
export class DisabledComponent {

}
