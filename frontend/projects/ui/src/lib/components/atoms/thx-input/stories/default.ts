import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-input',
  template: `
    <form>
      <thx-input
        [type]="'email'"
        [placeholder]="'Type your email'"
        [(ngModel)]="mymodel"
        name="mymodel"
        [inputId]="'name'"
        [required]="true">
      </thx-input>
    </form>
    <p>
      model test: {{mymodel}}
    </p>
  `,
  styles: []
})
export class DefaultComponent {
  mymodel = '';
  type = 'email';
}
