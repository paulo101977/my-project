import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';


@Component({
  selector: 'thx-input',
  template: `
    <input
      class="thx-input"
      [disabled]="disabled"
      [ngClass]="ngClass"
      [placeholder]="placeholder"
      [type]="type"
      [(ngModel)]="value"
      (blur)="onTouched()"
      [attr.id]="inputId"
      >
  `,
  styleUrls: ['./thx-input.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ThxInputComponent),
    multi: true
  }]
})

// TODO: implement interface ControlValueAccessor
export class ThxInputComponent implements ControlValueAccessor {

  @Input() disabled = false;
  @Input() ngClass: any;
  @Input() placeholder = '';
  @Input() type = '';
  @Input() inputId = '';

  // variables to use at ControlValueAccessor
  innervalue = '';

  //  the implementation methods call that functions
  onChange = (_: any) => { };
  onTouched = () => { };

  // get accessor
  get value(): any {
    return this.innervalue;
  }

  // set accessor including call the onchange callback
  set value(v: any) {
      if (v !== this.innervalue) {
          this.innervalue = v;
          this.onChange(v);
      }
  }

  // method of interface ControlValueAccessor
  writeValue(v: any): void {
    this.innervalue = v || '';
  }

  // method of interface ControlValueAccessor
  registerOnChange(fn: (_: any) => {}): void { this.onChange = fn; }

  // method of interface ControlValueAccessor
  registerOnTouched(fn: () => {}): void { this.onTouched = fn; }

  // method of interface ControlValueAccessor (optional)
  setDisabledState?(isDisabled: boolean): void {}
}
