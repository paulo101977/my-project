import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxWidgetBoxComponent } from './thx-widget-box.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ThxWidgetBoxComponent],
  exports: [ThxWidgetBoxComponent]
})
export class ThxWidgetBoxModule { }
