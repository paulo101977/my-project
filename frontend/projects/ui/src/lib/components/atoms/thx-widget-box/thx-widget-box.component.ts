import { Component } from '@angular/core';

@Component({
  selector: 'thx-widget-box',
  template: `
    <div class="widget-box">
      <ng-content></ng-content>
    </div>`,
  styles: [
    `.widget-box{ border: 1px solid var(--color-grey-sauvignon); border-radius: 3px; padding: 10px 10px; height: 100%; width: 100%;}`,
    ':host-context { display: block; height: 100%;}']
})
export class ThxWidgetBoxComponent {

}
