import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { InputBaseEfectV2Component } from './input-base-efect-v2.component';

describe('InputBaseEfectV2Component', () => {
  let component: InputBaseEfectV2Component;
  let fixture: ComponentFixture<InputBaseEfectV2Component>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ InputBaseEfectV2Component ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(InputBaseEfectV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
