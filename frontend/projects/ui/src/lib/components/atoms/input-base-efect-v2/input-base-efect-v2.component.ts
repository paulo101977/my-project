import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'thx-input-base-efect-v2',
  templateUrl: './input-base-efect-v2.component.html',
  styleUrls: ['./input-base-efect-v2.component.scss']
})
export class InputBaseEfectV2Component implements OnInit {


  @Input() title: string;
  @Input() leftIcon: string;
  @Input() rightIcon: string;
  @Input() prefix: string;
  @Input() required: boolean;
  @Input() disabled: boolean;
  @Input() hasError: boolean;
  @Input() hasFocus: boolean;
  @Input() isLabelTopMode: boolean;
  @Input() autoHeight = false;

  constructor() { }

  ngOnInit() {
  }

  getInputContainerClass() {
    return {
      'has-left-icon': this.leftIcon,
      'has-right-icon': this.rightIcon,
      'has-prefix': this.prefix ,
      'disabled': this.disabled,
      'has-error': this.hasError,
      'auto-height': this.autoHeight,
      'default-height': !this.autoHeight,
      'has-focus': this.hasFocus
    };
  }

  getHeightClass() {
    return {
      'auto-height': this.autoHeight,
      'default-height': !this.autoHeight
    };
  }

}
