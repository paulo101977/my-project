import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputBaseEfectV2Component } from './input-base-efect-v2.component';
import { ThxImgModule } from '../thx-img/thx-img.module';

/**
 * Do NOT expose this module outside libs domain
 */
@NgModule({
  imports: [CommonModule, ThxImgModule],
  declarations: [InputBaseEfectV2Component],
  exports: [InputBaseEfectV2Component]
})
export class InputBaseEfectV2Module { }
