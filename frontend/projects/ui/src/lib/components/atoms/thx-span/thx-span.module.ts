import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxSpanComponent } from './thx-span.component';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ ThxSpanComponent ],
  exports: [ ThxSpanComponent ]
})
export class ThxSpanModule { }
