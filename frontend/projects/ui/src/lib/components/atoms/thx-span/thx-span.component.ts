import { Component } from '@angular/core';

@Component({
  selector: 'thx-span',
  template: `
    <span>
      <ng-content></ng-content>
    </span>
  `
})
export class ThxSpanComponent { }
