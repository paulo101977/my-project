import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { ThxSpanComponent } from '../thx-span.component';

import { DefaultComponent } from './default';

storiesOf('Ui | Atoms/ThxSpan', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule ],
    declarations: [ ThxSpanComponent ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
