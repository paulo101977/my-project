import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-span',
  template: `
    <thx-span>
      my custom span
    </thx-span>
  `
})
export class DefaultComponent {

}
