import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxSpanComponent } from './thx-span.component';

describe('ThxSpanComponent', () => {
  let component: ThxSpanComponent;
  let fixture: ComponentFixture<ThxSpanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxSpanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxSpanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
