export class Notes {
    private notes = `
## Html:

~~~json
    <!-- H1 -->
    <thx-heading [type]="'h1'">
      H1 with text
    </thx-heading>

    <!-- H2 with span inside it -->
    <thx-heading [type]="'h2'">
      <span>h2 with span</span>
    </thx-heading>

    <!-- H3 with more complex el -->
    <thx-heading [type]="'h3'" >
      <span>
        h3 span with img
        <img
          style="width: 32px; height: 32px;"
          src="https://material.angularjs.org/latest/img/logo.svg" />
      </span>
    </thx-heading>

    <!-- H4 -->
    <thx-heading [type]="'h4'">
      H4
    </thx-heading>

    <!-- H5 -->
    <thx-heading [type]="'h5'">
      H5
    </thx-heading>

    <!-- H6 -->
    <thx-heading [type]="'h6'">
      H6
    </thx-heading>
~~~

## Typescript:

~~~typescript

~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
