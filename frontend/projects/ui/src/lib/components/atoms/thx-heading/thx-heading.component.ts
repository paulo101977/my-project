import { Component , Renderer2, OnInit, Input, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'thx-heading',
  templateUrl: './thx-heading.component.html'
})
export class ThxHeadingComponent implements OnInit {

  @Input() type = 'h1';
  @ViewChild('template') element: ElementRef;

  constructor(private renderer: Renderer2, private el: ElementRef) { }

  ngOnInit() {
    const { nativeElement } = this.element;
    let  titleElem;

    // get the inner element of ng-content
    let inner = nativeElement.children[0] || nativeElement.innerHTML;

    // if is a text, create a DOM element with text
    if (typeof inner === 'string') {
      inner = this.renderer.createText(inner);
    }

    // create the header element
    titleElem = this.renderer.createElement(this.type);

    // append the ng-content to generated heading
    this.renderer.appendChild(titleElem, inner);

    // append to template
    this.renderer.appendChild(this.el.nativeElement, titleElem);
  }

}
