import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-heading',
  template: `
    <thx-heading [type]="'h1'">
      H1 with text
    </thx-heading>
    <thx-heading [type]="'h2'">
      <span>h2 with span</span>
    </thx-heading>
    <thx-heading [type]="'h3'" >
      <span>
        h3 span with img
        <img
          style="width: 32px; height: 32px;"
          src="https://material.angularjs.org/latest/img/logo.svg" />
      </span>
    </thx-heading>
    <thx-heading [type]="'h4'">
      H4
    </thx-heading>
    <thx-heading [type]="'h5'">
      H5
    </thx-heading>
    <thx-heading [type]="'h6'">
      H6
    </thx-heading>
  `,
  styles: []
})
export class DefaultComponent {

}
