import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxHeadingComponent } from './thx-heading.component';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ ThxHeadingComponent ],
  exports: [ ThxHeadingComponent ]
})
export class ThxHeadingModule { }
