import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxHeadingComponent } from './thx-heading.component';

describe('ThxHeadingComponent', () => {
  let component: ThxHeadingComponent;
  let fixture: ComponentFixture<ThxHeadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxHeadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
