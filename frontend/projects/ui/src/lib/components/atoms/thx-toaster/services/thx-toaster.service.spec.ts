import { TestBed, inject, tick } from '@angular/core/testing';

import { ThxToasterService } from './thx-toaster.service';

describe('ThxToasterService', () => {
  let service: ThxToasterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ThxToasterService]
    });

    service = TestBed.get(ThxToasterService);
    spyOn(service['emitChangeSource'], 'next');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#emitChange', () => {
    it('should send a success message', () => {
      const success = true;
      const message = 'message';
      const next = [ success, message ];

      service.emitChange(success, message);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith(next);
    });

    it('should send an error message', () => {
      const success = false;
      const message = 'message';
      const next = [ success, message ];

      service.emitChange(success, message);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith(next);
    });
  });

  describe('#emitChangeTwoMessages', () => {
    it('should send a composed success message', () => {
      const success = true;
      const message = 'message';
      const secondMessage = 'second';
      const next = [ success, `${message} ${secondMessage}` ];

      service.emitChangeTwoMessages(success, message, secondMessage);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith(next);
    });

    it('should send a composed error message', () => {
      const success = false;
      const message = 'message';
      const secondMessage = 'second';
      const next = [ success, `${message} ${secondMessage}` ];

      service.emitChangeTwoMessages(success, message, secondMessage);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith(next);
    });
  });

  describe('#showMessage', () => {
    it('should send a success message', () => {
      const config = {messages: 'message'};
      const next = {
        message: config.messages,
        success: true,
        visibilityTime: undefined
      };

      service.showMessage(config);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith(next);
    });

    it('should send a composed message', () => {
      const config = {messages: ['message', 'message', 'message']};
      const next = {
        message: config.messages.join(' '),
        success: true,
        visibilityTime: undefined
      };

      service.showMessage(config);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith(next);
    });

    it('should send an error message', () => {
      const config = {messages: 'message', success: false};
      const next = {
        message: config.messages,
        success: config.success,
        visibilityTime: undefined
      };

      service.showMessage(config);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith(next);
    });

    it('should send a message with different visibility time', () => {
      const config = {
        messages: 'message',
        success: false,
        visibilityTime: 500
      };
      const next = {
        message: config.messages,
        success: config.success,
        visibilityTime: config.visibilityTime
      };

      service.showMessage(config);

      expect(service['emitChangeSource'].next).toHaveBeenCalledWith(next);
    });
  });
});
