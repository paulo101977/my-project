import { Pipe, PipeTransform } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Subject } from 'rxjs';
import { ThxToasterService } from './services/thx-toaster.service';

import { ThxToasterComponent } from './thx-toaster.component';

describe('ThxToasterComponent', () => {
  let component: ThxToasterComponent;
  let fixture: ComponentFixture<ThxToasterComponent>;
  const emitter = new Subject<any>();
  const thxToasterServiceStub: Partial<ThxToasterService> = {
    changeEmitted$: emitter.asObservable()
  };

  @Pipe({name: 'imgCdn'})
  class ImgCdnPipeStub implements PipeTransform {
    transform(value: any, ...args: any[]): any {
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxToasterComponent, ImgCdnPipeStub ],
      providers: [
        { provide: ThxToasterService, useValue: thxToasterServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxToasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set values with a call deprecated on change', fakeAsync(() => {
    const success = false;
    const message = 'message';

    emitter.next([success, message]);

    expect(component.isSuccess).toBe(success);
    expect(component.message).toBe(message);
    expect(component.isVisible).toBe(true);
    tick(5000);
    expect(component.isVisible).toBe(false);
  }));

  it('should set values on change', fakeAsync(() => {
    const success = false;
    const message = 'message';

    emitter.next({success, message});

    expect(component.isSuccess).toBe(success);
    expect(component.message).toBe(message);
    expect(component.isVisible).toBe(true);
    tick(5000);
    expect(component.isVisible).toBe(false);
  }));

  it('should cancel the timeout on a second change', fakeAsync(() => {
    const success = false;
    const message = 'message';

    emitter.next({success, message});
    tick(1000);
    emitter.next({success, message});
    tick(4000);

    expect(component.isVisible).toBe(true);
    tick(5000);
    expect(component.isVisible).toBe(false);
  }));

  it('should set a custom visibility time', fakeAsync(() => {
    const success = false;
    const message = 'message';
    const visibilityTime = 500;

    emitter.next({success, message, visibilityTime});

    expect(component.isVisible).toBe(true);
    tick(400);
    expect(component.isVisible).toBe(true);
    tick(100);
    expect(component.isVisible).toBe(false);
  }));
});
