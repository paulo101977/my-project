import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ThxToasterTypeV2Enum } from '../../thx-toaster-v2/thx-toaster-type-v2.enum';

@Injectable({
  providedIn: 'root'
})
export class ThxToasterService {
  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$: Observable<any> = this.emitChangeSource.asObservable();

  private generateToasterConfig(config) {
    const message = this.mergeMessages(config.messages);
    const type = config.type == null ? ThxToasterTypeV2Enum.SUCCESS : config.type;

    return {
      message,
      type,
      visibilityTime: config.visibilityTime
    };
  }

  private mergeMessages(messages) {
    if (!Array.isArray(messages)) {
      return messages;
    }

    return messages.join(' ');
  }

  public showMessage(config) {
    const toasterConfig = this.generateToasterConfig(config);
    this.emitChangeSource.next(toasterConfig);
  }

  // Service message commands
  public emitChange(isSuccess: boolean, message: string) {
    console.warn('This method is deprecated. Use ThxToasterService#showMessage instead.');
    this.emitChangeSource.next([isSuccess, message]);
  }

  public emitChangeTwoMessages(isSuccess: boolean, message: string, variable: string) {
    console.warn('This method is .deprecated Use ThxToasterService#showMessage instead.');
    const msgToShow = message + ' ' + variable;
    this.emitChangeSource.next([isSuccess, msgToShow]);
  }
}
