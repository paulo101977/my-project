import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';
import { ThxToasterComponent } from './thx-toaster.component';

@NgModule({
  imports: [
    CommonModule,
    ImgCdnModule
  ],
  declarations: [ThxToasterComponent],
  exports: [ThxToasterComponent]
})
export class ThxToasterModule { }
