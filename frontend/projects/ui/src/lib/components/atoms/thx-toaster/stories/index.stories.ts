import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ThxToasterModule } from '../thx-toaster.module';
import { DefaultComponent } from './default';
import { Notes } from './notes';


storiesOf('Ui | Atoms/ThxToaster', module)
.addDecorator(
  moduleMetadata({
    imports: [ ThxToasterModule ],
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
