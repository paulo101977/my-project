export class Notes {
    private notes = `
## Html:

~~~json
    <!-- toaster usage: -->
    <thx-toaster></thx-toaster>
~~~

## Typescript:

~~~typescript
import { Component  } from '@angular/core';
import { ThxToasterService } from '@inovacao-cmnet/thx-ui';

@Component({
  selector: 'thx-default-toaster',
  template: \`
    <div style="margin: 40px;">
      <div>
        <h4>New API</h4>
        <div style="display: flex; flex-direction: column">
          <button (click)="showSuccessMessage()">Success message</button>
          <button (click)="showErrorMessage()">Error message</button>
          <button (click)="showComposedMessage()">Composed message</button>
          <button (click)="showFastMessage()">Fast message</button>
        </div>
      </div>
      <div>
        <h4>Deprecated</h4>
        <div style="display: flex; flex-direction: column">
          <button (click)="showDeprecatedSuccessMessage()">Success message</button>
          <button (click)="showDeprecatedErrorMessage()">Error message</button>
          <button (click)="showDeprecatedComposedMessages()">Composed message</button>
        </div>
      </div>
    </div>
    <thx-toaster></thx-toaster>
  \`
})
export class DefaultComponent {
  constructor(private thxToasterService: ThxToasterService) {}

  public showDeprecatedSuccessMessage() {
    this.thxToasterService.emitChange(true, 'message');
  }

  public showDeprecatedErrorMessage() {
    this.thxToasterService.emitChange(false, 'message');
  }

  public showDeprecatedComposedMessages() {
    this.thxToasterService.emitChangeTwoMessages(true, 'message', 'other');
  }

  public showSuccessMessage() {
    this.showMessage({messages: 'message'});
  }

  public showErrorMessage() {
    this.showMessage({messages: 'message', success: false});
  }

  public showComposedMessage() {
    const messages = [
      'message',
      'other',
      'another',
      'hey'
    ];
    this.showMessage({messages});
  }

  public showFastMessage() {
    this.showMessage({messages: 'message', visibilityTime: 500});
  }

  private showMessage(config) {
    this.thxToasterService.showMessage(config);
  }
}

~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
