import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { ThxToasterService } from './services/thx-toaster.service';

@Component({
  selector: 'thx-toaster',
  templateUrl: './thx-toaster.component.html',
  styleUrls: ['./thx-toaster.component.css']
})
export class ThxToasterComponent implements OnInit, OnDestroy {
  public isSuccess: boolean;
  public message: string;
  public isVisible: boolean;
  private subscription: ISubscription;
  private activeTimeout: any;

  constructor(private toasterEmitService: ThxToasterService) {}

  ngOnInit() {
    this.listenToToasterCalls();
  }

  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }

  private listenToToasterCalls() {
    this.subscription = this.toasterEmitService.changeEmitted$
      .subscribe(response => {
        if (Array.isArray(response)) {
          this.deprecatedShowToaster(response);
        } else {
          const { success, message, visibilityTime } = response;
          this.isSuccess = success;
          this.showToaster(message, visibilityTime);
        }
      });
  }

  private deprecatedShowToaster(response) {
    const [isSuccess, message] = response;
    this.isSuccess = isSuccess;
    this.showToaster(message);
  }

  private showToaster(message: string, visibilityTime = 5000) {
    clearTimeout(this.activeTimeout);
    this.isVisible = true;
    this.message = message;

    this.activeTimeout = setTimeout(() => {
      this.hideToaster();
    }, visibilityTime);
  }

  private hideToaster() {
    this.isVisible = false;
    this.message = '';
  }
}
