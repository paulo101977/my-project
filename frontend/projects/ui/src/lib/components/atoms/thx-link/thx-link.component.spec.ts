import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxLinkComponent } from './thx-link.component';

describe('ThxLinkComponent', () => {
  let component: ThxLinkComponent;
  let fixture: ComponentFixture<ThxLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should clicked emit when href is null', () => {
    component.href = null;
    const event = {
      preventDefault: () => {}
    };

    spyOn(component.clicked, 'emit');
    spyOn(event, 'preventDefault');

    component.setHrefValidation(event);

    expect(component.clicked.emit).toHaveBeenCalled();
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should clicked emit when href has value', () => {
    component.href = 'www.google.com';
    const event = {
      preventDefault: () => {}
    };

    spyOn(component.clicked, 'emit');
    spyOn(event, 'preventDefault');
    component.setHrefValidation(event);

    expect(component.clicked.emit).toHaveBeenCalled();
    expect(event.preventDefault).not.toHaveBeenCalled();
  });
});
