import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxLinkComponent } from './thx-link.component';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ ThxLinkComponent ],
  exports: [ ThxLinkComponent ]
})
export class ThxLinkModule { }
