import { Component  } from '@angular/core';

@Component({
  selector: 'thx-underline-link',
  template: `
    <thx-link
      [ngClass]="'thx-link-underline'"
      [href]="'http://www.google.com.br'">
      my custom button default
    </thx-link>
  `,
  styles: []
})
export class UnderlineComponent {

}
