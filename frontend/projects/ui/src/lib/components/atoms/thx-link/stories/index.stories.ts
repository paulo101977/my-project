import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { ThxLinkComponent } from '../thx-link.component';

import { DefaultComponent } from './default';
import { UnderlineComponent } from './underline';
import { WithActionComponent } from './with-action';

storiesOf('Ui | Atoms/ThxLink', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule ],
    declarations: [ ThxLinkComponent ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }})
.add('WithAction', () => ({
  component: WithActionComponent
}),
    { notes: { markdown: new Notes().getNotes() }})
.add('Underline', () => ({
  component: UnderlineComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
