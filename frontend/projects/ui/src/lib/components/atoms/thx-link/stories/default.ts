import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-link',
  template: `
    <thx-link [href]="'http://www.google.com.br'">
      my custom button default
    </thx-link>
  `,
  styles: []
})
export class DefaultComponent {

}
