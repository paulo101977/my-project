import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'thx-link',
  template: `
    <a
        [attr.href]="href"
        class="thx-link"
        [ngClass]="ngClass"
        (click)="setHrefValidation($event)">
      <ng-content></ng-content>
    </a>
  `,
  styleUrls: ['./thx-link.component.scss']
})
export class ThxLinkComponent {
  @Input() href = '';
  @Input() ngClass: any;
  @Output() clicked = new EventEmitter();

  public setHrefValidation(event) {
    this.clicked.emit();

    if (!this.href) {
        event.preventDefault();
    }
  }
}
