import { Component  } from '@angular/core';

@Component({
  selector: 'thx-with-action-link',
  template: `
    <thx-link (clicked)="myAction($event)">
      my custom button default
    </thx-link>
  `,
  styles: []
})
export class WithActionComponent {
  public myAction(event) {
    console.log('Call me! I am here.');
  }
}
