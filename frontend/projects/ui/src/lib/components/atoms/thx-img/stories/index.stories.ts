import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { ThxImgComponent } from '../thx-img.component';

import { DefaultComponent } from './default';


storiesOf('Ui | Atoms/ThxImage', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule ],
    declarations: [ ThxImgComponent ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
