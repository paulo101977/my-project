import { Component, Input } from '@angular/core';


@Component({
  selector: 'thx-img',
  template: `
    <img
      [alt]="alt"
      [src]="src | imgCdn"
      [title]="title"
      [ngClass]="{'fullWidth' : fullImg, 'full-size': fullSize}"
      [ngStyle]="style"
    >
  `,
  styleUrls: ['./thx-img.component.scss']
})
export class ThxImgComponent {

  @Input() alt = '';
  @Input() src: string;

  @Input() title = '';
  @Input() fullImg = false;
  @Input() fullSize = false;
  @Input() style = {};


  constructor() {}

}
