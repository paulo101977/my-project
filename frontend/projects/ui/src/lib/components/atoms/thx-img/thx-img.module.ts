import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxImgComponent } from './thx-img.component';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';

@NgModule({
  imports: [
    CommonModule,
    ImgCdnModule
  ],
  declarations: [ ThxImgComponent ],
  exports: [ ThxImgComponent ]
})
export class ThxImgModule { }
