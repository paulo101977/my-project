import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-img',
  template: `
    <thx-img
      [src]="'input-error.svg'"
      [title]="'Test image title'"
      [alt]="'My alt image'">
    </thx-img>
  `,
  styles: []
})
export class DefaultComponent {

}
