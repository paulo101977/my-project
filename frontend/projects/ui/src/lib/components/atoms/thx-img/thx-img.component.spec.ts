import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxImgComponent } from './thx-img.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';
import { configureTestSuite } from 'ng-bullet';

describe('ThxImgComponent', () => {
  let component: ThxImgComponent;
  let fixture: ComponentFixture<ThxImgComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxImgComponent ],
      imports: [ ImgCdnModule ],
      schemas: [NO_ERRORS_SCHEMA]
    });
    fixture = TestBed.createComponent(ThxImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
