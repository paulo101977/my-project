import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { ThxDescriptionComponent } from '../thx-description.component';

import { DefaultComponent } from './default';


storiesOf('Ui | Atoms/ThxDescription', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule ],
    declarations: [ ThxDescriptionComponent ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
