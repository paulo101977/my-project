import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxDescriptionComponent } from './thx-description.component';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ ThxDescriptionComponent ],
  exports: [ ThxDescriptionComponent ]

})
export class ThxDescriptionModule { }
