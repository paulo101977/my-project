import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxDescriptionComponent } from './thx-description.component';

describe('ThxDescriptionComponent', () => {
  let component: ThxDescriptionComponent;
  let fixture: ComponentFixture<ThxDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
