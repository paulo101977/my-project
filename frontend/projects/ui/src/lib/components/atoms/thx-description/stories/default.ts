import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-description',
  template: `
    <thx-description>
      My custom p element
    </thx-description>
  `,
  styles: []
})
export class DefaultComponent {

}
