import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata, storiesOf } from '@storybook/angular';

import { Notes } from './notes';
import { ThxSwitchButtonComponent } from '../thx-switch-button.component';
import { DefaultComponent } from './default';
import { DisabledComponent } from './disabled';
import { OnComponent } from './on';

storiesOf('Ui | Atoms/ThxSwitchButton', module)
.addDecorator(
  moduleMetadata({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
    ],
    declarations: [ ThxSwitchButtonComponent ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes('default') }})
.add('Disabled', () => ({
  component: DisabledComponent
}),
    { notes: { markdown: new Notes().getNotes('disabled') }})
.add('On', () => ({
  component: OnComponent
}),
    { notes: { markdown: new Notes().getNotes('turnOn') }});
