export class Notes {
  private default = `
## Html:

~~~json
  <!-- switch-button usage: default -->
  <thx-switch-button></thx-switch-button>
~~~

## Typescript:

~~~typescript

~~~
  `;

  private disabled = `
## Html:

~~~json
  <!-- switch-button usage: disabled -->
  <thx-switch-button [disalbed]="true"></thx-switch-button>
~~~

## Typescript:

~~~typescript

~~~
  `;
  private on = `
## Html:

~~~json
  <!-- switch-button usage: on -->
  <thx-switch-button [turnOn]="true"></thx-switch-button>
~~~

## Typescript:

~~~typescript

~~~
  `;

  public getNotes(kind: string): string {
    switch (kind) {
      case 'default':
        return this.default;
      case 'disabled':
        return this.disabled;
      case 'on':
        return this.on;
      default:
        return this.default;
    }
  }
}
