import { componentFactory } from '.storybook/component-factory';

export const OnComponent = componentFactory('on', `
  <div style="width:500px; padding:30px; box-sizing: border-box;">
    <br>
    Case On:
    <br>
    <br>
    <thx-switch-button
      [(ngModel)]="scope.capivara"
      (ngModelChange)="scope.changed($event)"
    ></thx-switch-button>
    <br>
    value: {{ scope.capivara }}
  </div>
`, {capivara: true, changed: (event) => console.log('changed', event)});
