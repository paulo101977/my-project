import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ThxSwitchButtonComponent} from './thx-switch-button.component';

@NgModule({
  declarations: [ThxSwitchButtonComponent],
  imports: [CommonModule, FormsModule],
  exports: [ThxSwitchButtonComponent]
})
export class ThxSwitchButtonModule { }
