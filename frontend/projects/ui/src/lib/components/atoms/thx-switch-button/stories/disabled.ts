import { Component } from '@angular/core';

@Component({
    selector: 'thx-disabled-switch',
    template: `
<div style="width:500px; padding:30px; box-sizing: border-box;">
    <br>
    Disabled only:
    <thx-switch-button
        [disabled]="true"
        (change)="push($event)"></thx-switch-button>
    <br>
    Disabled and turn on:
    <thx-switch-button
        [disabled]="true"
        [(ngModel)]="model"
        (change)="push($event)"
    ></thx-switch-button>
    <br>
    Disabled events:
    <br>
    <div *ngFor="let event of arr">
        {{ event }}
    </div>
</div>
`
})
export class DisabledComponent {
    model = true;
    public arr = [];

    public push(event) {
        this.arr.push(event);
    }
}
