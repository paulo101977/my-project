import { FormControl, FormGroup } from '@angular/forms';
import { componentFactory } from '.storybook/component-factory';

export const DefaultComponent = componentFactory('default', `
  <div style="width:500px; padding:30px; box-sizing: border-box;" [formGroup]="scope.form">
    <br>
    Case Default:
    <br>
    <thx-switch-button formControlName="check"></thx-switch-button>
    <br>
    emitChanged: {{ scope.form.value | json }}
  </div>
`, {form: new FormGroup({check: new FormControl(false)})});
