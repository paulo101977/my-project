import { Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'thx-switch-button',
  templateUrl: './thx-switch-button.component.html',
  styleUrls: ['./thx-switch-button.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ThxSwitchButtonComponent),
      multi: true
    }
  ],
})
export class ThxSwitchButtonComponent implements ControlValueAccessor {

  public value: any;

  @Input() disabled;

  public getBackgroundColor(): string {
    return !this.disabled ? '#0c9abe' : '#9DA7A9';
  }

  onChange = (checked: boolean) => {};

  // Function to call when the input is touched (when a star is clicked).
  onTouched = () => {};

  valueChange(val: any) {
    this.writeValue(val);
  }

  writeValue(v: boolean): void {
    this.value = v;
    this.onChange(v);
  }

  registerOnChange(fn: (checked: boolean) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

}
