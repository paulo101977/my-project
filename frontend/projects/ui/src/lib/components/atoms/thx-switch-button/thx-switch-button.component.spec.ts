/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ThxSwitchButtonComponent } from './thx-switch-button.component';
import { configureTestSuite } from 'ng-bullet';

describe('ThxSwitchButtonComponent', () => {
  let component: ThxSwitchButtonComponent;
  let fixture: ComponentFixture<ThxSwitchButtonComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
       declarations: [ ThxSwitchButtonComponent ]
     });
     fixture = TestBed.createComponent(ThxSwitchButtonComponent);
     component = fixture.componentInstance;
     fixture.detectChanges();
   });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
