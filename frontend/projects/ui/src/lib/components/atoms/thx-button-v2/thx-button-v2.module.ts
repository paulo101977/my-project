import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxButtonV2Component } from './thx-button-v2.component';
import { ThxImgModule } from '../thx-img/thx-img.module';

@NgModule({
  imports: [CommonModule, ThxImgModule],
  declarations: [ ThxButtonV2Component ],
  exports: [ ThxButtonV2Component ]
})
export class ThxButtonV2Module { }
