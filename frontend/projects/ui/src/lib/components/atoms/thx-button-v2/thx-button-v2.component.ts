import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'thx-button-v2',
  templateUrl: './thx-button-v2.component.html',
  styleUrls: ['./thx-button-v2.component.scss']
})
export class ThxButtonV2Component {
  @Input() primary = false;
  @Input() outline = false;
  @Input() disabled = false;
  @Input() iconBefore: string;
  @Input() iconAfter: string;

  @Output() clicked = new EventEmitter();

  public getClasses() {
    return {
      '--primary': this.primary,
      '--disabled': this.disabled,
      '--outline': this.outline,
    };
  }

  public click() {
    if (this.disabled) {
      return;
    }

    this.clicked.emit();
  }
}
