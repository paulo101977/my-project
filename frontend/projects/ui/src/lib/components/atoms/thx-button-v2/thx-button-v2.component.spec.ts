import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ImgCdnPipeStub } from '@inovacao-cmnet/thx-ui';
import { configureTestSuite } from 'ng-bullet';
import { ThxButtonV2Component } from './thx-button-v2.component';


describe('ThxButtonV2Component', () => {
  let component: ThxButtonV2Component;
  let fixture: ComponentFixture<ThxButtonV2Component>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxButtonV2Component, ImgCdnPipeStub ]
    });

    fixture = TestBed.createComponent(ThxButtonV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#getClasses', () => {
    it('should return the css classes object', () => {
      component.primary = true;
      component.disabled = true;
      component.outline = true;

      expect(component.getClasses()).toEqual({
        '--primary': true,
        '--disabled': true,
        '--outline': true,
      });
    });
  });

  describe('#click', () => {
    beforeEach(() => {
      spyOn(component.clicked, 'emit');
    });

    it('should click when enabled', () => {
      component.disabled = false;
      component.click();
      expect(component.clicked.emit).toHaveBeenCalled();
    });

    it('should not click when disabled', () => {
      component.disabled = true;
      component.click();
      expect(component.clicked.emit).not.toHaveBeenCalled();
    });
  });
});
