import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxLabelComponent } from './thx-label.component';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ ThxLabelComponent ],
  exports: [ ThxLabelComponent ]
})
export class ThxLabelModule { }
