import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-label',
  template: `
    <thx-label [labelFor]="'name'">
      Label example
    </thx-label>
  `,
  styles: []
})
export class DefaultComponent {

}
