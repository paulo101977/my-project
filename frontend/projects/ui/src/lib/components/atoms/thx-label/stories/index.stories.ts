import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { ThxLabelComponent } from '../thx-label.component';

import { DefaultComponent } from './default';


import { componentFactory } from '.storybook/component-factory';

const CustomLabel = componentFactory('thex-scss-custom', `
    <thx-label [labelFor]="'name'" [ngClass]="'thex-tertiary thex-bg thex-focus thex-hover'">
      Label example
    </thx-label>
`);

storiesOf('Ui | Atoms/ThxLabel', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule ],
    declarations: [ ThxLabelComponent ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
{ notes: { markdown: new Notes().getNotes() }})
.add('Label with external main.scss', () => ({
        component: CustomLabel
    }),
    { notes: { markdown: new Notes().getNotes() }});
