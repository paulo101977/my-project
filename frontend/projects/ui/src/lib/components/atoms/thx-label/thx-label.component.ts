import { Component , Input } from '@angular/core';

@Component({
  selector: 'thx-label',
  template: `
    <label [attr.for]="labelFor">
      <ng-content></ng-content>
    </label>
  `
})
export class ThxLabelComponent {

    @Input() labelFor: string;
}
