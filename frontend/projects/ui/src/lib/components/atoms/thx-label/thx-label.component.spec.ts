import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxLabelComponent } from './thx-label.component';

describe('ThxLabelComponent', () => {
  let component: ThxLabelComponent;
  let fixture: ComponentFixture<ThxLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
