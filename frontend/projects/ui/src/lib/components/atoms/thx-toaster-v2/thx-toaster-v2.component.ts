import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { ThxToasterService } from '../thx-toaster/services/thx-toaster.service';
import { ThxToasterTypeV2Enum } from './thx-toaster-type-v2.enum';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'thx-toaster-v2',
  templateUrl: './thx-toaster-v2.component.html',
  styleUrls: ['./thx-toaster-v2.component.scss'],
  animations: [
    trigger(
      'fadeOut',
      [
        transition(':enter', [
          style({transform: 'translateY(-100%)'}),
          animate('500ms ease-out', style({transform: 'translateY(0%)'}))
        ]),
        transition(':leave', [
          animate('500ms ease-in', style({transform: 'translateY(-100%)'}))
        ]),
      ]
    )
  ]
})

export class ThxToasterV2Component implements OnInit {

  @Output() close = new EventEmitter<boolean>();
  @Output() closeError = new EventEmitter<boolean>();
  @Output() closeInformation = new EventEmitter<boolean>();

  type: ThxToasterTypeV2Enum;
  message: string;
  isVisible = false;
  progressValue = 0;

  constructor(
    private thxToasterService: ThxToasterService
  ) { }

  ngOnInit(): void {
    this.thxToasterService.changeEmitted$.subscribe((response) => {
      if (Array.isArray(response)) {
        this.deprecatedShowToaster(response);
      } else {
        const { type, message, visibilityTime } = response;
        this.showToaster(type, message, visibilityTime);
      }
    });
  }

  public showToaster(type: ThxToasterTypeV2Enum, message: string, visibilityTime = 1000) {
    this.isVisible = true;
    this.type = type;
    this.message = message;

    this.startTimer(visibilityTime);
  }

  public startTimer(visibilityTime = 1000) {
    timer(visibilityTime, 30).pipe(
      take(visibilityTime / 10 + 1)
    ).subscribe({
      next: () => this.progressValue += 1,
      error: () => {},
      complete: () => this.closeToaster()
    });
  }

  public closeToaster() {
    if (this.isVisible) {
      this.progressValue = 0;
      this.type = null;
      this.message = null;
      this.isVisible = false;
      this.emitCloseType();
    }
  }

  public getClass( type: ThxToasterTypeV2Enum) {
    const {
      ERROR, INFORMATION, SUCCESS,
    } = ThxToasterTypeV2Enum;
    switch (type) {
      case ERROR:
        return this.getClassObject(ERROR);
      case INFORMATION:
        return this.getClassObject(INFORMATION);
      case SUCCESS:
        return this.getClassObject(SUCCESS);
      default:
        return this.getClassObject(SUCCESS);
    }
  }

  private deprecatedShowToaster(response) {
    const [isSuccess, message] = response;
    const type = !!isSuccess
      ? ThxToasterTypeV2Enum.SUCCESS
      : ThxToasterTypeV2Enum.ERROR;

    this.showToaster(type, message);
  }

  private getClassObject(type: ThxToasterTypeV2Enum) {
    return {
      bg: `thex-${type} thex-bg`,
      border: `thex-box-${type} thex-bg`,
      icon: this.getIcon(type),
    };
  }

  private getIcon(type: ThxToasterTypeV2Enum) {
    if (type === ThxToasterTypeV2Enum.SUCCESS) {
      return 'icon-ok';
    }

    return 'icon-exclamation';
  }

  private emitCloseType() {
    const {
      ERROR, INFORMATION, SUCCESS,
    } = ThxToasterTypeV2Enum;
    switch (this.type) {
      case ERROR:
        return this.closeError.emit(true);
      case INFORMATION:
        return this.closeInformation.emit(true);
      case SUCCESS:
        return this.close.emit(true);
      default:
        return this.close.emit(true);
    }
  }
}
