import { componentFactory } from '.storybook/component-factory';
import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ThxToasterV2Component } from '../thx-toaster-v2.component';
import { Notes } from './notes';
import { ThxToasterTypeV2Enum } from '../thx-toaster-type-v2.enum';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const messageSucess = 'Conteúdo da mensagem de sucesso';
const messageError = 'Conteúdo da mensagem de erro';
const messageInfo = 'Conteúdo da mensagem de alerta';

const SucessToasterComponent = componentFactory('success', `

    <thx-toaster-v2
      [message] = "scope.messageSucess"
      [type] ="scope.type"
    >
    </thx-toaster-v2>

  `, {messageSucess, type: ThxToasterTypeV2Enum.SUCCESS});


const ErrorToasterComponent = componentFactory('error', `
  <thx-toaster-v2
    [message] = "scope.messageError"
    [type] ="scope.type"
  >
  </thx-toaster-v2>

`, {messageError, type: ThxToasterTypeV2Enum.ERROR});

const InformationToasterComponent = componentFactory('information', `
  <thx-toaster-v2
    [message] = "scope.messageInfo"
    [type] ="scope.type"
  >
  </thx-toaster-v2>

`, {messageInfo, type: ThxToasterTypeV2Enum.INFORMATION});

storiesOf('Ui | Atoms/ThxToaster-V2', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule, BrowserAnimationsModule ],
    declarations: [ ThxToasterV2Component ]
  })
)
.add(
    'Sucess Toaster',
    () => ({
      component: SucessToasterComponent
    }),
    { notes: { markdown: new Notes().getNotes() }}
)
.add(
  'Error Toaster',
  () => ({
    component: ErrorToasterComponent
  }),
  { notes: { markdown: new Notes().getNotes() }}
)
.add(
  'Information Toaster',
  () => ({
    component: InformationToasterComponent
  }),
  { notes: { markdown: new Notes().getNotes() }}
);


