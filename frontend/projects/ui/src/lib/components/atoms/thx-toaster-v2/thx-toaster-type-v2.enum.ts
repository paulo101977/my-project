export enum ThxToasterTypeV2Enum {
    SUCCESS = 'success',
    INFORMATION = 'complementary',
    ERROR = 'error',
}
