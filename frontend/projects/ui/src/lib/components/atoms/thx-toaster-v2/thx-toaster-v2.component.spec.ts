import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxToasterV2Component } from './thx-toaster-v2.component';

describe('ThxToasterV2Component', () => {
  let component: ThxToasterV2Component;
  let fixture: ComponentFixture<ThxToasterV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxToasterV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThxToasterV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
