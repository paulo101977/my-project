export class Notes {
    private notes = `
## Html:

~~~json
<!-- main div container -->
<div
  *ngIf="isVisible"
  class="toaster-container"
  [ngClass] ="getClass(type)?.bg"
  >
  <!-- progress bar -->
  <div class="toaster-progression"
    [ngClass] ="getClass(type)?.border"
    [ngStyle]="{'width': progressValue + '%'}"
  ></div>
  <!-- icon container -->
  <div class="toaster-component">
    <div class="toaster-dynamic-message">
      <span class="toaster-icon"
      [ngClass] ="getClass(type)?.icon"
      ></span>
      <span class="toaster-message-text">
        {{message}}
      </span>
    </div>
  </div>
  <!-- close button -->
  <div class="toaster-btnclose">
    <span class="icon-close"
      (click)="toggleToaster()"></span>
  </div>
</div>

~~~
## Typescript:

~~~typescript
    public message = 'Conteúdo da mensagem de sucesso';
~~~
    `;

    public getNotes(): string {
        return this.notes;
    }
}
