import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ThxToasterV2Component } from './thx-toaster-v2.component';

@NgModule({
  declarations: [ThxToasterV2Component],
  imports: [
    CommonModule,
    TranslateModule
  ],
  exports: [ThxToasterV2Component]
})
export class ThxToasterV2Module { }
