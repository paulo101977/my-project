import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThxImgInfoComponent } from './thx-img-info.component';
import { ThxImgModule } from '../thx-img/thx-img.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { configureTestSuite } from 'ng-bullet';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';

describe('ThxImgInfoComponent', () => {
  let component: ThxImgInfoComponent;
  let fixture: ComponentFixture<ThxImgInfoComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxImgInfoComponent ],
      imports: [
        ThxImgModule,
        TranslateTestingModule
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    });
    fixture = TestBed.createComponent(ThxImgInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
