import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImgCdnModule } from '../../../pipes/img-cdn/img-cdn.module';
import { ThxImgInfoComponent } from './thx-img-info.component';
import { ThxImgModule } from '../thx-img/thx-img.module';
import { TranslateModule } from '@ngx-translate/core';
import { ThxButtonV2Module } from '../thx-button-v2/thx-button-v2.module';

@NgModule({
  imports: [
    CommonModule,
    ImgCdnModule,
    ThxImgModule,
    TranslateModule,
    ThxButtonV2Module
  ],
  declarations: [ ThxImgInfoComponent ],
  exports: [ ThxImgInfoComponent ]
})
export class ThxImgInfoModule { }
