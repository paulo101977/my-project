import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ImgInfoEnum } from './models/img-info.enum';

@Component({
  selector: 'thx-img-info',
  templateUrl: './thx-img-info.component.html',
  styleUrls: ['./thx-img-info.component.scss']
})
export class ThxImgInfoComponent implements OnInit {

  /* IMG */
  @Input() closedOption = true;
  @Input() imgInfo: string;
  @Input() titleInfo: string;
  @Input() textInfo: string;
  @Input() newFlag = false;
  @Input() textClasses: ImgInfoComponentClasses;
  @Input() type: ImgInfoEnum = ImgInfoEnum.info;

  /* BUTTON */
  @Input() buttonOption = false;
  @Input() colorClass = false;
  @Input() iconAfter: string;
  @Input() labelButton: string;
  @Output() isClicked = new EventEmitter();

  public closedInfo: boolean;

  constructor() { }

  ngOnInit() {
    this.closedInfo = true;
  }

  public validateCss() {
   return ImgInfoEnum[this.type] == null || this.type == ImgInfoEnum.info;
  }

  public wasClicked() {
    this.isClicked.emit();
  }
}

export interface ImgInfoComponentClasses  {
  titleClass?: string;
  bodyClass?: string;
}
