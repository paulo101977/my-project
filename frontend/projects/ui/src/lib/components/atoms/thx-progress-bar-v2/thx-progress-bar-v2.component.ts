import { Component, Input } from '@angular/core';

@Component({
  selector: 'thx-progress-bar-v2',
  templateUrl: './thx-progress-bar-v2.component.html',
  styleUrls: ['./thx-progress-bar-v2.component.scss']
})
export class ThxProgressBarV2Component {

  private _percentage: number;

  @Input()
  set percentage(value: number) {
    this._percentage = this.validateRange(value);
  }

  get percentage(): number {
    return this._percentage;
  }

  validateRange(value: number): number {
    if (value >= 0 && value <= 100) {
      return value;
    }
    console.warn('Percentage incorrect, number must be from 0 to 100');
    return 0;
  }

}
