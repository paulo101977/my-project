/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'app/shared/mock-test/translate-testing.module';
import { configureTestSuite } from 'ng-bullet';

import { ThxProgressBarV2Component } from './thx-progress-bar-v2.component';

describe('ThxProgressBarV2Component', () => {
  let component: ThxProgressBarV2Component;
  let fixture: ComponentFixture<ThxProgressBarV2Component>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [ TranslateTestingModule ],
      declarations: [ ThxProgressBarV2Component ]
    });

    fixture = TestBed.createComponent(ThxProgressBarV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
