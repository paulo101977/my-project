import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxProgressBarV2Component } from './thx-progress-bar-v2.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [ThxProgressBarV2Component],
  imports: [CommonModule, TranslateModule],
  exports: [ThxProgressBarV2Component]
})
export class ThxProgressBarV2Module { }
