import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { componentFactory } from '.storybook/component-factory';
import { Notes } from './notes';
import { ThxProgressBarV2Module } from '../thx-progress-bar-v2.module';

const DefaultComponent = componentFactory('default', `
  <div style="width:500px; padding:30px; box-sizing: border-box;">
    <thx-progress-bar-v2 percentage="90"></thx-progress-bar-v2>
  </div>
`);

storiesOf('Ui | Atoms/ThxProgressBar', module)
.addDecorator(
  moduleMetadata({
    imports: [
      CommonModule,
      ThxProgressBarV2Module,
    ],
    declarations: [ DefaultComponent ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
