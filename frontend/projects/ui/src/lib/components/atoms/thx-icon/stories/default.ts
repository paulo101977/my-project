import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-icon',
  template: `
    <thx-icon></thx-icon>
  `,
  styles: []
})
export class DefaultComponent {

}
