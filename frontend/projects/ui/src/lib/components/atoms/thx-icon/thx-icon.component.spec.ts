import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';

import { ThxIconComponent } from './thx-icon.component';

describe('ThxIconComponent', () => {
  let component: ThxIconComponent;
  let fixture: ComponentFixture<ThxIconComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ThxIconComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    fixture = TestBed.createComponent(ThxIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
