import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThxIconComponent } from './thx-icon.component';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ ThxIconComponent ],
  exports: [ ThxIconComponent ]
})
export class ThxIconModule { }
