import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Notes } from './notes';

import { ThxIconComponent } from '../thx-icon.component';

import { DefaultComponent } from './default';
import { LockComponent } from './lock';

storiesOf('Ui | Atoms/ThxIcon', module)
.addDecorator(
  moduleMetadata({
    imports: [ CommonModule ],
    declarations: [ ThxIconComponent ]
  })
)
.add('Default', () => ({
  component: DefaultComponent
}),
    { notes: { markdown: new Notes().getNotes() }})
.add('Lock icon', () => ({
  component: LockComponent
}),
    { notes: { markdown: new Notes().getNotes() }});
