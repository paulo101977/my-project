import { Component  } from '@angular/core';

@Component({
  selector: 'thx-default-icon',
  template: `
    <thx-icon [type]="'mdi-lock-outline'"></thx-icon>
  `,
  styles: []
})
export class LockComponent {

}
