import { Component, Input } from '@angular/core';
import { ThxIconColor } from './enums/thx-icon-color.enum';

@Component({
  selector: 'thx-icon',
  template: `
    <i
      class="thx-icon mdi"
      [ngClass]="[type, color]">
    </i>
  `,
  styleUrls: ['./thx-icon.component.scss']
})
export class ThxIconComponent {

  @Input() type = '';
  @Input() color: ThxIconColor = ThxIconColor.Primary;
}
