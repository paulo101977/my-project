import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'acronym'
})
export class AcronymPipe implements PipeTransform {

  transform(sentence: string, length = 0): any {
    const words = sentence.split(' ');
    const firstLetters = words.map(word => word[0]);
    const delimiter = length ? length : firstLetters.length;

    return firstLetters.slice(0, delimiter).join('');
  }

}
