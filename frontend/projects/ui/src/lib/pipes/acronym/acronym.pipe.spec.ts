import { AcronymPipe } from './acronym.pipe';

describe('AcronymPipe', () => {
  it('create an instance', () => {
    const pipe = new AcronymPipe();
    expect(pipe).toBeTruthy();
  });

  describe('#transform', () => {
    it('should return the sentence as an acronym', () => {
      const pipe = new AcronymPipe();
      const acronym = pipe.transform('totvs otvs tvs vs s');
      expect(acronym).toBe('totvs');
    });

    it('should return an acronym limited by length', () => {
      const pipe = new AcronymPipe();
      const acronym = pipe.transform('totvs otvs tvs vs s', 2);
      expect(acronym).toBe('to');
    });
  });
});
