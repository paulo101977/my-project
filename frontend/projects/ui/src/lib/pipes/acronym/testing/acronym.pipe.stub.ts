import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'acronym'
})
export class AcronymPipeStub implements PipeTransform {

  transform(sentence: string, length = 0): any {
    return sentence;
  }

}
