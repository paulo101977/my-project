import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcronymPipeStub } from './acronym.pipe.stub';

@NgModule({
  declarations: [AcronymPipeStub],
  imports: [
    CommonModule
  ],
  exports: [AcronymPipeStub]
})
export class AcronymTestingModule { }
