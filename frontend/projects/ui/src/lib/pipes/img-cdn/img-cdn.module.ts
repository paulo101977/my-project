import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImgCdnPipe } from './img-cdn.pipe';
import { AssetsServiceModule } from '../../services/assets-service/assets-service.module';

@NgModule({
  imports: [
    CommonModule,
    AssetsServiceModule
  ],
  declarations: [ImgCdnPipe],
  exports: [ImgCdnPipe]
})
export class ImgCdnModule { }
