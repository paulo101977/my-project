import {NgModule} from '@angular/core';
import {ImgCdnPipeStub} from './img-cdn.pipe.stub';

@NgModule({
  declarations: [ ImgCdnPipeStub ],
  exports: [ ImgCdnPipeStub ]
})
export class ImgCdnTestingModule { }
