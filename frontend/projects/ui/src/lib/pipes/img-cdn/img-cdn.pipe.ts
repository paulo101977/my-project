import { Pipe, PipeTransform } from '@angular/core';
import { AssetsCdnService } from '../../services/assets-service/assets-cdn.service';

@Pipe({
  name: 'imgCdn'
})
export class ImgCdnPipe implements PipeTransform {

  constructor(private assetsService: AssetsCdnService) {}
  transform(value: any, args?: any): any {
    return this.assetsService.getImgUrlTo(value);
  }

}
