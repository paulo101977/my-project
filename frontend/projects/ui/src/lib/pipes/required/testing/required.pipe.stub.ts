import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'required'
})
export class RequiredPipeStub implements PipeTransform {

  transform(value: any, args?: any): any {
    return null;
  }

}
