import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequiredPipeStub } from './required.pipe.stub';

@NgModule({
  declarations: [RequiredPipeStub],
  imports: [
    CommonModule
  ],
  exports: [RequiredPipeStub]
})
export class RequiredTestingModule { }
