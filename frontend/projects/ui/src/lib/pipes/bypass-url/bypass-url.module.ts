import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BypassUrlPipe } from './bypass-url.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [BypassUrlPipe],
  exports: [BypassUrlPipe]
})
export class BypassUrlModule { }
