import { BypassUrlModule } from './bypass-url.module';

describe('BypassUrlModule', () => {
  let safeUrlModule: BypassUrlModule;

  beforeEach(() => {
    safeUrlModule = new BypassUrlModule();
  });

  it('should create an instance', () => {
    expect(safeUrlModule).toBeTruthy();
  });
});
