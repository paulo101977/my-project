import {NgModule} from '@angular/core';
import {BypassUrlPipeStub} from './bypass-url.pipe.stub';

@NgModule({
  declarations: [ BypassUrlPipeStub ],
  exports: [ BypassUrlPipeStub ]
})
export class BypassUrlTestingModule { }
