import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'bypassUrl'})
export class BypassUrlPipeStub implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    return value;
  }
}
