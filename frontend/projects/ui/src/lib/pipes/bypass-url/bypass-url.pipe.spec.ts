import { TestBed } from '@angular/core/testing';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { configureTestSuite } from 'ng-bullet';
import { BypassUrlPipe } from './bypass-url.pipe';

describe('BypassUrlPipe', () => {
  let sanitizer: DomSanitizer;
  let pipe: BypassUrlPipe;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule
      ]
    });

    sanitizer = TestBed.get(DomSanitizer);
    pipe = new BypassUrlPipe(sanitizer);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should ', function () {
    const url = 'test';
    spyOn(sanitizer, 'bypassSecurityTrustResourceUrl').and.returnValue(url);

    const response = pipe.transform('url');
    expect(response).toBe(url);
  });
});
