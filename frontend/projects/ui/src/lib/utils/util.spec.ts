
import {
  callFunction, convertDateToISOExtended, convertIsoToDate, convertToBoolean, convertToInt, getFormattedLink, isEquals,
  isExternalLink, isTypeof, openExternalLink, removeDuplicatedOptions, removeUndefinedAndNullOptions,
  sortOptionsByProperty, validValue
} from './util';

describe('Function convertToBoolean', () => {

  it('should be true', () => {
    expect(convertToBoolean('true')).toBe(true);
  });

  it('should be false', () => {
    expect(convertToBoolean('false')).toBe(false);
  });

  it('should be false because is a wrong string', () => {
    expect(convertToBoolean('teste')).toBe(false);
  });

  it('should be true because is a boolean', () => {
    expect(convertToBoolean(true)).toBe(true);
  });
});

describe('Function isTypeof', () => {

  it('should be true for this string', () => {
    expect(isTypeof('test', 'string')).toBe(true);
  });

  it('should be true for this object', () => {
    expect(isTypeof(new Date(), 'object')).toBe(true);
  });

});

describe('Function convertIsoToDate', () => {

  it('should be a object Date', () => {
    const date = new Date(2017, 1, 1).toISOString();
    expect(typeof convertIsoToDate(date, false, false)).toBe('object');
  });

  it('should be a object Date with start', () => {
    const date = new Date(2017, 1, 1);
    expect(typeof convertIsoToDate(date.toISOString(), true, false)).toBe('object');
  });

  it('should be a object Date with end', () => {
    const date = new Date(2017, 1, 1).toISOString();
    expect(typeof convertIsoToDate(date, false, true)).toBe('object');
  });

  it('should be a date with zero hour with dateStart', () => {
    const date = new Date(2017, 1, 1).toISOString();
    const dateStart = convertIsoToDate(date, true, false);

    expect(dateStart.getHours()).toBe(0);
    expect(dateStart.getMinutes()).toBe(0);
    expect(dateStart.getSeconds()).toBe(0);
  });

  it('should be a date with 23:59:59 hour with dateEnd', () => {
    const date = new Date(2017, 1, 1).toISOString();
    const dateStart = convertIsoToDate(date, false, true);

    expect(dateStart.getHours()).toBe(23);
    expect(dateStart.getMinutes()).toBe(59);
    expect(dateStart.getSeconds()).toBe(59);
  });

  it('should be a no value', () => {
    const date = '';
    expect(typeof convertIsoToDate(date, false, false)).toBe('undefined');
  });

  it('should add up to 1 hour if it is not reset', () => {
    const date = new Date(2017, 1, 1, 2).toISOString();
    expect(typeof convertIsoToDate(date, false, false)).toBe('object');
  });
});

describe('Function callFunction', () => {
  const context = {
    getName: function() {
      return 'THF';
    },
    'getAge': function() {
      return '2';
    }
  };

  it('should call function of context', () => {

    spyOn(context, 'getName');

    callFunction(context.getName, context);

    expect(context.getName).toHaveBeenCalled();
  });

  it('should call function of context passing attribute name', () => {
    spyOn(context, 'getAge');

    callFunction('getAge', context);

    expect(context.getAge).toHaveBeenCalled();
  });

  it('should sort with A > B', () => {
    const _options: Array<any> = [
      { name: 'Edward', value: 21 },
      { name: 'Sharpe', value: 37 }
    ];

    sortOptionsByProperty(_options, 'name');

    expect(_options).toEqual([
      { name: 'Edward', value: 21 },
      { name: 'Sharpe', value: 37 }
    ]);

  });

  it('should sort with B > A', () => {
    const _options: Array<any> = [
      { name: 'Sharpe', value: 37 },
      { name: 'Edward', value: 21 }
    ];

    sortOptionsByProperty(_options, 'name');

    expect(_options).toEqual([
      { name: 'Edward', value: 21 },
      { name: 'Sharpe', value: 37 }
    ]);

  });

  it('should sort to equals', () => {
    const _options: Array<any> = [
      { name: 'Sharpe', value: 37 },
      { name: 'Sharpe', value: 37 }
    ];

    sortOptionsByProperty(_options, 'name');

    expect(_options).toEqual([
      { name: 'Sharpe', value: 37 },
      { name: 'Sharpe', value: 37 }
    ]);

  });
});

describe('Function convertDateToISOExtended', () => {

  it('should null for no value for function', () => {
    const date: Date = null;
    expect(convertDateToISOExtended(date, '12')).toBe(null);
  });

  it('should return a ISO extended when pass Date with no hour', () => {
    const date = new Date(2017, 10, 28);
    const dateTemp = convertDateToISOExtended(date, null).substring(0, 19);
    expect(dateTemp).toBe('2017-11-28T00:00:00');
  });

  it('should return a ISO extended when pass Date with hour', () => {
    const date = new Date(2017, 10, 28);
    expect(convertDateToISOExtended(date, 'T00:50:20')).toBe('2017-11-28T00:50:20');
  });

  it('should return a ISO extended when pass Date with hour when day and month less then 10', () => {
    const date = new Date(2017, 5, 5);
    expect(convertDateToISOExtended(date, 'T00:50:20')).toBe('2017-06-05T00:50:20');
  });

});

describe('Function removeDuplicatedOptions', () => {
  it('should return items not duplicated ', () => {
    const options = [
      { value: 1, label: 1 },
      { value: 2, label: 2 },
      { value: 2, label: 2 },
      { value: 1, label: 1 },
      { value: 3, label: 3 }
    ];
    removeDuplicatedOptions(options);
    expect(options.length).toBe(3);
  });
});

describe('Function removeUndefinedAndNullOptions', () => {
  it('should return items not undefined and null ', () => {
    const options = [
      { value: 1, label: 1 },
      { value: 2, label: 2 },
      { value: <any>undefined, label: 'teste' },
      { value: null, label: 'teste2' },
      { value: 3, label: 3 }
    ];
    removeUndefinedAndNullOptions(options);
    expect(options.length).toBe(3);
  });
});

describe('Function validValueToOption', () => {
  it('should return false to invalid value', () => {
    expect(validValue('')).toBeFalsy();
    expect(validValue(null)).toBeFalsy();
    expect(validValue(undefined)).toBeFalsy();
  });

  it('should return true to valid value', () => {
    expect(validValue(0)).toBeTruthy();
    expect(validValue('a')).toBeTruthy();
    expect(validValue(1)).toBeTruthy();
    expect(validValue(true)).toBeTruthy();
    expect(validValue(false)).toBeTruthy();
  });
});

describe('Function isExternalLink', () => {
  it('should return true if is external link', () => {
    expect(isExternalLink('http://google.com.br')).toBe(true);
  });
  it('should return false if is internal link', () => {
    expect(isExternalLink('./home')).toBe(false);
  });
});

describe('Function openExternalLink', () => {
  it('should open external link', () => {
    spyOn(window, 'open');
    openExternalLink('http://google.com.br');
    expect(window.open).toHaveBeenCalledWith('http://google.com.br', '_blank');
  });
});

describe('Function getFormattedLink', () => {
  it('should format link', () => {

    expect(getFormattedLink(null)).toBe('/');

    expect(getFormattedLink('../link')).toBe('/link');

    expect(getFormattedLink('./link')).toBe('/link');

    expect(getFormattedLink('link')).toBe('/link');

    expect(getFormattedLink('/link')).toBe('/link');

    expect(getFormattedLink('/link/./test')).toBe('/link/./test');

    expect(getFormattedLink('/link/../otherTest')).toBe('/link/../otherTest');

    expect(getFormattedLink('/link./../otherTest')).toBe('/link./../otherTest');

    expect(getFormattedLink('.link')).toBe('/link');

    expect(getFormattedLink('.link.....')).toBe('/link.....');

    expect(getFormattedLink('/link/._./_.otherTest')).toBe('/link/._./_.otherTest');

    expect(getFormattedLink('.../link/../otherTest')).toBe('/link/../otherTest');

    expect(getFormattedLink('.../link/../.otherTest')).toBe('/link/../.otherTest');
  });
});

describe('Function convertToInt:', () => {

  it('should return a number when have a string number value param.', () => {
    expect(convertToInt('10.50')).toBe(10);
  });

  it('should return a number when have a decimal number value param.', () => {
    expect(convertToInt(10.5)).toBe(10);
  });

  it('should return undefined when have a null value param.', () => {
    expect(convertToInt(null)).toBeUndefined();
  });

  it('should return a undefined when have a string value param.', () => {
    expect(convertToInt('teste')).toBeUndefined();
  });

  it('should return a 0 when have a number 0 value param.', () => {
    expect(convertToInt(0)).toBe(0);
  });

  it('should return a 0 when have an invalid number value param and have a 0 default param.', () => {
    expect(convertToInt('invalidNumber', 0)).toBe(0);
  });

  it('should return a default value when have an invalid number value param and have a valid default param.', () => {
    expect(convertToInt('invalidNumber', 5)).toBe(5);
  });

  it('should return undefined when have an invalid number and have an invalid default param.', () => {
    expect(convertToInt('invalidNumber', 'tinvalidParam')).toBeUndefined();
  });

});

describe('Function isEquals: ', () => {

  it('should return true with same value', () => {
    expect(isEquals(1, 1)).toBeTruthy();
    expect(isEquals({value: '21'}, {value: '21'})).toBeTruthy();
  });

  it('should return false when different value', () => {
    expect(isEquals(2, 1)).toBeFalsy();
    expect(isEquals({value: '1'}, {value: '21'})).toBeFalsy();
  });

});
