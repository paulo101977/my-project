export enum DocumentTypeNaturalEnum {
  CPF = 1,
  DNI = 3,
  CI = 5,
  RUT = 7,
  RUC = 9,
  NIT = 11,
  PASSPORT = 15,
  RG = 16,
  NIF = 17,
}

export enum DocumentTypeLegalEnum {
  CNPJ = 2,
  CUIT = 4,
  RUT = 6,
  CI = 10,
  NIT = 12,
  STATEREGISTRATION = 13,
  MUNICIPALREGISTRATION = 14,
  NIF = 21,
}
