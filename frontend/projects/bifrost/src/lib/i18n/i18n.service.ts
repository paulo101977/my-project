import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';

@Injectable()
export class I18nService {

  private readonly STORAGE_NAME = 'i18n';
  private readonly DEFAULT_LANGUAGE = 'en-US';

  constructor(
    private translate: TranslateService
  ) {}

  public setInitialLanguage(): void {
    const language = this.getLanguage() || navigator.language;
    this.setLanguage(language);
  }

  private checkLanguageInAssets(language: string): Observable<string> {
    return this.translate.use(language);
  }

  public getLanguage() {
    return localStorage.getItem(this.STORAGE_NAME);
  }

  public setLanguage(newLanguage: string) {
    const setLanguage = (language) => {
      this.translate.setDefaultLang(language);
      moment.locale(language);
      localStorage.setItem(this.STORAGE_NAME, language);
    };

    const onSuccess = () => setLanguage(newLanguage);
    const onError = () => setLanguage(this.DEFAULT_LANGUAGE);

    this.checkLanguageInAssets(newLanguage).subscribe(onSuccess, onError);
  }

  public getCountry(language: string): string {
    const [, country] = language.split('-');

    return country || '';
  }
}
