import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';

import { I18nService } from './i18n.service';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

describe('I18nService', () => {
  let service: I18nService;
  let translateService: TranslateService;

  const translateStubService: Partial<TranslateService> = {
    setDefaultLang(lang: string): void {},
    use(lang: string): Observable<any> { return of(null); }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        I18nService,
        { provide: TranslateService, useValue: translateStubService }
      ]
    });

    service = TestBed.get(I18nService);
    translateService = TestBed.get(TranslateService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set the application language', () => {
    const userData = {
      preferredLanguage: 'pt-BR'
    };
    spyOn(translateService, 'setDefaultLang').and.callThrough();
    spyOn(translateService, 'use').and.callThrough();

    service.setLanguage(userData.preferredLanguage);

    expect(translateService.setDefaultLang).toHaveBeenCalledWith(userData.preferredLanguage);
    expect(translateService.use).toHaveBeenCalledWith(userData.preferredLanguage);
    expect(service.getLanguage()).toBe(userData.preferredLanguage);
  });

  it('should extract the country from an ISO string', () => {
    expect(service.getCountry('en-us')).toEqual('us');
  });

});
