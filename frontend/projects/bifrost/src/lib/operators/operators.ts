import { from, iif, Observable, of, OperatorFunction, pipe, UnaryFunction } from 'rxjs';
import { map, mergeMap, switchMap, toArray } from 'rxjs/operators';

export function extractField<R>(field: string): OperatorFunction<string, R> {
  return map((response) => response[field]);
}

export function transduceList<R, T>(operator: OperatorFunction<R, T>): OperatorFunction<R[], T[]> {
  return pipe(
    switchMap((list: R[]) => from(list)),
    operator,
    toArray()
  );
}

export function toOptions<R>(key: string, label: string): OperatorFunction<R, {key: string, label: string}> {
  return map((item: R) => ({
    key: item[key],
    label: item[label]
  }));
}

export function swapKeys<R extends object, T>(originalKeys: string[], desiredKeys: string[]): OperatorFunction<R, T> {
  return map((item: R) => {
    const newItem: any = { ...(item as object) };
    desiredKeys.map((key, index) => {
      const originalKey = originalKeys[index];
      delete newItem[originalKey];
      newItem[key] = item[originalKey];
    });
    return newItem as T;
  });
}

export function toThfOptions(label: string, value: string) {
  return pipe(
    toOptions(value, label),
    swapKeys(['key'], ['value'])
  );
}

export function filterBy(predicative: (value: any, index?: number, arr?: any[]) => {}) {
  return map((response: any[]) => {
    return response.filter(predicative);
  });
}

export function mergeNestedLists() {
  return map((nestedList: any[][]) => {
    return nestedList.reduce((mergedList, list) => {
      mergedList.push(...list);
      return mergedList;
    }, []);
  });
}

export function selectItem(predicative): OperatorFunction<any, any> {
  return map((item) => {
    return predicative(item) ? { ...item, $selected: true } : { ...item };
  });
}

export function destructureList<T, R>(operator): UnaryFunction<Observable<T[]>, Observable<R[]>> {
  return pipe(
    mergeMap((list: any[]) => from(list)),
    operator,
    toArray()
  );
}

export function selectListItems<T, R>(predicative): UnaryFunction<Observable<T[]>, Observable<R[]>> {
  const operator = selectItem(predicative);
  return destructureList(operator);
}

export function cleanList(): OperatorFunction<any[], any[]> {
  return map((items: any[]) => {
    return items.filter(item => item != null);
  });
}

export function mergeIf<T>(condition, trueFunc = of, falseFunc = of): OperatorFunction<T, T> {
  return mergeMap((value: T): Observable<T> => {
    return iif(condition, trueFunc(value), falseFunc(value));
  });
}
