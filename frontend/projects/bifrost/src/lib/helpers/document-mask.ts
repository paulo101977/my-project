import { DocumentTypeLegalEnum, DocumentTypeNaturalEnum } from '../enums/document-type-enum';

export function getDocumentMask(documentTypeId: DocumentTypeLegalEnum | DocumentTypeNaturalEnum): any {
  switch (documentTypeId) {
    case DocumentTypeNaturalEnum.CPF:
      return '999.999.999-99';
    case DocumentTypeNaturalEnum.DNI:
      return null;
    case DocumentTypeNaturalEnum.CI:
      return null;
    case DocumentTypeNaturalEnum.RUT:
      return null;
    case DocumentTypeNaturalEnum.RUC:
      return null;
    case DocumentTypeNaturalEnum.NIT:
      return null;
    case DocumentTypeNaturalEnum.PASSPORT:
      return null;
    case DocumentTypeNaturalEnum.RG:
      return '99.999.999-9';
    case DocumentTypeLegalEnum.CNPJ:
      return '99.999.999/9999-99';
  }
}
