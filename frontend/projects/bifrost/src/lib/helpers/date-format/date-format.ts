import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({ providedIn: 'root' })
export class DateFormatService {

  public static DATE_FORMAT_UNIVERSAL = 'YYYY-MM-DDTHH:mm:ss';

  public convertDateToStringWithFormat(date: Date, format: string) {
    if (typeof date === 'object') {
      return moment(date).format(format);
    }
    return null;
  }

  public convertUniversalDateToIMyDate(universalDate: string) {
    const momentDate = moment(universalDate, DateFormatService.DATE_FORMAT_UNIVERSAL).toDate();
    let dateValue;
    if (momentDate) {
      dateValue = {
        date: {
          day: momentDate.getDate(),
          month: momentDate.getMonth() + 1,
          year: momentDate.getFullYear(),
        },
      };
      return dateValue;
    }
    return null;
  }

}
