import { TestBed } from '@angular/core/testing';

import { TranslateBarcodeService } from './translate-barcode.service';

describe('TranslateBarcodeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TranslateBarcodeService = TestBed.get(TranslateBarcodeService);
    expect(service).toBeTruthy();
  });
});
