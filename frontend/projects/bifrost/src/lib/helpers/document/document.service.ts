import { Injectable } from '@angular/core';
import { CpfValidatorService } from './cpf-validator/cpf-validator.service';
import { NifValidatorService } from './nif-validator/nif-validator.service';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(
    private cpfValidatorService: CpfValidatorService,
    private nifValidatorService: NifValidatorService,
  ) {
  }

  public validateCPF(cpf: string): boolean {
    return this.cpfValidatorService.validateCPF(cpf);
  }

  // Form Validators
  public cpfControlIsValid(documentTypeId: string | number, cpf) {
    return this.cpfValidatorService.cpfControlIsValid(documentTypeId, cpf);
  }

  public validateNif(cpf: string): boolean {
    return this.nifValidatorService.validateNIF(cpf);
  }

  // Form Validators
  public nifControlIsValid(documentTypeId: string | number, nif, countryCode: string) {
    return this.nifValidatorService.nifControlIsValid(documentTypeId, nif, countryCode);
  }
}
