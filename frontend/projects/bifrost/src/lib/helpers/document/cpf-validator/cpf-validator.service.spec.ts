import { TestBed } from '@angular/core/testing';

import { CpfValidatorService } from './cpf-validator.service';
import { configureTestSuite } from 'ng-bullet';

describe('CpfValidatorService', () => {
  let service: CpfValidatorService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({});

    service = TestBed.get(CpfValidatorService);
  });

  it('should be created', () => {
    service = TestBed.get(CpfValidatorService);
    expect(service).toBeTruthy();
  });
});
