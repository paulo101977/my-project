import { Injectable } from '@angular/core';
import { DocumentTypeNaturalEnum } from '../../../enums/document-type-enum';

@Injectable({
  providedIn: 'root'
})
export class CpfValidatorService {

  constructor() {
  }

  public validateCPF(cpf: string) {
    let numbers, digits, sum, i, result, equal_digits;
    equal_digits = 1;
    if (cpf) {
      cpf += '';
      cpf = cpf.replace(/\./g, '').replace(/\-/g, '');
      if (cpf.length < 11) {
        return false;
      }
      for (i = 0; i < cpf.length - 1; i++) {
        if (cpf.charAt(i) != cpf.charAt(i + 1)) {
          equal_digits = 0;
          break;
        }
      }
      if (!equal_digits) {
        numbers = cpf.substring(0, 9);
        digits = cpf.substring(9);
        sum = 0;
        for (i = 10; i > 1; i--) {
          sum += numbers.charAt(10 - i) * i;
        }
        result = sum % 11 < 2 ? 0 : 11 - (sum % 11);
        if (result != digits.charAt(0)) {
          return false;
        }
        numbers = cpf.substring(0, 10);
        sum = 0;
        for (i = 11; i > 1; i--) {
          sum += numbers.charAt(11 - i) * i;
        }
        result = sum % 11 < 2 ? 0 : 11 - (sum % 11);
        return result == digits.charAt(1);
      } else {
        return false;
      }
    }
    return false;
  }

  // Form Validators
  public cpfControlIsValid(documentTypeId: string | number, cpf) {
    if (documentTypeId && cpf) {
      if (+documentTypeId == DocumentTypeNaturalEnum.CPF) {
        return this.validateCPF(cpf)
          ? null
          : {'invalidDocument': {document: 'CPF'}};
      } else {
        return null;
      }
    }
  }
}
