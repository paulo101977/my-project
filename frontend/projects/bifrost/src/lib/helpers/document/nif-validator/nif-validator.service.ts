import { Injectable } from '@angular/core';
import { DocumentTypeLegalEnum, DocumentTypeNaturalEnum } from '../../../enums/document-type-enum';
import { CountryCodeEnum } from '../../../location-manager/models/index';

@Injectable({
  providedIn: 'root'
})
export class NifValidatorService {
  constructor() {
  }

  public validateNIF(nif) {
    if (nif.length == 9) {
      let c = 0;
      for (let i = 0; i < nif.length - 1; ++i) {
        c += Number(nif[i]) * (10 - i - 1);
      }
      c = 11 - (c % 11);
      c = c >= 10 ? 0 : c;
      return (nif[8] == c);
    } else {
      return false;
    }
  }

  public nifControlIsValid(documentTypeId: string | number, nif, countryCode: string) {
    const invalidReturn = {'invalidDocument': {document: 'NIF'}};
    if ((documentTypeId == DocumentTypeNaturalEnum.NIF || documentTypeId == DocumentTypeLegalEnum.NIF)
      && countryCode == CountryCodeEnum.PT) {
      return this.validateNIF(nif)
        ? null
        : invalidReturn;
    } else {
      return invalidReturn;
    }
  }

}
