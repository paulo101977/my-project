import { TestBed } from '@angular/core/testing';

import { NifValidatorService } from './nif-validator.service';
import { configureTestSuite } from 'ng-bullet';
import { DocumentTypeNaturalEnum } from '../../../enums/document-type-enum';
import { CountryCodeEnum } from '../../../location-manager/models/index';

describe('NifValidatorService', () => {
  let service: NifValidatorService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({});

    service = TestBed.get(NifValidatorService);
  });

  it('should be created', () => {
    service = TestBed.get(NifValidatorService);
    expect(service).toBeTruthy();
  });

  // true cases
  it('checkNif singular 1', () => {
    expect(service.validateNIF('122371135')).toBeTruthy();
  });

  it('checkNif singular 2', () => {
    expect(service.validateNIF('243661037')).toBeTruthy();
  });

  it('checkNif singular 3', () => {
    expect(service.validateNIF('381852113')).toBeTruthy();
  });

  it('checkNif singular 5', () => {
    expect(service.validateNIF('593596951')).toBeTruthy();
  });

  it('checkNif singular 6', () => {
    expect(service.validateNIF('607000120')).toBeTruthy();
  });

  it('checkNif singular individual entrepreneur', () => {
    expect(service.validateNIF('844871796')).toBeTruthy();
  });

  // errors cases
  it('checkNif Irregular collective person or provisional number', () => {
    expect(service.validateNIF('951778838')).toBeFalsy();
  });

  it('checkNif singular 1 error', () => {
    expect(service.validateNIF('122371139')).toBeFalsy();
  });

  it('checkNif singular 2 error', () => {
    expect(service.validateNIF('243661737')).toBeFalsy();
  });

  it('checkNif singular 3 error', () => {
    expect(service.validateNIF('391852113')).toBeFalsy();
  });

  it('checkNif singular 5 error', () => {
    expect(service.validateNIF('593196951')).toBeFalsy();
  });

  it('checkNif singular 6 error', () => {
    expect(service.validateNIF('607080120')).toBeFalsy();
  });

  it('checkNif singular individual entrepreneur error', () => {
    expect(service.validateNIF('840871796')).toBeFalsy();
  });

  it('checkNif singular generic error', () => {
    expect(service.validateNIF('123')).toBeFalsy();
  });

  it('checkNif singular generic error empty', () => {
    expect(service.validateNIF('')).toBeFalsy();
  });

  it('checkNif singular generic error string', () => {
    expect(service.validateNIF('abcd')).toBeFalsy();
  });

  it('checkNif singular generic error undefined', () => {
    expect(service.validateNIF(undefined)).toBeFalsy();
  });

  it('should nifControlIsValid - VALID', () => {
    spyOn(service, 'validateNIF').and.returnValue(true);

    const documentTypeId = DocumentTypeNaturalEnum.NIF;
    const nif = '243661037';
    const countryCode = CountryCodeEnum.PT;

    const result = service.nifControlIsValid(documentTypeId, nif, countryCode);

    expect(result).toBeNull();
    expect(service.validateNIF)
      .toHaveBeenCalledWith(nif);
  });

  it('should nifControlIsValid - INVALID DOCUMENT TYPE', () => {
    spyOn(service, 'validateNIF');

    const documentTypeId = DocumentTypeNaturalEnum.CPF;
    const nif = '243661037';
    const countryCode = CountryCodeEnum.PT;

    const result = service.nifControlIsValid(documentTypeId, nif, countryCode);

    expect(result).toEqual({'invalidDocument': {document: 'NIF'}});
    expect(service.validateNIF).not.toHaveBeenCalledWith(nif);
  });

  it('should nifControlIsValid - INVALID NIF', () => {
    spyOn<any>(service, 'validateNIF').and.returnValue({'invalidDocument': {document: 'NIF'}});

    const documentTypeId = DocumentTypeNaturalEnum.NIF;
    const nif = '243669037';
    const countryCode = CountryCodeEnum.PT;

    const result = service.nifControlIsValid(documentTypeId, nif, countryCode);

    expect(result).toEqual({'invalidDocument': {document: 'NIF'}});
    expect(service.validateNIF).toHaveBeenCalledWith(nif);
  });

});
