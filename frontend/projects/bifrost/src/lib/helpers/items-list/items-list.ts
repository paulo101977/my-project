export function itemsToOptions(labelField: string, valueField: string) {
  return (items) => {
    return items.map(
      (item) => ({
        label: item[labelField],
        value: item[valueField]
      })
    );
  };
}


const clearList = (list) => {
  if (list) {
    list = [];
  }
  return list;
};

const removeItemsFromList = (items, sourceList) => {
  if (items) {
    items.forEach(itemSelected => {
      if (sourceList && sourceList.length) {
        sourceList.splice(sourceList.indexOf(itemSelected), 1);
      }
    });
  }
  return sourceList;
};

const addItemsInList = (selectedList: any[], destinationList: any[]) => {
  if (selectedList && selectedList.length) {
    if (!destinationList) {
      destinationList = [];
    }
    destinationList.push(...selectedList);
  }
  return destinationList;
};

export const associateListToOtherList = (selectedList, sourceList, destinationList)
  : { selectedList: any[], sourceList: any[], destinationList: any[] } => {
  if (selectedList && selectedList.length) {
    destinationList = addItemsInList(selectedList, destinationList);
    sourceList = removeItemsFromList(selectedList, sourceList);
    selectedList = clearList(selectedList);
    return {selectedList, sourceList, destinationList};
  }
};
