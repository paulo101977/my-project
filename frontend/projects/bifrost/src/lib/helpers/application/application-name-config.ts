import { InjectionToken } from '@angular/core';

export const APP_NAME = new InjectionToken<string>('application_name');

export function configureAppName(appName: string) {
  return {
    provide: APP_NAME,
    useValue: appName
  };
}
