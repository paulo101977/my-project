import { Injectable } from '@angular/core';
import { BaseApiService } from '../api-builder/services/base-api/base-api.service';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';


@Injectable()
export class CentralApiService extends BaseApiService {

  configUrlEnvironments() {
    return { // TODO: check env urls
      [ApiEnvironment.Development]: 'https://thex-api-central-fabric.azurewebsites.net/api/',
      [ApiEnvironment.Production]: 'https://thex-api-central.totvs.com.br/api/',
      [ApiEnvironment.Local]: 'http://localhost:5010/api/',
      [ApiEnvironment.Staging]: 'https://thex-api-central-fabric-stg.azurewebsites.net/api/',
      [ApiEnvironment.Test]: 'https://thex-api-central-fabric-tst.azurewebsites.net/api/',
      [ApiEnvironment.Uat]: 'https://thex-api-central-fabric-uat.azurewebsites.net/api/',
      [ApiEnvironment.Automization]: 'https://thex-api-central-fabric-aut.azurewebsites.net/api/',
      [ApiEnvironment.Bugfix]: 'https://thex-api-central-fabric-bugfix.azurewebsites.net/api/',
    };
  }

}
