import { CentralApiModule } from './central-api.module';

describe('AdminApiModule', () => {
  let adminApiModule: CentralApiModule;

  beforeEach(() => {
    adminApiModule = new CentralApiModule();
  });

  it('should create an instance', () => {
    expect(adminApiModule).toBeTruthy();
  });
});
