import { TestBed, inject } from '@angular/core/testing';
import { ApiBuilderService } from '../api-builder/api-builder.service';
import { CentralApiService } from './central-api.service';
import { ApiBuilderServiceStub } from '../api-builder/api-builder.mock';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { configureApi } from '../api-builder/models/api-config';




describe('AdminApiService', () => {
  let apiBuilderService: ApiBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CentralApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  it('should be created', inject([CentralApiService], (service: CentralApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'http://localhost:5010/api/',
        [ApiEnvironment.Production]: 'https://thex-api-central-brazil-south.azurewebsites.net/api/',
        [ApiEnvironment.Local]: 'http://localhost:5010/api/',
        [ApiEnvironment.Staging]: 'https://thex-api-central-fabric-stg.azurewebsites.net/api/',
        [ApiEnvironment.Test]: 'https://thex-api-central-fabric-tst.azurewebsites.net/api/',
        [ApiEnvironment.Uat]: 'https://thex-api-central-fabric-uat.azurewebsites.net/api/',
        [ApiEnvironment.Automization]: 'https://thex-api-central-fabric-aut.azurewebsites.net/api/',
        [ApiEnvironment.Bugfix]: 'https://thex-api-central-fabric-bugfix.azurewebsites.net/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
