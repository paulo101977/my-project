import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { CentralApiService } from './central-api.service';



@NgModule({
  imports: [
    ApiBuilderModule,
  ],
  providers: [ CentralApiService ]
})
export class CentralApiModule { }
