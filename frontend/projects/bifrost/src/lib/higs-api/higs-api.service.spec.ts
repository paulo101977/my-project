import { inject, TestBed } from '@angular/core/testing';
import { ApiBuilderServiceStub } from '../api-builder/api-builder.mock';
import { ApiBuilderService } from '../api-builder/api-builder.service';
import { configureApi } from '../api-builder/models/api-config';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';

import { HigsApiService } from './higs-api.service';

describe('HigsApiService', () => {
  let apiBuilderService: ApiBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HigsApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  it('should be created', inject([HigsApiService], (service: HigsApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'https://thex-api-higsintegration-fabric.azurewebsites.net/api/',
        [ApiEnvironment.Production]: 'https://thex-api-higs.totvs.com.br/api/',
        [ApiEnvironment.Local]: 'http://localhost:5005/api/',
        [ApiEnvironment.Staging]: 'https://thex-api-higsintegration-fabric-stg.azurewebsites.net/api/',
        [ApiEnvironment.Test]: 'https://thex-api-higsintegration-fabric-tst.azurewebsites.net/api/',
        [ApiEnvironment.Uat]: 'https://thex-api-higsintegration-fabric-uat.azurewebsites.net/api/',
        [ApiEnvironment.Automization]: 'https://thex-api-higsintegration-fabric-aut.azurewebsites.net/api/',
        [ApiEnvironment.Bugfix]: 'https://thex-api-higsintegration-fabric-bugfix.azurewebsites.net/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
