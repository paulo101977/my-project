import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { HigsApiService } from './higs-api.service';

@NgModule({
  imports: [
    ApiBuilderModule,
  ],
  providers: [ HigsApiService ]
})
export class HigsApiModule { }
