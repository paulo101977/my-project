import { Injectable } from '@angular/core';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { BaseApiService } from '../api-builder/services/base-api/base-api.service';

@Injectable()
export class HigsApiService extends BaseApiService {

  configUrlEnvironments() {
    return {
      [ApiEnvironment.Development]: 'https://thex-api-higsintegration-fabric.azurewebsites.net/api/',
      [ApiEnvironment.Production]: 'https://thex-api-higs.totvs.com.br/api/',
      [ApiEnvironment.Local]: 'http://localhost:5004/api/',
      [ApiEnvironment.Staging]: 'https://thex-api-higsintegration-fabric-stg.azurewebsites.net/api/',
      [ApiEnvironment.Test]: 'https://thex-api-higsintegration-fabric-tst.azurewebsites.net/api/',
      [ApiEnvironment.Uat]: 'https://thex-api-higsintegration-fabric-uat.azurewebsites.net/api/',
      [ApiEnvironment.Automization]: 'https://thex-api-higsintegration-fabric-aut.azurewebsites.net/api/',
      [ApiEnvironment.Bugfix]: 'https://thex-api-higsintegration-fabric-bugfix.azurewebsites.net/api/',
    };
  }

}
