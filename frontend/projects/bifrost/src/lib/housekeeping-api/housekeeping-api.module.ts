import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { HousekeepingApiService } from './housekeeping-api.service';


@NgModule({
  imports: [
    ApiBuilderModule,
  ],
  providers: [ HousekeepingApiService ]
})
export class HousekeepingApiModule { }
