import { TestBed, inject } from '@angular/core/testing';

import { HousekeepingApiService } from './housekeeping-api.service';
import { ApiBuilderService } from '../api-builder/api-builder.service';
import { ApiBuilderServiceStub } from '../api-builder/api-builder.mock';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { configureApi } from '../api-builder/models/api-config';

describe('HousekeepingApiService', () => {
  let apiBuilderService: ApiBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HousekeepingApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  // TODO: add more environments cases
  it('should be created', inject([HousekeepingApiService], (service: HousekeepingApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'https://thex-api-housekeeping-fabric.azurewebsites.net/api/',
        [ApiEnvironment.Local]: 'http://localhost:5003/api/',
        [ApiEnvironment.Production]: 'https://thex-api-housekeeping-brazil-south.azurewebsites.net/api/',
        [ApiEnvironment.Staging]: 'https://thex-api-housekeeping-fabric-stg.azurewebsites.net/api/',
        [ApiEnvironment.Test]: 'https://thex-api-housekeeping-fabric-tst.azurewebsites.net/api/',
        [ApiEnvironment.Uat]: 'https://thex-api-housekeeping-fabric-uat.azurewebsites.net/api/',
        [ApiEnvironment.Automization]: 'https://thex-api-housekeeping-fabric-aut.azurewebsites.net/api/',
        [ApiEnvironment.Bugfix]: 'https://thex-api-housekeeping-fabric-bugfix.azurewebsites.net/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
