import { HousekeepingApiModule } from './housekeeping-api.module';

describe('HousekeepingApiModule', () => {
  let adminApiModule: HousekeepingApiModule;

  beforeEach(() => {
    adminApiModule = new HousekeepingApiModule();
  });

  it('should create an instance', () => {
    expect(adminApiModule).toBeTruthy();
  });
});
