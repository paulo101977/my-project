import { Injectable } from '@angular/core';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { BaseApiService } from '../api-builder/services/base-api/base-api.service';

@Injectable()
export class HousekeepingApiService extends BaseApiService {

  configUrlEnvironments() {
    return {
      [ApiEnvironment.Development]: 'https://thex-api-housekeeping-fabric.azurewebsites.net/api/',
      [ApiEnvironment.Local]: 'http://localhost:5002/api/',
      [ApiEnvironment.Production]: 'https://thex-api-housekeeping.totvs.com.br/api/',
      [ApiEnvironment.Staging]: 'https://thex-api-housekeeping-fabric-stg.azurewebsites.net/api/',
      [ApiEnvironment.Test]: 'https://thex-api-housekeeping-fabric-tst.azurewebsites.net/api/',
      [ApiEnvironment.Uat]: 'https://thex-api-housekeeping-fabric-uat.azurewebsites.net/api/',
      [ApiEnvironment.Automization]: 'https://thex-api-housekeeping-fabric-aut.azurewebsites.net/api/',
      [ApiEnvironment.Bugfix]: 'https://thex-api-housekeeping-fabric-bugfix.azurewebsites.net/api/',
    };
  }

}
