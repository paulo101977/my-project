import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ApplicationStorageBaseService } from './application-storage-base.service';

@Injectable({providedIn: 'root'})
export class PropertyStorageService extends ApplicationStorageBaseService {
  private readonly KEY = 'property';
  private readonly KEYList = 'propertyList';
  private emitCurrentPropertyChangeSource = new Subject<any>();
  private propertyList: Array<any>;
  public currentPropertyChangeEmitted$ = this.emitCurrentPropertyChangeSource.asObservable();

  public addProperty(appName: string, property: any) {
    if (property) {
      const propertyInfo = this.getPropertyInfo(appName);
      propertyInfo[property.id] = property;
      this.savePropertyInfo(appName, propertyInfo);
    }
  }

  public getProperty(appName: string, propertyId: number) {
    const propertyInfo = this.getPropertyInfo(appName);
    return propertyInfo && propertyInfo[propertyId];
  }

  public setCurrentProperty(property: any) {
    if (!property || property && property.id !== this.getCurrentPropertyId()) {
      sessionStorage.setItem(this.KEY, JSON.stringify(property));
      this.emitCurrentPropertyChangeSource.next(property);
    }
  }

  public getCurrentProperty(): any {
    return JSON.parse(sessionStorage.getItem(this.KEY));
  }

  public getCurrentPropertyId(): number {
    const property = this.getCurrentProperty();
    return property ? property.id : 0;
  }

  public savePropertyList(property: any, appName: string) {
    localStorage.setItem(this.getStorageKeyName(appName, this.KEYList), JSON.stringify(property));
  }

  public getPropertyList(appName: string) {
    return JSON.parse(localStorage.getItem(this.getStorageKeyName(appName, this.KEYList))) || {};
  }

  private savePropertyInfo(appName: string, property: any) {
    localStorage.setItem(this.getStorageKeyName(appName, this.KEY), JSON.stringify(property));
  }

  private getPropertyInfo(appName: string) {
    return JSON.parse(localStorage.getItem(this.getStorageKeyName(appName, this.KEY))) || {};
  }

  public setCurrentPropertyFromPropertyList(propertyId: number, appName: string) {

    if (propertyId && this.getCurrentPropertyId() != propertyId) {
      const selectedProperty = this.getPropertyList(appName).find( item => item.id == propertyId);
      this.setCurrentProperty(selectedProperty);
      return selectedProperty;
    }
    return this.getCurrentProperty();
  }
}
