import { inject, TestBed } from '@angular/core/testing';

import { LocalUserManagerService } from './local-user-manager.service';
import { StorageFake } from './testing/storage';

describe('LocalUserManagerService', () => {

  const appName = 'test';
  let user;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalUserManagerService]
    });
    spyOn(localStorage, 'setItem').and.callFake(StorageFake.setItem);
    spyOn(localStorage, 'getItem').and.callFake(StorageFake.getItem);
    spyOn(localStorage, 'removeItem').and.callFake(StorageFake.removeItem);

    user = { name: 'Teste'};
  });

  it('should be created', inject([LocalUserManagerService], (service: LocalUserManagerService) => {
    expect(service).toBeTruthy();
  }));

  it('should set user', inject([LocalUserManagerService], (service: LocalUserManagerService) => {
    service.setUser(appName, user);

    expect(localStorage.setItem).toHaveBeenCalledWith(service.getStorageKeyName(appName, 'user'), JSON.stringify(user));
  }));

  it('should getItem User', inject([LocalUserManagerService], (service: LocalUserManagerService) => {
    service.setUser(appName, user);

    const myUser = service.getUser(appName);

    expect(localStorage.getItem).toHaveBeenCalledWith(service.getStorageKeyName(appName, 'user'));
    expect(myUser).toEqual(user);
  }));

  it('should clean user', inject([LocalUserManagerService], (service: LocalUserManagerService) => {
    service.setUser(appName, user);

    service.cleanUser(appName);

    expect(localStorage.removeItem).toHaveBeenCalledWith(service.getStorageKeyName(appName, 'user'));
    expect(service.getUser(appName)).toEqual({});
  }));
});
