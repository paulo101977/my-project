import { Injectable } from '@angular/core';
import { ApplicationStorageBaseService } from './application-storage-base.service';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class TabAuthControlService extends ApplicationStorageBaseService {
  private readonly KEY = 'tab';

  public generateTabId(appName: string) {
    return uuid.v4();
  }

  public getCurrentTabId(appName: string): string {
    return sessionStorage.getItem(this.getStorageKeyName(appName, this.KEY));
  }

  public verifyTabs(appName: string) {
    const tabToAdd = this.getCurrentTabId(appName);
    if (tabToAdd) {
      this.addTab( appName, tabToAdd );
    }

    const tabs: string[] = JSON.parse(localStorage
      .getItem(this.getStorageKeyName(appName, this.KEY)));
    if (!tabs || tabs && !tabs.length) {
      localStorage.clear();
    }

    sessionStorage.removeItem(this.getStorageKeyName(appName, this.KEY));
  }

  public addTab(appName: string, tabId: string) {
    const tabs: string[] = JSON.parse(localStorage.getItem(this.getStorageKeyName(appName, this.KEY))) || [];
    if (!tabs.find(item => item == tabId)) {
      tabs.push(tabId);
      localStorage.setItem(this.getStorageKeyName(appName, this.KEY), JSON.stringify(tabs));
    }
  }

  public removeTab(appName: string, tabId: string) {
      const tabs: string[] = JSON.parse(localStorage
        .getItem(this.getStorageKeyName(appName, this.KEY)));
      if (tabs.find(item => item == tabId )) {
        tabs.splice(tabs.indexOf(tabId), 1);
        localStorage.setItem(this.getStorageKeyName(appName, this.KEY), JSON.stringify(tabs));
        sessionStorage.setItem(this.getStorageKeyName(appName, this.KEY), tabId);
      }
  }
}
