import { TestBed, inject } from '@angular/core/testing';

import { TabAuthControlService } from './tab-auth-control.service';

describe('TabAuthControlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TabAuthControlService]
    });
  });

  it('should be created', inject([TabAuthControlService], (service: TabAuthControlService) => {
    expect(service).toBeTruthy();
  }));
});
