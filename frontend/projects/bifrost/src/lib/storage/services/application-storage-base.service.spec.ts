import { TestBed, inject } from '@angular/core/testing';

import { ApplicationStorageBaseService } from './application-storage-base.service';

describe('ApplicationStorageBaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApplicationStorageBaseService]
    });
  });

  it('should be created', inject([ApplicationStorageBaseService], (service: ApplicationStorageBaseService) => {
    expect(service).toBeTruthy();
  }));

  it('should get StorageKeyName', inject([ApplicationStorageBaseService], (service: ApplicationStorageBaseService) => {
    const key = service.getStorageKeyName('app', 'key');

    expect(key).toEqual('app:key');
  }));
});
