import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ApplicationStorageBaseService } from './application-storage-base.service';

@Injectable({providedIn: 'root'})
export class TokenManagerService extends ApplicationStorageBaseService {

  private PROPERTY_TOKEN = 'property-token';
  private APPLICATION_TOKEN = 'application-token';
  private EXTERNAL_TOKEN = 'external-token';


  // APPLICATION TOKEN
  public getToken(appName: string) {
    return localStorage.getItem(this.getStorageKeyName(appName, this.APPLICATION_TOKEN));
  }

  public setToken(appName: string, token: string) {
    localStorage.setItem(this.getStorageKeyName(appName, this.APPLICATION_TOKEN), token);
    this.setCurrentToken(token);
  }

  public getCurrentToken() {
    return sessionStorage.getItem(this.APPLICATION_TOKEN);
  }

  public setCurrentToken(token: string) {
    sessionStorage.setItem(this.APPLICATION_TOKEN, token);
  }

  public cleanApplicationToken(appName: string) {
    localStorage.removeItem(this.getStorageKeyName(appName, this.APPLICATION_TOKEN));
    sessionStorage.removeItem(this.APPLICATION_TOKEN);
  }

  public isAuthenticated(appName: string): boolean {
    const token = this.getToken(appName);
    const jwtHelper = new JwtHelperService();
    return token && !jwtHelper.isTokenExpired(token);
  }

  // PROPERTY TOKEN
  public setPropertyToken(appName: string, token: string, propertyId: number, property = null) {
    if (propertyId) {
      const propertyToken = this.getAllPropertyTokens(appName) || {};
      propertyToken[propertyId] = this.prepareToken(property, token);
      this.setAllPropertyTokens(appName, propertyToken);
    }
  }

  public getPropertyToken(appName: string, propertyId: number): string {
    const propertyToken = this.getAllPropertyTokens(appName) || {};
    const propertyInfo =  propertyToken[propertyId];
    return propertyInfo ? propertyInfo.token : null;
  }

  private setAllPropertyTokens(appName: string, tokensObj: any ) {
    return localStorage.setItem(this.getStorageKeyName(appName, this.PROPERTY_TOKEN), JSON.stringify(tokensObj));
  }

  private getAllPropertyTokens(appName: string) {
    return JSON.parse(localStorage.getItem(this.getStorageKeyName(appName, this.PROPERTY_TOKEN)) || '{}');
  }

  public prepareToken (property, token) {
    return  {
      propertyId: property && property.id,
      token: token,
      propertyName: property && property.name,
    };
  }

  public setCurrentPropertyToken(token: string) {
    sessionStorage.setItem(this.PROPERTY_TOKEN, token);
  }

  public getCurrentPropertyToken(): string {
    return sessionStorage.getItem(this.PROPERTY_TOKEN);
  }

  public cleanPropertyToken(appName: string) {
    localStorage.removeItem(this.getStorageKeyName(appName, this.PROPERTY_TOKEN));
    sessionStorage.removeItem(this.PROPERTY_TOKEN);
  }

  public isAuthenticatedOnProperty(appName: string, propertyId: number): boolean {
    const propertyToken = this.getPropertyToken(appName, propertyId);
    const jwtHelper = new JwtHelperService();
    return propertyToken && !jwtHelper.isTokenExpired(propertyToken);
  }

  // GENERAL TOKEN
  public cleanTokens(appName: string) {
    this.cleanPropertyToken(appName);
    this.cleanApplicationToken(appName);
  }

  public extractTokenPayload(token: string) {
    const jwtHelper = new JwtHelperService();
    return  jwtHelper.decodeToken(token);
  }

  public getUserIdFromToken(token: string) {
    const tokenInfo = this.extractTokenPayload(token);
    if (tokenInfo) {
      return tokenInfo.unique_name;
    }
    return null;
  }

  public isTokenExpired(token: string) {
    const jwtHelper = new JwtHelperService();
    return jwtHelper.isTokenExpired(token);
  }

  public setExternalToken(token: string) {
    return sessionStorage.setItem(this.EXTERNAL_TOKEN, token);
  }

  public getExternalToken() {
    return sessionStorage.getItem(this.EXTERNAL_TOKEN);
  }
}
