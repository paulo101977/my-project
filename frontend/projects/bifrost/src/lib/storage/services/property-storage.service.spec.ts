// import { inject, TestBed } from '@angular/core/testing';
// import { PropertyStorageService } from './property-storage.service';
//
// describe('PropertyStorageService', () => {
//
//   const store = {};
//
//   const setItem = (key, value) => {
//     store[key] = value;
//   };
//   const getItem = (key) => {
//     return store[key] || null;
//   };
//   const removeItem = (key) => {
//     delete store[key];
//   };
//
//   beforeAll(() => {
//     spyOn(localStorage, 'setItem').and.callFake(setItem);
//     spyOn(sessionStorage, 'setItem').and.callFake(setItem);
//
//     spyOn(localStorage, 'getItem').and.callFake(getItem);
//     spyOn(sessionStorage, 'getItem').and.callFake(getItem);
//
//     spyOn(localStorage, 'removeItem').and.callFake(removeItem);
//     spyOn(sessionStorage, 'removeItem').and.callFake(removeItem);
//   });
//
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       providers: [PropertyStorageService]
//     });
//   });
//
//   it('should be created', inject([PropertyStorageService], (service: PropertyStorageService) => {
//     expect(service).toBeTruthy();
//   }));
//
//   describe('propertyList', () => {
//
//     const list: Array<any> = [{id: 1, name: 'property 1'}, {id: 2, name: 'property 2'}];
//     it('should setPropertyList', inject([PropertyStorageService], (service: PropertyStorageService) => {
//       service.setPropertyList(list);
//       expect(service['propertyList']).toEqual(list);
//     }));
//
//     it('should getPropertyList', inject([PropertyStorageService], (service: PropertyStorageService) => {
//       service.setPropertyList(list);
//
//       const returnnedList = service.getPropertyList('pms');
//
//       expect(returnnedList).toEqual(list);
//     }));
//
//     describe('set setCurrentPropertyFromPropertyList', () => {
//       beforeEach(inject([PropertyStorageService], (service: PropertyStorageService) => {
//         spyOn(service['emitCurrentPropertyChangeSource'], 'next');
//         service.setCurrentProperty(list[0]);
//       }));
//
//       it('should return old value when propertyId null',
//         inject([PropertyStorageService], (service: PropertyStorageService) => {
//           service.setPropertyList(list);
//           service.setCurrentProperty(list[0]);
//
//           const property = service.setCurrentPropertyFromPropertyList(0, 'pms');
//
//           expect(property).toEqual(list[0]);
//         }));
//
//       it('should return old value when propertyId equals to currentProperty',
//         inject([PropertyStorageService], (service: PropertyStorageService) => {
//           service.setPropertyList(list);
//
//           const property = service.setCurrentPropertyFromPropertyList(list[0].id, 'pms');
//
//           expect(service['emitCurrentPropertyChangeSource'].next).not.toHaveBeenCalled();
//           expect(property).toEqual(list[0]);
//         }));
//
//       it('should set new value when propertyId different of currentProperty',
//         inject([PropertyStorageService], (service: PropertyStorageService) => {
//           service.setPropertyList(list);
//
//           const property = service.setCurrentPropertyFromPropertyList(list[1].id, 'pms');
//
//           expect(sessionStorage.setItem).toHaveBeenCalled();
//           expect(property).toEqual(list[1]);
//         }));
//
//     });
//   });
// });
