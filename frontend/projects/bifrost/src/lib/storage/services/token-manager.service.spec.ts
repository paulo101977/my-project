import { TestBed, inject } from '@angular/core/testing';
import { TokenManagerService } from './token-manager.service';
import { StorageFake } from './testing/storage';

describe('TokenManagerService', () => {

  const appName = 'test';
  const token = '123';
  const propertyId = 1;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenManagerService]
    });

    spyOn(localStorage, 'setItem').and.callFake(StorageFake.setItem);
    spyOn(localStorage, 'getItem').and.callFake(StorageFake.getItem);
    spyOn(localStorage, 'removeItem').and.callFake(StorageFake.removeItem);

    spyOn(sessionStorage, 'setItem').and.callFake(StorageFake.setItem);
    spyOn(sessionStorage, 'getItem').and.callFake(StorageFake.getItem);
    spyOn(sessionStorage, 'removeItem').and.callFake(StorageFake.removeItem);
  });

  describe('#set token', () => {
    it('should set token', inject([TokenManagerService], (service: TokenManagerService) => {
      service.setToken(appName, token);

      expect(localStorage.setItem).toHaveBeenCalledWith(service.getStorageKeyName(appName, service['APPLICATION_TOKEN']), token);
    }));

    it('should set current Token', inject([TokenManagerService], (service: TokenManagerService) => {
      service.setCurrentToken(token);

      expect(sessionStorage.setItem).toHaveBeenCalledWith(service['APPLICATION_TOKEN'], token);
    }));
  });

  describe('#get token', () => {
    it('should get token', inject([TokenManagerService], (service: TokenManagerService) => {
      const myToken = service.getToken(appName);

      expect(localStorage.getItem).toHaveBeenCalledWith(service.getStorageKeyName(appName, service['APPLICATION_TOKEN']));
      expect(myToken).toEqual(token);
    }));

    it('should set current Token', inject([TokenManagerService], (service: TokenManagerService) => {
      const myToken = service.getCurrentToken();

      expect(sessionStorage.getItem).toHaveBeenCalledWith(service['APPLICATION_TOKEN']);
      expect(myToken).toEqual(token);

    }));
  });

  describe('#set propertyToken', () => {

    it('should set propertyToken ', inject([TokenManagerService], (service: TokenManagerService) => {
      service.setPropertyToken(appName, token, propertyId);

      const items = {};
      const myToken = service.prepareToken(null, token);
      items[propertyId] = myToken;
      expect(localStorage.setItem)
        .toHaveBeenCalledWith(service.getStorageKeyName(appName, service['PROPERTY_TOKEN']), JSON.stringify(items) );
    }));

    it('should set current property Token', inject([TokenManagerService], (service: TokenManagerService) => {
      service.setCurrentPropertyToken(token);
      expect(sessionStorage.setItem).toHaveBeenCalledWith(service['PROPERTY_TOKEN'], token);
    }));
  });

  describe('#get propertyToken', () => {

    it('should get propertyToken ', inject([TokenManagerService], (service: TokenManagerService) => {
      const myToken = service.getPropertyToken(appName, propertyId);

      expect(localStorage.getItem).toHaveBeenCalledWith(service.getStorageKeyName(appName, service['PROPERTY_TOKEN']));
      expect(myToken).toEqual(token);
    }));

    it('should get current property Token', inject([TokenManagerService], (service: TokenManagerService) => {
      const myToken = service.getCurrentPropertyToken();

      expect(sessionStorage.getItem).toHaveBeenCalledWith(service['PROPERTY_TOKEN']);
      expect(myToken).toEqual(token);
    }));
  });

  it('should clear tokens ', inject([TokenManagerService], (service: TokenManagerService) => {
    service.setToken(appName, token);
    service.setCurrentToken(token);
    service.setPropertyToken(appName, token, propertyId);
    service.setCurrentPropertyToken(token);

    service.cleanTokens(appName);

    expect(service.getToken(appName)).toBeFalsy();
    expect(service.getCurrentToken()).toBeFalsy();
    expect(service.getPropertyToken(appName, propertyId)).toBeFalsy();
    expect(service.getCurrentPropertyToken()).toBeFalsy();
  }));


});
