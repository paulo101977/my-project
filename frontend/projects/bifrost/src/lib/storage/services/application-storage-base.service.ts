import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApplicationStorageBaseService {

  constructor() { }
  public getStorageKeyName(appName: string, key: string) {
    return `${appName}:${key}`;
  }
}
