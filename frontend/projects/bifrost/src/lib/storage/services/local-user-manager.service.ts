import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { User } from '../models/user';
import { ApplicationStorageBaseService } from './application-storage-base.service';

@Injectable({
  providedIn: 'root'
})
export class LocalUserManagerService extends ApplicationStorageBaseService {

  private localUserStore = new Subject();
  public localUser$ = this.localUserStore.asObservable().pipe(startWith(''));

  public setUser(appName: string, user: User) {
    localStorage.setItem(this.getStorageKeyName(appName, 'user'), JSON.stringify(user));
    this.localUserStore.next();
  }

  public getUser(appName: string): User {
    return <User>JSON.parse(localStorage.getItem(this.getStorageKeyName(appName, 'user')) || '{}');
  }

  public cleanUser(appName: string) {
    localStorage.removeItem(this.getStorageKeyName(appName, 'user'));
    this.localUserStore.next();
  }
}
