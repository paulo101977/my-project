export const StorageFake = {
  store: {},
  getItem: (name: string) => {
    return StorageFake.store[name];
  },
  setItem: (name: string, value: string) => {
    StorageFake.store[name] = value;
  },
  removeItem: (name: string, value: string) => {
    delete StorageFake.store[name];
  },
  clear: () => {
    StorageFake.store = {};
  }
};

