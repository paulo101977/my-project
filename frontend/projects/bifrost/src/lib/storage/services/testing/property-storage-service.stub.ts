import { createServiceStub } from '../../../../../../../testing';
import { PropertyStorageService } from '../property-storage.service';

export const propertyStorageServiceStub = createServiceStub(PropertyStorageService, {
  getStorageKeyName (appName: string, key: string): string {
    return null;
  },
  getPropertyList (appName: string): {} {
    return null;
  },
  getCurrentProperty (): any {
    return null;
  },
  addProperty (appName: string, property: any) {
    return null;
  },
  getCurrentPropertyId (): number {
    return null;
  },
  getProperty (appName: string, propertyId: number): any {
    return null;
  },
  savePropertyList (appName: string, property: any) {
    return null;
  },
  setCurrentProperty (property: any) {
    return null;
  }
});
