import { createServiceStub } from '../../../../../../../testing';
import { LocalUserManagerService } from '../local-user-manager.service';
import { User } from '../../models/user';
import { of } from 'rxjs/internal/observable/of';

export const LocalUserManagerServiceStub = createServiceStub(LocalUserManagerService, {
  getUser (appName: string): User {
    return null;
  },
  cleanUser (appName: string) {
    return null;
  },
  getStorageKeyName (appName: string, key: string): string {
    return  null;
  },
  setUser (appName: string, user: User) {
    return null;
  },
  localUser$: of(null)
});
