import { createServiceStub } from '../../../../../../../testing';
import { PropertyStorageService } from '../property-storage.service';

export const propertyStorageServiceStub = createServiceStub(PropertyStorageService, {
    addProperty(appName: string, property: any) {
    },

    getCurrentProperty(): any {
        return null;
    },

    getCurrentPropertyId(): number {
        return 1;
    },

    getProperty(appName: string, propertyId: number): any {
        return null;
    },

    getStorageKeyName(appName: string, key: string): string {
        return '';
    },

    setCurrentProperty(property: any) {
    },
});
