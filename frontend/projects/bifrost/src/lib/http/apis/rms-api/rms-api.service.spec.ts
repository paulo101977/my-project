import { TestBed, inject } from '@angular/core/testing';
import { RmsApiService } from './rms-api.service';
import { ApiBuilderService } from '../../../api-builder/api-builder.service';
import { ApiEnvironment } from '../../../api-builder/models/api-environment.enum';
import { configureApi } from '../../../api-builder/models/api-config';
import { ApiBuilderServiceStub } from '../../../api-builder/api-builder.mock';

describe('RmsApiService', () => {
  let apiBuilderService: ApiBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RmsApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  it('should be created', inject([RmsApiService], (service: RmsApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'https://thex-api-rms-fabric.azurewebsites.net/api/',
        [ApiEnvironment.Production]: 'https://thex-api.totvs.com.br/api/',
        [ApiEnvironment.Local]: 'http://localhost:5005/api/',
        [ApiEnvironment.Staging]: 'https://thex-api-rms-fabric-stg.azurewebsites.net/api/',
        [ApiEnvironment.Test]: 'https://thex-api-rms-fabric-tst.azurewebsites.net/api/',
        [ApiEnvironment.Uat]: 'https://thex-api-rms-fabric-uat.azurewebsites.net/api/',
        [ApiEnvironment.Automization]: 'https://thex-api-rms-fabric-aut.azurewebsites.net/api/',
        [ApiEnvironment.Bugfix]: 'https://thex-api-rms-fabric-bugfix.azurewebsites.net/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
