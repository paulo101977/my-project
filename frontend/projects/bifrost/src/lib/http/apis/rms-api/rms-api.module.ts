import { ApiBuilderModule } from './../../../api-builder/api-builder.module';
import { NgModule } from '@angular/core';
import { RmsApiService } from './rms-api.service';

@NgModule({
  imports: [
    ApiBuilderModule
  ],
  providers: [
    RmsApiService
  ]
})
export class RmsApiModule { }
