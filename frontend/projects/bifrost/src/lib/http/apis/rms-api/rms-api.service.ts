import { Injectable } from '@angular/core';
import { ApiEnvironment } from './../../../api-builder/models/api-environment.enum';
import { BaseApiService } from '../../../api-builder/services/base-api/base-api.service';

@Injectable()
export class RmsApiService extends BaseApiService {

  configUrlEnvironments() {
    return {
      [ApiEnvironment.Development]: 'https://thex-api-rms-fabric.azurewebsites.net/api/',
      [ApiEnvironment.Production]: 'https://thex-api.totvs.com.br/api/',
      [ApiEnvironment.Local]: 'http://localhost:5002/api/',
      [ApiEnvironment.Staging]: 'https://thex-api-rms-fabric-stg.azurewebsites.net/api/',
      [ApiEnvironment.Test]: 'https://thex-api-rms-fabric-tst.azurewebsites.net/api/',
      [ApiEnvironment.Uat]: 'https://thex-api-rms-fabric-uat.azurewebsites.net/api/',
      [ApiEnvironment.Automization]: 'https://thex-api-rms-fabric-aut.azurewebsites.net/api/',
      [ApiEnvironment.Bugfix]: 'https://thex-api-rms-fabric-bugfix.azurewebsites.net/api/',
    };
  }
}
