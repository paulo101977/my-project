export interface ItemsResponse<t> {
  hasNext: boolean;
  items: Array<t>;
}
