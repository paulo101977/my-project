import { ProductEnum } from '../resources/auth/models/product-enum';

export class LoginForm {
  login: string;
  password: string;
  grantType?: string;
  productId: ProductEnum;
}
