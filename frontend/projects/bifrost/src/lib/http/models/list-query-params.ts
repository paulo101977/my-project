export interface ListQueryParams {
  SearchData: string;
  Page: number;
  PageSize: number;
}
