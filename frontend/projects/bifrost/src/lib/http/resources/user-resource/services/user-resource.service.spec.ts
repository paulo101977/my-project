import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { AdminApiService } from '../../../../admin-api/admin-api.service';
import { User } from '../models/user';
import { UserInvitation } from '../models/user-invitation';

import { UserResourceService } from './user-resource.service';

describe('UserResourceService', () => {
  let service: UserResourceService;
  let adminApiService: AdminApiService;

  const adminApiServiceStub: Partial<AdminApiService> = {
    get<T>(path: any, options?: any): Observable<T> { return of(null); },
    post<T>(path: any, body?: any, options?: any): Observable<T> { return of(null); },
    put<T>(path: any, body?: any, options?: any): Observable<T> { return of(null); },
    patch<T>(path, body?, options?): Observable<T> { return of(null); }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserResourceService,
        { provide: AdminApiService, useValue: adminApiServiceStub }
      ]
    });

    service = TestBed.get(UserResourceService);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getAll', () => {
    it('should call AdminApiService#get', () => {
      spyOn(adminApiService, 'get');

      service.getAll();
      expect(adminApiService.get).toHaveBeenCalledWith('User');
    });
  });

  describe('#invite', () => {
    it('should call AdminApiService#post', () => {
      const mock: UserInvitation = {
        brandId: 0,
        brandName: '',
        chainId: 0,
        chainName: '',
        email: '',
        id: '',
        invitationAcceptanceDate: '',
        invitationDate: '',
        invitationLink: '',
        isActive: true,
        name: '',
        preferredCulture: '',
        preferredLanguage: '',
        propertyId: 0,
        propertyName: '',
        roleIdList: []
      };
      spyOn(adminApiService, 'post');

      service.invite(mock);
      expect(adminApiService.post).toHaveBeenCalledWith('User/createnewuser', mock);
    });
  });

  describe('#toggleActivation', () => {
    it('should call AdminApiService#patch', () => {
      const uid = '123';
      const propertyId = 1;
      spyOn(adminApiService, 'patch');

      service.toggleActivation(uid, propertyId);
      expect(adminApiService.patch)
        .toHaveBeenCalledWith(`User/userid/${uid}/propertyid/${propertyId}/toggleactivation`, null);
    });
  });

  describe('#updateLanguage', () => {
    it('should call AdminApiService#patch', () => {
      const uid = '123';
      const culture = 'pt-BR';
      spyOn(adminApiService, 'patch');

      service.updateLanguage(uid, culture);
      expect(adminApiService.patch).toHaveBeenCalledWith(`User/${uid}/${culture}/culture`, null);
    });
  });

  describe('#updateAdmin', () => {
    it('should call AdminApiService#patch', () => {
      const uid = '123';
      const isAdmin = false;
      spyOn(adminApiService, 'patch');

      service.updateAdmin(uid, isAdmin);
      expect(adminApiService.patch).toHaveBeenCalledWith(`User/${uid}/${isAdmin}/isadmin`, null);
    });
  });

  it('should call updateUserInfo', () => {
    const userId = '123';
    const dateOfBirth = '10/09/1908';
    const phoneNumber = '';
    const nickName = '';
    const param = {
      Id: userId,
      DateOfBirth: dateOfBirth,
      PhoneNumber: phoneNumber,
      NickName: nickName
    };

    spyOn(adminApiService, 'patch');

    service.updateUserInfo(userId, dateOfBirth, phoneNumber, nickName);
    expect(adminApiService.patch)
      .toHaveBeenCalledWith(`User/UpdateUser`, param);
  });

  describe('#updatePassword', () => {
    it('should call AdminApi#patch', () => {
      const params = {
        'UserId': 'userId',
        'OldPassword': 'previousPassword',
        'NewPassword': 'newPassword'
      };

      spyOn(adminApiService, 'patch');

      service.updatePassword(params.UserId, params.OldPassword, params.NewPassword);

      expect(adminApiService.patch).toHaveBeenCalledWith(`/User/person/changepassword`, params);
    });
  });

  describe('#delete', () => {
    it('should call AdminApi#delete', () => {
      it('should call AdminApi#patch', () => {

        spyOn(adminApiService, 'delete');

        service.delete('uid');

        expect(adminApiService.delete).toHaveBeenCalledWith(`/User/uid`);
      });
    });
  });

});
