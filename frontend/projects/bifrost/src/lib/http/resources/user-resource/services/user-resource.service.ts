import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemsResponse } from '../../../models/item-response';
import { User } from '../models/user';
import { UserInvitation } from '../models/user-invitation';
import { AdminApiService } from '../../../../admin-api/admin-api.service';
import { Permission } from '../models/permission';

@Injectable({
  providedIn: 'root'
})
export class UserResourceService {

  constructor(
    private adminApi: AdminApiService
  ) { }

  public getAll(): Observable<ItemsResponse<User>> {
    return this.adminApi.get<ItemsResponse<User>>(`User`);
  }

  public invite(userInvitation: UserInvitation) {
    return this.adminApi.post(`User/createnewuser`, userInvitation);
  }

  public toggleActivation(uid: string, propertyId: number) {
    return this.adminApi.patch(`User/userid/${uid}/propertyid/${propertyId}/toggleactivation`, null);
  }

  public updateLanguage(userId: string, culture: string) {
    return this.adminApi.patch(`User/${userId}/${culture}/culture`, null);
  }

  public updateAdmin(userId: string, isAdmin: boolean) {
    return this.adminApi.patch(`User/${userId}/${isAdmin}/isadmin`, null);
  }

  public getLink(userId: string) {
    return this.adminApi.patch(`User/${userId}/newInvitationLink`, null, { responseType: 'text' });
  }

  public getCurrentUser(): Observable<User> {
    return this.adminApi.get<User>(`User/currentUser`);
  }

  public getProfilePermissionList(): Observable<any> {
    return this.adminApi.get<Permission[]>('Permission/UserId');
  }
  public updateUserInfo(userId: string, name: string, dateOfBirth: string, phoneNumber: string, nickName: string) {
    const params = {
      Id: userId,
      DateOfBirth: dateOfBirth,
      name: name,
      PhoneNumber: phoneNumber,
      NickName: nickName
    };

    return this.adminApi.patch(`User/UpdateUser`, params);
  }

  public saveUserImage(item: User): Observable<User> {
    return this
      .adminApi
      .put<User>('User/changephoto', item);
  }

  public updatePassword(userId: string, previousPassword: string, newPassword: string) {
    const params = {
      'UserId': userId,
      'OldPassword': previousPassword,
      'NewPassword': newPassword
    };

    return this.adminApi.patch(`User/person/changepassword`, params);
  }

  public delete(userId: string) {
    return this.adminApi.delete((`User/${userId}`));
  }
}
