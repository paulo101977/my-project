export class User {
  id: string;
  userUid: string;
  personId: string;
  tenantId: string;
  userLogin: string;
  preferredLanguage: string;
  preferredCulture: string;
  isActive: boolean;
  name: string;
  email: string;
  lastLink: string;
  roleIdList: any[];
  phoneNumber?: string;
  dateOfBirth?: string;
  nickName?: string;
  photoUrl?: string;
  photoBase64?: string;
}
