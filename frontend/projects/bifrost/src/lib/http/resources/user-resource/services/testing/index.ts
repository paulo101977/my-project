import { createServiceStub } from '../../../../../../../../../testing';
import { Observable, of } from 'rxjs';
import { ItemsResponse } from 'app/shared/models/backend-api/item-response';
import { UserResourceService } from '../user-resource.service';
import { User } from '../../models/user';
import { UserInvitation } from '../../models/user-invitation';

export const userResourceStub = createServiceStub(UserResourceService, {
  getAll (): Observable<ItemsResponse<User>> {
    return of(null);
  },
  getCurrentUser (): Observable<User> {
    return of(null);
  },
  getLink (userId: string): Observable<{}> {
    return of(null);
  },
  invite (userInvitation: UserInvitation): Observable<{}> {
    return of(null);
  },
  toggleActivation (uid: string, propertyId: number): Observable<{}> {
    return of(null);
  },
  updateAdmin (userId: string, isAdmin: boolean): Observable<{}> {
    return of(null);
  },
  updateLanguage (userId: string, culture: string): Observable<{}> {
    return of(null);
  },
  updateUserInfo (userId: string, dateOfBirth: string, phoneNumber: string, nickName: string): Observable<any> {
    return of(null);
  }
});
