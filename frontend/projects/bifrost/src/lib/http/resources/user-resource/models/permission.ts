export interface Permission {
  permissionId: string;
  permission: string;
  permissionList: any[];
}
