import { Injectable } from '@angular/core';
import { switchMap, tap } from 'rxjs/internal/operators';
import { Observable } from 'rxjs';
import { AuthResource } from './auth.resource';
import { LoginForm } from '../../../models/login-form';
import { TokenManagerService } from '../../../../storage/services/token-manager.service';
import { LocalUserManagerService } from '../../../../storage/services/local-user-manager.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public redirectUrl: any;

  constructor(private authResource: AuthResource,
              private tokenManager: TokenManagerService,
              private userManager: LocalUserManagerService) {
  }

  public authenticate(applicationName: string, userForm: LoginForm): Observable<any> {
    return this.authResource.login(userForm)
      .pipe(
        tap( () => this.cleanToken(applicationName)),
        tap( result => this.setToken(applicationName, result) ),
        switchMap( () => this.getUser() ),
        tap( user => this.saveUser(applicationName, user))
      );
  }

  public authenticateOnProperty(appName: string, propertyId: number): Observable<any> {
    return this.authResource.getTokenByPropertyId(propertyId)
      .pipe(
        tap( token => this.tokenManager.setPropertyToken(appName, token.newPasswordToken, propertyId )),
        tap( token => this.tokenManager.setCurrentPropertyToken(token.newPasswordToken))
      );

  }

  public recoverPassword(userEmail) {
    return this.authResource.recoverPassword(userEmail);
  }

  public logout(appName: string) {
    this.tokenManager.cleanTokens(appName);
    this.userManager.cleanUser(appName);
    sessionStorage.clear();
  }

  private setToken(applicationName, result) {
    const { newPasswordToken } = result;
    this.tokenManager.setToken(applicationName, newPasswordToken );
  }

  public saveUser(applicationName, user) {
    this.userManager.setUser(applicationName, user);
  }

  public getUser(): Observable<any> {
    return this.authResource.getUserInfo();
  }

  private cleanToken(applicationName: string) {
    this.tokenManager.cleanTokens(applicationName);
  }

  public isAuthenticated(appName: string): boolean {
    return  this.tokenManager.isAuthenticated(appName);
  }

  public isAuthenticatedOnProperty(appName: string, propertyToken: number): boolean {
    return  this.tokenManager.isAuthenticatedOnProperty(appName, propertyToken);
  }

  public extractErrorMessage(error) {
    let errorMessage;
    switch (error.status) {
      case 401:
        errorMessage = 'authModule.login.unauthorized';
        break;
      case 403:
        errorMessage = 'authModule.login.forbidden';
        break;
      default:
        if (error.error.details && error.error.details.length > 0 && error.error.details[0].detailedMessage == 'UserLocked') {
          errorMessage = error.error.details[0].message;
        } else {
          errorMessage = 'commomData.errorMessage';
        }
        break;
    }
    return errorMessage;
  }
}
