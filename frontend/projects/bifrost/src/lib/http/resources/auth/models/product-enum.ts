export enum ProductEnum {
  PMS = 1,
  PDV = 2,
  HouseKeeping = 3,
  SuperAdmin = 4,
  Preferences = 5,
  Tributes = 6,
  HigsIntegration = 7,
  Central = 8
}
