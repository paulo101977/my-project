import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AdminApiService } from '../../../../admin-api/admin-api.service';
import { TokenInterceptor } from '../../../../interceptors/token-interceptor/token-interceptor.service';
import { LoginForm } from '../../../models/login-form';
import { User } from '../../user-resource/models/user';


@Injectable({
  providedIn: 'root'
})
export class AuthResource {

  constructor( private adminApi: AdminApiService,
               private tokenInterceptor: TokenInterceptor) { }

  public login(loginForm: LoginForm) {
    loginForm.grantType = 'password';

    this.tokenInterceptor.disableOnce();
    return this.adminApi
      .post(`User/login`, loginForm);
  }

  public getUserInfo() {
    const upperCaseLanguage = (languageIso) => {
      const [language, country] = languageIso.split('-');
      return `${language}-${country.toUpperCase()}`;
    };

    return this
      .adminApi
      .get(`User/currentUser`)
      .pipe(
        map((user: User): User => {
          const photoUrl = user.photoUrl + '?_=' + new Date().getTime();

          return {
            ...user,
            preferredCulture: upperCaseLanguage(user.preferredCulture),
            preferredLanguage: upperCaseLanguage(user.preferredLanguage),
            // timestamp is needed to avoid cache problems
            // TODO remove when the cache problem is solved
            ...(user.photoUrl ? {photoUrl} : null)
          };
        })
      );
  }

  public recoverPassword(email: string) {
    return this.adminApi.post(`User/forgotpassword/${email}`, null);
  }

  public getTokenByPropertyId(propertyId: number): Observable<any> {
    return this.adminApi.get<any>(`User/property/${propertyId}/token`);
  }
}
