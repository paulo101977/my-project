import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserTotvsRequest } from '../models/user-totvs-request';
import { AdminApiService } from '../../../../admin-api/admin-api.service';
import { LoginForm } from '../../../models/login-form';

@Injectable({
  providedIn: 'root'
})
export class UserTotvsResourceService {

  constructor(private adminApi: AdminApiService) { }

  public login(params: LoginForm) {
    return this.adminApi.post('UserTotvs/login', params);
  }

  public getUser(id: string): Observable<any> {
    return this.adminApi.get(`UserTotvs/${id}`);
  }

  public resetPassword(email: string) {
    return this.adminApi.post(`UserTotvs/forgotpassword/${email}`);
  }

  public createUser(user: UserTotvsRequest) {
    return this.adminApi.post(`UserTotvs`, user);
  }

  public updateUser(user: UserTotvsRequest, id: string) {
    return this.adminApi.put(`UserTotvs/${id}`, user);
  }
}
