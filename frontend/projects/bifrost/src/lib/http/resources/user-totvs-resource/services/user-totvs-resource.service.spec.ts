import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { AdminApiService } from '../../../../admin-api/admin-api.service';
import { LoginForm } from '../../../models/login-form';

import { UserTotvsResourceService } from './user-totvs-resource.service';

describe('UserTotvsResourceService', () => {
  let service: UserTotvsResourceService;
  let adminApiService: AdminApiService;

  const adminApiStubService: Partial<AdminApiService> = {
    get<T>(path, options?): Observable<T> { return of(null); },
    post<T>(path, body?, options?): Observable<T> { return of(null); }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserTotvsResourceService,
        { provide: AdminApiService, useValue: adminApiStubService }
      ]
    });

    service = TestBed.get(UserTotvsResourceService);
    adminApiService = TestBed.get(AdminApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#login', () => {
    it('should call AdminApiService#post', () => {
      const mock: LoginForm = { login: '', password: '', productId: 0 };
      spyOn(adminApiService, 'post');

      service.login(mock);
      expect(adminApiService.post).toHaveBeenCalledWith('UserTotvs/login', mock);
    });
  });

  describe('#getUser', () => {
    it('should call AdminApiService#get', () => {
      const userId = '123';
      spyOn(adminApiService, 'get');

      service.getUser(userId);
      expect(adminApiService.get).toHaveBeenCalledWith(`UserTotvs/${userId}`);
    });
  });

  describe('#resetPassword', () => {
    it('should call AdminApiService#post', () => {
      const email = 'myemail@email.com';
      spyOn(adminApiService, 'post');

      service.resetPassword(email);
      expect(adminApiService.post).toHaveBeenCalledWith(`UserTotvs/forgotpassword/${email}`);
    });
  });
});
