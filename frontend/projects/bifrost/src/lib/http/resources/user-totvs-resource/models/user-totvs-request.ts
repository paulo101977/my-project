export interface UserTotvsRequest {
  id?: string;
  name: string;
  userName?: string;
  isActive: true;
  email: string;
  chainId?: number;
  preferredLanguage: string;
  isAdmin: boolean;
  brandList: number[];
  propertyList: number[];
}
