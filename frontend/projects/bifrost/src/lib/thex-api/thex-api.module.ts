import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { ThexAddressApiService } from './services/thex-address-api/thex-address-api.service';
import { ThexApiService } from './thex-api.service';

@NgModule({
  imports: [
    ApiBuilderModule
  ],
  providers: [ ThexApiService, ThexAddressApiService ]
})
export class ThexApiModule { }
