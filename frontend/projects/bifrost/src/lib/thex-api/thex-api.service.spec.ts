import { TestBed, inject } from '@angular/core/testing';

import { ThexApiService } from './thex-api.service';
import { ApiBuilderService } from '../api-builder/api-builder.service';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { configureApi } from '../api-builder/models/api-config';
import { ApiBuilderServiceStub } from '../api-builder/api-builder.mock';

describe('ThexApiService', () => {
  let apiBuilderService: ApiBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ThexApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  it('should be created', inject([ThexApiService], (service: ThexApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'https://thex-api-pms-fabric.azurewebsites.net/api/',
        [ApiEnvironment.Production]: 'https://thex-api.totvs.com.br/api/',
        [ApiEnvironment.Local]: 'http://localhost:5000/api/',
        [ApiEnvironment.Staging]: 'https://thex-api-pms-fabric-stg.azurewebsites.net/api/',
        [ApiEnvironment.Test]: 'https://thex-api-pms-fabric-tst.azurewebsites.net/api/',
        [ApiEnvironment.Uat]: 'https://thex-api-pms-fabric-uat.azurewebsites.net/api/',
        [ApiEnvironment.Automization]: 'https://thex-api-pms-fabric-aut.azurewebsites.net/api/',
        [ApiEnvironment.Bugfix]: 'https://thex-api-pms-fabric-bugfix.azurewebsites.net/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
