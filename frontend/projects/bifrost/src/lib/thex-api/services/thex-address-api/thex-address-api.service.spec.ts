import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { ThexApiService } from '../../thex-api.service';

import { ThexAddressApiService } from './thex-address-api.service';

describe('ThexAddressApiService', () => {
  let service: ThexAddressApiService;

  const thexApiStubService: Partial<ThexApiService> = {
    get<T>(path, options?): Observable<any> { return of({items: []}); }
  };
  let thexApiService: ThexApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ThexAddressApiService,
        {
          provide: ThexApiService,
          useValue: thexApiStubService
        }
      ]
    });

    service = TestBed.get(ThexAddressApiService);
    thexApiService = TestBed.get(ThexApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getCountries', () => {
    it('should call ThexApiService#get', () => {
      const path = 'Country?LanguageIsoCode=pt-br';
      spyOn(thexApiService, 'get');

      service.getCountries();
      expect(thexApiService.get).toHaveBeenCalledWith(path);
    });
  });

  describe('#getNationalities', () => {
    it('should call ThexApiService#get', () => {
      const path = 'nationality?Language=pt-br';
      spyOn(thexApiService, 'get');

      service.getNationalities();
      expect(thexApiService.get).toHaveBeenCalledWith(path);
    });
  });
});
