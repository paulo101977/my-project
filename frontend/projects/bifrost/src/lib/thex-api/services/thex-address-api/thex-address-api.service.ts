import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ThexApiService } from '../../thex-api.service';

@Injectable()
export class ThexAddressApiService {

  constructor(private thexApi: ThexApiService) { }

  getCountries(): Observable<any> {
    return this.thexApi.get('Country?LanguageIsoCode=pt-br');
  }

  getNationalities(): Observable<any> {
    return this.thexApi.get('nationality?Language=pt-br');
  }
}
