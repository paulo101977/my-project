import { NgModule } from '@angular/core';
import { ThexApiService } from '../thex-api.service';
import { ThexApiTestingService } from './thex-api.testing.service';

@NgModule({
  providers: [
    ThexApiTestingService,
    {provide: ThexApiService, useExisting: ThexApiTestingService}
  ]
})
export class ThexApiTestingModule {}
