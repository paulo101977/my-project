import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { TributesApiService } from './tributes-api.service';


@NgModule({
  imports: [
    ApiBuilderModule,
  ],
  providers: [ TributesApiService ]
})
export class TributesApiModule { }
