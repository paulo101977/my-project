import { Injectable } from '@angular/core';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { BaseApiService } from '../api-builder/services/base-api/base-api.service';

@Injectable()
export class TributesApiService extends BaseApiService {

  // TODO: change end points when url is defined
  configUrlEnvironments() {
    return {
      [ApiEnvironment.Development]: 'https://thex-api-taxrule-fabric.azurewebsites.net/api/',
      [ApiEnvironment.Local]: 'http://localhost:5006/api/',
      [ApiEnvironment.Production]: 'https://taxrules-api.totvs.com.br/api/',
      [ApiEnvironment.Staging]: 'https://thex-api-taxrule-fabric-stg.azurewebsites.net/api/',
      [ApiEnvironment.Test]: 'https://thex-api-taxrule-fabric-tst.azurewebsites.net/api//',
      [ApiEnvironment.Uat]: 'https://thex-api-taxrule-fabric-uat.azurewebsites.net/api/',
      [ApiEnvironment.Automization]: 'http://localhost:5006/api/',
      [ApiEnvironment.Bugfix]: 'https://thex-api-taxrule-fabric-bugfix.azurewebsites.net/api/',
    };
  }
}
