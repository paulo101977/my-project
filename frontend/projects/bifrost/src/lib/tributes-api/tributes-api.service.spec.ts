import { TestBed, inject } from '@angular/core/testing';

import { TributesApiService } from './tributes-api.service';
import { ApiBuilderService } from '../api-builder/api-builder.service';
import { ApiBuilderServiceStub } from '../api-builder/api-builder.mock';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { configureApi } from '../api-builder/models/api-config';

describe('TributeApiService', () => {
  let apiBuilderService: ApiBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TributesApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  // TODO: change end points when url is defined
  it('should be created', inject([TributesApiService], (service: TributesApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'http://localhost:5006/api/',
        [ApiEnvironment.Local]: 'http://localhost:5006/api/',
        [ApiEnvironment.Production]: 'http://localhost:5006/api/',
        [ApiEnvironment.Staging]: 'http://localhost:5006/api/',
        [ApiEnvironment.Test]: 'http://localhost:5006/api/',
        [ApiEnvironment.Uat]: 'http://localhost:5006/api/',
        [ApiEnvironment.Automization]: 'http://localhost:5006/api/',
        [ApiEnvironment.Bugfix]: 'http://localhost:5006/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
