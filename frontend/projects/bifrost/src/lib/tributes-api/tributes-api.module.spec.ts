import { TributesApiModule } from './tributes-api.module';

describe('TributeApiModule', () => {
  let adminApiModule: TributesApiModule;

  beforeEach(() => {
    adminApiModule = new TributesApiModule();
  });

  it('should create an instance', () => {
    expect(adminApiModule).toBeTruthy();
  });
});
