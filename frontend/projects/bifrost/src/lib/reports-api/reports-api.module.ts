import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { ReportsApiService } from './reports-api.service';
@NgModule({
  imports: [
    ApiBuilderModule
  ],
  providers: [ ReportsApiService ]
})
export class ReportsApiModule { }
