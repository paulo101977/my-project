import { Injectable } from '@angular/core';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { BaseApiService } from '../api-builder/services/base-api/base-api.service';

@Injectable({providedIn: 'root'})
export class ReportsApiService extends BaseApiService {
  configUrlEnvironments() {
    return {
      [ApiEnvironment.Development]: 'https://thex-api-report-fabric.azurewebsites.net/api/',
      [ApiEnvironment.Production]: 'https://thex-api-report.totvs.com.br/api/',
      [ApiEnvironment.Local]: 'http://localhost:5007/api/',
      [ApiEnvironment.Staging]: 'https://thex-api-report-fabric-stg.azurewebsites.net/api/',
      [ApiEnvironment.Test]: 'https://thex-api-report-fabric-tst.azurewebsites.net/api/',
      [ApiEnvironment.Uat]: 'https://thex-api-report-fabric-uat.azurewebsites.net/api/',
      [ApiEnvironment.Automization]: 'https://thex-api-report-fabric-aut.azurewebsites.net/api/',
      [ApiEnvironment.Bugfix]: 'https://thex-api-report-fabric-bugfix.azurewebsites.net/api/',
    };
  }
}
