import { NgModule } from '@angular/core';
import { ReportsApiTestingService } from './reports-api.testing.service';
import { ReportsApiService } from '../reports-api.service';

@NgModule({
  providers: [
    ReportsApiTestingService,
    {provide: ReportsApiService, useExisting: ReportsApiTestingService}
  ]
})
export class ReportsApiTestingModule {}
