import { TestBed, inject } from '@angular/core/testing';
import { ReportsApiService } from './reports-api.service';
import {ApiBuilderServiceStub} from '../api-builder/api-builder.mock';
import {configureApi} from '../api-builder/models/api-config';
import {ApiEnvironment} from '../api-builder/models/api-environment.enum';
import {ApiBuilderService} from '../api-builder/api-builder.service';

describe('ReportsApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ReportsApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });
  });
  it('should be created', inject([ReportsApiService], (service: ReportsApiService) => {
    expect(service).toBeTruthy();
  }));
});
