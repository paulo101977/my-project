import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable()
export class TaxRuleApiTestingService {
  get<T>(path: any, options?: any): Observable<T> {
    return of(null);
  }

  post<T>(path: any, body?: any, options?: any): Observable<T> {
    return of(null);
  }

  put<T>(path: any, body?: any, options?: any): Observable<T> {
    return of(null);
  }

  delete<T>(path: any, options?: any): Observable<T> {
    return of(null);
  }

  patch<T>(path: any, body?: any, options?: any): Observable<T> {
    return of(null);
  }
}
