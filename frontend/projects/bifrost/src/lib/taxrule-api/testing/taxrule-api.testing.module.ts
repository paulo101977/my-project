import { NgModule } from '@angular/core';
import { TaxRuleApiTestingService } from './taxrule-api.testing.service';
import { TaxRuleApiService } from './../taxrule-api.service';

@NgModule({
  providers: [
    TaxRuleApiTestingService,
    {provide: TaxRuleApiService, useExisting: TaxRuleApiTestingService}
  ]
})
export class TaxRuleApiTestingModule {}
