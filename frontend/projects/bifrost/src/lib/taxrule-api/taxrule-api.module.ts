import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { TaxRuleApiService } from './taxrule-api.service';

@NgModule({
  imports: [
    ApiBuilderModule
  ],
  providers: [TaxRuleApiService]
})
export class TaxRuleApiModule {
}
