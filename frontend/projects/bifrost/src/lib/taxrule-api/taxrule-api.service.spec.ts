import { inject, TestBed } from '@angular/core/testing';

import { TaxRuleApiService } from './taxrule-api.service';
import { ApiBuilderService } from '../api-builder/api-builder.service';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { configureApi } from '../api-builder/models/api-config';
import { ApiBuilderServiceStub } from '../api-builder/api-builder.mock';
import { configureTestSuite } from 'ng-bullet';

describe('TaxRuleApiService', () => {
  let apiBuilderService: ApiBuilderService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [
        TaxRuleApiService,
        {provide: ApiBuilderService, useClass: ApiBuilderServiceStub},
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  it('should be created', inject([TaxRuleApiService], (service: TaxRuleApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'https://thex-api-taxrule-fabric.azurewebsites.net/api/',
        [ApiEnvironment.Production]: 'https://thex-api.totvs.com.br/api/',
        [ApiEnvironment.Local]: 'http://localhost:5000/api/',
        [ApiEnvironment.Staging]: 'https://thex-api-taxrule-fabric-stg.azurewebsites.net/api/',
        [ApiEnvironment.Test]: 'https://thex-api-taxrule-fabric-tst.azurewebsites.net/api/',
        [ApiEnvironment.Uat]: 'https://thex-api-taxrule-fabric-uat.azurewebsites.net/api/',
        [ApiEnvironment.Automization]: 'https://thex-api-taxrule-fabric-aut.azurewebsites.net/api/',
        [ApiEnvironment.Bugfix]: 'https://thex-api-taxrule-fabric-bugfix.azurewebsites.net/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
