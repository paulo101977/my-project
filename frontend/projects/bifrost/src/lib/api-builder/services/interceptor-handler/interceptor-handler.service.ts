import { Injectable } from '@angular/core';
import { InterceptorDisabledState } from '../../models/interceptor-disabled-state.enum';
import { InterceptorState } from '../../models/interceptor-state';

@Injectable({
  providedIn: 'root'
})
export class InterceptorHandlerService {
  private storage: any = {};

  private getInterceptorState(interceptorName: string) {
    const storage = this.getStorage();
    return storage[interceptorName] || {};
  }

  private getStorage() {
    return this.storage;
  }

  private setInterceptorDisabledState(interceptorName: string, disabled: InterceptorDisabledState): void {
    const interceptorState = { disabled };
    this.setInterceptorState(interceptorName, interceptorState);
  }

  private setInterceptorState(interceptorName: string, data: InterceptorState): void {
    const storage = this.getStorage();
    const newStorage = {
      ...storage,
      [interceptorName]: data
    };
    this.setStorage(newStorage);
  }

  private setStorage(data: {[p: string]: InterceptorState}) {
    this.storage = { ...data };
  }

  public enable(interceptorName: string): void {
    this.setInterceptorDisabledState(interceptorName, InterceptorDisabledState.Enabled);
  }

  public disable(interceptorName: string): void {
    this.setInterceptorDisabledState(interceptorName, InterceptorDisabledState.Disabled);
  }

  public disableOnce(interceptorName: string): void {
    this.setInterceptorDisabledState(interceptorName, InterceptorDisabledState.DisabledOnce);
  }

  public shouldRun(interceptorName: string): boolean {
    const interceptorState = this.getInterceptorState(interceptorName);
    const isEnabled = (!interceptorState.disabled || interceptorState.disabled === InterceptorDisabledState.Enabled);
    const isDisabledOnce = interceptorState.disabled === InterceptorDisabledState.DisabledOnce;

    if (isEnabled) {
      return true;
    }

    if (isDisabledOnce) {
      this.enable(interceptorName);
    }

    return false;
  }
}
