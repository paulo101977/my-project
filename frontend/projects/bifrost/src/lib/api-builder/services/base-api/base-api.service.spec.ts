import { TestBed, inject } from '@angular/core/testing';

import { BaseApiService } from './base-api.service';
import { ApiBuilderService } from '../../api-builder.service';
import { ApiBuilderServiceStub } from '../../api-builder.mock';
import { API_CONFIG } from '../../models/api-config';

describe('BaseApiService', () => {
  const apiConfig = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BaseApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        { provide: API_CONFIG, useValue: apiConfig }
      ]
    });
  });

  it('should be created', inject([BaseApiService], (service: BaseApiService) => {
    expect(service).toBeTruthy();
  }));
});
