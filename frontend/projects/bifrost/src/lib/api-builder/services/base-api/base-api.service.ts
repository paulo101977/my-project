import { Inject, Injectable } from '@angular/core';
import { Api } from '../../models/api';
import { ApiBuilderService } from '../../api-builder.service';
import { API_CONFIG } from '../../models/api-config';
import { Observable } from 'rxjs';

@Injectable()
export abstract class BaseApiService {

  protected api: Api;

  constructor(
    private apiBuilder: ApiBuilderService,
    @Inject(API_CONFIG) apiConfig: any
  ) {
    const config = {
      urls: this.configUrlEnvironments(),
      environment: apiConfig.environment
    };
    this.api = this.apiBuilder.build(config);
  }

  public configUrlEnvironments(): any {
    console.error('Url configuration hasn\'t been set');
  }

  public get<T>(path, options?): Observable<T> {
    return this.api.get(path, options);
  }

  public post<T>(path, body?, options?): Observable<T> {
    return this.api.post(path, body, options);
  }

  public put<T>(path, body?, options?): Observable<T> {
    return this.api.put(path, body, options);
  }

  public patch<T>(path, body?, options?): Observable<T> {
    return this.api.patch(path, body, options);
  }

  public delete<T>(path, options?): Observable<T> {
    return this.api.delete(path, options);
  }
}

