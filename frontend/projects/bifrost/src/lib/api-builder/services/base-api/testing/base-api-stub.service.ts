import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

/**
 *
 *  Base class for all extended service from BaseApiService
 *
 *  Usage in spec (example):
 *
 *  providers: [
 *  {
 *        provide: ThexApiService,
 *        useClass: BaseApiStubService,
 *  },
 *  {
 *        provide: AdminApiService,
 *        useClass: BaseApiStubService,
 *  },
 *  ]
 */

@Injectable()
export class BaseApiStubService {
    get<T>(path: any, options?: any): Observable<T> {
        return of(null);
    }

    post<T>(path: any, body?: any, options?: any): Observable<T> {
        return of(null);
    }

    put<T>(path: any, body?: any, options?: any): Observable<T> {
        return of(null);
    }

    delete<T>(path: any, options?: any): Observable<T> {
        return of(null);
    }

    patch<T>(path: any, body?: any, options?: any): Observable<T> {
        return of(null);
    }
}
