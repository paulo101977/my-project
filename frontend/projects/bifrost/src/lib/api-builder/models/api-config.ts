import { InjectionToken } from '@angular/core';
import { ApiEnvironment } from './api-environment.enum';

export const API_CONFIG = new InjectionToken<any>('api.config');

export function configureApi(config: ApiConfig) {
  return {
    provide: API_CONFIG,
    useValue: config
  };
}

export class ApiConfig {
  urls?: any;
  environment: ApiEnvironment;
  transformers?: any[];
}

export class RequestModel {
  body?: any;
  options?: any;
}
