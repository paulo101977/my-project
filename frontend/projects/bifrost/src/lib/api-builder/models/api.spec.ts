import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { Api } from './api';
import { ApiEnvironment } from './api-environment.enum';

describe('Api Class', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let api: Api;
  const testValue = '';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);

    api = new Api(httpClient, {
      urls: {
        [ApiEnvironment.Development]: 'thex-api.com'
      },
      environment: ApiEnvironment.Development
    });
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should make a GET request', () => {
    api.get('/User', {}).subscribe( data => {
      expect(data).toEqual(testValue);
    });

    const req = httpTestingController.expectOne('thex-api.com/User');
    expect(req.request.method).toEqual('GET');
    req.flush(testValue);
  });

  it('should make a POST request', () => {
    api.post('/User', {}, {}).subscribe(data => {
      expect(data).toEqual(testValue);
    });

    const req = httpTestingController.expectOne('thex-api.com/User');
    expect(req.request.method).toEqual('POST');
    req.flush(testValue);
  });

  it('should make a PUT request', () => {
    api.put('/User', {}, {}).subscribe(data => {
      expect(data).toEqual(testValue);
    });

    const req = httpTestingController.expectOne('thex-api.com/User');
    expect(req.request.method).toEqual('PUT');
    req.flush(testValue);
  });

  it('should make a PATCH request', () => {
    api.patch('/User', {}, {}).subscribe(data => {
      expect(data).toEqual(testValue);
    });

    const req = httpTestingController.expectOne('thex-api.com/User');
    expect(req.request.method).toEqual('PATCH');
    req.flush(testValue);
  });

  it('should make a DELETE request', () => {
    api.delete('/User', {}).subscribe( data => {
      expect(data).toEqual(testValue);
    });

    const req = httpTestingController.expectOne('thex-api.com/User');
    expect(req.request.method).toEqual('DELETE');
    req.flush(testValue);
  });
});
