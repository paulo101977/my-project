export enum ApiEnvironment {
  Development = 'development',
  Local = 'local',
  Production = 'production',
  Staging = 'staging',
  Uat = 'uat',
  Test = 'test',
  Automization = 'aut',
  Bugfix = 'bugfix'
}
