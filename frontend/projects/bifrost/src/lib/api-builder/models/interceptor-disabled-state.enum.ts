export enum InterceptorDisabledState {
  Enabled = 'false',
  Disabled = 'true',
  DisabledOnce = 'once'
}
