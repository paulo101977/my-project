import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiBuilderService } from './api-builder.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [ApiBuilderService]
})
export class ApiBuilderModule { }
