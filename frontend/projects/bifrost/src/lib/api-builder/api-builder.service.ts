import { Injectable } from '@angular/core';
import { Api } from './models/api';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiBuilderService {

  constructor(private http: HttpClient) {}

  build(config: any) {
    return new Api(this.http, config);
  }
}
