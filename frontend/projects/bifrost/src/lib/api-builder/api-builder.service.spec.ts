import { TestBed, inject } from '@angular/core/testing';

import { ApiBuilderService } from './api-builder.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ApiEnvironment } from './models/api-environment.enum';
import { HttpClient } from '@angular/common/http';
import { Api } from './models/api';

describe('ApiBuilderService', () => {
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiBuilderService]
    });

    httpClient = TestBed.get(HttpClient);
  });

  it('should be created', inject([ApiBuilderService], (service: ApiBuilderService) => {
    expect(service).toBeTruthy();
    const config = {
      urls: { [ApiEnvironment.Development]: ''},
      environment: ApiEnvironment.Development
    };
    expect(service.build(config)).toEqual(new Api(httpClient, config));
  }));
});
