import { TestBed } from '@angular/core/testing';

import { LocationManagerService } from './location-manager.service';
import { CountryCodeEnum } from './models';
import { JwtHelperService } from '@auth0/angular-jwt';
import { DATA } from './mock-data';

describe('LocationManagerService', () => {
  let service;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocationManagerService]
    });

    service = new LocationManagerService();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it( 'should test getPropertyCountryCode', () => {
    expect(service.getPropertyCountryCode(DATA.JP)).toEqual('JP');
  });

  describe('on couldShow', () => {
    beforeEach( () => {
      spyOn(service, 'getPropertyCountryCode').and.callThrough();
    });

    it('should return false to not found JP', () => {
      const list = [ CountryCodeEnum.BR, CountryCodeEnum.PT ];

      expect(service.couldShow(list, DATA.JP)).toEqual(false);
    });

    it('should return true to BR', () => {
      const list = [ CountryCodeEnum.BR, CountryCodeEnum.PT ];

      expect(service.couldShow(list, DATA.BR)).toEqual(true);
    });

    it('should return true to PT', () => {
      const list = [ CountryCodeEnum.BR, CountryCodeEnum.PT ];

      expect(service.couldShow(list, DATA.PT)).toEqual(true);
    });

    it('should return false to PT', () => {
      const list = [ CountryCodeEnum.BR ];

      expect(service.couldShow(list, DATA.PT)).toEqual(false);
    });

    it('should return false to BR', () => {
      const list = [ CountryCodeEnum.PT ];

      expect(service.couldShow(list, DATA.BR)).toEqual(false);
    });
  });
});
