export  class DATA {

public static readonly PT =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\
    .eyJQcm9wZXJ0eUNvdW50cnlDb2RlIjoiUFQifQ\
    .L5bepYCGjKQP4RmZT7Vt1F_kR_tzcOb2Y-L1uLrxO0I';


  public static readonly BR =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.\
    eyJQcm9wZXJ0eUNvdW50cnlDb2RlIjoiQlIifQ.\
    HtRJMLFB5zRaV7yvjW1czgTfJ0GzyAtcHWZdcqmAnTY';

  public static readonly JP =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\
    .eyJQcm9wZXJ0eUNvdW50cnlDb2RlIjoiSlAifQ\
    .YXDyG7LLU3ISFGo5M0us-rWeXXgiGGN-WKYomBXEw84';
}

