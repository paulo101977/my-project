import { NgModule } from '@angular/core';
import { LocationManagerService } from './location-manager.service';


@NgModule({
  imports: [],
  providers: [
    LocationManagerService,
  ]
})
export class LocationManagerModule { }
