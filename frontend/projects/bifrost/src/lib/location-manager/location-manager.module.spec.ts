import { LocationManagerModule } from './location-manager.module';

describe('LocationManagerModule', () => {
  let locationManagerModule: LocationManagerModule;

  beforeEach(() => {
    locationManagerModule = new LocationManagerModule();
  });

  it('should create an instance', () => {
    expect(locationManagerModule).toBeTruthy();
  });
});
