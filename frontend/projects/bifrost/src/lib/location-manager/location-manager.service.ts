import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CountryCodeEnum } from './models';

@Injectable({
  providedIn: 'root'
})
export class LocationManagerService {

  // TODO: move controller from property-service DEBIT number 7082
  private getPropertyCountryCode(token: string): string {
    const jwtHelper =  new JwtHelperService();
    return jwtHelper.decodeToken(token).PropertyCountryCode;
  }

  /**
   *  Verify if the CountryCode was founded in array and permit the navigation
   *  or visibility of component
   *
   * @param propertyCountryList list of country codes
   * @param token the application token
   * @return true if the CountryCode was found
   */
  couldShow(propertyCountryList: Array<CountryCodeEnum>, token: string): boolean {
    const countryCode = <CountryCodeEnum>this.getPropertyCountryCode(token);

    return !!propertyCountryList && propertyCountryList.includes(countryCode);
  }
}
