import { PreferenceApiModule } from './preference-api.module';

describe('PreferenceApiModule', () => {
  let adminApiModule: PreferenceApiModule;

  beforeEach(() => {
    adminApiModule = new PreferenceApiModule();
  });

  it('should create an instance', () => {
    expect(adminApiModule).toBeTruthy();
  });
});
