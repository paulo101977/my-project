import { Injectable } from '@angular/core';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { BaseApiService } from '../api-builder/services/base-api/base-api.service';

@Injectable()
export class PreferenceApiService extends BaseApiService {

  configUrlEnvironments() {
    return {

      [ApiEnvironment.Development]: 'https://thex-api-preferences-fabric.azurewebsites.net/api/',
      [ApiEnvironment.Local]: 'http://localhost:5006/api/',
      [ApiEnvironment.Production]: 'https://thex-api-preferences.totvs.com.br/api/',
      [ApiEnvironment.Staging]: 'https://thex-api-preferences-fabric-stg.azurewebsites.net/api/',
      [ApiEnvironment.Test]: 'https://thex-api-preferences-fabric-tst.azurewebsites.net/api/',
      [ApiEnvironment.Uat]: 'https://thex-api-preferences-fabric-uat.azurewebsites.net/api/',
      [ApiEnvironment.Automization]: 'https://thex-api-preferences-fabric-aut.azurewebsites.net/api/',
      [ApiEnvironment.Bugfix]: 'https://thex-api-preferences-fabric-bugfix.azurewebsites.net/api/',
    };
  }

}
