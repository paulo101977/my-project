import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { PreferenceApiService } from './preference-api.service';


@NgModule({
  imports: [
    ApiBuilderModule,
  ],
  providers: [ PreferenceApiService ]
})
export class PreferenceApiModule { }
