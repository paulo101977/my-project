import { TestBed, inject } from '@angular/core/testing';

import { PreferenceApiService } from './preference-api.service';
import { ApiBuilderService } from '../api-builder/api-builder.service';
import { ApiBuilderServiceStub } from '../api-builder/api-builder.mock';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { configureApi } from '../api-builder/models/api-config';

describe('PreferenceApiService', () => {
  let apiBuilderService: ApiBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PreferenceApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  // TODO: add more environments cases
  it('should be created', inject([PreferenceApiService], (service: PreferenceApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'https://thex-api-preferences-fabric.azurewebsites.net/api/',
        [ApiEnvironment.Local]: 'http://localhost:5002/api/',
        [ApiEnvironment.Production]: 'https://thex-api-preferences-brazil-south.azurewebsites.net/api/',
        [ApiEnvironment.Staging]: 'https://thex-api-preferences-fabric-stg.azurewebsites.net/api/',
        [ApiEnvironment.Test]: 'https://thex-api-preferences-fabric-tst.azurewebsites.net/api/',
        [ApiEnvironment.Uat]: 'https://thex-api-preferences-fabric-uat.azurewebsites.net/api/',
        [ApiEnvironment.Automization]: 'https://thex-api-preferences-fabric-aut.azurewebsites.net/api/',
        [ApiEnvironment.Bugfix]: 'https://thex-api-preferences-fabric-bugfix.azurewebsites.net/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
