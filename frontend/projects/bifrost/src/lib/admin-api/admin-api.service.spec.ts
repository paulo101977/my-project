import { TestBed, inject } from '@angular/core/testing';

import { AdminApiService } from './admin-api.service';
import { ApiBuilderService } from '../api-builder/api-builder.service';
import { ApiBuilderServiceStub } from '../api-builder/api-builder.mock';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { configureApi } from '../api-builder/models/api-config';

describe('AdminApiService', () => {
  let apiBuilderService: ApiBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AdminApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  it('should be created', inject([AdminApiService], (service: AdminApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'https://thex-api-sa-fabric.azurewebsites.net/api/',
        [ApiEnvironment.Production]: 'https://thex-api-super-adm-brazil-south.azurewebsites.net/api/',
        [ApiEnvironment.Local]: 'http://localhost:5005/api/',
        [ApiEnvironment.Staging]: 'https://thex-api-sa-fabric-stg.azurewebsites.net/api/',
        [ApiEnvironment.Test]: 'https://thex-api-sa-fabric-tst.azurewebsites.net/api/',
        [ApiEnvironment.Uat]: 'https://thex-api-sa-fabric-uat.azurewebsites.net/api/',
        [ApiEnvironment.Automization]: 'https://thex-api-sa-fabric-aut.azurewebsites.net/api/',
        [ApiEnvironment.Bugfix]: 'https://thex-api-sa-fabric-bugfix.azurewebsites.net/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
