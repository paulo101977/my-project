import { Injectable } from '@angular/core';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { BaseApiService } from '../api-builder/services/base-api/base-api.service';

@Injectable()
export class AdminApiService extends BaseApiService {

  configUrlEnvironments() {
    return {
      [ApiEnvironment.Development]: 'http://10.21.6.43:5105/api/', // 'https://thex-api-sa-fabric.azurewebsites.net/api/',
      [ApiEnvironment.Production]: 'https://thex-api-sa.totvs.com.br/api/',
      [ApiEnvironment.Local]: 'http://localhost:5005/api/',
      [ApiEnvironment.Staging]: 'https://thex-api-sa-fabric-stg.azurewebsites.net/api/',
      [ApiEnvironment.Test]: 'https://thex-api-sa-fabric-tst.azurewebsites.net/api/',
      [ApiEnvironment.Uat]: 'https://thex-api-sa-fabric-uat.azurewebsites.net/api/',
      [ApiEnvironment.Automization]: 'https://thex-api-sa-fabric-aut.azurewebsites.net/api/',
      [ApiEnvironment.Bugfix]: 'https://thex-api-sa-fabric-bugfix.azurewebsites.net/api/',
    };
  }

}
