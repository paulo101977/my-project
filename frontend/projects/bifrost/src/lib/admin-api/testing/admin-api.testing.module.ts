import { NgModule } from '@angular/core';
import { AdminApiService } from '../admin-api.service';
import { AdminApiTestingService } from './admin-api.testing.service';

@NgModule({
  providers: [
    AdminApiTestingService,
    {provide: AdminApiService, useExisting: AdminApiTestingService}
  ]
})
export class AdminApiTestingModule {}
