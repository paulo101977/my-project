import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { AdminApiService } from './admin-api.service';


@NgModule({
  imports: [
    ApiBuilderModule,
  ],
  providers: [ AdminApiService ]
})
export class AdminApiModule { }
