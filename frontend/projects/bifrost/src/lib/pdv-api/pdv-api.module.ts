import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { PdvApiService } from './pdv-api.service';


@NgModule({
  imports: [
    ApiBuilderModule,
  ],
  providers: [ PdvApiService ]
})
export class PdvApiModule { }
