import { TestBed, inject } from '@angular/core/testing';

import { PdvApiService } from './pdv-api.service';
import { ApiBuilderService } from '../api-builder/api-builder.service';
import { ApiBuilderServiceStub } from '../api-builder/api-builder.mock';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { configureApi } from '../api-builder/models/api-config';

describe('PDVApiService', () => {
  let apiBuilderService: ApiBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PdvApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  // TODO: add more environments cases
  it('should be created', inject([PdvApiService], (service: PdvApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'https://thex-api-pdv-fabric.azurewebsites.net/api/',
        [ApiEnvironment.Local]: 'http://localhost:5001/api/',
        [ApiEnvironment.Production]: 'https://thex-api-pdv-brazil-south.azurewebsites.net/api/',
        [ApiEnvironment.Staging]: 'https://thex-api-pdv-fabric-stg.azurewebsites.net/api/',
        [ApiEnvironment.Test]: 'https://thex-api-pdv-fabric-tst.azurewebsites.net/api/',
        [ApiEnvironment.Uat]: 'https://thex-api-pdv-fabric-uat.azurewebsites.net/api/',
        [ApiEnvironment.Automization]: 'https://thex-api-pdv-fabric-aut.azurewebsites.net/api/',
        [ApiEnvironment.Bugfix]: 'https://thex-api-pdv-fabric-bugfix.azurewebsites.net/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
