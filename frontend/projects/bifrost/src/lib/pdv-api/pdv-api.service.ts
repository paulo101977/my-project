import { Injectable } from '@angular/core';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { BaseApiService } from '../api-builder/services/base-api/base-api.service';

@Injectable()
export class PdvApiService extends BaseApiService {

  // TODO: add more environments
  configUrlEnvironments() {
    return {
      [ApiEnvironment.Development]: 'https://thex-api-pdv-fabric.azurewebsites.net/api/',
      [ApiEnvironment.Local]: 'http://localhost:5001/api/',
      [ApiEnvironment.Production]: 'https://thex-api-pdv.totvs.com.br/api/',
      [ApiEnvironment.Staging]: 'https://thex-api-pdv-fabric-stg.azurewebsites.net/api/',
      [ApiEnvironment.Test]: 'https://thex-api-pdv-fabric-tst.azurewebsites.net/api/',
      [ApiEnvironment.Uat]: 'https://thex-api-pdv-fabric-uat.azurewebsites.net/api/',
      [ApiEnvironment.Automization]: 'https://thex-api-pdv-fabric-aut.azurewebsites.net/api/',
      [ApiEnvironment.Bugfix]: 'https://thex-api-pdv-fabric-bugfix.azurewebsites.net/api/',
    };
  }

}
