import { NgModule } from '@angular/core';
import { PdvApiService } from '../pdv-api.service';
import { PdvApiTestingService } from './pdv-api.testing.service';

@NgModule({
  providers: [
    PdvApiTestingService,
    {provide: PdvApiService, useExisting: PdvApiTestingService}
  ]
})
export class PdvApiTestingModule {}
