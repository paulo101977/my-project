import { HttpParams, HttpRequest } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { I18nService } from '../../i18n/i18n.service';

import { CultureInterceptor } from './culture-interceptor.service';

describe('CultureInterceptor', () => {
  let service: CultureInterceptor;

  const i18nServiceStub: Partial<I18nService> = {
    getLanguage(): string { return 'pt-br'; }
  };
  let i18nService: I18nService;

  const next: any = {
    handle: (request: HttpRequest<any>) => ({
      catch: (callback: Function) => callback({})
    })
  };
  const httpRequest: any = {
    clone: () => {},
    params: new HttpParams()
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CultureInterceptor,
        {
          provide: I18nService,
          useValue: i18nServiceStub
        }
      ]
    });

    service = TestBed.get(CultureInterceptor);
    i18nService = TestBed.get(I18nService);
    httpRequest.params = new HttpParams();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#intercept', () => {
    it('should append a culture param', () => {
      let newRequest: any;
      httpRequest.clone = (request: any) => {
        newRequest = request;
      };

      expect(httpRequest.params.toString()).toEqual('');

      service.intercept(httpRequest, next);

      expect(newRequest.params.toString()).toEqual('culture=pt-br');
    });
  });
});
