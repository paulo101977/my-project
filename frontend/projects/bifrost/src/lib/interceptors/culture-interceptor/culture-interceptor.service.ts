import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { I18nService } from '../../i18n/i18n.service';

@Injectable()
export class CultureInterceptor implements HttpInterceptor {
  constructor(private i18nService: I18nService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const preferredCulture = this.i18nService.getLanguage();

    if (req.params.has('culture')) {
      req.params.delete('culture');
    }
    req = req.clone({
      params: req.params.append('culture', preferredCulture)
    });

    return next.handle(req);
  }
}
