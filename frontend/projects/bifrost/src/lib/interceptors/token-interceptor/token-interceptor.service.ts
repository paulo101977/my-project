import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { InterceptorHandlerService } from '../../api-builder/services/interceptor-handler/interceptor-handler.service';
import { TokenManagerService } from '../../storage/services/token-manager.service';
import { APP_NAME } from '../../helpers/application/application-name-config';

@Injectable({ providedIn: 'root' })
export class TokenInterceptor implements HttpInterceptor {
  private readonly INTERCEPTOR_STORAGE_NAME = 'auth';

  constructor(
    @Inject(APP_NAME) protected appName: string,
    protected interceptorHandler: InterceptorHandlerService,
    protected tokenService: TokenManagerService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.shouldRun()) {
      return next.handle(request);
    }

    const token = this.getToken(this.appName);
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
        },
      });
    }

    return next.handle(request);
  }

  public getToken(appName: string): string {
    let token = this.tokenService.getToken(appName);
    if (!token) {
      token = this.tokenService.getExternalToken();
    }
    return token;
  }

  public disable() {
    this.interceptorHandler.disable(this.INTERCEPTOR_STORAGE_NAME);
  }

  public disableOnce() {
    this.interceptorHandler.disableOnce(this.INTERCEPTOR_STORAGE_NAME);
  }

  public enable() {
    this.interceptorHandler.enable(this.INTERCEPTOR_STORAGE_NAME);
  }

  private shouldRun() {
    return this.interceptorHandler.shouldRun(this.INTERCEPTOR_STORAGE_NAME);
  }
}
