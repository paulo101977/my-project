import { HttpRequest } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { TokenInterceptor } from './token-interceptor.service';
import { TokenManagerService } from '../../storage/services/token-manager.service';
import { InterceptorHandlerService } from '../../api-builder/services/interceptor-handler/interceptor-handler.service';
import { configureAppName } from '../../helpers/application/application-name-config';

describe('TokenInterceptor', () => {
  let service: TokenInterceptor;
  const appNameConfigProvider = configureAppName('token');
  // @ts-ignore
  const interceptorHandlerStubService: Partial<InterceptorHandlerService> = {
    disable: () => {},
    disableOnce: () => {},
    enable: () => {},
    shouldRun: () => {},
  };
  let interceptorHandlerService: InterceptorHandlerService;

  // @ts-ignore
  const tokenManagerServiceStub: Partial<TokenManagerService> = {
    getToken: () => 'token',
  };
  let tokenInterceptor: TokenInterceptor;

  const err: any = { status: 500 };
  const next: any = {
    handle: (request: HttpRequest<any>) => ({
      catch: (callback: Function) => callback(err)
    })
  };
  const httpRequest: any = {
    clone: () => {}
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TokenInterceptor,
        {
          provide: InterceptorHandlerService,
          useValue: interceptorHandlerStubService
        },
        {
          provide: TokenManagerService,
          useValue: tokenManagerServiceStub
        },
        appNameConfigProvider
      ]
    });

    service = TestBed.get(TokenInterceptor);
    interceptorHandlerService = TestBed.get(InterceptorHandlerService);
    tokenInterceptor = TestBed.get(TokenInterceptor);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#intercept', () => {
    it('should set the bearer if there is an auth token', () => {
      const token = 'token';
      const headers = {
        setHeaders: {
          Authorization: `Bearer ${token}`,
        },
      };

      spyOn(interceptorHandlerService, 'shouldRun').and.returnValue(true);
      spyOn(tokenInterceptor, 'getToken').and.returnValue(token);
      spyOn(httpRequest, 'clone').and.callThrough();

      service.intercept(httpRequest, next);

      expect(httpRequest.clone).toHaveBeenCalledWith(headers);
    });

    it('shouldn\'t set the bearer if there is no auth token', () => {
      const token = null;

      spyOn(interceptorHandlerService, 'shouldRun').and.returnValue(true);
      spyOn(tokenInterceptor, 'getToken').and.returnValue(token);
      spyOn(httpRequest, 'clone').and.callThrough();

      service.intercept(httpRequest, next);

      expect(httpRequest.clone).not.toHaveBeenCalled();
    });

    it('shouldn\'t set the bearer if the interceptor was disabled', () => {
      spyOn(interceptorHandlerService, 'shouldRun').and.returnValue(false);
      spyOn(httpRequest, 'clone').and.callThrough();

      service.intercept(httpRequest, next);

      expect(httpRequest.clone).not.toHaveBeenCalled();
    });
  });

  describe('#disable', () => {
    it ('should call interceptorHandlerService#disable', () => {
      spyOn(interceptorHandlerService, 'disable');

      service.disable();
      expect(interceptorHandlerService.disable).toHaveBeenCalledWith(service['INTERCEPTOR_STORAGE_NAME']);
    });
  });

  describe('#disableOnce', () => {
    it ('should call interceptorHandlerService#disableOnce', () => {
      spyOn(interceptorHandlerService, 'disableOnce');

      service.disableOnce();
      expect(interceptorHandlerService.disableOnce).toHaveBeenCalledWith(service['INTERCEPTOR_STORAGE_NAME']);
    });
  });

  describe('#enable', () => {
    it ('should call interceptorHandlerService#enable', () => {
      spyOn(interceptorHandlerService, 'enable');

      service.enable();
      expect(interceptorHandlerService.enable).toHaveBeenCalledWith(service['INTERCEPTOR_STORAGE_NAME']);
    });
  });
});
