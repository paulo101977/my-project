import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThfNotificationService } from '@totvs/thf-ui';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DetailErrorMessageAPI } from './detail-error-message-api';

@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private notification: ThfNotificationService,
    private translate: TranslateService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const notifyError = (err: any) => {
      const errorMessage = this.getErrorMessage(err);

      errorMessage && this.notification.error(errorMessage);
      return throwError(err);
    };

    return next.handle(req)
      .pipe(
        catchError(notifyError)
      );
  }

  private getErrorMessage(error) {
    const shouldHandleError = error.status === 400 || error.status === 500;

    if (shouldHandleError) {
      const errorBody = error.hasOwnProperty('_body') ? JSON.parse(error._body) : error.error;
      const details: DetailErrorMessageAPI[] = errorBody.details || [];
      return this.getMessageFromDetails(details);
    }

    if (error.status === 403) {
      return this.translate.instant('alert.permissionDenied');
    }

    return this.translate.instant('commomData.errorMessage');
  }

  private getMessageFromDetails(details: DetailErrorMessageAPI[]) {
    if (!details.length) {
      return this.translate.instant('commomData.errorMessage');
    }

    return details
      .map(detail => detail.message)
      .join('\n');
  }
}
