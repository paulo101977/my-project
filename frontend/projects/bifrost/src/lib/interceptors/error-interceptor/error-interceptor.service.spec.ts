import { HttpRequest } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { ThfNotification, ThfNotificationService } from '@totvs/thf-ui';
import { throwError } from 'rxjs';

import { ErrorInterceptor } from './error-interceptor.service';

describe('ErrorInterceptor', () => {
  let service: ErrorInterceptor;

  const notificationStubService: Partial<ThfNotificationService> = {
    error(notification: ThfNotification | string): void {}
  };
  let notificationService: ThfNotificationService;

  const translateStub: Partial<TranslateService> = {
    instant(key: string | Array<string>, interpolateParams?: Object): string | any { return 'text'; }
  };
  let translateService: TranslateService;

  const generateNext = (err): any => {
    return {
      handle: (request: HttpRequest<any>) => {
        return throwError(err);
      }
    };
  };
  const httpRequest: any = {
    clone: () => {}
  };

  const intercept = (next) => {
    service.intercept(httpRequest, next).subscribe(() => {}, () => {});
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ErrorInterceptor,
        {
          provide: ThfNotificationService,
          useValue: notificationStubService
        },
        {
          provide: TranslateService,
          useValue: translateStub
        }
      ]
    });

    service = TestBed.get(ErrorInterceptor);
    notificationService = TestBed.get(ThfNotificationService);
    translateService = TestBed.get(TranslateService);

    spyOn(notificationService, 'error');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#intercept', () => {
    it('shouldn show an error message when status is different than 400 and 500', () => {
      const err = { status: 401 };
      const next = generateNext(err);

      intercept(next);

      expect(notificationService.error).toHaveBeenCalled();
    });

    describe('when status is 400', () => {
      it('should give preference to error messages on _body', () => {
        const errorDetails = { details: [{message: 'error message'}] };
        const bodyDetails = { details: [{message: 'body details'}] };
        const err = {
          status: 400,
          error: errorDetails,
          _body: JSON.stringify(bodyDetails)
        };
        const next = generateNext(err);

        intercept(next);

        expect(notificationService.error).toHaveBeenCalledWith(bodyDetails.details[0].message);
      });

      it('should show error messages from error when no _body is present', () => {
        const errorDetails = { details: [{message: 'error message'}] };
        const err = {
          status: 400,
          error: errorDetails
        };
        const next = generateNext(err);

        intercept(next);

        expect(notificationService.error).toHaveBeenCalledWith(errorDetails.details[0].message);
      });

      it('should show common error message if none is sent', () => {
        const errorDetails = { details: [] };
        const err = {
          status: 400,
          error: errorDetails
        };
        const next = generateNext(err);

        intercept(next);

        expect(notificationService.error).toHaveBeenCalled();
      });
    });

    describe('when status is 500', () => {
      it('should give preference to error messages on _body', () => {
        const errorDetails = { details: [{message: 'error message'}] };
        const bodyDetails = { details: [{message: 'body details'}] };
        const err = {
          status: 500,
          error: errorDetails,
          _body: JSON.stringify(bodyDetails)
        };
        const next = generateNext(err);

        intercept(next);

        expect(notificationService.error).toHaveBeenCalledWith(bodyDetails.details[0].message);
      });

      it('should show error messages from error when no _body is present', () => {
        const errorDetails = { details: [{message: 'error message'}] };
        const err = {
          status: 500,
          error: errorDetails
        };
        const next = generateNext(err);

        intercept(next);

        expect(notificationService.error).toHaveBeenCalledWith(errorDetails.details[0].message);
      });

      it('should show common error message if none is sent', () => {
        const errorDetails = { details: [] };
        const err = {
          status: 500,
          error: errorDetails
        };
        const next = generateNext(err);

        intercept(next);

        expect(notificationService.error).toHaveBeenCalled();
      });
    });
  });
});
