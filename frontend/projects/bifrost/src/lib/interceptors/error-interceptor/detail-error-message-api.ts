export interface DetailErrorMessageAPI {
  guid: string;
  code: string;
  message: string;
  detailedMessage: string;
}
