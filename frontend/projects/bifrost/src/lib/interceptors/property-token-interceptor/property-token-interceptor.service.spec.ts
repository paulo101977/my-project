import { TestBed, inject } from '@angular/core/testing';

import { PropertyTokenInterceptorService } from './property-token-interceptor.service';

describe('PropertyTokenInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PropertyTokenInterceptorService]
    });
  });

  it('should be created', inject([PropertyTokenInterceptorService], (service: PropertyTokenInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
