import {Injectable} from '@angular/core';
import {TokenInterceptor} from '../token-interceptor/token-interceptor.service';


@Injectable({ providedIn: 'root' })
export class PropertyTokenInterceptorService extends TokenInterceptor {

  public getToken(appName: string): string {
    const propertyToken =  this.tokenService.getCurrentPropertyToken();
    return propertyToken ? propertyToken : super.getToken(appName);
  }

}

