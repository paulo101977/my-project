import { SupportApiModule } from './support-api.module';

describe('SupportApiModule', () => {
  let supportApiModule: SupportApiModule;

  beforeEach(() => {
    supportApiModule = new SupportApiModule();
  });

  it('should create an instance', () => {
    expect(supportApiModule).toBeTruthy();
  });
});
