import { NgModule } from '@angular/core';
import { ApiBuilderModule } from '../api-builder/api-builder.module';
import { SupportApiService } from './support-api.service';

@NgModule({
  imports: [
    ApiBuilderModule,
  ],
  providers: [ SupportApiService ]
})
export class SupportApiModule { }
