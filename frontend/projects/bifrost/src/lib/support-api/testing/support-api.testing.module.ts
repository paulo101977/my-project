import { NgModule } from '@angular/core';
import { SupportingApiTestingService } from './support-api.testing.service';
import { SupportApiService } from '../support-api.service';

@NgModule({
  providers: [
    SupportingApiTestingService,
    {provide: SupportApiService, useExisting: SupportingApiTestingService}
  ]
})
export class SupportApiTestingModule {}
