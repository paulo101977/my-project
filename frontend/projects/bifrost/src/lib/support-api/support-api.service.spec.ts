import { TestBed, inject } from '@angular/core/testing';

import { SupportApiService } from './support-api.service';
import { ApiBuilderService } from '../api-builder/api-builder.service';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { configureApi } from '../api-builder/models/api-config';
import { ApiBuilderServiceStub } from '../api-builder/api-builder.mock';

describe('SupportApiService', () => {
  let apiBuilderService: ApiBuilderService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SupportApiService,
        { provide: ApiBuilderService, useClass: ApiBuilderServiceStub },
        configureApi({
          environment: ApiEnvironment.Development
        })
      ]
    });

    apiBuilderService = TestBed.get(ApiBuilderService);
    spyOn(apiBuilderService, 'build').and.callThrough();
  });

  it('should be created', inject([SupportApiService], (service: SupportApiService) => {
    const config = {
      urls: {
        [ApiEnvironment.Development]: 'https://thex-api-taxrule-fabric.azurewebsites.net/api/',
        [ApiEnvironment.Local]: 'http://localhost:5016/api/',
        [ApiEnvironment.Production]: 'https://taxrules-api.totvs.com.br/api/',
        [ApiEnvironment.Staging]: 'https://thex-api-taxrule-fabric-stg.azurewebsites.net/api/',
        [ApiEnvironment.Test]: 'https://thex-api-taxrule-fabric-tst.azurewebsites.net/api//',
        [ApiEnvironment.Uat]: 'https://thex-api-taxrule-fabric-uat.azurewebsites.net/api/',
        [ApiEnvironment.Automization]: 'http://localhost:5016/api/',
        [ApiEnvironment.Bugfix]: 'https://thex-api-taxrule-fabric-bugfix.azurewebsites.net/api/',
      },
      environment: ApiEnvironment.Development
    };
    expect(service).toBeTruthy();
    expect(apiBuilderService.build).toHaveBeenCalledWith(config);
  }));
});
