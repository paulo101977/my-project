import { Injectable } from '@angular/core';
import { ApiEnvironment } from '../api-builder/models/api-environment.enum';
import { BaseApiService } from '../api-builder/services/base-api/base-api.service';

@Injectable()
export class SupportApiService extends BaseApiService {

  // TODO: change end points when url is defined
  configUrlEnvironments() {
    return {
      [ApiEnvironment.Development]: 'https://thex-api-support-fabric.azurewebsites.net/api/',
      [ApiEnvironment.Local]: 'http://localhost:5016/api/',
      [ApiEnvironment.Production]: 'https://thex-api-support-brazil-south.azurewebsites.net/api/',
      [ApiEnvironment.Staging]: 'https://thex-api-support-fabric-stg.azurewebsites.net/api/',
      [ApiEnvironment.Test]: 'https://thex-api-support-fabric-tst.azurewebsites.net/api//',
      [ApiEnvironment.Uat]: 'https://thex-api-support-fabric-uat.azurewebsites.net/api/',
      [ApiEnvironment.Automization]: 'http://localhost:5016/api/',
      [ApiEnvironment.Bugfix]: 'https://thex-api-support-fabric-bugfix.azurewebsites.net/api/',
    };
  }
}
