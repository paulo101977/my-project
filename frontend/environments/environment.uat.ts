import { ApiEnvironment } from '@inovacaocmnet/thx-bifrost';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=uat` then `environment.uat.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  enableProdMode: true,
  isAutomationMode: false,
  urlBase: 'https://thex-api-pms-fabric-uat.azurewebsites.net/api/',
  urlSuperAdmin: 'https://thex-api-sa-fabric-uat.azurewebsites.net/api/',
  apiConfig: {
    environment: ApiEnvironment.Uat
  },
  // version
  appVersion: require('../package.json').version,
  adminVersion: require('../projects/admin/package.json').version,
  centralVersion: require('../projects/central/package.json').version,
  rmsVersion: require('../projects/rms/package.json').version,
};
