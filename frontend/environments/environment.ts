import { ApiEnvironment } from '@inovacaocmnet/thx-bifrost';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=dev` then `environment.dev.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  enableProdMode: false,
  isAutomationMode: false,
  urlBase: 'http://10.21.6.43:5100/api/', // 'https://thex-api-pms-fabric.azurewebsites.net/api/',
  urlSuperAdmin: 'http://10.21.6.43:5105/api/', // 'https://thex-api-sa-fabric.azurewebsites.net/api/',
  apiConfig: {
    environment: ApiEnvironment.Development
  },
  // version
  appVersion: require('../package.json').version,
  adminVersion: require('../projects/admin/package.json').version,
  centralVersion: require('../projects/central/package.json').version,
  rmsVersion: require('../projects/rms/package.json').version,
};
