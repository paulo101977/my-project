import { ApiEnvironment } from '@inovacaocmnet/thx-bifrost';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=tst` then `environment.tst.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  enableProdMode: true,
  isAutomationMode: true,
  urlBase: 'https://thex-api-pms-fabric-tst.azurewebsites.net/api/',
  urlSuperAdmin: 'https://thex-api-sa-fabric-tst.azurewebsites.net/api/',
  apiConfig: {
    environment: ApiEnvironment.Test
  },
  // version
  appVersion: require('../package.json').version,
  adminVersion: require('../projects/admin/package.json').version,
  centralVersion: require('../projects/central/package.json').version,
  rmsVersion: require('../projects/rms/package.json').version,
};
